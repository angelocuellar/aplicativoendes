package gob.inei.dnce.components;

import gob.inei.dnce.util.Util;

import java.util.List;

import android.content.Context;
import android.util.AttributeSet;

public abstract class NumberField extends TextBoxField {
	
	private Object minValue;
	private Object maxValue;
	//private List<Object> excepVals;
	private boolean rangoOk = true;
	private String mensajeRango;
	private Object omision;

	public NumberField(Context context) {
		this(context, true);
	}
	
	public NumberField(Context context, boolean hasTextWatcher) {
		super(context);
		if(hasTextWatcher){
			textWatcher = new Util.MyTextWatcher(this, Util.MyTextWatcher.NUMERO);
			this.addTextChangedListener(textWatcher);
		}
	}

	public NumberField(Context context, AttributeSet attrs) {
		super(context, attrs);
		textWatcher = new Util.MyTextWatcher(this, Util.MyTextWatcher.NUMERO);
		this.addTextChangedListener(textWatcher);
	}

	@Override
	public abstract Class<?> getValueClass();

//	@Override
//	public void callback() {
//	}
//	
	public Object getMinValue() {
		return minValue;
	}

	public void setMinValue(Object minValue) {
		this.minValue = minValue;
	}

	public Object getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Object maxValue) {
		this.maxValue = maxValue;
	}

	public Object getOmision() {
		return omision;
	}

	public void setOmision(Object omision) {
		this.omision = omision;
	}

	public abstract List<? extends Object> getExcepValues();

	public abstract void setExcepValues(List<? extends Object> excepVals);

	public boolean isRangoOk() {
		return rangoOk;
	}
	
	public void setRangoOk(boolean rangoOk) {
		this.rangoOk = rangoOk;
	}

	public abstract void verificarRangos();

	public String getMensajeRango() {
		return mensajeRango;
	}

	public void setMensajeRango(String mensajeRango) {
		this.mensajeRango = mensajeRango;
	}
}
