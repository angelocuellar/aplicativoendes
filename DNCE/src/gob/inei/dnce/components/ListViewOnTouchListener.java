package gob.inei.dnce.components;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class ListViewOnTouchListener implements OnTouchListener {

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		v.getParent().requestDisallowInterceptTouchEvent(true);
		return false;
	}

}
