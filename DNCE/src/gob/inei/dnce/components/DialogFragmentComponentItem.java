package gob.inei.dnce.components;

import android.widget.LinearLayout;

/**
 * 
 * @author Rdelacruz
 * @since 27/02/2015
 * @version 07/10/2015 
 */
public abstract class DialogFragmentComponentItem 
extends DialogFragmentComponentWithoutXML
{
	
	private String codigo;
	private String item;

	protected abstract void buildFields(String codigo) throws Exception;
	protected abstract void cargarDatosOrThrow(String codigo) throws Exception;
	protected abstract void addComponents(LinearLayout contenedor, String codigo) throws Exception;
	
	@Override
	protected final void buildFields() throws Exception {
		buildFields(codigo);
	}
	
	protected final void cargarDatosOrThrow() throws Exception {
		cargarDatosOrThrow(codigo);	
	}
	
	@Override
	protected final void addComponents(LinearLayout contenedor) throws Exception {
		addComponents(contenedor,codigo);
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
//	public String getCodigo() {
//		return codigo;
//	}	
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	
	@Override
	protected boolean onAceptar() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
//	@Override
//	public void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//	}
//	@Override
//	public void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//	}
	
}
