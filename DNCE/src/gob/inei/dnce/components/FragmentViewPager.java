package gob.inei.dnce.components;

import java.util.List;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class FragmentViewPager extends ViewPager {
	
	private boolean isPagingEnabled=true;
	private List<FragmentForm> fragments;
	
	public FragmentViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public FragmentForm getItem(int i) {
        return fragments.get(i);
    }

    public int getCount() {
        return fragments.size();
    }
    
    public void setFragments(List<FragmentForm> fragments){
    	this.fragments = fragments;
    }
	
	@Override
	public void setCurrentItem(int item) {
		super.setCurrentItem(item);
	}
	
	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
		super.setCurrentItem(item, smoothScroll);
	}

    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	/*if(Masterpage.prevPage == Masterpage.CAPITULO04) this.isPagingEnabled=false;
    	else this.isPagingEnabled=true;*/
    	return this.isPagingEnabled?super.onTouchEvent(event) : false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
    	return this.isPagingEnabled?super.onInterceptTouchEvent(event):false;
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }
}