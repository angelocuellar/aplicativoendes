package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;

/**
 * 
 * @author Rdelacruz
 * @since 27/02/2015
 */
public class CheckGroupOtherField 
extends LinearLayout
implements OnCheckedChangeListener
{

	private final String valorPorDefecto;
	private final int altoCheckBox;
	private final int anchoCheckBox;
	private final Map<String,CheckBoxField> buttons;
	private LinearLayout lyUltimo;
	
	private OnCheckedChangeListener onCheckedChangeListenerAlterno;
	
	public CheckGroupOtherField(Context context, 
			String valorPorDefecto, 
			int altoCheckBox, int anchoCheckBox) {
		super(context);
		this.valorPorDefecto = valorPorDefecto;
		this.altoCheckBox = altoCheckBox;
		this.anchoCheckBox = anchoCheckBox;
		this.setOrientation(LinearLayout.VERTICAL);
		this.buttons = new HashMap<String, CheckBoxField>();
	}
	
	public boolean isChecked(String key) {
		CheckBoxField cb = buttons.get(key);
		return cb!=null && cb.isChecked();
	}
	
	public List<String> getKeysSeleccionados() {
		List<String> lista = new ArrayList<String>();
		for(Entry<String,CheckBoxField> e : buttons.entrySet()) {
			CheckBoxField cb = e.getValue();
			if(cb.isChecked())
				lista.add(e.getKey());
		}
		return lista;
	}
	
	public CheckBoxField getCheckBoxField(String key) {
		return buttons.get(key);
	}

	public CheckBoxField agregar(String key, int etiquetaID) {
		return agregar(key,getContext().getResources().getString(etiquetaID));
	}
	
	
	private void agregar(View ... views) {
		LinearLayout ly = new LinearLayout(getContext());
		ly.setOrientation(LinearLayout.HORIZONTAL);
		//ly.setLayoutParams(new LayoutParams(anchoCheckBox, altoCheckBox + 10));
		
		for(View v : views) {
			ly.addView(v);
		}
		
		addView(ly);		
		lyUltimo = ly;
	}
	
	public CheckBoxField agregar(String key, String etiqueta) {
		CheckBoxField cb = agregarCheck(key,etiqueta);
		agregar(cb);	
		return cb;
	}
	
	private CheckBoxField agregarCheck(String key, String etiqueta) {
		String vpd = valorPorDefecto;
		
		//igualando la longitd del valor por defecto a la longitud del key
		if(vpd.length()==1) {
			int n = key.length() - vpd.length();  
			for(int i=0; i<n; i++) {
				vpd = vpd.concat(valorPorDefecto);
			}	
		}
		
		CheckBoxField cb = new CheckBoxField(getContext(),-1, key+":"+vpd)
		.size(altoCheckBox, anchoCheckBox);
		cb.setText(etiqueta);
		cb.setOnCheckedChangeListener(this);
		cb.setOnCheckedChangeListenerAlterno(this);
		buttons.put(key, cb);
		return cb;
	}
	
	public Data agregarEspecifique(String key, int etiquetaID) {
		return agregarEspecifique(key,getContext().getResources().getString(etiquetaID));
	}
	public Data agregarEspecifique(String key, String etiqueta) {
		Data d = new Data();
		d.checkBox = agregarCheck(key, etiqueta);
		d.checkBox.size(altoCheckBox, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		d.textField = new TextField(getContext())
		.maxLength(50)
		.size(altoCheckBox, LinearLayout.LayoutParams.MATCH_PARENT)
		.hint(R.string.especifique);
		
		d.checkBox.setTag(R.id.textview, d.textField);
		
		Util.lockView(getContext(), true, d.textField);
		
		LinearLayout ly = new LinearLayout(getContext());
		ly.setOrientation(LinearLayout.HORIZONTAL);
		ly.setLayoutParams(new LayoutParams(anchoCheckBox, altoCheckBox + 10));		
		ly.addView(d.checkBox);
		ly.addView(d.textField);
		
		agregar(ly);		
		return d;
	}


	public class Data {
		public CheckBoxField checkBox;
		public TextField textField;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {		
		if(onCheckedChangeListenerAlterno!=null)
			onCheckedChangeListenerAlterno.onCheckedChanged(buttonView, isChecked);
		
		TextField tf = (TextField) buttonView.getTag(R.id.textview);
		if(tf!=null) {
			//Util.lockView(getContext(), !isChecked, tf);
			Util.lockView(getContext(), !buttonView.isChecked(), tf);
		}
	}

	
	//public OnCheckedChangeListener getOnCheckedChangeListenerAlterno() {
	//	return onCheckedChangeListenerAlterno;
	//}

	public void setOnCheckedChangeListenerAlterno(
			OnCheckedChangeListener onCheckedChangeListenerAlterno) {
		this.onCheckedChangeListenerAlterno = onCheckedChangeListenerAlterno;
	}

	public int getAltoCheckBox() {
		return altoCheckBox;
	}

	public int getAnchoCheckBox() {
		return anchoCheckBox;
	}

	public LinearLayout getLyUltimo() {
		return lyUltimo;
	}
	
	
}
