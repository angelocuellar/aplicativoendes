package gob.inei.dnce.components;

import java.lang.reflect.Field;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

/**
 * 
 * @author Rdelacruz
 * @since 27/02/2015
 * @version 27/02/2015
 */
public abstract class FragmentFormWithoutXML 
extends FragmentForm
{

	private boolean eliminarDobleToque = false;
	
	//protected abstract void buildFields() throws Exception;	
	protected abstract void addComponents(LinearLayout contenedor) throws Exception;
	public abstract boolean grabarOrThrow() throws Exception;
	public abstract void cargarDatosOrThrow() throws Exception;
	
	@Override
	public final void onAttach(Activity activity) {
		this.parent = (MasterActivity)activity;
		super.onAttach(activity);		
	}
	
	@Override
	protected void buildFields() throws Exception {
		
	}

	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		esCargaInicial = true;
		
		rootView = createUI();
		boolean existenComponentes = getClass().getDeclaredFields().length>0;
		if(existenComponentes) {
			initObjectsWithoutXML(this, rootView);
			enlazarCajas();
			listening();
			
			if(eliminarDobleToque)
				eliminarDobleTouchEnComponentes(this);
		}	
		
		
		return rootView;
	}
	
	//Se ejecuta despues de onCreateView()
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {		
//		if(eliminarDobleToque)
//			eliminarDobleTouchEnComponentes(this);		
//		super.onActivityCreated(savedInstanceState);
//	}
	
	//FragmentForm.initObjects() coloca setFocusableInTouchMode(true) a todos los componentes
	private void eliminarDobleTouchEnComponentes(Object container) {
		try {
			setFocusableFields(container,false);
		} catch (IllegalArgumentException e) {
			manejarExcepcion(e);
		} catch (IllegalAccessException e) {
			manejarExcepcion(e);
		}
	}
	
	protected static void setFocusableFields(Object container, boolean focus) 
			throws IllegalArgumentException, IllegalAccessException {
		//Field[] cajas = this.getClass().getDeclaredFields();
		Field[] cajas = container.getClass().getDeclaredFields();
		for (Field caja : cajas) {
			View v  = null;
			if(caja!=null) {
				 if(caja.getType() == CheckBoxField.class)
					 v = (View)caja.get(container);
				 else if(caja.getType() == SpinnerField.class)
					 v = (View)caja.get(container);
			}				
			if(v!=null)
				v.setFocusableInTouchMode(false);
		}
	}
		
	@Override
	protected final View createUI() {
		try {
			//Instancia los componentes
			buildFields();
			
			
			ScrollView viewRoot = createForm();
			LinearLayout layout = (LinearLayout) viewRoot.getChildAt(0);

			Contenedor contenedor = new Contenedor(getActivity());			
			//Agrega los componentes al contenedor
			addComponents(contenedor);
			
			layout.addView(contenedor);			
			return viewRoot;
		} catch (Exception e) {
			manejarExcepcion(e);
		}		
		return null;
	}

	protected void manejarExcepcion(Exception e) {
		String mensaje = "ERROR: "+e.toString();
		
		Toast toast = Toast.makeText(this.getActivity(),mensaje,Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
		toast.show();
		
		e.printStackTrace();
	}
	
	public abstract boolean grabarSinValidar() throws Exception;
	
	@Override
	public final boolean grabar() {
		try {
			boolean b = grabarOrThrow();
			if(b) {
				ToastMessage.msgBox(this.getActivity(), "GUARDADO",
					ToastMessage.MESSAGE_INFO,
					ToastMessage.DURATION_SHORT);//ToastMessage.DURATION_LONG);						
			}
			return b;
		} catch (Exception e) {
			manejarExcepcion(e);
		}
		return false;
	}
		
	
	@Override
	public final void cargarDatos() {
		esCargaInicial = true;//para evitar se llamen los focos de los componentes
		antesCargarDatos();		
		try {
			cargarDatosOrThrow();
		} catch (Exception e) {
			manejarExcepcion(e);
		}				
		despuesCargarDatos();
		//esCargaInicial = false;//NO SIRVE. Debe estar en OnInterceptTouchListener
	}
	
	protected void antesCargarDatos() {
		
	}
	
	private void despuesCargarDatos() {
		MasterActivity a = this.parent;
		a.setTitulo(getTitulo());
		a.setSubTitulo(getSubtitulo());	
	}
	
	public String getTitulo() {
		return "";
	}
	
	public String getSubtitulo() {
		return "";
	}
		
	public final int getAltoComponente() {
		return altoComponente; 
	}
	
	public class Contenedor 
	extends LinearLayout
	{
		public Contenedor(Context context) {			
			super(context);
			this.setOrientation(LinearLayout.VERTICAL);
		}

		@Override
		public boolean onInterceptTouchEvent(MotionEvent ev) {		
			esCargaInicial = false;
			onFinalizarCargaInicial();
			return super.onInterceptTouchEvent(ev);
		}

//		@Override
//		protected boolean onRequestFocusInDescendants(int direction,
//				Rect previouslyFocusedRect) {
//			return false;
//		}
		
	}
	
	protected void onFinalizarCargaInicial() {
		
	}
	
	public boolean isEliminarDobleToque() {
		return eliminarDobleToque;
	}
	public void setEliminarDobleToque(boolean eliminarDobleToque) {
		this.eliminarDobleToque = eliminarDobleToque;
	}
	
}
