package gob.inei.dnce.components;

import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.LinearLayout.LayoutParams;

/**
 * 
 * @author ajulcamoro
 *
 */
public class SpinnerField extends Spinner implements FieldComponent {
	
	private static String TAG = "SpinnerField";
	private List<Object> keys = null;
	private List<String> callbacks;
	private EntitySpinnerAdapter<? extends Entity> adapter;
	private Context context;
	private boolean isTouched = false;
	private Entity entityTitle = null;
	
	//Ramon
	//private OnTouchListener onTouchListenerAlterno;
	//Ramon
	private AdapterView.OnItemSelectedListener onItemSelectedListenerAlterno;
		
	public SpinnerField(Context context) {
		this(context, Spinner.MODE_DIALOG);
	}

	public SpinnerField(Context context, int mode) {
		super(context, mode);
		this.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.btn_dropdown)); //R.drawable.bmi_onesided_spinner
		this.setPadding(2,2,2, 2);
		this.context = context;
	}

	public SpinnerField(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public SpinnerField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public SpinnerField(Context context, AttributeSet attrs, int defStyle, int mode) {
		super(context, attrs, defStyle, mode);
		this.context = context;
	}

	/**
	 * Establece el adaptador que alimenta al spinner y agrega una clave a cada uno de los elementos
	 * @param adapter Instancia de <code>SpinnerAdapter</code> que alimenta al spinner
	 * @param keys Juego de claves para asignar a cada uno de los elementos del spinner
	 */
	@SuppressWarnings("unchecked")
	public void setAdapterWithKey(SpinnerAdapter adapter, List<Object> keys) {
		super.setAdapter(adapter);
		this.keys = keys; 
		if (adapter instanceof EntitySpinnerAdapter<?>) {
			this.adapter = (EntitySpinnerAdapter<? extends Entity>) adapter;
		}
	}
	
	public List<? extends Entity> getEntityList() {
		if (adapter != null) {
			return adapter.getEntities();
		}
		return null;
	}

	/**
	 * Establece el adaptador que alimenta al spinner y agrega una clave a cada uno de los elementos segun el elemento insertado
	 * @param adapter Instancia de <code>SpinnerAdapter</code> que alimenta al spinner
	 */
	public void setAdapterWithoutKey(SpinnerAdapter adapter) {
		super.setAdapter(adapter);
		List<Object> keys = new ArrayList<Object>();
		for (int i = 0; i < adapter.getCount(); i++) {
			keys.add(adapter.getItem(i));
		}		
		this.keys = keys; 
	}
	
	/**
	 * 
	 * @param keys
	 */
	public void setKeys(List<Object> keys){
		this.keys = keys;
	}
	
	public List<Object> getKeys(){
		return keys;
	}
	
	/**
	 * Devuelve la clave del item seleccionado. Si no se ha suministrado un juego de claves para los elementos, 
	 * entonces este metodo devuelve el indice seleccionado. Si no se seleccionado ningun elemento este 
	 * metodo devuelve <code>null</code>
	 * @return Devuelve la clave del item seleccionado
	 */
	public Object getSelectedItemKey() {
		if( super.getSelectedItemPosition() ==-1 ) return null;
		if(this.keys == null) return super.getSelectedItemPosition();
		return keys.get(super.getSelectedItemPosition());
	}
	
	public int getSize() {
		return keys==null ? 0 : keys.size();
	}
	
	/**
	 * 
	 * @param key
	 */
	public void setSelectionKey(Object key) {
		//if(key == null) return;
		if(keys == null) return;
		int position = keys.indexOf(key); 
		if (position == -1) {
			position = 0;
		}
		super.setSelection(position);
		
		/*if(key == null) {
			super.setSelection(keys.indexOf(null));
			return;
		}
		if(keys == null) return;
		super.setSelection(keys.indexOf(key.toString()));*/
	}
	
	public Object getFirstKey(){
		if(keys == null) return null;
		return keys.get(0);
	}
	
	public void setSize( int alto, int ancho) {		
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		this.setLayoutParams(lp);
//        this.setPadding(this.getPaddingLeft() + (int) (10.0f * scale + 0.5f),
//                this.getPaddingTop(),
//                this.getPaddingRight(),
//                this.getPaddingBottom());
	}	
	public void setSizeFloat( float alto, float ancho) {		
		LayoutParams lp = new LayoutParams((int)Util.getTamañoEscaladoFloat(this.context, ancho), (int)Util.getTamañoEscaladoFloat(this.context, alto, 0.3f));
		this.setLayoutParams(lp);
//        this.setPadding(this.getPaddingLeft() + (int) (10.0f * scale + 0.5f),
//                this.getPaddingTop(),
//                this.getPaddingRight(),
//                this.getPaddingBottom());
	}
	
	public void addCallBack(String callBackMethodName) {
		if (this.callbacks == null) {
			this.callbacks = new ArrayList<String>();
		}
		this.callbacks.add(callBackMethodName);
	}
	
	public List<String> getCallbacks() {
		return this.callbacks;
	}
	
	public void setReadOnly(boolean readOnly) {
//		setEnabled(!readOnly);   
		setClickable(!readOnly);  
	}
	
	public void setReadOnly() {
		this.setReadOnly(true);
	}
	
	private <T extends Entity>void setTitle(T ent){
		List<Entity> entity = new ArrayList<Entity>();
		entity.add(entityTitle=ent);     
        EntitySpinnerAdapter<Entity> adapter = new EntitySpinnerAdapter<Entity>(
        		this.context, android.R.layout.simple_spinner_item, entity);
        List<Object> keysAdapter = new ArrayList<Object>();
		keysAdapter.add(null);
		this.setAdapterWithKey(adapter, keysAdapter);
	}
	
	public Entity gettitle() { return this.entityTitle; }
	
//	public SpinnerField size(int alto, int ancho) { this.setSize(alto,ancho); return this; }
	public SpinnerField size(float alto, float ancho) { this.setSizeFloat(alto,ancho); return this; }
	public SpinnerField readOnly() { this.setReadOnly();; return this; }
	public SpinnerField adapter(SpinnerAdapter adapter) { setAdapterWithoutKey(adapter); return this; }
    public SpinnerField callback(String methodCallBackName){ this.addCallBack(methodCallBackName); return this;}
    public SpinnerField title(Entity ent) { this.setTitle(ent); return this; }

	@Override
	public void setValue(Object aValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getValue() {
		return getSelectedItem();
	}

	@Override
	public Class<?> getValueClass() {
		return Object.class;
	}

	//Ramon
	public void onItemSelectedListenerAlterno(AdapterView<?> parent, View view, int position, long id) {
		if(onItemSelectedListenerAlterno!=null)
			onItemSelectedListenerAlterno.onItemSelected(parent, view, position, id);
	}

	//Ramon
	public void setOnItemSelectedListenerAlterno(
			AdapterView.OnItemSelectedListener onItemSelectedListenerAlterno) {
		this.onItemSelectedListenerAlterno = onItemSelectedListenerAlterno;
	}
	
//	public void limpiar(){
//		Object[] obj = new Object[] { null };
//        String[] values = new String[] { "-- SELECCIONE --" };        
//        this.setKeys(Arrays.asList(obj));        
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context,
//                      R.layout.spinner_item, values);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        this.setAdapter(adapter);
//	}
	
	public void limpiar(FieldComponent... views){
		List<Entity> entity = new ArrayList<Entity>();
		entity.add(null);     
        EntitySpinnerAdapter<Entity> adapter = new EntitySpinnerAdapter<Entity>(
        		this.context, android.R.layout.simple_spinner_item, entity);
        List<Object> keysAdapter = new ArrayList<Object>();
		keysAdapter.add(null);
		this.setAdapterWithKey(adapter, keysAdapter);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {		
		isTouched = true;
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onFocusChanged(boolean gainFocus, int direction,
			Rect previouslyFocusedRect) {
		if(!gainFocus){
			isTouched = false;
		}
		super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
	}
	
	public boolean isTouched() {
		return isTouched;
	}
	
	public void setTouched(boolean touched) {
		this.isTouched = touched;
	}
		
}