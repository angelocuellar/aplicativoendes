package gob.inei.dnce.components;
import gob.inei.dnce.R;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.IFormComponent;
import gob.inei.dnce.interfaces.IRadioGroupOptions;
import gob.inei.dnce.util.Util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

@SuppressLint("ResourceAsColor")
public class TableComponent extends LinearLayout {
	private Context context;
	private String[] beanProperties;
	private List<Header> headers;
	private List<? extends IDetailEntityComponent> data;
	private List<IDetailEntityComponent> dataDetail;
	private List<Map<String, Object>> dataMaps;
	private List<Class<?>> clases;
	private List<ALIGN> aligns;
	private Map<Integer, String> callbacks;
	private List<int[]> textos;
	private int[] colors;
	private TableComponentAdapter adapter;
	private List<IDetailEntityComponent> filas;
	private List<Map<String, Object>> filasMaps;
	private ListViewComponent listView;	
	private int ancho, scrollW;
	private int alto;
	private int altoColumnHeader;
	private int altoColumnData;
	private int textSize = 16;
	private int textSizeData = 16;
	private int textColorData;
	private IFormComponent parent;
	private boolean validaCampos;
	private List<Object> lstObjets;
	private boolean isEnabled;
	private boolean hasOrder;
	private boolean statusFilter;
	private int styleHeader;
	private String fieldFilter;
	private List<TCColor> lstColor;
	public HorizontalScrollView hScroll=null;
	private HashMap<Integer, Integer> mapRadioGroups;
	private enum TIPO {BORDE, FILA}
	private enum THEADER {HEADER, FOOTER}
	private boolean repintarTodo = false;

	public enum ALIGN {LEFT, CENTER, RIGHT};
	
	public TableComponent(Context context, IFormComponent parent) {
		this(context, parent, -1);
	}
	public TableComponent(Context context, IFormComponent parent, int styleHeader) {
		this(context, parent, styleHeader, false);
	}
	public TableComponent(Context context, IFormComponent parent, boolean hasOrder) {
		this(context, parent, -1, hasOrder);
	}
	public TableComponent(Context context, IFormComponent parent, int styleHeader, boolean hasOrder) {
		super(context);
		this.context = context;
		this.setSize(300, 400);
		setHeaderHeight(40);
		setDataColumHeight(40);
		this.textColorData = R.color.black;
		this.isEnabled = true;
		this.hasOrder = hasOrder;
		this.styleHeader = styleHeader;
		this.lstColor = new ArrayList<TCColor>();
		if(this.hasOrder) addHeader(R.string.tc_n, 0.5f);
		this.parent = parent;
		this.statusFilter = false;
	}
	
	public IDetailEntityComponent getSelectedItem() {
		if (getSelectedPosition() == -1) 
			return null;
		return data.get(getSelectedPosition());
	}
	
	public int getSelectedPosition() {
		if (listView == null) 
			return -1;
		return listView.getPosicion(); 
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
		if (listView != null) {
			listView.setEnabled(enabled);
		}
		if (!enabled) {
			if (data != null) {
				for (IDetailEntityComponent d : data) {
					d.cleanEntity();
				}
			}
		}
		recargar();
		super.setEnabled(enabled);
	}
	
	public TableComponentAdapter getAdapter() {
		return adapter;
	}

	public void setHeaderHeight(int headerHeight) {
		final float scale = this.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			headerHeight = (int) (headerHeight * scale + 0.3f);
		}
		this.altoColumnHeader = headerHeight;
	}
	
	public void setDataColumHeight(int height) {
		final float scale = this.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			height = (int) (height * scale + 0.3f);
		}
		this.altoColumnData = height;
	}
	
	public void setHeaderHeightFloat(float headerHeight) {
		final float scale = this.getResources().getDisplayMetrics().density;
//		if (scale >= 2) {
		headerHeight = (int)(Util.getTamañoEscaladoFloat(this.context, headerHeight))*1.2f;
//		}
		this.altoColumnHeader = (int)headerHeight;
		
	}
	public void setDataColumHeightFloat(float height) {
		final float scale = this.getResources().getDisplayMetrics().density;
//		if (scale >= 2) {
		height = (int)(Util.getTamañoEscaladoFloat(this.context, height))*1.3f;
//		}
		this.altoColumnData = (int)height;
	}

	public void setHeaderTextSize(int headerText) {
		this.textSize = headerText;
	}

	private Map<Integer, String> getCallbacks() {
		if (callbacks == null) {
			callbacks = new HashMap<Integer, String>();
		}
		return callbacks;
	}
	
	public void addCallback(int index, String callback) {
		getCallbacks().put(index, callback);
	}

	public void setSizeFloat(float alto, float ancho, int scrollW) {
		this.scrollW = scrollW!=-1 ? Util.getTamañoEscalado(this.context, scrollW): scrollW;
		this.ancho = (int)Util.getTamañoEscaladoFloat(this.context, ancho) ;
		this.alto  = (int)Util.getTamañoEscaladoFloat(this.context, alto);
		LayoutParams lp = new LayoutParams(this.ancho, this.alto);
		this.setLayoutParams(lp);
	}
	
	public void setSize(int alto, int ancho) {
		setSize(alto, ancho, -1);
	}
	
	public void setSize(int alto, int ancho, int scrollW) {
//		this.scrollW = scrollW;
//		this.ancho = ancho;
//		this.alto = alto;
//		LayoutParams lp = new LayoutParams(ancho, alto);
//		Log.e(getClass().getSimpleName(), "Ancho parametro:" + ancho);
//		Log.e(getClass().getSimpleName(), "Alto parametro:" + alto);
//		this.size(alto + (statusFilter?55:0), ancho+5, scrollW);
		this.scrollW = scrollW!=-1 ? Util.getTamañoEscalado(this.context, scrollW) /*+ Util.getTamañoEscalado(this.context, 10) */: scrollW;
		this.ancho = Util.getTamañoEscalado(this.context, ancho + 5) /*+ Util.getTamañoEscalado(this.context, 10)*/;
//		this.ancho = Util.getTamañoEscalado(this.context, ancho);
		this.alto = Util.getTamañoEscaladoTableComponent(this.context, alto, 0.1f) /* + Util.getTamañoEscalado(this.context, (statusFilter?55:0), 0.3f)*/;
//		this.ancho = ancho;
//		this.alto = alto;
//		Log.e(getClass().getSimpleName(), "this.ancho:" + this.ancho);
//		Log.e(getClass().getSimpleName(), "this.alto:" + this.alto);
		LayoutParams lp = new LayoutParams(this.ancho, this.alto);
		this.setLayoutParams(lp);
	}
	private void setSizeHeight(int alto) {
		this.alto = Util.getTamañoEscaladoTableComponent(this.context, alto, 0.3f) + Util.getTamañoEscaladoTableComponent(this.context, (statusFilter?55:0), 0.3f);		
		LayoutParams lp = new LayoutParams(this.ancho, this.alto);
		this.setLayoutParams(lp);
	}

	
	private TextField setFilter() {
		TextField txtFilter = null;
		if(this.statusFilter){
			txtFilter = new TextField(context).size(50, 200).alfanumerico().hint(R.string.lblBusqueda);
			listView.setTextFilterEnabled(true);
			txtFilter.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(adapter == null) return;
					adapter.getFilter().filter(s.toString());
				}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
				@Override
				public void afterTextChanged(Editable s) {}
			});
		}
		return txtFilter;
	}
	
//	public TableComponent size(int alto, int ancho) { size(alto, ancho, -1); return this;}
	public TableComponent size(float alto, float ancho) { this.setSizeFloat(alto, ancho,-1); return this;}
	public TableComponent size(int alto, int ancho, int scrollW) { this.setSize(alto, ancho, scrollW); return this;}
//	public TableComponent headerHeight(int headerHeight) { this.setHeaderHeight(headerHeight); return this;}
	public TableComponent headerHeight(float headerHeight) { this.setHeaderHeightFloat(headerHeight); return this;}
	public TableComponent headerTextSize(int headerTextSize) { this.setHeaderTextSize(headerTextSize); return this;}
//	public TableComponent dataColumHeight(int height) { this.setDataColumHeight(height); return this;}
	public TableComponent dataColumHeight(float height) { this.setDataColumHeightFloat(height); return this;}
	public TableComponent filter(String text) { this.statusFilter=true; this.fieldFilter=text; return this;}
	public TableComponent filter() { this.filter("0"); return this;}
	public TableComponent rePintar() { this.repintarTodo = true; return this;}
	
	public LabelComponent getHeaderPosition(int columna){
		if(columna >= getHeaders().size()) return null;
		return getHeaders().get(columna).header;
	}
	
	private List<Header> getHeaders() {
		if (headers == null) 
			headers = new ArrayList<TableComponent.Header>();
		return headers;
	}
	
	private List<Class<?>> getClases() {
		if (clases == null) 
			clases = new ArrayList<Class<?>>();
		return clases;
	}
	
	private List<ALIGN> getAligns() {
		if (aligns == null) 
			aligns = new ArrayList<ALIGN>();
		return aligns;
	}
	
	private List<int[]> getTextos() {
		if (textos == null) 
			textos = new ArrayList<int[]>();
		return textos;
	}
	
	public void addColorHeader(int... color) {
		if(color.length>0)
			colors = color;
		setColorHeader();
	}
	
	private void setColorHeader(){
		if(colors == null) return;
		for(int i=0;i<headers.size();i++){
			getHeaders().get(i).header.setColorFondo(colors.length-1>=i?colors[i]:colors[colors.length-1]);
		}
	}
	
	//CSSC
	public void setFirstHeaderWidth(int ancho){
		setLayoutWidth(getHeaders().get(0).header,ancho);
	}

	public void addHeader(int stringId, float colSpan, float rowSpan, Class<?> clase, ALIGN align, int... texts) {
		Context context = this.styleHeader!=-1?new ContextThemeWrapper(this.context, this.styleHeader):this.context;
		LabelComponent label = new LabelComponent(context).text(stringId).centrar()
				.negrita().textSize(textSize)
				.size(LayoutParams.MATCH_PARENT, 0, colSpan, false);
		getHeaders().add(new Header(label, colSpan, rowSpan, THEADER.HEADER));
		getClases().add(clase);
		getAligns().add(align);
		getTextos().add(texts);
	}
	public void addHeader(int stringId, float colSpan, Class<?> clase, ALIGN align, int... texts) {
		this.addHeader(stringId, colSpan, (float)1, clase, align, texts);
	}
	public void addHeader(int stringId, float colSpan, ALIGN align) {
		this.addHeader(stringId, colSpan, (float)1, LabelComponent.class, align);
	}
	public void addHeader(int stringId, float colSpan, Class<?> clase) {
		this.addHeader(stringId, colSpan, (float)1, clase, ALIGN.CENTER);
	}
	public void addHeader(int stringId, float colSpan) {
		this.addHeader(stringId, colSpan, (float)1, LabelComponent.class, ALIGN.CENTER);
	}
	public void addHeader(int stringId, float colSpan,  Class<?> clase, int... texts) {
		this.addHeader(stringId, colSpan, (float)1, clase, ALIGN.CENTER, texts);
	}
	public void addHeader(int stringId, Class<?> clase, ALIGN align, int... texts) {
		this.addHeader(stringId, 1, clase, align, texts);
	}
	public void addHeader(int stringId, Class<?> clase, int... texts) {
		this.addHeader(stringId, clase, ALIGN.CENTER, texts);
	}
	public void addHeader(int stringId, ALIGN align) {
		this.addHeader(stringId, LabelComponent.class, align);
	}
	public void addHeader(int stringId) {
		this.addHeader(stringId, LabelComponent.class);
	}
	
	public void addFooter(int stringId, float colSpan, float rowSpan, Class<?> clase, ALIGN align, int... texts) {
		Context context = this.styleHeader!=-1?new ContextThemeWrapper(this.context, this.styleHeader):this.context;
		LabelComponent label = new LabelComponent(context).text(stringId).centrar()
				.negrita().textSize(textSize)
				.size(LayoutParams.MATCH_PARENT, 0, colSpan, false);
		getHeaders().add(new Header(label, colSpan, rowSpan, THEADER.FOOTER));
		getClases().add(clase);
		getAligns().add(align);
		getTextos().add(texts);
	}
	
	public void setData(List<? extends IDetailEntityComponent> data, String... properties) {
		this.beanProperties = addFieldNro(properties);
		this.data = data;
		recargar();
	}
	
//	public void colorCell(int row, int column, int color) {
//        if (listView != null && adapter != null && row >= 0 && row < adapter.getCount()) {
//            View itemView = listView.getChildAt(row - listView.getFirstVisiblePosition());
//            if (itemView != null) {
//                View cellView = itemView.findViewById(column);
//                if (cellView != null) {
//                    cellView.setBackgroundColor(color);
//                }
//            }
//        }
//    }
	
	public void setMaps(List<Map<String, Object>> maps, String... properties) {
		this.beanProperties = addFieldNro(properties);
//		this.data = data;
		this.dataMaps = maps;
		recargarMaps();
	}
	
	public List<IDetailEntityComponent> getData() {
		return this.dataDetail;
	}
	
	private String[] addFieldNro(String... properties){
		if(hasOrder){ 
			List<String> lst = Util.convertArraytoList(properties);
			lst.add(0, "Nº");
			properties = Util.convertListtoArray(lst);
		}
		return properties;
	}
	
	public View getTableView() {
		GridComponent tc = new GridComponent(context, 1);		
		//tc.setColorFondo(R.color.griscabece);
		tc.colorFondo(R.color.griscabece);
		LinearLayout header = getHeader(); 
		setColorHeader();
		listView = new ListViewComponent(context, this);
		listView.setBackgroundColor(context.getResources().getColor(R.color.WhiteSmoke));
		listView.setLayoutParams(new AbsListView.LayoutParams(ancho, alto - altoColumnHeader - 10));
		listView.setOnTouchListener(new ListViewOnTouchListener());
		recargar();
		recargarMaps();
		tc.addComponent(setFilter());
		tc.addComponent(header);
		tc.addComponent(listView);
		tc.buildTable();
		this.addView(tc);
//		this.size(alto + (statusFilter?55:0), ancho+5, scrollW);
//		this.size(alto + (statusFilter?55:0), ancho+5);
		this.setSizeHeight(alto);
		hScroll = new HorizontalScrollView(context);
		hScroll.addView(this);
		return setScroll(hScroll);
	}
	
	public View setScroll(View hScroll){
		int margin = Util.getTamañoEscalado(this.context, 20);
		int anchoV = scrollW != -1 ? (scrollW > FragmentForm.SIZE_DISPLAY.x ? FragmentForm.SIZE_DISPLAY.x : scrollW ) 
				: FragmentForm.SIZE_DISPLAY.x;
//		int ancho = this.ancho <= FragmentForm.SIZE_DISPLAY.x-20? this.ancho:FragmentForm.SIZE_DISPLAY.x-20; 
		int ancho = this.ancho <= anchoV - margin ? this.ancho + Util.getTamañoEscalado(this.context, 5): anchoV - margin;
		Log.e(getClass().getSimpleName(), "this.ancho:"+this.ancho);
		Log.e(getClass().getSimpleName(), "FragmentForm.SIZE_DISPLAY.x:" + (FragmentForm.SIZE_DISPLAY.x));
		Log.e(getClass().getSimpleName(), "FragmentForm.SIZE_DISPLAY.x-20:" + (FragmentForm.SIZE_DISPLAY.x-20));
//		int ancho = this.ancho <= FragmentForm.SIZE_DISPLAY.x-20? this.ancho:FragmentForm.SIZE_DISPLAY.x-20; 
		Log.e(getClass().getSimpleName(), "nuevo ancho:"+ancho);
		Log.e(getClass().getSimpleName(), "nuevo scrollW:"+scrollW);
		Log.e(getClass().getSimpleName(), "nuevo margin:"+margin);
		LayoutParams lp = new LayoutParams(ancho, alto);
//		lp.gravity = Gravity.CENTER;
		hScroll.setLayoutParams(lp);
		
		return hScroll;
	}
	
	private void recargar() {
		mapRadioGroups = new HashMap<Integer, Integer>();
		dataDetail = new ArrayList<IDetailEntityComponent>();
		if (data != null) {
			//for (IDetailEntityComponent d : data) {
			for (int i = 0; i < data.size(); i++) {	
				dataDetail.add(data.get(i));
				if (data.get(i) instanceof IRadioGroupOptions) {
					mapRadioGroups.put(i, ((IRadioGroupOptions)data.get(i)).getNroOpcion());
				}
			}				
		}
		
		adapter = new TableComponentAdapter(context, dataDetail);		
		listView.setAdapter(adapter);
		setSeleccion();
	}
	
	private void recargarMaps() {
//		mapRadioGroups = new HashMap<Integer, Integer>();
//		dataDetail = new ArrayList<IDetailEntityComponent>();
//		if (data != null) {
//			//for (IDetailEntityComponent d : data) {
//			for (int i = 0; i < data.size(); i++) {	
//				dataDetail.add(data.get(i));
//				if (data.get(i) instanceof IRadioGroupOptions) {
//					mapRadioGroups.put(i, ((IRadioGroupOptions)data.get(i)).getNroOpcion());
//				}
//			}				
//		}
		if (dataMaps == null) {
			dataMaps = new ArrayList<Map<String,Object>>();
		}
		TableComponentMapAdapter adapter = new TableComponentMapAdapter(context, dataMaps);		
		listView.setAdapter(adapter);
		setSeleccion();
	}
	
	public void reloadAllData(List<? extends IDetailEntityComponent> newData){
		dataDetail = (List<IDetailEntityComponent>)newData;
		reloadData();
	}
	
	public void reloadAllDataForced(List<? extends IDetailEntityComponent> newData){
		adapter.notifyDataSetChanged();
		adapter.clear();
        for(int i = 0, l = newData.size(); i < l; i++){
        	adapter.add(newData.get(i));
        }
        dataDetail = (ArrayList)((ArrayList)newData).clone();
//        		(List<IDetailEntityComponent>)newData;
        filas = dataDetail;
        adapter.notifyDataSetInvalidated();
	}
	
	public void refreshDataForced(IDetailEntityComponent object) {
		adapter.notifyDataSetChanged();
		List<IDetailEntityComponent> lst = (ArrayList)((ArrayList)dataDetail).clone();
		adapter.clear();
        for(int i = 0, l = lst.size(); i < l; i++){
        	if(lst.get(i).equals(object)){
        		adapter.add(object);
        		dataDetail.set(i, object);
        	} else {
        		adapter.add(lst.get(i));
        	}
        }
        filas = dataDetail;
        adapter.notifyDataSetInvalidated();
	}
	
	public void reloadData(){
		filas = dataDetail;
		adapter.notifyDataSetChanged();
	}
	
	private void setSeleccion(){
		listView.setSelection(listView.getPosicion()+1);
		listView.setPosicion(-1);
		listView.setPosicionVisible(-1);
	}
	
	public void setSeleccionPosition(int row){
		listView.setSelection(row);		
	}
	
	public ListViewComponent getListView() {				
		return listView;
	}
	
	public void setCellTextColor(int row, int col, int textColor) {
		setCellColor(row, col, R.color.blanco, true, textSizeData, textColor);
	}
	
	public void setCellTextColor2(int row, int col, int textColor) {
		setCellColor(row, col, R.color.achurado, true, textSizeData, textColor);
	}
	
	public void setCellTextColor3(int row, int col, int textColor) {
		setCellColor(row, col, R.color.transparente, true, textSizeData, textColor);
	}
	
	public void setCellColor(int row, int col, int color) {
		setCellColor(row, col, color, false);
	}
	public void setCellColor(int row, int col, boolean bold) {
		setCellColor(row, col, R.color.transparente, bold);
	}
	public void setCellColor(int row, int col, int color, boolean bold) {
		setCellColor(row, col, color, bold, textSizeData);
	}
	public void setCellColor(int row, int col, int color, boolean bold, int sizeText) {
		setCellColor(row, col, color, bold, sizeText, textColorData);
	}
	public void setCellColor(int row, int col, int color, boolean bold, int sizeText, int textColor) {
		setCellColor(row, col, color, bold, sizeText, textColor, true);
	}

	
	private void setCellColor(int row, int col, int color, boolean bold, int sizeText, int textColor, boolean flag) {
		setCellColor(TIPO.FILA, true, row, col, color, null, null, bold, sizeText, textColor, flag);
	}
	private void setCellColor(TIPO tipo, boolean status, int row, int col, int color, String[] campos, String value, boolean bold, 
			int sizeText, int textColor, boolean flag) {
		lstColor.add(clearTCColor(new TCColor(tipo, status, row, col, color, campos, value, bold, sizeText, textColor)));
		if(data!=null && flag) adapter.notifyDataSetChanged();
	}
	
	public void resetCellColor() {
		resetCellColor(false);
	}
	public void resetCellColor(boolean activated) {
		for(int x=0;x<lstColor.size();x++)
			resetCellColor(x, activated);
	}
	public void resetCellColor(int key) {
		resetCellColor(key, false);
	}
	public void resetCellColor(int key, boolean activated) {
		if(lstColor.size()>key && key>=0){
			TCColor tcc = lstColor.get(key);
			if(tcc.tipo == TIPO.FILA)
				tcc.flag = activated;
		}
	}
	
	private TCColor clearTCColor(TCColor tcColor){
		for(TCColor tcc : lstColor){
			if(tcc.campos==null){
				if(tcc.rowColor == tcColor.rowColor && tcc.colColor == tcColor.colColor){
					lstColor.remove(tcc);
					break;
				}
			}
		}
		return tcColor;
	}
	
	public List<Object> getLstObjets() {
		return lstObjets;
	}
	public void setBorder(String... campos) {
		setBorder(R.color.blue, campos);
	}
	public void setBorder(int color, String... campos) {
		setBorder(0, color, campos);
	}
	public void setBorder(int col, int color, String... campos) {
		setCellColor(TIPO.BORDE, false, -5, col, color, campos, "", false, textSizeData, textColorData, false);
	}
	
	public void setBorderRow(int row){
		setBorderRow(row, true);
	}
	public void setBorderRow(int row, boolean state){
		setBorderRow(row, 0, state);
	}
	public void setBorderRow(int row, int col, boolean state){
		setBorderRow(row, col, state, R.color.blue);
	}
	public void setBorderRow(int row, boolean state, int color){
		setBorderRow(row, 0, state, color);
	}
	public void setBorderRow(int row, int col, boolean state, int color){
		setCellColor(TIPO.BORDE, state, row, col, color, null, null, false, textSizeData, textColorData, false);
	}
	
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion) {
		setColorCondition(campo, condicion, -1);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, boolean bold) {
		setColorCondition(campo, condicion, -1, bold);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, boolean bold, int sizeText) {
		setColorCondition(campo, condicion, -1, bold, sizeText);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, boolean bold, int sizeText, int textColor) {
		setColorCondition(campo, condicion, -1, bold, sizeText, textColor);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, int col) {
		setColorCondition(campo, condicion, col, false);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, int col, boolean bold) {
		setColorCondition(campo, condicion, col, bold, textSizeData);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, int col, boolean bold, int sizeText) {
		setColorCondition(campo, condicion, col, bold, sizeText, textColorData);
	}
	public <T>void setColorCondition(String campo, HashMap<T, T> condicion, int col, boolean bold, int sizeText, int textColor) {
		for(Map.Entry<T, T> tt:condicion.entrySet()){
			setCellColor(TIPO.FILA, false, -5, col, (Integer)tt.getValue(), new String[]{campo}, (String)tt.getKey(), bold, sizeText, textColor, false);
		}
	}
	
	public void setSelectorData(int selector){
		listView.setSelector(selector);
	}
	
	private LinearLayout getHeader() {		
		LinearLayout h = new LinearLayout(TableComponent.this.context);
		h.setOrientation(LinearLayout.HORIZONTAL);
		h.setBackgroundColor(context.getResources().getColor(R.color.griscabece));
		h.setClickable(false);
		h.setFocusable(false);
		h.setFocusableInTouchMode(false);
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ancho, altoColumnHeader);
		llp.gravity = Gravity.CENTER;
		llp.height = altoColumnHeader;
		h.setLayoutParams(llp);
		for (int i = 0; i < headers.size(); i++) {
			headers.get(i).header.setTextSize(textSize);
			h.addView(headers.get(i).header, getHeaders().get(i).header.getLayoutParams());
			if (i < headers.size() -1) {
				h.addView(getBarraVertical(R.color.griscabece, 2));
			}
		}
		
		return h;
	}
	
	public Object getItem(int position) {
		if (adapter == null) 
			return null;
		return adapter.getItem(position);
	}
	
	private class Header {
		LabelComponent header;
		float colSpan;
		float rowSpan;
		THEADER tipo;
		public Header(LabelComponent header, float colSpan, float rowSpan, THEADER tipo) {
			super();
			this.header = header;
			this.colSpan = colSpan;
			this.rowSpan = rowSpan;
			this.tipo = tipo;
		}
		//CSSC
//		public float getColSpan() {
//			return colSpan;
//		}
//		public void setColSpan(float colSpan) {
//			this.colSpan = colSpan;
//		}		
	}
	
	protected Field[] getFieldsToView(Object item) {
		return item.getClass().getDeclaredFields();
	}
	
	public class TableComponentAdapter extends ArrayAdapter<IDetailEntityComponent> {
//		private List<IDetailEntityComponent> filas;
		private Filter filter;
		public TableComponentAdapter(Context context, List<IDetailEntityComponent> objects) {
			super(context, -1, objects);
			filas = new ArrayList<IDetailEntityComponent>();
			filas.addAll(objects);
			lstObjets = new ArrayList<Object>();
			if (TableComponent.this.statusFilter) {
				filter = new DataFilter();	
			}
		}
				
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout view;
			if(TableComponent.this.repintarTodo) {
				view = new LinearLayout(TableComponent.this.context);
				view.setDescendantFocusability(android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS);
				view.setOrientation(LinearLayout.HORIZONTAL);	
				if (getItem(position).isTitle()) {
					view.addView(getObjectClass(getClases().get(0), 0, position), getHeaders().get(0).header.getLayoutParams());
					view.addView(getBarraVertical(R.color.griscabece, 2));
				} else {
					for (int i = 0; i < beanProperties.length; i++) {
						view.addView(getObjectClass(getClases().get(i), i, position), getHeaders().get(i).header.getLayoutParams());
						if (i < beanProperties.length -1) {
							view.addView(getBarraVertical(R.color.griscabece, 2));
						}
					}	
				}
				view.setTag(getItem(position));
			} else {
				if (convertView == null) {
					view = new LinearLayout(TableComponent.this.context);
					view.setDescendantFocusability(android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS);
					view.setOrientation(LinearLayout.HORIZONTAL);	
					if (getItem(position).isTitle()) {
						view.addView(getObjectClass(getClases().get(0), 0, position), getHeaders().get(0).header.getLayoutParams());
						view.addView(getBarraVertical(R.color.griscabece, 2));
					} else {
						for (int i = 0; i < beanProperties.length; i++) {
							view.addView(getObjectClass(getClases().get(i), i, position), getHeaders().get(i).header.getLayoutParams());
							if (i < beanProperties.length -1) {
								view.addView(getBarraVertical(R.color.griscabece, 2));
							}
						}	
					}
					view.setTag(getItem(position));
				} else {
					view = (LinearLayout)convertView;
					view.setTag(getItem(position));
				}
			}

//			Object item = getItem(position);
			Object item = view.getTag(); /*648647867*/
			Field[] fields = getFieldsToView(item);
			validaCampos = setBorder(fields, item, position, false);
			int count = 0;
			for (int i = 0; i < view.getChildCount(); i=i+2) {			
				for (int x = count; x < beanProperties.length; x++) {
					if(beanProperties[x].equals("Nº")){
						setValueObject(view.getChildAt(i), String.valueOf(position+1), x, position);
						break;
					}
					boolean encontroValor = false;
					for (int j = 0; j < fields.length; j++) {
						if (getItem(position) instanceof Entity && getItem(position).isTitle()) {
							break;
						}
						if (fields[j].getName().equalsIgnoreCase(beanProperties[x])) {
							encontroValor = true;
							try {
								if (fields[j].get(item) == null) {
									setValueObject(view.getChildAt(i), "", x, position);
								} else {
									setValueObject(view.getChildAt(i), Util.getText(fields[j].get(item).toString()), x, position);
								}
								x = beanProperties.length;
								break;
							} catch (IllegalArgumentException e) {
								Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
							} catch (IllegalAccessException e) {
								Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
							} 
						}
					}
					if (!encontroValor) {
						try {
							Method method; //= item.getClass().getDeclaredMethod(beanProperties[(i+1)/2]);
							if (getItem(position) instanceof Entity && getItem(position).isTitle()) {
								method = getItem(position).getClass().getDeclaredMethod("getTitle");
							} else {
								method = item.getClass().getDeclaredMethod(beanProperties[(i+1)/2]);
							}
//							String valor = (String) method.invoke(item);
							Object o = method.invoke(item);
							String valor = Util.getText(o);
							setValueObject(view.getChildAt(i), valor, x, position);
						} catch (NoSuchMethodException e) {
							//Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
						} catch (IllegalArgumentException e) {
							Log.e("TableComponent.TableComponentAdapter.getView() 1", e.getMessage(), e);
							setValueObject(view.getChildAt(i), "", x, position);
						} catch (IllegalAccessException e) {
							Log.e("TableComponent.TableComponentAdapter.getView() 2", e.getMessage(), e);
							setValueObject(view.getChildAt(i), "", x, position);
						} catch (InvocationTargetException e) {
							Log.e("TableComponent.TableComponentAdapter.getView() 3", e.getMessage(), e);
							setValueObject(view.getChildAt(i), "", x, position);
						} finally {
							x = beanProperties.length;
						}
					}
				}
				count++;
			}
			return view;
		}
		
		@Override
		public IDetailEntityComponent getItem(int position) {
			if (filas == null || filas.size() == 0) 
				return null;
			return filas.get(position);
		}
		
		@Override
		public Filter getFilter() {
			filas = (List<IDetailEntityComponent>)data;
		    if (filter == null)
		    	filter = new DataFilter();
		    return filter;
		}
		
		private class DataFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
			    if (constraint == null || constraint.length() == 0) {
			        results.values = filas;
			        results.count = filas.size();
			    } else {
			        List<Object> nDataList = new ArrayList<Object>();
			        boolean encontroValor = false;
			        for (Object p : filas) { 
			        	for (int x = (hasOrder?1:0); x < beanProperties.length; x++) {
//			        		if(fieldFilter.equals("0")) fieldFilter = beanProperties[x]; 
							if(beanProperties[x].equalsIgnoreCase(fieldFilter) || fieldFilter.equals("0")){
								encontroValor = true;
								try {
									Field field = p.getClass().getField(beanProperties[x].toLowerCase());
									if(field.get(p) != null) {
										if (field.get(p).toString().toUpperCase().contains(constraint.toString().toUpperCase())){
											//Log.e("entras", "valor:get(p): "+field.get(p).toString());
											nDataList.add(p);
						        		}
									}
								} catch (IllegalArgumentException e) {
									e.printStackTrace();
									Log.e("excep1", "excep1: "+e.getMessage());
								} catch (IllegalAccessException e) {
									e.printStackTrace();
									Log.e("excep2", "excep2: "+e.getMessage());
								} catch (NoSuchFieldException e) {
									e.printStackTrace();
									Log.e("excep3", "excep3: "+e.getMessage());
									executeMethod(p, x, constraint, nDataList);
								} 
							}
							
							if (!encontroValor) {
								executeMethod(p, x, constraint, nDataList);
							}
			        	}
			        }
			        synchronized(this) {
				        results.values = nDataList;
				        results.count = nDataList.size();
			        }
			    }
			    return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if(filas == null) return;
		    	filas = (List<IDetailEntityComponent>)results.values;
		        notifyDataSetChanged();
		        clear();
	            for(int i = 0, l = filas.size(); i < l; i++)
		        	add(filas.get(i));
	            notifyDataSetInvalidated();
			}
			
			private void executeMethod(Object p, int x, CharSequence constraint, List<Object> nDataList){
				try {
					Method method;
					IDetailEntityComponent entity = (IDetailEntityComponent)p;
					if (entity instanceof Entity && entity.isTitle()) {
						method = entity.getClass().getDeclaredMethod("getTitle");
					} else {
//						Log.e("metodo", "metodo: "+beanProperties[x]);
						method = entity.getClass().getDeclaredMethod(beanProperties[x]);
					}
					Object o = method.invoke(entity);
					String valor = Util.getText(o);
					if(valor != null) {
						if (valor.toUpperCase().contains(constraint.toString().toUpperCase())){
							//Log.e("entras", "valor:get(p): "+field.get(p).toString());
							nDataList.add(p);
		        		}
					}
				} catch (NoSuchMethodException e) {
					//Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
				} catch (IllegalArgumentException e) {
					Log.e("TableComponent.Filter 1", e.getMessage(), e);
				} catch (IllegalAccessException e) {
					Log.e("TableComponent.Filter 2", e.getMessage(), e);
				} catch (InvocationTargetException e) {
					Log.e("TableComponent.Filter 3", e.getMessage(), e);
				} 
			}
		}
	}
	
	private View getBarraVertical(int color, int width) {
		View barraVertical = new View(TableComponent.this.context);
		barraVertical.setBackgroundColor(context.getResources().getColor(color));
		AbsListView.LayoutParams llp = new AbsListView.LayoutParams(width, 
				LinearLayout.LayoutParams.MATCH_PARENT);
		barraVertical.setLayoutParams(llp);
		return barraVertical;
	}
	
	private View getObjectClass(Class<?> clase, int index, int position){
		LinearLayout ly = new LinearLayout(context);
		ly.setOrientation(LinearLayout.HORIZONTAL);
		int []texts = getTextos().get(index);
		if(clase == LabelComponent.class) {
			ly.addView(new LabelComponent(context).size(altoColumnData, 0, 1, false));
		} else if (clase == CheckBoxField.class) {
			ly.addView(new CheckBoxField(context, texts==null||texts.length==0?-1:texts[0], "1:0"));
		} else if (clase == RadioGroupOtherField.class) { 
//			LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			lp.gravity = Gravity.CENTER;
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lp.gravity = Gravity.CENTER;
			if (dataDetail.get(position) instanceof IRadioGroupOptions) {
				int[] array = new int[mapRadioGroups.get(position)];
				for (int i = 0; i < array.length; i++) {
					array[i] = texts[i];
				}
				ly.addView(new RadioGroupOtherField(context, array), lp);
			} else {
				ly.addView(new RadioGroupOtherField(context, texts), lp);
			}
			
		}
		return ly;
	}
	
	private void setValueObject(View view, String value, int index, int posicion){
		setLayoutHeight(view, LayoutParams.MATCH_PARENT);
		LinearLayout layout = ((LinearLayout)view);
		View viewChild = layout.getChildAt(layout.getChildCount()-1);
		setLayoutHeight(viewChild, altoColumnData);
		if (viewChild instanceof LabelComponent) {
			if (dataDetail != null&& !dataDetail.isEmpty()) {
				if (dataDetail.get(posicion).isTitle()) {
					((LabelComponent)viewChild).setText(Util.getText(value));
					((LabelComponent)viewChild).setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);	
				} else {
					((LabelComponent)viewChild).setText(Util.getText(value));
					((LabelComponent)viewChild).setGravity(Gravity.CENTER_VERTICAL|getAlign(index));
				}	
			} else {
				((LabelComponent)viewChild).setText(Util.getText(value));
				((LabelComponent)viewChild).setGravity(Gravity.CENTER_VERTICAL|getAlign(index));
			}
			
		} else if (viewChild instanceof CheckBoxField) {
			((CheckBoxField)viewChild).setCheckedTag(value);
			layout.setGravity(Gravity.CENTER_VERTICAL|getAlign(index));
		} else if (viewChild instanceof RadioGroupOtherField) {
			((RadioGroupOtherField)viewChild).setTagSelected(!value.equals("")?value:null);
			layout.setGravity(Gravity.CENTER_VERTICAL|getAlign(index));
		} 		
		asignarCallback(viewChild, index, posicion);
		asginarColor(layout.getChildAt(layout.getChildCount()-1), index, posicion, layout);
	}
	
	private void asginarColor(View viewChild, int index, int posicion, LinearLayout layout) {
		if(viewChild!=null){
			TCColor tcc;
			boolean flagColor = true;
//			Log.e("tamanio", "tamanio: "+lstColor.size());
			for(int x=0;x<lstColor.size();x++){
				tcc = lstColor.get(x);
				if(tcc.tipo == TIPO.FILA){
//					Log.e("lstColor: "+x+" FILA: "+posicion+" value: "+tcc.value+" flag:"+tcc.flag, " rowColor: "+tcc.rowColor+" index: "+index+" colColor: "+tcc.colColor+" status: "+tcc.status);
//					viewChild.setBackgroundResource(viewChild.getDrawingCacheBackgroundColor());
					if(tcc.flag && tcc.status && (tcc.rowColor==posicion||tcc.rowColor==-1) && (tcc.colColor==index||tcc.colColor==-1)) {
//						viewChild.setDrawingCacheBackgroundColor(tcc.color);
//						Log.e("entras con valor", "tcc.value: "+tcc.value);
						viewChild.setBackgroundResource(tcc.color);
						if(viewChild instanceof LabelComponent && tcc.bold) {
							((LabelComponent)viewChild).setTypeface(Typeface.DEFAULT_BOLD);
							((LabelComponent)viewChild).setTextSize(tcc.size);
							((LabelComponent)viewChild).setColorTexto(tcc.textColor);
						} else if (viewChild instanceof RadioGroupOtherField){
							layout.setBackgroundResource(tcc.color);
						}
						flagColor = false;
					} 
					if(flagColor) {
						viewChild.setBackgroundResource(viewChild.getDrawingCacheBackgroundColor());
						if(viewChild instanceof LabelComponent) {
							((LabelComponent)viewChild).setTypeface(Typeface.DEFAULT);
							((LabelComponent)viewChild).setTextSize(textSizeData);
							((LabelComponent)viewChild).setColorTexto(textColorData);
						} else if (viewChild instanceof RadioGroupOtherField){
							layout.setBackgroundResource(viewChild.getDrawingCacheBackgroundColor());
						}
					}
				} else if (tcc.tipo == TIPO.BORDE){
//					Log.e("lstColor: "+x+" BORDE: "+posicion, "getChildCount: "+layout.getChildCount()+" rowColor: "+tcc.rowColor+" index: "+index+" colColor: "+tcc.colColor+" status: "+tcc.status);
					if(tcc.campos!=null){
						if((index == tcc.colColor && tcc.rowColor == posicion && layout.getChildCount()<=1)){
							layout.addView(getBarraVertical(tcc.color, 5), 0);
						} 
						else {
							if( tcc.rowColor != posicion && layout.getChildCount()>1 && index == tcc.colColor)
								layout.removeViewAt(0);
						}
					} else {
//						Log.e("lstColor: "+x+" BORDE: "+posicion, "getChildCount: "+layout.getChildCount()+" rowColor: "+tcc.rowColor+" index: "+index+" colColor: "+tcc.colColor+" status: "+tcc.status);
						if(tcc.rowColor == 0 && layout.getChildCount()>1) layout.removeViewAt(0);
						if(tcc.status && (index == tcc.colColor && tcc.rowColor == posicion && layout.getChildCount()<=1)){
							layout.addView(getBarraVertical(tcc.color, 5), 0);
							flagColor = false;
//							tcc.status = false;
						} 
						if(flagColor) {
							if( tcc.rowColor != posicion && layout.getChildCount()>1 && index == tcc.colColor)
								layout.removeViewAt(0);
						}
					}
				} 
			}
		}
	}
	private void asignarCallback(View viewChild, int index, int posicion) {
		if (viewChild instanceof CheckBoxField) {
			((CheckBoxField)viewChild).setEnabled(isEnabled);
			final String callback = getCallbacks().get(index);
			if (callback != null) {
				TableComponentCompoundButtonOnCheckedChangeListener listener = new TableComponentCompoundButtonOnCheckedChangeListener(posicion, callback);
				((CheckBoxField)viewChild).setOnCheckedChangeListener(listener);	
			}
		} else if(viewChild instanceof RadioGroupOtherField){
			((RadioGroupOtherField)viewChild).setEnabled(isEnabled);
			final String callback = getCallbacks().get(index);
			if (callback != null) {
				TableComponentOnCheckedChangeListener listener = new TableComponentOnCheckedChangeListener(posicion, callback);
				((RadioGroupOtherField)viewChild).setOnCheckedChangeListener(listener);
			}		
		}
	}
	private void setLayoutHeight(View view, int layoutHeight){
		if(view == null) return;
		ViewGroup.LayoutParams ly = view.getLayoutParams();
		ly.height = layoutHeight;
		view.setLayoutParams(ly);
	}
	
	private void setLayoutWidth(View view, int layoutWidth){
		if(view == null) return;
		ViewGroup.LayoutParams ly = view.getLayoutParams();
		ly.width = layoutWidth;
		view.setLayoutParams(ly);
	}
	
	private int getAlign(int index){
		switch (getAligns().get(index)) {
			case CENTER: return Gravity.CENTER;
			case LEFT: return Gravity.LEFT;
			case RIGHT: return Gravity.RIGHT;
			default: return Gravity.CENTER;
		}
	} 

	private boolean setBorder(Field[] fields, Object item, int position, boolean flag){
		boolean validaCampos=false;
		for(TIPO tip : TIPO.values()){
			for(TCColor tcc : lstColor){
				if(tcc.flag && tcc.tipo == tip && tcc.campos!=null){
					validaCampos = true;
					boolean encontroValor = false;
					for(String campo : tcc.campos){
						for (int j = 0; j < fields.length; j++) {
							if (fields[j].getName().equalsIgnoreCase(campo)) {
								encontroValor=true;
								try {
									if(fields[j].get(item) == null) {validaCampos = false; break;}
									if(fields[j].get(item).toString().equals(tcc.value)) { 
										if(tip == TIPO.FILA) {
											tcc.status = true;
											tcc.rowColor = position;
										}
										validaCampos = false; 
									} else {
										if(tip == TIPO.FILA) {
											tcc.status = false;
										}
									}
								} catch (IllegalArgumentException e) {
									Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
								} catch (IllegalAccessException e) {
									Log.e("TableComponent.TableComponentAdapter.getView()", e.getMessage(), e);
								}
								break;
							}
						}
						if(!validaCampos) break;
					}
				}
				if(tip == TIPO.BORDE){
//					Log.e("entras", "tcc.rowColor: "+tcc.rowColor);
//					Log.e("entras", "border row: "+position);
					if(tcc.campos != null){
						if(validaCampos) tcc.rowColor = position;
						else this.lstObjets.add(item);
					}
				}
			}
		}
		return validaCampos;
	}
	
	private class TableComponentOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
		private int position;
		private String callback;
		public TableComponentOnCheckedChangeListener(int position, String callback) {
			this.position = position;
			this.callback = callback;
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			try {
				RadioGroupOtherField rg = (RadioGroupOtherField) group;
				Integer tag = Integer.valueOf(rg.getTagSelected("0").toString());
				if(rg.getCheckedRadioButtonId()!=-1){
					if(rg.findViewById(rg.getCheckedRadioButtonId()).isPressed()){
						Method method;
						method = parent.getClass().getDeclaredMethod(callback, Object.class, Integer.class, Integer.class);
						if (method == null) {
							Log.e(getClass().getSimpleName(), "Metodo [" + callback + "] no encontrado.");
							return;
						}
		            	method.invoke(parent, TableComponent.this.data.get(this.position), this.position, tag);
//		            	TableComponent.this.adapter.getView(this.position, 
//		            			TableComponent.this.listView.getChildAt(this.position-TableComponent.this.listView.getFirstVisiblePosition()) , null);
					}
				}
			} catch (NullPointerException e) {
				Log.e(getClass().getSimpleName(), "Error: Ausencia de datos.");
			} catch (Exception e) {
				Log.e(getClass().getSimpleName(), "Error: " + e.getMessage());
			} 
		}
	}
	
	private class TableComponentCompoundButtonOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
		private int position;
		private String callback;
		
		public TableComponentCompoundButtonOnCheckedChangeListener(int position, String callback) {
			this.position = position;
			this.callback = callback;
		}
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			try {
				if (!buttonView.isPressed()) {
					return;
				}
				int tag = isChecked ? 1 : 0;
				Method method;
				method = parent.getClass().getDeclaredMethod(callback, Object.class, Integer.class, Integer.class);
				if (method == null) {
					Log.e(getClass().getSimpleName(), "Metodo [" + callback + "] no encontrado.");
					return;
				}
            	method.invoke(parent, TableComponent.this.filas.get(this.position), this.position, tag);
			} catch (NullPointerException e) {
				Log.e(getClass().getSimpleName(), "Error: Ausencia de datos.");
			} catch (Exception e) {
				Log.e(getClass().getSimpleName(), "Error: " + e.getMessage());
			}
		}
	}
	
	private class TCColor{
		private boolean flag;
		private boolean status;
		private int rowColor;
		private int colColor;
		private int color;
		private boolean bold;
		private int size;
		private int textColor;
		private String[] campos;
		private String value;
		private TIPO tipo;
		public TCColor(TIPO tipo, boolean status, int rowColor, int colColor, int color, String[] campos, String value, 
				boolean bold, int sizeText, int textColor){
			this.flag = true;
			this.status = status;
			this.rowColor = rowColor;
			this.colColor = colColor;
			this.color = color;
			this.bold = bold;
			this.campos = campos;
			this.value = value;
			this.tipo = tipo;
			this.size = sizeText;
			this.textColor = textColor;
		}
	}
	
	public class TableComponentMapAdapter extends ArrayAdapter<Map<String, Object>> {
//		private List<IDetailEntityComponent> filas;
//		private Filter filter;
		public TableComponentMapAdapter(Context context, List<Map<String, Object>> objects) {
			super(context, -1, objects);
			filasMaps = new ArrayList<Map<String, Object>>();
			if (objects != null) {
				filasMaps.addAll(objects);	
			}
			lstObjets = new ArrayList<Object>();
//			if (TableComponent.this.statusFilter) {
//				filter = new DataFilter();	
//			}
		}
				
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout view;
//			if(TableComponent.this.repintarTodo) {
//				view = new LinearLayout(TableComponent.this.context);
//				view.setDescendantFocusability(android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS);
//				view.setOrientation(LinearLayout.HORIZONTAL);	
//				if (getItem(position).isTitle()) {
//					view.addView(getObjectClass(getClases().get(0), 0, position), getHeaders().get(0).header.getLayoutParams());
//					view.addView(getBarraVertical(R.color.griscabece, 2));
//				} else {
//					for (int i = 0; i < beanProperties.length; i++) {
//						view.addView(getObjectClass(getClases().get(i), i, position), getHeaders().get(i).header.getLayoutParams());
//						if (i < beanProperties.length -1) {
//							view.addView(getBarraVertical(R.color.griscabece, 2));
//						}
//					}	
//				}
//				view.setTag(getItem(position));
//			} else {
				if (convertView == null) {
					view = new LinearLayout(TableComponent.this.context);
					view.setDescendantFocusability(android.view.ViewGroup.FOCUS_BLOCK_DESCENDANTS);
					view.setOrientation(LinearLayout.HORIZONTAL);	
//					if (getItem(position).isTitle()) {
//						view.addView(getObjectClass(getClases().get(0), 0, position), getHeaders().get(0).header.getLayoutParams());
//						view.addView(getBarraVertical(R.color.griscabece, 2));
//					} else {
						for (int i = 0; i < beanProperties.length; i++) {
							view.addView(getObjectClass(getClases().get(i), i, position), getHeaders().get(i).header.getLayoutParams());
							if (i < beanProperties.length -1) {
								view.addView(getBarraVertical(R.color.griscabece, 2));
							}
						}	
//					}
					view.setTag(getItem(position));
				} else {
					view = (LinearLayout)convertView;
					view.setTag(getItem(position));
				}
//			}
//			Object item = getItem(position);
			Map<String, Object> item = (Map<String, Object>) view.getTag(); /*648647867*/
			int count = 0;
			String dato="";
			for (int i = 0; i < view.getChildCount(); i=i+2) {	
				if (count == 0) {
					if (beanProperties[count].equals("Nº")){
						setValueObject(view.getChildAt(i), String.valueOf(position+1), count, position);
					} else {
						dato = Util.getText(item.get(beanProperties[count].toUpperCase()),"");
						setValueObject(view.getChildAt(i), dato, count, position);
					}
				} else {
					dato = Util.getText(item.get(beanProperties[count].toUpperCase()),"");
					setValueObject(view.getChildAt(i), dato, count, position);
				}
				count++;
			}
			return view;
		}
		
		@Override
		public Map<String, Object> getItem(int position) {
			if (filasMaps == null || filasMaps.size() == 0) 
				return null;
			return filasMaps.get(position);
		}
	}
}
