package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout.LayoutParams;

/**
 * 
 * @author ajulcamoro
 *
 */
public class CheckBoxField extends CheckBox implements FieldComponent {
	
	private OnCheckedChangeListener onCheckedChangeListenerAlterno;
	
	private List<String> callbacks;
	private String fieldName = null;
	private String group = null;
//	private Context context;
	//private int textSize = 20;
	public CheckBoxField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CheckBoxField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CheckBoxField(Context context) {
		super(context);
	}
	
	public CheckBoxField(Context context, int question, String tags) {
		super(context);
//		this.context = context;
		this.setText(question==-1?"":getResources().getString(question));
		this.setTag(tags);
		this.setPadding(5, 0, 5, 0);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
	
	public CheckBoxField(Context context, String tags) {
		this(context, -1, tags);
	}
	
	public CheckBoxField negrita() {
		setTypeface(null, Typeface.BOLD);
		return this;
	}
	
	public Object getCheckedTag() {
		if(super.getTag() == null) return null;
		/*
		 * android.tag="on:off"
		 * p.e tag="1:0"
		 * */
		String[] tag = super.getTag().toString().split(":"); 
		if(isChecked()) return tag[0];
		return tag[1];
	}
	
	public void setCheckedTag(Object tag) {
		if (tag == null){
			setChecked(false);
		}
		else{
	        String[] _tag = super.getTag().toString().split(":");       
	        setChecked(_tag[0].equals(tag.toString()));
        }           
	}
	
	public void setSize(int alto, int ancho) {
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.getContext(), ancho), Util.getTamañoEscalado(this.getContext(), alto, 0.3f));
		this.setLayoutParams(lp);
		float multiplicador = 10.0f;
		if(alto == LayoutParams.WRAP_CONTENT && ancho == LayoutParams.WRAP_CONTENT) {
			multiplicador = 25.0f;
		}
		final float scale = this.getResources().getDisplayMetrics().density;
		if (scale < 1.33) {
			this.setPadding(this.getPaddingLeft() + (int) (multiplicador * scale + 0.5f),
	                this.getPaddingTop(),
	                this.getPaddingRight(),
	                this.getPaddingBottom());
		}
	}	
	
	public void addCallBack(String callBackMethodName) {
		if (this.callbacks == null) {
			this.callbacks = new ArrayList<String>();
		}
		this.callbacks.add(callBackMethodName);
	}
	
	public void setReadOnly(boolean readOnly) {
		this.setClickable(!readOnly); 
		this.setFocusable(!readOnly); 
		this.setFocusableInTouchMode(!readOnly); 
	}
	
	public void setReadOnly() {
		this.setReadOnly(true); 
	}
	
	public List<String> getCallbacks() {
		return this.callbacks;
	}
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public void setColorFondo(int color) {
		this.setBackgroundColor(getResources().getColor(color));
	}

	public CheckBoxField text(int question) { this.setText(getResources().getString(question)); return this;}
	public CheckBoxField size(int alto, int ancho) { this.setSize(alto,ancho); return this;}
    public CheckBoxField callback(String methodCallBackName){ this.addCallBack(methodCallBackName); return this;}
    public CheckBoxField readOnly() { this.setReadOnly(); return this;}
    public CheckBoxField sizeText(float size) { this.setTextSize(size); return this;}
    public CheckBoxField group(String group) { this.setGroup(group); return this;}
    public CheckBoxField fieldName(String fieldName) { this.setFieldName(fieldName); return this;}
    public CheckBoxField colorFondo(int color) { this.setColorFondo(color); return this;}

	@Override
	public void setValue(Object aValue) {
		setCheckedTag(aValue);
	}

	@Override
	public Object getValue() {
		return getCheckedTag();
	}

	@Override
	public Class<?> getValueClass() {
		return Object.class;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckBoxField other = (CheckBoxField) obj;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		return true;
	}

	public void setOnCheckedChangeListenerAlterno(
			OnCheckedChangeListener onCheckedChangeListenerAlterno) {
		this.onCheckedChangeListenerAlterno = onCheckedChangeListenerAlterno;
	}

	public void onCheckedChangedAlterno(CompoundButton buttonView, boolean isChecked) {
		if(onCheckedChangeListenerAlterno!=null)
			onCheckedChangeListenerAlterno.onCheckedChanged(buttonView, isChecked);	
	}
	
	
}
