package gob.inei.dnce.components;


import gob.inei.dnce.R;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.interfaces.RespondibleDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

/**
 * 
 * @author Rdelacruz
 * @since 27/02/2015
 * @version 07/10/2015 
 */
public abstract class DialogFragmentComponentWithoutXML 
extends DialogFragmentComponent
{
	
	private boolean eliminarDobleToque = false;
	
	private String titulo;

	private ButtonComponent btnAceptar;
	private ButtonComponent btnCancelar;
	private Contenedor contenedor;
	private RespondibleDialog responsibleListener;
	
	protected abstract void buildFields() throws Exception;
	protected abstract void addComponents(LinearLayout contenedor) throws Exception;
	protected abstract void cargarDatosOrThrow() throws Exception;		
	//protected abstract void limpiar(String codigo, Activity activity) throws Exception;
	protected abstract boolean onAceptar() throws Exception;
		
	public DialogFragmentComponentWithoutXML() {
		this.setRetainInstance(true);
		
		setAncho(DialogFragmentComponent.MATCH_PARENT);
	}
	
	
	@Override
	public final void setParent(Fragment parent) {
		if(parent instanceof FragmentFormWithoutXML) {
			FragmentFormWithoutXML ffw = (FragmentFormWithoutXML) parent; 
			eliminarDobleToque = ffw.isEliminarDobleToque();
		}
		super.setParent(parent);
	}
	
	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		try {
			esCargaInicial = true;
			
			getDialog().setTitle(titulo);
			//Ocultando el teclado
			getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			
			View rootView = createUI();
			boolean existenComponentes = getClass().getDeclaredFields().length>0;
			
			if(existenComponentes)
				initObjectsWithoutXML(this, rootView);
			
			cargarDatosOrThrow();		
			
			if(existenComponentes) {
				enlazarCajas(this);
				listening();
				
				if(eliminarDobleToque) {
					eliminarDobleTouchEnComponentes(this);
				}
			}
			
//			rootView.scrollTo(0,0);//no funciona
			
			return rootView;
		}
		catch(Exception e) {
			manejarExcepcion(e);
		}
		return null;
	}
	
	private void eliminarDobleTouchEnComponentes(Object container) {
		try {
			FragmentFormWithoutXML.setFocusableFields(container,false);
		} catch (IllegalArgumentException e) {
			manejarExcepcion(e);
		} catch (IllegalAccessException e) {
			manejarExcepcion(e);
		}
	}
	
	protected void onCancelar() throws Exception {
		if(responsibleListener!=null)
			responsibleListener.onCancel();
	}
	
	protected void onCerrar() throws Exception {
		if(responsibleListener!=null)
			responsibleListener.onCerrar();
	}
	
	public final boolean aceptar() {
		try {
			if(onAceptar()) {				
				if(responsibleListener!=null)
					responsibleListener.onAccept();
				dismiss();
				return true;
			}
		} catch (Exception e) {
			manejarExcepcion(e);
		}
		return false;
	}
			
	@Override
	protected final View createUI() {
		try {
			//buildFields();
			
			// ///////////////////////////
			ScrollView viewRoot = createForm();
			LinearLayout layout = (LinearLayout) viewRoot.getChildAt(0);
				
			Context context = viewRoot.getContext();
					
			contenedor = new Contenedor(context);
			//contenedor.setOrientation(LinearLayout.VERTICAL);
			layout.addView(contenedor);
			
			crearBotonesAceptarCancelar(layout);
			
			buildFields();//esta despues de crear los botones Aceptar y Cancelar
			
			btnAceptar.setOnClickListener(new View.OnClickListener() {					
				@Override
				public void onClick(View v) {			
					aceptar();												
				}				
			});
			
			btnCancelar.setOnClickListener(new View.OnClickListener() {					
				@Override
				public void onClick(View v) {			
					try {		
						onCancelar();						
						dismiss();
					} catch (Exception e) {
						manejarExcepcion(e);
					}
				}
			});
			
		
			/* Aca agregamos las preguntas a la pantalla */
			addComponents(contenedor);
			
			return viewRoot;
		} catch (Exception e) {
			manejarExcepcion(e);
		}		
		return null;
	}
		
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		try {		
			onCerrar();
			super.onDismiss(dialog);
		} catch (Exception e) {
			manejarExcepcion(e);
		}		
	}
	protected void manejarExcepcion(Exception e) {
		String mensaje = "ERROR: "+e.toString();
		
		Toast toast = Toast.makeText(this.getActivity(),mensaje,Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
		toast.show();
		
		e.printStackTrace();
	}
	
	protected void crearBotonesAceptarCancelar(LinearLayout layout) {
		btnAceptar = new ButtonComponent(layout.getContext(), R.style.btnStyleButtonGreen)
		.text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(layout.getContext(), R.style.btnStyleButtonGreen)
		.text(R.string.btnCancelar).size(200, 60);
		layout.addView(createButtonSection(btnAceptar,btnCancelar));
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Respondible getResponsibleListener() {
		return responsibleListener;
	}
	public void setResponsibleListener(RespondibleDialog responsibleListener) {
		this.responsibleListener = responsibleListener;
	}

	
	public class Contenedor 
	extends LinearLayout
	{

		public Contenedor(Context context) {			
			super(context);
			this.setOrientation(LinearLayout.VERTICAL);
		}

		@Override
		public boolean onInterceptTouchEvent(MotionEvent ev) {		
			esCargaInicial = false;
			return super.onInterceptTouchEvent(ev);
		}
	}


	public ButtonComponent getBtnAceptar() {
		return btnAceptar;
	}
	
	
}
