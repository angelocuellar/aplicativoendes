package gob.inei.dnce.components;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

public class GridComponent3 extends LinearLayout {
	
	private Context context;
	private List<Componente> componentes;
	private int columnas = 1;	
	private int width = 1;	
	private int cellspacing = 2;	

	public GridComponent3(Context context) {
		super(context);
		this.context = context;
	}

	public GridComponent3(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public GridComponent3(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}
	
	public void buildTable() {
		List<Header> cabecerasSpan = getCabeceras();
		int filas = 0;
		float tmp = 0;
		boolean filaNueva = true;
		for (int i = 0; i < getComponentes().size(); i++) {
			if (filaNueva) {
				tmp = 0;
			}
			Componente c = getComponentes().get(i);
			tmp += c.colspan;
			if (tmp >= columnas) {
				filas++;
				filaNueva = true;
			} else {
				filaNueva = false;
			}
		}
		int elementos = 0;
		for (int i = 0; i < filas; i++) {
			LinearLayout fila = getFila();
			int columns = 0;
			for (int j = 0; j < getComponentes().size(); j++) {
				Componente c = getComponentes().get(j);
				columns += c.colspan;
				if (columns >= columnas) {
					
				}
				if (c.rowspan > 1) { //rowspan
					LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, c.colspan);
					fila.addView(c.view, lp);
					if (c.colspan > columns) {
						
					}
					LinearLayout celda = new LinearLayout(context);
					
				} else { // no rowspan
					LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, c.colspan);
					fila.addView(c.view, lp);	
				}
				
			}
		}
	}
	
	private List<Header> getCabeceras() {
		
		return null;
	}

	public LinearLayout getFila() {
		LinearLayout fila = new LinearLayout(context);
		LayoutParams lp = new LayoutParams(width, LayoutParams.WRAP_CONTENT);
		lp.setMargins(cellspacing, cellspacing, cellspacing, cellspacing);
		fila.setLayoutParams(lp);
		fila.setGravity(Gravity.CENTER);
		fila.setOrientation(LinearLayout.HORIZONTAL);
		return fila;
	}
	
	public GridComponent3 columns(int col) { this.columnas = col; return this;}
	
	public void addComponent(View view) {
		this.addComponent(view, 1);
	}
	
	public void addComponent(View view, int colspan) {
		this.addComponent(view, colspan, 1);
	}
	
	public void addComponent(View view, int colspan, int rowspan) {
		getComponentes().add(new Componente(view, colspan, rowspan));
	}
	
	private List<Componente> getComponentes() {
		if (componentes == null) {
			componentes = new ArrayList<GridComponent3.Componente>();
		}
		return componentes;
	}
	
	private class Componente {
		int colspan;
		int rowspan;
		View view;
		
		public Componente(View view) {
			this(view, 1);
		}
		
		public Componente(View view, int colspan) {
			this(view, colspan, 1);
		}

		public Componente(View view, int colspan, int rowspan) {
			super();
			this.colspan = colspan;
			this.rowspan = rowspan;
			this.view = view;
		}		
	}
	
	private class Header {
		float colspan;

		public Header(float colspan) {
			super();
			this.colspan = colspan;
		}
		
	}
}
