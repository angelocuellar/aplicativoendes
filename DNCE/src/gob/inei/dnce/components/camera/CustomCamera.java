package gob.inei.dnce.components.camera;

import gob.inei.dnce.components.ToastMessage;
import android.app.Activity;
import android.content.Intent;

public class CustomCamera {
	Activity mActivity;
	public static int RESOLUTION_320x240 = 0;
	public static int RESOLUTION_640x480 = 1;
	public static int RESOLUTION_1024x768 = 2;
	public static int RESOLUTION_1280x720 = 3;
	public static int RESOLUTION_1280x768 = 4;
	public static int RESOLUTION_1280x960 = 5;
	public static int RESOLUTION_1600x1200 = 6;
	public static int RESOLUTION_2048x1536 = 7;
	public static int RESOLUTION_2560x1440 = 8;
	public static int RESOLUTION_2560x1920 = 9;
	public static int RESOLUTION_2880x1728 = 10;
	public CustomCamera(Activity mActivity){
		this.mActivity = mActivity;
	}
	
	public void startCamera(String fileName, boolean defaultCamera){
		if (fileName == null || "".equals(fileName)) {
			ToastMessage.msgBox(mActivity, "Ingrese un nombre v�lido para la foto", 
				ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			return;
		}
		CameraClass.useDefaultCamera = defaultCamera;
		CamPreview.front_camera = false;
		Intent i = new Intent(mActivity,CameraClass.class);
		i.putExtra(CameraClass.PICTURE_NAME_PROPERTY, fileName);
		mActivity.startActivity(i);
	}
	
	public void startCamera(String fileName){
		this.startCamera(fileName, false);
	}
	
	public void startCameraFront(){
		CameraClass.useDefaultCamera = false;
		CamPreview.front_camera = true;
		mActivity.startActivity(new Intent(mActivity,CameraClass.class));
	}
	
	public void sendCameraIntent(){
		CameraClass.useDefaultCamera = true;
		CamPreview.front_camera = false;
		mActivity.startActivity(new Intent(mActivity,CameraClass.class));
	}
	
	public void setPictureTakenListener(OnPictureTaken onPictureTaken){
		CameraClass.setOnPictureTakenListner(onPictureTaken);
	}
	
	public void setResolutionCamera(int resolution){
		CameraClass.RESOLUTION = resolution;
	}
	
	public boolean canTakePicture() {
		if (CameraClass.RUTA_FOTOS == null || "".equals(CameraClass.RUTA_FOTOS)) {
			return false;
		} else {
			return true;
		}
	}
}