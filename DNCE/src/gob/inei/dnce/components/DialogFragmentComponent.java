package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.interfaces.CallbackEvent;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.IFormComponent;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.PeriodoReferencia;
import gob.inei.dnce.util.Util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public abstract class DialogFragmentComponent extends DialogFragment implements IFormComponent {

	protected abstract View createUI();
	protected abstract void buildFields() throws Exception;
	private Fragment parent;
	private int ancho = LayoutParams.WRAP_CONTENT;
	private int cellSpacing = 2;
	private int topMargin = 10;
	private int bottomMargin = 10;
	private int leftMargin = 10;
	private int rightMargin = 10;
	private int textQuestionSize = 19;
	protected String mensaje = "";
	protected boolean error = false;
	protected View view = null;
	private static String TAG = "DialogFragmentComponent";
	
	public static final int MATCH_PARENT = LayoutParams.MATCH_PARENT;
	public static final int WRAP_CONTENT = LayoutParams.WRAP_CONTENT;
	
	public static int EVENT_ALL = 0;
	public static int EVENT_NONE = 1;
	public static int EVENT_CHECKED_CHECK = 2;
	public static int EVENT_CHECKED_RADIO = 3;
	public static int EVENT_KEY = 4;
	public static int EVENT_ITEM_SELECTED = 5;
	public static int EVENT_FOCUS = 6;
	
	public boolean returnFocus = false;
	public View viewLostFocus = null;
	
	protected Menu menu;
	protected Context dialog_context = this.getActivity();
	protected String obsGeneral = null;
	/*protected static final Logger logger = LoggerFactory.getLogger();hhh*/
	protected TextView tv;
	
	protected PeriodoReferencia periodoReferencia;
	protected int altoComponente = 54;
	protected Date fechaActual = Calendar.getInstance().getTime();
	protected View rootView;

	protected Caretaker<Entity> caretaker;
	
	private FragmentActivity activity;
	private Map<String, Field> mapFields;
	
	/**
	 * Lista de campos ordenados segun se indica con la anotacion
	 */
	View[] fieldsOrdered = null;
	Field[] fieldsOrderedxAnot = null;
	
	/**
	 * Esta variable contiene clave=campo y valor=[orden segun anotacion(entero), numero de pregunta(texto)]
	 */
	HashMap<Object, Integer> fieldsPosition = null;
	
	/**
	 * Mapa de preguntas: clave="numero de pregunta", valor="array de views que conforman la pregunta"
	 * Aun no usado
	 */
	HashMap<String, View[]> questionView = null;
	
	/**
	 * clave = contenedor(ventana principal o dialogo), valor = fieldsOrdered
	 * Contenedor, objetos
	 */
	HashMap<Object, View[]> containerFieldsOrdered;
	
	/**
	 * clave = contenedor(ventana principal o dialogo), valor = fieldsPosition
	 */
	HashMap<Object, HashMap<Object, Integer>> containerFieldsPosition;
	
	HashMap<Object, ArrayList<View>> rangeados;
	
	HashMap<Object,List<NumberField>> camposRangeados;
	
	View viewContainer = null;
	Object containerContext = null;
	
	HashMap<Object, Integer> containerNumFields;
	
	HashMap<View, List<View>> blockerAndBlocked;
	
	Drawable backgroundDrawableFocus = null;
	Drawable backgroundDrawableUnfocus = null;
	
	HashMap<Object, Drawable> containerbackgroundDrawableUnfocus;
	
	Integer numFields = 0;
	InputMethodManager imm;
	
	/**
	 * Object[0] = CallbackEvent
	 * Object[1] = to
	 * Object[2] = next
	 * Object[3] = dataSource
	 * Object[4] = values
	 * Object[5] = cancel
	 * Object[6] = allEquals
	 */
	HashMap<View, HashMap<Integer, Object[]>> callbackEvents = null;
	
	HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>> containerCallbackEvents = null;
	
	protected boolean esCargaInicial;
	private HashMap<String, List<CheckBoxField>> checkBoxGroups;
	public static int COLOR_LINEA_SECCION_PREGUNTA = R.color.marco;
//	protected final Logger logger = LoggerFactory.getLogger();	
//	
//	public Logger getLogger() {
//		return logger;
//	}
	
	public DialogFragmentComponent() {		
		this.setCancelable(false);
	}
	
	public int getCellSpacing() {
		return cellSpacing;
	}
	
	public void setCellSpacing(int cellSpacing) {
		this.cellSpacing = cellSpacing;
	}
	
	public int getTopMargin() {
		return topMargin;
	}
	
	public void setTopMargin(int topMargin) {
		this.topMargin = topMargin;
	}
	
	public int getBottomMargin() {
		return bottomMargin;
	}
	
	public void setBottomMargin(int bottomMargin) {
		this.bottomMargin = bottomMargin;
	}
	
	public int getLeftMargin() {
		return leftMargin;
	}
	
	public void setLeftMargin(int leftMargin) {
		this.leftMargin = leftMargin;
	}
	
	public int getRightMargin() {
		return rightMargin;
	}
	
	public void setRightMargin(int rightMargin) {
		this.rightMargin = rightMargin;
	}
	
	public int getTextQuestionSize() {
		return textQuestionSize;
	}
	
	public void setTextQuestionSize(int textQuestionSize) {
		this.textQuestionSize = textQuestionSize;
	}
	
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}
	
	public Fragment getParent() {
		return parent;
	}
	
	public FragmentActivity getParentActivity() {
		return activity;
	}
	public void setParent(Fragment parent) {
		this.parent = parent;
		this.activity = (FragmentActivity) this.parent.getActivity();
	}
	public void setParentActivity(MasterActivity parentActivity) {
		this.activity = parentActivity;
	}
	protected LinearLayout createQuestionSection(int question, View... components) {
		return this.createQuestionSection(question, Gravity.CENTER, components);
	}
	
	protected LinearLayout createQuestionSection(View... components) {
		return this.createQuestionSection(0, components);
	}
	
	protected LinearLayout createForm(LinearLayout title) {
		LinearLayout principal = new LinearLayout(this.activity);
		principal.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams principalLayout = new LinearLayout.LayoutParams(MATCH_PARENT, 
				LinearLayout.LayoutParams.MATCH_PARENT);
		principal.setLayoutParams(principalLayout);
		ScrollView contenedor = new ScrollView(this.activity);
		ViewGroup.LayoutParams lcontenedor = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
		contenedor.setLayoutParams(lcontenedor);
        LinearLayout form = new LinearLayout(this.activity);
		form.setOrientation(LinearLayout.VERTICAL);
		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ancho, 
				LinearLayout.LayoutParams.WRAP_CONTENT);	
		form.setLayoutParams(llp);
        contenedor.addView(form);
        principal.addView(title);
        principal.addView(contenedor);
		return principal;
	}
	
	protected ScrollView createForm() {
		Log.e("fffff","rrttttttt");
		ScrollView contenedor = new ScrollView(this.activity);
        LinearLayout form = new LinearLayout(this.activity);
		form.setOrientation(LinearLayout.VERTICAL);
		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ancho, 
				LinearLayout.LayoutParams.WRAP_CONTENT);	
		form.setLayoutParams(llp);
        contenedor.addView(form);
		return contenedor;
	}
	
	protected LinearLayout createQuestionSection(int question, int gravity, View... components) {		
		return this.createQuestionSection(question, gravity, LinearLayout.VERTICAL, components);
	}	

	protected LinearLayout createQuestionSection(int question, int gravity, int orientacion, View... components) {
		return this.createQuestionSection(question, gravity, orientacion, -1, components);
	}

	protected LinearLayout createQuestionSection(int question, int gravity, int orientacion, int backgroundImage, View... components) {
		LinearLayout ll = new LinearLayout(this.activity);
		ll.setOrientation(LinearLayout.VERTICAL);
		if (backgroundImage == -1) {
			ll.setBackgroundColor(getResources().getColor(R.color.WhiteSmoke));
		} else {
//			ll.setBackground(getResources().getDrawable(backgroundImage));
			ll.setBackgroundDrawable(getResources().getDrawable(backgroundImage));
		}
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		llp.setMargins(cellSpacing,cellSpacing,cellSpacing,cellSpacing);
		ll.setLayoutParams(llp);
		if (question != 0) {
	        TextView tv = createQuestionLabel(question);
	        ll.addView(tv);	
		}        
        LinearLayout contenido = createSection(orientacion, gravity);        
        for (View view : components) {
        	contenido.addView(view);
		}
        ll.addView(contenido);
		return ll;
	}
	
	protected TextView createQuestionLabel(int question) {
		TextView tv = new TextView(this.activity);
		tv.setPadding(7, 10, 7, 0);
		tv.setTextSize(textQuestionSize);
        tv.setText(getResources().getString(question));
        tv.setTypeface(Typeface.DEFAULT_BOLD);
		return tv;
	}
	
	protected LinearLayout createSection(int orientacion, int gravity) {
		LayoutParams lp = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout ll = new LinearLayout(this.activity);
		ll.setOrientation(orientacion);
		lp.topMargin = topMargin;
		lp.bottomMargin = bottomMargin;
		if (gravity != Gravity.CENTER) {
			lp.leftMargin = leftMargin;
			lp.rightMargin = rightMargin;	
		}
		ll.setLayoutParams(lp);
		ll.setGravity(gravity);	
		return ll;
	}
	
	protected LinearLayout createButtonSection(ButtonComponent... components) {
		return this.createQuestionSection(0, Gravity.CENTER, LinearLayout.HORIZONTAL, components);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, View...views){
		return createLy(orientation, gravity, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, View...views){
		return createLy(orientation, gravity, topMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin, View...views){
		return createLy(orientation, gravity, topMargin, bottomMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin,
			int leftMargin, View...views){
		return createLy(orientation, gravity, topMargin, bottomMargin, leftMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin,
			int leftMargin, int rightMargin, View...views){
		LinearLayout ly = new LinearLayout(this.activity);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.MATCH_PARENT);
		lp.topMargin = topMargin;
		lp.bottomMargin = bottomMargin;
		lp.leftMargin = leftMargin;
		lp.rightMargin = rightMargin;
		ly.setLayoutParams(lp);
		ly.setOrientation(orientation);
		ly.setGravity(gravity);
		for(View view : views)
			ly.addView(view);
		return ly;
	}

	public <T>void initObjectsWithoutXML(T container, View viewContainer){
		mapFields = new HashMap<String, Field>();
		Field[] cajas = container.getClass().getDeclaredFields();
		FieldAnnotation aFAnnotation = null;
		List<View> views = new ArrayList<View>();
		fieldsOrdered = new View[cajas.length-1];
		fieldsOrderedxAnot = orderFields(cajas);
		callbackEvents = new HashMap<View, HashMap<Integer, Object[]>>();
		if(containerFieldsOrdered == null) containerFieldsOrdered = new HashMap<Object, View[]>();
		fieldsPosition = new HashMap<Object, Integer>();
		if(containerFieldsPosition == null) containerFieldsPosition = new HashMap<Object, HashMap<Object, Integer>>();
		if(containerNumFields == null) containerNumFields = new HashMap<Object, Integer>();
		if(containerbackgroundDrawableUnfocus == null) containerbackgroundDrawableUnfocus = new HashMap<Object, Drawable>();
		if(containerCallbackEvents == null) containerCallbackEvents = new HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>>();
		this.viewContainer = viewContainer;
		setContainerContext(container);
		for (int i = 0; i < cajas.length; i++) {
			String cajaNombre = "";
			try {
				cajaNombre = cajas[i].getName(); 
				aFAnnotation = cajas[i].getAnnotation(FieldAnnotation.class);
				if(cajas[i].getType() == EditText.class || cajas[i].getType() == TextAreaField.class
						|| cajas[i].getType() == DecimalField.class || cajas[i].getType() == IntegerField.class
						|| cajas[i].getType() == TextAutCompleteField.class || cajas[i].getType() == TextAreaField.class
						|| cajas[i].getType() == TextField.class || cajas[i].getType() == CheckBoxField.class
						|| cajas[i].getType() == RadioGroupOtherField.class || cajas[i].getType() == RadioGroupField.class
						|| cajas[i].getType() == SpinnerField.class || cajas[i].getType() == ButtonComponent.class
						|| cajas[i].getType() == DateTimeField.class || cajas[i].getType() == ImageViewField.class){//Caja de texto
					if(aFAnnotation != null) {
						views.add((View)cajas[i].get(container));
					}
				}
			} catch (IllegalArgumentException e) {
				Log.e(TAG + "**IllegalArgumentException**"+cajaNombre, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.e(TAG + "**IllegalAccessException "+cajaNombre, e.getMessage());
			} catch (ClassCastException e){
				Log.e(TAG + "**ClassCastException", cajaNombre);
			} catch (NullPointerException e){
				Log.e(TAG + "**NullPointerException", cajaNombre);
			}
		}
		fieldsOrdered = new View[views.size()+1];
		for(Field caja : cajas){
			try {
				if (caja.getName().startsWith("txt")) {
					mapFields.put(caja.getName().replace("txt", ""), caja);
				} else if (caja.getName().startsWith("chb")) {
					mapFields.put(caja.getName().replace("chb", ""), caja);
				} else if (caja.getName().startsWith("rg")) {
					mapFields.put(caja.getName().replace("rg", ""), caja);
				} else if (caja.getName().startsWith("spn")) {
					mapFields.put(caja.getName().replace("spn", ""), caja);
				}
				aFAnnotation = caja.getAnnotation(FieldAnnotation.class);
				String prefix = "txt";
				if(caja.getType() == EditText.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == TextAreaField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == DecimalField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == IntegerField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == TextField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				}
				//Para label aun no he implementado funcionalidad
				else if(caja.getType() == TextView.class){//label
					prefix = "";
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					prefix = "chb";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (CheckBoxField)caja.get(container);
					((CheckBoxField)caja.get(container)).setFocusable(true);
					((CheckBoxField)caja.get(container)).setFocusableInTouchMode(true);
					if (((CheckBoxField)caja.get(container)).getFieldName() == null) {
						((CheckBoxField)caja.get(container)).setFieldName(caja.getName().substring(3, caja.getName().length()));
					}
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupField)caja.get(container);
					((RadioGroupField)caja.get(container)).setFocusable(true);
					((RadioGroupField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == RadioGroupOtherField.class){//radiogroup
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupOtherField)caja.get(container);
					((RadioGroupOtherField)caja.get(container)).setFocusable(true);
					((RadioGroupOtherField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (SpinnerField)caja.get(container);
					((SpinnerField)caja.get(container)).setFocusable(true);
					((SpinnerField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == DateTimeField.class){//DateTimeField
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (DateTimeField)caja.get(container);
					((DateTimeField)caja.get(container)).setFocusable(true);
					((DateTimeField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == Spinner.class){//spinner
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Spinner)caja.get(container);
				}
				else if(caja.getType() == ButtonComponent.class){
					prefix = "";
					if(aFAnnotation != null) {
						fieldsOrdered[aFAnnotation.orderIndex()] = (ButtonComponent)caja.get(container);
					}
					//continue;
				}
				else if(caja.getType() == ImageViewField.class){
					prefix = "";
					if(aFAnnotation != null) {
						fieldsOrdered[aFAnnotation.orderIndex()] = (ImageViewField)caja.get(container);
					}
					//continue;
				}
				else if(caja.getType() == Button.class){
					prefix = "";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Button)caja.get(container);
					//continue;
				}
				else if(caja.getType() == LinearLayout.class){//label
					prefix = "";
				}
				else if(caja.getType() == ListView.class){//label
					prefix = "";
				}
				else {
					prefix = "";
				}
				if(aFAnnotation != null){
					fieldsPosition.put(caja.get(container), aFAnnotation.orderIndex());
					numFields = Math.max(numFields, aFAnnotation.orderIndex());
				}
				//if(aFAnnotation != null) numFields = Math.max(numFields, aFAnnotation.orderIndex());
				
				//((View)caja.get(this)).setFocusable(true);
				//((View)caja.get(this)).setFocusableInTouchMode(true);
				
				//onFocus
				if(caja.getType() != Button.class && caja.getType() != ButtonComponent.class) {					
					setHover((View)caja.get(container), prefix);
				}
				
			} catch (IllegalArgumentException e) {
				Log.d(TAG + "**IllegalArgumentException**"+caja.getName(), e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + "**IllegalAccessException "+caja.getName(), e.getMessage());
			} catch (ClassCastException e){
				Log.d(TAG + "**ClassCastException", caja.getName());
			} catch (NullPointerException e){
				Log.d(TAG + "**NullPointerException", caja.getName() + "--" + caja.getModifiers());
			}
			caja = null;
		}
		/*int contador = 0;
		for(View v:fieldsOrdered){
			Log.e("fieldsOrdered["+contador+"}", String.valueOf(v));
			contador++;
		}*/
		cajas = null;
//		fieldsOrdered = Arrays.copyOf(fieldsOrdered, numFields+1);
		containerFieldsOrdered.put(container, fieldsOrdered);
		containerFieldsPosition.put(container, fieldsPosition);
		containerNumFields.put(container, numFields);
		
		/*En el proyecto de hogares rural se necesita tener el nombre del persona de la 
		 * encuesta en la parte superior de algunas paginas. Por defecto voy a realizar la
		 * asignación del nombre completo*/
//		cargarNombrePersona();
	}
	
	public void setContainerContext(Object containerContext){
		//if( this.containerContext != null && rangeados!=null ) rangeados.remove(getContainerContext());
		this.containerContext = containerContext;
	}
	
	public Object getContainerContext(){
		if( this.containerContext == null ) return this;
		return this.containerContext;
	}
	
	protected void setHover(final View field, final String prefix){
		Object container = getContainerContext()==null?this:getContainerContext();
		setHover(container, field, prefix);
	}
	
	protected <T extends Object>void setHover(final T container, final View field, final String prefix){
		
		field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					backgroundDrawableUnfocus = v.getBackground();
					containerbackgroundDrawableUnfocus.put((Object)container, backgroundDrawableUnfocus);
					if(prefix.equals("chb") || prefix.equals("rg")){
						field.setBackgroundResource(R.color.verdeaguatransparent);
//						imm = (InputMethodManager) FragmentForm.this.this.activity.getSystemService(Context.INPUT_METHOD_SERVICE);							
//						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					} else if(prefix.equals("txt") || prefix.equals("spn")){
						//backgroundDrawableUnfocus = v.getBackground();
						if (prefix.equals("txt")) {
							((EditText)field).setBackgroundDrawable(getResources().getDrawable(R.drawable.backwithborderfocused));
//							if(getUserVisibleHint() && field.getClass() == DateTimeField.class){
////								getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//							}
							
						} else {
							field.setBackgroundResource(R.color.verdeagua);	
						}
						//INI-Elthon-20140305
						if ("txt".equals(prefix)) {
							//mostrar teclado
//							imm = (InputMethodManager) FragmentForm.this.this.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						} else {
//							imm = (InputMethodManager) FragmentForm.this.this.activity.getSystemService(Context.INPUT_METHOD_SERVICE);							
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
						//FIN-Elthon-20140305 y Bardales 2014
					}
					try {
						if (field instanceof TextBoxField) {
//							((TextBoxField)field).callback();
							String callback = "";
							try {
					            Method method;
					            List<String> callbacks = ((TextBoxField)field).getCallbacksOnFocusGained();
					            if (callbacks != null) {
						            for (String c : callbacks) {
						            	callback = c;
//						            	method = containerContext.getClass().getDeclaredMethod(c);
//										if (method == null) {
//											continue;
//										}
//						            	method.invoke(containerContext);	
						            	try {
							            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
											if (method != null) {
												method.invoke(containerContext, (TextBoxField)field);
											} else {
												
											}	
						            	} catch (NoSuchMethodException nsme) {
								            Log.d(getClass().getSimpleName(),"ssNo existe el metodo "+callback+" con parametros en "+this.getClass());
								            try {
									            method = containerContext.getClass().getDeclaredMethod(c);
									            if (method == null) continue;
								            	method.invoke(containerContext);	
								            } catch (NoSuchMethodException nsme2) {
								            	Log.d(getClass().getSimpleName(),"ppNo existe el metodo "+callback+" con parametros en "+this.getClass());
								            	throw new NoSuchMethodException("nnNo existe el metodo "+callback+" en "+this.getClass());
								            }
								        }	
									}	
								}
					        } catch (NoSuchMethodException nsme) {
					            throw new RuntimeException(
					                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
					        } catch (SecurityException se) {
					        	Log.e("FragmentForm", se.toString(), se);
					        } catch (IllegalArgumentException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (IllegalAccessException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (InvocationTargetException ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        } catch (Exception ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        }
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
					}
				}
				else {
					if(prefix.equals("chb") || prefix.equals("rg")){
						//field.setBackgroundResource(R.color.transparent);
					}
					else if(prefix.equals("txt") || prefix.equals("spn")){
						//field.setBackgroundResource(R.color.cajaweb);
						//field.setBackgroundDrawable(backgroundDrawableUnfocus);
						if ("txt".equals(prefix)) {
//							imm = (InputMethodManager) FragmentForm.this.this.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
					backgroundDrawableUnfocus = containerbackgroundDrawableUnfocus.get((Object)container);
					if (prefix.equals("txt")) {
						((EditText)field).setBackgroundDrawable(getResources().getDrawable(R.drawable.backwithborder));
					} else {
						field.setBackgroundDrawable(backgroundDrawableUnfocus);
					}
					try {						
//						if (field instanceof DecimalField) {
//							((DecimalField)field).callback();
//						} else if (field instanceof IntegerField) {
//							((IntegerField)field).callback();
//						} else if (field instanceof TextField) {
//							((TextField)field).callback();
//						} else if (field instanceof TextAreaField) {
//							((TextAreaField)field).callback();
//						}
						if (field instanceof TextBoxField) {
							((TextBoxField)field).callback();
							String callback = "";
							try {
					            Method method;
					            List<String> callbacks = ((TextBoxField)field).getCallbacksOnFocus();
					            if (callbacks != null) {
						            for (String c : callbacks) {
						            	callback = c;
						            	method = containerContext.getClass().getDeclaredMethod(c);
										if (method == null) {
											continue;
										}
						            	method.invoke(containerContext);													            
									}	
								}
					        } catch (NoSuchMethodException nsme) {
					            throw new RuntimeException(
					                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
					        } catch (SecurityException se) {
					        	Log.e("FragmentForm", se.toString(), se);
					        } catch (IllegalArgumentException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (IllegalAccessException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (InvocationTargetException ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        } catch (Exception ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        }
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
					}
				}
			}
		});		
	}

	public boolean isInRange() {
		
		List<NumberField> numberFields = getCamposRangeados(getContainerContext());
		if (numberFields.size() != 0) {
			for (NumberField field : numberFields) {
				field.callback();
				String callback = "";
				try {
		            Method method = null;
		            List<String> callbacks = field.getCallbacks();
		            if (callbacks != null) {
		            	for (String c : callbacks) {
			            	callback = c;
			            	if (c.equals("verificarRangos")) {
			            		method = field.getClass().getDeclaredMethod(c);
							}
			            	if (method == null) {
								continue;
							}
			            	if (c.equals("verificarRangos")) {
			            		method.invoke((NumberField)field);
							}
				            if (c.equals("verificarRangos")) {
				            	if (!field.isRangoOk()) {
									ToastMessage.msgBox(this.activity, field.getMensajeRango(), 
											ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
									field.requestFocus();
									return false;
								}
				            }
						}
		            }
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e("FragmentForm", se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        }
			}
		}		
		return true;
	}
	
	private List<NumberField> getCamposRangeados(Object container) {
		if (camposRangeados == null) {
			camposRangeados = new HashMap<Object, List<NumberField>>();
			camposRangeados.put(container, new ArrayList<NumberField>());
		}
		if (camposRangeados.get(container) == null) {
			camposRangeados.put(container, new ArrayList<NumberField>());
		}
		return camposRangeados.get(container);
	}
	
	protected void enlazarCajas(){
		enlazarCajas(this);
	}
	
	public <T>void enlazarCajas(T container){
		this.fieldsOrdered = containerFieldsOrdered.get(container);
		
		for(int i=0; i<=fieldsOrdered.length-1; i++){
			View from = fieldsOrdered[i];
			if(from == null) continue;
			View to = null;
			if (i + 1 <= fieldsOrdered.length - 1) {
				to = fieldsOrdered[i+1];
			}			 
			if (from instanceof TextBoxField) {
				jumpFromTextBoxField(i, (TextBoxField)from, to, null, (TextBoxField)from, null, null, null);
			} 
			else if (from instanceof TextAutCompleteField) {
				jumpFromTextBoxField(i, (TextAutCompleteField)from, to, null, (TextAutCompleteField)from, null, null, null);
			} else if (from instanceof CheckBoxField) {
				jumpFromCheckBoxField(i, (CheckBoxField)from, to, null, (CheckBoxField)from, null, null, null);
			} else if (from instanceof RadioGroupOtherField) {
				jumpFromRadioGroupField(i, (RadioGroupOtherField)from, to, null, (RadioGroupOtherField)from, null, null, null);
			} else if (from instanceof SpinnerField) {
				jumpFromSpinnerField(i, (SpinnerField)from, to, null, (SpinnerField)from, null, null, null);
			}							
			from = to = null;
		}
	}
	
	private void hideKeyboard(View view){
		if(view == null) return;
		if(view.getClass() == DateTimeField.class){
 			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
 		}
	}
	
	public void listening(){
		
		Iterator<View> viewsIter = callbackEvents.keySet().iterator();
		imm = (InputMethodManager)getDialog().getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		while(viewsIter.hasNext()){
			
			final View from = viewsIter.next();
			final HashMap<Integer, Object[]> call = callbackEvents.get(from);
			
	 		if(call == null) continue;
	 		
	 		from.post(new Runnable() {
				
				@Override
				public void run() {
					if(from.getClass() == EditText.class || from.getClass() == DecimalField.class || from.getClass() == IntegerField.class
							|| from.getClass() == TextField.class || from.getClass() == TextAreaField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		/*if(args[1]!=null && !((View)args[1]).isEnabled()&&KeyEvent.ACTION_UP==event.getAction()) {
							 			Log.e("entras444", String.valueOf(args[1]));
							 			//args[1]=from;
							 			return false;
							 		}*/
							 		if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)
							 			hideKeyboard((View)args[1]);
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});
			 			
					}
			 		else if(from.getClass() == SpinnerField.class){		
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
						 		
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});

			 			((SpinnerField)from).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			 				
							@Override
							public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
								if(!esCargaInicial)
									((SpinnerField)from).onItemSelectedListenerAlterno(arg0, arg1, arg2, arg3);
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onItemSelected(arg0, from, (View)args[1], (View)args[2], (View)args[3], arg2, arg3, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {}
						});
			 		}
			 		else if(from.getClass() == RadioGroupField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {		
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});			 			
			 			((RadioGroupField)from).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
							
							@Override
							public void onCheckedChanged(RadioGroup group, int checkedId) {
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged(group, (View)args[1], (View)args[2], (View)args[3], checkedId, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}
						});
			 		}
			 		else if(from.getClass() == RadioGroupOtherField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {		
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});			 			
			 			((RadioGroupOtherField)from).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
							
							@Override
							public void onCheckedChanged(RadioGroup group, int checkedId) {
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged((RadioGroupOtherField)from, (View)args[1], (View)args[2], (View)args[3], checkedId, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}
						});
			 		}
			 		else if(from.getClass() == CheckBoxField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
									
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});

			 			((CheckBoxField)from).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
							
							@Override
							public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
								if(!esCargaInicial)
									((CheckBoxField)from).onCheckedChangedAlterno(buttonView,isChecked);
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged(buttonView, (View)args[1], (View)args[2], (View)args[3], isChecked, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
								
							}
						});
			 		}
				}
			});
	 		
		}
		
		viewsIter = null;
	}
	
	private void jumpFromTextBoxField(final Integer fromPosition, EditText from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		Log.e("nuuuu","siiiii");
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					((TextBoxField)from).callback();
					if (!((TextBoxField)from).esCoincidente()) {
						ToastMessage.msgBox(DialogFragmentComponent.this.activity, "ERROR: Valores no coinciden.", 
								ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						from.requestFocus();
						return true;
					} 
					if (!((TextBoxField)from).esDigitacionFinalizada()) {
						from.requestFocus();
						return true;
					} else {				
//						if (to != null) {
//							if (to.isEnabled() && to.getVisibility() == View.VISIBLE) {
//			            		from.setNextFocusDownId(to.getId());
//								to.requestFocus();
//							} else {
//								setNextFocusRequest(fromPosition, from);
//							}
//						} else {
//							from.clearFocus();
//						}
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
					            if (callbacks.size() == 0) {
				            		setNextFocusRequest(fromPosition, from);
				            		return true;
								}
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										try {
							            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
											if (method != null) {
												method.invoke(containerContext, from);
											} else {
												
											}	
						            	} catch (NoSuchMethodException nsme) {
								            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
								            try {
									            method = containerContext.getClass().getDeclaredMethod(c);
												if (method != null) {
													method.invoke(containerContext);
												} else {
													continue;
												}
								            } catch (NoSuchMethodException nsme2) {
								            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
								            	throw new NoSuchMethodException("No existe el metodo "+callback+" en "+this.getClass());
								            }
								        }
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		if (to != null) {
						            			if (to.isEnabled()) {
								            		from.setNextFocusDownId(to.getId());
													to.requestFocus();
												} else {
													setNextFocusRequest(fromPosition, from);
												}
											} else {
												from.clearFocus();
											}
										} else {
											ToastMessage.msgBox(DialogFragmentComponent.this.activity, ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
				            	
							} else {
								if (to != null) {
									if (to.isEnabled()) {
					            		from.setNextFocusDownId(to.getId());
										to.requestFocus();
									} else {
										setNextFocusRequest(fromPosition, from);
									}
								} else {
									from.clearFocus();
								}
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						setNextFocusRequest(fromPosition, from);
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	try {
						            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
										if (method != null) {
											method.invoke(containerContext, from);
										} else {
											
										}	
					            	} catch (NoSuchMethodException nsme) {
							            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
							            try {
								            method = containerContext.getClass().getDeclaredMethod(c);
											if (method != null) {
												method.invoke(containerContext);
											} else {
												continue;
											}
							            } catch (NoSuchMethodException nsme2) {
							            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
							            	throw new NoSuchMethodException("No existe el metodo "+callback+" en "+this.getClass());
							            }
							        }													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        } catch (Exception ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}					
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
//					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.setNextFocusDownId(from.getId());
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.-100");
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}	

	private void jumpFromRadioGroupField(final Integer fromPosition, RadioGroupOtherField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((RadioGroupOtherField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        } 
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpFromCheckBoxField(final Integer fromPosition, CheckBoxField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return true; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((CheckBoxField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        }													            
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpFromSpinnerField(final Integer fromPosition, SpinnerField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((SpinnerField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        }													            
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}

	protected void setNextFocusRequest(Integer fromPosition, View from) {
		for (int i = fromPosition + 1; i <= fieldsOrdered.length-1; i++) {
			if (fieldsOrdered[i] == null) {
				continue;
			}
			if (fieldsOrdered[i].isEnabled() && fieldsOrdered[i].isFocusable() && fieldsOrdered[i].isFocusableInTouchMode()
					&& fieldsOrdered[i].getVisibility() == View.VISIBLE) {
				from.setNextFocusDownId(fieldsOrdered[i].getId());
				if(fieldsOrdered[i].getClass() == DateTimeField.class){
					final DateTimeField dt = (DateTimeField)fieldsOrdered[i];
					fieldsOrdered[i].post(new Runnable() {
						@Override
						public void run() {
							dt.requestFocus();
						}
					});
				}
				else
					fieldsOrdered[i].requestFocus();
				return;
			}
		}
		from.clearFocus();
	}
	
	private HashMap<View, HashMap<Integer,Object[]>> addOrInsertHashMap(HashMap<View, HashMap<Integer, Object[]>> hash, View from, Object[] obj){
 		HashMap<Integer, Object[]> value = hash.get(from);
 		if(value == null){
 			hash.put(from, Util.getHashMap(Integer.valueOf(0), obj));
 		}
 		else{
 			value.put(Integer.valueOf(value.size()), obj);
 			hash.put(from, value);
 		}
 		containerCallbackEvents.put(getContainerContext(), hash);
 		return hash;
 	}

	protected void replaceText(TextView view, List<HashMap<String, String>> replaces){
		if(view==null || !(view instanceof TextView)) return;
		String preg = view.getText().toString();
        if(replaces.size()>0){
    		for(HashMap<String, String> hm:replaces){
    			for(Map.Entry<String, String> t:hm.entrySet()){
    				preg=preg.replace(t.getKey(), t.getValue());
    			}
    		}
    	}
        view.setText(preg);
	}
	
	public void replaceText(List<HashMap<String, String>> replaces, LinearLayout... layouts){
		if(layouts!=null){
			for(LinearLayout layout:layouts)
				replaceText(layout, replaces);
		}
	}
	
	protected void replaceText(LinearLayout layout, List<HashMap<String, String>> replaces){
		replaceText((TextView)layout.getChildAt(0), replaces);
	}
	
	protected void jump(FieldComponent from, View to, View next, HashMap<String, LinkedList> values) {
		Iterator<String> iter = values.keySet().iterator();
		boolean cumple = false;		
		while(iter.hasNext()){
			String oper = iter.next();
			LinkedList val = values.get(oper);
			if(Util.cumpleAlguno(from.toString(), val, oper, true)){
				handleLocks((View)from, to, true);
				cumple = true;
				((View)from).setNextFocusDownId(to.getId());
				if( !(to instanceof CheckBoxField || to instanceof Button) ) {
					Util.lockView(this.activity, false, to);
					to.requestFocus();
				}
				return;
			}
		}
		if(!cumple){
			handleLocks((View)from, to, false);
			if(next == null) return;
			Util.lockView(this.activity, false, next);
			((View)from).setNextFocusDownId(next.getId());			
			next.requestFocus();
			return;
		}
		return;
	}
	
	protected void handleLocks(View from, View to, boolean lock){
		Object containerContext = getContainerContext()==null?this:getContainerContext();
		
		handleLocks(from, to, lock, containerContext);
	}
	
	protected <T>void handleLocks(View from, View to, boolean lock, T container){
		if(from == null) return;
		List<View> views = blockerAndBlocked.get(from);
		
		//Primero desbloqueo todo lo que from haya bloqueado
		if(views != null) Util.lockView(this.activity, false, views);
		
		//Si se va a desbloquear elementos y from ha bloqueado elementos anteriormente antes entonces salir,
		//porque los elementos anteriormente bloqueados por from ya fueron desbloqueados
		if(!lock && views != null) return;
		
		/**TODO: Falta terminar el soporte para dialogos*/
		fieldsOrdered = containerFieldsOrdered.get(container);
		fieldsPosition = containerFieldsPosition.get(container);
		numFields = containerNumFields.get(container);
		
		Integer start = fieldsPosition.get(from);
		Integer end = fieldsPosition.get(to)==null?numFields:fieldsPosition.get(to);
		
		if(start == end || start == end-1) return;
		
		views = new ArrayList<View>();
		for(int i=start+1; i<=end-1; i++){
			View view = fieldsOrdered[i];
			if(lock) Util.cleanAndLockView(this.activity, view);
			else Util.lockView(this.activity, false, view);
			views.add(view);
		}
		
		blockerAndBlocked.put(from, views);
	}

	public <V> void rango(final Activity activity, final EditText editText, final Long minimo, final Long maximo, final LinkedList otros, final V omision){
		if (editText instanceof NumberField) {
//			Log.e(this.getClass().getSimpleName(), "verificarRangos");
			NumberField numberField = (NumberField) editText;
			numberField.setMinValue(minimo);
			numberField.setMaxValue(maximo);
			numberField.setOmision(omision);
			numberField.setExcepValues(otros);
			List<String> callbacksOld = numberField.getCallbacks();
			List<String> callbacks = new ArrayList<String>();
			callbacks.add("verificarRangos");
			if (callbacksOld != null) {
				callbacks.addAll(callbacksOld);	
			}
			numberField.setCallbacks(callbacks);
			getCamposRangeados(getContainerContext()).add(numberField);
			return;
		}
	}
	
	public <V> void rango(final Activity activity, final EditText editText, final Integer minimo, final Integer maximo, final LinkedList otros, final V omision){
		this.rango(activity, editText, Long.valueOf(minimo.toString()), Long.valueOf(maximo.toString()), otros, omision);
	}
//	
//	public <V> void rango(final Activity activity, final EditText editText, final Long minimo, final Long maximo, final LinkedList otros, final V omision){
//		this.rango(activity, editText, minimo, maximo, otros, omision);
//	}
	
	protected void entityToUI(Object entity){
		entityToUI(entity, this);
	}
	
	protected void mapToUI(Map<String, Object> map){
		mapToUI(map, this);
	}
	
	public void mapToUI(Map<String, Object> map, Object ui){
		for (int i = 0; i < fieldsOrdered.length; i++) {
			Util.cleanView(this.activity, fieldsOrdered[i]);
		}
		if (map == null) return;
		esCargaInicial = true;
		try {
			for(Entry<String, Field> entry : mapFields.entrySet()){
				if (entry.getValue().getType() == EditText.class) {//Caja de texto
					((EditText)entry.getValue().get(ui)).setText(map.get(entry.getKey())==null?"":Util.getText(map.get(entry.getKey())));
				}else if(entry.getValue().getType() == IntegerField.class){//Caja de texto
					((IntegerField)entry.getValue().get(ui)).setValue(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == TextAreaField.class){//Caja de texto
					((TextAreaField)entry.getValue().get(ui)).setText(map.get(entry.getKey())==null?"":Util.getText(map.get(entry.getKey())));
				} else if(entry.getValue().getType() == DecimalField.class){//Caja de texto
					((DecimalField)entry.getValue().get(ui)).setValue(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == DateTimeField.class){//Caja de texto
					if (((DateTimeField)entry.getValue().get(ui)).isComodin()) {
						continue;
					}
					String formato = ((DateTimeField)entry.getValue().get(ui)).getFormato();
					if (formato == null) {
						continue;
					}
					if (map.get(entry.getKey()) == null) {
						((DateTimeField)entry.getValue().get(ui)).setValue(null);
						continue;
					}
					DateFormat formatter = new SimpleDateFormat(formato);
					String _res = Util.getText(map.get(entry.getKey()));
					if("99".equals(_res) || "9999".equals(_res)) {
						if(((DateTimeField)entry.getValue().get(ui)).getType() == TIPO_DIALOGO.FECHA)
							((DateTimeField)entry.getValue().get(ui)).setValue(Util.getFecha(9999, 99, 99));
						else if(((DateTimeField)entry.getValue().get(ui)).getType() == TIPO_DIALOGO.HORA)
							((DateTimeField)entry.getValue().get(ui)).setValue(Util.getHora(99, 99, 99));
					}
					else {
						Date value = formatter.parse(Util.getText(map.get(entry.getKey())));
						((DateTimeField)entry.getValue().get(ui)).setValue(value);
					}
				} else if(entry.getValue().getType() == CheckBoxField.class){//checkbox
					((CheckBoxField)entry.getValue().get(ui)).setCheckedTag(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == RadioGroupField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupField)entry.getValue().get(ui)).setTagSelected(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == RadioGroupOtherField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupOtherField)entry.getValue().get(ui)).setTagSelected(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == SpinnerField.class){//spinner
					((SpinnerField)entry.getValue().get(ui)).setSelectionKey(map.get(entry.getKey()));
				} else if(entry.getValue().getType() == ImageViewField.class){//spinner
					ImageViewField im = (ImageViewField)entry.getValue().get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						byte[] imageByte = map.get(entry.getKey()) == null ? null : (byte[])map.get(entry.getKey());
						im.setValue(Util.getImage(imageByte));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File file = new File(im.getPathSave() + File.separator + im.getFileName());
						if (file.exists()) {
							FileInputStream in;
							BufferedInputStream buf;
							in = new FileInputStream(im.getPathSave() + File.separator + im.getFileName());
							buf = new BufferedInputStream(in);
							byte[] bMapArray = new byte[buf.available()];
							buf.read(bMapArray);
							Bitmap bitmap = BitmapFactory.decodeByteArray(bMapArray, 0,
									bMapArray.length);
							im.setValue(bitmap);
							buf.close();
							in.close();		
						} else {
							im.setValue(null);
						}
					}
				}
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
		} catch (NullPointerException e) {
			Log.e(TAG, "NullPointer al leer el campo []");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void entityToUI(Object entity, Object ui){
		for (int i = 0; i < fieldsOrdered.length; i++) {
			Util.cleanView(this.activity, fieldsOrdered[i]);
		}
		if (entity == null) return;
		esCargaInicial = true;
		Field[] cajas = ui.getClass().getDeclaredFields();
		for(Field caja : cajas){
			
			String nombreCampo = "";
			if(caja.getName().length()<3) continue;
			if(!caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("") &&
					!caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) continue;
			
			if(caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("")) nombreCampo = caja.getName().substring(3).toLowerCase();
			else if(caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) nombreCampo = caja.getName().substring(2).toLowerCase();
			
			try {
				if(entity.getClass().getDeclaredField(nombreCampo) == null)	continue;
				
				Field campo = entity.getClass().getDeclaredField(nombreCampo);
				//Log.d(TAG, "--" + nombreCampo + "--" + (campo.get(entity)==null?"":campo.get(entity).toString()));
				//Log.d(TAG, "campo = " + campo.getName());
//				Log.e(TAG, "campo = " + campo.getName());
				if(caja.getType() == EditText.class){//Caja de texto
					((EditText)caja.get(ui)).setText(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				else if(caja.getType() == TextField.class){//Caja de texto
					((TextField)caja.get(ui)).setValue(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				else if(caja.getType() == TextAutCompleteField.class){//Caja de texto busca
					((TextAutCompleteField)caja.get(ui)).setValue(campo.get(entity)==null?"":campo.get(entity).toString());
				}
				else if(caja.getType() == IntegerField.class){//Caja de texto
					((IntegerField)caja.get(ui)).setValue(campo.get(entity));
				}
				else if(caja.getType() == TextAreaField.class){//Caja de texto
					((TextAreaField)caja.get(ui)).setText(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				else if(caja.getType() == DecimalField.class){//Caja de texto
					((DecimalField)caja.get(ui)).setValue(campo.get(entity));
				} 
				else if(caja.getType() == DateTimeField.class){//Caja de texto
					if (((DateTimeField)caja.get(ui)).isComodin()) {
						continue;
					}
					String formato = ((DateTimeField)caja.get(ui)).getFormato();
					if (formato == null) {
						continue;
					}
					if (campo.get(entity) == null) {
						((DateTimeField)caja.get(ui)).setValue(null);
						continue;
					}
					DateFormat formatter = new SimpleDateFormat(formato);
					String _res = campo.get(entity).toString();
					if("99".equals(_res) || "9999".equals(_res)) {
						if(((DateTimeField)caja.get(ui)).getType() == TIPO_DIALOGO.FECHA)
							((DateTimeField)caja.get(ui)).setValue(Util.getFecha(9999, 99, 99));
						else if(((DateTimeField)caja.get(ui)).getType() == TIPO_DIALOGO.HORA)
							((DateTimeField)caja.get(ui)).setValue(Util.getHora(99, 99, 99));
					}
					else {
						Date value = formatter.parse(campo.get(entity).toString());
						((DateTimeField)caja.get(ui)).setValue(value);
					}
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					((CheckBoxField)caja.get(ui)).setCheckedTag(campo.get(entity));
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupField)caja.get(ui)).setTagSelected(campo.get(entity));
				}
				else if(caja.getType() == RadioGroupOtherField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupOtherField)caja.get(ui)).setTagSelected(campo.get(entity));
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					((SpinnerField)caja.get(ui)).setSelectionKey(campo.get(entity));
				}
				else if(caja.getType() == ImageViewField.class){//spinner
					ImageViewField im = (ImageViewField)caja.get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						byte[] imageByte = campo.get(entity) == null ? null : (byte[])campo.get(entity);
						im.setValue(Util.getImage(imageByte));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File file = new File(im.getPathSave() + File.separator + im.getFileName());
						if (file.exists()) {
							FileInputStream in;
							BufferedInputStream buf;
							in = new FileInputStream(im.getPathSave() + File.separator + im.getFileName());
							buf = new BufferedInputStream(in);
							byte[] bMapArray = new byte[buf.available()];
							buf.read(bMapArray);
							Bitmap bitmap = BitmapFactory.decodeByteArray(bMapArray, 0,
									bMapArray.length);
							im.setValue(bitmap);
							buf.close();
							in.close();		
						} else {
							im.setValue(null);
						}
					}
				}
				
			} catch (IllegalArgumentException e) {
				Log.d(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, e.getMessage());
			} catch (NoSuchFieldException e) {
				if(caja.getType() == DateTimeField.class)
					setDateTimeFields(cajas, caja, entity, ui);
				Log.d(TAG, e.getMessage());
			} catch (NullPointerException e) {
				Log.e(TAG, "NullPointer al leer el campo [" + nombreCampo +"]");
			} catch (ParseException e) {
				Log.e(TAG, e.getMessage());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			caja = null;
		}
		cajas = null;
		esCargaInicial = false;
	}
	
	protected void uiToEntity(Object entity){
		this.uiToEntity(this, entity);
	}
	
	protected void uiToMap(Map<String, Object> map){
		this.uiToMap(this, map);
	}
	
	private Field[] orderFields(Field[] fields){
		ArrayList<Field> afc = new ArrayList<Field>();
		ArrayList<Field> afs = new ArrayList<Field>();
		for(int y=0;y<fields.length;y++){
			FieldAnnotation ant = fields[y].getAnnotation(FieldAnnotation.class);
			if(ant != null){
				afc.add(fields[y]);
			} else {
				afs.add(fields[y]);
			}
		}
		afc.addAll(afs);
		return afc.toArray(new Field[fields.length]);
	}
	
	/**
	 * Obtiene los view de la actividad y coloca sus valores en la entidad
	 * @param entity
	 * @return
	 */
	public void uiToEntity(Object ui, Object entity){
		if (entity == null) return ;
		
		Field[] cajas =  fieldsOrderedxAnot; //ui.getClass().getDeclaredFields();	
		String nombreCampo = "";	
		
		for(Field caja : cajas){	
			try {
				if(caja.getName().toString().length()<3) continue;
				if(!caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("") &&
						!caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) continue;
				
				if(caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("")) nombreCampo = caja.getName().substring(3).toLowerCase();
				else if(caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) nombreCampo = caja.getName().substring(2).toLowerCase();
				
				if(entity.getClass().getDeclaredField(nombreCampo) == null) continue;
				
				Field campo = entity.getClass().getDeclaredField(nombreCampo);
				if (caja.getType() == TextBoxField.class) {
					((TextBoxField)caja.get(ui)).callback();	
				}
				if(caja.getType() == TextAreaField.class){//Caja de texto
					TextAreaField editText = (TextAreaField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
				} 
			
				else if(caja.getType() == TextField.class){//Caja de texto
					TextField editText = (TextField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					editText.callback();
				}
				else if(caja.getType() == TextAutCompleteField.class){//Caja de texto busca
					TextAutCompleteField editText = (TextAutCompleteField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					//editText.callback();
				}
				
				else if(caja.getType() == IntegerField.class){//Caja de texto
					IntegerField editText = (IntegerField)caja.get(ui);
					if(campo.getType() == Integer.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new Integer(editText.getText().toString()));
					} else if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					editText.callback();
				} else if(caja.getType() == DecimalField.class){//Caja de texto
					DecimalField editText = (DecimalField)caja.get(ui);
					if(campo.getType() == BigDecimal.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new BigDecimal(editText.getText().toString()));
					}
					editText.callback();
				} else if(caja.getType() == DateTimeField.class){//Caja de texto
					DateTimeField editText = (DateTimeField)caja.get(ui);
					if (campo.getType() == Integer.class) {
						campo.set(entity, editText.getText().toString().equals("") ? null : Integer.valueOf(editText.getStringValue()));
					}
					if (campo.getType() == String.class) {
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getStringValue());
					}
//						editText.callback();
				}
				else if(caja.getType() == TextBoxField.class){//Caja de texto
					TextBoxField editText = (TextBoxField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					editText.callback();
				} 
				else if(caja.getType() == EditText.class){//Caja de texto
					EditText editText = (EditText)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : Integer.parseInt(editText.getText().toString()));
					}
					else if(campo.getType() == BigDecimal.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new BigDecimal(editText.getText().toString()));
					}
				} else if(caja.getType() == CheckBoxField.class){//checkbox 
					//campo.set(entity, ((CenacomCheckBox)caja.get(this)).getCheckedTag());
					CheckBoxField checkbox = (CheckBoxField)caja.get(ui);
					if (checkbox.getGroup() != null) {
						agregarCheckBoxFieldHijo(checkbox);
						continue;
					}
					if (campo.getType() == String.class){
						campo.set(entity, checkbox.getCheckedTag()==null ? null : checkbox.getCheckedTag().toString());
					} else if(campo.getType() == Integer.class){
						campo.set(entity, checkbox.getCheckedTag()==null ? null : Integer.parseInt(checkbox.getCheckedTag().toString()));
					}
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup 
					RadioGroupField radiogroup = (RadioGroupField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected().toString());
					} else if(campo.getType() == Integer.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : Integer.parseInt(radiogroup.getTagSelected().toString()));
					}
				}
				else if(caja.getType() == RadioGroupOtherField.class){//radiogroup 
					RadioGroupOtherField radiogroup = (RadioGroupOtherField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected().toString());
					} else if(campo.getType() == Integer.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : Integer.parseInt(radiogroup.getTagSelected().toString()));
					}
				}
				else if(caja.getType() == SpinnerField.class){//spinner 
					SpinnerField spinner = (SpinnerField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : spinner.getSelectedItemKey().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : Integer.parseInt(spinner.getSelectedItemKey().toString()));
					}
				}
				else if(caja.getType() == ImageViewField.class){//ImageViewField 
					ImageViewField im = (ImageViewField)caja.get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						campo.set(entity, Util.getBytes((Bitmap)im.getValue()));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File carpeta = new File(im.getPathSave());
						if (!carpeta.exists()) {
							carpeta.mkdir();
						}
						Bitmap bitmap = (Bitmap) im.getValue();
						if (bitmap != null) {
							String fileName = im.getPathSave() + File.separator + im.getFileName();
							Util.savePicture(bitmap, fileName);	
						}
					}
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			} catch (NullPointerException e) {
				Log.d(TAG + " - NullPointerException", "El objeto " + nombreCampo + " es nulo");
			} catch (IOException e) {
				Log.e(TAG + " - IOException", "El objeto " + nombreCampo + " fallo al ser grabado en archivo");
			}
		}
		try {
			if (checkBoxGroups != null) {
//				Log.e(TAG, "Existen checkBoxGroups");
				for (Map.Entry<String, List<CheckBoxField>> entry : checkBoxGroups.entrySet()) {
//					Log.e(TAG, "Primer Grupo de Check: " + entry.getKey());
					boolean todoDesmarcado = true;
					List<CheckBoxField> group = checkBoxGroups.get(entry.getKey());
					for (CheckBoxField cb : group) {
						if (cb.isChecked()) {
							todoDesmarcado = false;
//							Log.e(TAG, "En Grupo de Check: " + entry.getKey() + " existe uno marcado");
							break;
						}
					}
					if (todoDesmarcado) {
//						Log.e(TAG, "Colocando null al Grupo de Check: " + entry.getKey());
						for (CheckBoxField cb : group) {
							Field campo = entity.getClass().getDeclaredField(cb.getFieldName().toLowerCase());
							campo.set(entity, null);
						}
					} else {
//						Log.e(TAG, "Colocando valor a cada CheckBox del grupo: " + entry.getKey());
						for (CheckBoxField cb : group) {
							Field campo = entity.getClass().getDeclaredField(cb.getFieldName().toLowerCase());
							if (campo.getType() == String.class){
								campo.set(entity, cb.getCheckedTag()==null ? null : cb.getCheckedTag().toString());
							} else if(campo.getType() == Integer.class){
								campo.set(entity, cb.getCheckedTag()==null ? null : Integer.parseInt(cb.getCheckedTag().toString()));
							}
						}						
					}
				}
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG + " - IllegalArgumentException", e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG + " - IllegalAccessException", e.getMessage());
		} catch (NoSuchFieldException e) {
			Log.d(TAG + " - NoSuchFieldException", e.getMessage());
		} catch (NullPointerException e) {
			Log.d(TAG + " - NullPointerException", "El objeto " + nombreCampo + " es nulo");
		}
		cajas = null;
//		return entity;
	}
	
	/**
	 * Obtiene los view de la actividad y coloca sus valores en la entidad
	 * @param entity
	 * @return
	 */
	public void uiToMap(Object ui, Map<String, Object> map){
		if (map == null) return;		
		Field[] cajas =  fieldsOrderedxAnot; //ui.getClass().getDeclaredFields();	
		String nombreCampo = "";	
		try {		
			for(Entry<String, Field> entry : mapFields.entrySet()){
				if (entry.getValue().getType() == TextBoxField.class) {
					((TextBoxField)entry.getValue().get(ui)).callback();	
				}			
				if(entry.getValue().getType() == DateTimeField.class){//Caja de texto
					DateTimeField editText = (DateTimeField)entry.getValue().get(ui);
					map.put(entry.getKey(), editText.getStringValue());
					//TODO revisar cuando es datetimefield
	//				if (campo.getType() == Integer.class) {
	//					campo.set(entity, editText.getText().toString().equals("") ? null : Integer.valueOf(editText.getStringValue()));
	//				}
	//				if (campo.getType() == String.class) {
	//					campo.set(entity, editText.getText().toString().equals("") ? null : editText.getStringValue());
	//				}
				} else if (entry.getValue().getType() == TextBoxField.class) {//Caja de texto
					map.put(entry.getKey(), ((TextBoxField)entry.getValue().get(ui)).getText().toString().equals("") ? null : ((TextBoxField)entry.getValue().get(ui)).getText().toString());
				} else if(entry.getValue().getType() == CheckBoxField.class){//checkbox 
					CheckBoxField checkbox = (CheckBoxField)entry.getValue().get(ui);
					if (checkbox.getGroup() != null) {
						agregarCheckBoxFieldHijo(checkbox);
						continue;
					}
					map.put(entry.getKey(), checkbox.getCheckedTag()==null ? null : checkbox.getCheckedTag());
				} else if(entry.getValue().getType() == RadioGroupOtherField.class){//radiogroup 
					RadioGroupOtherField radiogroup = (RadioGroupOtherField)entry.getValue().get(ui);
					map.put(entry.getKey(), radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected());				
				} else if(entry.getValue().getType() == SpinnerField.class){//spinner 
					SpinnerField spinner = (SpinnerField)entry.getValue().get(ui);
					map.put(entry.getKey(), spinner.getSelectedItemKey()==null ? null : spinner.getSelectedItemKey());
				} else if(entry.getValue().getType() == ImageViewField.class){//ImageViewField 
					ImageViewField im = (ImageViewField)entry.getValue().get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						map.put(entry.getKey(), im.getValue()==null ? null : Util.getBytes((Bitmap)im.getValue()));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File carpeta = new File(im.getPathSave());
						if (!carpeta.exists()) {
							carpeta.mkdir();
						}
						Bitmap bitmap = (Bitmap) im.getValue();
						if (bitmap != null) {
							String fileName = im.getPathSave() + File.separator + im.getFileName();
							Util.savePicture(bitmap, fileName);	
						}
					}
				}
			}
			if (checkBoxGroups != null) {
//				Log.e(TAG, "Existen checkBoxGroups");
				for (Map.Entry<String, List<CheckBoxField>> entry : checkBoxGroups.entrySet()) {
//					Log.e(TAG, "Primer Grupo de Check: " + entry.getKey());
					boolean todoDesmarcado = true;
					List<CheckBoxField> group = checkBoxGroups.get(entry.getKey());
					for (CheckBoxField cb : group) {
						if (cb.isChecked()) {
							todoDesmarcado = false;
//							Log.e(TAG, "En Grupo de Check: " + entry.getKey() + " existe uno marcado");
							break;
						}
					}
					if (todoDesmarcado) {
//						Log.e(TAG, "Colocando null al Grupo de Check: " + entry.getKey());
						for (CheckBoxField cb : group) {
							map.put(cb.getFieldName(), null);
						}
					} else {
//						Log.e(TAG, "Colocando valor a cada CheckBox del grupo: " + entry.getKey());
						for (CheckBoxField cb : group) {
							map.put(cb.getFieldName(), cb.getCheckedTag());
						}						
					}
				}
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG + " - IllegalArgumentException", e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG + " - IllegalAccessException", e.getMessage());
		} catch (NullPointerException e) {
			Log.d(TAG + " - NullPointerException", "El objeto " + nombreCampo + " es nulo");
		} catch (IOException e) {
			Log.d(TAG + " - IOException", e.getMessage());
		}
		cajas = null;
	}
	
	private void setDateTimeFields(Field[] cajas, Field caja, Object entity, Object ui){
		
		try {
			DateTimeField dtf = ((DateTimeField)caja.get(ui));
			if(dtf.isdateOrhour()){
				EditText[] edt = dtf.getDateEditText();
				String formato = dtf.getFormato();
				String day="00", month="00", year="0000";
				for(Field c : cajas){
					if(c.getType() == IntegerField.class){
						for(int x=0;x<edt.length;x++){
							if(c.get(ui).equals(edt[x])){
								String nCampo = c.getName().substring(3).toLowerCase();
								Field cpo = entity.getClass().getDeclaredField(nCampo);
								if(cpo.get(entity) == null) {
									dtf.setValue(dtf.getValueDefault(null));
									return;
								}
								switch (x) {
									case 0: day = cpo.get(entity).toString(); break;
									case 1: month = cpo.get(entity).toString(); break;
									case 2: year = cpo.get(entity).toString(); break;
									default: break;
								}
								break;
							}
						}
					}
				}
				
				if(dtf.getType() == TIPO_DIALOGO.FECHA) {
					String fecha = Util.getFechaFormateada(year,month,day, formato);
//					Log.e("fromato", "fromato: "+formato);
//					Log.e("fecha", "fecha: "+fecha);
					dtf.setValue(Util.getFechaFormateada(fecha, formato));
				} else {
					Log.e("fromato", "day: "+day);
					Log.e("fecha", "month: "+month);
					if(day.equals("99") && month.equals("99")) dtf.setValue(Util.getHora(99, 99, 99));
					else dtf.setValue(Util.getHora(day, month, "00"));
				}

			}
		} catch (IllegalArgumentException e) {
			Log.e("IllegalArgumentException", "IllegalArgumentException: "+e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e("IllegalAccessException", "IllegalAccessException: "+e.getMessage());
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			Log.e("NoSuchFieldException", "NoSuchFieldException: "+e.getMessage());
			e.printStackTrace();
		} 
	}

	private void agregarCheckBoxFieldHijo(CheckBoxField checkBox) {
		String key = checkBox.getGroup();
		if (checkBoxGroups == null) {
			checkBoxGroups = new HashMap<String, List<CheckBoxField>>();
		}
		if (checkBoxGroups.containsKey(key)) {
			List<CheckBoxField> group = checkBoxGroups.get(key);
			if (!group.contains(checkBox)) {
				group.add(checkBox);
			}
		} else {
			List<CheckBoxField> group = new ArrayList<CheckBoxField>();
			group.add(checkBox);
			checkBoxGroups.put(key, group);
		}
	}
	
	public View[] getFieldsOrdered() {
		return fieldsOrdered;
	}
	
	public List<TextField> getTextFields() {
		List<TextField> texts = new ArrayList<TextField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof TextField) {
				texts.add((TextField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<IntegerField> getIntegerFields() {
		List<IntegerField> texts = new ArrayList<IntegerField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof IntegerField) {
				texts.add((IntegerField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<DecimalField> getDecimalFields() {
		List<DecimalField> texts = new ArrayList<DecimalField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof DecimalField) {
				texts.add((DecimalField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<CheckBoxField> getCheckBoxFields() {
		List<CheckBoxField> texts = new ArrayList<CheckBoxField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof CheckBoxField) {
				texts.add((CheckBoxField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<RadioGroupOtherField> getRadioGroupOtherFields() {
		List<RadioGroupOtherField> texts = new ArrayList<RadioGroupOtherField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof RadioGroupOtherField) {
				texts.add((RadioGroupOtherField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public View getView(String name) {
		View v = null;
		Field field;
		try {
			field = getContainerContext().getClass().getDeclaredField(name);
			v = (View) field.get(containerContext);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}				
		return v;
	}
	
	public RadioGroupOtherField getRadioGroupOtherField(String name) {
		RadioGroupOtherField rg = null;
		View v = getView(name);
		if (v != null) {
			try {
				rg = (RadioGroupOtherField) v;
			} catch (ClassCastException e) {				
			}
		}
		return rg;
	}
	
	public IntegerField getIntegerField(String name) {
		IntegerField inf = null;
		View v = getView(name);
		if (v != null) {
			try {
				inf = (IntegerField) v;
			} catch (ClassCastException e) {				
			}
		}
		return inf;
	}
}
