package gob.inei.dnce.components;

import gob.inei.dnce.R;
import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.interfaces.CallbackEvent;
import gob.inei.dnce.interfaces.EventCheckedCheck;
import gob.inei.dnce.interfaces.EventCheckedRadio;
import gob.inei.dnce.interfaces.EventKey;
import gob.inei.dnce.interfaces.EventSelectedItem;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.IFormComponent;
import gob.inei.dnce.util.PeriodoReferencia;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * 
 * @author ajulcamoro <julcamorosm@gmail.com> || <ajulcamoroq@unc.edu.pe> 
 *
 */
public abstract class FragmentForm extends Fragment implements IFormComponent {
	
	private static String TAG = "FragmentForm";
	
	public static int EVENT_ALL = 0;
	public static int EVENT_NONE = 1;
	public static int EVENT_CHECKED_CHECK = 2;
	public static int EVENT_CHECKED_RADIO = 3;
	public static int EVENT_KEY = 4;
	public static int EVENT_ITEM_SELECTED = 5;
	public static int EVENT_FOCUS = 6;
	
	public static final int MATCH_PARENT = LayoutParams.MATCH_PARENT;
	public static final int WRAP_CONTENT = LayoutParams.WRAP_CONTENT;
	public static enum OPCION{ONE,TWO,THREE,FOUR}
	
	public boolean returnFocus = false;
	public View viewLostFocus = null;
	
	protected Menu menu;
	protected Context dialog_context = this.getActivity();
	protected String obsGeneral = null;
	protected String mensaje = "";
	protected boolean error = false;
	protected View view = null;
	/*protected static final Logger logger = LoggerFactory.getLogger();*/
	protected TextView tv;
	private Display display;
	public static Point SIZE_DISPLAY;
	
	protected PeriodoReferencia periodoReferencia;
	public static int altoComponente = 55;
	private int cellSpacing = 2;
	private int topMargin = 10;
	private int bottomMargin = 10;
	private int leftMargin = 10;
	private int rightMargin = 10;
	public static int textQuestionSize = 19;

//	protected GPSTrackerOld gpsTracker;
	
	protected Date fechaActual = Calendar.getInstance().getTime();
	
	protected View rootView;
	
	protected MasterActivity parent;
	
	/**
	 * Lista de campos ordenados segun se indica con la anotacion
	 */
	View[] fieldsOrdered = null;
	
	/**
	 * Esta variable contiene clave=campo y valor=[orden segun anotacion(entero), numero de pregunta(texto)]
	 */
	HashMap<Object, Integer> fieldsPosition = null;
	
	/**
	 * Mapa de preguntas: clave="numero de pregunta", valor="array de views que conforman la pregunta"
	 * Aun no usado
	 */
	HashMap<String, View[]> questionView = null;
	
	/**
	 * clave = contenedor(ventana principal o dialogo), valor = fieldsOrdered
	 * Contenedor, objetos
	 */
	HashMap<Object, View[]> containerFieldsOrdered;
	
	/**
	 * clave = contenedor(ventana principal o dialogo), valor = fieldsPosition
	 */
	HashMap<Object, HashMap<Object, Integer>> containerFieldsPosition;
	
	HashMap<Object, ArrayList<View>> rangeados;
	
	HashMap<Object,List<NumberField>> camposRangeados;
	
	View viewContainer = null;
	Object containerContext = null;
	
	HashMap<Object, Integer> containerNumFields;
	
	HashMap<View, List<View>> blockerAndBlocked;
	
	Drawable backgroundDrawableFocus = null;
	Drawable backgroundDrawableUnfocus = null;
	
	HashMap<Object, Drawable> containerbackgroundDrawableUnfocus;
	
	Integer numFields = 0;
	InputMethodManager imm;
	
	/**
	 * Object[0] = CallbackEvent
	 * Object[1] = to
	 * Object[2] = next
	 * Object[3] = dataSource
	 * Object[4] = values
	 * Object[5] = cancel
	 * Object[6] = allEquals
	 */
	HashMap<View, HashMap<Integer, Object[]>> callbackEvents = null;
	
	HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>> containerCallbackEvents = null;
	boolean esCargaInicial;
	public static int COLOR_LINEA_SECCION_PREGUNTA = R.color.marco;
	private Map<String, List<CheckBoxField>> checkBoxGroups = null;
//	protected final Logger logger = LoggerFactory.getLogger();
//	
//	public Logger getLogger() {
//		return logger;
//	}
	
	public FragmentForm() {		
	}
	
	public MasterActivity getParent(){
		return parent;
	}
	
	public void setContainerContext(Object containerContext){
		//if( this.containerContext != null && rangeados!=null ) rangeados.remove(getContainerContext());
		this.containerContext = containerContext;
	}
	
	public Object getContainerContext(){
		if( this.containerContext == null ) return this;
		return this.containerContext;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		blockerAndBlocked = new HashMap<View, List<View>>();
		display = getActivity().getWindowManager().getDefaultDisplay();
		SIZE_DISPLAY = new Point();
		display.getSize(SIZE_DISPLAY);
	}
	
	protected void entityToUI(Object entity){
		entityToUI(entity, this);
	}
	
	/**
	 * Obtiene los datos de la entidad y los coloca en los view de la actividad
	 * @param entity
	 */
	public void entityToUI(Object entity, Object ui){
		for (int i = 0; i < fieldsOrdered.length; i++) {
			Util.cleanView(getActivity(), fieldsOrdered[i]);
		}
		esCargaInicial = true;
		if (entity == null) return;		
		Field[] cajas = ui.getClass().getDeclaredFields();
		for(Field caja : cajas){
			String nombreCampo = "";
			if(caja.getName().length()<3) continue;
			if(!caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("") &&
					!caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) continue;
			
			if(caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("")) nombreCampo = caja.getName().substring(3).toLowerCase();
			else if(caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) nombreCampo = caja.getName().substring(2).toLowerCase();
			
			try {
				if(entity.getClass().getDeclaredField(nombreCampo) == null) continue;
				
				Field campo = entity.getClass().getDeclaredField(nombreCampo);
				//Log.d(TAG, "--" + nombreCampo + "--" + (campo.get(entity)==null?"":campo.get(entity).toString()));
				//Log.d(TAG, "campo = " + campo.getName());
				if(caja.getType() == EditText.class){//Caja de texto
					((EditText)caja.get(ui)).setText(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				else if(caja.getType() == TextField.class){//Caja de texto
					((TextField)caja.get(ui)).setValue(campo.get(entity)==null?"":campo.get(entity).toString());
					((TextField)caja.get(ui)).callback();
				} 
				else if(caja.getType() == IntegerField.class){//Caja de texto
					((IntegerField)caja.get(ui)).setValue(campo.get(entity));
					((IntegerField)caja.get(ui)).callback();
				}
				else if(caja.getType() == TextAreaField.class){//Caja de texto
					((TextAreaField)caja.get(ui)).setText(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				else if(caja.getType() == DecimalField.class){//Caja de texto
					
					int dec = ((DecimalField)caja.get(ui)).getDecimales();
					String num = new BigDecimal(campo.get(entity).toString()).setScale(dec,BigDecimal.ROUND_HALF_UP).toString();
					((DecimalField)caja.get(ui)).setValue(num);
					((DecimalField)caja.get(ui)).callback();
					
				}
				else if(caja.getType() == DateTimeField.class){//Caja de texto
					if (((DateTimeField)caja.get(ui)).isComodin()) {
						continue;
					}
					String formato = ((DateTimeField)caja.get(ui)).getFormato();
					if (formato == null) {
						continue;
					}
					if (campo.get(entity) == null) {
						((DateTimeField)caja.get(ui)).setValue(null);
						continue;
					}
					DateFormat formatter = new SimpleDateFormat(formato);
					String _res = campo.get(entity).toString();
					if("99".equals(_res) || "9999".equals(_res)) {
						if(((DateTimeField)caja.get(ui)).getType() == TIPO_DIALOGO.FECHA)
							((DateTimeField)caja.get(ui)).setValue(Util.getFecha(9999, 99, 99));
						else if(((DateTimeField)caja.get(ui)).getType() == TIPO_DIALOGO.HORA)
							((DateTimeField)caja.get(ui)).setValue(Util.getHora(99, 99, 99));
					}
					else {
						Date value = formatter.parse(campo.get(entity).toString());
						((DateTimeField)caja.get(ui)).setValue(value);
					}
//					Date value = formatter.parse(campo.get(entity).toString());
//					((DateTimeField)caja.get(ui)).setValue(value);
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					((CheckBoxField)caja.get(ui)).setCheckedTag(campo.get(entity));
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupField)caja.get(ui)).setTagSelected(campo.get(entity));
				}
				else if(caja.getType() == RadioGroupOtherField.class){//radiogroup
					//Log.d(TAG, "campocaja = " + caja.getName()+"="+campo.get(entity).toString());
					((RadioGroupOtherField)caja.get(ui)).setTagSelected(campo.get(entity));
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					((SpinnerField)caja.get(ui)).setSelectionKey(campo.get(entity));
				}
				else if(caja.getType() == DateTimeField.class){//Caja de texto
					((DateTimeField)caja.get(ui)).setValue(campo.get(entity)==null?"":campo.get(entity).toString());
				} 
				if (caja.getType() == TextBoxField.class) {
					((TextBoxField)caja.get(ui)).callback();	
				}
				else if(caja.getType() == ImageViewField.class){//spinner
					ImageViewField im = (ImageViewField)caja.get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						byte[] imageByte = campo.get(entity) == null ? null : (byte[])campo.get(entity);
						im.setValue(Util.getImage(imageByte));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File file = new File(im.getPathSave() + File.separator + im.getFileName());
						if (file.exists()) {
							FileInputStream in;
							BufferedInputStream buf;
							in = new FileInputStream(im.getPathSave() + File.separator + im.getFileName());
							buf = new BufferedInputStream(in);
							byte[] bMapArray = new byte[buf.available()];
							buf.read(bMapArray);
							Bitmap bitmap = BitmapFactory.decodeByteArray(bMapArray, 0,
									bMapArray.length);
							im.setValue(bitmap);
							buf.close();
							in.close();		
						} else {
							im.setValue(null);
						}
					}
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, e.getMessage());
			} catch (NoSuchFieldException e) {
				if(caja.getType() == DateTimeField.class)
					setDateTimeFields(cajas, caja, entity, ui);
				Log.d(TAG, e.getMessage());
			} catch (NullPointerException e) {
				Log.e(TAG, "NullPointer al leer el campo [" + nombreCampo +"]");
			} catch (ParseException e) {
				Log.e(TAG, e.getMessage(), e);
			} catch (IOException e) {
				Log.e(TAG, e.getMessage(), e);
			} 
			caja = null;
		}
		cajas = null;
		esCargaInicial = false;
	}
	
	private void setDateTimeFields(Field[] cajas, Field caja, Object entity, Object ui){
		
		try {
			DateTimeField dtf = ((DateTimeField)caja.get(ui));
			if(dtf.isdateOrhour()){
				EditText[] edt = dtf.getDateEditText();
				String formato = dtf.getFormato();
				String day="00", month="00", year="0000";
				for(Field c : cajas){
					if(c.getType() == IntegerField.class){
						for(int x=0;x<edt.length;x++){
							if(c.get(ui).equals(edt[x])){
								String nCampo = c.getName().substring(3).toLowerCase();
								Field cpo = entity.getClass().getDeclaredField(nCampo);
								if(cpo.get(entity) == null) {
									dtf.setValue(dtf.getValueDefault(null));
									return;
								}
								switch (x) {
									case 0: day = cpo.get(entity).toString(); break;
									case 1: month = cpo.get(entity).toString(); break;
									case 2: year = cpo.get(entity).toString(); break;
									default: break;
								}
								break;
							}
						}
					}
				}
				
				if(dtf.getType() == TIPO_DIALOGO.FECHA) {
					String fecha = Util.getFechaFormateada(year,month,day, formato);
//					Log.e("fromato", "fromato: "+formato);
//					Log.e("fecha", "fecha: "+fecha);
					dtf.setValue(Util.getFechaFormateada(fecha, formato));
				} else {
					Log.e("fromato", "day: "+day);
					Log.e("fecha", "month: "+month);
					if(day.equals("99") && month.equals("99")) dtf.setValue(Util.getHora(99, 99, 99));
					else dtf.setValue(Util.getHora(day, month, "00"));
				}

			}
		} catch (IllegalArgumentException e) {
			Log.e("IllegalArgumentException", "IllegalArgumentException: "+e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e("IllegalAccessException", "IllegalAccessException: "+e.getMessage());
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			Log.e("NoSuchFieldException", "NoSuchFieldException: "+e.getMessage());
			e.printStackTrace();
		} 
	}
	
	protected Object uiToEntity(Object entity){
		return uiToEntity(this, entity);
	}
	
	/**
	 * Obtiene los view de la actividad y coloca sus valores en la entidad
	 * @param entity
	 * @return
	 */
	public Object uiToEntity(Object ui, Object entity){
		if (entity == null) return null;
		Field[] cajas = ui.getClass().getDeclaredFields();
		String nombreComponente = "No definido";
		checkBoxGroups = null;
		for(Field caja : cajas){
			try {
				String nombreCampo = "";
				if(caja.getName().toString().length()<3) continue;
				if(!caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("") &&
						!caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) continue;
				
				if(caja.getName().substring(0,3).replaceAll("txt|spn|chb|img", "").equals("")) nombreCampo = caja.getName().substring(3).toLowerCase();
				else if(caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) nombreCampo = caja.getName().substring(2).toLowerCase();
			
			
				if(entity.getClass().getDeclaredField(nombreCampo) == null) continue;
				
				Field campo = entity.getClass().getDeclaredField(nombreCampo);
				if (caja.getType() == TextBoxField.class) {
					((TextBoxField)caja.get(ui)).callback();	
				}
				if(caja.getType() == TextAreaField.class){//Caja de texto
					TextAreaField editText = (TextAreaField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
				} else if(caja.getType() == TextField.class){//Caja de texto
					TextField editText = (TextField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
				} else if(caja.getType() == IntegerField.class){//Caja de texto
					IntegerField editText = (IntegerField)caja.get(ui);
					if(campo.getType() == Integer.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : Integer.valueOf(editText.getText().toString()));
					} else if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
				} else if(caja.getType() == DecimalField.class){//Caja de texto
					DecimalField editText = (DecimalField)caja.get(ui);
					if(campo.getType() == BigDecimal.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new BigDecimal(editText.getText().toString()));
					} else if(campo.getType() == Float.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : Float.parseFloat(editText.getText().toString()));
					}
				} else if(caja.getType() == TextBoxField.class){//Caja de texto
					TextBoxField editText = (TextBoxField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
				} else if(caja.getType() == EditText.class){//Caja de texto
					EditText editText = (EditText)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : Integer.parseInt(editText.getText().toString()));
					}
					else if(campo.getType() == BigDecimal.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new BigDecimal(editText.getText().toString()));
					}
				} else if(caja.getType() == DateTimeField.class){//DateTimeField 
					DateTimeField editText = (DateTimeField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : new String(editText.getText().toString()));
					}
				} else if(caja.getType() == DateTimeField.class){//Caja de texto
					DateTimeField editText = (DateTimeField)caja.get(ui);
					if (campo.getType() == Integer.class) {
						campo.set(entity, editText.getText().toString().equals("") ? null : Integer.valueOf(editText.getStringValue()));
					}
					if (campo.getType() == String.class) {
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getStringValue());
					}
				} else if(caja.getType() == CheckBoxField.class){//checkbox 
					//campo.set(entity, ((CenacomCheckBox)caja.get(this)).getCheckedTag());
					CheckBoxField checkbox = (CheckBoxField)caja.get(ui);
					if (checkbox.getGroup() != null) {
						agregarCheckBoxFieldHijo(checkbox);
					} else {
						if (campo.getType() == String.class){
							campo.set(entity, checkbox.getCheckedTag()==null ? null : checkbox.getCheckedTag().toString());
						} else if(campo.getType() == Integer.class){
							campo.set(entity, checkbox.getCheckedTag()==null ? null : Integer.parseInt(checkbox.getCheckedTag().toString()));
						}
					}
				} else if(caja.getType() == RadioGroupField.class){//radiogroup 
					RadioGroupField radiogroup = (RadioGroupField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected().toString());
					} else if(campo.getType() == Integer.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : Integer.parseInt(radiogroup.getTagSelected().toString()));
					}
				} else if(caja.getType() == RadioGroupOtherField.class) {//radiogroup 
					RadioGroupOtherField radiogroup = (RadioGroupOtherField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected().toString());
					} else if(campo.getType() == Integer.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : Integer.parseInt(radiogroup.getTagSelected().toString()));
					}
				} else if(caja.getType() == SpinnerField.class){//spinner 
					SpinnerField spinner = (SpinnerField)caja.get(ui);
					if(campo.getType() == String.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : spinner.getSelectedItemKey().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : Integer.parseInt(spinner.getSelectedItemKey().toString()));
					}
				} else if(caja.getType() == ImageViewField.class){//ImageViewField 
					ImageViewField im = (ImageViewField)caja.get(ui);
					if (im.getPlaceToSave() == ImageViewField.BASE_DATOS) {
						campo.set(entity, Util.getBytes((Bitmap)im.getValue()));
					} else if (im.getPlaceToSave() == ImageViewField.ARCHIVO) {
						File carpeta = new File(im.getPathSave());
						if (!carpeta.exists()) {
							carpeta.mkdir();
						}
						Bitmap bitmap = (Bitmap) im.getValue();
						if (bitmap != null) {
							String fileName = im.getPathSave() + File.separator + im.getFileName();
							Util.savePicture(bitmap, fileName);	
						}
					}
				}
				caja = null;
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			} catch (NullPointerException e) {
				Log.d(TAG + " - NullPointerException", "El objeto " + nombreComponente + " es nulo");
			} catch (IOException e) {
				Log.d(TAG + " - IOException", e.getMessage());
			}
		}
		try{
			if (checkBoxGroups != null) {
	//				Log.e(TAG, "Existen checkBoxGroups");
				for (Map.Entry<String, List<CheckBoxField>> entry : checkBoxGroups.entrySet()) {
	//					Log.e(TAG, "Primer Grupo de Check: " + entry.getKey());
					boolean todoDesmarcado = true;
					List<CheckBoxField> group = checkBoxGroups.get(entry.getKey());
					for (CheckBoxField cb : group) {
						if (cb.isChecked()) {
							todoDesmarcado = false;
	//							Log.e(TAG, "En Grupo de Check: " + entry.getKey() + " existe uno marcado");
							break;
						}
					}
					if (todoDesmarcado) {
	//						Log.e(TAG, "Colocando null al Grupo de Check: " + entry.getKey());
						for (CheckBoxField cb : group) {
							Field campo = entity.getClass().getDeclaredField(cb.getFieldName().toLowerCase());
							campo.set(entity, null);
						}
					} else {
	//						Log.e(TAG, "Colocando valor a cada CheckBox del grupo: " + entry.getKey());
						for (CheckBoxField cb : group) {
							Field campo = entity.getClass().getDeclaredField(cb.getFieldName().toLowerCase());
							if (campo.getType() == String.class){
								campo.set(entity, cb.getCheckedTag()==null ? null : cb.getCheckedTag().toString());
							} else if(campo.getType() == Integer.class){
								campo.set(entity, cb.getCheckedTag()==null ? null : Integer.parseInt(cb.getCheckedTag().toString()));
							}
						}						
					}
				}
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG + " - IllegalArgumentException", e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG + " - IllegalAccessException", e.getMessage());
		} catch (NoSuchFieldException e) {
			Log.d(TAG + " - NoSuchFieldException", e.getMessage());
		} catch (NullPointerException e) {
			Log.d(TAG + " - NullPointerException", "El objeto " + nombreComponente + " es nulo");
		}
		cajas = null;
		return entity;
	}
	
	private void agregarCheckBoxFieldHijo(CheckBoxField checkBox) {
		String key = checkBox.getGroup();
		if (checkBoxGroups == null) {
			checkBoxGroups = new HashMap<String, List<CheckBoxField>>();
		}
		if (checkBoxGroups.containsKey(key)) {
			List<CheckBoxField> group = checkBoxGroups.get(key);
			if (!group.contains(checkBox)) {
				group.add(checkBox);
			}
		} else {
			List<CheckBoxField> group = new ArrayList<CheckBoxField>();
			group.add(checkBox);
			checkBoxGroups.put(key, group);
		}
	}
	
	protected Object uiToEntity2(Object entity){
		if (entity == null) return null;
		
		Field[] cajas = this.getClass().getDeclaredFields();
		for(Field caja : cajas){
			String nombreCampo = "";
			if(!caja.getName().substring(0,3).replaceAll("txt|spn|chb", "").equals("") &&
					!caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) continue;
			
			if(caja.getName().substring(0,3).replaceAll("txt|spn|chb", "").equals("")) nombreCampo = caja.getName().substring(3).toLowerCase();
			else if(caja.getName().substring(0,2).replaceAll("rg|rb", "").equals("")) nombreCampo = caja.getName().substring(2).toLowerCase();
			
			try {
				if(entity.getClass().getDeclaredField(nombreCampo) == null) continue;
				
				Field campo = entity.getClass().getDeclaredField(nombreCampo);
				if(caja.getType() == EditText.class){//Caja de texto
					EditText editText = (EditText)caja.get(this);
					if(campo.getType() == String.class){
						campo.set(entity, editText.getText().toString().equals("") ? null : editText.getText().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, editText.getText().toString().equals("") ? Integer.valueOf(0) : Integer.valueOf(editText.getText().toString()));
					}
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox 
					//campo.set(entity, ((CenacomCheckBox)caja.get(this)).getCheckedTag());
					CheckBoxField checkbox = (CheckBoxField)caja.get(this);
					if(campo.getType() == String.class){
						campo.set(entity, checkbox.getCheckedTag()==null ? null : checkbox.getCheckedTag().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, checkbox.getCheckedTag()==null ? null : Integer.parseInt(checkbox.getCheckedTag().toString()));
					}
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup 
					//campo.set(entity, ((CenacomRadioGroup)caja.get(this)).getTagSelected());
					RadioGroupField radiogroup = (RadioGroupField)caja.get(this);
					if(campo.getType() == String.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : radiogroup.getTagSelected().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, radiogroup.getTagSelected()==null ? null : Integer.parseInt(radiogroup.getTagSelected().toString()));
					}
				}
				else if(caja.getType() == SpinnerField.class){//spinner 
					SpinnerField spinner = (SpinnerField)caja.get(this);
					if(campo.getType() == String.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : spinner.getSelectedItemKey().toString());
					}
					else if(campo.getType() == Integer.class){
						campo.set(entity, spinner.getSelectedItemKey()==null ? null : Integer.parseInt(spinner.getSelectedItemKey().toString()));
					}
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG, e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG, e.getMessage());
			} catch (NullPointerException e) {
				Log.d(TAG, "El objeto " + caja.getName() + " es nulo");
			}
			caja = null;
		}
		cajas = null;
		return entity;
	}
	
	protected void initObjects(){
		initObjects(this, this.getView());
	}
	
	protected <T>void initObjects(T container){
		initObjects(container, this.getView());
	}
	
	/**
	 * Inicializa todas las view de la actividad
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public <T>void initObjects(T container, View viewContainer){
		
		Field[] cajas = container.getClass().getDeclaredFields();
		FieldAnnotation aFAnnotation = null;
		fieldsOrdered = new View[cajas.length-1];
		callbackEvents = new HashMap<View, HashMap<Integer, Object[]>>();
		if(containerFieldsOrdered == null) containerFieldsOrdered = new HashMap<Object, View[]>();
		fieldsPosition = new HashMap<Object, Integer>();
		if(containerFieldsPosition == null) containerFieldsPosition = new HashMap<Object, HashMap<Object, Integer>>();
		if(containerNumFields == null) containerNumFields = new HashMap<Object, Integer>();
		if(containerbackgroundDrawableUnfocus == null) containerbackgroundDrawableUnfocus = new HashMap<Object, Drawable>();
		if(containerCallbackEvents == null) containerCallbackEvents = new HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>>();
		this.viewContainer = viewContainer;
		setContainerContext(container);
		
		//Log.d(TAG, "++"+container.getClass().isMemberClass() +"++");
		//Log.d(TAG, "++"+container.getClass().getDeclaringClass() +"++");
		for(Field caja : cajas){
			try {
				aFAnnotation = caja.getAnnotation(FieldAnnotation.class);
				String prefix = "txt";
//				Log.e(TAG, "---"+caja.getName()+"---");
				if(caja.getType() == EditText.class  ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (EditText)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == TextAreaField.class  ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (TextAreaField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == DecimalField.class  ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (DecimalField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == IntegerField.class  ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (DecimalField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				}
				else if(caja.getType() == TextField.class ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (TextField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				}
				//Para label aun no he implementado funcionalidad
				else if(caja.getType() == TextView.class){//label
					caja.set(container, (TextView)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "";
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					caja.set(container, (CheckBoxField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "chb";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (CheckBoxField)caja.get(container);
					((CheckBoxField)caja.get(container)).setFocusable(true);
					((CheckBoxField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					caja.set(container, (RadioGroupField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupField)caja.get(container);
					((RadioGroupField)caja.get(container)).setFocusable(true);
					((RadioGroupField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					caja.set(container, (SpinnerField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (SpinnerField)caja.get(container);
					((SpinnerField)caja.get(container)).setFocusable(true);
					((SpinnerField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == Spinner.class){//spinner
					caja.set(container, (Spinner)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Spinner)caja.get(container);
				}
				else if(caja.getType() == Button.class){
					caja.set(container, (Button)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Button)caja.get(container);
					//continue;
				}
				else if(caja.getType() == LinearLayout.class){//label
					caja.set(container, (LinearLayout)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "";
				}
				
				if(aFAnnotation != null){
					fieldsPosition.put(caja.get(container), aFAnnotation.orderIndex());
					numFields = Math.max(numFields, aFAnnotation.orderIndex());
				}
				//if(aFAnnotation != null) numFields = Math.max(numFields, aFAnnotation.orderIndex());
				
				//((View)caja.get(this)).setFocusable(true);
				//((View)caja.get(this)).setFocusableInTouchMode(true);
				
				//onFocus
				if(caja.getType() != Button.class && caja.getType() != ButtonComponent.class) setHover((View)caja.get(container), prefix);
				
			} catch (IllegalArgumentException e) {
				Log.d(TAG + "**IllegalArgumentException**"+caja.getName(), e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + "**IllegalAccessException "+caja.getName(), e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + "**NoSuchFieldException", e.getMessage());
			} catch (ClassCastException e){
				Log.d(TAG + "**ClassCastException", caja.getName());
			} catch (NullPointerException e){
				Log.d(TAG + "**NullPointerException", caja.getName() + "--" + caja.getModifiers());
			}
			caja = null;
		}
		cajas = null;
		fieldsOrdered = Arrays.copyOf(fieldsOrdered, numFields+1);
		containerFieldsOrdered.put(container, fieldsOrdered);
		containerFieldsPosition.put(container, fieldsPosition);
		containerNumFields.put(container, numFields);
		
		/*En el proyecto de hogares rural se necesita tener el nombre del persona de la 
		 * encuesta en la parte superior de algunas paginas. Por defecto voy a realizar la
		 * asignación del nombre completo*/
		cargarNombrePersona();
	}
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public <T>void initObjectsWithoutXML(T container, View viewContainer){
		
		Field[] cajas = container.getClass().getDeclaredFields();
		FieldAnnotation aFAnnotation = null;
		List<View> views = new ArrayList<View>();
		fieldsOrdered = new View[cajas.length-1];
		callbackEvents = new HashMap<View, HashMap<Integer, Object[]>>();
		if(containerFieldsOrdered == null) containerFieldsOrdered = new HashMap<Object, View[]>();
		fieldsPosition = new HashMap<Object, Integer>();
		if(containerFieldsPosition == null) containerFieldsPosition = new HashMap<Object, HashMap<Object, Integer>>();
		if(containerNumFields == null) containerNumFields = new HashMap<Object, Integer>();
		if(containerbackgroundDrawableUnfocus == null) containerbackgroundDrawableUnfocus = new HashMap<Object, Drawable>();
		if(containerCallbackEvents == null) containerCallbackEvents = new HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>>();
		this.viewContainer = viewContainer;
		setContainerContext(container);
		for (int i = 0; i < cajas.length; i++) {
			String cajaNombre = "";
			try {
				cajaNombre = cajas[i].getName(); 
				aFAnnotation = cajas[i].getAnnotation(FieldAnnotation.class);
				if(cajas[i].getType() == EditText.class || cajas[i].getType() == TextAreaField.class
						|| cajas[i].getType() == DecimalField.class || cajas[i].getType() == IntegerField.class
						|| cajas[i].getType() == TextField.class || cajas[i].getType() == CheckBoxField.class
						|| cajas[i].getType() == RadioGroupOtherField.class || cajas[i].getType() == RadioGroupField.class
						|| cajas[i].getType() == SpinnerField.class || cajas[i].getType() == ButtonComponent.class
						|| cajas[i].getType() == Button.class || cajas[i].getType() == DateTimeField.class
						|| cajas[i].getType() == ImageViewField.class){//Caja de texto
					if(aFAnnotation != null) {
						views.add((View)cajas[i].get(container));
					}
				}
			} catch (IllegalArgumentException e) {
				Log.e(TAG + "**IllegalArgumentException**"+cajaNombre, e.getMessage());
			} catch (IllegalAccessException e) {
				Log.e(TAG + "**IllegalAccessException "+cajaNombre, e.getMessage());
			} catch (ClassCastException e){
				Log.e(TAG + "**ClassCastException", cajaNombre);
			} catch (NullPointerException e){
				Log.e(TAG + "**NullPointerException", cajaNombre);
			}
		}
		fieldsOrdered = new View[views.size()+1];
		for(Field caja : cajas){
			try {
				aFAnnotation = caja.getAnnotation(FieldAnnotation.class);
				String prefix = "txt";
				if(caja.getType() == EditText.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == TextAreaField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == DecimalField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == IntegerField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				} 
				else if(caja.getType() == TextField.class  ){//Caja de texto
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				}
				//Para label aun no he implementado funcionalidad
				else if(caja.getType() == TextView.class){//label
					prefix = "";
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					prefix = "chb";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (CheckBoxField)caja.get(container);
					((CheckBoxField)caja.get(container)).setFocusable(true);
					((CheckBoxField)caja.get(container)).setFocusableInTouchMode(true);
					if (((CheckBoxField)caja.get(container)).getFieldName() == null) {
						((CheckBoxField)caja.get(container)).setFieldName(caja.getName().substring(3, caja.getName().length()));
					}
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupField)caja.get(container);
					((RadioGroupField)caja.get(container)).setFocusable(true);
					((RadioGroupField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == RadioGroupOtherField.class){//radiogroup
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupOtherField)caja.get(container);
					((RadioGroupOtherField)caja.get(container)).setFocusable(true);
					((RadioGroupOtherField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (SpinnerField)caja.get(container);
					((SpinnerField)caja.get(container)).setFocusable(true);
					((SpinnerField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == DateTimeField.class){//DateTimeField
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (DateTimeField)caja.get(container);
				}
				else if(caja.getType() == Spinner.class){//spinner
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Spinner)caja.get(container);
				}
				else if(caja.getType() == ButtonComponent.class){
					prefix = "";
					if(aFAnnotation != null) {
						fieldsOrdered[aFAnnotation.orderIndex()] = (ButtonComponent)caja.get(container);
					}
					//continue;
				}
				else if(caja.getType() == Button.class){
					prefix = "";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Button)caja.get(container);
					//continue;
				}
				else if(caja.getType() == LinearLayout.class){//label
					prefix = "";
				}
				else if(caja.getType() == ListView.class){//label
					prefix = "";
				}
				else {
					prefix="";
				}
				if(aFAnnotation != null){
					fieldsPosition.put(caja.get(container), aFAnnotation.orderIndex());
					numFields = Math.max(numFields, aFAnnotation.orderIndex());
				}
				//if(aFAnnotation != null) numFields = Math.max(numFields, aFAnnotation.orderIndex());
				
				//((View)caja.get(this)).setFocusable(true);
				//((View)caja.get(this)).setFocusableInTouchMode(true);
				
				//onFocus
				if(caja.getType() != Button.class && caja.getType() != ButtonComponent.class) {					
					setHover((View)caja.get(container), prefix);
				}
				
			} catch (IllegalArgumentException e) {
				Log.d(TAG + "**IllegalArgumentException**"+caja.getName(), e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + "**IllegalAccessException "+caja.getName(), e.getMessage());
			} catch (ClassCastException e){
				Log.d(TAG + "**ClassCastException", caja.getName());
			} catch (NullPointerException e){
				Log.d(TAG + "**NullPointerException", caja.getName() + "--" + caja.getModifiers());
			}
			caja = null;
		}
		/*int contador = 0;
		for(View v:fieldsOrdered){
			Log.e("fieldsOrdered["+contador+"}", String.valueOf(v));
			contador++;
		}*/
		cajas = null;
//		fieldsOrdered = Arrays.copyOf(fieldsOrdered, numFields+1);
		containerFieldsOrdered.put(container, fieldsOrdered);
		containerFieldsPosition.put(container, fieldsPosition);
		containerNumFields.put(container, numFields);
		
		/*En el proyecto de hogares rural se necesita tener el nombre del persona de la 
		 * encuesta en la parte superior de algunas paginas. Por defecto voy a realizar la
		 * asignación del nombre completo*/
//		cargarNombrePersona();
	}
	
	protected void cargarNombrePersona() {
//		if(viewContainer != null){
//			tv = (TextView)viewContainer.findViewById(R.id.txtPersona);
//			if(tv != null){
//				tv.setText(Globales.nombrecompleto==null?"Persona: No definida":("Persona: " + Globales.nombrecompleto));
//			}
//		}
	}

	public void FocuseaObjects(Object control) {
		if (control == null)
			return;
		try {

			if (control instanceof EditText) {// Caja de texto

				EditText vEditText;
				vEditText = (EditText) control;
				vEditText.setFocusable(true);
				vEditText.setFocusableInTouchMode(true);
				vEditText.requestFocus();
				vEditText.setBackgroundResource(R.color.verdeagua);

			} else if (control instanceof CheckBoxField) {// checkbox

				CheckBoxField vRuralCheckBox;
				vRuralCheckBox = (CheckBoxField) control;
				vRuralCheckBox.setFocusable(true);
				vRuralCheckBox.setFocusableInTouchMode(true);
				vRuralCheckBox.requestFocus();
				vRuralCheckBox.setBackgroundResource(R.color.verdeagua);

			} else if (control instanceof RadioGroupField) {// radiogroup
				RadioGroupField vRuralRadioGroup;
				vRuralRadioGroup = (RadioGroupField) control;
				vRuralRadioGroup.setFocusable(true);
				vRuralRadioGroup.setFocusableInTouchMode(true);
				vRuralRadioGroup.requestFocus();
				vRuralRadioGroup.setBackgroundResource(R.color.verdeagua);

			} else if (control instanceof SpinnerField) {// spinner
				SpinnerField vRuralSpinner;
				vRuralSpinner = (SpinnerField) control;
				vRuralSpinner.setFocusable(true);
				vRuralSpinner.setFocusableInTouchMode(true);
				vRuralSpinner.requestFocus();
				vRuralSpinner.setBackgroundResource(R.color.verdeagua);

			} else if (control instanceof AbsoluteLayout) {// spinner
				AbsoluteLayout layout;
				layout = (AbsoluteLayout) control;
				layout.setFocusable(true);
				layout.setFocusableInTouchMode(true);
				layout.requestFocus();
				layout.setBackgroundResource(R.color.verdeagua);

			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} 
	}
	
	protected <T>void initObjects(T container, Dialog viewContainer){
		//Log.d(TAG, "---" + viewContainer.toString() + "---");
		Field[] cajas = container.getClass().getDeclaredFields();
		FieldAnnotation aFAnnotation = null;
		fieldsOrdered = new View[cajas.length-1];
		callbackEvents = new HashMap<View, HashMap<Integer, Object[]>>();
		if(containerFieldsOrdered == null) containerFieldsOrdered = new HashMap<Object, View[]>();
		fieldsPosition = new HashMap<Object, Integer>();
		if(containerFieldsPosition == null) containerFieldsPosition = new HashMap<Object, HashMap<Object, Integer>>();
		if(containerNumFields == null) containerNumFields = new HashMap<Object, Integer>();
		if(containerbackgroundDrawableUnfocus == null) containerbackgroundDrawableUnfocus = new HashMap<Object, Drawable>();
		if(containerCallbackEvents == null) containerCallbackEvents = new HashMap<Object, HashMap<View, HashMap<Integer, Object[]>>>();
		this.viewContainer = viewContainer.getWindow().getDecorView();
		setContainerContext(container);
		
		for(Field caja : cajas){
			try {
				aFAnnotation = caja.getAnnotation(FieldAnnotation.class);
				String prefix = "txt";
				//Log.d(TAG, "---"+caja.getName()+"---");
				if(caja.getType() == EditText.class  ){//Caja de texto
					//txtINF101_1 = (EditText)findViewById(R.id.txtINF101_1);
					caja.set(container, (EditText)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "txt";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (EditText)caja.get(container);
				}
				//Para label aun no he implementado funcionalidad
				else if(caja.getType() == TextView.class){//label
					caja.set(container, (TextView)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "";
				}
				else if(caja.getType() == CheckBoxField.class){//checkbox
					caja.set(container, (CheckBoxField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "chb";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (CheckBoxField)caja.get(container);
					((CheckBoxField)caja.get(container)).setFocusable(true);
					((CheckBoxField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == RadioGroupField.class){//radiogroup
					caja.set(container, (RadioGroupField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "rg";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (RadioGroupField)caja.get(container);
					((RadioGroupField)caja.get(container)).setFocusable(true);
					((RadioGroupField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == SpinnerField.class){//spinner
					caja.set(container, (SpinnerField)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (SpinnerField)caja.get(container);
					((SpinnerField)caja.get(container)).setFocusable(true);
					((SpinnerField)caja.get(container)).setFocusableInTouchMode(true);
				}
				else if(caja.getType() == Spinner.class){//spinner
					caja.set(container, (Spinner)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "spn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Spinner)caja.get(container);
				}
				else if(caja.getType() == Button.class){
					caja.set(container, (Button)viewContainer.findViewById(R.id.class.getDeclaredField(caja.getName()).getInt(R.id.class)));
					prefix = "btn";
					if(aFAnnotation != null) fieldsOrdered[aFAnnotation.orderIndex()] = (Button)caja.get(container);
					//continue;
				}
				
				if(aFAnnotation != null){
					fieldsPosition.put(caja.get(container), aFAnnotation.orderIndex());
					numFields = Math.max(numFields, aFAnnotation.orderIndex());
				}
				//if(aFAnnotation != null) fieldsPosition.put(caja.get(container), aFAnnotation.orderIndex());
				//if(aFAnnotation != null) numFields = Math.max(numFields, aFAnnotation.orderIndex());
				
				//((View)caja.get(this)).setFocusable(true);
				//((View)caja.get(this)).setFocusableInTouchMode(true);
				
				//onFocus
				if(caja.getType() != Button.class) setHover((View)caja.get(container), prefix);
				
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			} catch (ClassCastException e){
				Log.d(TAG + " - ClassCastException", caja.getName());
			}
		}
		containerFieldsOrdered.put(container, fieldsOrdered);
		containerFieldsPosition.put(container, fieldsPosition);
		containerNumFields.put(container, numFields);
	}
	
	protected <T>void initHover(T container){
		Field[] cajas = container.getClass().getDeclaredFields();
        String prefix = "";
        for(Field caja : cajas){
        	try {
        		if(caja.getType() == EditText.class || caja.getType() == DecimalField.class
        				 || caja.getType() == IntegerField.class  || caja.getType() == TextField.class
        				 || caja.getType() == TextAreaField.class) prefix = "txt";
                else if(caja.getType() == CheckBoxField.class) prefix = "chb";
                else if(caja.getType() == RadioGroupField.class) prefix = "rg";
                else if(caja.getType() == RadioGroupOtherField.class) prefix = "rg";
                else if(caja.getType() == SpinnerField.class) prefix = "spn";
                else if(caja.getType() == Spinner.class) prefix = "spn";
                      
                setHover((View)caja.get(container), prefix);
                      
        	} catch (IllegalArgumentException e) {
        		Log.d(TAG, e.getMessage());
        	} catch (IllegalAccessException e) {
                Log.d(TAG, e.getMessage());
            } catch (ClassCastException e){
            	Log.d(TAG, caja.getName());
            }
        }
  }

	protected void enlazarCajas(){
		enlazarCajas(this);
	}
	
	public <T>void enlazarCajas(T container){
		this.fieldsOrdered = containerFieldsOrdered.get(container);
		
		for(int i=0; i<=fieldsOrdered.length-1; i++){
			
			View from = fieldsOrdered[i];
			 
			if(from == null) continue;
			
			View to = null;
			if (i + 1 <= fieldsOrdered.length - 1) {
				to = fieldsOrdered[i+1];
			}
			boolean nuevaImplementacion = false;
			if (from instanceof DateTimeField) {
				nuevaImplementacion = true;
				jumpFromDateTimeField(i, (DateTimeField)from, to, null, (DateTimeField)from, null, null, null);
			} else if (from instanceof TextBoxField) {
				nuevaImplementacion = true;
				jumpFromTextBoxField(i, (TextBoxField)from, to, null, (TextBoxField)from, null, null, null);
			} else if (from instanceof CheckBoxField) {
				nuevaImplementacion = true;
				jumpFromCheckBoxField(i, (CheckBoxField)from, to, null, (CheckBoxField)from, null, null, null);
			} else if (from instanceof RadioGroupOtherField) {
				nuevaImplementacion = true;
				jumpFromRadioGroupField(i, (RadioGroupOtherField)from, to, null, (RadioGroupOtherField)from, null, null, null);
			} else if (from instanceof SpinnerField) {
				nuevaImplementacion = true;
				jumpFromSpinnerField(i, (SpinnerField)from, to, null, (SpinnerField)from, null, null, null);
			}
			
			if (!nuevaImplementacion) {
				if(to == null) continue;	
				if(i==fieldsOrdered.length-1){
					if(fieldsOrdered[i].getClass() == EditText.class){//RuralRadioGroup-->View
						jumpEditText2View((EditText)from, viewContainer, null, (EditText)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == DecimalField.class){//RuralSpinner-->View
						jumpEditText2View((EditText)from, viewContainer, null, (EditText)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == IntegerField.class){//RuralSpinner-->View
						jumpEditText2View((EditText)from, viewContainer, null, (EditText)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == TextField.class){//RuralSpinner-->View
						jumpEditText2View((EditText)from, viewContainer, null, (EditText)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == TextAreaField.class){//RuralSpinner-->View
						jumpEditText2View((EditText)from, viewContainer, null, (EditText)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == SpinnerField.class){//RuralSpinner-->View
						jumpCenacomSpinner2View((SpinnerField)from, viewContainer, null, (SpinnerField)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == RadioGroupField.class){//RuralRadioGroup-->View
						jumpCenacomRadioGroup2View((RadioGroupField)from, viewContainer, null, (RadioGroupField)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class){//RuralRadioGroup-->View
						jumpCenacomRadioGroup2View((RadioGroupOtherField)from, viewContainer, null, (RadioGroupOtherField)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == CheckBoxField.class){//RuralCheckBox-->View
						jumpCenacomCheckBox2View((CheckBoxField)from, viewContainer, null, (CheckBoxField)from, null, null, null);
					}
					else if(fieldsOrdered[i].getClass() == ButtonComponent.class){//ButtonComponent-->View
						jumpEditButton2View((ButtonComponent)from, viewContainer, null, (ButtonComponent)from, null, null, null);
					}
					continue;
				}			
				if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == EditText.class){//EditText-->EditText
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == TextField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//EditText-->CenacomSpinner
					jumpEditText2CenacomSpinner((EditText)from, (SpinnerField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioGroup((EditText)from, (RadioGroupField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioOtherGroup((EditText)from, (RadioGroupOtherField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//EditText-->CenacomCheckBox
					jumpEditText2CenacomCheckBox((EditText)from, (CheckBoxField)to, null, (EditText)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == EditText.class && fieldsOrdered[i+1].getClass() == Button.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}	
				
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == TextField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == EditText.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioGroup((EditText)from, (RadioGroupField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioOtherGroup((EditText)from, (RadioGroupOtherField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//EditText-->CenacomCheckBox
					jumpEditText2CenacomCheckBox((EditText)from, (CheckBoxField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//EditText-->CenacomSpinner
					jumpEditText2CenacomSpinner((EditText)from, (SpinnerField)to, null, (EditText)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == IntegerField.class && fieldsOrdered[i+1].getClass() == Button.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}			
				
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == TextField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == EditText.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioGroup((EditText)from, (RadioGroupField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioOtherGroup((EditText)from, (RadioGroupOtherField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//EditText-->CenacomCheckBox
					jumpEditText2CenacomCheckBox((EditText)from, (CheckBoxField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//EditText-->CenacomSpinner
					jumpEditText2CenacomSpinner((EditText)from, (SpinnerField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == DecimalField.class && fieldsOrdered[i+1].getClass() == Button.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}					
				
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == TextField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == EditText.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioGroup((TextBoxField)from, (RadioGroupField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioOtherGroup((EditText)from, (RadioGroupOtherField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//EditText-->CenacomCheckBox
					jumpEditText2CenacomCheckBox((TextBoxField)from, (CheckBoxField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//EditText-->CenacomSpinner
					jumpEditText2CenacomSpinner((EditText)from, (SpinnerField)to, null, (EditText)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == TextField.class && fieldsOrdered[i+1].getClass() == Button.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}			
				
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == TextField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == EditText.class){//EditText-->CenacomSpinner
					jumpEditText2EditText((EditText)from, (EditText)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioGroup((EditText)from, (RadioGroupField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//EditText-->CenacomRadioGroup
					jumpEditText2CenacomRadioOtherGroup((EditText)from, (RadioGroupOtherField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//EditText-->CenacomCheckBox
					jumpEditText2CenacomCheckBox((EditText)from, (CheckBoxField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//EditText-->CenacomSpinner
					jumpEditText2CenacomSpinner((EditText)from, (SpinnerField)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == TextAreaField.class && fieldsOrdered[i+1].getClass() == Button.class){//EditText-->Button
					jumpEditText2Button((EditText)from, (Button)to, null, (EditText)from, null, null, null);
				}				
				
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == EditText.class){//CenacomSpinner-->EditText
					jumpCenacomSpinner2EditText((SpinnerField)from, (EditText)to, null, (SpinnerField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//CenacomSpinner-->EditText
					jumpCenacomSpinner2EditText((SpinnerField)from, (EditText)to, null, (SpinnerField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//CenacomSpinner-->EditText
					jumpCenacomSpinner2EditText((SpinnerField)from, (EditText)to, null, (SpinnerField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == TextField.class){//CenacomSpinner-->EditText
					jumpCenacomSpinner2EditText((SpinnerField)from, (EditText)to, null, (SpinnerField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//CenacomSpinner-->EditText
					jumpCenacomSpinner2EditText((SpinnerField)from, (EditText)to, null, (SpinnerField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//CenacomSpinner-->CenacomSpinner
					jumpCenacomSpinner2CenacomSpinner((SpinnerField)from, (SpinnerField)to, null, (SpinnerField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//CenacomSpinner-->CenacomRadioGroup
					jumpCenacomSpinner2CenacomRadioGroup((SpinnerField)from, (RadioGroupField)to, null, (SpinnerField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomSpinner-->CenacomRadioGroup
					jumpCenacomSpinner2CenacomRadioOtherGroup((SpinnerField)from, (RadioGroupOtherField)to, null, (SpinnerField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//CenacomSpinner-->CenacomCheckBox
					jumpCenacomSpinner2CenacomCheckBox((SpinnerField)from, (CheckBoxField)to, null, (SpinnerField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == Button.class){//CenacomSpinner-->Button
					jumpCenacomSpinner2Button((SpinnerField)from, (Button)to, null, (SpinnerField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == SpinnerField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//CenacomSpinner-->Button
					jumpCenacomSpinner2Button((SpinnerField)from, (Button)to, null, (SpinnerField)from, null, null, null);
				}
				
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == EditText.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupField)from, (EditText)to, null, (RadioGroupField)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupField)from, (EditText)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupField)from, (EditText)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == TextField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupField)from, (EditText)to, null, (RadioGroupField)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupField)from, (EditText)to, null, (RadioGroupField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//CenacomRadioGroup-->CenacomSpinner
					jumpCenacomRadioGroup2CenacomSpinner((RadioGroupField)from, (SpinnerField)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioGroup((RadioGroupField)from, (RadioGroupField)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioOtherGroup((RadioGroupField)from, (RadioGroupOtherField)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioOtherGroup((RadioGroupField)from, (RadioGroupOtherField)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//CenacomRadioGroup-->CenacomCheckBox
					jumpCenacomRadioGroup2CenacomCheckBox((RadioGroupField)from, (CheckBoxField)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == Button.class){//CenacomRadioGroup-->Button
					jumpCenacomRadioGroup2Button((RadioGroupField)from, (Button)to, null, (RadioGroupField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//CenacomRadioGroup-->Button
					jumpCenacomRadioGroup2Button((RadioGroupField)from, (Button)to, null, (RadioGroupField)from, null, null, null);
				}
				
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == EditText.class){//CenacomCheckBox-->EditText
					jumpCenacomCheckBox2EditText((CheckBoxField)from, (EditText)to, null, (CheckBoxField)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//CenacomCheckBox-->EditText
					jumpCenacomCheckBox2EditText((CheckBoxField)from, (EditText)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//CenacomCheckBox-->EditText
					jumpCenacomCheckBox2EditText((CheckBoxField)from, (EditText)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == TextField.class){//CenacomCheckBox-->EditText
					jumpCenacomCheckBox2EditText((CheckBoxField)from, (EditText)to, null, (CheckBoxField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//CenacomCheckBox-->EditText
					jumpCenacomCheckBox2EditText((CheckBoxField)from, (EditText)to, null, (CheckBoxField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//CenacomCheckBox-->CenacomSpinner
					jumpCenacomCheckBox2CenacomSpinner((CheckBoxField)from, (SpinnerField)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//CenacomCheckBox-->CenacomRadioGroup
					jumpCenacomCheckBox2CenacomRadioGroup((CheckBoxField)from, (RadioGroupField)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomCheckBox-->CenacomRadioGroup
					jumpCenacomCheckBox2CenacomRadioOtherGroup((CheckBoxField)from, (RadioGroupOtherField)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//CenacomCheckBox-->CenacomCheckBox
					jumpCenacomCheckBox2CenacomCheckBox((CheckBoxField)from, (CheckBoxField)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == Button.class){//CenacomCheckBox-->Button
					jumpCenacomCheckBox2Button((CheckBoxField)from, (Button)to, null, (CheckBoxField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == CheckBoxField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//CenacomCheckBox-->Button
					jumpCenacomCheckBox2Button((CheckBoxField)from, (Button)to, null, (CheckBoxField)from, null, null, null);
				}
				
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == EditText.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupOtherField)from, (EditText)to, null, (RadioGroupOtherField)from, null, null, null);
				}		
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == IntegerField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupOtherField)from, (EditText)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == DecimalField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupOtherField)from, (EditText)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == TextField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupOtherField)from, (EditText)to, null, (RadioGroupOtherField)from, null, null, null);
				}	
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == TextAreaField.class){//CenacomRadioGroup-->EditText
					jumpCenacomRadioGroup2EditText((RadioGroupOtherField)from, (EditText)to, null, (RadioGroupOtherField)from, null, null, null);
				}			
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == SpinnerField.class){//CenacomRadioGroup-->CenacomSpinner
					jumpCenacomRadioGroup2CenacomSpinner((RadioGroupOtherField)from, (SpinnerField)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == RadioGroupField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioGroup((RadioGroupOtherField)from, (RadioGroupField)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioOtherGroup((RadioGroupOtherField)from, (RadioGroupOtherField)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == RadioGroupOtherField.class){//CenacomRadioGroup-->CenacomRadioGroup
					jumpCenacomRadioGroup2CenacomRadioOtherGroup((RadioGroupOtherField)from, (RadioGroupOtherField)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == CheckBoxField.class){//CenacomRadioGroup-->CenacomCheckBox
					jumpCenacomRadioGroup2CenacomCheckBox((RadioGroupOtherField)from, (CheckBoxField)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == ButtonComponent.class){//CenacomRadioGroup-->Button
					jumpCenacomRadioGroup2Button((RadioGroupOtherField)from, (Button)to, null, (RadioGroupOtherField)from, null, null, null);
				}
				else if(fieldsOrdered[i].getClass() == RadioGroupOtherField.class && fieldsOrdered[i+1].getClass() == Button.class){//CenacomRadioGroup-->Button
					jumpCenacomRadioGroup2Button((RadioGroupOtherField)from, (Button)to, null, (RadioGroupOtherField)from, null, null, null);
				}
			}						
			from = to = null;
		}
		
		
	}
	
	public final void listening(){
		
		Iterator<View> viewsIter = callbackEvents.keySet().iterator();
		while(viewsIter.hasNext()){
			
			final View from = viewsIter.next();
			final HashMap<Integer, Object[]> call = callbackEvents.get(from);
			
	 		if(call == null) continue;
	 		
	 		from.post(new Runnable() {
				
				@Override
				public void run() {
					if (from.getClass() == DateTimeField.class) {
						((DateTimeField)from).setChangeValueListener(new DateTimeField.ChangeValueListener() {							
							@Override
							public void onChangeValue() {
							}
						});
					} else if(from.getClass() == EditText.class || from.getClass() == DecimalField.class || from.getClass() == IntegerField.class
							|| from.getClass() == TextField.class || from.getClass() == TextAreaField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		/*Log.e("entras11", String.valueOf(from));
							 		if(args[1]!=null && !((View)args[1]).isEnabled()&&KeyEvent.ACTION_UP==event.getAction()) {
							 			Log.e("entras444", String.valueOf(args[1]));
							 			//args[1]=from;
							 			return false;
							 		}*/
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}								
								return false;
							}
						});			 			
					} else if(from.getClass() == SpinnerField.class){		
			 			from.setOnKeyListener(new View.OnKeyListener() {							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});
			 			((SpinnerField)from).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			 				
							@Override
							public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
								if(!esCargaInicial)
									((SpinnerField)from).onItemSelectedListenerAlterno(arg0, arg1, arg2, arg3);
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onItemSelected(arg0, from, (View)args[1], (View)args[2], (View)args[3], arg2, arg3, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {}
						});
			 		} else if(from.getClass() == RadioGroupField.class){
			 			from.setOnKeyListener(new View.OnKeyListener() {
							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {		
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});			 			
			 			((RadioGroupField)from).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
							
							@Override
							public void onCheckedChanged(RadioGroup group, int checkedId) {
								
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged(group, (View)args[1], (View)args[2], (View)args[3], checkedId, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}
						});
			 		} else if(from.getClass() == RadioGroupOtherField.class){
//						Log.e(TAG, "listening().RadioGroupOtherField");
			 			from.setOnKeyListener(new View.OnKeyListener() {							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {		
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});			 			
			 			((RadioGroupOtherField)from).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {							
							@Override
							public void onCheckedChanged(RadioGroup group, int checkedId) {
								for(int i=0; i<call.size(); i++){
//									Log.e(TAG, "listening().RadioGroupOtherField.onCheckedChanged");
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged((RadioGroupOtherField)from, (View)args[1], (View)args[2], (View)args[3], checkedId, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}
						});
			 		} else if(from.getClass() == CheckBoxField.class){
//						Log.e(TAG, "listening().CheckBoxField: " + ((CheckBoxField)from).getFieldName());
			 			from.setOnKeyListener(new View.OnKeyListener() {							
							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {									
								for(int i=0; i<call.size(); i++){
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], keyCode, event, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) return true;
								}
								
								return false;
							}
						});
			 			((CheckBoxField)from).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {							
							@Override
							public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
								if(!esCargaInicial)
									((CheckBoxField)from).onCheckedChangedAlterno(buttonView,isChecked);
								
								for(int i=0; i<call.size(); i++){
//									Log.e(TAG, "listening().CheckBoxField.onCheckedChanged: " + ((CheckBoxField)from).getFieldName());
							 		Object[] args = call.get(Integer.valueOf(i));
							 		CallbackEvent _call = (CallbackEvent)args[0];
							 		boolean consume = _call.onCheckedChanged(buttonView, (View)args[1], (View)args[2], (View)args[3], isChecked, (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
							 		if(consume) break;
								}
							}
						});
			 		}
				}
			});
	 		
		}
		
		viewsIter = null;
	}
	
	public void lockboot(){
		Iterator<View> fromsIter = callbackEvents.keySet().iterator();
		
		while(fromsIter.hasNext()){
			final View from = fromsIter.next();
			final HashMap<Integer, Object[]> call = callbackEvents.get(from);
			
			if(call == null) continue;
			
			for(int i=0; i<call.size(); i++){
				
				Object[] args = call.get(Integer.valueOf(i));
				
				if (args == null) continue; 
				
				View to = (View)args[1];
				View dataSource = args[3]==null?from:(View)args[3];
				@SuppressWarnings("unchecked")
				HashMap<String, LinkedList> values = (HashMap<String, LinkedList>)args[4];
				Boolean cancel = (Boolean)args[5];
				Boolean allEquals = (Boolean)args[6];
				
				if(values == null) continue;
				
				Iterator<String> iter = values.keySet().iterator();
				boolean cumple = false;
				
				while(iter.hasNext()){
					String oper = iter.next();
					LinkedList val = values.get(oper);
					if(Util.cumpleAlguno(val(dataSource), val, oper, allEquals)){
						handleLocks(from, to, true);
						return;
					}
				}
			}
		}
	}	
	
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T>void addQueueCallbackEvent(U from, CallbackEvent callbackEvent, V to, S next, T dataSource, HashMap<String, LinkedList> values){
		addQueueCallbackEvent(from, callbackEvent, to, next, dataSource, values, true, false);
	}
	
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T>void addQueueCallbackEvent(U from, CallbackEvent callbackEvent, V to, S next, T dataSource, HashMap<String, LinkedList> values, Boolean cancel){
		addQueueCallbackEvent(from, callbackEvent, to, next, dataSource, values, cancel, false);
	}
	
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T>void addQueueCallbackEvent(U from, CallbackEvent callbackEvent, V to, S next, T dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		Object[] obj = new Object[]{callbackEvent, (View)to, next, dataSource, values, cancel, allEquals};
		View _from = null;
		if(from instanceof EditText) _from = (EditText)from;
		else if(from instanceof SpinnerField) _from = (SpinnerField)from;
		else if(from instanceof RadioGroupField) _from = (RadioGroupField)from;
		else if(from instanceof CheckBoxField) _from = (CheckBoxField)from;
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
	}
	
	public <U>void bind(final EventKey eventKey, U... from){
		for( U _from : from){
			bind(_from, eventKey);
		}
	}
	
	public <U>void bind(final EventCheckedCheck eventCheck, U... from){
		for( U _from : from){
			bind(_from, eventCheck);
		}
	}
	
	public <U>void bind(final EventCheckedRadio eventCheck, U... from){
		for( U _from : from){
			bind(_from, eventCheck);
		}
	}
	
	protected <U>void bind(final EventKey eventKey, final EventCheckedCheck eventCheck, U... from){
		for( U _from : from){
			bind(_from, eventKey);
			bind(_from, eventCheck);
		}
	}
	
	protected <U>void bind(final EventKey eventKey, final EventCheckedRadio eventCheck, U... from){
		for( U _from : from){
			bind(_from, eventKey);
			bind(_from, eventCheck);
		}
	}
	
	protected <U>void bind(U from, final EventKey eventKey, final EventCheckedRadio eventCheck){
			bind(from, eventKey);
			bind(from, eventCheck);
	}
	
	public <U>void bind(U from, final EventKey eventKey, final EventCheckedCheck eventCheck){
		bind(from, eventKey);
		bind(from, eventCheck);
	}
	
	public <U>void bind(U from, final EventKey eventKey){	
		
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return eventKey.onKey(from, to, next, keyCode, event, values, cancel, allEquals);
			}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		
		Object[] obj = new Object[]{callbackEvent, null, null, from, null, null, null};
		View _from = null;
		if(from instanceof EditText) _from = (EditText)from;
		else if(from instanceof TextField) _from = (TextField)from;
		else if(from instanceof SpinnerField) _from = (SpinnerField)from;
		else if(from instanceof RadioGroupField) _from = (RadioGroupField)from;
		else if(from instanceof CheckBoxField) _from = (CheckBoxField)from;
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
	}
	
	public <U>void bind (U from, final EventCheckedRadio checkedRadioEvent){
		
		if(!(from instanceof RadioGroupField)) return;
		
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return checkedRadioEvent.onCheckedChanged(from, to, next, checkedId, values, cancel, allEquals);
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return checkedRadioEvent.onCheckedChanged(from, to, next, checkedId, values, cancel, allEquals);
			}
		};
		
		Object[] obj = new Object[]{callbackEvent, null, null, from, null, null, null};
		View _from = null;
		if(from instanceof RadioGroupField) _from = (RadioGroupField)from;
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
	}
	
	
	public <U>void bind (U from, final EventSelectedItem selectedItemEvent){		
		if(!(from instanceof SpinnerField)) return;
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return selectedItemEvent.onItemSelected(arg0, from, to, next, dataSource, arg2, arg3, values, cancel, allEquals);
			}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return false;
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

		};
		
		Object[] obj = new Object[]{callbackEvent, null, null, from, null, null, null};
		View _from = null;
		if(from instanceof SpinnerField) _from = (SpinnerField)from;
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
	}
	
	public <U>void bind (U from, final EventCheckedCheck checkedCheckEvent){
		
		if(!(from instanceof CheckBoxField)) return;
		
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return checkedCheckEvent.onCheckedChanged(from, to, next, isChecked, values, cancel, allEquals);
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		
		Object[] obj = new Object[]{callbackEvent, null, null, from, null, null, null};
		View _from = null;
		if(from instanceof CheckBoxField) _from = (CheckBoxField)from;
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
	}
	
	protected void unbind(View from){
		unbind(from, EVENT_ALL);
	}
	
	protected void unbind(View from, int... events){
		if(from == null) return;
		
		for (int event:events){
			unbind(from, event);
		}
	}
	
	protected void unbind(View from, int event){
		if(from == null) return;
		
		if( event == EVENT_ALL){
			//callbackEvents.remove(from);
			unbindOnKey(from);
			unbindOnClick(from);
			unbindOnFocus(from);
			
			if(from instanceof Spinner || from instanceof SpinnerField){
				unbindOnItemSelected(from);
			}
			else if(from instanceof CheckBox || from instanceof CheckBoxField){
				unbindOnCheckedCheck(from);
			}
			else if(from instanceof RadioGroup || from instanceof RadioGroupField){
				unbindOnCheckedRadio(from);
			}
			
			if(callbackEvents!=null) callbackEvents.remove(from);
		}
		else if(event == EVENT_CHECKED_CHECK){
			unbindOnCheckedCheck(from);
		}
		else if(event == EVENT_CHECKED_RADIO){
			unbindOnCheckedRadio(from);
		}
		else if(event == EVENT_KEY){
			unbindOnKey(from);
		}
		else if(event == EVENT_ITEM_SELECTED){
			unbindOnItemSelected(from);
		}
		else if(event == EVENT_FOCUS){
			unbindOnFocus(from);
		}
		
		if(callbackEvents==null) return;
		
		Iterator<View> viewsIter = callbackEvents.keySet().iterator();
		while(viewsIter.hasNext()){

			final View _from = viewsIter.next();
			final HashMap<Integer, Object[]> call = callbackEvents.get(from);
		}
		
	}
	
	private void unbindOnKey(View from){
		if(from == null) return;
		from.setOnKeyListener(null);
	}
	
	private void unbindOnCheckedCheck(View from){
		if(from == null || !(from instanceof CheckBox) || !(from instanceof CheckBoxField)) return;
		if(from instanceof CheckBox) ((CheckBox)from).setOnCheckedChangeListener(null);
		else if(from instanceof CheckBoxField) ((CheckBoxField)from).setOnCheckedChangeListener(null);
	}
	
	private void unbindOnCheckedRadio(View from){
		if(from == null || !(from instanceof RadioGroupField)) return;
		if(from instanceof RadioGroup) ((RadioGroup)from).setOnCheckedChangeListener(null);
		else if(from instanceof RadioGroupField) ((RadioGroupField)from).setOnCheckedChangeListener(null);
	}
	
	private void unbindOnItemSelected(View from){
		if(from == null || !(from instanceof Spinner) || !(from instanceof SpinnerField)) return;
		if(from instanceof Spinner) ((Spinner)from).setOnItemSelectedListener(null);
		else if(from instanceof SpinnerField) ((SpinnerField)from).setOnItemSelectedListener(null);
	}
	
	private void unbindOnClick(View from){
		if(from == null) return;

		//setOnItemClickListener cannot be used with a spinner.
		/*if(from instanceof Spinner) ((Spinner)from).setOnItemClickListener(null);
		else if(from instanceof CenacomSpinner) ((CenacomSpinner)from).setOnItemClickListener(null);*/
		
		if(from instanceof EditText || from instanceof Button || from instanceof CheckBox|| from instanceof RadioGroup || 
				from instanceof CheckBoxField || from instanceof RadioGroupField ) from.setOnClickListener(null);
	}
	
	private void unbindOnFocus(View from){
		if(from == null) return;
		from.setOnFocusChangeListener(null);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2EditText(EditText from, EditText to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("jumpEditText2EditText.onKey", from.getClass().getSimpleName() + "["+from.hashCode()+"]");
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					
					Log.e("obs", String.valueOf(to));
					from.setNextFocusDownId(to.getId());
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										method = containerContext.getClass().getDeclaredMethod(c);
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		from.setNextFocusDownId(to.getId());
						            		to.requestFocus();
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
							} else {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
	            		from.setNextFocusDownId(to.getId());
			            to.requestFocus();
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	method = containerContext.getClass().getDeclaredMethod(c);
									if (method == null) {
										continue;
									}
					            	method.invoke(containerContext);													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        } catch (Exception ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.setNextFocusDownId(from.getId());
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.-100");
					return false;
				}
//				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
//					from.setNextFocusDownId(from.getId());
//					return false;
//				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2CenacomSpinner(EditText from, SpinnerField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				
				if ( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} 
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2CenacomRadioGroup(final EditText from, final RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("EditText.onKey","Ejecucion de " + from.hashCode());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
//					Log.e("EditText.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName());
					from.setNextFocusDownId(to.getId());
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										method = containerContext.getClass().getDeclaredMethod(c);
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		from.setNextFocusDownId(to.getId());
						            		to.requestFocus();
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
							} else {
								to.requestFocus();
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            to.requestFocus();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	method = containerContext.getClass().getDeclaredMethod(c);
									if (method == null) {
										continue;
									}
					            	method.invoke(containerContext);													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.setNextFocusDownId(from.getId());
//					from.requestFocus();
//					Log.e("EditText.onKey", "event.getAction()==-100");
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpEditText2CenacomRadioOtherGroup(final EditText from, final RadioGroupOtherField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("EditText.onKey","Ejecucion de " + from.hashCode());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					Log.e("EditText.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName());
					from.setNextFocusDownId(to.getId());
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
				            	if (callbacks.size() == 0) {
				            		to.requestFocus();
									return true;
								}
				            	for (String c : callbacks) {
				            		Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", 
				            				"KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"] callback["+c+"]");
								
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										method = containerContext.getClass().getDeclaredMethod(c);
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		from.setNextFocusDownId(to.getId());
						            		to.requestFocus();
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
							} else {
								Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", 
			            				"KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"] no hay callback");
								to.requestFocus();
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            to.requestFocus();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	method = containerContext.getClass().getDeclaredMethod(c);
									if (method == null) {
										continue;
									}
					            	method.invoke(containerContext);													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.setNextFocusDownId(from.getId());
//					from.requestFocus();
//					Log.e("EditText.onKey", "event.getAction()==-100");
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2CenacomCheckBox(EditText from, CheckBoxField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver3", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
//					Log.e("EditText.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName());
					from.setNextFocusDownId(to.getId());
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
				            	if (callbacks.size() == 0) {
				            		to.requestFocus();
									return true;
								}
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										method = containerContext.getClass().getDeclaredMethod(c);
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		from.setNextFocusDownId(to.getId());
						            		to.requestFocus();
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
							} else {
								to.requestFocus();
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            to.requestFocus();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	method = containerContext.getClass().getDeclaredMethod(c);
									if (method == null) {
										continue;
									}
					            	method.invoke(containerContext);													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2Button(EditText from, Button to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("EditText.onKey", "KeyEvent to: " + to.getClass().getSimpleName() + "[" + to.hashCode() + "]");
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					Log.e("EditText.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName() + "[" + to.hashCode() + "]");
					from.setNextFocusDownId(to.getId());
					to.requestFocus();
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
				            	if (callbacks.size() == 0) {
				            		to.requestFocus();
									return true;
								}
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										method = containerContext.getClass().getDeclaredMethod(c);
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		from.setNextFocusDownId(to.getId());
						            		to.requestFocus();
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
							} else {
								to.requestFocus();
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            to.requestFocus();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	method = containerContext.getClass().getDeclaredMethod(c);
									if (method == null) {
										continue;
									}
					            	method.invoke(containerContext);													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					return true;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpEditButton2View(Button from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.requestFocus();
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.requestFocus();
					return true;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpEditText2View(EditText from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					Log.e("EditText.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName() + "["+ to.hashCode() +"]");
					from.clearFocus();
					if (from instanceof TextBoxField) {
						((TextBoxField)from).callback();	
					} else {
						return true;
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();

			            	if (callbacks == null) {
			            		to.requestFocus();
								return true;
							}
			            	if (callbacks.size() == 0) {
			            		to.requestFocus();
								return true;
							}
				            for (String c : callbacks) {
				            	callback = c;
				            	if (c.equals("verificarRangos")) {
				            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
								} else {
									method = containerContext.getClass().getDeclaredMethod(c);
								}
				            	if (method == null) {
									continue;
								}
				            	if (c.equals("verificarRangos")) {
				            		method.invoke((NumberField)from);
								} else {
									method.invoke(containerContext);
								}
					            if (c.equals("verificarRangos")) {
					            	if (!((NumberField)from).isRangoOk()) {
					            		ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
												ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
										from.setNextFocusDownId(from.getId());
										from.requestFocus();
									}
					            }
							}				            
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2EditText(SpinnerField from, EditText to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN == event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER ){
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				} 
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2CenacomSpinner(SpinnerField from, SpinnerField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2CenacomRadioGroup(SpinnerField from, RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2CenacomRadioOtherGroup(SpinnerField from, RadioGroupOtherField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2CenacomCheckBox(SpinnerField from, CheckBoxField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2Button(SpinnerField from, Button to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomSpinner2View(SpinnerField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.clearFocus();
					//nextFragment();
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.clearFocus();
				//nextFragment();
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2EditText(RadioGroupField from, EditText to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onCheckedChanged", to.getClass().getSimpleName());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onKey", to.getClass().getSimpleName());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
//					Log.e("RadioGroupField.onKey", "KeyEvent.ACTION_DOWN");
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
//					Log.e("RadioGroupField.onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2EditText(RadioGroupOtherField from, EditText to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onCheckedChanged", to.getClass().getSimpleName());
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("jumpCenacomRadioGroup2EditText.onCheckedChanged", from.getClass().getSimpleName() + "["+from.hashCode()+"] to " + to.getClass().getSimpleName() + "["+to.hashCode()+"]");				
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onKey", to.getClass().getSimpleName());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
//					Log.e("RadioGroupField.onKey", "KeyEvent.ACTION_DOWN");
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
//					Log.e("RadioGroupField.onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomSpinner(RadioGroupField from, SpinnerField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onCheckedChanged", to.getClass().getSimpleName());
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}

	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomSpinner(RadioGroupOtherField from, SpinnerField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("RadioGroupField.onCheckedChanged", to.getClass().getSimpleName());
				return false;
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomRadioGroup(RadioGroupField from, RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver8", to.toString());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return false;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver9", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}	

	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomRadioGroup(RadioGroupOtherField from, RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver8", to.toString());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver9", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomRadioOtherGroup(RadioGroupField from, RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver8", to.toString());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return false;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver9", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomRadioOtherGroup(RadioGroupField from, RadioGroupOtherField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver8", to.toString());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver9", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}

	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomRadioOtherGroup(RadioGroupOtherField from, RadioGroupOtherField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver8", to.toString());
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver9", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					//from.setNextFocusDownId(to.getId());
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomCheckBox(RadioGroupField from, CheckBoxField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver10", to.toString());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver11", to.toString());
				if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					to.requestFocus();
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2CenacomCheckBox(RadioGroupOtherField from, CheckBoxField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver10", to.toString());
				to.requestFocus();
				return true;
			}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver11", to.toString());
				if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					to.requestFocus();
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2Button(RadioGroupField from, Button to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomRadioGroup2Button(RadioGroupOtherField from, Button to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("jumpCenacomRadioGroup2Button.RadioGroupOtherField.onCheckedChanged", from.getClass().getSimpleName() + "["+from.hashCode()+"] to " + to.getClass().getSimpleName() + "["+to.hashCode()+"]");
				
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { 
				Log.e("jumpCenacomRadioGroup2Button.RadioGroup.onCheckedChanged", from.getClass().getSimpleName() + "["+from.hashCode()+"] to " + to.getClass().getSimpleName() + "["+to.hashCode()+"]");
				return false; 
			}
			
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpCenacomRadioGroup2View(RadioGroupField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver15", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.clearFocus();
					//nextFragment();
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.clearFocus();
				//nextFragment();
				return true;
			}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}

	private void jumpCenacomRadioGroup2View(RadioGroupOtherField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("ver15", to.toString());
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.clearFocus();
					//nextFragment();
					return true;
				}
				else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(from.getId());
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				return false;
			}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				from.clearFocus();
				return true;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2EditText(CheckBoxField from, EditText to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					lockView(to, false);
					to.requestFocus();
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				lockView(to, false);
				from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2CenacomSpinner(CheckBoxField from, SpinnerField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					to.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2CenacomRadioGroup(CheckBoxField from, RadioGroupField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				if(to instanceof RadioGroupField) to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2CenacomRadioOtherGroup(CheckBoxField from, RadioGroupOtherField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.setNextFocusDownId(to.getId());
				if(to instanceof RadioGroupField) to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2CenacomCheckBox(CheckBoxField from, CheckBoxField to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					to.requestFocus();
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					return true;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER){
					return true;
				}
				
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				//from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2Button(CheckBoxField from, Button to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.setNextFocusDownId(to.getId());
					return true;
				}
				
				return false;
			}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				//from.setNextFocusDownId(to.getId());
				to.requestFocus();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	@SuppressWarnings("rawtypes")
	private void jumpCenacomCheckBox2View(CheckBoxField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					from.clearFocus();
					//nextFragment();
					return true;
				}
				
				return false;
			}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				from.clearFocus();
				//nextFragment();
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param values
	 */
	@SuppressWarnings("rawtypes")
	public <U, V> void saltar(U from, V to, HashMap<String, LinkedList> values){
		saltar(from, to, null, null, values, true, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param values
	 * @param cancel
	 */
	@SuppressWarnings("rawtypes")
	public <U, V> void saltar(U from, V to, HashMap<String, LinkedList> values, boolean cancel){
		saltar(from, to, null, null, values, cancel, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param values
	 * @param cancel
	 * @param allEquals
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V> void saltar(U from, V to, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals){
		saltar(from, to, null, null, values, cancel, allEquals, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @param lock
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V> void saltar(U from, V to, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals, boolean lock){
		saltar(from, to, null, null, values, cancel, allEquals, lock);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param values
	 */
	@SuppressWarnings("rawtypes")
	public <U, V, S> void saltar(U from, V to, S next, HashMap<String, LinkedList> values){
		saltar(from, to, next, null, values, true, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param values
	 * @param cancel
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S> void saltar(U from, V to, S next, HashMap<String, LinkedList> values, boolean cancel){
		saltar(from, to, next, null, values, cancel, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param values
	 * @param cancel
	 * @param allEquals
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S> void saltar(U from, V to, S next, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals){
		saltar(from, to, next, null, values, cancel, allEquals, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @param lock
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S> void saltar(U from, V to, S next, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals, boolean lock){
		saltar(from, to, next, null, values, cancel, allEquals, lock);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param dataSource
	 * @param values
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T> void saltar(U from, V to, S next, T dataSource, HashMap<String, LinkedList> values){
		saltar(from, to, next, dataSource, values, true, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param dataSource
	 * @param values
	 * @param cancel
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T> void saltar(U from, V to, S next, T dataSource, HashMap<String, LinkedList> values, boolean cancel){
		saltar(from, to, next, dataSource, values, cancel, false, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param dataSource
	 * @param values
	 * @param cancel
	 * @param allEquals
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T> void saltar(U from, V to, S next, T dataSource, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals){
		saltar(from, to, next, dataSource, values, cancel, allEquals, true);
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param values
	 * @param cancel
	 * @param allEquals
	 */
	@SuppressWarnings("rawtypes")
	protected <U, V, S, T> void saltar(U from, V to, S next, T dataSource, HashMap<String, LinkedList> values, boolean cancel, boolean allEquals, boolean lock){
				
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				
				dataSource = dataSource==null?from:dataSource;
				
				if(KeyEvent.KEYCODE_ENTER == keyCode && KeyEvent.ACTION_DOWN == event.getAction()){
					Iterator<String> iter = values.keySet().iterator();
					boolean cumple = false;
					
					while(iter.hasNext()){
						String oper = iter.next();
						LinkedList val = values.get(oper);
						if(Util.cumpleAlguno(val(dataSource), val, oper, allEquals)){
							handleLocks(from, to, true);
							cumple = true;
							from.setNextFocusDownId(to.getId());
							if( !(to instanceof CheckBoxField || to instanceof Button) ) {
								lockView(to, false);
								to.requestFocus();
							}
							if(to instanceof EditText || to instanceof CheckBoxField) to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
							if(cancel) return true;
							return false;
						}
					}
					if(!cumple){
						handleLocks(from, to, false);
						if(next == null) return false;
						lockView(next, false);
						from.setNextFocusDownId(next.getId());
						if(next instanceof EditText || next instanceof CheckBoxField) next.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
						else next.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
						next.requestFocus();
						return true;
					}
					return true;
				}
				return false;
			}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {

				dataSource = dataSource==null?from:dataSource;
				Iterator<String> iter = values.keySet().iterator();
				boolean cumple = false;
				
				while(iter.hasNext()){
					String key = iter.next();
					LinkedList val = values.get(key);
					
					if(Util.cumpleAlguno(val(dataSource), val, key, allEquals)){
						handleLocks(from, to, true);
						cumple = true;
						lockView(to, false);
						from.setNextFocusDownId(to.getId());
						to.requestFocus();
						if(cancel) return true;
						return false;
					}
				}
				if(!cumple){
					handleLocks(from, to, false);
					if(next == null) return false;
					lockView(next, false);
					from.setNextFocusDownId(next.getId());
					next.requestFocus();
					return true;
				}
				return false;
			}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				
				Iterator<String> iter = values.keySet().iterator();
				boolean cumple = false;
				
				while(iter.hasNext()){
					String key = iter.next();
					LinkedList val = values.get(key);
					if(Util.cumpleAlguno(val(from), val, key, allEquals)){
						handleLocks(from, to, true);
						cumple = true;
						//from.setNextFocusDownId(to.getId());
						lockView(to, false);
						to.requestFocus();
						if(cancel) return true;
						return false;
					}
				}
				if(!cumple){
					handleLocks(from, to, false);
					if(next == null) return false;
					lockView(next, false);
					from.setNextFocusDownId(next.getId());
					next.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
					next.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {

				dataSource = dataSource==null?from:dataSource;
				
				Iterator<String> iter = values.keySet().iterator();
				boolean cumple = false;
				
				while(iter.hasNext()){
					String key = iter.next();
					LinkedList val = values.get(key);
					if(Util.cumpleAlguno(val(dataSource), val, key, allEquals)){
						handleLocks(from, to, true);
						cumple = true;
						lockView(to, false);
						from.setNextFocusDownId(to.getId());
						to.requestFocus();
						if(cancel) return true;
						return false;
					}
				}
				if(!cumple){
					handleLocks(from, to, false);
					if(next == null) return false;
					lockView(next, false);
					from.setNextFocusDownId(next.getId());
					next.requestFocus();
					return true;
				}
				return false;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				dataSource = dataSource==null?from:dataSource;
				Iterator<String> iter = values.keySet().iterator();
				boolean cumple = false;
				
				while(iter.hasNext()){
					String key = iter.next();
					LinkedList val = values.get(key);
					
					if(Util.cumpleAlguno(val(dataSource), val, key, allEquals)){
						handleLocks(from, to, true);
						cumple = true;
						lockView(to, false);
						from.setNextFocusDownId(to.getId());
						to.requestFocus();
						if(cancel) return true;
						return false;
					}
				}
				if(!cumple){
					handleLocks(from, to, false);
					if(next == null) return false;
					lockView(next, false);
					from.setNextFocusDownId(next.getId());
					next.requestFocus();
					return true;
				}
				return false;
			}
		};
		View _from=(View)from;
		if(from instanceof EditText)_from = (EditText)from;
		else if(from instanceof SpinnerField)_from = (SpinnerField)from;
		else if(from instanceof RadioGroupField)_from = (RadioGroupField)from;
		else if(from instanceof CheckBoxField)_from = (CheckBoxField)from;
		
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, _from, obj);
		
	}
	
	protected void handleLocks(View from, View to, boolean lock){
		Object containerContext = getContainerContext()==null?this:getContainerContext();
		
		handleLocks(from, to, lock, containerContext);
	}
	
	/**
	 * Maneja los bloqueos de views. Este m\u00e9todo es usado por {@link #saltar(Object, Object, Object, Object, HashMap, boolean, boolean, boolean)}
	 * para limpiar y bloquear views y para desbloquear views
	 * 
	 * @param from
	 * @param to
	 * @param lock
	 */
	protected <T>void handleLocks(View from, View to, boolean lock, T container){
		if(from == null) return;
		List<View> views = blockerAndBlocked.get(from);
		
		//Primero desbloqueo todo lo que from haya bloqueado
		if(views != null) lockView(views, false);
		
		//Si se va a desbloquear elementos y from ha bloqueado elementos anteriormente antes entonces salir,
		//porque los elementos anteriormente bloqueados por from ya fueron desbloqueados
		if(!lock && views != null) return;
		
		/**TODO: Falta terminar el soporte para dialogos*/
		fieldsOrdered = containerFieldsOrdered.get(container);
		fieldsPosition = containerFieldsPosition.get(container);
		numFields = containerNumFields.get(container);
		
		Integer start = fieldsPosition.get(from);
		Integer end = fieldsPosition.get(to)==null?numFields:fieldsPosition.get(to);
		
		if(start == end || start == end-1) return;
		
		views = new ArrayList<View>();
		for(int i=start+1; i<=end-1; i++){
			View view = fieldsOrdered[i];
			if(lock) cleanAndLockView(view);
			else lockView(view, false);
			views.add(view);
		}
		
		blockerAndBlocked.put(from, views);
	}
	
	/**
	 * 
	 * @param activity
	 * @param minimo
	 * @param maximo
	 * @param editText
	 */
	protected <V> void rango(final Activity activity, final Integer minimo, final Integer maximo, final EditText... editText){
		for(EditText et:editText){
			rango(activity, et, minimo, maximo, null, null);
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected <V> void rango(final Activity activity, final Integer minimo, final Integer maximo, LinkedList otros, final EditText... editText){
		for(EditText et:editText){
			rango(activity, et, minimo, maximo, otros, null);
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected <V> void rango(final Activity activity, final Integer minimo, final Integer maximo, LinkedList otros, final V omision, final EditText... editText){
		for(EditText et:editText){
			rango(activity, et, minimo, maximo, otros, omision);
		}
	}
	
	/**
	 * 
	 * @param activity
	 * @param editText
	 * @param minimo
	 * @param maximo
	 */
	public <V> void rango(final Activity activity, final EditText editText, final Integer minimo, final Integer maximo){
		rango(activity, editText, minimo, maximo, null, null);
	}
	
	@SuppressWarnings("rawtypes")
	protected <V> void rango(final Activity activity, final EditText editText, final Integer minimo, final Integer maximo, LinkedList otros){
		rango(activity, editText, minimo, maximo, otros, null);
	}
	
	/**
	 * 
	 * @param activity
	 * @param editText
	 * @param minimo
	 * @param maximo
	 * @param omision
	 */
//	public <V> void rango(final Activity activity, final EditText editText, final Integer minimo, final Integer maximo, final LinkedList otros, final V omision){
//		
//		CallbackEvent callbackEvent = new CallbackEvent() {
//			
//			@Override
//			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
//				Log.e("Rango.onKey", from.getClass().getSimpleName() + "["+from.hashCode()+"]");
//				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
//					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].Rango.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
//					if(Util.esVacio(editText.getText().toString())) return false;
//					
//					Integer val = Integer.valueOf(editText.getText().toString());
//					
//					if(val.toString().length()<minimo.toString().length() && KeyEvent.KEYCODE_ENTER != keyCode) return false;
//					if( val<minimo || val>maximo ) {
//						if( otros!=null && otros.indexOf(val) !=-1 ) return false;
//						else if(omision == null){
//							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y no existe valor por omisi\u00f3n\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//						}
//						else if( !val.toString().equals(omision.toString()) ){
//							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y el valor por omisi\u00f3n es \"" + omision + "\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//						}
//						else if( val.toString().equals(omision.toString()) ){
//							return false;
//						}
//						from.setNextFocusDownId(to.getId());
////						if (to instanceof EditText || to instanceof IntegerField || to instanceof DecimalField
////								 || to instanceof TextField || to instanceof TextAreaField) {
////							to.dispatchKeyEvent(KeyEvent.changeAction(event, -100));
////						}
//						return true;
//					} 
//					return false;
//				}
//				return false;
//			}
//			
//			@Override
//			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
//			
//			@Override
//			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
//			
//			@Override
//			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
//
//			@Override
//			public void onFocusChange(View v, boolean hasFocus) {
//				if(hasFocus){
//					if( returnFocus==true ){
//						if(viewLostFocus!=null) viewLostFocus.requestFocus();
//						v.clearFocus();
//					}
//					return;
//				}
//				
//				viewLostFocus = null;
//				returnFocus = false;
//				
//				
//				//if(hasFocus) return;
//				if(Util.esVacio(editText.getText().toString())) return;
//				
//				Integer val = Integer.valueOf(editText.getText().toString());
//				
//				if(val.toString().length()<minimo.toString().length() && !hasFocus) return;
//				if( val<minimo || val>maximo ) {
//					if(omision == null){
//						ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y no existe valor por omisi\u00f3n\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//					}
//					else if( !val.toString().equals(omision.toString()) ){
//						ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y el valor por omisi\u00f3n es \"" + omision + "\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//					}
//					else if( val.toString().equals(omision.toString()) ){
//						return;
//					}
//					//v.requestFocus();
//					returnFocus = true;
//					viewLostFocus = v;
//					return;
//				}
//				return;
//			}
//		};
		
	public <V> void rango(final Activity activity, final EditText editText, final Integer minimo, final Integer maximo, final LinkedList otros, final V omision){
		if (editText instanceof NumberField) {
//			Log.e(this.getClass().getSimpleName(), "verificarRangos");
			NumberField numberField = (NumberField) editText;
			numberField.setMinValue(minimo);
			numberField.setMaxValue(maximo);
			numberField.setOmision(omision);
			numberField.setExcepValues(otros);
			List<String> callbacksOld = numberField.getCallbacks();
			List<String> callbacks = new ArrayList<String>();
			callbacks.add("verificarRangos");
			if (callbacksOld != null) {
				callbacks.addAll(callbacksOld);	
			}
			numberField.setCallbacks(callbacks);
			getCamposRangeados(getContainerContext()).add(numberField);
			return;
		}
		CallbackEvent callbackEvent = new CallbackEvent() {
		
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				Log.e("Rango.onKey", from.getClass().getSimpleName() + "["+from.hashCode()+"]");
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].Rango.onKey", "KeyEvent.ACTION_DOWN to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if(Util.esVacio(editText.getText().toString())) return false;
					
					Integer val = Integer.valueOf(editText.getText().toString());
					
					if(val.toString().length()<minimo.toString().length() && KeyEvent.KEYCODE_ENTER != keyCode) return false;
					if( val<minimo || val>maximo ) {
						if( otros!=null && otros.indexOf(val) !=-1 ) return false;
						else if(omision == null){
							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y no existe valor por omisi\u00f3n\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						}
						else if( !val.toString().equals(omision.toString()) ){
							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y el valor por omisi\u00f3n es \"" + omision + "\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						}
						else if( val.toString().equals(omision.toString()) ){
							return false;
						}
						from.setNextFocusDownId(to.getId());
						return true;
					} 
					return false;
				}
				return false;
			}
		
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
	
			@Override
			public void onFocusChange(View v, boolean hasFocus) { return; }

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}			
		};
		Object[] obj = new Object[]{callbackEvent, editText, null, editText, null, null, null};
		callbackEvents = addOrInsertHashMap(callbackEvents, editText, obj);
		
		if( rangeados == null ) rangeados = new HashMap<Object, ArrayList<View>>();
		ArrayList<View> ls = rangeados.get(getContainerContext());
		if( ls == null ) ls = new ArrayList<View>();
		ls.add(editText);
		rangeados.put(getContainerContext(), ls);
	}
	
	public <V> void rango(final Activity activity, final EditText editText, final BigDecimal minimo, final BigDecimal maximo, final LinkedList otros){
		rango(activity, editText, minimo, maximo, otros, null);
	}
	
	public <V> void rango(final Activity activity, final EditText editText, final BigDecimal minimo, final BigDecimal maximo){
		rango(activity, editText, minimo, maximo, null, null);
	}
	
	public <V> void rango(final Activity activity, final EditText editText, final Long minimo, final Long maximo, final LinkedList otros, final V omision){
		if (editText instanceof NumberField) {
//			Log.e(this.getClass().getSimpleName(), "verificarRangos");
			NumberField numberField = (NumberField) editText;
			numberField.setMinValue(minimo);
			numberField.setMaxValue(maximo);
			numberField.setOmision(omision);
			numberField.setExcepValues(otros);
			List<String> callbacksOld = numberField.getCallbacks();
			List<String> callbacks = new ArrayList<String>();
			callbacks.add("verificarRangos");
			if (callbacksOld != null) {
				callbacks.addAll(callbacksOld);	
			}
			numberField.setCallbacks(callbacks);
			getCamposRangeados(getContainerContext()).add(numberField);
			return;
		}
	}
	
	public <V> void rango(final Activity activity, final EditText editText, final BigDecimal minimo, final BigDecimal maximo, final LinkedList otros, final V omision){
		
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					if(Util.esVacio(editText.getText().toString())) return false;
					
					BigDecimal val = new BigDecimal(editText.getText().toString());
					
					/*
					 * val.compareTo(otherVal);
					 * 1 if val > otherVal, -1 if val < otherVal, 0 if val == otherVal
					 * */
					if(val.toString().length()<minimo.toString().length() && KeyEvent.KEYCODE_ENTER != keyCode) return false;
					//if( val<minimo || val>maximo ) {
					if( val.compareTo(minimo)==-1 || val.compareTo(maximo)==1 ) {
						if( otros!=null && otros.indexOf(val) !=-1 ) return false;
						if(omision == null){
							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y no existe valor por omisi\u00f3n\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						}
						else if( !val.toString().equals(omision.toString()) ){
							ToastMessage.msgBox(activity, "El valor ingresado esta fuera del rango. Los valores permitidos van desde \"" + minimo + "\" hasta \"" + maximo + "\" y el valor por omisi\u00f3n es \"" + omision + "\"", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						}
						from.setNextFocusDownId(to.getId());
						return true;
					}
					return false;
				}
				return false;
			}
			
			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}

			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to,
					View next, Object dataSource, int checkedId,
					HashMap<String, LinkedList> values, Boolean cancel,
					Boolean allEquals) {
				// TODO Auto-generated method stub
				return false;
			}
		};

		Object[] obj = new Object[]{callbackEvent, editText, null, editText, null, null, null};
		callbackEvents = addOrInsertHashMap(callbackEvents, editText, obj);
		
		if( rangeados == null ) rangeados = new HashMap<Object, ArrayList<View>>();
		ArrayList<View> ls = rangeados.get(getContainerContext());
		if( ls == null ) ls = new ArrayList<View>();
		ls.add(editText);
		rangeados.put(getContainerContext(), ls);
	}
	
	/**
	 * 
	 * @param view
	 * @return
	 */
	private <T> String val(T view){
		if(view.getClass() == EditText.class){
			return ((EditText)view).getText().toString();
		}
		else if(view.getClass() == RadioGroupField.class){
			return ((RadioGroupField)view).getTagSelected("").toString();
		}
		else if(view.getClass() == CheckBoxField.class){
			return ((CheckBoxField)view).getCheckedTag().toString();
		}
		if(view.getClass() == SpinnerField.class){
            return ((SpinnerField)view).getSelectedItemKey()==null?null:((SpinnerField)view).getSelectedItemKey().toString();
            //return ((CenacomSpinner)view).getSelectedItemKey().toString();
		}
		else{
			return "";
		}
	}
	
	/**
	 * 
	 * @param field
	 */
	protected void setHover(final View field, final String prefix){
		Object container = getContainerContext()==null?this:getContainerContext();
		setHover(container, field, prefix);
	}
	
	protected <T extends Object>void setHover(final T container, final View field, final String prefix){
		
		field.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				//RAMON:
				View.OnFocusChangeListener listener = (View.OnFocusChangeListener) v.getTag(R.id.onFocusChangeListenerInicio);
				if(listener!=null)
					listener.onFocusChange(v, hasFocus);
				//////
				if(hasFocus){
					backgroundDrawableUnfocus = v.getBackground();
					containerbackgroundDrawableUnfocus.put((Object)container, backgroundDrawableUnfocus);
					if(prefix.equals("chb") || prefix.equals("rg")){
						field.setBackgroundResource(R.color.verdeaguatransparent);
//						imm = (InputMethodManager) FragmentForm.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);							
//						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					} else if(prefix.equals("txt") || prefix.equals("spn")){
						//backgroundDrawableUnfocus = v.getBackground();
						if (prefix.equals("txt")) {
							Log.e("txt", "txt: ");
							((EditText)field).setBackgroundDrawable(getResources().getDrawable(R.drawable.backwithborderfocused));
						} else {
							field.setBackgroundResource(R.color.verdeagua);	
						}
						//INI-Elthon-20140305
						if ("txt".equals(prefix)) {
							//mostrar teclado
//							imm = (InputMethodManager) FragmentForm.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						} else {
//							imm = (InputMethodManager) FragmentForm.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);							
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
						//FIN-Elthon-20140305 y Bardales 2014
					}
				}
				else {
					if(prefix.equals("chb") || prefix.equals("rg")){
						//field.setBackgroundResource(R.color.transparent);
					}
					else if(prefix.equals("txt") || prefix.equals("spn")){
						//field.setBackgroundResource(R.color.cajaweb);
						//field.setBackgroundDrawable(backgroundDrawableUnfocus);
						if ("txt".equals(prefix)) {
//							imm = (InputMethodManager) FragmentForm.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
					backgroundDrawableUnfocus = containerbackgroundDrawableUnfocus.get((Object)container);
					if (prefix.equals("txt")) {
						((EditText)field).setBackgroundDrawable(getResources().getDrawable(R.drawable.backwithborder));
					} else {
						field.setBackgroundDrawable(backgroundDrawableUnfocus);
					}
					try {
//						if (field instanceof DecimalField) {
//							((DecimalField)field).callback();
//						} else if (field instanceof IntegerField) {
//							((IntegerField)field).callback();
//						} else if (field instanceof TextField) {
//							((TextField)field).callback();
//						} else if (field instanceof TextAreaField) {
//							((TextAreaField)field).callback();
//						}

						if (field instanceof TextBoxField) {
							((TextBoxField)field).callback();
							String callback = "";
							try {
					            Method method;
					            List<String> callbacks = ((TextBoxField)field).getCallbacksOnFocus();
					            if (callbacks != null) {
						            for (String c : callbacks) {
						            	callback = c;
						            	method = containerContext.getClass().getDeclaredMethod(c);
										if (method == null) {
											continue;
										}
						            	method.invoke(containerContext);	
//						            	try {
//							            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
//											if (method != null) {
//												method.invoke(containerContext, (TextBoxField)field);
//											} else {
//												
//											}	
//						            	} catch (NoSuchMethodException nsme) {
//								            Log.d(getClass().getSimpleName(),"ssNo existe el metodo "+callback+" con parametros en "+this.getClass());
//								            try {
//									            method = containerContext.getClass().getDeclaredMethod(c);
//									            if (method == null) continue;
//								            	method.invoke(containerContext);	
//								            } catch (NoSuchMethodException nsme2) {
//								            	Log.d(getClass().getSimpleName(),"ppNo existe el metodo "+callback+" con parametros en "+this.getClass());
//								            	throw new NoSuchMethodException("nnNo existe el metodo "+callback+" en "+this.getClass());
//								            }
//								        }	
									}	
								}
					        } catch (NoSuchMethodException nsme) {
					            throw new RuntimeException(
					                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
					        } catch (SecurityException se) {
					        	Log.e("FragmentForm", se.toString(), se);
					        } catch (IllegalArgumentException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (IllegalAccessException iae) {
					        	Log.e("FragmentForm", iae.toString(), iae);
					        } catch (InvocationTargetException ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        } catch (Exception ite) {
					        	Log.e("FragmentForm", ite.toString(), ite);
					        }
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
					}
				}
				
				
				//RAMON:
				View.OnFocusChangeListener listener2 = (View.OnFocusChangeListener) v.getTag(R.id.onFocusChangeListenerFin);
				if(listener2!=null)
					listener2.onFocusChange(v, hasFocus);
				//////
				
			}
		});		
	}
	
	public boolean isInRange() {
		
		List<NumberField> numberFields = getCamposRangeados(getContainerContext());
		if (numberFields.size() != 0) {
			for (NumberField field : numberFields) {
				field.callback();
				String callback = "";
				try {
		            Method method = null;
		            List<String> callbacks = field.getCallbacks();
		            if (callbacks != null) {
		            	for (String c : callbacks) {
			            	callback = c;
			            	if (c.equals("verificarRangos")) {
			            		method = field.getClass().getDeclaredMethod(c);
							}
			            	if (method == null) {
								continue;
							}
			            	if (c.equals("verificarRangos")) {
			            		method.invoke((NumberField)field);
							}
				            if (c.equals("verificarRangos")) {
				            	if (!field.isRangoOk()) {
									ToastMessage.msgBox(getActivity(), field.getMensajeRango(), 
											ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
									field.requestFocus();
									return false;
								}
				            }
						}
		            }
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e("FragmentForm", se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e("FragmentForm", iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e("FragmentForm", ite.toString(), ite);
		        }
			}
		}
		
		if(rangeados == null) return true;
		ArrayList<View> ls = rangeados.get(getContainerContext());
		if(ls==null) return true;
		for(View from:ls){
			from.requestFocus();
	 		
	 		/*---*/
			//HashMap<Integer, Object[]> call = callbackEvents.get(from);
			HashMap<View, HashMap<Integer, Object[]>> calls = containerCallbackEvents.get(getContainerContext()); 
			HashMap<Integer, Object[]> call = calls.get(from);
			/*---*/
	 		if(call == null) continue;
	 		
	 		/*Llamo al primer elemento porque el rango es el primer elemento de la cola*/
	 		Object[] args = call.get(0);
	 		CallbackEvent _call = (CallbackEvent)args[0];
		 	boolean consume = _call.onKey(from, (View)args[1], (View)args[2], (View)args[3], KeyEvent.KEYCODE_ENTER, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER), (HashMap<String, LinkedList>)args[4], (Boolean)args[5], (Boolean)args[6]);
	 		if(consume) return false;
		}
		return true;
	}
	
	protected void cleanQuestion(int start, int end){
		for(int i=start; i<=end; i++){
			cleanQuestion(i);
		}
	}
	
	protected void cleanQuestion(int nQuestion){
		cleanQuestion("" + nQuestion);
	}
	
	protected void cleanQuestion(String nQuestion){
		for(View view : getViews(nQuestion)){
			cleanView(view);
		}
	}
	
	protected void cleanViewNext(View view){
		cleanView(nextView(view));
	}
	
	protected void cleanViewNext(View view, int nexts){
		
		if(nexts<=0) return;
		
		for(int i=1; i<=nexts; i++){
			view = nextView(view);
			cleanView(view);
		}
		
	}
	
	public void cleanView(List<View> views){	
		Util.cleanView(getActivity(), views);
	}
	
	public void cleanView(View... views){
		Util.cleanView(this.getActivity(),Arrays.asList(views));
	}
	
	protected void cleanView(View view){
		Util.cleanView(getActivity(), view);
	}
	
	public void cleanAndLockQuestion(int nQuestion){
		cleanAndLockQuestion(""+ nQuestion);
	}
	
	public void cleanAndLockQuestion(String nQuestion){
		for(View view : getViews(nQuestion)){
			cleanAndLockView(view);
		}
	}
	
	public void cleanAndLockQuestion(int start, int end){
		cleanQuestion(start, end);
		lockQuestion(start, end, true);
	}
	
	public void cleanAndLockView(View view){
		if(view == null) return;
		cleanView(view);
		lockView(view, true);
	}
	
	protected void cleanAndLockView(List<View> views){
		if(views == null) return;
		cleanView(views);
		lockView(views, true);
	}
	
	public void cleanAndLockView(View... views){
		if(views == null) return;
		cleanView(views);
		lockView(true, views);
	}
	
		
	public void lockQuestion(int start, int end){
		lockQuestion(start, end, true);
	}
	
	protected void lockQuestion(int nQuestion){
		lockQuestion(nQuestion, true);
	}
	
	protected void lockQuestion(String nQuestion){
		lockQuestion(nQuestion, true);
	}
	
	public void lockQuestion(int start, int end, boolean lock){
		for(int i=start; i<=end; i++){
			lockQuestion("" + i, lock);
		}
	}
	
	protected void lockQuestion(int nQuestion, boolean lock){
		lockQuestion("" + nQuestion, lock);
	}
	
	protected void lockQuestion(String nQuestion, boolean lock){
		for(View view : getViews(nQuestion)){
			lockView(view, lock);
		}
	}
	
	
	protected void lockViewNext(View view){
		lockView(nextView(view), true);
	}
	
	protected void lockViewNext(View view, boolean lock){
		lockView(nextView(view), lock);
	}
	
	protected void lockViewNext(View view, int nexts){
		lockViewNext(view, nexts, true);
	}
	
	protected void lockViewNext(View view, int nexts, boolean lock){
		
		if(nexts<=0) return;
		
		for(int i=1; i<=nexts; i++){
			view = nextView(view);
			lockView(view, lock);
		}
		
	}
	
	protected void lockView(List<View> views){
		
		if(views == null) return;
		
		for(View view:views){
			lockView(view, true);
		}
	}
	
	public void lockView(List<View> views, boolean lock){
		
		if(views == null) return;
		
		for(View view:views){
			lockView(view, lock);
		}
	}
	
	protected void lockView(View view){
		Util.lockView(this.getActivity(),view, true);
	}
	
	protected void lockView(View view, boolean lock) {
		Util.lockView(this.getActivity(), view, lock);
	}
	
	public void lockView(boolean lock, View... views){
		Util.lockView(this.getActivity(),lock,views);		
	} 
	
	
	/**
	 * Obtiene el siguiente view segun la numeracion de las anotaciones
	 * 
	 * @param view
	 * @return
	 */
	protected View nextView(View view){
		return nextView(getContainerContext(), view);
	}
	
	protected <T>View nextView(T container, View view){
		Integer pos = fieldsPosition.get(view); 
		return fieldsOrdered[++pos];
	}
	
	
	protected boolean isClean(EditText editText){
		return esVacio(editText.getText().toString());
	}
	
	protected boolean isClean(RadioGroupField radiogroup){
		return radiogroup.getTagSelected() == null;
	}
	
	protected boolean isClean(CheckBoxField checkbox){
		return !checkbox.isChecked();
	}
	
	protected boolean isClean(SpinnerField spinner){
		return spinner.getSelectedItemKey() == null;
	}
	
	protected boolean isCleanView(View view){
		if (view instanceof EditText) return isClean((EditText)view);
		else if (view instanceof IntegerField) return isClean((EditText)view);
		else if (view instanceof SpinnerField) return isClean((SpinnerField)view);
		else if (view instanceof CheckBoxField) return isClean((CheckBoxField)view);
		else if (view instanceof RadioGroupField) return isClean((RadioGroupField)view);
		else if (view == null) return true;
		return false;
	}
	
	protected boolean isCleanView(List<View> views){
		for(View view : views){
			if(!isCleanView(view)) return false;
		}
		return true;
	}
	
	protected boolean isCleanView(View... views){
		for(View view : views){
			if(!isCleanView(view)) return false;
		}
		return true;
	}
	
	protected boolean isCleanQuestion(String nQuestion){
		boolean isClean = true;
		
		for(View view : getViews(nQuestion)){
			isClean = isClean && isCleanView(view);
		}
		
		return isClean;
	}
	
	public boolean isCleanQuestion(int nQuestion){
		boolean isClean = true;
		
		for(View view : getViews(""+nQuestion)){
			isClean = isClean && isCleanView(view);
		}
		
		return isClean;
	}
	
	public boolean isCleanQuestion(int start, int end){
		for(int i=start; i<=end; i++){
			for(View view : getViews(""+i)){
				if(!isCleanView(view)) return false;
			}
		}
		return true;
	}
	
	public View getCleanView(int nQuestion){
		
		for(View view : getViews(""+nQuestion)){
			if(isCleanView(view)) return view;
		}
		
		return null;
	}
	
	public View getCleanView(int start, int end){
		for(int i=start; i<=end; i++){
			for(View view : getViews(""+i)){
				if(isCleanView(view)) return view;
			}
		}
		return null;
	}
	
	public View getNotCleanView(int nQuestion){
		
		for(View view : getViews(""+nQuestion)){
			if(!isCleanView(view)) return view;
		}
		
		return null;
	}
	
	public View getNotCleanView(int start, int end){
		for(int i=start; i<=end; i++){
			for(View view : getViews(""+i)){
				if(!isCleanView(view)) return view;
			}
		}
		return null;
	}
	
	
	private Bitmap printScreen(View layout, int w, int h){
		Bitmap bitmap = Bitmap.createBitmap(layout.getWidth(), layout.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		layout.draw(canvas);
		bitmap = Bitmap.createScaledBitmap(bitmap, 1000, 2000, true);
		
		Bitmap _bitmap = Bitmap.createBitmap(bitmap, 1000-w, 2000-h, w, h);
		bitmap.recycle();
		
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
		//_bitmap.compress(Bitmap.CompressFormat.JPEG, 1, out);
		//_bitmap.recycle();
		//Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
		
		return _bitmap;
	}
	
	protected void printScreen(View layout, String name, int x, int y){
		PrintScreen ps = new PrintScreen(layout, name, x, y, -1, -1);
		ps.execute();
	}
	
	protected void printScreen(View layout, String name, int x, int y, int w, int h){
		PrintScreen ps = new PrintScreen(layout, name, x, y, w, h);
		ps.execute();
	}
	
	private static int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	
	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	
	    return inSampleSize;
	}
	
	private static Bitmap decodeBitmap(Bitmap bitmap, int reqWidth, int reqHeight) {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
		bitmap.recycle();
		
	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    
	    BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()), new Rect(0, 0, 0, 0), options);
	    
	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    
	    return BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()), null, options);
	}
	
	
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected List<EditText> getEditTexts(int start, int end){
		return getEditTexts(this, start, end);
	}
	
	protected <T>List<EditText> getEditTexts(T container, int start, int end){
		List<EditText> lista = new ArrayList<EditText>();
		
		for(String nombre : getFieldMatches(start, end, "txt[a-zA-Z0-9]+")){
			try {
				if(container.getClass().getDeclaredField(nombre).getType() != EditText.class) continue;
				EditText caja = (EditText)getClass().getDeclaredField(nombre).get(container);
				lista.add(caja);
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	protected List<EditText> getEditTexts(String nQuestion){
		return getEditTexts(this, nQuestion);
	}
	
	protected <T>List<EditText> getEditTexts(T container, String nQuestion){
		List<EditText> lista = new ArrayList<EditText>();
		for(String nombre : getFieldMatches(nQuestion, "txtC1P", container)){
			try {
				View caja = null;
				if(container.getClass().getDeclaredField(nombre).getType() == EditText.class){
					lista.add((EditText)container.getClass().getDeclaredField(nombre).get(container));
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected List<CheckBoxField> getCheckBoxs(int start, int end){
		return getCheckBoxs(this, start, end);
	}
	
	protected <T>List<CheckBoxField> getCheckBoxs(T container, int start, int end){
		List<CheckBoxField> lista = new ArrayList<CheckBoxField>();
		
		for(String nombre : getFieldMatches(start, end, "chbC1P")){
			try {
				if(container.getClass().getDeclaredField(nombre).getType() != CheckBoxField.class) continue;
				CheckBoxField caja = (CheckBoxField)container.getClass().getDeclaredField(nombre).get(container);
				lista.add(caja);
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	protected List<CheckBoxField> getCheckBoxs(String nQuestion){
		return getCheckBoxs(getContainerContext(), nQuestion);
	}
	
	protected <T>List<CheckBoxField> getCheckBoxs(T container, String nQuestion){
		List<CheckBoxField> lista = new ArrayList<CheckBoxField>();
		Log.d(TAG, container.getClass().getName());
		for(String nombre : getFieldMatches(nQuestion, "chbC1P", container)){
			try {
				if(container.getClass().getDeclaredField(nombre).getType() == CheckBoxField.class){
					lista.add((CheckBoxField)container.getClass().getDeclaredField(nombre).get(container));
				}
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected List<RadioGroupField> getRadioGroups(int start, int end){
		List<RadioGroupField> lista = new ArrayList<RadioGroupField>();
		
		for(String nombre : getFieldMatches(start, end, "[a-zA-Z0-9_]+")){
			try {
				if(getClass().getDeclaredField(nombre).getType() != RadioGroupField.class) continue;
				RadioGroupField caja = (RadioGroupField)getClass().getDeclaredField(nombre).get(this);
				lista.add(caja);
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	protected List<RadioGroupOtherField> getRadioGroupsOthers(int start, int end){
		List<RadioGroupOtherField> lista = new ArrayList<RadioGroupOtherField>();
		
		for(String nombre : getFieldMatches(start, end, "[a-zA-Z0-9_]+")){
			try {
				if(getClass().getDeclaredField(nombre).getType() != RadioGroupOtherField.class) continue;
				RadioGroupOtherField caja = (RadioGroupOtherField)getClass().getDeclaredField(nombre).get(this);
				lista.add(caja);
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		return lista;
	}
	
	public List<View> getViews(int start, int end){
		List<View> ls = new ArrayList<View>();
		for( int i=start; i<=end; i++ ){
			ls.addAll(getViews(i));
		}
		return ls;
	}
	
	public List<View> getViews(int nQuestion){
		return getViews("" + nQuestion);
	}
	
	public List<View> getViews(String nQuestion){
		return getViews(getContainerContext(), nQuestion);
	}
	
	public <T>List<View> getViews(T container, String nQuestion){
		return getViews(container, nQuestion, null);
	}
	
	public <T>List<View> getViews(T container, String nQuestion, Class<?> componente){
		List<View> lista = new ArrayList<View>();
		
		for(String nombre : getFieldMatches(nQuestion, "[a-zA-Z0-9_]+", container)){
			try {
				View caja = null;
				Field campo = container.getClass().getDeclaredField(nombre);
				if(campo.getType() == EditText.class){
					caja = (EditText)campo.get(container);
				}
				else if(campo.getType() == IntegerField.class){
					caja = (IntegerField)campo.get(container);
				}
				else if(campo.getType() == DecimalField.class){
					caja = (DecimalField)campo.get(container);
				}
				else if(campo.getType() == TextField.class){
					caja = (TextField)campo.get(container);
				}
				else if(campo.getType() == SpinnerField.class){
					caja = (SpinnerField)campo.get(container);
				}
				else if(campo.getType() == Spinner.class){
					caja = (Spinner)campo.get(container);
				}
				else if(campo.getType() == CheckBoxField.class){
					caja = (CheckBoxField)campo.get(container);
				}
				else if(campo.getType() == RadioGroupField.class){
					caja = (RadioGroupField)campo.get(container);
				}
				else if(campo.getType() == RadioGroupOtherField.class){
					caja = (RadioGroupOtherField)campo.get(container);
				}
				else if(campo.getType() == DateTimeField.class){
					caja = (DateTimeField)campo.get(container);
				}
				else continue;
				
				if(componente==null || campo.getType()==componente)
					lista.add(caja);
			} catch (IllegalArgumentException e) {
				Log.d(TAG + " - IllegalArgumentException", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.d(TAG + " - IllegalAccessException", e.getMessage());
			} catch (NoSuchFieldException e) {
				Log.d(TAG + " - NoSuchFieldException", e.getMessage());
			}
		}
		if(lista.size() == 0) return null;
		//Log.d(TAG, "--------------************"+lista.size()+"**********----------");
		return lista;
	}
	
	public <T>View getView1(T container, String nView){
		View caja = null;
		
		try {
			if(container.getClass().getDeclaredField(nView).getType() == EditText.class){
				caja = (EditText)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == TextField.class){
				caja = (TextField)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == IntegerField.class){
				caja = (IntegerField)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == DecimalField.class){
				caja = (DecimalField)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == SpinnerField.class){
				caja = (SpinnerField)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == Spinner.class){
				caja = (Spinner)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == CheckBoxField.class){
				caja = (CheckBoxField)container.getClass().getDeclaredField(nView).get(container);
			}
			else if(container.getClass().getDeclaredField(nView).getType() == RadioGroupField.class){
				caja = (RadioGroupField)container.getClass().getDeclaredField(nView).get(container);
			}
				
		} catch (IllegalArgumentException e) {
			Log.d(TAG + " - IllegalArgumentException", e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG + " - IllegalAccessException", e.getMessage());
		} catch (NoSuchFieldException e) {
			Log.d(TAG + " - NoSuchFieldException", e.getMessage());
		}

		return caja;
	}
	
	/** 
	 * Obtiene una lista con los nombres de la preguntas desde <code>start<code> hasta <code>end<code>
	 * @param start Numero de la pregunta inicial 
	 * @param end Numero de la pregunta final
	 * @return
	 */
	private List<String> getFieldMatches(int start, int end, String prefix){
		return getFieldMatches(start, end, prefix, getContainerContext());
	}
	
	private <T>List<String> getFieldMatches(int start, int end, String prefix, T container){
		List<String> allMatches = new ArrayList<String>();
		for(int i=start; i<=end; i++){
			Matcher m = Pattern.compile(prefix+""+i+"[a-zA-Z0-9_]*;").matcher(getAllFieldsString(container));
			while (m.find()) {
				allMatches.add(m.group().replace(";", ""));
			}
		}
		
		return allMatches;
	}
	
	private List<String> getFieldMatches(String nro, String prefix){
		return getFieldMatches(nro, prefix, getContainerContext());
	}
	
	private <T>List<String> getFieldMatches(String nro, String prefix, T container){
		List<String> allMatches = new ArrayList<String>();
		Matcher m = Pattern.compile(prefix + nro + "[a-zA-Z0-9_]*;").matcher(getAllFieldsString(container));
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}
		
		return allMatches;
	}
	
	/**
	 * Construye una cadena conteniendo los nombres de las propiedades, separados por punto y coma(;)
	 * @return
	 */
	private String getAllFieldsString(){
		return getAllFieldsString(getContainerContext());
	}
	
	private <T>String getAllFieldsString(T container){
		Field[] campos = container.getClass().getDeclaredFields();
		String nameFields="";
		for(Field campo : campos){
			nameFields += campo.getName() + ";";
		}
		return nameFields;
	}
	
	protected boolean esVacio(String text){
		if (isNull(text)) return true;
		return text.replaceAll("\\s\\u0020 ", "").equals("");
	}
	
	/**
	 * 
	 * @param hash
	 * @param from
	 * @param obj
	 * @return
	 */
	private HashMap<View, HashMap<Integer,Object[]>> addOrInsertHashMap(HashMap<View, HashMap<Integer, Object[]>> hash, View from, Object[] obj){
 		HashMap<Integer, Object[]> value = hash.get(from);
 		if(value == null){
 			hash.put(from, Util.getHashMap(Integer.valueOf(0), obj));
 		}
 		else{
 			value.put(Integer.valueOf(value.size()), obj);
 			hash.put(from, value);
 		}
 		containerCallbackEvents.put(getContainerContext(), hash);
 		return hash;
 	}
	
	/**
	 * Tomado de una soluci&oacute;n planteada por Joachim Sauer en 
	 * <a href="http://stackoverflow.com/questions/80476/how-to-concatenate-two-arrays-in-java/784842#784842">stackoverflow</a> 
	 * 
	 * @param first
	 * @param rest
	 * @return
	 */
	/*public static <T> T[] concatAll(T[] first, T[]... rest) {
		int totalLength = first.length;
		for (T[] array : rest) {
			totalLength += array.length;
		}
		T[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (T[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}*/
	
	
	
	private List<NumberField> getCamposRangeados(Object container) {
		if (camposRangeados == null) {
			camposRangeados = new HashMap<Object, List<NumberField>>();
			camposRangeados.put(container, new ArrayList<NumberField>());
		}
		if (camposRangeados.get(container) == null) {
			camposRangeados.put(container, new ArrayList<NumberField>());
		}
		return camposRangeados.get(container);
	}
	
	private class PrintScreen extends AsyncTask<Void, Void, Bitmap>{
		
		private View layout;
		private int x;
		private int y;
		private int width;
		private int height;
		private String name;
		
		public PrintScreen(View view, String name, int x, int y, int width, int height) {
			this.layout = view;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.name = name;
		}
		
		@Override
		protected Bitmap doInBackground(Void... params) {
			//return decodeBitmap(printScreen(layout, 520, 850), 520, 850);
			Bitmap bitmap = Bitmap.createBitmap(layout.getWidth(), layout.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			layout.draw(canvas);
			//bitmap = Bitmap.createScaledBitmap(bitmap, 1000, 2000, true);
			//bitmap = Bitmap.createScaledBitmap(bitmap, Double.valueOf(bitmap.getWidth()*.6).intValue(), Double.valueOf(bitmap.getHeight()*.6).intValue(), true);
			
			Bitmap _bitmap;
			
			if(width==-1 || height==-1){
				_bitmap = Bitmap.createBitmap(bitmap, x, y, bitmap.getWidth()-x, bitmap.getHeight()-y);
			}
			else{
				_bitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
			}
			_bitmap = Bitmap.createScaledBitmap(_bitmap, Double.valueOf(_bitmap.getWidth()*.8).intValue(), Double.valueOf(_bitmap.getHeight()*.8).intValue(), true);
			
			bitmap.recycle();
			
			//Globales.getInstance().addBitmapToMemoryCache(question, decodeBitmap(_bitmap, width, height));
			//Globales.getInstance().setBitmapFromMemCache(question, decodeBitmap(_bitmap, width, height));
			
			/*Bitmap b = decodeBitmap(_bitmap, width, height);*/
			
			try {
				File imgDirectory = new File(Environment.getExternalStorageDirectory() + "/imgcenacom/");
				imgDirectory.mkdirs();
				
				File img = new File(imgDirectory, name + ".jpg");
				if(img.exists()) img.delete();
				img.createNewFile();
				
				FileOutputStream out = new FileOutputStream(img);
				/*b.compress(Bitmap.CompressFormat.JPEG, 90, out);*/
				_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//Globales.getInstance().setClipHelp(name, name + ".jpg");

			_bitmap.recycle();
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {}
		
		
	}

	public boolean isNull(Object o) {
		if (o != null) return false;
		return true;
	}
	
	public boolean esVacio(Integer i) {
		if(isNull(i)) return true;
		if(Util.esVacio(i)) return true;
		return false;
	}
	
	public boolean esVacio(BigDecimal bd) {
		if(isNull(bd)) return true;
		if(Util.esVacio(bd)) return true;
		return false;
	}
	
	public String getFechaActualToString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.format(fechaActual);
	}
	
	public Date getFechaStringToDate(String fechaString) {
		Date fecha = null;
		if("".equals(fechaString) || isNull(fechaString)) {
			return null;
		}
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			fecha = formatter.parse(fechaString);
		} catch (ParseException e) {
			Log.e(TAG, e.getMessage(), e);
		}
		return fecha;		
	}
	
	public BigDecimal obtenerBigDecimal(View vint,View vdec,int n){
		if(vint == null) return new BigDecimal(0);
		if(vdec == null) return new BigDecimal(0);
		
		BigDecimal valint= new BigDecimal(((EditText) vint).getText().toString().equals("") ? "0" :((EditText) vint).getText().toString());
		BigDecimal valdec=new BigDecimal(((EditText) vdec).getText().toString().equals("") ? "0" :((EditText) vdec).getText().toString());
		
		int size = ((EditText) vdec).getText().toString().trim().length();
		
		for(int i=1;i<n;i++){
			 if(size==i){valdec=valdec.multiply(BigDecimal.TEN.pow(n-i));}
		}
		
		BigDecimal superficie = (valdec.divide(BigDecimal.TEN.pow(n))).add(valint);
		
		return superficie;
	}
	
	public void setTextEditText(BigDecimal numdec,int n,View vint,View vdec){
		int scale = numdec.scale();
		int entero = numdec.intValue();
		BigDecimal decimal = numdec.subtract(new BigDecimal(entero));
		
		if(scale>n){
			decimal = decimal.setScale(n,BigDecimal.ROUND_HALF_UP);
			scale = decimal.scale();
		}
		
		for(int i=1;i<=n;i++){
			 if(scale==i){decimal=decimal.multiply(BigDecimal.TEN.pow(n));}
		}
		
		String valuezero = String.valueOf(decimal.intValue());
		String a ="00000";
		
		for(int i=1;i<n;i++){
			 if(valuezero.length()==i){valuezero = a.substring(0,n-i)+valuezero;}
		}
	
		((EditText) vint).setText(String.valueOf(entero));
		((EditText) vdec).setText(valuezero);
	}
	
	public void rellenarcerosder(int val,View... views){
		if(views == null) return;
		for(View view:views){
			rellenarder(view,val);
		}
	}
		
	protected void rellenarder(View view, int n){
		if(view == null) return;	
		if (view instanceof EditText) {
			String valuezero = ((EditText) view).getText().toString().trim();
			String a ="00000";
			
			for(int i=1;i<n;i++){
				 if(valuezero.length()==i){valuezero = valuezero+a.substring(0,n-i);}
			}
			
			((EditText) view).setText(valuezero);
		}
	}
	
	public boolean esDiferente(Integer valor, int valor2) {
		return Util.esDiferente(valor, valor2);
	}
	
	public boolean esDiferente(Integer valor, int... valores) {
		return Util.esDiferente(valor, valores);
	}
	
	public boolean esMayor(Integer valor, int valor2) {
		return Util.esMayor(valor, valor2);
	}
	
	public boolean esMenor(Integer valor, int valor2) {		
		return Util.esMenor(valor, valor2);
	}	
    
    protected void interceptarTouchListView(ListView... listViews) {
    	for (ListView lv : listViews) {
    		lv.setOnTouchListener(new ListViewOnTouchListener());
		}
    }
    	
	protected void turnWIFIOn() {
		changeWIFIState(true);
	}
	
	protected void turnWIFIOff() {
		changeWIFIState(false);
	}
	
	private void changeWIFIState(boolean status) {
		WifiManager wifiManager = (WifiManager)this.getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		wifiManager.setWifiEnabled(status);
	}
	
	protected abstract View createUI();
	protected abstract void buildFields() throws Exception;
	
	protected LinearLayout createQuestionSection(int question, View... components) {
		return this.createQuestionSection(question, Gravity.CENTER, components);
	}
	
	protected LinearLayout createQuestionSection(View... components) {
		return this.createQuestionSection(0, components);
	}
	
	protected LinearLayout createButtonSection(ButtonComponent... components) {
		return this.createQuestionSection(0, Gravity.CENTER,LinearLayout.HORIZONTAL, components);
	}
	
	protected ScrollView createFrameForm() {
//		FrameLayout contenedor = new FrameLayout(getActivity());
//		ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 
//				ViewGroup.LayoutParams.MATCH_PARENT);
//		contenedor.setLayoutParams(params);
//        LinearLayout form = new LinearLayout(getActivity());
//		form.setOrientation(LinearLayout.VERTICAL);
//		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
//		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
//				LinearLayout.LayoutParams.WRAP_CONTENT);	
//		form.setLayoutParams(llp);
//        contenedor.addView(form);
		ScrollView contenedor = new ScrollView(getActivity());
		FrameLayout form = new FrameLayout(getActivity());
//		ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 
//				ViewGroup.LayoutParams.MATCH_PARENT);
		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);	
		form.setLayoutParams(llp);
        contenedor.addView(form);
		return contenedor;
	}
	
	protected LinearLayout createDashboard() {
//		ScrollView contenedor = new ScrollView(getActivity());
        LinearLayout contenedor = new LinearLayout(getActivity());
		contenedor.setOrientation(LinearLayout.VERTICAL);
		contenedor.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout form = new LinearLayout(getActivity());
		form.setOrientation(LinearLayout.VERTICAL);
		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);	
		form.setLayoutParams(llp);
		contenedor.setGravity(Gravity.CENTER);
		contenedor.addView(form);
		return contenedor;
//        contenedor.addView(form);
//		return contenedor;
	}
	
	protected LinearLayout createDashboardSection(ButtonComponent... components) {
		return this.createDashboardSection(-1, components);
	}

	protected LinearLayout createDashboardSection(int backgroundImage, ButtonComponent... components) {
		LinearLayout ll = new LinearLayout(getActivity());
		ll.setOrientation(LinearLayout.VERTICAL);
		if (backgroundImage == -1) {
			ll.setBackgroundColor(getResources().getColor(R.color.WhiteSmoke));
		} else {
//			ll.setBackground(getResources().getDrawable(backgroundImage));
			ll.setBackgroundDrawable(getResources().getDrawable(backgroundImage));
		}
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		llp.setMargins(1,1,1,1);
//		llp.gravity = Gravity.CENTER;
		ll.setLayoutParams(llp);	
		ll.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);
        LinearLayout contenido = createDashboardSection(LinearLayout.HORIZONTAL, Gravity.CENTER);        
        for (View view : components) {
        	contenido.addView(view);
		}
        ll.addView(contenido);
		return ll;
	}
	
	protected ScrollView createForm() {
		ScrollView contenedor = new ScrollView(getActivity());
        LinearLayout form = new LinearLayout(getActivity());
		form.setOrientation(LinearLayout.VERTICAL);
		form.setBackgroundColor(getResources().getColor(COLOR_LINEA_SECCION_PREGUNTA));
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);	
		form.setLayoutParams(llp);
        contenedor.addView(form);
		return contenedor;
	}
	
	protected LinearLayout createQuestionSection(int question, int gravity, View... components) {		
		return this.createQuestionSection(question, gravity, LinearLayout.VERTICAL, components);
	}	

	protected LinearLayout createQuestionSection(int question, int gravity, int orientacion, View... components) {
		return this.createQuestionSection(question, gravity, orientacion, -1, components);
	}

	protected LinearLayout createQuestionSection(int question, int gravity, int orientacion, int backgroundImage, View... components) {
		LinearLayout ll = new LinearLayout(getActivity());
		ll.setOrientation(LinearLayout.VERTICAL);
		if (backgroundImage == -1) {
			ll.setBackgroundColor(getResources().getColor(R.color.WhiteSmoke));
		} else {
//			ll.setBackground(getResources().getDrawable(backgroundImage));
			ll.setBackgroundDrawable(getResources().getDrawable(backgroundImage));
		}
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		llp.setMargins(cellSpacing,cellSpacing,cellSpacing,cellSpacing);
		llp.gravity = Gravity.CENTER;
		ll.setLayoutParams(llp);
		
		if (question != 0) {
	        TextView tv = createQuestionLabel(question);
	        ll.addView(tv);	
		}        
        LinearLayout contenido = createSection(orientacion, gravity);        
        for (View view : components) {
        	contenido.addView(view);
		}
        ll.addView(contenido);
		return ll;
	}
	
	protected TextView createQuestionLabel(int question) {
		TextView tv = new TextView(getActivity());
		tv.setPadding(7, 10, 7, 0);
		tv.setTextSize(getTextQuestionSize());
        tv.setText(getResources().getString(question));
        tv.setTypeface(Typeface.DEFAULT_BOLD);
		return tv;
	}
	
	protected LinearLayout createSection(int orientacion, int gravity) {
		LayoutParams lp = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout ll = new LinearLayout(getActivity());
		ll.setOrientation(orientacion);
		lp.topMargin = getTopMargin();
		lp.bottomMargin = getBottomMargin();
		if (gravity != Gravity.CENTER) {
			lp.leftMargin = getLeftMargin();
			lp.rightMargin = getRightMargin();	
		}
		ll.setLayoutParams(lp);
		ll.setGravity(gravity);	
		return ll;
	}
	
	protected LinearLayout createDashboardSection(int orientacion, int gravity) {
		LayoutParams lp = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout ll = new LinearLayout(getActivity());
		ll.setOrientation(orientacion);
		lp.topMargin = 25;
		lp.bottomMargin = 25;
		if (gravity != Gravity.CENTER) {
			lp.leftMargin = 25;
			lp.rightMargin = 25;	
		}
		ll.setLayoutParams(lp);
		ll.setGravity(gravity);	
		return ll;
	}
	
	protected LinearLayout createLy(int orientation, int gravity, View...views){
		return createLy(orientation, gravity, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, View...views){
		return createLy(orientation, gravity, topMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin, View...views){
		return createLy(orientation, gravity, topMargin, bottomMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin,
			int leftMargin, View...views){
		return createLy(orientation, gravity, topMargin, bottomMargin, leftMargin, 0, views);
	}
	
	protected LinearLayout createLy(int orientation, int gravity, int topMargin, int bottomMargin,
			int leftMargin, int rightMargin, View...views){
		LinearLayout ly = new LinearLayout(getActivity());
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.MATCH_PARENT);
		lp.topMargin = topMargin;
		lp.bottomMargin = bottomMargin;
		lp.leftMargin = leftMargin;
		lp.rightMargin = rightMargin;
		ly.setLayoutParams(lp);
		ly.setOrientation(orientation);
		ly.setGravity(gravity);
		for(View view : views)
			ly.addView(view);
		return ly;
	}
	
	protected void replaceText(TextView view, List<HashMap<String, String>> replaces){
		if(view==null || !(view instanceof TextView)) return;
		String preg = view.getText().toString();
        if(replaces.size()>0){
    		for(HashMap<String, String> hm:replaces){
    			for(Map.Entry<String, String> t:hm.entrySet()){
    				preg=preg.replace(t.getKey(), t.getValue());
    			}
    		}
    	}
        view.setText(preg);
	}
	
	public void replaceText(List<HashMap<String, String>> replaces, LinearLayout... layouts){
		if(layouts!=null){
			for(LinearLayout layout:layouts)
				replaceText(layout, replaces);
		}
	}
	
	protected void replaceText(LinearLayout layout, List<HashMap<String, String>> replaces){
		replaceText((TextView)layout.getChildAt(0), replaces);
	}
	
	public int getCellSpacing() {
		return cellSpacing;
	}
	
	public void setCellSpacing(int cellSpacing) {
		this.cellSpacing = cellSpacing;
	}
	
	public int getTopMargin() {
		return topMargin;
	}
	
	public void setTopMargin(int topMargin) {
		this.topMargin = topMargin;
	}
	
	public int getBottomMargin() {
		return bottomMargin;
	}
	
	public void setBottomMargin(int bottomMargin) {
		this.bottomMargin = bottomMargin;
	}
	
	public int getLeftMargin() {
		return leftMargin;
	}
	
	public void setLeftMargin(int leftMargin) {
		this.leftMargin = leftMargin;
	}
	
	public int getRightMargin() {
		return rightMargin;
	}
	
	public void setRightMargin(int rightMargin) {
		this.rightMargin = rightMargin;
	}
	
	public int getTextQuestionSize() {
		return textQuestionSize;
	}
	
	public void setTextQuestionSize(int textQuestionSize) {
		this.textQuestionSize = textQuestionSize;
	}
	
	public abstract boolean grabar();
	public abstract void cargarDatos();
	public abstract Integer grabadoParcial();
	
	
	public void abrirObservaciones() {
		
	}
	
	//Ramon
	//Retorna si se debe ejecutar cargarDatos()
	public boolean preCargarDatosAlAdelantar() throws Exception {
		return true;
	}
	
	//Ramon
	//Retorna si se debe ejecutar cargarDatos()
	public boolean preCargarDatosAlRetroceder() throws Exception {
		return true;
	}
	
	//Ramon
	public Integer getPaginaAnterior() throws Exception {
		return -1;
	}
	
	//Ramon
	public Integer getPaginaSiguiente() throws Exception {
		return -1;
	}
	
	public Integer getSalto() {
		return -1;
	}
	
	public boolean getSaltoNavegation() {
		return true;
	}
	
	public void invoke(OPCION opcion) {
		
	}
	
	private void jumpFromDateTimeField(final Integer fromPosition, DateTimeField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}				
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}	
	
	private void jumpFromTextBoxField(final Integer fromPosition, EditText from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {
			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}
				if( KeyEvent.ACTION_DOWN==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER){
					((TextBoxField)from).callback();
					if (!((TextBoxField)from).esCoincidente()) {
						ToastMessage.msgBox(getActivity(), "ERROR: Valores no coinciden.", 
								ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
						from.requestFocus();
						return true;
					} 
					if (!((TextBoxField)from).esDigitacionFinalizada()) {
						from.requestFocus();
						return true;
					} else {				
//						if (to != null) {
//							if (to.isEnabled() && to.getVisibility() == View.VISIBLE) {
//			            		from.setNextFocusDownId(to.getId());
//								to.requestFocus();
//							} else {
//								setNextFocusRequest(fromPosition, from);
//							}
//						} else {
//							from.clearFocus();
//						}
					}
					if (from instanceof NumberField) {
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((NumberField)from).getCallbacks();
				            if (callbacks != null) {
					            if (callbacks.size() == 0) {
				            		setNextFocusRequest(fromPosition, from);
				            		return true;
								}
				            	for (String c : callbacks) {
					            	callback = c;
					            	if (c.equals("verificarRangos")) {
					            		method = ((NumberField)from).getClass().getDeclaredMethod(c);
									} else {
										try {
							            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
											if (method != null) {
												method.invoke(containerContext, from);
											} else {
												
											}	
						            	} catch (NoSuchMethodException nsme) {
								            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
								            try {
									            method = containerContext.getClass().getDeclaredMethod(c);
												if (method != null) {
													method.invoke(containerContext);
												} else {
													continue;
												}
								            } catch (NoSuchMethodException nsme2) {
								            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
								            	throw new NoSuchMethodException("No existe el metodo "+callback+" en "+this.getClass());
								            }
								        }
									}
					            	if (method == null) {
										continue;
									}
					            	if (c.equals("verificarRangos")) {
					            		method.invoke((NumberField)from);
									} else {
										method.invoke(containerContext);
									}
						            if (c.equals("verificarRangos")) {
						            	if (((NumberField)from).isRangoOk()) {
						            		if (to != null) {
						            			if (to.isEnabled()) {
								            		from.setNextFocusDownId(to.getId());
													to.requestFocus();
												} else {
													setNextFocusRequest(fromPosition, from);
												}
											} else {
												from.clearFocus();
											}
										} else {
											ToastMessage.msgBox(getActivity(), ((NumberField)from).getMensajeRango(), 
													ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
											from.setNextFocusDownId(from.getId());
											from.requestFocus();
										}
						            }
								}
				            	
							} else {
								if (to != null) {
									if (to.isEnabled()) {
					            		from.setNextFocusDownId(to.getId());
										to.requestFocus();
									} else {
										setNextFocusRequest(fromPosition, from);
									}
								} else {
									from.clearFocus();
								}
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					} else if (from instanceof TextBoxField) {
						setNextFocusRequest(fromPosition, from);
						String callback = "";
						try {
				            Method method;
				            List<String> callbacks = ((TextBoxField)from).getCallbacks();
				            if (callbacks != null) {
					            for (String c : callbacks) {
					            	callback = c;
					            	try {
						            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
										if (method != null) {
											method.invoke(containerContext, from);
										} else {
											
										}	
					            	} catch (NoSuchMethodException nsme) {
							            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
							            try {
								            method = containerContext.getClass().getDeclaredMethod(c);
											if (method != null) {
												method.invoke(containerContext);
											} else {
												continue;
											}
							            } catch (NoSuchMethodException nsme2) {
							            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
							            	throw new NoSuchMethodException("No existe el metodo "+callback+" en "+this.getClass());
							            }
							        }													            
								}	
							}
				        } catch (NoSuchMethodException nsme) {
				            throw new RuntimeException(
				                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
				        } catch (SecurityException se) {
				        	Log.e("FragmentForm", se.toString(), se);
				        } catch (IllegalArgumentException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (IllegalAccessException iae) {
				        	Log.e("FragmentForm", iae.toString(), iae);
				        } catch (InvocationTargetException ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        } catch (Exception ite) {
				        	Log.e("FragmentForm", ite.toString(), ite);
				        }
					}					
					return true;
				} else if( KeyEvent.ACTION_UP==event.getAction() && keyCode == KeyEvent.KEYCODE_ENTER) {
//					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.ACTION_UP to: " + to.getClass().getSimpleName()+ "["+to.hashCode()+"]");
					if (from instanceof TextBoxField) {
						return true;	
					}
					return false;
				} else if( event.getAction()==-100 && keyCode == KeyEvent.KEYCODE_ENTER) {
					from.setNextFocusDownId(from.getId());
					Log.e(from.getClass().getSimpleName() + "["+from.hashCode()+"].onKey", "KeyEvent.-100");
					return false;
				}
				return false;
			}

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}	

	private void jumpFromRadioGroupField(final Integer fromPosition, RadioGroupOtherField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((RadioGroupOtherField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        } 
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpFromCheckBoxField(final Integer fromPosition, CheckBoxField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return true; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((CheckBoxField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        }													            
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}
	
	private void jumpFromSpinnerField(final Integer fromPosition, SpinnerField from, View to, View next, Object dataSource, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals){
		CallbackEvent callbackEvent = new CallbackEvent() {			
			@Override
			public boolean onKey(View from, View to, View next, Object dataSource, int keyCode, KeyEvent event, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onItemSelected(AdapterView<?> arg0, View from, View to, View next, Object dataSource, int arg2, long arg3, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {
				if (esCargaInicial) {
					return true;
				}
				if (!from.isEnabled()) {
					return true;
				}
				String callback = "";
				try {
		            Method method;
		            List<String> callbacks = ((SpinnerField)from).getCallbacks();
		            if (callbacks != null) {
		            	if (callbacks.size() == 0) {
		            		setNextFocusRequest(fromPosition, from);
		            		return true;
						}
			            for (String c : callbacks) {
			            	callback = c;
//			            	Log.e(getClass().getSimpleName(),"Ejecutando: "+callback+" en: "+this.getClass());
			            	try {
				            	method = containerContext.getClass().getDeclaredMethod(c, FieldComponent.class);
								if (method != null) {
									method.invoke(containerContext, from);
								} else {
									
								}	
			            	} catch (NoSuchMethodException nsme) {
					            Log.e(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            try {
						            method = containerContext.getClass().getDeclaredMethod(c);
									if (method != null) {
										method.invoke(containerContext);
									} else {
										continue;
									}
					            } catch (NoSuchMethodException nsme2) {
					            	Log.d(getClass().getSimpleName(),"No existe el metodo "+callback+" con parametros en "+this.getClass());
					            	throw new Exception("No existe el metodo "+callback+" en "+this.getClass());
					            }
					        }										            
						}	
					} else {
						if (to != null) {
							if (to.isEnabled()) {
			            		from.setNextFocusDownId(to.getId());
								to.requestFocus();
							} else {
								setNextFocusRequest(fromPosition, from);
							}	
						} else {
							from.clearFocus();
						}
					}
		        } catch (NoSuchMethodException nsme) {
		            throw new RuntimeException(
		                    "No existe el metodo "+callback+"  en "+this.getClass(),nsme);
		        } catch (SecurityException se) {
		        	Log.e(TAG, se.toString(), se);
		        } catch (IllegalArgumentException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (IllegalAccessException iae) {
		        	Log.e(TAG, iae.toString(), iae);
		        } catch (InvocationTargetException ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        } catch (Exception ite) {
		        	Log.e(TAG, ite.toString(), ite);
		        }
				return true;
			}

			@Override
			public boolean onCheckedChanged(RadioGroup from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) {return false;}
			
			@Override
			public boolean onCheckedChanged(RadioGroupOtherField from, View to, View next, Object dataSource, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public boolean onCheckedChanged(CompoundButton from, View to, View next, Object dataSource, boolean isChecked, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals) { return false; }

			@Override
			public void onFocusChange(View v, boolean hasFocus) {}
		};
		Object[] obj = new Object[]{callbackEvent, to, next, dataSource, values, cancel, allEquals};
		callbackEvents = addOrInsertHashMap(callbackEvents, from, obj);
	}

	protected void setNextFocusRequest(Integer fromPosition, View from) {
		for (int i = fromPosition + 1; i <= fieldsOrdered.length-1; i++) {
			if (fieldsOrdered[i] == null) {
				continue;
			}
			if (fieldsOrdered[i].isEnabled() && fieldsOrdered[i].isFocusable() && fieldsOrdered[i].isFocusableInTouchMode()
					&& fieldsOrdered[i].getVisibility() == View.VISIBLE) {
				from.setNextFocusDownId(fieldsOrdered[i].getId());
				fieldsOrdered[i].requestFocus();
				return;
			}
		}
		from.clearFocus();
	}

	public View[] getFieldsOrdered() {
		return fieldsOrdered;
	}
	
	public List<TextField> getTextFields() {
		List<TextField> texts = new ArrayList<TextField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof TextField) {
				texts.add((TextField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<IntegerField> getIntegerFields() {
		List<IntegerField> texts = new ArrayList<IntegerField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof IntegerField) {
				texts.add((IntegerField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<DecimalField> getDecimalFields() {
		List<DecimalField> texts = new ArrayList<DecimalField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof DecimalField) {
				texts.add((DecimalField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<CheckBoxField> getCheckBoxFields() {
		List<CheckBoxField> texts = new ArrayList<CheckBoxField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof CheckBoxField) {
				texts.add((CheckBoxField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<RadioGroupOtherField> getRadioGroupOtherFields() {
		List<RadioGroupOtherField> texts = new ArrayList<RadioGroupOtherField>();
		for (int i = 0; i < fieldsOrdered.length; i++) {
			if (fieldsOrdered[i] instanceof RadioGroupOtherField) {
				texts.add((RadioGroupOtherField)fieldsOrdered[i]);
			}
		}
		return texts;
	}
	
	public List<RadioGroupOtherField> getRadioGroupOtherFields(String nQuestion) {
		List<RadioGroupOtherField> texts = new ArrayList<RadioGroupOtherField>();
		List<View> lst = getViews(getContainerContext(), nQuestion, RadioGroupOtherField.class);
		for (int i = 0; i < lst.size(); i++) {
			texts.add((RadioGroupOtherField)lst.get(i));
		}
		return texts;
	}
	
	public View getView(String name) {
		View v = null;
		Field field;
		try {
			field = getContainerContext().getClass().getDeclaredField(name);
			v = (View) field.get(containerContext);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}				
		return v;
	}
	
	public List<View> getViews(Integer seccion, Integer subseccion, Integer questionNumber) {
		List<View> vs = new ArrayList<View>();
		List<String> viewsName = getViewsNameMatches(seccion, subseccion, questionNumber);
		if (viewsName == null) {
			return vs;
		}
		View v = null;
		for (String string : viewsName) {
			v = getView(string);
			if (v != null) {
				vs.add(v);
			}
		}
		return vs;
	}
	
	public List<String> getViewsNameMatches(Integer seccion, Integer subseccion, Integer questionNumber) {
		List<String> allMatches = new ArrayList<String>();
		String allFieldsString = getAllFieldsString();
		String tmp = Entity.PATRON_VARIABLE.getPatron().replace("$", 
				Util.completarCadena(questionNumber.toString(), "0", Entity.PATRON_VARIABLE.getLongitudPregunta(), 
				COMPLETAR.IZQUIERDA));
		tmp = tmp.replace("%", seccion==-1?"":Util.completarCadena(seccion.toString(), "0", 
				Entity.PATRON_VARIABLE.getLongitudSeccion(), COMPLETAR.IZQUIERDA));
		tmp = tmp.replace("&", subseccion==-1?"":Util.completarCadena(subseccion.toString(), "0", 
				Entity.PATRON_VARIABLE.getLongitudSubSeccion(), COMPLETAR.IZQUIERDA));
		tmp = tmp.replace("#", "");
		tmp = tmp.substring(0, 1).toUpperCase() + tmp.substring(1); 
		String txt = "txt" + tmp;
		String spn = "spn" + tmp;
		String chb = "chb" + tmp;
		String rg = "rg" + tmp;
		Matcher m = Pattern.compile(txt).matcher(allFieldsString);
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}	
		m = Pattern.compile(spn).matcher(allFieldsString);
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}	
		m = Pattern.compile(chb).matcher(allFieldsString);
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}	
		m = Pattern.compile(rg).matcher(allFieldsString);
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}	
		return allMatches;
	}
	
	public RadioGroupOtherField getRadioGroupOtherField(String name) {
		RadioGroupOtherField rg = null;
		View v = getView(name);
		if (v != null) {
			try {
				rg = (RadioGroupOtherField) v;
			} catch (ClassCastException e) {				
			}
		}
		return rg;
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		display.getSize(SIZE_DISPLAY);
		super.onConfigurationChanged(newConfig);
	}
	
	public IntegerField getIntegerField(String name) {
		IntegerField inf = null;
		View v = getView(name);
		if (v != null) {
			try {
				inf = (IntegerField) v;
			} catch (ClassCastException e) {				
			}
		}
		return inf;
	}
	
	public void lockQuestion(Integer seccion, Integer subseccion, Integer preguntaIni, Integer preguntaFin) {
		this.lockQuestion(true, seccion, subseccion, preguntaIni, preguntaFin);
	}
	
	public void lockQuestion(boolean lock, Integer seccion, Integer subseccion, Integer preguntaIni, Integer preguntaFin) {
		for (int i = preguntaIni; i <= preguntaFin; i++) {
			List<View> views = getViews(seccion, subseccion, i);
			Util.lockView(getActivity(), lock, views);
		}
	}

	public void cleanQuestion(Integer seccion, Integer subseccion, Integer preguntaIni, Integer preguntaFin) {
		for (int i = preguntaIni; i <= preguntaFin; i++) {
			List<View> views = getViews(seccion, subseccion, i);
			Util.cleanView(getActivity(), views);
		}
	}
	
	public void cleanAndLockQuestion(Integer seccion, Integer subseccion, Integer preguntaIni, Integer preguntaFin) {
		this.lockQuestion(true, seccion, subseccion, preguntaIni, preguntaFin);
		this.cleanQuestion(seccion, subseccion, preguntaIni, preguntaFin);
	}
	
	public List<String> getListFields(Object ui) {
		return getListFields(ui, null);
	}
	
	public List<String> getListFields(Object ui, String[] others) {
		return getListFields(ui, "", others);
	}
	
	public List<String> getListFields(String[] others) {
		return getFieldMatches(getContainerContext(), "", "[a-zA-Z]+", others);
	}
	
	public <T>List<String> getListFields(T container, String nro, String[] others){
		return getListFields(container, nro, "[a-zA-Z]+", others);
	}
	
	public <T>List<String> getListFields(T container, String nro, String prefix, String[] others){
		return getFieldMatches(container, nro, prefix, others);
	}
	
	public <T>List<String> getFieldMatches(T container, String nro, String prefix, String[] others){
		List<String> allMatches = new ArrayList<String>();
		Matcher m = Pattern.compile(prefix + nro + "[a-zA-Z0-9_]*;").matcher(getAllFieldsString(container));
		while (m.find()) {
			allMatches.add(m.group().replace(";", ""));
		}
		
		if(others!=null){
			for(String other:others)
				allMatches.add(other);
		}
		
		return allMatches;
	}
	
	public void clearSpinner(SpinnerField... views){
//		List<Entity> entity = new ArrayList<Entity>();
//		entity.add(null);     
//        EntitySpinnerAdapter<Entity> adapter = new EntitySpinnerAdapter<Entity>(
//        		getActivity(), android.R.layout.simple_spinner_item, entity);
//        List<Object> keysAdapter = new ArrayList<Object>();
//		keysAdapter.add(null);
		for(SpinnerField spn : views){
			List<Entity> entity = new ArrayList<Entity>();
			entity.add(spn.gettitle());     
	        EntitySpinnerAdapter<Entity> adapter = new EntitySpinnerAdapter<Entity>(
	        		getActivity(), android.R.layout.simple_spinner_item, entity);
	        List<Object> keysAdapter = new ArrayList<Object>();
			keysAdapter.add(null);
			spn.setAdapterWithKey(adapter, keysAdapter);
		}
	}
	
	
	public void onNoNextFragment() {
		
	}
	
	public final boolean isCargaInicial() {
		return esCargaInicial;
	}
}