package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.CallbackEvent;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

public class DateTimeField extends TextBoxField implements FieldComponent {

	private Date componentValue;
	CallbackEvent callbackEvent;
	private String formato;
	
	private DatePickerComponent datePiker;
	private TimePickerComponent timePiker;
	private SimpleDateFormat dateFormat, sdf;
	public static enum TIPO_DIALOGO {HORA, FECHA};
	public static enum SHOW_HIDE {CALENDAR, DAY, MONTH, YEAR, DAY_MONTH, DAY_YEAR, MONTH_YEAR, ALL};
	private TIPO_DIALOGO tipo;
	private AlertDialog widget;
	public SHOW_HIDE tipoShow;
	private boolean isUpdated;
	private boolean isEnabled;
	private boolean isOmision;
	private boolean isCurDate;
	public static int DEFAULT = 0;
	public static int MODEL1 = 1;
	public static int MODEL2 = 2;
	public static int MODEL3 = 3;
	public static int THEME = DEFAULT;
	private boolean comodin, dateOrhour;
	private EditText day, month, year;
	private FragmentForm formParent;
	private DialogFragmentComponent dialogParent;
	private ChangeValueListener listener;
	private OnFocusChangeListener onFocuslistener;
	private OnTouchListener onTouchListenerAlterno;
	
	public DateTimeField(final Context context) {
		this(context, TIPO_DIALOGO.FECHA);
	}
	public DateTimeField(final Context context, TIPO_DIALOGO tipo) {
		this(context, tipo, tipo==TIPO_DIALOGO.FECHA?"dd/MM/yyyy":
			(tipo==TIPO_DIALOGO.HORA?"HH:mm":"dd/MM/yyyy HH:mm"));
	}
	public DateTimeField(final Context context, TIPO_DIALOGO tipo, String format) {
		this(context, tipo, format, false);
	}
	public DateTimeField(final Context context, TIPO_DIALOGO tipo, boolean isCurDate) {
		this(context, tipo, tipo==TIPO_DIALOGO.FECHA?"dd/MM/yyyy":
			(tipo==TIPO_DIALOGO.HORA?"HH:mm":"dd/MM/yyyy HH:mm"), isCurDate);
	}
	public DateTimeField(final Context context, TIPO_DIALOGO tipo, String format, boolean isCurDate) {
		super(context);
		this.formato = format;
//		this.setInputType(getInputType(INPUT_TYPE.TEXTO));
		this.setInputType(EditorInfo.TYPE_NULL);
		this.setGravity(Gravity.CENTER);
		this.setPadding(4, 0, 7, 0);
		this.setFocusable(false);
		this.dateFormat = new SimpleDateFormat(format);
		this.sdf = new SimpleDateFormat();
		this.tipoShow = SHOW_HIDE.ALL;
		this.LoadComponent(tipo);
		this.isCurDate = isCurDate;
		this.isEnabled = true;
		this.comodin = false;
		this.dateOrhour = false;
		this.tipo = tipo;
		this.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		this.onTouchListenerAlterno = null;
		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(isUpdated = isEnabled) {
					widget.show();
					isOmision = false;
				}
				
				if(onTouchListenerAlterno!=null)
					return onTouchListenerAlterno.onTouch(v, event);
				
				return false;
			}
		});
	}
	
		
	public void setOnTouchListenerAlterno(OnTouchListener onTouchListenerAlterno) {
		this.onTouchListenerAlterno = onTouchListenerAlterno;
	}
	
	public ChangeValueListener getChangeValueListener() {
		return listener;
	}
	
	public void setChangeValueListener(ChangeValueListener listener) {
		this.listener = listener;
	}
	
	private void LoadComponent(TIPO_DIALOGO tipo) {
		switch (tipo) {
			case FECHA: widget = datePiker = new DatePickerComponent(this.getContext(), THEME, this); break;
			case HORA:	widget = timePiker = new TimePickerComponent(this.getContext(), THEME, this); break;
			default: break;
		}
	}
	
	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}
	
	public boolean isUpdated() {
		return this.isUpdated;
	}
	
	public TIPO_DIALOGO getType(){
		return this.tipo;
	}

	@Override
	public void setValue(Object aValue) {
		if (aValue == null) {
			componentValue = null;
			this.setText("");
			setDateEditText();
			return;
		}
		if (!(aValue instanceof Date)) {
			return;
		}				
		componentValue=(Date)aValue;
		isOmision(componentValue);
		String result = getTextShow(aValue);
		result = isOmision?result.replaceAll("[0-9]", "9"):result;
		setDateEditText();
		this.setText(result);
	}

	public Object getValueDefault(Object aValue) {
		if(this.isCurDate){
			switch (tipo) {
				case FECHA:	return Util.getFecha(datePiker.getcYear(), datePiker.getcMonth()+1, datePiker.getcDay());
				case HORA: return Util.getHora(timePiker.getcHour(), timePiker.getcMinute(), timePiker.getcSegundo());
			}	
		}
		return aValue;
	}
	@Override
	public void setEnabled(boolean enabled) {
		this.isEnabled=enabled;
		super.setEnabled(enabled);
	}
	
	@Override
	public void setFocusable(boolean focusable) {
		this.setCursorVisible(false);
	}

	@Override
	public void setFocusableInTouchMode(boolean focusableInTouchMode) {
		this.setCursorVisible(false);
	}
	
	private void isOmision(Date aValue){
		if(tipo == TIPO_DIALOGO.FECHA) isOmision = Util.getFecha(9999, 99, 99).equals(aValue);
		else if(tipo == TIPO_DIALOGO.HORA) isOmision = Util.getHora(99, 99, 99).equals(aValue);
	}
	
	private String getTextShow(Object aValue) {
		switch (tipoShow) {
			case ALL:CALENDAR: return dateFormat.format((Date)aValue); 
			case DAY_MONTH: return String.valueOf(getAnio());
			case DAY_YEAR: return String.valueOf(getMes());
			case MONTH_YEAR: return String.valueOf(getDia());
			case DAY: return String.valueOf(getMes_Anio());
			case MONTH: return String.valueOf(getDia_Anio());
			case YEAR: return String.valueOf(getDia_Mes());
			default:return dateFormat.format((Date)aValue);
		}
	}
	
	private String setFormato(SHOW_HIDE tipoShow) {
		switch (tipoShow) {
			case ALL:CALENDAR: return "dd/MM/yyyy"; 
			case DAY_MONTH: return "yyyy";
			case DAY_YEAR: return "MM";
			case MONTH_YEAR: return "dd";
			case DAY: return "MM/yyyy";
			case MONTH: return "dd/yyyy";
			case YEAR: return "dd/MM";
			default:return "dd/MM/yyyy";
		}
	}

	@Override
	public Object getValue() {
		return componentValue;
	}
	
	public String getStringValue() {
		isOmision(componentValue);
		String result = getTextShow(componentValue);
		result = isOmision?result.replaceAll("[0-9]", "9"):result;
		return result; //dateFormat.format(componentValue);
	}
	
	public FragmentForm getFormParent() {
		return formParent;
	}
	public void setFormParent(FragmentForm formParent) {
		this.formParent = formParent;
	}
	public DialogFragmentComponent getDialogParent() {
		return dialogParent;
	}
	public void setDialogParent(DialogFragmentComponent dialogParent) {
		this.dialogParent = dialogParent;
	}
	public String getFormato() {
		return formato;
	}

	public Integer getAnio() {
		return getAnio("yyyy");
	}
	public Integer getAnio(String format) {
		if (componentValue == null) return null;
		if (isOmision) return 9999;
		sdf.applyLocalizedPattern(format);
		return Integer.valueOf(sdf.format(componentValue));
	}
	
	public Integer getMes() {
		if (componentValue == null) return null;
		if (isOmision) return 99;
		sdf.applyLocalizedPattern("MM");
		return Integer.valueOf(sdf.format(componentValue));
	}
	
	public Integer getDia() {
		if (componentValue == null) return null;
		if (isOmision) return 99;
		sdf.applyLocalizedPattern("dd");
		return Integer.valueOf(sdf.format(componentValue));
	}
	
	public String getDia_Mes() {
		if (componentValue == null)	return null;
		if (isOmision) return "99/99";
		sdf.applyLocalizedPattern("dd/MM");
		return sdf.format(componentValue);
	}
	
	public String getMes_Anio() {
		if (componentValue == null) return null;
		if (isOmision) return "99/9999";
		sdf.applyLocalizedPattern("MM/yyyy");
		return sdf.format(componentValue);
	}
	
	public String getDia_Anio() {
		if (componentValue == null) return null;
		if (isOmision) return "99/9999";
		sdf.applyLocalizedPattern("dd/yyyy");
		return sdf.format(componentValue);
	}
	
	public Integer getHora() {
		if (componentValue == null) return null;
		if (isOmision) return 99;
		sdf.applyLocalizedPattern("HH");
		return Integer.valueOf(sdf.format(componentValue));
	}
	
	public Integer getMinuto() {
		if (componentValue == null) return null;
		if (isOmision) return 99;
		sdf.applyLocalizedPattern("mm");
		return Integer.valueOf(sdf.format(componentValue));
	}
	
	public Integer getSegundo() {
		if (componentValue == null) return null;
		if (isOmision) return 99;
		sdf.applyLocalizedPattern("ss");
		return Integer.valueOf(sdf.format(componentValue));
	}

	public Class<String> getValueClass() {
        return String.class;
    }	
	
	private void setShowObject(SHOW_HIDE object, boolean show){
		if(datePiker == null) return;
		this.formato = setFormato(tipoShow = object); 
		this.dateFormat = new SimpleDateFormat(this.formato);
		datePiker.showObject(object, show);
	}
	
	private void rangeDay(int from, int to){
		if(datePiker == null) return;
		datePiker.setRangoDay(from, to);
	}
	
	private void rangeMonth(int from, int to){
		if(datePiker == null) return;
		datePiker.setRangoMonth(from, to);
	}
	
	private void rangeYear(int from, int to){
		if(datePiker == null) return;
		datePiker.setRangoYear(from, to);
	}
	
	private void rangeHour(int from, int to){
		if(timePiker == null) return;
		timePiker.setRangoHour(from, to);
	}
	
	private void rangeMinute(int from, int to){
		if(timePiker == null) return;
		timePiker.setRangoMinute(from, to);
	}
	
	private void focusOnDissmis(View focus){  
		if(datePiker != null) {
			datePiker.setFocusOnDissmis(focus);
		}
		if(timePiker != null) {
			timePiker.setFocusOnDissmis(focus);
		}	
	}
	
	public void setReadOnlyD(boolean flag) {
		super.setReadOnly(true);
		this.setClickable(false);
		this.isEnabled=!flag;
		if(flag){
			this.onFocuslistener = getOnFocusChangeListener();
			this.setOnFocusChangeListener(null);
		} else {
			this.setOnFocusChangeListener(this.onFocuslistener);
		}
	}
	
	public void setComodin(boolean b) {
		this.comodin = b;
	}
	
	public void setButtons(String ok, String cancel) {
		if(Util.esVacio(ok)){
			if(datePiker != null) ok = "Grabar Fecha";
			if(timePiker != null) ok = "Grabar Hora";
		}
		if(Util.esVacio(cancel)) cancel = "Cancelar";
		if(datePiker != null) datePiker.setButtons(ok, cancel);
		if(timePiker != null) timePiker.setButtons(ok, cancel);
	}
	
	public boolean isComodin() {
		return this.comodin;
	}
	
	public boolean isdateOrhour(){
		return this.dateOrhour;
	}
	
	private void setDateEditText() {
		if(this.day!=null) this.day.setText(Util.completarCadena(""+(tipo==TIPO_DIALOGO.FECHA?
				getDia():getHora()),"0",2,Util.COMPLETAR.IZQUIERDA));
		if(this.month!=null) this.month.setText(Util.completarCadena(""+(tipo==TIPO_DIALOGO.FECHA?
				getMes():getMinuto()),"0",2,Util.COMPLETAR.IZQUIERDA));
		if(this.year!=null)	this.year.setText(Util.completarCadena(""+(tipo==TIPO_DIALOGO.FECHA?
				getAnio():getSegundo()),"0",4,Util.COMPLETAR.IZQUIERDA));
	}
	
	public EditText[] getDateEditText() {
		return new EditText[]{this.day, this.month, this.year};
	}
	
	private void setDateOrHour(EditText dayOrhour, EditText monthOrmin, EditText yearOrseg){
		this.dateOrhour = true;
		this.day = dayOrhour;
		this.month = monthOrmin;
		this.year = yearOrseg;
	}
	
	public boolean isInRange(){
		if(datePiker!=null){
			return datePiker.isInRange(getStringValue());
		}
		return true;
	}
	
	/*Fluent API*/
	public DateTimeField maxLength(int length) {this.setMaxLength(length); return this;}
//	public DateTimeField size(int alto, int ancho) {super.setSize(alto, ancho); return this;}
	public DateTimeField size(float alto, float ancho) {super.setSizeFloat(alto, ancho); return this;}
	public DateTimeField setRangoDay(int from, int to) {this.rangeDay(from, to);return this;}
	public DateTimeField setRangoMonth(int from, int to) {this.rangeMonth(from, to);return this;}
	public DateTimeField setRangoYear(int from, int to) {this.rangeYear(from, to);return this;}
	public DateTimeField setRangoHour(int from, int to) {this.rangeHour(from, to);return this;}
	public DateTimeField setRangoMinute(int from, int to) {this.rangeMinute(from, to);return this;}
	public DateTimeField showObject(SHOW_HIDE object) {this.showObject(object, false);return this;}
	public DateTimeField showObject(SHOW_HIDE object, boolean show) {this.setShowObject(object, show);;return this;}
	public DateTimeField setRangoDate(String from, String to) {datePiker.setRangoDate(from, to);return this;}
	public DateTimeField hint(int hintId) {this.setHintText(hintId); return this;}
	public DateTimeField readOnly() {this.readOnly(true); return this;}
	public DateTimeField readOnly(boolean flag) {this.setReadOnlyD(flag); return this;}
	public DateTimeField comodin() {this.setComodin(true); return this;}
	public DateTimeField buttons() {this.buttons(null, null); return this;}
	public DateTimeField buttons(String ok, String cancel) {this.setButtons(ok, cancel); return this;}
	public DateTimeField callback(String method) {this.setCallback(method); return this;}
	public DateTimeField parent(FragmentForm parent) {this.setFormParent(parent); return this;}
	public DateTimeField parent(DialogFragmentComponent parent) {this.setDialogParent(parent); return this;}
	public DateTimeField dateOrhour(EditText dayOrhour, EditText monthOrmin, EditText yearOrseg) {this.setDateOrHour(dayOrhour, monthOrmin, yearOrseg); return this;}
	
	public void setFocusOnDissmis(View focus) {this.focusOnDissmis(focus);}

	@Override
	public void callback() {
	}

	public CallbackEvent getCallbackEvent() {
		return callbackEvent;
	}
	
	public interface ChangeValueListener {
		void onChangeValue();
	}
}
