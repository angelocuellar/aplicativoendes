package gob.inei.dnce.components;

import android.support.v4.view.ViewPager;
import android.util.Log;

/**
 * 
 * @author Rdelacruz
 *
 */
public class CuestionarioSimpleOnPageChangeListener 
extends FragmentViewPager.SimpleOnPageChangeListener {

	//private boolean debeGrabar = true;
	//private boolean jump = true;
	//private final List<TransicionFragmento> transicionesProhibidas;
	//private final Map<Integer, Integer> transicionesPersonalizadas;
	
	private boolean retrocesoPorError = false;
	
	private int TRANSICION_INACTIVA = -1;//antes de la transicion
	private int TRANSICION_REALIZADA = -2;//despues de la transicion
	private int transicionPersonalizada = TRANSICION_INACTIVA;
	
	private MasterActivity activity;
	
	public CuestionarioSimpleOnPageChangeListener(MasterActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public void onPageScrollStateChanged(int state) {
		if (state == ViewPager.SCROLL_STATE_DRAGGING) {
			activity.setPrevPage(activity.viewPager.getCurrentItem());
			activity.esArrastre = true;
		}
		super.onPageScrollStateChanged(state);
	}
	
	@Override
	public void onPageSelected(final int position) {
		try {
			activity.setCurPage(position);
			int prevPage = activity.getPrevPage();
			boolean continuar = true;
			boolean haciaAdelante = prevPage < position;
			
			if(activity.esArrastre) {
				
				FragmentForm ff_previo = activity.pageAdapter.getItem(prevPage);
				
				if(transicionPersonalizada==TRANSICION_INACTIVA || transicionPersonalizada>=0) {
					if(haciaAdelante) {//|| debeGrabar) {//&& debeGrabar) {
						continuar = ff_previo.grabar();
					}
				}
				
				if(continuar) {
											
					if(transicionPersonalizada==TRANSICION_INACTIVA) {							
						//TRANSICION PERSONALIZADA						
						
						//Salto a cualquier fragmento siguiente o anterior
//						Integer fragmentoSiguiente = ff_previo.getSalto();
//						if(fragmentoSiguiente!=null && fragmentoSiguiente>=0) {
//							transicionPersonalizada = fragmentoSiguiente;
//						}
//						else {
							if(position > prevPage) {
								// >= 0 : se salta al n�mero de fragmento indicado
								// < 0 : salto a la p�gina siguiente
								// null : no sucede el salto
								Integer sig = ff_previo.getPaginaSiguiente();
								if(sig!=null) {
									if(sig>=0)//ignoro los negativos del usuario
										transicionPersonalizada = sig;
								}
								else
									continuar = false;
							}
							else {
								// >= 0 : se salta al n�mero de fragmento indicado
								// < 0 : salto a la p�gina anterior
								// null : no sucede el salto
								Integer ant = ff_previo.getPaginaAnterior();							
								if(ant!=null) {
									if(ant>=0)//ignoro los negativos del usuario
										transicionPersonalizada = ant;
								}
								else
									continuar = false;
							}
//						}
						
					}
				}
			}
			
			if(transicionPersonalizada==TRANSICION_REALIZADA)
				transicionPersonalizada = TRANSICION_INACTIVA;
			
			if(continuar) {
				if(transicionPersonalizada>=0) {
					activity.nextFragment(transicionPersonalizada);
					transicionPersonalizada = TRANSICION_REALIZADA;
				} else {
					if(retrocesoPorError) {
						retrocesoPorError = false;
					}
					else {
						FragmentForm ff = activity.pageAdapter.getItem(position);
						boolean cargar;
						if(haciaAdelante) {
							cargar = ff.preCargarDatosAlAdelantar();
						}
						else {
							cargar = ff.preCargarDatosAlRetroceder();
						}
						
						if(cargar)
							ff.cargarDatos();
						
					}
					activity.esArrastre = false;
				}
			}
			else {			
				if(transicionPersonalizada>=0) {
					transicionPersonalizada = TRANSICION_INACTIVA;
				}
				
				if(prevPage!=position) {
					retrocesoPorError = true;					
					activity.nextFragment(prevPage);
				}
				
				activity.esArrastre = false;
			}
			
								
			//AVANCES
		} catch(Exception e) {
			manejarExcepcion(e);
		}
	}
	
	public void manejarExcepcion(Exception e) {
		ToastMessage.msgBox(activity, "ERROR PageChangeListener: "+e.getMessage(),
				ToastMessage.MESSAGE_ERROR,
				ToastMessage.DURATION_LONG);	
		Log.e("PageChangeListener", "ERROR PageChangeListener: ", e);
		//e.printStackTrace();
	}

	
}
