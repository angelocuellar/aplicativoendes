package gob.inei.dnce.components;

import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;

/**
 * 
 * @author ajulcamoro
 *
 */
public class RadioGroupOtherField extends RadioGroup implements Cloneable, FieldComponent {

	private static String TAG = "RadioGroupOtherField";
	private Context context;
	public static enum ORIENTATION{VERTICAL, HORIZONTAL};
	private ORIENTATION orientacion;
	protected int[] opciones;
	protected MyRadioButtonListener rbListener = null;
	protected List<Componente> buttons;
	private List<String> callbacks;
	private int indexTag = -1;
		
	public RadioGroupOtherField(Context context) {
		//super(context);
		//this.context = context;
		this(context,new int[]{});//RAMON
	}
	
	public RadioGroupOtherField(Context context, int... opciones) {
		this(context, null, opciones);
	}
	
	public RadioGroupOtherField(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}
	
	public RadioGroupOtherField(Context context, AttributeSet attrs, int... opciones) {
		super(context, attrs);
		this.context = context;
		this.setOrientation(HORIZONTAL);
		//this.setGravity(17);
		crearOptions(opciones);
		this.setSize(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
	
	public void setOrientation(ORIENTATION sentido) {
		this.orientacion = sentido;
		switch (sentido) {
		case HORIZONTAL:
			this.setOrientation(LinearLayout.HORIZONTAL);
			break;
		case VERTICAL:
			this.setOrientation(LinearLayout.VERTICAL);
			break;
		}		
	}
	
	private void crearOptions(int[] opciones) {
		if (opciones == null) {
			return;
		}
		this.opciones = opciones;
		this.buttons = new ArrayList<Componente>();
//		rbListener = new MyRadioButtonListener();
		for (int i = 0; i < this.opciones.length; i++) {
			RadioButton rb = new RadioButton(context);
//			rb.setOnCheckedChangeListener(rbListener);
			rb.setText(getResources().getString(this.opciones[i]));
			rb.setTag(i+1);
			buttons.add(new Componente(rb));
			this.addView(rb);
		}
		setTagIndexSelected(-1);
//		this.setOnCheckedChangeListener(new RadioGroupFieldListener());
	}
	
	
	public RadioButton agregarEspecifique(int position, TextBoxField txtOTRO) {
		if (rbListener == null) {
			rbListener = new MyRadioButtonListener();
			for (Componente c : buttons) {
				c.radioButton.setOnCheckedChangeListener(rbListener);
//				c.radioButton.setOnClickListener(rbListener);
			}
		}
		Util.lockView(context, txtOTRO);
		//RadioButton rb = new RadioButton(context);
		//rb.setOnCheckedChangeListener(rbListener);
		//rb.setText(getResources().getString(this.opciones[position]));
		//rb.setTag(position+1);
		RadioButton rbb = (RadioButton)getChildAt(position);
		this.removeViewAt(position);
		this.buttons.set(position, new Componente(rbb,true,txtOTRO));
		LinearLayout.LayoutParams lp = new LayoutParams(10+(2*txtOTRO.getLayoutParams().width), txtOTRO.getLayoutParams().height);
		lp.gravity = Gravity.CENTER_VERTICAL|Gravity.LEFT;
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		lp.rightMargin = 10;
		lp.topMargin = 2;
		lp.bottomMargin = 2;
		//ll.addView(rb);
		ll.addView(rbb);
		ll.addView(txtOTRO);		
		this.addView(ll, position);
		//return rb;
		return rbb;
	}
	
	public void agregarTitle(int position, LabelComponent lblTitle) {
		LinearLayout.LayoutParams lp = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.gravity = Gravity.CENTER_VERTICAL|Gravity.LEFT;
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		ll.addView(lblTitle);		
		this.addView(ll, position, lp);
	}
	
	//RAMON
	public final RadioButton addOption(int key, String etiqueta) {
		RadioButton rb = new RadioButton(context);
//		rb.setOnCheckedChangeListener(rbListener);
		rb.setText(etiqueta);
		rb.setTag(key);
		buttons.add(new Componente(rb));
		this.addView(rb);
		return rb;
	}
	
	public RadioGroupOtherField setTagsReplace(Object... tags) {
		for (int i = 0; i < tags.length; i++) {
			if(buttons.size()<=i) break;
			buttons.get(i).radioButton.setTag(tags[i]);
		}
		return this;
	}	
	
	public final void removeOption() {
		//RadioButton rb = 
		Componente com = buttons.remove(buttons.size()-1);
		//this.addView(rb);
		this.removeView(com.radioButton);
	}	
	
	public final int numberOptions() {
		return buttons.size();
	}
	
	//RAMON
	public void addEspecifique(int position, RadioButton rb, TextBoxField txtOTRO) {
		Util.lockView(getContext(), txtOTRO);
		
		/*
		RadioButton rb = new RadioButton(context);
		rb.setOnCheckedChangeListener(rbListener);
		rb.setText(getResources().getString(this.opciones[position]));
		rb.setTag(position+1);
		this.removeViewAt(position);
		*/
		
		buttons.set(position, new Componente(rb,true,txtOTRO));
		LinearLayout.LayoutParams lp = new LayoutParams(10+(2*txtOTRO.getLayoutParams().width), txtOTRO.getLayoutParams().height);
		lp.gravity = Gravity.CENTER_VERTICAL|Gravity.LEFT;
		LinearLayout ll = new LinearLayout(getContext());
		
		ll.setOrientation(LinearLayout.HORIZONTAL);
		lp.rightMargin = 10;
		lp.topMargin = 2;
		lp.bottomMargin = 2;
		
		((ViewGroup)rb.getParent()).removeView(rb);//RAMON
		ll.addView(rb);
		ll.addView(txtOTRO);		
		this.addView(ll, position);
	}

	@Override
	public void setEnabled(boolean enabled) {
		for (int i = 0; i<getChildCount(); i++) {
		    getChildAt(i).setEnabled(enabled);      
		}
		super.setEnabled(enabled);
		if (buttons == null) {
			return;
		}
		for (Componente c : buttons) {
			c.radioButton.setEnabled(enabled);
			if (c.tieneDetalle) {
				if (!enabled) {
					Util.cleanAndLockView(context, c.view);
				}
			}
		}
	}
	
	public void setReadOnly(boolean readOnly) {
		this.setClickable(!readOnly); 
		this.setFocusable(!readOnly); 
		this.setFocusableInTouchMode(!readOnly); 
		if (buttons != null) {
			for (int i = 0; i < buttons.size(); i++) {
				buttons.get(i).radioButton.setClickable(!readOnly);
				buttons.get(i).radioButton.setFocusable(!readOnly); 
				buttons.get(i).radioButton.setFocusableInTouchMode(!readOnly);
			}	
		}
	}
	
	public void setReadOnly() {
		this.setReadOnly(true); 
	}
	
//	@Override
//	public void setFocusable(boolean enabled) {
//		for (int i = 0; i<getChildCount(); i++) {
//		    getChildAt(i).setFocusable(enabled);      
//		}
//		super.setFocusable(enabled);
//		if (buttons == null) {
//			return;
//		}
//		for (Componente c : buttons) {
//			c.radioButton.setFocusable(enabled);
//		}
//	}
//	
//	@Override
//	public void setFocusableInTouchMode(boolean enabled) {
//		for (int i = 0; i<getChildCount(); i++) {
//		    getChildAt(i).setFocusableInTouchMode(enabled);      
//		}
//		super.setFocusableInTouchMode(enabled);
//		if (buttons == null) {
//			return;
//		}
//		for (Componente c : buttons) {
//			c.radioButton.setFocusableInTouchMode(enabled);
//		}
//	}

	public Object getTagSelected(){
//		if(getCheckedRadioButtonId()==-1) return null;
		Object tag = null; 
//		Log.e("buttons", "buttons is null: "+(buttons==null?"true":"false"));
//		Log.e("seleee", "selecc: "+);
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
//		for (int i = 0; i < buttons.size(); i++) {
//			Log.e("ver: "+this.getCheckedRadioButtonId() +" - "+ buttons.get(i).radioButton.isChecked(), "ver: "+buttons.get(i).radioButton.getTag());
//		}
		for (int i = 0; i < buttons.size(); i++) {
//			Log.e("obs: "+buttons.get(i).radioButton.getTag(), "obs: "+buttons.get(i).radioButton.isChecked());
			if (buttons.get(i).radioButton.isChecked()) {
				tag = buttons.get(i).radioButton.getTag();
				break;
			}
		}
		
		return tag;
	}
	
	public Object getTagSelected(String _null){
		Object tag = _null; 
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		for (int i = 0; i < buttons.size(); i++) {
			if (buttons.get(i).radioButton.isChecked()) {
				tag = buttons.get(i).radioButton.getTag();
				break;
			}
		}
		return tag;
	}
	
	public boolean isRadioButtonSelected() {
		if (buttons == null) {
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		boolean flag = false;
		for (Componente c : buttons) {
			if (c.radioButton.isChecked()) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	public void setTagSelected(Object tag) {
		if (tag == null) {
			limpiarCheck(-1);
			return;
		}
		if (buttons == null) {
//			rbListener = new MyRadioButtonListener();
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
//					((RadioButton) getChildAt(i)).setOnCheckedChangeListener(rbListener);
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
//						((RadioButton) ll.getChildAt(0)).setOnCheckedChangeListener(rbListener);
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
				/* No cumple con la funcion equals */
				// if(rb.getTag().equals(tag)){
			}
//			int tot = getChildCount();
//			for (int i = 0; i < tot; i++) {
//				if (getChildAt(i) instanceof RadioButton) {
//					RadioButton rb = (RadioButton) getChildAt(i);
//					if (rb.getTag().toString().equals(tag.toString())) {
//						((RadioButton) getChildAt(i)).setChecked(true);
//						break;
//					}
//				} else if (getChildAt(i) instanceof LinearLayout) {
//					LinearLayout ll = (LinearLayout) getChildAt(i);
//					if (ll.getChildAt(0) instanceof RadioButton) {
//						RadioButton rb = (RadioButton) ll.getChildAt(0);
//						if (rb.getTag().toString().equals(tag.toString())) {
//							((RadioButton) ll.getChildAt(0)).setChecked(true);
//							break;
//						}
//					}
//				}
//				/* No cumple con la funcion equals */
//				// if(rb.getTag().equals(tag)){
//			}
		}
		int tagIndex = -1;
		for (int i = 0; i < buttons.size(); i++) {
			if (buttons.get(i).radioButton.getTag().toString().equals(tag.toString())) {
				tagIndex = i;
				buttons.get(i).radioButton.setChecked(true);
				if (buttons.get(i).tieneDetalle) {
					Util.lockView(context, false, buttons.get(i).view);
				}
				executeCallBack();
				break;
			}
		}
		limpiarComponentes(tagIndex);
	}
	
	public void setTagIndexSelected(int tagIndex){
		limpiarComponentes(tagIndex);
		limpiarCheck(tagIndex);
		if(tagIndex == -1){
			return;
		}
//		ToastMessage.msgBox(context, buttons.get(tagIndex).radioButton.getText().toString(), 
//				ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//		Log.e("checkaaaabbb", "buttons.get(tagIndex).radioButton: "+buttons.get(tagIndex).radioButton.getTag());
		
		buttons.get(tagIndex).radioButton.setSelected(true);
		buttons.get(tagIndex).radioButton.setChecked(true);
//		buttons.get(tagIndex).radioButton.toggle();
//		Log.e("checkaaaabbbccc", "antesssssss");
		if (buttons.get(tagIndex).tieneDetalle) {
			Util.lockView(context, buttons.get(tagIndex).view, false);
			buttons.get(tagIndex).view.requestFocus();
		}
		limpiarComponentes(tagIndex);
//		Log.e("terminas", "terminas");
	}
	
	public void changeOthersValues(int tagIndex){		
		if (buttons.get(tagIndex).tieneDetalle) {
			Util.lockView(context, false, buttons.get(tagIndex).view);
			buttons.get(tagIndex).view.requestFocus();
		}
		limpiarComponentes(tagIndex);
		executeCallBack();
	}
	
	private void limpiarComponentes(int tagIndexSelected) {
		if(buttons != null){
			for (int i = 0; i < buttons.size(); i++) {
				if (i == tagIndexSelected) {
					continue;
				}
				if (buttons.get(i).tieneDetalle) {
					Util.cleanAndLockView(context, buttons.get(i).view);
					buttons.get(i).radioButton.setChecked(false);
					buttons.get(i).radioButton.setSelected(false);
				}
			}
		}
	}
		
	private void limpiarCheck(int tagIndex) {
//		int tot = getChildCount();
//		for (int i = 0; i < tot; i++) {
//			if (getChildAt(i) instanceof RadioButton) {
//				((RadioButton) getChildAt(i)).setChecked(false);
//			} else if (getChildAt(i) instanceof LinearLayout) {
//				LinearLayout ll = (LinearLayout) getChildAt(i);
//				if (ll.getChildAt(0) instanceof RadioButton) {
//					((RadioButton) ll.getChildAt(0)).setChecked(false);
//					break;
//				}
//			}
//		}
		if (tagIndex == -1 || buttons.get(tagIndex).tieneDetalle) {
			super.clearCheck();
		}
		clearCheck();
//		if (buttons == null) {
//			return;
//		}
//		for (Componente b : buttons) {
//			b.radioButton.setChecked(false);
//		}
	}
	
	@Override
	public void clearCheck() {
//		super.clearCheck();
		if (buttons != null) {
//			Log.e("buttons.size", "buttons.size: "+buttons.size());
			for (Componente b : buttons) {
				b.radioButton.setChecked(false);
				b.radioButton.setSelected(false);
			}
		}
	}

	public void setSize(int alto, int ancho) {		
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(this.context, ancho), Util.getTamañoEscalado(this.context, alto, 0.3f));
		if (this.orientacion == ORIENTATION.HORIZONTAL) {
			this.setGravity(Gravity.CENTER);
		} else if (this.orientacion == ORIENTATION.VERTICAL) {
			this.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
			lp.leftMargin = 7;
		}
		this.setLayoutParams(lp);
	}
	public void setSizeFloat(float alto, float ancho) {		
		LayoutParams lp = new LayoutParams((int)Util.getTamañoEscaladoFloat(this.context, ancho), (int)Util.getTamañoEscaladoFloat(this.context, alto));
		if (this.orientacion == ORIENTATION.HORIZONTAL) {
			this.setGravity(Gravity.CENTER);
		} else if (this.orientacion == ORIENTATION.VERTICAL) {
			this.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
			lp.leftMargin = 7;
		}
		this.setLayoutParams(lp);
	}
	
	private void setSizeText(float size) {
		for(Componente c:buttons){
			c.radioButton.setTextSize(size);
		}
	}
	
	public void addCallBack(String callBackMethodName) {
		if (this.callbacks == null) {
			this.callbacks = new ArrayList<String>();
		}
		this.callbacks.add(callBackMethodName);
	}
	
	public List<String> getCallbacks() {
		return this.callbacks;
	}
	
	private void executeCallBack() {
		
	}
	
	public void lock(boolean lock) {
		if (buttons == null) {
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		for (Componente rb : buttons) {
			if (rb.tieneDetalle) {
				rb.radioButton.setEnabled(!lock);
				rb.radioButton.setFocusable(!lock);
				rb.radioButton.setFocusableInTouchMode(!lock);
				Util.lockView(context, true, rb.view);
			}
		}
	}
	
	public void lockButtons(boolean lock, int... indexes) {
		if (buttons == null) {
			buttons = new ArrayList<Componente>();
			int tot = getChildCount();
			for (int i = 0; i < tot; i++) {
				if (getChildAt(i) instanceof RadioButton) {
					buttons.add(new Componente((RadioButton) getChildAt(i)));					
				} else if (getChildAt(i) instanceof LinearLayout) {
					LinearLayout ll = (LinearLayout) getChildAt(i);
					if (ll.getChildAt(0) instanceof RadioButton) {
						buttons.add(new Componente((RadioButton) ll.getChildAt(0)));
					}
				}
			}
		}
		for (int i = 0; i < indexes.length; i++) {
			int j = indexes[i];
			if (j < buttons.size()) {
				buttons.get(j).radioButton.setEnabled(!lock);
				if(lock){
					buttons.get(j).radioButton.setChecked(!lock);
					buttons.get(j).radioButton.setSelected(!lock);
				}
//				buttons.get(j).radioButton.setFocusable(!lock);
//				buttons.get(j).radioButton.setFocusableInTouchMode(!lock);
				if (buttons.get(j).tieneDetalle) {
					Util.lockView(context, true, buttons.get(j).view);
				}
			}
		}
//		for (Componente rb : buttons) {
//			if (rb.tieneDetalle) {
//				rb.radioButton.setEnabled(!lock);
//				rb.radioButton.setFocusable(!lock);
//				rb.radioButton.setFocusableInTouchMode(!lock);
//				Util.lockView(context, true, rb.view);
//			}
//		}
	}
	
	public void checkRadioButton(int index) {
	    if (buttons == null) {
	        // Initialization code as in your existing method
	    }

	    if (index >= 0 && index < buttons.size()) {
	        buttons.get(index).radioButton.setChecked(true);
	    }
	}
	
	
	public Integer getTagSelectedInteger(Integer _null){
		if(getCheckedRadioButtonId()==-1) {
			String val = getTagSelected(_null==null?"":String.valueOf(_null)).toString();
			return val.equals("")?_null:Integer.valueOf(val);
		}
		RadioButton rb = (RadioButton) findViewById(getCheckedRadioButtonId());
		
		Integer ret = null;
		
		try{
			ret = Integer.valueOf(rb.getTag().toString());
		}
		catch(NumberFormatException e){
			return null;
		}
		return ret;
	}
	
	public boolean isTagSelected(Integer tag){
		Integer sel = getTagSelectedInteger(null);
		Log.e("tag: "+tag, "sel: "+sel);
		
		if( tag==null && sel==null ) return true;
		else if( (tag==null && sel!=null) || (tag!=null && sel==null) ) return false;
		
		return tag.equals(sel);
	}
	
	public boolean isTagSelected(Object tag){
		Object sel = getTagSelected();
		
		if( tag==null && sel==null ) return true;
		else if( (tag==null && sel!=null) || (tag!=null && sel==null) ) return false;
		
		return tag.equals(sel);
	}
	
	public boolean isTagSelectedBetween(Integer from, Integer to){
		return isTagSelectedBetween(from, to, true);
	}
	
	public boolean isTagSelectedBetween(Integer from, Integer to, boolean inclusive){
		
		Integer sel = getTagSelectedInteger(null);
		if(sel == null) return false;
		
		if(inclusive) return sel>=from && sel<=to;
		else return sel>from && sel<to;
	}
	
	public boolean isTagSelectedBetween(Integer[] more){
		Integer sel = getTagSelectedInteger(null);
		if(sel == null) return false;
		
		for( Integer val : more ){
			if( val.equals(sel) ) return true;
		}
			
		return false;
	}
	
	@Override
	public boolean isPressed() {
		if(getCheckedRadioButtonId()!=-1)
			return ((RadioButton)findViewById(getCheckedRadioButtonId())).isPressed();
		
		int tot = getChildCount();
		for (int i = 0; i < tot; i++) {
			if (getChildAt(i) instanceof LinearLayout) {
				LinearLayout ll = (LinearLayout) getChildAt(i);
				if (ll.getChildAt(0) instanceof RadioButton) {
					return ((RadioButton) ll.getChildAt(0)).isPressed();
				}
			}
		}
			
		return false;

//		return (getCheckedRadioButtonId()!=-1 && 
//				((RadioButton)findViewById(getCheckedRadioButtonId())).isPressed());
	}
	
//	public RadioGroupOtherField size(int alto, int ancho) { this.setSize(alto,ancho); return this;}
	public RadioGroupOtherField size(int alto, int ancho) { this.setSizeFloat(alto,ancho); return this;}
	public RadioGroupOtherField orientation(ORIENTATION sentido) { this.setOrientation(sentido); return this; }
    public RadioGroupOtherField callback(String methodCallBackName){ this.addCallBack(methodCallBackName); return this;}
    public RadioGroupOtherField readOnly() { this.setReadOnly(); return this;}
    public RadioGroupOtherField centrar() { this.centrar(Gravity.CENTER); return this;}
    public RadioGroupOtherField centrar(int gravity) { this.setGravity(gravity); return this;}
    public RadioGroupOtherField sizeText(float size) { ; this.setSizeText(size); return this;}
	
	private class MyRadioButtonListener implements android.widget.CompoundButton.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				try {
					int index = Integer.parseInt(((RadioButton) buttonView)
							.getTag().toString());
					indexTag = index - 1;
//					Log.e("index", "index: "+index);
//					limpiarComponentes(index - 1, true);
					RadioGroupOtherField.this.setTagIndexSelected(index - 1);
				} catch (NumberFormatException e) {
					RadioGroupOtherField.this.setTagIndexSelected(-1);
				}
			}
		}
	}
	
	private class MyRadioButtonListener2 implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			try {
				int index = Integer.parseInt(((RadioButton) v)
						.getTag().toString());
				//TODO arreglar cuando tag empiece en 0
				RadioGroupOtherField.this.setTagIndexSelected(index-1);
			} catch (NumberFormatException e) {
				RadioGroupOtherField.this.setTagIndexSelected(-1);
			}
		}

	}
	
	protected class Componente {
		RadioButton radioButton;
		boolean tieneDetalle;
		TextBoxField view;
		
		public Componente(RadioButton radioButton, boolean tieneDetalle, TextBoxField view) {
			super();
			this.radioButton = radioButton;
			this.tieneDetalle = tieneDetalle;
			this.view = view;
		}
		
		public Componente(RadioButton radioButton) {
			this(radioButton, false, null);
		}
		
	}

	@Override
	public void setValue(Object aValue) {
		setTagSelected(aValue);
	}

	@Override
	public Object getValue() {
		return getTagSelected();
	}

	@Override
	public Class<?> getValueClass() {
		return Object.class;
	}
	
	 // Function to check a RadioButton
    private void checkRadioButton(RadioButton radioButton) {
        if (radioButton != null) {
            radioButton.setChecked(true);
        }
    }

    

	
	//RAMON
	public final int getNumeroOpciones() {
		if(opciones!=null)
			return opciones.length;
		return 0;
	}
	
	//RAMON
	public MyRadioButtonListener getMyRadioButtonListener() {
		return new MyRadioButtonListener();
	}
}