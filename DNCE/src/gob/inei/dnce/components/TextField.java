package gob.inei.dnce.components;

import gob.inei.dnce.util.Util;
import gob.inei.dnce.components.TextBoxField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.TextBoxField.INPUT_TYPE;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;

public class TextField extends TextBoxField {

	private String componentValue;
	public final static int TEXTO = Util.MyTextWatcher.TEXTO;
	public final static int TEXTO_NUMERO = Util.MyTextWatcher.ALFANUMERICO;
	public final static int SOLO_TEXTO = Util.MyTextWatcher.SOLO_TEXTO;
	public final static int SOLO_TEXTO_NUMERO = Util.MyTextWatcher.SOLO_TEXTO_NUMERO;
	public final static int SOLO_TEXTO_NUMERO_PUNTO = Util.MyTextWatcher.SOLO_TEXTO_NUMERO_PUNTO;
	public final static int DECIMAL = Util.MyTextWatcher.DECIMAL;
	public final static int TELEFONO = Util.MyTextWatcher.TELEFONO;
	
	public TextField(Context context) {
		this(context, TEXTO_NUMERO);
//		super(context);
//		this.setInputType(TEXTO_NUMERO);
		setTipoTexto(TEXTO_NUMERO);
        
//		this(context, TEXTO);
//		this(context, TEXTO_NUMERO);
//        this.setInputType(getInputType(INPUT_TYPE.TEXTO_MULTILINEA));
        
	}
	
	public TextField(Context context, int tipoTexto) {
		this(context, tipoTexto, true);
	}
	
	public TextField(Context context, boolean hasTextWatcher) {
		this(context, TEXTO, hasTextWatcher);
	}
	
	public TextField(Context context, int tipoTexto, boolean hasTextWatcher) {
		super(context);
//		this.setFilters(new InputFilter[]{Util.filtroTexto});
		this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TEXTO));
		this.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		this.setPadding(7, 0, 4, 0);
		if(hasTextWatcher){
			textWatcher = new Util.MyTextWatcher(this, tipoTexto);
			this.addTextChangedListener(textWatcher);
		}
	}

	public TextField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}	

	public void setTipoTexto(int tipo) { 
		if (textWatcher != null) {
			this.removeTextChangedListener(textWatcher);
			textWatcher = null;
		}
		textWatcher = new Util.MyTextWatcher(this, tipo);
		this.setTextWatcher(textWatcher);
		if (tipo == TEXTO_NUMERO) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TEXTO_NUMEROS));
		} else if(tipo == DECIMAL) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.DECIMALES));
		} else if(tipo == TELEFONO) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TELEFONO));
		} else if (tipo == SOLO_TEXTO_NUMERO_PUNTO) {
			this.setInputType(getInputType(TextBoxField.INPUT_TYPE.TEXTO_NUMEROS_PUNTO));
		}
	}
	
	
	public Class<String> getValueClass() {
        return String.class;
    }
	
	/*Fluent API*/
	public TextField maxLength(int length) {this.setMaxLength(length); return this;}
//	public TextField size(int alto, int ancho) { super.setSize(alto, ancho); return this;}
	public TextField size(float alto, float ancho) { super.setSizeFloat(alto, ancho); return this;}
	public TextField readOnly() { readOnly(true); return this;}
	public TextField readOnly(boolean lock) { this.setReadOnly(lock); return this;}
	public TextField visible() { this.setVisible(); return this;}
	public TextField negrita() { this.setTypeface(null, Typeface.BOLD); return this;}
	public TextField hint(int id) { this.setHintText(id); return this;}
	public TextField centrar() { this.posicionar(Gravity.CENTER); return this;}
	public TextField alinearIzquierda() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.LEFT); return this;}
	public TextField alinearDerecha() { this.posicionar(Gravity.CENTER_VERTICAL|Gravity.RIGHT); return this;}
	public TextField completarCaracteres(int espacios, String caracter) { this.setCompletarCaracteres(true, espacios); this.setCaracterCompletar(caracter); return this;}
	public TextField completarIzquierda() { this.direccionCompletar = DIRECCION_COMPLETAR.IZQUIERDA; return this;}
	public TextField completarDerecha() { this.direccionCompletar = DIRECCION_COMPLETAR.DERECHA; return this;}
	public TextField inputType(INPUT_TYPE input) { this.setInputType(getInputType(input)); return this;}
	public TextField callback(String callback) { this.setCallback(callback); return this; }
	public TextField callbackOnFocus(String callback) { this.setCallbackOnFocus(callback); return this; }
	public TextField alfanumerico() { this.setTipoTexto(TEXTO_NUMERO); return this;}
	public TextField soloTexto() { this.setTipoTexto(SOLO_TEXTO); return this;}
	public TextField soloTextoNumero() { this.setTipoTexto(SOLO_TEXTO_NUMERO); return this;}
	public TextField reescribir(int numVeces) { this.setReescribir(numVeces); return this;}
	public TextField esTelefono() { this.setTipoTexto(TELEFONO); return this;}
}
