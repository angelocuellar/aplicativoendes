package gob.inei.dnce.components;

import java.util.ArrayList;
import java.util.List;

import gob.inei.dnce.R;
import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Util;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.Adapter;
import android.widget.AutoCompleteTextView;
import android.widget.SpinnerAdapter;
import android.widget.LinearLayout.LayoutParams;

public class TextAutoCompleteField extends AutoCompleteTextView implements FieldComponent {
	protected Object componentValue;
	private String valorAnterior = null;
	protected List<String> callbacksOnFocus = null;
	protected List<String> callbacks = null;
	private EntitySpinnerAdapter<? extends Entity> adapter;
	private List<Object> keys;
	
	private boolean readonly = false;
	
	public TextAutoCompleteField(Context context) {
		super(context);
	}
	
	@Override
	public void setValue(Object aValue) {
		this.valorAnterior = Util.getText(componentValue, null);
		if (aValue == null) {
			return;
		}
		this.setText(completarText(aValue));
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return this.componentValue;
	}

	@Override
	public Class<?> getValueClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSize(int alto, int ancho) {
		LayoutParams lp = new LayoutParams(Util.getTamañoEscalado(getContext(), ancho), Util.getTamañoEscalado(getContext(), alto, 0.3f));
		lp.setMargins(2, 2, 2, 2);
		this.setLayoutParams(lp);		
	}
	protected String completarText(Object aValue) {
		if (aValue == null) {
			componentValue = null;
			return "";
		}
		if (aValue.toString().trim().equals("")) {
			componentValue = null;
			return "";
		}
		
		componentValue = aValue;
		String valueToString = componentValue.toString();
		return valueToString;
	}
	public void setReadOnly(boolean flag) {
		this.setCursorVisible(!flag);
		this.setFocusable(!flag);
		this.setFocusableInTouchMode(!flag);
		this.readonly = flag;
	}
	
	public boolean isReadOnly() {
		return readonly;
	}
	
	public void setTextAutomatico(boolean flag) {
		if (flag) {
			this.setTypeface(null, Typeface.BOLD);
			this.setTextColor(getResources().getColor(R.color.blue));
			this.setReadOnly(true);
			this.setBackgroundColor(getResources().getColor(R.color.blanco));
		} else {
			this.setTypeface(null, Typeface.NORMAL);
			this.setTextColor(getResources().getColor(R.color.black));
			// this.setText("");
			this.setReadOnly(false);
			this.setBackgroundColor(getResources().getColor(R.color.cajaweb));
		}
	}
	protected void setTextAutomatico() {
		this.setTextAutomatico(true);
	}
	public List<String> getCallbacksOnFocus() {
		return this.callbacksOnFocus;
	}
	public void setCallbackOnFocus(String method) {
		if (callbacksOnFocus == null) {
			this.callbacksOnFocus = new ArrayList<String>();
		}
		this.callbacksOnFocus.add(method);
	}
	public void setCallback(String method) {
		if (callbacks == null) {
			this.callbacks = new ArrayList<String>();
		}
		Log.e("","CALL: "+method);
		this.callbacks.add(method);
	}
	
	@SuppressWarnings("unchecked")
	public void setAdapterWithKey(SpinnerAdapter adapter, List<Object> keys) {
		this.adapter=(EntitySpinnerAdapter<? extends Entity>) adapter;
		this.keys = keys; 
		if (adapter instanceof EntitySpinnerAdapter<?>) {
			this.adapter = (EntitySpinnerAdapter<? extends Entity>) adapter;
		}
	}
}
