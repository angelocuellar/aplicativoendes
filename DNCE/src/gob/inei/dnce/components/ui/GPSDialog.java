package gob.inei.dnce.components.ui;

import gob.inei.dnce.R;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.GifView;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.IGPSDialog;
import gob.inei.dnce.util.CapturadorGPS;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.mapswithme.maps.api.MapsWithMeApi;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.GetChars;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class GPSDialog extends DialogFragmentComponent {

	private LabelComponent lblTitulo;
	public ButtonComponent btnAceptar;
	public ButtonComponent btnCancelar;
	public ButtonComponent btnVerMapa;
	public TableComponent tcGPS;
	private CapturadorGPS tracker;
	LinearLayout q0, q1;
	private List<String> properties;
	private int estilo = R.color.griscabece;
	private int estiloBoton = 0;
	private Map<String, String> map;
	List<GPSValues> values;
	private IGPSDialog caller;
	private Runnable runnable;
	private Thread thread;
	private boolean running;
	private GifView mapa;
	private String omision = "9999999999";

	public static GPSDialog newInstance(IGPSDialog pagina, CapturadorGPS tracker) {
		GPSDialog f = new GPSDialog();
		f.estilo = R.color.griscabece;
		f.estiloBoton = 0;
		f.caller = pagina;
		f.setParent(pagina.getForm());
		f.tracker = tracker;
		f.property(IGPSDialog.LONGITUD);
		f.property(IGPSDialog.LATITUD);
		f.property(IGPSDialog.ALTURA);
		f.property(IGPSDialog.SATELITES_NRO);
		return f;
	}

	public GPSDialog estilo(int estilo) {
		this.estilo = estilo;
		return this;
	}

	public GPSDialog estiloBoton(int estilo) {
		this.estiloBoton = estilo;
		return this;
	}

	public GPSDialog omision(String omision) {
		this.omision = omision;
		return this;
	}

	public GPSDialog property(String property) {
		setProperty(property);
		return this;
	}

	public void setProperty(String property) {
		if (properties == null) {
			properties = new ArrayList<String>();
		}
		properties.add(property);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View rootView = createUI();
		rootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		return rootView;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (thread != null) {
			thread.interrupt();	
		}
		caller.postShow(map);
	}

	@Override
	protected View createUI() {
		buildFields();
		/* Aca creamos las preguntas */
		LinearLayout llTitulo = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(0, Gravity.CENTER, LinearLayout.VERTICAL, R.drawable.fondo0, mapa);
		q0 = createQuestionSection(tcGPS.getTableView());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		LinearLayout botonMapa = createButtonSection(btnVerMapa);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(llTitulo);
		form.addView(q1);
		form.addView(q0);
		form.addView(botones);
		form.addView(botonMapa);

		return contenedor;
	}

	@Override
	protected void buildFields() {
		mapa = new GifView(caller.getForm().getActivity()).gif(R.drawable.mapa);
		lblTitulo = new LabelComponent(caller.getForm().getActivity(), estilo)
				.size(MATCH_PARENT, MATCH_PARENT).text(R.string.gps_titulo)
				.textSize(21).centrar();
		if (estiloBoton != 0) {
			btnAceptar = new ButtonComponent(caller.getForm().getActivity(), estiloBoton)
				.text(R.string.btnAceptar).size(200, 60);
		} else {
			btnAceptar = new ButtonComponent(caller.getForm().getActivity()).text(R.string.btnAceptar).size(200, 60);			
		}
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				running = false;
				map=new HashMap<String, String>();	
				if (values != null) {
					for (GPSValues propiedad : values) {					
						map.put(propiedad.key, propiedad.value);
					}
				} else {
					map.put(IGPSDialog.LONGITUD, omision);
					map.put(IGPSDialog.LATITUD, omision);
					map.put(IGPSDialog.ALTURA, omision);
				}
				GPSDialog.this.dismiss();
			}
		});
		if (estiloBoton != 0) {
			btnCancelar = new ButtonComponent(caller.getForm().getActivity(),
					estiloBoton).text(R.string.btnCancelar).size(200, 60);
		} else {
			btnCancelar = new ButtonComponent(caller.getForm().getActivity()).text(R.string.btnCancelar).size(200, 60);			
		}
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				running = false;
				map = null;
				GPSDialog.this.dismiss();				
			}
		});
		if (estiloBoton != 0) {
			btnVerMapa = new ButtonComponent(caller.getForm().getActivity(),
					estiloBoton).text(R.string.btnMapa).size(300, 60);
		} else {
			btnVerMapa = new ButtonComponent(caller.getForm().getActivity()).text(R.string.btnMapa).size(200, 60);			
		}
		btnVerMapa.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (map == null) {
					ToastMessage.msgBox(getActivity(), "Aun no hay un punto v�lido",
							ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					return;
				}
				cargarMapa(Double.valueOf(map.get(IGPSDialog.LATITUD)), Double.valueOf(map.get(IGPSDialog.LONGITUD)), "Ubicaci�n actual.");
			}
		});
		tcGPS = new TableComponent(caller.getForm().getActivity(), this, estilo).size(400, 450)
				.headerHeight(50).dataColumHeight(50);
		tcGPS.addHeader(R.string.gps_key, 0.6f, TableComponent.ALIGN.LEFT);
		tcGPS.addHeader(R.string.gps_value, 1.3f, TableComponent.ALIGN.LEFT);
		runnable = new Runnable() {			
			@Override
			public void run() {
				mostrarTabla();
			}
		};
		thread = new Thread() {
			public void run() {
				//tracker = new CapturadorGPS(caller.getForm().getActivity());
				while (running) {
					try {
						caller.getForm().getActivity().runOnUiThread(runnable);
						sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		};
	}

	public void cargarDatos() {
//		ToastMessage.msgBox(caller.getForm().getActivity(), "Hilo GPS", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		Log.e(getClass().getSimpleName(), "Hilo GPS");
		running=true;
//		caller.getForm().getActivity().runOnUiThread(runnable);
//		caller.getForm().getActivity().runOnUiThread(thread);
		thread.start();
		String stringInfo = "";
        stringInfo += "Duration: " + mapa.getMovieDuration() + "\n";
        stringInfo += "W x H: " + mapa.getMovieWidth() + " x " + mapa.getMovieHeight() + "\n";
        Log.e(getClass().getSimpleName(), "Info: " + stringInfo);
	}
	
	private void cargarMapa(Double lati, Double longi, String msj) {
		final double lat = lati;
		final double lon = longi;
		final String name = msj;
		//MapsWithMeApi.showPointOnMap(getActivity(), lat, lon, name);
		
	}

	
	private synchronized void mostrarTabla() {
		Map<String, String> tmpMap = null;	
		//ToastMessage.msgBox(caller.getForm().getActivity(), "Buscando GPS", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//		Log.e(getClass().getSimpleName(), "Buscando GPS [" + Util.getText(tracker.getLongitude()) + "] ["+Util.getText(tracker.getLatitude())+"]");
		if (tracker.getLatitude() != 0) {
			tmpMap = new HashMap<String, String>();
			//ToastMessage.msgBox(caller.getForm().getActivity(), "Encontro GPS [" + Util.getText(tracker.getLongitude()) + "] ["+Util.getText(tracker.getLatitude())+"]", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//			Log.e(getClass().getSimpleName(), "Encontr� GPS");
			values = new ArrayList<GPSDialog.GPSValues>();
			for (String property : properties) {
				if (property.equals(IGPSDialog.LONGITUD)) {
					values.add(new GPSValues(property, Util.getText(tracker.getLongitude(), null)));
					tmpMap.put(property, Util.getText(tracker.getLongitude(), null));
				} else if (property.equals(IGPSDialog.LATITUD)) {
					values.add(new GPSValues(property, Util.getText(tracker.getLatitude(), null)));
					tmpMap.put(property, Util.getText(tracker.getLatitude(), null));
				} else if (property.equals(IGPSDialog.ALTURA)) {
					values.add(new GPSValues(property, Util.getText(tracker.getAltitude(), null)));
					tmpMap.put(property, Util.getText(tracker.getAltitude(), null));
				} else if (property.equals(IGPSDialog.BEARING)) {
					values.add(new GPSValues(property, Util.getText(tracker.getBearing(), null)));
					tmpMap.put(property, Util.getText(tracker.getBearing(), null));
				} else if (property.equals(IGPSDialog.ACCURACY)) {
					values.add(new GPSValues(property, Util.getText(tracker.getAccuracy(), null)));
					tmpMap.put(property, Util.getText(tracker.getAccuracy(), null));
				}
			}
			if (tracker.getBundle() != null) {
				for (String key : tracker.getBundle().keySet()) {
					for (String property : properties) {
						if (property.toUpperCase().equals(key.toUpperCase())) {
							values.add(new GPSValues(key.toUpperCase(), Util.getText(tracker.getBundle()
									.get(key))));
							tmpMap.put(key.toUpperCase(), Util.getText(tracker.getBundle().get(key)));
						}
						if (property.toUpperCase().equals(IGPSDialog.SATELITES_NRO.toUpperCase())) {
							switch (Util.getInt(tracker.getBundle().get(key), 0)) {
							case 0:								
								ponerFondo(q1, R.drawable.fondo0);
								break;
							case 1:
								ponerFondo(q1, R.drawable.fondo1);
								break;
							case 2:
								ponerFondo(q1, R.drawable.fondo2);
								break;
							case 3:
								ponerFondo(q1, R.drawable.fondo3);
								break;
							case 4:
								ponerFondo(q1, R.drawable.fondo4);
								break;
							case 5:
								ponerFondo(q1, R.drawable.fondo5);
								break;
							case 6:
								ponerFondo(q1, R.drawable.fondo6);
								break;
							case 7:
								ponerFondo(q1, R.drawable.fondo7);
								break;
							case 8:
								ponerFondo(q1, R.drawable.fondo8);
								break;
							case 9:
								ponerFondo(q1, R.drawable.fondo9);
								break;
							case 10:
								ponerFondo(q1, R.drawable.fondo10);
								break;
							case 11:
								ponerFondo(q1, R.drawable.fondo11);
								break;
							case 12:
								ponerFondo(q1, R.drawable.fondo12);
								break;
							default:
								ponerFondo(q1, R.drawable.fondo12);
								break;
							}
						}
					}
				}	
			}			
			if (map == null) {
				map=new HashMap<String, String>();	
				for (GPSValues propiedad : values) {					
					map.put(propiedad.key, propiedad.value);
				}
			} else {
				BigDecimal precisionActual = new BigDecimal(Util.getText(map.get(IGPSDialog.ACCURACY), "0"));
				BigDecimal precisionNuevo = new BigDecimal(Util.getText(tmpMap.get(IGPSDialog.ACCURACY), "0"));
				int satelitesActual = Util.getInt(map.get(IGPSDialog.SATELITES_NRO.toUpperCase()));
				int satelitesNuevo = Util.getInt(tmpMap.get(IGPSDialog.SATELITES_NRO.toUpperCase()));
				if (precisionActual.compareTo(precisionNuevo) > 0) {
					map = tmpMap;
				} else if (precisionActual.compareTo(precisionNuevo) == 0) {
					if (satelitesActual < satelitesNuevo) {
						map = tmpMap;	
					}
				}
			}
			tcGPS.setData(values, "key", "value");
		} else {
			ponerFondo(q1, R.drawable.fondo0);
			tcGPS.setData(null, "key", "value");
		}
	}
	
	private void ponerFondo(LinearLayout l,int fondo){		
		l.setBackgroundDrawable(getResources().getDrawable(fondo));
	}

	public static class GPSValues implements IDetailEntityComponent {
		public String key;
		public String value;

		public GPSValues(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}

		@Override
		public boolean isTitle() {
			return false;
		}

		@Override
		public void cleanEntity() {
		}
	}
}