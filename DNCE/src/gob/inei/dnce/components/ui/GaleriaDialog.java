package gob.inei.dnce.components.ui;

import gob.inei.dnce.R;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.ImageComponent;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.camera.CameraClass;
import gob.inei.dnce.components.camera.CustomCamera;
import gob.inei.dnce.components.camera.OnPictureTaken;
import gob.inei.dnce.interfaces.IGaleriaDialog;
import gob.inei.dnce.util.CapturadorGPS;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class GaleriaDialog extends DialogFragmentComponent implements
		OnPictureTaken {
	Integer contador;
	private LabelComponent lblTitulo;
	private Gallery miGaleria;

	public ImageComponent btnCamera;
	private CustomCamera mCustomCamera;

	public ButtonComponent btnAceptar;
	public ButtonComponent btnCancelar;
	LinearLayout q0_2, q0_3, q0_1, q0, q1;
	private int estilo = R.color.griscabece;
	private int estiloBoton = 0;
	private IGaleriaDialog caller;
	public static String RUTA_FOTOS = null;
	private String rutaTmp = null;
	private String prefijoFoto = "camara";
	private int resolucion = CustomCamera.RESOLUTION_640x480;
	private ImagenesAdaptador imageAdapter;

	public static GaleriaDialog newInstance(IGaleriaDialog pagina) {
		GaleriaDialog f = new GaleriaDialog();
		f.estilo = R.color.griscabece;
		f.estiloBoton = 0;
		f.caller = pagina;
		f.setParent(f.caller.getForm());
		return f;
	}

	public GaleriaDialog estilo(int estilo) {
		this.estilo = estilo;
		return this;
	}

	public GaleriaDialog estiloBoton(int estilo) {
		this.estiloBoton = estilo;
		return this;
	}

	public GaleriaDialog prefijoFoto(String prefix) {
		this.prefijoFoto = prefix;
		return this;
	}

	public GaleriaDialog resolucion(int resolucion) {
		this.resolucion = resolucion;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		return rootView;
	}

	@Override
	protected View createUI() {
		buildFields();
		/* Aca creamos las preguntas */
		LinearLayout llTitulo = createQuestionSection(lblTitulo);
		q0_2 = createQuestionSection(btnCamera);
		q0_1 = createQuestionSection(miGaleria);
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(llTitulo);
		form.addView(q0_2);
		form.addView(q0_1);
		form.addView(botones);
		return contenedor;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		String ruta = RUTA_FOTOS + "/" + prefijoFoto + "_";
		CameraClass.RUTA_FOTOS = RUTA_FOTOS;

		for (int i = 0; i < 5; i++) {
			String rutaf = ruta + i + ".jpg";
			Log.e("", "ruta de eliminacion=" + rutaf);
			File file = new File(rutaf);
			if (file.exists()) {
				file.delete();
			}

		}

		CameraClass.RUTA_FOTOS = rutaTmp;
		// caller.refrescarTabla();
		// caller.tcCapPersonas.getListView().requestFocus();
	}

	public class ImagenesAdaptador extends BaseAdapter {

		ArrayList<Bitmap> bitmapArray;
		ArrayList<String> bitmapRuta;

		public ImagenesAdaptador() {
			bitmapArray = new ArrayList<Bitmap>();
			bitmapRuta = new ArrayList<String>();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return bitmapArray.size();// imagenes.length;
		}

		@Override
		public Object getItem(int arg0) {
			return bitmapArray.get(arg0);
		}

		public String getItemRuta(int arg0) {
			return bitmapRuta.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}
		
		public void addBitmap(Bitmap bm, String ruta) {
			bitmapArray.add(bm);
			bitmapRuta.add(ruta);
		}

		@Override
		public View getView(int posicion, View arg1, ViewGroup arg2) {
			Log.e("", "posicion//////" + posicion);
			Log.e("", "arg2//////" + arg2);
			ImageView imagen = new ImageView(caller.getForm().getActivity());
			// imagen.setImageResource(imagenes[posicion]);
			// imagen.setImageBitmap(imagenesbit[posicion]);
			imagen.setImageBitmap(bitmapArray.get(posicion));
			imagen.setLayoutParams(new Gallery.LayoutParams(450, 250));
			// reescalamos la imagen para evitar "java.lang.OutOfMemory" en el
			// caso de im�genes de gran resoluci�n
			// como es este ejemplo
			return imagen;
		}

	}

	@Override
	protected void buildFields() {
		imageAdapter = new ImagenesAdaptador();
		contador = 0;
		// bitmapArray.add(myBitMap);
		// miImageView = new ImageView(caller.getForm().getActivity());
		// miImageView.setImageResource(imagenes[0]);
		// String ruta = Environment.getExternalStorageDirectory().getPath() +
		// "/encred2015/Pictures/camara_1.jpg";
		// String ruta = RUTA_FOTOS + "/camara_";
		rutaTmp=CameraClass.RUTA_FOTOS;
		String ruta = RUTA_FOTOS + prefijoFoto + "_";

		CameraClass.RUTA_FOTOS = RUTA_FOTOS;
		Log.e("", "ruta recuperacion =" + ruta);
		FileInputStream in;
		BufferedInputStream buf;

//		for (int i = 0; i < 5; i++) {
//			String rutaf = ruta + i + ".jpg";
//			Log.e("", "rutaf=" + rutaf);
//			try {
//				in = new FileInputStream(rutaf);
//				buf = new BufferedInputStream(in);
//				byte[] bMapArray = new byte[buf.available()];
//				buf.read(bMapArray);
//				Bitmap bMap = BitmapFactory.decodeByteArray(bMapArray, 0,
//						bMapArray.length);
//				// misBitmap[0]=bMap;
//				bitmapArray.add(bMap);
//				// miImageView.setImageBitmap(bMap);
//				if (in != null) {
//					in.close();
//				}
//				if (buf != null) {
//					buf.close();
//				}
//				contador++;
//				Log.e("", "contador=" + contador);
//			} catch (Exception e) {
//				Log.e("Error reading file", e.toString());
//			}
//
//		}		
		mCustomCamera = new CustomCamera(getActivity());		
		mCustomCamera.setPictureTakenListener(this);
		mCustomCamera.setResolutionCamera(resolucion);
		btnCamera = new ImageComponent(getParent().getActivity(),
				R.drawable.btn_camera2, R.drawable.btn_camera).size(200, 60);
		btnCamera.setBackgroundColor(getResources()
				.getColor(R.color.WhiteSmoke));
		btnCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (contador >= 3) {
					ToastMessage.msgBox(
							GaleriaDialog.this.getActivity(),
							"Ya se tomaron 3 fotos.",
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
					return;
				}
				if (!mCustomCamera.canTakePicture()
						&& (!"".equals(RUTA_FOTOS) && RUTA_FOTOS != null)) {
					ToastMessage.msgBox(
							GaleriaDialog.this.getActivity(),
							"No se pueden tomar fotos en estos momentos.\nReinicie la aplicaci�n.",
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				} else {
					Integer contador2;
					if (contador == 4) {
						return;
					} else {
						contador2 = contador + 1;
					}

					Log.e("", "contador 1 " + contador);

					String fotoName = prefijoFoto + "_" + (contador2);
					// mCustomCamera.startCamera( fotoName + ".jpg", true);
					Log.e("", "fotoName" + fotoName);

					// mCustomCamera.
					mCustomCamera.startCamera(fotoName + ".jpg", true);
				}
			}
		});
		miGaleria = new Gallery(caller.getForm().getActivity());
		miGaleria.setAdapter(imageAdapter);
		miGaleria.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				Log.e("", "setOnLongClickListener");
				return false;
			}
		});

		// con este listener, s�lo se mostrar�an las im�genes sobre las que se
		// pulsa
		miGaleria.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Log.e("", "onItemClick");
				Log.e("", "arg2" + arg2);
				Log.e("", "arg3" + arg3);
				// miImageView.setImageDrawable(imagenes[arg2].);
				// miImageView.setImageBitmap(Bitmap.decodeSampledBitmapFromResource(getResources(),
				// imagenes[position], 300, 0));
				// miImageView.setImageResource(imagenes[arg2]);
				mostrarImagen((Bitmap) imageAdapter.getItem(arg2));
			}
		});

		// mapa = new
		// GifView(caller.getForm().getActivity()).gif(R.drawable.mapa);
		lblTitulo = new LabelComponent(caller.getForm().getActivity(), estilo)
				.size(MATCH_PARENT, MATCH_PARENT).text(R.string.galeria_titulo)
				.textSize(21).centrar();
		if (estiloBoton != 0) {
			btnAceptar = new ButtonComponent(caller.getForm().getActivity(),
					estiloBoton).text(R.string.btnAceptar).size(200, 60);
		} else {
			btnAceptar = new ButtonComponent(caller.getForm().getActivity())
					.text(R.string.btnAceptar).size(200, 60);
		}
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				bitmapArray.get(miGaleria.getSelectedItemPosition());				
				//File f = moverImagen(miGaleria.getSelectedItemPosition()+1);
//				caller.pictureChosen(bitmapArray.get(miGaleria.getSelectedItemPosition()), f);	
				if (AdapterView.INVALID_POSITION != miGaleria.getSelectedItemPosition()) {
					caller.pictureChosen((Bitmap)miGaleria.getSelectedItem(), new File(imageAdapter.getItemRuta(miGaleria.getSelectedItemPosition())));
				}
				GaleriaDialog.this.dismiss();
			}
		});
		if (estiloBoton != 0) {
			btnCancelar = new ButtonComponent(caller.getForm().getActivity(),
					estiloBoton).text(R.string.btnCancelar).size(200, 60);
		} else {
			btnCancelar = new ButtonComponent(caller.getForm().getActivity())
					.text(R.string.btnCancelar).size(200, 60);
		}
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// running = false;
				// map = null;
				GaleriaDialog.this.dismiss();
			}
		});
	}

	public File moverImagen(int i) {

		String ruta = RUTA_FOTOS + prefijoFoto + "_";
//		CameraClass.RUTA_FOTOS = RUTA_FOTOS;

		String desde = ruta + i + ".jpg";
		String hasta = rutaTmp+prefijoFoto+ ".jpg";

		Log.e("", "desde =" + desde);
		Log.e("", "hasta =" + hasta);

		File inFile = new File(desde);
		File outFile = new File(hasta);
		try {
			Util.copy(inFile, outFile);
		} catch (IOException e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
			outFile = null;
		}
		return outFile;
//		try {
//
//			FileInputStream in = new FileInputStream(inFile);
//			FileOutputStream out = new FileOutputStream(outFile);
//
//			int c;
//			while ((c = in.read()) != -1)
//				out.write(c);
//
//			in.close();
//			out.close();
//
////			File file = new File(desde);
////			if (file.exists()) {
////				file.delete();
////			}
//
//		} catch (Exception e) {
//			System.err.println("Hubo un error de entrada/salida!!!");
//		}

	}

	public void mostrarImagen(Bitmap bitmap) {
		Log.e("", "FragmentManager");
		FragmentManager fm = this.getFragmentManager();
		ImagenDialog dialogo = ImagenDialog.newInstance(this, bitmap,
				estiloBoton);
		dialogo.setAncho(MATCH_PARENT);
		dialogo.show(fm, "gpsDialog");
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		super.onContextItemSelected(item);
		AdapterView.AdapterContextMenuInfo oMenuInfo = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		long id = oMenuInfo.id;
		return true;
	}

	public void cargarDatos() {
	}

	@Override
	public void pictureTaken(Bitmap bitmap, File filePath) {

		Log.e("", "filePath " + filePath.getAbsolutePath());

		if (contador == 4) {
			contador = 0;
		} else {
			contador++;
		}

		Log.e("", "contador 2 =" + contador);

		String ruta = RUTA_FOTOS + prefijoFoto + "_";

		String rutaf = ruta + contador + ".jpg";
		Log.e("", "rutaffffffffff 2 = " + rutaf);
//		FileInputStream in;
//		BufferedInputStream buf;

		try {
			if (!filePath.getAbsolutePath().equals(rutaf)) {
				File outFile = new File(rutaf);
				try {
					Util.copy(filePath, outFile);
					rutaf = filePath.getAbsolutePath();
				} catch (IOException e) {
					Log.e(getClass().getSimpleName(), e.getMessage());
					outFile = null;
				}
			}
//			in = new FileInputStream(rutaf);
//			buf = new BufferedInputStream(in);
//			byte[] bMapArray = new byte[buf.available()];
//			buf.read(bMapArray);
//			Bitmap bMap = BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
//			bitmapArray.add(bMap);
//			BitmapFactory.Options options = new BitmapFactory.Options();
////			options.inSampleSize = 8;
//			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//			bitmap = BitmapFactory.decodeFile(rutaf, options);
//			miImageView.setImageBitmap(bMap);
//			if (in != null) {
//				in.close();
//			}
//			if (buf != null) {
//				buf.close();
//			}
			imageAdapter.addBitmap(bitmap, rutaf);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), "Error: " + e.getMessage());
		}
		Log.e("", "bitmapArray=" + imageAdapter.getCount());
		miGaleria.setAdapter(imageAdapter);
	}

}