package gob.inei.dnce.components;

import gob.inei.dnce.components.TextBoxField.INPUT_TYPE;
import gob.inei.dnce.util.Util;
import android.content.Context;

public class TextAutCompleteField extends TextAutoCompleteField {
	
	private String componentValue;
	public final static int TEXTO = Util.MyTextWatcher.TEXTO;
	
	public TextAutCompleteField(Context context) {
		super(context);
//		this(context, TEXTO_NUMERO);
//		super(context);
//		this.setInputType(TEXTO_NUMERO);
//		setTipoTexto(TEXTO_NUMERO);
		
		// TODO Auto-generated constructor stub
	}
	
	public TextAutCompleteField size(int alto, int ancho) { super.setSize(alto, ancho); return this;}
	public TextAutCompleteField readOnly() { readOnly(true); return this;}
	public TextAutCompleteField readOnly(boolean lock) { this.setReadOnly(lock); return this;}
	public TextAutCompleteField callback(String callback) { this.setCallback(callback); return this; }
	public TextAutCompleteField callbackOnFocus(String callback) { this.setCallbackOnFocus(callback); return this; }
	
}
