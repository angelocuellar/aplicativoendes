package gob.inei.dnce.adapter;

import gob.inei.dnce.interfaces.Spinneable;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//public class EntitySpinnerAdapter<T extends Entity> extends ArrayAdapter<T> {
public class EntitySpinnerAdapter<T> extends ArrayAdapter<T> {
	
	private List<T> entities;
	private Context context;

	public EntitySpinnerAdapter(Context context, int textViewResourceId,
			List<T> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		entities = new ArrayList<T>();
		//entities.add(null);
		entities.addAll(objects);
	}

	public List<T> getEntities() {
		return entities;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView label = this.getItem(position) == null ? new TextView(getContext()) : (TextView) super
				.getView(position, convertView, parent);
		label.setTextColor(Color.BLACK);
		label.setTextSize(18);
		label.setHeight(55);
		label.setGravity(Gravity.CENTER_VERTICAL|Gravity.START);
		if (this.getItem(position) == null) {
			label.setText("-- SELECCIONE --");
		} else {
			if (this.getItem(position) instanceof Spinneable) {
				label.setText(((Spinneable)this.getItem(position)).getSpinnerLabel());				
			} else {
				label.setText(this.getItem(position).toString());
			}
		}
		final float scale = this.context.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			label.setHeight(70);
//			LayoutParams lp = label.getLayoutParams();
//			int alto = lp.height;
//			int ancho = lp.width;		
//			Log.e(getClass().getSimpleName(), "Alto [" + alto + "]" + "Ancho [" + ancho + "]");
//			alto = (int) (alto * scale + 0.5f);
//			ancho = (int) (ancho * scale + 0.5f);
//			Log.e(getClass().getSimpleName(), "Alto [" + alto + "]" + "Ancho [" + ancho + "]");
//			label.setHeight(alto);
//			label.setWidth(ancho);
//			label.setLayoutParams(new android.widget.AbsListView.LayoutParams(android.widget.AbsListView.LayoutParams.WRAP_CONTENT, android.widget.AbsListView.LayoutParams.WRAP_CONTENT));
		}
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView label = this.getItem(position) == null ? new TextView(getContext()) : (TextView) super
				.getView(position, convertView, parent);
		label.setTextColor(Color.BLACK);
		label.setTextSize(20);
		label.setHeight(55);
		label.setGravity(Gravity.START|Gravity.CENTER_VERTICAL);
		label.setTypeface(null, Typeface.BOLD);
		if (this.getItem(position) == null) {
			label.setText("-- SELECCIONE --");
		} else {
			if (this.getItem(position) instanceof Spinneable) {
				label.setText(((Spinneable)this.getItem(position)).getSpinnerLabel());
				if (((Spinneable)this.getItem(position)).isTitle()) {
					label.setBackgroundColor(Color.GRAY);
					label.setTextColor(Color.WHITE);
					label.setGravity(Gravity.CENTER_HORIZONTAL);	
				}
			} else {
				label.setText(this.getItem(position).toString());
			}
		}
		final float scale = this.context.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			label.setHeight(70);
//			LayoutParams lp = label.getLayoutParams();
//			int alto = lp.height;
//			int ancho = lp.width;		
//			Log.e(getClass().getSimpleName(), "Alto [" + alto + "]" + "Ancho [" + ancho + "]");
//			alto = (int) (alto * scale + 0.5f);
//			ancho = (int) (ancho * scale + 0.5f);
//			Log.e(getClass().getSimpleName(), "Alto [" + alto + "]" + "Ancho [" + ancho + "]");
//			label.setHeight(alto);
//			label.setWidth(ancho);
//			label.setLayoutParams(new android.widget.AbsListView.LayoutParams(android.widget.AbsListView.LayoutParams.WRAP_CONTENT, android.widget.AbsListView.LayoutParams.WRAP_CONTENT));
		}
		return label;
	}
	
	@Override
	public T getItem(int position) {
		return entities.get(position);
	}

	@Override
	public boolean isEnabled(int position) {
		T it = this.getItem(position); 
		if (it instanceof Spinneable)
			return !((Spinneable)it).isTitle();
		return true;
	}
	
	
}

