package gob.inei.dnce.adapter;

import java.util.Vector;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


public abstract class DrawerAdapter extends BaseAdapter {

	protected Vector<String> lista;
	protected Activity actividad;
	
	public DrawerAdapter (Vector<String> lista, Activity actividad){
		this.lista = lista;
		this.actividad = actividad;
	}
	
	@Override
	public int getCount() {		
		return lista.size();
	}

	@Override
	public Object getItem(int position) {		
		return lista.elementAt(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public abstract View getView(int position, View convertView, ViewGroup parent);	
}
