package gob.inei.dnce.adapter;

import gob.inei.dnce.components.FragmentForm;

import java.util.List;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
	private List<FragmentForm> fragments;

    public MyFragmentPagerAdapter(FragmentActivity activity, List<FragmentForm> fragments) {
        super(activity.getSupportFragmentManager());
        this.fragments = fragments;
    }

    @Override
    public FragmentForm getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
