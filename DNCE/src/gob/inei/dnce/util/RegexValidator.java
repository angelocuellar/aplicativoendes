package gob.inei.dnce.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidator {
	
	public static final String TELEFONO_01 = "(([,#|\\*])?[0-9]){6,15}";
	public static final String PISO = "(([,A|S|M])|(2[0-5]{1})|(1[0-9]{1})|([1-9]{1}))"; 
//	public static final String PUERTA = "((SN)|[0-9]{1,6})";
	public static final String PUERTA = "^((SN)|([1-9]{1})|([1-9]{1}[0-9]{1})|([1-9]{1}[0-9]{1}[0-9]{1})|([1-8]{1}[0-9]{1}[0-9]{1}[0-9]{1})|(9[0-9]{1}[0-8]{1}[0-9]{1})|(9[0-9]{1}9[0-8]{1}))$";
	public static final String EMAIL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"; 
	public static final String IP = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	public static final String IMAGE_FILE_NAME = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
	
	public static final String PISO_1_30 = "(([,A|S|M])|(3[0]{1})|(2[0-9]{1})|(1[0-9]{1})|([1-9]{1}))";
	public static final String PISO_1_20 = "(([,A|M])|(S[1-5]{1})|(2[0]{1})|(1[0-9]{1})|([1-9]{1}))";
	public static final String PISO_1_15 = "(([,A|M])|(S[1-5]{1})|(1[0-5]{1})|([1-9]{1}))";
	public static final String TELEFONOSUSALUD = "(([,#|\\*|\\-])?[0-9]){6,15}";
	public static final String DNIA = "([0-9]{1}|[9]{2})|([A-Z])";
	public static final String BLOCK = "([A-Z|�]|[1-9])*";
	public static final String BLOCK2 = "([A-Z|�]|[1-10])*";
	public static final String NOMBRE_VIA = "([\\s]|[A-Z]|[1-9]+[0]*|[^(\\s[0]{2,})])*";
	public static final String TELEFONO_ENEDU = "(([,#|\\*])?[0-9]|[^([0]{6,})]){6,15}";
//	public static final String NOMBRE_IE = "([\\s]|[A-Z]|[1-9]*|[\\(|[A-Z]|[1-9]|\\)]";

	private RegexValidator() {
	}
	
	public static boolean validarString(String regex, String... valores) {
		if (valores == null) {
			return false;
		}
		for (int i = 0; i < valores.length; i++) {
			if (!validarString(regex, valores[i])) {
				return false;
			}
		}
		return true;
	}
	
	private static boolean validarString(String regex, String valor) {
		Pattern pattern = Pattern.compile(regex);
	    Matcher matcher = pattern.matcher(valor);
	    if (matcher.matches()){
		      return true; 
		    } 
//	    if (matcher.find()){
//	      return true; 
//	    } 
	    return false; 
	}

}
