package gob.inei.dnce.util;

import gob.inei.dnce.R;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.ImageViewField;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.RadioGroupField;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextBoxField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.util.encrypt.CriptoTripleDESBasico;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class Util {
	
	private static final String CLASS_TAG = "Util";
	
	/************************/
	/**
	 * 
	 * @param val1 Valor 1
	 * @param val2 Valor 2
	 * @param oper Operador
	 * @param todos Bandera para saber si la condicion es que todos cumplan
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean cumpleAlguno(String val1, LinkedList val2, String oper, boolean todos){
		boolean continuar;
		if(val2.size()==0) return false;
		
		if(todos==true){
			continuar = true;
			for(int i=0; i<val2.size(); i++){
				continuar = continuar && comparar(val1, val2.get(i), oper, val2.get(i).getClass());
			}
			return continuar;
		}
		else{
			for(int i=0; i<val2.size(); i++){
				Log.d("-----------", "cumpliendo-" + val2.get(i).toString());
				if(comparar(val1, val2.get(i), oper, val2.get(i).getClass())) return true;
			}
			return false;
		}
	}

	public static <T> boolean comparar(String val1, T val2, String oper, Class type){
		//Log.d(CLASS_TAG, type.toString());
		if(val1 == null || val2==null) return false;
		
		if(type == String.class){
			if(oper.equals("=")){
				//Log.d(CLASS_TAG, oper);
				return val1.toString().equals(val2.toString());
			}
			else if(oper.equals("!=")){
				//Log.d(CLASS_TAG, oper);
				return !val1.toString().equals(val2.toString());
			}
			return false;
		}
		else if(type == Integer.class){
			//si val1 es una cadena vacia entonces no cumplira ninguna condicion
			if(esVacio(val1)) return false;
			//si val1 o val2 no es un numero no hace falta ninguna comparacion, de cualquier forma no
			//cumplira ninguna de las condiciones
			if( Double.isNaN(Double.valueOf(val1)) || Double.isNaN(Double.valueOf(val2.toString())) ) return false;
			
			if(oper.equals("=")){
				return Integer.valueOf(val1) == Integer.valueOf(val2.toString());
			}
			else if(oper.equals("!=")){
				return Integer.valueOf(val1) != Integer.valueOf(val2.toString());
			}
			else if(oper.equals(">")){
				return Integer.valueOf(val1) > Integer.valueOf(val2.toString());
			}
			else if(oper.equals("<")){
				return Integer.valueOf(val1) < Integer.valueOf(val2.toString());
			}
			else if(oper.equals(">=")){
				return Integer.valueOf(val1) >= Integer.valueOf(val2.toString());
			}
			else if(oper.equals("<=")){
				return Integer.valueOf(val1) <= Integer.valueOf(val2.toString());
			}
		}
		return false;
	}
	
	public static boolean esVacio(Integer i){
		if (i == null) return true;
		return esVacio(i.toString());
	}
	
	public static boolean esVacio(BigDecimal i){
		if (i == null) return true;
		return esVacio(i.toString());
	}
	
	public static boolean esVacio(EditText editText){
		if(editText == null) return true;
		return esVacio(editText.getText().toString());
	}
	
	public static boolean esVacio(RadioGroupField rg){
		if(rg == null) return true;
		if (rg.getTagSelected() == null) return true; 
		return esVacio(rg.getTagSelected().toString());
	}
	
	public static boolean esVacio(RadioGroupOtherField rg){
		if(rg == null) return true;
		if (rg.getTagSelected() == null) return true; 
		return esVacio(rg.getTagSelected().toString());
	}
	
	public static boolean esVacio(CheckBoxField chb){
		if (chb.getCheckedTag() == null) return true; 
		return esVacio(chb.getCheckedTag().toString());
	}
	
	public static boolean esVacio(String text){
		if (text == null) {
			return true;
		}
		return text.trim().length()==0;
//		return text.replaceAll("\\s\\u0020 ", "").equals("");
	}
	
	public static EditText editTextVacio(List<EditText> views){
		if(views == null) return null;
		for(EditText view : views){
			if(esVacio(view.getText().toString())) return view;
		}
		views = null;
		return null;
	}
	
	public static <T>T verificaNoVacio(List<T> views, Class<?> c){
		if(views == null) return null;
		return verificaViews(views, c, 1).valueAt(0);
	}
	public static <T> int verificaNoVacioCount(List<T> views, Class<?> c){
		if(views == null) return -1;
		return verificaViews(views, c, 2).keyAt(0);
	}
	private static <T> SparseArray<T> verificaViews(List<T> views, Class<?> c, int cod){
		int cont = 0;
		SparseArray<T> spa = new SparseArray<T>();
		if(views.size()>0){
			for(T view : views){
				try{
					Class<?> objclass=view.getClass();
					if(objclass==c||c==null){
						if(objclass==TextField.class||objclass==IntegerField.class||objclass==DecimalField.class){
							if(!esVacio(((EditText)view).getText().toString())) cont++;
						} else if(objclass == CheckBoxField.class){
							if(((CheckBoxField)view).getCheckedTag() != null && !(((CheckBoxField)view).getCheckedTag().equals(""))) {
								if (Integer.valueOf(((CheckBoxField)view).getCheckedTag().toString()).compareTo(Integer.valueOf("1"))>=0) cont++;
							}
						} else if(objclass == RadioGroupOtherField.class){
							if(((RadioGroupOtherField)view).getTagSelected() != null) {
								if (((RadioGroupOtherField)view).getTagSelected().toString().equals("1")) cont++;
							}
						}
						if((cod == 1 && cont>0) || (cod == 2 && view.equals(views.get(views.size()-1)))) {
							spa.put(cont, view); break;
						}
					} else continue;
				} catch(NullPointerException e){
					continue;
				}
			}
		}
		return spa;
	}
	
	public static EditText editTextNoVacio(List<EditText> views){
		if(views == null) return null;
		for(EditText view : views){
			if(!esVacio(view.getText().toString())) return view;
		}
		views = null;
		return null;
	}
	
	public static CheckBoxField checkboxVacio(List<CheckBoxField> views){
		if(views == null) return null;
		for(CheckBoxField view : views){
			if(view.getCheckedTag() == null) return view;
			else if (view.getCheckedTag().toString().equals("0")) return view;
		}
		views = null;
		return null;
	}
	
	public static CheckBoxField checkboxNoVacio(List<CheckBoxField> views, int count){
		CheckBoxField chb=null, chba=null;
		if(count>views.size())count=views.size();
		for(int c=1;c<=count+1;c++){
			if(c==count+1) chba=chb;
			chb=checkboxNoVacio(views);
			if(c==count+1){
				if(chb!=null) return null;
				return chba;
			}
			if(chb==null) return null;
			views.remove(chb);
		}
		return chb;
	}
	
	public static CheckBoxField checkboxNoVacio(List<CheckBoxField> views){
		if(views == null) Log.d(CLASS_TAG, "views is null");
		if(views == null) return null;
		for(CheckBoxField view : views){
			if(view.getCheckedTag() != null) {
				if (view.getCheckedTag().toString().equals("1")) return view;
			}
		}
		views = null;
		return null;
	}
	
	public static boolean isCheckBoxEmpty(List<CheckBoxField> views){
		if(views == null) {
			return true;
		}
		for(CheckBoxField view : views){
			if( view.isChecked() ) {
				return false;
			}
		}
		return true;
	}
	
	public static RadioGroupField radiogroupVacio(List<RadioGroupField> views){
		if(views == null) return null;
		for(RadioGroupField view : views){
			if(view.getTagSelected() == null) return view;
		}
		views = null;
		return null;
	}
	
	public static RadioGroupOtherField radiogroupOtherVacio(List<RadioGroupOtherField> views){
		if(views == null) return null;
		for(RadioGroupOtherField view : views){
			if(view.getTagSelected() == null) return view;
		}
		views = null;
		return null;
	}
	
	public static RadioGroupField radiogroupNoVacio(List<RadioGroupField> views){
		if(views == null) return null;
		for(RadioGroupField view : views){
			if(view.getTagSelected() != null) return view;
		}
		views = null;
		return null;
	}
	
	
	public static <K, V> HashMap<K, V> getHashMap(K oper1, V values1){
		HashMap<K, V> hashMap = new HashMap<K, V>(1);
		hashMap.put(oper1, values1);
		return hashMap;
	}
	
	public static <K, V> HashMap<K, V> getHashMap(K oper1, V values1, K oper2, V values2){
		HashMap<K, V> hashMap = new HashMap<K, V>(1);
		hashMap.put(oper1, values1);
		hashMap.put(oper2, values2);
		return hashMap;
	}
	
	public static <T>List<HashMap<T, T>> getListHashMap(T... values){
		List<HashMap<T, T>> lsthm=new ArrayList<HashMap<T,T>>();
		T key=null, value=null;
		if(values!=null){
			for(int i=0;i<values.length;i++){
				if(i%2==0) key=values[i];
				if(i%2!=0) {
					value=values[i];
					lsthm.add(getHashMap(key, value));
				}
			}
		}
		return lsthm;
	}
	
	public static <T extends Object>HashMap<T, T> getHMObject(T... values){
		HashMap<T, T> hashMap = new HashMap<T, T>();
		T key=null; T value=null;
		if(values!=null){
			for(int i=0;i<values.length;i++){
				if(i%2==0) key=values[i];
				if(i%2!=0) {
					value=values[i];
					hashMap.put(key, value);
				}
			}
		}
		return hashMap;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] extractArraytoArray(T[] campos, T... exceptionCampos){
		return convertListtoArray(convertArraytoList(campos), exceptionCampos);
	}
	public static <T> List<T> convertArraytoList(T[] campos, T... exceptionCampos){
		boolean flag;
		List<T> lst=new ArrayList<T>();
		if (campos!=null) {
			for(T campo:campos){
				flag = true;
				for(T exc : exceptionCampos){
					if(campo.equals(exc)) {
						flag = false;
						break;
					}
				}
				if(!flag) continue;
				lst.add(campo);
			}
		}
		return lst;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] convertListtoArray(List<T> campos, T... exceptionCampos){
		Class<?> clase=null;
		if (campos.size()>0) {
			clase = campos.get(0).getClass();
			for(T campo:campos){
				for(T exc : exceptionCampos){
					if(campo.equals(exc)) {
						campos.remove(campo);
						break;
					}
				}
			}
		}
		return clase==null?null:campos.toArray((T[])Array.newInstance(clase, campos.size()));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <E> LinkedList getLinkedList(E... value){
		LinkedList linkedList = new LinkedList();
		for(E val:value ){
			linkedList.add(val);
		}
		return linkedList;
	}
	
	public static <E> List<E> getListList(E... value){
		List<E> listList = new ArrayList<E>();
		for(E val:value ){
			listList.add(val);
		}
		return listList;
	}
	
	public static BigDecimal getBigDecimal(double val){
		return getBigDecimal(val, null);
	}
	
	/**
	 * Tomado de una soluci&oacute;n planteada por Joachim Sauer en 
	 * <a href="http://stackoverflow.com/questions/80476/how-to-concatenate-two-arrays-in-java/784842#784842">stackoverflow</a> 
	 * 
	 * @param first
	 * @param rest
	 * @return
	 */
	public static <T> T[] concatAll(T[] first, T[]... rest) {
		int totalLength = first.length;
		for (T[] array : rest) {
			totalLength += array.length;
		}
		T[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (T[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
	
	public static void zip(String rutaComprimido, File... archivos)
			throws IOException {
		zip(null, rutaComprimido, archivos);
	}	
	
	
	public static void zipEncriptado(String key, String rutaComprimido, File... archivos)
			throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		int BUFFER = 2048;
		FileOutputStream dest = new FileOutputStream(rutaComprimido);
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(dest));
		for (int i = 0; i < archivos.length; i++) {
			File archivo = archivos[i];
			
			FileInputStream fi = new FileInputStream(archivo);			
			BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);
			byte data[] = new byte[BUFFER];
			ZipEntry entry = new ZipEntry(archivo.getName());
			zos.putNextEntry(entry);
			int count;
			
			CriptoTripleDESBasico crip = new CriptoTripleDESBasico(key);
			while ((count = origin.read(data, 0, BUFFER)) != -1) {
				data = crip.encryptToBytes(data);
				zos.write(data, 0, count);
			}			
			origin.close();
		}
		zos.close();
	}
	
	public static void unzipEncriptador(String key, final String sourceZipFile,	final String destinationDir) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		String path = destinationDir;
		File zipFile = new File(sourceZipFile);
		
		int BUFFER = 2048;		
		InputStream is;
		ZipInputStream zis;
		String filename;
		is = new FileInputStream(zipFile);
		zis = new ZipInputStream(new BufferedInputStream(is));
		ZipEntry ze;
		byte[] buffer = new byte[BUFFER];
		int count;
		while ((ze = zis.getNextEntry()) != null) {
			filename = ze.getName();
			if (ze.isDirectory()) {
				File fmd = new File(path + filename);
				fmd.mkdirs();
				continue;
			}
//			Log.e("Util.descomprimirArchivo", "ruta: "+path+filename);
			FileOutputStream fout = new FileOutputStream(path + File.separator + filename);
			
			CriptoTripleDESBasico crip = new CriptoTripleDESBasico(key);
			while ((count = zis.read(buffer)) != -1) {
				buffer = crip.decryptToBytes(buffer);
				fout.write(buffer, 0, count);
			}
			fout.close();
			zis.closeEntry();
		}
		zis.close();
	}
	
	public static void zip(String password, String rutaComprimido, File... archivos)
			throws IOException {
		int BUFFER = 2048;
		FileOutputStream dest = new FileOutputStream(rutaComprimido);
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(dest));
		for (int i = 0; i < archivos.length; i++) {
			File archivo = archivos[i];
			FileInputStream fi = new FileInputStream(archivo);
			BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);
			byte data[] = new byte[BUFFER];
			ZipEntry entry = new ZipEntry(archivo.getName());
			zos.putNextEntry(entry);
			int count;
			while ((count = origin.read(data, 0, BUFFER)) != -1) {
				zos.write(data, 0, count);
			}			
			origin.close();
		}
		zos.close();
		if (password == null) {
			return;
		}
//		String zipFileName = new File(rutaComprimido).getAbsolutePath();
//		String tempZipFileName = new File(rutaComprimido).getAbsoluteFile()
//				.getParent() + File.separator + "tempencrypted.zip";
//		try {
//			AESEncrypterJCA encrypter = new AESEncrypterJCA();
//			AesZipFileEncrypter enc = new AesZipFileEncrypter(tempZipFileName, encrypter);
//			enc.add(new File(zipFileName), password);
//			new File(zipFileName).delete();
//			new File(tempZipFileName).renameTo(new File(zipFileName));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	private static void descomprimirArchivo(String path, File zipFile)
			throws IOException {
//		Log.e("Util.descomprimirArchivo", "ruta: "+path);
		int BUFFER = 2048;		
		InputStream is;
		ZipInputStream zis;
		String filename;
		is = new FileInputStream(zipFile);
		zis = new ZipInputStream(new BufferedInputStream(is));
		ZipEntry ze;
		byte[] buffer = new byte[BUFFER];
		int count;
		while ((ze = zis.getNextEntry()) != null) {
			filename = ze.getName();
			if (ze.isDirectory()) {
				File fmd = new File(path + filename);
				fmd.mkdirs();
				continue;
			}
//			Log.e("Util.descomprimirArchivo", "ruta: "+path+filename);
			FileOutputStream fout = new FileOutputStream(path + File.separator + filename);
			while ((count = zis.read(buffer)) != -1) {
				fout.write(buffer, 0, count);
			}
			fout.close();
			zis.closeEntry();
		}
		zis.close();
	}
	
	public static void unzip(final String sourceZipFile,
			final String destinationDir) throws IOException {
		unzip(sourceZipFile, destinationDir, null);
	}
	
	/**
	 * This method is used to unzip a password protected zip file.
	 * 
	 * @param sourceZipFile
	 *            of type String indicating the source zip file
	 * @param destinationDir
	 *            of type String indicating the directory where the zip file
	 *            will be extracted.
	 * @param password
	 *            of type String indicating the password.
	 * @throws IOException 
	 */
	public static void unzip(final String sourceZipFile,
			final String destinationDir, final String password) throws IOException {
		if (password == null) {
			descomprimirArchivo(destinationDir, new File(sourceZipFile));
			return;
		}
//		RandomAccessFile randomAccessFile = null;
//		ISevenZipInArchive inArchive = null;
//		try {
//			randomAccessFile = new RandomAccessFile(sourceZipFile, "r");
//			inArchive = SevenZip.openInArchive(null, // autodetect archive type
//					new RandomAccessFileInStream(randomAccessFile));
//			// Getting simple interface of the archive inArchive
//			ISimpleInArchive simpleInArchive = inArchive.getSimpleInterface();
//			for (final ISimpleInArchiveItem item : simpleInArchive
//					.getArchiveItems()) {
//				final int[] hash = new int[] { 0 };
//				if (!item.isFolder()) {
//					ExtractOperationResult result;
//					result = item.extractSlow(new ISequentialOutStream() {
//						public int write(final byte[] data)
//								throws SevenZipException {
//							try {
//								if (item.getPath().indexOf(File.separator) > 0) {
//									String path = destinationDir
//											+ File.separator
//											+ item.getPath().substring(
//													0,
//													item.getPath().lastIndexOf(
//															File.separator));
//									File folderExisting = new File(path);
//									if (!folderExisting.exists())
//										new File(path).mkdirs();
//								}
//								OutputStream out = new FileOutputStream(
//										destinationDir + File.separator
//												+ item.getPath());
//								out.write(data);
//								out.close();
//							} catch (Exception e) {
//								e.printStackTrace();
//							}
//							hash[0] |= Arrays.hashCode(data);
//							return data.length; // Return amount of proceed data
//						}
//					}, password); // / password.
//					if (result == ExtractOperationResult.OK) {
//						System.out.println(String.format("%9X | %s", hash[0],
//								item.getPath()));
//					} else {
//						System.err.println("Error extracting item: " + result);
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			if (inArchive != null) {
//				try {
//					inArchive.close();
//				} catch (SevenZipException e) {
//					System.err.println("Error closing archive: " + e);
//					e.printStackTrace();
//				}
//			}
//			if (randomAccessFile != null) {
//				try {
//					randomAccessFile.close();
//				} catch (IOException e) {
//					System.err.println("Error closing file: " + e);
//					e.printStackTrace();
//				}
//			}
//		}
	}
	
	public static void deleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory()) {
	        for (File child : fileOrDirectory.listFiles()) {
	            deleteRecursive(child);
	        }
	    }
	    fileOrDirectory.delete();
	}
	
	public static <T> ArrayList<T> removeNulls(ArrayList<T> arr){		
		for(int i=arr.size()-1; i>=0; i--){
			if(arr.get(i) == null) arr.remove(i);
		}
		
		return arr;
	}
	
	public static <T> boolean isCorrelative(ArrayList<T> arr){
		
		for(int i=0; i<=arr.size()-1; i++){
			if (i==arr.size()-1) continue;
			
			Integer a = Integer.valueOf(arr.get(i).toString()),
					b = Integer.valueOf(arr.get(i+1).toString());
			if( Math.abs(a-b)!=1 ) return false;
		}
		
		return true;
	}
	
	public static boolean isCorrelative(Object[] arr){
		
		for(int i=0; i<=arr.length-1; i++){
			if (i==arr.length-1) continue;
			
			Integer a = Integer.valueOf(arr[i].toString()),
					b = Integer.valueOf(arr[i+1].toString());
			if( Math.abs(a-b)!=1 ) return false;
		}
		
		return true;
	}
	
	public static boolean between(Integer num, Integer[]... range){
		
		boolean _between = true;
		for(int i=0; i<=range.length-1; i++){
			_between = _between && between(num, range[i]);
		}
		
		return _between;
	}
	
	public static boolean between(Integer num, Integer[] range){
		
		if( num==null || range==null ) return false;
		else if( range.length < 2 ) return false;
		else if( range[0]==null || range[1]==null ) return false;
		
		Integer min = Math.min(range[0], range[1]);
		Integer max = Math.max(range[0], range[1]);
		
		if( num<min || num>max ){
			return false;
		}
		
		return true;
	}
	
	public static boolean between(Integer num, Integer min, Integer max){
		
		if( num==null || min==null || max==null ) return false;
		
		Integer _min = Math.min(min, max);
		Integer _max = Math.max(min, max);
		min = _min;
		max = _max;
		
		if( num<min || num>max ) return false;
		
		return true;
	}
	
	/**
	 * 
	 * @param val
	 * @param precision posiciones decimales
	 * @return
	 */
	public static BigDecimal getBigDecimal(double val, Integer precision){
		return getBigDecimal(new BigDecimal(val), precision);
	}
	
	public static BigDecimal getBigDecimal(Integer ent, String dec, Integer precision){
		ent = ent==null||Util.esVacio(ent)?0:ent;
		dec = dec==null||Util.esVacio(dec)?"0":dec;
		return getBigDecimal(new BigDecimal(ent.toString().concat(".").concat(dec)), precision);
	}
	
	public static BigDecimal getBigDecimal(Integer ent, Integer dec, Integer precision){
		ent = ent==null||Util.esVacio(ent)?0:ent;
		dec = dec==null||Util.esVacio(dec)?0:dec;
		if (precision == null) {
			return new BigDecimal(ent.toString().concat(".").concat(dec.toString()));
		} else {
			return getBigDecimal(new BigDecimal(ent.toString().concat(".").concat(dec.toString())), precision);
		}
	}
	
	public static BigDecimal getBigDecimal(Integer ent, Integer dec){
		return getBigDecimal(ent, dec, null);
	}
	
	public static BigDecimal getBigDecimal(String ent, String dec, Integer precision){
		return getBigDecimal(ent, dec, precision, null);
	}

	public static BigDecimal getBigDecimal(String ent, String dec, Integer precision, BigDecimal defaultValue){
		ent = ent==null||Util.esVacio(ent)?"0":ent;
		dec = dec==null||Util.esVacio(dec)?"0":dec;
		return getBigDecimal(new BigDecimal(ent.concat(".").concat(dec)), precision, defaultValue);
	}
	
	public static BigDecimal getBigDecimal(String val){
		return getBigDecimal(val, null, null, null);
	}	
	
	public static BigDecimal getBigDecimal(String val, BigDecimal defaultValue){
		return getBigDecimal(val, null, defaultValue);
	}
	
	/**
	 * 
	 * @param val
	 * @param precision lugares decimales
	 * @return
	 */
	public static BigDecimal getBigDecimal(String val, Integer precision){
		return getBigDecimal(val.equals("")?null:new BigDecimal(val), precision);
	}

	public static BigDecimal getBigDecimal(String val, Integer precision, BigDecimal defaultValue){
		return getBigDecimal(val.equals("")?null:new BigDecimal(val), precision, defaultValue);
	}
	
	public static BigDecimal getBigDecimal(BigDecimal val, Integer precision){
		return getBigDecimal(val, precision, BigDecimal.ROUND_HALF_EVEN, null);
	}
	
	public static BigDecimal getBigDecimal(BigDecimal val, Integer precision, BigDecimal defaultValue){
		return getBigDecimal(val, precision, BigDecimal.ROUND_HALF_EVEN, defaultValue);
	}
	
	public static BigDecimal getBigDecimal(BigDecimal val, Integer precision, int redondeo){
		return getBigDecimal(val, precision, redondeo, null);
	}
	
	public static BigDecimal getBigDecimal(BigDecimal val, Integer precision, int redondeo, BigDecimal defaultValue){		
		if (val == null) return defaultValue;
		if (precision != null) return val.setScale(precision, redondeo);
		return val;
	}
	
	public static BigDecimal getSumaDecimal(int presicion, BigDecimal... valores){
		return getSumaDecimal(presicion, BigDecimal.ROUND_HALF_EVEN, valores);
	}
	
	public static BigDecimal getSumaDecimal(int presicion, int redondeo, BigDecimal... valores){
		if (valores == null) return null;
		BigDecimal resultado=new BigDecimal("0");
		for(BigDecimal val:valores){
			if(val==null) val=BigDecimal.ZERO;
			resultado=resultado.add(val);
		}
		if (presicion != -1) 
			resultado = resultado.setScale(presicion, redondeo);
		return resultado;
	}
	
	public static BigDecimal getSumaDecimal(BigDecimal... valores){
		return getSumaDecimal(-1, valores);
	}
	
	public static BigDecimal getSumaDecimal(EditText... valores){
		if(valores==null) return null;
		List<BigDecimal> lstDec=new ArrayList<BigDecimal>();
		for(EditText val:valores)
			lstDec.add(getBigDecimal(val.getText().toString()));
		return getSumaDecimal(lstDec.toArray(new BigDecimal[lstDec.size()-1]));
	}
	
	public static long getEnteroDesdeBigDecimal(BigDecimal numero) {
		if (numero == null) {
			return 0;
		}
		return numero.longValue();
	} 
	
	public static long getFraccionDesdeBigDecimal(BigDecimal numero) {
		if (numero == null) {
			return 0;
		}
		long decimal = Long.valueOf((numero.subtract(new BigDecimal(numero.longValue())).toString().substring(2))); 
		return decimal;
	}
	
//	public static int llenarDepartamento(Activity activity, UbigeoService service, SpinnerField spinner) {
//		List lista = service.getDepartamentos();
//		AdaptadorUbigeo dataAdapterdpto = new AdaptadorUbigeo(
//				activity, android.R.layout.simple_spinner_item,
//				lista);
//		List<Object> obj = new ArrayList<Object>();
//		for (int i = 0; i < lista.size(); i++) {
//			DepartamentoBE d = (DepartamentoBE) lista.get(i);
//			obj.add(d.getCCDD());
//		}
//		spinner.setAdapterWithKey(dataAdapterdpto,obj);
//		return lista.size();
//	}
//	
//	public static int llenarProvincia(Activity activity, UbigeoService service, SpinnerField spinner, String ccdd) {
//		List lista = service.getProvincias(ccdd);
//		AdaptadorUbigeo dataAdapterprov = new AdaptadorUbigeo(
//				activity, android.R.layout.simple_spinner_item,
//				lista);
//		List<Object> obj = new ArrayList<Object>();
//		for (int i = 0; i < lista.size(); i++) {
//			ProvinciaBE d = (ProvinciaBE) lista.get(i);
//			obj.add(d.ccpp);
//		}
//		spinner.setAdapterWithKey(dataAdapterprov,obj);
//		return lista.size();
//	}
//	
//	public static int llenarDistrito(Activity activity, UbigeoService service, SpinnerField spinner, String ccdd,String ccpp) {
//		List lista = service.getDistritos(ccdd, ccpp);
//		AdaptadorUbigeo dataAdapterdist = new AdaptadorUbigeo(
//				activity, android.R.layout.simple_spinner_item,
//				lista);
//		List<Object> obj = new ArrayList<Object>();
//		for (int i = 0; i < lista.size(); i++) {
//			DistritoBE d = (DistritoBE) lista.get(i);
//			obj.add(d.ccdi);
//		}
//		spinner.setAdapterWithKey(dataAdapterdist,obj);
//		return lista.size();
//	}
	
	//public static Edad calcularEdad(int anio, int mes, int dia, Date fecha) {

	public static Edad calcularEdad(int anio, int mes, int dia) {
		return calcularEdad(-1, -1, -1, anio, mes, dia);
	}
	public static Edad calcularEdad(int anioA, int mesA, int diaA, int anio, int mes, int dia) {
		Calendar c1 = Calendar.getInstance();
		int anioActual=0, mesActual=0, diaActual=0;
		if(anioA!=-1 || mesA!=-1 || diaA!=-1){
			Log.e("anioA", "antes: "+anioA);
			Log.e("mesA", "antes: "+mesA);
			Log.e("diaA", "antes: "+diaA);
			anioActual = anioA;
			mesActual = mesA;
			diaActual = diaA;
		} else {
			c1.setTime(c1.getTime());
			anioActual = c1.get(Calendar.YEAR);
			mesActual = c1.get(Calendar.MONTH) + 1;
			diaActual = c1.get(Calendar.DAY_OF_MONTH);
		}
		
		int edadMes;
		Log.e("anioA", "anioA: "+anioActual);
		Log.e("mesA", "mesA: "+mesActual);
		Log.e("diaA", "diaA: "+diaActual);
		
		if (anioActual == anio && mesActual == 99 && diaActual == 99) {
			return new Edad(0, 0);
		}
		if (anioActual == 9999 && mesActual == 99 && diaActual == 99) {
			return new Edad(99, 99);
		}
		if (anioActual == 9999) {
			return new Edad(0, 0);
		}
		
		if (anio == anioActual && mes == 99 && dia == 99) {
			return new Edad(0, 0);
		}
		if (anio == 9999 && mes == 99 && dia == 99) {
			return new Edad(99, 99);
		}
		if (anio == 9999) {
			return new Edad(0, 0);
		}

	    int anios = anioActual - anio;
	    int meses = (mesActual==99?0:mesActual) - (mesActual==99?0:(mes==99?0:mes));
	    int dias = (diaActual==99?0:diaActual) - (diaActual==99?0:(dia==99?0:dia));
	    boolean estado = false;
	    
	    Log.e("aniossss", "anios: "+anios);
		Log.e("mesesssss", "meses: "+meses);
		Log.e("diasssss", "dias: "+dias);
	    
	    if (mesActual==99 && dias<0) {
			return new Edad(anios, 0);
		}
	    
	    if(anios < 0)
	    	estado=true;
	    else if (anios==0){
	    	if(meses < 0)
	    		estado = true;
	    	else if(meses==0){
	    		if(dias < 0)
	    			estado=true;
	    	}
	    }
	    	
	    if(!estado){
		    edadMes= mesActual-mes+(dias<0?(meses<=0?12:0):13)-1;
		    
		    if(meses<=0)
		    	anios=anios-1;
		    
		    if(edadMes>=12){
		    	edadMes=edadMes-12;
		    	anios=anioActual - anio;
		    }
		     
			return new Edad(anios, anios>0?0:edadMes);
	    } else
	    	return null;
	}
	
	public static Long sumar(String Omision, EditText... cajas) {
		long vsuma = 0, valor = 0;
		for (EditText varcaja : cajas) {
			if (!(varcaja.getText().toString().equalsIgnoreCase(Omision))) {
				valor = varcaja.getText().toString().length() == 0 ? 0
						: Integer.parseInt(varcaja.getText().toString());
				vsuma = vsuma + valor;
			}
		}
		return vsuma;
	}
	
	public static String getNombreMes(int mes) {
		String retorno = "";
		switch (mes) {
		case 1:
			retorno = "Enero";
			break;
		case 2:
			retorno = "Febrero";
			break;
		case 3:
			retorno = "Marzo";
			break;
		case 4:
			retorno = "Abril";
			break;
		case 5:
			retorno = "Mayo";
			break;
		case 6:
			retorno = "Junio";
			break;
		case 7:
			retorno = "Julio";
			break;
		case 8:
			retorno = "Agosto";
			break;
		case 9:
			retorno = "Septiembre";
			break;
		case 10:
			retorno = "Octubre";
			break;
		case 11:
			retorno = "Noviembre";
			break;
		case 12:
			retorno = "Diciembre";
			break;
		default:
			retorno = "No definido";
			break;
		}
		return retorno;
	}
	
	public static String getNombreMesAbrev(int mes) {
		String[] meses = new String[]{null, "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"};
		return meses[mes];
	}
	
	public static boolean verificaFechas(String dia, String mes, String anio){
		if(dia.equals("99")) // Omision
			return true;
		if(mes.length()==1)
			mes="0".concat(mes);
		GregorianCalendar c = new GregorianCalendar();
		Date birthDate = new Date();
		try {
			birthDate = new SimpleDateFormat("ddMMyyyy").parse("01"+mes+anio);
		} catch (ParseException e) {
			e.printStackTrace();
			return true;
		}
		c.setTime(birthDate);
		
		int diaFrom = Integer.parseInt(dia);
		int diaTo = c.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		if(diaFrom > diaTo)
			return false;
		return true;
	}
	
	public static String getText(String text) {
		if (text == null) {
			return "";
		}
		return text;
	}
	
	public static String getText(Object obj, String other) {
		if (obj == null) {
			return other;
		}
		return obj.toString();
	}
	
	public static String getText(Object obj) {
		return getText(obj, "");
	}
	
	public static String getText(Integer text) {
		if (text == null) {
			return "";
		}
		return getText(text.toString());
	}
	
	public static String getText(Double text) {
		if (text == null) {
			return "";
		}
		return getText(text.toString());
	}
	
	public static String getText(BigDecimal text) {
		if (text == null) {
			return "";
		}
		return getText(text.toString());
	}
	
	public static String completarCadena(String cadena, String caracter, int tamanio) {
		return completarCadena(cadena, caracter, tamanio, COMPLETAR.DERECHA);
	}
	
	public enum COMPLETAR{IZQUIERDA,DERECHA};
	public static String completarCadena(String cadena, String caracter, int tamanio, COMPLETAR direccion) {
		String tmp = "";
		for (int i = 0; i < tamanio-cadena.length(); i++) {
			tmp += caracter;
		}
		if (direccion == COMPLETAR.IZQUIERDA) {
			return (tmp+cadena);
		} else {
			return (cadena+tmp);
		}
	}
	
	public static List<Object> getSpinnerKeys(int listSize) {
		return getSpinnerKeys(listSize, 0);
	}
	
	public static List<Object> getSpinnerKeys(int listSize, int initKey) {
		List<Object> keys = new ArrayList<Object>();
		for (int i = 0; i < listSize; i++) {
			keys.add(initKey + i);
		}
		return keys;
	}
	
	public static Date getFechaFormateada(String fecha) {
		return getFechaFormateada(fecha, "yyyyMMdd");
	}
	
	public static Date getFechaFormateada(String fecha, String formato) {
		if (fecha == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		Date toFecha = null;
		try {
			toFecha = sdf.parse(fecha);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return toFecha;
	}
		
	public static String getFechaFormateada(Date fecha, String formato) {
		if (fecha == null) {
			return "";
		}
		String fechaToString = "No especificada";
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		fechaToString = sdf.format(fecha);
		return fechaToString;
	}
	
	public static String getFechaFormateada(String anio, String mes, String dia, String formato) {		
		if ("".equals(anio) || anio == null || "".equals(mes) || mes == null || "".equals(dia) || dia == null) {
			return "";
		}
		return getFechaFormateada(Integer.parseInt(anio), Integer.parseInt(mes), Integer.parseInt(dia), formato);
	}
	
	public static String getHoraFormateada(String hora, String minuto, String segundo, String formato) {
		if ("".equals(hora) || hora == null || "".equals(minuto) || minuto == null || "".equals(segundo) || segundo == null) {
			return "";
		}
		return getHoraFormateada(Integer.parseInt(hora), Integer.parseInt(minuto), Integer.parseInt(segundo), formato);
	}
		
	public static String getFechaFormateada(int anio, int mes, int dia, String formato) {		
		Date fecha = null;
		String fechaToString = completarCadena(""+anio, "0", 4, COMPLETAR.IZQUIERDA)+completarCadena(""+mes, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+dia, "0", 2, COMPLETAR.IZQUIERDA);
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		try {
			fecha = formatter.parse(fechaToString);
		} catch (ParseException e) {	
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		fechaToString = sdf.format(fecha);
		return fechaToString;
	}
	
	public static String getHoraFormateada(int hora, int minuto, int segundo, String formato) {
		Date fecha = null;
		String fechaToString = completarCadena(""+hora, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+minuto, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+segundo, "0", 2, COMPLETAR.IZQUIERDA);
		DateFormat formatter = new SimpleDateFormat("HHmmss");
		try {
			fecha = formatter.parse(fechaToString);
		} catch (ParseException e) {		
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		fechaToString = sdf.format(fecha);
		return fechaToString;
	}
	
	public static Date getFecha(String anio, String mes, String dia) {
		return getFecha(anio, mes, dia, "yyyyMMdd");
	}
	
	public static Date getFecha(String anio, String mes, String dia, String formato) {
		if ("".equals(anio) || anio == null || "".equals(mes) || mes == null || "".equals(dia) || dia == null) {
			return null;
		}
		return getFecha(Integer.parseInt(anio), Integer.parseInt(mes), Integer.parseInt(dia), formato);
	}
	
	public static Date getFechaHora(String anio, String mes, String dia, String hora, String minuto, String segundo) {
		if ("".equals(anio) || anio == null || "".equals(mes) || mes == null || "".equals(dia) || dia == null
				|| "".equals(hora) || hora == null || "".equals(minuto) || minuto == null || "".equals(segundo) || segundo == null) {
			return null;
		}
		return getFechaHora(Integer.parseInt(anio), Integer.parseInt(mes), Integer.parseInt(dia), Integer.parseInt(hora),
				Integer.parseInt(minuto), Integer.parseInt(segundo));
	}
	
	public static Date getHora(String hora, String minuto, String segundo) {
		if ("".equals(hora) || hora == null || "".equals(minuto) || minuto == null || "".equals(segundo) || segundo == null) {
			return null;
		}
		return getHora(Integer.parseInt(hora), Integer.parseInt(minuto), Integer.parseInt(segundo));
	}
	
	public static Date getFecha(int anio, int mes, int dia) {
		return getFecha(anio, mes, dia, "yyyyMMdd");
	}
	public static Date getFecha(int anio, int mes, int dia, String formato) {
		Date fecha = null;
		String fechaToString = completarCadena(""+anio, "0", 4, COMPLETAR.IZQUIERDA)+
				completarCadena(""+mes, "0", 2, COMPLETAR.IZQUIERDA)+
				completarCadena(""+dia, "0", 2, COMPLETAR.IZQUIERDA);
		DateFormat formatter = new SimpleDateFormat(formato);
		try {
			fecha = formatter.parse(fechaToString);
		} catch (ParseException e) {		
			return null;
		}
		return fecha;
	}
	
	public static Date getHora(int hora, int minuto, int segundo) {
		Date fecha = null;
		String fechaToString = completarCadena(""+hora, "0", 2, COMPLETAR.IZQUIERDA)+
				completarCadena(""+minuto, "0", 2, COMPLETAR.IZQUIERDA)+
				completarCadena(""+segundo, "0", 2, COMPLETAR.IZQUIERDA);
		DateFormat formatter = new SimpleDateFormat("HHmmss");
		try {
			fecha = formatter.parse(fechaToString);
		} catch (ParseException e) {		
			return null;
		}
		return fecha;
	}
	
	public static Date getFechaHora(int anio, int mes, int dia, int hora, int minuto, int segundo) {
		Date fecha = null;
		String fechaToString = completarCadena(""+anio, "0", 4, COMPLETAR.IZQUIERDA)+completarCadena(""+mes, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+dia, "0", 2, COMPLETAR.IZQUIERDA)
				+ " " + completarCadena(""+hora, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+minuto, "0", 2, COMPLETAR.IZQUIERDA)+completarCadena(""+segundo, "0", 2, COMPLETAR.IZQUIERDA);
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
		try {
			fecha = formatter.parse(fechaToString);
		} catch (ParseException e) {		
			return null;
		}
		return fecha;
	}	

	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
	
	public static void lockView(Context context, View view){
		lockView(context, view, true);
	}
	
	public static void lockView(Context context, View view, boolean lock){
		if(view == null) return;
		if (view instanceof EditText || view instanceof TextBoxField) {
//			if(view.isEnabled()){
				if (lock) {
//					((EditText) view).setBackgroundColor(context.getResources().getColor(R.color.achurado));
					((EditText) view).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.backwithborderdisabled));
				} else {
//					((EditText) view).setBackgroundColor(context.getResources().getColor(R.color.cajaweb));
					((EditText) view).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.backwithborder));
				}
//			}
		}
		if (lock) {
			view.clearFocus();
		}
		view.setEnabled(!lock);
		view.setFocusable(!lock);
		view.setFocusableInTouchMode(!lock);
	}
	
	public static void lockView(Context context, boolean lock, View... views){
		if(views == null) return;
		for (View view : views) {
			lockView(context, view, lock);
		}		
	} 
	
	public static void lockView(Context context, boolean lock, List<View> views){
		if(views == null) return;
		for (View view : views) {
			lockView(context, view, lock);
		}		
	} 
		
	public static void cleanAndLockView(Context context, View view){
		if(view == null) return;
		lockView(context, view, true);
		cleanView(context, view);
	}
	
	public static void cleanAndLockView(Context context, View... views){
		if(views == null) return;
		lockView(context, true, views);
		cleanView(context, Arrays.asList(views));
	}
	
	public static void cleanView(Context context, List<View> views){	
		if(views == null) return;		
		for(View view:views){
			cleanView(context,view);
		}
	}
	
	public static void cleanView(Context context, View... views){	
		if(views == null) return;		
		for(View view:views){
			cleanView(context,view);
		}
	}
	
	public static void cleanView(Context context, View view){
		if(view == null) return;
		if (view instanceof DateTimeField)	((DateTimeField)view).setValue(null);
		else if (view instanceof CheckBox) ((CheckBox)view).setChecked(false);
		else if (view instanceof IntegerField) ((IntegerField)view).setText("");
		else if (view instanceof DecimalField) ((DecimalField)view).setText("");
		else if (view instanceof TextField) ((TextField)view).setText("");
		else if (view instanceof SpinnerField) ((SpinnerField)view).setSelectionKey( ((SpinnerField)view).getFirstKey());
		else if (view instanceof CheckBoxField) ((CheckBoxField)view).setChecked(false);
		else if (view instanceof RadioGroupField) ((RadioGroupField)view).setTagSelected(null);
		else if (view instanceof RadioGroupOtherField) ((RadioGroupOtherField)view).setTagIndexSelected(-1);
		else if (view instanceof ImageViewField) ((ImageViewField)view).setValue(null);
		else if (view instanceof EditText) ((EditText)view).setText("");
//		else if (view instanceof TableComponent) ((TableComponent)view).;
	}
	
	public static void esVacio(Entity bean, int from, int to, String... campos) throws IllegalArgumentException, 
		IllegalAccessException {
		List<Field> fields = bean.getFields(from, to);
		fields.addAll(bean.getFields(campos));		
		for (int i = 0; i < fields.size(); i++) {
			Object o = fields.get(i).get(bean);
//			if (esVacio(o)) {
//				
//			}
		}
	}
		
	public static boolean esDiferente(String valor, String... valores) {
		boolean flag = true;
		for (int i = 0; i < valores.length; i++) {
			if (!esDiferente(valor, valores[i])) {
				flag = false;
				break;
			}
		}	
		return flag;
	}
	
	public static boolean esDiferente(String valor, String valor2) {
		boolean flag;
		try {
			if (valor == null && valor2 == null) {
				return false;
			}
			if (valor.toString().equals(valor2.toString())) {
				flag = false;
			} else {
				flag = true;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}
	
	public static boolean esDiferente(Integer valor, Integer valor2) {
		boolean flag;
		try {
			if (valor == null && valor2 == null) {
				return false;
			}
			if (valor.intValue() != valor2.intValue()) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}
	
	public static boolean esDiferente(BigDecimal valor, BigDecimal valor2) {
		boolean flag;
		try {
			if (valor == null && valor2 == null) {
				return false;
			}
			if (valor.compareTo(valor2) != 0) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}

	/**
     * Retorna true si valor es diferente de valor2  
     * @param valor  Integer a evaluar
     * @param valor2 Dato con el que se evalua el valor
     * @return true si es cierto, false en caso contrario
     */

	public static boolean esDiferente(Integer valor, int valor2) {
		boolean flag;
		try {
			if (valor.intValue() != valor2) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}
	
	/**
     * Retorna true si valor es diferente de valor2  
     * @param valor  Integer a evaluar
     * @param valores Datos con el que se evalua el valor
     * @return true si es cierto, false en caso contrario
     */
	public static boolean esDiferente(Integer valor, int... valores) {
		boolean flag = true;
		for (int i = 0; i < valores.length; i++) {
			if (!esDiferente(valor, valores[i])) {
				flag = false;
				break;
			}
		}	
		return flag;
	}
	
	public static boolean esMayor(Integer valor, Integer valor2) {
		boolean flag;
		try {
			if (valor.intValue() > valor2.intValue()) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}
	
	public static boolean esMenor(Integer valor, Integer valor2) {
		boolean flag;
		try {
			if (valor.intValue() < valor2.intValue()) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			flag = true;
		}
		return flag;
	}
	
	/**
     * Compara dos fechas  
     * @param fecha1 fecha a evaluar
     * @param fecha2 fecha a evaluar
     * @return 1 cuando fecha1 > fecha2, 0 cuando fecha1 = fecha2 o -1 cuando fecha1 < fecha2 
     */
	public static int compare(Date fecha1, Date fecha2) {
		int a1,m1,d1,a2,m2,d2;
		a1 = Integer.parseInt(Util.getFechaFormateada(fecha1, "yyyy"));
		m1 = Integer.parseInt(Util.getFechaFormateada(fecha1, "MM"));
		d1 = Integer.parseInt(Util.getFechaFormateada(fecha1, "dd"));
		a2 = Integer.parseInt(Util.getFechaFormateada(fecha2, "yyyy"));
		m2 = Integer.parseInt(Util.getFechaFormateada(fecha2, "MM"));
		d2 = Integer.parseInt(Util.getFechaFormateada(fecha2, "dd"));
		if (a1 > a2) {
			return 1;
		} else if (a1 < a2) {
			return -1;
		}		
		if (m1 > m2) {
			return 1;
		} else if (m1 < m2) {
			return -1;
		}		
		if (d1 > d2) {
			return 1;
		} else if (d1 < d2) {
			return -1;
		}		
		return 0;
	}
	
	/**
     * Compara dos horas
     * @param hora1 fecha a evaluar
     * @param hora2 fecha a evaluar
     * @return 1 cuando hora1 > hora2, 0 cuando hora1 = hora2 o -1 cuando hora1 < hora2 
     */
	public static int compareTime(String hora1, String hora2, String formato) {
		return compareTime(getFechaFormateada(hora1.replace(":", ""), formato), getFechaFormateada(hora2.replace(":", ""), formato));
	}
	public static int compareTime(Date hora1, Date hora2) {
		int a1,m1,d1,a2,m2,d2;
		a1 = Integer.parseInt(Util.getFechaFormateada(hora1, "HH"));
		m1 = Integer.parseInt(Util.getFechaFormateada(hora1, "mm"));
		d1 = Integer.parseInt(Util.getFechaFormateada(hora1, "ss"));
		a2 = Integer.parseInt(Util.getFechaFormateada(hora2, "HH"));
		m2 = Integer.parseInt(Util.getFechaFormateada(hora2, "mm"));
		d2 = Integer.parseInt(Util.getFechaFormateada(hora2, "ss"));
		if (a1 > a2) {
			return 1;
		} else if (a1 < a2) {
			return -1;
		}		
		if (m1 > m2) {
			return 1;
		} else if (m1 < m2) {
			return -1;
		}		
		if (d1 > d2) {
			return 1;
		} else if (d1 < d2) {
			return -1;
		}		
		return 0;
	}
	
	/**
     * Compara dos fechas  
     * @param fecha1 fecha a evaluar
     * @param fecha2 fecha a evaluar
     * @return -1 cuando fecha1 > fecha2, 0 cuando fecha1 = fecha2 o 1 cuando fecha1 < fecha2 
     */
	public static int compareDate(String fecha1, String fecha2, String formato) {
		return compareDate(getFechaFormateada(fecha1.replace("/", ""), formato), getFechaFormateada(fecha2.replace("/", ""), formato));
	}
	public static int compareDate(Date fecha1, Date fecha2) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(fecha1);
		c2.setTime(fecha2);
		return c1.getTime().compareTo(c2.getTime());
	}
	
	/**
     * Verifica si algunas de las variables enviadas es igual al valor buscado    
     * @param valor valor buscado
     * @param variables conjunto de variables a encontrar una con el valor buscado
     * @return true cuando alguna variable es igual al valor buscado, falso en caso contrario 
     */
	public static boolean alMenosUnoEsIgualA(int valor, Integer... variables) {
		if (variables == null) {
			return false;
		}
		for (int i = 0; i < variables.length; i++) {
			if(variables[i] == null) continue;
			if (!esDiferente(variables[i], valor)) {
				return true;
			}
		}
		return false;
	}
	
	/**
     * Verifica si algunas de las variables enviadas es diferente al valor buscado    
     * @param valor valor buscado
     * @param variables conjunto de variables a encontrar una con el valor buscado
     * @return true cuando alguna variable es igual al valor buscado, falso en caso contrario 
     */
	public static boolean alMenosUnoEsDiferenteA(int valor, Integer... variables) {
		if (variables == null) {
			return false;
		}
		for (int i = 0; i < variables.length; i++) {
			if(variables[i] == null) continue;
			if (esDiferente(variables[i], valor)) {
				return true;
			}
		}
		return false;
	}
	
	public static InputFilter filtro = new InputFilter(){
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (end > start) {

                char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�','�','�','�','�','�','�',
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '@', '.', '_', '#', '$', '%', '&', '*', '-', '+', '(', ')', '!', '"', ':', 
                        ';', '/', '?', ',', '|', '\\', '<', '>', '{', '}', '[', ']', '=', '.', '?', '�', ' '};

                for (int index = start; index < end; index++) {                                         
                    if (!new String(acceptedChars).contains(String.valueOf(source.charAt(index)))) { 
                        return ""; 
                    }               
                }
            }
            return null;
        }

    };
    
    public static InputFilter filtroTexto = new InputFilter(){
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (end > start) {

                char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�','�','�','�','�','�','�',
                        '@', '.', '#', '$', '%', '*', '-', '+', '(', ')', '!', ':', '_',
                        ';', '/', '?', ',', '<', '>', '.', '?', '�', ' '};

                for (int index = start; index < end; index++) {                                         
                    if (!new String(acceptedChars).contains(String.valueOf(source.charAt(index)))) { 
                        return ""; 
                    }               
                }
            }
            return null;
        }

    };
    
    private static boolean esCaracterTextoValido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�','_','�','�','�','�','�','�',
                '@', '.', '#', '$', '%', '*', '-', '+', '(', ')', '!', ':', 
                ';', '/', '?', ',', '<', '>', '.', '?', '�', ' '};
    	if (!new String(acceptedChars).contains(String.valueOf(letra))) { 
            return false; 
        }   
    	return true;
    }
    
    private static boolean esCaracterTextoAlfanumericoValido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�','_','�','�','�','�','�','�',
                '@', '.', '#', '$', '%', '*', '-', '+', '(', ')', '!', ':', 
                ';', '/', '?', ',', '<', '>', '.', '?', '�', ' ',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esCaracterSoloAlfanumericoValido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�', ' ','�','�','�','�','�','�',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esCaracterSoloAlfanumericoPuntoValido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�', ' ', '.','�','�','�','�','�','�',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esCaracterSoloTextoValido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z','�', ' ','�','�','�','�','�','�'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esCaracterSoloTextoUTF8Valido(char letra) {
    	char[] acceptedChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 
    			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
    			'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    public static boolean esPalabraUTF8Valido(String palabra) {
    	char[] letras = palabra.toCharArray(); 
    	for (int i = 0; i < letras.length; i++) {
    		if (!esCaracterSoloTextoUTF8Valido(letras[i])) {
				return false;
			}
		}
    	return true;
    }
        
    private static boolean esNumeroValido(char letra) {
    	char[] acceptedChars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esDecimalValido(char letra) {
    	char[] acceptedChars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
    
    private static boolean esTelefono(char letra) {
    	char[] acceptedChars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-','#','*'};
    	return new String(acceptedChars).contains(String.valueOf(letra));
    }
	
	public static class MyTextWatcher implements TextWatcher {
		
		public static final int ALFANUMERICO = 0;
		public static final int TEXTO = 1;
		public static final int NUMERO = 2;
		public static final int SOLO_TEXTO = 3;
		public static final int SOLO_TEXTO_NUMERO = 4;
		public static final int DECIMAL = 5;
		public static final int TELEFONO = 6;
		public static final int SOLO_TEXTO_NUMERO_PUNTO = 7;
		private int tipo;
		private TextBoxField caja;
		private boolean flag=false;
		private boolean flagReemplazo=false;
		private int nroDecimales=0;
		
		public MyTextWatcher(TextBoxField caja, int tipo) {
			this.caja = caja;
			this.tipo = tipo;
		}
		
		public MyTextWatcher decimales(int decimales) { this.nroDecimales=decimales; return this; }
		
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
	    }
		
	    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) {
	    }
	
	    public void afterTextChanged(Editable arg0) {
	    	if (flag) {
				flag = false;
				return;
			}
	        if (arg0.length() > 0) {
	            char[] letras = arg0.toString().toCharArray();
	            for (int i = 0; i < letras.length; i++) {
	            	if (tipo == ALFANUMERICO) {
		            	if (!esCaracterTextoAlfanumericoValido(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == TEXTO) {
						if (!esCaracterTextoValido(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == NUMERO) {
						if (!esNumeroValido(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == DECIMAL) {
						if (!esDecimalValido(letras[i])) {
			            	letras[i] = '&';
						} else {
							int index=arg0.toString().indexOf('.');							
							if(index>=0) {
								int diferencia=i-index;	
								if(diferencia>nroDecimales){
									letras[i] = '&';
								}
							} else {
								int maxLength = this.caja.getMaxLength()-1-nroDecimales;
								if (i >= maxLength && letras[i] != '.') {
									letras[i] = '&';
								}
							}
						}
					} else if (tipo == TELEFONO) {
						if (!esTelefono(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == SOLO_TEXTO_NUMERO) {
						if (!esCaracterSoloAlfanumericoValido(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == SOLO_TEXTO) {
						if (!esCaracterSoloTextoValido(letras[i])) {
			            	letras[i] = '&';
						}
					} else if (tipo == SOLO_TEXTO_NUMERO_PUNTO) {
						if (!esCaracterSoloAlfanumericoPuntoValido(letras[i])) {
			            	letras[i] = '&';
						}
					}
				}
//	            if (tipo == ALFANUMERICO) {
//	            	if (!esCaracterTextoAlfanumericoValido(letras[letras.length-1])) {
//		            	letras[letras.length-1] = '&';
//					}
//				} else if (tipo == TEXTO) {
//					if (!esCaracterTextoValido(letras[letras.length-1])) {
//		            	letras[letras.length-1] = '&';
//					}
//				} else if (tipo == NUMERO) {
//					if (!esNumeroValido(letras[letras.length-1])) {
//		            	letras[letras.length-1] = '&';
//					}
//				}
	            String str=new String(letras);
	            if (str.contains("&")) {
					flagReemplazo = true;
				} else {
					flagReemplazo = false;
				}      
	            flag = true;    
	            if (flagReemplazo) {
		            str = str.replace("&", "");	  
		            this.caja.setText(str);
		            this.caja.setSelection(str.length());	
				}
	            flag = false;
	        }
	    }
	}
	
	public static class MyAutoSumaTextWatcher implements TextWatcher {
		private EditText caja;
		private EditText cajaTotal;
		private boolean flag=false;
		
		public MyAutoSumaTextWatcher(EditText caja,EditText cajaTotal) {
			this.caja = caja;
			this.cajaTotal=cajaTotal;
		}
		
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
	    }
		
	    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) {
	    }
	
	    public void afterTextChanged(Editable arg0) {
	    	if (flag) {
				flag = false;
				return;
			}
	        if (arg0.length() > 0) {
	            char[] letras = arg0.toString().toCharArray();
            	if (!esNumeroValido(letras[letras.length-1])) {
	            	letras[letras.length-1] = '&';
				}            
	            String str=new String(letras).replace("&", "");	        
	            flag = true;    
	            this.caja.setText(str);
	            this.caja.setSelection(str.length());
	            int tmpTotal=this.cajaTotal.getText().toString().trim().equals("")?0:Integer.parseInt(this.cajaTotal.getText().toString().trim());
	            int tmp=str.equals("")?0:Integer.parseInt(str);
	            tmpTotal=tmpTotal+tmp;
	            this.cajaTotal.setText(Util.getText(tmpTotal));
	            flag = false;
	        }
	    }
	}
	
	public static class MyAutoMuliplicacionTextWatcher implements TextWatcher {
		private EditText cajaTotal;
		private EditText[][] multiplicadores;
		private boolean flag=false;
		private int presicionMutiplicador;
		private int presicionResultado;
		
		
		
		public MyAutoMuliplicacionTextWatcher(EditText cajaTotal, int presicion,int presicionResul, EditText[]... multiplicadores) {
			this.cajaTotal=cajaTotal;
			this.multiplicadores=multiplicadores;
			this.presicionMutiplicador=presicion;
			this.presicionResultado=presicionResul;
		}
		
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
	    }
		
	    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) {
	    }
	
	    public void afterTextChanged(Editable arg0) {
	    	if (flag) {
				flag = false;
				return;
			}
	        if (arg0.length() > 0) {
	            flag = true;    
	            BigDecimal total=Util.getBigDecimal(multiplicadores[0][0].getText().toString(),multiplicadores[0][1].getText().toString(),this.presicionMutiplicador);
	            for (int i = 1; i < multiplicadores.length; i++) {
					EditText[] cajaTmp = multiplicadores[i];
					BigDecimal cajaN=Util.getBigDecimal(cajaTmp[0].getText().toString(),cajaTmp[1].getText().toString(),this.presicionMutiplicador);
					total=total.multiply(cajaN);
				}
	            this.cajaTotal.setText(Util.getText(getBigDecimal(total,this.presicionResultado)));
	            flag = false;
	        }
	    }
	}
	
	public static class MyAutoMuliplicacionTextWatcher2<T extends EditText> implements TextWatcher {
		private FragmentForm _this;
		private T cajaTotal;
		private View next;
		private List<HashMap<T, T>> multiplicadores;
		private boolean flag=false;
		private int presicionMutiplicador;
		private int presicionResultado;

		public MyAutoMuliplicacionTextWatcher2(FragmentForm fg, T cajaTotal, View next, int presicion,int presicionResul, List<HashMap<T, T>> lsthm) {
			this._this=fg;
			this.next=next;
			this.cajaTotal=cajaTotal;
			this.multiplicadores=lsthm;
			this.presicionMutiplicador=presicion;
			this.presicionResultado=presicionResul;
			this.addOrRemoveListener();
		}
		
		public void addOrRemoveListener(){
			if(this.multiplicadores.size()>0){
        		for(HashMap<T, T> hm:this.multiplicadores){
        			for(Map.Entry<T, T> t:hm.entrySet()){
        				t.getKey().addTextChangedListener(this);
        				t.getValue().addTextChangedListener(this);
        			}
        		}
			}		
		}
		
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) { }
	    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) { }
	    public void afterTextChanged(Editable arg0) {
	    	if (flag) return;
	    	boolean bloqueo=false;
	    	BigDecimal total=Util.getBigDecimal("1",this.presicionMutiplicador);
            if(multiplicadores.size()>0){
        		for(HashMap<T, T> hm:multiplicadores){
        			for(Map.Entry<T, T> t:hm.entrySet()){
        				Util.lockView(_this.getActivity(), false, t.getKey(), t.getValue());
        				if(total==null) break;
        				if(bloqueo){flag=true;Util.cleanAndLockView(_this.getActivity(),t.getKey(), t.getValue()); continue;}
        				if(t.getKey().getText().toString().equals("")||t.getValue().getText().toString().equals("")){total=null; break;}
        				if(Util.getSumaDecimal(t.getKey(), t.getValue()).doubleValue()==Util.getBigDecimal("0").doubleValue()){bloqueo=true; break;}
        				total=total.multiply(Util.getBigDecimal(t.getKey().getText().toString(),t.getValue().getText().toString(),this.presicionMutiplicador));
        			}
        		}
            }
            if(bloqueo){Util.cleanAndLockView(_this.getActivity(),this.cajaTotal); flag = false;}
            else {Util.lockView(_this.getActivity(), false, this.cajaTotal);
            this.cajaTotal.setText(Util.getText(getBigDecimal(total,this.presicionResultado)));}
	    }
	}
	
	public static class MyAutoSumaDiferenciaTextWatcher<T extends EditText> implements TextWatcher {
		private FragmentForm _this;
		private T cajaTotal;
		private List<T> Sumadores;
		private List<T> Diferencias;
		private boolean flag=false;
		private int presicionSumaDieferncia;
		private int presicionResultado;

		public MyAutoSumaDiferenciaTextWatcher(FragmentForm v, T cajaTotal, int presicion,int presicionResul, List<T> lstSum, List<T> lstDif) {
			this._this=v;
			this.cajaTotal=cajaTotal;
			this.Sumadores=lstSum;
			this.Diferencias=lstDif;
			this.presicionSumaDieferncia=presicion;
			this.presicionResultado=presicionResul;
			this.addOrRemoveListener();
		}
		
		public void addOrRemoveListener(){
			if(this.Sumadores.size()>0){
				for(T obj:this.Sumadores)
					obj.addTextChangedListener(this);
			}
					
			if(this.Diferencias.size()>0){
				for(T obj1:this.Diferencias)
					obj1.addTextChangedListener(this);
			}	
		}
		
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) { }
	    public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) { }
	    public void afterTextChanged(Editable arg0) {
	    	if (flag) return;
	    	if (arg0.length() > 0) {
		    	BigDecimal suma=Util.getBigDecimal("0",this.presicionSumaDieferncia);
		    	BigDecimal diferecia=Util.getBigDecimal("0",this.presicionSumaDieferncia);
		    	BigDecimal total=Util.getBigDecimal("0",this.presicionSumaDieferncia);
		    	
		    	if(this.Sumadores.size()>0){
					for(T obj:this.Sumadores)
						suma=Util.getSumaDecimal(presicionSumaDieferncia, suma, Util.getBigDecimal(obj.getText().toString()));
				}
						
				if(this.Diferencias.size()>0){
					//for(T obj1:this.Diferencias){
					EditText key=new EditText(_this.getActivity()), value=new EditText(_this.getActivity()), result=new EditText(_this.getActivity());
					for(int i=0;i<Diferencias.size();i++){
						if(i%2==0) key=Diferencias.get(i);
						if(i%2!=0) {
							value=Diferencias.get(i);
							//result.add(getHashMap(key, value));
							result.setText(Util.getText(Util.getBigDecimal(key.getText().toString(), value.getText().toString(), 2)));
							diferecia=Util.getSumaDecimal(presicionSumaDieferncia, diferecia, Util.getBigDecimal(result.getText().toString()));
						}
					}
					diferecia=Util.getSumaDecimal(presicionSumaDieferncia, diferecia, Util.getBigDecimal(Diferencias.get(Diferencias.size()-1).getText().toString()));
				}	
		    	
				Log.e("suma", String.valueOf(suma));
				Log.e("diferecia", String.valueOf(diferecia));
				
				
		    	total=getSumaDecimal(presicionSumaDieferncia, suma, diferecia.multiply(Util.getBigDecimal("-1")));
		    	Log.e("resultado", String.valueOf(Util.getText(total.doubleValue()<0?BigDecimal.ZERO:total)));
		    	total=getBigDecimal(total.intValue());
	            this.cajaTotal.setText(Util.getText(total.doubleValue()<0?BigDecimal.ZERO:total));
	    	}
	    }
	}
	
	public static int getInt(Object object){
		return getInt(object, 0);
	}
	
	public static int getInt(Object object, int other){
		int val=other;
		try {
			val=Integer.parseInt(object.toString());
		} catch (Exception e) {
		}
		return val;
	}
	
	public static<T extends EditText> int getInt(T txt){
		return getInt(txt.getText().toString().trim(),0);
	}
	
	public static int getInt(String texto){
		return getInt(texto,0);
	}
	
	public static int getInt(String texto, int other){
		int val=other;
		try {
			val=Integer.parseInt(texto.trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return val;
	}	
	
	public static int sumDigits(String txt){
		if(txt == null) return 0;
    	int sumtDigits = 0;
		for(char c : txt.toCharArray()){
			if(Character.isDigit(c)) sumtDigits+=Integer.parseInt(""+c);
		}
		return sumtDigits;
    }
	
	public static int contDigits(String txt){
		if(txt == null) return 0;
    	int conttDigits = 0;
		for(char c : txt.toCharArray()){
			if(Character.isDigit(c)) conttDigits++;
		}
		return conttDigits;
    }
	
	public static SparseArray<String> checkDatesValid(int dia, int mes, int anio){
		SparseArray<String> spa = new SparseArray<String>();
		if(mes > 12) spa.put(2, "Error el a\u00f1o solo debe tener 12 meses");
		else if(dia > 31) spa.put(1, "Error el mes solo debe tener 31 dias");
		else if((mes == 4 || mes == 6 || mes == 9 || mes == 11) && (dia > 30))
			spa.put(1, "Error el mes solo debe tener 30 dias");
		else if(mes == 2 && bisiesto(anio) && dia > 29)
			spa.put(1, "Error el mes solo debe tener 29 dias");
		else if (mes == 2 && !bisiesto(anio) && dia > 28)
			spa.put(1, "Error el mes solo debe tener 28 dias");
		return spa;
	}
	
	public static boolean bisiesto(int anio){
		if(anio % 400 == 0) return true;
		if(anio % 4 == 0 && anio % 100 != 0) return true;
		return false;
	}
	
	public static Integer diffMonths(Calendar from, Calendar to){
		Integer mesesFrom = from.get(Calendar.YEAR) * 12 + from.get(Calendar.MONTH)+1;
		Integer mesesTo = to.get(Calendar.YEAR) * 12 + to.get(Calendar.MONTH)+1;
		return mesesTo - mesesFrom;
	}
	
	// convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
    	if (bitmap == null) {
			return null;
		}
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
    
    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

	public static String getFechaActualToString() {
		Date fechaActual = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return sdf.format(fechaActual);
	}
	
	public static String getSoloFechaActualToString() {
		Date fechaActual = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fechaActual);
	}
	
	
	public static File savePicture(Bitmap bitmap, String ruta) throws IOException {
		File f = new File(ruta);
		if (f.exists()) {
			f.delete();
		}
		FileOutputStream outStream = null;
		outStream = new FileOutputStream(ruta);
		bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
		outStream.close();
		return f;
	}
	public static float getTama�oEscaladoFloat(Context context, float valor) {
		return getTama�oEscaladoFloat(context, valor, 0.2f);
	}
	
	public static float getTama�oEscaladoFloat(Context context, float valor, float factor) {
		final float scale = context.getResources().getDisplayMetrics().density;
//		Log.e("SCALE:-;-",": "+scale);
		float division=1.0f;
		if(scale==(float)1.5){
//			Log.e("aaaa:","SCALE:1.5 ");
			division=1.05f;			
		}
		if(scale==(float)1.3312501){
//			Log.e("bbbb:","SCALE:1.3312501 ");
			division=1.33f;	
		}
		else{
//			Log.e("ccc:","OTROS");
			division=1.05f;			
		}
		
//		if (scale >= 1.5) {
//			if (valor != FragmentForm.WRAP_CONTENT && valor != FragmentForm.MATCH_PARENT) {
				valor = (int) (valor*(scale/division)); 
//				valor = (int) (valor * (scale/1.33) + factor);	
//			}
//		}
		return valor;
	}
	
	public static int getTama�oEscalado(Context context, int valor) {
		return getTama�oEscalado(context, valor, 0.2f);
	}
	
	public static int getTama�oEscalado(Context context, int valor, float factor) {
		final float scale = context.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			if (valor != FragmentForm.WRAP_CONTENT && valor != FragmentForm.MATCH_PARENT) {
				valor = (int) (valor * (scale/1.33) + factor);	
			}
		}
		return valor;
	}
	public static int getTama�oEscaladoTableComponent(Context context, int valor, float factor) {
		final float scale = context.getResources().getDisplayMetrics().density;
		if (scale >= 2) {
			if (valor != FragmentForm.WRAP_CONTENT && valor != FragmentForm.MATCH_PARENT) {
				valor = (int) (valor * (scale/2) + factor);	
			}
		}
		return valor;
	}
}