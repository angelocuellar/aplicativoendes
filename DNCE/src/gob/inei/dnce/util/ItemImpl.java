package gob.inei.dnce.util;

import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.interfaces.Item;
import gob.inei.dnce.interfaces.Pregunta;
import gob.inei.dnce.interfaces.Respuesta;
import android.view.View;

/**
 * 
 * @author Rdelacruz
 *
 */
public final class ItemImpl 
implements Pregunta, Item
{	
	private String codigoInterno;
	private String codigoExterno;
	private String nombre;
	
	private boolean requerido = false;
	private boolean relevancia = true;
	private boolean omitible = false;
	private boolean otro = false;
	private boolean concatenarCodigo = true;
//	private boolean eliminarOtro = false;
	
	private TextField txtEspecifiqueEtiqueta;
	private TextField txtEspecifiqueSi;	
	private Respuesta respuesta;
	
	@Override
	public String getCodigo() {
		return codigoInterno;
	}
		
	@Override
	public String getNombre() {
		StringBuilder sb = new StringBuilder();

		if(nombre!=null) {
			sb.append(nombre); 
			sb.append(" "); 
		}
		
		if(txtEspecifiqueEtiqueta!=null)
			sb.append(getEspecifique());
		
		return sb.toString();
	}
	
	@Override
	public String getEspecifique() {
		//if(isRelevante())
			return txtEspecifiqueEtiqueta!=null ? txtEspecifiqueEtiqueta.getText().toString() : null;
		//return null;
	}
	
	@Override
	public final boolean isEspecifique() {
		return txtEspecifiqueEtiqueta!=null;
	}
	
	@Override
	public String toString() {
		if(!concatenarCodigo || (codigoExterno==null || codigoExterno.isEmpty()))
			return getNombre();
		return codigoExterno+". "+getNombre();
	}
	
	public boolean isRequerido() {
		return requerido;
	}
	public void setRequerido(boolean requerido) {
		this.requerido = requerido;
	}

	public void setSiRequerido(boolean siRequerido) {
		if(respuesta instanceof RespuestaRadioGroup) {
			((RespuestaRadioGroup)respuesta).setSiRequerido(siRequerido);
		}
	}

	@Override
	public boolean isRelevante() {
		return relevancia;
	}

	public void setRelevancia(boolean relevancia) {
		this.relevancia = relevancia;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	
	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Respuesta getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Respuesta respuesta) {
		((RespuestaImpl) respuesta).setPregunta(this);
		this.respuesta = respuesta;
	}

	public RadioGroupOtherField getRadioGroup() {
		if(respuesta instanceof RespuestaRadioGroup)
			return (RadioGroupOtherField)respuesta.getView();
		return null;
	}

	public TextField getTxtEspecifiqueEtiqueta() {
		return txtEspecifiqueEtiqueta;
	}

	public void setTxtEspecifiqueEtiqueta(TextField txtEspecifiqueEtiqueta) {
		this.txtEspecifiqueEtiqueta = txtEspecifiqueEtiqueta;
	}

	public TextField getTxtEspecifiqueSi() {
		return txtEspecifiqueSi;
	}

	public void setTxtEspecifiqueSi(TextField txtEspecifiqueSi) {
		this.txtEspecifiqueSi = txtEspecifiqueSi;
	}
	
	public boolean isOtro() {
		return otro;
	}

	public void setOtro(boolean otro) {
		this.otro = otro;
	}

	public void requestFocus() {
		View v = null; 
		if(isEspecifique())
			v = getTxtEspecifiqueEtiqueta();
		else if(txtEspecifiqueSi!=null)
			txtEspecifiqueSi.requestFocus();
		else
			v = getRespuesta().getView();
		
		v.clearFocus();
		v.requestFocus();
	}

	@Override
	public String getValor() {
		return respuesta.getValor();
	}

	@Override
	public boolean isOmitible() {
		return omitible;
	}

	public void setOmitible(boolean omitible) {
		this.omitible = omitible;
	}
	
	public boolean isConcatenarCodigo() {
		return concatenarCodigo;
	}

	public void setConcatenarCodigo(boolean concatenarCodigo) {
		this.concatenarCodigo = concatenarCodigo;
	}

	public void desactivarNo() {
		if(respuesta instanceof RespuestaRadioGroup) {
			((RespuestaRadioGroup) respuesta).desactivarNo();
		}
	}



//	public boolean isEliminarOtro() {
//		return isEspecifique() && eliminarOtro;
//	}
//
//	public void setEliminarOtro(boolean eliminarOtro) {
//		this.eliminarOtro = eliminarOtro;
//	}
	
	
}
