package gob.inei.dnce.util;

import gob.inei.dnce.interfaces.Respuesta;

/**
 * 
 * @author Rdelacruz
 *
 */
public abstract class RespuestaImpl 
implements Respuesta
{
	ItemImpl pregunta;

	public ItemImpl getPregunta() {
		return pregunta;
	}

	public void setPregunta(ItemImpl pregunta) {
		this.pregunta = pregunta;
	}
	
	@Override
	public final void onCambioValor() throws Exception {
		onCambioValor(getValor());
	}
	
}
