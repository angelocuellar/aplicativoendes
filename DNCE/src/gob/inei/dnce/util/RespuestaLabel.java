package gob.inei.dnce.util;

import gob.inei.dnce.components.LabelComponent;

import android.view.View;

/**
 * 
 * @author Rdelacruz
 *
 */
public class RespuestaLabel 
extends RespuestaImpl
{
	private final LabelComponent lblResultado;
	
	public RespuestaLabel(LabelComponent lblResultado) {
		this.lblResultado = lblResultado;
	}
	
	public LabelComponent getLblResultado() {
		return lblResultado;
	}


	@Override
	public String getValor() {
		return lblResultado.getText().toString();
	}
	
	@Override
	public boolean isMissing() {
		return false;
	}

	@Override
	public View getView() {
		return lblResultado;
	}

	@Override
	public String validar(String valor) {
		return null;
	}

	@Override
	public boolean debeLimpiar() {
		return false;
	}

	@Override
	public void setValor(String valor) {
		lblResultado.setText(valor);
	}

	@Override
	public void onCambioValor(String valor) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
