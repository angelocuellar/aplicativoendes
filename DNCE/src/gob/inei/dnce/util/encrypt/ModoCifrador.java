package gob.inei.dnce.util.encrypt;

/**
 * 
 * @author Rdelacruz
 *
 */
public enum ModoCifrador {
    ECB,//usado en .NET
    CBC
}