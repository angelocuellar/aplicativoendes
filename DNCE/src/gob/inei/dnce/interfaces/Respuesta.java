package gob.inei.dnce.interfaces;

import android.view.View;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface Respuesta 
{
	
	void setValor(String valor) throws Exception;
	String getValor();
	View getView();	
	//void setValorRegistrado();//retorna al valor anterior
	//void registrarValor();
	
	String validar(String valor);//Retorna el mensaje de invalidacion. Null (valido), "" (se debe ignorar)
	Pregunta getPregunta();
	boolean debeLimpiar();
	//boolean debeLimpiar(Map<String,Item> mapRespuestasAnteriores);
	boolean isMissing();
	
	void onCambioValor() throws Exception;
	void onCambioValor(String valor) throws Exception;
}
