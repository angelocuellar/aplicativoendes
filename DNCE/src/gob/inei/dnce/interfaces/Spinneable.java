package gob.inei.dnce.interfaces;

public interface Spinneable extends IDetailTitle {
	String getSpinnerLabel();
}
