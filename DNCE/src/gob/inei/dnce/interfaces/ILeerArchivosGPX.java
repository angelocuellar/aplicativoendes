package gob.inei.dnce.interfaces;

import gob.inei.dnce.dao.xml.XMLDataObject;
import android.app.Activity;

public interface ILeerArchivosGPX {
	void postLoad(XMLDataObject[] maps);
	Activity getActivity();
}
