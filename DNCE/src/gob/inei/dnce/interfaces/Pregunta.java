package gob.inei.dnce.interfaces;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface Pregunta 
{	
	String getCodigo();
	String getNombre();
	boolean isEspecifique();
	boolean isRequerido();
	boolean isRelevante();
	boolean isOmitible();
	String getEspecifique();
	Respuesta getRespuesta();
	boolean isOtro();
	
}
