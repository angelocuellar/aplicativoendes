package gob.inei.dnce.interfaces;

public interface FieldComponent {
	void setValue(Object aValue);
	Object getValue();
	Class<?> getValueClass();
	void setSize(int alto, int ancho);	
}
