package gob.inei.dnce.interfaces;

public interface IDetailEntityComponent extends IDetailTitle {
	void cleanEntity();
}
