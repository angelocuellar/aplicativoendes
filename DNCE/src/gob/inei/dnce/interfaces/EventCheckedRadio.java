package gob.inei.dnce.interfaces;

import java.util.HashMap;
import java.util.LinkedList;

import android.view.View;
import android.widget.RadioGroup;

/**
 * 
 * @author ajulcamoro
 *
 */
public interface EventCheckedRadio {

	/**
	 * Evento para RadioGroup
	 * 
	 * @param from
	 * @param to
	 * @param next
	 * @param checkedId
	 * @param values
	 * @param cancel
	 * @param allEquals
	 * @return
	 */
	boolean onCheckedChanged(RadioGroup from, View to, View next, int checkedId, HashMap<String, LinkedList> values, Boolean cancel, Boolean allEquals);
	
}
