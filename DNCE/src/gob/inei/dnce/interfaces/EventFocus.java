/**
 * 
 */
package gob.inei.dnce.interfaces;

import android.view.View;

/**
 * @author ajulcamoro
 *
 */
public interface EventFocus {
	
	/**
	 * 
	 * @param v
	 * @param hasFocus
	 * @return
	 */
	void onFocusChange(View v, boolean hasFocus);
}
