package gob.inei.dnce.listeners;

/**
 * 
 * @author Rdelacruz
 *
 */
public interface OnGenerarCodigo {
	String getCodigo(int nfila, String codigoAnterior);
}
