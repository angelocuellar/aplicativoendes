package gob.inei.dnce.listeners;

import gob.inei.dnce.components.MasterActivity;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public abstract class NavigationClickListener implements OnItemClickListener {

	private ListView drawerList;
	private DrawerLayout drawerLayout;
	private MasterActivity activity;
	private int opcionId;
	
	public NavigationClickListener(MasterActivity activity, int opcionId, 
			ListView drawerList, DrawerLayout drawerLayout) {
		this.drawerList = drawerList;
		this.drawerLayout = drawerLayout;
		this.activity = activity;
		this.opcionId = opcionId;
	}

	public ListView getDrawerList() {
		return drawerList;
	}

	public DrawerLayout getDrawerLayout() {
		return drawerLayout;
	}

	public MasterActivity getActivity() {
		return activity;
	}

	public int getOpcionId() {
		return opcionId;
	}
	
}
