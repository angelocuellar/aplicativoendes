package gob.inei.dnce.dao;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.PatronVariable;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public abstract class SQLiteDAO {
	private static String TAG = "SQLiteDAO";
	protected final DatabaseHelper dbh;
	protected Cursor cursor;
	public static Integer USER_ID = null;
	public static PatronVariable PATRON_VARIABLE = new PatronVariable("p$[a-z0-9_]*;", -1, -1, 3);

	public SQLiteDAO(DatabaseHelper dbh) {
		this.dbh = dbh;
	}

	public SQLiteDatabase startTX() {
		SQLiteDatabase dbw = dbh.getWritableDatabase();
		dbw.beginTransaction();
		return dbw;
	}

	public void commitTX(SQLiteDatabase dbTX) {
		dbTX.setTransactionSuccessful();
	}

	public void endTX(SQLiteDatabase dbTX) {
		dbTX.endTransaction();
		dbTX.close();
	}

	protected String getCamposSelect(List<String> campos) {
		if (campos == null)
			return null;
		return getCamposSelect(null, campos.toArray(new String[campos.size()]));
	}

	protected String getCamposSelect(String[] campos) {
		return this.getCamposSelect(null, campos);
	}

	@SuppressLint("DefaultLocale")
	protected String getCamposSelect(String prefix, String[] campos) {
		if (prefix == null) {
			prefix = "";
		} else {
			prefix += ".";
		}
		StringBuilder retorno = new StringBuilder();
		for (String c : campos) {
			if (c == null) {
				continue;
			}
			retorno.append(prefix).append(c.toUpperCase()).append(",");
		}
		return retorno.toString().substring(0, retorno.toString().length() - 1);
	}

	protected boolean existeRegistro(String campo, String tabla,
			String condicion, String... valores) {
		return this.existeRegistro(this.dbh.getReadableDatabase(), campo,
				tabla, condicion, valores);
	}

	protected boolean existeRegistro(SQLiteDatabase dbwTx, String campo,
			String tabla, String condicion, String... valores) {
		Cursor cursor = dbwTx.rawQuery("SELECT " + campo + " FROM " + tabla
				+ " WHERE " + condicion, valores);
		int count = cursor.getCount();
		cursor.close();
		cursor = null;
		return count > 0;
	}

	protected Integer nextID(String campo, String tabla, String condicion,
			String... valores) {
		return this.nextID(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}

	protected Integer nextID(SQLiteDatabase dbwTx, String campo, String tabla,
			String condicion, String... valores) {
		boolean isTX = true;
		if (dbwTx == null) {
			isTX = false;
			dbwTx = this.dbh.getReadableDatabase();
		}
		if (condicion != null && !condicion.isEmpty()) {
			cursor = dbwTx.rawQuery("SELECT IFNULL(MAX(" + campo
					+ "),0) + 1 AS ID FROM " + tabla + " WHERE " + condicion,
					valores);
		} else {
			cursor = dbwTx.rawQuery("SELECT IFNULL(MAX(" + campo
					+ "),0) + 1 AS ID FROM " + tabla, valores);
		}
		cursor.moveToNext();
		int count = getInt("ID", 0);
		cursor.close();
		cursor = null;
		if (!isTX) {
			dbwTx.close();
			SQLiteDatabase.releaseMemory();
		}
		return count;
	}

	public Double getDouble(String field, Double defaultValue) {
//		Double aString = cursor.getString(cursor.getColumnIndex(field))==null ? (defaultValue == null ? null : defaultValue) : Double.valueOf(cursor.getString(cursor.getColumnIndex(field)));
		Double aString = defaultValue;
		try {
			if (!cursor.isNull(cursor.getColumnIndex(field))) {
				aString = cursor.getDouble(cursor.getColumnIndex(field));	
			}			
		} catch (Exception e) {
			Log.e(TAG, "Error al leer " + field + ": " + e.getMessage());
		}
		//cursor.get
		return aString;
	}

	public Double getDouble(String field) {
		return getDouble(field, null);
	}

	public String getString(String field, String defaultValue) {
		String aString = defaultValue;
		try {
			if (!cursor.isNull(cursor.getColumnIndex(field))) {
				aString = cursor.getString(cursor.getColumnIndex(field));	
			}			
		} catch (Exception e) {
			Log.e(TAG, "Error al leer " + field + ": " + e.getMessage());
		}
		return aString;
	}

	public String getString(String field) {
		return getString(field, null);
	}

	public int getInt(String field, int defaultValue) {
		Integer aInteger = cursor.getInt(cursor.getColumnIndex(field));
		return aInteger;
	}

	public byte[] getBlob(String field) {
		return getBlob(field, null);
	}

	public byte[] getBlob(String field, byte[] defaultValue) {
		byte[] aBlob = defaultValue;		
		try {
			if (!cursor.isNull(cursor.getColumnIndex(field))) {
				aBlob = cursor.getBlob(cursor.getColumnIndex(field));	
			}			
		} catch (Exception e) {
			Log.e(TAG, "Error al leer " + field + ": " + e.getMessage());
		}
		return aBlob;
	}

	public int getInt(String field) {
		return getInt(field, -1);
	}

	public Integer getInteger(String field, Integer defaultValue) {
		Integer aInteger = defaultValue;
		try {
			if (!cursor.isNull(cursor.getColumnIndex(field))) {
				aInteger = cursor.getInt(cursor.getColumnIndex(field));		
			}			
		} catch (Exception e) {
			Log.e(TAG, "Error al leer " + field + ": " + e.getMessage());
		}
		return aInteger;
	}
	
	public Integer getInteger(String field) {
		return getInteger(field, -1);
	}

	public BigDecimal getBigDecimal(String string) {
		return this.getBigDecimal(string, BigDecimal.ZERO);
	}

	public BigDecimal getBigDecimal(String string, BigDecimal otherValue) {
		BigDecimal value = null;
		Double doubleNro = getDouble(string);//cursor.getDouble(col)
		//Float floatNro = Float.valueOf(getString(string));
		try {
			double vdouble = doubleNro.doubleValue();
			value = new BigDecimal(doubleNro.toString());
			if(vdouble==(int)vdouble){
				value = Util.getBigDecimal(value, 0);
			}
			
			Log.e(TAG, "CAMPO: " + string);
			Log.e(TAG, "Doulbe: " + doubleNro);
			Log.e(TAG, "String: " + getString(string));
			Log.e(TAG, "BigDecimal: " + value.toString());
		} catch (Exception e) {
			value = otherValue;
		}
		return value;
	}

	public String getFechaActualToString() {
		return Util.getFechaActualToString();
	}

	public boolean saveOrUpdate(String tableName, String whereClause,
			Entity bean, String[] whereClauseValuesName, int from, int to,
			String... campos) throws SQLException {
		return this.saveOrUpdate(null, tableName, whereClause, bean,
				whereClauseValuesName, from, to, campos);
	}

	public boolean saveOrUpdate(String tableName, String whereClause,
			Entity bean, String[] whereClauseValuesName,
			SeccionCapitulo... secciones) throws SQLException {
		return this.saveOrUpdate(null, tableName, whereClause, bean,
				whereClauseValuesName, secciones);
	}

	public boolean saveOrUpdate(SQLiteDatabase dbtx, String tableName,
			String whereClause, Entity bean, String[] whereClauseValuesName,
			int from, int to, String... campos) throws SQLException {
		boolean result = false;
		boolean isTX = true;
		if (dbtx == null) {
			dbtx = this.dbh.getWritableDatabase();
			isTX = false;
		}
		try {
			List<Field> fields = bean.getFields(whereClauseValuesName);
			String[] whereValues = new String[fields.size()];
			for (int i = 0; i < whereValues.length; i++) {
				if (fields.get(i).get(bean) == null) {
					throw new SQLException("La llave "
							+ whereClauseValuesName[i].toUpperCase()
							+ " no tiene información.");
				}
				whereValues[i] = fields.get(i).get(bean).toString();
			}
			String oper = existeRegistro(dbtx, "ID", tableName, whereClause,
					whereValues) ? "edit" : "add";
			ContentValues content = new ContentValues();
			List<String> listaCampos = bean.getFieldMatches(from, to);
			if (campos != null) {
				for (int i = 0; i < campos.length; i++) {
					listaCampos.add(campos[i]);
				}
			}
			for (int i = 0; i < whereClauseValuesName.length; i++) {
				listaCampos.add(whereClauseValuesName[i].toUpperCase());
			}
			content = bean.getContentValues(content, -1, -1,
					listaCampos.toArray(new String[listaCampos.size()]));
			if (oper.equals("add")) {
				content.put("USUCRE", Util.getInt(USER_ID, -1));
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECCRE", getFechaActualToString());
				content.put("FECREG", getFechaActualToString());
				result = dbtx.insertOrThrow(tableName, null, content) != -1;
			} else {
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECREG", getFechaActualToString());
				result = dbtx.update(tableName, content, whereClause,
						whereValues) > 0;
			}

		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			throw new SQLException(e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			if (!isTX) {
				dbtx.close();
				dbtx = null;
			}
			SQLiteDatabase.releaseMemory();
		}
		return result;
	}

	public boolean saveOrUpdate(SQLiteDatabase dbtx, String tableName,
			String whereClause, Entity bean, String[] whereClauseValuesName,
			SeccionCapitulo... secciones) throws SQLException {
		boolean result = false;
		boolean isTX = true;
		if (dbtx == null) {
			dbtx = this.dbh.getWritableDatabase();
			isTX = false;
		}
		try {
			List<Field> fields = bean.getFields(whereClauseValuesName);
			String[] whereValues = new String[fields.size()];
			for (int i = 0; i < whereValues.length; i++) {
				if (fields.get(i).get(bean) == null) {
					throw new SQLException("La llave "
							+ whereClauseValuesName[i].toUpperCase()
							+ " no tiene información, para la tabla " + tableName);
				}
				whereValues[i] = fields.get(i).get(bean).toString();
			}
			String oper = existeRegistro(dbtx, "ID", tableName, whereClause,
					whereValues) ? "edit" : "add";
			ContentValues content = new ContentValues();
			// List<String> listaCampos = bean.getFieldMatches(from, to);
			// listaCampos.add("USUCRE");
			// listaCampos.add("USUREG");
			// if (campos != null) {
			// for (int i = 0; i < campos.length; i++) {
			// listaCampos.add(campos[i]);
			// }
			// }
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				// Log.e(TAG, seccion.toString());
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			for (int i = 0; i < whereClauseValuesName.length; i++) {
				listaCampos.add(whereClauseValuesName[i].toUpperCase());
			}
			// Log.e(TAG, "" + Arrays.toString(listaCampos.toArray(new
			// String[listaCampos.size()])));
			content = bean.getContentValues(content, -1, -1,
					listaCampos.toArray(new String[listaCampos.size()]));
			if (oper.equals("add")) {
				if (content.get("USUCRE") == null) {
					content.put("USUCRE", Util.getInt(USER_ID, -1));	
				}
				if (content.get("USUREG") == null) {
					content.put("USUREG", Util.getInt(USER_ID, -1));
				}
				if (content.get("FECCRE") == null) {
					content.put("FECCRE", getFechaActualToString());
				}
				if (content.get("FECREG") == null) {
					content.put("FECREG", getFechaActualToString());
				}
				result = dbtx.insertOrThrow(tableName, null, content) != -1;
			} else {
				if (content.get("USUREG") == null) {
					content.put("USUREG", Util.getInt(USER_ID, -1));
				}
				if (content.get("FECREG") == null) {
					content.put("FECREG", getFechaActualToString());
				}
				result = dbtx.update(tableName, content, whereClause,
						whereValues) > 0;
			}
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			throw new SQLException(e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			if (!isTX) {
				dbtx.close();
				dbtx = null;
			}
			SQLiteDatabase.releaseMemory();
		}
		return result;
	}

	public boolean save(SQLiteDatabase dbtx, String tableName,
			String whereClause, Entity bean, String[] whereClauseValuesName,
			int from, int to, String... campos) throws SQLException {
		boolean result = false;
		boolean isTX = true;
		if (dbtx == null) {
			dbtx = this.dbh.getWritableDatabase();
			isTX = false;
		}
		try {
			List<Field> fields = bean.getFields(whereClauseValuesName);
			String[] whereValues = new String[fields.size()];
			for (int i = 0; i < whereValues.length; i++) {
				if (fields.get(i).get(bean) == null) {
					throw new SQLException("La llave "
							+ whereClauseValuesName[i].toUpperCase()
							+ " no tiene información.");
				}
				whereValues[i] = fields.get(i).get(bean).toString();
			}
			String oper = existeRegistro(dbtx, "ID", tableName, whereClause,
					whereValues) ? "edit" : "add";
			if (oper.equals("edit")) {
				throw new SQLException("Registro " + bean.toString()
						+ " ya existe");
			}
			ContentValues content = new ContentValues();
			List<String> listaCampos = bean.getFieldMatches(from, to);
			if (campos != null) {
				for (int i = 0; i < campos.length; i++) {
					listaCampos.add(campos[i]);
				}
			}
			for (int i = 0; i < whereClauseValuesName.length; i++) {
				listaCampos.add(whereClauseValuesName[i].toUpperCase());
			}
			content = bean.getContentValues(content, -1, -1,
					listaCampos.toArray(new String[listaCampos.size()]));
			if (oper.equals("add")) {
				if (content.get("USUCRE") == null) {
					content.put("USUCRE", Util.getInt(USER_ID, -1));	
				}
				if (content.get("USUREG") == null) {
					content.put("USUREG", Util.getInt(USER_ID, -1));
				}
				if (content.get("FECCRE") == null) {
					content.put("FECCRE", getFechaActualToString());
				}
				if (content.get("FECREG") == null) {
					content.put("FECREG", getFechaActualToString());
				}
				result = dbtx.insertOrThrow(tableName, null, content) != -1;
			} else {
				if (content.get("USUREG") == null) {
					content.put("USUREG", Util.getInt(USER_ID, -1));
				}
				if (content.get("FECREG") == null) {
					content.put("FECREG", getFechaActualToString());
				}
				result = dbtx.update(tableName, content, whereClause,
						whereValues) > 0;
			}
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			throw new SQLException(e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			if (!isTX) {
				dbtx.close();
				dbtx = null;
			}
			SQLiteDatabase.releaseMemory();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public <T> Object getBean(String tableName, String whereClause,
			Class<? extends Entity> clase, String... whereClauseValuesName) {
		Entity bean = null;
		Object o;
		T returnObject = null;
		try {
			o = clase.getConstructor().newInstance();
			bean = (Entity) o;
			String[] campos = bean.getFieldsSaveNames();
			returnObject = (T) this.getBean(tableName, whereClause,
					whereClauseValuesName, null, clase, -1, -1, campos);
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return returnObject;
	}

	@SuppressWarnings("unchecked")
	public <T> Object getBean(String tableName, String whereClause,
			String orderBy, Class<? extends Entity> clase,
			String... whereClauseValuesName) {
		Entity bean = null;
		Object o;
		T returnObject = null;
		try {
			o = clase.getConstructor().newInstance();
			bean = (Entity) o;
			String[] campos = bean.getFieldsSaveNames();
			returnObject = (T) this.getBean(tableName, whereClause,
					whereClauseValuesName, orderBy, clase, -1, -1, campos);
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return returnObject;
	}

	public <T> Object getBean(String tableName, String whereClause,
			String[] whereClauseValuesName, String orderBy,
			Class<? extends Entity> clase, int from, int to, String... others) {
		List<T> listas = null;
		T bean = null;
		try {
			listas = this.getBeans(tableName, whereClause,
					whereClauseValuesName, orderBy, clase, from, to, others);
			if (listas.size() > 0) {
				bean = listas.get(0);
			} else {
				bean = null;
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return bean;
	}

	public <T> Object getBean(String tableName, String whereClause,
			String[] whereClauseValuesName, String orderBy,
			Class<? extends Entity> clase, SeccionCapitulo... secciones) {
		List<T> listas = null;
		T bean = null;
		try {
			listas = this.getBeans(tableName, whereClause,
					whereClauseValuesName, orderBy, clase, secciones);
			if (listas.size() > 0) {
				bean = listas.get(0);
			} else {
				bean = null;
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return bean;
	}

	public <T> Object getBean(String tableName, String whereClause,
			String[] whereClauseValuesName, Class<? extends Entity> clase,
			SeccionCapitulo... secciones) {
		List<T> listas = null;
		T bean = null;
		try {
			listas = this.getBeans(tableName, whereClause,
					whereClauseValuesName, null, clase, secciones);
			if (listas.size() > 0) {
				bean = listas.get(0);
			} else {
				bean = null;
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return bean;
	}

	public <T> Object getBean(Query query, Class<? extends Entity> clase,
			String... campos) {
		List<T> listas = null;
		T bean = null;
		try {
			listas = this.getBeans(query, clase, campos);
			if (listas.size() > 0) {
				bean = listas.get(0);
			} else {
				bean = null;
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}
		return bean;
	}

	public <T> List<T> getBeans(String tableName, String whereClause,
			Class<? extends Entity> clase, String... whereClauseValuesName) {
		List<T> listas = new ArrayList<T>();
		Entity bean;
		Object o;
		try {
			o = clase.getConstructor().newInstance();
			bean = (Entity) o;
			String[] campos = bean.getFieldsNames();
			listas = this.getBeans(tableName, whereClause,
					whereClauseValuesName, null, clase, -1, -1, campos);
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			bean = null;
		}
		return listas;
	}

	public <T> List<T> getBeans(String tableName, String whereClause,
			String orderBy, Class<? extends Entity> clase,
			String... whereClauseValues) {
		List<T> listas = new ArrayList<T>();
		Entity bean;
		try {
			bean = clase.getConstructor().newInstance();
			String[] campos = bean.getFieldsNames();
			listas = this.getBeans(tableName, whereClause, whereClauseValues,
					orderBy, clase, -1, -1, campos);
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			bean = null;
		}
		return listas;
	}

	public <T> List<T> getBeans(String tableName, String whereClause,
			String[] whereClauseValues, String orderBy,
			Class<? extends Entity> clase, int from, int to, String... others) {
		List<T> listas = new ArrayList<T>();
		Entity bean;
		try {
			bean = clase.getConstructor().newInstance();
			List<String> listaCampos = bean.getFieldMatches(from, to);
			if (others != null) {
				for (int i = 0; i < others.length; i++) {
					listaCampos.add(others[i]);
				}
			}
			StringBuilder queryBuilder = new StringBuilder();
			String camposSelect = getCamposSelect(listaCampos);
			queryBuilder.append("SELECT DISTINCT ").append(camposSelect)
					.append(" ").append("FROM ").append(tableName).append(" ")
					.append("WHERE ").append(whereClause).append(" ");
			if (orderBy != null) {
				queryBuilder.append("ORDER BY ").append(orderBy);
			}
			Query query = new Query(queryBuilder.toString(), whereClauseValues);
			listas = this.getBeans(query, clase,
					listaCampos.toArray(new String[listaCampos.size()]));
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			bean = null;
		}
		return listas;
	}

	public <T> List<T> getBeans(String tableName, String whereClause,
			String[] whereClauseValues, String orderBy,
			Class<? extends Entity> clase, SeccionCapitulo... secciones) {
		List<T> listas = new ArrayList<T>();
		Entity bean;
		try {
			bean = clase.getConstructor().newInstance();
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			StringBuilder queryBuilder = new StringBuilder();
			String camposSelect = getCamposSelect(listaCampos);
			queryBuilder.append("SELECT DISTINCT ").append(camposSelect)
					.append(" ").append("FROM ").append(tableName).append(" ")
					.append("WHERE ").append(whereClause).append(" ");
			if (orderBy != null) {
				queryBuilder.append("ORDER BY ").append(orderBy);
			}
			Query query = new Query(queryBuilder.toString(), whereClauseValues);
			listas = this.getBeans(query, clase,
					listaCampos.toArray(new String[listaCampos.size()]));
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			bean = null;
		}
		return listas;
	}

	public <T> List<T> getBeans(Query query, Class<? extends Entity> clase,
			String... campos) {
		List<T> listas = new ArrayList<T>();
		Entity bean;
		SQLiteDatabase dbr = dbh.getReadableDatabase();
		try {
			cursor = dbr.rawQuery(query.statement, query.whereValues);
			while (cursor.moveToNext()) {
				bean = clase.getConstructor().newInstance();
				bean = (Entity) bean.fillEntity(cursor, campos);
				listas.add((T) bean);
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InstantiationException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (InvocationTargetException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			cursor.close();
			cursor = null;
			dbr.close();
			SQLiteDatabase.releaseMemory();
		}
		return listas;
	}
	
	/************************/
	/**
	 * 
	 * @param table tabla de la cual se sacaran los datos
	 * @param where cadena que representa la condicion de filtro
	 * @param whereValues los valores que se remplazaran en la condicion del where
	 * @return mapa de campos y valores resultantes en la consulta
	 */
	public Map<String, Object> getMaxMap(String table, String field, String where, String... whereValues) {
//		return getMap(null, table, fields, where, whereValues);
		Query query = new Query();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT IFNULL(MAX(").append(field).append("),0) AS ").append(field).append(" ")
			.append(" FROM ").append(table).append(" ")
			.append("WHERE ").append(where);
		query.setStatement(sb.toString());
		query.setFields(new String[]{field});
		query.setTable(table);
		query.setWhereValues(whereValues);
		return getMap(query);
	}
	
	/************************/
	/**
	 * 
	 * @param table tabla de la cual se sacaran los datos
	 * @param fields array de campos que se buscaran en la tabla
	 * @param where cadena que representa la condicion de filtro
	 * @param whereValues los valores que se remplazaran en la condicion del where
	 * @return mapa de campos y valores resultantes en la consulta
	 */
	public Map<String, Object> getMap(String table, String[] fields, String where, String... whereValues) {
		return getMap(null, table, fields, where, whereValues);
	}
	
	public Map<String, Object> getMap(SQLiteDatabase dbTX, String table, String[] fields, String where, String... whereValues) {
		List<Map<String, Object>> lista = getMaps(dbTX, table, fields, where, null, whereValues);
		if (lista.isEmpty()) {
			return null;
		}
		return lista.get(0);
	}
		
	/**
	 * 
	 * @param query de la cual se sacaran los datos, ver @link Query
	 * @return mapa de campos y valores resultantes en la consulta
	 */
	public Map<String, Object> getMap(Query query) {
		return getMap(null, query);
	}
	
	public Map<String, Object> getMap(SQLiteDatabase dbTX, Query query) {
		List<Map<String, Object>> lista = getMaps(dbTX, query);
		if (lista.isEmpty()) {
			return null;
		}
		return lista.get(0);
	}

	/**
	 * 
	 * @param table tabla de la cual se sacaran los datos
	 * @param fields array de campos que se buscaran en la tabla
	 * @param where cadena que representa la condicion de filtro
	 * @param whereValues los valores que se remplazaran en la condicion del where
	 * @return lista de mapas de campos y valores resultantes en la consulta
	 */
	public List<Map<String, Object>> getMaps(String table, String[] fields, String where, String... whereValues) {
		return getMaps(null, table, fields, where, null, whereValues);
	}
	
	public List<Map<String, Object>> getMaps(SQLiteDatabase dbTX, String table, String[] fields, String where, String... whereValues) {
		return getMaps(dbTX, table, fields, where, null, whereValues);
	}
	
	/**
	 * 
	 * @param table tabla de la cual se sacaran los datos
	 * @param fields array de campos que se buscaran en la tabla
	 * @param where cadena que representa la condicion de filtro
	 * @param orderBy cadena que representa el ordenamiento de los resultados
	 * @param whereValues los valores que se remplazaran en la condicion del where
	 * @return lista de mapas de campos y valores resultantes en la consulta
	 */
	public List<Map<String, Object>> getMaps(String table, String[] fields, String where, String orderBy, String... whereValues) {
		return getMaps(null,  table, fields, where, orderBy, whereValues);
	}
	
	public List<Map<String, Object>> getMaps(SQLiteDatabase dbTX, String table, String[] fields, String where, String orderBy, String... whereValues) {
		String camposSelect = getCamposSelect(fields);
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT DISTINCT ").append(camposSelect)
				.append(" ").append("FROM ").append(table).append(" ");
		if (where != null) {
			queryBuilder.append("WHERE ").append(where).append(" ");	
		}
		if (orderBy != null) {
			queryBuilder.append("ORDER BY ").append(orderBy);
		}
		Query query = new Query(queryBuilder.toString(), table, fields, whereValues);
		List<Map<String, Object>> lista = getMaps(dbTX, query);
//		Log.e(TAG, "Query: " + query.toString());
		return lista;
	}
	
	/**
	 * 
	 * @param query de la cual se sacaran los datos, ver @link Query
	 * @return Lista de mapas de campos y valores resultantes en la consulta
	 */
	public List<Map<String, Object>> getMaps(Query query) {
		return getMaps(null, query);
	}
	
	public List<Map<String, Object>> getMaps(SQLiteDatabase dbTX, Query query) {
		List<Map<String, Object>> listas = new ArrayList<Map<String, Object>>();
		SQLiteDatabase dbr = dbTX;
		boolean isTX = true;
		if (dbTX == null) {
			 dbr = dbh.getReadableDatabase();
			 isTX = false;
		}
		Map.Entry<String, Class> map=null;
		Map<String, Object> field=null;
		try {
			cursor = dbr.rawQuery(query.statement, query.whereValues);
			int count = 0;
			Map<String, Class> clases = null;
			Map<String, Object> campo = null;
			while (cursor.moveToNext()) {
				if (count == 0) {
					if (query.fields.length == 1 && "*".equals(query.fields[0])) {
						clases = getClases(dbr, query.table);
					} else {
						clases = getClases(cursor, query.fields);
					}
//					Log.e(TAG, clases.toString());
				}
				count++;
				campo = new HashMap<String, Object>();
				for (Map.Entry<String, Class> entry : clases.entrySet()) {
					map = entry;
					if (entry.getValue() == Integer.class) {
						campo.put(entry.getKey(), getInteger(entry.getKey(), null));						
					} else if (entry.getValue() == BigDecimal.class) {
						campo.put(entry.getKey(), getBigDecimal(entry.getKey(), null));	
					} else if (entry.getValue() == String.class) {
						campo.put(entry.getKey(), getString(entry.getKey(), null));	
					} else if (entry.getValue() == byte[].class) {
						campo.put(entry.getKey(), getBlob(entry.getKey(), null));	
					}
				}
				field=campo;
				listas.add(campo);
//				Log.e(TAG, "count["+count+"]");
			}
		} catch (IllegalArgumentException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			if (field != null) {
				Log.e(getClass().getSimpleName(), "\nField: "+field.toString());
			} 
			if (map != null) {
				Log.e(getClass().getSimpleName(), "\nEntry: " + map.toString());
			}
		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			if (field != null) {
				Log.e(getClass().getSimpleName(), "\nField: "+field.toString());
			} 
			if (map != null) {
				Log.e(getClass().getSimpleName(), "\nEntry: " + map.toString());
			}
		} finally {
			cursor.close();
			cursor = null;
			if (!isTX) {
				dbr.close();	
			}
			SQLiteDatabase.releaseMemory();
		}
		return listas;
	}
	
	public boolean saveOrUpdate(String tableName,
			Map<String, Object> map, String[] whereClauseValuesName,
			SeccionCapitulo... preguntasEnNull) throws SQLException {
		return saveOrUpdate(null, tableName, map, whereClauseValuesName, preguntasEnNull);
	}
	
	public boolean saveOrUpdate(SQLiteDatabase dbtx, String tableName,
			Map<String, Object> map, String[] whereClauseValuesName,
			SeccionCapitulo... preguntasEnNull) throws SQLException {
		boolean result = false;
		boolean isTX = true;
		if (dbtx == null) {
			dbtx = this.dbh.getWritableDatabase();
			isTX = false;
		}
		try {
			String[] whereValues = new String[whereClauseValuesName.length];
			String whereClause = "";
			for (int i = 0; i < whereClauseValuesName.length; i++) {
				whereClause += whereClauseValuesName[i].toUpperCase() + " = ? AND ";
				if (map.get(whereClauseValuesName[i].toUpperCase()) == null) {
					throw new SQLException("La llave "
							+ whereClauseValuesName[i].toUpperCase()
							+ " no tiene información, para la tabla: " + tableName);
				}
				whereValues[i] = map.get(whereClauseValuesName[i].toUpperCase()).toString();
			}
			whereClause = whereClause.substring(0, whereClause.length()-5);
			String oper = existeRegistro(dbtx, whereClauseValuesName[0].toUpperCase(), tableName, whereClause,
					whereValues) ? "edit" : "add";
			ContentValues content = new ContentValues();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().contains(":")) {
					continue;
				}
				if (entry.getValue() == null) {
					content.putNull(entry.getKey());	
				} else {
					if(entry.getValue() instanceof byte[]) {	
						byte[] blob = (byte[]) entry.getValue();
						content.put(entry.getKey().toUpperCase(), blob);
					} else {
						content.put(entry.getKey(), entry.getValue().toString());
					}
				}
			}
			String columns = getColumnsAsString(dbtx, tableName);
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < preguntasEnNull.length; i++) {
				SeccionCapitulo seccion = preguntasEnNull[i];
				// Log.e(TAG, seccion.toString());
				listaCampos.addAll(getFieldMatches(columns, seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			if (oper.equals("add")) {
				content.put("USUCRE", Util.getInt(USER_ID, -1));
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECCRE", getFechaActualToString());
				content.put("FECREG", getFechaActualToString());
				result = dbtx.insertOrThrow(tableName, null, content) != -1;
			} else {
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECREG", getFechaActualToString());
				result = dbtx.update(tableName, content, whereClause,
						whereValues) > 0;
			}

		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			throw new SQLException(e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			if (!isTX) {
				dbtx.close();
				dbtx = null;
			}
			SQLiteDatabase.releaseMemory();
		}
		return result;
	}

	public boolean save(SQLiteDatabase dbtx, String tableName,
			Map<String, Object> map, String[] whereClauseValuesName,
			SeccionCapitulo... preguntasEnNull) throws SQLException {
		boolean result = false;
		boolean isTX = true;
		if (dbtx == null) {
			dbtx = this.dbh.getWritableDatabase();
			isTX = false;
		}
		try {
			String[] whereValues = new String[whereClauseValuesName.length];
			String whereClause = "";
			for (int i = 0; i < whereClauseValuesName.length; i++) {
				whereClause += whereClauseValuesName[i].toUpperCase() + " = ? AND ";
				if (map.get(whereClauseValuesName[i].toUpperCase()) == null) {
					throw new SQLException("La llave "
							+ whereClauseValuesName[i].toUpperCase()
							+ " no tiene información, para la tabla: " + tableName);
				}
				whereValues[i] = map.get(whereClauseValuesName[i].toUpperCase()).toString();
			}
			whereClause = whereClause.substring(0, whereClause.length()-5);
			String oper = existeRegistro(dbtx, whereClauseValuesName[0].toUpperCase(), tableName, whereClause,
					whereValues) ? "edit" : "add";
			if (oper.equals("edit")) {
				throw new SQLException("Registro " + map.toString()
						+ " ya existe");
			}
			ContentValues content = new ContentValues();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().contains(":")) {
					continue;
				}
				if (entry.getValue() == null) {
					content.putNull(entry.getKey());	
				} else {
					if(entry.getValue() instanceof byte[]) {	
						byte[] blob = (byte[]) entry.getValue();
						content.put(entry.getKey().toUpperCase(), blob);
					} else {
						content.put(entry.getKey(), entry.getValue().toString());
					}
				}
			}
			String columns = getColumnsAsString(dbtx, tableName);
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < preguntasEnNull.length; i++) {
				SeccionCapitulo seccion = preguntasEnNull[i];
				// Log.e(TAG, seccion.toString());
				listaCampos.addAll(getFieldMatches(columns, seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			if (oper.equals("add")) {
				content.put("USUCRE", Util.getInt(USER_ID, -1));
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECCRE", getFechaActualToString());
				content.put("FECREG", getFechaActualToString());
				result = dbtx.insertOrThrow(tableName, null, content) != -1;
			} else {
				content.put("USUREG", Util.getInt(USER_ID, -1));
				content.put("FECREG", getFechaActualToString());
				result = dbtx.update(tableName, content, whereClause,
						whereValues) > 0;
			}

		} catch (NullPointerException e) {
			Log.e(getClass().getSimpleName(), "Ausencia de datos.", e);
		} catch (SQLException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
			throw new SQLException(e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		} finally {
			if (!isTX) {
				dbtx.close();
				dbtx = null;
			}
			SQLiteDatabase.releaseMemory();
		}
		return result;
	}
	
	private List<String> getColumns(SQLiteDatabase dbtx, String tableName) {
		List<String> columns = new ArrayList<String>();
		String query = "PRAGMA table_info("+tableName+")";
		boolean isTX = true;
		if (dbtx == null) {
			isTX = false;
			dbtx = this.dbh.getReadableDatabase();
		}
		Cursor cursor = dbtx.rawQuery(query, null);
		while (cursor.moveToNext()) {
			columns.add(cursor.getString(cursor.getColumnIndex("name")));
		}
		cursor.close();
		cursor = null;
		if (!isTX) {
			dbtx.close();	
			SQLiteDatabase.releaseMemory();
		}
		return columns;
	}
	
	private List<String> getColumns(String tableName) {
		return this.getColumns(null, tableName);
	}
	
	private String getColumnsAsString(SQLiteDatabase dbtx, String tableName) {
		List<String> columns = getColumns(dbtx, tableName);
		String columnsString = "";
		for (String string : columns) {
			columnsString += string + ";";
		}
		return columnsString;
	}
	
	private List<String> getFieldMatches(String columnas, int seccion, int subSeccion, int start, int end, int subPregI, int subPregF) {
		List<String> coincidentes = new ArrayList<String>();
		for(int i=start; i<=end; i++){
			String tmp = PATRON_VARIABLE.getPatron().replace("$", Util.completarCadena(String.valueOf(i), "0", 
					PATRON_VARIABLE.getLongitudPregunta(), COMPLETAR.IZQUIERDA));
			tmp = tmp.replace("&", subSeccion==-1?"":Util.completarCadena(String.valueOf(subSeccion), "0", 
					PATRON_VARIABLE.getLongitudSubSeccion(), COMPLETAR.IZQUIERDA));
			tmp = tmp.replace("%", seccion==-1?"":Util.completarCadena(String.valueOf(seccion), "0", 
					PATRON_VARIABLE.getLongitudSeccion(), COMPLETAR.IZQUIERDA));
			if (subPregI == -1) {
				tmp = tmp.replace("#", "");
//				Log.e(TAG, "Patron [" + tmp + "]");
				Matcher m = Pattern.compile(tmp).matcher(columnas);
				while (m.find()) {
					coincidentes.add(m.group().replace(";", ""));
				}
			} else {
				for(int j=subPregI; j<=subPregF; j++){
					String tmps = tmp.replace("#", j==-1?"":"_"+String.valueOf(j));
					Matcher m = Pattern.compile(tmps).matcher(columnas);
					while (m.find()) {
						coincidentes.add(m.group().replace(";", ""));
					}
				}
			}
		}		
//		Log.e(TAG, ""+Arrays.toString(allMatches.toArray(new String[allMatches.size()])));
		return coincidentes;
	}
	
	public Map<String, Class> getClases(SQLiteDatabase dbtx, String tableName) {
		Map<String, Class> clases = new HashMap<String, Class>();
		List<String> columnsName = new ArrayList<String>();
		List<String> columnsClase = new ArrayList<String>();
		String query = "PRAGMA table_info("+tableName+")";
		boolean isTX = true;
		if (dbtx == null) {
			isTX = false;
			dbtx = this.dbh.getReadableDatabase();
		}
		Cursor cursor = dbtx.rawQuery(query, null);
		while (cursor.moveToNext()) {
			columnsClase.add(cursor.getString(cursor.getColumnIndex("type")));
			columnsName.add(cursor.getString(cursor.getColumnIndex("name")));
		}
		cursor.close();
		cursor = null;
		if (!isTX) {
			dbtx.close();	
			SQLiteDatabase.releaseMemory();
		}
		for (int i=0; i < columnsClase.size(); i++) {

			if("P1111_CTGCCPP_O".equals(columnsName.get(i)) || "P707_8_O".equals(columnsName.get(i))) {
				clases.put(columnsName.get(i), String.class);
				continue;
			}
			if (columnsClase.get(i).startsWith("CHAR") || columnsClase.get(i).startsWith("VARCHAR") 
					|| columnsClase.get(i).startsWith("char") || columnsClase.get(i).startsWith("varchar") || columnsClase.get(i).isEmpty()) {
				clases.put(columnsName.get(i), String.class);
			} else if (columnsClase.get(i).startsWith("INT") || columnsClase.get(i).startsWith("TINYINT")
					|| columnsClase.get(i).startsWith("int") || columnsClase.get(i).startsWith("tinyint")) {
				clases.put(columnsName.get(i), Integer.class);				
			} else if (columnsClase.get(i).startsWith("BLOB")) {
				clases.put(columnsName.get(i), byte[].class);
			} else if (columnsClase.get(i).startsWith("NUM") || columnsClase.get(i).startsWith("DEC") 
					|| columnsClase.get(i).startsWith("DOU") || columnsClase.get(i).startsWith("FLO")) {
				clases.put(columnsName.get(i), BigDecimal.class);
			}
		}
		return clases;
	}
	
	public Map<String, Class> getClases(Cursor cursor, String[] fields) {
		Map<String, Class> clases = new HashMap<String, Class>();
		for (int i = 0; i < fields.length; i++) {
			switch (cursor.getType(cursor.getColumnIndex(fields[i]))) {
			case Cursor.FIELD_TYPE_INTEGER:
				clases.put(fields[i], Integer.class);
				break;
			case Cursor.FIELD_TYPE_BLOB:
				clases.put(fields[i], byte[].class);
				break;
			case Cursor.FIELD_TYPE_FLOAT:
				clases.put(fields[i], BigDecimal.class);
				break;
			default:
				clases.put(fields[i], String.class);
				break;
			}
		}
		return clases;
	}

	public boolean borrar(String tableName, String whereClause,
			String... values) {
		return this.borrar(null, tableName, whereClause, values);
	}

	public boolean borrar(SQLiteDatabase dbwTX, String tableName,
			String whereClause, String... values) {
		boolean flag = true;
		boolean isTX = true;
		if (dbwTX == null) {
			dbwTX = this.dbh.getWritableDatabase();
			isTX = false;
		}
		dbwTX.delete(tableName, whereClause, values);
		if (!isTX) {
			dbwTX.close();
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}

	public String[] getColumnNames(String table) {
		SQLiteDatabase db = dbh.getReadableDatabase();
		List<String> columnsName = new ArrayList<String>();
		String query = "PRAGMA table_info("+table+")";
		Cursor cursor = db.rawQuery(query, null);
		while (cursor.moveToNext()) {
			columnsName.add(cursor.getString(cursor.getColumnIndex("name")));
		}
		cursor.close();
		cursor = null;
		SQLiteDatabase.releaseMemory();		
		return columnsName.toArray(new String[columnsName.size()]);
	}

	protected Integer count(String campo, String tabla, String condicion,
			String... valores) {
		return this.count(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}

	protected Integer count(SQLiteDatabase dbwTx, String campo, String tabla,
			String condicion, String... valores) {
		cursor = dbwTx.rawQuery("SELECT IFNULL(COUNT(" + campo
				+ "),0) AS ID FROM " + tabla + " WHERE " + condicion, valores);
		cursor.moveToNext();
		int count = getInt("ID", 0);
		cursor.close();
		cursor = null;
		return count;
	}

	protected Integer max(String campo, String tabla, String condicion,
			String... valores) {
		return this.max(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}
	protected Integer max(SQLiteDatabase dbwTx, String campo, String tabla,
			String condicion, String... valores) {
		cursor = dbwTx.rawQuery("SELECT IFNULL(MAX(" + campo
				+ "),0) AS ID FROM " + tabla + " WHERE " + condicion, valores);
		cursor.moveToNext();
		int max = getInt("ID", 0);
		cursor.close();
		cursor = null;
		return max;
	}
	protected Integer min(String campo, String tabla, String condicion,
			String... valores) {
		return this.min(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}
	protected Integer min(SQLiteDatabase dbwTx, String campo, String tabla,
			String condicion, String... valores) {
		cursor = dbwTx.rawQuery("SELECT IFNULL(MIN(" + campo
				+ "),0) AS ID FROM " + tabla + " WHERE " + condicion, valores);
		cursor.moveToNext();
		int min = getInt("ID", 0);
		cursor.close();
		cursor = null;
		return min;
	}
	protected Integer countdistinct(String campo, String tabla,
			String condicion, String... valores) {
		return this.countdistinct(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}

	protected Integer countdistinct(SQLiteDatabase dbwTx, String campo,
			String tabla, String condicion, String... valores) {
		cursor = dbwTx.rawQuery("SELECT  IFNULL(COUNT(DISTINCT " + campo
				+ "),0) AS ID FROM " + tabla + " WHERE " + condicion, valores);
		cursor.moveToNext();
		int count = getInt("ID", 0);
		cursor.close();
		cursor = null;
		return count;
	}

	protected Integer sum(String campo, String tabla, String condicion,
			String... valores) {
		return this.sum(this.dbh.getReadableDatabase(), campo, tabla,
				condicion, valores);
	}

	protected Integer sum(SQLiteDatabase dbwTx, String campo, String tabla,
			String condicion, String... valores) {
		cursor = dbwTx.rawQuery("SELECT SUM(CASE WHEN IFNULL(" + campo
				+ ",0)=0 THEN 0 ELSE " + campo + " END) AS N_E FROM " + tabla
				+ " WHERE " + condicion, valores);
		cursor.moveToNext();
		int sum = getInt("N_E", 0);
		cursor.close();
		cursor = null;
		return sum;
	}

	public static class Query {
		String statement;
		String table;
		String[] fields;
		String[] whereValues;

		public Query() {
			super();
		}

		public Query(String statement, String... whereValues) {
			super();
			this.statement = statement;
			this.whereValues = whereValues;
		}

		public Query(String statement, String[] fields, String... whereValues) {
			super();
			this.statement = statement;
			this.fields = fields;
			this.whereValues = whereValues;
		}

		public Query(String statement, String table, String[] fields, String... whereValues) {
			super();
			this.statement = statement;
			this.table = table;
			this.fields = fields;
			this.whereValues = whereValues;
		}

		public String getStatement() {
			return statement;
		}

		public void setStatement(String statement) {
			this.statement = statement;
		}

		public String[] getWhereValues() {
			return whereValues;
		}

		public void setWhereValues(String[] whereValues) {
			this.whereValues = whereValues;
		}

		public String[] getFields() {
			return fields;
		}

		public void setFields(String[] fields) {
			this.fields = fields;
		}

		public String getTable() {
			return table;
		}

		public void setTable(String table) {
			this.table = table;
		}

		@Override
		public String toString() {
			return statement + "\n" + Arrays.toString(whereValues);
		}
		
	}
}
