package gob.inei.dnce.dao.xml;

import gob.inei.dnce.components.Entity;

import java.util.List;

public class XMLObject {

	private String tableName;
	private Class<? extends Entity> clase;
	private List<? extends Entity> beans;
	private String[] columNames;
	
	public XMLObject() {}

	public XMLObject(String tableName, Class<? extends Entity> clase, List<? extends Entity> beans) {
		this(tableName, clase, beans, null);
	}
	
	public XMLObject(String tableName, Class<? extends Entity> clase, List<? extends Entity> beans, String[] columNames) {
		super();
		this.clase = clase;
		this.tableName = tableName;
		this.beans = beans;
		this.columNames = columNames;
	}

	public String getTableName() {
		return tableName;
	}

	public Class<? extends Entity> getClase() {
		return clase;
	}

	public void setClase(Class<? extends Entity> clase) {
		this.clase = clase;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<? extends Entity> getBeans() {
		return beans;
	}

	public void setBeans(List<? extends Entity> beans) {
		this.beans = beans;
	}
	
	public String[] getColumNames() {
		return columNames;
	}

	public void setColumNames(String[] columNames) {
		this.columNames = columNames;
	}

	public static class BeansProcesados {
		private int total;
		private int procesado;
		public BeansProcesados(int total, int procesado) {
			super();
			this.total = total;
			this.procesado = procesado;
		}
		public int getTotal() {
			return total;
		}
		public int getProcesado() {
			return procesado;
		}
		
	}
}
