package gob.inei.dnce.dao.xml;

import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class XMLDataObject {

	public static final int ACTUALIZAR_PRIMERO = 0;
	public static final int BORRAR_PRIMERO = 1;
	public static final int VALIDA_ESTADO_ENVIO = 1;
	public static final int NO_VALIDA_ESTADO_ENVIO = 0;
	
	private String tableName;
	private String beanName;
	private List<String> pkFields;
	private int primeraAccion;
	private int validaEstadoEnvio;
	private List<Map<String, Object>> registros;
	
	public XMLDataObject(String tableName, String beanName, int primeraAccion) {
		this(tableName, beanName, primeraAccion, null);
	}
	
	public XMLDataObject(String tableName, String beanName, int primeraAccion, List<Map<String, Object>> registros) {
		this.tableName = tableName;
		this.beanName = beanName;
		this.primeraAccion = primeraAccion;
		if (registros == null) {
			this.registros = new ArrayList<Map<String, Object>>();
		} else {
			this.registros = registros;
		}
		validaEstadoEnvio = NO_VALIDA_ESTADO_ENVIO;
	}
	
	public XMLDataObject pk(String pk) {
		if (pkFields == null) {
			pkFields = new ArrayList<String>();
		}
		pkFields.add(pk);
		return this;
	}

	@Override
	public String toString() {
		return "XMLDataObject [tableName=" + tableName + ", beanName="
				+ Util.getText(beanName) + ", validaEstadoEnvio=" + validaEstadoEnvio
				+ ", registros.size=" + (registros==null?0:registros.size()) + "]";
	}

	public int getPrimeraAccion() {
		return primeraAccion;
	}

	public void setPrimeraAccion(int primeraAccion) {
		this.primeraAccion = primeraAccion;
	}

	public List<String> getPkFields() {
		return pkFields;
	}

	public void setPkFields(List<String> pkFields) {
		this.pkFields = pkFields;
	}

	public String getTableName() {
		return tableName;
	}
	
	public String getBeanName() {
		return beanName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public void add(Map<String, Object> data) {
		this.registros.add(data);
	}

	public void addAll(List<Map<String, Object>> datas) {
		this.registros.addAll(datas);
	}
	
	public List<Map<String, Object>> getRegistros() {		
		return this.registros;
	}
	
	public int getValidaEstadoEnvio() {
		return validaEstadoEnvio;
	}

	public void setValidaEstadoEnvio(int validaEstadoEnvio) {
		this.validaEstadoEnvio = validaEstadoEnvio;
	}

	public Map<String, Object> getRegistroPKs(Map<String, Object> row, List<String> camposPK) {
		Map<String, Object> pks = new HashMap<String, Object>();
		for (String pk : camposPK) {
			pks.put(pk, row.get(pk));
		}
		return pks;
	}
	
	public String getStringWhere() {
		String where=null;
		if (pkFields == null) {
			return where;
		}
		where = "";
		for (String k : pkFields) {
			where += k + " = ? AND ";
		}
		where = where.substring(0, where.length()-4);
		return where;
	}
	
	public String getStringWhereNextID() {
		String where=null;
		if (pkFields == null) {
			return where;
		}
		where = "";
		for (int i = 0; i < pkFields.size() - 1; i++) {
			where += pkFields.get(i) + " = ? AND ";
		}
		if (where.length() > 4) {
			where = where.substring(0, where.length()-4);	
		}
		return where;
	}
	
	public String getStringQueryNextID() {
		String queryString=null;
		if (pkFields == null) {
			return queryString;
		}
		String where = getStringWhereNextID();
		if (where == null || where.isEmpty()) {
			where = "1=1";
		}
		queryString = "SELECT IFNULL(MAX(" + pkFields.get(pkFields.size()-1) + "), 0) + 1 AS ID FROM " + tableName + " WHERE " + where; 
		Log.e(getClass().getSimpleName(), queryString);
		return queryString;
	}
	
	public String[] getWhereValuesNextID(Map<String, Object> registro) {
		if (pkFields == null) {
			return null;
		}		
		List<String> values = new ArrayList<String>();
		for (int i = 0; i < pkFields.size()-1; i++) {
			values.add(Util.getText(registro.get(pkFields.get(i)), ""));
		}
		return values == null ? null : values.toArray(new String[values.size()]);
	}
}
