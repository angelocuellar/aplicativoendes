package gob.inei.endes2024.model; 
import gob.inei.dnce.components.Entity; 
import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_04B_TARJETA extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null;     
    public Integer ninio_id=null;
    public String pregunta=null;
    public Integer indice=null; 
    public String dia=null; 
    public String mes=null; 
    public Integer anio=null; 

    public CISECCION_04B_TARJETA() {} 
} 
