package gob.inei.endes2024.model;

import gob.inei.dnce.interfaces.IDetailEntityComponent;

public interface Exportable extends IDetailEntityComponent {

	       String getCodigoExportacion();
	       String getDescripcionExportacion();
}
