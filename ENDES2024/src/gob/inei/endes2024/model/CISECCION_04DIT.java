package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_04DIT extends Entity implements Serializable,IDetailEntityComponent{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
   // public Integer niniodit_id=null;
    public Integer nro_orden_ninio=null;
    public Integer qi478 =null;
    public Integer qi478a=null;
    public Integer qi478a1=null; 
    public Integer qi478a2=null; 
    public Integer qi478a3=null; 
    public Integer qi478a4=null; 
    public Integer qi478a5=null; 
    public Integer qi478a6=null; 
    public Integer qi478a7=null; 
    public Integer qi478a8=null; 
    public Integer qi478a9a=null; 
    public Integer qi478a9b=null; 
    public Integer qi478a10=null; 
    public Integer qi478b1=null; 
    public Integer qi478b2=null; 
    public Integer qi478b3=null; 
    public Integer qi478b4=null; 
    public Integer qi478b5=null; 
    public Integer qi478b6=null; 
    public Integer qi478b7=null; 
    public Integer qi478b8=null; 
    public Integer qi478b9a=null; 
    public Integer qi478b9b=null; 
    public Integer qi478b9c=null; 
    public Integer qi478b9d=null; 
    public Integer qi478b9e=null; 
    public Integer qi478b10=null; 
    public Integer qi478c1=null; 
    public Integer qi478c2=null; 
    public Integer qi478c3=null; 
    public Integer qi478c4=null; 
    public Integer qi478c5=null; 
    public Integer qi478c6=null; 
    public Integer qi478c7=null; 
    public Integer qi478c8=null; 
    public Integer qi478c9=null; 
    public Integer qi478c10=null; 
    public Integer qi478d1=null; 
    public Integer qi478d2=null; 
    public Integer qi478d3=null; 
    public Integer qi478d4=null; 
    public Integer qi478d5=null; 
    public Integer qi478d6=null; 
    public Integer qi478d7=null; 
    public Integer qi478d8=null; 
    public Integer qi478d9a=null; 
    public Integer qi478d9b=null; 
    public Integer qi478d10=null; 
    public CISECCION_04DIT() {}
    
	@FieldEntity(ignoreField=true, saveField=false)
	public String qi212_nom=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estadocap4dit=null;
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	} 
	
	public Integer setConvertconcuatrovariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 4:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconcuatrovariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=4;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertcontresvariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 3:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertcontresvariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=3;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	
} 
