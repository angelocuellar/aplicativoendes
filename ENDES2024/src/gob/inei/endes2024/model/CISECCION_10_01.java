package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_10_01 extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer qi1000a=null; 
    
    public Integer qi1001a=null;     
    public Integer qi1001b=null; 
    public String  qi1001b_o=null; 
    public Integer qi1001c=null; 
    public String  qi1001c_o=null; 
    
    
    public Integer qi456_1a=null;
    public Integer qi456_1b=null;
    public Integer qi456_1x=null;
    public String qi456_1o=null; 
    
    public Integer qi466e_1a=null;
    public Integer qi466e_1b=null;
    public Integer qi466e_1x=null;
    public String qi466e_1o=null;
//    public Integer qi456_1a=null;
//    public Integer qi456_1b=null;
//    public Integer qi456_1x=null;
//    public Integer qi456_1o=null; 
    
    public Integer qi1002a=null; 
    public Integer qi1002b=null; 
    public Integer qi1002c=null; 
    public Integer qi1002d=null; 
    public Integer qi1002e=null; 
    public Integer qi1003a=null;
    public Integer qi1003an=null;
    public Integer qi1003b=null; 
    public Integer qi1003bn=null;
    public Integer qi1003c=null; 
    public Integer qi1003cn=null;
    public Integer qi1003d=null; 
    public Integer qi1003dn=null;
    public Integer qi1003e=null; 
    public Integer qi1003en=null;
    public Integer qi1003f=null; 
    public Integer qi1003fn=null;
    public Integer qi1004a=null; 
    public Integer qi1004an=null; 
    public Integer qi1004b=null; 
    public Integer qi1004bn=null; 
    public Integer qi1004c=null; 
    public Integer qi1004cn=null; 
    public Integer qi1004d=null; 
    public Integer qi1004dn=null; 
    public Integer qi1004e=null; 
    public Integer qi1004en=null; 
    public Integer qi1004f=null; 
    public Integer qi1004fn=null; 
    public Integer qi1004g=null; 
    public Integer qi1004gn=null; 
    public Integer qi1004h=null; 
    public Integer qi1004hn=null; 
    public Integer qi1005a=null; 
    public Integer qi1005an=null; 
    public Integer qi1005b=null; 
    public Integer qi1005bn=null; 
    public Integer qi1005c=null; 
    public Integer qi1005cn=null; 
    public Integer qi1005d=null; 
    public Integer qi1005dn=null; 
    public Integer qi1005e=null; 
    public Integer qi1005en=null; 
    public Integer qi1005f=null; 
    public Integer qi1005fn=null; 
    public Integer qi1005g=null; 
    public Integer qi1005gn=null; 
    public Integer qi1005h=null; 
    public Integer qi1005hn=null; 
    public Integer qi1005i=null; 
    public Integer qi1005in=null; 
    public Integer qi1007=null;
    
    public Integer qi1007m=null; 
    
    public Integer qi1008a=null; 
    public Integer qi1008an=null; 
    public Integer qi1008b=null; 
    public Integer qi1008bn=null; 
    public Integer qi1008c=null; 
    public Integer qi1008cn=null; 
    public Integer qi1008d=null; 
    public Integer qi1008dn=null; 
    public Integer qi1009=null; 
    public Integer qi1010=null; 
    public Integer qi1011=null; 
    public Integer qi1012=null; 
    public Integer qi1012b=null; 
    public Integer qi1012bn=null; 
    public Integer qi1013a=null; 
    
    public Integer qi1013aa=null; 
    public Integer qi1013b=null; 
    
    public Integer qi1014_a=null;
    public Integer qi1014_an=null;
    public Integer qi1014_b=null;
    public Integer qi1014_bn=null;
    public Integer qi1014_c=null;
    public Integer qi1014_cn=null;
    public Integer qi1014_d=null;
    public Integer qi1014_dn=null;
    public Integer qi1014_e=null;
    public Integer qi1014_en=null;
    public Integer qi1014_f=null;
    public Integer qi1014_fn=null;
    public Integer qi1014_g=null;
    public Integer qi1014_gn=null;
    public Integer qi1014_h=null;
    public Integer qi1014_hn=null;
    public Integer qi1014_i=null;
    public Integer qi1014_in=null;
    public Integer qi1014_j=null;
    public Integer qi1014_jn=null;
    public Integer qi1014_k=null;
    public Integer qi1014_kn=null;
    public Integer qi1014_l=null;
    public Integer qi1014_ln=null;
    public Integer qi1014_m=null;
    public Integer qi1014_mn=null;
    public Integer qi1014_n=null;
    public Integer qi1014_nn=null;
    public Integer qi1014_o=null; 
    public Integer qi1014_on=null;
    public Integer qi1014_p=null;
    public Integer qi1014_pn=null;
    public Integer qi1014_q=null;
    public Integer qi1014_qn=null;
    public Integer qi1014_r=null;
    public Integer qi1014_rn=null;
    public Integer qi1014_s=null;
    public Integer qi1014_sn=null;
    public Integer qi1014_t=null;
    public Integer qi1014_tn=null;
    public Integer qi1014_u=null;
    public Integer qi1014_un=null;
    public Integer qi1014_v=null;
    public Integer qi1014_vn=null;
    public Integer qi1014_x=null; 
    public String qi1014xi=null; 
    public Integer qi1014_xn=null;
    public Integer qi1016=null; 
    public String qi1016x=null; 
    public Integer qi1017=null; 
    public Integer qi1019=null; 
    public Integer qi1020_a=null;
    public Integer qi1020_an=null;
    public Integer qi1020_b=null;
    public Integer qi1020_bn=null;
    public Integer qi1020_c=null;
    public Integer qi1020_cn=null;
    public Integer qi1020_d=null;
    public Integer qi1020_dn=null;
    public Integer qi1020_e=null;
    public Integer qi1020_en=null;
    public Integer qi1020_f=null;
    public Integer qi1020_fn=null;
    public Integer qi1020_g=null;
    public Integer qi1020_gn=null;
    public Integer qi1020_h=null;
    public Integer qi1020_hn=null;
    public Integer qi1020_i=null;
    public Integer qi1020_in=null;
    public Integer qi1020_j=null;
    public Integer qi1020_jn=null;
    public Integer qi1020_k=null;
    public Integer qi1020_kn=null;
    public Integer qi1020_l=null;
    public Integer qi1020_ln=null;
    public Integer qi1020_m=null;
    public Integer qi1020_mn=null;
    public Integer qi1020_n=null;
    public Integer qi1020_nn=null;
    public Integer qi1020_o=null;
    public Integer qi1020_on=null;
    public Integer qi1020_p=null;
    public Integer qi1020_pn=null;
    public Integer qi1020_q=null;
    public Integer qi1020_qn=null;
    public Integer qi1020_r=null;
    public Integer qi1020_rn=null;
    public Integer qi1020_s=null;
    public Integer qi1020_sn=null;
    public Integer qi1020_t=null;
    public Integer qi1020_tn=null;
    public Integer qi1020_u=null;
    public Integer qi1020_un=null;
    
    public Integer qi1020_v=null;
    public Integer qi1020_vn=null;
    
    public Integer qi1020_w=null;
    public Integer qi1020_wn=null;
    
    
    public Integer qi1020_x=null; 
    public String qi1020_xi=null; 
    public Integer qi1020_xn=null;
    public Integer qi1022=null; 
    public Integer qi1023_a=null; 
    public Integer qi1023_b=null; 
    public Integer qi1023_c=null; 
    public Integer qi1023_d=null; 
    public Integer qi1023_e=null; 
    public Integer qi1023_f=null; 
    public Integer qi1023_g=null; 
    public Integer qi1023_h=null; 
    public Integer qi1023_i=null; 
    public Integer qi1023_j=null; 
    public Integer qi1023_k=null; 
    public Integer qi1023_l=null; 
    public Integer qi1023_m=null; 
    public Integer qi1023_x=null; 
    public String qi1023_xi=null; 
    public Integer qi1023a_a=null; 
    public Integer qi1023a_b=null; 
    public Integer qi1023a_c=null; 
    public Integer qi1023a_d=null; 
    public Integer qi1023a_e=null; 
    public Integer qi1023a_f=null; 
    public Integer qi1023a_g=null; 
    public Integer qi1023a_h=null; 
    public Integer qi1023a_x=null; 
    public String qi1023a_xi=null; 
    public Integer qi1023a_z=null; 
    public Integer qi1024=null; 
    public String qi1024x=null;
    
    public Integer qi1025=null; 
    public Integer qi1026=null; 
    
    public Integer qi1026a_a=null; 
    public Integer qi1026a_b=null; 
    public Integer qi1026a_c=null; 
    
    public Integer qi1028_a=null; 
    public Integer qi1028_b=null; 
    public Integer qi1028_x=null; 
    public String qi1028_xi=null; 
    public Integer qi1028_y=null; 
    public Integer qi1030a_a=null; 
    public Integer qi1030a_b=null; 
    public Integer qi1030a_c=null; 
    public Integer qi1030a_d=null; 
    public Integer qi1030a_e=null; 
    public Integer qi1030a_f=null; 
    public Integer qi1030a_g=null; 
    public Integer qi1030a_h=null; 
    public Integer qi1030a_i=null; 
    public Integer qi1030a_j=null; 
    public Integer qi1030a_k=null; 
    public Integer qi1030a_l=null; 
    public Integer qi1030a_m=null; 
    public Integer qi1030a_x=null; 
    public String qi1030a_xi=null; 
    public Integer qi1030b_a=null; 
    public Integer qi1030b_b=null; 
    public Integer qi1030b_c=null; 
    public Integer qi1030b_d=null; 
    public Integer qi1030b_e=null; 
    public Integer qi1030b_f=null; 
    public Integer qi1030b_g=null; 
    public Integer qi1030b_h=null; 
    public Integer qi1030b_i=null; 
    public Integer qi1030b_j=null; 
    public Integer qi1030b_k=null; 
    public Integer qi1030b_l=null; 
    public Integer qi1030b_m=null; 
    public Integer qi1030b_x=null; 
    public String qi1030b_xi=null; 
    public Integer qi1030c_a=null; 
    public Integer qi1030c_b=null; 
    public Integer qi1030c_c=null; 
    public Integer qi1030c_d=null; 
    public Integer qi1030c_e=null; 
    public Integer qi1030c_f=null; 
    public Integer qi1030c_g=null; 
    public Integer qi1030c_h=null; 
    public Integer qi1030c_i=null; 
    public Integer qi1030c_j=null; 
    public Integer qi1030c_k=null; 
    public Integer qi1030c_l=null; 
    public Integer qi1030c_m=null; 
    public Integer qi1030c_x=null; 
    public String qi1030c_xi=null; 
    public Integer qi1031=null; 
    public Integer qi1032_a=null; 
    public Integer qi1032_b=null; 
    public Integer qi1032_c=null; 
    public Integer qi1032_d=null; 
    public Integer qi1032_e=null; 
    public Integer qi1032_f=null; 
    public Integer qi1032_g=null; 
    public Integer qi1032_h=null; 
    public Integer qi1032_i=null; 
    public Integer qi1032_j=null; 
    public Integer qi1032_k=null; 
    public Integer qi1032_l=null; 
    public Integer qi1032_m=null; 
    public Integer qi1032_n=null; 
    public Integer qi1032_x=null; 
    public String qi1032_xi=null; 
    public Integer qi1032_y=null; 
    public Integer qi1033=null;
    
    public Integer qi1041_1=null; 
    public Integer qi1041_2=null; 
    public Integer qi1042_1=null;
    public Integer qi1042_2=null;
    public Integer qi1042_3=null;
   
    public Integer qi1042a_1a=null;
    public Integer qi1042a_1b=null;
    public Integer qi1042a_2a=null;
    public Integer qi1042a_2b=null;
    public Integer qi1042a_3a=null;
    public Integer qi1042a_3b=null;
    public Integer qi1042a_4a=null;
    public Integer qi1042a_4b=null;
    public Integer qi1042a_5a=null;
    public Integer qi1042a_5b=null;
    public Integer qi1042a_6a=null;
    public Integer qi1042a_6b=null;
    public Integer qi1042a_7a=null;
    public Integer qi1042a_7b=null;
    public Integer qi1042a_8a=null;
    public Integer qi1042a_8b=null;
    
    public String qi1043=null; 
    public String qi1043_1=null;
    
       
    public String qiobs_va=null; 
    public String qiobs_ct=null; 
    
    
    
    public String qiobs_en=null; 
    public String qiobs_sl=null; 
    public String qiobs_sn=null; 
    
    
    //public String qi456_a=null; 
    
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer filtro1014;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer filtro1005;
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtro1034=false;
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtroSiNinio=false;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer filtroSiNinioS10=0;  
    
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtro1014nico;
    
    public CISECCION_10_01() {}
    
    public Integer getConvertQi1013(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1013(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi1016(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 23: valorReturn=96; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1016(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 96: valorReturn=23; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi1024(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 12: valorReturn=96; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1024(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 96: valorReturn=12; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi1025(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1025(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi1026(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1026(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi1026a_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1026a_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi1026a_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1026a_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi1026a_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1026a_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi1031(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi1031(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
}