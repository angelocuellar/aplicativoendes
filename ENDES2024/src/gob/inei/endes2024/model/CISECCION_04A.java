package gob.inei.endes2024.model; 
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_04A extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer ninio_id=null; 
    public Integer qi405=null; 
    public Integer qi406_a=null; 
    public Integer qi406_b=null; 
    public Integer qi407_a=null; 
    public Integer qi407_b=null; 
    public Integer qi407_c=null; 
    public Integer qi407_d=null; 
    public Integer qi407_e=null; 
    public Integer qi407_f=null; 
    public Integer qi407_x=null; 
    public String qi407_xi=null; 
    public Integer qi407_y=null; 
    public String qi408=null; 
    public Integer qi408_a=null; 
    public Integer qi408_b=null; 
    public Integer qi408_c=null; 
    public Integer qi408_d=null; 
    public Integer qi408_e=null; 
    public Integer qi408_f=null; 
    public Integer qi408_g=null; 
    public Integer qi408_h=null; 
    public Integer qi408_i=null; 
    public Integer qi408_j=null; 
    public Integer qi408_k=null; 
    public Integer qi408_l=null; 
    public Integer qi408_x=null; 
    public String qi408_o=null; 
    public Integer qi409=null; 
    public Integer qi410=null; 
    public Integer qi410_b=null; 
    public Integer qi411_a=null; 
    public Integer qi411_b=null; 
    public Integer qi411_c=null; 
    public Integer qi411_d=null; 
    public Integer qi411_e=null; 
    public Integer qi411_f=null; 
    public Integer qi411_g=null; 
    public Integer qi411_h=null;
    public Integer qi411_i=null; 
    public Integer qi411_j=null; 
    public Integer qi411_k=null; 
    public Integer qi411_l=null;
    public Integer qi411_m=null;
    public Integer qi411b=null; 
    public Integer qi411c=null; 
    public Integer qi411d=null; 
    public Integer qi411e=null; 
    public Integer qi411f=null; 
    public Integer qi412=null; 
    public Integer qi412a=null; 
    public Integer qi413=null; 
    public Integer qi414=null; 
    public Integer qi415=null; 
    public Integer qi417=null; 
    public Integer qi418=null; 
    public String qi419m=null; 
    public Integer qi419y=null; 
    public Integer qi420=null; 
    public Integer qi421=null;
    public String  qi421_o = null;
    public Integer qi422d=null; 
    public Integer qi422i=null; 
    public String qi422_o=null;
    public Integer qi422a=null;
    public Integer qi422a_a=null;
    public Integer qi422a_b=null;
    public Integer qi422a_c=null;
    public Integer qi422a_d=null;    
    public Integer qi423=null; 
    public Integer qi424=null; 
    public Integer qi425=null; 
    public Integer qi426_a=null; 
    public Integer qi426_b=null; 
    public Integer qi426_c=null; 
    public Integer qi426_d=null; 
    public Integer qi426_e=null; 
    public Integer qi426_f=null; 
    public Integer qi426_g=null; 
    public Integer qi426_x=null; 
    public String qi426_x_o=null; 
    public Integer qi426_y=null; 
    public String qi426a_nom=null; 
    public Integer qi426a=null; 
    public String qi426a_o=null; 
    public Integer qi426b=null; 
    public String qi426bx=null; 
    public Integer qi426d=null; 
    public Integer qi426e=null; 
    public Integer qi426fb=null; 
    public Integer qi426fb_c=null;
    public String qi426fb_o=null; 
    public Integer qi426ga=null; 
    public Integer qi426gb=null; 
    public Integer qi426gc=null; 
    public Integer qi426gd=null; 
    public Integer qi426ge=null; 
    public String qi426ge_o=null; 
    public Integer qi427=null; 
    public Integer qi427aa=null; 
    public Integer qi427ab=null; 
    public String qi427a_o=null; 
    public Integer qi427b=null; 
    public String qi427b_o=null; 
    public String qi427c_nom=null; 
    public Integer qi427c=null; 
    public String qi427c_o=null; 
    public Integer qi427da=null; 
    public Integer qi427db=null; 
    public Integer qi427dc=null; 
    public Integer qi427dd=null; 
    public Integer qi427de=null; 
    public Integer qi427df=null; 
    public Integer qi427dg=null; 
    public Integer qi427f=null; 
    public Integer qi427g=null; 
    public Integer qi428=null; 
    public Integer qi428a=null; 
    public Integer qi428b=null; 
    public Integer qi429=null; 
    public Integer qi429a=null;
    public String qi429a_obs=null; 
    public Integer qi430=null; 
    public Integer qi430a=null; 
    public Integer qi430b=null; 
    public Integer qi430b_c=null; 
    public Integer qi430c=null; 
    public Integer qi430d=null; 
    public String qi430dn=null; 
    public Integer qi431a=null; 
    public Integer qi432=null; 
    public Integer qi433=null; 
    public Integer qi433au=null; 
    public Integer qi433an=null; 
    public Integer qi433b=null; 
    public String qi433bx=null; 
    public String qi433c_n=null; 
    public Integer qi433c=null; 
    public String qi433cx=null;
    public String qi433a_o=null;

    public Integer qi434=null; 
    public Integer qi435=null; 
    public String qi435x=null; 
    public Integer qi436u=null; 
    public Integer qi436n=null; 
    public Integer qi436a=null;
    public String qi436_o=null;
    public Integer qi436b_a=null; 
    public Integer qi436b_b=null; 
    public Integer qi436b_c=null; 
    public Integer qi436b_d=null; 
    public Integer qi436b_e=null; 
    public Integer qi436b_f=null; 
    public Integer qi436b_g=null; 
    public Integer qi436b_h=null; 
    public Integer qi436b_x=null; 
    public String qi436b_xi=null; 
    public Integer qi436c=null; 
    public Integer qi438=null; 
    public Integer qi439=null; 
    public Integer qi440=null; 
    public String qi440x=null; 
    public Integer qi440b=null;
    public String qi440b_o=null;
    public Integer qi441=null; 
    public String qi442_nom=null; 
    public Integer qi442=null; 
    public String qi442x=null; 
    public Integer qi444=null; 
    public Integer qi445=null; 
    public Integer qi446=null; 
    public Integer qi447=null; 
    public Integer qi448=null; 
    public String qiobs_4a=null; 
     
    public CISECCION_04A() {} 
    
   
    public Integer getConvertQi406(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=998; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi406(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 998: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
    
    public Integer getConvertQi411a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
    	
	public Integer getConvertQi411b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi411c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
    public Integer getConvertQi411d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
    
	public Integer getConvertQi411e(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411e(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi411f(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411f(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi411g(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411g(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi411h(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi411h(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi412(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi412(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi412a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi412a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi414(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi414(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
		
	public Integer getConvertQi417(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi417(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi421(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi421(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
//	public Integer getConvertQi422a(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 4: valorReturn=8; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
//	public Integer setConvertQi422a(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 8: valorReturn=4; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
	
	public Integer getConvertQi422a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi422a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi423(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi423(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi424(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi424(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi425(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi425(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi426fb(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 5: valorReturn=998; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi426fb(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 998: valorReturn=5; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi427a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=998; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi427a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 998: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi430(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi430(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi430a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi430a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi430b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=99998; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi430b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 99998: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi430c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi430c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi430d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		
		}
		return valorReturn;
	}
	
	public Integer setConvertQi430d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi431a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi431a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi432(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 2: valorReturn=3; break;
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi432(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=2; break;
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi433(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi433(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi433a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=998; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi433a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 998: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi436(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi436(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi436a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi436a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi440b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		case 4: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi440b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		case 96: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi446(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi446(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	

	public Integer getConvertQi447(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi447(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}

} 

