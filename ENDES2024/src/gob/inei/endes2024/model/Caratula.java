package gob.inei.endes2024.model;
import gob.inei.dnce.annotations.FieldCalificacion;
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.util.Util;

import java.io.Serializable; 
import java.math.BigDecimal; 
public class Caratula extends Entity implements Serializable{ 
//	@FieldCalificacion(ignoreField = true)
//	@FieldEntity(ignoreField = true, saveField = false)
//	public static final int URBANO = 1;
//	@FieldEntity(ignoreField = true, saveField = false)
    private static final long serialVersionUID = 1L; 
//	@FieldCalificacion(ignoreField = true)
    public String conglomerado=null; 
    public String nselv=null; 
//	@FieldCalificacion(ignoreField = true)
    public Integer tipsel=null; 
    public Integer ccdd=null; 
    public String nom_dpto=null; 
    public Integer ccpp=null; 
    public String provincia=null; 
    public Integer ccdi=null; 
    public String distrito=null; 
    public Integer cccpp=null; 
    public String nom_ccpp=null; 
    public String zon_num=null; 
    public String mza = null;
    public String mz=null; 
    public String aer_ini=null; 
    public String aer_fin=null; 
    public String vivienda=null; 
    public Integer tipvia=null; 
    public String tipvia_o=null;
    public String nomvia=null; 
    public String ptanum=null; 
    public String block=null; 
    public String interior=null; 
    public String piso=null; 
    public String lote=null; 
    public BigDecimal km=null; 
    public Integer n_hogar=null; 
    public String gps_long=null; 
    public String gps_lat=null;
    public String gps_alt=null; 
    public Integer p19= null;
    public Integer resviv = null;   
    public String qhvresul_o = null;
    public Caratula() {} 
    public String getTipoVivienda() {
		if (tipsel == null) {
			return "INDEFINIDO";
		} else if (!Util.esDiferente(tipsel, 1)) {
			return "URBANO";
		} else {
			return "RURAL";
		}
	}
}