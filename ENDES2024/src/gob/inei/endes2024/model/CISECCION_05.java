package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable; 
public class CISECCION_05 extends Entity implements IDetailEntityComponent, Serializable ,Coberturable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer pareja_id=null;
    public Integer qi513bu=null; 
    public Integer qi513bn=null; 
    public Integer qi514=null; 
    public Integer qi514a=null; 
    public Integer qi515=null; 
    public String qi515x=null; 
    public Integer qi516u=null; 
    public Integer qi516n=null; 
    public Integer qi516b=null; 
    public Integer qi516c=null; 
    public Integer qi516d=null; 
    public Integer qi516e=null; 
    public Integer qi516f=null; 
    public Integer qi517=null; 
    public Integer qi521=null; 
   
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
    
    public String getOrdenPareja(){
    	if(pareja_id!=null){
    		if (pareja_id==1) {
    			return "ULTIMO";
    		}
    		else if (pareja_id==2) {
    			return "PENULTIMO";
    		}
    		else if (pareja_id==3) {
    			return "ANTEPENULTIMO";
    		}
    		else {
    			return  "";
    		}
    	}else{
    		return  "";
    	}
    }
    
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer qi106 = null;
    
    public CISECCION_05() {}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	} 
} 
