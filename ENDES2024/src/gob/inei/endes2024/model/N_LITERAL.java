package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable;

public class N_LITERAL extends Entity implements Serializable, IDetailEntityComponent {
	private static final long serialVersionUID = 1L;
	
	public String n1 = null;
	public String n1_literal = null;
	public String n2 = null;
	public String n2_literal = null;
	public String n3 = null;
	public String n3_literal = null;
	public String n4_literal = null;
	public String n5_literal = null;

	public String color = "0";

	public String getN1_literal() {
		return n1_literal;
	}

	public void setN1_literal(String n1_literal) {
		this.n1_literal = n1_literal;
	}

	
	public String getN2_literal() {
		return n2_literal;
	}

	public void setN2_literal(String n2_literal) {
		this.n2_literal = n2_literal;
	}
	
	public String getN3_literal() {
		return n3_literal;
	}

	public void setN3_literal(String n3_literal) {
		this.n3_literal = n3_literal;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer activo = null;
	
	

	public N_LITERAL() {
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + ((codccpp == null) ? 0 : codccpp.hashCode());
//		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
//		result = prime * result
//				+ ((frente_id == null) ? 0 : frente_id.hashCode());
//		result = prime * result + ((id_reg == null) ? 0 : id_reg.hashCode());
//		result = prime * result + ((idp33 == null) ? 0 : idp33.hashCode());
//		result = prime
//				* result
//				+ ((manzana_final_id == null) ? 0 : manzana_final_id.hashCode());
//		result = prime * result + ((ubigeo == null) ? 0 : ubigeo.hashCode());
//		result = prime * result + ((zona_id == null) ? 0 : zona_id.hashCode());
//		return result;
//	}

	

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}	
	
	public boolean esEspecifique() {
		if ("96".equals(n1)) {
			return true;
		} 
		return false;
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

}
