package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

public class Incentivos extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	
	public String qh8_01a = null;
    public Integer qh8_01 = null;
    public Integer qh8_02_a = null;
    public Integer qh8_02_b = null;
    public Integer qh8_02_c = null;
    public Integer qh8_02_d = null;
    public Integer qh8_02_e = null;
    public Integer qh8_02_f = null;
    public Integer qh8_02_x = null;
    public String qh8_02_xo = null;
    
    public Integer qh8_03_a = null;
    public Integer qh8_03_b = null;
    public Integer qh8_03_c = null;
    public Integer qh8_03_d = null;
    public Integer qh8_03_e = null;
    public Integer qh8_03_f = null;
    public Integer qh8_03_g = null;
    public Integer qh8_03_h = null;
    public String qh8_03_ho = null;
    public Integer qh8_03_i = null;
    public String qh8_03_io = null;
    public Integer qh8_03_j = null;
    public Integer qh8_03_k = null;
    public Integer qh8_03_l = null;
    public Integer qh8_03_m = null;
    public Integer qh8_03_n = null; 
    public Integer qh8_03_o = null;    
    public Integer qh8_03_p = null;
    public String qh8_03_po = null;
    public Integer qh8_03_q = null;
    public String qh8_03_qo = null;
    public Integer qh8_03_r = null;
    public Integer qh8_03_s = null;
    public Integer qh8_03_t = null;
    public Integer qh8_03_u = null;
    public Integer qh8_03_v = null;
    public Integer qh8_03_w = null;
    public Integer qh8_03_x = null;
    public String qh8_03_xo = null;
    public Integer qh8_03_y = null;
    public String qh8_03_yo = null;
    public Integer qh8_03_z = null;
    public String qh8_03_zo = null;
    
    public Integer qh8_03c_a = null;
    public Integer qh8_03c_b = null;
    public Integer qh8_03c_c = null;
    public Integer qh8_03c_d = null;
    public Integer qh8_03c_e = null;
    public Integer qh8_03c_f = null;
    public Integer qh8_03c_g = null;
    public Integer qh8_03c_h = null;
    public Integer qh8_03c_i = null;
    public Integer qh8_03c_j = null;
    public Integer qh8_03c_k = null;
    public Integer qh8_03c_l = null;
    public Integer qh8_03c_m = null;
    public Integer qh8_03c_n = null;
    public Integer qh8_03c_o = null;
    public Integer qh8_03c_p = null;
    public Integer qh8_03c_q = null;
    public Integer qh8_03c_r = null;
    public Integer qh8_03c_s = null;
    public Integer qh8_03c_t = null;
    public Integer qh8_03c_u = null;
    public Integer qh8_03c_v = null;
    public Integer qh8_03c_w = null;
    public Integer qh8_03c_x = null;
    public Integer qh8_03c_y = null;
    public Integer qh8_03c_z = null;
    
    public Integer qh8_04_a = null;
    public Integer qh8_04_b = null;
    public Integer qh8_04_c = null;
    public Integer qh8_04_d = null;
    public Integer qh8_04_e = null;
    public Integer qh8_04_f = null;
    public Integer qh8_04_g = null;
    public Integer qh8_04_h = null;
    public Integer qh8_04_i = null;
    public Integer qh8_04_j = null;
    public Integer qh8_04_k = null;
    public Integer qh8_04_l = null;
    public Integer qh8_04_m = null;
    public Integer qh8_04_x = null;
    public String qh8_04_xo = null;
    
    public String qh8_05 = null;
	

	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	
	
	public Incentivos() {
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getNombre() {
		return "";//"Hogar N� " + hogar_id + " - " + getJefeHogar();
	}
	


	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer setValue3a8(Integer qh233){
		Integer valorReturn=0;
		switch(qh233){
			case 3: valorReturn=8; break;	
			default: valorReturn=qh233;
		}
		return valorReturn;
	}
	public Integer getValue8a3(Integer qh233){
		Integer valorReturn = 0;
		switch (qh233) {
			case 8: valorReturn = 3; break;
			default:valorReturn = qh233;
		}
		return valorReturn;
	}
	
}
