package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Spinneable;
import gob.inei.dnce.util.Util;

import java.io.Serializable; 
import java.math.BigDecimal; 

import android.util.Log;
public class CSSECCION_08 extends Entity implements IDetailEntityComponent,
Coberturable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer persona_id_ninio=null;
    public Integer qs802=null; 
    public String qs802n=null; 
    public Integer qs802a=null; 
    public String qs802a_o=null; 
    public String qs802bh=null; 
    public String qs802bm=null; 
    public Integer qs802cd=null; 
    public Integer qs802cm=null; 
    public Integer qs802ca=null; 
    public Integer qs802d=null; 
    public String qs802cons=null;
    public Integer qs803=null; 
    public Integer qs804=null; 
    public Integer qs804c=null; 
    public Integer qs805=null; 
    public String qs805_o=null; 
    public Integer qs806=null; 
    public Integer qs807_a=null; 
    public Integer qs807_b=null; 
    public Integer qs807_c=null; 
    public Integer qs807_d=null; 
    public Integer qs807_e=null; 
    public Integer qs807_f=null; 
    public Integer qs807_g=null; 
    public Integer qs807_h=null; 
    public Integer qs807_i=null; 
    public Integer qs807_j=null; 
    public Integer qs807_x=null; 
    public String qs807_o=null; 
    public Integer qs807_y=null; 
    public Integer qs809=null; 
    public Integer qs810=null; 
    public Integer qs811=null; 
    public Integer qs812=null; 
    public Integer qs812a=null; 
    public Integer qs813=null; 
    public Integer qs814=null; 
    public Integer qs817=null; 
    public Integer qs818=null; 
    public Integer qs818a=null; 
    public Integer qs819=null; 
    public String qs819_o=null; 
    public Integer qs820=null; 
    public Integer qs821a=null; 
    public Integer qs821b=null; 
    public Integer qs821c=null; 
    public Integer qs821d=null; 
    public Integer qs821x=null; 
    public String qs821_o=null; 
    public Integer qs821y=null; 
    public Integer qs822=null; 
    public Integer qs822a=null; 
    public Integer qs823=null; 
    public String qs823_o=null; 
    public Integer qs824=null; 
    public Integer qs825=null; 
    public String qs825_o=null; 
    public Integer qs826=null; 
    public Integer qs827=null; 
    public String qs827_o=null; 
    public Integer qs828=null; 
    public Integer qs829=null; 
    public Integer qs831=null; 
    public Integer qs832=null; 
    public Integer qs833=null;
    public Integer qs834=null; 
    public Integer qs835=null; 
    public Integer qs836=null; 
    public Integer qs837=null; 
    public Integer qs838_a=null; 
    public Integer qs838_b=null; 
    public Integer qs838_c=null; 
    public Integer qs838_d=null; 
    public Integer qs838_e=null; 
    public Integer qs838_f=null; 
    public Integer qs838_g=null; 
    public Integer qs838_h=null; 
    public Integer qs838_i=null; 
    public Integer qs838_j=null; 
    public Integer qs838_k=null; 
    public Integer qs838_l=null; 
    public Integer qs838_m=null; 
    public Integer qs838_n=null; 
    public Integer qs838_x=null; 
    public String qs838_o=null; 
    public Integer qs838_y=null; 
    public Integer qs838_z =null;
    public String qs838a_h=null; 
    public String qs838a_m=null; 
    public Integer qs840a=null; 
    public Integer qs840ppm=null;     
    public Integer qs840b=null; 
    public String qs840_o=null; 
    
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh02_2=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qh06=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qh07=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public boolean estadofinal=false;
	@FieldEntity(ignoreField=true, saveField=false)
	public boolean estadoseccion1_7=false;
//	@FieldEntity(ignoreField=true, saveField=false)
//	public boolean estadoseccion8P840=false;

    public CSSECCION_08() {} 
    
    public String getSexo()
    {String Sexo= "";
    Integer sexo=qh06!=null?qh06:0;
    	switch (sexo) {
		case 1:Sexo="Hombre";		
			break;

		case 2:Sexo="Mujer";
			break;
		}
    	return Sexo;
    }
   
	public String getNombre() {
		String nombre = "";
		qh02_2=qh02_2==null?"":qh02_2;
		if (qh02_1 != null) {
			nombre = Util.getText(qh02_1)+" "+Util.getText(qh02_2);
		}
		return nombre;
	}
	 public String get802a(){
//		 	 Log.e("estado",""+estado);
//		 	Log.e("qs802a",""+qs802a);
			 String cadena = "";
			 switch (qs802a){			 
				case 1: if (estado==null ){cadena="5. INCOMPLETA";} else{cadena="1. COMPLETA";}break;
				case 2: cadena="2. AUSENTE"; break;
				case 4: cadena="4. RECHAZADA"; break;
				case 5: cadena="5. INCOMPLETA"; break;
				case 6: cadena="6. DISCAPACIDA(O)"; break;
				case 9: cadena="9. OTRA"; break;		
				
				}
//			 Log.e("cadena",""+cadena);
			 return cadena;
	 }
	public Integer getConvertqs802a(Integer qs802a) {
		Integer valorReturn=0;
		switch (qs802a){
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 6: valorReturn=9; break;
		default: valorReturn=qs802a;
		}
		return valorReturn;
	}
	public Integer setConvertqs802a(Integer qs802a) {
		Integer valorReturn=0;
		switch (qs802a){
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		case 9: valorReturn=6; break;
		default: valorReturn=qs802a;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs804(Integer qs804) {
		Integer valorReturn=0;
		switch (qs804){
		case 3: valorReturn=8; break;
		default: valorReturn=qs804;
		}
		return valorReturn;
	}
	public Integer setConvertqs804(Integer qs804) {
		Integer valorReturn=0;
		switch (qs804){
		case 8: valorReturn=3; break;
		default: valorReturn=qs804;
		}
		return valorReturn;
	}
	public Integer getConvertqs806(Integer qs806) {
		Integer valorReturn=0;
		switch (qs806){
		case 4: valorReturn=9; break;
		default: valorReturn=qs806;
		}
		return valorReturn;
	}
	public Integer setConvertqs806(Integer qs806) {
		Integer valorReturn=0;
		switch (qs806){
		case 9: valorReturn=4; break;
		default: valorReturn=qs806;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs819(Integer qs805) {
		Integer valorReturn=0;
		switch (qs805){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=20; break;		
		case 8: valorReturn=30; break;
		case 9: valorReturn=31; break;
		case 10: valorReturn=40; break;
		case 11: valorReturn=41; break;
		case 12: valorReturn=42; break;
		case 13: valorReturn=96; break;
		case 14: valorReturn=98; break;
		case 15: valorReturn=99; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}	
	public Integer setConvertqs819(Integer qs805) {
		Integer valorReturn=0;
		switch (qs805){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 20: valorReturn=7; break;
		case 30: valorReturn=8; break;
		case 31: valorReturn=9; break;
		case 40: valorReturn=10; break;
		case 41: valorReturn=11; break;
		case 42: valorReturn=12; break;
		case 96: valorReturn=13; break;
		case 98: valorReturn=14; break;
		case 99: valorReturn=15; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertqs805(Integer qs805) {
		Integer valorReturn=0;
		switch (qs805){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=20; break;
		case 8: valorReturn=30; break;
		case 9: valorReturn=31; break;
		case 10: valorReturn=40; break;
		case 11: valorReturn=41; break;
		case 12: valorReturn=42; break;
		case 13: valorReturn=96; break;
		case 14: valorReturn=98; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}	
	public Integer setConvertqs805(Integer qs805) {
		Integer valorReturn=0;
		switch (qs805){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 20: valorReturn=7; break;
		case 30: valorReturn=8; break;
		case 31: valorReturn=9; break;
		case 40: valorReturn=10; break;
		case 41: valorReturn=11; break;
		case 42: valorReturn=12; break;
		case 96: valorReturn=13; break;
		case 98: valorReturn=14; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs825(Integer qs825) {
		Integer valorReturn=0;
		switch (qs825){		
		case 5: valorReturn=6; break;
		case 6: valorReturn=8; break;
		default: valorReturn=qs825;
		}
		return valorReturn;
	}
	public Integer setConvertqs825(Integer qs825) {
		Integer valorReturn=0;
		switch (qs825){
		case 6: valorReturn=5; break;
		case 8: valorReturn=6; break;
		default: valorReturn=qs825;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs828(Integer qs828) {
		Integer valorReturn=0;
		switch (qs828){		
		case 4: valorReturn=8; break;
		default: valorReturn=qs828;
		}
		return valorReturn;
	}
	public Integer setConvertqs828(Integer qs828) {
		Integer valorReturn=0;
		switch (qs828){
		case 8: valorReturn=4; break;		
		default: valorReturn=qs828;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs834(Integer qs834) {
		Integer valorReturn=0;
		switch (qs834){		
		case 5: valorReturn=8; break;
		default: valorReturn=qs834;
		}
		return valorReturn;
	}
	public Integer setConvertqs834(Integer qs834) {
		Integer valorReturn=0;
		switch (qs834){
		case 8: valorReturn=5; break;		
		default: valorReturn=qs834;
		}
		return valorReturn;
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	
	
} 
