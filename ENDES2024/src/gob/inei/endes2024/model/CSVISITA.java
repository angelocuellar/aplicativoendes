package gob.inei.endes2024.model; 
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;
import android.util.Log;
public class CSVISITA extends Entity implements IDetailEntityComponent,	Coberturable {
	 private static final long serialVersionUID = 1L; 
	    public Integer hogar_id=null; 
	    public Integer persona_id=null; 
	    public Integer nro_visita=null; 
	    public String qsvdia=null; 
	    public String qsvmes=null; 
	    public Integer qsvanio=null;
	    public String qsvhora_ini = null;
	    public String qsvminuto_ini = null;
	    public String qsvhora_fin = null;
	    public String qsvminuto_fin = null;
	    public Integer qsventrev=null; 
	    public Integer qsvresul=null; 
	    public String qsvresul_o=null; 
	    public String qsprox_dia=null; 
	    public String qsprox_mes=null; 
	    public String qsprox_anio=null; 
	    public String qsprox_hora=null; 
	    public String qsprox_minuto=null; 
	    public Integer qsresult=null; 
	    public String qsresult_o=null; 
	   
	    public CSVISITA() {}
		@Override
		public boolean isTitle() {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public String getNombre() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public void cleanEntity() {
			// TODO Auto-generated method stub
			
		} 
		
		public String getResultado(){
		String resultado="";
		int op = qsvresul!=null?Util.getInt(qsvresul):-1;
		switch (op) {
		case 1:resultado= "Completa";break;
		case 2:resultado="Ausente";break;
		case 3:resultado="Aplazada";break;
		case 4:resultado="Rechazada";break;
		case 5:resultado="Incompleta";break;
		case 6:resultado="Discapacitada(o)";break;
		case 9:resultado="Otro";break;
		default:resultado="";break;
			}
		return resultado;
		}
		public String getFechainicio() {
			String fecha = "",dia="",mes="";
			
			if (qsvdia != null && qsvmes != null) {
				if(qsvdia.length()<2) dia="0"+qsvdia.toString(); else  dia= qsvdia.toString();
				if(qsvmes.length()<2) mes="0"+qsvmes.toString(); else mes= qsvmes.toString();
				fecha = dia + "/" + mes;
			}
			return fecha;
		}
		public String getHoraInicio() {
			String Horainicio = "";
			if (qsvhora_ini != null && qsvminuto_ini != null) {
				Horainicio = (qsvhora_ini.toString().length() == 1 ? "0"+ qsvhora_ini.toString() : qsvhora_ini.toString())
						+ ":"+ (qsvminuto_ini.toString().length() == 1 ? "0"+ qsvminuto_ini.toString() : qsvminuto_ini.toString());
			}
			return Horainicio;
		}
		public String getFechaProxima() {
			String fechaproxima = "",dia="",mes="";

			if (qsprox_mes != null && qsprox_dia != null) {
				if(qsprox_dia.length()<2) dia="0"+qsprox_dia.toString(); else  dia= qsprox_dia.toString();
				if(qsprox_mes.length()<2) mes="0"+qsprox_mes.toString(); else mes= qsprox_mes.toString();
				fechaproxima = dia + "/" + mes;
			}
			return fechaproxima;
		}
		public String getHoraProxima() {
			String proximahora = "";
			if (qsprox_hora != null && qsprox_minuto != null) {
				proximahora = (qsprox_hora.toString().length() == 1 ? "0"
						+ qsprox_hora.toString() : qsprox_hora.toString())
						+ ":"
						+ (qsprox_minuto.toString().length() == 1 ? "0"	+ qsprox_minuto.toString() : qsprox_minuto.toString());
			}
			return proximahora;
		}
		public String getHoraFin()
		{String horaFinal="";
			if(qsvhora_fin!=null && qsvminuto_fin!=null)
			{
				horaFinal = (qsvhora_fin.toString().length()==1?"0"+qsvhora_fin.toString():qsvhora_fin.toString())+":"+(qsvminuto_fin.toString().length()==1?"0"+qsvminuto_fin.toString():qsvminuto_fin.toString());
			}
			return horaFinal;
		}
		
		public Integer setConvertResult(Integer qsvresul) {
			Integer valorReturn=0;
			switch(qsvresul){
			case 9: valorReturn=7; break;
			default: valorReturn=qsvresul;
			}
			return valorReturn;
	    }
		
		public Integer getConvertResult(Integer qsvresul) {
			Integer valorReturn = qsvresul;
			switch (qsvresul) {
			case 7: valorReturn = 9; break;	
			}
			return valorReturn;
	     }
} 
