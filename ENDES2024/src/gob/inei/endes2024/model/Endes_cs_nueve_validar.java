package gob.inei.endes2024.model;

import java.math.BigDecimal;

import gob.inei.dnce.components.Entity;

public class Endes_cs_nueve_validar extends Entity {
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    
    public BigDecimal qs900=null; 
    public BigDecimal qs901=null; 
    public Integer qs902=null; 
    public String qs902_o=null; 
    public Integer qs903=null; 
    public Integer qs903_1=null; 
    public String qs904h=null; 
    public String qs904m=null; 
    public Integer qs905=null; 
    public Integer qs905_1=null; 
    public Integer qs906=null;     
    public String qs906_o=null;
    
    public BigDecimal qs907=null;
    public Integer qs908=null; 
    public String qs908_o=null; 
    
    public String qsnom_ant=null; 
    public String qscod_ant=null; 
    public String qsnom_aux=null; 
    public String qscod_aux=null; 
    public String qsobs_antro=null; 
    public String qsobs_entrev=null; 
    public String qsobs_super=null;
}
