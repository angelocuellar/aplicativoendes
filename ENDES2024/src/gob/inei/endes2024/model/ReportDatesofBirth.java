package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

public class ReportDatesofBirth extends Entity implements IDetailEntityComponent  {
	public String conglomerado=null;
	public String vivienda =null;
	public String hogar=null;
	public Integer orden=null;
	public Integer sexo=null;
	public Integer edad=null;
	public String nombre=null;
	public String fecha_hogar=null;
	public String fecha_seccion04=null;
	public String fecha_individual=null;
	public String fecha_individualseccion02=null;
	public String fecha_salud=null;
	public String fechaseccion08=null;

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	public String getSexo(){
		String retornasexo="";
		if(sexo==1)
			retornasexo="Hombre";
		else
			retornasexo="Mujer";
		
		return retornasexo;
	}
	public String getEdad(){
		String retornaedad="";
		retornaedad=edad.toString().length()<2?"0"+edad:edad.toString();
		return retornaedad;
	}
}
