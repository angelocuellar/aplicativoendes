package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;

import java.io.Serializable;

import android.location.GpsStatus.NmeaListener;

public class CISECCION_10_03 extends Entity implements Serializable,IDetailEntityComponent{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer ninio_id=null;
    public Integer vacuna_id=null;
    
    public String qi456_vac_g_d1=null;
    public String qi456_vac_g_m1=null;
    public Integer qi456_vac_g_a1=null;
    
    public String qi456_vac_g_d2=null;
    public String qi456_vac_g_m2=null;
    public Integer qi456_vac_g_a2=null;
    
    public String qi456_vac_g_d3=null;
    public String qi456_vac_g_m3=null;
    public Integer qi456_vac_g_a3=null;

    public CISECCION_10_03() {} 
    public String getCodigodeVacuna(){
    	String Nombre="";
    	if(vacuna_id!=null){
    		switch (vacuna_id) {
			case 1:Nombre ="01";
				break;
			case 2:Nombre ="02";
				break;
			case 3:Nombre="03";
				break;
			case 4:Nombre="04";
				break;
			case 5:Nombre="05";
				break;
			case 6:Nombre="06";
				break;
			case 7:Nombre="07";
				break;
			case 8:Nombre="08";
				break;
			case 9:Nombre="09";
				break;
			case 10:Nombre="10";
				break;
			case 11:Nombre="11";
				break;
			case 12:Nombre="12";
				break;
			case 13:Nombre="13";
				break;
			case 14:Nombre="14";
				break;
			case 15:Nombre="15";
				break;
			
			case 16:Nombre="16";
				break;
			case 17:Nombre="17";
				break;
			case 18:Nombre="18";
				break;
			case 19:Nombre="19";
				break;
			}
    	}
    	return Nombre;
    }

    
    public String getNombredeVacuna(){
    	String Nombre="";
    	if(vacuna_id!=null){
    		switch (vacuna_id) {
			case 1:Nombre = "BCG (Reci�n nacido)";
				break;
			case 2:Nombre = "ANTIHEPATITIS B HvB (Reci�n nacido)";
				break;
			case 3:Nombre="POLIO";
				break;
			case 4:Nombre="PENTAVALENTE (DPT + HVB + HIB)";
				break;
			case 5:Nombre="DPT";
				break;
			case 6:Nombre="DT";
				break;
			case 7:Nombre="ANTIHEPATITIS B - HVB";
				break;
			case 8:Nombre="ANTIHAEMOPHILUS - HIB";
				break;
			case 9:Nombre="TETRAVALENTE";
				break;
			case 10:Nombre="HEXAVALENTE";
				break;
			case 11:Nombre="ROTAVIRUS";
				break;
			case 12:Nombre="NEUMOCOCO";
				break;
			case 13:Nombre="INFLUENZA";
				break;
			case 14:Nombre="ANTISARAMPIONOSA / SPR";
				break;
			case 15:Nombre="ANTIAMAR�LICA / AMA";
				break;
			case 16:Nombre="VARICELA";
				break;
			case 17:Nombre="REFUERZOS DPT";
				break;
			case 18:Nombre="REFUERZOS DT";
				break;
			case 19:Nombre="REFUERZOS POLIO";
				break;
	    	
    		}
    	}
    	return Nombre;
    }
    
	 public String PrimeraFecha(){
		 String fecha="";
		 if(qi456_vac_g_d1!=null && qi456_vac_g_m1!=null && qi456_vac_g_a1!=null){
			 fecha=qi456_vac_g_d1+"/"+qi456_vac_g_m1+"/"+qi456_vac_g_a1;
		 }
		 else{
			 fecha=qi456_vac_g_d1!=null?qi456_vac_g_d1+"":"";
		 }
		 return fecha;
    }
	 public String SegundaFecha(){
		 String fecha="";
		 if(qi456_vac_g_d2!=null && qi456_vac_g_m2!=null && qi456_vac_g_a2!=null){
			 fecha=qi456_vac_g_d2+"/"+qi456_vac_g_m2+"/"+qi456_vac_g_a2;
		 }
		 else{
			 fecha=qi456_vac_g_d2!=null?qi456_vac_g_d2+"":"";
		 }
		 return fecha;
    }
	 public String TerceraFecha(){
		 String fecha="";
		 if(qi456_vac_g_d3!=null && qi456_vac_g_m3!=null && qi456_vac_g_a3!=null){
			 fecha=qi456_vac_g_d3+"/"+qi456_vac_g_m3+"/"+qi456_vac_g_a3;
		 }
		 else{
			 fecha=qi456_vac_g_d3!=null?qi456_vac_g_d3+"":"";
		 }
		 return fecha;
    }
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}

}