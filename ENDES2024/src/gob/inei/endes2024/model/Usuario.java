package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Spinneable;

public class Usuario extends Entity implements IDetailEntityComponent,
Coberturable,Spinneable{
	
	private static final long serialVersionUID = 1L;
	public String usuario; 
	public String clave;
	public Integer cargo_id; 
	public String telefono; 	
	public String email; 
	public String estado; 
	public String nombres; 
	public String apellidos; 
	public String correo; 
	public String dni;
	public String cod_sede_operativa; 
	public String ccdd; 
	public String ccpp; 
	public String ccdi;
	public String jefe_equipo; 
	public String equipo; 
	public String ruta;
	
	@FieldEntity(ignoreField=true)
	public Usuario jefeEquipo;
	@FieldEntity(ignoreField=true)
	public Usuario supervisor;
	@FieldEntity(ignoreField=true)
	public Usuario coordinador;
	@FieldEntity(ignoreField=true)
	public Usuario supervisor_nacional;
	
	@Override
	public String toString() {
		return "UsuarioBE [_id=" + id + ", usuario=" + usuario + "]";
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSpinnerLabel() {
		// TODO Auto-generated method stub
		apellidos=apellidos==null?"":apellidos;
		return usuario==null? nombres:"("+usuario+") "+nombres+"-"+apellidos;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	
}
