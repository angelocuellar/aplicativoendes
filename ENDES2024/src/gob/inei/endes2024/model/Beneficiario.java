package gob.inei.endes2024.model;

public class Beneficiario {
	public int num_beneficiario;
	public String nombre_beneficiario;
	public int anios_beneficio;
	public int meses_beneficio;
	
	public Beneficiario(){}
	
	public Beneficiario(int num_beneficiario, String nombre_beneficiario, 
						int anios_beneficio, int meses_beneficio){
		this.num_beneficiario = num_beneficiario;
		this.nombre_beneficiario = nombre_beneficiario;
		this.anios_beneficio = anios_beneficio;
		this.meses_beneficio = meses_beneficio;
	}
}
