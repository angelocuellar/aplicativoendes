package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

public class Seccion03 extends Entity implements IDetailEntityComponent,
Coberturable{
	private static final long serialVersionUID = 1L; 
	public Integer hogar_id=null;
	public Integer persona_id=null;
	public Integer persona_id_orden=null;
	public Integer pregunta_id=null;	
	public Integer qhs3_1a = null;
	public Integer qhs3_1m=null;	
	public Integer qh96ac = null;
	public String qh97dni = null;	
	public Integer qh97d = null;
	public Integer qh97m=null;
	public Integer qh97a=null;	
	public Integer qh109_1r=null;	
//	public Integer qh109_1a=null;
//	public Integer qh109_1m =null;	
	public Integer qh110h=null;
	public Integer qh110m = null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1=null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh97_fecha=null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado3;
	
//	
	
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	
	public Integer getConvertqh92(Integer qh92) {
		Integer valorReturn=0;
		switch (qh92){
		case 1: valorReturn=98; break;
		default: valorReturn=qh92;
		}
		return valorReturn;
	}
	public Integer setConvertqh92(Integer qh92) {
		Integer valorReturn=0;
		switch (qh92){
		case 98: valorReturn=1; break;
		default: valorReturn=qh92;
		}
		return valorReturn;
	}
	
	public Integer getConvertqh97a(Integer qh97a) {
		Integer valorReturn=0;
		switch (qh97a){
		case 1: valorReturn=9998; break;
		default: valorReturn=qh97a;
		}
		return valorReturn;
	}
	public Integer setConvertqh97a(Integer qh97a) {
		Integer valorReturn=0;
		switch (qh97a){
		case 9998: valorReturn=1; break;
		default: valorReturn=qh97a;
		}
		return valorReturn;
	}
	
	public boolean IniciarQh97_fecha(){
		boolean entro=false;
		if(this.qh97a!=null){
			this.qh97_fecha = ""+this.qh97d+"/"+this.qh97m+"/"+this.qh97a;
			entro = true;
		}
		return entro;
	}
	public Integer getConver109_r(Integer qh109_r) {
		Integer valorReturn=0;
		switch (qh109_r) {
		case 3: valorReturn=98; break;
		default: valorReturn=qh109_r;
		}
		return valorReturn;
	}
	public Integer setConver109_r(Integer qh109_r) {
		Integer valorReturn=0;
		switch (qh109_r) {
		case 98: valorReturn=3; break;
		default: valorReturn=qh109_r;
		}
		return valorReturn;
	}
	
	public String getQh109_1r(){
		String p109_1r="";
		
		switch(this.qh109_1r){
			case 1: p109_1r="1. SI";break;
			case 2: p109_1r="2. NO";break;
			case 98: p109_1r="98. NS";break;
			default: p109_1r="";
		}
		
		return p109_1r;
	}
}
