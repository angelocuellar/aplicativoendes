package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.endes2024.common.MyUtil;

import java.io.Serializable; 
import java.math.BigDecimal; 
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.util.Log;
public class Seccion04_05 extends Entity implements Serializable,IDetailEntityComponent,
Coberturable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer persona_id_orden=null;
    public Integer qh202=null;
    public String qh203d=null; 
    public String qh203m=null; 
    public Integer qh203y=null; 
    public BigDecimal qh204=null; 
    public BigDecimal qh205=null; 
    
    public Integer qh205a=null; 
    public Integer qh205b=null; 
    public BigDecimal qh205c=null; 
    public Integer qh205c1=null;
    
    public Integer qh206=null; 
    public Integer qh207=null; 
    public Integer qhmeses=null;
    public String qh207_o=null;
    public String qh207a_d=null; 
    public String qh207a_m=null; 
    public Integer qh207_y=null; 
    public Integer qh208=null; 
    public Integer qh209=null; 
    public Integer qh210=null; 
    public BigDecimal qh211=null; 
    public Integer qh212=null; 
    public Integer qh212ah=null;
    public Integer qh212am=null;
    public String qh212b_d=null; 
    public String qh212b_m=null; 
    public Integer qh212b_y=null; 
    
    public Integer qh212c=null;
    public Integer qh212d=null;
    public Integer qh212e=null;
    public String qh212e_o=null;
    public Integer qh212f1=null;
    public BigDecimal qh212f=null;
    public Integer qh212gh=null;
    public Integer qh212gm=null;
    public String qh212h_d=null; 
    public String qh212h_m=null; 
    public Integer qh212h_y=null;
    public Integer qh212i=null;
    public Integer qh212j=null;
    public String qh212j_o=null;
    public BigDecimal qh212k=null;
    public Integer qh212l=null;

    
    public Integer qh213=null; 
    public String qh213_o=null;
    public Integer qhrec_04=null;
    public String qhfrc_fecha04=null;
    public Integer qhrec_05=null;
    public String qhfrc_fecha05=null;
    
    public String qh203a=null;
    public String qh207b=null;
    public String qh209a=null;
    public String qh213a=null;
    
    public String qh200_ref_s4=null;
    public String qh200_ref_s5=null;
    
                 
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer qh07 = null;
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh7dd=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh7mm=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer qh06=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer estados5=null;
    
    
    @FieldEntity(ignoreField=true, saveField=false)
	public String s4dia=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public String s4mes=null;
    @FieldEntity(ignoreField=true, saveField=false)
	public Integer s4anio=null;
    
    public Seccion04_05() {}
    
    
    public String getHoraPmedicion() {
    	String hora="";
		
		if (qh212ah != null && qh212am != null ) {
			hora+= qh212ah + ":" + qh212am;
		} else {
			    hora+="";
		}
		
		return hora;
		
	}
    
    public String getHoraSmedicion() {
    	String hora="";
		
		if (qh212gh != null && qh212gm != null ) {
			hora+= qh212gh + ":" + qh212gm;
		} else {
			    hora+="";
		}
		
		return hora;
		
	}
    
    public String getPeso() {
    	String peso="";
    	if(qh204!=null){
    		peso= Redondear(qh204)+"";
    	}
		return peso;
	}
    public String getTalla() {
    	String talla="";
    	if(qh205!=null){
    		talla=Redondear(qh205)+"";
    	}
		return talla;
	}
    public String getTalla2() {
    	String talla2="";
    	if(qh205c!=null){
    		talla2=Redondear(qh205c)+"";
    	}
		return talla2;
	}
    public String getHemoglonina() {
		String hemoglobina="";
		if(qh211!=null){
			hemoglobina=Redondear(qh211)+"";
		}
		return hemoglobina;
	}
    public String getHemoglonina2() {
		String hemoglobina2="";
		if(qh212f!=null){
			hemoglobina2=Redondear(qh212f)+"";
		}
		return hemoglobina2;
	}
    public String getSexo() {
    	String Sexo="";
		if(qh06==1) Sexo="Hombre";
		else Sexo="Mujer";
		return Sexo;
	}
    public String getMedido() {
    	String medido="";
    	if(qh206!=null){
			if(qh206==1) medido="Acostado";
			else medido="Parado";
    	}
		return medido;
	}
    public String getResultado() {
    	String resultado = "";
    	if(qh207!=null){
			if(qh207==1) resultado="Medida";
			if(qh207==2) resultado="No Presente";
			if(qh207==3) resultado="Rechazo";
			if(qh207==5) resultado="Medida Parcialmente";
			if(qh207==6) resultado="Otro";
    	}
		return resultado;
	}
    public String getResultadoS5() {
    	String resultado = "";
    	if(qh213!=null){
			if(qh213==1) resultado="Medida";
			if(qh213==2) resultado="No Presente";
			if(qh213==3) resultado="Rechazo";
			if(qh213==6) resultado="Otro";
		}
		return resultado;
	}
    public String getConsentimiento() {
    	String resultado = "";
    	if(qh210!=null){
    		if(qh210==1) resultado="Acepto";
    		if(qh210==2) resultado="Rechazo/Otro";
    	}
		return resultado;
	}
    
    public double Redondear(BigDecimal num) {
		Double n=0.0;
		n= num.doubleValue();
		n=Math.rint(n*10)/10;
		return n;
	}
    public String getFecha2() {
		String fecha="";
		if(MyUtil.incluyeRango(0,5, qh07)){
			if (qh7dd != null && qh7mm != null && qh203y!=null) {
				fecha+= qh7dd + "/" + qh7mm+ "/" + qh203y;
			} else {
				    fecha+="";
			}
		}else{
			  fecha+="";	
		}
	    
		return fecha;
	
	}
    public String getFecha3() {
		String fecha="";
		
			if (qh207a_d != null && qh207a_m != null && qh207_y!=null) {
				fecha+= qh207a_d + "/" + qh207a_m+ "/" + qh207_y;
			} else {
				    fecha+="";
			}
		
	    
		return fecha;
	
	}
    
    public String getEdadMesesSeccion4() {
    	
    	Integer s4meses=-1;
    	
    	if (qh207a_d!=null && qh207a_m!=null && qh207_y!=null && qh7dd!=null && qh7mm!=null && qh203y!=null) {
    		
         	Integer dia=Integer.parseInt(qh7dd);
           	Integer mes=Integer.parseInt(qh7mm);
           	Integer a�o=(qh203y);
           	
    		Integer diam=Integer.parseInt(qh207a_d);
    		Integer mesm=Integer.parseInt(qh207a_m);
    		Integer a�om=(qh207_y);
    		
    		Calendar fecha_nacimiento = new GregorianCalendar(a�o, mes-1, dia);
    		Calendar fecha_medicion = new GregorianCalendar(a�om, mesm-1, diam);
    		s4meses=MyUtil.CalcularEdadEnMesesFelix(fecha_nacimiento,fecha_medicion);
		}    	
 		return s4meses==-1?"":s4meses+""; 	
 	}
    
  public String getEdadMesesSeccion5() {
    	
    	Integer s5meses=-1;
    	
    	if (s4dia!=null && s4mes!=null && s4anio!=null && qh7dd!=null && qh7mm!=null && qh203y!=null) {
    		
         	Integer dia=Integer.parseInt(qh7dd);
           	Integer mes=Integer.parseInt(qh7mm);
           	Integer a�o=(qh203y);
           	
    		Integer diam=Integer.parseInt(s4dia);
    		Integer mesm=Integer.parseInt(s4mes);
    		Integer a�om=(s4anio);
    		
    		Calendar fecha_nacimiento = new GregorianCalendar(a�o, mes-1, dia);
    		Calendar fecha_medicion = new GregorianCalendar(a�om, mesm-1, diam);
    		s5meses=MyUtil.CalcularEdadEnMesesFelix(fecha_nacimiento,fecha_medicion);
		}    	
 		return s5meses==-1?"":s5meses+""; 	
 	}
    
    public String getFecha4() {
		String fecha="";
		
			if (qh212b_d != null && qh212b_m != null && qh212b_y!=null) {
				fecha+= qh212b_d + "/" + qh212b_m+ "/" + qh212b_y;
			} else {
				    fecha+="";
			}
		
	    
		return fecha;
	
	}
    public String getFecha5() {
		String fecha="";
		
			if (qh212h_d != null && qh212h_m != null && qh212h_y!=null) {
				fecha+= qh212h_d + "/" + qh212h_m+ "/" + qh212h_y;
			} else {
				    fecha+="";
			}
		
	    
		return fecha;
	
	}
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	} 
	
	public Integer setConvert207(Integer qh207) {
		Integer valorReturn=0;
		switch(qh207){
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		default: valorReturn=qh207;
		}
		return valorReturn;
}
	public Integer getConvert207(Integer qh207) {
		Integer valorReturn = 0;
		switch (qh207) {
		case 4: valorReturn = 5; break;	
		case 5: valorReturn = 6; break;	
		default:valorReturn = qh207;
		}
		return valorReturn;
}
	public Integer setConvert213(Integer qh213) {
		Integer valorReturn=0;
		switch(qh213){
		case 6: valorReturn=4; break;
		default: valorReturn=qh213;
		}
		return valorReturn;
}
	public Integer getConvert213(Integer qh213) {
		Integer valorReturn = 0;
		switch (qh213) {
		case 4: valorReturn = 6; break;	
		default:valorReturn = qh213;
		}
		return valorReturn;
}
    
} 
