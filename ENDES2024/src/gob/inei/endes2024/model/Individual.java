package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

import java.io.Serializable; 
import java.math.BigDecimal; 

public class Individual extends Entity implements IDetailEntityComponent,
Coberturable{  	
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null;
    public String qivdia=null; 
    public String qivmes=null; 
    public Integer qivanio=null;
    public String qivhora=null;
    public String qivmin=null;
    public String qiventrev=null;
    public Integer qivresul=null;
    public String qivresul_o=null;
    public String qiprox_dia=null;
    public String qiprox_mes=null;
    public Integer qiprox_anio=null;
    public String qiprox_hora=null;
    public String qiprox_minuto=null;
//    public Integer qi106=null;
//    public Integer qitotaln=null;
//    public Integer qitotalc=null;
        
    
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_2 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_3 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qh06 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qh07 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer violencia = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qs28 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qs29a = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qs29b = null;
    
    public Individual() {}   
    
    public String getNombre() {
		if (qh02_1 != null) {
			return Util.getText(qh02_1) + " " + Util.getText(qh02_2) + " "
					+ Util.getText(qh02_3);
		} else {
			return  "";
		}
	}
    
    public String getViolencia(){
    	if(violencia!=null){
    		if (violencia==1) {
    			return "SI";
    		} else {
    			return  "NO";
    		}
    	}else{
    		return  "";
    	}
    	
    }
	
    public String getFechaRegistro(){    	
    	if (qivdia!=null) {
			return qivdia+"/"+qivmes+"/"+qivanio;
		} else {
			return  "";
		}
    }
    
    public String getResultado(){
    	String resultado="";
    	if (qivresul!=null) {
    		//1. Completa | 2. Hogar presente pero entrevistado competente ausente | 3. Hogar ausente4. Aplazada | 5. Rechazada | 6. Vivienda desocupada o no es vivienda | 7. Vivienda destruida | 8. Vivienda no encontrada | 9. Otra
    		switch(qivresul){
    			case 1: resultado = "1. COMPLETA";break;
    			case 2: resultado = "2. AUSENTE";break;
    			case 3: resultado = "3. APLAZADA";break;
    			case 4: resultado = "4. RECHAZADA";break;
    			case 6: resultado = "6. DISCAPACITADA";break;
    			case 7: resultado = "7. OTRA";break;
    		}
			return resultado;
		} else {
			return  resultado;
		}
    }

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
} 
