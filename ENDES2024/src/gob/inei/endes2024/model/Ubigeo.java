package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;

public class Ubigeo extends Entity {

	private static final long serialVersionUID = 1L;
	public String ubigeo = null;
	public String ccdd = null;
	public String ccpp = null;
	public String ccdi = null;
	public String departamento = null;
	public String provincia = null;
	public String distrito = null;

	public Ubigeo() {
	}

	@Override
	public String toString() {
		if (ccpp == null && ccdi == null) {
			return ccdd != null ? ccdd + " - " + departamento : departamento;
		} else if (ccdi == null) {
			return ccpp != null ? ccpp + " - " + provincia : provincia;
		} else {
			return ccdi != null ? ccdi + " - " + distrito : distrito;
		}
	}

}
