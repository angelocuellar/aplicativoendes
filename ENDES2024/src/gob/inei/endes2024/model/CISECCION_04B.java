package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_04B extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer ninio_id=null;
    public Integer qi454=null; 
    public Integer qi455=null; 
    public Integer qi456x=null; 
    public String qi456x_o =null;
    public Integer qi456n=null; 
    public String qi456d=null; 
    public String qi456m=null; 
    public Integer qi456y=null; 
    
    public Integer qi456_1a=null;
    public Integer qi456_1b=null;
    public Integer qi456_1x=null;
    public String qi456_1o=null; 
    
    
    public Integer qi466e_1a=null;
    public Integer qi466e_1b=null;
    public Integer qi466e_1x=null;
    public String qi466e_1o=null;
    
    public Integer qi457=null; 
    public Integer qi458=null; 
    public Integer qi459=null; 
    public Integer qi460=null; 
    public Integer qi460a=null; 
    public Integer qi461=null; 
    public Integer qi461a=null; 
    public Integer qi461b=null; 
    public Integer qi461cu=null; 
    public Integer qi461cn=null; 
    public Integer qi461d=null; 
    public Integer qi461e=null; 
    public Integer qi461f=null;     
    public Integer qi461g=null;
    public Integer qi461h=null;
    public Integer qi461i=null;
    public Integer qi461j=null;    
    public Integer qi462=null; 
    public Integer qi462a=null; 
    public Integer qi462b=null; 
    public Integer qi463=null; 
    public Integer qi463a=null; 
    public Integer qi463b=null;
    public Integer qi464=null; 
    public Integer qi464a_a=null; 
    public Integer qi464a_b=null; 
    public Integer qi464a_c=null; 
    public Integer qi464a_x=null; 
    public String qi464a_xi=null; 
    public Integer qi465a=null; 
    public Integer qi465b=null; 
    public Integer qi465c=null; 
    public Integer qi465d=null; 
    public Integer qi465db_a=null; 
    public Integer qi465db_b=null; 
    public Integer qi465db_c=null; 
    public Integer qi465db_d=null; 
    public String qi465db_dx=null; 
    public Integer qi465dd_ar=null; 
    public Integer qi465dd_ac=null; 
    public Integer qi465dd_br=null; 
    public Integer qi465dd_bc=null; 
    public Integer qi465dd_cr=null; 
    public Integer qi465dd_cc=null; 
    public Integer qi465dd_dr=null; 
    public Integer qi465dd_dc=null; 
    public Integer qi465df=null; 
    public String qi465df_o=null; 
    public Integer qi465dh=null; 
    public String qi465dh_o=null; 
    public Integer qi465dj_a=null; 
    public Integer qi465dj_b=null; 
    public Integer qi465dj_c=null; 
    public Integer qi465dj_d=null; 
    public Integer qi465dj_e=null; 
    public Integer qi465dj_f=null; 
    public String  qi465djf_o=null;
    public Integer qi465dj_g=null; 
    public Integer qi465dj_h=null; 
    public Integer qi465dj_i=null; 
    public String  qi465dj_o=null; 
    
    public Integer qi465ea=null; 
    public Integer qi465eb=null;   
    public Integer qi465ec=null;    
    public Integer qi465ed=null; 
    public String  qi465ed_o=null;    
        
    public String  qi465e1cons=null;
    public Integer qi465e1_a1=null;
    public Integer qi465e1_a2=null;
    public Integer qi465e1_a3=null;
    public Integer qi465e1_a4=null;
    public Integer qi465e1_a5=null;
    public Integer qi465e1_a6=null;
    public Integer qi465e1_a7=null;
    public Integer qi465e1_a8=null;
    public Integer qi465e1_a9=null;    
    public Integer qi465e1_b1=null;
    public Integer qi465e1_b2=null;
    public Integer qi465e1_b3=null;
    public Integer qi465e1_b4=null;
    public Integer qi465e1_b5=null;
    public Integer qi465e1_b6=null;
    public Integer qi465e1_b7=null;
    public Integer qi465e1_b8=null;
    public Integer qi465e1_b9=null;
    public Integer qi465e1_c1=null;
    public Integer qi465e1_c2=null;
    public Integer qi465e1_c3=null;
    public Integer qi465e1_c4=null;
    public Integer qi465e1_c5=null;
    public Integer qi465e1_c6=null;
    public Integer qi465e1_c7=null;
    public Integer qi465e1_c8=null;
    public Integer qi465e1_c9=null;
    public Integer qi465e1_d1=null;
    public Integer qi465e1_d2=null;
    public Integer qi465e1_d3=null;
    public Integer qi465e1_d4=null;
    public Integer qi465e1_d5=null;
    public Integer qi465e1_d6=null;
    public Integer qi465e1_d7=null;
    public Integer qi465e1_d8=null;
    public Integer qi465e1_d9=null;
    
    
    public Integer qi465eb_cc=null;    
    public String  qi465eb_cc_o=null;
    
    public Integer qi465ec_cc=null;
    public Integer qi465ed_cc=null; 
    
    public String  qi465ec_o=null;//Observaciones 465ec CARNE
    public Integer qi465ec1_a=null;
    public Integer qi465ec1_b=null;
    public Integer qi465ec1_c=null;
    public Integer qi465ec1_d=null;
    public Integer qi465ec1_e=null;
    public Integer qi465ec1_f=null;
    public Integer qi465ec1_g=null;
    public Integer qi465ec1_h=null;
    public Integer qi465ec1_i=null;
    public String  qi465ec1_i_o=null;
    public Integer qi465ec1_j=null;
    public Integer qi465ec1_k=null;
    public Integer qi465ec1_l=null;
    public Integer qi465ec1_m=null;
    public Integer qi465ec1_n=null;
    public Integer qi465ec1_o=null;
    public Integer qi465ec1_x=null;
    public String qi465ec1_x_o=null;
    public Integer qi465ec1_z=null;
    
    public Integer qi465ed_cc_a=null;    
    public Integer qi465ed_cc_a1=null;
    public Integer qi465ed_cc_a1_d=null;
    public Integer qi465ed_cc_a1_m=null;
    public String  qi465ed_cc_a1_f1=null;
    public String  qi465ed_cc_a1_f2=null;
    
    public Integer    qi465ed_cc_b=null;
    public Integer    qi465ed_cc_b1=null;
    public BigDecimal qi465ed_cc_b1_1=null;
    public BigDecimal qi465ed_cc_b1_2=null;
    
    public Integer qi465ed_cc_c=null;
    public Integer qi465ed_cc_d=null;
    
    public Integer qi465ef_1=null;
    public Integer qi465ef_1_d=null;
    public Integer qi465ef_1_m=null;
    public Integer qi465ef_1_a=null;
    public BigDecimal qi465ef_1_r=null;
    public Integer qi465ef_2=null;
    public Integer qi465ef_2_d=null;
    public Integer qi465ef_2_m=null;
    public Integer qi465ef_2_a=null;
    public BigDecimal qi465ef_2_r=null;
    public Integer qi465ef_3=null;
    public Integer qi465ef_3_d=null;
    public Integer qi465ef_3_m=null;
    public Integer qi465ef_3_a=null;
    public BigDecimal qi465ef_3_r=null;
    public Integer qi465ef_4=null;
    public Integer qi465ef_4_d=null;
    public Integer qi465ef_4_m=null;
    public Integer qi465ef_4_a=null;
    public BigDecimal qi465ef_4_r=null;    
    public Integer qi465ef_7=null;
    public Integer qi465ef_8=null;
    
    public Integer qi466=null; 
    public Integer qi466a_a=null; 
    public Integer qi466a_b=null; 
    public Integer qi466a_c=null; 
    public Integer qi466a_d=null; 
    public Integer qi466a_e=null; 
    public Integer qi466a_x=null; 
    public String qi466a_x_o=null; 
    public Integer qi466b=null; 
    public String qi466b_a=null; 
    public String qi466b_o=null; 
    public Integer qi466c=null; 
    public Integer qi467=null; 
    public String qi467cons=null;
    public Integer qi468=null;
    public String qi468cons=null;
    public Integer qi468a=null; 
    public Integer qi468b=null; 
    public String qi468b_o=null; 
    public Integer qi469a=null; 
    public Integer qi469b=null; 
    public Integer qi469c=null; 
    public String qi469d=null; 
    public Integer qi469d_a=null; 
    public Integer qi469d_b=null; 
    public Integer qi469d_c=null; 
    public Integer qi469d_d=null; 
    public Integer qi469d_e=null; 
    public Integer qi469d_f=null; 
    public Integer qi469d_g=null; 
    public Integer qi469d_h=null; 
    public Integer qi469d_i=null; 
    public Integer qi469d_j=null; 
    public Integer qi469d_k=null; 
    public Integer qi469d_l=null; 
    public Integer qi469d_m=null; 
    public Integer qi469d_n=null; 
    public Integer qi469d_o=null; 
    public Integer qi469d_p=null; 
    public Integer qi469d_x=null; 
    public String qi469d_xo=null; 
    public String qi469f=null; 
    public Integer qi469g=null; 
    public Integer qi470=null; 
    public String qi470_o=null; 
    public Integer qi471=null; 
    public Integer qi471a=null; 
    public Integer qi471b_a=null; 
    public Integer qi471b_b=null; 
    public Integer qi471b_c=null; 
    public Integer qi471b_d=null; 
    public Integer qi471b_e=null; 
    public Integer qi471b_f=null; 
    public Integer qi471b_x=null; 
    public String qi471b_xo=null; 
    public Integer qi471b_z=null; 
    public Integer qi471ca=null; 
    public Integer qi471cb=null; 
    public Integer qi471cc=null; 
    public Integer qi471cd=null; 
    public Integer qi471ce=null; 
    public Integer qi471cf=null; 
    public Integer qi472=null;
    public String qi472cons=null;
    public Integer qi472aa=null; 
    public Integer qi472ab=null; 
    public Integer qi472ac=null; 
    public Integer qi472ad=null; 
    public Integer qi472b=null; 
    public Integer qi472c=null; 
    public Integer qi473=null; 
    public Integer qi473a=null; 
    public Integer qi473ba=null; 
    public Integer qi473bb=null; 
    public Integer qi473bc=null; 
    public Integer qi473c=null; 
    public Integer qi473d_a=null; 
    public Integer qi473d_b=null; 
    public Integer qi473d_c=null; 
    public Integer qi473d_d=null; 
    public Integer qi473d_e=null; 
    public Integer qi473d_f=null; 
    public Integer qi473d_x=null; 
    public String qi473d_xo=null; 
    public Integer qi473f=null; 
    public Integer qi474=null; 
    public String qi474a=null; 
    public Integer qi474a_a=null; 
    public Integer qi474a_b=null; 
    public Integer qi474a_c=null; 
    public Integer qi474a_d=null; 
    public Integer qi474a_e=null; 
    public Integer qi474a_f=null; 
    public Integer qi474a_g=null; 
    public Integer qi474a_h=null; 
    public Integer qi474a_i=null; 
    public Integer qi474a_j=null; 
    public Integer qi474a_k=null; 
    public Integer qi474a_l=null; 
    public Integer qi474a_m=null; 
    public Integer qi474a_n=null; 
    public Integer qi474a_o=null; 
    public Integer qi474a_p=null; 
    public Integer qi474a_x=null; 
    public String qi474a_xo=null; 
    public String qi474c=null; 
    public Integer qi474d=null; 
    public Integer qi475=null; 
    public String qi475_o=null; 
    public Integer qi475b=null; 
    public Integer qi476an1=null; 
    public Integer qi476as1=null; 
    public Integer qi476at1=null; 
    public Integer qi476ar1=null; 
    public Integer qi476an2=null; 
    public Integer qi476as2=null; 
    public Integer qi476at2=null; 
    public Integer qi476ar2=null; 
    public Integer qi476an3=null; 
    public Integer qi476as3=null; 
    public Integer qi476at3=null; 
    public Integer qi476ar3=null; 
    public Integer qi476an4=null; 
    public Integer qi476as4=null; 
    public Integer qi476at4=null; 
    public Integer qi476ar4=null; 
    public Integer qi476an5=null; 
    public Integer qi476as5=null; 
    public Integer qi476at5=null; 
    public Integer qi476ar5=null; 
    public Integer qi476an6=null; 
    public Integer qi476as6=null; 
    public Integer qi476at6=null; 
    public Integer qi476ar6=null; 
    public Integer qi476an7=null; 
    public Integer qi476as7=null; 
    public Integer qi476at7=null; 
    public Integer qi476ar7=null; 
    
    public Integer qi476b_a=null;
    public Integer qi476b_b=null;
    public Integer qi476b_c=null;
    public Integer qi476b_d=null;
    public Integer qi476b_e=null;
    public Integer qi476b_f=null;
    public Integer qi476b_g=null;
    public Integer qi476b_h=null;
    public Integer qi476b_i=null;
    public Integer qi476b_j=null;
    public Integer qi476b_k=null;
    public Integer qi476b_x=null;
    public String  qi476b_x_o=null;
    public Integer qi476b_y=null;
    public Integer qi476b_z=null;
    
    public Integer qi476c=null;
    public Integer qi476c_n=null;
    public Integer qi476c_a=null;
    public Integer qi476c_b=null;
    public Integer qi476c_c=null;
    public Integer qi476c_d=null;
    public Integer qi476c_e=null;
    public Integer qi476c_f=null;
    public Integer qi476c_x=null;
    public String  qi476c_x_o=null;
    public Integer qi476c_z=null;
    
    
    public Integer qi477=null; 
    public String qi477cons=null;
    
    public Integer qi477_1=null;
    public Integer qi477_1_n=null;
    public Integer qi477_2=null;
    public String  qi477_2_o=null;
    public Integer qi477_3=null;
    public Integer qi477_4_a=null;
    public Integer qi477_4_b=null;
    public Integer qi477_4_c=null;
    public Integer qi477_4_d=null;
    public Integer qi477_4_e=null;
    public Integer qi477_4_f=null;
    public Integer qi477_4_g=null;
    public Integer qi477_4_h=null;
    public Integer qi477_4_i=null;
    public Integer qi477_4_j=null;
    public Integer qi477_4_k=null;
    public Integer qi477_4_l=null;
    public Integer qi477_4_x=null;
    public String  qi477_4_x_o=null;
    public Integer qi477_4_y=null;
    public Integer qi477_4_z=null;
    
    public Integer qi477a=null;
    public String qiobs_4b=null;
    
    @FieldEntity(ignoreField=true, saveField=false)
   	public boolean qi465da;
    @FieldEntity(ignoreField=true, saveField=false)
   	public boolean filtro465de;
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtro465dg;
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtro465di;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer filtro469d;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer filtro474a;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qi216;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qi218;
    @FieldEntity(ignoreField=true, saveField=false)
    public boolean filtro215;
    
    public CISECCION_04B() {} 
    
    public Integer getConvertQi457(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi457(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi458(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi458(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi459(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi459(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi460(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi460(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
    
	
	public Integer getConvertQi461(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi461(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi461b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi461b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi461c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi461c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi461e(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi461e(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi462(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi462(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi462b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi462b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi463(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi463(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi463a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi463a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi464(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi464(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi465db_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465db_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	

	public Integer getConvertQi465db_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465db_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465db_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465db_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}

	public Integer getConvertQi465db_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465db_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
		
	public Integer getConvertQi465df(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 7: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465df(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 96: valorReturn=7; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465dh(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 5: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465dh(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 96: valorReturn=5; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465e_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465e_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465e_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465e_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465e_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465e_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465e_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465e_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi465eb(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 5: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465eb(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 96: valorReturn=5; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
//	public Integer getConvertQi465ed_cc(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 4: valorReturn=8; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
//	public Integer setConvertQi465ed_cc(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 8: valorReturn=4; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
	
	
	public Integer getConvertQi465ed_cc(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi465ed_cc(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi466(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi466(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi467(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi467(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi468(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi468(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi468a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi468a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi468b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=6; break;
		case 5: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi468b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=4; break;
		case 8: valorReturn=5; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi469a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi469a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}	
	
	public Integer getConvertQi469b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi469b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}	
	
	public Integer getConvertQi471(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 5: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi471(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=5; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi471a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi471a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi472(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi472a_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472a_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi472a_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472a_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi472a_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472a_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi472a_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472a_d(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi472c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi472c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi473(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi473a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi473b_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473b_a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi473b_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473b_b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi473b_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473b_c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi473c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi473c(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi475b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi475b(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public String getConvertQi469f(String element) {
		String valorReturn="";
		if(element.equals("1")) valorReturn="A"; 
		else if(element.equals("2")) valorReturn="B"; 
		else if(element.equals("3")) valorReturn="C"; 
		else if(element.equals("4")) valorReturn="D"; 
		else if(element.equals("5")) valorReturn="E"; 
		else if(element.equals("6")) valorReturn="F"; 
		else if(element.equals("7")) valorReturn="G"; 
		else if(element.equals("8")) valorReturn="H"; 
		else if(element.equals("9")) valorReturn="I"; 
		else if(element.equals("10")) valorReturn="J"; 
		else if(element.equals("11")) valorReturn="K"; 
		else if(element.equals("12")) valorReturn="L"; 
		else if(element.equals("13")) valorReturn="M"; 
		else if(element.equals("14")) valorReturn="N"; 
		else if(element.equals("15")) valorReturn="O"; 
		else if(element.equals("16")) valorReturn="P"; 
		else if(element.equals("17")) valorReturn="X"; 
		return valorReturn;
	}
	public String setConvertQi469f(String element) {
		String valorReturn="";
		if(element.equals("A")) valorReturn="1";
		else if(element.equals("B")) valorReturn="2";  
		else if(element.equals("C")) valorReturn="3";
		else if(element.equals("D")) valorReturn="4";
		else if(element.equals("E")) valorReturn="5";
		else if(element.equals("F")) valorReturn="6";
		else if(element.equals("G")) valorReturn="7";
		else if(element.equals("H")) valorReturn="8";
		else if(element.equals("I")) valorReturn="9";
		else if(element.equals("J")) valorReturn="10";
		else if(element.equals("K")) valorReturn="11";
		else if(element.equals("L")) valorReturn="12";
		else if(element.equals("M")) valorReturn="13";
		else if(element.equals("N")) valorReturn="14";
		else if(element.equals("O")) valorReturn="15";
		else if(element.equals("P")) valorReturn="16";
		else if(element.equals("X")) valorReturn="17";
		
		return valorReturn;
	}
	
	
	public String getConvertQi474c(String element) {
		String valorReturn="";
		
		if(element.equals("1")) valorReturn="A"; 
		else if(element.equals("2")) valorReturn="B"; 
		else if(element.equals("3")) valorReturn="C"; 
		else if(element.equals("4")) valorReturn="D"; 
		else if(element.equals("5")) valorReturn="E"; 
		else if(element.equals("6")) valorReturn="F"; 
		else if(element.equals("7")) valorReturn="G"; 
		else if(element.equals("8")) valorReturn="H"; 
		else if(element.equals("9")) valorReturn="I"; 
		else if(element.equals("10")) valorReturn="J"; 
		else if(element.equals("11")) valorReturn="K"; 
		else if(element.equals("12")) valorReturn="L"; 
		else if(element.equals("13")) valorReturn="M"; 
		else if(element.equals("14")) valorReturn="N"; 
		else if(element.equals("15")) valorReturn="O"; 
		else if(element.equals("16")) valorReturn="P"; 
		else if(element.equals("17")) valorReturn="X"; 
		
		return valorReturn;
	}
	public String setConvertQi474c(String element) {
		String valorReturn="";
		if(element.equals("A")) valorReturn="1";
		else if(element.equals("B")) valorReturn="2";  
		else if(element.equals("C")) valorReturn="3";
		else if(element.equals("D")) valorReturn="4";
		else if(element.equals("E")) valorReturn="5";
		else if(element.equals("F")) valorReturn="6";
		else if(element.equals("G")) valorReturn="7";
		else if(element.equals("H")) valorReturn="8";
		else if(element.equals("I")) valorReturn="9";
		else if(element.equals("J")) valorReturn="10";
		else if(element.equals("K")) valorReturn="11";
		else if(element.equals("L")) valorReturn="12";
		else if(element.equals("M")) valorReturn="13";
		else if(element.equals("N")) valorReturn="14";
		else if(element.equals("O")) valorReturn="15";
		else if(element.equals("P")) valorReturn="16";
		else if(element.equals("X")) valorReturn="17";
		return valorReturn;
	}
	
} 
