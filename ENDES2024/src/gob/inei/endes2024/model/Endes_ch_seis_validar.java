package gob.inei.endes2024.model;

import java.math.BigDecimal;

import gob.inei.dnce.components.Entity;

public class Endes_ch_seis_validar extends Entity{
		private static final long serialVersionUID = 1L; 
	    public Integer hogar_id=null; 
	    public Integer persona_id=null; 
		public String qh_obs_seccion06 = null;
		public Integer qh224 = null;
		public Integer qh225u = null;
		public Integer qh225 = null;
		public String qh225t = null;
		public Integer qh227 = null;
		public BigDecimal qh227r = null;
		public Integer qh227a = null;
		public Integer qh227b = null;
		public String qh227b_o = null;
}
