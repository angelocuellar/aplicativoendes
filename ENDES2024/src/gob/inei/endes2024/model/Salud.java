package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class Salud extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer qs22d=null;
    public Integer qs22m=null;
    public Integer qs22a=null; 
    public String qs22cons=null; 
    public Integer qs23=null;
    public Integer qs23a=null;
    public String qs23a_n=null;  
    public String qs23a_o=null;
    public Integer qs24=null; 
    public Integer qs25n=null; 
    public Integer qs25a=null; 
    public Integer qs25g=null;
    public Integer qs25_a=null;
    public String qs25_ao=null;
    public Integer qs25_b=null;
    public String qs25_bo=null;
    
    public Integer qs25c1=null;
    public Integer qs25c2=null;
    public Integer qs25c3=null;
    public Integer qs25c4=null;
    public Integer qs25c5=null;
    public Integer qs25c6=null;
    
    public Integer qs26=null; 
    public Integer qs27_a=null; 
    public Integer qs27_b=null; 
    public Integer qs27_c=null; 
    public Integer qs27_d=null; 
    public Integer qs27_e=null; 
    public Integer qs27_x=null; 
    public String qs27_o=null; 
    public Integer qs28=null; 
    public Integer qs29a=null; 
    public Integer qs29b=null; 
    public Integer qs100=null;
    public String qs100fech_ref=null;
    public Integer qs101=null; 
    public String qs101_o=null; 
    public Integer qs102=null; 
    public Integer qs103u=null; 
    public Integer qs103c=null; 
    public Integer qs104=null;
    public String qs104fech_ref=null;
    public Integer qs105=null; 
    public Integer qs106=null; 
    public Integer qs107=null;
    public String qs107fech_ref=null;
    public Integer qs108=null; 
    public String qs108_o=null; 
    public Integer qs109=null; 
    public Integer qs110u=null; 
    public Integer qs110c=null; 
    public Integer qs111=null;
    public String qs111fech_ref=null;
    public Integer qs112=null; 
    public Integer qs113=null; 
    public Integer qs200=null;
    public String qs200fech_ref=null;
    public Integer qs201=null;
    public String qs201fech_ref=null;
    public Integer qs202=null; 
    public Integer qs203=null; 
    public Integer qs203c=null; 
    public Integer qs204u=null; 
    public Integer qs204c=null; 
    public Integer qs205u=null; 
    public Integer qs205c=null; 
    public Integer qs206=null; 
    public Integer qs207u=null; 
    public Integer qs207c=null; 
    public Integer qs208=null;
    public String qs208fech_ref=null;
    public Integer qs209=null; 
    public Integer qs210=null; 
    public String qs210fech_ref=null;
    public Integer qs211u=null; 
    public Integer qs211c=null; 
    public Integer qs212a=null; 
    public Integer qs212a1=null; 
    public Integer qs212a2=null; 
    public Integer qs212b=null; 
    public Integer qs212b1=null; 
    public Integer qs212b2=null; 
    public Integer qs212c=null; 
    public Integer qs212c1=null; 
    public Integer qs212c2=null; 
    public Integer qs212d=null; 
    public Integer qs212d1=null; 
    public Integer qs212d2=null; 
    public Integer qs212e=null; 
    public Integer qs212e1=null; 
    public Integer qs212e2=null; 
    public Integer qs212f=null; 
    public Integer qs212f1=null; 
    public Integer qs212f2=null; 
    public Integer qs212g=null; 
    public Integer qs212g1=null; 
    public Integer qs212g2=null; 
    public Integer qs212x=null; 
    public String qs212_o=null; 
    public Integer qs212x1=null; 
    public Integer qs212x2=null; 
    public Integer qs212z=null; 
    public Integer qs213=null; 
    public Integer qs213a=null;
    public String qs213fech_ref=null;
    public Integer qs214=null; 
    public BigDecimal qs214a=null; 
    public Integer qs215=null; 
    public Integer qs215a=null;
    public String qs215fech_ref=null;
    public Integer qs216=null; 
    public BigDecimal qs216a=null; 
    public Integer qs217=null; 
    public Integer qs217a=null;
    public String qs217fech_ref=null;
    public Integer qs218=null; 
    public BigDecimal qs218a=null; 
    public Integer qs219=null; 
    public Integer qs219a=null;
    public String qs219fech_ref=null;
    public Integer qs220=null; 
    public BigDecimal qs220a=null; 
    public Integer qs301=null; 
    public Integer qs302=null; 
    public Integer qs302a=null; 
    public Integer qs303=null; 
    public String qs303_o=null; 
    public Integer qs304=null; 
    public Integer qs305=null; 
    public Integer qs305a=null; 
    public Integer qs306=null; 
    public Integer qs307=null; 
    public Integer qs308=null; 
    public Integer qs309=null; 
    public Integer qs311=null; 
    public Integer qs312=null; 
    public Integer qs312a=null; 
    public Integer qs313=null; 
    public String qs313_o=null; 
    public String qsobs_seccion3=null;
    
    @FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh06=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh07=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public String qh7dd=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public String qh7mm=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh14=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh15n=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh15y=null;
    @FieldEntity(ignoreField=true, saveField=false)
    public Integer qh15g=null;    
    
    public Salud() {} 
    
	public Integer getConvertqs25n(Integer qs25n) {
		Integer valorReturn=0;
		switch (qs25n){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvertqs25n(Integer qs25n) {
		Integer valorReturn=0;
		switch (qs25n){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 8: valorReturn=7; break;	
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer getConvertqs25a(Integer qs25a) {
		Integer valorReturn=0;
		switch (qs25a){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		case 7: valorReturn=6; break;
		case 8: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvertqs25a(Integer qs25a) {
		Integer valorReturn=0;
		switch (qs25a){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 6: valorReturn=7; break;	
		case 8: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs25g(Integer qs25g) {
		Integer valorReturn=0;
		switch (qs25g){
		case 7: valorReturn=8; break;
		default: valorReturn=qs25g;
		}
		return valorReturn;
	}
	public Integer setConvertqs25g(Integer qs25g) {
		Integer valorReturn=0;
		switch (qs25g){
		case 8: valorReturn=7; break;
		default: valorReturn=qs25g;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs100(Integer qs100) {
		Integer valorReturn=0;
		switch (qs100){
		case 3: valorReturn=8; break;
		default: valorReturn=qs100;
		}
		return valorReturn;
	}
	public Integer setConvertqs100(Integer qs100) {
		Integer valorReturn=0;
		switch (qs100){
		case 8: valorReturn=3; break;	
		default: valorReturn=qs100;
		}
		return valorReturn;
	}
	public Integer getConvertqs101(Integer qs101) {
		Integer valorReturn=0;
		switch (qs101){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=20; break;
		case 8: valorReturn=30; break;
		case 9: valorReturn=31; break;
		case 10: valorReturn=40; break;
		case 11: valorReturn=41; break;
		case 12: valorReturn=42; break;
		case 13: valorReturn=96; break;
		case 14: valorReturn=98; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs101(Integer qs101) {
		Integer valorReturn=0;
		switch (qs101){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 20: valorReturn=7; break;
		case 30: valorReturn=8; break;
		case 31: valorReturn=9; break;
		case 40: valorReturn=10; break;
		case 41: valorReturn=11; break;
		case 42: valorReturn=12; break;
		case 96: valorReturn=13; break;
		case 98: valorReturn=14; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer getConvertqs203(Integer qs203) {
		Integer valorReturn=0;
		switch (qs203){
		case 2: valorReturn=8; break;
		default: valorReturn=qs203;
		}
		return valorReturn;
	}
	public Integer setConvertqs203(Integer qs203) {
		Integer valorReturn=0;
		switch (qs203){
		case 8: valorReturn=2; break;	
		default: valorReturn=qs203;
		}
		return valorReturn;
	}
	public Integer getConvertqs215(Integer qs215) {
		Integer valorReturn=0;
		switch (qs215){
		case 2: valorReturn=3; break;
		case 3: valorReturn=8; break;
		default: valorReturn=qs215;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs215(Integer qs215) {
		Integer valorReturn=0;
		switch (qs215){
		case 3: valorReturn=2; break;	
		case 8: valorReturn=3; break;
		default: valorReturn=qs215;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs303(Integer qs303) {
		Integer valorReturn=0;
		switch (qs303){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=20; break;
		case 8: valorReturn=21; break;
		case 9: valorReturn=30; break;
		case 10: valorReturn=31; break;
		case 11: valorReturn=40; break;
		case 12: valorReturn=41; break;
		case 13: valorReturn=42; break;
		case 14: valorReturn=96; break;
		case 15: valorReturn=98; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs303(Integer qs303) {
		Integer valorReturn=0;
		switch (qs303){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 20: valorReturn=7; break;
		case 21: valorReturn=8; break;
		case 30: valorReturn=9; break;
		case 31: valorReturn=10; break;
		case 40: valorReturn=11; break;
		case 41: valorReturn=12; break;
		case 42: valorReturn=13; break;
		case 96: valorReturn=14; break;
		case 98: valorReturn=15; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs313(Integer qs303) {
		Integer valorReturn=0;
		switch (qs303){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=20; break;
		case 8: valorReturn=30; break;
		case 9: valorReturn=31; break;
		case 10: valorReturn=40; break;
		case 11: valorReturn=41; break;
		case 12: valorReturn=42; break;
		case 13: valorReturn=96; break;
		case 14: valorReturn=98; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs313(Integer qs303) {
		Integer valorReturn=0;
		switch (qs303){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 20: valorReturn=7; break;
		case 30: valorReturn=8; break;
		case 31: valorReturn=9; break;
		case 40: valorReturn=10; break;
		case 41: valorReturn=11; break;
		case 42: valorReturn=12; break;
		case 96: valorReturn=13; break;
		case 98: valorReturn=14; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvertqs25_b(Integer qs25_b){
	Integer data=0;
		switch(qs25_b){
		case 9:data=98;break;
		default:data=qs25_b;
		}
		return data;
	}
	public Integer getConvertqs25_b(Integer qs25_b){
		Integer data=0;
		switch(qs25_b){
		case 98:data=9;break;
		default:data=qs25_b;
		}
		return data;
	}
} 
