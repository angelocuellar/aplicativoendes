package gob.inei.endes2024.model; 
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable; 
import java.math.BigDecimal; 
public class Auditoria extends Entity implements Serializable,IDetailEntityComponent,
Coberturable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id_orden=null;    
    public String pregunta_id=null;
    public Integer orden_id=null;
    public String  pregunta_valor=null;
    public String  fecha_reg=null; 

                 
//    @FieldEntity(ignoreField=true, saveField=false)
//	public String qh02_1=null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public Integer qh07 = null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public String qh7dd=null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public String qh7mm=null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public Integer qh06=null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public Integer estado=null;
//    @FieldEntity(ignoreField=true, saveField=false)
//	public Integer estados5=null;
    
    public Auditoria() {}
    
    public double Redondear(BigDecimal num) {
		Double n=0.0;
		n= num.doubleValue();
		n=Math.rint(n*10)/10;
		return n;
	}
  

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	} 
	
//	public Integer setConvert207(Integer qh207) {
//		Integer valorReturn=0;
//		switch(qh207){
//		case 5: valorReturn=4; break;
//		case 6: valorReturn=5; break;
//		default: valorReturn=qh207;
//		}
//		return valorReturn;
//	}
//	public Integer getConvert207(Integer qh207) {
//		Integer valorReturn = 0;
//		switch (qh207) {
//		case 4: valorReturn = 5; break;	
//		case 5: valorReturn = 6; break;	
//		default:valorReturn = qh207;
//		}
//		return valorReturn;
//	}
//	public Integer setConvert213(Integer qh213) {
//		Integer valorReturn=0;
//		switch(qh213){
//		case 6: valorReturn=4; break;
//		default: valorReturn=qh213;
//		}
//		return valorReturn;
//	}
//	public Integer getConvert213(Integer qh213) {
//		Integer valorReturn = 0;
//		switch (qh213) {
//		case 4: valorReturn = 6; break;	
//		default:valorReturn = qh213;
//		}
//		return valorReturn;
//	}
    
} 
