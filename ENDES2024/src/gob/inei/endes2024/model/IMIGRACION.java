package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

public class IMIGRACION extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;  
    public Integer qh35a = null;
	
    public String qh35b_1 = null;
    public String qh35b_11 = null;
    public String qh35b_2 = null; 
    public String qh35b_3 = null;
			
    public Integer qh35c_a = null;
    public Integer qh35c_m = null;
			
    public String qh35d_ccdd = null;
    public Integer qh35d_ccdd_ns = null;
    public String qh35d_ccpp = null;
    public Integer qh35d_ccpp_ns = null;
    public String qh35d_ccdi = null;
    public Integer qh35d_ccdi_ns = null;		
    public String qh35d_pais = null;
    public Integer qh35d_pais_ns = null;
	
    public Integer qh35e = null;
    public String qh35eo = null;
    public String qh35e_observacion = null;
			
    public Integer qh35f = null;
    public String qh35fo = null;
    public String qh35f_ref = null;

    public Integer qh35g = null;
    
    public String qh35_observacion = null;

	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	
	
	public IMIGRACION() {
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getNombre() {
		return "";//"Hogar N� " + hogar_id + " - " + getJefeHogar();
	}
	public String getNombres() {
		String nombre = "";
		if (qh35b_1 != null) {
			nombre = Util.getText(qh35b_1)+ " " +Util.getText(qh35b_11)+ " " +(Util.getText(qh35b_2)) +" "+ (Util.getText(qh35b_3));
		}
		return nombre;
	}
	


	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer setValue3a8(Integer qh233){
		Integer valorReturn=0;
		switch(qh233){
			case 3: valorReturn=8; break;	
			default: valorReturn=qh233;
		}
		return valorReturn;
	}
	public Integer getValue8a3(Integer qh233){
		Integer valorReturn = 0;
		switch (qh233) {
			case 8: valorReturn = 3; break;
			default:valorReturn = qh233;
		}
		return valorReturn;
	}
	
}
