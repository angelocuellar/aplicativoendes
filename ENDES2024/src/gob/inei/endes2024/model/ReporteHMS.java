package gob.inei.endes2024.model;
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

import java.io.Serializable; 
import java.math.BigDecimal; 

public class ReporteHMS extends Entity implements IDetailEntityComponent,
Coberturable{  	
	public Integer vivienda=null;
	public Integer hogar=null;
	public Integer linea=null;
	public String entrevistadora=null;
	public String seccion=null;
	public String nombre=null;
	public String resultado=null;
	public Integer visitas=null;
	public String peso_talla=null;
	public String hemog=null;
	public Integer pesotalla6=null;
	public Integer hemo6=null;
	public Integer menor6=null;
	public String sal=null;
	public String violencia=null;
	public String presion_arterial=null;
	public Integer salud12=null;
	public Integer menor12=null;
	public Integer cepillo=null;
	public Integer menor1_12=null;
	public String nselv=null;
	public Integer completo=null;
	
	public Integer totales=null;
	public Integer completas=null;
	public Integer incompletas=null;
	
	public String gps=null;
	public boolean gps_mostrar=false;
    
	public String res_viv=null;
	
    public ReporteHMS() {}   
    
    public String getNombre() {
    	return null;
	}
    
    public String getViv_hogar() {
    	if(this.hogar==null){
    		return ""+getVivienda();
    	}else{
	    	if(this.seccion.equals("HOGAR")){
	    		return ""+getVivienda()+"/"+getHogar();
	    	}else{
	    		return null;
	    	}  
    	}
	}
    
    public String getVivienda(){
    	String res="";
    	if(this.vivienda!=null){
    		if(this.vivienda<10){
    			res = "0" + this.vivienda;
    		}else{
    			res = "" + this.vivienda;
    		}
    	}
    	return res;
    }
    
    public String getHogar(){
    	String res="";
    	if(this.hogar!=null){
    		if(this.hogar<10){
    			res = "0" + this.hogar;
    		}else{
    			res = "" + this.hogar;
    		}
    	}
    	return res;
    }
    
    public String getPeso_talla(){
    	if(this.seccion.equals("MEF") || this.seccion.equals("SALUD")){
    		return this.peso_talla;
    	}else{
    		return "- - -";
    	}
    }
    
    public String getHemog(){
    	if(this.seccion.equals("MEF")){
    		return this.hemog ;
    	}else{
    		return "- - -";
    	}
    }
    
    public String getPeso_talla6() {
    	if(this.seccion.equals("HOGAR")){
    		return ""+this.pesotalla6+"/"+this.menor6;
    	}else{
    		return "- - -";
    	}    	
	}
    
    public String getHemo6() {
    	if(this.seccion.equals("HOGAR")){
    		return ""+this.hemo6+"/"+this.menor6;
    	}else{
    		return "- - -";
    	}    	
	}
    
    public String getSal() {
    	if(this.seccion.equals("HOGAR")){
    		return this.sal;
    	}else{
    		return "- - -";
    	}    	
	}
    
    public String getViolencia(){
    	if(this.seccion.equals("MEF")){
    		return this.violencia;
    	}else{
    		return "- - -";
    	}
    }
    
    public String getPresion_arterial(){
    	if(this.seccion.equals("SALUD")){
    		return this.presion_arterial;
    	}else{
    		return "- - -";
    	}
    }
    
    public String getSalud12() {
    	if(this.seccion.equals("SALUD")){
        	return ""+this.salud12+"/"+this.menor12;
    	}else{
    		return "- - -";
    	}
	}
    
    public String getCepillo() {
    	if(this.seccion.equals("SALUD")){
    		return ""+this.cepillo+"/"+this.menor1_12;
    	}else{
    		return "- - -";
    	}    	
	}
    
    public Integer getLinea(){
//    	if(this.seccion.equals("HOGAR")){
//    		return null;
//    	}else{
    		return this.linea;
//    	} 
    }
    
    public String getResultado() {
    	if(this.hogar==null){
    		if(this.res_viv==null){
    			return "SIN APERTURAR";
    		}else{
    			return this.res_viv;
    		}    		
    	}else{
	    	if(this.visitas==null){
	    		return "SIN APERTURAR";
	    	}else{
	    		if(this.resultado==null){
	    			if(this.seccion.equals("MEF")){
	    				if(this.visitas==0)
	    					return "SIN APERTURAR";	
	    				else
	    					return "INCOMPLETO";
	    			}else{
		    			return "INCOMPLETO";
	    			}
	    		}else{
	    			return this.resultado;
	    		}
	    	}
    	}
	}
    
    public void Completado(){
    	completo = null;
    	if(this.seccion.equals("HOGAR")){
//    		if(visitas==null){
//    			completo = null;
//    		}else{
//    			if(this.resultado==null){
//    				completo = null;
//    			}else{
//    				if(resultado.equals("4-Aplazada") || pesotalla6<menor6 || hemo6<menor6 || sal.equals("NO")){
//    					completo = null;
//    				}else{
//    					completo = 0;
//    				}
//    			}
//    		}
    		if(visitas!=null && resultado!=null){
    			if(!(resultado.equals("4-Aplazada") || pesotalla6<menor6 || hemo6<menor6 || sal.equals("NO") || gps.equals("NO"))){
    				completo = 0;
    			}
    		}
    	}
    	if(this.seccion.equals("MEF")){
    		if(visitas!=0 && resultado!=null){
    			if(peso_talla!=null && hemog!=null){
    				completo = 0;
    			}
    		}
    	}
    	if(this.seccion.equals("SALUD")){
    		if(visitas!=null && resultado!=null && peso_talla!=null && presion_arterial!=null){
    			if(!(salud12<menor12 || cepillo<menor1_12)){
    				completo = 0;
    			}
    		}
    	}
    }
    
    public void detalle(String seccion, Integer totales, Integer completas){
    	this.seccion = seccion;
    	this.totales = totales;
    	this.completas = completas;
    	this.incompletas = totales - completas;
    }
    
    public void detallev(Integer aperturadas, Integer sin_aperturar){
    	this.totales = aperturadas + sin_aperturar;
    	this.completas = aperturadas;
    	this.incompletas = sin_aperturar;
    }
    
    public String getGps(){
    	if(gps_mostrar){
    		return this.gps;
    	}else{
    		return null;
    	}
    }   
    
    
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
} 
