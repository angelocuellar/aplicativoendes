package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

import java.math.BigDecimal;
public class Marco extends Entity implements IDetailEntityComponent{ 
	
	private static final long serialVersionUID = 1L;
	public String anio;
	public String mes;
	public String periodo;
	public String conglomerado;
	public String semestre;
	public Integer tselv;
	public String nselv;
	public String nroorden;
	public String odei;
	public String ubigeo;
	public String codccpp;
	public String nomccpp;
	public String zona;
	public String manzana;
	public String aer_ini;
	public String aer_fin;
	public Integer catvia;
	public String via;
	public String puerta;
	public String mz;
	public String lote;
	public String piso;
	public String block;
	public String interior;
	public BigDecimal km;
	public Integer p19;
	public String p21;
	public Integer asignado;
	public String usu_id;
	public String f_recuperacion;
	// nuevas
	public String p18_1;
	public String p18_2;
	public String p18_3;
	
	public Integer viv_reserva;   
	
	
	@FieldEntity(ignoreField=true, saveField=false)
	public String estado_envio;
	@FieldEntity(ignoreField=true, saveField=false)
	public String gps;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer resviv;
	@FieldEntity(ignoreField=true, saveField=false)
	public String departamento;
	@FieldEntity(ignoreField=true, saveField=false)
	public String provincia;
	@FieldEntity(ignoreField=true, saveField=false)
	public String distrito;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado;
	@FieldEntity(ignoreField=true, saveField=false)
	public String usuario_asig;

	
	public Marco() {
	}
	
	public String getTipoVivienda() {
		if (tselv == null) {
			return "Indefinido";
		} else if (!Util.esDiferente(tselv, 1)) {
			return "Urbano";
		} else {
			return "Rural";
		}
	}
	
	public String getEstado() {
		if (estado == null) {
			return "";
		} else if (!Util.esDiferente(estado, 0)) {
			return "Completo";
		} else {
			return "Incompleto";
		}
	}
	
//	public String getResViv() {
//		if (!Util.esDiferente(id_muestra, 1)) {
//			if (!Util.esDiferente(resviv, 1)) {
//				return "1. Completa";
//			} else if (!Util.esDiferente(resviv, 2)) {
//				return "2. Hogar Presente pero Entrevistado competente Ausente";
//			} else if (!Util.esDiferente(resviv, 3)) {
//				return "3. Hogar Ausente";
//			} else if (!Util.esDiferente(resviv, 4)) {
//				return "4. Aplazada";
//			} else if (!Util.esDiferente(resviv, 5)) {
//				return "5. Rechazo vivienda";
//			} else if (!Util.esDiferente(resviv, 6)) {
//				return "6. Vivienda Desocupada o no es vivienda";
//			} else if (!Util.esDiferente(resviv, 7)) {
//				return "7. Vivienda destruida";
//			} else if (!Util.esDiferente(resviv, 8)) {
//				return "8. Vivienda no encontrada";
//			} else if (!Util.esDiferente(resviv, 9)) {
//				return "9. Otra";
//			}
//			
//		}
//		else if (!Util.esDiferente(id_muestra, 2)) {
//			if (!Util.esDiferente(resviv, 1)) {
//				return "1. Completa";
//			} else if (!Util.esDiferente(resviv, 2)) {
//				return "2. Incompleta";
//			} else if (!Util.esDiferente(resviv, 3)) {
//				return "3. Ausente vivienda";
//			} else if (!Util.esDiferente(resviv, 4)) {
//				return "4. Ausente informante";
//			} else if (!Util.esDiferente(resviv, 5)) {
//				return "5. Rechazo vivienda";
//			} else if (!Util.esDiferente(resviv, 6)) {
//				return "6. Rechazo informante";
//			} else if (!Util.esDiferente(resviv, 7)) {
//				return "7. Persona con discapacidad";
//			} else if (!Util.esDiferente(resviv, 8)) {
//				return "8. Vivienda desocupada";
//			} else if (!Util.esDiferente(resviv, 9)) {
//				return "9. No se inici� la entrevista";
//			} else if (!Util.esDiferente(resviv, 10)) {
//				return "10. Otro";
//			}
//		}
//		
//		return "";
//	}
	
//	public String getTipoCuestionario() {
//		if (tselv == null) {
//			return "Indefenido";
//		} else if (!Util.esDiferente(tcuest, 1)) {
//			return "Cuestionario 1";
//		} else {
//			return "Cuestionario 2";
//		}
//	}
	
	
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString() {
//		return "Vivienda [conglomerado=" + conglomerado + ", varnroviv="
//				+ nselv + "]";
		return nselv;
	}
	
//	public String getVivrem() {
//		if (vivrem == null) {
//			return "";
//		}
//		switch (vivrem) {
//		case 0:
//			return "Selec.";
//		case 1:
//			return "Reem.";
//		default:
//			return "";
//		}
//	}
	
	public String getP19() {
		if ("".equals(p19) || p19 == null) {
			return "";
		}		
		if ("1".equals(p19)) {
			return "1. Viv. Particular";
		} else if ("2".equals(p19)) {
			return "2. Viv. y Estab.";
		} else if ("3".equals(p19)) {
			return "3. Estab.";
		} else if ("4".equals(p19)) {
			return "4. Viv. Colect.";
		} else if ("5".equals(p19)) {
			return "5. Otro";
		}  else {
			return "";
		}
	}
	
	public String getDireccion() {
		if (catvia == null) {
			return "";
		}		
		switch (catvia) {
		case 1:
			return "AV. " + Util.getText(via);
		case 2:
			return "JR. " + Util.getText(via);
		case 3:
			return "CALLE " + Util.getText(via);
		case 4:
			return "PJE. " + Util.getText(via);
		case 5:
			return "CARR. " + Util.getText(via);
		default:
			return "" + Util.getText(via);
		}		
	}
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
} 
