package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.endes2024.common.MyUtil;

import java.io.Serializable;

public class CICALENDARIO_COL01_03 extends Entity implements Serializable,IDetailEntityComponent {
	private static final long serialVersionUID = 1L;
	 public Integer hogar_id=null; 
	 public Integer persona_id=null; 
	 public Integer qitramo_id=null;
	 public Integer qiindice =null;
	 public Integer qimes_id = null;
	 public Integer qianio = null;
	 public String qicol1 =null;
	 public String qicol1_o = null;
	 public String qicol2 = null;
	 public String qicol2_o = null;
	 public String qicol3 = null;
	 public String qicol3_o =null;
	 
	@FieldEntity(ignoreField=true, saveField=false)
	public String qicol1superior = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public String qicol4 = null;
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	public String getMes(){
		String mesretorno="";
		switch (qimes_id) {
		case 1:mesretorno=MyUtil.Mes(0);
			break;
		case 2:mesretorno=MyUtil.Mes(1);
		break;
		case 3:mesretorno=MyUtil.Mes(2);
		break;
		case 4:mesretorno=MyUtil.Mes(3);
		break;
		case 5:mesretorno=MyUtil.Mes(4);
		break;
		case 6:mesretorno=MyUtil.Mes(5);
		break;
		case 7:mesretorno=MyUtil.Mes(6);
		break;
		case 8:mesretorno=MyUtil.Mes(7);
		break;
		case 9:mesretorno=MyUtil.Mes(8);
		break;
		case 10:mesretorno=MyUtil.Mes(9);
		break;
		case 11:mesretorno=MyUtil.Mes(10);
		break;
		case 12:mesretorno=MyUtil.Mes(11);
		break;
		}
		return mesretorno;
	}
}
