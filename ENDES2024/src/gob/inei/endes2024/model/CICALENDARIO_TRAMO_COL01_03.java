package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.endes2024.common.MyUtil;

import java.io.Serializable;
import java.util.Calendar;

import android.util.Log;

public class CICALENDARIO_TRAMO_COL01_03 extends Entity implements IDetailEntityComponent, Serializable{
	  private static final long serialVersionUID = 1L; 
	    public Integer hogar_id=null; 
	    public Integer persona_id=null;     
	    public Integer qitramo_id=null;
	    public Integer ninio_id = null;
	    public Integer terminacion_id=null;
	    public Integer embarazo_id=null;
	    public Integer metodo_id=null;
	    public Integer ninguno_id=null;
	    public Integer qimes_ini=null;
	    public Integer qianio_ini =null;
	    public String qir_inicial=null;
	    public String qir_final=null;
	    public Integer qimes_fin=null;
	    public Integer qianio_fin=null;
	    public String qicol1_o=null;
	    public String qicol2 = null;
	    public String qicol2_o = null;
	    public String qicol3 = null;
	    public String qicol3_o = null;
	    public Integer qicantidad=null;
		@Override
		public boolean isTitle() {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public void cleanEntity() {
			// TODO Auto-generated method stub
			
		}
		public String getInicio(){
			
			return MyUtil.MesNombreCorto(qimes_ini-1)+"/"+qianio_ini;
		}
		public String getFin(){
		
			return MyUtil.MesNombreCorto(qimes_fin-1)+"/"+qianio_fin;
		}
}
