package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable;
import java.math.BigDecimal;

public class CISECCION_10_04 extends Entity implements Serializable,
	IDetailEntityComponent{ 
    private static final long serialVersionUID = 1L;
    public Integer hogar_id=null;
    public Integer persona_id=null;
    public Integer ninio_id=null;
    
    public Integer qi_ctrl     = null;
    public String  qi_ctrl_desc= null;
    public String  qi_ctrl_dia = null;
    public String  qi_ctrl_mes = null;
    public Integer qi_ctrl_anio = null;
    public BigDecimal qi_ctrl_peso = null;
    public BigDecimal qi_ctrl_talla = null;
    
    
    public Integer getQi_ctrl() {
		return qi_ctrl;
	}
    
    public String getPeso() {
    	
		String pesoValor = "";
		if (qi_ctrl_peso!=null) {
			String peso = String.valueOf(qi_ctrl_peso);
			double dpart2 = Double.parseDouble(peso);
			String[] parts = String.valueOf(dpart2).split("\\.");
			String part1 = parts[0];
			String part2 = parts[1];
			
			if (part2.length()==1) {
				pesoValor=part1+"."+part2+"0";
			}
			if (part2.length()==2) {
				pesoValor=part1+"."+part2;
			}
		}
	
		return pesoValor;

	}
    
    public String getTalla() {
    	
		String tallaValor = "";
		if (qi_ctrl_talla!=null) {
			String talla = String.valueOf(qi_ctrl_talla);
			double dpart2 = Double.parseDouble(talla);
			String[] parts = String.valueOf(dpart2).split("\\.");
			String part1 = parts[0];
			String part2 = parts[1];
			
			if (part2.length()==1) {
				tallaValor=part1+"."+part2;
			}
			if (part2.length()==2) {
				tallaValor=part1+"."+part2;
			}
		}
		return tallaValor;
	}

	public void setQi_ctrl(Integer qi_ctrl) {
		this.qi_ctrl = qi_ctrl;
	}


	public String getQi_ctrl_dia() {
		return qi_ctrl_dia;
	}


	public void setQi_ctrl_dia(String qi_ctrl_dia) {
		this.qi_ctrl_dia = qi_ctrl_dia;
	}


	public String getQi_ctrl_mes() {
		return qi_ctrl_mes;
	}


	public void setQi_ctrl_mes(String qi_ctrl_mes) {
		this.qi_ctrl_mes = qi_ctrl_mes;
	}


	public Integer getQi_ctrl_anio() {
		return qi_ctrl_anio;
	}


	public void setQi_ctrl_anio(Integer qi_ctrl_anio) {
		this.qi_ctrl_anio = qi_ctrl_anio;
	}


	public BigDecimal getQi_ctrl_peso() {
		return qi_ctrl_peso;
	}


	public void setQi_ctrl_peso(BigDecimal qi_ctrl_peso) {
		this.qi_ctrl_peso = qi_ctrl_peso;
	}


	public BigDecimal getQi_ctrl_talla() {
		return qi_ctrl_talla;
	}


	public void setQi_ctrl_talla(BigDecimal qi_ctrl_talla) {
		this.qi_ctrl_talla = qi_ctrl_talla;
	}


	public CISECCION_10_04() {}


	public String getQi_ctrl_desc() {
		return qi_ctrl_desc=qi_ctrl_desc.equals("0")?qi_ctrl_desc+qi_ctrl+"":qi_ctrl_desc;
	}

	public void setQi_ctrl_desc(String qi_ctrl_desc) {
		this.qi_ctrl_desc = qi_ctrl_desc;
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	} 
}