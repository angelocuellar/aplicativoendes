package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.util.Util;

import java.io.Serializable;

public class Parameter extends Entity implements Serializable {
	private static final long serialVersionUID = 1L;
	public String parameter = null;
	public String value1 = null;
	public String value2 = null;
	public String value3 = null;
	
	@Override
	public String toString() {		
		return "Parameter "+id+": [" + Util.getText(value1) + "]"+" [" + Util.getText(value2) + "]"+" [" + Util.getText(value3) + "]";
	}
	
}
