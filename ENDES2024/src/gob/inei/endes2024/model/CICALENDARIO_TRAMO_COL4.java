package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.endes2024.common.MyUtil;

import java.io.Serializable;

public class CICALENDARIO_TRAMO_COL4 extends Entity implements Serializable,IDetailEntityComponent{
	private static final long serialVersionUID = 1L;
	public Integer hogar_id=null; 
	public Integer persona_id=null;
	public Integer qitramo_id=null;
	public Integer qimes_ini=null;
	public Integer qianio_ini=null;
	public String  qiresult=null;
	public Integer qimes_fin=null;
	public Integer qianio_fin=null;
	public Integer qicantidad = null;
	public Integer conviviente_id=null;
	
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	} 
	public String getfechaInicio(){
		return MyUtil.MesNombreCorto(qimes_ini-1)+" / "+qianio_ini;
	}
	public String getFechaFinal(){
		return MyUtil.MesNombreCorto(qimes_fin-1)+" / "+qianio_fin;
	}
}
