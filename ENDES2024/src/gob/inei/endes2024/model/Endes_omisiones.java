package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

public class Endes_omisiones extends Entity implements IDetailEntityComponent{
	private static final long serialVersionUID = 1L; 
	public String conglomerado=null;
	public String vivienda=null;
	public String cuestionario=null;
	public String seccion=null;
	public String hogar=null;
	public String entrevistadora=null;
	public String resultado=null;
	public String persona=null; 
	public String resultado1=null;
	public String resultado2=null;
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	public String getResultado()
	{String resultados="";
	
	int op = resultado!=null?Util.getInt(resultado):-1;
	switch (op) {
	case 1:resultados= "Completa";break;
	case 2:resultados="Hogar Presente";break;
	case 3:resultados="hogar Ausente";break;
	case 4:resultados="Aplazada";break;
	case 5:resultados="rechazada";break;
	case 6:resultados="Vivienda Desocupada";break;
	case 7:resultados="Vivienda Destruida";break;
	case 8:resultados="Vivienda No Encontrada";break;
	case 9:resultados="Otro";break;
	default:resultados="";break;
		}
	return resultados;
	}
}
