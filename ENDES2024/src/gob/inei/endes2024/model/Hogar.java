package gob.inei.endes2024.model;

import java.math.BigDecimal;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.EndesCalendario;

public class Hogar extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	public Integer qhbebe =null;
	public Integer qhnofam=null;
	public Integer qhtempo=null;
	public String p12_1 = null;
	public String p12_2 = null;
	public String p12_3 = null;
	public String r_final_dia = null;
	public String r_final_mes = null;
	public Integer r_final_anio = null;
	public Integer n_equipo = null;
	public Integer cod_entrevistadora = null;
	public Integer r_final = null;
	public Integer qhtot_vis = null;
	public Integer qhtot_per_hog = null;
	public Integer nro_muj_15_49 = null;
	public Integer tot_men_12 = null;
	public Integer tot_men_6 = null;
	public Integer tot_men_5 = null;
	public Integer tot_men_3 = null;
//	public Integer nro_info_h = null;
	public Integer nro_info_s = null;
	public String ini_hora = null;
	public String ini_min = null;
	public Integer qh27=null;
	public Integer qh28=null;
	public String qhm_obs=null;
	public Integer qh40 = null;
	public String qh40_o = null;
	public Integer qh41 = null;
	public String qh41_o = null;
	public Integer qh42 = null;
	public String qh45 = null;
	public Integer qh46 = null;
	public String qh46_o = null;
	public Integer qh47 = null;
	public String qh47_o = null;
	public Integer qh48 = null;
	public Integer qh49 = null;
	public String qh49_o = null;
	public Integer qh50 = null;
	public Integer qh53 = null;
	public String qh53_o = null;
	public Integer qh54 = null;
	public Integer qh61a = null;
	public Integer qh61b = null;
	public Integer qh61c = null;
	public Integer qh61d = null;
	public Integer qh61e = null;
	public Integer qh61f = null;
	public Integer qh61g = null;
	public Integer qh61h = null;
	public Integer qh61i = null;
	public Integer qh61j = null;
	public Integer qh61k = null;
	public Integer qh61l = null;
	public Integer qh61m = null;
	public Integer qh61n = null;
	public Integer qh61o = null;
	public Integer qh61p = null;
	
	public Integer qh61q = null;
	public Integer qh61q_a = null;
	public Integer qh61q_b = null;
	public Integer qh61q_c = null;
	public Integer qh61q_d = null;
	public Integer qh61q_x = null;
	public String qh61q_xo = null;
		
	public Integer qh61r = null;
	public Integer qh61r_a = null;
	public Integer qh61r_b = null;
	public Integer qh61r_c = null;
	public Integer qh61r_d = null;
	public Integer qh61r_x = null;
	public String qh61r_xo = null;
	
	public Integer qh61s = null;
	public Integer qh61t = null;
	public Integer qh62 = null;
	public String qh62_o = null;
	public Integer qh63 = null;
	public Integer qh64 = null;
	public String qh64_o = null;
	public Integer qh66 = null;
	public Integer qh68 = null;
	public Integer qh70 = null;
	public String qh70_o = null;
	public Integer qh71 = null;
	public Integer qh72 = null;

	public Integer qh73 = null;
	public String qh73_o = null;
	public Integer qh74 = null;
	public String qh74_o = null;
	public Integer qh75 = null;
	public String qh75_o = null;
	public Integer qh76a = null;
	public Integer qh76b = null;
	public Integer qh76c = null;
	public Integer qh76d = null;
	public Integer qh76e = null;

	public Integer qh77a = null;
	public Integer qh77b = null;
	public Integer qh77c = null;
	public Integer qh77d = null;
	public Integer qh77e = null;
	public Integer qh77f = null;
	public String qh77_o = null;
	public Integer qh78 = null;
	public Integer qh79 = null;
	public String qh79_o = null;
	public BigDecimal qh79_1 = null;
	public Integer qh80a = null;
	public Integer qh80an = null;
	public Integer qh80b = null;
	public Integer qh80bn = null;
	public Integer qh80c = null;
	public Integer qh80cn = null;
	public Integer qh80d = null;
	public Integer qh80dn = null;
	public Integer qh80e = null;
	public Integer qh80en = null;
	public Integer qh80f = null;
	public Integer qh80fn = null;
	public Integer qh80g = null;
	public Integer qh80gn = null;
	public Integer qh80h = null;
	public Integer qh80hn = null;
	public Integer qh80i = null;
	public String qh80i_o = null;
	public Integer qh80in = null;
	public Integer qh91 = null;

	public Integer qh93 = null;
	public Integer qh95 = null;
	public Integer qh99 = null;
	public Integer qh101 = null;
	public Integer qh103 = null;
	public Integer qh106 = null;
	public String qh110h = null;
	public String qh110m = null;
	public String qh215 = null;
	public String qh215_c = null;
	public String qh216 = null;
	public String qh216_c = null;
	public String qh_obs_seccion02 = null;
	public String qh_obs_seccion04 = null;
	public String qh_obs_seccion05 = null;
	public String qh_obs_seccion06 = null;
	public Integer qh224 = null;
	public Integer qh225u = null;
	public Integer qh225 = null;
	public String qh225t = null;
	public Integer qh227 = null;
	public BigDecimal qh227r = null;
	public BigDecimal qh227r2 = null;
	public Integer qhviolen = null;
	public Integer qh227a = null;
	public Integer qh227b = null;
	public String qh227b_o = null;
	public Integer qh100b=null;           
	public Integer persona_informante_id=null;
	public String qh_obs_seccion01=null;
	public Integer qhrec_06=null;
	public String qhfrc_fecha06=null;
	
	public String qh228_h=null;
	public String qh228_m=null;
	public String qh238_h=null;
	public String qh238_m=null;
	public Integer qh229_a=null;
	public Integer qh229_b=null;
	public Integer qh229_c=null;
	public Integer qh229_d=null;
	public Integer qh229_x=null;
	public String qh229_x_o=null;
	public Integer qh229_y=null;
	public Integer qh230_a=null;
	public Integer qh230_b=null;
	public Integer qh230_c=null;
	public Integer qh230_d=null;
	public Integer qh230_e=null;
	public Integer qh230_f=null;
	public Integer qh230_x=null;
	public String qh230_x_o=null;
	public Integer qh230_y=null;
	public Integer qh231_a=null;
	public Integer qh231_b=null;
	public Integer qh231_c=null;
	public Integer qh231_d=null;
	public Integer qh231_e=null;
	public Integer qh231_x=null;
	public String qh231_x_o=null;
	public Integer qh231_y=null;
	public Integer qh232_a=null; 
	public Integer qh232_b=null;
	public Integer qh232_c=null;
	public Integer qh232_d=null;
	public Integer qh232_e=null;
	public Integer qh232_f=null;
	public Integer qh232_g=null;
	public Integer qh232_h=null;
	public Integer qh232_i=null;
	public Integer qh232_j=null;
	public Integer qh232_k=null;
	public Integer qh232_l=null;
	public Integer qh232_m=null;
	public Integer qh232_n=null;
	public Integer qh232_o=null;
	public Integer qh232_p=null;
	public Integer qh232_x=null; 
	public String qh232_x_o=null;
	public Integer qh232_y=null;
	public Integer qh233=null;
	public Integer qh234=null;
	public Integer qh234_n=null;
	public Integer qh235=null;
	public Integer qh235_n=null;
	
	public Integer qh235a=null;
	public Integer qh235a_n=null;
	
	
	public Integer qh236_a=null;
	public Integer qh236_b=null;
	public Integer qh236_c=null;
	public Integer qh236_d=null;
	public Integer qh236_e=null;
	public Integer qh236_f=null;
	public Integer qh236_g=null;
	public Integer qh236_h=null;
	public Integer qh236_i=null;
	public Integer qh236_j=null;
	public Integer qh236_k=null;
	public Integer qh236_l=null;
	public Integer qh236_m=null;
	public Integer qh236_x=null;
	public String qh236_x_o=null;
	public Integer qh236_y=null;
	
	public Integer qh228_1=null;

	public Integer qh228_2=null;
	public Integer qh228_3=null;
	public Integer qh228_4=null;
	public Integer qh228_5=null;
	public Integer qh228_6=null;
	public Integer qh228_7=null;
	public Integer qh228_8=null;
	public Integer qh228_9=null;
	public Integer qh228_10=null;	
	
	public String qh223a=null;
    public String qh225a=null;
    public String qh226a=null;
    public String qh227c=null;
    public String qh200_ref_s6=null;
    
    public Integer qh34 = null;
    public Integer qh34_n = null;
    public String qh34_obs = null;
    public Integer qh35 = null;
    public Integer qh35_n = null;
    public String qh35_obs = null;
    
    	
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_1 = null;
//	@FieldEntity(ignoreField=true, saveField=false)
//	public String qh02_11 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_2 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public String qh02_3 = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public boolean existeseccion04=false;
	public Hogar() {
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getNombre() {
		return "Hogar N� " + hogar_id + " - " + getJefeHogar();
	}
	
	public String getJefeHogar() {
		if (qh02_1 != null) {
			return Util.getText(qh02_1) + " " + Util.getText(qh02_2) + " "
					+ Util.getText(qh02_3);
		} else {
			return  "";
		}
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer getConvertqh40(Integer qh40) {
		Integer valorReturn = 0;
		switch (qh40) {
		case 1:	valorReturn = 11; break;
		case 2: valorReturn = 12; break;
		case 3: valorReturn = 13; break;
		case 4: valorReturn = 21; break;
		case 5: valorReturn = 22; break;
		case 6: valorReturn = 31; break;
		case 7: valorReturn = 32; break;
		case 8: valorReturn = 41; break;
		case 9: valorReturn = 51; break;
		case 10:valorReturn = 91; break;
		case 11:valorReturn = 96; break;
		default:valorReturn = -1;
		}
		return valorReturn;
	}
	public Integer setConvertqh40(Integer qh40) {
		Integer valorReturn = 0;
		switch (qh40) {
		case 11: valorReturn = 1; break;
		case 12: valorReturn = 2; break;
		case 13: valorReturn = 3; break;
		case 21: valorReturn = 4; break;
		case 22: valorReturn = 5; break;
		case 31: valorReturn = 6; break;
		case 32: valorReturn = 7; break;
		case 41: valorReturn = 8; break;
		case 51: valorReturn = 9; break;
		case 91: valorReturn = 10; break;
		case 96: valorReturn = 11; break;
		default: valorReturn = -1;
		}
		return valorReturn;
	}

	public Integer getConvertqh41(Integer qh41) {
		Integer valorReturn = 0;
		switch (qh41) {
		case 1: valorReturn = 11; break;
		case 2: valorReturn = 12; break;
		case 3: valorReturn = 13; break;
		case 4: valorReturn = 21; break;
		case 5: valorReturn = 22; break;
		case 6: valorReturn = 31; break;
		case 7: valorReturn = 32; break;
		case 8: valorReturn = 41; break;
		case 9: valorReturn = 51; break;
		case 10: valorReturn = 96; break;
		default: valorReturn = -1;
		}
		return valorReturn;
	}
	public Integer setConvertqh41(Integer qh41) {
		Integer valorReturn = 0;
		switch (qh41) {
		case 11:valorReturn = 1; break;
		case 12:valorReturn = 2; break;
		case 13:valorReturn = 3; break;
		case 21:valorReturn = 4; break;
		case 22:valorReturn = 5; break;
		case 31:valorReturn = 6; break;
		case 32:valorReturn = 7; break;
		case 41:valorReturn = 8; break;
		case 51:valorReturn = 9; break;
		case 96:valorReturn = 10;break;
		default:valorReturn = -1;
		}
		return valorReturn;

	}
	//QH228
	public Integer getConver228_1(Integer qh228_1) {
		Integer valorReturn=0;
		switch (qh228_1) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_1;
		}
		return valorReturn;
	}
	public Integer setConver228_1(Integer qh228_1) {
		Integer valorReturn=0;
		switch (qh228_1) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_1;
		}
		return valorReturn;
	}
	public Integer getConver228_2(Integer qh228_2) {
		Integer valorReturn=0;
		switch (qh228_2) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_2;
		}
		return valorReturn;
	}
	public Integer setConver228_2(Integer qh228_2) {
		Integer valorReturn=0;
		switch (qh228_2) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_2;
		}
		return valorReturn;
	}
	public Integer getConver228_3(Integer qh228_3) {
		Integer valorReturn=0;
		switch (qh228_3) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_3;
		}
		return valorReturn;
	}
	public Integer setConver228_3(Integer qh228_3) {
		Integer valorReturn=0;
		switch (qh228_3) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_3;
		}
		return valorReturn;
	}
	public Integer getConver228_4(Integer qh228_4) {
		Integer valorReturn=0;
		switch (qh228_4) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_4;
		}
		return valorReturn;
	}
	public Integer setConver228_4(Integer qh228_4) {
		Integer valorReturn=0;
		switch (qh228_4) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_4;
		}
		return valorReturn;
	}
	public Integer getConver228_5(Integer qh228_5) {
		Integer valorReturn=0;
		switch (qh228_5) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_5;
		}
		return valorReturn;
	}
	public Integer setConver228_5(Integer qh228_5) {
		Integer valorReturn=0;
		switch (qh228_5) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_5;
		}
		return valorReturn;
	}
	public Integer getConver228_6(Integer qh228_6) {
		Integer valorReturn=0;
		switch (qh228_6) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_6;
		}
		return valorReturn;
	}
	public Integer setConver228_6(Integer qh228_6) {
		Integer valorReturn=0;
		switch (qh228_6) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_6;
		}
		return valorReturn;
	}
	public Integer getConver228_7(Integer qh228_7) {
		Integer valorReturn=0;
		switch (qh228_7) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_7;
		}
		return valorReturn;
	}
	public Integer setConver228_7(Integer qh228_7) {
		Integer valorReturn=0;
		switch (qh228_7) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_7;
		}
		return valorReturn;
	}
	public Integer getConver228_8(Integer qh228_8) {
		Integer valorReturn=0;
		switch (qh228_8) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_8;
		}
		return valorReturn;
	}
	public Integer setConver228_8(Integer qh228_8) {
		Integer valorReturn=0;
		switch (qh228_8) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_8;
		}
		return valorReturn;
	}
	public Integer getConver228_9(Integer qh228_9) {
		Integer valorReturn=0;
		switch (qh228_9) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_9;
		}
		return valorReturn;
	}
	public Integer setConver228_9(Integer qh228_9) {
		Integer valorReturn=0;
		switch (qh228_9) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_9;
		}
		return valorReturn;
	}
	public Integer getConver228_10(Integer qh228_10) {
		Integer valorReturn=0;
		switch (qh228_10) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh228_10;
		}
		return valorReturn;
	}
	public Integer setConver228_10(Integer qh228_10) {
		Integer valorReturn=0;
		switch (qh228_10) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh228_10;
		}
		return valorReturn;
	}
	
	
	//QH228_fin
	
	
	
	

	public Integer getConvertqh46( Integer qh46) {
		Integer valorReturn=0;
		switch (qh46){
		case 5: valorReturn=96; break;
		default:valorReturn=qh46;
		}	
		return valorReturn;
	}
	public Integer setConvertqh46( Integer qh46) {
		Integer valorReturn=0;
		switch (qh46){
		case 96: valorReturn=5; break;
		default:valorReturn=qh46;
		}	
		return valorReturn;
	}
	
	public Integer getConvertqh47(Integer qh47) {
		Integer valorReturn=0;
		switch (qh47){
		case 8: valorReturn=91; break;
		case 9: valorReturn=96; break;
		default: valorReturn=qh47;
		}
		return valorReturn;
	}
	public Integer setConvertqh47(Integer qh47) {
		Integer valorReturn=0;
		switch (qh47){
		case 91: valorReturn=8; break;
		case 96: valorReturn=9; break;
		default: valorReturn=qh47;
		}
		return valorReturn;
	}
	
	public Integer getConvertqh49(Integer qh49) {
		Integer valorReturn=0;
		switch (qh49){
		case 5: valorReturn=96; break;
		default: valorReturn=qh49;
		}
		return valorReturn;
	}
	public Integer setConvertqh49(Integer qh49) {
		Integer valorReturn=0;
		switch (qh49){
		case 96: valorReturn=5; break;
		default: valorReturn=qh49;
		}
		return valorReturn;
	}
	
	public Integer getConvertqh53(Integer qh53) {
		Integer valorReturn=0;
		switch (qh53) {
		case 1: valorReturn=11; break;
		case 2: valorReturn=12; break;
		case 3: valorReturn=21; break;
		case 4: valorReturn=31; break;
		case 5: valorReturn=32; break;
		case 6: valorReturn=33; break;
		case 7: valorReturn=34; break;
		case 8: valorReturn=35; break;
		case 9: valorReturn=41; break;
		case 10: valorReturn=51; break;
		case 11: valorReturn=96; break;
		default: valorReturn= -1;
		}
		return valorReturn;
	}
	public Integer setConvertqh53(Integer qh53) {
		Integer valorReturn=0;
		switch (qh53) {
		case 11: valorReturn=1; break;
		case 12: valorReturn=2; break;
		case 21: valorReturn=3; break;
		case 31: valorReturn=4; break;
		case 32: valorReturn=5; break;
		case 33: valorReturn=6; break;
		case 34: valorReturn=7; break;
		case 35: valorReturn=8; break;
		case 41: valorReturn=9; break;
		case 51: valorReturn=10; break;
		case 96: valorReturn=11; break;
		default: valorReturn= -1;
		}
		return valorReturn;
	}
	
	public Integer getConvert62(Integer qh62) {
		Integer valorReturn=0;
		switch (qh62){
		case 11: valorReturn=95; break;
		case 12: valorReturn=96; break;
		default: valorReturn=qh62;
		}
		return valorReturn;
	}
	public Integer setConvert62(Integer qh62) {
		Integer valorReturn=0;
		switch (qh62) {
		case 95: valorReturn=11; break;
		case 96: valorReturn=12; break;
		default: valorReturn=qh62;		
		}
		return valorReturn;
		}
	
	public Integer getConvert64(Integer qh64) {
		Integer valorReturn=0;
		switch (qh64) {
		case 11: valorReturn=96; break;		
		default: valorReturn=qh64;
		}
		return valorReturn;
	}
	public Integer setConvert64(Integer qh64) {
		Integer valorReturn=0;
		switch(qh64){
		case 96: valorReturn=11; break;
		default: valorReturn=qh64;
		}
		return valorReturn;
	}
	
	public Integer getConvert70(Integer qh70) {
		Integer valorReturn=0;
		switch(qh70){
		case 7: valorReturn=96; break;
		default: valorReturn=qh70;
		}
		return valorReturn;			
	}
	public Integer setConvert70(Integer qh70) {
		Integer valorReturn=0;
		switch(qh70){
		case 96: valorReturn=7; break;
		default: valorReturn=qh70;
		}
		return valorReturn;
	}
	
	public Integer getConvert73(Integer qh73) {
		Integer valorReturn=0;
		switch(qh73){
		case 1: valorReturn=11; break;
		case 2: valorReturn=12; break;
		case 3: valorReturn=13; break;
		case 4: valorReturn=14; break;
		case 5: valorReturn=21; break;
		case 6: valorReturn=22; break;
		case 7: valorReturn=31; break;
		case 8: valorReturn=96; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvert73(Integer qh73) {
		Integer valorReturn=0;
		switch(qh73){
		case 11: valorReturn=1; break;
		case 12: valorReturn=2; break;
		case 13: valorReturn=3; break;
		case 14: valorReturn=4; break;
		case 21: valorReturn=5; break;
		case 22: valorReturn=6; break;
		case 31: valorReturn=7; break;
		case 96: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvert74(Integer qh74) {
		Integer valorReturn=0;
		switch(qh74){
		case 1: valorReturn=11; break;
		case 2: valorReturn=12; break;
		case 3: valorReturn=13; break;
		case 4: valorReturn=21; break;
		case 5: valorReturn=22; break;
		case 6: valorReturn=23; break;
		case 7: valorReturn=24; break;
		case 8: valorReturn=31; break;
		case 9: valorReturn=32; break;
		case 10: valorReturn=33; break;
		case 11: valorReturn=34; break;
		case 12: valorReturn=35; break;
		case 13: valorReturn=96;break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvert74(Integer qh74) {
		Integer valorReturn=0;
		switch(qh74){
		case 11: valorReturn=1; break;
		case 12: valorReturn=2; break;
		case 13: valorReturn=3; break;
		case 21: valorReturn=4; break;
		case 22: valorReturn=5; break;
		case 23: valorReturn=6; break;
		case 24: valorReturn=7; break;
		case 31: valorReturn=8; break;
		case 32: valorReturn=9; break;
		case 33: valorReturn=10; break;
		case 34: valorReturn=11; break;
		case 35: valorReturn=12; break;
		case 96: valorReturn=13;break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvert75(Integer qh75) {
		Integer valorReturn=0;
		switch(qh75){
		case 1: valorReturn=11; break;
		case 2: valorReturn=12; break;
		case 3: valorReturn=21; break;
		case 4: valorReturn=22; break;
		case 5: valorReturn=23; break;
		case 6: valorReturn=31; break;
		case 7: valorReturn=32; break;
		case 8: valorReturn=33; break;
		case 9: valorReturn=34; break;
		case 10: valorReturn=96; break;	
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvert75(Integer qh75) {
		Integer valorReturn=0;
		switch(qh75){
		case 11: valorReturn=1; break;
		case 12: valorReturn=2; break;
		case 21: valorReturn=3; break;
		case 22: valorReturn=4; break;
		case 23: valorReturn=5; break;
		case 31: valorReturn=6; break;
		case 32: valorReturn=7; break;
		case 33: valorReturn=8; break;
		case 34: valorReturn=9; break;
		case 96: valorReturn=10; break;	
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConver79(Integer qh79) {
		Integer valorReturn=0;
		switch (qh79) {
		case 2: valorReturn=6; break;
		case 3: valorReturn=8; break;
		default: valorReturn=qh79;
		}
		return valorReturn;
	}
	public Integer setConver79(Integer qh79) {
		Integer valorReturn=0;
		switch (qh79) {
		case 6: valorReturn=2; break;
		case 8: valorReturn=3; break;
		default: valorReturn=qh79;
		}
		return valorReturn;
	}
	
	public Integer getConver80a(Integer qh80a) {
		Integer valorReturn=0;
		switch (qh80a) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80a;
		}
		return valorReturn;
	}
	public Integer setConver80a(Integer qh80a) {
		Integer valorReturn=0;
		switch (qh80a) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80a;
		}
		return valorReturn;
	}
	
	public Integer getConver80b(Integer qh80b) {
		Integer valorReturn=0;
		switch (qh80b) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80b;
		}
		return valorReturn;
	}
	public Integer setConver80b(Integer qh80b) {
		Integer valorReturn=0;
		switch (qh80b) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80b;
		}
		return valorReturn;
	}
	
	public Integer getConver80c(Integer qh80c) {
		Integer valorReturn=0;
		switch (qh80c) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80c;
		}
		return valorReturn;
	}	
	public Integer setConver80c(Integer qh80c) {
		Integer valorReturn=0;
		switch (qh80c) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80c;
		}
		return valorReturn;
	}
	
	public Integer getConver80d(Integer qh80d) {
		Integer valorReturn=0;
		switch (qh80d) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80d;
		}
		return valorReturn;
	}	
	public Integer setConver80d(Integer qh80d) {
		Integer valorReturn=0;
		switch (qh80d) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80d;
		}
		return valorReturn;
	}
	
	public Integer getConver80e(Integer qh80e) {
		Integer valorReturn=0;
		switch (qh80e) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80e;
		}
		return valorReturn;
	}
	public Integer setConver80e(Integer qh80e) {
		Integer valorReturn=0;
		switch (qh80e) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80e;
		}
		return valorReturn;
	}
	
	public Integer getConver80f(Integer qh80f) {
		Integer valorReturn=0;
		switch (qh80f) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80f;
		}
		return valorReturn;
	}
	public Integer setConver80f(Integer qh80f) {
		Integer valorReturn=0;
		switch (qh80f) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80f;
		}
		return valorReturn;
	}
	
	public Integer getConver80g(Integer qh80g) {
		Integer valorReturn=0;
		switch (qh80g) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80g;
		}
		return valorReturn;
	}
	public Integer setConver80g(Integer qh80g) {
		Integer valorReturn=0;
		switch (qh80g) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80g;
		}
		return valorReturn;
	}
	
	public Integer getConver80h(Integer qh80h) {
		Integer valorReturn=0;
		switch (qh80h) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80h;
		}
		return valorReturn;
	}
	public Integer setConver80h(Integer qh80h) {
		Integer valorReturn=0;
		switch (qh80h) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80h;
		}
		return valorReturn;
	}
	public Integer getConver80i(Integer qh80i) {
		Integer valorReturn=0;
		switch (qh80i) {
		case 3: valorReturn=8; break;
		default: valorReturn=qh80i;
		}
		return valorReturn;
	}
	public Integer setConver80i(Integer qh80i) {
		Integer valorReturn=0;
		switch (qh80i) {
		case 8: valorReturn=3; break;
		default: valorReturn=qh80i;
		}
		return valorReturn;
	}
	
	
	
	public Integer setConvert224(Integer qh224) {
			Integer valorReturn=0;
			switch(qh224){
			case 9: valorReturn=6; break;	
			default: valorReturn=qh224;
			}
			return valorReturn;
	}

	public Integer getConvert224(Integer qh224) {
			Integer valorReturn = 0;
			switch (qh224) {
			case 6: valorReturn = 9; break;		
			default:valorReturn = qh224;
			}
			return valorReturn;
	}
	public Integer setConvert225u(Integer qh225u) {
		Integer valorReturn=0;
		switch(qh225u){
		case 998: valorReturn=3; break;	
		default: valorReturn=qh225u;
		}
		return valorReturn;
}
	public Integer getConvert225u(Integer qh225u) {
		Integer valorReturn = 0;
		switch (qh225u) {
		case 3: valorReturn = 998; break;		
		default:valorReturn = qh225u;
		}
		return valorReturn;
}
	
	public Integer setConvert227(Integer qh227) {
		Integer valorReturn=0;
		switch(qh227){
		case 9: valorReturn=7; break;	
		default: valorReturn=qh227;
		}
		return valorReturn;
	}
	
	public Integer getConvert227(Integer qh227) {
		Integer valorReturn = 0;
		switch (qh227) {
		case 7: valorReturn = 9; break;
		default:valorReturn = qh227;
		}
		return valorReturn;
	}
	public Integer setValue3a8(Integer qh233){
		Integer valorReturn=0;
		switch(qh233){
			case 3: valorReturn=8; break;	
			default: valorReturn=qh233;
		}
		return valorReturn;
	}
	public Integer getValue8a3(Integer qh233){
		Integer valorReturn = 0;
		switch (qh233) {
			case 8: valorReturn = 3; break;
			default:valorReturn = qh233;
		}
		return valorReturn;
	}
	
}
