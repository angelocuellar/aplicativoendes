package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

public class MORTALIDAD extends Entity implements IDetailEntityComponent,Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	public Integer persona_id=null;
	public Integer nro_orden_id=null;
	public String qh29m=null;
	public Integer qh30m=null;
	public Integer qh31m=null;
	public Integer qh32m_m=null;
	public Integer qh32m_y= null;
	public Integer qh33m=null;
	public String qh33m_o=null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return "Mortalidad";
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	public String getEdad(){
		String edad="";
		if(qh30m!=null){
			if(qh31m.toString().length()==1){
				edad="0"+qh31m;
			}
			else{
				edad=qh31m+"";
			}
		}
		return edad;
	}
	public String getSexo(){
		String sexo="";
		if(qh30m!=null){
			switch(qh30m){
				case 1: sexo="Hombre"; break;
				case 2: sexo="Mujer"; break;
			}
		}
		return sexo;
	}
	public String getFecha(){
		String fecha="";
		if(qh32m_m!=null && qh32m_m!=null){
			fecha=(qh32m_m.toString().length()==1?"0"+qh32m_m:qh32m_m)+"/"+qh32m_y;
		}
		return fecha;
	}
}
