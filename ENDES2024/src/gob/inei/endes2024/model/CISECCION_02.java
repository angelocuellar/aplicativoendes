package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
public class CISECCION_02 extends Entity implements IDetailEntityComponent, Serializable {   
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    
    public Integer qi212=null; 
    public String qi212_nom=null; 
    public Integer qi213=null; 
    public Integer qi214=null; 
    public String qi215d=null; 
    public String qi215m=null; 
    public Integer qi215y=null; 
    public Integer qi216=null; 
    public Integer qi217=null;
    public String qi217cons=null;
    public Integer qi218=null; 
    public Integer qi219=null; 
    public Integer qi220u=null; 
    public Integer qi220n=null; 
    public Integer qi220a=null; 
    public Integer qi221=null; 
    public Integer qiedad_meses=null;
    
    public Integer qiedad_gest=null; 
    public String qi200_obs = null;
    
    @FieldEntity(ignoreField= true, saveField=false)
	public boolean esPrimerRegistro = false;
    @FieldEntity(ignoreField= true, saveField=false)
	public Integer estadocap4a=null;
    @FieldEntity(ignoreField= true, saveField=false)
	public Integer estadocap4b=null;
    @FieldEntity(ignoreField= true, saveField=false)
	public Integer estadocap2=null;
    
    public CISECCION_02() {}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub		
	}

	public Integer getQi212() {
		return qi212;
	}
	
	public void setQi212(Integer qi212) {
		this.qi212 = qi212;
	}

	public String getQi212_nom() {
		return qi212_nom;
	}
	public void setQi212_nom(String qi212_nom) {
		this.qi212_nom = qi212_nom;
	} 
   
	public String getQi215() {
		String fecha ="";
		if(qi215d!=null && qi215m!=null && qi215y!=null )
			fecha = qi215d+ "/" + qi215m + "/"+qi215y;
		return fecha;
	}
	
	public String getQi216() {
		return qi216==1 ? "Si" : "No";
	}
	
	public String getQi217() {
		return qi216==1 ? qi217.toString() : "";
	} 
	
	public String getQi218() {
		return qi216==1 ? qi218==1 ? "Si" : "No" : "";
	}
	public Calendar fechanacimiento(){
		Calendar data = null;
		data= new GregorianCalendar(qi215y, Integer.parseInt(qi215m.toString())-1, Integer.parseInt(qi215d));
		return data;
	}
} 
