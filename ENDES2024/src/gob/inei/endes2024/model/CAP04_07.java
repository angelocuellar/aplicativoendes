package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CAP04_07 extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer qs401=null; 
    public Integer qs402=null; 
    public Integer qs403=null; 
    public Integer qs404=null;    
//    public Integer qs404a=null;
//    public Integer qs404b=null;
    public Integer qs404c=null;
    
    public Integer qs406=null; 
    public Integer qs407_a=null; 
    public Integer qs407_b=null; 
    public Integer qs407_c=null; 
    public Integer qs407_d=null; 
    public Integer qs407_e=null; 
    public Integer qs407_f=null; 
    public Integer qs407_g=null; 
    public Integer qs407_h=null; 
    public Integer qs407_i=null; 
    public Integer qs407_j=null; 
    public Integer qs407_k=null; 
    public Integer qs407_l=null; 
    public Integer qs407_m=null; 
    public Integer qs407_n=null; 
    public String qs407_n_o=null; 
    public Integer qs407_x=null; 
    public String qs407_x_o=null; 
    public Integer qs407_y=null; 
    public Integer qs409=null; 
    public Integer qs410=null; 
    public Integer qs410_a=null; 
    public Integer qs411=null; 
    public Integer qs412=null; 
    public Integer qs412_a=null; 
    public Integer qs413=null;     
    public Integer qs413a=null; 
    public Integer qs413b=null; 
    public Integer qs413b_a=null; 
    public Integer qs413c=null; 
    public Integer qs413d=null; 
    public Integer qs413e=null; 
    public Integer qs413e_a=null;
    public Integer qs413f=null; 
    
    public Integer qs415=null; 
    public Integer qs416=null; 
    public Integer qs416_a=null;    
//    public Integer qs418=null; 
//    public Integer qs419=null; 
//    public Integer qs419_a=null;
    
    public Integer qs418a=null; 
    public Integer qs419a=null; 
    public Integer qs419a_a=null; 
    
    public String qsobs_seccion4=null;
    public Integer qs500=null; 
    public Integer qs501=null; 
    public Integer qs501_a=null; 
    public Integer qs503=null; 
    public Integer qs505a=null; 
    public Integer qs505b=null; 
    public Integer qs505c=null; 
    public Integer qs505d=null; 
    public String qsobs_seccion5=null;
    
    public Integer qs505a1=null; 
    public String  qs505a1_o=null; 
    public Integer qs505b1=null; 
    public String  qs505b1_o=null; 
    
    public Integer qs506=null;
    
    public Integer qs506a=null; 
    public Integer qs506b=null; 
    
    public Integer qs601a=null; 
    public Integer qs601b=null; 
    public Integer qs603=null;
    public Integer qs604=null; 
    public Integer qs606=null; 
    public Integer qs607=null; 
    public Integer qs608=null; 
    public Integer qs609=null; 
    public Integer qs610=null; 
    public Integer qs611=null; 
    public String qsobs_s6=null;
    public Integer qs700a=null; 
    public Integer qs700b=null; 
    public Integer qs700c=null; 
    public Integer qs700d=null; 
    public Integer qs700e=null; 
    public Integer qs700f=null; 
    public Integer qs700g=null; 
    public Integer qs700h=null; 
    public Integer qs700i=null; 
    public String qs700fech_ref=null;
    public Integer qs702=null; 
    public Integer qs703=null; 
    public String qs703fech_ref=null;
    public String qs704prv=null; 
    public Integer qs704a=null; 
    public Integer qs704b=null; 
    public Integer qs704c=null; 
    public Integer qs704d=null; 
    public Integer qs704e=null; 
    public Integer qs704f=null; 
    public Integer qs704g=null; 
    public Integer qs704h=null; 
    public Integer qs704i=null; 
    public Integer qs706=null; 
    public Integer qs707=null; 
    public Integer qs708_a=null;
    
    public Integer qs708_aa=null;
    public Integer qs708_ab=null;
    
    public Integer qs708_b=null; 
    public Integer qs708_c=null; 
    public Integer qs708_d=null; 
    public Integer qs708_e=null; 
    public Integer qs708_ea=null; 
    public Integer qs708_f=null; 
    public Integer qs708_g=null; 
    public Integer qs708_h=null; 
    public Integer qs708_i=null; 
    public Integer qs708_j=null; 
    public Integer qs708_x=null; 
    public String qs708_o=null; 
    public Integer qs708_y=null; 
    public Integer qs709=null; 
    public String qs709fech_ref=null;
    public Integer qs710=null; 
    public Integer qs711=null; 
    public Integer qs713=null;
    public String qs713fech_ref=null;
    public Integer qs714=null; 
    public Integer qs715=null; 
    public Integer qs716=null; 
    public Integer qs717=null; 
    public Integer qs719=null; 
    public Integer qs720=null; 
    public Integer qs721=null; 
    public Integer qs722=null; 
    public Integer qs723=null; 
    public Integer qs724=null; 
    public Integer qs725=null; 
    public Integer qs726=null; 
    public Integer qs727=null; 
    public Integer qs728=null; 
    public Integer qs729=null; 
    public Integer qsp730=null; 
    public Integer qs731_a=null;
    
    public Integer qs731_aa=null;
    public Integer qs731_ab=null;
    
    public Integer qs731_b=null; 
    public Integer qs731_c=null; 
    public Integer qs731_d=null; 
    public Integer qs731_e=null; 
    public Integer qs731_ea=null; 
    public Integer qs731_f=null; 
    public Integer qs731_g=null; 
    public Integer qs731_h=null; 
    public Integer qs731_i=null; 
    public Integer qs731_j=null; 
    public Integer qs731_x=null; 
    public String qs731_o=null; 
    public Integer qs731_y=null; 
    public String qs731ah=null; 
    public String qs731am=null;
    public String qsobs_s7=null;
    public BigDecimal qs900=null; 
    public BigDecimal qs901=null; 
    public Integer qs902=null; 
    public String qs902_o=null; 
    public Integer qs903=null; 
    public Integer qs903_1=null; 
    public String qs904h=null; 
    public String qs904m=null; 
    public Integer qs905=null; 
    public Integer qs905_1=null; 
    public Integer qs906=null;     
    public String qs906_o=null;
    
    public BigDecimal qs907=null;
    public Integer qs908=null; 
    public String qs908_o=null; 
    
    public Integer qs908b_a = null;
    public Integer qs908b_b = null;
    public Integer qs908c = null;
    public String qs908c1 = null;
    public String qs908c2 = null;
    public String qs908c3 = null;
    public Integer qs908d = null;
    public String qs908d1 = null;
    
    public Integer qs908e = null;
    public String qs908e1 = null;
    public String qs908e_o = null;
    
    public String qsnom_ant=null; 
    public String qscod_ant=null; 
    public String qsnom_aux=null; 
    public String qscod_aux=null; 
    public String qsobs_antro=null; 
    public String qsobs_entrev=null; 
    public String qsobs_super=null;
    public Integer qsrec_09=null;
    public String qsfrc_fecha09=null; 
    
    public String qs900a=null;
    public String qs902a=null;
    public String qs903a=null;
    public String qs906a=null;
    public String qs907a=null;
    public String qs908a=null;
    public String qs900_ref_s9=null;
    
    @FieldEntity(ignoreField=true, saveField=false)
  	public boolean entrars8=false;
    @FieldEntity(ignoreField=true, saveField=false)
	public boolean existes8=false;
    @FieldEntity(ignoreField=true, saveField=false)
	public boolean existemef=false;
	@FieldEntity(ignoreField=true, saveField=false)
	public boolean estadoseccion8Completo=false;
	@FieldEntity(ignoreField=true, saveField=false)
  	public boolean MEF=false;
	
    public CAP04_07() {} 

    public Double getPeso() {
		Double peso= Redondear(qs900);
		return peso;
	}
    public Double getTalla() {
		Double talla=Redondear(qs901);
		return talla;
	}
    public Double getPerimetro() {
		Double perim=Redondear(qs907);
		return perim;
	}
    
    public String getResultado1() {
    	String resultado = "";
    	if(qs902!=null){
			if(qs902==1) resultado="Medido(a)";
			if(qs902==2) resultado="No Presente";
			if(qs902==3) resultado="Rechazo";
			if(qs902==4) resultado="Fue eval. en C. Hogar";
			if(qs902==5) resultado="Medido(a) parcialmente";
			if(qs902==6) resultado="Otro";
    	}
		return resultado;
	}
    public String getResultado2() {
    	String resultado = "";
    	if(qs906!=null){
			if(qs906==1) resultado="Medido(a)";
			if(qs906==2) resultado="No Presente";
			if(qs906==3) resultado="Rechazo";
			if(qs906==6) resultado="Otro";
    	}
		return resultado;
	}
    public String getResultado3() {
    	String resultado = "";
    	if(qs908!=null){
    		if(qs908==1) resultado="Medido(a)";
			if(qs908==2) resultado="No Presente";
			if(qs908==3) resultado="Rechazo";
			if(qs908==6) resultado="Otro";
    	}
		return resultado;
	}
    
    
    public double Redondear(BigDecimal num) {
		Double n=0.0;
		n= num.doubleValue();
		n=Math.rint(n*10)/10;
		return n;
	}
    public Integer getConvertqs401(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 3: valorReturn=8; break;
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
    
    public Integer getConvertqs505a1(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 3: valorReturn=3; break;
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
    
    public Integer getConvertqs505b1(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 6: valorReturn=8; break;
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
    
    
	public Integer setConvertqs401(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 8: valorReturn=3; break;	
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs505a1(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 8: valorReturn=8; break;	
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
	
	public Integer setConvertqs505b1(Integer qs401) {
		Integer valorReturn=0;
		switch (qs401){
		case 8: valorReturn=6; break;	
		default: valorReturn=qs401;
		}
		return valorReturn;
	}
	
    public Integer getConvertqs413b(Integer qs413b) {
		Integer valorReturn=0;
		switch (qs413b){
		case 3: valorReturn=8; break;
		default: valorReturn=qs413b;
		}
		return valorReturn;
	}
	public Integer setConvertqs413b(Integer qs413b) {
		Integer valorReturn=0;
		switch (qs413b){
		case 8: valorReturn=3; break;	
		default: valorReturn=qs413b;
		}
		return valorReturn;
	}
	
    public Integer getConvertqs413e(Integer qs413e) {
		Integer valorReturn=0;
		switch (qs413e){
		case 3: valorReturn=8; break;
		default: valorReturn=qs413e;
		}
		return valorReturn;
	}
	public Integer setConvertqs413e(Integer qs413e) {
		Integer valorReturn=0;
		switch (qs413e){
		case 8: valorReturn=3; break;	
		default: valorReturn=qs413e;
		}
		return valorReturn;
	}
	
	
	public Integer setConvert603(Integer qs603) {
		Integer valorReturn=0;
		switch(qs603){
		case 8: valorReturn=3; break;
		default: valorReturn=qs603;
		}
		return valorReturn;
    }
	public Integer getConvert603(Integer qs603) {
		Integer valorReturn = 0;
		switch (qs603) {
		case 3: valorReturn = 8; break;	
		default:valorReturn = qs603;
		}
		return valorReturn;
     }

	public Integer setConvert700(Integer qs700) {
		Integer valorReturn=0;
		switch(qs700){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		}
		return valorReturn;
    }
	public Integer getConvert700(Integer qs700) {
		Integer valorReturn = 0;
		switch (qs700) {
		case 1: valorReturn =0; break;	
		case 2: valorReturn =1; break;
		case 3: valorReturn =2; break;
		case 4: valorReturn =3; break;
		}
		return valorReturn;
     }
	
	public Integer getConvertqs708(Integer qs708) {
		Integer valorReturn=0;
		switch (qs708){
		case 0: valorReturn=1; break;
		case 1: valorReturn=0; break;
		default: valorReturn=qs708;
		}
		return valorReturn;
	}
	public Integer setConvertqs708(Integer qs708) {
		Integer valorReturn=0;
		switch (qs708){
		case 1: valorReturn=0; break;
		case 0: valorReturn=1; break;
		default: valorReturn=1;
		}
		return valorReturn;
	}
	
	
	public Integer setConvert713(Integer qs713) {
		Integer valorReturn=0;
		switch(qs713){
		case 8: valorReturn=3; break;
		case 9: valorReturn=4; break;	
		default: valorReturn=qs713;
		}
		return valorReturn;
    }
	public Integer getConvert713(Integer qs713) {
		Integer valorReturn = 0;
		switch (qs713) {
		case 3: valorReturn = 8; break;	
		case 4: valorReturn = 9; break;	
		default:valorReturn = qs713;
		}
		return valorReturn;
     }

	
	public Integer getConvertqs407(Integer qs407) {
		Integer valorReturn=0;
		switch (qs407){
		case 0: valorReturn=1; break;
		case 1: valorReturn=0; break;
		default: valorReturn=qs407;
		}
		return valorReturn;
	}
	public Integer setConvertqs407(Integer qs407) {
		Integer valorReturn=0;
		switch (qs407){
		case 1: valorReturn=0; break;
		case 0: valorReturn=1; break;
		default: valorReturn=1;
		}
		return valorReturn;
	}
	
	public Integer getConvertqs731(Integer qs731) {
		Integer valorReturn=0;
		switch (qs731){
		case 0: valorReturn=1; break;
		case 1: valorReturn=0; break;
		default: valorReturn=qs731;
		}
		return valorReturn;
	}
	public Integer setConvertqs731(Integer qs731) {
		Integer valorReturn=0;
		switch (qs731){
		case 1: valorReturn=0; break;
		case 0: valorReturn=1; break;
		default: valorReturn=1;
		}
		return valorReturn;
	}
	
	public Integer setConvert906(Integer qs906) {
		Integer valorReturn=0;
		switch(qs906){
		case 6: valorReturn=4; break;
		default: valorReturn=qs906;
		}
		return valorReturn;
    }
	public Integer getConvert906(Integer qs906) {
		Integer valorReturn = 0;
		switch (qs906) {
		case 4: valorReturn = 6; break;	
		default:valorReturn = qs906;
		}
		return valorReturn;
     }
} 
