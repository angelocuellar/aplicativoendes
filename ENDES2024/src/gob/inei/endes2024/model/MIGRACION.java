package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

public class MIGRACION extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	public Integer qh34a = null;
	
    public String qh34b_1 = null;
    public String qh34b_11 = null;
    public String qh34b_2 = null; 
    public String qh34b_3 = null;
	
	public Integer qh34c = null;
	public Integer qh34d_a = null;
	public Integer qh34d_m = null;
	
	public Integer qh34e_a = null;
	public Integer qh34e_m = null;
	
	public String qh34f_ccdd = null;
	public Integer qh34f_ccdd_ns = null;
	public String qh34f_ccpp = null;
	public Integer qh34f_ccpp_ns = null;
	public String qh34f_ccdi = null;
	public Integer qh34f_ccdi_ns = null;		
	public String qh34f_pais = null;
	public Integer qh34f_pais_ns = null;
	public String qh34f_observacion = null;
	
	public Integer qh34g = null;
	public String qh34go = null;
	public String qh34g_observacion = null;
	
	public Integer qh34h = null;
	public String qh34ho = null;	
		
	public Integer qh34i = null;
	
	public Integer qh34j = null;
	public String qh34j_ref = null;
	
	public Integer qh34k = null;
	public String qh34ko = null;

	public String qh34_observacion = null;

	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	
	
	public MIGRACION() {
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
	}

	@Override
	public String getNombre() {
		return "";//"Hogar N� " + hogar_id + " - " + getJefeHogar();
	}
	
	public String getNombres() {
		String nombre = "";
		if (qh34b_1 != null) {
			nombre = Util.getText(qh34b_1)+ " " +Util.getText(qh34b_11)+ " " +(Util.getText(qh34b_2)) +" "+ (Util.getText(qh34b_3));
		}
		return nombre;
	}
	
	public String getSexo() {
		int op = qh34c != null ? Util.getInt(qh34c) : -1;
		String retorna = "";

		switch (op) {
		case 1:
			retorna = "Hombre";
			break;
		case 2:
			retorna = "Mujer";
			break;
		default:
			retorna = "";
		}
		return retorna;
	}


	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	


	public Integer setValue3a8(Integer qh233){
		Integer valorReturn=0;
		switch(qh233){
			case 3: valorReturn=8; break;	
			default: valorReturn=qh233;
		}
		return valorReturn;
	}
	public Integer getValue8a3(Integer qh233){
		Integer valorReturn = 0;
		switch (qh233) {
			case 8: valorReturn = 3; break;
			default:valorReturn = qh233;
		}
		return valorReturn;
	}
	
}
