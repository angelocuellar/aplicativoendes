package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;

import java.io.Serializable;

public class DISCAPACIDAD  extends Entity implements Serializable {
	private static final long serialVersionUID = 1L; 
	public Integer hogar_id=null;
	public Integer persona_id=null;
	public Integer cuestionario_id=null;
	public Integer ninio_id=null;
	public Integer qd333_1=null;
	public Integer qd333_2=null;
	public Integer qd333_3=null;
	public Integer qd333_4=null;
	public Integer qd333_5=null;
	public Integer qd333_6=null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public String nombre=null;
	public DISCAPACIDAD(){
		
	}
}
