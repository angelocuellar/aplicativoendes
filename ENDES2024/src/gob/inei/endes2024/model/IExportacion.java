package gob.inei.endes2024.model;

import gob.inei.dnce.components.FragmentForm;

public interface IExportacion {
	FragmentForm getFragmentForm();

}
