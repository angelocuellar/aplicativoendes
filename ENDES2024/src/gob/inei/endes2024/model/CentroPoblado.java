package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;

public class CentroPoblado extends Entity {

	private static final long serialVersionUID = 1L;
	public String ubigeo = null;
	public String cccp = null;
	public String centro_poblado = null;

	public CentroPoblado() {
	}

	@Override
	public String toString() {
		return cccp != null ? cccp + " - " + centro_poblado : centro_poblado;
	}
}
