package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_04DIT_02 extends Entity implements Serializable,IDetailEntityComponent{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer nro_orden_ninio=null;
    public Integer qi478 =null;
    public Integer qi478a=null;    
    public Integer qi478e1=null; 
    public Integer qi478e2=null; 
    public Integer qi478e3=null; 
    public Integer qi478e4=null; 
    public Integer qi478e5=null; 
    public Integer qi478e6=null; 
    public Integer qi478e7=null; 
    public Integer qi478e8=null; 
    public Integer qi478e9=null; 
    public Integer qi478e10=null; 
    public Integer qi478f1=null; 
    public Integer qi478f2_a=null; 
    public Integer qi478f2_b=null; 
    public Integer qi478f2_c=null; 
    public Integer qi478f2_d=null; 
    public Integer qi478f2_e=null; 
    public Integer qi478f3=null; 
    public Integer qi478f4=null; 
    public Integer qi478f5=null; 
    public Integer qi478f6=null; 
    public Integer qi478g1=null; 
    public Integer qi478g2_a=null; 
    public Integer qi478g2_b=null; 
    public Integer qi478g2_c=null; 
    public Integer qi478g3=null; 
    public Integer qi478g4=null; 
    public Integer qi478h1=null; 
    public Integer qi478h2=null; 
    public Integer qi478h3=null; 
    public Integer qi478h4=null; 
    public Integer qi478h5=null; 
    public Integer qi478h6=null; 
    public Integer qi478h7=null; 
    public Integer qi478h8_a=null; 
    public Integer qi478h8_b=null; 
    public Integer qi478h9=null; 
    public Integer qi478h10=null; 
    public Integer qi478h11=null; 
    public Integer qi478h12=null; 
    public Integer qi478i1=null; 
    public Integer qi478i2=null; 
    public Integer qi478i3=null; 
    public Integer qi478i4_a=null; 
    public Integer qi478i4_b=null; 
    public Integer qi478i5=null; 
    public Integer qi478i6=null; 
    public Integer qi478i7=null; 
    public Integer qi478i8=null; 
    public Integer qi478j1=null; 
    public Integer qi478j2=null; 
    public Integer qi478j3=null; 
    public Integer qi478j4_a=null; 
    public Integer qi478j4_b=null; 
    public Integer qi478j5=null; 
    public Integer qi478j6=null; 
    public Integer qi478j7=null; 
    public Integer qi478j8=null; 
    
    public CISECCION_04DIT_02() {}
    
	@FieldEntity(ignoreField=true, saveField=false)
	public String qi212_nom=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estadocap4dit=null;
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	} 
	
	public Integer setConvertconCuatroVariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 4:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconCuatroVariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=4;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertconTresVariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 3:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconTresVariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=3;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	
	
	public Integer setConvertconSieteVariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 7:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconSieteVariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=7;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	
	
	public Integer setConvertconCincoVariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 5:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconCincoVariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=5;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	
	public Integer setConvertconSeisVariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 6:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconSeisVariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=6;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	
} 
