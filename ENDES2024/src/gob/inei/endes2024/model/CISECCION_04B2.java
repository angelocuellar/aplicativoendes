package gob.inei.endes2024.model;

import java.io.Serializable;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;

public class CISECCION_04B2 extends Entity implements Serializable{
    private static final long serialVersionUID = 1L;
	public Integer hogar_id  = null; 
	public Integer persona_id  = null; 
    public Integer qi479a = null;
	public Integer qi479b = null;
	public Integer qi479c = null;
	public Integer qi479d = null;	
	public Integer qi479e_a = null;
	public Integer qi479e_b = null;
	public Integer qi479e_c = null;
	public Integer qi479e_d = null;
	public Integer qi479e_e = null;
	public Integer qi479e_f = null;
	public Integer qi479e_g = null;
	public Integer qi479e_h = null;
	public Integer qi479e_i = null;
	public Integer qi479e_j = null;
	public Integer qi479e_x = null;
	public String qi479e_xi = null;
	public Integer qi479e_y = null;	
	public Integer qi479f_a = null;
	public Integer qi479f_b = null;
	public Integer qi479f_c = null;
	public Integer qi479f_d = null;
	public Integer qi479f_e = null;
	public Integer qi479f_f = null;
	public Integer qi479f_g = null;
	public Integer qi479f_h = null;
	public Integer qi479f_i = null;
	public Integer qi479f_j = null;
	public Integer qi479f_k = null;
	public Integer qi479f_l = null;
	public Integer qi479f_m = null;
	public Integer qi479f_x = null;
	public String qi479f_xi = null;
	public Integer qi479f_y = null;	
	public Integer qi479g = null;
	public Integer qi479g_a = null;
	public Integer qi479g_b = null;
	public Integer qi479g_c = null;
	public Integer qi479g_d = null;
	public Integer qi479g_e = null;
	public Integer qi479g_f = null;
	public Integer qi479g_g = null;
	public Integer qi479g_h = null;
	public Integer qi479g_i = null;
	public Integer qi479g_j = null;
	public Integer qi479g_x = null;
	public String  qi479g_xi = null;
	public Integer qi479g_y = null;
	
	public String  qi479g_cons=null;
	public Integer qi479gb_a=null;
	public Integer qi479gb_b=null;
	public Integer qi479gb_c=null;
	public Integer qi479gb_d=null;
	public Integer qi479gb_e=null;
	public Integer qi479gb_f=null;
	public Integer qi479gb_g=null;
	public Integer qi479gb_h=null;
	public Integer qi479gb_i=null;
	public Integer qi479gb_j=null;
	public Integer qi479gb_x=null;
	public Integer qi479gc_a=null;
	public Integer qi479gc_b=null;
	public Integer qi479gc_c=null;
	public Integer qi479gc_d=null;
	public Integer qi479gc_e=null;
	public Integer qi479gc_f=null;
	public Integer qi479gc_g=null;
	public Integer qi479gc_h=null;
	public Integer qi479gc_i=null;
	public Integer qi479gc_j=null;
	public Integer qi479gc_x=null;
	
	public Integer qi480a = null;
	public Integer qi481a_a = null;
	public Integer qi481a_b = null;
	public Integer qi481a_c = null;
	public Integer qi481a_d = null;
	public Integer qi481a_e = null;
	public Integer qi481a_f = null;
	public Integer qi481a_g = null;
	public Integer qi481a_x = null;
	public String qi481a_xi = null;
	public Integer qi481a_z = null;
	public Integer qi482 = null;
	public Integer qi483 = null;
	public String qi483_x = null;
	public Integer qi487_a = null;
	public Integer qi487_b = null;
	public Integer qi487_c = null;
	public Integer qi487_d = null;
	public Integer qi487_e = null;
	public Integer qi487_f = null;
	public Integer qi487_g = null;
	public Integer qi487_h = null;
	public Integer qi487_i = null;
	public Integer qi488 = null;
	public Integer qi488a = null;
	public Integer qi489 = null;
	public Integer qi489a_a = null;
	public Integer qi489a_b = null;
	public Integer qi489a_c = null;
	public Integer qi489a_d = null;
	public Integer qi489a_e = null;
	public Integer qi489a_f = null;
	public Integer qi489a_x = null;
	public String qi489a_xi = null;
	public Integer qi489a_z = null;
	public Integer qi489b = null;
	public Integer qi489c = null;
	public Integer qi489d = null;
	public Integer qi490 = null;
	public Integer qi490a_a = null;
	public Integer qi490a_b = null;
	public Integer qi490a_c = null;
	public Integer qi490a_d = null;
	public Integer qi490a_e = null;
	public Integer qi490a_f = null;
	public Integer qi490a_g = null;
	public Integer qi490a_x = null;
	public String qi490a_xi = null;
	public Integer qi490b_a = null;
	public Integer qi490b_b = null;
	public Integer qi490b_c = null;
	public Integer qi490b_d = null;
	public Integer qi490b_x = null;
	public String qi490b_xi = null;
	public Integer qi492_a = null;
	public Integer qi492_b = null;
	public Integer qi492_c = null;
	public Integer qi492_d = null;
	public Integer qi492_e = null;
	public Integer qi492_f = null;
	public Integer qi492_g = null;
	public Integer qi493_a = null;
	public Integer qi493_b = null;
	public Integer qi493_c = null;
	public Integer qi493_d = null;
	public Integer qi493_e = null;
	public Integer qi493_f = null;
	public Integer qi493_g = null;
	public Integer qi493_h = null;
	public Integer qi493_i = null;
	public Integer qi493_i1 = null;
	public Integer qi493_i2 = null;
	public Integer qi493_j = null;
	public Integer qi493_k = null;
	public Integer qi493_l = null;
	public Integer qi493_m = null;
	public Integer qi493_n = null;
	public Integer qi493_o = null;
	public Integer qi493_p = null;
	public Integer qi495a = null;
	public Integer qi496 = null;
	public String qi496x = null;
	
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean filtro479 = false;
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean filtro480 = false;
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean filtro481 = false;	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean filtro491 = false;
	
	public Integer setConvertcontresvariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 3:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertcontresvariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=3;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertconcuatrovariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 4:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconcuatrovariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=4;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertconnuevevariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 9:retonar=96;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconnuevevariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 96:retornar=9;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertconseisvariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 6:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconseisvariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=6;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertcondiezvariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 10:retonar=96;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertcondiezvariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 96:retornar=10;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
}
