package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

public class Visita extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	public Integer nro_visita = null;
	public String qhvdia_ini = null;
	public String qhvmes_ini = null;
	public Integer qhvanio_ini = null;
	public Integer qhvresul = null;
	public Integer qhvresul_r = null;
	public String  qhvresul_o = null;
	public String qhhora_ini = null;
	public String qhmin_ini = null;
	public String qhvdiap = null;
	public String qhvmesp = null;
	public String qhvhorap = null;
	public String qhvminup = null;
	public String qhvmin_fin=null;   
	public String qhvhora_fin =null;
    
	public Visita() {
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub

	}

	public String getFechainicio() {
		String fecha = "";
		String mes="";
		String dia="";
		if (qhvdia_ini != null && qhvmes_ini != null) {
			mes= qhvmes_ini.toString().length()>1?qhvmes_ini.toString():"0"+qhvmes_ini.toString();
			dia=qhvdia_ini.toString().length()>1?qhvdia_ini.toString():"0"+qhvdia_ini.toString();
			fecha = dia + "/" + mes;
		}
		return fecha;
	}

	public String getHoraInicio() {
		String Horainicio = "";
		if (qhhora_ini != null && qhmin_ini != null) {
			Horainicio = (qhhora_ini.toString().length() == 1 ? "0"
					+ qhhora_ini.toString() : qhhora_ini.toString())
					+ ":"
					+ (qhmin_ini.toString().length() == 1 ? "0"
							+ qhmin_ini.toString() : qhmin_ini.toString());
		}
		return Horainicio;
	}

	public String getFechaProxima() {
		String fechaproxima = "";
		String mes="";
		String dia="";
		if (qhvmesp != null && qhvdiap != null) {
			mes=qhvmesp.toString().length()>1?qhvmesp.toString():"0"+qhvmesp.toString();
			dia = qhvdiap.toString().length()>1?qhvdiap.toString():"0"+qhvdiap.toString();
			fechaproxima = dia + "/" + mes;
		}
		return fechaproxima;
	}

	public String getHoraProxima() {
		String proximahora = "";
		if (qhvhorap != null && qhvminup != null) {
			proximahora = (qhvhorap.toString().length() == 1 ? "0"
					+ qhvhorap.toString() : qhvhorap.toString())
					+ ":"
					+ (qhvminup.toString().length() == 1 ? "0"
							+ qhvminup.toString() : qhvminup.toString());
		}
		return proximahora;
	}

	public String getResultado()
	{String resultado="";
	
	int op = qhvresul!=null?Util.getInt(qhvresul):-1;
	switch (op) {
	case 1:resultado= "Completa";break;
	case 2:resultado="Hogar Presente";break;
	case 3:resultado="hogar Ausente";break;
	case 4:resultado="Aplazada";break;
	case 5:resultado="rechazada";break;
	case 6:resultado="Vivienda Desocupada";break;
	case 7:resultado="Vivienda Destruida";break;
	case 8:resultado="Vivienda No Encontrada";break;
	case 9:resultado="Otro";break;
	default:resultado="";break;
		}
	return resultado;
	}
	
	public String getResultado_r()
	{String resultado_r="";
	
	int op = qhvresul_r!=null?Util.getInt(qhvresul_r):-1;
	switch (op) {
	case 1:resultado_r="No desean la entrevista";break;
	case 2:resultado_r="Ya fue entrevistada por la ENDES";break;
	case 3:resultado_r="Ya fue entrevistada por otra encuesta del INEI";break;
	default:resultado_r="";break;
		}
	return resultado_r;
	}
	
	public String getHoraFin()
	{String horaFinal="";
		if(qhvhora_fin!=null && qhvmin_fin!=null)
		{
			horaFinal = (qhvhora_fin.toString().length()==1?"0"+qhvhora_fin.toString():qhvhora_fin.toString())+":"+(qhvmin_fin.toString().length()==1?"0"+qhvmin_fin.toString():qhvmin_fin.toString());
		}
		return horaFinal;
	}
}
