package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

import java.io.Serializable;
import java.math.BigDecimal;
public class CISECCION_01_03 extends Entity implements IDetailEntityComponent, Serializable ,Coberturable{   
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer qitotaln=null;
    public Integer qitotalc=null;
    public Integer qi102=null; 
    public Integer qi103=null; 
    public Integer qi104=null; 
    public String  qi105d=null; 
    public String  qi105m=null; 
    public Integer qi105y=null;
    public String qi105cons=null;
    public Integer qi106=null; 
    public Integer qi107=null; 
    public Integer qi108n=null; 
    public Integer qi108y=null; 
    public Integer qi108g=null; 
    public Integer qi111=null; 
    public Integer qi112=null; 
    public String  qi112_o=null; 
    public Integer qi114=null; 
    public String  qi114_o=null; 
    public Integer qi114a=null; 
    public Integer qi115=null; 
    public Integer qi116=null; 
    public Integer qi117=null; 
    public Integer qi119=null;
    public String  qi119_o=null;
    public Integer qi119a=null;
    public String  qi119a_o=null;
    public Integer qi119b=null; 
    public String  qi119b_o=null;
    public Integer qi119c=null;
    public String  qi119c_o=null;
    public Integer qi119d=null;
    public String  qi119d_o=null;
    public Integer qi201=null; 
    public Integer qi202=null; 
    public Integer qi203_a=null; 
    public Integer qi203_b=null; 
    public Integer qi204=null; 
    public Integer qi205_a=null; 
    public Integer qi205_b=null; 
    public Integer qi206=null; 
    public Integer qi207_a=null; 
    public Integer qi207_b=null; 
    public Integer qi208=null; 
    public Integer qi209=null;
    public String qiobs_s2=null; 
    public String qiobs_s3=null; 
    public Integer qi221a=null;      
    public Integer qi222=null;    
    public Integer qi224=null; 
    public Integer qi226=null;
    public String qi226cons=null;
    public Integer qi227=null; 
    public Integer qi228=null; 
    public String qi229_nom=null; 
    public Integer qi229_a=null; 
    public Integer qi229_b=null; 
    public Integer qi229_c=null; 
    public Integer qi229_d=null; 
    public Integer qi229_e=null; 
    public Integer qi229_f=null; 
    public Integer qi229_g=null; 
    public Integer qi229_h=null; 
    public Integer qi229_i=null; 
    public Integer qi229_j=null; 
    public Integer qi229_k=null; 
    public Integer qi229_l=null; 
    public Integer qi229_x=null; 
    public String qi229_o=null; 
    public Integer qi229_y=null; 
    public Integer qi229a=null;
    public Integer qi229aa=null;
    public Integer qi229b=null; 
    public Integer qi229c_a=null; 
    public Integer qi229c_b=null; 
    public Integer qi229c_c=null; 
    public Integer qi229c_d=null; 
    public Integer qi229c_e=null; 
    public Integer qi229c_x=null; 
    public String qi229c_o=null; 
    public Integer qi230=null;
    public String qi230_o=null;
    public String qi231_m=null; 
    public Integer qi231_y=null; 
    public Integer qi233=null; 
    public Integer qi234=null; 
    public Integer qi235a=null; 
    public String qi235b_m=null; 
    public Integer qi235b_y=null; 
    public Integer qi236=null; 
    public Integer qi236_a=null;
    public String qi236_o=null;
    public Integer qi237=null; 
    public Integer qi238=null; 
    public String qi238_tex=null; 
    public Integer qi239a=null; 
    public Integer qi239b=null; 
    public Integer qi239c=null; 
    public Integer qi239e=null; 
    public Integer qi239f=null; 
    public Integer qi239x=null; 
    public String qi239o=null; 
    public Integer qi302_01=null; 
    public Integer qi302_02=null; 
    public Integer qi302_03=null; 
    public Integer qi302_04=null; 
//    public Integer qi302_05=null; 
    public Integer qi302_06=null; 
    public Integer qi302_07=null; 
    public Integer qi302_08=null; 
    public Integer qi302_09=null; 
    public Integer qi302_10=null; 
    public Integer qi302_11=null; 
    public Integer qi302_12=null; 
    public Integer qi302_13=null; 
    public Integer qi302_14=null;
    public String  qi302o=null;
    public Integer qi304=null; 
    public Integer qi307=null; 
    public Integer qi310=null; 
    public Integer qi311_a=null; 
    public Integer qi311_b=null; 
    public Integer qi311_c=null; 
    public Integer qi311_d=null; 
    public Integer qi311_e=null; 
    public Integer qi311_f=null; 
    public Integer qi311_g=null; 
    public Integer qi311_h=null; 
    public Integer qi311_i=null; 
    public Integer qi311_j=null; 
    public Integer qi311_k=null; 
    public Integer qi311_l=null; 
    public Integer qi311_m=null; 
    public Integer qi311_x=null; 
    public String qi311_o=null; 
    public String qi312_nom=null; 
    public Integer qi312=null; 
    public String qi312_o=null; 
    public Integer qi313=null; 
    public Integer qi314=null; 
    public String qi315m=null; 
    public Integer qi315y=null; 
    public Integer qi315b=null; 
    public String qi315b_o=null; 
    public String qi316m=null; 
    public Integer qi316y=null; 
    public BigDecimal qi317a=null; 
    public Integer qi320=null; 
    public String qi320_o=null; 
    public String qi321_nom=null; 
    public Integer qi321a=null; 
    public String qi321a_o=null; 
    public Integer qi322=null; 
    public Integer qi323=null; 
    public Integer qi324=null; 
    public Integer qi324a=null; 
    public Integer qi325=null; 
    public Integer qi325a=null; 
    public String qi325a_o=null; 
    public Integer qi325b=null; 
    public String qi325b_o=null; 
    public Integer qi325c_a=null; 
    public Integer qi325c_b=null; 
    public Integer qi325c_x=null; 
    public String qi325c_o=null; 
    public Integer qi325d=null; 
    public String qi325d_o=null; 
    public Integer qi325e=null; 
    public String qi325e_o=null; 
    public Integer qi326=null; 
    public String qi327_nom=null; 
    public Integer qi327=null; 
    public String qi327_o=null; 
    public Integer qi327b=null; 
    public Integer qi327c=null; 
    public Integer qi327d=null; 
    public String qi327d_o=null; 
    public Integer qi328=null; 
    public Integer qi330=null; 
    public Integer qi331=null; 
    public Integer qi332=null; 
    public String qical_cons=null;
    public String qiobs_cal=null;
    public String qi_fant=null;
    public String qi_fant_o= null;
    public Integer qi302_05a=null;
    public Integer qi302_05b=null;
    public Integer qi101a=null;
    
    @FieldEntity(ignoreField=true, saveField=false)
   	public String qh02_1 = null;
    @FieldEntity(ignoreField=true, saveField=false)
   	public String qh01 = null;
   	@FieldEntity(ignoreField=true, saveField=false)
   	public String qh02_2 = null;
   	@FieldEntity(ignoreField=true, saveField=false)
   	public String qh02_3 = null;
   	@FieldEntity(ignoreField=true, saveField=false)
   	public Integer qh06 = null;
   	@FieldEntity(ignoreField=true, saveField=false)
   	public Integer qh07 = null;
   	
   	@FieldEntity(ignoreField=true, saveField=false)
   	public Integer qh7dd = null;
   	@FieldEntity(ignoreField=true, saveField=false)
   	public Integer qh7mm = null;
   	
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer violencia = null;
		
	@FieldEntity(ignoreField= true, saveField=false)
	public Integer total_ninios = 0;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public Integer total_ninios_vivos = 0;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public Integer total_ninios_4a = 0;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public Integer total_ninios_4dit = 0;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto1y3 =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto4a =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto4b =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto4dit =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto5y7 =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto8 =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean estadoCompleto10 =false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public String qi215 = null;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public String fechaPrimerAborto = null;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public String qi405 = null;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public String qi411_h = null;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public String qi409 = null;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean viotartarjeta = false;
	
	@FieldEntity(ignoreField= true, saveField=false)
	public boolean ExisteninioDe61Hasta71ParaDit = false;
	
    public CISECCION_01_03() {} 
    public String getNombre() {
		if (qh02_1 != null) {
			return Util.getText(qh02_1) + " " + Util.getText(qh02_2) + " "
					+ Util.getText(qh02_3);
		} else {
			return  "";
		}
	}
    public Integer getConvertQi108n(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 0: valorReturn=1; break;
			case 1: valorReturn=2; break;
			case 2: valorReturn=3; break;
			case 3: valorReturn=4; break;
			case 4: valorReturn=5; break;
			case 5: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi108n(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 1: valorReturn=0; break;
			case 2: valorReturn=1; break;
			case 3: valorReturn=2; break;
			case 4: valorReturn=3; break;
			case 5: valorReturn=4; break;
			case 6: valorReturn=5; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
    public Integer setConvertQi108y(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 1: valorReturn=0; break;
			case 2: valorReturn=1; break;
			case 3: valorReturn=2; break;
			case 4: valorReturn=3; break;
			case 5: valorReturn=4; break;
			case 6: valorReturn=5; break;
			case 7: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi108y(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 0: valorReturn=1; break;
			case 1: valorReturn=2; break;
			case 2: valorReturn=3; break;
			case 3: valorReturn=4; break;
			case 4: valorReturn=5; break;
			case 5: valorReturn=6; break;
			case 6: valorReturn=7; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
    public Integer getConvertqi312(Integer value){
    	Integer valoraretornar=value;
    	switch (value) {
		case 1:valoraretornar=10;break;
		case 2:valoraretornar=11;break;
		case 3:valoraretornar=12;break;
		case 4:valoraretornar=13;break;
		case 5:valoraretornar=14;break;
		case 6:valoraretornar=15;break;
		case 7:valoraretornar=16;break;
		case 8:valoraretornar=20;break;
		case 9:valoraretornar=21;break;
		case 10:valoraretornar=31;break;
		case 11:valoraretornar=32;break;
		case 12:valoraretornar=96;break;
		case 13:valoraretornar=98;break;
		default: valoraretornar=value;
			break;
		}
    	return valoraretornar;
    }
    public Integer setConvertqi312(Integer value){
    	Integer valoraretornar=value;
    	switch (value) {
		case 10:valoraretornar=1;break;
		case 11:valoraretornar=2;break;
		case 12:valoraretornar=3;break;
		case 13:valoraretornar=4;break;
		case 14:valoraretornar=5;break;
		case 15:valoraretornar=6;break;
		case 16:valoraretornar=7;break;
		case 20:valoraretornar=8;break;
		case 21:valoraretornar=9;break;
		case 31:valoraretornar=10;break;
		case 32:valoraretornar=11;break;
		case 96:valoraretornar=12;break;
		case 98:valoraretornar=13;break;
		default: valoraretornar=value;
			break;
		}
    	return valoraretornar;
    }
    public Integer getConvert226(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvert226(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
//	public Integer getConvertQi236(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 5: valorReturn=994; break;
//		case 6: valorReturn=995; break;
//		case 7: valorReturn=996; break;
//		case 8: valorReturn=998; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
//	public Integer setConvertQi236(Integer element) {
//		Integer valorReturn=0;
//		switch (element){
//		case 994: valorReturn=5; break;
//		case 995: valorReturn=6; break;
//		case 996: valorReturn=7; break;
//		case 998: valorReturn=8; break;
//		default: valorReturn=element;
//		}
//		return valorReturn;
//	}
	public Integer getConvertQi237(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi237(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi320(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		case 7: valorReturn=6; break;
		case 8: valorReturn=7; break;
		case 9: valorReturn=8; break;
		case 10: valorReturn=9; break;
		case 11: valorReturn=10; break;
		case 12: valorReturn=11; break;
		case 13: valorReturn=12; break;
		case 14: valorReturn=13; break;
		case 15: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi320(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 6: valorReturn=7; break;
		case 7: valorReturn=8; break;
		case 8: valorReturn=9; break;
		case 9: valorReturn=10; break;
		case 10: valorReturn=11; break;
		case 11: valorReturn=12; break;
		case 12: valorReturn=13; break;
		case 13: valorReturn=14; break;
		case 96: valorReturn=15; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi321a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=16; break;
		case 8: valorReturn=17; break;
		case 9: valorReturn=18; break;
		case 10: valorReturn=20; break;
		case 11: valorReturn=21; break;
		case 12: valorReturn=22; break;
		case 13: valorReturn=31; break;
		case 14: valorReturn=32; break;
		case 15: valorReturn=33; break;
		case 16: valorReturn=41; break;
		case 17: valorReturn=42; break;
		case 18: valorReturn=95; break;
		case 19: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi321a(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 16: valorReturn=7; break;
		case 17: valorReturn=8; break;
		case 18: valorReturn=9; break;
		case 20: valorReturn=10; break;
		case 21: valorReturn=11; break;
		case 22: valorReturn=12; break;
		case 31: valorReturn=13; break;
		case 32: valorReturn=14; break;
		case 33: valorReturn=15; break;
		case 41: valorReturn=16; break;
		case 42: valorReturn=17; break;
		case 95: valorReturn=18; break;
		case 96: valorReturn=19; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi325a(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 5: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi325a(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 6: valorReturn=5; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi325b(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 14: valorReturn=96; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi325b(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 96: valorReturn=14; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi325d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 6: valorReturn=96; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi325d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 96: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi325e(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 6: valorReturn=96; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi325e(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 96: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi326(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 2: valorReturn=3; break;
			case 3: valorReturn=4; break;
			case 4: valorReturn=5; break;
			case 5: valorReturn=6; break;
			case 6: valorReturn=9; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi326(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=2; break;
			case 4: valorReturn=3; break;
			case 5: valorReturn=4; break;
			case 6: valorReturn=5; break;
			case 9: valorReturn=6; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi327(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 1: valorReturn=10; break;
		case 2: valorReturn=11; break;
		case 3: valorReturn=12; break;
		case 4: valorReturn=13; break;
		case 5: valorReturn=14; break;
		case 6: valorReturn=15; break;
		case 7: valorReturn=16; break;
		case 8: valorReturn=17; break;
		case 9: valorReturn=18; break;
		case 10: valorReturn=20; break;
		case 11: valorReturn=21; break;
		case 12: valorReturn=22; break;
		case 13: valorReturn=31; break;
		case 14: valorReturn=32; break;
		case 15: valorReturn=33; break;
		case 16: valorReturn=41; break;
		case 17: valorReturn=42; break;
		case 18: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi327(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 10: valorReturn=1; break;
		case 11: valorReturn=2; break;
		case 12: valorReturn=3; break;
		case 13: valorReturn=4; break;
		case 14: valorReturn=5; break;
		case 15: valorReturn=6; break;
		case 16: valorReturn=7; break;
		case 17: valorReturn=8; break;
		case 18: valorReturn=9; break;
		case 20: valorReturn=10; break;
		case 21: valorReturn=11; break;
		case 22: valorReturn=12; break;
		case 31: valorReturn=13; break;
		case 32: valorReturn=14; break;
		case 33: valorReturn=15; break;
		case 41: valorReturn=16; break;
		case 42: valorReturn=17; break;
		case 96: valorReturn=18; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi327d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 1: valorReturn=11; break;
			case 2: valorReturn=21; break;
			case 3: valorReturn=22; break;
			case 4: valorReturn=23; break;
			case 5: valorReturn=24; break;
			case 6: valorReturn=25; break;
			case 7: valorReturn=26; break;
			case 8: valorReturn=27; break;
			case 9: valorReturn=28; break;
			case 10: valorReturn=31; break;
			case 11: valorReturn=32; break;
			case 12: valorReturn=33; break;
			case 13: valorReturn=34; break;
			case 14: valorReturn=41; break;
			case 15: valorReturn=42; break;
			case 16: valorReturn=51; break;
			case 17: valorReturn=52; break;
			case 18: valorReturn=53; break;
			case 19: valorReturn=54; break;
			case 20: valorReturn=61; break;
			case 21: valorReturn=62; break;
			case 22: valorReturn=96; break;
			case 23: valorReturn=98; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi327d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 11: valorReturn=1; break;
			case 21: valorReturn=2; break;
			case 22: valorReturn=3; break;
			case 23: valorReturn=4; break;
			case 24: valorReturn=5; break;
			case 25: valorReturn=6; break;
			case 26: valorReturn=7; break;
			case 27: valorReturn=8; break;
			case 28: valorReturn=9; break;
			case 31: valorReturn=10; break;
			case 32: valorReturn=11; break;
			case 33: valorReturn=12; break;
			case 34: valorReturn=13; break;
			case 41: valorReturn=14; break;
			case 42: valorReturn=15; break;
			case 51: valorReturn=16; break;
			case 52: valorReturn=17; break;
			case 53: valorReturn=18; break;
			case 54: valorReturn=19; break;
			case 61: valorReturn=20; break;
			case 62: valorReturn=21; break;
			case 96: valorReturn=22; break;
			case 98: valorReturn=23; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertqi119ab(Integer qi119ab){
		Integer data=0;
		switch(qi119ab){
		case 14:data=98;break;
		default:data=qi119ab;
		}
		return data;
	}
	public Integer getConvertqi119ab(Integer qi119ab){
		Integer data=0;
		switch(qi119ab){
		case 98:data=14;break;
		default:data=qi119ab;
		}
		return data;
	}
	public Integer setConvertqi119d(Integer qi119d){
		Integer data=0;
		switch(qi119d){
		case 9:data=98;break;
		default:data=qi119d;
		}
		return data;
	}
	public Integer getConvertqi119d(Integer qi119d){
		Integer data=0;
		switch(qi119d){
		case 98:data=9;break;
		default:data=qi119d;
		}
		return data;
	}
	
	
    public String getViolencia(){
    	if(violencia!=null){
    		if (violencia==1) {
    			return "SI";
    		} else {
    			return  "NO";
    		}
    	}else{
    		return  "";
    	}
    }
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
	}
	
} 
