package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

public class Endes_Resultado_vivienda extends Entity {
	public static final long serialVersionUID = 1L;
	public String conglomerado=null;
	public Integer id=null;
	public Integer resultadoviv_final=null;
	public Integer num_hogares=null;
	
	public Endes_Resultado_vivienda(){
		
	}
}
