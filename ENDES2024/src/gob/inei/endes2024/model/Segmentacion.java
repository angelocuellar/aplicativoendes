package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;

public class Segmentacion extends Entity {

	private static final long serialVersionUID = 1L;
	public String cod_sede_operativa = null;
	public String ccdd = null;
	public String ccpp = null;
	public String ccdi = null;
	public String equipo = null;
	public String ruta = null;
	public Integer anio = null;
	public String mes = null;
	public Integer periodo = null;
	public String conglomerado = null;

	@FieldEntity(ignoreField = true)
	public Usuario encuestador;
	@FieldEntity(ignoreField = true)
	public Usuario supervisor;
	@FieldEntity(ignoreField = true)
	public Usuario coordinador;

	public Segmentacion() {
	}

	public static class SegmentacionFiltro extends Entity {
		private static final long serialVersionUID = 1L;
		public String nombre;

		@Override
		public String toString() {
			return nombre;
		}

	}
}