package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Spinneable;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import android.util.Log;


public class Seccion01 extends Entity implements IDetailEntityComponent, Coberturable, Spinneable{
	private static final long serialVersionUID = 1L;
	public Integer hogar_id = null;
	public Integer persona_id = null;
	public Integer qh01 = null;
	public String qh02_1 = null;
	public String qh02_11 = null;
	public String qh02_2 = null;
	public String qh02_3 = null;

	// no va estas 3 variables
//	public Integer qhbebe = null;
//	public Integer qhnofam = null;
//	public Integer qhtempo = null;

	public Integer qh03 = null;
	public Integer qh04 = null;
	public Integer qh05 = null;
	public Integer qh06 = null;

	public Integer qh07 = null;

	public String qh7dd = null;
	public String qh7mm = null;

	public Integer qh08 = null;

	// no van estas 4 variables
	public Integer qh09 = null;
	public Integer qh10 = null;
	public Integer qh10b = null;
	public Integer qh10c = null;

	public Integer qh11_a = null;
	public Integer qh11_b = null;
	public Integer qh11_c = null;
	public Integer qh11_d = null;
	public Integer qh11_e = null;
	public Integer qh11_y = null;
	public Integer qh11_z = null;
	public Integer qh12 = null;
	public Integer qh13 = null;
	public String qh13fech_ref = null;
	public Integer qh14 = null;
	public Integer qh15n = null;
	public Integer qh15y = null;
	public Integer qh15g = null;
	public Integer qh16 = null;
	public Integer qh17 = null;
	public Integer qh18n = null;
	public Integer qh18y = null;
	public Integer qh18g = null;
	public Integer qh19 = null;
	public Integer qh20n = null;
	public Integer qh20y = null;
	public Integer qh20g = null;
	public Integer qh21 = null;
	public Integer qh21a = null;
	public Integer qh22 = null;
	public Integer qh23 = null;
	public Integer qh24 = null;
	public Integer qh25 = null;
	
	public String qh25a = null;
	public Integer qh25b = null;
	public String qh25cm = null;
	public String qh25ca = null;
	
	public String qh_obs = null;
	public Integer qh26a1 = null;
	public Integer qh26a2 = null;
	public Integer qh26a3 = null;
	public Integer qh26a4 = null;
	public Integer qh26a5 = null;
	public Integer qh26a6 = null;
	public String qh_observacion = null;
	public String qhobspersona = null;

	public Integer qhinfo=null;  
	
	public Integer pc701=null;
	public Integer pc702=null;
	public Integer pc703=null;
	public String  pc703_o=null;
	public Integer pc704=null;
	public Integer estado_rec_ci=null;
	public String f_rec_ci=null;
	public Integer estado_rec_cs=null;
	public String f_rec_cs=null;
	
	
	public String qh240a=null;
	public Integer qh240=null;
	public Integer qh241=null;
	public Integer qh242=null;	
	
	public String qh243_ref=null;
	
	public Integer qh243_ds1=null;
	public Integer qh243_d1=null;
	public Integer qh243_m1=null;
	public Integer qh243_a1=null;
	public String qh243_l1=null;	
	public Integer qh244_i1=null;
	public Integer qh244_v1=null;
	public String qh244_vo1=null;
	
	public Integer qh243_ds2=null;
	public Integer qh243_d2=null;
	public Integer qh243_m2=null;
	public Integer qh243_a2=null;
	public String qh243_l2=null;
	public Integer qh244_i2=null;
	public Integer qh244_v2=null;
	public String qh244_vo2=null;
	
	public Integer qh243_ds3=null;
	public Integer qh243_d3=null;
	public Integer qh243_m3=null;
	public Integer qh243_a3=null;
	public String qh243_l3=null;
	public Integer qh244_i3=null;
	public Integer qh244_v3=null;
	public String qh244_vo3=null;
	
	public Integer qh243_ds4=null;
	public Integer qh243_d4=null;
	public Integer qh243_m4=null;
	public Integer qh243_a4=null;
	public String qh243_l4=null;
	public Integer qh244_i4=null;
	public Integer qh244_v4=null;
	public String qh244_vo4=null;
	
	public Integer qh243_ds5=null;
	public Integer qh243_d5=null;
	public Integer qh243_m5=null;
	public Integer qh243_a5=null;
	public String qh243_l5=null;
	public Integer qh244_i5=null;
	public Integer qh244_v5=null;
	public String qh244_vo5=null;
	
	public Integer qh243_ds6=null;
	public Integer qh243_d6=null;
	public Integer qh243_m6=null;
	public Integer qh243_a6=null;
	public String qh243_l6=null;
	public Integer qh244_i6=null;
	public Integer qh244_v6=null;
	public String qh244_vo6=null;
	
	public Integer qh245_a=null;
	public Integer qh245_b=null;
	public Integer qh245_c=null;
	public Integer qh245_d=null;
	public Integer qh245_e=null;
	public Integer qh245_y=null;		
	public Integer qh246=null;
	public Integer qh247=null;
	public String qh247_o=null;
	public Integer qh248=null;
	public String qh248_o=null;
	public String qhvacunas_obs=null; 
	public String qh248a=null;

	
	
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qhs3_1a = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qhs3_1m=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado2=null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado3=null;
//	@FieldEntity(ignoreField=true, saveField=false)
//	public Integer violencia = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estadomef = null;

	//agregado para el listado de discapacidad individual
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer cuestionario_id = null;
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer qi212 = null;

	@FieldEntity(ignoreField=true, saveField=false)
	public Integer ordenado = null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public boolean todaslaspersonasmenoresa16 = false;
	public Seccion01() {
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

//	@Override
//	public String getNombre() {
//		String nombre = "";
//		if (qh02_1 != null) {
//			nombre = Util.getText(qh02_1)+ (qh02_11==null?"":EndesCalendario.DesencriptarData(qh02_11)+" ")+ 
//					EndesCalendario.DesencriptarData(Util.getText(qh02_2)) +" "+
//                    EndesCalendario.DesencriptarData(Util.getText(qh02_3));
//		}
//		return nombre;
//	}
	
	public Integer getEdade() {
		Integer edad = 0;
		if (qh07 != null) {
			edad = qh07;
		}
		return edad;
	}
	
	@Override
	public String getNombre() {
		String nombre = "";
		if (qh02_1 != null) {
			nombre = Util.getText(qh02_1)+ " " +(Util.getText(qh02_2)) +" "+ (Util.getText(qh02_3));
		}
		return nombre;
	}
	
	public String getNombre2() {
		String nombre = "";
		if (qh02_1 != null) {
			nombre = Util.getText(qh02_1) + " " + Util.getText(qh02_2);
		}
		return nombre;
	}

	public String getParentesco() {
		int op = qh03 != null ? Util.getInt(qh03) : -1;
		int sexo=qh06 !=null? Util.getInt(qh06) : -1;
		
		String Retorna = "";
		switch (op) {
		
		case 1: Retorna = "Jefe"; break;
		case 2: switch (sexo) {
				case 1: Retorna = "Esposo"; break;
				case 2: Retorna = "Esposa"; break;		
				default: Retorna = "Esposo(a)";
				} return Retorna;

		case 3: switch (sexo) {
				case 1: Retorna = "Hijo"; break;
				case 2: Retorna = "Hija"; break;		
				default: Retorna = "Hijo(a)";
				} return Retorna;
			
		case 4: switch (sexo) {
				case 1: Retorna = "Yerno"; break;
				case 2: Retorna = "Nuera"; break;		
				default: Retorna = "Yerno/Nuera";
				} return Retorna;
				
		case 5: switch (sexo) {
				case 1: Retorna = "Nieto"; break;
				case 2: Retorna = "Nieta"; break;		
				default: Retorna = "Nieto(a)";
				} return Retorna;	
		
		case 6:	switch (sexo) {
				case 1: Retorna = "Padre"; break;
				case 2: Retorna = "Madre"; break;		
				default: Retorna = "Padres";
				} return Retorna; 
				
		case 7: switch (sexo) {
				case 1: Retorna = "Suegro"; break;
				case 2: Retorna = "Suegra"; break;		
				default: Retorna = "Suegro(a)";
				} return Retorna;		

		case 8: switch (sexo) {
				case 1: Retorna = "Hermano"; break;
				case 2: Retorna = "Hermana"; break;		
				default: Retorna = "Hermano(a)";
				} return Retorna;		
	
		case 9:	Retorna = "Otro Familiar"; break;
		case 10:switch (sexo) {
				case 1: Retorna = "Hijo Adoptado"; break;
				case 2: Retorna = "Hija Adoptada"; break;		
				default: Retorna = "Hijo Adoptado(a)";
				} return Retorna;		
	
		case 11:Retorna = "Sin Parentesco";	break;
		case 12:switch (sexo) {
				case 1: Retorna = "Empleado Domestico"; break;
				case 2: Retorna = "Empleada Domestica"; break;		
				default: Retorna = "Empleado Domestico(a)";
				} return Retorna;		
		
		default:Retorna = "";
		}
		return Retorna;

	}

	public String getViveAqui() {
		int op = qh04 != null ? Util.getInt(qh04) : -1;
		String retorna = "";
		switch (op) {
		case 1:
			retorna = "Si";
			break;
		case 2:
			retorna = "No";
			break;
		default:
			retorna = "";
		}
		return retorna;
	}

	public String getDurmioAqui() {
		int op = qh05 != null ? Util.getInt(qh05) : -1;
		String retorna = "";
		switch (op) {
		case 1:
			retorna = "Si";
			break;
		case 2:
			retorna = "No";
			break;
		default:
			retorna = "";
		}
		return retorna;

	}

	public String getSexo() {
		int op = qh06 != null ? Util.getInt(qh06) : -1;
		String retorna = "";

		switch (op) {
		case 1:
			retorna = "Hombre";
			break;
		case 2:
			retorna = "Mujer";
			break;
		default:
			retorna = "";
		}
		return retorna;
	}

	public String getFecha() {
		if (qh7dd != null && qh7mm != null) {
			return qh7dd + "/" + qh7mm;
		} else {
			return "";
		}
	}
	public String getFecha2() {
		String fecha="";
		if(qh06==1){
			if (qh7dd != null && qh7mm != null) {
				fecha+= qh7dd + "/" + qh7mm;
			} else {
				    fecha+="";
			}
		}else{
			  if(qh06==2) fecha+="";	
		}
	    
		return fecha;
	
	}
	public String getSegurodeSalud(){
		String retornar_a="";
		retornar_a=qh11_a==1? " A. Essalud / Antes IPSS ":"";
		retornar_a=qh11_b==1?retornar_a+"\n B. Fuerzas Armadas o Policiales":retornar_a;
		retornar_a=qh11_c==1?retornar_a+"\n C. Seguro Integral de Salud(SIS)":retornar_a;
		retornar_a=qh11_d==1?retornar_a+"\n D. Entidad Prestadora de Salud":retornar_a;
		retornar_a=qh11_e==1?retornar_a+"\n E. Seguro Privado de Salud":retornar_a;
		retornar_a=qh11_y==1?" Y. NO SABE":retornar_a;
		retornar_a=qh11_z==1?" Z. NO ESTA AFILIADO":retornar_a;
		return retornar_a;
	}
	public Integer getConvertqh7(Integer qh7) {
		Integer valorReturn=0;
		switch (qh7){
		case 1: valorReturn=97; break;
		default: valorReturn=qh7;
		}
		return valorReturn;
	}
	public Integer setConvertqh7(Integer qh7) {
		Integer valorReturn=0;
		switch (qh7){
		case 97: valorReturn=1; break;	
		default: valorReturn=qh7;
		}
		return valorReturn;
	}
	public Integer getConvertqh12(Integer qh12) {
		Integer valorReturn=0;
		switch (qh12){
		case 3: valorReturn=8; break;
		default: valorReturn=qh12;
		}
		return valorReturn;
	}
	public Integer setConvertqh12(Integer qh12) {
		Integer valorReturn=0;
		switch (qh12){
		case 8: valorReturn=3; break;
		default: valorReturn=qh12;
		}
		return valorReturn;
	}
	public Integer getConvertqh13(Integer qh13) {
		Integer valorReturn=0;
		switch (qh13){
		case 9: valorReturn=96; break;
		case 10: valorReturn=98;break;
		default: valorReturn=qh13;
		}
		return valorReturn;
	}
	public Integer setConvertqh13(Integer qh13) {
		Integer valorReturn=0;
		switch (qh13){
		case 96: valorReturn=9; break;
		case 98:valorReturn=10;break;
		default: valorReturn=qh13;
		}
		return valorReturn;
	}
	public Integer getConvertqh15n(Integer qh15n) {
		Integer valorReturn=0;
		switch (qh15n){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		case 7: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvertqh15n(Integer qh15n) {
		Integer valorReturn=0;
		switch (qh15n){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 8: valorReturn=7; break;	
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer getConvertqh15_y(Integer qh15y) {
		Integer valorReturn=0;
		switch (qh15y){
		case 1: valorReturn=0; break;
		case 2: valorReturn=1; break;
		case 3: valorReturn=2; break;
		case 4: valorReturn=3; break;
		case 5: valorReturn=4; break;
		case 6: valorReturn=5; break;
		case 7: valorReturn=6; break;
		case 8: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	public Integer setConvertqh15_y(Integer qh15y) {
		Integer valorReturn=0;
		switch (qh15y){
		case 0: valorReturn=1; break;
		case 1: valorReturn=2; break;
		case 2: valorReturn=3; break;
		case 3: valorReturn=4; break;
		case 4: valorReturn=5; break;
		case 5: valorReturn=6; break;
		case 6: valorReturn=7; break;	
		case 8: valorReturn=8; break;
		default: valorReturn=-1;
		}
		return valorReturn;
	}
	
	public Integer getConvertqh15_g(Integer qh15g) {
		Integer valorReturn=0;
		switch (qh15g){
		case 7: valorReturn=8; break;
		default: valorReturn=qh15g;
		}
		return valorReturn;
	}
	public Integer setConvertqh15_g(Integer qh15g) {
		Integer valorReturn=0;
		switch (qh15g){
		case 8: valorReturn=7; break;
		default: valorReturn=qh15g;
		}
		return valorReturn;
	}
	public String getPregunta14(){
		return qh14==1?"Si":"No";
	}
	public String getPregunta15(){
		String mensaje="";
		if(qh15n!=null){
			switch(qh15n) {
			case 0:mensaje="0. Inicial/Pre-escolar";break;
			case 1:mensaje="1. Primaria";break;
			case 2:mensaje="2. Secundaria";break;
			case 3:mensaje="3. Superior no Universitaria";break;
			case 4:mensaje="4. Superior Universitaria"; break;
			case 5: mensaje="5. Postgrado";break;
			case 8: mensaje="8. No sabe";break;
			}
		}
		String anio="";
		if(qh15y!=null){
			anio="A. ";
			switch (qh15y) {
			case 0:anio=anio+"0";break;
			case 1:anio=anio+"1";break;
			case 2:anio=anio+"2";break;
			case 3:anio=anio+"3";break;
			case 4:anio=anio+"4";break;
			case 5:anio=anio+"5";break;
			case 6:anio=anio+"6";break;
			case 8:anio=anio+"No sabe";break;
			}
		}
		String grado="";
		if(qh15g!=null){
			grado="G. ";
			switch (qh15g) {
			case 1:grado=grado+"1";break;
			case 2:grado=grado+"2";break;
			case 3:grado=grado+"3";break;
			case 4:grado=grado+"4";break;
			case 5:grado=grado+"5";break;
			case 6:grado=grado+"6";break;
			case 8: grado=grado+"No sabe";break;
			}
		}
		return mensaje+"\n"+anio+"\n"+grado;
		
	}
	
	public Integer getConvertqh4a8(Integer qh242) {
		Integer valorReturn=0;
		switch (qh242){
		case 4: valorReturn=8; break;
		default: valorReturn=qh242;
		}
		return valorReturn;
	}
	public Integer setConvertqh8a4(Integer qh242) {
		Integer valorReturn=0;
		switch (qh242){
		case 8: valorReturn=4; break;
		default: valorReturn=qh242;
		}
		return valorReturn;
	}
	
	public Integer getConvertqh6a8(Integer qh246) {
		Integer valorReturn=0;
		switch (qh246){
		case 6: valorReturn=8; break;
		default: valorReturn=qh246;
		}
		return valorReturn;
	}
	public Integer setConvertqh8a6(Integer qh246) {
		Integer valorReturn=0;
		switch (qh246){
		case 8: valorReturn=6; break;
		default: valorReturn=qh246;
		}
		return valorReturn;
	}
	public Integer getConvertqh10a98(Integer qh247) {
		Integer valorReturn=0;
		switch (qh247){
		case 10: valorReturn=98; break;
		default: valorReturn=qh247;
		}
		return valorReturn;
	}
	public Integer setConvertqh98a10(Integer qh247) {
		Integer valorReturn=0;
		switch (qh247){
		case 98: valorReturn=10; break;
		default: valorReturn=qh247;
		}
		return valorReturn;
	}
	
	
	public String getViolencia(){
    	if(App.getInstance().getQhviolen()!=null){
    		if (App.getInstance().getQhviolen()==persona_id) {
    			return "SI";
    		} else {
    			return  "NO";
    		}
    	}else{
    		return  "";
    	}
    }
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public String getSpinnerLabel() {
		qh02_2=qh02_2==null?"":qh02_2;
		return persona_id==null? qh02_1:"("+persona_id+") "+qh02_1+"-"+qh02_2;
	}
	
	public String getQH248() {
		int op = qh248 != null ? Util.getInt(qh248) : -1;
		String retorna = "";
		switch (op) {
			case 1:	retorna = "1. COMPLETO";	break;
			case 2:	retorna = "2. INFORMANTE/RESPONSABLE AUSENTE";	break;
			case 3:	retorna = "3. INFORMANTE/RESPONSABLE RECHAZ�";	break;
			case 4:	retorna = "4. OTRA";	break;
			default:retorna = "";
		}
		return retorna;
	}
		

}
