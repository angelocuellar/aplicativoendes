package gob.inei.endes2024.model;

import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.IDetailEntityComponent;

import java.io.Serializable;

public class CISECCION_09 extends Entity implements Serializable,IDetailEntityComponent {
	private static final long serialVersionUID = 1L; 
	public Integer hogar_id=null;
	public Integer persona_id=null;
	public Integer qinro_orden = null;
	public String qi904 = null;
	public Integer qi905= null;
	public Integer qi906 = null;
	public Integer qi907 = null;
	public Integer qi908 = null;
	public Integer qi909 = null;
	public Integer qi910 = null;
	public Integer qi911 = null;
	public Integer qi912 = null;
	public Integer qi913 = null;
	public Integer qi914 = null;
	public Integer qi915 = null;
	public Integer qi916 = null;
	public Integer qi917 = null;
	public String qi900_obs = null;
	
	@FieldEntity(ignoreField=true, saveField=false)
	public Integer estado=null;
	
	@FieldEntity(ignoreField=true, saveField=false)
   	public Integer edad = null;
	
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	public Integer setConvertconcuatrovariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 4:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertconcuatrovariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=4;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public Integer setConvertcontresvariables(Integer valor){
		Integer retonar=valor;
		switch (valor) {
		case 3:retonar=8;
			break;
		default:
			retonar=valor;
			break;
		}
		return retonar;
	}
	public Integer getConvertcontresvariables(Integer valor){
		Integer retornar=valor;
		switch (valor) {
		case 8:retornar=3;
			break;
		default:
			retornar=valor;
			break;
		}
		return retornar;
	}
	public String getEstaVivo(){
		String retorna="";
		if(qi906!=null){
			switch (qi906) {
			case 1:retorna="Si";				
				break;
			case 2:retorna="No";break;
			default:retorna="NS";
				break;
			}
		}
		return retorna;
	}
	
	public String getSexo(){
		String retorna="";
		if(qi905!=null){
			switch (qi905) {
			case 1:retorna="HOMBRE";				
				break;
			case 2:retorna="MUJER";break;
			default:retorna="";
				break;
			}
		}
		return retorna;
	}
}
