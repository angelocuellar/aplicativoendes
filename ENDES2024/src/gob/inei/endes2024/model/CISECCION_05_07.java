package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 

import java.io.Serializable; 
import java.math.BigDecimal; 
public class CISECCION_05_07 extends Entity implements Serializable{ 
    private static final long serialVersionUID = 1L; 
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer qi500a=null; 
    public Integer qi500b=null; 
    public Integer qi500c=null; 
    public Integer qi500d=null; 
    public Integer qi501=null; 
    public Integer qi502=null; 
    public Integer qi505=null; 
    public Integer qi506=null; 
    public Integer qi507=null; 
    public Integer qi508=null; 
    public Integer qi508a=null; 
    public String qi508ax=null; 
    public String qi509m=null; 
    public Integer qi509y=null;
    public Integer qimes_cal = null;
    public Integer qianio_cal = null;
    public Integer qiindice=null;
    public Integer qi510=null; 
    public Integer qi512=null; 
    public Integer qi512ab=null; 
    public Integer qi512b=null; 
    public Integer qi512ba=null; 
    public Integer qi512bb=null; 
    public Integer qi512bc=null; 
    public Integer qi512c=null; 
    public String qi512cx=null;
    public Integer qi512d=null;
    public String qi512d_o=null;
    public Integer qi513u=null; 
    public Integer qi513n=null; 
    public Integer qi513bu=null; 
    public Integer qi513bn=null; 
    public Integer qi514=null; 
    public Integer qi514a=null; 
    public Integer qi515=null; 
    public String qi515x=null; 
    public Integer qi516u=null; 
    public Integer qi516n=null; 
    public Integer qi516b=null; 
    public Integer qi516c=null; 
    public Integer qi516d=null; 
    public Integer qi516e=null; 
    public Integer qi516f=null; 
    public Integer qi517=null; 
    public Integer qi521=null; 
    public Integer qi521a=null; 
    public String qi521a_o=null;
    public Integer qi522=null; 
    public String qi523=null; 
    public Integer qi523_a=null; 
    public Integer qi523_b=null; 
    public Integer qi523_c=null; 
    public Integer qi523_d=null; 
    public Integer qi523_e=null; 
    public Integer qi523_f=null; 
    public Integer qi523_g=null; 
    public Integer qi523_h=null; 
    public Integer qi523_i=null; 
    public String qi523_ix=null; 
    public Integer qi523_j=null; 
    public Integer qi523_k=null; 
    public Integer qi523_l=null; 
    public Integer qi523_m=null; 
    public String qi523_mx=null; 
    public Integer qi523_n=null; 
    public Integer qi523_o=null; 
    public Integer qi523_p=null; 
    public Integer qi523_q=null; 
    public Integer qi523_r=null; 
    public Integer qi523_x=null; 
    public String qi523_xi=null; 
    public Integer qi524=null; 
    public Integer qi602=null; 
    public Integer qi603u=null; 
    public Integer qi603n=null; 
    public String qi603nx=null; 
    public Integer qi607_a=null; 
    public Integer qi607_b=null; 
    public Integer qi607_c=null; 
    public Integer qi607_d=null; 
    public Integer qi607_e=null; 
    public Integer qi607_f=null; 
    public Integer qi607_g=null; 
    public Integer qi607_h=null; 
    public Integer qi607_i=null; 
    public Integer qi607_j=null; 
    public Integer qi607_k=null; 
    public Integer qi607_l=null; 
    public Integer qi607_m=null; 
    public Integer qi607_n=null; 
    public Integer qi607_o=null; 
    public Integer qi607_p=null; 
    public Integer qi607_q=null; 
    public Integer qi607_r=null; 
    public Integer qi607_s=null; 
    public Integer qi607_t=null; 
    public Integer qi607_u=null; 
    public Integer qi607_x=null; 
    public String qi607_x_i=null; 
    public Integer qi607_z=null; 
    public Integer qi608=null; 
    public Integer qi610=null; 
    public Integer qi611=null; 
    public String qi611x=null; 
    public Integer qi612=null; 
    public String qi612x=null; 
    public Integer qi614=null; 
    public String qi614x=null; 
    public Integer qi615a=null; 
    public Integer qi615b=null; 
    public Integer qi615c=null; 
    public Integer qi616aa=null; 
    public Integer qi616ab=null; 
    public Integer qi616ac=null;
    public Integer qi616ad=null;
    public Integer qi617=null; 
    public Integer qi618_a=null; 
    public Integer qi618_b=null; 
    public Integer qi618_c=null; 
    public Integer qi618_d=null; 
    public Integer qi618_e=null; 
    public Integer qi618_f=null; 
    public Integer qi618_g=null; 
    public Integer qi618_h=null; 
    public Integer qi618_i=null; 
    public Integer qi618_j=null; 
    public Integer qi618_k=null; 
    public Integer qi618_l=null; 
    public Integer qi618_m=null; 
    public Integer qi618_x=null; 
    public String qi618_x_i=null; 
    public Integer qi619b=null; 
    public Integer qi620=null; 
    public String qi620x=null; 
    public Integer qi621=null; 
    public Integer qi621a=null; 
    public Integer qi623=null; 
    public Integer qi624a=null; 
    public Integer qi624b=null; 
    public Integer qi624c=null; 
    public Integer qi624d=null; 
    public Integer qi624e=null; 
    public Integer qi624f=null; 
    public Integer qi702=null; 
    public Integer qi703=null; 
    public Integer qi704n=null; 
    public Integer qi704y=null; 
    public Integer qi704g=null; 
    public String qi706a=null; 
    public String qi706_cod=null; 
    public Integer qi707=null; 
    public String qi707_cons=null;
    public Integer qi708=null; 
    public Integer qi708a=null; 
    public Integer qi709=null; 
    public String qi710a=null; 
    public String qi710_cod=null; 
    public Integer qi711=null;
    public Integer qi712=null; 
    public Integer qi713=null; 
    public Integer qi714=null; 
    public Integer qi715=null; 
    public Integer qi716=null; 
    public Integer qi717=null; 
    public Integer qi718a=null; 
    public Integer qi718aa=null; 
    public Integer qi719a=null; 
    public Integer qi719b=null; 
    public Integer qi719c=null; 
    public Integer qi719d=null; 
    public Integer qi719e=null; 
    public Integer qi720a=null; 
    public String qi720ax=null; 
    public Integer qi721a=null; 
    public Integer qi721b=null; 
    public Integer qi721c=null; 
    public Integer qi721d=null; 
    public Integer qi721e=null; 
    public String qiobs_s5=null; 
    public String qiobs512=null; 
    
//    public Integer qianio_cal=null;
//    public Integer qimes_cal=null;
//    public Integer qiidentificador=null;
    
    @FieldEntity(ignoreField=true, saveField=false)
  	public Integer qi217 = null;
    @FieldEntity(ignoreField=true, saveField=false)
   	public Integer qi218 = null;
     
    @FieldEntity(ignoreField=true, saveField=false)
   	public boolean filtro720 = false;
    
    public CISECCION_05_07() {}
    
    public Integer getConvertQi610(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi610(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi619B(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi512d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 3: valorReturn=8; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi512d(Integer element) {
		Integer valorReturn=0;
		switch (element){
			case 8: valorReturn=3; break;
			default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer setConvertQi619B(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi620(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=6; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi620(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 6: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi621(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi621(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi623(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 4: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi623(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=4; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi624A(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624A(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi624B(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624B(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi624C(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624C(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer getConvertQi624D(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624D(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi624E(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624E(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	public Integer getConvertQi624F(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 3: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi624F(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=3; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	
	
	public Integer getConvertQi512(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 8: valorReturn=96; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
	public Integer setConvertQi512(Integer element) {
		Integer valorReturn=0;
		switch (element){
		case 96: valorReturn=8; break;
		default: valorReturn=element;
		}
		return valorReturn;
	}
}