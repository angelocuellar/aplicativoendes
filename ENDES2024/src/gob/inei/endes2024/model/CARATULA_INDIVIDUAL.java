package gob.inei.endes2024.model;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

import java.io.Serializable;
public class CARATULA_INDIVIDUAL extends Entity implements Serializable, IDetailEntityComponent,	Coberturable{
    private static final long serialVersionUID = 1L;
    public Integer hogar_id=null;
    public Integer persona_id=null;
    public Integer nro_visita=null;
    public String qivdia=null;
    public String qivmes=null;
    public Integer qivanio=null;
    public String qivhora=null;
    public String qivmin=null;
    public String qivhora_fin = null;
    public String qivmin_fin =null;
    public String qiventrev=null;
    public Integer qivresul=null;
    public String qivresul_o=null;
    public String qiprox_dia=null;
    public String qiprox_mes=null;
    public String qiprox_anio=null;
    public String qiprox_hora=null;
    public String qiprox_minuto=null;
    /*public String qires_fin_d=null;
    public String qirs_fin_m=null;
    public Integer qires_fin_y=null;
    public Integer qiresult=null;
    public String qiresult_o=null;
    public Integer t_ninos=null;
    public Integer t_ninos_c=null;*/
    public CARATULA_INDIVIDUAL() {}
    
    public String getFechainicio() {
		String fecha = "",dia="",mes="";
		
		if (qivdia != null && qivmes != null) {
			if(qivdia.length()<2) dia="0"+qivdia.toString(); else  dia= qivdia.toString();
			if(qivmes.length()<2) mes="0"+qivmes.toString(); else mes= qivmes.toString();
			fecha = dia + "/" + mes;
		}
		return fecha;
	}
    public String getHoraInicio() {
		String Horainicio = "";
		if (qivhora != null && qivmin != null) {
			Horainicio = (qivhora.toString().length() == 1 ? "0"+ qivhora.toString() : qivhora.toString())
					+ ":"+ (qivmin.toString().length() == 1 ? "0"+ qivmin.toString() : qivmin.toString());
		}
		return Horainicio;
	}
    public String getHoraFin() {
    	String horaFinal="";
		if(qivhora_fin!=null && qivmin_fin!=null){
			horaFinal = (qivhora_fin.toString().length()==1?"0"+qivhora_fin.toString():qivhora_fin.toString())+":"+(qivmin_fin.toString().length()==1?"0"+qivmin_fin.toString():qivmin_fin.toString());
		}
		return horaFinal;
	}
    public String getFechaProxima() {
		String fechaproxima = "",dia="",mes="";

		if (qiprox_mes != null && qiprox_dia != null) {
			if(qiprox_dia.length()<2) dia="0"+qiprox_dia.toString(); else  dia= qiprox_dia.toString();
			if(qiprox_mes.length()<2) mes="0"+qiprox_mes.toString(); else mes= qiprox_mes.toString();
			fechaproxima = dia + "/" + mes;
		}
		return fechaproxima;
	}
    public String getHoraProxima() {
		String proximahora = "";
		if (qiprox_hora != null && qiprox_minuto != null) {
			proximahora = (qiprox_hora.toString().length() == 1 ? "0"
					+ qiprox_hora.toString() : qiprox_hora.toString())
					+ ":"
					+ (qiprox_minuto.toString().length() == 1 ? "0"
							+ qiprox_minuto.toString() : qiprox_minuto.toString());
		}
		return proximahora;
	}
    public String getResultado() {
    	
    	String resultado="";
		int op = qivresul!=null?Util.getInt(qivresul):-1;
		switch (op) {
			case 1:resultado= "Completa";break;
			case 2:resultado="Ausente";break;
			case 3:resultado="Aplazada";break;
			case 4:resultado="Rechazada";break;
			case 5:resultado="Incompleta";break;
			case 6:resultado="Discapacitada";break;
			case 7:resultado="Otro";break;
		//	case 8:resultado="Vivienda No Encontrada";break;
		//	case 9:resultado="Otro";break;
			default:resultado="";break;
		}
		return resultado;
	}
    
    public Integer getConvertResult(Integer qsvresul) {
		Integer valorReturn = qsvresul;
		switch (qsvresul) {
		case 7: valorReturn = 9; break;	
//		default:valorReturn = qsvresul;
		}
		return valorReturn;
     }
    public Integer setConvertResult(Integer qsvresul) {
		Integer valorReturn=0;
		switch(qsvresul){
		case 9: valorReturn=7; break;
		default: valorReturn=qsvresul;
		}
		return valorReturn;
    }
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
}