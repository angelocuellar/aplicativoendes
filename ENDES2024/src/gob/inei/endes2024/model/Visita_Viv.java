package gob.inei.endes2024.model;

import gob.inei.dnce.components.Entity;
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.util.Util;

public class Visita_Viv extends Entity implements IDetailEntityComponent,
		Coberturable {
	private static final long serialVersionUID = 1L;
	public Integer nro_visita = null;
	public String qvvdia_ini = null;
	public String qvvmes_ini = null;
	public Integer qvvanio_ini = null;
	public Integer qvvresul = null;
	public Integer qvvresul_r = null;
	public String  qvvresul_o = null;
	public String qvhora_ini = null;
	public String qvmin_ini = null;
	public String qvvdiap = null;
	public String qvvmesp = null;
	public String qvvhorap = null;
	public String qvvminup = null;
	public String qvvmin_fin=null;   
	public String qvvhora_fin =null;
    public String  qvgps_long=null; 
    public String  qvgps_lat=null;
    public String  qvgps_alt=null; 
    
    public Integer qvcondicion = null;
    public String  qvcondicion_f = null;
    public Integer qvcondicion_fn = null;
    public Integer qvinformante = null;
    public String  qvinformante_o = null;
    
	public Visita_Viv() {
	}

	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub

	}

	public String getFechainicio() {
		String fecha = "";
		String mes="";
		String dia="";
		if (qvvdia_ini != null && qvvmes_ini != null) {
			mes= qvvmes_ini.toString().length()>1?qvvmes_ini.toString():"0"+qvvmes_ini.toString();
			dia=qvvdia_ini.toString().length()>1?qvvdia_ini.toString():"0"+qvvdia_ini.toString();
			fecha = dia + "/" + mes;
		}
		return fecha;
	}

	public String getHoraInicio() {
		String Horainicio = "";
		if (qvhora_ini != null && qvmin_ini != null) {
			Horainicio = (qvhora_ini.toString().length() == 1 ? "0"
					+ qvhora_ini.toString() : qvhora_ini.toString())
					+ ":"
					+ (qvmin_ini.toString().length() == 1 ? "0"
							+ qvmin_ini.toString() : qvmin_ini.toString());
		}
		return Horainicio;
	}

	public String getFechaProxima() {
		String fechaproxima = "";
		String mes="";
		String dia="";
		if (qvvmesp != null && qvvdiap != null) {
			mes=qvvmesp.toString().length()>1?qvvmesp.toString():"0"+qvvmesp.toString();
			dia = qvvdiap.toString().length()>1?qvvdiap.toString():"0"+qvvdiap.toString();
			fechaproxima = dia + "/" + mes;
		}
		return fechaproxima;
	}

	public String getHoraProxima() {
		String proximahora = "";
		if (qvvhorap != null && qvvminup != null) {
			proximahora = (qvvhorap.toString().length() == 1 ? "0"
					+ qvvhorap.toString() : qvvhorap.toString())
					+ ":"
					+ (qvvminup.toString().length() == 1 ? "0"
							+ qvvminup.toString() : qvvminup.toString());
		}
		return proximahora;
	}

	public String getResultado()
	{String resultado="";
	
	int op = qvvresul!=null?Util.getInt(qvvresul):-1;
	switch (op) {
	case  1:resultado="Vivienda completa";break;
	case  2:resultado="Vivienda ocupada pero con entrevistado competente ausente";break;
	case  3:resultado="Vivienda ausente";break;
	case  4:resultado="Vivienda aplazada ";break;
	case  5:resultado="Vivienda rechazada";break;
	case  6:resultado="Vivienda desocupada o no es vivienda ";break;
	case  7:resultado="Vivienda destruida";break;
	case  8:resultado="Vivienda no existe";break;
	case  9:resultado="Otro";break;
	case 10:resultado="Vivienda incompleta";break;
	default:resultado="";break;
		}
	return resultado;
	}
	
	public String getResultado_r()
	{String resultado_r="";
	
	int op = qvvresul_r!=null?Util.getInt(qvvresul_r):-1;
	switch (op) {
	case 1:resultado_r="No desean la entrevista";break;
	case 2:resultado_r="Ya fue entrevistada por la ENDES";break;
	case 3:resultado_r="Ya fue entrevistada por otra encuesta del INEI";break;
	default:resultado_r="";break;
		}
	return resultado_r;
	}
	
	public String getHoraFin()
	{String horaFinal="";
		if(qvvhora_fin!=null && qvvmin_fin!=null)
		{
			horaFinal = (qvvhora_fin.toString().length()==1?"0"+qvvhora_fin.toString():qvvhora_fin.toString())+":"+(qvvmin_fin.toString().length()==1?"0"+qvvmin_fin.toString():qvvmin_fin.toString());
		}
		return horaFinal;
	}
	
	public Integer setValue3a8(Integer v){
		Integer valorReturn=0;
		switch(v){
			case 3: valorReturn=8; break;	
			default: valorReturn=v;
		}
		return valorReturn;
	}
	public Integer getValue8a3(Integer v){
		Integer valorReturn = 0;
		switch (v) {
			case 8: valorReturn = 3; break;
			default:valorReturn = v;
		}
		return valorReturn;
	}
	
//	public Integer setValue8a96(Integer v){
//		Integer valorReturn=0;
//		switch(v){
//			case 8: valorReturn=96; break;	
//			default: valorReturn=v;
//		}
//		return valorReturn;
//	}
//	public Integer getValue96a8(Integer v){
//		Integer valorReturn = 0;
//		switch (v) {
//			case 96: valorReturn = 8; break;
//			default:valorReturn = v;
//		}
//		return valorReturn;
//	}
//	
	public Integer setValue9a96(Integer v){
		Integer valorReturn=0;
		switch(v){
			case 9: valorReturn=96; break;	
			default: valorReturn=v;
		}
		return valorReturn;
	}
	public Integer getValue96a9(Integer v){
		Integer valorReturn = 0;
		switch (v) {
			case 96: valorReturn = 9; break;
			default:valorReturn = v;
		}
		return valorReturn;
	}
}
