package gob.inei.endes2024.model; 
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.Entity; 
import gob.inei.dnce.interfaces.Coberturable;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.endes2024.common.MyUtil;

import java.io.Serializable; 
import java.math.BigDecimal; 
public class Seccion04_05_Validar extends Entity implements Serializable,IDetailEntityComponent,
Coberturable{ 
    private static final long serialVersionUID = 1L; 
    
    public Integer hogar_id=null; 
    public Integer persona_id=null; 
    public Integer persona_id_orden=null;
    public Integer qh202=null;
    public String qh203d=null; 
    public String qh203m=null; 
    public Integer qh203y=null; 
    public BigDecimal qh204=null; 
    public BigDecimal qh205=null; 
    
    public Integer qh205a=null; 
    public Integer qh205b=null; 
    public BigDecimal qh205c=null;
    public Integer qh205c1=null;
    
    public Integer qh206=null; 
    public Integer qh207=null; 
    public Integer qhmeses=null;
    public String qh207_o=null;
    public String qh207a_d=null; 
    public String qh207a_m=null; 
    public Integer qh207_y=null; 
    public Integer qh208=null; 
    public Integer qh209=null; 
    public Integer qh210=null; 
    public BigDecimal qh211=null; 
    public Integer qh212=null; 
    public Integer qh212ah=null;
    public Integer qh212am=null;
    public String qh212b_d=null; 
    public String qh212b_m=null; 
    public Integer qh212b_y=null; 
    
    
    public Integer qh212c=null;
    public Integer qh212d=null;
    public Integer qh212e=null;
    public String qh212e_o=null;
    public Integer qh212f1=null;
    public BigDecimal qh212f=null;
    public Integer qh212gh=null;
    public Integer qh212gm=null;
    public String qh212h_d=null; 
    public String qh212h_m=null; 
    public Integer qh212h_y=null;
    public Integer qh212i=null;
    public Integer qh212j=null;
    public String qh212j_o=null;
    public BigDecimal qh212k=null;
    public Integer qh212l=null;
    
    public Integer qh213=null; 
    
    public String qh203a=null;
    public String qh207b=null;
    public String qh209a=null;
    public String qh213a=null;              
  
    
    public Seccion04_05_Validar() {}
    
   
	@Override
	public boolean isTitle() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void cleanEntity() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return null;
	} 
	
	
    
} 
