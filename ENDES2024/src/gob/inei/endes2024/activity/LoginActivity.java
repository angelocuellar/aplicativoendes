package gob.inei.endes2024.activity;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.controller.Inicializacion;
import gob.inei.endes2024.service.Service;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
	private EditText txtUSUARIO, txtCLAVE;
	private TextView tvVERSION;
	private UsuarioService service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		txtUSUARIO = (EditText) findViewById(R.id.txtUSUARIO);
		txtCLAVE = (EditText) findViewById(R.id.txtCLAVE);
		tvVERSION = (TextView) findViewById(R.id.tvVERSION);
		String version = getResources().getString(R.string.app_version);
		try {
			version +=  " " + App.getInstance().getVersion(this);
		} catch (NotFoundException e) {
			
		} catch (NameNotFoundException e) {
				
		}
		tvVERSION.setText(version);
		// TODO QUITAR
//		txtUSUARIO.setText("73645269");
//		txtCLAVE.setText("73645269");
//		txtUSUARIO.setText("42317082");
//		txtCLAVE.setText("42317082");
//		txtUSUARIO.setText("46065926");
//		txtCLAVE.setText("46065926");
//		txtUSUARIO.setText("46453046");
//		txtCLAVE.setText("46453046");

	}

	@Override
	public void onBackPressed() {
		super.onDestroy();
		finish();
		return;
	}

	private UsuarioService getService() {
		if (service == null) {
			service = UsuarioService.getInstance(getApplicationContext());
		}
		return service;
	}

	public void cerrar(View v) {
		try {
			File f = new File(App.RUTA_BASE + "bd/");
			if (!f.exists()) {
				f.mkdirs();
			}
			File dst = new File(App.RUTA_BASE + "bd/" + Service.DATABASE_NAME);
			try {
				if (dst.exists()) {
					dst.delete();
				}
				dst.createNewFile();
			} catch (IOException e) {
			
			}
			String rutaBD = getApplicationContext().getDatabasePath(
					Service.DATABASE_NAME).getAbsolutePath();
			
			Util.copy(new File(rutaBD), dst);

		} catch (IOException e) {
			
		}
		android.os.Process.killProcess(android.os.Process.myPid());
		finish();
	}

	public void validarLogin(View v) {
		String ruta = App.RUTA_BASE;
		File directorio = new File(ruta+"/config");
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		directorio = new File(ruta+"/backups");
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		directorio = new File(ruta+"/bd");
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		directorio = new File(ruta+"/practicas");
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		v.setEnabled(false);
		if (valida()) {
			Inicializacion i = new Inicializacion(LoginActivity.this);
			i.setUsuario(txtUSUARIO.getText().toString().trim());
			i.setClave(txtCLAVE.getText().toString().trim());
			i.execute();
		}
		v.setEnabled(true);
	}

	public boolean valida() {
		if (Util.esVacio(txtUSUARIO)) {
			ToastMessage.msgBox(this, "El usuario no puede estar vacio",
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
			return false;
		}
		if (Util.esVacio(txtUSUARIO)) {
			ToastMessage.msgBox(this, "La clave no puede estar vacio",
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}
}
