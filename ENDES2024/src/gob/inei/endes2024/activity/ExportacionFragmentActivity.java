package gob.inei.endes2024.activity;

import gob.inei.dnce.adapter.DepthPageTransformer;
import gob.inei.dnce.adapter.MyFragmentPagerAdapter;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.FragmentViewPager;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.adapter.MyDrawerAdapter;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.controller.Importacion;
import gob.inei.endes2024.fragment.ExportacionFragment;
import gob.inei.endes2024.listener.MyNavigationClickListener;
import gob.inei.endes2024.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import paul.arian.fileselector.FileSelectionActivity;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;


@SuppressLint("NewApi") 
public class ExportacionFragmentActivity extends MasterActivity implements
             ActionBar.TabListener, Respondible, Observer {

       private enum PROCCES {
             MARCO, DATA
       }

       private static int REQUEST_CODE_PICK_MARCO = 1;
       private static int REQUEST_CODE_PICK_IMPORT = 2;
       private PROCCES action = null;
       /**
       * Fragment managing the behaviors, interactions and presentation of the
       * navigation drawer.
       */
       private DrawerLayout drawerLayout;
       private ListView drawerList;
       private ActionBarDrawerToggle drawerToggle;
       private Vector<String> opcionesMenu = new Vector<String>();
       /**
       * Used to store the last screen title. For use in
       * {@link #restoreActionBar()}.
       */
       // public static int prevPage = -1;
       // public static int currentPage = 0;
       private static String TAG = "CuestionarioRuralFragmentActivity";
       private static String TITULO = "";
       private boolean sec4TE, sec4TE1;

       SharedPreferences preferencias;
       SharedPreferences.Editor editor;

       private static String PREFERENCIAS = "preferencias";
       private CharSequence mTitle;
       // private CuestionarioService cuestionarioService;
       private SeccionCapitulo[] seccionesCargado;

       public static Context baseContext;
       public boolean band=false; 
       public boolean salto=false; 
       private List<FragmentForm> forms;

       @Override
       protected void onCreate(Bundle savedInstanceState) {
             super.onCreate(savedInstanceState);
             baseContext = getApplicationContext();
             TAG = this.getClass().toString();
             setContentView(R.layout.activity_principal);
             mTitle = getTitle();
             preferencias = this.getSharedPreferences(PREFERENCIAS,
                           Context.MODE_PRIVATE);
             editor = preferencias.edit();
             forms = createFragments();
             pageAdapter = new MyFragmentPagerAdapter(this, forms);
             viewPager = (FragmentViewPager) findViewById(R.id.pagerPrincipal);
             viewPager.setAdapter(pageAdapter);
             viewPager.setFragments(forms);
             final CuestionarioSimpleOnPageChangeListener viewPagerListener = new CuestionarioSimpleOnPageChangeListener();
//             viewPager.setPageTransformer(true, new DepthPageTransformer());
             viewPager.setOnPageChangeListener(viewPagerListener);
             // viewPager.post(new Runnable() {
             // @Override
             // public void run() {
             // viewPagerListener.onPageSelected(viewPager.getCurrentItem());
             // }
             // });
             for (String s : getResources().getStringArray(R.array.drawer_array)) {
                    opcionesMenu.add(s);
             }
             if (App.getInstance().getUsuario().cargo_id != 25) {
                    opcionesMenu.remove(opcionesMenu.size() - 1);
             }
             drawerLayout = (DrawerLayout) findViewById(R.id.drawer_principal_layout);
             drawerList = (ListView) findViewById(R.id.left_principal_drawer);
             drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                           GravityCompat.START);
             drawerList.setAdapter(new MyDrawerAdapter(opcionesMenu, this));
             navigationClickListener = new MyNavigationClickListener(this, 0,
                           drawerList, drawerLayout);
             drawerList.setOnItemClickListener(navigationClickListener);
             TITULO = getResources().getString(R.string.app_name);
             final ActionBar actionBar = getActionBar();
             drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                           R.drawable.ic_navigation_drawer,
                           R.string.navigation_drawer_open,
                           R.string.navigation_drawer_close) {

                    public void onDrawerClosed(View view) {
                           // getActionBar().setIcon(R.drawable.ic_launcher);
                           getActionBar().setTitle(TITULO);
                           ActivityCompat
                                        .invalidateOptionsMenu(ExportacionFragmentActivity.this);
                    }

                    public void onDrawerOpened(View drawerView) {
                           // getActionBar().setIcon(R.drawable.ic_launcher);
                           getActionBar().setTitle("Menu");
                           ActivityCompat
                                        .invalidateOptionsMenu(ExportacionFragmentActivity.this);
                    }
             };
             actionBar.setDisplayHomeAsUpEnabled(true);
             actionBar.setHomeButtonEnabled(true);
             drawerLayout.setDrawerListener(drawerToggle);

             if (savedInstanceState != null) {
                    actionBar.setSelectedNavigationItem(savedInstanceState.getInt(
                                  "tab", 1));
             }
       }

       public static final Integer MARCO = 0;


       private List<FragmentForm> createFragments() {
             List<FragmentForm> fragments = new ArrayList<FragmentForm>();

//             /* 00 */fragments.add(new ExportacionFragment.parent(this));
             fragments.add(new ExportacionFragment().parent(this));

             
             return fragments;
       }

       public void restoreActionBar() {
             ActionBar actionBar = getActionBar();
             actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
             actionBar.setDisplayShowTitleEnabled(true);
             actionBar.setTitle(mTitle);
       }

       @Override
       public boolean onPrepareOptionsMenu(Menu menu) {
             boolean menuAbierto = drawerLayout.isDrawerOpen(drawerList);
             // if (menuAbierto)
             // menu.findItem(R.id.menu_info).setVisible(false);
             // else
             // menu.findItem(R.id.menu_info).setVisible(true);
             return super.onPrepareOptionsMenu(menu);
       }

       @Override
       public boolean onCreateOptionsMenu(Menu menu) {
//             getMenuInflater().inflate(R.menu.principal, menu);
             // restoreActionBar();
             // ToastMessage.msgBox(this, "Grabo", ToastMessage.MESSAGE_INFO,
             // ToastMessage.DURATION_LONG);
             return super.onCreateOptionsMenu(menu);
       }

       @Override
       public boolean onOptionsItemSelected(MenuItem item) {
             // Handle action bar item clicks here. The action bar will
             // automatically handle clicks on the Home/Up button, so long
             // as you specify a parent activity in AndroidManifest.xml.
             if (drawerToggle.onOptionsItemSelected(item)) {
                    return true;
             }
             int id = item.getItemId();
             if (id == R.id.action_ir_marco) {
                    irA(MARCO);
             } 
                    
             return super.onOptionsItemSelected(item);
       }

       public enum OBSV {
             LOCAL, INICIAL, PRIMARIA, SECUNDARIA
       };

       private void abrirObservaciones(OBSV modulo) {
             FragmentManager fm = this.getSupportFragmentManager();
             // ObservacionesDialog dialog = ObservacionesDialog.newInstance(this,
             // modulo);
             // dialog.show(fm, "obsDialog");
       }

       private void grabarYContinuar() {
             this.grabarYContinuar(viewPager.getCurrentItem() + 1);
       }

       private void irA(int position) {
             nextFragment(position);
       }

       private void grabarYContinuar(int position) {
             int posicionActual = viewPager.getCurrentItem();
             boolean flag = ExportacionFragmentActivity.this.pageAdapter.getItem(
                           posicionActual).grabar();
             if (flag && posicionActual != MARCO) {
                    esArrastre = true;
                    if (posicionActual == viewPager.getCount() - 1) {
                           
                           ToastMessage.msgBox(this, "Registro Finalizado",
                                        ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
                           nextFragment(MARCO);
                    } else {
                           nextFragment(position);
                    }
                    return;
             } else {
                    esArrastre = false;
             }
       }

       public void setTitulo(String titulo) {
             TITULO = getResources().getString(R.string.app_name);
             if (titulo != null) {
                    if (!"".equals(titulo)) {
                           TITULO += " " + titulo;
                    }
             }
             getActionBar().setTitle(TITULO);
       }

       @Override
       public boolean onKeyDown(int keyCode, KeyEvent event) {
             if (keyCode == KeyEvent.KEYCODE_BACK) {
                    new AlertDialog.Builder(this)
                                  .setIcon(android.R.drawable.ic_dialog_alert)
                                  .setTitle("Salir")
                                  .setMessage(
                                               "Esta a punto de salir de la aplicaci\u00f3n, se perder\u00e1 todo aquello que no haya guardado. \u00bfEsta seguro que desea salir del sistema?")
                                  .setPositiveButton("Si",
                                               new DialogInterface.OnClickListener() {
                                                      @Override
                                                      public void onClick(DialogInterface dialog,
                                                                   int which) {
                                                            finish();
                                                      }
                                               }).setNegativeButton("No", null).show();
             }
             return super.onKeyDown(keyCode, event);
       }

       @Override
       public void onTabReselected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       public void onTabSelected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       public void onTabUnselected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       protected void onResume() {
             super.onResume();
             if (preferencias.getBoolean("primera_ejecucion", true)) {
                    editor.putBoolean("primera_ejecucion", false);
                    editor.commit();
                    drawerLayout.postDelayed(new Runnable() {
                           @Override
                           public void run() {
                                  drawerLayout.openDrawer(Gravity.LEFT);
                           }
                    }, 500);
             }

       }

       @Override
       protected void onSaveInstanceState(Bundle outState) {
             // super.onSaveInstanceState(outState);
             outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
       }

       private class CuestionarioSimpleOnPageChangeListener extends
                    FragmentViewPager.SimpleOnPageChangeListener {

             private boolean jump = true;
             private boolean pasada = true;

             @Override
             public void onPageScrollStateChanged(int state) {
                  
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                           ExportacionFragmentActivity.this.setPrevPage(viewPager
                                        .getCurrentItem());
                           esArrastre = true;
                           // pasada = false;
                    }
                    super.onPageScrollStateChanged(state);
             }

             @Override
             public void onPageSelected(final int position) {

                    ExportacionFragmentActivity.this.setCurPage(position);
                    int prevPage = ExportacionFragmentActivity.this.getPrevPage();
                 
                    
                    // VALIDACIONES AVANCES
                    if (!validarAvances(prevPage, position))
                           return;
                    boolean flag = false;
                    if (prevPage < position && debeGrabar) {
                           flag = ExportacionFragmentActivity.this.pageAdapter.getItem(
                                        prevPage).grabar();
                           if (!flag) {
                                  esArrastre = false;
                                  nextFragment(prevPage);
                                  return;
                           }
                           esArrastre = true;
                    }


                    // RETROCESOS
                    int diferencia = position - prevPage;
                    if (diferencia < 2) {
                           setEsSalto(false);
                    }
                    if (esArrastre) {
                           if (!ejecutarRetrocesos(prevPage, position)) {
                                  return;
                           } else {
                                         ExportacionFragmentActivity.this.pageAdapter.getItem(
                                               position).cargarDatos();
                           }
                    }
                    if (!ejecutarAvances(prevPage, position)) {
                        
                           return;
                    }

                    debeGrabar = true;
             }
       }
       
       private boolean validarAvances(int prevPage, int curPage) {

             if (prevPage >= curPage) {
                    return true;
             }
           
             esArrastre = true;
             return true;
       }
       
       private boolean ejecutarAvances(int prevPage, int curPage) {
                        
             if (curPage < prevPage) {
                    return true;
             }
          
             return true;
       }
       
       private boolean ejecutarRetrocesos(int prevPage, int curPage) {                
             
                    return true;
       }
       public void blanquearHogar() {
                                   if (App.getInstance().getMarco() == null) {
                                           setTitulo("");
                                      return;
                                   }
                                   setTitulo(Util.getText(App.getInstance().getMarco().conglomerado)
                                                   + " - "
                                                   + Util.getText(App.getInstance().getMarco().nselv));
                           }

       public void uploadData() {
             action = PROCCES.DATA;
             DialogComponent dialog = new DialogComponent(this, this,
                           TIPO_DIALOGO.YES_NO, getResources()
                                        .getString(R.string.app_name),
                           "Desea importar archivos de respaldo?");
             dialog.showDialog();
       }

       public void uploadMarco() {
             action = PROCCES.MARCO;
             DialogComponent dialog = new DialogComponent(this, this,
                           TIPO_DIALOGO.YES_NO, getResources()
                                        .getString(R.string.app_name),
                           "Desea importar archivos de marco?");
             dialog.showDialog();
       }

       @Override
       public void onCancel() {
             action = null;
       }

       @Override
       public void onAccept() {
             Intent intent = new Intent(this, FileSelectionActivity.class);
             String ruta = App.RUTA_BASE;
             if (action == PROCCES.MARCO) {
                    ruta += "/config";
                    File directorio = new File(ruta);
                    if (!directorio.exists()) {
                           directorio.mkdirs();
                    }
                    intent.putExtra(FileSelectionActivity.START_FOLDER, ruta);
                    intent.putExtra("FILTER_EXTENSION", new String[] { "cfg", "zip" });
                    startActivityForResult(intent, REQUEST_CODE_PICK_MARCO);
             } else if (action == PROCCES.DATA) {
                    ruta += "/backups";
                    File directorio = new File(ruta);
                    if (!directorio.exists()) {
                           directorio.mkdirs();
                    }
                    intent.putExtra(FileSelectionActivity.START_FOLDER, ruta);
                    intent.putExtra("FILTER_EXTENSION", new String[] { "xml", "zip" });
                    startActivityForResult(intent, REQUEST_CODE_PICK_IMPORT);
             }
             action = null;
       }

       protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       	if( resultCode != RESULT_OK) return;
       	if (requestCode != REQUEST_CODE_PICK_MARCO && requestCode != REQUEST_CODE_PICK_MARCO) {
               if (forms != null) {
                   for (FragmentForm fragment : forms) {
                       fragment.onActivityResult(requestCode, resultCode, data);
                   }
               }
               return;
   		}
       	ArrayList<File> files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);    	
       	if( files != null && files.size()==0 ){
           	ToastMessage.msgBox(this, "Ning\u00fan archivo ha sido seleccionado", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
           }    	
           if( requestCode == REQUEST_CODE_PICK_MARCO ){
               importar(files);
           } else if( requestCode == REQUEST_CODE_PICK_IMPORT ){
           	importar(files);
           }
       }
//       protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//             if (resultCode != RESULT_OK)
//                    return;
//             ArrayList<File> files = (ArrayList<File>) data
//                           .getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
//             if (files != null && files.size() == 0) {
//                    ToastMessage.msgBox(this,
//                                  "Ning\u00fan archivo ha sido seleccionado",
//                                  ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
//             }
//             if (requestCode == REQUEST_CODE_PICK_MARCO) {
//                    importar(files);
//             } else if (requestCode == REQUEST_CODE_PICK_IMPORT) {
//                    importar(files);
//             }
//       }

       private void importar(ArrayList<File> archivos) {
             DialogComponent dlg = new DialogComponent(this, this,
                           DialogComponent.TIPO_DIALOGO.NEUTRAL, getResources().getString(
                                        R.string.app_name), "Seleccione alg�n item.");
             if (archivos.size() == 0) {
                    dlg.showDialog();
                    return;
             }
             Importacion r = new Importacion(this, "Importando informaci�n. ");
             r.setArchivos(archivos);
             r.execute();
       }

       @Override
       public void update(Observable observable, Object data) {
       }

       @Override
       public void calificar() {
             // TODO Auto-generated method stub

       }
}

