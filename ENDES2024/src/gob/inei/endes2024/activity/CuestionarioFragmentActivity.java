package gob.inei.endes2024.activity;
import gob.inei.dnce.adapter.MyFragmentPagerAdapter;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.FragmentViewPager;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.adapter.MyDrawerAdapter;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.controller.Importacion;
import gob.inei.endes2024.fragment.MarcoFragment;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_002;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_003;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_004;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_005;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_006;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_01Fragment_007;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_02Fragment_008;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_02Fragment_009;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_02Fragment_010;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_02Fragment_011;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_02Fragment_012;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_03Fragment_013;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_03Fragment_014;
import gob.inei.endes2024.fragment.C1seccion01_03.CS_03Fragment_015;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_04Fragment_001;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_04Fragment_002;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_04Fragment_003;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_04Fragment_004;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_05Fragment_005;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_06Fragment_006;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_06Fragment_007;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_008;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_009;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_010;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_011;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_012;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_013;
import gob.inei.endes2024.fragment.C1seccion04_07.CS_07Fragment_014;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_001;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_002;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_003;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_004;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_005;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_006;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_007;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_008;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_009;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_010;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_011;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_012;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_013;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_09Fragment_014;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_09Fragment_015;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_09Fragment_016;
import gob.inei.endes2024.fragment.C1visita.CS_VISITAFragment_001;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_000;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_002_1;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_006;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_007;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_008;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_009;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_000;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_006;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_007;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_008;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_001;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_002;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_003;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_004_1;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_004_2;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_005;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_006;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_007;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_008;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_009;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_010;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_011;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_012;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_013;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_014;
import gob.inei.endes2024.fragment.CIseccion_04A.CISECCION_04AFragment_015;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_001;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_002;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_003;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_004;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_005;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_006;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_007;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_008;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_009;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_010;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_011;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_012;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_013;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_014;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_015;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_016;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_017;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_018;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_019;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_020;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_021;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_1;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_2;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_3;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_4;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_5;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_6;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_04B_2.CISECCION_04B2_Fragment_006;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_06Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_06Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_06Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_06Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_06Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_07Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_07Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_07Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_07Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_006;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_08Fragment_007;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_09Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_002;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_004;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_005;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_006;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_007;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_008;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_009;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_010;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_011;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_012;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_013;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_02Fragment_001;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_02Fragment_002;
import gob.inei.endes2024.fragment.caratula.CARATULAFragment_002;
import gob.inei.endes2024.fragment.caratula.CaratulaFragmentCI;
import gob.inei.endes2024.fragment.hogar.HogarFragment_000;
import gob.inei.endes2024.fragment.hogar.HogarFragment_001;
import gob.inei.endes2024.fragment.hogar.HogarFragment_002;
import gob.inei.endes2024.fragment.hogar.HogarFragment_003;
import gob.inei.endes2024.fragment.hogar.HogarFragment_004;
import gob.inei.endes2024.fragment.hogar.HogarFragment_005;
import gob.inei.endes2024.fragment.hogar.HogarFragment_006;
import gob.inei.endes2024.fragment.hogar.HogarFragment_007;
import gob.inei.endes2024.fragment.hogar.HogarFragment_008;
import gob.inei.endes2024.fragment.hogar.HogarFragment_009;
import gob.inei.endes2024.fragment.hogar.HogarFragment_010;
import gob.inei.endes2024.fragment.hogar.HogarFragment_011;
import gob.inei.endes2024.fragment.hogar.HogarFragment_012;
import gob.inei.endes2024.fragment.hogar.HogarFragment_013;
import gob.inei.endes2024.fragment.hogar.HogarFragment_013_Antr;
import gob.inei.endes2024.fragment.hogar.HogarFragment_014;
import gob.inei.endes2024.fragment.hogar.HogarFragment_016;
import gob.inei.endes2024.fragment.hogar.HogarFragment_017;
import gob.inei.endes2024.fragment.hogar.HogarFragment_020;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_001;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_002;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_003;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_004;
import gob.inei.endes2024.fragment.seccion04.Seccion04_05Fragment_001;
import gob.inei.endes2024.fragment.seccion04.Seccion04_05Fragment_002;
import gob.inei.endes2024.fragment.visita.VisitaFragment_001;
import gob.inei.endes2024.listener.MyNavigationClickListener;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.R;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0001;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0002;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_01;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_02;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_03;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import paul.arian.fileselector.FileSelectionActivity;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;


@SuppressLint("NewApi")
public class CuestionarioFragmentActivity extends MasterActivity implements ActionBar.TabListener, Respondible, Observer {

       private enum PROCCES {
             MARCO, DATA,GRABADOPARCIAL
       }

       private static int REQUEST_CODE_PICK_MARCO = 1;
       private static int REQUEST_CODE_PICK_IMPORT = 2;
       private PROCCES action = null;
       private DrawerLayout drawerLayout;
       private ListView drawerList;
       private ActionBarDrawerToggle drawerToggle;
       private Vector<String> opcionesMenu = new Vector<String>();
       /**
       * Used to store the last screen title. For use in
       * {@link #restoreActionBar()}.
       */
       private static String TAG = "CuestionarioRuralFragmentActivity";
       private static String TITULO = "";
       private boolean sec4TE, sec4TE1;

       SharedPreferences preferencias;
       SharedPreferences.Editor editor;

       private static String PREFERENCIAS = "preferencias";
       private CharSequence mTitle;
       // private CuestionarioService cuestionarioService;
       private SeccionCapitulo[] seccionesCargado;

       public static Context baseContext;
       public boolean band=false; 
       public boolean salto=false; 
       
       public boolean saltoMenu = false;
       
       public Menu menu;
       
       public SubMenu sh,si,ss;
       
       @Override
       protected void onCreate(Bundle savedInstanceState) {
             super.onCreate(savedInstanceState);
             baseContext = getApplicationContext();
             TAG = this.getClass().toString();
             setContentView(R.layout.activity_principal);
             mTitle = getTitle();
             preferencias = this.getSharedPreferences(PREFERENCIAS,
                           Context.MODE_PRIVATE);
             editor = preferencias.edit();
             List<FragmentForm> forms = createFragments();
             pageAdapter = new MyFragmentPagerAdapter(this, forms);
             viewPager = (FragmentViewPager) findViewById(R.id.pagerPrincipal);
             viewPager.setAdapter(pageAdapter);
             viewPager.setFragments(forms);
             final CuestionarioSimpleOnPageChangeListener viewPagerListener = new CuestionarioSimpleOnPageChangeListener();
             ///se coment� esta linea de codigo debido a que para la version del android 5.1.1 no funciona cuando cuando arrastras con el dedo. (18/05/2017)
//             viewPager.setPageTransformer(true, new DepthPageTransformer());
             viewPager.setOnPageChangeListener(viewPagerListener);
             // viewPager.post(new Runnable() {
             // @Override
             // public void run() {
             // viewPagerListener.onPageSelected(viewPager.getCurrentItem());
             // }
             // });
             for (String s : getResources().getStringArray(R.array.drawer_array)) {
                    opcionesMenu.add(s);
             }
             if (App.getInstance().getUsuario().cargo_id != 25) {
                    opcionesMenu.remove(opcionesMenu.size() - 1);
             }
             drawerLayout = (DrawerLayout) findViewById(R.id.drawer_principal_layout);
             drawerList = (ListView) findViewById(R.id.left_principal_drawer);
             drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                           GravityCompat.START);
             drawerList.setAdapter(new MyDrawerAdapter(opcionesMenu, this));
             navigationClickListener = new MyNavigationClickListener(this, 0,
                           drawerList, drawerLayout);
             drawerList.setOnItemClickListener(navigationClickListener);
             TITULO = getResources().getString(R.string.app_name);
             final ActionBar actionBar = getActionBar();
             drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                           R.drawable.ic_navigation_drawer,
                           R.string.navigation_drawer_open,
                           R.string.navigation_drawer_close) {

                    public void onDrawerClosed(View view) {
                           // getActionBar().setIcon(R.drawable.ic_launcher);
                           getActionBar().setTitle(TITULO);
                           ActivityCompat
                                        .invalidateOptionsMenu(CuestionarioFragmentActivity.this);
                    }

                    public void onDrawerOpened(View drawerView) {
                           // getActionBar().setIcon(R.drawable.ic_launcher);
                           getActionBar().setTitle("Menu");
                           ActivityCompat
                                        .invalidateOptionsMenu(CuestionarioFragmentActivity.this);
                    }
             };
             actionBar.setDisplayHomeAsUpEnabled(true);
             actionBar.setHomeButtonEnabled(true);
             drawerLayout.setDrawerListener(drawerToggle);

             if (savedInstanceState != null) {
                    actionBar.setSelectedNavigationItem(savedInstanceState.getInt(
                                  "tab", 1));
             }
       }
       
       public static final Integer MARCO = 0;
       public static final Integer CARATULA = 1;
             
       public static final Integer VISITA = 2;
       
       public static final Integer SECCION01 = 3;//litado de personas
       public static final Integer CHSECCION1f_2 = 4;
       public static final Integer CHSECCION1f_3 = 5;
       public static final Integer CHSECCION1f_4 = 6;
//       public static final Integer CHSECCION1f_5 = 7;
       
       public static final Integer MORTALIDAD = 7;
//       public static final Integer MIGRACION = 8;
//       public static final Integer IMIGRACION = 8;
       public static final Integer HOGAR = 8;       
       
       public static final Integer HOGARFRAGMENTO8=13;
       public static final Integer HOGARMAYORADOS = 16;
    
       public static final Integer SECCION03=17;
       public static final Integer QHSECCION2f_11=18;
       public static final Integer SECCION03B=19;
       
       public static final Integer REPORTEHOGAR=21;
       
       public static final Integer REPORTEANTROP = 22;
       
       public static final Integer SECCIONPOSTCENSO=23; 
       
       public static final Integer SECCION8INCENTIVOS=24; 
        
       public static final Integer SECCION04=25;
       
       public static final Integer SECCION05=26;
       
//       public static final Integer SECCIONCOVID_1=26;
//       public static final Integer SECCIONCOVID_2=27;
//       public static final Integer SECCIONCOVID_3=28;
       
       public static final Integer SECCION06=27;//29
       public static final Integer CSVISITA=28;//30
       
       
       public static final Integer CSSECCION1f_2=29;//31
       public static final Integer CSSECCION1f_4=31;//33
       public static final Integer CSSECCION1f_5=33;//35
       public static final Integer CSSECCION2f_8=34;//36
       public static final Integer CSSECCION2f_9=36;//38
       public static final Integer CSSECCION2f_11=38;//40
       public static final Integer CSSECCION2f_12=39;//41
       
       public static final Integer CSSECCION3f_1=40;//42
       public static final Integer CSSECCION3f_2=41;//43
       public static final Integer CSSECCION3f_3=42;//44
       
       public static final Integer CSSECCION4f_1=43;//45
       public static final Integer CSSECCION4f_2=44;//46
       public static final Integer CSSECCION4f_3=45;//47       
       public static final Integer CSSECCION4f_4=46;//48
       
       public static final Integer CSSECCION5f_1=47;//49
       
       public static final Integer CSSECCION6f_1=48;//50
       public static final Integer CSSECCION6f_2=49;//51
       public static final Integer CSSECCION7f_1=50;//52
       public static final Integer CSSECCION7f_2=51;//53
       public static final Integer CSSECCION7f_3=52;//54
       public static final Integer CSSECCION7f_4=53;//55
       public static final Integer CSSECCION7f_5=54;//56
       public static final Integer CSSECCION7f_6=55;//57
       public static final Integer CSSECCION7f_7=56;//58
       
       public static final Integer CSSECCION8f_1=57;//59
       public static final Integer CSSECCION8f_2=58;//60
       public static final Integer CSSECCION8f_3=59;//61
       public static final Integer CSSECCION8f_4=60;//62
       public static final Integer CSSECCION8f_5=61;//63
       public static final Integer CSSECCION8f_6=62;//64
       public static final Integer CSSECCION8f_7=63;//65
       public static final Integer CSSECCION8f_8=64;//66
       public static final Integer CSSECCION8f_9=65;//67
       public static final Integer CSSECCION8f_10=66;//68
       public static final Integer CSSECCION8f_11=67;//69
       public static final Integer CSSECCION8f_12=68;//70
       public static final Integer CSSECCION8f_13=69;//71
       
       public static final Integer CSSECCION9f_1=70;//72
       public static final Integer CSSECCION9f_2=71;//73
       public static final Integer CSSECCION9f_3=72;//74
       
       
       /***********CUESTIONARIO INDIVIDUAL**********/
       public static final Integer CISECCION_f_carat=73;//75
       public static final Integer CISECCION_01f_0=74;//76
       public static final Integer CISECCION_01f_1=75;//77
       public static final Integer CISECCION_01f_2=76;//78
       public static final Integer CISECCION_01f_2_1=77;//79
       public static final Integer CISECCION_01f_3=78;//80
       public static final Integer CISECCION_01f_4=79;//81
       public static final Integer CISECCION_01f_5=80;//82
       
       public static final Integer CISECCION_02f_1=81;//83
       public static final Integer CISECCION_02f_2=82;//84
       public static final Integer CISECCION_02f_3=83;//85
       public static final Integer CISECCION_02f_4=84;//86
       public static final Integer CISECCION_02f_5=85;//87
       public static final Integer CISECCION_02f_6=86;//88
       public static final Integer CISECCION_02f_7=87;//89
       public static final Integer CISECCION_02f_8=88;//90
       public static final Integer CISECCION_02f_9=89;//91
       
       public static final Integer CISECCION_03f_0=90;//92       
       public static final Integer CISECCION_03f_1=91;//93
       public static final Integer CISECCION_03f_2=92;//94
       public static final Integer CISECCION_03f_3=93;//95
       public static final Integer CISECCION_03f_4=94;//96
       public static final Integer CISECCION_03f_5=95;//97
       public static final Integer CISECCION_03f_6=96;//98
       public static final Integer CISECCION_03f_7=97;//99
       public static final Integer CISECCION_03f_8=98;//100
       
              
       public static final Integer CISECCION_04Af_1=99;//101
       public static final Integer CISECCION_04Af_2=100;//102
       public static final Integer CISECCION_04Af_3=101;//103
       public static final Integer CISECCION_04Af_4_1=102;//104
       public static final Integer CISECCION_04Af_4_2=103;//105
       public static final Integer CISECCION_04Af_5=104;//106
       public static final Integer CISECCION_04Af_6=105;//107
       public static final Integer CISECCION_04Af_7=106;//108
       public static final Integer CISECCION_04Af_8=107;//109
       public static final Integer CISECCION_04Af_9=108;//110
       public static final Integer CISECCION_04Af_10=109;//111
       public static final Integer CISECCION_04Af_11=110;//112
       public static final Integer CISECCION_04Af_12=111;//113
       public static final Integer CISECCION_04Af_13=112;//114
       public static final Integer CISECCION_04Af_14=113;//115
       public static final Integer CISECCION_04Af_15=114;//116

       public static final Integer CISECCION_04Bf_1=115;//117
       public static final Integer CISECCION_04Bf_2=116;//118
       public static final Integer CISECCION_04Bf_3=117;//119
       public static final Integer CISECCION_04Bf_4=118;//120
       public static final Integer CISECCION_04Bf_5=119;//121
       public static final Integer CISECCION_04Bf_6=120;//122
       public static final Integer CISECCION_04Bf_7=121;//123
       public static final Integer CISECCION_04Bf_8=122;//124
       public static final Integer CISECCION_04Bf_9=123;//125
       public static final Integer CISECCION_04Bf_10=124;//126
       public static final Integer CISECCION_04Bf_11=125;//127
       public static final Integer CISECCION_04Bf_12=126;//128
       public static final Integer CISECCION_04Bf_13=127;//129
       public static final Integer CISECCION_04Bf_14=128;//130
       public static final Integer CISECCION_04Bf_15=129;//131
       public static final Integer CISECCION_04Bf_16=130;//132
       public static final Integer CISECCION_04Bf_17=131;//133
       public static final Integer CISECCION_04Bf_18=132;//134              
       public static final Integer CISECCION_04Bf_19=133;//135
       public static final Integer CISECCION_04Bf_20=134;//136
       public static final Integer CISECCION_04Bf_21=135;//137
       public static final Integer CISECCION_04Bf_20_1=136;//138
       public static final Integer CISECCION_04Bf_20_2=137;//139
       public static final Integer CISECCION_04Bf_20_3=138;//140
       public static final Integer CISECCION_04Bf_20_4=139;//141       
       public static final Integer CISECCION_04Bf_20_5=140;//142
       public static final Integer CISECCION_04Bf_20_6=141;//143
       
       public static final Integer CISECCION_04B2f_1=142;//144
       public static final Integer CISECCION_04B2f_2=143;//145
       public static final Integer CISECCION_04B2f_3=144;//146
       public static final Integer CISECCION_04B2f_4=145;//147
       public static final Integer CISECCION_04B2f_5=146;//148
       public static final Integer CISECCION_04B2f_6=147;//149
      
       public static final Integer CISECCION_05f_1=148;//150
       public static final Integer CISECCION_05f_2=149;//151
       public static final Integer CISECCION_05f_3=150;//152
       public static final Integer CISECCION_05f_4=151;//153
       public static final Integer CISECCION_05f_5=152;//154       
       public static final Integer CISECCION_06f_1=153;//155
       public static final Integer CISECCION_06f_2=154;//156
       public static final Integer CISECCION_06f_3=155;//157
       public static final Integer CISECCION_06f_4=156;//158
       public static final Integer CISECCION_06f_5=157;//159       
       public static final Integer CISECCION_07f_1=158;//160
       public static final Integer CISECCION_07f_2=159;//161
       public static final Integer CISECCION_07f_4=160;//162
       public static final Integer CISECCION_07f_5=161;//163     
       public static final Integer CISECCION_08f_1=162;//164
       public static final Integer CISECCION_08f_2=163;//165
       public static final Integer CISECCION_08f_3=164;//166
       public static final Integer CISECCION_08f_4=165;//167
       public static final Integer CISECCION_08f_5=166;//168
       public static final Integer CISECCION_08f_6=167;//169
       public static final Integer CISECCION_08f_7=168;//170      
       public static final Integer CISECCION_09f_1=169;//171       
       public static final Integer CISECCION_10f_1=170;//172
       public static final Integer CISECCION_10f_2=171;//173
       public static final Integer CISECCION_10f_3=172;//174
       public static final Integer CISECCION_10f_4=173;//175
       public static final Integer CISECCION_10f_5=174;//176
       public static final Integer CISECCION_10f_6=175;//177
       public static final Integer CISECCION_10f_7=176;//178
       public static final Integer CISECCION_10f_8=177;//179
       public static final Integer CISECCION_10f_9=178;//180
       public static final Integer CISECCION_10f_10=179;//181
       public static final Integer CISECCION_10f_11=180;//182
       public static final Integer CISECCION_10f_12=181;//183
       public static final Integer CISECCION_10f_13=182;//184
       public static final Integer CISECCION_10_2f_1=183;//185
       public static final Integer CISECCION_10_2f_2=184;//186
       //public static final Integer REPORTEANTROP = 184;

       public static boolean blanquear=false;
       public static boolean blanquear2=false;
       public static boolean blanquear3=false;



       private List<FragmentForm> createFragments() {
             List<FragmentForm> fragments = new ArrayList<FragmentForm>();

             fragments.add(new MarcoFragment().parent(this));
             /* 01 */fragments.add(new CARATULAFragment_002().parent(this));
             /* 02 */fragments.add(new VisitaFragment_001().parent(this));
             
             /* 03 */fragments.add(new Seccion01Fragment_001().parent(this));//listado de personas
             /* 04 */fragments.add(new Seccion01Fragment_002().parent(this));
             /* 05 */fragments.add(new Seccion01Fragment_003().parent(this));
             /* 06 */fragments.add(new Seccion01Fragment_004().parent(this));
//             /* 07 */fragments.add(new Seccion01Fragment_005().parent(this)); // listado de personas discapacidad
          
             /* 07 */fragments.add(new HogarFragment_000().parent(this));
//             /* 08 */fragments.add(new HogarFragment_0001().parent(this));
//             /* 07 */fragments.add(new HogarFragment_0002().parent(this));
             
             /* 08 */fragments.add(new HogarFragment_001().parent(this));
             /* 09 */fragments.add(new HogarFragment_002().parent(this));
             /* 10 */fragments.add(new HogarFragment_003().parent(this));
             /* 11 */fragments.add(new HogarFragment_004().parent(this));
             /* 12 */fragments.add(new HogarFragment_005().parent(this));
             /* 13 */fragments.add(new HogarFragment_006().parent(this));
             /* 14 */fragments.add(new HogarFragment_007().parent(this));
             /* 15 */fragments.add(new HogarFragment_008().parent(this));
             /* 16 */fragments.add(new HogarFragment_009().parent(this));
             
             /* 17 */fragments.add(new HogarFragment_010().parent(this));
             /* 18 */fragments.add(new HogarFragment_011().parent(this));
             /* 19 */fragments.add(new HogarFragment_012().parent(this));
             /* 20 */fragments.add(new HogarFragment_014().parent(this));
             
             /* 21 */fragments.add(new HogarFragment_013().parent(this));
             /* 186 */fragments.add(new HogarFragment_013_Antr().parent(this));
             
             /* 22 */fragments.add(new HogarFragment_017().parent(this));
             /* 22 */fragments.add(new HogarFragment_020().parent(this));
             
             /* 23 */fragments.add(new Seccion04_05Fragment_001().parent(this));
             /* 24 */fragments.add(new Seccion04_05Fragment_002().parent(this));
             		 
 //              /* 25 */fragments.add(new HogarFragment_013_Antr().parent(this));
//             /* 25 */fragments.add(new HogarFragment_013_01().parent(this));
//             /* 26 */fragments.add(new HogarFragment_013_02().parent(this));
//             /* 26 */fragments.add(new HogarFragment_013_03().parent(this));
             
             /* 27 */fragments.add(new HogarFragment_016().parent(this));
       
             /* 28 */fragments.add(new CS_VISITAFragment_001().parent(this));
             
             /* 29 */fragments.add(new CS_01Fragment_002().parent(this));
             /* 30 */fragments.add(new CS_01Fragment_003().parent(this));
             /* 31 */fragments.add(new CS_01Fragment_004().parent(this));
             /* 32 */fragments.add(new CS_01Fragment_005().parent(this));
             /* 33 */fragments.add(new CS_01Fragment_006().parent(this));
             /* 34 */fragments.add(new CS_01Fragment_007().parent(this));
//             
             /* 35 */fragments.add(new CS_02Fragment_008().parent(this));
             /* 36 */fragments.add(new CS_02Fragment_009().parent(this));
             /* 37 */fragments.add(new CS_02Fragment_010().parent(this));
             /* 38 */fragments.add(new CS_02Fragment_011().parent(this));
             /* 39 */fragments.add(new CS_02Fragment_012().parent(this));
            
             /* 40 */fragments.add(new CS_03Fragment_013().parent(this));
             /* 41 */fragments.add(new CS_03Fragment_014().parent(this));
             /* 42 */fragments.add(new CS_03Fragment_015().parent(this));
             
             /* 43 */fragments.add(new CS_04Fragment_001().parent(this));
             /* 44 */fragments.add(new CS_04Fragment_002().parent(this));
             /* 45 */fragments.add(new CS_04Fragment_003().parent(this));
             /* 46 */fragments.add(new CS_04Fragment_004().parent(this));
             
             /* 47 */fragments.add(new CS_05Fragment_005().parent(this));
             
             /* 48 */fragments.add(new CS_06Fragment_006().parent(this));
             /* 49 */fragments.add(new CS_06Fragment_007().parent(this));
             
             /* 50 */fragments.add(new CS_07Fragment_008().parent(this));
             /* 51 */fragments.add(new CS_07Fragment_009().parent(this));
             /* 52 */fragments.add(new CS_07Fragment_010().parent(this));
             /* 53 */fragments.add(new CS_07Fragment_011().parent(this));
             /* 54 */fragments.add(new CS_07Fragment_012().parent(this));
             /* 55 */fragments.add(new CS_07Fragment_013().parent(this));
             /* 56 */fragments.add(new CS_07Fragment_014().parent(this));
             
             /* 57 */fragments.add(new CS_08Fragment_001().parent(this));
             /* 58 */fragments.add(new CS_08Fragment_002().parent(this));
             /* 59 */fragments.add(new CS_08Fragment_003().parent(this));
             /* 60 */fragments.add(new CS_08Fragment_004().parent(this));
             /* 61 */fragments.add(new CS_08Fragment_005().parent(this));
             /* 62 */fragments.add(new CS_08Fragment_006().parent(this));
             /* 63 */fragments.add(new CS_08Fragment_007().parent(this));
             /* 64 */fragments.add(new CS_08Fragment_008().parent(this));
             /* 65 */fragments.add(new CS_08Fragment_009().parent(this));
             /* 66 */fragments.add(new CS_08Fragment_010().parent(this));
             /* 67 */fragments.add(new CS_08Fragment_011().parent(this));
             /* 68 */fragments.add(new CS_08Fragment_012().parent(this));
             /* 69 */fragments.add(new CS_08Fragment_013().parent(this));
                        
             /* 70 */fragments.add(new CS_09Fragment_014().parent(this));
             /* 71 */fragments.add(new CS_09Fragment_015().parent(this)); 
             /* 72 */fragments.add(new CS_09Fragment_016().parent(this)); 
             
             
             
             
             /********************CUESTIONARIO INDIVIDUAL *********************/
             /* 73 */fragments.add(new CaratulaFragmentCI().parent(this));
             /* 74 */fragments.add(new CISECCION_01Fragment_000().parent(this));
             /* 75 */fragments.add(new CISECCION_01Fragment_001().parent(this));
             /* 76 */fragments.add(new CISECCION_01Fragment_002().parent(this));
             /* 77 */fragments.add(new CISECCION_01Fragment_002_1().parent(this));  //
             /* 78 */fragments.add(new CISECCION_01Fragment_003().parent(this));
             /* 79 */fragments.add(new CISECCION_01Fragment_004().parent(this));
             /* 80 */fragments.add(new CISECCION_01Fragment_005().parent(this));
            
             /* 81 */fragments.add(new CISECCION_02Fragment_001().parent(this));
             /* 82 */fragments.add(new CISECCION_02Fragment_002().parent(this));
             /* 83 */fragments.add(new CISECCION_02Fragment_003().parent(this));            
             /* 84 */fragments.add(new CISECCION_02Fragment_004().parent(this));
             /* 85 */fragments.add(new CISECCION_02Fragment_005().parent(this));
             /* 86 */fragments.add(new CISECCION_02Fragment_006().parent(this));
             /* 87 */fragments.add(new CISECCION_02Fragment_007().parent(this));
             /* 88 */fragments.add(new CISECCION_02Fragment_008().parent(this));
             /* 89 */fragments.add(new CISECCION_02Fragment_009().parent(this));
             
             /* 90 */fragments.add(new CISECCION_03Fragment_000().parent(this));             
             /* 91 */fragments.add(new CISECCION_03Fragment_001().parent(this));
             /* 92 */fragments.add(new CISECCION_03Fragment_002().parent(this));
             /* 93 */fragments.add(new CISECCION_03Fragment_003().parent(this));      
             /* 94 */fragments.add(new CISECCION_03Fragment_004().parent(this));
             /* 95 */fragments.add(new CISECCION_03Fragment_005().parent(this));
             /* 96 */fragments.add(new CISECCION_03Fragment_006().parent(this));
             /* 97 */fragments.add(new CISECCION_03Fragment_007().parent(this));
             /* 98 */fragments.add(new CISECCION_03Fragment_008().parent(this));
             

             /* 99 */fragments.add(new CISECCION_04AFragment_001().parent(this));
             /* 100 */fragments.add(new CISECCION_04AFragment_002().parent(this));
             /* 101 */fragments.add(new CISECCION_04AFragment_003().parent(this));
             /* 102 */fragments.add(new CISECCION_04AFragment_004_1().parent(this));
             /* 103 */fragments.add(new CISECCION_04AFragment_004_2().parent(this));
             /* 104 */fragments.add(new CISECCION_04AFragment_005().parent(this));
             /* 105 */fragments.add(new CISECCION_04AFragment_006().parent(this));
             /* 106 */fragments.add(new CISECCION_04AFragment_007().parent(this));
             /* 107 */fragments.add(new CISECCION_04AFragment_008().parent(this));
             /* 108 */fragments.add(new CISECCION_04AFragment_009().parent(this));
             /* 109 */fragments.add(new CISECCION_04AFragment_010().parent(this));
             /* 110 */fragments.add(new CISECCION_04AFragment_011().parent(this));
             /* 111 */fragments.add(new CISECCION_04AFragment_012().parent(this));
             /* 112 */fragments.add(new CISECCION_04AFragment_013().parent(this));
             /* 113 */fragments.add(new CISECCION_04AFragment_014().parent(this));
             /* 114 */fragments.add(new CISECCION_04AFragment_015().parent(this));

             /* 115 */fragments.add(new C2SECCION_04BFragment_001().parent(this));
             /* 116 */fragments.add(new C2SECCION_04BFragment_002().parent(this));
             /* 117 */fragments.add(new C2SECCION_04BFragment_003().parent(this));
             /* 118 */fragments.add(new C2SECCION_04BFragment_004().parent(this));
             /* 119 */fragments.add(new C2SECCION_04BFragment_005().parent(this));
             /* 120 */fragments.add(new C2SECCION_04BFragment_006().parent(this));
             /* 121 */fragments.add(new C2SECCION_04BFragment_007().parent(this));
             /* 122 */fragments.add(new C2SECCION_04BFragment_008().parent(this));
             /* 123 */fragments.add(new C2SECCION_04BFragment_009().parent(this));
             /* 124 */fragments.add(new C2SECCION_04BFragment_010().parent(this));
             /* 125 */fragments.add(new C2SECCION_04BFragment_011().parent(this));
             /* 126 */fragments.add(new C2SECCION_04BFragment_012().parent(this));
             /* 127 */fragments.add(new C2SECCION_04BFragment_013().parent(this));
             /* 128 */fragments.add(new C2SECCION_04BFragment_014().parent(this));
             /* 129 */fragments.add(new C2SECCION_04BFragment_015().parent(this));
             /* 130 */fragments.add(new C2SECCION_04BFragment_016().parent(this));
             /* 131 */fragments.add(new C2SECCION_04BFragment_017().parent(this));
             /* 132 */fragments.add(new C2SECCION_04BFragment_018().parent(this));
             /* 133 */fragments.add(new C2SECCION_04BFragment_019().parent(this));
             /* 134 */fragments.add(new C2SECCION_04BFragment_020().parent(this));
             /* 135 */fragments.add(new C2SECCION_04BFragment_021().parent(this));               
             
             /* 136 */fragments.add(new C2SECCION_04BFragment_022DIT_1().parent(this));
             /* 137 */fragments.add(new C2SECCION_04BFragment_022DIT_2().parent(this));
             /* 138 */fragments.add(new C2SECCION_04BFragment_022DIT_3().parent(this));
             /* 139 */fragments.add(new C2SECCION_04BFragment_022DIT_4().parent(this));
             /* 140 */fragments.add(new C2SECCION_04BFragment_022DIT_5().parent(this));
             /* 141 */fragments.add(new C2SECCION_04BFragment_022DIT_6().parent(this));
             
                  
             /* 142 */fragments.add(new CISECCION_04B2_Fragment_001().parent(this));
             /* 143 */fragments.add(new CISECCION_04B2_Fragment_002().parent(this));
             /* 144 */fragments.add(new CISECCION_04B2_Fragment_003().parent(this));
             /* 145 */fragments.add(new CISECCION_04B2_Fragment_004().parent(this));
             /* 146 */fragments.add(new CISECCION_04B2_Fragment_005().parent(this));
             /* 147 */fragments.add(new CISECCION_04B2_Fragment_006().parent(this));
             
             /* 148 */fragments.add(new CISECCION_05Fragment_001().parent(this));
             /* 149 */fragments.add(new CISECCION_05Fragment_002().parent(this));
             /* 150 */fragments.add(new CISECCION_05Fragment_003().parent(this));
             /* 151 */fragments.add(new CISECCION_05Fragment_004().parent(this));
             /* 152 */fragments.add(new CISECCION_05Fragment_005().parent(this));
             
             /* 153 */fragments.add(new CISECCION_06Fragment_001().parent(this));
             /* 154 */fragments.add(new CISECCION_06Fragment_002().parent(this));
             /* 155 */fragments.add(new CISECCION_06Fragment_003().parent(this));
             /* 156 */fragments.add(new CISECCION_06Fragment_004().parent(this));
             /* 157 */fragments.add(new CISECCION_06Fragment_005().parent(this));
             
             /* 158 */fragments.add(new CISECCION_07Fragment_001().parent(this));
             /* 159 */fragments.add(new CISECCION_07Fragment_002().parent(this));
             /* 160 */fragments.add(new CISECCION_07Fragment_004().parent(this));
             /* 161 */fragments.add(new CISECCION_07Fragment_005().parent(this));
             
             /* 162 */fragments.add(new CISECCION_08Fragment_001().parent(this));
             /* 163 */fragments.add(new CISECCION_08Fragment_002().parent(this));
             /* 164 */fragments.add(new CISECCION_08Fragment_003().parent(this));
             /* 165 */fragments.add(new CISECCION_08Fragment_004().parent(this));
             /* 166 */fragments.add(new CISECCION_08Fragment_005().parent(this));
             /* 167 */fragments.add(new CISECCION_08Fragment_006().parent(this));
             /* 168 */fragments.add(new CISECCION_08Fragment_007().parent(this));
                  
             /* 169 */fragments.add(new CISECCION_09Fragment_001().parent(this));
                  
             /* 170 */fragments.add(new CISECCION_10_01Fragment_001().parent(this));
             /* 171 */fragments.add(new CISECCION_10_01Fragment_002().parent(this));
             /* 172 */fragments.add(new CISECCION_10_01Fragment_003().parent(this));
             /* 173 */fragments.add(new CISECCION_10_01Fragment_004().parent(this));
             /* 174 */fragments.add(new CISECCION_10_01Fragment_005().parent(this));
             /* 175 */fragments.add(new CISECCION_10_01Fragment_006().parent(this));
             /* 176 */fragments.add(new CISECCION_10_01Fragment_007().parent(this));
             /* 177 */fragments.add(new CISECCION_10_01Fragment_008().parent(this));
             /* 178 */fragments.add(new CISECCION_10_01Fragment_009().parent(this));
             /* 179 */fragments.add(new CISECCION_10_01Fragment_010().parent(this));
             /* 180 */fragments.add(new CISECCION_10_01Fragment_011().parent(this));
             /* 181 */fragments.add(new CISECCION_10_01Fragment_012().parent(this));
             /* 182 */fragments.add(new CISECCION_10_01Fragment_013().parent(this));
             
             /* 183 */fragments.add(new CISECCION_10_02Fragment_001().parent(this));
             /* 184 */fragments.add(new CISECCION_10_02Fragment_002().parent(this));
//           /* 185 */fragments.add(new CISECCION_10_02Fragment_003().parent(this));
             
             /* 186 *///fragments.add(new HogarFragment_013_Antr().parent(this));
             
             return fragments;
       }

       public void restoreActionBar() {
             ActionBar actionBar = getActionBar();
             actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
             actionBar.setDisplayShowTitleEnabled(true);
             actionBar.setTitle(mTitle);
       }

       @Override
       public boolean onPrepareOptionsMenu(Menu menu) {
             boolean menuAbierto = drawerLayout.isDrawerOpen(drawerList);
             // if (menuAbierto)
             // menu.findItem(R.id.menu_info).setVisible(false);
             // else
             // menu.findItem(R.id.menu_info).setVisible(true);
             return super.onPrepareOptionsMenu(menu);
       }

       @Override
       public boolean onCreateOptionsMenu(Menu menu) {
             getMenuInflater().inflate(R.menu.principal, menu);
             menu.getItem(0).setVisible(false);
             
//             ToastMessage.msgBox(CuestionarioFragmentActivity.this,
//                     "ffffff",
//                     ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
             
             menu.add(3,menu.CATEGORY_CONTAINER,menu.CATEGORY_CONTAINER, R.string.action_reporte);
             menu.add(5,menu.CATEGORY_CONTAINER,menu.CATEGORY_CONTAINER, "Ir a Reporte de Medici�n");
             
             sh = menu.addSubMenu(menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,R.string.chsecciones);
             sh.add(1,0,0,R.string.chseccion01);
             sh.add(1,1,0,R.string.chseccion02);
             sh.add(1,2,0,R.string.chseccion03a);
             sh.add(1,3,0,R.string.chseccion03b);
             sh.add(1,4,0,R.string.chseccion04);
             sh.add(1,5,0,R.string.chseccion05);
             sh.add(1,6,0,R.string.chseccion06);
//             sh.add(1,7,0,R.string.chseccion07);
             sh.add(1,8,0,R.string.chseccion08);
             
             si = menu.addSubMenu(menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,R.string.cisecciones);
             si.add(4,0,0,R.string.ciseccion_caratula);
             si.add(4,1,0,R.string.ciseccion_visitas);
             si.add(4,2,0,R.string.ciseccion01);
             si.add(4,3,0,R.string.ciseccion02);
             si.add(4,4,0,R.string.ciseccion02_nacimientos);
             si.add(4,5,0,R.string.ciseccion03);
             si.add(4,6,0,R.string.ciseccion02_calendario);             
             si.add(4,7,0,R.string.ciseccion04a);
             si.add(4,8,0,R.string.ciseccion04b);
             si.add(4,9,0,R.string.ciseccion04b_2);
             si.add(4,10,0,R.string.ciseccion05);
             si.add(4,11,0,R.string.ciseccion06);
             si.add(4,12,0,R.string.ciseccion07);
             si.add(4,13,0,R.string.ciseccion08);
             si.add(4,14,0,R.string.ciseccion09);
             si.add(4,15,0,R.string.ciseccion10);
             si.add(4,16,0,R.string.ciseccionTarjeta);
             
             ss = menu.addSubMenu(menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,menu.CATEGORY_ALTERNATIVE,R.string.cssecciones);
             ss.add(2,0,0,R.string.caratulasalud);
             ss.add(2,1,0,R.string.csseccion00);
             ss.add(2,2,0,R.string.csseccion01);
             ss.add(2,3,0,R.string.csseccion02);
             ss.add(2,4,0,R.string.csseccion03);
             ss.add(2,5,0,R.string.csseccion04);
             ss.add(2,6,0,R.string.csseccion05);
             ss.add(2,7,0,R.string.csseccion06);
             ss.add(2,8,0,R.string.csseccion07);
             ss.add(2,9,0,R.string.csseccion08);
             ss.add(2,10,0,R.string.csseccion09);
             
             
             this.menu = menu;
             return super.onCreateOptionsMenu(menu);
       }
       
       public void visibleSubmenuIndividual(boolean valor,Integer... indices) {
    	   for(Integer a:indices) {
    		   si.getItem(a).setVisible(valor);  
    	   }
       }

       @Override
       public boolean onOptionsItemSelected(MenuItem item) {
             // Handle action bar item clicks here. The action bar will
             // automatically handle clicks on the Home/Up button, so long
             // as you specify a parent activity in AndroidManifest.xml.
    	   	 esArrastre=true;
             if (drawerToggle.onOptionsItemSelected(item)) {
                    return true;
             }
            int id = item.getItemId();
           if(item.getGroupId()==1){
            	 if(App.getInstance().getHogar()==null){
            		 return super.onOptionsItemSelected(item);
            	 }
            	 if(id==0 && App.getInstance().getHogar().qhbebe!=null){
	            	 irA(SECCION01);
            	 }
            	 if(id==1 && App.getInstance().getHogar().qh40!=null){
            		 irA(HOGAR);
            	 }
            	 if(id==2 && App.getInstance().getHogar().qh93!=null){
            		 irA(SECCION03);
            	 }
            	 if(id==3 && (App.getInstance().getHogar().qh101!=null || App.getInstance().getHogar().qh103!=null)){
            		 irA(SECCION03B);
            	 }
            	 if(id==4 && App.getInstance().getHogar().existeseccion04 && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1){
            		 irA(SECCION04);
            	 }
            	 if(id==5 && App.getInstance().getHogar().existeseccion04 && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1){
            		 irA(SECCION05);
            	 }
            	 if(id==6 && App.getInstance().getHogar().qh224!=null){
            		 irA(SECCION06);
            	 }
//            	 if(id==7 && App.getInstance().getHogar().qh40!=null){
//            		 irA(SECCIONCOVID_1);
//            	 }
            	 if(id==8 && App.getInstance().getIncentivos()!=null && App.getInstance().getIncentivos().id!=null && App.getInstance().getIncentivos().hogar_id!=null){
            		 irA(SECCION8INCENTIVOS);
            	 }
             }
           if(item.getGroupId()==2){        	   
        	   if(App.getInstance().getSaludNavegacionS1_3()==null || App.getInstance().getVisita()==null || App.getInstance().getVisita().qhvresul==null || App.getInstance().getVisita().qhvresul!=1){        		   
        		   return super.onOptionsItemSelected(item);
        	   } 
        	   if(id==0 && App.getInstance().getPersonaSeccion01()!=null){
        		   irA(CSVISITA);
        	   }
        	   if(id==1 && App.getInstance().getSaludNavegacionS1_3().qs23!=null && App.getInstance().getC1Visita()!=null){
        		   irA(CSSECCION1f_2);
        	   }
        	   if(id==2 && App.getInstance().getSaludNavegacionS1_3().qs100!=null){
        		   irA(CSSECCION1f_4);
        	   }
        	   if(id==3 && App.getInstance().getSaludNavegacionS1_3().qs200!=null){
        		   irA(CSSECCION2f_8);
        	   }        	   
        	   if(id==4 && App.getInstance().getSaludNavegacionS1_3().qs301!=null && App.getInstance().getSaludNavegacionS1_3().qs23>49){
        		   irA(CSSECCION3f_1);
        	   }
        	   if(App.getInstance().getSaludNavegacionS4_7()==null){
        		   return super.onOptionsItemSelected(item);
        	   } 
        	   if(id==5 && App.getInstance().getSaludNavegacionS4_7().qs401!=null && App.getInstance().getSaludNavegacionS1_3().qs23<76){
//        		   Log.e("","212121 ");
        		   irA(CSSECCION4f_1);
        	   }
        	   if(id==6 && App.getInstance().getSaludNavegacionS4_7().qs500!=null){
        		   irA(CSSECCION5f_1);
        	   }
        	   if(id==7 && App.getInstance().getSaludNavegacionS4_7().qs601a!=null && App.getInstance().getSaludNavegacionS1_3().qs23<50 && App.getInstance().getSaludNavegacionS1_3().qh06==1){
        		   irA(CSSECCION6f_1);
        	   }
        	   if(id==7 && App.getInstance().getSaludNavegacionS4_7().qs603!=null && App.getInstance().getSaludNavegacionS1_3().qs23<50 && App.getInstance().getSaludNavegacionS1_3().qh06==2){
        		   irA(CSSECCION6f_1);
        	   }
        	   if(id==8 && App.getInstance().getSaludNavegacionS4_7().qs700a!=null){
        		   irA(CSSECCION7f_1);
        	   }
        	  if(id==9 && App.getInstance().getSaludNavegacionS4_7().existes8){
        		   irA(CSSECCION8f_1);
        	   }
        	   if(id==10 && App.getInstance().getSaludNavegacionS4_7().qs906!=null){
        		   irA(CSSECCION9f_1);
        	   }
           }
           if(item.getGroupId()==3){
        	   if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null && App.getInstance().getVisita()!=null && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1)
        		   Log.e("ir a reportes ","reporte de hogar");
        	   irA(REPORTEHOGAR);
           }
           
           if(item.getGroupId()==4){
          	 if(App.getInstance().getHogar()==null){
          		 return super.onOptionsItemSelected(item);
          	 }
          	           	 
          	
          	if (id==0 && App.getInstance().getSaludNavegacionS4_7()!=null && App.getInstance().getSaludNavegacionS4_7().existemef && App.getInstance().getVisita()!=null && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1) {
        		App.getInstance().setPersonaCuestionarioIndividual(null);
               irA(CISECCION_f_carat);
          	}          	
          	
          	if(id==1){  //&& App.getInstance().getHogar().qh40!=null){
        		irA(CISECCION_01f_0);
        	}
          	if(id==2){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_01f_1);
          	}
          	if(id==3){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_02f_1);
          	}
          	
          	if(id==4){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_02f_3);
            }
          	
          	if(id==5){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_03f_0);
          		
            }
          	
          	if(id==6){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_03f_3);
            }
          	
          	if(id==7){  //&& App.getInstance().getHogar().qh40!=null){
          		
          		saltoMenu = true;
          		Integer prevpage = CuestionarioFragmentActivity.this.getPrevPage();
          		Integer curpage = CuestionarioFragmentActivity.this.getCurPage();
          		 if (curpage==CISECCION_02f_3 && saltoMenu) {
                	 
                	 Log.d("Mensaje","test pasar fragmento 2_3");
                	 esArrastre=true;
                	 debeGrabar = false;
                	 saltoMenu = false;
                 }
                 
          		irA(CISECCION_04Af_1);
            }
          	
          	if(id==8){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_04Bf_1);
            }
          	
          /*	if(id==9){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_04Bf_18);
            }*/
          	
          	if(id==9){  //&& App.getInstance().getHogar().qh40!=null){
          		irA(CISECCION_04B2f_1);
            }
          	
          	if(id==10){  //&& App.getInstance().getHogar().qh40!=null){
         		 irA(CISECCION_05f_1);
         	 }
          	if(id==11){  //&& App.getInstance().getHogar().qh40!=null){
        		 
         	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
          		 Integer int320= App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
                	 
                	if (int320==1 || int320==2) {
                		 irA(CISECCION_06f_4);
      				}
                	 else {
                		 irA(CISECCION_06f_1);
                	 }
         	   }
          		
        	 }
          	
          	if(id==12){  //&& App.getInstance().getHogar().qh40!=null){
       		 irA(CISECCION_07f_1);
          	}
          	
          	if(id==13){  //&& App.getInstance().getHogar().qh40!=null){
          		 irA(CISECCION_08f_1);
            }
          	
          	if(id==14){  //&& App.getInstance().getHogar().qh40!=null){
         		 irA(CISECCION_09f_1);
          	}
         
          	if(id==15){  //&& App.getInstance().getHogar().qh40!=null){
          		Integer int106= App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
            	if (int106>=12 && int106<=14 || App.getInstance().getQhviolen()!=App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
            		Log.e("rrr","tttt");
            		irA(CISECCION_10f_12);
				}
            	else{
            		Log.e("rrr","uuu");
            		irA(CISECCION_10f_1);
            	}
            	
          		
          	}
         
        	if(id==16){  //&& App.getInstance().getHogar().qh40!=null){
        		 irA(CISECCION_10f_13);
         	}
          	
          	/* if(id==0 && App.getInstance().getHogar().qhbebe!=null){
	            	 irA(SECCION01);
          	 }*/
          	 /*if(id==1){  //&& App.getInstance().getHogar().qh40!=null){
          		 irA(CISECCION_01f_0);
          	 }
          	 if(id==2 && App.getInstance().getHogar().qh93!=null){
          		 irA(SECCION03);
          	 }
          	 if(id==3 && (App.getInstance().getHogar().qh101!=null || App.getInstance().getHogar().qh103!=null)){
          		 irA(SECCION03B);
          	 }
          	 if(id==4 && App.getInstance().getHogar().existeseccion04 && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1){
          		 irA(SECCION04);
          	 }
          	 if(id==5 && App.getInstance().getHogar().existeseccion04 && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1){
          		 irA(SECCION05);
          	 }
          	 if(id==6 && App.getInstance().getHogar().qh224!=null){
          		 irA(SECCION06);
          	 }*/
           }
           
           if(item.getGroupId()==5){
        	   if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null && App.getInstance().getVisita()!=null && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1)
        		 
        	   	
        		 Log.e("ir a reportes ","reporte de hogar");
	          	 irA(REPORTEANTROP);
           }
           
           
             
             if (id == R.id.action_ir_marco) {
                    irA(MARCO);
             }
             else if (id == R.id.action_grabar) {
                    grabarYContinuar();
                    MyUtil.GetDatosDeGeolocalizacion();
             }
             else if(id==R.id.action_grabar_parcial){
            	 grabadoyregresoacaratula();
             }
             else if(id==R.id.action_ir_hogar){
                    if(App.getInstance().getMarco()!=null){
                    	irA(CARATULA);
                    }
                    
             }
//             else if(id==R.id.action_ir_reporhemo){
//            	 
//            	 if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null && App.getInstance().getVisita()!=null && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1)
//	          		 Log.e("ir a reportes ","reporte de hogar");
//	          	 irA(REPORTEANTROP);
//            	 
//            	 
////                 if(App.getInstance().getMarco()!=null){
////                 	irA(REPORTEANTROP);
////                 }
//                 
//             }
             
             
             
             /*
             else if (id == R.id.action_visitasindividual) {
                    if (viewPager.getCurrentItem() != MARCO && App.getInstance().getSaludNavegacionS4_7()!=null && App.getInstance().getSaludNavegacionS4_7().existemef && App.getInstance().getVisita()!=null && App.getInstance().getVisita().qhvresul!=null && App.getInstance().getVisita().qhvresul==1) {
                    		App.getInstance().setPersonaCuestionarioIndividual(null);
                           irA(CaratulaCI);
                    }
             }*/
             
             
                    
             return super.onOptionsItemSelected(item);
       }

       public enum OBSV {
             LOCAL, INICIAL, PRIMARIA, SECUNDARIA
       };

       private void abrirObservaciones(OBSV modulo) {
             FragmentManager fm = this.getSupportFragmentManager();
             // ObservacionesDialog dialog = ObservacionesDialog.newInstance(this,
             // modulo);
             // dialog.show(fm, "obsDialog");
       }

       private void grabarYContinuar() {
             this.grabarYContinuar(viewPager.getCurrentItem() + 1);
       }
       private void grabadoyregresoacaratula(){
    	   this.grabadoParcial(viewPager.getCurrentItem());
       }

       private void irA(int position) {
             nextFragment(position);
       }

       private void grabadoParcial(int position){
    	   action = PROCCES.GRABADOPARCIAL;
    	   int posicionActual= viewPager.getCurrentItem();
    	   
    	   if(posicionActual!=MARCO && posicionActual!=CARATULA && posicionActual!=VISITA){
    		   Integer flag=CuestionarioFragmentActivity.this.pageAdapter.getItem(posicionActual).grabadoParcial();
    		   DialogComponent dialog = new DialogComponent(this, this,
                       TIPO_DIALOGO.YES_NO, getResources()
                                    .getString(R.string.app_name),
                       "Desea ir a Visita?");
    		   dialog.showDialog();    		   
    	   }
    	   
       }
       private void grabarYContinuar(int position) {
             int posicionActual = viewPager.getCurrentItem();
             boolean flag = CuestionarioFragmentActivity.this.pageAdapter.getItem(posicionActual).grabar();
             if (flag && posicionActual != MARCO) {
                    esArrastre = true;
                    if (posicionActual == viewPager.getCount() - 1) {
                           
                           ToastMessage.msgBox(this, "Registro Finalizado",
                                        ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
                           nextFragment(CARATULA);
                    } else {
                           nextFragment(position);
                    }
                    return;
             } else {
                    esArrastre = false;
             }
       }

       public void setTitulo(String titulo) {
             TITULO = getResources().getString(R.string.app_name);
             if (titulo != null) {
                    if (!"".equals(titulo)) {
                           TITULO += " " + titulo;
                    }
             }
             getActionBar().setTitle(TITULO);
       }

       @Override
       public boolean onKeyDown(int keyCode, KeyEvent event) {
             if (keyCode == KeyEvent.KEYCODE_BACK) {
                    new AlertDialog.Builder(this)
                                  .setIcon(android.R.drawable.ic_dialog_alert)
                                  .setTitle("Salir")
                                  .setMessage(
                                               "Esta a punto de salir de la aplicaci\u00f3n, se perder\u00e1 todo aquello que no haya guardado. \u00bfEsta seguro que desea salir del sistema?")
                                  .setPositiveButton("Si",
                                               new DialogInterface.OnClickListener() {
                                                      @Override
                                                      public void onClick(DialogInterface dialog,
                                                                   int which) {
                                                            finish();
                                                      }
                                               }).setNegativeButton("No", null).show();
             }
             return super.onKeyDown(keyCode, event);
       }

       @Override
       public void onTabReselected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       public void onTabSelected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       public void onTabUnselected(Tab tab, FragmentTransaction ft) {
             // TODO Auto-generated method stub

       }

       @Override
       protected void onResume() {
             super.onResume();
             if (preferencias.getBoolean("primera_ejecucion", true)) {
                    editor.putBoolean("primera_ejecucion", false);
                    editor.commit();
                    drawerLayout.postDelayed(new Runnable() {
                           @Override
                           public void run() {
                                  drawerLayout.openDrawer(Gravity.LEFT);
                           }
                    }, 500);
             }

       }

       @Override
       protected void onSaveInstanceState(Bundle outState) {
             // super.onSaveInstanceState(outState);
             outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
       }

       private class CuestionarioSimpleOnPageChangeListener extends
                    FragmentViewPager.SimpleOnPageChangeListener {

             private boolean jump = true;
             private boolean pasada = true;

             @Override
             public void onPageScrollStateChanged(int state) {
                    
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                           CuestionarioFragmentActivity.this.setPrevPage(viewPager
                                        .getCurrentItem());
                           esArrastre = true;
                           // pasada = false;
                    }
                    super.onPageScrollStateChanged(state);
             }

             @Override
             public void onPageSelected(final int position) {

                    CuestionarioFragmentActivity.this.setCurPage(position);
                    int prevPage = CuestionarioFragmentActivity.this.getPrevPage();
                                        
                    // VALIDACIONES AVANCES
                    if (!validarAvances(prevPage, position))
                           return;
                    boolean flag = false;
                    if (prevPage < position && debeGrabar) {
                           flag = CuestionarioFragmentActivity.this.pageAdapter.getItem(
                                        prevPage).grabar();
                           MyUtil.GetDatosDeGeolocalizacion();
                           if (!flag) {
                                  esArrastre = false;
                                  nextFragment(prevPage);
                                  return;
                           }
                           esArrastre = true;
                    }


                    // RETROCESOS
                    int diferencia = position - prevPage;
                    if (diferencia < 2) {
                           setEsSalto(false);
                    }
                    if (esArrastre) {
                           if (!ejecutarRetrocesos(prevPage, position)) {
                                  return;
                           } else {		
                        	   				
                                         CuestionarioFragmentActivity.this.pageAdapter.getItem(
                                               position).cargarDatos();                           				
                           }
                    }
                    if (!ejecutarAvances(prevPage, position)) {
                          
                           return;
                    }
                    validarSubmenu();
                    debeGrabar = true;
             }
       }
       
       public void validarSubmenu() {
    	   List<Integer> lista_visibles = new ArrayList<Integer>();
    	   List<Integer> lista_ocultos = new ArrayList<Integer>();
    	   if(viewPager.getCurrentItem() < CISECCION_01f_0) {
    		   menu.getItem(0).setVisible(false);
    		   visibleSubmenuIndividual(false,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
            }
    	   else {
    		   menu.getItem(0).setVisible(true);    		   
    		   lista_visibles.add(1);
    	   }
    	   
    	   
    	if(viewPager.getCurrentItem() >= CISECCION_01f_0 && viewPager.getCurrentItem() <= CISECCION_10_2f_2 ) {
    	   
    		Menu me2 = menu.getItem(0).getSubMenu();
    		MenuItem mi1= me2.getItem(0);
    		MenuItem mi2= me2.getItem(1);
    		MenuItem mi3= me2.getItem(2);
    		MenuItem mi4= me2.getItem(3);
    		MenuItem mi5= me2.getItem(4);
    		
    		Menu m5 = mi5.getSubMenu();
    		MenuItem mi5_1 = m5.getItem(0);
    		MenuItem mi5_2 = m5.getItem(1);
    		MenuItem mi5_3 = m5.getItem(2);
    		MenuItem mi5_4 = m5.getItem(3);
    		MenuItem mi5_5_1 = m5.getItem(4);
    		MenuItem mi5_5_2 = m5.getItem(5);
    		MenuItem mi5_6 = m5.getItem(6);
    		MenuItem mi5_7 = m5.getItem(7);
    		MenuItem mi5_8 = m5.getItem(8);
    		MenuItem mi5_9 = m5.getItem(9);
    		MenuItem mi5_10 = m5.getItem(10);
    		MenuItem mi5_11 = m5.getItem(11);
    		MenuItem mi5_12 = m5.getItem(12);
    		MenuItem mi5_13 = m5.getItem(13);
    		MenuItem mi5_14 = m5.getItem(14);
    		
    		MenuItem mi6= me2.getItem(5);
    		MenuItem mi7= me2.getItem(6);
    		MenuItem mi8= me2.getItem(7);
    		MenuItem mi9= me2.getItem(8);
    		MenuItem mi10= me2.getItem(9);
    		MenuItem mi11= me2.getItem(10);
    		MenuItem mi12= me2.getItem(11);
    		
    		Seccion01 s1 =  App.getInstance().getPersonaCuestionarioI();
    		if(s1!=null) {
	    		mi1.setTitle(R.string.menu_ver_nombre);
	    		mi1.setTitle(mi1.getTitle().toString().replace("-", s1.getNombre())); 
    		}
    		
    		CISECCION_01_03 s1_3 = App.getInstance().getPersonaCuestionarioIndividual();
    		
    		if(s1_3!=null ) {
	    		if(s1_3.qi105d!=null &&  s1_3.qi105m!=null &&  s1_3.qi105y!=null) {
	    			mi2.setTitle(R.string.menu_ver_fech_nac);
		    		mi2.setTitle(mi2.getTitle().toString().replace("-",s1_3.qi105d+"/"+s1_3.qi105m+"/"+s1_3.qi105y));    			
	    		}
	    		else{
	    			mi2.setTitle(R.string.menu_ver_fech_nac);
	    		}
	    		
	    		if(s1_3.qi106!=null ) {
	    			mi3.setTitle(R.string.menu_ver_edad);
		    		mi3.setTitle(mi3.getTitle().toString().replace("-",s1_3.qi106.toString()));
	    		}
	    		else{
	    			mi3.setTitle(R.string.menu_ver_edad);
	    		}
	    		if(s1_3.qi215!=null) {
	    			mi4.setTitle(R.string.menu_ver_fech_nac_hij_may);
		    		mi4.setTitle(mi4.getTitle().toString().replace("-",s1_3.qi215));
	    		}
	    		else{
	    			mi4.setTitle(R.string.menu_ver_fech_nac_hij_may);
	    		}
	    		if(s1_3.fechaPrimerAborto!=null) {
	    			mi6.setTitle(R.string.menu_ver_primer_aborto);
		    		mi6.setTitle(mi6.getTitle().toString().replace("-",s1_3.fechaPrimerAborto));
	    		}
	    		else{
	    			mi6.setTitle(R.string.menu_ver_primer_aborto);
	    		}
	    		if(s1_3.qi307!=null) {
	    			mi7.setTitle(R.string.menu_ver_P307);
		    		mi7.setTitle(mi7.getTitle().toString().replace("-",s1_3.qi307.toString()));
	    		}
	    		else{
	    			mi7.setTitle(R.string.menu_ver_P307);
	    		}
	    		if(s1_3.qi405!=null) {
	    			mi8.setTitle(R.string.menu_ver_P405);
	    			
	    			String descripcion = s1_3.qi405.toString();
	    			if(s1_3.qi405.equals("1")) 
	    				descripcion = "QUER�A QUEDAR EMBARAZADA ENTONCES";
	    			if(s1_3.qi405.equals("2")) 
	    				descripcion = "QUER�A ESPERAR M�S";
	    			if(s1_3.qi405.equals("8")) 
	    				descripcion = "NO QUER�A M�S";
	    			mi8.setTitle(mi8.getTitle().toString().replace("-",descripcion));
	    		}
	    		else{
	    			mi8.setTitle(R.string.menu_ver_P405);
	    		}
	    		if(s1_3.qi409!=null) {
	    			mi9.setTitle(R.string.menu_ver_P409);
		    		mi9.setTitle(mi9.getTitle().toString().replace("-",s1_3.qi409.toString()));
	    		}
	    		else{
	    			mi9.setTitle(R.string.menu_ver_P409);
	    		}
	    		if(s1_3.qi411_h!=null) {
	    			mi10.setTitle(R.string.menu_ver_P411_H);
	    			
		    		String descripcion = s1_3.qi411_h.toString();
	    			if(s1_3.qi411_h.equals("1")) 
	    				descripcion = "SI";
	    			if(s1_3.qi411_h.equals("2")) 
	    				descripcion = "NO";
	    			if(s1_3.qi411_h.equals("8")) 
	    				descripcion = "NO SABE";
	    			mi10.setTitle(mi10.getTitle().toString().replace("-",descripcion));
	    		}
	    		else{
	    			mi10.setTitle(R.string.menu_ver_P411_H);
	    		}
	    		
	    		if(s1_3.qi302_01!=null)
	    			if(s1_3.qi302_01==1)
	    				mi5_1.setVisible(true);
	    			else
	    				mi5_1.setVisible(false);
	    		else
	    			mi5_1.setVisible(false);
	    		
	    		if(s1_3.qi302_02!=null)
	    			if(s1_3.qi302_02==1)
	    				mi5_2.setVisible(true);
	    			else
	    				mi5_2.setVisible(false);
	    		else
	    			mi5_2.setVisible(false);
	    		
	    		if(s1_3.qi302_03!=null)
	    			if(s1_3.qi302_03==1)
	    				mi5_3.setVisible(true);
	    			else
	    				mi5_3.setVisible(false);
	    		else
	    			mi5_3.setVisible(false);
	    		
	    		if(s1_3.qi302_04!=null)
	    			if(s1_3.qi302_04==1)
	    				mi5_4.setVisible(true);
	    			else
	    				mi5_4.setVisible(false);
	    		else
	    			mi5_4.setVisible(false);
	    		
	    		// modificar aqui para ver metodos en el menu
	    		if(s1_3.qi302_05a!=null)
	    			if(s1_3.qi302_05a==1)
	    				mi5_5_1.setVisible(true);
	    			else
	    				mi5_5_1.setVisible(false);
	    		else
	    			mi5_5_1.setVisible(false);
	    		
	    		
	    		if(s1_3.qi302_05b!=null)
	    			if(s1_3.qi302_05b==1)
	    				mi5_5_2.setVisible(true);
	    			else
	    				mi5_5_2.setVisible(false);
	    		else
	    			mi5_5_2.setVisible(false);
	    		
	    		
	    		if(s1_3.qi302_06!=null)
	    			if(s1_3.qi302_06==1)
	    				mi5_6.setVisible(true);
	    			else
	    				mi5_6.setVisible(false);
	    		else
	    			mi5_6.setVisible(false);
	    		
	    		if(s1_3.qi302_07!=null)
	    			if(s1_3.qi302_07==1)
	    				mi5_7.setVisible(true);
	    			else
	    				mi5_7.setVisible(false);
	    		else
	    			mi5_7.setVisible(false);
	    		
	    		if(s1_3.qi302_08!=null)
	    			if(s1_3.qi302_08==1)
	    				mi5_8.setVisible(true);
	    			else
	    				mi5_8.setVisible(false);
	    		else
	    			mi5_8.setVisible(false);
	    		
	    		if(s1_3.qi302_09!=null)
	    			if(s1_3.qi302_09==1)
	    				mi5_9.setVisible(true);
	    			else
	    				mi5_9.setVisible(false);
	    		else
	    			mi5_9.setVisible(false);
	    		
	    		if(s1_3.qi302_10!=null)
	    			if(s1_3.qi302_10==1)
	    				mi5_10.setVisible(true);
	    			else
	    				mi5_10.setVisible(false);
	    		else
	    			mi5_10.setVisible(false);
	    		
	    		if(s1_3.qi302_11!=null)
	    			if(s1_3.qi302_11==1)
	    				mi5_11.setVisible(true);
	    			else
	    				mi5_11.setVisible(false);
	    		else
	    			mi5_11.setVisible(false);
	    		
	    		if(s1_3.qi302_12!=null)
	    			if(s1_3.qi302_12==1)
	    				mi5_12.setVisible(true);
	    			else
	    				mi5_12.setVisible(false);
	    		else
	    			mi5_12.setVisible(false);
	    		
	    		if(s1_3.qi302_13!=null)
	    			if(s1_3.qi302_13==1)
	    				mi5_13.setVisible(true);
	    			else
	    				mi5_13.setVisible(false);
	    		else
	    			mi5_13.setVisible(false);
	    		
	    		if(s1_3.qi302_14!=null)
	    			if(s1_3.qi302_14==1)
	    			{
	    				mi5_14.setTitle(R.string.menu_ver_ant14);
	    				mi5_14.setTitle(mi5_14.getTitle().toString().replace("-",s1_3.qi302o!=null?s1_3.qi302o:""));
	    				
	    				mi5_14.setVisible(true);
	    			}
	    			else
	    				mi5_14.setVisible(false);
	    		else
	    			mi5_14.setVisible(false);
    		}	
    		
    		CISECCION_05_07 ci5_7 = App.getInstance().getCiseccion05_07();
    		if(ci5_7!=null) {
    			if(ci5_7.qi509m!=null && ci5_7.qi509y!=null) {
	    			mi11.setTitle(R.string.menu_ver_P509);
		    		mi11.setTitle(mi11.getTitle().toString().replace("-",ci5_7.qi509m+"/"+ci5_7.qi509y.toString()));
	    		}
    			else{
    				mi11.setTitle(R.string.menu_ver_P509);
    			}
    			if(ci5_7.qi512!=null) {
	    			mi12.setTitle(R.string.menu_ver_P512);
	    			String descripcion = ci5_7.qi512.toString();
	    			if(ci5_7.qi512==0) 
	    				descripcion = "Nunca";
	    			if(ci5_7.qi512==95) 
	    				descripcion = "Cuando se Cas�";
		    		mi12.setTitle(mi12.getTitle().toString().replace("-",descripcion));
	    		}
    			else{
    				mi12.setTitle(R.string.menu_ver_P512);
    			}
    		}
    		/*
    		menu_ver_P509
    		menu_ver_P512*/
    		
    		
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().qi102!=null) {
    		   lista_visibles.add(2);
    	   } 
    	   else 
    		   lista_ocultos.add(2);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    	   	  App.getInstance().getPersonaCuestionarioIndividual().qi201!=null) {
    	      lista_visibles.add(3);
    	   }
    	   else 
    		   lista_ocultos.add(3);    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().qi208!=null &&
    	      App.getInstance().getPersonaCuestionarioIndividual().qi208>0) {
    		   lista_visibles.add(4);
    	   }
    	   else 
    		   lista_ocultos.add(4);
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().qi302_01!=null) {
    	       lista_visibles.add(5);
    	   }
    	   else 
    		   lista_ocultos.add(5);
       	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {

    	      Integer int304 = App.getInstance().getPersonaCuestionarioIndividual().qi304==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi304;
    	      Integer int310 = App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
    	      Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
    	      Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
    	      Integer int307 = App.getInstance().getPersonaCuestionarioIndividual().qi307==null?-1:App.getInstance().getPersonaCuestionarioIndividual().qi307;
    	      Integer mayor=int315>int316?int315:int316;
    	      if (Util.esDiferente(int304, 0,2) || int310!=0 || mayor>=App.ANIOPORDEFECTO || int307!=-1) {//calendario
    	    	  lista_visibles.add(6);		 
    	      }    	      	 
    	   }    	    	   
    	   else 
    		   lista_ocultos.add(6);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a!=null &&
    		  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a>0 &&
    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3) {
    	      lista_visibles.add(7);
    	   }
    	   else 
    		   lista_ocultos.add(7);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null &&
    		  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos!=null &&
    	      (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos>0 || App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true)&&
    	      App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto4a) {
    		  lista_visibles.add(8); 
    	   }    	   
    	   else 
    		   lista_ocultos.add(8);
    	   
    /*	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null &&
    		   App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4dit!=null &&
    		   App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4dit>0 &&
    	   	   App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto4b) {
    	   	   lista_visibles.add(9); 
    	   }
    	   else 
    		   lista_ocultos.add(9);*/
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null &&
    		  App.getInstance().getSeccion04B2().filtro479 &&
    		  App.getInstance().getSeccion04B2().qi479a!=null) {
    		  lista_visibles.add(9);
    	   }
    	   else 
    		   lista_ocultos.add(9);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getCiseccion05_07()!=null &&
    		  App.getInstance().getCiseccion05_07().qi501!=null &&
    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3) {
    		  lista_visibles.add(10);
    	   }
    	   else 
    		   lista_ocultos.add(10);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
    		 Integer int320= App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
         	   if(App.getInstance().getCiseccion05_07()!=null &&
	    	      App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 && 
	    	      (((int320==1 || int320==2 ) && App.getInstance().getCiseccion05_07().qi614!=null) || 
	    	      (!(int320==1 || int320==2 ) && App.getInstance().getCiseccion05_07().qi602!=null))
	    	         ) {
	    	      lista_visibles.add(11);
	    	   }
    	   }
    	   else 
    		   lista_ocultos.add(11);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    	    		  App.getInstance().getCiseccion05_07()!=null &&
    	    		  App.getInstance().getCiseccion05_07().qi707!=null &&
    	    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3) {
    	    		  lista_visibles.add(12);
    	   }
    	   else 
    		   lista_ocultos.add(12);
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
 	    		  App.getInstance().getCiseccion_08()!=null &&
 	    		  App.getInstance().getCiseccion_08().qi801a!=null &&
 	    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 &&
 	    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 
 	    		  ) {
 	    		  lista_visibles.add(13);
 	    	   }
    	   else 
    		   lista_ocultos.add(13);
    	   /*
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
		      App.getInstance().getCiseccion05_07()!=null &&
		      App.getInstance().getCiseccion05_07().qi702!=null &&
		      App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3) {
    	      lista_visibles.add(13);
    	   } */
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
  	    		  App.getInstance().getCiseccion_08()!=null &&
  	    		  App.getInstance().getCiseccion_08().qi901!=null &&
  	    				App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 &&
  	    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7
  	    		  ) {
  	    		  lista_visibles.add(14);
  	    	   }
     	   else 
     		   lista_ocultos.add(14);

    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 &&
	    	  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 && 
	    	  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8) {
    		   
    		  if(App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id &&
    			 App.getInstance().getCiseccion_10_01()!=null &&
    		     App.getInstance().getCiseccion_10_01().qi1000a!=null ) 
    			  lista_visibles.add(15);
    		  else if (App.getInstance().getQhviolen()!=App.getInstance().getPersonaCuestionarioIndividual().persona_id &&
    		  App.getInstance().getCiseccion_10_01().qi1043!=null)
    			  lista_visibles.add(15);
    		  else 
    			  lista_ocultos.add(15);
    	   }
      	   else 
      		   lista_ocultos.add(15);
    	   
    	   
    	   if(App.getInstance().getPersonaCuestionarioIndividual()!=null && 
    		  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 &&
	    	  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 && 
	    	  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8 && 
	    	  App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto10 && 
	    	  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos>0 &&
	    	  App.getInstance().getPersonaCuestionarioIndividual().viotartarjeta) {
    		  
    		  lista_visibles.add(16);
    	
    	   }
      	   else 
      		   lista_ocultos.add(16);
    	   
    	   
    	   if(lista_ocultos.size()>0) {
    		   visibleSubmenuIndividual(false,lista_ocultos.toArray(new Integer[lista_ocultos.size()]));
    	   }
    	   
    	   if(lista_visibles.size()>0) {
    		   visibleSubmenuIndividual(true,lista_visibles.toArray(new Integer[lista_visibles.size()]));
    	   }
    	       	   
    	   /*else {
          	 visibleSubmenuIndividual(true,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
           }*/
    	   
    	}
       }
       
       
       
              
       private boolean validarAvances(int prevPage, int curPage) {
             if (prevPage >= curPage) {
                    return true;
             }
             if(prevPage==SECCION06 && curPage ==CSVISITA && ((prevPage+1)==curPage)){
            	 Log.e("&&&& ir"," blanquea 00 000 ");
            	 blanquear=true;
             }
             if(prevPage==CSVISITA && curPage ==SECCION06 && ((prevPage+1)==curPage)){
            	 Log.e("&&&& ir"," blanquea 00 111 ");
            	 blanquear=true;
            }
             if(prevPage==SECCION05 && curPage ==SECCION06 && ((prevPage+1)==curPage)){
            	 Log.e("&&&& ir"," blanquea 00 222 ");
           	     blanquear=true;
            }
            if (prevPage == VISITA && curPage == SECCION01 && ((prevPage+1)==curPage)) {
                    if ((App.getInstance().getVisita() != null && !Util.esDiferente(App.getInstance().getVisita().qhvresul,2,3,4,5,6,7,8,9)) || App.getInstance().getVisita()==null) {
                           esArrastre = false;
                           ToastMessage.msgBox(CuestionarioFragmentActivity.this,
                                        "Aperture una visita primero ",
                                        ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                           nextFragment(prevPage);
                           return false;
                    }
             }
            if (prevPage == CISECCION_01f_0 && curPage == CISECCION_01f_1  && ((prevPage+1)==curPage)) {
                if (App.getInstance().getVisitaI()==null) {  //(App.getInstance().getVisitaI() != null && !Util.esDiferente(App.getInstance().getVisitaI().qivresul,2,3,4,6,7,8,9)) ||
                       esArrastre = false;
                       ToastMessage.msgBox(CuestionarioFragmentActivity.this,
                                    "Aperture una visita primero ",
                                    ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                       nextFragment(prevPage);
                       return false;
                }
            }
             if (prevPage == CSVISITA && curPage == CSSECCION1f_2 && ((prevPage+1)==curPage)) {
            	 
                 if ((App.getInstance().getC1Visita() != null && !Util.esDiferente(App.getInstance().getC1Visita().qsvresul,2,3,6,9)) || App.getInstance().getC1Visita()==null) {
                        esArrastre = false;
                        ToastMessage.msgBox(CuestionarioFragmentActivity.this,
                                     "Aperture una visita primero ",
                                     ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                        nextFragment(prevPage);
                        return false;
                 }
             }
             
             if (prevPage == CARATULA && curPage ==VISITA && ((prevPage+1)==curPage)){
                    if (App.getInstance().getHogar() == null) {
                           esArrastre = false;
                           nextFragment(prevPage);
                           return false;
                    }
             }
             

             if(prevPage==CSSECCION8f_1 && App.getInstance().getPersonabySeccion8Salud()==null && curPage==CSSECCION8f_2 && ((prevPage+1)==curPage)){         
                   esArrastre=false;                  
                   nextFragment(prevPage);
                   return false;
            }
             
             if (prevPage==SECCION01 && prevPage<curPage && curPage==CHSECCION1f_2 && App.getInstance().getEditarPersonaSeccion01()==null &&((prevPage+1)==curPage)) {
            	 Log.e("sss","discaoa");
            	 esArrastre=false;
            	 nextFragment(prevPage);
            	 return false;
			}
            if(prevPage==CISECCION_f_carat && prevPage<curPage && curPage== CISECCION_01f_1 && App.getInstance().getPersonaCuestionarioIndividual()==null &&((prevPage+1)==curPage)){
            	esArrastre=false;
            	ToastMessage.msgBox(CuestionarioFragmentActivity.this,
                        "Seleccione una mef",
                        ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
           	 	nextFragment(prevPage);
           	 	return false;
            }
        /*     
             if(prevPage==CISECCION_04a_caratula && curPage==CISECCION_04a_caratula+1 && !isEsAvanceDual() && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage){
            	 Log.e("Aki","27");
                    esArrastre=false;
                    debeGrabar=false; 
             }  
             
             if(prevPage==CISECCION_04a_caratula+1 && curPage==C2SECCION_04b_caratula && !isEsAvanceDual() && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage){
            	 Log.e("Aki","27");
                    esArrastre=false;
                    debeGrabar=false; 
             }  
            */ 
   	         esArrastre = true;
             return true;
       }
              
       
       
       
       
       
       
       
       private boolean ejecutarAvances(int prevPage, int curPage) {
             Log.d("Avance ","paginas "+ prevPage+" "+curPage +" =>> arrastre "+esArrastre + " salto "+esSalto);
             if (curPage < prevPage) {
                    return true;
             }
             if (curPage - prevPage > 1) {
                    return true;
             }
             if(prevPage==HOGARFRAGMENTO8 && App.getInstance().getHogar().hogar_id!=1 && (prevPage+1==curPage)){      
                    esArrastre = true;
                    nextFragment(HOGARMAYORADOS);
                    return true;
             }
             if(prevPage==REPORTEHOGAR){
//             if(prevPage==REPORTEHOGAR && band==false){
            	 Log.e("evaluar","donde va");
                    esArrastre=true;
                    nextFragment(REPORTEANTROP);
                    return true;
             }
             
             if(prevPage==REPORTEANTROP){
               //if(prevPage==REPORTEHOGAR && band==false){
              	 Log.e("evaluar","donde va");
	              	 esArrastre=true;
	                 nextFragment(CARATULA);
	                 return true;
             }
             
             
             if(prevPage==SECCION8INCENTIVOS){
            	 Log.e("evaluar","donde va el incentivos");
	            	esArrastre=true;
	            	debeGrabar=false;
                    nextFragment(CARATULA);
                    return true;
             }
             
//             if(prevPage==SECCION8INCENTIVOS 
//            		 && App.getInstance().getCiseccion_10_01() !=null 
//            		 && App.getInstance().getPersonaCuestionarioIndividual()!=null 
//            		 &&  prevPage<curPage && (prevPage+1==curPage)){
//            	 
//            	 	Log.e("evaluar","donde va el incentivos");
//            		 esArrastre=true;
//            		 debeGrabar=false;
//            		 nextFragment(CARATULA);
//            		 return false;
//             }
             
             if(prevPage==SECCIONPOSTCENSO && band==false){
            	 Log.e("evaluar","donde va el censo");
                    esArrastre=true;
                    nextFragment(CARATULA);
                    return true;
             }
             
             if(prevPage==CSVISITA && App.getInstance().getC1Visita()!=null && App.getInstance().getC1Visita().qsresult!=null && App.getInstance().getC1Visita().qsresult!=5 && MyUtil.incluyeRango(2, 9, App.getInstance().getC1Visita().qsresult) && (prevPage+1==curPage)){
                 esArrastre=true;
                 nextFragment(CARATULA);                  
                 return false;            		 
            }
             
             if(prevPage==CHSECCION1f_2 && App.getInstance().getEditarPersonaSeccion01()!=null && App.getInstance().getEditarPersonaSeccion01().qh07!=null && App.getInstance().getEditarPersonaSeccion01().qh07<3 && (prevPage+1==curPage)){
                 Log.e("pase de menores","a super");
                 esArrastre=true;                  
                 nextFragment(CHSECCION1f_4);
                 return false;
             }
//             if(prevPage==CHSECCION1f_3 && App.getInstance().getEditarPersonaSeccion01()!=null && App.getInstance().getEditarPersonaSeccion01().qh07!=null && App.getInstance().getEditarPersonaSeccion01().qh07>14 && (prevPage+1==curPage)){
//                 Log.e("Mayor a 14","AMyor a 14");
//                 esArrastre=true;                  
//                 nextFragment(SECCION01);
//                 App.getInstance().setEditarPersonaSeccion01(null);
//                 return false;
//             }
//             if(prevPage==CHSECCION1f_4 && App.getInstance().getEditarPersonaSeccion01()!=null && App.getInstance().getEditarPersonaSeccion01().qh07!=null && App.getInstance().getEditarPersonaSeccion01().qh07<15 && (prevPage+1==curPage)){
//                 Log.e("menores a 14","menor a 14");
//                 esArrastre=true;                  
//                 nextFragment(SECCION01);
//                 App.getInstance().setEditarPersonaSeccion01(null);
//                 return false;
//             }
             
             if(prevPage==CHSECCION1f_4 && App.getInstance().getEditarPersonaSeccion01()!=null && (prevPage+1==curPage)){
                 Log.e("TODOS","SSALTA AL LISTADO");
                 esArrastre=true;                  
                 nextFragment(SECCION01);
                 App.getInstance().setEditarPersonaSeccion01(null);
                 return false;
             }
             

             if(prevPage==HOGARMAYORADOS && App.getInstance().getEditarPersonaSeccion01()!=null && App.getInstance().getEditarPersonaSeccion01().todaslaspersonasmenoresa16==true && (prevPage+1==curPage)){
                 Log.e("menores a 15","PASE A JUNTOS o PENSION 65");
                 esArrastre=true;                  
                 nextFragment(QHSECCION2f_11);
                 return false;
             }
             

             if (prevPage==CSSECCION2f_9 && App.getInstance().getPersonabySalud()!=null) {
            	   if(App.getInstance().getPersonabySalud().qs206>1 || App.getInstance().getPersonabySalud().qs208>1 || App.getInstance().getPersonabySalud().qs210>1 || App.getInstance().getPersonabySalud().qs211u>1 && (prevPage+1==curPage)){
            		   Log.e("Aki","11");
                       esArrastre=true;                  
                       nextFragment(CSSECCION2f_11);      
                       return false;
                   }
			}
             
             if(curPage==CSSECCION3f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23<50){
        		   Log.e("Aki","1222");
        		   esArrastre=true;
        		   nextFragment(CSSECCION4f_1);                  
        		   return false;            		 
        	   }
           }
             
             if(curPage==CSSECCION3f_3 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
            	   if(App.getInstance().getPersonabySalud().qs23!=null && MyUtil.incluyeRango(50,59,App.getInstance().getPersonabySalud().qs23)){
            		   Log.e("Aki","3333");
            		   esArrastre=true;
            		   nextFragment(CSSECCION4f_1);                  
            		   return false;            		 
            	   }
             }             
             
          
           //Hasta el a�om 2019
           /*
           
          
           
           if(curPage==CSSECCION4f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23>59){
        		   Log.e("Aki","44444");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
           
           if(curPage==CSSECCION4f_2 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && MyUtil.incluyeRango(15,29,App.getInstance().getPersonabySalud().qs23)){
        		   Log.e("Aki","55555");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
           if(curPage==CSSECCION4f_2 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qh06!=null && MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==2){
        		   Log.e("Aki","6666");
        		   esArrastre=true;
        		   nextFragment(CSSECCION4f_3);                  
        		   return false;            		 
        	 	}
         	}
           if(curPage==CSSECCION4f_2 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==1){
        		   Log.e("Aki","666611100");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
           if(curPage==CSSECCION4f_3 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(MyUtil.incluyeRango(30,59,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==1){
        		   Log.e("Aki","6666111");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
   
           if(curPage==CSSECCION4f_4 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23)){
        		   Log.e("Aki","777");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
           
           if(curPage==CSSECCION4f_4 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
        	   if(MyUtil.incluyeRango(40,59,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==1){
        		   Log.e("Aki","777111");
        		   esArrastre=true;
        		   nextFragment(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}*/
           
        
             
           if(prevPage==CSSECCION3f_3 && curPage==CSSECCION4f_1 && App.getInstance().getPersonabySalud()!=null && prevPage<curPage && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23>75){
        		   Log.e("Aki","XX1");
        		   esArrastre=true;
        		   debeGrabar=false;
        		   irA(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
           
           if(prevPage==CSSECCION4f_1 && curPage==CSSECCION4f_2 && App.getInstance().getPersonabySalud()!=null && prevPage<curPage && (prevPage+1==curPage)){
        	   if(App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23<18 || App.getInstance().getPersonabySalud().qs23>75) ){
        		   Log.e("Aki","XX2");
        		   esArrastre=true;      
        		   //debeGrabar = false;
        		   irA(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
           }  
           
           if(prevPage==CSSECCION4f_2 && curPage==CSSECCION4f_3 && App.getInstance().getPersonabySalud()!=null && prevPage<curPage && (prevPage+1==curPage)){
        	   if( App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23<18 || (App.getInstance().getPersonabySalud().qh06==1 && (App.getInstance().getPersonabySalud().qs23<50 || App.getInstance().getPersonabySalud().qs23>75)))){
        		   Log.e("qs23",""+App.getInstance().getPersonabySalud().qs23);
        		   Log.e("qh06",""+App.getInstance().getPersonabySalud().qh06);
        		   Log.e("Aki","XX3a");
        		   esArrastre=true;
        		   //fcp se comenta por la edad 25 a�os
        		   //debeGrabar = false;
        		   irA(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
        	   if( App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23>=50 && App.getInstance().getPersonabySalud().qs23<=75 && App.getInstance().getPersonabySalud().qh06==1) ){
        		   Log.e("qs23",""+App.getInstance().getPersonabySalud().qs23);
        		   Log.e("qh06",""+App.getInstance().getPersonabySalud().qh06);
        		   Log.e("Aki","XX3b");
        		   esArrastre=true;   
        		   //debeGrabar = false;
        		   irA(CSSECCION4f_4);                  
        		   return false;            		 
        	 	}
         	} 
           if(prevPage==CSSECCION4f_3 && curPage==CSSECCION4f_4 && App.getInstance().getPersonabySalud()!=null && prevPage<curPage && (prevPage+1==curPage)){
        	   if( App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23<40 && App.getInstance().getPersonabySalud().qh06==2 ){
        		   Log.e("Aki","XX4");
        		   esArrastre=true;        		   
        		   irA(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
        	   if( App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23<50 && App.getInstance().getPersonabySalud().qh06==1 ){
        		   Log.e("Aki","XX5");
        		   esArrastre=true;
        		   debeGrabar=false;
        		   irA(CSSECCION5f_1);                  
        		   return false;            		 
        	 	}
         	}
///NUEVO           
           if(curPage==CSSECCION4f_4 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage && App.getInstance().getPersonabySalud().qs23>70 && App.getInstance().getPersonabySalud().qh06==2)){        	   
        		   Log.e("Aki","XX4");
        		   esArrastre=true;        		   
        		   irA(CSSECCION5f_1);                  
        		   return false;
         	}
////           
           
           
           
           
           
            if(curPage==CSSECCION5f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
            	
            	 if(!MyUtil.incluyeRango(15, 99, App.getInstance().getPersonabySalud().qs23)){
            		 Log.e("Aki","8");
                     esArrastre=true;
                     nextFragment(CSSECCION6f_1);                  
                     return false;            		 
            	 }
             }
             if(prevPage==SECCION05 || prevPage==SECCION06){
            	 Log.e("&&&& ir"," blanquea 00 333  ");
            	 blanquear=false;
            	 blanquear2=false;
             }
//             if(prevPage==SECCION05 && band==false)	 
             if(prevPage==SECCION05){
                    esArrastre=true;
                    nextFragment(CARATULA);
                    return true;
             }
//             COVID
//             if(prevPage==SECCIONCOVID_3){
////             if(prevPage==SECCIONCOVID_2 && band==false){
//            	 esArrastre=true;
//                 nextFragment(CARATULA);
//                 return true;
//             }
             
             //if(prevPage==SECCION06 && band==false)
             if(prevPage==SECCION06){
                    esArrastre=true;
                    nextFragment(CARATULA);
                    return true;
             }
             
             if(prevPage==CSSECCION5f_1 && App.getInstance().getPersonabySalud()!=null && !MyUtil.incluyeRango(15, 49, App.getInstance().getPersonabySalud().qs23) && (prevPage+1==curPage)){
            	 Log.e("Aki","9");
                 esArrastre=true;
                 nextFragment(CSSECCION7f_1);                  
                 return false;
             }
             if(prevPage==CSSECCION5f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){ 
            	 if(MyUtil.incluyeRango(2, 2, App.getInstance().getPersonabySalud().qs29a) && MyUtil.incluyeRango(2, 2, App.getInstance().getPersonabySalud().qs29b)){
            		 Log.e("Aki","10");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_1);                  
                     return false;
            	 }
             }
             if(prevPage==CSSECCION6f_1 && App.getInstance().getPersona_04_07()!=null && (prevPage+1==curPage)){ 
            	 if(MyUtil.incluyeRango(2, 2, App.getInstance().getPersona_04_07().qs601a) && MyUtil.incluyeRango(2, 2, App.getInstance().getPersona_04_07().qs601b)){
            		 Log.e("Aki","11");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_1);                  
                     return false;
            	 }
             }
//          if(prevPage==CSSECCION6f_1 && App.getInstance().getPersonabySalud()!=null && !MyUtil.incluyeRango(15, 29, App.getInstance().getPersonabySalud().qs23) && (prevPage+1==curPage)){
            if(prevPage==CSSECCION6f_1 && App.getInstance().getPersonabySalud()!=null  
            		&& ( (MyUtil.incluyeRango(15, 29, App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonaSeccion01()!=null &&MyUtil.incluyeRango(2, 2, App.getInstance().getPersonaSeccion01().qh06)) 
            				||  MyUtil.incluyeRango(30, 49, App.getInstance().getPersonabySalud().qs23) 
            			)            			
            		&& (prevPage+1==curPage)){
            	 Log.e("Aki salud seccion 6","12");
                 esArrastre=true;
                 nextFragment(CSSECCION7f_1);                  
                 return false;
             }
             if(prevPage==CSSECCION7f_3 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
            	 if(App.getInstance().getPersonabySalud().qs209==null || !MyUtil.incluyeRango(1,1,App.getInstance().getPersonabySalud().qs209)){
            		 Log.e("Aki","13");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_7);                  
                     return false;
            	 }
             }
             if(prevPage==CSSECCION7f_4 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){ 
            	 if(preg717() && MyUtil.incluyeRango(1,1,App.getInstance().getPersonabySalud().qs209)){
            		 Log.e("Aki","14");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_7);                  
                     return false;
            	 }
             }
             if(prevPage==CSSECCION7f_7 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){            	 
            	 if(!App.getInstance().getPersona_04_07().entrars8){
            		 Log.e("Aki Salud","15");
            		 esArrastre=true;
                     nextFragment(CARATULA);                  
                     return false;
            	 }
             }

             if(prevPage==CSSECCION8f_4 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d==0 && (prevPage+1==curPage)){
                    Log.e("0a�os","0a�os111");
                    Log.e("Aki","16");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_12);                  
                    return false;
             }            
             if(prevPage==CSSECCION8f_12 && App.getInstance().getPersonabySeccion8Salud()!=null && (prevPage+1==curPage)){
                    Log.e("0a�os","0a�os11");
                    Log.e("Aki","17");
                    esArrastre=true;                  
                    nextFragment(CSSECCION8f_1);
                    App.getInstance().setPersonabySeccion8Salud(null);
                    return false;
             }
             
             if(prevPage==CSSECCION8f_5 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d<3 && (prevPage+1==curPage)){
                    Log.e("<3","Menores a 3 a�os");
                    Log.e("Aki","18");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_12);                  
                    return false;
             }
             
             if(prevPage==CSSECCION8f_9 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d<6 && (prevPage+1==curPage)){
                    Log.e("<6","Menores a 6 a�os");
                    Log.e("Aki","19");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_12);                  
                    return false;
             }

             if(prevPage==CSSECCION8f_6 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs817!=null && App.getInstance().getPersonabySeccion8Salud().qs817>1 && (prevPage+1==curPage)){
                    Log.e(">2","Cuando en la pregunta 817=1,8");
                    Log.e("Aki","20");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_9);                   
                    return false;
             }
             
             if(prevPage==CSSECCION8f_7 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs820!=null && App.getInstance().getPersonabySeccion8Salud().qs820>1 && (prevPage+1==curPage)){
                    Log.e(">2","Cuando en la pregunta 820=1,8");
                    Log.e("Aki","21");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_9);                   
                    return false;
             }
             
             if(prevPage==CSSECCION8f_10 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>5 && App.getInstance().getPersonabySeccion8Salud().qs833!=null && App.getInstance().getPersonabySeccion8Salud().qs833>1 && (prevPage+1==curPage)){
                    Log.e(">5","Cuando en la pregunta 833=1,8");
                    Log.e("Aki","22");
                    esArrastre=true;
                    nextFragment(CSSECCION8f_12);                  
                    return false;
             }
             if(prevPage==CSSECCION8f_13 && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().estadoseccion1_7==false && (prevPage+1==curPage)){
                 Log.e(">5","cuando no llego la secc 1 a 7");
                 Log.e("Aki","23");
                 esArrastre=true;
                 nextFragment(CARATULA);                  
                 return false;
             }
                   
             if(prevPage==CSSECCION1f_2 && App.getInstance().getPersonabySalud()!=null && App.getInstance().getPersonabySalud().qh06==2 && App.getInstance().getPersonabySalud().qs23<50 && (prevPage+1==curPage)){
            	 Log.e(">5","Cuando es Mef");
            	 Log.e("Aki","24");
                 esArrastre=true;
                 nextFragment(CSSECCION1f_4);                  
                 return false;
             }
             
             if(prevPage==CSSECCION8f_2 && App.getInstance().getPersonabySeccion8Salud()!=null &&  !Util.esDiferente(App.getInstance().getPersonabySeccion8Salud().qs802a,2,4,6,9) && prevPage<curPage && (prevPage+1==curPage)){
            	 esArrastre=true;
            	 Log.e("","lllllllll");
            	 Log.e("Aki","25");
            	 App.getInstance().setPersonabySeccion8Salud(null);
            	 nextFragment(CSSECCION8f_1);
                 
             } 
          //MODIFICADO PARA EL 2020
             /*if(prevPage==CSSECCION9f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage+1==curPage)){
            	if(App.getInstance().getPersona_04_07().MEF){
            		Log.e("Aki","26");
                    esArrastre=true;
                    nextFragment(CARATULA);                  
                    return false;
            	}
             }*/
             if(prevPage==CSSECCION9f_3 && App.getInstance().getPersonabySalud()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","27");
                    esArrastre=true;
                    nextFragment(CARATULA);                  
                    return false;       
                    
             }
             
             
             
             
             
             
             
             
             /*****************AVANCES CUESTIONARIO INDIVIDUAL**************/
             
             if (prevPage==CISECCION_01f_0 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Integer intViolen = App.getInstance().getQhviolen();
            	 if ((App.getInstance().getVisitaI() != null && !Util.esDiferente(App.getInstance().getVisitaI().qivresul,2,3,4,6,7,8,9)) || App.getInstance().getVisitaI()==null) {
            		 esArrastre=true;
            		 nextFragment(CISECCION_f_carat);
            		 return false;
            	 }else {
//            		 if (intViolen!=App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
//                		 esArrastre=true;
//                		 nextFragment(CISECCION_10f_12);
//                		 return false;
//                	 }else {
                		 return true;
//                	 }
				}
             }
             
             if (prevPage==CISECCION_01f_2 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Integer int108 = App.getInstance().getPersonaCuestionarioIndividual().qi108n==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108n;
            	 Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
            	 Integer int107 = App.getInstance().getPersonaCuestionarioIndividual().qi107==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi107;
            	 
            	 Integer int108y = App.getInstance().getPersonaCuestionarioIndividual().qi108y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108y;
            	 Integer int108g = App.getInstance().getPersonaCuestionarioIndividual().qi108g==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108g;
            	 
            	if (int106<App.HogEdadMefMin || int106>App.HogEdadMefMax) {
            		Log.e("","sI NO ES MEF.");
            		esArrastre=true;
            		nextFragment(CARATULA);
            		return false;
				}   
            	else{     
            		Integer intqh14=App.getInstance().getEditarPersonaSeccion01().qh14==null?0:App.getInstance().getEditarPersonaSeccion01().qh14;
            		Integer intqh15n=App.getInstance().getEditarPersonaSeccion01().qh15n==null?0:App.getInstance().getEditarPersonaSeccion01().qh15n;
            		Integer intqh15y=App.getInstance().getEditarPersonaSeccion01().qh15y==null?0:App.getInstance().getEditarPersonaSeccion01().qh15y;
            		Integer intqh15g=App.getInstance().getEditarPersonaSeccion01().qh15g==null?0:App.getInstance().getEditarPersonaSeccion01().qh15g;
            		Log.e("","CI: 106 "+int106); 
            		
            		Log.e("","CI: 107 "+int107); 
                    Log.e("","CH: 14 "+intqh14);
                    Log.e("","CI: 108n "+int108); 
                    Log.e("","CH: 15n "+intqh15n);
                    Log.e("","CI: 108y "+int108y); 
                    Log.e("","CH: 15y "+intqh15y);
                    Log.e("","CI: 108g "+int108g); 
                    Log.e("","CH: 15g "+intqh15g);
            		if(int106<25 && int107==intqh14 && int108==intqh15n && int108y==intqh15y && int108g==intqh15g){
            			 Log.e("0. pase de:  ","Cuando es: ");
            			esArrastre=true;
                  		 nextFragment(CISECCION_01f_3);
                  		 return false;
            		}
            	   	 if ((int106>=25 && !(int108==2 || int108==3||int108==4||int108==5||int108==6)) || (int106<25 && int107==2)) {
                		 Log.e("1. pase de:  ","Cuando es: !(int108==2 || int108==3||int108==4||int108==5||int108==6)");
                   		 esArrastre=true;
                   		 nextFragment(CISECCION_01f_3);
                   		 return false;
       				} 
                	 
                	if (int106>=25 && (int108==2 || int108==3||int108==4||int108==5||int108==6)) {
                		 Log.e("2. pase de:  ","Cuando es: (int108==2 || int108==3||int108==4||int108==5||int108==6)");
                		 esArrastre=true;
                		 nextFragment(CISECCION_01f_4);
                		 return false;
                	}
            	}
             }
             
             if (prevPage==CISECCION_02f_2 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Integer int208 = App.getInstance().getPersonaCuestionarioIndividual().qi208==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi208;
            	 if (int208==0) {
            		 Log.e("2. pase de: 208 ","Cuando es: 0 ");
            		 esArrastre=true;
            		 debeGrabar = false;
            		 nextFragment(CISECCION_02f_4);
            		 return false;
       			}else {
       				return true;
       			}
             }
           
            
             if (prevPage==CISECCION_02f_4 && prevPage<curPage && (prevPage+1==curPage)) {
              	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
           		if (int226==2||int226==8) {
           			Log.e("3. pase de: 226 ","Cuando es: 2,8 ");
           			esArrastre=true;
//           			debeGrabar = false;
                     nextFragment(CISECCION_02f_6);
                     return false;
       			}else {
       				return true;
  				}
             }
             
             if (prevPage==CISECCION_02f_6 && prevPage<curPage && (prevPage+1==curPage)) {
               	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
            	Integer int231 = App.getInstance().getPersonaCuestionarioIndividual().qi231_y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi231_y;
            	if (int230==2 || int231<App.ANIOPORDEFECTO) {
            		Log.e("4. pase de: 230 , 231_a�o ","Cuando es: 230 es 2 � 321_a�o < 2011 ");
            		esArrastre=true;
            		nextFragment(CISECCION_02f_8);
            		return false;
            	}else {
            		return true;
            	}
             }
             
//             if (prevPage==CISECCION_02f_9 && prevPage<curPage && (prevPage+1==curPage)) {
//             	Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//	           	Integer int201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
//	           	Integer int206 = App.getInstance().getPersonaCuestionarioIndividual().qi206==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi206;
//	           	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;	           	
//	           	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
//	           	Log.e("int106 ac", " " + int106);
//	           	Log.e("int201 ac", " " + int201);
//	           	Log.e("int206 ac", " " + int206);
//	           	Log.e("int226 ac", " " + int226);
//	           	Log.e("int230 ac", " " + int230);
//	           	
//             	if (int106>=12 && int106<=14  && int201==2 && int206==2 && (int226==2 || int226==8) &&int230==2) {
//             		Log.e("ultimo agregado 11","Pase mef de 12 a 14 a�os hasta hora de la seccion 10 ");
////             		esArrastre=true;
//             		esArrastre=false;
//                    debeGrabar=false;SS
//             		nextFragment(CISECCION_10f_12);             	
//             	}
//             }
             
            
             
             
             if (prevPage==CISECCION_03f_1 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Log.e("1 IDA :  ","1");
            	 Integer int310 = App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
            	 Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
            	 Integer int304 = App.getInstance().getPersonaCuestionarioIndividual().qi304==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi304;
            	 if (int304==2) {
            		 Log.e("5. pase de:  ","int304==2");
            		 esArrastre=true;
            		 debeGrabar = false;
            		 nextFragment(CISECCION_03f_8);
            		 return false;
            	 }else {
            		 if (int310==2 || int226==1) {
            			 Log.e("5. pase de:  ","int310==2 || int226==1");
            			 esArrastre=true;
            			 nextFragment(CISECCION_03f_3);
            			 return false;
            		 }else {
            			 return true;
            		 }
            	 }
             }
             
             if(prevPage==CISECCION_03f_2 && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
            	 Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
            	 Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
            	 Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
            	 Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
            	 Integer mayor=int315>int316?int315:int316;
           		
            	 if ((mayor>100 && mayor<App.ANIOPORDEFECTO) && int226!=1) {
            		
               		 if(!Util.esDiferente(int320, 0,1,2,10,11,12,13,96)){
               			Log.e("1. < 2011 f_3_2","Cuando es: int320, 0,1,2,10,11,12,13,96");
               			esArrastre=true;
               			nextFragment(CISECCION_03f_8);
               			return false;
	               	 }
	               	 if (!Util.esDiferente(int320,3,4,5,6,9,7,8)) { 
	               		 Log.e("2. < 2011 f_3_2. pase de:  ","Cuando es: int320,3,4,5,6,9,7,8");
	               		 esArrastre=true;
	               		 nextFragment(CISECCION_03f_7);
	               		 return false;
	               	 }
            	 }
             }
             
             
             
             
             if(prevPage==CISECCION_03f_3  && curPage==CISECCION_03f_4 && prevPage<curPage && (prevPage+1==curPage)){
            	 
            	 Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
            	 Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
            	 Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
            	 Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
            	 Integer mayor=int315>int316?int315:int316;
           		
            	 if (mayor>=App.ANIOPORDEFECTO || mayor==0) {
            		
            	 	 if(!Util.esDiferente(int320, 1)){
	               		 Log.e("1. >=2011 pase de:  ","Cuando es: int320, 1");
	               		 esArrastre=true;
	               		 nextFragment(CISECCION_03f_5);
	               		 return false;
	               	 }
            	  	 if(!Util.esDiferente(int320, 7,8)){
	               		 Log.e("2. >=2011 pase de:  ","Cuando es: 320 = 7,8");
	               		 esArrastre=true;
	               		 nextFragment(CISECCION_03f_7);
	               		 return false;
	               	 }
            	  	 
            		 if(!Util.esDiferente(int320, 0,2,10,11,12,13,96)){
               			Log.e("3. >=2011  pase de:  ","Cuando es: 320 = 0,2,10,11,12,13,96");
               			esArrastre=true;
               			nextFragment(CISECCION_03f_8);
               			return false;
            		 }                    	
            	 }
             }
             
             if (prevPage==CISECCION_03f_5 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Log.e("5 IDA  ","Cuando es: 5");
             	Integer int325a = App.getInstance().getPersonaCuestionarioIndividual().qi325a==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi325a;
             	Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
          		if (int325a==1) {
          			if (int320==1) {
          				Log.e("8. pase de:  ","320 = 1 y 325a=1");
               			esArrastre=true;
               			nextFragment(CISECCION_03f_8);
               			return false;
					}
          			else{
          				Log.e("8. pase de:  ","325a = 1 y 320<>1");
              			esArrastre=true;
                        nextFragment(CISECCION_03f_7);
                        return false;
          			}
      			}else {      				
      				return true;
 				}
             }
             
             if (prevPage==CISECCION_03f_6 && prevPage<curPage && (prevPage+1==curPage)) {
            	 Log.e("6 IDA  ","Cuando es: 6");
              	Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
           		if (int320==1) {
           			Log.e("9. pase de:  ","320 = 1");
           			esArrastre=true;
           			nextFragment(CISECCION_03f_8);
           			return false;
           		}else {
           			return true;
           		}
             }
             
             
             
             if(prevPage==CISECCION_03f_8 && curPage==CISECCION_04Af_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null   && prevPage<curPage && (prevPage+1==curPage)){
            	 
            	 if (App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a == 0) {
            		 Log.e("Aki ULTIMO SOLO PARA PARA DIT","11 ");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04Bf_1);                  
                     return false;  
            	 }            
            	 if (App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a == 0) {
                		 Log.e("Aki ULTIMO","11 ");
                         esArrastre=false;
                         debeGrabar=false;
                         irA(CISECCION_04B2f_2);                  
                         return false;  
            	}
             }  
             
             if(prevPage==CISECCION_04Af_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
//            	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==false ) {
//            		 Log.e("Aki","22_a");
//                     esArrastre=false;
//                     debeGrabar=false;
//                     irA(CISECCION_04B2f_2);                 
//                     return false;  
//            	 }
            	 
            	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) 
            			 && (App.getInstance().getSeccion04B2().filtro480==false  || (App.getInstance().getSeccion04B2().filtro480==false && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478!=null && App.getInstance().getSeccion04DIT_02().qi478 >App.SOLODITMAX))) {
            		 Log.e("Aki","22_a");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_2);                 
                     return false;  
            	 }
            	 
               	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==true) {
            		 Log.e("Aki","33_a");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_2);                  
                     return false;  
            	 }
            	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false) {
            		 Log.e("Aki","44_a");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_3);                  
                     return false;  
            	 }       
             } 
             
             
             if(prevPage==CISECCION_04Af_1 && curPage==CISECCION_04Af_2 && !isEsAvanceDual() && App.getInstance().getPersonaCuestionarioIndividual()!=null && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos>0 || App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true) && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","3_3");
                    esArrastre=false;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_2 && curPage==CISECCION_04Af_3 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","pase 4");
                    esArrastre=false;
                    debeGrabar=false;
                    irA(CISECCION_04Af_5);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_2 && curPage==CISECCION_04Af_3 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi407_y==1 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","5");
                    esArrastre=false;
                    debeGrabar=false;
                    irA(CISECCION_04Af_4_1);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_6 && curPage==CISECCION_04Af_7 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi427==2 && prevPage<curPage && (prevPage+1==curPage)){
              	 Log.e("Aki","6");
                      esArrastre=true;
                      debeGrabar=false;
                      irA(CISECCION_04Af_8);                  
                      return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_6 && curPage==CISECCION_04Af_7 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage<curPage && (prevPage+1==curPage)){
              	 Log.e("Aki","7");
                      esArrastre=true;
                      debeGrabar=false;
                      irA(CISECCION_04Af_9);                  
                      return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_10 && curPage==CISECCION_04Af_11 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","8");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_12);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_10 && curPage==CISECCION_04Af_11 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi433!=1 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","9");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_12);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Af_12 && curPage==CISECCION_04Af_13 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi435!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","10bbbbbb");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_14);                  
                    return false;            	
             }       
             /*
             if(prevPage==CISECCION_04Af_14 && curPage==CISECCION_04Af_15 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getNacimiento().qi216==2 && prevPage<curPage){
            	 Log.e("Aki","27");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_1);                  
                    return false;            	
             } */
             if(prevPage==CISECCION_04Af_14 && curPage==CISECCION_04Af_15 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().qi216==2 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","11");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_1);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Af_2 && curPage==CISECCION_04Bf_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","12");
                    esArrastre=false;
                    debeGrabar=false;
                    return false;            	
             } 
                          
             if(prevPage==CISECCION_04Af_15 && curPage==CISECCION_04Bf_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","13");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Af_1);                  
                    return false;            	
             } 
             
                   
             if(prevPage==CISECCION_04Bf_1 && curPage==CISECCION_04Bf_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 
            	/* if (!isEsAvanceDual() && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4dit>0) {
            		 Log.e("Aki","14_1");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04Bf_18);                  
                     return false;  
            	 }*/
            	 
            	 if (!isEsAvanceDual()  && App.getInstance().getSeccion04B2().filtro479==true) {
            		 Log.e("Aki","test B2f1");
                     esArrastre=true;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_1);                  
                     return false;  
            	 }
            	 if (!isEsAvanceDual()  &&  App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==false ) {
            		 Log.e("Aki","test B2f2");
                     esArrastre=true;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_2);                  
                     return false;  
				}
            	 if (!isEsAvanceDual()  && App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==true  ) {
            		 Log.e("Aki","test B2f2b");
                     esArrastre=true;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_2);                  
                     return false;  
				}
            	 if (!isEsAvanceDual()  && App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false  ) {
            		 Log.e("Aki","test B2f3");
                     esArrastre=true;
                     debeGrabar=false;
                     irA(CISECCION_04B2f_3);                  
                     return false;  
				}
             } 
             
             if(prevPage==CISECCION_04Bf_2 && curPage==CISECCION_04Bf_3 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki individual","15");
                    esArrastre=true;
                    debeGrabar=false;
//                  irA(CISECCION_04Bf_4);    
                    irA(CISECCION_04Bf_5);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Bf_3 && curPage==CISECCION_04Bf_4 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458!=1 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","16");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_5);                  
                    return false;            	
             }
             
             if(prevPage==CISECCION_04Bf_5 && curPage==CISECCION_04Bf_6 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && !(App.getInstance().getSeccion04B().qi216==1 && App.getInstance().getSeccion04B().filtro215 && App.getInstance().getSeccion04B().qi218==1) && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("&& App.getInstance().getSeccion04B().filtro215 := ",""+ App.getInstance().getSeccion04B().filtro215);
            	 
            	 Log.e("Aki","17");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_10);                  
                    return false;            	
             }
             
             
             if(prevPage==CISECCION_04Bf_6 && curPage==CISECCION_04Bf_7 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null 
            		 &&   !(App.getInstance().getSeccion04B().qi465db_a!=null && App.getInstance().getSeccion04B().qi465db_a==2 && App.getInstance().getSeccion04B().qi465db_b!=null && App.getInstance().getSeccion04B().qi465db_b==2 && App.getInstance().getSeccion04B().qi465db_c!=null && App.getInstance().getSeccion04B().qi465db_c==2 && App.getInstance().getSeccion04B().qi465db_d!=null && App.getInstance().getSeccion04B().qi465db_d==2) 
            		 &&   !(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi465dd_cc!=null && App.getInstance().getSeccion04B().qi465dd_cc > 0)
            		 &&   !((App.getInstance().getSeccion04B().qi465dd_ac !=null && App.getInstance().getSeccion04B().qi465dd_ar!=null &&  App.getInstance().getSeccion04B().qi465dd_ac < App.getInstance().getSeccion04B().qi465dd_ar) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_bc !=null && App.getInstance().getSeccion04B().qi465dd_br!=null && App.getInstance().getSeccion04B().qi465dd_bc < App.getInstance().getSeccion04B().qi465dd_br) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_cc !=null && App.getInstance().getSeccion04B().qi465dd_cr!=null && App.getInstance().getSeccion04B().qi465dd_cc < App.getInstance().getSeccion04B().qi465dd_cr) || 
            		 		(App.getInstance().getSeccion04B().qi465dd_dc !=null && App.getInstance().getSeccion04B().qi465dd_dr!=null && App.getInstance().getSeccion04B().qi465dd_dc < App.getInstance().getSeccion04B().qi465dd_dr))  
            		 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","18");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_8);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_12 && curPage==CISECCION_04Bf_13 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467!=1 && App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468!=1 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","19");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_15);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_15 && curPage==CISECCION_04Bf_16 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi472!=1 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","20");
                    esArrastre=true;
                    debeGrabar=false;
                    //05/08/2019
                    //irA(CISECCION_04Bf_21);
                    irA(CISECCION_04Bf_19);
                    return false;            	
             } 
             
//             if(prevPage==CISECCION_04Bf_17 && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()==null && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","21_0");
//                    esArrastre=true;
//                    debeGrabar=false;
//                    irA(CISECCION_04Bf_1);                  
//                    return false;            	
//             } 
//             
//             
//             
//             if(prevPage==CISECCION_04Bf_17 && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=9 && App.getInstance().getSeccion04DIT().qi478 <=12 && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","9 a 12");
//            	 Log.e("9 a 12",""+App.getInstance().getSeccion04DIT().qi478);
//                    esArrastre=true;
//                 //   debeGrabar=false;
//                 //   irA(CISECCION_04Bf_18_1);                  
//                    return false;            	
//             } 
//             
//             if(prevPage==CISECCION_04Bf_17 && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=15 && App.getInstance().getSeccion04DIT().qi478 <=18 && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","15 a 18");
//            	 Log.e("15 a 18",""+App.getInstance().getSeccion04DIT().qi478);
//                    esArrastre=true;
//                    debeGrabar=false;
//                    irA(CISECCION_04Bf_18_2);                  
//                    return false;            	
//             } 
//             
//             if(prevPage==CISECCION_04Bf_17 && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=30 && App.getInstance().getSeccion04DIT().qi478 <=36 && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","30 a 36");
//            	 Log.e("30 a 36",""+App.getInstance().getSeccion04DIT().qi478);
//                    esArrastre=true;
//                    debeGrabar=false;
//                    irA(CISECCION_04Bf_18_3);                  
//                    return false;            	
//             } 
//             
//             if(prevPage==CISECCION_04Bf_17 && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=53 && App.getInstance().getSeccion04DIT().qi478 <=59 && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","53 - 59");
//            	 Log.e("53 - 59",""+App.getInstance().getSeccion04DIT().qi478);
//                    esArrastre=true;
//                    debeGrabar=false;
//                    irA(CISECCION_04Bf_18_4);                  
//                    return false;            	
//             } 
//             
//             if(prevPage==CISECCION_04Bf_18_1 && curPage==CISECCION_04Bf_18_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=9 && App.getInstance().getSeccion04DIT().qi478 <=12 && prevPage<curPage && (prevPage+1==curPage)){
//            	 Log.e("Aki","21_1");
//            	 Log.e(" 9-a12-  21_1",""+App.getInstance().getSeccion04DIT().qi478);
//                    esArrastre=true;
//                    debeGrabar=false;
//                    irA(CISECCION_04Bf_1);                  
//                    return false;            	
//             } 
             
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()==null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","21_0");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             } 
             
             
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=9 && App.getInstance().getSeccion04DIT_02().qi478 <=12 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","9 a 12");
//            	 Log.e("9 a 12",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                 //   debeGrabar=false;
                 //   irA(CISECCION_04Bf_18_1);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=13 && App.getInstance().getSeccion04DIT_02().qi478 <=18 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","13 a 18");
//            	 Log.e("13 a 18",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_2);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=19 && App.getInstance().getSeccion04DIT_02().qi478 <=23 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","19 a 23");
//            	 Log.e("19 a 23",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_3);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=24 && App.getInstance().getSeccion04DIT_02().qi478 <=36 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","24 - 36");
//            	 Log.e("24 - 36",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_4);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=37 && App.getInstance().getSeccion04DIT_02().qi478 <=54 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","37 - 54");
//            	 Log.e("37 - 54",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_5);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=55 && App.getInstance().getSeccion04DIT_02().qi478 <=71 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","55-71");
//            	 Log.e("55 - 60",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_6);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_1 && curPage==CISECCION_04Af_15 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null 
//            		 && App.getInstance().getSeccion04DIT_02().qi478 >=61 && App.getInstance().getSeccion04DIT_02().qi478 <=71 
            		 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("Aki","61-71");
//            	 Log.e("61 - 71",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_20_6);                  
                    return false;            	
             }
             
             
             if(prevPage==CISECCION_04Bf_20_1 && curPage==CISECCION_04Bf_20_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=9 && App.getInstance().getSeccion04DIT_02().qi478 <=12 && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 1");
//            	 Log.e(" 9-a12-  21_1",""+App.getInstance().getSeccion04DIT_02().qi478);
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             }                                   
             
             if(prevPage==CISECCION_04Bf_20_2 && curPage==CISECCION_04Bf_20_3 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 2");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             }
             
             if(prevPage==CISECCION_04Bf_20_3 && curPage==CISECCION_04Bf_20_4 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 3");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             }  
             
             if(prevPage==CISECCION_04Bf_20_4 && curPage==CISECCION_04Bf_20_5 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 4");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_5 && curPage==CISECCION_04Bf_20_6 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 5");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_6 && curPage==CISECCION_04B2f_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("salto despues de DTI ","tramo 6");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);                  
                    return false;            	
             } 

             
             
             
             
             if(prevPage==CISECCION_04B2f_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	if (App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false) {
            		 Log.e("Aki","24");
                	 esArrastre=true;
                	 nextFragment(CISECCION_04B2f_3);
                	 return false;
				}
             }
             
             
             
             if(prevPage==CISECCION_04B2f_4  && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in490= App.getInstance().getSeccion04B2().qi490==null?0:App.getInstance().getSeccion04B2().qi490;
            	 Log.e("490",""+in490);
//            	 Log.e("fltr",""+App.getInstance().getSeccion04B2().filtro491);
            	 if (in490==5 && App.getInstance().getSeccion04B2().filtro491==false) {
            		 Log.e("Aki","26_1");
                	 esArrastre=true;
                 	 nextFragment(CISECCION_05f_1);
                 	 return false;
				}
             }
             
             if(prevPage==CISECCION_04B2f_5 && curPage==CISECCION_04B2f_6 && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in490= App.getInstance().getSeccion04B2().qi490==null?0:App.getInstance().getSeccion04B2().qi490;
//            	Log.e("490",""+in490);
//            	Log.e("friltro",""+App.getInstance().getSeccion04B2().filtro491);
            	 if (in490!=5 && App.getInstance().getSeccion04B2().filtro491==false) {
            		 Log.e("Aki","26_2");
                	 esArrastre=true;
                	 debeGrabar = false;
                 	 nextFragment(CISECCION_05f_1);
                 	 return false;
            	 }
             }
             
             
             
             
             
             if(prevPage==CISECCION_05f_2 && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
             	 Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
            	 Integer int106= App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
            	 
            	 if (in512>0 && int106>=25) {
            		 Log.e("f 5_1 (1)" ," "+ "f_5_2 cuando in512>0 && int106>=25");
                 	 esArrastre=true;
                 	 debeGrabar = false;
                 	 nextFragment(CISECCION_05f_4);
                 	 return false;
            	 }
            	 
            	 if (in512==0) {
            		 Log.e("f 5_1  (2)" ," "+ "f_5_2 cuando in512==0");
                 	 esArrastre=true;
                 	 debeGrabar = false;
                 	 nextFragment(CISECCION_05f_5);
                 	 return false;
            	 }     
             }
         
             
             if(prevPage==CISECCION_05f_5 && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer int320= App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
            	 
            	 if (int320==1 || int320==2) {
            		 Log.e("f 5_5 (1)" ," "+ "f_6_4 cuando int320==1 || int320==2");
                 	 esArrastre=true;
                 	 nextFragment(CISECCION_06f_4);
                 	 return false;
				}
             }
             
             if(prevPage==CISECCION_06f_1 &&  App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 
            	 Integer in602= App.getInstance().getCiseccion05_07().qi602==null?0:App.getInstance().getCiseccion05_07().qi602;
            	 Integer in603u= App.getInstance().getCiseccion05_07().qi603u==null?0:App.getInstance().getCiseccion05_07().qi603u;
            	 Integer in603n= App.getInstance().getCiseccion05_07().qi603n==null?0:App.getInstance().getCiseccion05_07().qi603n;
            	 Integer in310= App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
            	 Integer in226= App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;

            	 if(	  (in602==5 && (in310==2 ||in310==0))  
            		 ||   (in602==1 && in226==1 && (in603u==1 || in603u==2))
            		 ||   (in602==2 && in226==1)
            		 ||  ((in602==1 || in602==2) && (in226==2  || in226==8) && (in310==2 ||in310==0) && ((in603u==1 && in603n>0 && in603n<24) || (in603u==2 && in603n>0 && in603n<2))) 
            		 ||	  (in602==1 && (in603u==3 || in603u==5 || in603u==6 || in603u==7) && (in310==2 ||in310==0) )		){
            		 Log.e("f 6_1  (1)" ," "+ "f_6_3 cuando caso 1");
            		 esArrastre=true;
            		 nextFragment(CISECCION_06f_3);
            		 return false;
            	 }
            	 
            	 if(in602==3 || (in602==1 && in603u==4) || (in602==5 && in310==1) || (in602==1 && (in603u==3 || in603u==5 || in603u==6 || in603u==7) && in310==1) ){
            		 Log.e("f 6_1 (2)" ," "+ "f_6_4 cuando caso 2");
                 	 esArrastre=true;
                 	 nextFragment(CISECCION_06f_4);
                 	 return false;          		 
            	 }
             }
             
             if(prevPage==CISECCION_06f_2 &&  App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in310= App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
            	 
            	 if( in310==1){
            		 Log.e("f 6_2 (1)" ," "+ "f_6_4 cuando caso a");
                 	 esArrastre=true;
                 	 nextFragment(CISECCION_06f_4);
                 	 return false;          		 
            	 }
             }
             if(prevPage==CISECCION_07f_2 && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in501= App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
            	 Integer in709= App.getInstance().getCiseccion05_07().qi709==null?0:App.getInstance().getCiseccion05_07().qi709;
            	 if (in501==3 && in709==2 ) {
            		 Log.e("f 7_2 (1)" ," "+ "f_6_5 cuando in501==3");
                 	 esArrastre=true;
                 	 nextFragment(CISECCION_07f_5);
                 	 return false;
            	 }
             }
             if(prevPage==CISECCION_08f_1 && App.getInstance().getCiseccion_08() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  App.getInstance().getCiseccion_08().qi801a!=null && App.getInstance().getCiseccion_08().qi801b!=null && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in801a= App.getInstance().getCiseccion_08().qi801a==null?0:App.getInstance().getCiseccion_08().qi801a;
            	 Integer in801b= App.getInstance().getCiseccion_08().qi801b==null?0:App.getInstance().getCiseccion_08().qi801b;
            			 
            	 if (in801a==2 && in801b==2) {
            		 Log.e("f 8_1 (1)" ," "+ "f_8_5 cuando in801a==2 && in801b==2");
            		 esArrastre=true;
                	 nextFragment(CISECCION_08f_5);
                	 return false;
            	 }
             } 
             
             if(prevPage==CISECCION_08f_5 && App.getInstance().getCiseccion_08() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in815= App.getInstance().getCiseccion_08().qi815==null?0:App.getInstance().getCiseccion_08().qi815;
            	 Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
            	
            	 if (in815==2 && in512==0) {
            		 Log.e("f 8_5 (1)" ," "+ "f_9_1 cuando in815==2 && in512!=0");
            		 esArrastre=true;
                	 nextFragment(CISECCION_09f_1);
                	 return false;
            	 }
             } 

	          
             if(prevPage==CISECCION_08f_6 && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){
            	 
            		Integer in817b=0;
    	           	Integer in817c=0;
    	           	if (App.getInstance().getCiseccion_08()!=null) {
    	           		in817b= App.getInstance().getCiseccion_08().qi817b==null?0:App.getInstance().getCiseccion_08().qi817b;
    		           	in817c= App.getInstance().getCiseccion_08().qi817c==null?0:App.getInstance().getCiseccion_08().qi817c;
    	           	 
    				}
    	           	
            	 Integer in815= App.getInstance().getCiseccion_08().qi815==null?0:App.getInstance().getCiseccion_08().qi815;
            	 Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
            	 
            	 Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
            	 
//               	if (int106>=12 && int106<=14) {
//               		if ((in817b==2 || in817b==3) && (in817c==2 || in817c==3) ) {
//               			Log.e("agredado f 8_6 (0)" ," "+ "Cuando es de 12 a 14");
//                 		 esArrastre=true;
//                     	 nextFragment(CISECCION_10f_12);
//                     	 return false;
//					}
//               		
//               	}
//               	else{
               		if ((in817b==2 || in817b==3) && (in817c==2 || in817c==3)  ) {
               		 Log.e("f 8_6 (1)" ," "+ "f_9_1 cuando in817b!=1 && in817c!=1");
               		 esArrastre=true;
                   	 nextFragment(CISECCION_09f_1);
                   	 return false;
               		}	
               	 
               		if (in815==1 && in512==0) {
               		 Log.e("f 8_6 (2)" ," "+ "f_9_1 cuando in815==1 && in512==0");
               		 esArrastre=true;
                   	 nextFragment(CISECCION_09f_1);
                   	 return false;
               		}
//               	}
             }
             
             if(prevPage==CISECCION_09f_1 && curPage== CISECCION_10f_1 && prevPage<curPage && App.getInstance().getPersonaCuestionarioIndividual()!=null && (prevPage+1==curPage)){
            	 Integer qhviolen = App.getInstance().getQhviolen();
            	 Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
            	 
            	 if ( (int106>=12 && int106<=14) || qhviolen!=App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
                 	 Log.e("","si no es QHVIOLENCIA.... ooo mef de 12 a 14");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10f_12);
            		 return false;
            	 }            	
             }
             
             if(prevPage==CISECCION_10f_1 && App.getInstance().getCiseccion_10_01() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in1000A= App.getInstance().getCiseccion_10_01().qi1000a==null?0:App.getInstance().getCiseccion_10_01().qi1000a;
            	 Integer in1001A= App.getInstance().getCiseccion_10_01().qi1001a==null?0:App.getInstance().getCiseccion_10_01().qi1001a;
            	 Integer in501= App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
            	 Integer in502= App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
            	 
            	 
            	 if (in1000A==1 && in1001A==1 && in501==3 && in502==3 ) {
            		 Log.e("f 10_1 (1)" ," "+ "f_10_6 cuando in501==3 && in502==3 ");
          		   esArrastre=true;
          		   nextFragment(CISECCION_10f_2);
          		   return false;
            	 }

            	 if (in1000A==1 && in1001A==2 && in501==3 && in502==3 ) {
            		 Log.e("f 10_1 (1)" ," "+ "f_10_6 cuando in501==3 && in502==3 ");
          		   esArrastre=true;
          		   nextFragment(CISECCION_10f_6);
          		   return false;
            	 }
            
            	 if (in1000A==2 ) {
            		 Log.e("f 10_1 (2)" ," "+ "f_10_12 cuando in1000A==2");
          		   esArrastre=true;
          		   nextFragment(CISECCION_10f_12);
          		   return false;
            	 }
             }          
            
 
             if(prevPage==CISECCION_10f_3 && App.getInstance().getCiseccion_10_01() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){
            	 Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
            	 
            	 if (infiltro1005==0) {
            		 Log.e("f 10_3 (1)" ," "+ "f_10_5 cuando in1005A==2 && in1005B==2 && in1005C==2 && in1005D==2 && in1005E==2 && in1005F==2 && in1005G==2 && in1005H==2 && in1005I==2");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10f_5);
            		 return false;
            	 }
            	 
            	 if (infiltro1005>=1) {
            		 Log.e("f 10_3 (2)" ," "+ "AQUI ENTRO");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10f_4);
            		 return false;
            	 }
             }
             
             if(prevPage==CISECCION_10f_6 && App.getInstance().getCiseccion_10_01() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){

            	 Integer in1013 = App.getInstance().getCiseccion_10_01().qi1013a==null?0:App.getInstance().getCiseccion_10_01().qi1013a;
            	 Integer in1013aa = App.getInstance().getCiseccion_10_01().qi1013aa==null?0:App.getInstance().getCiseccion_10_01().qi1013aa;
            	 Integer in1013b = App.getInstance().getCiseccion_10_01().qi1013b==null?0:App.getInstance().getCiseccion_10_01().qi1013b;
            	 
            	 Integer in1019 = App.getInstance().getCiseccion_10_01().qi1019==null?0:App.getInstance().getCiseccion_10_01().qi1019;
            	 Integer in201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
            	 Integer in226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
            	 Integer in230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
            	 
            	 Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
            	 Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
            	 
            	 Integer in208=App.getInstance().getPersonaCuestionarioIndividual().qi208==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi208;
            	 
            	// Boolean infiltro1014nico=App.getInstance().getCiseccion_10_01().filtro1014nico==null?false:App.getInstance().getCiseccion_10_01().filtro1014nico;
//            	 Log.e("ingreso aki" ," ,"+in1013 + ",  "+in201+" , "+in226+" , "+in230+" infiltro1014="+infiltro1014+" infiltro1005="+infiltro1005+"in1019 ="+in1019);
            	
            	 
            	 
              	 if( in1013 != 0){
              		if ((in1013==2 || in1013==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
                 		 Log.e("f 10_6 (1)" ," "+ "f_10_8 cuando algun SI");
                 		 esArrastre=true;
                 		 nextFragment(CISECCION_10f_8);
                 		 return false;
                 	 }
                 	 
	               	 if ((in1013==2 || in1013==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
	   	            		 Log.e("f 10_6 (2)" ," "+ "f_10_9 cuando todo es NO");
	   	            		 esArrastre=true;
	   	            		 nextFragment(CISECCION_10f_9);
	   	            		 return false;
	               	 }
	               	 
				/////////////////////////////////
				            	if ( in1013==2 || in1013==8 ){
				          		if (in201==1 || in226==1 || in230==1 || in208>0) {
				          		}
				          		else{
				          		}
				          	}
				          	else{
				          		if (infiltro1014>1 ) {
				          			if((in201==1 || in226==1 || in230==1 || in208>0)) {
				                  	}
				          			else{            				
				          			}
				      			}
				          		else{            			
				          			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
				          			}
				          			else{
				          				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
						             				esArrastre=true;
						             				nextFragment(CISECCION_10f_8);  
						             				return false;
				          					
				          				}else{
				          					   
				          				}
				          				
				          			}
				          		}         
				          	}
            	/////////////////////////////////
              	 }   
              	 
              	 
              	if( in1013aa != 0){
              		if ((in1013aa==2 || in1013aa==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
                 		 Log.e("f 10_6 (1)" ," "+ "f_10_8 cuando algun SI");
                 		 esArrastre=true;
                 		 nextFragment(CISECCION_10f_8);
                 		 return false;
                 	 }
                 	 
	               	 if ((in1013aa==2 || in1013aa==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
	   	            		 Log.e("f 10_6 (2)" ," "+ "f_10_9 cuando todo es NO");
	   	            		 esArrastre=true;
	   	            		 nextFragment(CISECCION_10f_9);
	   	            		 return false;
	               	 }
	               	 
	                	/////////////////////////////////
		               	if ( in1013aa==2 || in1013aa==8 ){
		             		if (in201==1 || in226==1 || in230==1 || in208>0) {
		             		}
		             		else{
		             		}
		             	}
		             	else{
		             		if (infiltro1014>1 ) {
		             			if((in201==1 || in226==1 || in230==1 || in208>0)) {
		                     	}
		             			else{            				
		             			}
		         			}
		             		else{            			
		             			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
		             			}
		             			else{
		             				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
				             				esArrastre=true;
				             				nextFragment(CISECCION_10f_8);  
				             				return false;
		             					
		             				}else{
		             					   
		             				}
		             				
		             			}
		             		}         
		             	}
		               	/////////////////////////////////
	               	 
	               
              	 } 
              	
              	
              	if( in1013b != 0){
              		if ((in1013b==2 || in1013b==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
                 		 Log.e("f 10_6 (1)" ," "+ "f_10_8 cuando algun SI");
                 		 esArrastre=true;
                 		 nextFragment(CISECCION_10f_8);
                 		 return false;
                 	 }
                 	 
	               	 if ((in1013b==2 || in1013b==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
	   	            		 Log.e("f 10_6 (2)" ," "+ "f_10_9 cuando todo es NO");
	   	            		 esArrastre=true;
	   	            		 nextFragment(CISECCION_10f_9);
	   	            		 return false;
	               	 }
	               	 
					/////////////////////////////////
		            if ( in1013b==2 || in1013b==8 ){
		          		if (in201==1 || in226==1 || in230==1 || in208>0) {
		          		}
		          		else{
		          		}
		          	}
		          	else{
		          		if (infiltro1014>1 ) {
		          			if((in201==1 || in226==1 || in230==1 || in208>0)) {
		                  	}
		          			else{            				
		          			}
		      			}
		          		else{            			
		          			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
		          			}
		          			else{
		          				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
				             				esArrastre=true;
				             				nextFragment(CISECCION_10f_8);  
				             				return false;
		          					
		          				}else{
		          					   
		          				}
		          				
		          			}
		          		}         
		          	}
		            /////////////////////////////////
              	 } 
              	
              	
           

              	 
            	 
             }
             
             if(prevPage==CISECCION_10f_7 && App.getInstance().getCiseccion_10_01() !=null && App.getInstance().getPersonaCuestionarioIndividual()!=null &&  prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in1013 = App.getInstance().getCiseccion_10_01().qi1013a==null?0:App.getInstance().getCiseccion_10_01().qi1013a;
            	 Integer in1013aa = App.getInstance().getCiseccion_10_01().qi1013aa==null?0:App.getInstance().getCiseccion_10_01().qi1013aa;
            	 Integer in1013b = App.getInstance().getCiseccion_10_01().qi1013b==null?0:App.getInstance().getCiseccion_10_01().qi1013b;
            	 
            	 Integer in1019 = App.getInstance().getCiseccion_10_01().qi1019==null?0:App.getInstance().getCiseccion_10_01().qi1019;
            	 Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
            	 Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
            	 
//            	 Log.e("ingreso aki" ,""+in1013 + " "+" filtro"+App.getInstance().getCiseccion_10_01().filtro1005);
      
            	 if( in1013 != 0){
            		 if ((in1013==2 || in1013==8)  && infiltro1005==0 && in1019==2 ) {
	            		 Log.e("f 10_7 (1)" ," "+ "f_10_9 cuando todo es NO");
	            		 esArrastre=true;
	            		 nextFragment(CISECCION_10f_9);
	            		 return false;
            		 } 
            	 }
            	 
            	 if( in1013aa != 0){
            		 if ((in1013aa==2 || in1013aa==8)  && infiltro1005==0 && in1019==2 ) {
	            		 Log.e("f 10_7 (1)" ," "+ "f_10_9 cuando todo es NO");
	            		 esArrastre=true;
	            		 nextFragment(CISECCION_10f_9);
	            		 return false;
            		 } 
            	 }
            	 
            	 if( in1013b != 0){
            		 if ((in1013b==2 || in1013b==8)  && infiltro1005==0 && in1019==2 ) {
	            		 Log.e("f 10_7 (1)" ," "+ "f_10_9 cuando todo es NO");
	            		 esArrastre=true;
	            		 nextFragment(CISECCION_10f_9);
	            		 return false;
            		 } 
            	 }
            	 
             }
             
             if(prevPage==CISECCION_10f_10 && App.getInstance().getCiseccion_10_01()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 Integer in1000A= App.getInstance().getCiseccion_10_01().qi1000a==null?0:App.getInstance().getCiseccion_10_01().qi1000a;
//            	 Log.e("ssss",""+App.getInstance().getCiseccion_10_01().filtro1034);
            	
            	 if (!App.getInstance().getCiseccion_10_01().filtro1034 && in1000A==1 ) {
            		 Log.e("f 10_7 (1)" ," "+ "!App.getInstance().getCiseccion_10_01().filtro1034 && in1000A==1 ");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10f_12);
            		 return false;
				}
             }
             
             if(prevPage==CISECCION_10f_12 && App.getInstance().getCiseccion_10_01()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 
            	 if (App.getInstance().getCiseccion_10_01().filtroSiNinioS10==0) {
            		 Log.e("f 10_12 (1)" ," "+ "CUANDO NO EXISTE NI�OS ");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10_2f_1);
            		 return false;
            	 }        
             }
             
             if(prevPage==CISECCION_10_2f_1 && App.getInstance().getCiseccion_10_01()!=null  && prevPage<curPage && (prevPage+1==curPage)){
            	 Log.e("f 10_13 (1)" ," "+ "FINNNNN ");
            	 esArrastre=true;
            	 nextFragment(CARATULA);
            	 return false;
             }           

	             
             return true;
       }
       
       public boolean preg717(){
    	   boolean band717=false;
    	   
    		   band717=!MyUtil.incluyeRango(1,1,App.getInstance().getPersona_04_07().qs713) && !MyUtil.incluyeRango(1,1,App.getInstance().getPersona_04_07().qs714) && !MyUtil.incluyeRango(1,1,App.getInstance().getPersona_04_07().qs715) 
          			 && !MyUtil.incluyeRango(1,1,App.getInstance().getPersona_04_07().qs716) && !MyUtil.incluyeRango(1,1,App.getInstance().getPersona_04_07().qs717);
          	
        	 return band717;
        }
       
       
       private boolean ejecutarRetrocesos(int prevPage, int curPage) {                
             if(prevPage==SECCION04){
                    band=false;
             }
             if(prevPage==SECCION06){
                    band=false;
             }
             if (prevPage==SECCION06){
            	 Log.e("&&&& retro"," blanquea b");
            	 blanquear2=false;
             }
           
//           if (prevPage==SECCION04 && band==false && curPage==prevPage-1) {
             if (prevPage==SECCION04 && curPage==prevPage-1) {
            	 Log.e("retro Aki","28");
            	 band=true;
//               nextFragment(CuestionarioFragmentActivity.SECCION04);
                 nextFragment(CuestionarioFragmentActivity.CARATULA);
                 return false;
             }
             
             
//           if (prevPage==SECCION06 && band==false && curPage==prevPage-1) {
             if (prevPage==SECCION06  && curPage==prevPage-1) {
            	 Log.e("Aki","29");   
            	 band=true;
                 nextFragment(CARATULA);
                 return false;
             }
             
             if (prevPage==SECCION8INCENTIVOS&& curPage==prevPage-1) {
            	 Log.e("Aki","29_11");               	 
                 nextFragment(CARATULA);          
                 return false;
             } 
             if (prevPage==SECCIONPOSTCENSO && curPage==prevPage-1) {
            	 Log.e("Aki","29_1");               	 
                 nextFragment(CARATULA);          
                 return false;
             }             
//             if (prevPage==SECCIONCOVID_1 && curPage==prevPage-1) {
//            	 Log.e("Aki","29_1");               	 
//                 nextFragment(CARATULA);          
//                 return false;
//             }
             if (prevPage==CSVISITA  && curPage<prevPage && (curPage+1==prevPage)) {	
                 Log.e("Aki","30");
                 Log.e("&&&& retro"," blanquea a");
                 blanquear2=true;
            	 esArrastre=true;
                 nextFragment(CARATULA);
                 return true;
             }

             if (prevPage==HOGARMAYORADOS &&  App.getInstance().getHogar().hogar_id != 1 && curPage<prevPage && (curPage+1==prevPage)) {
            	 Log.e("Aki","31");
            	 debeGrabar = false;
            	 nextFragment(CuestionarioFragmentActivity.HOGARMAYORADOS -3);
            	 return true;
             }	
             
//           if(prevPage==CHSECCION1f_5 && curPage<prevPage  && (curPage+1==prevPage)){ /*modificado poque se elimino el fracmento de discapacidad*/
             if(prevPage==MORTALIDAD && curPage<prevPage  && (curPage+1==prevPage)){
            	 Log.e("sss","111112");
            	 Log.e("Aki","32");
            	 esArrastre=true;
            	 nextFragment(SECCION01);   
            	 App.getInstance().setEditarPersonaSeccion01(null);
            	 return true;
             }
             
             if(prevPage==CHSECCION1f_4 && curPage<prevPage && App.getInstance().getEditarPersonaSeccion01()!=null && (curPage+1==prevPage)){
            	 Log.e("sss","2222");
            	 if(App.getInstance().getEditarPersonaSeccion01().qh07<3){
            		 Log.e("Aki","33");
                     esArrastre=true;
                     nextFragment(CHSECCION1f_2);                  
                     return true;            		 
            	 }            	 
             }
             if(prevPage==QHSECCION2f_11 && curPage<prevPage && App.getInstance().getEditarPersonaSeccion01()!=null
            		 && App.getInstance().getEditarPersonaSeccion01().todaslaspersonasmenoresa16==true && (curPage +1==prevPage)){
                 Log.e("menores a 15","PASE A JUNTOS o PENSION 65");
                 esArrastre=true;                  
                 nextFragment(HOGARMAYORADOS);
//                 App.getInstance().setEditarPersonaSeccion01(null);
                 return false;
             }
             if(prevPage==CHSECCION1f_2 && curPage<prevPage  && (curPage+1==prevPage)){
            	 Log.e("sss","11111");
            	 Log.e("Aki","34");
            	 esArrastre=true;
            	 nextFragment(SECCION01);     
            	 App.getInstance().setEditarPersonaSeccion01(null);
            	 return true;
             }
                   
             if(prevPage==CSSECCION1f_4 && curPage<prevPage  && (curPage+1==prevPage)){
            	 if(App.getInstance().getPersonabySalud().qh06==2 && App.getInstance().getPersonabySalud().qs23<50){
            		 Log.e("Aki","35");
                     esArrastre=true;
                     nextFragment(CSSECCION1f_2);                  
                     return true;            		 
            	 }            	 
             }
             
             if(prevPage==CSSECCION3f_1 && curPage<prevPage  && (curPage+1==prevPage)){
            	 if(!MyUtil.incluyeRango(15, 99, App.getInstance().getPersonabySalud().qs23)){
            		 Log.e("Aki","36");
                     esArrastre=true;
                     nextFragment(CSSECCION2f_11);                  
                     return true;            		 
            	 }            	 
             }
             
             if(prevPage==CSSECCION3f_2 && curPage<prevPage  && (curPage+1==prevPage)){
            	 if(App.getInstance().getPersonabySalud().qs23<50){
            		 Log.e("Aki","37");
                     esArrastre=true;
                     nextFragment(CSSECCION2f_12);                  
                     return true;            		 
            	 }            	 
             }
             
           if(prevPage==CSSECCION4f_1 && curPage<prevPage  && (curPage+1==prevPage)){
        	 if(App.getInstance().getPersonabySalud().qs23<50){
        		 Log.e("Aki","38");
                 esArrastre=true;
                 nextFragment(CSSECCION2f_12);                  
                 return true;            		 
        	 }            	 
           }   
           
           
           if(prevPage==CSSECCION4f_1 && curPage<prevPage &&  (curPage+1==prevPage)){
        	   if(MyUtil.incluyeRango(50,59,App.getInstance().getPersonabySalud().qs23)){
          		 Log.e("Aki","39");
                   esArrastre=true;
                   nextFragment(CSSECCION3f_2);                  
                   return true;            		 
          	 }            	 
           } 
           
           
    
           
           
           
           
        
          
           
           
           
           
     
           
           
           
           
           if(prevPage==CSSECCION4f_4 && curPage==CSSECCION4f_3 && prevPage>curPage && (curPage+1)==prevPage){
        	   if( App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23>=50 && App.getInstance().getPersonabySalud().qs23<=75 && App.getInstance().getPersonabySalud().qh06==1) ){
	      			Log.e("Aki","RE_XX3b");
	      			esArrastre=true;
	      			irA(CSSECCION4f_2);                  
	      			return true;            		 
	      		}            	 
	      }     
           
           if(prevPage==CSSECCION5f_1 && curPage==CSSECCION4f_4 && prevPage>curPage && (curPage+1)==prevPage){
        	  if(App.getInstance().getPersonabySalud().qs23>75){
       			Log.e("Aki","RE_XX1");
       			esArrastre=true;
       			irA(CSSECCION3f_3);                  
       			return true;            		 
	       	  }
        	  else{
        		  if(App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23<18 || App.getInstance().getPersonabySalud().qs23>75) ){
   	       		   Log.e("Aki","RE_XX2");
   	       		   esArrastre=true;
   	       		   irA(CSSECCION4f_1);                  
   	       		   return true;            		 
	   	       	  }
        		  else{
        			  if( App.getInstance().getPersonabySalud().qs23!=null && (App.getInstance().getPersonabySalud().qs23<18 || (App.getInstance().getPersonabySalud().qh06==1 && App.getInstance().getPersonabySalud().qs23<50))){
	  	           		   Log.e("Aki","RE_XX3a");
	  	   	      			esArrastre=true;
	  	   	      			irA(CSSECCION4f_2);                  
	  	   	      			return true;
	  	           	 }
        			  else{
        				  if( App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23<50 && App.getInstance().getPersonabySalud().qh06==1 ){
        	   	      			Log.e("Aki","RE_XX5");
        	   	      			esArrastre=true;
        	   	      			irA(CSSECCION4f_2);                  
        	   	      			return true;            		 
      	  	   	      	 }   
      	  	           	 
      	  	           	 if( App.getInstance().getPersonabySalud().qs23!=null && App.getInstance().getPersonabySalud().qs23<40 && App.getInstance().getPersonabySalud().qh06==2 ){
      	  	   	      			Log.e("Aki","RE_XX4");
      	  	   	      			esArrastre=true;
      	  	   	      			irA(CSSECCION4f_3);                  
      	  	   	      			return true;            		 
      	  	   	      	}
        			  }
        			  
        			  
        		  }
        	  }      	   
	       	
	      }
           
         ///NUEVO    
//           if(prevPage==CSSECCION4f_4 && curPage==CSSECCION5f_1 && prevPage>curPage && (curPage+1)==prevPage && App.getInstance().getPersonabySalud().qs23>=75 && App.getInstance().getPersonabySalud().qh06==2){
////           if(curPage==CSSECCION5f_1 && App.getInstance().getPersonabySalud()!=null && (prevPage>curPage && App.getInstance().getPersonabySalud().qs23>=75 && App.getInstance().getPersonabySalud().qh06==2)){        	   
//        		   Log.e("Aki","XX4");
//        		   esArrastre=true;        		   
//        		   nextFragment(CSSECCION4f_3);                  
//        		   return false;
//         	}
           
           if(prevPage==CSSECCION5f_1 && curPage<prevPage && (curPage+1==prevPage) && App.getInstance().getPersonabySalud().qs23>70 && App.getInstance().getPersonabySalud().qh06==2){///AQUI
          	 Log.e("Aki","888888");
          	 esArrastre=true;
               nextFragment(CSSECCION4f_3);                  
               return true;
           }
           
         ////        
        	   
           /*
           if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
        	 if(App.getInstance().getPersonabySalud().qs23>70){
      			Log.e("Aki","A1");
      			esArrastre=true;
      			nextFragment(CSSECCION3f_3);                  
      			return true;            		 
      		}            	 
           }
	         if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
	        	 if(MyUtil.incluyeRango(15,24,App.getInstance().getPersonabySalud().qs23)){
	      			Log.e("Aki","A2");
	      			esArrastre=true;
	      			nextFragment(CSSECCION4f_1);                  
	      			return true;            		 
	      		}            	 
	      	}
         
	       if(prevPage==CSSECCION4f_3 && curPage<prevPage  && (curPage+1==prevPage)){
	    	   if(MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==2){
	    			Log.e("Aki","A3");
	    			esArrastre=true;
	    			nextFragment(CSSECCION4f_1);                  
	    			return true;            		 
	    		}            	 
	    	}
	       if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
	    	   if(MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==1){
	    			Log.e("Aki","A4");
	    			esArrastre=true;
	    			nextFragment(CSSECCION4f_1);                  
	    			return true;            		 
	    		}            	 
	    	}
	       if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
	    	   if(MyUtil.incluyeRango(30,39,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==2){
	    			Log.e("Aki","A5");
	    			esArrastre=true;
	    			nextFragment(CSSECCION4f_3);                  
	    			return true;            		 
	    		}            	 
	    	}
	     if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
	    	 if(MyUtil.incluyeRango(40,59,App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonabySalud().qh06==1){
				Log.e("Aki","A6");
				esArrastre=true;
				nextFragment(CSSECCION4f_2);                  
				return true;            		 
			}            	 
		}
       
           

     
             if(prevPage==CSSECCION5f_1 && curPage<prevPage  && (curPage+1==prevPage)){
            	 if(!MyUtil.incluyeRango(40, 59, App.getInstance().getPersonabySalud().qs23)|| !MyUtil.incluyeRango(2, 2, App.getInstance().getPersonabySalud().qh06)){
            		 Log.e("Aki","43");
                     esArrastre=true;
                     nextFragment(CSSECCION4f_3);                  
                     return true;            		 
            	 }            	 
            }
             
             if(prevPage==CSSECCION6f_1 && curPage<prevPage  && (curPage+1==prevPage)){
            	 if(!MyUtil.incluyeRango(15, 99, App.getInstance().getPersonabySalud().qs23)){
            		 Log.e("Aki","44");
                     esArrastre=true;
                     nextFragment(CSSECCION4f_4);                  
                     return true;            		 
            	 }            	 
            }
             */
             
             if(prevPage==CSSECCION7f_1 && curPage<prevPage && !MyUtil.incluyeRango(15, 49, App.getInstance().getPersonabySalud().qs23)  && (curPage+1==prevPage)){///AQUI
            	 Log.e("Aki","45");
            	 esArrastre=true;
                 nextFragment(CSSECCION5f_1);                  
                 return true;
             }
             
             if(prevPage==CSSECCION7f_1 && curPage<prevPage  && (curPage+1==prevPage)){
            		 if(MyUtil.incluyeRango(2, 2, App.getInstance().getPersonabySalud().qs29a) && MyUtil.incluyeRango(2, 2, App.getInstance().getPersonabySalud().qs29b)){
            			 Log.e("Aki","46");
                		 esArrastre=true;
                         nextFragment(CSSECCION5f_1);                  
                         return true;
                	 }
             }
             if(prevPage==CSSECCION7f_1 && curPage<prevPage  && (curPage+1==prevPage)){  //OTRO
            	 if(MyUtil.incluyeRango(2, 2, App.getInstance().getPersona_04_07().qs601a) && MyUtil.incluyeRango(2, 2, App.getInstance().getPersona_04_07().qs601b)){
            		 Log.e("Aki","47");
            		 esArrastre=true;
                     nextFragment(CSSECCION6f_1);                  
                     return true;
            	 }
             }

             if(prevPage==CSSECCION7f_1 && curPage<prevPage 
            		 && ( (MyUtil.incluyeRango(15, 29, App.getInstance().getPersonabySalud().qs23) && App.getInstance().getPersonaSeccion01()!=null &&MyUtil.incluyeRango(2, 2, App.getInstance().getPersonaSeccion01().qh06)) 
              				||  MyUtil.incluyeRango(30, 49, App.getInstance().getPersonabySalud().qs23) 
              			)
            		 && (curPage+1==prevPage)){
            	 Log.e("Aki retross salud","48");
            	 esArrastre=true;
                 nextFragment(CSSECCION6f_1);                  
                 return true;
         			
             }
             
             if(prevPage==CSSECCION7f_7 && curPage<prevPage  && (curPage+1==prevPage)){  
            	
            	 if((App.getInstance().getPersonabySalud().qs209==null || !MyUtil.incluyeRango(1,1,App.getInstance().getPersonabySalud().qs209))){            	
            		 Log.e("Aki","49");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_3);                  
                     return true;
            	 }
             }             
             if(prevPage==CSSECCION7f_7 && curPage<prevPage  && (curPage+1==prevPage)){  
            	 if(preg717() && MyUtil.incluyeRango(1,1,App.getInstance().getPersonabySalud().qs209)){
            		 Log.e("Aki","50");
            		 esArrastre=true;
                     nextFragment(CSSECCION7f_4);                  
                     return true;
            	 }
             }
             
             if(curPage==CSSECCION8f_1 && curPage<prevPage  && (curPage+1==prevPage)){
            	 Log.e("Aki","51");   
            	 App.getInstance().setPersonabySeccion8Salud(null);
                    esArrastre=true;
                    return true;
             }      
             if (prevPage==CSSECCION2f_11 && curPage<prevPage && App.getInstance().getPersonabySalud()!=null  && (curPage+1==prevPage)) {
            	  	
         
            	if(App.getInstance().getPersonabySalud().qs206>1 || App.getInstance().getPersonabySalud().qs208>1 || App.getInstance().getPersonabySalud().qs210>1 || App.getInstance().getPersonabySalud().qs211u>1 ){          
            		Log.e("Aki","52");
            		esArrastre=true;
            		nextFragment(CSSECCION2f_9);
            		return true;
            	}                 
             }
 
             if(prevPage==CSSECCION8f_13 && curPage<prevPage  && (curPage+1==prevPage)){          
            	 Log.e("Aki","53");
            	 esArrastre=true;
                 nextFragment(CSSECCION8f_1);
                 return true;
             }
             if(prevPage==CSSECCION8f_12 && curPage<prevPage && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d==0  && (curPage+1==prevPage)){          
            	 Log.e("Aki","54");   
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_4);
                    return true;
             }
             if(prevPage==CSSECCION8f_12 && curPage<prevPage && App.getInstance().getPersonabySeccion8Salud()!=null &&  App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>0 && App.getInstance().getPersonabySeccion8Salud().qs802d<3 &&  (curPage+1==prevPage)){           
            	 Log.e("Aki","55");   
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_5);
                    return true;
             }
             if(prevPage==CSSECCION8f_12 && curPage<prevPage  && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs802d<6 &&   (curPage+1==prevPage)){           
            	 Log.e("Aki","56");   
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_9);
                    return true;
             }
             
             if(prevPage==CSSECCION8f_9 && curPage<prevPage && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs817>1 && App.getInstance().getPersonabySeccion8Salud().qs817<9  && (curPage+1==prevPage)){           
            	 Log.e("Aki","57");  
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_6);
                    return true;
             }
             if(prevPage==CSSECCION8f_9 && curPage<prevPage && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs820>1 && App.getInstance().getPersonabySeccion8Salud().qs820<9  && (curPage+1==prevPage)){           
            	 Log.e("Aki","58");  
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_7);
                    return true;
             }
             
             if(prevPage==CSSECCION8f_12 && curPage<prevPage && App.getInstance().getPersonabySeccion8Salud()!=null && App.getInstance().getPersonabySeccion8Salud().qs833!=null && App.getInstance().getPersonabySeccion8Salud().qs802d!=null && App.getInstance().getPersonabySeccion8Salud().qs802d>2 && App.getInstance().getPersonabySeccion8Salud().qs833>1 && App.getInstance().getPersonabySeccion8Salud().qs833<9  && (curPage+1==prevPage)){           
            	 Log.e("Aki","59");   
            	 esArrastre=true;
                    nextFragment(CSSECCION8f_10);
                    return true;
             }
          //MODIFICADO PARA EL 2020
             /*if (prevPage==CSSECCION9f_1  && curPage<prevPage  && (curPage+1==prevPage)) {
            	 Log.e("Aki","60");
            	 esArrastre=true;
                 nextFragment(CARATULA);
                 return true;
             }*/
            
             if (prevPage==CISECCION_f_carat  && curPage<prevPage  && (curPage+1==prevPage)) {
            	 Log.e("Aki","61");
            	 esArrastre=true;
                 nextFragment(CARATULA);
                 return true;
             }
             
             
        
             /*******************************RETRO CUESTIONARIO INDIVIDUAL************************************/
             
             if (prevPage==CISECCION_01f_3 && curPage==CISECCION_01f_2_1  && curPage<prevPage ) {
               	Integer int108 = App.getInstance().getPersonaCuestionarioIndividual().qi108n==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108n;
               	Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
               	Integer int107 = App.getInstance().getPersonaCuestionarioIndividual().qi107==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi107;
            	Integer int108y = App.getInstance().getPersonaCuestionarioIndividual().qi108y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108y;
               	Integer int108g = App.getInstance().getPersonaCuestionarioIndividual().qi108g==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108g;
               	if (((int106>=25 && !(int108==2 || int108==3||int108==4||int108==5||int108==6)) || (int106<25 && int107==2)) ) {
               		Log.e("1. retro de:  ","Cuando es: !(int108==2 || int108==3||int108==4||int108==5||int108==6)");
               		esArrastre=true;
               		nextFragment(CISECCION_01f_2);
               		return true;
               	}
               	Integer intqh14=App.getInstance().getEditarPersonaSeccion01().qh14==null?0:App.getInstance().getEditarPersonaSeccion01().qh14;
        		Integer intqh15n=App.getInstance().getEditarPersonaSeccion01().qh15n==null?0:App.getInstance().getEditarPersonaSeccion01().qh15n;
        		Integer intqh15y=App.getInstance().getEditarPersonaSeccion01().qh15y==null?0:App.getInstance().getEditarPersonaSeccion01().qh15y;
        		Integer intqh15g=App.getInstance().getEditarPersonaSeccion01().qh15g==null?0:App.getInstance().getEditarPersonaSeccion01().qh15g;
        		if(int106<25 && int107==intqh14 && int108==intqh15n && int108y==intqh15y && int108g==intqh15g){
        			esArrastre=true;
              		 nextFragment(CISECCION_01f_2);
              		 return false;
        		}
              }
             
             if (prevPage==CISECCION_01f_4 && curPage==CISECCION_01f_3  && curPage<prevPage) {
              	Integer int108 = App.getInstance().getPersonaCuestionarioIndividual().qi108n==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi108n;
              	Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
              	if (int106>=25 && (int108==2 || int108==3||int108==4||int108==5||int108==6)) {
              		Log.e("2. retro de:  ","Cuando es: 110>=25 � 108==3,4,5,6");
           			esArrastre=true;
           			nextFragment(CISECCION_01f_2);
           			return true;
           		}
             }
             
             if (prevPage==CISECCION_02f_4 && curPage==CISECCION_02f_3  && curPage<prevPage && (curPage+1)==prevPage) {
            	 Integer int208 = App.getInstance().getPersonaCuestionarioIndividual().qi208==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi208;
            	 if (int208==0)  {
            		 Log.e("2. retro de: 208 ","Cuando es: 0 ");
            		 esArrastre=true;
            		 nextFragment(CISECCION_02f_2);
            		 return true;
            	 }else {
            		 return true;
            	 }
             }
             
             if (prevPage==CISECCION_02f_6 && curPage==CISECCION_02f_5 && curPage<prevPage && (curPage+1)==prevPage) {
               	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
            		if (int226==2 || int226==8) {
            			Log.e("3. retro de: 226 ","Cuando es: 2,8 ");
            			esArrastre=true;
                      nextFragment(CISECCION_02f_4);
                      return true;
        			}else {
        				return true;
   				}
             }
             
            
             
             if (prevPage==CISECCION_02f_8 && curPage==CISECCION_02f_7 && curPage<prevPage && (curPage+1)==prevPage) {
                	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
                	Integer int231 = App.getInstance().getPersonaCuestionarioIndividual().qi231_y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi231_y;
                	Log.d("log230", "log230 "+int230);
             		if (int230==2 || int231<App.ANIOPORDEFECTO) {
             			Log.e("4. retro de: 230 , 231_a�o ","Cuando es: 230 es 2 � 321_a�o < 2011 ");
             			esArrastre=true;
                       nextFragment(CISECCION_02f_6);
                       return true;
         			}else {
         				return true;
    				}
             }
             
             if(prevPage==CISECCION_03f_3 && curPage==CISECCION_03f_2 && curPage<prevPage && (curPage+1)==prevPage){
             	Integer int310 = App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
             	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
          		if (int310==2 || int226==1) {
          			Log.e("5. retro1 de:  ","310=2 � 226=1");
          			esArrastre=true;
          			nextFragment(CISECCION_03f_1);
          			return true;
      			}else {
      				return true;
 				}
             }
             
             if(prevPage==CISECCION_03f_5 && curPage==CISECCION_03f_4 && curPage<prevPage && (curPage+1)==prevPage){
            	 Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
             	 Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
            	 Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
            	 Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
            	 Integer mayor=int315>int316?int315:int316;
            	 
            	 if (mayor>=App.ANIOPORDEFECTO && int320==1){
            		 Log.e("RETRO 1. >=2011 pase de:  ","Cuando es: int320, 1");
            		 esArrastre=true;
                	 nextFragment(CISECCION_03f_3);
                	 return false;
            	 }            	 
              }
                          
                    
             
             if (prevPage==CISECCION_03f_7 && curPage==CISECCION_03f_6 &&  curPage<prevPage && (curPage+1)==prevPage) {
               	Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
               	Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
             	Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
             	Integer int325a = App.getInstance().getPersonaCuestionarioIndividual().qi325a==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi325a;
             	Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
             	Integer mayor=int315>int316?int315:int316;
             	
             	if(mayor>=App.ANIOPORDEFECTO && !Util.esDiferente(int320, 7,8)){
             		 Log.e("RETRO 2. >=2011 pase de:  ","Cuando es: 320 = 7,8");
             		 esArrastre=true;
             		 nextFragment(CISECCION_03f_3);
             		 return false;
             	}
             	if ((mayor>100 && mayor<App.ANIOPORDEFECTO) && !Util.esDiferente(int320,3,4,5,6,9,7,8)) {
             		 Log.e("RETRO 2. < 2011 f_3_2. pase de:  ","Cuando es: int320,3,4,5,6,9,7,8");
             		 esArrastre=true;
             		 nextFragment(CISECCION_03f_2);
             		 return false;
             	} 
             	
             	if (int325a==1 && int320!=1 && int320!=0) {
             		Log.e("6. retro de:  ","Cuando es: int325a==1 && int320!=1");
             		esArrastre=true;
             		nextFragment(CISECCION_03f_5);
             		return true;
				}
             	else{
             		Log.e("6. retro de: corregir cuando es 320=1 ","OTRSO");
             		esArrastre =true;
             		return true;
             	}
             }             
             
             if (prevPage==CISECCION_03f_8 && curPage==CISECCION_03f_7 &&curPage<prevPage && (curPage+1)==prevPage) {
               	Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
               	Integer int304 = App.getInstance().getPersonaCuestionarioIndividual().qi304==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi304;
               	Integer int326 = App.getInstance().getPersonaCuestionarioIndividual().qi326==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi326;
               	Integer int325a = App.getInstance().getPersonaCuestionarioIndividual().qi325a==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi325a;
      
                Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
                Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
                Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
                Integer mayor=int315>int316?int315:int316;
                
               	if (int304==2) {
               		 Log.e("RETRO 5. retro de:  ","304=2");
               		esArrastre=true;
                     nextFragment(CISECCION_03f_1);
                     return true;
 				}
               	else{
               		if (mayor>=App.ANIOPORDEFECTO || mayor==0) {
               	 		if (int320==0 || int320==2||int320==10||int320==11||int320==12||int320==13||int320==96) {
               	 			Log.e("RETROS 3. >=2011  pase de:  ","Cuando es: 320 = 0,2,10,11,12,13,96");
                   			esArrastre=true;
                   			nextFragment(CISECCION_03f_3);
                   			return true;
                   		}
                   		if (int320==1) {
                   			if (int325a==1) {
                   				Log.e("RETRO 8. retro de:  ","320 = 1 y 325a=1");
     		           			esArrastre=true;
     		           			nextFragment(CISECCION_03f_5);
     		                    return true;
    						}	
                   			else{
                   				Log.e("RETRO 9. retro de:  ","326 = 1");
                   				esArrastre=true;
     	 	                    nextFragment(CISECCION_03f_6);
     	 	                    return true;
                   			}
                   		}
					}
               		else{
               			if(!Util.esDiferente(int320, 0,1,2,10,11,12,13,96)){
               				Log.e("RETRO 1. < 2011 f_3_2","Cuando es: int320, 0,1,2,10,11,12,13,96");
               				esArrastre=true;
               				nextFragment(CISECCION_03f_2);
               				return false;
               			}
               		}
               	}
             }

             if(prevPage==CISECCION_04Af_5 && curPage==CISECCION_04Af_4_2 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki","3");
                    esArrastre=true;
                    irA(CISECCION_04Af_2);                  
                    return false;            	
           }    
           
           if(prevPage==CISECCION_04Af_4_1 && curPage==CISECCION_04Af_3 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi407_y==1 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","entro aki 4");
                  esArrastre=true;
                  irA(CISECCION_04Af_2);                  
                  return false;            	
           }  
           
           
           if(prevPage==CISECCION_04Af_8 && curPage==CISECCION_04Af_7 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi427==2 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki","5");
                    esArrastre=true;
                    irA(CISECCION_04Af_6);                  
                    return false;            	
           }  
          
           if(prevPage==CISECCION_04Af_9 && curPage==CISECCION_04Af_8 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","6");
                  esArrastre=true;
                 // debeGrabar=false;
                  irA(CISECCION_04Af_6);                  
                  return false;            	
           }
           
           if(prevPage==CISECCION_04Af_12 && curPage==CISECCION_04Af_11 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == false && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki","7");
                    esArrastre=true;
                   // debeGrabar=false;
                    irA(CISECCION_04Af_10);                  
                    return false;            	
           }  
           
           if(prevPage==CISECCION_04Af_12 && curPage==CISECCION_04Af_11 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi433!=1 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","8");
                  esArrastre=true;
                  irA(CISECCION_04Af_10);                  
                  return false;            	
           } 
             
           if(prevPage==CISECCION_04Af_14 && curPage==CISECCION_04Af_13 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi435!=null && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","9");
                  esArrastre=true;
                  irA(CISECCION_04Af_12);                  
                  return false;            	
           }
           
           if(prevPage==CISECCION_04Bf_15 && curPage==CISECCION_04Bf_14 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467!=1 && App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468!=1 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","10");
                  esArrastre=true;
                  irA(CISECCION_04Bf_12);                  
                  return false;            	
           } 
           
           
           if(prevPage==CISECCION_04Bf_1 && curPage==CISECCION_04Af_15 && App.getInstance().getPersonaCuestionarioIndividual()!=null && prevPage>curPage && (curPage+1)==prevPage){
        	
        	   if (App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a == 0) {
          		 	Log.e("RETRO Aki ULTIMO SOLO PARA PARA DIT","11 ");
                   esArrastre=true;
                   irA(CISECCION_03f_8);                  
                   return false;  
          	 	}        	   
        	   else{
        		   Log.e("RETRO Aki","11");
        		   esArrastre=true;
        		   irA(CISECCION_04Af_1);                  
        		   return false;
        	   }
         }
           

//           if(prevPage==CISECCION_04Bf_4 && curPage==CISECCION_04Bf_3 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null && prevPage>curPage && (curPage+1)==prevPage){
        	 if(prevPage==CISECCION_04Bf_5 && curPage==CISECCION_04Bf_4 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki retro indiv","13");
                  esArrastre=true;
                  irA(CISECCION_04Bf_2);                  
                  return false;            	
           }  
           
           if(prevPage==CISECCION_04Bf_5 && curPage==CISECCION_04Bf_4 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458!=1 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","14");
                  esArrastre=true;
                  irA(CISECCION_04Bf_3);                  
                  return false;            	
           }  
           
           if(prevPage==CISECCION_04Bf_10 && curPage==CISECCION_04Bf_9 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null 
        		   && !(App.getInstance().getSeccion04B().qi216==1 
        		   && App.getInstance().getSeccion04B().filtro215 
        		   && App.getInstance().getSeccion04B().qi218==1) && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","17");
                  esArrastre=true;
                  irA(CISECCION_04Bf_5);                  
                  return false;            	
           }
           
           
           if(prevPage==CISECCION_04Bf_8 && curPage==CISECCION_04Bf_7 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null 
          		 &&   !(App.getInstance().getSeccion04B().qi465db_a!=null && App.getInstance().getSeccion04B().qi465db_a==2 && App.getInstance().getSeccion04B().qi465db_b!=null && App.getInstance().getSeccion04B().qi465db_b==2 && App.getInstance().getSeccion04B().qi465db_c!=null && App.getInstance().getSeccion04B().qi465db_c==2 && App.getInstance().getSeccion04B().qi465db_d!=null && App.getInstance().getSeccion04B().qi465db_d==2) 
          		 &&   !(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi465dd_cc!=null && App.getInstance().getSeccion04B().qi465dd_cc > 0)
          		 &&   !((App.getInstance().getSeccion04B().qi465dd_ac !=null && App.getInstance().getSeccion04B().qi465dd_ar !=null && App.getInstance().getSeccion04B().qi465dd_ac < App.getInstance().getSeccion04B().qi465dd_ar) ||
          		 		(App.getInstance().getSeccion04B().qi465dd_bc !=null && App.getInstance().getSeccion04B().qi465dd_br !=null && App.getInstance().getSeccion04B().qi465dd_bc < App.getInstance().getSeccion04B().qi465dd_br) ||
          		 		(App.getInstance().getSeccion04B().qi465dd_cc !=null && App.getInstance().getSeccion04B().qi465dd_cr!=null &&  App.getInstance().getSeccion04B().qi465dd_cc < App.getInstance().getSeccion04B().qi465dd_cr) || 
          		 		(App.getInstance().getSeccion04B().qi465dd_dc !=null &&  App.getInstance().getSeccion04B().qi465dd_dr!=null && App.getInstance().getSeccion04B().qi465dd_dc < App.getInstance().getSeccion04B().qi465dd_dr))  
          		 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("Aki","16");
                  esArrastre=true;
                  irA(CISECCION_04Bf_6);                  
                  return false;            	
           } 
           //05/08/2019
           //if(prevPage==CISECCION_04Bf_21 && curPage==CISECCION_04Bf_18 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi472!=1 && prevPage>curPage && (curPage+1)==prevPage){
           if(prevPage==CISECCION_04Bf_19 && curPage==CISECCION_04Bf_18 &&  App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi472!=1 && prevPage>curPage && (curPage+1)==prevPage){
          	 Log.e("aaaAki","17");
                  esArrastre=true;
                 // debeGrabar=false;
                  irA(CISECCION_04Bf_15);                  
                  return false;            	
           }            
           
           
           /*DIT HASTA EL 2017*/
//           if(prevPage==CISECCION_04Bf_18_1 && curPage==CISECCION_04Bf_17 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=9 && App.getInstance().getSeccion04DIT().qi478 <=12 && prevPage>curPage && (curPage+1)==prevPage){
//          	 Log.e("Aki","21");
//                  esArrastre=true;
//                  debeGrabar=false;
//                  irA(CISECCION_04Bf_17);                  
//                  return false;            	
//           } 
//           
//           if(prevPage==CISECCION_04Bf_18_2  && curPage==CISECCION_04Bf_18_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=15 && App.getInstance().getSeccion04DIT().qi478 <=18 && prevPage>curPage && (curPage+1)==prevPage){
//          	 Log.e("Aki","21");
//                  esArrastre=true;
//                  debeGrabar=false;
//                  irA(CISECCION_04Bf_17);                  
//                  return false;            	
//           } 
//           
//           if(prevPage==CISECCION_04Bf_18_3  && curPage==CISECCION_04Bf_18_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=30 && App.getInstance().getSeccion04DIT().qi478 <=36 && prevPage>curPage && (curPage+1)==prevPage){
//          	 Log.e("Aki","21");
//                  esArrastre=true;
//                  debeGrabar=false;
//                  irA(CISECCION_04Bf_17);                  
//                  return false;            	
//           } 
//           
//           if(prevPage==CISECCION_04Bf_18_4  && curPage==CISECCION_04Bf_18_3 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=53 && App.getInstance().getSeccion04DIT().qi478 <=59 && prevPage>curPage && (curPage+1)==prevPage){
//          	 Log.e("Aki","21");
//                  esArrastre=true;
//                  debeGrabar=false;
//                  irA(CISECCION_04Bf_17);                  
//                  return false;            	
//           } 
           
           if(prevPage==CISECCION_04Bf_20_1 && curPage==CISECCION_04Bf_21 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=9 && App.getInstance().getSeccion04DIT_02().qi478 <=12 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO","9-12");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_2  && curPage==CISECCION_04Bf_20_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=13 && App.getInstance().getSeccion04DIT_02().qi478 <=18 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO","13-18");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_3  && curPage==CISECCION_04Bf_20_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=19 && App.getInstance().getSeccion04DIT_02().qi478 <=23 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO","19-23");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_4  && curPage==CISECCION_04Bf_20_3 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=24 && App.getInstance().getSeccion04DIT_02().qi478 <=36 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO","24-36");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             } 
             
             if(prevPage==CISECCION_04Bf_20_5  && curPage==CISECCION_04Bf_20_4 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=37 && App.getInstance().getSeccion04DIT_02().qi478 <=54 && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki","37-54");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             }
           //cambio de 60 a 59 (FCP)
             if(prevPage==CISECCION_04Bf_20_6  && curPage==CISECCION_04Bf_20_5 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=55 && App.getInstance().getSeccion04DIT_02().qi478 <=60 && App.getInstance().getNacimiento().qi215y>=App.ANIOPORDEFECTO &&  prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO","55-60");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);                  
                    return false;            	
             }
             
             if(prevPage==CISECCION_04Bf_20_6  && curPage==CISECCION_04Bf_20_5 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getNacimiento() !=null && App.getInstance().getNacimiento().qi215y!=null && App.getInstance().getNacimiento().qi215y>=App.ANIOPORDEFECTO && App.getInstance().getSeccion04DIT_02().qi478 >=App.SOLODITMIN && App.getInstance().getSeccion04DIT_02().qi478 <=App.SOLODITMAX && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO cuando si toca s4b","61-71");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_21);               
                    return false;            	
             }
             
             if(prevPage==CISECCION_04Bf_20_6  && curPage==CISECCION_04Bf_20_5 && App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getNacimiento() !=null && App.getInstance().getNacimiento().qi215y!=null && App.getInstance().getNacimiento().qi215y<App.ANIOPORDEFECTO && App.getInstance().getSeccion04DIT_02().qi478 >=App.SOLODITMIN && App.getInstance().getSeccion04DIT_02().qi478 <=App.SOLODITMAX && prevPage>curPage && (curPage+1)==prevPage){
            	 Log.e("Aki RETRO cuando no toca s4b","61-71");
                    esArrastre=true;
                    debeGrabar=false;
                    irA(CISECCION_04Bf_1);               
                    return false;            	
             }
             

             
             
             
           
           if(prevPage==CISECCION_04B2f_1  && App.getInstance().getPersonaCuestionarioIndividual()!=null && curPage<prevPage && (curPage+1)==prevPage){
        	   if (!isEsAvanceDual()  && App.getInstance().getSeccion04B2().filtro479==true) {
	            		 Log.e("RETRO Aki","14_1"); 
	                     esArrastre=true;
	                     irA(CISECCION_04Bf_1);                  
	                     return false;  
	        	   
        	   }
           }
           
           
           
           
          if(prevPage==CISECCION_04B2f_2 && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	  
        	  if (App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a == 0) {
         		 Log.e("RETRO Aki ULTIMO SOLO PARA PARA DIT","11_2 ");
                  esArrastre=true;
                  irA(CISECCION_04Bf_1);                  
                  return false;  
        	  }
        	if (App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false && App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a == 0 ) {
            		  Log.e("RETRO Aki ULTIMO","11");
            		  esArrastre=true;
            		  irA(CISECCION_03f_8);
                      return false;
            }
        	 
        		
        	  if ((App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==false && App.getInstance().getSeccion04B2().filtro481==true ) {
           		 Log.e("RETRO Aki","22");
                  esArrastre=false;
                  irA(CISECCION_04Af_1);                  
                  return false;  
           	 }
         	 
         	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos != 0 || App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true)  && App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==false ) {
          		 Log.e("RETRO Aki","14_2");
          		 esArrastre=true;
          		 irA(CISECCION_04Bf_1);                  
          		 return false;  
          	 }
          	if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos != 0 || App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==true) && App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==true  ) {
	      		  Log.e("RETRO Aki","14_3");
	      		  esArrastre=true;
	      		  irA(CISECCION_04Bf_1);                  
	      		  return false;  
	      	 }
          	
        	if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==false  ) {
	      		  Log.e("RETRO Aki","22_a");
	      		  esArrastre=true;
	      		  irA(CISECCION_04Af_1);                  
	      		  return false;  
	      	 }
           	 
           	 if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==true) {
        		 Log.e("RETRO Aki","33_a");
                 esArrastre=false;
                 debeGrabar=false;
                 irA(CISECCION_04Af_1);                  
                 return false;  
        	 }
          }
          
          
          if(prevPage==CISECCION_04B2f_3 && App.getInstance().getPersonaCuestionarioIndividual()!=null && curPage<prevPage && (curPage+1)==prevPage){
        	  if ((App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false) && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false) {
        		  Log.e("RETRO Aki","33");
        		  esArrastre=true;
        		  irA(CISECCION_04Af_1);                  
        		  return false;  
        	  }
        	  if (!isEsAvanceDual() && App.getInstance().getSeccion04B2().filtro479==false && App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false  ) {
        		  Log.e("RETRO Aki","14_3");
        		  esArrastre=true;
        		  irA(CISECCION_04Bf_1);                  
        		  return false;  
        	  }
        	  
        	  if (!isEsAvanceDual() && (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos == 0 && App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit==false)&& App.getInstance().getSeccion04B2().filtro480==true && App.getInstance().getSeccion04B2().filtro481==false) {
            		 Log.e("RETRO Aki","44_a");
                     esArrastre=false;
                     debeGrabar=false;
                     irA(CISECCION_04Af_1);                  
                     return false;  
            	 } 
              	
        	
          }
          
          if(prevPage==CISECCION_05f_1 && App.getInstance().getPersonaCuestionarioIndividual()!=null && curPage<prevPage && (curPage+1)==prevPage){
        	  Integer in490= App.getInstance().getSeccion04B2().qi490==null?0:App.getInstance().getSeccion04B2().qi490;
        	  if (in490!=5 && App.getInstance().getSeccion04B2().filtro491==false) {
          		 Log.e("RETRO Aki","26_2");
              	 esArrastre=true;
               	 nextFragment(CISECCION_04B2f_5);
               	 return false;
          	 }
          	 
        	  if (in490==5 && App.getInstance().getSeccion04B2().filtro491==false) {
            		 Log.e("RETRO Aki","26_1");
                	 esArrastre=true;
                 	 nextFragment(CISECCION_04B2f_4);
                 	 return false;
        	  }      	  
          }
          
          if(prevPage==CISECCION_01f_1 && curPage==CISECCION_f_carat && App.getInstance().getPersonaCuestionarioIndividual()!=null){
        	  Log.e("Aki","19");
          	App.getInstance().setPersonaCuestionarioIndividual(null);
          	return true;
          }
          
          if(prevPage==CISECCION_05f_5 && curPage==CISECCION_05f_4  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	  Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
        	  Integer int106= App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
        	  if (in512==0) {
         		 Log.e(" RETRO f 5_1  22" ," "+ "f_5_2 cuando in512==0"+in512);
         		esArrastre=true;
            	 nextFragment(CISECCION_05f_2);
            	 return false;
         	 } 
        	  
          }
          
          if(prevPage==CISECCION_05f_4  && curPage==CISECCION_05f_3  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	  Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
        	  Integer int106= App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
          	           
        	  if (in512!=0 && int106>=25) {
         		 Log.e("RETRO f 5_4 " ," "+ "f_5_2 cuando in512!=0 && int106>=25");
         		 esArrastre=true;
             	 nextFragment(CISECCION_05f_2);
             	 return false;
         	 }		 
          }
                    
          if(prevPage==CISECCION_06f_3  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null && curPage<prevPage && (curPage+1)==prevPage){
        	  Integer in602= App.getInstance().getCiseccion05_07().qi602==null?0:App.getInstance().getCiseccion05_07().qi602;
        	  Integer in603u= App.getInstance().getCiseccion05_07().qi603u==null?0:App.getInstance().getCiseccion05_07().qi603u;
        	  Integer in603n= App.getInstance().getCiseccion05_07().qi603n==null?0:App.getInstance().getCiseccion05_07().qi603n;
        	  Integer in310= App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
        	  Integer in226= App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
        	  
        	  	 if(	  (in602==5 && (in310==2 ||in310==0))  
                		 ||   (in602==1 && in226==1 && (in603u==1 || in603u==2))
                		 ||   (in602==2 && in226==1)
                		 ||  ((in602==1 || in602==2) && (in226==2  || in226==8) && (in310==2 ||in310==0) && ((in603u==1 && in603n>0 && in603n<24) || (in603u==2 && in603n>0 && in603n<2))) 
                		 ||	  (in602==1 && (in603u==3 || in603u==5 || in603u==6 || in603u==7) && (in310==2 ||in310==0) )		){
        			 	Log.e("RETRO f 6_3" ," "+ "f_6_1 cuando caso1");
        			 	esArrastre=true;
        			 	nextFragment(CISECCION_06f_1);
        			 	return false;
        		 }        	
          }
         
          
          if(prevPage==CISECCION_06f_4  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null && curPage<prevPage && (curPage+1)==prevPage){
        	  Integer in602= App.getInstance().getCiseccion05_07().qi602==null?0:App.getInstance().getCiseccion05_07().qi602;
        	  Integer in603u= App.getInstance().getCiseccion05_07().qi603u==null?0:App.getInstance().getCiseccion05_07().qi603u;
        	  Integer in320= App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
        	  Integer in310= App.getInstance().getPersonaCuestionarioIndividual().qi310==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi310;
        	  if ( (in320==1 || in320==2) ) {
        		  Log.e("RETRO f 6_4" ," "+ "f_5_5 cuando in320==1 || in320==2");
            	  esArrastre=true;
            	  nextFragment(CISECCION_05f_5);
            	  return false;
        	  }
        	  if(in602==3 || (in602==1 && in603u==4) || (in602==5 && in310==1) || (in602==1 && (in603u==3 || in603u==5 || in603u==6 || in603u==7) && in310==1) ){
         		 Log.e(" RETRO 6_4" ," "+ "f_6_1 cuando caso 2");
              	 esArrastre=true;
              	 nextFragment(CISECCION_06f_1);
              	 return false;          		 
        	  }
        	  if( in310==1){
         		 Log.e("RETRO f 6_4 (1)" ," "+ "f_6_2 cuando caso 3");
              	 esArrastre=true;
              	 nextFragment(CISECCION_06f_2);
              	 return false;          		 
         	 }
          }
          
        
      	 
           if(prevPage==CISECCION_07f_5  && curPage==CISECCION_07f_4  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	   Integer in501= App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
        	   Integer in709= App.getInstance().getCiseccion05_07().qi709==null?0:App.getInstance().getCiseccion05_07().qi709;
        	   if (in501==3 && in709==2 ) {
        		   Log.e("RETRO f 7_2 (1)" ," "+ "f_6_5 cuando in501==3");
        		   esArrastre=true;
        		   nextFragment(CISECCION_07f_2);
        		   return false;
        	   }	 
           }
           
           if(prevPage==CISECCION_08f_5  && curPage==CISECCION_08f_4  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	   Integer in801a= App.getInstance().getCiseccion_08().qi801a==null?0:App.getInstance().getCiseccion_08().qi801a;
        	   Integer in801b= App.getInstance().getCiseccion_08().qi801b==null?0:App.getInstance().getCiseccion_08().qi801b;
        	   Log.e("in801a","+"+in801a);
        	   Log.e("in801b","+"+in801b);
        	   
        	   if (in801a==2 && in801b==2) {
        		   Log.e("RETRO f 8_5 (1)" ," "+ "f_8_1 cuando in801a==2 && in801b==2");
        		   esArrastre=true;
        		   nextFragment(CISECCION_08f_1);
        		   return false;
        	   } 
           }
           
           if(prevPage==CISECCION_09f_1  && curPage==CISECCION_08f_7  && App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	   
        	   Integer in512= App.getInstance().getCiseccion05_07().qi512==null?0:App.getInstance().getCiseccion05_07().qi512;
        	   Integer in815=0;
        	   Integer in817b=0;
	           Integer in817c=0;
	           	
	           	
	           	Log.e("in512" ," "+ in512);
	           
	           	
	           	
	           	if (App.getInstance().getCiseccion_08()!=null) {
	           		
	           		in815= App.getInstance().getCiseccion_08().qi815==null?0:App.getInstance().getCiseccion_08().qi815;	           		
	           		in817b= App.getInstance().getCiseccion_08().qi817b==null?0:App.getInstance().getCiseccion_08().qi817b;
		           	in817c= App.getInstance().getCiseccion_08().qi817c==null?0:App.getInstance().getCiseccion_08().qi817c;
		           	Log.e("in815" ," "+in815 );
		           	Log.e("in817b" ," "+in817b );
		           	Log.e("in817c" ," "+in817c );
				}
	           	
              	 	 
        	   if (in815==2 && in512==0) {
        		   Log.e("RETRO f 9_1 (1)" ," "+ "f_8_5 cuando in815==2 && in512!=0");
        		   esArrastre=true;
        		   nextFragment(CISECCION_08f_5);
        		   return false;
        	   }
            	
        	   if ((in817b==2 || in817b==3) && (in817c==2 || in817c==3)  ) {
        		   Log.e("RETRO f 9_1 (1)" ," "+ "if ((in817b==2 || in817b==3) && (in817c==2 || in817c==3)  ) {");
        		   esArrastre=true;
        		   nextFragment(CISECCION_08f_6);
        		   return false;
        	   }
        	   
        	   if (in815==1 && in512==0) {
          		 Log.e("RETRO f 9_1 (3)" ," "+ "f_8_6 cuando in815==1 && in512==0");
          		 esArrastre=true;
              	 nextFragment(CISECCION_08f_6);
              	 return false;
          	 }
           }
           
           if(prevPage==CISECCION_10f_5  && curPage==CISECCION_10f_4  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
	           	 Integer in1005A= App.getInstance().getCiseccion_10_01().qi1005a==null?0:App.getInstance().getCiseccion_10_01().qi1005a;
	        	 Integer in1005B= App.getInstance().getCiseccion_10_01().qi1005b==null?0:App.getInstance().getCiseccion_10_01().qi1005b;
	        	 Integer in1005C= App.getInstance().getCiseccion_10_01().qi1005c==null?0:App.getInstance().getCiseccion_10_01().qi1005c;
	        	 Integer in1005D= App.getInstance().getCiseccion_10_01().qi1005d==null?0:App.getInstance().getCiseccion_10_01().qi1005d;
	        	 Integer in1005E= App.getInstance().getCiseccion_10_01().qi1005e==null?0:App.getInstance().getCiseccion_10_01().qi1005e;
	        	 Integer in1005F= App.getInstance().getCiseccion_10_01().qi1005f==null?0:App.getInstance().getCiseccion_10_01().qi1005f;
	        	 Integer in1005G= App.getInstance().getCiseccion_10_01().qi1005g==null?0:App.getInstance().getCiseccion_10_01().qi1005g;
	        	 Integer in1005H= App.getInstance().getCiseccion_10_01().qi1005h==null?0:App.getInstance().getCiseccion_10_01().qi1005h;
	        	 Integer in1005I= App.getInstance().getCiseccion_10_01().qi1005i==null?0:App.getInstance().getCiseccion_10_01().qi1005i;
	        	 
	        	 if (in1005A==2 && in1005B==2 && in1005C==2 && in1005D==2 && in1005E==2 && in1005F==2 && in1005G==2 && in1005H==2 && in1005I==2) {
	        		 Log.e("RETRO f 10_5 (1)" ," "+ "f_10_3 cuando in1005A==2 && in1005B==2 && in1005C==2 && in1005D==2 && in1005E==2 && in1005F==2 && in1005G==2 && in1005H==2 && in1005I==2");
	        		 esArrastre=true;
	        		 nextFragment(CISECCION_10f_3);
	        		 return false;
	        	 } 
           }
           //CAMBIO
           if(prevPage==CISECCION_10f_6  && curPage==CISECCION_10f_5  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	   Integer in1000A= App.getInstance().getCiseccion_10_01().qi1000a==null?0:App.getInstance().getCiseccion_10_01().qi1000a;
        	   Integer in1001A= App.getInstance().getCiseccion_10_01().qi1001a==null?0:App.getInstance().getCiseccion_10_01().qi1001a;
        	   Integer in501= App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
        	   Integer in502= App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;

//            	 if (in1000A==1 && in501==3 && in502==3 ) {
//            		 Log.e("RETRO f 10_6 (1)" ," "+ "f_10_1 cuando in501==3 && in502==3 ");
//          		   esArrastre=true;
//          		   nextFragment(CISECCION_10f_5);
//          		   return false;
//            	 }
            	 
            	 
            	 if (in1000A==1 && in1001A==1 && in501==3 && in502==3 ) {
            		 Log.e("f 10_1 (1)" ," "+ "RETRO_1 cuando in501==3 && in502==3 ");
          		   esArrastre=true;
          		   nextFragment(CISECCION_10f_5);
          		   return false;
            	 }

            	 if (in1000A==1 && in1001A==2 && in501==3 && in502==3 ) {
            		 Log.e("f 10_1 (1)" ," "+ "RETRO_2 cuando in501==3 && in502==3 ");
          		   esArrastre=true;
          		   nextFragment(CISECCION_10f_1);
          		   return false;
            	 }
            
            	 
            	 
            	 
           }
           
           
           if(prevPage==CISECCION_10f_8  && curPage==CISECCION_10f_7  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        	   Integer in1013 = App.getInstance().getCiseccion_10_01().qi1013a==null?0:App.getInstance().getCiseccion_10_01().qi1013a;
        	   Integer in1013aa = App.getInstance().getCiseccion_10_01().qi1013aa==null?0:App.getInstance().getCiseccion_10_01().qi1013aa;
          	   Integer in1013b = App.getInstance().getCiseccion_10_01().qi1013b==null?0:App.getInstance().getCiseccion_10_01().qi1013b;
        	   
        	   Integer in1019 = App.getInstance().getCiseccion_10_01().qi1019==null?0:App.getInstance().getCiseccion_10_01().qi1019;
        	   Integer in201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
        	   Integer in226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
        	   Integer in230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
        	   Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
        	   Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
        	   
        	   Integer in208=App.getInstance().getPersonaCuestionarioIndividual().qi208==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi208;
            	 
//        	   Log.e("ingreso aki" ,""+in1013 + " "+in201+" "+in226+" "+in230+" filtro"+App.getInstance().getCiseccion_10_01().filtro1014+" filtro"+App.getInstance().getCiseccion_10_01().filtro1005);
        	   if( in1013 != 0){
        		   if ((in1013==2 || in1013==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
            		   Log.e("RETRO f 10_8 (1)" ," "+ "f_10_6 cuando algun SI");
            		   esArrastre=true;
            		   nextFragment(CISECCION_10f_6);
            		   return false;
            	   }
        		   
        		 /////////////////////////////////
			            	if ( in1013==2 || in1013==8 ){
			          		if (in201==1 || in226==1 || in230==1 || in208>0) {
			          		}
			          		else{
			          		}
			          	}
			          	else{
			          		if (infiltro1014>1 ) {
			          			if((in201==1 || in226==1 || in230==1 || in208>0)) {
			                  	}
			          			else{            				
			          			}
			      			}
			          		else{            			
			          			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
			          			}
			          			else{
			          				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
					             				esArrastre=true;
					             				nextFragment(CISECCION_10f_6);  
					             				return false;
			          					
			          				}else{
			          					   
			          				}
			          				
			          			}
			          		}         
			          	}
			/////////////////////////////////
        	   }	
        	   
        	   if( in1013aa != 0){
        		   if ((in1013aa==2 || in1013aa==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
            		   Log.e("RETRO f 10_8 (1)" ," "+ "f_10_6 cuando algun SI");
            		   esArrastre=true;
            		   nextFragment(CISECCION_10f_6);
            		   return false;
            	   }
        		   
					/////////////////////////////////
						            if ( in1013aa==2 || in1013aa==8 ){
						          		if (in201==1 || in226==1 || in230==1 || in208>0) {
						          		}
						          		else{
						          		}
						          	}
						          	else{
						          		if (infiltro1014>1 ) {
						          			if((in201==1 || in226==1 || in230==1 || in208>0)) {
						                  	}
						          			else{            				
						          			}
						      			}
						          		else{            			
						          			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
						          			}
						          			else{
						          				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
								             				esArrastre=true;
								             				nextFragment(CISECCION_10f_6);  
								             				return false;
						          					
						          				}else{
						          					   
						          				}
						          				
						          			}
						          		}         
						          	}
						/////////////////////////////////
        	   }	
        	   
        	   if( in1013b != 0){
        		   if ((in1013b==2 || in1013b==8)  && (in201==2 && (in226==2 || in226==8) && in230==2) && (infiltro1014==0) && (infiltro1005!=0 || in1019==1 ) ) {
            		   Log.e("RETRO f 10_8 (1)" ," "+ "f_10_6 cuando algun SI");
            		   esArrastre=true;
            		   nextFragment(CISECCION_10f_6);
            		   return false;
            	   }
        		   
        		   	/////////////////////////////////
			            	if ( in1013b==2 || in1013b==8 ){
			          		if (in201==1 || in226==1 || in230==1 || in208>0) {
			          		}
			          		else{
			          		}
			          	}
			          	else{
			          		if (infiltro1014>1 ) {
			          			if((in201==1 || in226==1 || in230==1 || in208>0)) {
			                  	}
			          			else{            				
			          			}
			      			}
			          		else{            			
			          			if((in201==1 || in226==1 || in230==1 || in208>0)) {            			
			          			}
			          			else{
			          				if(App.getInstance().getCiseccion_10_01().filtro1014nico){				
					             				esArrastre=true;
					             				nextFragment(CISECCION_10f_6);  
					             				return false;
			          					
			          				}else{
			          					   
			          				}
			          				
			          			}
			          		}         
			          	}
			/////////////////////////////////
        	   }	
        	   
           }
           
           if(prevPage==CISECCION_10f_9  && curPage==CISECCION_10f_8  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
      		 Integer in1013 = App.getInstance().getCiseccion_10_01().qi1013a==null?0:App.getInstance().getCiseccion_10_01().qi1013a;
      		 Integer in1013aa = App.getInstance().getCiseccion_10_01().qi1013aa==null?0:App.getInstance().getCiseccion_10_01().qi1013aa;
       	     Integer in1013b = App.getInstance().getCiseccion_10_01().qi1013b==null?0:App.getInstance().getCiseccion_10_01().qi1013b;
      		 
      		 Integer in1019 = App.getInstance().getCiseccion_10_01().qi1019==null?0:App.getInstance().getCiseccion_10_01().qi1019;
          	 Integer in201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
          	 Integer in226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
          	 Integer in230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
          	 Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
          	 Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
       	 
//          	 Log.e("ingreso aki" ,""+in1013 + " "+in201+" "+in226+" "+in230+" filtro"+App.getInstance().getCiseccion_10_01().filtro1014+" filtro"+App.getInstance().getCiseccion_10_01().filtro1005);
          	if( in1013 != 0){
          		if ((in1013==2 || in1013==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
           		 Log.e("RETRO f 10_9 (2)" ," "+ "f_10_7 cuando todo es NO");
           		 esArrastre=true;
           		 nextFragment(CISECCION_10f_6);
           		 return false;
	           	 }
	           	 
	           	 if ((in1013==2 || in1013==8)  && infiltro1005==0 && in1019==2 ) {
	           		 Log.e("RETRO f 10_9 (1)" ," "+ "f_10_7 cuando todo es NO");
	           		 esArrastre=true;
	           		 nextFragment(CISECCION_10f_7);
	           		 return false;
	           	 } 
          	}
          	
          	
          	if( in1013aa != 0){
          		if ((in1013aa==2 || in1013aa==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
           		 Log.e("RETRO f 10_9 (2)" ," "+ "f_10_7 cuando todo es NO");
           		 esArrastre=true;
           		 nextFragment(CISECCION_10f_6);
           		 return false;
	           	 }
	           	 
	           	 if ((in1013aa==2 || in1013aa==8)  && infiltro1005==0 && in1019==2 ) {
	           		 Log.e("RETRO f 10_9 (1)" ," "+ "f_10_7 cuando todo es NO");
	           		 esArrastre=true;
	           		 nextFragment(CISECCION_10f_7);
	           		 return false;
	           	 } 
          	}
          	
          	
          	if( in1013b != 0){
          		if ((in1013b==2 || in1013b==8) && (in201==2 && (in226==2 || in226==8) && in230==2) && infiltro1014==0 && (infiltro1005==0 && in1019!=1 ) ) {
           		 Log.e("RETRO f 10_9 (2)" ," "+ "f_10_7 cuando todo es NO");
           		 esArrastre=true;
           		 nextFragment(CISECCION_10f_6);
           		 return false;
	           	 }
	           	 
	           	 if ((in1013b==2 || in1013b==8)  && infiltro1005==0 && in1019==2 ) {
	           		 Log.e("RETRO f 10_9 (1)" ," "+ "f_10_7 cuando todo es NO");
	           		 esArrastre=true;
	           		 nextFragment(CISECCION_10f_7);
	           		 return false;
	           	 } 
          	}
          	
           }
           

           if(prevPage==CISECCION_10f_12  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null   && curPage<prevPage && (curPage+1)==prevPage){
        	   Integer in1000A= App.getInstance().getCiseccion_10_01().qi1000a==null?0:App.getInstance().getCiseccion_10_01().qi1000a;
        	   Integer intViolen = App.getInstance().getQhviolen()==null?0:App.getInstance().getQhviolen();
        	   
        	   Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
	           	Integer int201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
	           	Integer int206 = App.getInstance().getPersonaCuestionarioIndividual().qi206==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi206;
	           	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;	           	
	           	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
          	 
	           	Integer in817b=0;
	           	Integer in817c=0;
	           	if (App.getInstance().getCiseccion_08()!=null) {
	           		in817b= App.getInstance().getCiseccion_08().qi817b==null?0:App.getInstance().getCiseccion_08().qi817b;
		           	in817c= App.getInstance().getCiseccion_08().qi817c==null?0:App.getInstance().getCiseccion_08().qi817c;
	           	 
				}
	           	
                    		
	           	Log.e("int106 rt", " " + int106);
	           	Log.e("int201 rt", " " + int201);
	           	Log.e("int206 rt", " " + int206);
	           	Log.e("int226 rt", " " + int226);
	           	Log.e("int230 rt", " " + int230);
	           	
	           	Log.e("in817c rt", " " + in817c);
	           	Log.e("in817b rt", " " + in817b);
	           	
	           	
        	   Log.e("","CURPAGE: "+curPage);
        	   Log.e("in1000A" ," "+in1000A);
        	   
//        	   if (int106>=12 && int106<=14  && int201==2 && int206==2 && (int226==2 || int226==8 )&& int230==2) {
//	           		Log.e("retro ultimo agregado 11","Pase mef de 12 a 14 a�os hasta hora de la seccion 10 ");
//	           		esArrastre=true;
//	           		nextFragment(CISECCION_02f_9);
//	           		return false;
//           		}
//        	   	else{
        	   		
        	   		if (int106>=12 && int106<=14) {
//        	   			if ((in817b==2 || in817b==3) && (in817c==2 || in817c==3)) {
//        	   				Log.e("retro ultimo agregado aa"," Regresa a la 6 ");
//                     		esArrastre=true;
//                     		nextFragment(CISECCION_08f_6);  
//                     		return false;
//						}
//        	   			else{
        	   				Log.e("retro ultimo agregado bb","regresa a la 7");
                     		esArrastre=true;
                     		nextFragment(CISECCION_09f_1);  
                     		return false;
//        	   			}
                 		
                 	}
        	   		else{
        	   			if (in1000A==2) {
                  		   Log.e("RETRO f 10_12 (1)" ," "+ "f_10_1 cuando in1000A==2 TMBAQUI");
                  		   esArrastre=true;
                  		   nextFragment(CISECCION_10f_1);
                  		   return false;
                  	   }	
                  	   
                  	   if (!App.getInstance().getCiseccion_10_01().filtro1034 && in1000A==1 ) {
                  		   Log.e("RETRO f 10_12 (2)" ," "+ "!filtro1034 && in1000A==1");
                  		   esArrastre=true;
                  		   nextFragment(CISECCION_10f_10);
                  		   return false;
                  	   }
                  	  
                  	   if (Util.esDiferente(intViolen, 0,App.getInstance().getPersonaCuestionarioIndividual().persona_id)) {
                  		   Log.e("RETRO f 10_12 (2)" ," "+ "intViolen!=App.getInstance().getPersonaCuestionarioIndividual().persona_id ENTRO");
                  		   esArrastre=true;
                  		   nextFragment(CISECCION_09f_1);
                  		   return false;
                  	   } 
        	   		}
//        	   	}
           }
           
           if(prevPage==CISECCION_10_2f_1  && App.getInstance().getCiseccion_10_01()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null  && curPage<prevPage && (curPage+1)==prevPage){
        		 if (App.getInstance().getCiseccion_10_01().filtroSiNinioS10==0) {
            		 Log.e("RETRO f 10_12 (1)" ," "+ "CUANDO NO EXISTE NI�OS ");
            		 esArrastre=true;
            		 nextFragment(CISECCION_10f_12);
            		 return false;
                 } 
           }
           

           return true;
       }
       
       
       
       public void blanquearHogar() {
                                   if (App.getInstance().getMarco() == null) {
                                           setTitulo("");
                                      return;
                                   }
                                   setTitulo(Util.getText(App.getInstance().getMarco().conglomerado)
                                                   + " - "
                                                   + Util.getText(App.getInstance().getMarco().nselv));
       }

       public void uploadData() {
             action = PROCCES.DATA;
             DialogComponent dialog = new DialogComponent(this, this,
                           TIPO_DIALOGO.YES_NO, getResources()
                                        .getString(R.string.app_name),
                           "Desea importar archivos de respaldo?");
             dialog.showDialog();
       }

       public void uploadMarco() {
             action = PROCCES.MARCO;
             DialogComponent dialog = new DialogComponent(this, this,
                           TIPO_DIALOGO.YES_NO, getResources()
                                        .getString(R.string.app_name),
                           "Desea importar archivos de marco?");
             dialog.showDialog();
       }

       @Override
       public void onCancel() {
             action = null;
       }

       @Override
       public void onAccept() {
             Intent intent = new Intent(this, FileSelectionActivity.class);
             String ruta = App.RUTA_BASE;
             if (action == PROCCES.MARCO) {
                    ruta += "/config";
                    File directorio = new File(ruta);
                    if (!directorio.exists()) {
                           directorio.mkdirs();
                    }
                    intent.putExtra(FileSelectionActivity.START_FOLDER, ruta);
                    intent.putExtra("FILTER_EXTENSION", new String[] { "cfg", "zip" });
                    startActivityForResult(intent, REQUEST_CODE_PICK_MARCO);
             } else if (action == PROCCES.DATA) {
                    ruta += "/backups";
                    File directorio = new File(ruta);
                    if (!directorio.exists()) {
                           directorio.mkdirs();
                    }
                    intent.putExtra(FileSelectionActivity.START_FOLDER, ruta);
                    intent.putExtra("FILTER_EXTENSION", new String[] { "xml", "zip" });
                    startActivityForResult(intent, REQUEST_CODE_PICK_IMPORT);
             }
             else if (action==PROCCES.GRABADOPARCIAL){
            	   int posicionActual= viewPager.getCurrentItem();
            	 Integer flag=CuestionarioFragmentActivity.this.pageAdapter.getItem(posicionActual).grabadoParcial();
          	   if(App.HOGAR==flag && posicionActual!=MARCO && posicionActual!=CARATULA){
          		   esArrastre=true;
          		   nextFragment(VISITA);
          	   }
          	   else if(App.SALUD==flag && posicionActual!=MARCO && posicionActual!=CARATULA){
          		   esArrastre=true;
          		   nextFragment(CSVISITA);
          	   }
          	 else if(App.INDIVIDUAL==flag && posicionActual!=MARCO && posicionActual!=CARATULA){
        		   esArrastre=true;
        		   nextFragment(CISECCION_01f_0);
        	   }
             }
             action = null;
       }

       protected void onActivityResult(int requestCode, int resultCode, Intent data) {
             if (resultCode != RESULT_OK)
                    return;
             ArrayList<File> files = (ArrayList<File>) data
                           .getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
             if (files != null && files.size() == 0) {
                    ToastMessage.msgBox(this,
                                  "Ning\u00fan archivo ha sido seleccionado",
                                  ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
             }
             if (requestCode == REQUEST_CODE_PICK_MARCO) {
                    importar(files);
             } else if (requestCode == REQUEST_CODE_PICK_IMPORT) {
                    importar(files);
             }
       }

       private void importar(ArrayList<File> archivos) {
             DialogComponent dlg = new DialogComponent(this, this,
                           DialogComponent.TIPO_DIALOGO.NEUTRAL, getResources().getString(
                                        R.string.app_name), "Seleccione alg�n item.");
             if (archivos.size() == 0) {
                    dlg.showDialog();
                    return;
             }
             Importacion r = new Importacion(this, "Importando informaci�n. ");
             r.setArchivos(archivos);
             r.execute();
       }

       @Override
       public void update(Observable observable, Object data) {
       }

       @Override
       public void calificar() {

       }
}

