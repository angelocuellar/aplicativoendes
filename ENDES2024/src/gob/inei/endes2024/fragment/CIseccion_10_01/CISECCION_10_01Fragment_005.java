package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_005 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1009;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1010;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1011;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1012;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1012B;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1012BN;
	
	CISECCION_10_01 individual;
	CISECCION_05_07 individualS5;
	private CuestionarioService cuestionarioService;
	public ButtonComponent btnAntes,btnDespues;
	private LabelComponent lblTitulo,lblpregunta1009,lblpregunta1010,lblpregunta1011,lblpregunta1012,lblpregunta1012A,lblQI1012bA,lblQI1012bA2;
	public TextField txtCabecera;
	public GridComponent2 gridPreguntas1012B;
	LinearLayout q0,q1,q2,q3,q4,q5;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5;
	private Calendar fecha_actual =null; 
	public CISECCION_10_01Fragment_005() {}
	public CISECCION_10_01Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override 
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1009","QI1010","QI1011","QI1012","QI1012B","QI1012BN","QI1000A","QI1005A","QI1005B","QI1005C","QI1005D","QI1005E","QI1005F","QI1005G","QI1005H", "QI1005I","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1009","QI1010","QI1011","QI1012","QI1012B","QI1012BN")};
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI707_CONS")};
		return rootView;
	}
    
    @Override 
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta1009 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1009);
		lblpregunta1010 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1010);
		lblpregunta1011 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1011);
		lblpregunta1012 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1012);
		lblpregunta1012A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1012b);
		
		rgQI1009=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1009_1,R.string.ciseccion_10_01qi1009_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1009ChangeValue");
		rgQI1010=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1010_1,R.string.ciseccion_10_01qi1010_2,R.string.ciseccion_10_01qi1010_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1011=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1011_1,R.string.ciseccion_10_01qi1011_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1011ChangeValue");
		rgQI1012=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1012_1,R.string.ciseccion_10_01qi1012_2,R.string.ciseccion_10_01qi1012_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblQI1012bA = new LabelComponent(getActivity()).textSize(19).size(altoComponente+150, 460).text(R.string.ciseccion_10_01qi1012b_pa);
		rgQI1012B = new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1012b_o1,R.string.ciseccion_10_01qi1012b_o2).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1012bAChangeValue");
		lblQI1012bA2 = new LabelComponent(getActivity()).textSize(18).size(altoComponente+100, 460).text(R.string.ciseccion_10_01qi1004_sp);
		rgQI1012BN  =new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1012b_so1,R.string.ciseccion_10_01qi1012b_so2,R.string.ciseccion_10_01qi1012b_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
				
		gridPreguntas1012B=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1012B.addComponent(lblQI1012bA);
		gridPreguntas1012B.addComponent(rgQI1012B);
		gridPreguntas1012B.addComponent(lblQI1012bA2);
		gridPreguntas1012B.addComponent(rgQI1012BN);	
    }
    
    
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1009,rgQI1009);
		q2 = createQuestionSection(lblpregunta1010,rgQI1010);
		q3 = createQuestionSection(lblpregunta1011,rgQI1011);
		q4 = createQuestionSection(lblpregunta1012,rgQI1012);
		q5 = createQuestionSection(lblpregunta1012A,gridPreguntas1012B.component());

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		return contenedor;
    }
    
    
    @Override 
    public boolean grabar() {

		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
		
		if (Util.esVacio(individual.qi1009)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1009");
			view = rgQI1009;
			error = true;
			return false;
		}
		if (!Util.esDiferente(individual.qi1009,1)) {
			if (Util.esVacio(individual.qi1010)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1010");
				view = rgQI1010;
				error = true;
				return false;
			}
		}
		
		if (Util.esVacio(individual.qi1011)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1011");
			view = rgQI1011;
			error = true;
			return false;
		}
		if (!Util.esDiferente(individual.qi1011,1)) {
			if (Util.esVacio(individual.qi1012)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1012");
				view = rgQI1012;
				error = true;
				return false;
			}
			
			if (infiltro1005>=1) {
				if (Util.esVacio(individual.qi1012b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1012B");
					view = rgQI1012B;
					error = true;
					return false;
				}
				if (individual.qi1012b==1) {
					if (Util.esVacio(individual.qi1012bn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1012BN");
						view = rgQI1012BN;
						error = true;
						return false;
					}
				}
			}
		}
	
		return true;
    }
    
    @Override 
    public void cargarDatos() {
    	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if(individualS5!=null && individualS5.qi707_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individualS5.qi707_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		
		entityToUI(individual);
		RenombrarEtiquetas();
		inicio();
    }
    
    public void validarPregunta1005(){
    	Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
//    	Log.e("105",""+infiltro1005);
 		if (infiltro1005>=1) {
 			Util.lockView(getActivity(), false,rgQI1012B,rgQI1012BN);
 			q5.setVisibility(View.VISIBLE);
 		} 
 		else {
 			Util.cleanAndLockView(getActivity(),rgQI1012B,rgQI1012BN);
 			q5.setVisibility(View.GONE);
 		}
    }

    
    private void inicio() {
    	onrgQI1009ChangeValue();
    	onrgQI1011ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void onrgQI1009ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1009.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI1010);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			rgQI1011.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1010);
			q2.setVisibility(View.VISIBLE);
			rgQI1010.requestFocus();
		}
    }
    
    public void onrgQI1011ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI1011.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI1012,rgQI1012B,rgQI1012BN);
			MyUtil.LiberarMemoria();
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1012,rgQI1012B,rgQI1012BN);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onrgQI1012bAChangeValue();
			validarPregunta1005();
			rgQI1012.requestFocus();
			
		}
    }
    
    public void onrgQI1012bAChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1012B.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI1012BN);
			lblQI1012bA2.setVisibility(View.GONE);
			rgQI1012BN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1012BN);
			lblQI1012bA2.setVisibility(View.VISIBLE);
			rgQI1012BN.setVisibility(View.VISIBLE);
			rgQI1012BN.requestFocus();
		}
    }
    
    public void RenombrarEtiquetas() {
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fecha_actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecha_actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta1010.text(R.string.ciseccion_10_01qi1010);
    	lblpregunta1010.setText(lblpregunta1010.getText().toString().replace("#", F2));
    	lblpregunta1010.setText(lblpregunta1010.getText().toString().replace("$", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta1010.setText(lblpregunta1010.getText().toString().replace("%", F3));
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1009.readOnly();
    		rgQI1010.readOnly();
    		rgQI1011.readOnly();
    		rgQI1012.readOnly();
    		rgQI1012B.readOnly();
    		rgQI1012BN.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}