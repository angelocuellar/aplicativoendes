package gob.inei.endes2024.fragment.CIseccion_10_01; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_02Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public TextAreaField txtQIOBS_EN; 
	@FieldAnnotation(orderIndex=2) 
	public TextAreaField txtQIOBS_SL; 
	@FieldAnnotation(orderIndex=3) 
	public TextAreaField txtQIOBS_SN; 

//	@FieldAnnotation(orderIndex=4) 
//	public TextAreaField chbQI456_A;
	
	
CISECCION_10_01 individual; 
	private CuestionarioService cuestionarioService; 
	
	
	private LabelComponent lblTitulo,lblTitulo1,lblTitulo2,lblTitulo3; 
	LinearLayout q0,q1,q2,q3; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoVisita; 

	public CISECCION_10_02Fragment_001() {} 
	public CISECCION_10_02Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIOBS_EN","QIOBS_SL","QIOBS_SN","ID","QI1043","QI1043_1","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIOBS_EN","QIOBS_SL","QIOBS_SN")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);  
		
		lblTitulo1=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02qiobsen).textSize(16).centrar().negrita();
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02qiobssl).textSize(16).centrar().negrita();
		lblTitulo3=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02qi1obssn).textSize(16).centrar().negrita();
		
		txtQIOBS_EN = new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
		txtQIOBS_SL = new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
		txtQIOBS_SN = new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
		
		//chbQI456_A=new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(lblTitulo1,txtQIOBS_EN); 
		q2 = createQuestionSection(lblTitulo2,txtQIOBS_SL); 
		q3 = createQuestionSection(lblTitulo3,txtQIOBS_SN); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados s10.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
  		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		
		entityToUI(individual); 
		inicio(); 
    } 
    
    private void inicio() {
    	ValidarsiesSupervisora();
    } 
    public void ValidarsiesSupervisora(){
		  Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    	txtQIOBS_EN.setEnabled(false);
	    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	} 
} 
