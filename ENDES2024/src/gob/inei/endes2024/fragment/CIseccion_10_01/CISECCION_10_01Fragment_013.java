package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_10_01.Dialog.CISECCION_10_01Fragment_011_1Dialog;
import gob.inei.endes2024.fragment.CIseccion_10_01.Dialog.CISECCION_10_01Fragment_011_2Dialog;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.icu.text.CaseMap.Title;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_013 extends FragmentForm implements Respondible {
	

	public TextAreaField txtQIOBS_VA;
	public TextAreaField txtQIOBS_CT; 
	
	
//	public CheckBoxField chbQI466E_1A;
//	public CheckBoxField chbQI466E_1B;
//	public CheckBoxField chbQI466E_1X;
//	public TextField txtQI466E_1O;


	
	public List<CISECCION_02> detalles;
	Hogar bean;
	CISECCION_10_01 individual; 
	
	//CISECCION_04B individual4B2;
	
	private CuestionarioService cuestionarioService;
	public TableComponent tcPersonas,tcPersonas2;
	Seccion01ClickListener adapter;
	Seccion01ClickListener2 adapter2;
	public TextField txtCabecera;	
	public LabelComponent lblTitulo,lblTitulo3,lblTitulo4,lblpregunta456,lblpregunta466E,lblpregunta466E_ind,lblpregunta466E_ind2,lblpregunta456_ind,lblpregunta456_ind2,lblpregunta456_ind3,lblTitulo1,lblTitulo2;
	
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabado1;
	SeccionCapitulo[] seccionesCargado,seccionesCargado1, seccionesCargado2;
	
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	//public ACTION_NACIMIENTO siguienteaccion;

	public CISECCION_10_01Fragment_013() {}
	public CISECCION_10_01Fragment_013 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		adapter2 = new Seccion01ClickListener2();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		tcPersonas2.getListView().setOnItemClickListener(adapter2);
		enlazarCajas();
		listening();
		seccionesCargado1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIOBS_VA","QIOBS_CT","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIOBS_VA","QIOBS_CT")};
		
		seccionesCargado = new SeccionCapitulo[]{ new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","QI217","QI215D","QI215M","QI215Y","ID","HOGAR_ID","PERSONA_ID") };

		//seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI454","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
		//seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI454","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView;
	}
    
    @Override
    protected void buildFields() {
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01_4).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta456 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi456).negrita();
		lblpregunta456_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi456_ind);
		lblpregunta456_ind2= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi456_ind2).negrita();
		lblpregunta456_ind3= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi456_ind3).negrita();
		lblpregunta466E = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi466E);
		lblpregunta466E_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi466E_ind).negrita();
		lblpregunta466E_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi466E_ind2).negrita();
		lblTitulo1=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02qiobsen1).textSize(16).centrar().negrita();
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02qiobsen2).textSize(16).centrar().negrita();
		//lblTitulo3=new LabelComponent(this.getActivity()).text(R.string.c2seccion_04bqi456_1).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();

		//lblTitulo4=new LabelComponent(this.getActivity()).text(R.string.c2seccion_04bqi466E_1).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();

		
		txtQIOBS_VA = new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
		txtQIOBS_CT = new TextAreaField(getActivity()).maxLength(10000).size(200, 650).alfanumerico();
		
//		chbQI456_1A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbQI456_1B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		txtQI456_1O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar(); 
//		chbQI456_1X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
// 
//		chbQI456_1X.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (isChecked ){
//					
//					Util.lockView(getActivity(), false,txtQI456_1O);
//		  		}
//				else {
//					Util.cleanAndLockView(getActivity(),txtQI456_1O);
//		  		}
//			}
//		});
		
		
		
		
//		chbQI466E_1A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbQI466E_1B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		
//		txtQI466E_1O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar(); 
//		chbQI466E_1X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI466E_XChangeValue");
//		
//		chbQI466E_1X.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (isChecked ){
//					
//					Util.lockView(getActivity(), false,txtQI466E_1O);
//		  		}
//				else {
//					Util.cleanAndLockView(getActivity(),txtQI466E_1O);
//		  		}
//			}
//		});
		
		
		
		
		
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(250, 700).headerHeight(altoComponente + 10).dataColumHeight(50);
//			tcPersonas2 = new TableComponent(getActivity(), this, App.ESTILO).size(250, 700).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(350, 700).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tcPersonas2 = new TableComponent(getActivity(), this, App.ESTILO).size(350, 700).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcPersonas.addHeader(R.string.seccion01_nro_ordens2, 0.25f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01nombress2, 1f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01_anioscumplidos, 0.5f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01_fechanacimiento,0.5f,TableComponent.ALIGN.CENTER);
		
		
		tcPersonas2.addHeader(R.string.seccion01_nro_ordens2, 0.25f,TableComponent.ALIGN.CENTER);
		tcPersonas2.addHeader(R.string.seccion01nombress2, 1f,TableComponent.ALIGN.LEFT);
		tcPersonas2.addHeader(R.string.seccion01_anioscumplidos, 0.5f,TableComponent.ALIGN.LEFT);
		tcPersonas2.addHeader(R.string.seccion01_fechanacimiento,0.5f,TableComponent.ALIGN.CENTER);
    }
  
    public void abrirDetalle(CISECCION_02 tmp) {
		FragmentManager fm = CISECCION_10_01Fragment_013.this.getFragmentManager();
		CISECCION_10_01Fragment_011_1Dialog aperturaDialog = CISECCION_10_01Fragment_011_1Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public void abrirDetalle2(CISECCION_02 tmp) {
    	FragmentManager fm = CISECCION_10_01Fragment_013.this.getFragmentManager();
    	CISECCION_10_01Fragment_011_2Dialog aperturaDialog = CISECCION_10_01Fragment_011_2Dialog.newInstance(this, tmp);
		aperturaDialog.show(fm, "aperturaDialog");
	}

    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta456,lblpregunta456_ind2,lblpregunta456_ind,tcPersonas.getTableView(),lblTitulo1,txtQIOBS_VA);
		q2 = createQuestionSection(lblpregunta466E,lblpregunta466E_ind2,tcPersonas2.getTableView(),lblTitulo2,txtQIOBS_CT);
//		LinearLayout ly1032 = new LinearLayout(getActivity());
//		ly1032.addView(chbQI456_1X);
//		ly1032.addView(txtQI456_1O);
		
//		LinearLayout ly1033 = new LinearLayout(getActivity());
//		ly1033.addView(chbQI466E_1X);
//		ly1033.addView(txtQI466E_1O);
//		//q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblTitulo3,chbQI456_1A,chbQI456_1B,ly1032);
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblTitulo4,chbQI466E_1A,chbQI466E_1B,ly1033);
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		
		form.addView(q0);
		form.addView(q1);
		//form.addView(q3);
		form.addView(q2);
		//form.addView(q4);

		
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
    	uiToEntity(individual); 
    	
    	
    	if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
    	//individual.qi456_a = 0 ;
    	//ToastMessage.msgBox(this.getActivity(),individual.qi456_a == null?"cero":individual.qi456_a.toString() , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado1)){ 
				
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados s10.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
   
 
		
		return true; 
    }
    
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		detalles = getCuestionarioService().getListaNacimientos4bTarjetabyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		
		//Integer cant = detalles.size()==0?-1:detalles.size();
		
		if(detalles.size()>0){
//			if(individual.qi456_1a==0 && individual.qi456_1b==0 && individual.qi456_1x==0){
//				mensaje = "Debe seleccionar al menos una alternativa 456";
//				view =chbQI456_1A;
//				error = true;
//				return false;
//			}
//			
//			if (chbQI456_1X.isChecked()) {
//				if (Util.esVacio(individual.qi456_1o)) {
//					mensaje = "Debe ingresar informaci\u00f3n en Especifique 456";
//					view = txtQI456_1O;
//					error = true;
//					return false;
//				}
//			}
//			
//			if(individual.qi466e_1a==0 && individual.qi466e_1b==0 && individual.qi466e_1x==0){
//				mensaje = "Debe seleccionar al menos una alternativa 466E";
//				view =chbQI456_1A;
//				error = true;
//				return false;
//			}
			
			
//			if (chbQI466E_1X.isChecked()) {
//				
//				if (Util.esVacio(individual.qi466e_1o)) {
//					mensaje = "Debe ingresar informaci\u00f3n en Especifique 466E";
//					view = txtQI466E_1O;
//					error = true;
//					return false;
//				}
//			}
		}
	
		return true;
    }
    
    
   
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado1);
    	//estadocarne = getCuestionarioService().getNiniosVivosYquetienenLatarjetadevacunacion(bean.id,bean.hogar_id);
		//detalles = getCuestionarioService().getListaNacimientos4bTarjetabyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);

    	//individual4B2 = getCuestionarioService().getCISECCION_04B(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,1,seccionesCargado2);
    	
    	//Log.e("Num de controles", "vvvvvvvvvvvv"+individual4B2.qi454);
    	//Log.e("holaaaaaaaa", "hhhhhhhhhhh"+detalles.size());

    	
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if(App.getInstance().getHogar()!=null){
    		cargarTabla();
    	}
		
		//ToastMessage.msgBox(this.getActivity(), estadocarne.qi454.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		
		
		
		entityToUI(individual); 
		inicio(); 
    }
    
    public void refrescarPersonas(CISECCION_02 personas) {
		if (detalles.contains(personas)) {
			cargarTabla();
			return;
		}else {
			detalles.add(personas);
			cargarTabla();
		}
	}
    
    public void cargarTabla() {
    	
    	// getNacimientosVivoListbyPersona 
		detalles = getCuestionarioService().getListaNacimientos4bTarjetabyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		tcPersonas.setData(detalles, "getQi212", "getQi212_nom", "qi217","getQi215");
		registerForContextMenu(tcPersonas.getListView());
		tcPersonas2.setData(detalles, "getQi212", "getQi212_nom", "qi217","getQi215");
		registerForContextMenu(tcPersonas2.getListView());
	}
    
    
    private void ValidarFiltro456() {
    	if (App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos==0) {
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		//q3.setVisibility(View.GONE);
    		//q4.setVisibility(View.GONE);
		}
    	else{
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		//q3.setVisibility(View.VISIBLE);
    		//q4.setVisibility(View.VISIBLE);
    	}
    }
    
    private void inicio() {
    	ValidarFiltro456();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
   
    
    public void ValidarsiesSupervisora(){
		  Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    	txtQIOBS_CT.setEnabled(false);
	    	txtQIOBS_VA.setEnabled(false);
	    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_02 c = (CISECCION_02) detalles.get(arg2);
			abrirDetalle(c);
		}
	}
    private class Seccion01ClickListener2 implements OnItemClickListener {
		public Seccion01ClickListener2() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_02 c = (CISECCION_02) detalles.get(arg2);
			abrirDetalle2(c);
		}
	}
    
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcPersonas.getListView())) {
			menu.setHeaderTitle("Opciones");
			menu.add(1, 0, 1, "Editar Registro");
			//menu.add(1, 1, 1, "Eliminar Registro");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = detalles.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			
			CISECCION_02 seleccion = (CISECCION_02) info.targetView.getTag();
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				menu.getItem(0).setVisible(false);
				menu.getItem(1).setVisible(false);
			}
		}
		else if (v.equals(tcPersonas2.getListView())) {
			menu.setHeaderTitle("Opciones");
			menu.add(2, 0, 1, "Editar Registro");
			//menu.add(2, 1, 1, "Eliminar Registro");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = detalles.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			
			CISECCION_02 seleccion = (CISECCION_02) info.targetView.getTag();
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				menu.getItem(0).setVisible(false);
				menu.getItem(1).setVisible(false);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalle((CISECCION_02) detalles.get(info.position));
				break;
			/*case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_02) detalles.get(info.position));
				dialog.showDialog();
				break;*/

			}
		}else if (item.getGroupId() == 2) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalle2((CISECCION_02) detalles.get(info.position));
				break;
			/*case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_02) detalles.get(info.position));
				dialog.showDialog();
				break;*/

			}
		}
		return super.onContextItemSelected(item);
	}
    
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
//		uiToEntity(individual);
//		try {
//			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado1)){
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//				return App.NODEFINIDO;
//			}
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//			return App.NODEFINIDO;
//		}
//		return App.INDIVIDUAL;
		
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
		/*if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		CISECCION_02 bean = (CISECCION_02) dialog.get("seleccion");
		try {
			detalles.remove(bean);
			boolean borrado = false;
			borrado = getCuestionarioService().BorrarNacimiento(bean.id, bean.hogar_id, bean.persona_id, bean.qi212);
			Toast.makeText(getActivity(), "fragment", Toast.LENGTH_SHORT).show();
			if(detalles.size()>0){
			}
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		}catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}*/
		try {
			boolean borrado = false;
			//borrado = getCuestionarioService().BorrarNacimiento(bean.id, bean.hogar_id, bean.persona_id, bean.qi212);
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		}catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	}
}