package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1003A;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1003AN;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1003B;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1003BN;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1003C;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1003CN;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1003D;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1003DN;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1003E;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1003EN;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI1003F;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI1003FN;
	
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI1004A;
	@FieldAnnotation(orderIndex=14)
	public RadioGroupOtherField rgQI1004AN;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI1004B;
	@FieldAnnotation(orderIndex=16)
	public RadioGroupOtherField rgQI1004BN;
	@FieldAnnotation(orderIndex=17)
	public RadioGroupOtherField rgQI1004C;
	@FieldAnnotation(orderIndex=18)
	public RadioGroupOtherField rgQI1004CN;
	@FieldAnnotation(orderIndex=19)
	public RadioGroupOtherField rgQI1004D;
	@FieldAnnotation(orderIndex=20)
	public RadioGroupOtherField rgQI1004DN;
	
	@FieldAnnotation(orderIndex=21)
	public RadioGroupOtherField rgQI1004E;
	@FieldAnnotation(orderIndex=22)
	public RadioGroupOtherField rgQI1004EN;
	@FieldAnnotation(orderIndex=23)
	public RadioGroupOtherField rgQI1004F;
	@FieldAnnotation(orderIndex=24)
	public RadioGroupOtherField rgQI1004FN;
	@FieldAnnotation(orderIndex=25)
	public RadioGroupOtherField rgQI1004G;
	@FieldAnnotation(orderIndex=26)
	public RadioGroupOtherField rgQI1004GN;
	@FieldAnnotation(orderIndex=27)
	public RadioGroupOtherField rgQI1004H;
	@FieldAnnotation(orderIndex=28)
	public RadioGroupOtherField rgQI1004HN;
	
	
	
	CISECCION_10_01 individual;
	//CISECCION_05_07 individualS5;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1004,lblpregunta1003, lblQI1003A2,lblQI1003B2,lblQI1003C2,lblQI1003D2,lblQI1003E2,lblQI1003F2;
	private LabelComponent lblQI1003A,lblQI1003B,lblQI1003C,lblQI1003D,lblQI1003E,lblQI1003F,lblQI1004A,lblQI1004B,lblQI1004C,lblQI1004D,lblQI1004E,lblQI1004F,lblQI1004G,lblQI1004H,lblQI1004A2,lblQI1004B2,lblQI1004C2,lblQI1004D2,lblQI1004E2,lblQI1004F2,lblQI1004G2,lblQI1004H2;
	public TextField txtCabecera;
	
	LinearLayout q0,q1,q2;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5;
	GridComponent2 gridPreguntas1003,gridPreguntas1004;

	public CISECCION_10_01Fragment_002() {}
	public CISECCION_10_01Fragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override 
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1003A","QI1003AN","QI1003B","QI1003BN","QI1003C","QI1003CN","QI1003D","QI1003DN","QI1003E","QI1003EN","QI1003F","QI1003FN","QI1004A","QI1004AN","QI1004B","QI1004BN","QI1004C","QI1004CN","QI1004D","QI1004DN","QI1004E","QI1004EN","QI1004F","QI1004FN","QI1004G","QI1004GN","QI1004H","QI1004HN","QI1000A","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1003A","QI1003AN","QI1003B","QI1003BN","QI1003C","QI1003CN","QI1003D","QI1003DN","QI1003E","QI1003EN","QI1003F","QI1003FN","QI1004A","QI1004AN","QI1004B","QI1004BN","QI1004C","QI1004CN","QI1004D","QI1004DN","QI1004E","QI1004EN","QI1004F","QI1004FN","QI1004G","QI1004GN","QI1004H","QI1004HN")};
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI501","QI502","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView;
	}
    
    @Override 
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1003 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1003);
		lblpregunta1004 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1004);
		
//		lblblanco1 = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 500).negrita().centrar();
//		lblP1003_1 = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 80).text(R.string.ciseccion_10_01qi1003_1).centrar().negrita();
//		lblP1003_2 = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 80).text(R.string.ciseccion_10_01qi1003_2).centrar().negrita();
//		lblP1003_3 = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 80).text(R.string.ciseccion_10_01qi1003_3).centrar().negrita();

		lblQI1003A=new LabelComponent(getActivity()).textSize(17).size(altoComponente+60, 470).text(R.string.ciseccion_10_01qi1003_pa);
		lblQI1003B=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1003_pb);
		lblQI1003C=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1003_pc);
		lblQI1003D=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1003_pd);
		lblQI1003E=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1003_pe);
		lblQI1003F=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1003_pf);
		
		rgQI1003A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+40,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003AChangeValue");
		rgQI1003B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+20,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003BChangeValue");
		rgQI1003C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+20,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003CChangeValue");
		rgQI1003D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+20,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003DChangeValue");
		rgQI1003E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+20,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003EChangeValue");
		rgQI1003F=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_o1,R.string.ciseccion_10_01qi1003_o2,R.string.ciseccion_10_01qi1003_o3).size(altoComponente+20,240).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1003FChangeValue");
		
		lblQI1003A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		lblQI1003B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		lblQI1003C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		lblQI1003D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		lblQI1003E2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		lblQI1003F2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1003_sp);
		
		rgQI1003AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1003BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1003CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1003DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1003EN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1003FN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1003_so1,R.string.ciseccion_10_01qi1003_so2,R.string.ciseccion_10_01qi1003_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		
		

		lblQI1004A =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1004_pa);
		lblQI1004B =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 470).text(R.string.ciseccion_10_01qi1004_pb);
		lblQI1004C =new LabelComponent(getActivity()).textSize(17).size(altoComponente+30, 470).text(R.string.ciseccion_10_01qi1004_pc);
		lblQI1004D =new LabelComponent(getActivity()).textSize(17).size(altoComponente+30, 470).text(R.string.ciseccion_10_01qi1004_pd);
		lblQI1004E =new LabelComponent(getActivity()).textSize(17).size(altoComponente+40, 470).text(R.string.ciseccion_10_01qi1004_pe);
		lblQI1004F =new LabelComponent(getActivity()).textSize(17).size(altoComponente+40, 470).text(R.string.ciseccion_10_01qi1004_pf);
		lblQI1004G =new LabelComponent(getActivity()).textSize(17).size(altoComponente+50, 470).text(R.string.ciseccion_10_01qi1004_pg);
		lblQI1004H =new LabelComponent(getActivity()).textSize(17).size(altoComponente+50, 470).text(R.string.ciseccion_10_01qi1004_ph);
		
		rgQI1004A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004AChangeValue");
		rgQI1004B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004BChangeValue");
		rgQI1004C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+30,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004CChangeValue");
		rgQI1004D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+30,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004DChangeValue");
		rgQI1004E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+40,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004EChangeValue");
		rgQI1004F=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+40,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004FChangeValue");
		rgQI1004G=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+50,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004GChangeValue");
		rgQI1004H=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_o1,R.string.ciseccion_10_01qi1004_o2).size(altoComponente+50,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1004HChangeValue");
		
		lblQI1004A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004E2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004F2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004G2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		lblQI1004H2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1004_sp);
		
		rgQI1004AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004EN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004FN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004GN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1004HN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);

		
		gridPreguntas1003=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1003.addComponent(lblQI1003A);
		gridPreguntas1003.addComponent(rgQI1003A);
		gridPreguntas1003.addComponent(lblQI1003A2);
		gridPreguntas1003.addComponent(rgQI1003AN);
		
		gridPreguntas1003.addComponent(lblQI1003B);
		gridPreguntas1003.addComponent(rgQI1003B);
		gridPreguntas1003.addComponent(lblQI1003B2);
		gridPreguntas1003.addComponent(rgQI1003BN);
					
		gridPreguntas1003.addComponent(lblQI1003C);
		gridPreguntas1003.addComponent(rgQI1003C);
		gridPreguntas1003.addComponent(lblQI1003C2);
		gridPreguntas1003.addComponent(rgQI1003CN);
		
		gridPreguntas1003.addComponent(lblQI1003D);
		gridPreguntas1003.addComponent(rgQI1003D);
		gridPreguntas1003.addComponent(lblQI1003D2);
		gridPreguntas1003.addComponent(rgQI1003DN);
		
		gridPreguntas1003.addComponent(lblQI1003E);
		gridPreguntas1003.addComponent(rgQI1003E);
		gridPreguntas1003.addComponent(lblQI1003E2);
		gridPreguntas1003.addComponent(rgQI1003EN);
		
		gridPreguntas1003.addComponent(lblQI1003F);
		gridPreguntas1003.addComponent(rgQI1003F);
		gridPreguntas1003.addComponent(lblQI1003F2);
		gridPreguntas1003.addComponent(rgQI1003FN);
		
		
		gridPreguntas1004=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1004.addComponent(lblQI1004A);
		gridPreguntas1004.addComponent(rgQI1004A);
		gridPreguntas1004.addComponent(lblQI1004A2);
		gridPreguntas1004.addComponent(rgQI1004AN);
		
		gridPreguntas1004.addComponent(lblQI1004B);
		gridPreguntas1004.addComponent(rgQI1004B);
		gridPreguntas1004.addComponent(lblQI1004B2);		
		gridPreguntas1004.addComponent(rgQI1004BN);
		
		gridPreguntas1004.addComponent(lblQI1004C);
		gridPreguntas1004.addComponent(rgQI1004C);
		gridPreguntas1004.addComponent(lblQI1004C2);
		gridPreguntas1004.addComponent(rgQI1004CN);
		
		gridPreguntas1004.addComponent(lblQI1004D);
		gridPreguntas1004.addComponent(rgQI1004D);
		gridPreguntas1004.addComponent(lblQI1004D2);
		gridPreguntas1004.addComponent(rgQI1004DN);
		
		gridPreguntas1004.addComponent(lblQI1004E);
		gridPreguntas1004.addComponent(rgQI1004E);
		gridPreguntas1004.addComponent(lblQI1004E2);
		gridPreguntas1004.addComponent(rgQI1004EN);
		
		gridPreguntas1004.addComponent(lblQI1004F);
		gridPreguntas1004.addComponent(rgQI1004F);
		gridPreguntas1004.addComponent(lblQI1004F2);
		gridPreguntas1004.addComponent(rgQI1004FN);
		
		gridPreguntas1004.addComponent(lblQI1004G);
		gridPreguntas1004.addComponent(rgQI1004G);
		gridPreguntas1004.addComponent(lblQI1004G2);
		gridPreguntas1004.addComponent(rgQI1004GN);
		
		gridPreguntas1004.addComponent(lblQI1004H);
		gridPreguntas1004.addComponent(rgQI1004H);
		gridPreguntas1004.addComponent(lblQI1004H2);
		gridPreguntas1004.addComponent(rgQI1004HN);
    }
    
    @Override 
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1003,gridPreguntas1003.component());
		q2 = createQuestionSection(lblpregunta1004,gridPreguntas1004.component());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
		Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	
		if (in1000a==1 && (in501!=3 || in502!=3)) {
			if (Util.esVacio(individual.qi1003a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003A");
				view = rgQI1003A;
				error = true;
				return false;
			}
			if (individual.qi1003a==1) {
				if (Util.esVacio(individual.qi1003an)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003AN");
					view = rgQI1003AN;
					error = true;
					return false;
				}
			}
			
			if (Util.esVacio(individual.qi1003b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003B");
				view = rgQI1003B;
				error = true;
				return false;
			}
			if (individual.qi1003b==1) {
				if (Util.esVacio(individual.qi1003bn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003BN");
					view = rgQI1003BN;
					error = true;
					return false;
				}
			}
			
			if (Util.esVacio(individual.qi1003c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003C");
				view = rgQI1003C;
				error = true;
				return false;
			}
			if (individual.qi1003c==1) {
				if (Util.esVacio(individual.qi1003cn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003CN");
					view = rgQI1003CN;
					error = true;
					return false;
				}
			}
			
			if (Util.esVacio(individual.qi1003d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003D");
				view = rgQI1003D;
				error = true;
				return false;
			}
			if (individual.qi1003d==1) {
				if (Util.esVacio(individual.qi1003dn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003DN");
					view = rgQI1003DN;
					error = true;
					return false;
				}
			}
			
			if (Util.esVacio(individual.qi1003e)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003E");
				view = rgQI1003E;
				error = true;
				return false;
			}
			if (individual.qi1003e==1) {
				if (Util.esVacio(individual.qi1003en)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003EN");
					view = rgQI1003EN;
					error = true;
					return false;
				}
			}
			
			if (Util.esVacio(individual.qi1003f)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1003F");
				view = rgQI1003F;
				error = true;
				return false;
			}
			if (individual.qi1003f==1) {
				if (Util.esVacio(individual.qi1003fn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1003FN");
					view = rgQI1003FN;
					error = true;
					return false;
				}
			}
			
			
			
			
			if (Util.esVacio(individual.qi1004a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004A");
				view = rgQI1004A;
				error = true;
				return false;
			}
			if (individual.qi1004a==1) {
				if (Util.esVacio(individual.qi1004an)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004AN");
					view = rgQI1004AN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1004b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004B");
				view = rgQI1004B;
				error = true;
				return false;
			}
			if (individual.qi1004b==1) {
				if (Util.esVacio(individual.qi1004bn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004BN");
					view = rgQI1004BN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1004c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004C");
				view = rgQI1004C;
				error = true;
				return false;
			}
			if (individual.qi1004c==1) {
				if (Util.esVacio(individual.qi1004cn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004CN");
					view = rgQI1004CN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1004d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004D");
				view = rgQI1004D;
				error = true;
				return false;
			}
			if (individual.qi1004d==1) {
				if (Util.esVacio(individual.qi1004dn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004DN");
					view = rgQI1004DN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1004e)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004E");
				view = rgQI1004E;
				error = true;
				return false;
			}
			if (individual.qi1004e==1) {
				if (Util.esVacio(individual.qi1004en)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004EN");
					view = rgQI1004EN;
					error = true;
					return false;
				}
			}	
			if (Util.esVacio(individual.qi1004f)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004F");
				view = rgQI1004F;
				error = true;
				return false;
			}
			if (individual.qi1004f==1) {
				if (Util.esVacio(individual.qi1004fn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004FN");
					view = rgQI1004FN;
					error = true;
					return false;
				}
			}	
			if (Util.esVacio(individual.qi1004g)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004G");
				view = rgQI1004G;
				error = true;
				return false;
			}
			if (individual.qi1004g==1) {
				if (Util.esVacio(individual.qi1004gn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004GN");
					view = rgQI1004GN;
					error = true;
					return false;
				}
			}	
			if (Util.esVacio(individual.qi1004h)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1004H");
				view = rgQI1004H;
				error = true;
				return false;
			}
			if (individual.qi1004h==1) {
				if (Util.esVacio(individual.qi1004hn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1004HN");
					view = rgQI1004HN;
					error = true;
					return false;
				}
			}	
			
		}
		
		return true;
    }
    
    @Override 
    public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
	 	//individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	/*App.getInstance().getCiseccion05_07().qi501=individualS5.qi501;
    	App.getInstance().getCiseccion05_07().qi502=individualS5.qi502;*/
    	
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		Log.e("ci2", "entro al ci2");
		entityToUI(individual);
		inicio();
    }
    
//    public void ValidarPregunta1000A() {
//    	Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
//    	Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
//    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
//    	
//    	if (in1000a!=1 || (in501==3 && in502==3)) {
//    		Util.cleanAndLockView(getActivity(),rgQI1003A,rgQI1003AN,rgQI1003B,rgQI1003BN,rgQI1003C,rgQI1003CN,rgQI1003D,rgQI1003DN,rgQI1003E,rgQI1003EN,rgQI1003F,rgQI1003FN,rgQI1004A,rgQI1004AN,rgQI1004B,rgQI1004BN,rgQI1004C,rgQI1004CN,rgQI1004D,rgQI1004DN,rgQI1004E,rgQI1004EN,rgQI1004F,rgQI1004FN,rgQI1004G,rgQI1004GN,rgQI1004H,rgQI1004HN);
//    		q0.setVisibility(View.GONE);
//    		q1.setVisibility(View.GONE);
//    		q2.setVisibility(View.GONE);
//		}
//    	else{
//    		Util.lockView(getActivity(), false,rgQI1003A,rgQI1003AN,rgQI1003B,rgQI1003BN,rgQI1003C,rgQI1003CN,rgQI1003D,rgQI1003DN,rgQI1003E,rgQI1003EN,rgQI1003F,rgQI1003FN,rgQI1004A,rgQI1004AN,rgQI1004B,rgQI1004BN,rgQI1004C,rgQI1004CN,rgQI1004D,rgQI1004DN,rgQI1004E,rgQI1004EN,rgQI1004F,rgQI1004FN,rgQI1004G,rgQI1004GN,rgQI1004H,rgQI1004HN);
//    		q0.setVisibility(View.VISIBLE);
//    		q1.setVisibility(View.VISIBLE);
//    		q2.setVisibility(View.VISIBLE);
//    		
//    		onrgQI1003AChangeValue();
//    		onrgQI1003BChangeValue();
//    		onrgQI1003CChangeValue();
//    		onrgQI1003DChangeValue();
//    		onrgQI1003EChangeValue();
//    		onrgQI1003FChangeValue();
//    		
//    		onrgQI1004AChangeValue();
//        	onrgQI1004BChangeValue();
//        	onrgQI1004CChangeValue();
//        	onrgQI1004DChangeValue();
//        	onrgQI1004EChangeValue();
//        	onrgQI1004FChangeValue();
//        	onrgQI1004GChangeValue();
//        	onrgQI1004HChangeValue();
//    	}
//	}
    
    
    public void ValidarPregunta1000A() {
    	Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
    	Integer in1001a = individual.qi1001a==null?0:individual.qi1001a;
    	Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	
    	if (in1000a==1 && in1001a==2 && (in501==3 && in502==3)) {
    		Util.cleanAndLockView(getActivity(),rgQI1003A,rgQI1003AN,rgQI1003B,rgQI1003BN,rgQI1003C,rgQI1003CN,rgQI1003D,rgQI1003DN,rgQI1003E,rgQI1003EN,rgQI1003F,rgQI1003FN,rgQI1004A,rgQI1004AN,rgQI1004B,rgQI1004BN,rgQI1004C,rgQI1004CN,rgQI1004D,rgQI1004DN,rgQI1004E,rgQI1004EN,rgQI1004F,rgQI1004FN,rgQI1004G,rgQI1004GN,rgQI1004H,rgQI1004HN);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
		}
    	else{
    		Util.lockView(getActivity(), false,rgQI1003A,rgQI1003AN,rgQI1003B,rgQI1003BN,rgQI1003C,rgQI1003CN,rgQI1003D,rgQI1003DN,rgQI1003E,rgQI1003EN,rgQI1003F,rgQI1003FN,rgQI1004A,rgQI1004AN,rgQI1004B,rgQI1004BN,rgQI1004C,rgQI1004CN,rgQI1004D,rgQI1004DN,rgQI1004E,rgQI1004EN,rgQI1004F,rgQI1004FN,rgQI1004G,rgQI1004GN,rgQI1004H,rgQI1004HN);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		
    		onrgQI1003AChangeValue();
    		onrgQI1003BChangeValue();
    		onrgQI1003CChangeValue();
    		onrgQI1003DChangeValue();
    		onrgQI1003EChangeValue();
    		onrgQI1003FChangeValue();
    		
    		onrgQI1004AChangeValue();
        	onrgQI1004BChangeValue();
        	onrgQI1004CChangeValue();
        	onrgQI1004DChangeValue();
        	onrgQI1004EChangeValue();
        	onrgQI1004FChangeValue();
        	onrgQI1004GChangeValue();
        	onrgQI1004HChangeValue();
    	}
	}
    
    private void inicio() {
    	ValidarPregunta1000A();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    
    }
    
    public void onrgQI1003AChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003A.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003AN);
			lblQI1003A2.setVisibility(View.GONE);
			rgQI1003AN.setVisibility(View.GONE);
			rgQI1003B.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003AN);
			lblQI1003A2.setVisibility(View.VISIBLE);
			rgQI1003AN.setVisibility(View.VISIBLE);
			rgQI1003AN.requestFocus();
		}
    }
    
    public void onrgQI1003BChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003B.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003BN);
			lblQI1003B2.setVisibility(View.GONE);
			rgQI1003BN.setVisibility(View.GONE);
			rgQI1003C.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003BN);
			lblQI1003B2.setVisibility(View.VISIBLE);
			rgQI1003BN.setVisibility(View.VISIBLE);
			rgQI1003BN.requestFocus();
		}
    }
    
    public void onrgQI1003CChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003C.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003CN);
			lblQI1003C2.setVisibility(View.GONE);
			rgQI1003CN.setVisibility(View.GONE);
			rgQI1003D.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003CN);
			lblQI1003C2.setVisibility(View.VISIBLE);
			rgQI1003CN.setVisibility(View.VISIBLE);
			rgQI1003CN.requestFocus();
		}
    }
    
    public void onrgQI1003DChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003D.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003DN);
			lblQI1003D2.setVisibility(View.GONE);
			rgQI1003DN.setVisibility(View.GONE);
			rgQI1003E.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003DN);
			lblQI1003D2.setVisibility(View.VISIBLE);
			rgQI1003DN.setVisibility(View.VISIBLE);
			rgQI1003DN.requestFocus();
		}
    }
    
    public void onrgQI1003EChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003E.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003EN);
			lblQI1003E2.setVisibility(View.GONE);
			rgQI1003EN.setVisibility(View.GONE);
			rgQI1003F.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003EN);
			lblQI1003E2.setVisibility(View.VISIBLE);
			rgQI1003EN.setVisibility(View.VISIBLE);
			rgQI1003EN.requestFocus();
		}
    }
    
    public void onrgQI1003FChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQI1003F.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1003FN);
			lblQI1003F2.setVisibility(View.GONE);
			rgQI1003FN.setVisibility(View.GONE);
			rgQI1004A.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1003FN);
			lblQI1003F2.setVisibility(View.VISIBLE);
			rgQI1003FN.setVisibility(View.VISIBLE);
			rgQI1003FN.requestFocus();
		}
    }
    
   
    
    
    public void onrgQI1004AChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004A.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004AN);
			lblQI1004A2.setVisibility(View.GONE);
			rgQI1004AN.setVisibility(View.GONE);
			rgQI1004B.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1004AN);
			lblQI1004A2.setVisibility(View.VISIBLE);
			rgQI1004AN.setVisibility(View.VISIBLE);
			rgQI1004AN.requestFocus();
		}
    }
    
    public void onrgQI1004BChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004B.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004BN);
			lblQI1004B2.setVisibility(View.GONE);
			rgQI1004BN.setVisibility(View.GONE);
			rgQI1004C.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1004BN);
			lblQI1004B2.setVisibility(View.VISIBLE);
			rgQI1004BN.setVisibility(View.VISIBLE);
			rgQI1004BN.requestFocus();
		}
    }
    
    public void onrgQI1004CChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004C.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004CN);
			lblQI1004C2.setVisibility(View.GONE);
			rgQI1004CN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004CN);
			lblQI1004C2.setVisibility(View.VISIBLE);
			rgQI1004CN.setVisibility(View.VISIBLE);
			rgQI1004CN.requestFocus();
		}
    }
    
    public void onrgQI1004DChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004D.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004DN);
			lblQI1004D2.setVisibility(View.GONE);
			rgQI1004DN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004DN);
			lblQI1004D2.setVisibility(View.VISIBLE);
			rgQI1004DN.setVisibility(View.VISIBLE);
			rgQI1004DN.requestFocus();
		}
    }
    
    public void onrgQI1004EChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004E.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004EN);
			lblQI1004E2.setVisibility(View.GONE);
			rgQI1004EN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004EN);
			lblQI1004E2.setVisibility(View.VISIBLE);
			rgQI1004EN.setVisibility(View.VISIBLE);
			rgQI1004EN.requestFocus();
		}
    }
    
    public void onrgQI1004FChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004F.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004FN);
			lblQI1004F2.setVisibility(View.GONE);
			rgQI1004FN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004FN);
			lblQI1004F2.setVisibility(View.VISIBLE);
			rgQI1004FN.setVisibility(View.VISIBLE);
			rgQI1004FN.requestFocus();
		}
    }
    
    public void onrgQI1004GChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004G.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004GN);
			lblQI1004G2.setVisibility(View.GONE);
			rgQI1004GN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004GN);
			lblQI1004G2.setVisibility(View.VISIBLE);
			rgQI1004GN.setVisibility(View.VISIBLE);
			rgQI1004GN.requestFocus();
		}
    }
    
    public void onrgQI1004HChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1004H.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1004HN);
			lblQI1004H2.setVisibility(View.GONE);
			rgQI1004HN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1004HN);
			lblQI1004H2.setVisibility(View.VISIBLE);
			rgQI1004HN.setVisibility(View.VISIBLE);
			rgQI1004HN.requestFocus();
		}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1003A.readOnly();
    		rgQI1003B.readOnly();
    		rgQI1003C.readOnly();
    		rgQI1003D.readOnly();
    		rgQI1003E.readOnly();
    		rgQI1003F.readOnly();
    		rgQI1004A.readOnly();
    		rgQI1004AN.readOnly();
    		rgQI1004B.readOnly();
    		rgQI1004BN.readOnly();
    		rgQI1004C.readOnly();
    		rgQI1004CN.readOnly();
    		rgQI1004D.readOnly();
    		rgQI1004DN.readOnly();
    		rgQI1004E.readOnly();
    		rgQI1004EN.readOnly();
    		rgQI1004F.readOnly();
    		rgQI1004FN.readOnly();
    		rgQI1004G.readOnly();
    		rgQI1004GN.readOnly();
    		rgQI1004H.readOnly();
    		rgQI1004HN.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }

	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}