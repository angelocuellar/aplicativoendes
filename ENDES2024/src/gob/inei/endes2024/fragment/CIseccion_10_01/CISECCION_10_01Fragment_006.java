package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_006 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1013A;
	
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1013AA;
	
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1013B;
	
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI1014_A;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1014_AN;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI1014_B;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1014_BN;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI1014_C;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1014_CN;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI1014_D;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI1014_DN;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI1014_E;
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI1014_EN;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI1014_F;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI1014_FN;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI1014_G;
	@FieldAnnotation(orderIndex=17)
	public RadioGroupOtherField rgQI1014_GN;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI1014_H;
	@FieldAnnotation(orderIndex=19)
	public RadioGroupOtherField rgQI1014_HN;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI1014_I;
	@FieldAnnotation(orderIndex=21)
	public RadioGroupOtherField rgQI1014_IN;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI1014_J;
	@FieldAnnotation(orderIndex=23)
	public RadioGroupOtherField rgQI1014_JN;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI1014_K;
	@FieldAnnotation(orderIndex=25)
	public RadioGroupOtherField rgQI1014_KN;
	@FieldAnnotation(orderIndex=26)
	public CheckBoxField chbQI1014_L;
	@FieldAnnotation(orderIndex=27)
	public RadioGroupOtherField rgQI1014_LN;
	@FieldAnnotation(orderIndex=28)
	public CheckBoxField chbQI1014_M;
	@FieldAnnotation(orderIndex=29)
	public RadioGroupOtherField rgQI1014_MN;
	@FieldAnnotation(orderIndex=30)
	public CheckBoxField chbQI1014_N;
	@FieldAnnotation(orderIndex=31)
	public RadioGroupOtherField rgQI1014_NN;
	@FieldAnnotation(orderIndex=32)
	public CheckBoxField chbQI1014_O;
	@FieldAnnotation(orderIndex=33)
	public RadioGroupOtherField rgQI1014_ON;
	@FieldAnnotation(orderIndex=34)
	public CheckBoxField chbQI1014_P;
	@FieldAnnotation(orderIndex=35)
	public RadioGroupOtherField rgQI1014_PN;
	@FieldAnnotation(orderIndex=36)
	public CheckBoxField chbQI1014_Q;
	@FieldAnnotation(orderIndex=37)
	public RadioGroupOtherField rgQI1014_QN;
	@FieldAnnotation(orderIndex=38)
	public CheckBoxField chbQI1014_R;
	@FieldAnnotation(orderIndex=39)
	public RadioGroupOtherField rgQI1014_RN;
	@FieldAnnotation(orderIndex=40)
	public CheckBoxField chbQI1014_S;
	@FieldAnnotation(orderIndex=41)
	public RadioGroupOtherField rgQI1014_SN;
	@FieldAnnotation(orderIndex=42)
	public CheckBoxField chbQI1014_T;
	@FieldAnnotation(orderIndex=43)
	public RadioGroupOtherField rgQI1014_TN;
	
	@FieldAnnotation(orderIndex=44)
	public CheckBoxField chbQI1014_U;
	@FieldAnnotation(orderIndex=45)
	public RadioGroupOtherField rgQI1014_UN;
	@FieldAnnotation(orderIndex=46)
	public CheckBoxField chbQI1014_V;
	@FieldAnnotation(orderIndex=47)
	public RadioGroupOtherField rgQI1014_VN;
	
	@FieldAnnotation(orderIndex=48)
	public CheckBoxField chbQI1014_X;	
	@FieldAnnotation(orderIndex=49)
	public TextField txtQI1014XI;
	@FieldAnnotation(orderIndex=50)
	public RadioGroupOtherField rgQI1014_XN;
	
	CISECCION_10_01 individual;
	CISECCION_05_07 individualS5;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1013_A,lblpregunta1013_AA,lblpregunta1013_B,lblpregunta1014
	,lblQI1014_A2,lblQI1014_B2,lblQI1014_C2,lblQI1014_D2,lblQI1014_E2,lblQI1014_F2,lblQI1014_G2,lblQI1014_H2,lblQI1014_I2,lblQI1014_J2,lblQI1014_K2,lblQI1014_L2,lblQI1014_M2,lblQI1014_N2,lblQI1014_O2
	,lblQI1014_P2,lblQI1014_Q2,lblQI1014_R2,lblQI1014_S2,lblQI1014_T2,lblQI1014_X2,lblQI1014_U2,lblQI1014_V2;
	public GridComponent2 gridPreguntas1012B,gridPreguntas1014;
	public TextField txtCabecera;
	LinearLayout q0,q1,q2,q3, q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5;

	public CISECCION_10_01Fragment_006() {}
	public CISECCION_10_01Fragment_006 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override 
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1001A","QI1013A","QI1013AA","QI1013B","QI1014_A","QI1014_AN","QI1014_B","QI1014_BN","QI1014_C","QI1014_CN","QI1014_D","QI1014_DN","QI1014_E","QI1014_EN","QI1014_F","QI1014_FN","QI1014_G","QI1014_GN","QI1014_H","QI1014_HN","QI1014_I","QI1014_IN","QI1014_J","QI1014_JN","QI1014_K","QI1014_KN","QI1014_L","QI1014_LN","QI1014_M","QI1014_MN","QI1014_N","QI1014_NN","QI1014_O","QI1014_ON","QI1014_P","QI1014_PN","QI1014_Q","QI1014_QN","QI1014_R","QI1014_RN","QI1014_S","QI1014_SN","QI1014_T","QI1014_TN","QI1014_X","QI1014XI","QI1014_XN","QI1000A","ID","HOGAR_ID","PERSONA_ID","QI1014_U","QI1014_UN","QI1014_V","QI1014_VN")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1013A","QI1013AA","QI1013B","QI1014_A","QI1014_AN","QI1014_B","QI1014_BN","QI1014_C","QI1014_CN","QI1014_D","QI1014_DN","QI1014_E","QI1014_EN","QI1014_F","QI1014_FN","QI1014_G","QI1014_GN","QI1014_H","QI1014_HN","QI1014_I","QI1014_IN","QI1014_J","QI1014_JN","QI1014_K","QI1014_KN","QI1014_L","QI1014_LN","QI1014_M","QI1014_MN","QI1014_N","QI1014_NN","QI1014_O","QI1014_ON","QI1014_P","QI1014_PN","QI1014_Q","QI1014_QN","QI1014_R","QI1014_RN","QI1014_S","QI1014_SN","QI1014_T","QI1014_TN","QI1014_X","QI1014XI","QI1014_XN","QI1014_U","QI1014_UN","QI1014_V","QI1014_VN")};
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI501","QI502","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
	}
    
    @Override 
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1013_A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1013_a);
		
		lblpregunta1013_AA = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1013_aa);
		
		lblpregunta1013_B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1013_b);
		lblpregunta1014 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1014);
		
		rgQI1013A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1013_1,R.string.ciseccion_10_01qi1013_2,R.string.ciseccion_10_01qi1013_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1013ChangeValue");
		
		rgQI1013AA=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1013_1,R.string.ciseccion_10_01qi1013_2,R.string.ciseccion_10_01qi1013_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1013AAChangeValue");
		rgQI1013B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1013_1,R.string.ciseccion_10_01qi1013_2,R.string.ciseccion_10_01qi1013_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1013BChangeValue");
		
		chbQI1014_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_a, "1:0").size(altoComponente, 770).callback("onchbQI1014_AChangeValue");
		chbQI1014_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_b, "1:0").size(altoComponente, 770).callback("onchbQI1014_BChangeValue");
		chbQI1014_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_c, "1:0").size(altoComponente, 770).callback("onchbQI1014_CChangeValue");
		chbQI1014_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_d, "1:0").size(altoComponente, 770).callback("onchbQI1014_DChangeValue");
		chbQI1014_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_e, "1:0").size(altoComponente, 770).callback("onchbQI1014_EChangeValue");
		chbQI1014_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_f, "1:0").size(altoComponente, 770).callback("onchbQI1014_FChangeValue");
		chbQI1014_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_g, "1:0").size(altoComponente, 770).callback("onchbQI1014_GChangeValue");
		chbQI1014_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_h, "1:0").size(altoComponente, 770).callback("onchbQI1014_HChangeValue");
		chbQI1014_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_i, "1:0").size(altoComponente, 770).callback("onchbQI1014_IChangeValue");
		chbQI1014_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_j, "1:0").size(altoComponente, 770).callback("onchbQI1014_JChangeValue");
		chbQI1014_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_k, "1:0").size(altoComponente, 770).callback("onchbQI1014_KChangeValue");
		chbQI1014_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_l, "1:0").size(altoComponente, 770).callback("onchbQI1014_LChangeValue");
		chbQI1014_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_m, "1:0").size(altoComponente, 770).callback("onchbQI1014_MChangeValue");
		chbQI1014_N=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_n, "1:0").size(altoComponente, 770).callback("onchbQI1014_NChangeValue");
		chbQI1014_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_o, "1:0").size(altoComponente, 770).callback("onchbQI1014_OChangeValue");
		chbQI1014_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_p, "1:0").size(altoComponente, 770).callback("onchbQI1014_PChangeValue");
		chbQI1014_Q=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_q, "1:0").size(altoComponente, 770).callback("onchbQI1014_QChangeValue");
		chbQI1014_R=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_r, "1:0").size(altoComponente, 770).callback("onchbQI1014_RChangeValue");
		chbQI1014_S=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_s, "1:0").size(altoComponente, 770).callback("onchbQI1014_SChangeValue");
		chbQI1014_T=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_t, "1:0").size(altoComponente, 770).callback("onchbQI1014_TChangeValue");
		
		chbQI1014_U=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_u, "1:0").size(altoComponente, 770).callback("onchbQI1014_UChangeValue");
		chbQI1014_V=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_v, "1:0").size(altoComponente, 770).callback("onchbQI1014_VChangeValue");
		
		chbQI1014_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1014_x, "1:0").size(altoComponente, 470).callback("onchbQI1014_XChangeValue");
		txtQI1014XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 300);
		
		lblQI1014_A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_E2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_F2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_G2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_H2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_I2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_J2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_K2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_L2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_M2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_N2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_O2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_P2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_Q2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_R2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_S2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_T2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		
		lblQI1014_U2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		lblQI1014_V2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);
		
		lblQI1014_X2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1014_sp);

		
		rgQI1014_AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_EN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_FN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_GN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_HN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_IN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_JN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_KN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_LN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_MN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_NN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_ON=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_PN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_QN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_RN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_SN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_TN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		rgQI1014_UN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1014_VN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		rgQI1014_XN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1014_so1,R.string.ciseccion_10_01qi1014_so2,R.string.ciseccion_10_01qi1014_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		
		
		gridPreguntas1014=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1014.addComponent(chbQI1014_A,2);
		gridPreguntas1014.addComponent(lblQI1014_A2);
		gridPreguntas1014.addComponent(rgQI1014_AN);
				
		gridPreguntas1014.addComponent(chbQI1014_B,2);
		gridPreguntas1014.addComponent(lblQI1014_B2);
		gridPreguntas1014.addComponent(rgQI1014_BN);
		
		gridPreguntas1014.addComponent(chbQI1014_C,2);
		gridPreguntas1014.addComponent(lblQI1014_C2);
		gridPreguntas1014.addComponent(rgQI1014_CN);
		
		gridPreguntas1014.addComponent(chbQI1014_D,2);
		gridPreguntas1014.addComponent(lblQI1014_D2);
		gridPreguntas1014.addComponent(rgQI1014_DN);
		
		gridPreguntas1014.addComponent(chbQI1014_E,2);
		gridPreguntas1014.addComponent(lblQI1014_E2);
		gridPreguntas1014.addComponent(rgQI1014_EN);
		
		gridPreguntas1014.addComponent(chbQI1014_F,2);
		gridPreguntas1014.addComponent(lblQI1014_F2);
		gridPreguntas1014.addComponent(rgQI1014_FN);
		
		gridPreguntas1014.addComponent(chbQI1014_G,2);
		gridPreguntas1014.addComponent(lblQI1014_G2);
		gridPreguntas1014.addComponent(rgQI1014_GN);
		
		gridPreguntas1014.addComponent(chbQI1014_H,2);
		gridPreguntas1014.addComponent(lblQI1014_H2);
		gridPreguntas1014.addComponent(rgQI1014_HN);
		
		gridPreguntas1014.addComponent(chbQI1014_I,2);
		gridPreguntas1014.addComponent(lblQI1014_I2);
		gridPreguntas1014.addComponent(rgQI1014_IN);
		
		gridPreguntas1014.addComponent(chbQI1014_J,2);
		gridPreguntas1014.addComponent(lblQI1014_J2);
		gridPreguntas1014.addComponent(rgQI1014_JN);
		
		gridPreguntas1014.addComponent(chbQI1014_K,2);
		gridPreguntas1014.addComponent(lblQI1014_K2);
		gridPreguntas1014.addComponent(rgQI1014_KN);
		
		gridPreguntas1014.addComponent(chbQI1014_L,2);
		gridPreguntas1014.addComponent(lblQI1014_L2);
		gridPreguntas1014.addComponent(rgQI1014_LN);
		
		gridPreguntas1014.addComponent(chbQI1014_M,2);
		gridPreguntas1014.addComponent(lblQI1014_M2);
		gridPreguntas1014.addComponent(rgQI1014_MN);
		
		gridPreguntas1014.addComponent(chbQI1014_N,2);
		gridPreguntas1014.addComponent(lblQI1014_N2);
		gridPreguntas1014.addComponent(rgQI1014_NN);
		
		gridPreguntas1014.addComponent(chbQI1014_O,2);
		gridPreguntas1014.addComponent(lblQI1014_O2);
		gridPreguntas1014.addComponent(rgQI1014_ON);
		
		gridPreguntas1014.addComponent(chbQI1014_P,2);
		gridPreguntas1014.addComponent(lblQI1014_P2);
		gridPreguntas1014.addComponent(rgQI1014_PN);
		
		gridPreguntas1014.addComponent(chbQI1014_Q,2);
		gridPreguntas1014.addComponent(lblQI1014_Q2);
		gridPreguntas1014.addComponent(rgQI1014_QN);
		
		gridPreguntas1014.addComponent(chbQI1014_R,2);
		gridPreguntas1014.addComponent(lblQI1014_R2);
		gridPreguntas1014.addComponent(rgQI1014_RN);
		
		gridPreguntas1014.addComponent(chbQI1014_S,2);
		gridPreguntas1014.addComponent(lblQI1014_S2);
		gridPreguntas1014.addComponent(rgQI1014_SN);
		
		gridPreguntas1014.addComponent(chbQI1014_T,2);
		gridPreguntas1014.addComponent(lblQI1014_T2);
		gridPreguntas1014.addComponent(rgQI1014_TN);
		
		gridPreguntas1014.addComponent(chbQI1014_U,2);
		gridPreguntas1014.addComponent(lblQI1014_U2);
		gridPreguntas1014.addComponent(rgQI1014_UN);
		
		gridPreguntas1014.addComponent(chbQI1014_V,2);
		gridPreguntas1014.addComponent(lblQI1014_V2);
		gridPreguntas1014.addComponent(rgQI1014_VN);
		
		gridPreguntas1014.addComponent(chbQI1014_X);
		gridPreguntas1014.addComponent(txtQI1014XI);
		gridPreguntas1014.addComponent(lblQI1014_X2);
		gridPreguntas1014.addComponent(rgQI1014_XN);
		
    }
    
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		//q1 = createQuestionSection(lblpregunta1013_A, lblpregunta1013_AA,lblpregunta1013_B, rgQI1013A);//rgQI1013
		q1 = createQuestionSection(lblpregunta1013_A, rgQI1013A);//rgQI1013
//		LinearLayout ly1014 = new LinearLayout(getActivity());
//		ly1014.addView(chbQI1014_X);
//		ly1014.addView(txtQI1014XI);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1014,gridPreguntas1014.component());
		
		q3 = createQuestionSection( lblpregunta1013_AA, rgQI1013AA);
		q4 = createQuestionSection(lblpregunta1013_B, rgQI1013B);		
//		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1014,chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,ly1014);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		
		form.addView(q3);
		form.addView(q4);
		
		form.addView(q2);
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi1013a!=null)
			individual.qi1013a=individual.getConvertQi1013(individual.qi1013a);
		
		if (individual.qi1013aa!=null)
			individual.qi1013aa=individual.getConvertQi1013(individual.qi1013aa);
		
		if (individual.qi1013b!=null)
			individual.qi1013b=individual.getConvertQi1013(individual.qi1013b);
		
		int cont=0;		
		if(individual.qi1014_a==1)
			cont+=1;
		if(individual.qi1014_b==1)
			cont+=1;
		if(individual.qi1014_c==1)
			cont+=1;
		if(individual.qi1014_d==1)
			cont+=1;
		if(individual.qi1014_e==1)
			cont+=1;
		if(individual.qi1014_f==1)
			cont+=1;
		if(individual.qi1014_g==1)
			cont+=1;
		if(individual.qi1014_h==1)
			cont+=1;
		if(individual.qi1014_i==1)
			cont+=1;
		if(individual.qi1014_j==1)
			cont+=1;
		if(individual.qi1014_k==1)
			cont+=1;
		if(individual.qi1014_l==1)
			cont+=1;
		if(individual.qi1014_m==1)
			cont+=1;
		if(individual.qi1014_n==1)
			cont+=1;
		if(individual.qi1014_o==1)
			cont+=1;
		if(individual.qi1014_p==1)
			cont+=1;
		if(individual.qi1014_q==1)
			cont+=1;
		if(individual.qi1014_r==1)
			cont+=1;
		if(individual.qi1014_s==1)
			cont+=1;
		if(individual.qi1014_t==1)
			cont+=1;
		
		if(individual.qi1014_u==1)
			cont+=1;
		if(individual.qi1014_v==1)
			cont+=1;
		
		if(individual.qi1014_x==1)
			cont+=1;
		
		App.getInstance().getCiseccion_10_01().filtro1014nico = ContadorPregunta1014();
		
		App.getInstance().getCiseccion_10_01().filtro1014 = cont;
		App.getInstance().getCiseccion_10_01().qi1014_a=individual.qi1014_a;
		App.getInstance().getCiseccion_10_01().qi1014_b=individual.qi1014_b;
		App.getInstance().getCiseccion_10_01().qi1014_c=individual.qi1014_c;
		App.getInstance().getCiseccion_10_01().qi1014_d=individual.qi1014_d;
		App.getInstance().getCiseccion_10_01().qi1014_e=individual.qi1014_e;
		App.getInstance().getCiseccion_10_01().qi1014_f=individual.qi1014_f;
		App.getInstance().getCiseccion_10_01().qi1014_g=individual.qi1014_g;
		App.getInstance().getCiseccion_10_01().qi1014_h=individual.qi1014_h;
		App.getInstance().getCiseccion_10_01().qi1014_i=individual.qi1014_i;
		App.getInstance().getCiseccion_10_01().qi1014_j=individual.qi1014_j;
		App.getInstance().getCiseccion_10_01().qi1014_k=individual.qi1014_k;
		App.getInstance().getCiseccion_10_01().qi1014_l=individual.qi1014_l;
		App.getInstance().getCiseccion_10_01().qi1014_m=individual.qi1014_m;
		App.getInstance().getCiseccion_10_01().qi1014_n=individual.qi1014_n;
		App.getInstance().getCiseccion_10_01().qi1014_o=individual.qi1014_o;
		App.getInstance().getCiseccion_10_01().qi1014_p=individual.qi1014_p;
		App.getInstance().getCiseccion_10_01().qi1014_k=individual.qi1014_q;
		App.getInstance().getCiseccion_10_01().qi1014_r=individual.qi1014_r;
		App.getInstance().getCiseccion_10_01().qi1014_s=individual.qi1014_s;
		App.getInstance().getCiseccion_10_01().qi1014_t=individual.qi1014_t;
		
		App.getInstance().getCiseccion_10_01().qi1014_u=individual.qi1014_u;
		App.getInstance().getCiseccion_10_01().qi1014_v=individual.qi1014_v;
		
		App.getInstance().getCiseccion_10_01().qi1014_x=individual.qi1014_x;
		
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		App.getInstance().getCiseccion_10_01().qi1013a=individual.qi1013a;
		
		App.getInstance().getCiseccion_10_01().qi1013aa=individual.qi1013aa;
		App.getInstance().getCiseccion_10_01().qi1013b=individual.qi1013b;
		
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000A=individual.qi1000a==null?0:individual.qi1000a;
		
		Integer in1013= App.getInstance().getCiseccion_10_01().qi1013a==null?0:App.getInstance().getCiseccion_10_01().qi1013a;
		Integer in1013AA= App.getInstance().getCiseccion_10_01().qi1013aa==null?0:App.getInstance().getCiseccion_10_01().qi1013aa;
		Integer in1013B= App.getInstance().getCiseccion_10_01().qi1013b==null?0:App.getInstance().getCiseccion_10_01().qi1013b;
		
		Integer in501=individualS5.qi501==null?0:individualS5.qi501;
    	Integer in502=individualS5.qi502==null?0:individualS5.qi502;
    	
    	Integer in1001A=individual.qi1001a==null?0:individual.qi1001a;
		
		if (in1000A!=2) {
			
			
			if( in1013 != 0){
				if (Util.esVacio(individual.qi1013b) && in1013B != 0) {//
					mensaje = preguntaVacia.replace("$", "La pregunta QI1013");
					view = rgQI1013A;
					error = true;
					return false;
				}
			}
			
			if( in1013AA != 0){
				if (Util.esVacio(individual.qi1013aa) && in1013AA != 0) {//
					mensaje = preguntaVacia.replace("$", "La pregunta QI1013AA");
					view = rgQI1013AA;
					error = true;
					return false;
				}
			}
			
			if( in1013B != 0){
				if (Util.esVacio(individual.qi1013b) && in1013B != 0) {//
					mensaje = preguntaVacia.replace("$", "La pregunta QI1013B");
					view = rgQI1013B;
					error = true;
					return false;
				}
			}
			
			//////////////////////////////////////////////////////////////////////////////////////
			
			if( in1013 != 0){
				if (individual.qi1013a!=2 && individual.qi1013a!=8 ) {
					if (!verificarCheck()) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1014_A");
						view = chbQI1014_A;
						error = true;
						return false;
					}
					
					if (chbQI1014_A.isChecked()) {
						if (Util.esVacio(individual.qi1014_an)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_AN");
							view = rgQI1014_AN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_B.isChecked()) {
						if (Util.esVacio(individual.qi1014_bn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_BN");
							view = rgQI1014_BN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_C.isChecked()) {
						if (Util.esVacio(individual.qi1014_cn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_CN");
							view = rgQI1014_CN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_D.isChecked()) {
						if (Util.esVacio(individual.qi1014_dn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_DN");
							view = rgQI1014_DN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_E.isChecked()) {
						if (Util.esVacio(individual.qi1014_en)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_EN");
							view = rgQI1014_EN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_F.isChecked()) {
						if (Util.esVacio(individual.qi1014_fn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_FN");
							view = rgQI1014_FN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_G.isChecked()) {
						if (Util.esVacio(individual.qi1014_gn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_GN");
							view = rgQI1014_GN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_H.isChecked()) {
						if (Util.esVacio(individual.qi1014_hn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_HN");
							view = rgQI1014_HN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_I.isChecked()) {
						if (Util.esVacio(individual.qi1014_in)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_IN");
							view = rgQI1014_IN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_J.isChecked()) {
						if (Util.esVacio(individual.qi1014_jn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_JN");
							view = rgQI1014_JN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_K.isChecked()) {
						if (Util.esVacio(individual.qi1014_kn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_KN");
							view = rgQI1014_KN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_L.isChecked()) {
						if (Util.esVacio(individual.qi1014_ln)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_LN");
							view = rgQI1014_LN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_M.isChecked()) {
						if (Util.esVacio(individual.qi1014_mn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_MN");
							view = rgQI1014_MN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_N.isChecked()) {
						if (Util.esVacio(individual.qi1014_nn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_NN");
							view = rgQI1014_NN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_O.isChecked()) {
						if (Util.esVacio(individual.qi1014_on)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_ON");
							view = rgQI1014_ON;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_P.isChecked()) {
						if (Util.esVacio(individual.qi1014_pn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_PN");
							view = rgQI1014_PN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_Q.isChecked()) {
						if (Util.esVacio(individual.qi1014_qn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_QN");
							view = rgQI1014_QN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_R.isChecked()) {
						if (Util.esVacio(individual.qi1014_rn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_RN");
							view = rgQI1014_RN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_S.isChecked()) {
						if (Util.esVacio(individual.qi1014_sn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_SN");
							view = rgQI1014_SN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_T.isChecked()) {
						if (Util.esVacio(individual.qi1014_tn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_TN");
							view = rgQI1014_TN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_U.isChecked()) {
						if (Util.esVacio(individual.qi1014_un)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_UN");
							view = rgQI1014_UN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_V.isChecked()) {
						if (Util.esVacio(individual.qi1014_vn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_VN");
							view = rgQI1014_VN;
							error = true;
							return false;
						}
					}
					
					
									
					if (chbQI1014_X.isChecked()) {
						if (Util.esVacio(individual.qi1014xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014XI");
							view = txtQI1014XI;
							error = true;
							return false;
						}
						if (Util.esVacio(individual.qi1014_xn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_XN");
							view = rgQI1014_XN;
							error = true;
							return false;
						}
					}
				}
			}
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			if( in1013AA != 0){
				if (individual.qi1013aa!=2 && individual.qi1013aa!=8) {
					if (!verificarCheck()) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1014_A");
						view = chbQI1014_A;
						error = true;
						return false;
					}
					
					if (chbQI1014_A.isChecked()) {
						if (Util.esVacio(individual.qi1014_an)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_AN");
							view = rgQI1014_AN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_B.isChecked()) {
						if (Util.esVacio(individual.qi1014_bn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_BN");
							view = rgQI1014_BN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_C.isChecked()) {
						if (Util.esVacio(individual.qi1014_cn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_CN");
							view = rgQI1014_CN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_D.isChecked()) {
						if (Util.esVacio(individual.qi1014_dn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_DN");
							view = rgQI1014_DN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_E.isChecked()) {
						if (Util.esVacio(individual.qi1014_en)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_EN");
							view = rgQI1014_EN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_F.isChecked()) {
						if (Util.esVacio(individual.qi1014_fn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_FN");
							view = rgQI1014_FN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_G.isChecked()) {
						if (Util.esVacio(individual.qi1014_gn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_GN");
							view = rgQI1014_GN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_H.isChecked()) {
						if (Util.esVacio(individual.qi1014_hn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_HN");
							view = rgQI1014_HN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_I.isChecked()) {
						if (Util.esVacio(individual.qi1014_in)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_IN");
							view = rgQI1014_IN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_J.isChecked()) {
						if (Util.esVacio(individual.qi1014_jn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_JN");
							view = rgQI1014_JN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_K.isChecked()) {
						if (Util.esVacio(individual.qi1014_kn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_KN");
							view = rgQI1014_KN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_L.isChecked()) {
						if (Util.esVacio(individual.qi1014_ln)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_LN");
							view = rgQI1014_LN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_M.isChecked()) {
						if (Util.esVacio(individual.qi1014_mn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_MN");
							view = rgQI1014_MN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_N.isChecked()) {
						if (Util.esVacio(individual.qi1014_nn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_NN");
							view = rgQI1014_NN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_O.isChecked()) {
						if (Util.esVacio(individual.qi1014_on)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_ON");
							view = rgQI1014_ON;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_P.isChecked()) {
						if (Util.esVacio(individual.qi1014_pn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_PN");
							view = rgQI1014_PN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_Q.isChecked()) {
						if (Util.esVacio(individual.qi1014_qn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_QN");
							view = rgQI1014_QN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_R.isChecked()) {
						if (Util.esVacio(individual.qi1014_rn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_RN");
							view = rgQI1014_RN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_S.isChecked()) {
						if (Util.esVacio(individual.qi1014_sn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_SN");
							view = rgQI1014_SN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_T.isChecked()) {
						if (Util.esVacio(individual.qi1014_tn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_TN");
							view = rgQI1014_TN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_U.isChecked()) {
						if (Util.esVacio(individual.qi1014_un)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_UN");
							view = rgQI1014_UN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_V.isChecked()) {
						if (Util.esVacio(individual.qi1014_vn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_VN");
							view = rgQI1014_VN;
							error = true;
							return false;
						}
					}
					
					
									
					if (chbQI1014_X.isChecked()) {
						if (Util.esVacio(individual.qi1014xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014XI");
							view = txtQI1014XI;
							error = true;
							return false;
						}
						if (Util.esVacio(individual.qi1014_xn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_XN");
							view = rgQI1014_XN;
							error = true;
							return false;
						}
					}
				}
			}
			
			////////////////////////////////////////////////////////////////////////////////
			
			if( in1013B != 0){
				if (individual.qi1013b!=2 && individual.qi1013b!=8) {
					if (!verificarCheck()) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1014_A");
						view = chbQI1014_A;
						error = true;
						return false;
					}
					
					if (chbQI1014_A.isChecked()) {
						if (Util.esVacio(individual.qi1014_an)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_AN");
							view = rgQI1014_AN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_B.isChecked()) {
						if (Util.esVacio(individual.qi1014_bn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_BN");
							view = rgQI1014_BN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_C.isChecked()) {
						if (Util.esVacio(individual.qi1014_cn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_CN");
							view = rgQI1014_CN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_D.isChecked()) {
						if (Util.esVacio(individual.qi1014_dn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_DN");
							view = rgQI1014_DN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_E.isChecked()) {
						if (Util.esVacio(individual.qi1014_en)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_EN");
							view = rgQI1014_EN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_F.isChecked()) {
						if (Util.esVacio(individual.qi1014_fn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_FN");
							view = rgQI1014_FN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_G.isChecked()) {
						if (Util.esVacio(individual.qi1014_gn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_GN");
							view = rgQI1014_GN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_H.isChecked()) {
						if (Util.esVacio(individual.qi1014_hn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_HN");
							view = rgQI1014_HN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_I.isChecked()) {
						if (Util.esVacio(individual.qi1014_in)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_IN");
							view = rgQI1014_IN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_J.isChecked()) {
						if (Util.esVacio(individual.qi1014_jn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_JN");
							view = rgQI1014_JN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_K.isChecked()) {
						if (Util.esVacio(individual.qi1014_kn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_KN");
							view = rgQI1014_KN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_L.isChecked()) {
						if (Util.esVacio(individual.qi1014_ln)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_LN");
							view = rgQI1014_LN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_M.isChecked()) {
						if (Util.esVacio(individual.qi1014_mn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_MN");
							view = rgQI1014_MN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_N.isChecked()) {
						if (Util.esVacio(individual.qi1014_nn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_NN");
							view = rgQI1014_NN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_O.isChecked()) {
						if (Util.esVacio(individual.qi1014_on)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_ON");
							view = rgQI1014_ON;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_P.isChecked()) {
						if (Util.esVacio(individual.qi1014_pn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_PN");
							view = rgQI1014_PN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_Q.isChecked()) {
						if (Util.esVacio(individual.qi1014_qn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_QN");
							view = rgQI1014_QN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_R.isChecked()) {
						if (Util.esVacio(individual.qi1014_rn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_RN");
							view = rgQI1014_RN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_S.isChecked()) {
						if (Util.esVacio(individual.qi1014_sn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_SN");
							view = rgQI1014_SN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_T.isChecked()) {
						if (Util.esVacio(individual.qi1014_tn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_TN");
							view = rgQI1014_TN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_U.isChecked()) {
						if (Util.esVacio(individual.qi1014_un)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_UN");
							view = rgQI1014_UN;
							error = true;
							return false;
						}
					}
					
					if (chbQI1014_V.isChecked()) {
						if (Util.esVacio(individual.qi1014_vn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_VN");
							view = rgQI1014_VN;
							error = true;
							return false;
						}
					}
					
					
									
					if (chbQI1014_X.isChecked()) {
						if (Util.esVacio(individual.qi1014xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014XI");
							view = txtQI1014XI;
							error = true;
							return false;
						}
						if (Util.esVacio(individual.qi1014_xn)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1014_XN");
							view = rgQI1014_XN;
							error = true;
							return false;
						}
					}
				}
			}
			
			////////////////////////////////////////////
		}
		
		
			
			
	    	
	    	
    	if (in501==3 && in502==3) {

			if(in1001A == 1){
			
			
				if (Util.esVacio(individual.qi1013aa)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1013AA");
					view = rgQI1013AA;
					error = true;
					return false;
				}
				
			}else if(in1001A == 2){
				
				if (Util.esVacio(individual.qi1013b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1013B");
					view = rgQI1013B;
					error = true;
					return false;
				}
				
				
			}
		}
		else{
			
			if (Util.esVacio(individual.qi1013a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1013A");
				view = rgQI1013A;
				error = true;
				return false;
			}
	
			
		}
		
		return true;
    }
    
    @Override 
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	App.getInstance().getCiseccion05_07().qi501=individualS5.qi501;
    	App.getInstance().getCiseccion05_07().qi502=individualS5.qi502;
    	
    	if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
    	
		if(individual.qi1013a!=null)
			individual.qi1013a=individual.setConvertQi1013(individual.qi1013a);
		
		if(individual.qi1013aa!=null)
			individual.qi1013aa=individual.setConvertQi1013(individual.qi1013aa);
		
		if(individual.qi1013b!=null)
			individual.qi1013b=individual.setConvertQi1013(individual.qi1013b);
		
		
		entityToUI(individual);
		inicio();
    }
    
    public void ValidarPregunta501() {
    	Integer in501=individualS5.qi501==null?0:individualS5.qi501;
    	Integer in502=individualS5.qi502==null?0:individualS5.qi502;
    	
    	Integer in1001A=individual.qi1001a==null?0:individual.qi1001a;
		
    	
    	
    	if (in501==3 && in502==3) {
			
    		//ToastMessage.msgBox(this.getActivity(), in1001A.toString()+" "+in501.toString()+" "+in502.toString() , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    		
    		individual.qi1013a= null;
    		
			Util.cleanAndLockView(getActivity(),rgQI1013A );
			q1.setVisibility(View.GONE);
			
			/*Util.lockView(getActivity(), false,rgQI1013B);
			q4.setVisibility(View.VISIBLE);*/
			
			if(in1001A == 1){
				individual.qi1013b= null;
				
				Util.lockView(getActivity(), false,rgQI1013AA);
				q3.setVisibility(View.VISIBLE);
				
				Util.cleanAndLockView(getActivity(),rgQI1013B );
				q4.setVisibility(View.GONE);
				
			
				
			}else if(in1001A == 2){
				individual.qi1013aa= null;
				
				Util.cleanAndLockView(getActivity(),rgQI1013AA );
				q3.setVisibility(View.GONE);
				
				Util.lockView(getActivity(), false,rgQI1013B);
				q4.setVisibility(View.VISIBLE);
				
				
			}
		}
		else{
			individual.qi1013b= null;
			individual.qi1013aa= null;
			
			Util.cleanAndLockView(getActivity(),rgQI1013B,rgQI1013AA);
			q4.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			
			Util.lockView(getActivity(), false,rgQI1013A);
			q1.setVisibility(View.VISIBLE);
			
		}
    	
    	/*String in1013=individual.qi1013==null?"cero":individual.qi1013.toString();
    	String in1013aa=individual.qi1013aa==null?"cero":individual.qi1013aa.toString();
    	String in1013b=individual.qi1013b==null?"cero":individual.qi1013b.toString();
    	ToastMessage.msgBox(this.getActivity(), in1013+"-"+in1013aa+"-"+in1013b , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		*/
	}
    
    private void inicio() {
    	ValidarPregunta501();
    	onrgQI1013ChangeValue();
    	onrgQI1013AAChangeValue();
    	onrgQI1013BChangeValue();
    	ValidarsiesSupervisora();
    	
    	Integer in1013=individual.qi1013a==null?0:individual.qi1013a;
    	Integer in1013aa=individual.qi1013aa==null?0:individual.qi1013aa;
    	Integer in1013b=individual.qi1013b==null?0:individual.qi1013b;
    	
    	if(in1013 == 1 || in1013aa == 1 || in1013b == 1){
			Util.lockView(getActivity(), false,chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.VISIBLE);
		}else{
			Util.cleanAndLockView(getActivity(),chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.GONE);
		}
    	txtCabecera.requestFocus();
    }

    public void onrgQI1013ChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,8,rgQI1013A.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.VISIBLE);
			
			onchbQI1014_AChangeValue();
			onchbQI1014_BChangeValue();
			onchbQI1014_CChangeValue();
			onchbQI1014_DChangeValue();
			onchbQI1014_EChangeValue();
			onchbQI1014_FChangeValue();
			onchbQI1014_GChangeValue();
			onchbQI1014_HChangeValue();
			onchbQI1014_IChangeValue();
			onchbQI1014_JChangeValue();
			onchbQI1014_KChangeValue();
			onchbQI1014_LChangeValue();
			onchbQI1014_MChangeValue();
			onchbQI1014_NChangeValue();
			onchbQI1014_OChangeValue();
			onchbQI1014_PChangeValue();
			onchbQI1014_QChangeValue();
			onchbQI1014_RChangeValue();
			onchbQI1014_SChangeValue();
			onchbQI1014_TChangeValue();
			
			onchbQI1014_UChangeValue();
			onchbQI1014_VChangeValue();
			
			onchbQI1014_XChangeValue();
			
			chbQI1014_A.requestFocus();
		}
    }
    
    public void onrgQI1013AAChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,8,rgQI1013AA.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.VISIBLE);
			
			onchbQI1014_AChangeValue();
			onchbQI1014_BChangeValue();
			onchbQI1014_CChangeValue();
			onchbQI1014_DChangeValue();
			onchbQI1014_EChangeValue();
			onchbQI1014_FChangeValue();
			onchbQI1014_GChangeValue();
			onchbQI1014_HChangeValue();
			onchbQI1014_IChangeValue();
			onchbQI1014_JChangeValue();
			onchbQI1014_KChangeValue();
			onchbQI1014_LChangeValue();
			onchbQI1014_MChangeValue();
			onchbQI1014_NChangeValue();
			onchbQI1014_OChangeValue();
			onchbQI1014_PChangeValue();
			onchbQI1014_QChangeValue();
			onchbQI1014_RChangeValue();
			onchbQI1014_SChangeValue();
			onchbQI1014_TChangeValue();
			
			onchbQI1014_UChangeValue();
			onchbQI1014_VChangeValue();
			
			onchbQI1014_XChangeValue();
			
			chbQI1014_A.requestFocus();
		}
    }
    
    public void onrgQI1013BChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,8,rgQI1013B.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,chbQI1014_A,chbQI1014_B,chbQI1014_C,chbQI1014_D,chbQI1014_E,chbQI1014_F,chbQI1014_G,chbQI1014_H,chbQI1014_I,chbQI1014_J,chbQI1014_K,chbQI1014_L,chbQI1014_M,chbQI1014_N,chbQI1014_O,chbQI1014_P,chbQI1014_Q,chbQI1014_R,chbQI1014_S,chbQI1014_T,chbQI1014_U,chbQI1014_V,chbQI1014_X);
			q2.setVisibility(View.VISIBLE);
			
			onchbQI1014_AChangeValue();
			onchbQI1014_BChangeValue();
			onchbQI1014_CChangeValue();
			onchbQI1014_DChangeValue();
			onchbQI1014_EChangeValue();
			onchbQI1014_FChangeValue();
			onchbQI1014_GChangeValue();
			onchbQI1014_HChangeValue();
			onchbQI1014_IChangeValue();
			onchbQI1014_JChangeValue();
			onchbQI1014_KChangeValue();
			onchbQI1014_LChangeValue();
			onchbQI1014_MChangeValue();
			onchbQI1014_NChangeValue();
			onchbQI1014_OChangeValue();
			onchbQI1014_PChangeValue();
			onchbQI1014_QChangeValue();
			onchbQI1014_RChangeValue();
			onchbQI1014_SChangeValue();
			onchbQI1014_TChangeValue();
			
			onchbQI1014_UChangeValue();
			onchbQI1014_VChangeValue();
			
			onchbQI1014_XChangeValue();
			
			chbQI1014_A.requestFocus();
		}
    }

    
    public boolean verificarCheck() {
  		if (chbQI1014_A.isChecked() || chbQI1014_B.isChecked() || chbQI1014_C.isChecked() || chbQI1014_D.isChecked() || chbQI1014_E.isChecked() || chbQI1014_F.isChecked() || chbQI1014_G.isChecked() || chbQI1014_H.isChecked() || chbQI1014_I.isChecked() || chbQI1014_J.isChecked() || chbQI1014_K.isChecked() || chbQI1014_L.isChecked() || chbQI1014_M.isChecked() || chbQI1014_N.isChecked() || chbQI1014_O.isChecked() || chbQI1014_P.isChecked() || chbQI1014_Q.isChecked() || chbQI1014_R.isChecked() || chbQI1014_S.isChecked() || chbQI1014_T.isChecked() || chbQI1014_U.isChecked() || chbQI1014_V.isChecked() || chbQI1014_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void onchbQI1014_AChangeValue() {
    	if (chbQI1014_A.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_A2,rgQI1014_AN);
			lblQI1014_A2.setVisibility(View.VISIBLE);
			rgQI1014_AN.setVisibility(View.VISIBLE);
			rgQI1014_AN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_A2, rgQI1014_AN);
			lblQI1014_A2.setVisibility(View.GONE);
			rgQI1014_AN.setVisibility(View.GONE);
			chbQI1014_B.requestFocus();
		}
    }
    
    public void onchbQI1014_BChangeValue() {
    	if (chbQI1014_B.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_B2,rgQI1014_BN);
			lblQI1014_B2.setVisibility(View.VISIBLE);
			rgQI1014_BN.setVisibility(View.VISIBLE);
			rgQI1014_BN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_B2, rgQI1014_BN);
			lblQI1014_B2.setVisibility(View.GONE);
			rgQI1014_BN.setVisibility(View.GONE);
			chbQI1014_C.requestFocus();
		}
    }
    
    public void onchbQI1014_CChangeValue() {
    	if (chbQI1014_C.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_C2,rgQI1014_CN);
			lblQI1014_C2.setVisibility(View.VISIBLE);
			rgQI1014_CN.setVisibility(View.VISIBLE);
			rgQI1014_CN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_C2, rgQI1014_CN);
			lblQI1014_C2.setVisibility(View.GONE);
			rgQI1014_CN.setVisibility(View.GONE);
			chbQI1014_D.requestFocus();
		}
    }
    
    public void onchbQI1014_DChangeValue() {
    	if (chbQI1014_D.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_D2,rgQI1014_DN);
			lblQI1014_D2.setVisibility(View.VISIBLE);
			rgQI1014_DN.setVisibility(View.VISIBLE);
			rgQI1014_DN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_D2, rgQI1014_DN);
			lblQI1014_D2.setVisibility(View.GONE);
			rgQI1014_DN.setVisibility(View.GONE);
			chbQI1014_E.requestFocus();
		}
    }
    
    public void onchbQI1014_EChangeValue() {
    	if (chbQI1014_E.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_E2,rgQI1014_EN);
			lblQI1014_E2.setVisibility(View.VISIBLE);
			rgQI1014_EN.setVisibility(View.VISIBLE);
			rgQI1014_EN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_E2, rgQI1014_EN);
			lblQI1014_E2.setVisibility(View.GONE);
			rgQI1014_EN.setVisibility(View.GONE);
			chbQI1014_F.requestFocus();
		}
    }
    
    public void onchbQI1014_FChangeValue() {
    	if (chbQI1014_F.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_F2,rgQI1014_FN);
			lblQI1014_F2.setVisibility(View.VISIBLE);
			rgQI1014_FN.setVisibility(View.VISIBLE);
			rgQI1014_FN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_F2, rgQI1014_FN);
			lblQI1014_F2.setVisibility(View.GONE);
			rgQI1014_FN.setVisibility(View.GONE);
			chbQI1014_G.requestFocus();
		}
    }
    
    public void onchbQI1014_GChangeValue() {
    	if (chbQI1014_G.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_G2,rgQI1014_GN);
			lblQI1014_G2.setVisibility(View.VISIBLE);
			rgQI1014_GN.setVisibility(View.VISIBLE);
			rgQI1014_GN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_G2, rgQI1014_GN);
			lblQI1014_G2.setVisibility(View.GONE);
			rgQI1014_GN.setVisibility(View.GONE);
			chbQI1014_H.requestFocus();
		}
    }
    
    public void onchbQI1014_HChangeValue() {
    	if (chbQI1014_H.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_H2,rgQI1014_HN);
			lblQI1014_H2.setVisibility(View.VISIBLE);
			rgQI1014_HN.setVisibility(View.VISIBLE);
			rgQI1014_HN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_H2, rgQI1014_HN);
			lblQI1014_H2.setVisibility(View.GONE);
			rgQI1014_HN.setVisibility(View.GONE);
			chbQI1014_I.requestFocus();
		}
    }
    
    public void onchbQI1014_IChangeValue() {
    	if (chbQI1014_I.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_I2,rgQI1014_IN);
			lblQI1014_I2.setVisibility(View.VISIBLE);
			rgQI1014_IN.setVisibility(View.VISIBLE);
			rgQI1014_IN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_I2, rgQI1014_IN);
			lblQI1014_I2.setVisibility(View.GONE);
			rgQI1014_IN.setVisibility(View.GONE);
			chbQI1014_J.requestFocus();
		}
    }
    
    public void onchbQI1014_JChangeValue() {
    	if (chbQI1014_J.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_F2,rgQI1014_JN);
			lblQI1014_J2.setVisibility(View.VISIBLE);
			rgQI1014_JN.setVisibility(View.VISIBLE);
			rgQI1014_JN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_J2, rgQI1014_JN);
			lblQI1014_J2.setVisibility(View.GONE);
			rgQI1014_JN.setVisibility(View.GONE);
			chbQI1014_K.requestFocus();
		}
    }
    
    public void onchbQI1014_KChangeValue() {
    	if (chbQI1014_K.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_F2,rgQI1014_KN);
			lblQI1014_K2.setVisibility(View.VISIBLE);
			rgQI1014_KN.setVisibility(View.VISIBLE);
			rgQI1014_KN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_K2, rgQI1014_KN);
			lblQI1014_K2.setVisibility(View.GONE);
			rgQI1014_KN.setVisibility(View.GONE);
			chbQI1014_L.requestFocus();
		}
    }
    
    public void onchbQI1014_LChangeValue() {
    	if (chbQI1014_L.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_L2,rgQI1014_LN);
			lblQI1014_L2.setVisibility(View.VISIBLE);
			rgQI1014_LN.setVisibility(View.VISIBLE);
			rgQI1014_LN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_L2, rgQI1014_LN);
			lblQI1014_L2.setVisibility(View.GONE);
			rgQI1014_LN.setVisibility(View.GONE);
			chbQI1014_M.requestFocus();
		}
    }
    
    public void onchbQI1014_MChangeValue() {
    	if (chbQI1014_M.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_M2,rgQI1014_MN);
			lblQI1014_M2.setVisibility(View.VISIBLE);
			rgQI1014_MN.setVisibility(View.VISIBLE);
			rgQI1014_MN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_M2, rgQI1014_MN);
			lblQI1014_M2.setVisibility(View.GONE);
			rgQI1014_MN.setVisibility(View.GONE);
			chbQI1014_N.requestFocus();
		}
    }
    
    public void onchbQI1014_NChangeValue() {
    	if (chbQI1014_N.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_N2,rgQI1014_NN);
			lblQI1014_N2.setVisibility(View.VISIBLE);
			rgQI1014_NN.setVisibility(View.VISIBLE);
			rgQI1014_NN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_N2, rgQI1014_NN);
			lblQI1014_N2.setVisibility(View.GONE);
			rgQI1014_NN.setVisibility(View.GONE);
			chbQI1014_O.requestFocus();
		}
    }
    
    public void onchbQI1014_OChangeValue() {
    	if (chbQI1014_O.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_O2,rgQI1014_ON);
			lblQI1014_O2.setVisibility(View.VISIBLE);
			rgQI1014_ON.setVisibility(View.VISIBLE);
			rgQI1014_ON.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_O2, rgQI1014_ON);
			lblQI1014_O2.setVisibility(View.GONE);
			rgQI1014_ON.setVisibility(View.GONE);
			chbQI1014_P.requestFocus();
		}
    }
    
    public void onchbQI1014_PChangeValue() {
    	if (chbQI1014_P.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_P2,rgQI1014_PN);
			lblQI1014_P2.setVisibility(View.VISIBLE);
			rgQI1014_PN.setVisibility(View.VISIBLE);
			rgQI1014_PN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_P2, rgQI1014_PN);
			lblQI1014_P2.setVisibility(View.GONE);
			rgQI1014_PN.setVisibility(View.GONE);
			chbQI1014_Q.requestFocus();
		}
    }
    
    public void onchbQI1014_QChangeValue() {
    	if (chbQI1014_Q.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_Q2,rgQI1014_QN);
			lblQI1014_Q2.setVisibility(View.VISIBLE);
			rgQI1014_QN.setVisibility(View.VISIBLE);
			rgQI1014_QN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_Q2, rgQI1014_QN);
			lblQI1014_Q2.setVisibility(View.GONE);
			rgQI1014_QN.setVisibility(View.GONE);
			chbQI1014_R.requestFocus();
		}
    }
    
    public void onchbQI1014_RChangeValue() {
    	if (chbQI1014_R.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_R2,rgQI1014_RN);
			lblQI1014_R2.setVisibility(View.VISIBLE);
			rgQI1014_RN.setVisibility(View.VISIBLE);
			rgQI1014_RN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_R2, rgQI1014_RN);
			lblQI1014_R2.setVisibility(View.GONE);
			rgQI1014_RN.setVisibility(View.GONE);
			chbQI1014_S.requestFocus();
		}
    }
    
    public void onchbQI1014_SChangeValue() {
    	if (chbQI1014_S.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_S2,rgQI1014_SN);
			lblQI1014_S2.setVisibility(View.VISIBLE);
			rgQI1014_SN.setVisibility(View.VISIBLE);
			rgQI1014_SN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_S2, rgQI1014_SN);
			lblQI1014_S2.setVisibility(View.GONE);
			rgQI1014_SN.setVisibility(View.GONE);
			chbQI1014_T.requestFocus();
		}
    }
    
    public void onchbQI1014_TChangeValue() {
    	if (chbQI1014_T.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_T2,rgQI1014_TN);
			lblQI1014_T2.setVisibility(View.VISIBLE);
			rgQI1014_TN.setVisibility(View.VISIBLE);
			rgQI1014_TN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_T2, rgQI1014_TN);
			lblQI1014_T2.setVisibility(View.GONE);
			rgQI1014_TN.setVisibility(View.GONE);
			chbQI1014_U.requestFocus();
		}
    }
    
    
    public void onchbQI1014_UChangeValue() {
    	if (chbQI1014_U.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_U2,rgQI1014_UN);
			lblQI1014_U2.setVisibility(View.VISIBLE);
			rgQI1014_UN.setVisibility(View.VISIBLE);
			rgQI1014_UN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_U2, rgQI1014_UN);
			lblQI1014_U2.setVisibility(View.GONE);
			rgQI1014_UN.setVisibility(View.GONE);
			chbQI1014_V.requestFocus();
		}
    }
    
    
    public void onchbQI1014_VChangeValue() {
    	if (chbQI1014_V.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1014_V2,rgQI1014_VN);
			lblQI1014_V2.setVisibility(View.VISIBLE);
			rgQI1014_VN.setVisibility(View.VISIBLE);
			rgQI1014_VN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1014_V2, rgQI1014_VN);
			lblQI1014_V2.setVisibility(View.GONE);
			rgQI1014_VN.setVisibility(View.GONE);
			chbQI1014_X.requestFocus();
		}
    }
    
    
    
    public void onchbQI1014_XChangeValue() {
    	if (chbQI1014_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI1014XI, lblQI1014_X2,rgQI1014_XN);
  			lblQI1014_X2.setVisibility(View.VISIBLE);
			rgQI1014_XN.setVisibility(View.VISIBLE);
  			txtQI1014XI.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI1014XI,lblQI1014_X2, rgQI1014_XN);
			lblQI1014_X2.setVisibility(View.GONE);
			rgQI1014_XN.setVisibility(View.GONE);
  		}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1013A.readOnly();
    		
    		rgQI1013AA.readOnly();
    		rgQI1013B.readOnly();
    		
    		txtQI1014XI.readOnly();
    		chbQI1014_A.readOnly();
    		chbQI1014_B.readOnly();
    		chbQI1014_C.readOnly();
    		chbQI1014_D.readOnly();
    		chbQI1014_E.readOnly();
    		chbQI1014_F.readOnly();
    		chbQI1014_G.readOnly();
    		chbQI1014_H.readOnly();
    		chbQI1014_I.readOnly();
    		chbQI1014_J.readOnly();
    		chbQI1014_K.readOnly();
    		chbQI1014_L.readOnly();
    		chbQI1014_M.readOnly();
    		chbQI1014_N.readOnly();
    		chbQI1014_O.readOnly();
    		chbQI1014_P.readOnly();
    		chbQI1014_Q.readOnly();
    		chbQI1014_R.readOnly();
    		chbQI1014_S.readOnly();
    		chbQI1014_T.readOnly();
    		
    		chbQI1014_U.readOnly();
    		chbQI1014_V.readOnly();
    		
    		chbQI1014_X.readOnly();
    		rgQI1014_AN.readOnly();
    		rgQI1014_BN.readOnly();
    		rgQI1014_CN.readOnly();
    		rgQI1014_DN.readOnly();
    		rgQI1014_EN.readOnly();
    		rgQI1014_FN.readOnly();
    		rgQI1014_GN.readOnly();
    		rgQI1014_HN.readOnly();
    		rgQI1014_IN.readOnly();
    		rgQI1014_JN.readOnly();
    		rgQI1014_KN.readOnly();
    		rgQI1014_LN.readOnly();
    		rgQI1014_MN.readOnly();
    		rgQI1014_NN.readOnly();
    		rgQI1014_ON.readOnly();
    		rgQI1014_PN.readOnly();
    		rgQI1014_QN.readOnly();
    		rgQI1014_RN.readOnly();
    		rgQI1014_SN.readOnly();
    		rgQI1014_TN.readOnly();
    		
    		rgQI1014_UN.readOnly();
    		rgQI1014_VN.readOnly();
    		
    		rgQI1014_XN.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi1013a!=null)
			individual.qi1013a=individual.getConvertQi1013(individual.qi1013a);
		
		if (individual.qi1013aa!=null)
			individual.qi1013aa=individual.getConvertQi1013(individual.qi1013aa);
		
		if (individual.qi1013b!=null)
			individual.qi1013b=individual.getConvertQi1013(individual.qi1013b);
		
		int cont=0;		
		if(individual.qi1014_a==1)
			cont+=1;
		if(individual.qi1014_b==1)
			cont+=1;
		if(individual.qi1014_c==1)
			cont+=1;
		if(individual.qi1014_d==1)
			cont+=1;
		if(individual.qi1014_e==1)
			cont+=1;
		if(individual.qi1014_f==1)
			cont+=1;
		if(individual.qi1014_g==1)
			cont+=1;
		if(individual.qi1014_h==1)
			cont+=1;
		if(individual.qi1014_i==1)
			cont+=1;
		if(individual.qi1014_j==1)
			cont+=1;
		if(individual.qi1014_k==1)
			cont+=1;
		if(individual.qi1014_l==1)
			cont+=1;
		if(individual.qi1014_m==1)
			cont+=1;
		if(individual.qi1014_n==1)
			cont+=1;
		if(individual.qi1014_o==1)
			cont+=1;
		if(individual.qi1014_p==1)
			cont+=1;
		if(individual.qi1014_q==1)
			cont+=1;
		if(individual.qi1014_r==1)
			cont+=1;
		if(individual.qi1014_s==1)
			cont+=1;
		if(individual.qi1014_t==1)
			cont+=1;
		
		if(individual.qi1014_u==1)
			cont+=1;
		if(individual.qi1014_v==1)
			cont+=1;
		
		if(individual.qi1014_x==1)
			cont+=1;
		
		App.getInstance().getCiseccion_10_01().filtro1014nico = ContadorPregunta1014();
		
		App.getInstance().getCiseccion_10_01().filtro1014 = cont;
		App.getInstance().getCiseccion_10_01().qi1014_a=individual.qi1014_a;
		App.getInstance().getCiseccion_10_01().qi1014_b=individual.qi1014_b;
		App.getInstance().getCiseccion_10_01().qi1014_c=individual.qi1014_c;
		App.getInstance().getCiseccion_10_01().qi1014_d=individual.qi1014_d;
		App.getInstance().getCiseccion_10_01().qi1014_e=individual.qi1014_e;
		App.getInstance().getCiseccion_10_01().qi1014_f=individual.qi1014_f;
		App.getInstance().getCiseccion_10_01().qi1014_g=individual.qi1014_g;
		App.getInstance().getCiseccion_10_01().qi1014_h=individual.qi1014_h;
		App.getInstance().getCiseccion_10_01().qi1014_i=individual.qi1014_i;
		App.getInstance().getCiseccion_10_01().qi1014_j=individual.qi1014_j;
		App.getInstance().getCiseccion_10_01().qi1014_k=individual.qi1014_k;
		App.getInstance().getCiseccion_10_01().qi1014_l=individual.qi1014_l;
		App.getInstance().getCiseccion_10_01().qi1014_m=individual.qi1014_m;
		App.getInstance().getCiseccion_10_01().qi1014_n=individual.qi1014_n;
		App.getInstance().getCiseccion_10_01().qi1014_o=individual.qi1014_o;
		App.getInstance().getCiseccion_10_01().qi1014_p=individual.qi1014_p;
		App.getInstance().getCiseccion_10_01().qi1014_k=individual.qi1014_q;
		App.getInstance().getCiseccion_10_01().qi1014_r=individual.qi1014_r;
		App.getInstance().getCiseccion_10_01().qi1014_s=individual.qi1014_s;
		App.getInstance().getCiseccion_10_01().qi1014_t=individual.qi1014_t;
		
		App.getInstance().getCiseccion_10_01().qi1014_u=individual.qi1014_u;
		App.getInstance().getCiseccion_10_01().qi1014_v=individual.qi1014_v;
		
		App.getInstance().getCiseccion_10_01().qi1014_x=individual.qi1014_x;
		
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		App.getInstance().getCiseccion_10_01().qi1013a=individual.qi1013a;
		
		App.getInstance().getCiseccion_10_01().qi1013aa=individual.qi1013aa;
		App.getInstance().getCiseccion_10_01().qi1013b=individual.qi1013b;
		
		return App.INDIVIDUAL;
	}
	
	public boolean ContadorPregunta1014(){
    	Integer p1014a=0;
    	Integer p1014b=0;
    	Integer p1014c=0;
    	Integer p1014d=0;
    	Integer p1014e=0;
    	Integer p1014f=0;
    	Integer p1014g=0;
    	Integer p1014h=0;
    	Integer p1014i=0;
    	Integer p1014j=0;
    	Integer p1014k=0;
    	Integer p1014l=0;
    	Integer p1014m=0;
    	Integer p1014n=0;
    	Integer p1014o=0;
    	Integer p1014p=0;
    	Integer p1014q=0;
    	Integer p1014r=0;
    	Integer p1014s=0;
    	Integer p1014t=0;
    	Integer p1014u=0;
    	Integer p1014v=0;
    	Integer p1014x=0;
    	Integer total=0;
    	Integer totaln=0;
    	if(individual.qi1014_a!=null && individual.qi1014_a==1)p1014a=1;
    	if(individual.qi1014_b!=null && individual.qi1014_b==1)p1014b=1;
    	if(individual.qi1014_c!=null && individual.qi1014_c==1)p1014c=1;
    	if(individual.qi1014_d!=null && individual.qi1014_d==1)p1014d=1;
    	if(individual.qi1014_e!=null && individual.qi1014_e==1)p1014e=1;
    	if(individual.qi1014_f!=null && individual.qi1014_f==1)p1014f=1;
    	if(individual.qi1014_g!=null && individual.qi1014_g==1)p1014g=1;
    	if(individual.qi1014_h!=null && individual.qi1014_h==1)p1014h=1;
    	if(individual.qi1014_i!=null && individual.qi1014_i==1)p1014i=1;
    	if(individual.qi1014_j!=null && individual.qi1014_j==1)p1014j=1;
    	if(individual.qi1014_k!=null && individual.qi1014_k==1)p1014k=1;
    	if(individual.qi1014_l!=null && individual.qi1014_l==1)p1014l=1;
    	if(individual.qi1014_m!=null && individual.qi1014_m==1)p1014m=1;
    	if(individual.qi1014_n!=null && individual.qi1014_n==1)p1014n=1;
    	if(individual.qi1014_o!=null && individual.qi1014_o==1)p1014o=1;
    	if(individual.qi1014_p!=null && individual.qi1014_p==1)p1014p=1;
    	if(individual.qi1014_q!=null && individual.qi1014_q==1)p1014q=1;
    	if(individual.qi1014_r!=null && individual.qi1014_r==1)p1014r=1;
    	if(individual.qi1014_s!=null && individual.qi1014_s==1)p1014s=1;
    	if(individual.qi1014_t!=null && individual.qi1014_t==1)p1014t=1;
    	if(individual.qi1014_u!=null && individual.qi1014_u==1)p1014u=1;
    	if(individual.qi1014_v!=null && individual.qi1014_v==1)p1014v=1;
    	if(individual.qi1014_x!=null && individual.qi1014_x==1)p1014x=1;
    	total=p1014a+p1014b+p1014c+p1014d+	p1014e+	p1014f+	p1014g+	p1014h+	p1014i+	p1014j+	p1014k+	p1014l+	p1014m+	p1014n+	p1014o+	p1014p+	p1014q+	p1014r+	p1014s+	p1014t+	p1014u+	p1014v+	p1014x;
    	Log.e("","TOTAl: "+total);
    	  p1014a=0; p1014b=0; p1014c=0; p1014d=0; p1014e=0; p1014f=0; p1014g=0; p1014h=0;
          p1014i=0; p1014j=0; p1014k=0; p1014l=0; p1014m=0; p1014n=0; p1014o=0; p1014p=0;
          p1014q=0; p1014r=0; p1014s=0; p1014t=0; p1014u=0; p1014v=0; p1014x=0;
        if(individual.qi1014_an!=null && individual.qi1014_an==3)p1014a=1;
        if(individual.qi1014_bn!=null && individual.qi1014_bn==3)p1014b=1;
        if(individual.qi1014_cn!=null && individual.qi1014_cn==3)p1014c=1;
        if(individual.qi1014_dn!=null && individual.qi1014_dn==3)p1014d=1;
        if(individual.qi1014_en!=null && individual.qi1014_en==3)p1014e=1;
        if(individual.qi1014_fn!=null && individual.qi1014_fn==3)p1014f=1;
        if(individual.qi1014_gn!=null && individual.qi1014_gn==3)p1014g=1;
        if(individual.qi1014_hn!=null && individual.qi1014_hn==3)p1014h=1;
        if(individual.qi1014_in!=null && individual.qi1014_in==3)p1014i=1;
        if(individual.qi1014_jn!=null && individual.qi1014_jn==3)p1014j=1;
        if(individual.qi1014_kn!=null && individual.qi1014_kn==3)p1014k=1;
        if(individual.qi1014_ln!=null && individual.qi1014_ln==3)p1014l=1;
        if(individual.qi1014_mn!=null && individual.qi1014_mn==3)p1014m=1;
        if(individual.qi1014_nn!=null && individual.qi1014_nn==3)p1014n=1;
        if(individual.qi1014_on!=null && individual.qi1014_on==3)p1014o=1;
        if(individual.qi1014_pn!=null && individual.qi1014_pn==3)p1014p=1;
        if(individual.qi1014_qn!=null && individual.qi1014_qn==3)p1014q=1;
        if(individual.qi1014_rn!=null && individual.qi1014_rn==3)p1014r=1;
        if(individual.qi1014_sn!=null && individual.qi1014_sn==3)p1014s=1;
        if(individual.qi1014_tn!=null && individual.qi1014_tn==3)p1014t=1;
        if(individual.qi1014_un!=null && individual.qi1014_un==3)p1014u=1;
        if(individual.qi1014_vn!=null && individual.qi1014_vn==3)p1014v=1;
        if(individual.qi1014_xn!=null && individual.qi1014_xn==3)p1014x=1;
         totaln=p1014a+p1014b+p1014c+p1014d+	p1014e+	p1014f+	p1014g+	p1014h+	p1014i+	p1014j+	p1014k+	p1014l+	p1014m+	p1014n+	p1014o+	p1014p+	p1014q+	p1014r+	p1014s+	p1014t+	p1014u+	p1014v+	p1014x;
         Log.e("","TOTAlN: "+totaln);
    	return total==totaln;
    }
	
}