package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.IRadioGroupOptions;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_007 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1016;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQI1016X;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI1017;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1019;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI1020_A;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1020_AN;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI1020_B;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1020_BN;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI1020_C;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1020_CN;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI1020_D;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI1020_DN;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI1020_E;
	@FieldAnnotation(orderIndex=14)
	public RadioGroupOtherField rgQI1020_EN;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI1020_F;
	@FieldAnnotation(orderIndex=16)
	public RadioGroupOtherField rgQI1020_FN;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI1020_G;
	@FieldAnnotation(orderIndex=18)
	public RadioGroupOtherField rgQI1020_GN;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI1020_H;
	@FieldAnnotation(orderIndex=20)
	public RadioGroupOtherField rgQI1020_HN;
	@FieldAnnotation(orderIndex=21)
	public CheckBoxField chbQI1020_I;
	@FieldAnnotation(orderIndex=22)
	public RadioGroupOtherField rgQI1020_IN;
	@FieldAnnotation(orderIndex=23)
	public CheckBoxField chbQI1020_J;
	@FieldAnnotation(orderIndex=24)
	public RadioGroupOtherField rgQI1020_JN;
	@FieldAnnotation(orderIndex=25)
	public CheckBoxField chbQI1020_K;
	@FieldAnnotation(orderIndex=26)
	public RadioGroupOtherField rgQI1020_KN;
	@FieldAnnotation(orderIndex=27)
	public CheckBoxField chbQI1020_L;
	@FieldAnnotation(orderIndex=28)
	public RadioGroupOtherField rgQI1020_LN;
	@FieldAnnotation(orderIndex=29)
	public CheckBoxField chbQI1020_M;
	@FieldAnnotation(orderIndex=30)
	public RadioGroupOtherField rgQI1020_MN;
	@FieldAnnotation(orderIndex=31)
	public CheckBoxField chbQI1020_N;
	@FieldAnnotation(orderIndex=32)
	public RadioGroupOtherField rgQI1020_NN;
	@FieldAnnotation(orderIndex=33)
	public CheckBoxField chbQI1020_O;
	@FieldAnnotation(orderIndex=34)
	public RadioGroupOtherField rgQI1020_ON;
	@FieldAnnotation(orderIndex=35)
	public CheckBoxField chbQI1020_P;
	@FieldAnnotation(orderIndex=36)
	public RadioGroupOtherField rgQI1020_PN;
	@FieldAnnotation(orderIndex=37)
	public CheckBoxField chbQI1020_Q;
	@FieldAnnotation(orderIndex=38)
	public RadioGroupOtherField rgQI1020_QN;
	@FieldAnnotation(orderIndex=39)
	public CheckBoxField chbQI1020_R;
	@FieldAnnotation(orderIndex=40)
	public RadioGroupOtherField rgQI1020_RN;
	@FieldAnnotation(orderIndex=41)
	public CheckBoxField chbQI1020_S;
	@FieldAnnotation(orderIndex=42)
	public RadioGroupOtherField rgQI1020_SN;
	@FieldAnnotation(orderIndex=43)
	public CheckBoxField chbQI1020_T;
	@FieldAnnotation(orderIndex=44)
	public RadioGroupOtherField rgQI1020_TN;
	@FieldAnnotation(orderIndex=45)
	public CheckBoxField chbQI1020_U;
	@FieldAnnotation(orderIndex=46)
	public RadioGroupOtherField rgQI1020_UN;
	
	@FieldAnnotation(orderIndex=47)
	public CheckBoxField chbQI1020_V;
	@FieldAnnotation(orderIndex=48)
	public RadioGroupOtherField rgQI1020_VN;
	
	@FieldAnnotation(orderIndex=49)
	public CheckBoxField chbQI1020_W;
	@FieldAnnotation(orderIndex=50)
	public RadioGroupOtherField rgQI1020_WN;
	
	@FieldAnnotation(orderIndex=51)
	public CheckBoxField chbQI1020_X;
	@FieldAnnotation(orderIndex=52)
	public TextField txtQI1020_XI;
	@FieldAnnotation(orderIndex=53)
	public RadioGroupOtherField rgQI1020_XN;
	
	
	CISECCION_10_01 individual;
	CISECCION_01_03 individualS1;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1016,lblpregunta1017,lblpregunta1019,lblpregunta1020,lblnumero
	,lblQI1020_A2,lblQI1020_B2,lblQI1020_C2,lblQI1020_D2,lblQI1020_E2,lblQI1020_F2,lblQI1020_G2,lblQI1020_H2,lblQI1020_I2,lblQI1020_J2,lblQI1020_K2,lblQI1020_L2,lblQI1020_M2,lblQI1020_N2,lblQI1020_O2
	,lblQI1020_P2,lblQI1020_Q2,lblQI1020_R2,lblQI1020_S2,lblQI1020_T2,lblQI1020_U2 ,lblQI1020_V2,lblQI1020_W2 ,lblQI1020_X2;
	public TextField txtCabecera;
	public GridComponent2 gdViolencia, gridPreguntas1020;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS1;

	public CISECCION_10_01Fragment_007() {}
	public CISECCION_10_01Fragment_007 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1014_A","QI1014_AN","QI1014_B","QI1014_BN","QI1014_C","QI1014_CN","QI1014_D","QI1014_DN","QI1014_E","QI1014_EN","QI1014_F","QI1014_FN","QI1014_G","QI1014_GN","QI1014_H","QI1014_HN","QI1014_I","QI1014_IN","QI1014_J","QI1014_jN","QI1014_K","QI1014_KN","QI1014_L","QI1014_LN","QI1014_M","QI1014_MN","QI1014_N","QI1014_NN","QI1014_O","QI1014_ON","QI1014_P","QI1014_PN","QI1014_Q","QI1014_QN","QI1014_R","QI1014_RN","QI1014_S","QI1014_SN","QI1014_T","QI1014_TN","QI1014_X","QI1014_XN","QI1016","QI1016X","QI1017","QI1019","QI1020_A","QI1020_AN","QI1020_B","QI1020_BN","QI1020_C","QI1020_CN","QI1020_D","QI1020_DN","QI1020_E","QI1020_EN","QI1020_F","QI1020_FN","QI1020_G","QI1020_GN","QI1020_H","QI1020_HN","QI1020_I","QI1020_IN","QI1020_J","QI1020_JN","QI1020_K","QI1020_KN","QI1020_L","QI1020_LN","QI1020_M","QI1020_MN","QI1020_N","QI1020_NN","QI1020_O","QI1020_ON","QI1020_P","QI1020_PN","QI1020_Q","QI1020_QN","QI1020_R","QI1020_RN","QI1020_S","QI1020_SN","QI1020_T","QI1020_TN","QI1020_U","QI1020_UN","QI1020_V","QI1020_VN","QI1020_W","QI1020_WN","QI1020_X","QI1020_XI","QI1020_XN","qi1013a","qi1013aa","qi1013b","ID","HOGAR_ID","PERSONA_ID","QI1014_U","QI1014_UN","QI1014_V","QI1014_VN")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1016","QI1016X","QI1017","QI1019","QI1020_A","QI1020_AN","QI1020_B","QI1020_BN","QI1020_C","QI1020_CN","QI1020_D","QI1020_DN","QI1020_E","QI1020_EN","QI1020_F","QI1020_FN","QI1020_G","QI1020_GN","QI1020_H","QI1020_HN","QI1020_I","QI1020_IN","QI1020_J","QI1020_JN","QI1020_K","QI1020_KN","QI1020_L","QI1020_LN","QI1020_M","QI1020_MN","QI1020_N","QI1020_NN","QI1020_O","QI1020_ON","QI1020_P","QI1020_PN","QI1020_Q","QI1020_QN","QI1020_R","QI1020_RN","QI1020_S","QI1020_SN","QI1020_T","QI1020_TN","QI1020_U","QI1020_UN","QI1020_V","QI1020_VN","QI1020_W","QI1020_WN","QI1020_X","QI1020_XI","QI1020_XN")};
		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI201","QI208","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView;
	}
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1016 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1016);
		lblpregunta1017 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1017);
		lblpregunta1019 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1019);
		lblpregunta1020 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1020);
		lblnumero  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.ciseccion_10_01qi1017_num).textSize(16).centrar();
		
		rgQI1016=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1016_1,R.string.ciseccion_10_01qi1016_2,R.string.ciseccion_10_01qi1016_3,R.string.ciseccion_10_01qi1016_4,R.string.ciseccion_10_01qi1016_5,R.string.ciseccion_10_01qi1016_6,R.string.ciseccion_10_01qi1016_7,R.string.ciseccion_10_01qi1016_8,R.string.ciseccion_10_01qi1016_9,R.string.ciseccion_10_01qi1016_10,R.string.ciseccion_10_01qi1016_11,R.string.ciseccion_10_01qi1016_12,R.string.ciseccion_10_01qi1016_13,R.string.ciseccion_10_01qi1016_14,R.string.ciseccion_10_01qi1016_15,R.string.ciseccion_10_01qi1016_16,R.string.ciseccion_10_01qi1016_17,R.string.ciseccion_10_01qi1016_18,R.string.ciseccion_10_01qi1016_19,R.string.ciseccion_10_01qi1016_20,R.string.ciseccion_10_01qi1016_21,R.string.ciseccion_10_01qi1016_22,R.string.ciseccion_10_01qi1016_23).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI1016X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI1016.agregarEspecifique(22,txtQI1016X);
		txtQI1017=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		gdViolencia = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdViolencia.addComponent(lblnumero);
		gdViolencia.addComponent(txtQI1017);
		
		rgQI1019=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1019_1,R.string.ciseccion_10_01qi1019_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1019ChangeValue");
		
		chbQI1020_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_a, "1:0").size(altoComponente, 770).callback("onchbQI1020_AChangeValue");
		chbQI1020_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_b, "1:0").size(altoComponente, 770).callback("onchbQI1020_BChangeValue");
		chbQI1020_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_c, "1:0").size(altoComponente, 770).callback("onchbQI1020_CChangeValue");
		chbQI1020_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_d, "1:0").size(altoComponente, 770).callback("onchbQI1020_DChangeValue");
		chbQI1020_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_e, "1:0").size(altoComponente, 770).callback("onchbQI1020_EChangeValue");
		chbQI1020_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_f, "1:0").size(altoComponente, 770).callback("onchbQI1020_FChangeValue");
		chbQI1020_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_g, "1:0").size(altoComponente, 770).callback("onchbQI1020_GChangeValue");
		chbQI1020_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_h, "1:0").size(altoComponente, 770).callback("onchbQI1020_HChangeValue");
		chbQI1020_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_i, "1:0").size(altoComponente, 770).callback("onchbQI1020_IChangeValue");
		chbQI1020_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_j, "1:0").size(altoComponente, 770).callback("onchbQI1020_JChangeValue");
		chbQI1020_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_k, "1:0").size(altoComponente, 770).callback("onchbQI1020_KChangeValue");
		chbQI1020_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_l, "1:0").size(altoComponente, 770).callback("onchbQI1020_LChangeValue");
		chbQI1020_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_m, "1:0").size(altoComponente, 770).callback("onchbQI1020_MChangeValue");
		chbQI1020_N=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_n, "1:0").size(altoComponente, 770).callback("onchbQI1020_NChangeValue");
		chbQI1020_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_o, "1:0").size(altoComponente, 770).callback("onchbQI1020_OChangeValue");
		chbQI1020_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_p, "1:0").size(altoComponente, 770).callback("onchbQI1020_PChangeValue");
		chbQI1020_Q=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_q, "1:0").size(altoComponente, 770).callback("onchbQI1020_QChangeValue");
		chbQI1020_R=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_r, "1:0").size(altoComponente, 770).callback("onchbQI1020_RChangeValue");
		chbQI1020_S=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_s, "1:0").size(altoComponente, 770).callback("onchbQI1020_SChangeValue");
		chbQI1020_T=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_t, "1:0").size(altoComponente, 770).callback("onchbQI1020_TChangeValue");
		chbQI1020_U=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_u, "1:0").size(altoComponente, 770).callback("onchbQI1020_UChangeValue");
		
		chbQI1020_V=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_v, "1:0").size(altoComponente, 770).callback("onchbQI1020_VChangeValue");
		chbQI1020_W=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_w, "1:0").size(altoComponente, 770).callback("onchbQI1020_WChangeValue");
		
		chbQI1020_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1020_x, "1:0").size(altoComponente, 470).callback("onchbQI1020_XChangeValue");
		txtQI1020_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 300);
		
		lblQI1020_A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_E2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_F2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_G2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_H2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_I2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_J2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_K2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_L2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_M2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_N2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_O2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_P2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_Q2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_R2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_S2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_T2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_U2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		
		lblQI1020_V2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		lblQI1020_W2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);
		
		lblQI1020_X2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 470).text(R.string.ciseccion_10_01qi1020_sp);

		
		rgQI1020_AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_EN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_FN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_GN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_HN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_IN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_JN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_KN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_LN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_MN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_NN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_ON=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_PN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_QN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_RN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_SN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_TN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_UN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		rgQI1020_VN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1020_WN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		rgQI1020_XN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1020_so1,R.string.ciseccion_10_01qi1020_so2,R.string.ciseccion_10_01qi1020_so3).size(altoComponente+80, 300).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		
		gridPreguntas1020=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1020.addComponent(chbQI1020_A,2);
		gridPreguntas1020.addComponent(lblQI1020_A2);
		gridPreguntas1020.addComponent(rgQI1020_AN);
				
		gridPreguntas1020.addComponent(chbQI1020_B,2);
		gridPreguntas1020.addComponent(lblQI1020_B2);
		gridPreguntas1020.addComponent(rgQI1020_BN);
		
		gridPreguntas1020.addComponent(chbQI1020_C,2);
		gridPreguntas1020.addComponent(lblQI1020_C2);
		gridPreguntas1020.addComponent(rgQI1020_CN);
		
		gridPreguntas1020.addComponent(chbQI1020_D,2);
		gridPreguntas1020.addComponent(lblQI1020_D2);
		gridPreguntas1020.addComponent(rgQI1020_DN);
		
		gridPreguntas1020.addComponent(chbQI1020_E,2);
		gridPreguntas1020.addComponent(lblQI1020_E2);
		gridPreguntas1020.addComponent(rgQI1020_EN);
		
		gridPreguntas1020.addComponent(chbQI1020_F,2);
		gridPreguntas1020.addComponent(lblQI1020_F2);
		gridPreguntas1020.addComponent(rgQI1020_FN);
		
		gridPreguntas1020.addComponent(chbQI1020_G,2);
		gridPreguntas1020.addComponent(lblQI1020_G2);
		gridPreguntas1020.addComponent(rgQI1020_GN);
		
		gridPreguntas1020.addComponent(chbQI1020_H,2);
		gridPreguntas1020.addComponent(lblQI1020_H2);
		gridPreguntas1020.addComponent(rgQI1020_HN);
		
		gridPreguntas1020.addComponent(chbQI1020_I,2);
		gridPreguntas1020.addComponent(lblQI1020_I2);
		gridPreguntas1020.addComponent(rgQI1020_IN);
		
		gridPreguntas1020.addComponent(chbQI1020_J,2);
		gridPreguntas1020.addComponent(lblQI1020_J2);
		gridPreguntas1020.addComponent(rgQI1020_JN);
		
		gridPreguntas1020.addComponent(chbQI1020_K,2);
		gridPreguntas1020.addComponent(lblQI1020_K2);
		gridPreguntas1020.addComponent(rgQI1020_KN);
		
		gridPreguntas1020.addComponent(chbQI1020_L,2);
		gridPreguntas1020.addComponent(lblQI1020_L2);
		gridPreguntas1020.addComponent(rgQI1020_LN);
		
		gridPreguntas1020.addComponent(chbQI1020_M,2);
		gridPreguntas1020.addComponent(lblQI1020_M2);
		gridPreguntas1020.addComponent(rgQI1020_MN);
		
		gridPreguntas1020.addComponent(chbQI1020_N,2);
		gridPreguntas1020.addComponent(lblQI1020_N2);
		gridPreguntas1020.addComponent(rgQI1020_NN);
		
		gridPreguntas1020.addComponent(chbQI1020_O,2);
		gridPreguntas1020.addComponent(lblQI1020_O2);
		gridPreguntas1020.addComponent(rgQI1020_ON);
		
		gridPreguntas1020.addComponent(chbQI1020_P,2);
		gridPreguntas1020.addComponent(lblQI1020_P2);
		gridPreguntas1020.addComponent(rgQI1020_PN);
		
		gridPreguntas1020.addComponent(chbQI1020_Q,2);
		gridPreguntas1020.addComponent(lblQI1020_Q2);
		gridPreguntas1020.addComponent(rgQI1020_QN);
		
		gridPreguntas1020.addComponent(chbQI1020_R,2);
		gridPreguntas1020.addComponent(lblQI1020_R2);
		gridPreguntas1020.addComponent(rgQI1020_RN);
		
		gridPreguntas1020.addComponent(chbQI1020_S,2);
		gridPreguntas1020.addComponent(lblQI1020_S2);
		gridPreguntas1020.addComponent(rgQI1020_SN);
		
		gridPreguntas1020.addComponent(chbQI1020_T,2);
		gridPreguntas1020.addComponent(lblQI1020_T2);
		gridPreguntas1020.addComponent(rgQI1020_TN);
		
		gridPreguntas1020.addComponent(chbQI1020_U,2);
		gridPreguntas1020.addComponent(lblQI1020_U2);
		gridPreguntas1020.addComponent(rgQI1020_UN);
		
		gridPreguntas1020.addComponent(chbQI1020_V,2);
		gridPreguntas1020.addComponent(lblQI1020_V2);
		gridPreguntas1020.addComponent(rgQI1020_VN);
		
		gridPreguntas1020.addComponent(chbQI1020_W,2);
		gridPreguntas1020.addComponent(lblQI1020_W2);
		gridPreguntas1020.addComponent(rgQI1020_WN);
		
		gridPreguntas1020.addComponent(chbQI1020_X);
		gridPreguntas1020.addComponent(txtQI1020_XI);
		gridPreguntas1020.addComponent(lblQI1020_X2);
		gridPreguntas1020.addComponent(rgQI1020_XN);
		
    }
  
    @Override 
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1016,rgQI1016);
		q2 = createQuestionSection(lblpregunta1017,gdViolencia.component());
		q3 = createQuestionSection(lblpregunta1019,rgQI1019);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1020,gridPreguntas1020.component());
		
//		LinearLayout ly1020 = new LinearLayout(getActivity());
//		ly1020.addView(chbQI1020_X);
//		ly1020.addView(txtQI1020_XI);
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1020,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,ly1020);
		
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
 
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi1016!=null)
			individual.qi1016=individual.getConvertQi1016(individual.qi1016);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1013=individual.qi1013a==null?0:individual.qi1013a;
		Integer in1013aa=individual.qi1013aa==null?0:individual.qi1013aa;
		Integer in1013b=individual.qi1013b==null?0:individual.qi1013b;
		
    	Integer in201=individualS1.qi201==null?0:individualS1.qi201;
    	Integer in226=individualS1.qi226==null?0:individualS1.qi226;
    	Integer in230=individualS1.qi230==null?0:individualS1.qi230;
    	Integer in208=individualS1.qi208==null?0:individualS1.qi208;
    	Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
		
		if (in1013==1 || in1013aa==1 ||in1013b==1 ) {
			if (infiltro1014>1) {
				if (Util.esVacio(individual.qi1016)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1016");
					view = rgQI1016;
					error = true;
					return false;
				}
				if (individual.qi1016==96) {
					if (Util.esVacio(individual.qi1016x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1016X");
						view = txtQI1016X;
						error = true;
						return false;
					}
				}
			}
			if(!ContadorPregunta1014()){
				if (Util.esVacio(individual.qi1017)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1017");
					view = txtQI1017;
					error = true;
					return false;
				}
				if (Util.esMenor(individual.qi1017,0) || Util.esMayor(individual.qi1017, 50)) { 
					mensaje = "Valor fuera de rango pregunta 1017"; 
					view = txtQI1017; 
					error = true; 
					return false; 
				}	
			}
			
		}
		
		if (in201==1 || in226==1 || in230==1 || in208>0) {
			if (Util.esVacio(individual.qi1019)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1019");
				view = rgQI1019;
				error = true;
				return false;
			}
			if (!Util.esDiferente(individual.qi1019,1)) {
				if (!verificarCheck()) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1020_A");
					view = chbQI1020_A;
					error = true;
					return false;
				}
				
				if (chbQI1020_A.isChecked()) {
					if (Util.esVacio(individual.qi1020_an)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_AN");
						view = rgQI1020_AN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_B.isChecked()) {
					if (Util.esVacio(individual.qi1020_bn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_BN");
						view = rgQI1020_BN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_C.isChecked()) {
					if (Util.esVacio(individual.qi1020_cn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_CN");
						view = rgQI1020_CN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_D.isChecked()) {
					if (Util.esVacio(individual.qi1020_dn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_DN");
						view = rgQI1020_DN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_E.isChecked()) {
					if (Util.esVacio(individual.qi1020_en)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_EN");
						view = rgQI1020_EN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_F.isChecked()) {
					if (Util.esVacio(individual.qi1020_fn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_FN");
						view = rgQI1020_FN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_G.isChecked()) {
					if (Util.esVacio(individual.qi1020_gn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_GN");
						view = rgQI1020_GN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_H.isChecked()) {
					if (Util.esVacio(individual.qi1020_hn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_HN");
						view = rgQI1020_HN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_I.isChecked()) {
					if (Util.esVacio(individual.qi1020_in)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_IN");
						view = rgQI1020_IN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_J.isChecked()) {
					if (Util.esVacio(individual.qi1020_jn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_JN");
						view = rgQI1020_JN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_K.isChecked()) {
					if (Util.esVacio(individual.qi1020_kn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_KN");
						view = rgQI1020_KN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_L.isChecked()) {
					if (Util.esVacio(individual.qi1020_ln)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_LN");
						view = rgQI1020_LN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_M.isChecked()) {
					if (Util.esVacio(individual.qi1020_mn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_MN");
						view = rgQI1020_MN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_N.isChecked()) {
					if (Util.esVacio(individual.qi1020_nn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_NN");
						view = rgQI1020_NN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_O.isChecked()) {
					if (Util.esVacio(individual.qi1020_on)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_ON");
						view = rgQI1020_ON;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_P.isChecked()) {
					if (Util.esVacio(individual.qi1020_pn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_PN");
						view = rgQI1020_PN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_Q.isChecked()) {
					if (Util.esVacio(individual.qi1020_qn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_QN");
						view = rgQI1020_QN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_R.isChecked()) {
					if (Util.esVacio(individual.qi1020_rn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_RN");
						view = rgQI1020_RN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_S.isChecked()) {
					if (Util.esVacio(individual.qi1020_sn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_SN");
						view = rgQI1020_SN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_T.isChecked()) {
					if (Util.esVacio(individual.qi1020_tn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_TN");
						view = rgQI1020_TN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_U.isChecked()) {
					if (Util.esVacio(individual.qi1020_un)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_UN");
						view = rgQI1020_UN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_V.isChecked()) {
					if (Util.esVacio(individual.qi1020_vn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_VN");
						view = rgQI1020_VN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_W.isChecked()) {
					if (Util.esVacio(individual.qi1020_wn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_WN");
						view = rgQI1020_WN;
						error = true;
						return false;
					}
				}
				
				if (chbQI1020_X.isChecked()) {
					if (Util.esVacio(individual.qi1020_xi)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_XI");
						view = txtQI1020_XI;
						error = true;
						return false;
					}
					if (Util.esVacio(individual.qi1020_xn)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1020_XN");
						view = rgQI1020_XN;
						error = true;
						return false;
					}
				}
			}
		}
		
		return true;
    }
    
    @Override 
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS1 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1);
		
    	if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
    	
    	App.getInstance().getCiseccion_10_01().qi1013a=individual.qi1013a;
    	App.getInstance().getCiseccion_10_01().qi1013aa=individual.qi1013aa;
    	App.getInstance().getCiseccion_10_01().qi1013b=individual.qi1013b;
    	
    	/*String in1013=individual.qi1013==null?"cero":individual.qi1013.toString();
    	String in1013aa=individual.qi1013aa==null?"cero":individual.qi1013aa.toString();
    	String in1013b=individual.qi1013b==null?"cero":individual.qi1013b.toString();
    	ToastMessage.msgBox(this.getActivity(), in1013+"-"+in1013aa+"-"+in1013b , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    	*/
    	
		App.getInstance().getCiseccion_10_01().qi1019=individual.qi1019;
    	App.getInstance().getPersonaCuestionarioIndividual().qi201=individualS1.qi201;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individualS1.qi226;
    	App.getInstance().getPersonaCuestionarioIndividual().qi230=individualS1.qi230;
		
		if(individual.qi1016!=null)
			individual.qi1016=individual.setConvertQi1016(individual.qi1016);

		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta1013() {
    	Integer in1013=individual.qi1013a==null?0:individual.qi1013a;
    	Integer in1013aa=individual.qi1013aa==null?0:individual.qi1013aa;
    	Integer in1013b=individual.qi1013b==null?0:individual.qi1013b;
    	
    	Integer in201=individualS1.qi201==null?0:individualS1.qi201;
    	Integer in226=individualS1.qi226==null?0:individualS1.qi226;
    	Integer in230=individualS1.qi230==null?0:individualS1.qi230;
    	Integer in208=individualS1.qi208==null?0:individualS1.qi208;
    	Integer infiltro1014=App.getInstance().getCiseccion_10_01().filtro1014==null?0:App.getInstance().getCiseccion_10_01().filtro1014;
    	
    	if (in1013==2 || in1013==8 || in1013aa==2 || in1013aa==8 || in1013b==2 || in1013b==8){
    		if (in201==1 || in226==1 || in230==1 || in208>0) {
    			Log.e("a","1");
        		Util.cleanAndLockView(getActivity(),rgQI1016,txtQI1017);
        		q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
        		Util.lockView(getActivity(),false,rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,chbQI1020_V,chbQI1020_W,chbQI1020_X);
        		q3.setVisibility(View.VISIBLE);
        		q4.setVisibility(View.VISIBLE);	
        		onrgQI1019ChangeValue();
    		}
    		else{
    			Log.e("a","2");
        		Util.cleanAndLockView(getActivity(),rgQI1016,txtQI1017,rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U, chbQI1020_V, chbQI1020_W,chbQI1020_X);
        		q0.setVisibility(View.GONE);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
        		q3.setVisibility(View.GONE);
        		q4.setVisibility(View.GONE);	
        		
        		
    		}
    		
    	}
    	else{
    		
    		
    		if (infiltro1014>1 ) {
    			if((in201==1 || in226==1 || in230==1 || in208>0)) {
            		Log.e("a","3");
            		Util.lockView(getActivity(),false,rgQI1016,txtQI1017,rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,chbQI1020_V,chbQI1020_W,chbQI1020_X);
            		q0.setVisibility(View.VISIBLE);
            		q1.setVisibility(View.VISIBLE);
            		q2.setVisibility(View.VISIBLE);
            		q3.setVisibility(View.VISIBLE);
            		q4.setVisibility(View.VISIBLE);	
            		onrgQI1019ChangeValue();
            		ValidarPregunta1014();
            	}
    			else{
    				Log.e("a","4");
            		Util.lockView(getActivity(),false,rgQI1016,txtQI1017);
            		q0.setVisibility(View.VISIBLE);
            		q1.setVisibility(View.VISIBLE);
            		q2.setVisibility(View.VISIBLE);
            		Util.cleanAndLockView(getActivity(),rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U, chbQI1020_V, chbQI1020_W,chbQI1020_X);
            		q3.setVisibility(View.GONE);
            		q4.setVisibility(View.GONE);
            		ValidarPregunta1014();
    			}
			}
    		else{
    			
    			if((in201==1 || in226==1 || in230==1 || in208>0)) {
    				Log.e("a","5");
            		Util.cleanAndLockView(getActivity(),rgQI1016);
            		q0.setVisibility(View.VISIBLE);
            		q1.setVisibility(View.GONE);
            		Util.lockView(getActivity(),false,txtQI1017,rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,chbQI1020_V, chbQI1020_W,chbQI1020_X);
            		q2.setVisibility(View.VISIBLE);
            		q3.setVisibility(View.VISIBLE);
            		q4.setVisibility(View.VISIBLE);
            		ValidarPregunta1014();
            		onrgQI1019ChangeValue();
    			}
    			else{
    				Log.e("a","6");
            		Util.cleanAndLockView(getActivity(),rgQI1016,rgQI1019,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U, chbQI1020_V, chbQI1020_W,chbQI1020_X);
            		q0.setVisibility(View.VISIBLE);
            		q1.setVisibility(View.GONE);
            		Util.lockView(getActivity(),false,txtQI1017);
            		q2.setVisibility(View.VISIBLE);
            		q3.setVisibility(View.GONE);
            		q4.setVisibility(View.GONE);
            		ValidarPregunta1014();
            		            		
            		
    			}
    		}         
    	}
    }
    
  
	public void ValidarPregunta1014(){
		if(ContadorPregunta1014()==true){
			Log.e(""," todos son iguales");
			Util.cleanAndLockView(getActivity(), txtQI1017);
			q2.setVisibility(View.GONE);
		}
		else{
			Log.e(""," diferentes ");
			Util.lockView(getActivity(), false,txtQI1017);
			q2.setVisibility(View.VISIBLE);
		}
	}
    
    public boolean ContadorPregunta1014(){
    	Integer p1014a=0;
    	Integer p1014b=0;
    	Integer p1014c=0;
    	Integer p1014d=0;
    	Integer p1014e=0;
    	Integer p1014f=0;
    	Integer p1014g=0;
    	Integer p1014h=0;
    	Integer p1014i=0;
    	Integer p1014j=0;
    	Integer p1014k=0;
    	Integer p1014l=0;
    	Integer p1014m=0;
    	Integer p1014n=0;
    	Integer p1014o=0;
    	Integer p1014p=0;
    	Integer p1014q=0;
    	Integer p1014r=0;
    	Integer p1014s=0;
    	Integer p1014t=0;
    	Integer p1014u=0;
    	Integer p1014v=0;
    	Integer p1014x=0;
    	Integer total=0;
    	Integer totaln=0;
    	if(individual.qi1014_a!=null && individual.qi1014_a==1)p1014a=1;
    	if(individual.qi1014_b!=null && individual.qi1014_b==1)p1014b=1;
    	if(individual.qi1014_c!=null && individual.qi1014_c==1)p1014c=1;
    	if(individual.qi1014_d!=null && individual.qi1014_d==1)p1014d=1;
    	if(individual.qi1014_e!=null && individual.qi1014_e==1)p1014e=1;
    	if(individual.qi1014_f!=null && individual.qi1014_f==1)p1014f=1;
    	if(individual.qi1014_g!=null && individual.qi1014_g==1)p1014g=1;
    	if(individual.qi1014_h!=null && individual.qi1014_h==1)p1014h=1;
    	if(individual.qi1014_i!=null && individual.qi1014_i==1)p1014i=1;
    	if(individual.qi1014_j!=null && individual.qi1014_j==1)p1014j=1;
    	if(individual.qi1014_k!=null && individual.qi1014_k==1)p1014k=1;
    	if(individual.qi1014_l!=null && individual.qi1014_l==1)p1014l=1;
    	if(individual.qi1014_m!=null && individual.qi1014_m==1)p1014m=1;
    	if(individual.qi1014_n!=null && individual.qi1014_n==1)p1014n=1;
    	if(individual.qi1014_o!=null && individual.qi1014_o==1)p1014o=1;
    	if(individual.qi1014_p!=null && individual.qi1014_p==1)p1014p=1;
    	if(individual.qi1014_q!=null && individual.qi1014_q==1)p1014q=1;
    	if(individual.qi1014_r!=null && individual.qi1014_r==1)p1014r=1;
    	if(individual.qi1014_s!=null && individual.qi1014_s==1)p1014s=1;
    	if(individual.qi1014_t!=null && individual.qi1014_t==1)p1014t=1;
    	if(individual.qi1014_u!=null && individual.qi1014_u==1)p1014u=1;
    	if(individual.qi1014_v!=null && individual.qi1014_v==1)p1014v=1;
    	if(individual.qi1014_x!=null && individual.qi1014_x==1)p1014x=1;
    	total=p1014a+p1014b+p1014c+p1014d+	p1014e+	p1014f+	p1014g+	p1014h+	p1014i+	p1014j+	p1014k+	p1014l+	p1014m+	p1014n+	p1014o+	p1014p+	p1014q+	p1014r+	p1014s+	p1014t+	p1014u+	p1014v+	p1014x;
    	Log.e("","TOTAl: "+total);
    	  p1014a=0; p1014b=0; p1014c=0; p1014d=0; p1014e=0; p1014f=0; p1014g=0; p1014h=0;
          p1014i=0; p1014j=0; p1014k=0; p1014l=0; p1014m=0; p1014n=0; p1014o=0; p1014p=0;
          p1014q=0; p1014r=0; p1014s=0; p1014t=0; p1014u=0; p1014v=0; p1014x=0;
        if(individual.qi1014_an!=null && individual.qi1014_an==3)p1014a=1;
        if(individual.qi1014_bn!=null && individual.qi1014_bn==3)p1014b=1;
        if(individual.qi1014_cn!=null && individual.qi1014_cn==3)p1014c=1;
        if(individual.qi1014_dn!=null && individual.qi1014_dn==3)p1014d=1;
        if(individual.qi1014_en!=null && individual.qi1014_en==3)p1014e=1;
        if(individual.qi1014_fn!=null && individual.qi1014_fn==3)p1014f=1;
        if(individual.qi1014_gn!=null && individual.qi1014_gn==3)p1014g=1;
        if(individual.qi1014_hn!=null && individual.qi1014_hn==3)p1014h=1;
        if(individual.qi1014_in!=null && individual.qi1014_in==3)p1014i=1;
        if(individual.qi1014_jn!=null && individual.qi1014_jn==3)p1014j=1;
        if(individual.qi1014_kn!=null && individual.qi1014_kn==3)p1014k=1;
        if(individual.qi1014_ln!=null && individual.qi1014_ln==3)p1014l=1;
        if(individual.qi1014_mn!=null && individual.qi1014_mn==3)p1014m=1;
        if(individual.qi1014_nn!=null && individual.qi1014_nn==3)p1014n=1;
        if(individual.qi1014_on!=null && individual.qi1014_on==3)p1014o=1;
        if(individual.qi1014_pn!=null && individual.qi1014_pn==3)p1014p=1;
        if(individual.qi1014_qn!=null && individual.qi1014_qn==3)p1014q=1;
        if(individual.qi1014_rn!=null && individual.qi1014_rn==3)p1014r=1;
        if(individual.qi1014_sn!=null && individual.qi1014_sn==3)p1014s=1;
        if(individual.qi1014_tn!=null && individual.qi1014_tn==3)p1014t=1;
        if(individual.qi1014_un!=null && individual.qi1014_un==3)p1014u=1;
        if(individual.qi1014_vn!=null && individual.qi1014_vn==3)p1014v=1;
        if(individual.qi1014_xn!=null && individual.qi1014_xn==3)p1014x=1;
         totaln=p1014a+p1014b+p1014c+p1014d+	p1014e+	p1014f+	p1014g+	p1014h+	p1014i+	p1014j+	p1014k+	p1014l+	p1014m+	p1014n+	p1014o+	p1014p+	p1014q+	p1014r+	p1014s+	p1014t+	p1014u+	p1014v+	p1014x;
         Log.e("","TOTAlN: "+totaln);
    	return total==totaln;
    }
    private void inicio() {
    	
    	validarPregunta1013();
    	ValidarsiesSupervisora();
    	esconderPregunta1014();
    	txtCabecera.requestFocus();
    }
    

    public void onrgQI1019ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI1019.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,chbQI1020_V, chbQI1020_W,chbQI1020_X);
			q4.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,chbQI1020_A,chbQI1020_B,chbQI1020_C,chbQI1020_D,chbQI1020_E,chbQI1020_F,chbQI1020_G,chbQI1020_H,chbQI1020_I,chbQI1020_J,chbQI1020_K,chbQI1020_L,chbQI1020_M,chbQI1020_N,chbQI1020_O,chbQI1020_P,chbQI1020_Q,chbQI1020_R,chbQI1020_S,chbQI1020_T,chbQI1020_U,chbQI1020_V, chbQI1020_W,chbQI1020_X);
			q4.setVisibility(View.VISIBLE);
			ValidarCheckPregunta1020();
			chbQI1020_A.requestFocus();
		}
    }
    public void esconderPregunta1014() {
    	Integer int1014a = individual.qi1014_a==null?0:individual.qi1014_a;
    	Integer int1014b = individual.qi1014_b==null?0:individual.qi1014_b;
    	Integer int1014c = individual.qi1014_c==null?0:individual.qi1014_c;
    	Integer int1014d = individual.qi1014_d==null?0:individual.qi1014_d;
    	Integer int1014e = individual.qi1014_e==null?0:individual.qi1014_e;
    	Integer int1014f = individual.qi1014_f==null?0:individual.qi1014_f;
    	Integer int1014g = individual.qi1014_g==null?0:individual.qi1014_g;
    	Integer int1014h = individual.qi1014_h==null?0:individual.qi1014_h;
    	Integer int1014i = individual.qi1014_i==null?0:individual.qi1014_i;
    	Integer int1014j = individual.qi1014_j==null?0:individual.qi1014_j;
    	Integer int1014k = individual.qi1014_k==null?0:individual.qi1014_k;
    	Integer int1014l = individual.qi1014_l==null?0:individual.qi1014_l;
    	Integer int1014m = individual.qi1014_m==null?0:individual.qi1014_m;
    	Integer int1014n = individual.qi1014_n==null?0:individual.qi1014_n;
    	Integer int1014o = individual.qi1014_o==null?0:individual.qi1014_o;
    	Integer int1014p = individual.qi1014_p==null?0:individual.qi1014_p;
    	Integer int1014q = individual.qi1014_q==null?0:individual.qi1014_q;
    	Integer int1014r = individual.qi1014_r==null?0:individual.qi1014_r;
    	Integer int1014s = individual.qi1014_s==null?0:individual.qi1014_s;
    	Integer int1014t = individual.qi1014_t==null?0:individual.qi1014_t;
    	Integer int1014u = individual.qi1014_u==null?0:individual.qi1014_u;
    	Integer int1014v = individual.qi1014_v==null?0:individual.qi1014_v;
    	Integer int1014x = individual.qi1014_x==null?0:individual.qi1014_x;
        if (int1014a!=1)
        	rgQI1016.lockButtons(true,0);
        if (int1014b!=1)
        	rgQI1016.lockButtons(true,1);
        if (int1014c!=1)
        	rgQI1016.lockButtons(true,2);
        if (int1014d!=1)
        	rgQI1016.lockButtons(true,3);
        if (int1014e!=1)
        	rgQI1016.lockButtons(true,4);
        if (int1014f!=1)
        	rgQI1016.lockButtons(true,5);
        if (int1014g!=1)
        	rgQI1016.lockButtons(true,6);
        if (int1014h!=1)
        	rgQI1016.lockButtons(true,7);
        if (int1014i!=1)
        	rgQI1016.lockButtons(true,8);
        if (int1014j!=1)
        	rgQI1016.lockButtons(true,9);
        if (int1014k!=1)
        	rgQI1016.lockButtons(true,10);
        if (int1014l!=1)
        	rgQI1016.lockButtons(true,11);
        if (int1014m!=1)
        	rgQI1016.lockButtons(true,12);
        if (int1014n!=1)
        	rgQI1016.lockButtons(true,13);
        if (int1014o!=1)
        	rgQI1016.lockButtons(true,14);
        if (int1014p!=1)
        	rgQI1016.lockButtons(true,15);
        if (int1014q!=1)
        	rgQI1016.lockButtons(true,16);
        if (int1014r!=1)
        	rgQI1016.lockButtons(true,17);
        if (int1014s!=1)
        	rgQI1016.lockButtons(true,18);
        if (int1014t!=1)
        	rgQI1016.lockButtons(true,19);
        if (int1014u!=1)
        	rgQI1016.lockButtons(true,20);
        if (int1014v!=1)
        	rgQI1016.lockButtons(true,21);
        if (int1014x!=1)
        	rgQI1016.lockButtons(true,22);
    }
    
    public void ValidarCheckPregunta1020() {
    	onchbQI1020_AChangeValue();
		onchbQI1020_BChangeValue();
		onchbQI1020_CChangeValue();
		onchbQI1020_DChangeValue();
		onchbQI1020_EChangeValue();
		onchbQI1020_FChangeValue();
		onchbQI1020_GChangeValue();
		onchbQI1020_HChangeValue();
		onchbQI1020_IChangeValue();
		onchbQI1020_JChangeValue();
		onchbQI1020_KChangeValue();
		onchbQI1020_LChangeValue();
		onchbQI1020_MChangeValue();
		onchbQI1020_NChangeValue();
		onchbQI1020_OChangeValue();
		onchbQI1020_PChangeValue();
		onchbQI1020_QChangeValue();
		onchbQI1020_RChangeValue();
		onchbQI1020_SChangeValue();
		onchbQI1020_TChangeValue();
		onchbQI1020_UChangeValue();
		
		onchbQI1020_VChangeValue();
		onchbQI1020_WChangeValue();
		
		onchbQI1020_XChangeValue();		
	}
    
    public void onchbQI1020_AChangeValue() {
    	if (chbQI1020_A.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_A2,rgQI1020_AN);
			lblQI1020_A2.setVisibility(View.VISIBLE);
			rgQI1020_AN.setVisibility(View.VISIBLE);
			rgQI1020_AN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_A2, rgQI1020_AN);
			lblQI1020_A2.setVisibility(View.GONE);
			rgQI1020_AN.setVisibility(View.GONE);
			chbQI1020_B.requestFocus();
		}
    }
    
    public void onchbQI1020_BChangeValue() {
    	if (chbQI1020_B.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_B2,rgQI1020_BN);
			lblQI1020_B2.setVisibility(View.VISIBLE);
			rgQI1020_BN.setVisibility(View.VISIBLE);
			rgQI1020_BN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_B2, rgQI1020_BN);
			lblQI1020_B2.setVisibility(View.GONE);
			rgQI1020_BN.setVisibility(View.GONE);
			chbQI1020_C.requestFocus();
		}
    }
    
    public void onchbQI1020_CChangeValue() {
    	if (chbQI1020_C.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_C2,rgQI1020_CN);
			lblQI1020_C2.setVisibility(View.VISIBLE);
			rgQI1020_CN.setVisibility(View.VISIBLE);
			rgQI1020_CN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_C2, rgQI1020_CN);
			lblQI1020_C2.setVisibility(View.GONE);
			rgQI1020_CN.setVisibility(View.GONE);
			chbQI1020_D.requestFocus();
		}
    }
    
    public void onchbQI1020_DChangeValue() {
    	if (chbQI1020_D.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_D2,rgQI1020_DN);
			lblQI1020_D2.setVisibility(View.VISIBLE);
			rgQI1020_DN.setVisibility(View.VISIBLE);
			rgQI1020_DN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_D2, rgQI1020_DN);
			lblQI1020_D2.setVisibility(View.GONE);
			rgQI1020_DN.setVisibility(View.GONE);
			chbQI1020_E.requestFocus();
		}
    }
    
    public void onchbQI1020_EChangeValue() {
    	if (chbQI1020_E.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_E2,rgQI1020_EN);
			lblQI1020_E2.setVisibility(View.VISIBLE);
			rgQI1020_EN.setVisibility(View.VISIBLE);
			rgQI1020_EN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_E2, rgQI1020_EN);
			lblQI1020_E2.setVisibility(View.GONE);
			rgQI1020_EN.setVisibility(View.GONE);
			chbQI1020_F.requestFocus();
		}
    }
    
    public void onchbQI1020_FChangeValue() {
    	if (chbQI1020_F.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_F2,rgQI1020_FN);
			lblQI1020_F2.setVisibility(View.VISIBLE);
			rgQI1020_FN.setVisibility(View.VISIBLE);
			rgQI1020_FN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_F2, rgQI1020_FN);
			lblQI1020_F2.setVisibility(View.GONE);
			rgQI1020_FN.setVisibility(View.GONE);
			chbQI1020_G.requestFocus();
		}
    }
    
    public void onchbQI1020_GChangeValue() {
    	if (chbQI1020_G.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_G2,rgQI1020_GN);
			lblQI1020_G2.setVisibility(View.VISIBLE);
			rgQI1020_GN.setVisibility(View.VISIBLE);
			rgQI1020_GN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_G2, rgQI1020_GN);
			lblQI1020_G2.setVisibility(View.GONE);
			rgQI1020_GN.setVisibility(View.GONE);
			chbQI1020_H.requestFocus();
		}
    }
    
    public void onchbQI1020_HChangeValue() {
    	if (chbQI1020_H.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_H2,rgQI1020_HN);
			lblQI1020_H2.setVisibility(View.VISIBLE);
			rgQI1020_HN.setVisibility(View.VISIBLE);
			rgQI1020_HN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_H2, rgQI1020_HN);
			lblQI1020_H2.setVisibility(View.GONE);
			rgQI1020_HN.setVisibility(View.GONE);
			chbQI1020_I.requestFocus();
		}
    }
    
    public void onchbQI1020_IChangeValue() {
    	if (chbQI1020_I.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_I2,rgQI1020_IN);
			lblQI1020_I2.setVisibility(View.VISIBLE);
			rgQI1020_IN.setVisibility(View.VISIBLE);
			rgQI1020_IN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_I2, rgQI1020_IN);
			lblQI1020_I2.setVisibility(View.GONE);
			rgQI1020_IN.setVisibility(View.GONE);
			chbQI1020_J.requestFocus();
		}
    }
    
    public void onchbQI1020_JChangeValue() {
    	if (chbQI1020_J.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_F2,rgQI1020_JN);
			lblQI1020_J2.setVisibility(View.VISIBLE);
			rgQI1020_JN.setVisibility(View.VISIBLE);
			rgQI1020_JN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_J2, rgQI1020_JN);
			lblQI1020_J2.setVisibility(View.GONE);
			rgQI1020_JN.setVisibility(View.GONE);
			chbQI1020_K.requestFocus();
		}
    }
    
    public void onchbQI1020_KChangeValue() {
    	if (chbQI1020_K.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_F2,rgQI1020_KN);
			lblQI1020_K2.setVisibility(View.VISIBLE);
			rgQI1020_KN.setVisibility(View.VISIBLE);
			rgQI1020_KN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_K2, rgQI1020_KN);
			lblQI1020_K2.setVisibility(View.GONE);
			rgQI1020_KN.setVisibility(View.GONE);
			chbQI1020_L.requestFocus();
		}
    }
    
    public void onchbQI1020_LChangeValue() {
    	if (chbQI1020_L.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_L2,rgQI1020_LN);
			lblQI1020_L2.setVisibility(View.VISIBLE);
			rgQI1020_LN.setVisibility(View.VISIBLE);
			rgQI1020_LN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_L2, rgQI1020_LN);
			lblQI1020_L2.setVisibility(View.GONE);
			rgQI1020_LN.setVisibility(View.GONE);
			chbQI1020_M.requestFocus();
		}
    }
    
    public void onchbQI1020_MChangeValue() {
    	if (chbQI1020_M.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_M2,rgQI1020_MN);
			lblQI1020_M2.setVisibility(View.VISIBLE);
			rgQI1020_MN.setVisibility(View.VISIBLE);
			rgQI1020_MN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_M2, rgQI1020_MN);
			lblQI1020_M2.setVisibility(View.GONE);
			rgQI1020_MN.setVisibility(View.GONE);
			chbQI1020_N.requestFocus();
		}
    }
    
    public void onchbQI1020_NChangeValue() {
    	if (chbQI1020_N.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_N2,rgQI1020_NN);
			lblQI1020_N2.setVisibility(View.VISIBLE);
			rgQI1020_NN.setVisibility(View.VISIBLE);
			rgQI1020_NN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_N2, rgQI1020_NN);
			lblQI1020_N2.setVisibility(View.GONE);
			rgQI1020_NN.setVisibility(View.GONE);
			chbQI1020_O.requestFocus();
		}
    }
    
    public void onchbQI1020_OChangeValue() {
    	if (chbQI1020_O.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_O2,rgQI1020_ON);
			lblQI1020_O2.setVisibility(View.VISIBLE);
			rgQI1020_ON.setVisibility(View.VISIBLE);
			rgQI1020_ON.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_O2, rgQI1020_ON);
			lblQI1020_O2.setVisibility(View.GONE);
			rgQI1020_ON.setVisibility(View.GONE);
			chbQI1020_P.requestFocus();
		}
    }
    
    public void onchbQI1020_PChangeValue() {
    	if (chbQI1020_P.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_P2,rgQI1020_PN);
			lblQI1020_P2.setVisibility(View.VISIBLE);
			rgQI1020_PN.setVisibility(View.VISIBLE);
			rgQI1020_PN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_P2, rgQI1020_PN);
			lblQI1020_P2.setVisibility(View.GONE);
			rgQI1020_PN.setVisibility(View.GONE);
			chbQI1020_Q.requestFocus();
		}
    }
    
    public void onchbQI1020_QChangeValue() {
    	if (chbQI1020_Q.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_Q2,rgQI1020_QN);
			lblQI1020_Q2.setVisibility(View.VISIBLE);
			rgQI1020_QN.setVisibility(View.VISIBLE);
			rgQI1020_QN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_Q2, rgQI1020_QN);
			lblQI1020_Q2.setVisibility(View.GONE);
			rgQI1020_QN.setVisibility(View.GONE);
			chbQI1020_R.requestFocus();
		}
    }
    
    public void onchbQI1020_RChangeValue() {
    	if (chbQI1020_R.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_R2,rgQI1020_RN);
			lblQI1020_R2.setVisibility(View.VISIBLE);
			rgQI1020_RN.setVisibility(View.VISIBLE);
			rgQI1020_RN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_R2, rgQI1020_RN);
			lblQI1020_R2.setVisibility(View.GONE);
			rgQI1020_RN.setVisibility(View.GONE);
			chbQI1020_S.requestFocus();
		}
    }
    
    public void onchbQI1020_SChangeValue() {
    	if (chbQI1020_S.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_S2,rgQI1020_SN);
			lblQI1020_S2.setVisibility(View.VISIBLE);
			rgQI1020_SN.setVisibility(View.VISIBLE);
			rgQI1020_SN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_S2, rgQI1020_SN);
			lblQI1020_S2.setVisibility(View.GONE);
			rgQI1020_SN.setVisibility(View.GONE);
			chbQI1020_T.requestFocus();
		}
    }
    
    public void onchbQI1020_TChangeValue() {
    	if (chbQI1020_T.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_T2,rgQI1020_TN);
			lblQI1020_T2.setVisibility(View.VISIBLE);
			rgQI1020_TN.setVisibility(View.VISIBLE);
			rgQI1020_TN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_T2, rgQI1020_TN);
			lblQI1020_T2.setVisibility(View.GONE);
			rgQI1020_TN.setVisibility(View.GONE);
			chbQI1020_U.requestFocus();
		}
    }
    
    public void onchbQI1020_UChangeValue() {
    	if (chbQI1020_U.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_U2,rgQI1020_UN);
			lblQI1020_U2.setVisibility(View.VISIBLE);
			rgQI1020_UN.setVisibility(View.VISIBLE);
			rgQI1020_UN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_U2, rgQI1020_UN);
			lblQI1020_U2.setVisibility(View.GONE);
			rgQI1020_UN.setVisibility(View.GONE);
			chbQI1020_V.requestFocus();
		}
    }
    
    public void onchbQI1020_VChangeValue() {
    	if (chbQI1020_V.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_V2,rgQI1020_VN);
			lblQI1020_V2.setVisibility(View.VISIBLE);
			rgQI1020_VN.setVisibility(View.VISIBLE);
			rgQI1020_VN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_V2, rgQI1020_VN);
			lblQI1020_V2.setVisibility(View.GONE);
			rgQI1020_VN.setVisibility(View.GONE);
			chbQI1020_W.requestFocus();
		}
    }
    
    public void onchbQI1020_WChangeValue() {
    	if (chbQI1020_W.isChecked() ) {
    		Util.lockView(getActivity(), false,lblQI1020_W2,rgQI1020_WN);
			lblQI1020_W2.setVisibility(View.VISIBLE);
			rgQI1020_WN.setVisibility(View.VISIBLE);
			rgQI1020_WN.requestFocus();    		
		} else {
			Util.cleanAndLockView(getActivity(),lblQI1020_W2, rgQI1020_WN);
			lblQI1020_W2.setVisibility(View.GONE);
			rgQI1020_WN.setVisibility(View.GONE);
			chbQI1020_X.requestFocus();
		}
    }
    
    public void onchbQI1020_XChangeValue() {
    	if (chbQI1020_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI1020_XI, lblQI1020_X2,rgQI1020_XN);
  			lblQI1020_X2.setVisibility(View.VISIBLE);
			rgQI1020_XN.setVisibility(View.VISIBLE);
  			txtQI1020_XI.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI1020_XI, lblQI1020_X2, rgQI1020_XN);
			lblQI1020_X2.setVisibility(View.GONE);
			rgQI1020_XN.setVisibility(View.GONE);
  		}
    }
    
    public boolean verificarCheck() {
  		if (chbQI1020_A.isChecked() || chbQI1020_B.isChecked() || chbQI1020_C.isChecked() || chbQI1020_D.isChecked() || chbQI1020_E.isChecked() || chbQI1020_F.isChecked() || chbQI1020_G.isChecked() || chbQI1020_H.isChecked() || chbQI1020_I.isChecked() || chbQI1020_J.isChecked() || chbQI1020_K.isChecked() || chbQI1020_L.isChecked() || chbQI1020_M.isChecked() || chbQI1020_N.isChecked() || chbQI1020_O.isChecked() || chbQI1020_P.isChecked() || chbQI1020_Q.isChecked() || chbQI1020_R.isChecked() || chbQI1020_S.isChecked() || chbQI1020_T.isChecked() || chbQI1020_U.isChecked() || chbQI1020_V.isChecked() || chbQI1020_W.isChecked() || chbQI1020_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1016.readOnly();
    		rgQI1019.readOnly();
    		txtQI1016X.readOnly();
    		txtQI1017.readOnly();
    		txtQI1020_XI.readOnly();
    		chbQI1020_A.readOnly();
    		chbQI1020_B.readOnly();
    		chbQI1020_C.readOnly();
    		chbQI1020_D.readOnly();
    		chbQI1020_E.readOnly();
    		chbQI1020_F.readOnly();
    		chbQI1020_G.readOnly();
    		chbQI1020_H.readOnly();
    		chbQI1020_I.readOnly();
    		chbQI1020_J.readOnly();
    		chbQI1020_K.readOnly();
    		chbQI1020_L.readOnly();
    		chbQI1020_M.readOnly();
    		chbQI1020_N.readOnly();
    		chbQI1020_O.readOnly();
    		chbQI1020_P.readOnly();
    		chbQI1020_Q.readOnly();
    		chbQI1020_R.readOnly();
    		chbQI1020_S.readOnly();
    		chbQI1020_T.readOnly();
    		chbQI1020_U.readOnly();
    		
    		chbQI1020_V.readOnly();
    		chbQI1020_W.readOnly();
    		
    		chbQI1020_X.readOnly();
    		
    		rgQI1020_AN.readOnly();
    		rgQI1020_BN.readOnly();
    		rgQI1020_CN.readOnly();
    		rgQI1020_DN.readOnly();
    		rgQI1020_EN.readOnly();
    		rgQI1020_FN.readOnly();
    		rgQI1020_GN.readOnly();
    		rgQI1020_HN.readOnly();
    		rgQI1020_IN.readOnly();
    		rgQI1020_JN.readOnly();
    		rgQI1020_KN.readOnly();
    		rgQI1020_LN.readOnly();
    		rgQI1020_MN.readOnly();
    		rgQI1020_NN.readOnly();
    		rgQI1020_ON.readOnly();
    		rgQI1020_PN.readOnly();
    		rgQI1020_QN.readOnly();
    		rgQI1020_RN.readOnly();
    		rgQI1020_SN.readOnly();
    		rgQI1020_TN.readOnly();
    		rgQI1020_UN.readOnly();
    		
    		rgQI1020_VN.readOnly();
    		rgQI1020_WN.readOnly();
    		
    		rgQI1020_XN.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi1016!=null)
			individual.qi1016=individual.getConvertQi1016(individual.qi1016);
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}