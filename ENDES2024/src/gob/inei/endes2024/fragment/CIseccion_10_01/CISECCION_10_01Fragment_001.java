package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.text.Html;
import android.text.Spanned;


public class CISECCION_10_01Fragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1000A;
	
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1001A;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1001B;
	@FieldAnnotation(orderIndex=4)
	public TextField txtQI1001B_O;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1001C;
	@FieldAnnotation(orderIndex=6)
	public TextField txtQI1001C_O;
	
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1002A;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1002B;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1002C;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1002D;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI1002E;
	
	CISECCION_10_01 individual;
	CISECCION_05_07 individualS5;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1000A_i1,lblpregunta1000A_i2,lblpregunta1000B_i1,lblpregunta1000B_i2,lblpregunta1002_i2,lblpregunta1002_i3,lblpregunta1000A_i0,lblpregunta1000B_i0,lblpregunta1001A,lblpregunta1001B,lblpregunta1001C,lblpregunta1002_ip1,lblpregunta1002_ip2;
	private LabelComponent lblblanco1,lblP1002_1,lblP1002_2,lblP1002_3,lblQI1002A,lblQI1002B,lblQI1002C,lblQI1002D,lblQI1002E;
	public TextField txtCabecera;
	LinearLayout q0,q1,q2,q3,q4,q5,q6;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5;
	GridComponent2 gridPreguntas1002A;

	public CISECCION_10_01Fragment_001() {}
	public CISECCION_10_01Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override 
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1000A","QI1002A","QI1002B","QI1002C","QI1002D","QI1002E","ID","HOGAR_ID","PERSONA_ID","QI1001A","QI1001B","QI1001B_O","QI1001C","QI1001C_O")};//
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1000A","QI1002A","QI1002B","QI1002C","QI1002D","QI1002E","QI1001A","QI1001B","QI1001B_O","QI1001C","QI1001C_O")};//
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI501","QI502","ID","HOGAR_ID","PERSONA_ID","QI505")};
		return rootView;
	}
    
    @Override 
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1000A_i0 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_10_01qi1000_i0);
		lblpregunta1000A_i1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1000_i1).negrita();
		lblpregunta1000A_i2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1000_i2);
		
		lblpregunta1000B_i0 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_10_01qi1000b_i0);
		lblpregunta1000B_i1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1000b_i1).negrita();
		lblpregunta1000B_i2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.ciseccion_10_01qi1000b_i2);
		
		lblpregunta1001A 	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1001a);
		lblpregunta1001B 	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.ciseccion_10_01qi1001b);
		lblpregunta1001C 	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.ciseccion_10_01qi1001c);
		
		lblpregunta1002_ip1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1002_ip1);
		lblpregunta1002_ip2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1002_ip2);
		lblpregunta1002_i2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_10_01qi1002_i2);
		lblpregunta1002_i3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1002_i3);
		
		rgQI1000A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1000a_1,R.string.ciseccion_10_01qi1000a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1000AChangeValue");		

		lblblanco1 = new LabelComponent(getActivity()).textSize(15).size(altoComponente+15, 450).negrita().centrar();
		lblP1002_1 = new LabelComponent(getActivity()).textSize(14).size(altoComponente+15, 110).text(R.string.ciseccion_10_01qi1002_1).centrar().negrita();
		lblP1002_2 = new LabelComponent(getActivity()).textSize(14).size(altoComponente+15, 100).text(R.string.ciseccion_10_01qi1002_2).centrar().negrita();
		lblP1002_3 = new LabelComponent(getActivity()).textSize(14).size(altoComponente+15, 100).text(R.string.ciseccion_10_01qi1002_3).centrar().negrita();

		lblQI1002A=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1002_pa);
		lblQI1002B=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1002_pb);
		lblQI1002C=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1002_pc);
		lblQI1002D=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1002_pd);
		lblQI1002E=new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1002_pe);
		
		
		rgQI1001A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1001a_1,R.string.ciseccion_10_01qi1001a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1001AChangeValue");
		rgQI1001B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1001b_1,R.string.ciseccion_10_01qi1001b_2,R.string.ciseccion_10_01qi1001b_3,R.string.ciseccion_10_01qi1001b_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI1001BChangeValue");
		//rgQI1001B.agregarEspecifique(2, txtQI1001B_O);
		
		txtQI1001B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico().hint(R.string.especifique).centrar();
		rgQI1001B.agregarEspecifique(2,txtQI1001B_O);
		rgQI1001C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1001c_1,R.string.ciseccion_10_01qi1001c_2,R.string.ciseccion_10_01qi1001c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI1001C_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico();
		rgQI1001C.agregarEspecifique(2,txtQI1001C_O);

		
		rgQI1002A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1002_o1,R.string.ciseccion_10_01qi1002_o2,R.string.ciseccion_10_01qi1002_o3).size(altoComponente+20,310).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1002B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1002_o1,R.string.ciseccion_10_01qi1002_o2,R.string.ciseccion_10_01qi1002_o3).size(altoComponente+40,310).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1002C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1002_o1,R.string.ciseccion_10_01qi1002_o2,R.string.ciseccion_10_01qi1002_o3).size(altoComponente+20,310).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1002D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1002_o1,R.string.ciseccion_10_01qi1002_o2,R.string.ciseccion_10_01qi1002_o3).size(altoComponente+20,310).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1002E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1002_o1,R.string.ciseccion_10_01qi1002_o2,R.string.ciseccion_10_01qi1002_o3).size(altoComponente+20,310).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);

		gridPreguntas1002A=new GridComponent2(this.getActivity(),App.ESTILO,4,1);
		gridPreguntas1002A.addComponent(lblblanco1);
		gridPreguntas1002A.addComponent(lblP1002_1);
		gridPreguntas1002A.addComponent(lblP1002_2);
		gridPreguntas1002A.addComponent(lblP1002_3);
		gridPreguntas1002A.addComponent(lblQI1002A);
		gridPreguntas1002A.addComponent(rgQI1002A.centrar(),3);
		gridPreguntas1002A.addComponent(lblQI1002B);
		gridPreguntas1002A.addComponent(rgQI1002B.centrar(),3);
		gridPreguntas1002A.addComponent(lblQI1002C);
		gridPreguntas1002A.addComponent(rgQI1002C.centrar(),3);
		gridPreguntas1002A.addComponent(lblQI1002D);
		gridPreguntas1002A.addComponent(rgQI1002D.centrar(),3);
		gridPreguntas1002A.addComponent(lblQI1002E);
		gridPreguntas1002A.addComponent(rgQI1002E.centrar(),3);

		
		textoNegrita();
    }
    
    @Override 
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1000A_i0,lblpregunta1000A_i1,lblpregunta1000A_i2,rgQI1000A);
		q2 = createQuestionSection(lblpregunta1000B_i0,lblpregunta1000B_i1,lblpregunta1000B_i2);
		
		q3 = createQuestionSection(lblpregunta1001A,rgQI1001A);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1001B,rgQI1001B);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1001C,rgQI1001C);
		
		q6 = createQuestionSection(lblpregunta1002_ip1,lblpregunta1002_ip2,lblpregunta1002_i2,lblpregunta1002_i3,gridPreguntas1002A.component());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		
		form.addView(q6);
		
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		App.getInstance().setCiseccion_10_01(individual);
		App.getInstance().getCiseccion_10_01().qi1000a=individual.qi1000a;
		
		if(App.getInstance().getCiseccion_10_01()!=null){
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);			
		}
		else{
			App.getInstance().setCiseccion_10_01(individual);
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);
		}
		
		
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
		Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	Integer in505=App.getInstance().getCiseccion05_07().qi505==null?0:App.getInstance().getCiseccion05_07().qi505;
    	Integer int1001a = individual.qi1001a==null?0:individual.qi1001a;
    	Integer int1001b = individual.qi1001b==null?0:individual.qi1001b;
    	Integer int1001c = individual.qi1001c==null?0:individual.qi1001c;
    	if(App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id){
	    	if (Util.esVacio(individual.qi1000a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1000A");
				view = rgQI1000A;
				error = true;
				return false;
			}
	    	
	    	if(in1000a==1){
	    		if(int1001a!=null && int1001a!=2 && (in501==3 && in502==3)){	
	    	
	    		if (Util.esVacio(individual.qi1001a)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1001A");
					view = rgQI1001A;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1001b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1001B");
				view = rgQI1001B;
				error = true;
				return false;
				}
				if(!Util.esDiferente(individual.qi1001b,3)){
					if (Util.esVacio(individual.qi1001b_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique";
						view = txtQI1001B_O;
						error = true;
						return false;
					}
				}	    		
				if (Util.esVacio(individual.qi1002a)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002A");
					view = rgQI1002A;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002B");
					view = rgQI1002B;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002c)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002C");
					view = rgQI1002C;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002d)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002D");
					view = rgQI1002D;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002e)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002E");
					view = rgQI1002E;
					error = true;
					return false;
				}
	    	}
	    }
	    	
	    	
	    	
	    	if(int1001b==4){
	    		
	    		if(int1001c==0){
	    			
					if (Util.esVacio(individual.qi1001c)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1001C");
						view = rgQI1001C;
						error = true;
						return false;
						}
						if(!Util.esDiferente(individual.qi1001c,3)){
							if (Util.esVacio(individual.qi1001c_o)) {
								mensaje = "Debe ingresar informaci\u00f3n en Especifique";
								view = txtQI1001C_O;
								error = true;
								return false;
							}
						}
				    
				  }
		    }

	    	
	    	
			if (in1000a==1 && (in501!=3 || in502!=3)) {
				if (Util.esVacio(individual.qi1002a)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002A");
					view = rgQI1002A;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002B");
					view = rgQI1002B;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002c)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002C");
					view = rgQI1002C;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002d)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002D");
					view = rgQI1002D;
					error = true;
					return false;
				}
				if (Util.esVacio(individual.qi1002e)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1002E");
					view = rgQI1002E;
					error = true;
					return false;
				}
			}
    	}
		return true;
    }
    
    @Override 
    public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
	  	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	
		App.getInstance().getCiseccion05_07().qi501=individualS5.qi501;
    	App.getInstance().getCiseccion05_07().qi502=individualS5.qi502;    	
    	
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if (App.getInstance().getCiseccion_10_01()!=null) {
			App.getInstance().getCiseccion_10_01().qi1000a=individual.qi1000a;	
		}
			
		entityToUI(individual);
		inicio();
    }
    
    public void ValidarPregunta501() {
    	Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	

    	if(in501==3 && in502==3){	
    		Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);			
    		q3.setVisibility(View.VISIBLE); 
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);    
    	}else {
    		Util.cleanAndLockView(getActivity(),rgQI1001A,rgQI1001B,rgQI1001C);  		
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    	}
    	
    		
	}
    
    
    
    public void ValidarSiesViolencia() {
    
    	Integer Violencia= App.getInstance().getQhviolen()==null?-1:App.getInstance().getQhviolen();
    	if (App.getInstance().getPersonaCuestionarioIndividual().persona_id==Violencia) {
    		Util.lockView(getActivity(), false,rgQI1000A,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		onrgQI1000AChangeValue();
		}
		else{
			Util.cleanAndLockView(getActivity(),rgQI1000A,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E);
			q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
		}
	}
    
    public void ValidaTextoPregunta1002(){
    	Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	if (in501==3 && in502==3) {
    		q6.setVisibility(View.VISIBLE);
    		lblpregunta1002_ip1.setVisibility(View.GONE);
			lblpregunta1002_ip2.setVisibility(View.VISIBLE);
		}
    	else{
			q6.setVisibility(View.GONE);
			lblpregunta1002_ip1.setVisibility(View.VISIBLE);
			lblpregunta1002_ip2.setVisibility(View.GONE);
    	}
    }
    
    private void inicio() {
    	ValidarSiesViolencia();
    	ValidarsiesSupervisora();
    	ValidaTextoPregunta1002();
		Integer int1001a = individual.qi1001a==null?0:individual.qi1001a;
		Integer int1000a = individual.qi1000a==null?0:individual.qi1000a;
		Integer int1001b = individual.qi1001b==null?0:individual.qi1001b;
		Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	
		if(in501==3 && in502==3){
			Log.e("in501", in501.toString());
			Log.e("in502", in502.toString());
			
			Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);			
    		q3.setVisibility(View.VISIBLE); 
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);    			
			if(int1000a==1){
				Log.e("in1000a", int1000a.toString());
				Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);			
	    		q3.setVisibility(View.VISIBLE); 
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
	    		q6.setVisibility(View.VISIBLE);    		
			}else if(int1000a==2)
			{
				Log.e("in1000aelse", int1000a.toString());
				Util.cleanAndLockView(getActivity(),rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);					
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);	
			}
			if(int1001a==1)				
			{
				Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001B,rgQI1001C);		
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
	    		q6.setVisibility(View.VISIBLE); 
			}else if(int1001a==2)
			{
				Util.cleanAndLockView(getActivity(),rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001B,rgQI1001C);				
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);
			}
			if(int1001b==4)				
			{
				Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001C);		
	    		q5.setVisibility(View.VISIBLE);
	    		q6.setVisibility(View.VISIBLE); 
			}else if(int1001b==1 || int1001b==2 || int1001b==3)
			{
				Util.cleanAndLockView(getActivity(),rgQI1001C);			
				q5.setVisibility(View.GONE);
			}
			
		}else{
			Log.e("in501else", in501.toString());
			Log.e("in502else", in502.toString());
			Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E);		
    		q6.setVisibility(View.VISIBLE); 
    		Util.cleanAndLockView(getActivity(),rgQI1001A,rgQI1001B,rgQI1001C);					
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			if(int1000a==1){
				Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E);		
	    		q6.setVisibility(View.VISIBLE);
			}
			else if(int1000a==2){
				Util.cleanAndLockView(getActivity(),rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);					
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);
			}		

		}
		
		


    	 			
    	txtCabecera.requestFocus();
    }
    
 
    
    public void onrgQI1000AChangeValue() {
    	
		if (MyUtil.incluyeRango(2,2,rgQI1000A.getTagSelected("").toString())) {
	    	MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001A,rgQI1001B,rgQI1001C);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E);
			q2.setVisibility(View.VISIBLE);			
			q6.setVisibility(View.VISIBLE);
			ValidarPregunta501();
			rgQI1001A.requestFocus();
			
		}
    }
    
    public void onrgQI1001AChangeValue(){
    	if (MyUtil.incluyeRango(2,2,rgQI1001A.getTagSelected("").toString())) {
	    	MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001B,rgQI1001C);					
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001B,rgQI1001C);						
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);	
			q6.setVisibility(View.VISIBLE);			
			rgQI1001B.requestFocus();
			
		}
    }   

    public void onrgQI1001BChangeValue(){
    	if (MyUtil.incluyeRango(1,3,rgQI1001B.getTagSelected("").toString())) {
	    	MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI1001C);
			q5.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1002A,rgQI1002B,rgQI1002C,rgQI1002D,rgQI1002E,rgQI1001C);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			rgQI1002A.requestFocus();			
		}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1000A.readOnly();
    		rgQI1002A.readOnly();
    		rgQI1002B.readOnly();
    		rgQI1002C.readOnly();
    		rgQI1002D.readOnly();
    		rgQI1002E.readOnly();
    		rgQI1001A.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
 		if(cuestionarioService==null){
 			cuestionarioService = CuestionarioService.getInstance(getActivity());
 		}
 		return cuestionarioService;
     }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		App.getInstance().setCiseccion_10_01(individual);
		App.getInstance().getCiseccion_10_01().qi1000a=individual.qi1000a;
		
		if(App.getInstance().getCiseccion_10_01()!=null){
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);			
		}
		else{
			App.getInstance().setCiseccion_10_01(individual);
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);
		}
		
		
		return App.INDIVIDUAL;
	}
	
	
	public void textoNegrita(){
		
  	    Spanned texto1002c = Html.fromHtml(lblQI1002C.getText()+"</br> <b>(LEA \"del hogar\" SOLO PARA ALGUNA VEZ UNIDA(O))</b>");
  	  lblQI1002C.text(R.string.ciseccion_10_01qi1002_pc);
  	  lblQI1002C.setText(texto1002c);
  	    
    }
	
}