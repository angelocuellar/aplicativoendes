package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_003 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1005A;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1005AN;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1005B;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1005BN;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1005C;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1005CN;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1005D;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1005DN;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1005E;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1005EN;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI1005F;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI1005FN;
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI1005G;
	@FieldAnnotation(orderIndex=14)
	public RadioGroupOtherField rgQI1005GN;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI1005H;
	@FieldAnnotation(orderIndex=16)
	public RadioGroupOtherField rgQI1005HN;
	@FieldAnnotation(orderIndex=17)
	public RadioGroupOtherField rgQI1005I;
	@FieldAnnotation(orderIndex=18)
	public RadioGroupOtherField rgQI1005IN;
	
	CISECCION_10_01 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1005,lblQI1005A,lblQI1005B,lblQI1005C,lblQI1005D,lblQI1005E,lblQI1005F,lblQI1005G,lblQI1005H,lblQI1005I,lblQI1005A2,lblQI1005B2,lblQI1005C2,lblQI1005D2,lblQI1005E2,lblQI1005F2,lblQI1005G2,lblQI1005H2,lblQI1005I2;
	public TextField txtCabecera;
	LinearLayout q0,q1;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	GridComponent2 gridPreguntas1005;

	public CISECCION_10_01Fragment_003() {}
	public CISECCION_10_01Fragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
	 	super.onCreate(savedInstanceState);
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1005A","QI1005AN","QI1005B","QI1005BN","QI1005C","QI1005CN","QI1005D","QI1005DN","QI1005E","QI1005EN","QI1005F","QI1005FN","QI1005G","QI1005GN","QI1005H","QI1005HN","QI1005I","QI1005IN","QI1000A","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1005A","QI1005AN","QI1005B","QI1005BN","QI1005C","QI1005CN","QI1005D","QI1005DN","QI1005E","QI1005EN","QI1005F","QI1005FN","QI1005G","QI1005GN","QI1005H","QI1005HN","QI1005I","QI1005IN")};
		return rootView;
	}
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1005 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1005);
		
		lblQI1005A =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pa);
		lblQI1005B =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pb);
		lblQI1005C =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pc);
		lblQI1005D =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pd);
		lblQI1005E =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pe);
		lblQI1005F =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pf);
		lblQI1005G =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pg);
		lblQI1005H =new LabelComponent(getActivity()).textSize(17).size(altoComponente+50, 450).text(R.string.ciseccion_10_01qi1005_ph);
		lblQI1005I =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1005_pi);
			
		rgQI1005A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005AChangeValue");
		rgQI1005B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005BChangeValue");
		rgQI1005C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005CChangeValue");
		rgQI1005D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005DChangeValue");
		rgQI1005E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005EChangeValue");
		rgQI1005F=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005FChangeValue");
		rgQI1005G=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005GChangeValue");
		rgQI1005H=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+50,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005HChangeValue");
		rgQI1005I=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1005_1,R.string.ciseccion_10_01qi1005_2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1005IChangeValue");
			
		lblQI1005A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005E2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005F2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005G2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005H2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);
		lblQI1005I2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1005_sp);

		rgQI1005AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005EN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005FN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005GN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005HN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1005IN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1004_so1,R.string.ciseccion_10_01qi1004_so2,R.string.ciseccion_10_01qi1004_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		gridPreguntas1005=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1005.addComponent(lblQI1005A);
		gridPreguntas1005.addComponent(rgQI1005A);
		gridPreguntas1005.addComponent(lblQI1005A2);
		gridPreguntas1005.addComponent(rgQI1005AN);
		
		gridPreguntas1005.addComponent(lblQI1005B);
		gridPreguntas1005.addComponent(rgQI1005B);
		gridPreguntas1005.addComponent(lblQI1005B2);
		gridPreguntas1005.addComponent(rgQI1005BN);
		
		gridPreguntas1005.addComponent(lblQI1005C);
		gridPreguntas1005.addComponent(rgQI1005C);
		gridPreguntas1005.addComponent(lblQI1005C2);
		gridPreguntas1005.addComponent(rgQI1005CN);
		
		gridPreguntas1005.addComponent(lblQI1005D);
		gridPreguntas1005.addComponent(rgQI1005D);
		gridPreguntas1005.addComponent(lblQI1005D2);
		gridPreguntas1005.addComponent(rgQI1005DN);
		
		gridPreguntas1005.addComponent(lblQI1005E);
		gridPreguntas1005.addComponent(rgQI1005E);
		gridPreguntas1005.addComponent(lblQI1005E2);
		gridPreguntas1005.addComponent(rgQI1005EN);
		
		gridPreguntas1005.addComponent(lblQI1005F);
		gridPreguntas1005.addComponent(rgQI1005F);
		gridPreguntas1005.addComponent(lblQI1005F2);
		gridPreguntas1005.addComponent(rgQI1005FN);
		
		gridPreguntas1005.addComponent(lblQI1005G);
		gridPreguntas1005.addComponent(rgQI1005G);
		gridPreguntas1005.addComponent(lblQI1005G2);
		gridPreguntas1005.addComponent(rgQI1005GN);
		
		gridPreguntas1005.addComponent(lblQI1005H);
		gridPreguntas1005.addComponent(rgQI1005H);
		gridPreguntas1005.addComponent(lblQI1005H2);
		gridPreguntas1005.addComponent(rgQI1005HN);
		
		gridPreguntas1005.addComponent(lblQI1005I);
		gridPreguntas1005.addComponent(rgQI1005I);
		gridPreguntas1005.addComponent(lblQI1005I2);
		gridPreguntas1005.addComponent(rgQI1005IN);
    }
    
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1005,gridPreguntas1005.component());

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		int cont=0;		
		if(individual.qi1005a==1)
			cont+=1;
		if(individual.qi1005b==1)
			cont+=1;
		if(individual.qi1005c==1)
			cont+=1;
		if(individual.qi1005d==1)
			cont+=1;
		if(individual.qi1005e==1)
			cont+=1;
		if(individual.qi1005f==1)
			cont+=1;
		if(individual.qi1005g==1)
			cont+=1;
		if(individual.qi1005h==1)
			cont+=1;
		if(individual.qi1005i==1)
			cont+=1;

		App.getInstance().setCiseccion_10_01(individual);
//		Log.e("App.getInstance().getCiseccion_10_01().filtro1005",""+App.getInstance().getCiseccion_10_01().filtro1005);
		App.getInstance().getCiseccion_10_01().filtro1005 = cont;
		App.getInstance().getCiseccion_10_01().qi1005a=individual.qi1005a;
		App.getInstance().getCiseccion_10_01().qi1005b=individual.qi1005b;
		App.getInstance().getCiseccion_10_01().qi1005c=individual.qi1005c;
		App.getInstance().getCiseccion_10_01().qi1005d=individual.qi1005d;
		App.getInstance().getCiseccion_10_01().qi1005e=individual.qi1005e;
		App.getInstance().getCiseccion_10_01().qi1005f=individual.qi1005f;
		App.getInstance().getCiseccion_10_01().qi1005g=individual.qi1005g;
		App.getInstance().getCiseccion_10_01().qi1005h=individual.qi1005h;
		App.getInstance().getCiseccion_10_01().qi1005i=individual.qi1005i;

		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000A=individual.qi1000a==null?0:individual.qi1000a;
		
		if (in1000A==1) {
			if (Util.esVacio(individual.qi1005a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005A");
				view = rgQI1005A;
				error = true;
				return false;
			}
			if (individual.qi1005a==1) {
				if (Util.esVacio(individual.qi1005an)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005AN");
					view = rgQI1005AN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005B");
				view = rgQI1005B;
				error = true;
				return false;
			}
			if (individual.qi1005b==1) {
				if (Util.esVacio(individual.qi1005bn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005BN");
					view = rgQI1005BN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005C");
				view = rgQI1005C;
				error = true;
				return false;
			}
			if (individual.qi1005c==1) {
				if (Util.esVacio(individual.qi1005cn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005CN");
					view = rgQI1005CN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005D");
				view = rgQI1005D;
				error = true;
				return false;
			}
			if (individual.qi1005d==1) {
				if (Util.esVacio(individual.qi1005dn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005DN");
					view = rgQI1005DN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005e)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005E");
				view = rgQI1005E;
				error = true;
				return false;
			}
			if (individual.qi1005e==1) {
				if (Util.esVacio(individual.qi1005en)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005EN");
					view = rgQI1005EN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005f)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005F");
				view = rgQI1005F;
				error = true;
				return false;
			}
			if (individual.qi1005f==1) {
				if (Util.esVacio(individual.qi1005fn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005FN");
					view = rgQI1005FN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005g)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005G");
				view = rgQI1005G;
				error = true;
				return false;
			}
			if (individual.qi1005g==1) {
				if (Util.esVacio(individual.qi1005gn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005GN");
					view = rgQI1005GN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005h)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005H");
				view = rgQI1005H;
				error = true;
				return false;
			}
			if (individual.qi1005h==1) {
				if (Util.esVacio(individual.qi1005hn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005HN");
					view = rgQI1005HN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1005i)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1005I");
				view = rgQI1005I;
				error = true;
				return false;
			}
			if (individual.qi1005i==1) {
				if (Util.esVacio(individual.qi1005in)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1005IN");
					view = rgQI1005IN;
					error = true;
					return false;
				}
			}
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		
		entityToUI(individual);
		inicio();
    }
    
    private void inicio() {
    	onrgQI1005AChangeValue();
    	onrgQI1005BChangeValue();
    	onrgQI1005CChangeValue();
    	onrgQI1005DChangeValue();
    	onrgQI1005EChangeValue();
    	onrgQI1005FChangeValue();
    	onrgQI1005GChangeValue();
    	onrgQI1005HChangeValue();
    	onrgQI1005IChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void onrgQI1005AChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005A.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005AN);
			lblQI1005A2.setVisibility(View.GONE);
			rgQI1005AN.setVisibility(View.GONE);
			rgQI1005B.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005AN);
			lblQI1005A2.setVisibility(View.VISIBLE);
			rgQI1005AN.setVisibility(View.VISIBLE);
			rgQI1005AN.requestFocus();
		}
    }
    
    public void onrgQI1005BChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005B.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005BN);
			lblQI1005B2.setVisibility(View.GONE);
			rgQI1005BN.setVisibility(View.GONE);
			rgQI1005C.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005BN);
			lblQI1005B2.setVisibility(View.VISIBLE);
			rgQI1005BN.setVisibility(View.VISIBLE);
			rgQI1005BN.requestFocus();
		}
    }
    
    public void onrgQI1005CChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005C.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005CN);
			lblQI1005C2.setVisibility(View.GONE);
			rgQI1005CN.setVisibility(View.GONE);
			rgQI1005D.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005CN);
			lblQI1005C2.setVisibility(View.VISIBLE);
			rgQI1005CN.setVisibility(View.VISIBLE);
			rgQI1005CN.requestFocus();
		}
    }
    
    public void onrgQI1005DChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005D.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005DN);
			lblQI1005D2.setVisibility(View.GONE);
			rgQI1005DN.setVisibility(View.GONE);
			rgQI1005E.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005DN);
			lblQI1005D2.setVisibility(View.VISIBLE);
			rgQI1005DN.setVisibility(View.VISIBLE);
			rgQI1005DN.requestFocus();
		}
    }
    
    public void onrgQI1005EChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005E.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005EN);
			lblQI1005E2.setVisibility(View.GONE);
			rgQI1005EN.setVisibility(View.GONE);
			rgQI1005F.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005EN);
			lblQI1005E2.setVisibility(View.VISIBLE);
			rgQI1005EN.setVisibility(View.VISIBLE);
			rgQI1005EN.requestFocus();
		}
    }
    
    public void onrgQI1005FChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005F.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI1005FN);
			lblQI1005F2.setVisibility(View.GONE);
			rgQI1005FN.setVisibility(View.GONE);
			rgQI1005G.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005FN);
			lblQI1005F2.setVisibility(View.VISIBLE);
			rgQI1005FN.setVisibility(View.VISIBLE);
			rgQI1005FN.requestFocus();
		}
    }
    
    public void onrgQI1005GChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005G.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005GN);
    		MyUtil.LiberarMemoria();
			lblQI1005G2.setVisibility(View.GONE);
			rgQI1005GN.setVisibility(View.GONE);
			rgQI1005H.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005GN);
			lblQI1005G2.setVisibility(View.VISIBLE);
			rgQI1005GN.setVisibility(View.VISIBLE);
			rgQI1005GN.requestFocus();
		}
    }
    
    public void onrgQI1005HChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005H.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005HN);
    		MyUtil.LiberarMemoria();
			lblQI1005H2.setVisibility(View.GONE);
			rgQI1005HN.setVisibility(View.GONE);
			rgQI1005I.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1005HN);
			lblQI1005H2.setVisibility(View.VISIBLE);
			rgQI1005HN.setVisibility(View.VISIBLE);
			rgQI1005HN.requestFocus();
		}
    }
    
    public void onrgQI1005IChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1005I.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1005IN);
    		MyUtil.LiberarMemoria();
			lblQI1005I2.setVisibility(View.GONE);
			rgQI1005IN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1005IN);
			lblQI1005I2.setVisibility(View.VISIBLE);
			rgQI1005IN.setVisibility(View.VISIBLE);
			rgQI1005IN.requestFocus();
		}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1005A.readOnly();
    		rgQI1005B.readOnly();
    		rgQI1005C.readOnly();
    		rgQI1005D.readOnly();
    		rgQI1005E.readOnly();
    		rgQI1005F.readOnly();
    		rgQI1005G.readOnly();
    		rgQI1005H.readOnly();
    		rgQI1005I.readOnly();
    		rgQI1005AN.readOnly();
    		rgQI1005BN.readOnly();
    		rgQI1005CN.readOnly();
    		rgQI1005DN.readOnly();
    		rgQI1005EN.readOnly();
    		rgQI1005FN.readOnly();
    		rgQI1005GN.readOnly();
    		rgQI1005HN.readOnly();
    		rgQI1005IN.readOnly();
    	}
    }
    
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }

	@Override
	public Integer grabadoParcial() {		
		uiToEntity(individual);
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		int cont=0;		
		if(individual.qi1005a==1)
			cont+=1;
		if(individual.qi1005b==1)
			cont+=1;
		if(individual.qi1005c==1)
			cont+=1;
		if(individual.qi1005d==1)
			cont+=1;
		if(individual.qi1005e==1)
			cont+=1;
		if(individual.qi1005f==1)
			cont+=1;
		if(individual.qi1005g==1)
			cont+=1;
		if(individual.qi1005h==1)
			cont+=1;
		if(individual.qi1005i==1)
			cont+=1;

		App.getInstance().setCiseccion_10_01(individual);
//		Log.e("App.getInstance().getCiseccion_10_01().filtro1005",""+App.getInstance().getCiseccion_10_01().filtro1005);
		App.getInstance().getCiseccion_10_01().filtro1005 = cont;
		App.getInstance().getCiseccion_10_01().qi1005a=individual.qi1005a;
		App.getInstance().getCiseccion_10_01().qi1005b=individual.qi1005b;
		App.getInstance().getCiseccion_10_01().qi1005c=individual.qi1005c;
		App.getInstance().getCiseccion_10_01().qi1005d=individual.qi1005d;
		App.getInstance().getCiseccion_10_01().qi1005e=individual.qi1005e;
		App.getInstance().getCiseccion_10_01().qi1005f=individual.qi1005f;
		App.getInstance().getCiseccion_10_01().qi1005g=individual.qi1005g;
		App.getInstance().getCiseccion_10_01().qi1005h=individual.qi1005h;
		App.getInstance().getCiseccion_10_01().qi1005i=individual.qi1005i;

		return App.NODEFINIDO;
	}
}