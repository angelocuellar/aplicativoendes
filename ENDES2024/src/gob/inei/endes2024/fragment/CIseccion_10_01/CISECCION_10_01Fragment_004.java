package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.graphics.YuvImage;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CISECCION_10_01Fragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI1007;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI1007m;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1008A;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1008AN;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1008B;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1008BN;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1008C;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1008CN;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1008D;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1008DN;
	
	CISECCION_10_01 individual;
	private CuestionarioService cuestionarioService;
	public ButtonComponent btnAntes,btnDespues;
	private LabelComponent lblTitulo,lblnumero,lblDespues,lblAntes,lblpregunta1008,lblpregunta1007_p1,lblpregunta1007_p2,lblpregunta1007_negrita, lblmes;
	private LabelComponent lblQI1008A,lblQI1008B,lblQI1008C,lblQI1008D,lblQI1008A2,lblQI1008B2,lblQI1008C2,lblQI1008D2;
	public TextField txtCabecera;
	public CheckBoxField chbP1007_1,chbP1007_2;
	public GridComponent2 gridPreguntas1007,gridPreguntas1008;
	LinearLayout q0,q1,q2,q3;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_10_01Fragment_004() {}
	public CISECCION_10_01Fragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override 
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1007","QI1008A","QI1008AN","QI1008B","QI1008BN","QI1008C","QI1008CN","QI1008D","QI1008DN","QI1000A","ID","HOGAR_ID","PERSONA_ID","QI1007M")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1007","QI1008A","QI1008AN","QI1008B","QI1008BN","QI1008C","QI1008CN","QI1008D","QI1008DN","QI1007M")};
		return rootView;
	}
    
    @Override 
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta1007_p1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1007_p1);
		lblpregunta1007_p2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1007_p2);
		lblpregunta1008 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1008);
	
		txtQI1007=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		
		
		txtQI1007m=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		  
		lblpregunta1007_negrita = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1007_text).negrita();
		
		chbP1007_1=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1007_95_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbP1007_1.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked ){
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),txtQI1007,txtQI1007m, chbP1007_2);
//					Util.cleanAndLockView(getActivity(),chbP1007_2);
//					Util.cleanAndLockView(getActivity(),txtQI1007m);
		  		}
				else {
					Util.lockView(getActivity(), false,txtQI1007, txtQI1007m, chbP1007_2);
//					Util.lockView(getActivity(), false,chbP1007_2);
//					Util.lockView(getActivity(), false,txtQI1007m);
		  		}
			}
		});
		
		chbP1007_2=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1007_96_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbP1007_2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI1007, txtQI1007m, chbP1007_1);
		    		//Util.cleanAndLockView(getActivity(),txtQI1007m,chbP1007_1);
		  		}
				else {
					Util.lockView(getActivity(), false,txtQI1007, txtQI1007m, chbP1007_1);
					//Util.lockView(getActivity(), false,txtQI1007m,chbP1007_1);	
		  		}
			}
		});
		
		txtQI1007.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				
				if(chbP1007_1.isChecked() || chbP1007_2.isChecked()){
					
				}else{
					if(s.toString().trim().length()>0){
						Util.cleanAndLockView(getActivity(),txtQI1007m);					
					}
					else{
						Util.lockView(getActivity(),false, txtQI1007m);
					}
				}
				
				
			}
		});
		
		txtQI1007m.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(chbP1007_1.isChecked() || chbP1007_2.isChecked()){
					
				}else{
					if(s.toString().trim().length()>0){
					    Util.cleanAndLockView(getActivity(),txtQI1007);					
					}
					else{
						Util.lockView(getActivity(),false, txtQI1007);
					}
				}
				
			}
		});
		  
		
		lblnumero  = new LabelComponent(this.getActivity()).size(altoComponente+10, 450).text(R.string.ciseccion_10_01qi1007_num).textSize(16);
		lblmes  = new LabelComponent(this.getActivity()).size(altoComponente+10, 450).text(R.string.ciseccion_10_01qi1007_mes).textSize(16);		
		lblAntes  = new LabelComponent(this.getActivity()).size(altoComponente+10, 405).text(R.string.ciseccion_10_01qi1007_95).textSize(16);
		lblDespues  = new LabelComponent(this.getActivity()).size(altoComponente+10, 450).text(R.string.ciseccion_10_01qi1007_96).textSize(16);
		
		lblQI1008A =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1008_pa);
		lblQI1008B =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1008_pb);
		lblQI1008C =new LabelComponent(getActivity()).textSize(17).size(altoComponente+20, 450).text(R.string.ciseccion_10_01qi1008_pc);
		lblQI1008D =new LabelComponent(getActivity()).textSize(17).size(altoComponente+30, 450).text(R.string.ciseccion_10_01qi1008_pd);
		
		rgQI1008A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_o1,R.string.ciseccion_10_01qi1008_o2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1008AChangeValue");
		rgQI1008B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_o1,R.string.ciseccion_10_01qi1008_o2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1008BChangeValue");
		rgQI1008C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_o1,R.string.ciseccion_10_01qi1008_o2).size(altoComponente+20,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1008CChangeValue");
		rgQI1008D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_o1,R.string.ciseccion_10_01qi1008_o2).size(altoComponente+30,290).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQI1008DChangeValue");
		
		lblQI1008A2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1008_sp);
		lblQI1008B2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1008_sp);
		lblQI1008C2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1008_sp);
		lblQI1008D2=new LabelComponent(getActivity()).textSize(17).size(altoComponente+80, 450).text(R.string.ciseccion_10_01qi1008_sp);
			
		rgQI1008AN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_so1,R.string.ciseccion_10_01qi1008_so2,R.string.ciseccion_10_01qi1008_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1008BN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_so1,R.string.ciseccion_10_01qi1008_so2,R.string.ciseccion_10_01qi1008_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1008CN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_so1,R.string.ciseccion_10_01qi1008_so2,R.string.ciseccion_10_01qi1008_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1008DN=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1008_so1,R.string.ciseccion_10_01qi1008_so2,R.string.ciseccion_10_01qi1008_so3).size(altoComponente+80,290).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);

		gridPreguntas1007 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas1007.addComponent(lblnumero);
		gridPreguntas1007.addComponent(txtQI1007);
		gridPreguntas1007.addComponent(lblmes);
		gridPreguntas1007.addComponent(txtQI1007m);	
		gridPreguntas1007.addComponent(lblpregunta1007_negrita,2);
		gridPreguntas1007.addComponent(lblAntes);
		gridPreguntas1007.addComponent(chbP1007_1);
		gridPreguntas1007.addComponent(lblDespues);
		gridPreguntas1007.addComponent(chbP1007_2);
		
		
		gridPreguntas1008=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1008.addComponent(lblQI1008A);
		gridPreguntas1008.addComponent(rgQI1008A);
		gridPreguntas1008.addComponent(lblQI1008A2);
		gridPreguntas1008.addComponent(rgQI1008AN);
		
		gridPreguntas1008.addComponent(lblQI1008B);
		gridPreguntas1008.addComponent(rgQI1008B);
		gridPreguntas1008.addComponent(lblQI1008B2);
		gridPreguntas1008.addComponent(rgQI1008BN);
		
		gridPreguntas1008.addComponent(lblQI1008C);
		gridPreguntas1008.addComponent(rgQI1008C);
		gridPreguntas1008.addComponent(lblQI1008C2);
		gridPreguntas1008.addComponent(rgQI1008CN);
		
		gridPreguntas1008.addComponent(lblQI1008D);
		gridPreguntas1008.addComponent(rgQI1008D);
		gridPreguntas1008.addComponent(lblQI1008D2);
		gridPreguntas1008.addComponent(rgQI1008DN);
	
    }
    
    
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1007_p1,lblpregunta1007_p2,gridPreguntas1007.component());
		q2 = createQuestionSection(lblpregunta1008,gridPreguntas1008.component());

		//q3 = createQuestionSection(lblpregunta1007_negrita);
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		//form.addView(q3);
		form.addView(q1);
		form.addView(q2);
		return contenedor;
    }
    
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		
		if (chbP1007_1.isChecked())
			individual.qi1007=95;			
		if (chbP1007_2.isChecked())
			individual.qi1007=96;
			
		if (chbP1007_1.isChecked())
			individual.qi1007m=95;			
		if (chbP1007_2.isChecked())
			individual.qi1007m=96;
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
		
		Integer in1007m=individual.qi1007m==null?0:individual.qi1007m; 
		Integer in1007=individual.qi1007==null?-1:individual.qi1007;
		
		//ToastMessage.msgBox(this.getActivity(), in1007m.toString()+" "+in1007.toString()  , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		//ToastMessage.msgBox(this.getActivity(), infiltro1005.toString()  , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		
		if(infiltro1005 >0){
			if(in1007m > 11 && in1007m !=95 && in1007m !=96  ){
				ToastMessage.msgBox(this.getActivity(), "El valor ingresado est� fuera de rango"  , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				
				error = true;
				return false;
			}
			if(in1007==0 && in1007 !=95 && in1007 !=96){
			ToastMessage.msgBox(this.getActivity(), "El valor ingresado est� fuera de rango"  , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			
				error = true;
				return false;
			}
			
		}
		
    	
		if (infiltro1005!=0) {
			if(!chbP1007_1.isChecked() && !chbP1007_2.isChecked() && Util.esVacio(individual.qi1007) && Util.esVacio(individual.qi1007m)){
				mensaje = preguntaVacia.replace("$", "La pregunta 1007");
				//view = rgQI1008A;
				error = true;
				return false;
			}
		}
		
		
	
        
		
		if (infiltro1005>=1) {
//			if (Util.esVacio(individual.qi1007)) {
//				mensaje = preguntaVacia.replace("$", "La pregunta QI1007");
//				view = txtQI1007n;
//				error = true;
//				return false;
//			}
			if (Util.esVacio(individual.qi1008a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1008A");
				view = rgQI1008A;
				error = true;
				return false;
			}
			if (individual.qi1008a==1) {
				if (Util.esVacio(individual.qi1008an)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1008AN");
					view = rgQI1008AN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1008b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1008B");
				view = rgQI1008B;
				error = true;
				return false;
			}
			if (individual.qi1008b==1) {
				if (Util.esVacio(individual.qi1008bn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1008BN");
					view = rgQI1008BN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1008c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1008C");
				view = rgQI1008C;
				error = true;
				return false;
			}
			if (individual.qi1008c==1) {
				if (Util.esVacio(individual.qi1008cn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1008CN");
					view = rgQI1008CN;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1008d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1008D");
				view = rgQI1008D;
				error = true;
				return false;
			}
			if (individual.qi1008d==1) {
				if (Util.esVacio(individual.qi1008dn)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1008DN");
					view = rgQI1008DN;
					error = true;
					return false;
				}
			}			
		}
		return true;
    }
    
    @Override 
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta1005(){
    	Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
    	
		if (infiltro1005==0) {
			Util.cleanAndLockView(getActivity(),txtQI1007,txtQI1007m,rgQI1008A,rgQI1008AN,rgQI1008B,rgQI1008BN,rgQI1008C,rgQI1008CN,rgQI1008D,rgQI1008DN);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			//q3.setVisibility(View.GONE);
			
			//ToastMessage.msgBox(this.getActivity(), "puros no",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
	
		} else{
			
			//ToastMessage.msgBox(this.getActivity(), "al menos un si",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
			
			Util.lockView(getActivity(), false,txtQI1007,txtQI1007m,rgQI1008A,rgQI1008AN,rgQI1008B,rgQI1008BN,rgQI1008C,rgQI1008CN,rgQI1008D,rgQI1008DN);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			//q3.setVisibility(View.VISIBLE);
			onrgQI1008AChangeValue();
	    	onrgQI1008BChangeValue();
	    	onrgQI1008CChangeValue();
	    	onrgQI1008DChangeValue();
	    	
	     	if (individual.qi1007!=null && individual.qi1007==95 || (individual.qi1007m!=null && individual.qi1007m==95) ) { //
	    		chbP1007_1.setChecked(true);
	    		chbP1007_2.setChecked(false);      
	    		Util.cleanAndLockView(getActivity(),txtQI1007);
	    		Util.cleanAndLockView(getActivity(),txtQI1007m);
			}
	    	if (individual.qi1007!=null && individual.qi1007==96 || ( individual.qi1007m!=null && individual.qi1007m==96) ) { //
	    		chbP1007_1.setChecked(false);
	    		chbP1007_2.setChecked(true);        
				Util.cleanAndLockView(getActivity(),txtQI1007);	
				Util.cleanAndLockView(getActivity(),txtQI1007m);
			}	    	

		}
    }
    
    public void validarPregunta1005a(){
    	Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
    	
		if (infiltro1005>=1) {
			Util.lockView(getActivity(), false,txtQI1007,txtQI1007m,rgQI1008A,rgQI1008AN,rgQI1008B,rgQI1008BN,rgQI1008C,rgQI1008CN,rgQI1008D,rgQI1008DN);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			onrgQI1008AChangeValue();
	    	onrgQI1008BChangeValue();
	    	onrgQI1008CChangeValue();
	    	onrgQI1008DChangeValue();
	    	
	    	if (individual.qi1007!=null && individual.qi1007==95 ||( individual.qi1007m!=null && individual.qi1007m==95) ) {//
	    		chbP1007_1.setChecked(true);
	    		chbP1007_2.setChecked(false);      
	    		Util.cleanAndLockView(getActivity(),txtQI1007);
	    		Util.cleanAndLockView(getActivity(),txtQI1007m);
			}
	    	if (individual.qi1007!=null && individual.qi1007==96 || ( individual.qi1007m!=null && individual.qi1007m==96) ) {//
	    		chbP1007_1.setChecked(false);
	    		chbP1007_2.setChecked(true);        
				Util.cleanAndLockView(getActivity(),txtQI1007);
				Util.cleanAndLockView(getActivity(),txtQI1007m);
			}    	

		} 
	
    }
    
    
    public void ValidaTextoPregunta1007(){
    	Integer in501=App.getInstance().getCiseccion05_07().qi501==null?0:App.getInstance().getCiseccion05_07().qi501;
    	Integer in502=App.getInstance().getCiseccion05_07().qi502==null?0:App.getInstance().getCiseccion05_07().qi502;
    	if (in501==3 && in502==3) {
    		q1.setVisibility(View.VISIBLE);
    		lblpregunta1007_p1.setVisibility(View.GONE);
    		lblpregunta1007_p2.setVisibility(View.VISIBLE);
    		//lblpregunta1007_negrita.setVisibility(View.VISIBLE);
    		
    		
		}
    	else{
			q1.setVisibility(View.GONE);
			lblpregunta1007_p1.setVisibility(View.VISIBLE);
			lblpregunta1007_p2.setVisibility(View.GONE);
    		//lblpregunta1007_negrita.setVisibility(View.GONE);

    	}
    }
    
    private void inicio() {
    	validarPregunta1005();
    	ValidarsiesSupervisora();
    	ValidaTextoPregunta1007();
    	validarPregunta1005a();
    	txtCabecera.requestFocus();
    }
    
    public void onrgQI1008AChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1008A.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1008AN);
    		MyUtil.LiberarMemoria();
			lblQI1008A2.setVisibility(View.GONE);
			rgQI1008AN.setVisibility(View.GONE);
			rgQI1008B.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1008AN);
			lblQI1008A2.setVisibility(View.VISIBLE);
			rgQI1008AN.setVisibility(View.VISIBLE);
			rgQI1008AN.requestFocus();
		}
    }
    
    public void onrgQI1008BChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1008B.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1008BN);
    		MyUtil.LiberarMemoria();
			lblQI1008B2.setVisibility(View.GONE);
			rgQI1008BN.setVisibility(View.GONE);
			rgQI1008C.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1008BN);
			lblQI1008B2.setVisibility(View.VISIBLE);
			rgQI1008BN.setVisibility(View.VISIBLE);
			rgQI1008BN.requestFocus();
		}
    }
    
    public void onrgQI1008CChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1008C.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1008CN);
    		MyUtil.LiberarMemoria();
			lblQI1008C2.setVisibility(View.GONE);
			rgQI1008CN.setVisibility(View.GONE);
			rgQI1008D.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI1008CN);
			lblQI1008C2.setVisibility(View.VISIBLE);
			rgQI1008CN.setVisibility(View.VISIBLE);
			rgQI1008CN.requestFocus();
		}
    }
    
    public void onrgQI1008DChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI1008D.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI1008DN);
    		MyUtil.LiberarMemoria();
			lblQI1008D2.setVisibility(View.GONE);
			rgQI1008DN.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI1008DN);
			lblQI1008D2.setVisibility(View.VISIBLE);
			rgQI1008DN.setVisibility(View.VISIBLE);
			rgQI1008DN.requestFocus();
		}
    }

    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1008A.readOnly();
    		rgQI1008B.readOnly();
    		rgQI1008C.readOnly();
    		rgQI1008D.readOnly();
    		rgQI1008AN.readOnly();
    		rgQI1008BN.readOnly();
    		rgQI1008CN.readOnly();
    		rgQI1008DN.readOnly();
    		txtQI1007.readOnly();
    		txtQI1007m.readOnly();
    		chbP1007_1.readOnly();
    		chbP1007_2.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }

	@Override
	public Integer grabadoParcial() {
	uiToEntity(individual);		

		if (chbP1007_1.isChecked())
			individual.qi1007=95;			
		if (chbP1007_2.isChecked())
			individual.qi1007=96;
			
		if (chbP1007_1.isChecked())
			individual.qi1007m=95;			
		if (chbP1007_2.isChecked())
			individual.qi1007m=96;
	
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}