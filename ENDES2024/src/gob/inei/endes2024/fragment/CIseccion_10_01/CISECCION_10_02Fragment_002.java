package gob.inei.endes2024.fragment.CIseccion_10_01; 
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_02Fragment_002 extends FragmentForm { 

	
	CISECCION_10_01 individual; 
	private CuestionarioService cuestionarioService; 


	public CISECCION_10_02Fragment_002() {} 
	public CISECCION_10_02Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 

    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
	

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 

		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 

		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		
		entityToUI(individual); 
		inicio(); 
    } 
    
    private void inicio() { 
    } 
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	} 
} 
