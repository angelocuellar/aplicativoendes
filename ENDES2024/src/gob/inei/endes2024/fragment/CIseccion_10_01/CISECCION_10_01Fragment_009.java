package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.IFormComponent;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_009 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1025;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1026;
	
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI1026A_A; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI1026A_B; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI1026A_C; 	
	
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI1028_A;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI1028_B;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI1028_X;
	@FieldAnnotation(orderIndex=9)
	public TextField txtQI1028_XI;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI1028_Y;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI1030A_A;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI1030A_B;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI1030A_C;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI1030A_D;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI1030A_E;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI1030A_F;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI1030A_G;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI1030A_H;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI1030A_I;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI1030A_J;
	@FieldAnnotation(orderIndex=21)
	public CheckBoxField chbQI1030A_K;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI1030A_L;
	@FieldAnnotation(orderIndex=23)
	public CheckBoxField chbQI1030A_M;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI1030A_X;
	@FieldAnnotation(orderIndex=25)
	public TextField txtQI1030A_XI;
	@FieldAnnotation(orderIndex=26)
	public CheckBoxField chbQI1030B_A;
	@FieldAnnotation(orderIndex=27)
	public CheckBoxField chbQI1030B_B;
	@FieldAnnotation(orderIndex=28)
	public CheckBoxField chbQI1030B_C;
	@FieldAnnotation(orderIndex=29)
	public CheckBoxField chbQI1030B_D;
	@FieldAnnotation(orderIndex=30)
	public CheckBoxField chbQI1030B_E;
	@FieldAnnotation(orderIndex=31)
	public CheckBoxField chbQI1030B_F;
	@FieldAnnotation(orderIndex=32)
	public CheckBoxField chbQI1030B_G;
	@FieldAnnotation(orderIndex=33)
	public CheckBoxField chbQI1030B_H;
	@FieldAnnotation(orderIndex=34)
	public CheckBoxField chbQI1030B_I;
	@FieldAnnotation(orderIndex=35)
	public CheckBoxField chbQI1030B_J;
	@FieldAnnotation(orderIndex=36)
	public CheckBoxField chbQI1030B_K;
	@FieldAnnotation(orderIndex=37)
	public CheckBoxField chbQI1030B_L;
	@FieldAnnotation(orderIndex=38)
	public CheckBoxField chbQI1030B_M;
	@FieldAnnotation(orderIndex=39)
	public CheckBoxField chbQI1030B_X;
	@FieldAnnotation(orderIndex=40)
	public TextField txtQI1030B_XI;
	@FieldAnnotation(orderIndex=41)
	public CheckBoxField chbQI1030C_A;
	@FieldAnnotation(orderIndex=42)
	public CheckBoxField chbQI1030C_B;
	@FieldAnnotation(orderIndex=43)
	public CheckBoxField chbQI1030C_C;
	@FieldAnnotation(orderIndex=44)
	public CheckBoxField chbQI1030C_D;
	@FieldAnnotation(orderIndex=45)
	public CheckBoxField chbQI1030C_E;
	@FieldAnnotation(orderIndex=46)
	public CheckBoxField chbQI1030C_F;
	@FieldAnnotation(orderIndex=47)
	public CheckBoxField chbQI1030C_G;
	@FieldAnnotation(orderIndex=48)
	public CheckBoxField chbQI1030C_H;
	@FieldAnnotation(orderIndex=49)
	public CheckBoxField chbQI1030C_I;
	@FieldAnnotation(orderIndex=50)
	public CheckBoxField chbQI1030C_J;
	@FieldAnnotation(orderIndex=51)
	public CheckBoxField chbQI1030C_K;
	@FieldAnnotation(orderIndex=52)
	public CheckBoxField chbQI1030C_L;
	@FieldAnnotation(orderIndex=53)
	public CheckBoxField chbQI1030C_M;
	@FieldAnnotation(orderIndex=54)
	public CheckBoxField chbQI1030C_X;
	@FieldAnnotation(orderIndex=55)
	public TextField txtQI1030C_XI;
	
	CISECCION_10_01 individual;	
	CISECCION_01_03 individualS1;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1025,lblpregunta1026,lblpregunta1026a,lblpregunta1026a_a,lblpregunta1026a_b,lblpregunta1026a_c,lblpregunta1028,lblpregunta1030,lblvacio;
	private LabelComponent lblP1030_0,lblP1030_1,lblP1030_2,lblP1030_3,lblQI1030A,lblQI1030B,lblQI1030C,lblQI1030D,lblQI1030E,lblQI1030F,lblQI1030G,lblQI1030H,lblQI1030I,lblQI1030J,lblQI1030K,lblQI1030L,lblQI1030M,lblQI1030X;
	public GridComponent2 gridPreguntas1026a,gridPreguntas1030;
	public TextField txtCabecera;
	LinearLayout q0,q1,q2,q3,q4,q5;
	
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS1;

	public CISECCION_10_01Fragment_009() {}
	public CISECCION_10_01Fragment_009 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1000A","QI1025","QI1026","QI1026A_A","QI1026A_B","QI1026A_C","QI1028_A","QI1028_B","QI1028_X","QI1028_XI","QI1028_Y","QI1030A_A","QI1030A_B","QI1030A_C","QI1030A_D","QI1030A_E","QI1030A_F","QI1030A_G","QI1030A_H","QI1030A_I","QI1030A_J","QI1030A_K","QI1030A_L","QI1030A_M","QI1030A_X","QI1030A_XI","QI1030B_A","QI1030B_B","QI1030B_C","QI1030B_D","QI1030B_E","QI1030B_F","QI1030B_G","QI1030B_H","QI1030B_I","QI1030B_J","QI1030B_K","QI1030B_L","QI1030B_M","QI1030B_X","QI1030B_XI","QI1030C_A","QI1030C_B","QI1030C_C","QI1030C_D","QI1030C_E","QI1030C_F","QI1030C_G","QI1030C_H","QI1030C_I","QI1030C_J","QI1030C_K","QI1030C_L","QI1030C_M","QI1030C_X","QI1030C_XI","qi1013a","qi1013aa","qi1013b","QI1019","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1025","QI1026","QI1026A_A","QI1026A_B","QI1026A_C","QI1028_A","QI1028_B","QI1028_X","QI1028_XI","QI1028_Y","QI1030A_A","QI1030A_B","QI1030A_C","QI1030A_D","QI1030A_E","QI1030A_F","QI1030A_G","QI1030A_H","QI1030A_I","QI1030A_J","QI1030A_K","QI1030A_L","QI1030A_M","QI1030A_X","QI1030A_XI","QI1030B_A","QI1030B_B","QI1030B_C","QI1030B_D","QI1030B_E","QI1030B_F","QI1030B_G","QI1030B_H","QI1030B_I","QI1030B_J","QI1030B_K","QI1030B_L","QI1030B_M","QI1030B_X","QI1030B_XI","QI1030C_A","QI1030C_B","QI1030C_C","QI1030C_D","QI1030C_E","QI1030C_F","QI1030C_G","QI1030C_H","QI1030C_I","QI1030C_J","QI1030C_K","QI1030C_L","QI1030C_M","QI1030C_X","QI1030C_XI")};
		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI201","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
	}
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1025 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1025);
		lblpregunta1026 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1026);
		lblpregunta1026a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1026a);
		lblpregunta1028 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1028);
		lblpregunta1030 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1030);
				
		
		rgQI1025=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1025_1,R.string.ciseccion_10_01qi1025_2,R.string.ciseccion_10_01qi1025_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI1026=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1026_1,R.string.ciseccion_10_01qi1026_2,R.string.ciseccion_10_01qi1026_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta1026a_a = new LabelComponent(this.getActivity()).size(altoComponente+60, 460).textSize(17).text(R.string.ciseccion_10_01qi1026a_a);
		lblpregunta1026a_b = new LabelComponent(this.getActivity()).size(altoComponente+60, 460).textSize(17).text(R.string.ciseccion_10_01qi1026a_b);
		lblpregunta1026a_c = new LabelComponent(this.getActivity()).size(altoComponente+10, 460).textSize(17).text(R.string.ciseccion_10_01qi1026a_c);
		
		rgQI1026A_A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1026a_1,R.string.ciseccion_10_01qi1026a_2,R.string.ciseccion_10_01qi1026a_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI1026A_B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1026a_1,R.string.ciseccion_10_01qi1026a_2,R.string.ciseccion_10_01qi1026a_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI1026A_C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1026a_1,R.string.ciseccion_10_01qi1026a_2,R.string.ciseccion_10_01qi1026a_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		chbQI1028_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1028_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1028_AChangeValue");
		chbQI1028_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1028_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1028_BChangeValue");
		chbQI1028_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1028_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1028_XChangeValue");
		txtQI1028_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		chbQI1028_Y=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1028_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1028_YChangeValue");
		
		chbQI1030A_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_a, "1:0").size(60, 160);
		chbQI1030A_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_b, "1:0").size(60, 160);
		chbQI1030A_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_c, "1:0").size(60, 160);
		chbQI1030A_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_d, "1:0").size(60, 160);
		chbQI1030A_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_e, "1:0").size(60, 160);
		chbQI1030A_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_f, "1:0").size(60, 160);
		chbQI1030A_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_g, "1:0").size(60, 160);
		chbQI1030A_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_h, "1:0").size(60, 160);
		chbQI1030A_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_i, "1:0").size(60, 160);
		chbQI1030A_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_j, "1:0").size(60, 160);
		chbQI1030A_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_k, "1:0").size(60, 160);
		chbQI1030A_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_l, "1:0").size(60, 160);
		chbQI1030A_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_m, "1:0").size(60, 160);
		chbQI1030A_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_x, "1:0").size(60, 160).callback("onctxtQI1030A_XIChangeValue");
		txtQI1030A_XI=new TextField(this.getActivity()).maxLength(1000).size(60, 160);
		chbQI1030B_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_a, "1:0").size(60, 160);
		chbQI1030B_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_b, "1:0").size(60, 160);
		chbQI1030B_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_c, "1:0").size(60, 160);
		chbQI1030B_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_d, "1:0").size(60, 160);
		chbQI1030B_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_e, "1:0").size(60, 160);
		chbQI1030B_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_f, "1:0").size(60, 160);
		chbQI1030B_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_g, "1:0").size(60, 160);
		chbQI1030B_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_h, "1:0").size(60, 160);
		chbQI1030B_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_i, "1:0").size(60, 160);
		chbQI1030B_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_j, "1:0").size(60, 160);
		chbQI1030B_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_k, "1:0").size(60, 160);
		chbQI1030B_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_l, "1:0").size(60, 160);
		chbQI1030B_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_m, "1:0").size(60, 160);
		chbQI1030B_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_x, "1:0").size(60, 160).callback("onctxtQI1030B_XIChangeValue");
		txtQI1030B_XI=new TextField(this.getActivity()).maxLength(1000).size(60, 160);
		chbQI1030C_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_a, "1:0").size(60, 160);
		chbQI1030C_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_b, "1:0").size(60, 160);
		chbQI1030C_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_c, "1:0").size(60, 160);
		chbQI1030C_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_d, "1:0").size(60, 160);
		chbQI1030C_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_e, "1:0").size(60, 160);
		chbQI1030C_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_f, "1:0").size(60, 160);
		chbQI1030C_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_g, "1:0").size(60, 160);
		chbQI1030C_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_h, "1:0").size(60, 160);
		chbQI1030C_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_i, "1:0").size(60, 160);
		chbQI1030C_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_j, "1:0").size(60, 160);
		chbQI1030C_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_k, "1:0").size(60, 160);
		chbQI1030C_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_l, "1:0").size(60, 160);
		chbQI1030C_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_m, "1:0").size(60, 160);
		chbQI1030C_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1030_x, "1:0").size(60, 160).callback("onctxtQI1030C_XIChangeValue"); 
		txtQI1030C_XI=new TextField(this.getActivity()).maxLength(1000).size(60, 160);
		
		lblP1030_0 = new LabelComponent(getActivity()).textSize(16).size(160, 250).text(R.string.ciseccion_10_01qi1030_0).centrar();
		lblP1030_1 = new LabelComponent(getActivity()).textSize(16).size(160, 160).text(R.string.ciseccion_10_01qi1030_1).centrar();
		lblP1030_2 = new LabelComponent(getActivity()).textSize(16).size(160, 160).text(R.string.ciseccion_10_01qi1030_2).centrar();
		lblP1030_3 = new LabelComponent(getActivity()).textSize(16).size(160, 160).text(R.string.ciseccion_10_01qi1030_3).centrar();
		lblvacio = new LabelComponent(getActivity()).textSize(16).size(60, 160);
		
		lblQI1030A =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pa);
		lblQI1030B =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pb);
		lblQI1030C =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pc);
		lblQI1030D =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pd);
		lblQI1030E =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pe);
		lblQI1030F =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pf);
		lblQI1030G =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pg);
		lblQI1030H =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Ph);
		lblQI1030I =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pi);
		lblQI1030J =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pj);
		lblQI1030K =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pk);
		lblQI1030L =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pl);
		lblQI1030M =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Pm);
		lblQI1030X =new LabelComponent(getActivity()).textSize(16).size(60, 250).text(R.string.ciseccion_10_01qi1030_Px);

		gridPreguntas1026a = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas1026a.addComponent(lblpregunta1026a_a);
		gridPreguntas1026a.addComponent(rgQI1026A_A);
		gridPreguntas1026a.addComponent(lblpregunta1026a_b);
		gridPreguntas1026a.addComponent(rgQI1026A_B);
		gridPreguntas1026a.addComponent(lblpregunta1026a_c);
		gridPreguntas1026a.addComponent(rgQI1026A_C);
		
		
		gridPreguntas1030=new GridComponent2(this.getActivity(),App.ESTILO,4,1);
		gridPreguntas1030.addComponent(lblP1030_0);
		gridPreguntas1030.addComponent(lblP1030_1);
		gridPreguntas1030.addComponent(lblP1030_2);
		gridPreguntas1030.addComponent(lblP1030_3);
		
		gridPreguntas1030.addComponent(lblQI1030A);
		gridPreguntas1030.addComponent(chbQI1030A_A);
		gridPreguntas1030.addComponent(chbQI1030B_A);
		gridPreguntas1030.addComponent(chbQI1030C_A);

		gridPreguntas1030.addComponent(lblQI1030B);
		gridPreguntas1030.addComponent(chbQI1030A_B);
		gridPreguntas1030.addComponent(chbQI1030B_B);
		gridPreguntas1030.addComponent(chbQI1030C_B);

		gridPreguntas1030.addComponent(lblQI1030C);
		gridPreguntas1030.addComponent(chbQI1030A_C);
		gridPreguntas1030.addComponent(chbQI1030B_C);
		gridPreguntas1030.addComponent(chbQI1030C_C);

		gridPreguntas1030.addComponent(lblQI1030D);
		gridPreguntas1030.addComponent(chbQI1030A_D);
		gridPreguntas1030.addComponent(chbQI1030B_D);
		gridPreguntas1030.addComponent(chbQI1030C_D);

		gridPreguntas1030.addComponent(lblQI1030E);
		gridPreguntas1030.addComponent(chbQI1030A_E);
		gridPreguntas1030.addComponent(chbQI1030B_E);
		gridPreguntas1030.addComponent(chbQI1030C_E);

		gridPreguntas1030.addComponent(lblQI1030F);
		gridPreguntas1030.addComponent(chbQI1030A_F);
		gridPreguntas1030.addComponent(chbQI1030B_F);
		gridPreguntas1030.addComponent(chbQI1030C_F);

		gridPreguntas1030.addComponent(lblQI1030G);
		gridPreguntas1030.addComponent(chbQI1030A_G);
		gridPreguntas1030.addComponent(chbQI1030B_G);
		gridPreguntas1030.addComponent(chbQI1030C_G);

		gridPreguntas1030.addComponent(lblQI1030H);
		gridPreguntas1030.addComponent(chbQI1030A_H);
		gridPreguntas1030.addComponent(chbQI1030B_H);
		gridPreguntas1030.addComponent(chbQI1030C_H);

		gridPreguntas1030.addComponent(lblQI1030I);
		gridPreguntas1030.addComponent(chbQI1030A_I);
		gridPreguntas1030.addComponent(chbQI1030B_I);
		gridPreguntas1030.addComponent(chbQI1030C_I);

		gridPreguntas1030.addComponent(lblQI1030J);
		gridPreguntas1030.addComponent(chbQI1030A_J);
		gridPreguntas1030.addComponent(chbQI1030B_J);
		gridPreguntas1030.addComponent(chbQI1030C_J);

		gridPreguntas1030.addComponent(lblQI1030K);
		gridPreguntas1030.addComponent(chbQI1030A_K);
		gridPreguntas1030.addComponent(chbQI1030B_K);
		gridPreguntas1030.addComponent(chbQI1030C_K);

		gridPreguntas1030.addComponent(lblQI1030L);
		gridPreguntas1030.addComponent(chbQI1030A_L);
		gridPreguntas1030.addComponent(chbQI1030B_L);
		gridPreguntas1030.addComponent(chbQI1030C_L);

		gridPreguntas1030.addComponent(lblQI1030M);
		gridPreguntas1030.addComponent(chbQI1030A_M);
		gridPreguntas1030.addComponent(chbQI1030B_M);
		gridPreguntas1030.addComponent(chbQI1030C_M);

		gridPreguntas1030.addComponent(lblQI1030X);
		gridPreguntas1030.addComponent(chbQI1030A_X);
		gridPreguntas1030.addComponent(chbQI1030B_X);
		gridPreguntas1030.addComponent(chbQI1030C_X);
		
		gridPreguntas1030.addComponent(lblvacio);
		gridPreguntas1030.addComponent(txtQI1030A_XI);
		gridPreguntas1030.addComponent(txtQI1030B_XI);
		gridPreguntas1030.addComponent(txtQI1030C_XI);
    }
    
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1025,rgQI1025);
		q2 = createQuestionSection(lblpregunta1026,rgQI1026);
		
		q3 = createQuestionSection(lblpregunta1026a,gridPreguntas1026a.component()); 
		
		LinearLayout ly1028 = new LinearLayout(getActivity());
		ly1028.addView(chbQI1028_X);
		ly1028.addView(txtQI1028_XI);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1028,chbQI1028_A,chbQI1028_B,ly1028,chbQI1028_Y);
		q5 = createQuestionSection(lblpregunta1030,gridPreguntas1030.component());
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);

		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		
		if (individual.qi1025!=null)
			individual.qi1025=individual.getConvertQi1025(individual.qi1025);
		if (individual.qi1026!=null)
			individual.qi1026=individual.getConvertQi1026(individual.qi1026);
		if (individual.qi1026a_a!=null)
			individual.qi1026a_a=individual.getConvertQi1026a_a(individual.qi1026a_a);
		if (individual.qi1026a_b!=null)
			individual.qi1026a_b=individual.getConvertQi1026a_b(individual.qi1026a_b);
		if (individual.qi1026a_c!=null)
			individual.qi1026a_c=individual.getConvertQi1026a_c(individual.qi1026a_c);
		if (individual.qi1031!=null)
			individual.qi1031=individual.getConvertQi1031(individual.qi1031);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000A=individual.qi1000a==null?0:individual.qi1000a;
		if (in1000A!=2) {
			if (Util.esVacio(individual.qi1025)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1025");
				view = rgQI1025;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1026)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1026");
				view = rgQI1026;
				error = true;
				return false;
			}
			
			if (Util.esVacio(individual.qi1026a_a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI1026A_A"); 
				view = rgQI1026A_A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi1026a_b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI1026A_B"); 
				view = rgQI1026A_B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi1026a_c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI1026A_C"); 
				view = rgQI1026A_C; 
				error = true; 
				return false; 
			} 
			
			
			
			
			
			
			
			if (getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id)) {
				if (!verificarCheck() && !chbQI1028_Y.isChecked()) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1028_A");
					view = chbQI1028_A;
					error = true;
					return false;				
				}
				if (chbQI1028_X.isChecked()) {
					if (Util.esVacio(individual.qi1028_xi)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1028_XI");
						view = txtQI1028_XI;
						error = true;
						return false;
					}
				}
				if (chbQI1028_A.isChecked()) {
					if (!Util.alMenosUnoEsDiferenteA(0,individual.qi1030a_a,individual.qi1030a_b,individual.qi1030a_c,individual.qi1030a_d,individual.qi1030a_e,individual.qi1030a_f,individual.qi1030a_g,individual.qi1030a_h,individual.qi1030a_i,individual.qi1030a_j,individual.qi1030a_k,individual.qi1030a_l,individual.qi1030a_m,individual.qi1030a_x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1030A_A");
						view = chbQI1030A_A;
						error = true;
						return false;
					}
					if (chbQI1030A_X.isChecked()) {
						if (Util.esVacio(individual.qi1030a_xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1030A_XI");
							view = txtQI1030A_XI;
							error = true;
							return false;
						}
					}
				}
				
				if (chbQI1028_B.isChecked()) {
					if (!Util.alMenosUnoEsDiferenteA(0,individual.qi1030b_a,individual.qi1030b_b,individual.qi1030b_c,individual.qi1030b_d,individual.qi1030b_e,individual.qi1030b_f,individual.qi1030b_g,individual.qi1030b_h,individual.qi1030b_i,individual.qi1030b_j,individual.qi1030b_k,individual.qi1030b_l,individual.qi1030b_m,individual.qi1030b_x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1030B_A");
						view = chbQI1030B_A;
						error = true;
						return false;
					}
					if (chbQI1030B_X.isChecked()) {
						if (Util.esVacio(individual.qi1030b_xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1030B_XI");
							view = txtQI1030B_XI;
							error = true;
							return false;
						}
					}
				}
				
				if (chbQI1028_X.isChecked()) {
					if (!Util.alMenosUnoEsDiferenteA(0,individual.qi1030c_a,individual.qi1030c_b,individual.qi1030c_c,individual.qi1030c_d,individual.qi1030c_e,individual.qi1030c_f,individual.qi1030c_g,individual.qi1030c_h,individual.qi1030c_i,individual.qi1030c_j,individual.qi1030c_j,individual.qi1030c_l,individual.qi1030c_m,individual.qi1030c_x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1030C_A");
						view = chbQI1030C_A;
						error = true;
						return false;
					}
					if (chbQI1030C_X.isChecked()) {
						if (Util.esVacio(individual.qi1030c_xi)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI1030C_XI");
							view = txtQI1030C_XI;
							error = true;
							return false;
						}
					}
				}
			}
			

		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS1 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1);
		
    	if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if(individual.qi1025!=null)
			individual.qi1025=individual.setConvertQi1025(individual.qi1025);
		if(individual.qi1026!=null)
			individual.qi1026=individual.setConvertQi1026(individual.qi1026);
		if(individual.qi1026a_a!=null)
			individual.qi1026a_a=individual.setConvertQi1026a_a(individual.qi1026a_a);
		if(individual.qi1026a_b!=null)
			individual.qi1026a_b=individual.setConvertQi1026a_b(individual.qi1026a_b);
		if(individual.qi1026a_c!=null)
			individual.qi1026a_c=individual.setConvertQi1026a_c(individual.qi1026a_c);
		if(individual.qi1031!=null)
			individual.qi1031=individual.setConvertQi1031(individual.qi1031);
		
		App.getInstance().getCiseccion_10_01().qi1013a=individual.qi1013a;
		App.getInstance().getCiseccion_10_01().qi1013aa=individual.qi1013aa;
		App.getInstance().getCiseccion_10_01().qi1013b=individual.qi1013b;
		
		App.getInstance().getCiseccion_10_01().qi1019=individual.qi1019;
		App.getInstance().getPersonaCuestionarioIndividual().qi201=individualS1.qi201;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individualS1.qi226;
    	App.getInstance().getPersonaCuestionarioIndividual().qi230=individualS1.qi230;
    	
		entityToUI(individual);
		inicio();
    }
    
    
    public void validarPregunta1027(){
    	if (getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id)) {
			Log.e("s","1");
    		Util.lockView(getActivity(), false,chbQI1028_A,chbQI1028_B,chbQI1028_X,chbQI1028_Y,chbQI1030A_A,chbQI1030A_B,chbQI1030A_C,chbQI1030A_D,chbQI1030A_E,chbQI1030A_F,chbQI1030A_G,chbQI1030A_H,chbQI1030A_I,chbQI1030A_J,chbQI1030A_K,chbQI1030A_L,chbQI1030A_M,chbQI1030A_X,chbQI1030B_A,chbQI1030B_B,chbQI1030B_C,chbQI1030B_D,chbQI1030B_E,chbQI1030B_F,chbQI1030B_G,chbQI1030B_H,chbQI1030B_I,chbQI1030B_J,chbQI1030B_K,chbQI1030B_L,chbQI1030B_M,chbQI1030B_X,chbQI1030C_A,chbQI1030C_B,chbQI1030C_C,chbQI1030C_D,chbQI1030C_E,chbQI1030C_F,chbQI1030C_G,chbQI1030C_H,chbQI1030C_I,chbQI1030C_J,chbQI1030C_K,chbQI1030C_L,chbQI1030C_M,chbQI1030C_X);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onchbQI1028_YChangeValue();
		} 
    	else {
    		Log.e("s","2");
    		Util.cleanAndLockView(getActivity(),chbQI1028_A,chbQI1028_B,chbQI1028_X,txtQI1028_XI,chbQI1028_Y,chbQI1030A_A,chbQI1030A_B,chbQI1030A_C,chbQI1030A_D,chbQI1030A_E,chbQI1030A_F,chbQI1030A_G,chbQI1030A_H,chbQI1030A_I,chbQI1030A_J,chbQI1030A_K,chbQI1030A_L,chbQI1030A_M,chbQI1030A_X,txtQI1030A_XI,chbQI1030B_A,chbQI1030B_B,chbQI1030B_C,chbQI1030B_D,chbQI1030B_E,chbQI1030B_F,chbQI1030B_G,chbQI1030B_H,chbQI1030B_I,chbQI1030B_J,chbQI1030B_K,chbQI1030B_L,chbQI1030B_M,chbQI1030B_X,txtQI1030B_XI,chbQI1030C_A,chbQI1030C_B,chbQI1030C_C,chbQI1030C_D,chbQI1030C_E,chbQI1030C_F,chbQI1030C_G,chbQI1030C_H,chbQI1030C_I,chbQI1030C_J,chbQI1030C_K,chbQI1030C_L,chbQI1030C_M,chbQI1030C_X,txtQI1030C_XI);
    		q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		}
    }
    
    
    
    private void inicio() {
    	validarPregunta1027();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
 
    public void onchbQI1028_YChangeValue() {
    	if (chbQI1028_Y.isChecked()){
    		Log.e("s","3");
    		Util.cleanAndLockView(getActivity(),chbQI1028_A,chbQI1028_B,chbQI1028_X);
    		MyUtil.LiberarMemoria();
    		q5.setVisibility(View.GONE);
    	}
  		else{
  			Log.e("s","4");
  			q5.setVisibility(View.VISIBLE);
  			Util.lockView(getActivity(),false,chbQI1028_A,chbQI1028_B,chbQI1028_X);
  			onchbQI1028_AChangeValue();
  			onchbQI1028_BChangeValue();
  			onchbQI1028_XChangeValue();
  			if (verificarCheck()) {
  				Util.cleanAndLockView(getActivity(),chbQI1028_Y);
			}
  			else{
  				Util.lockView(getActivity(),false,chbQI1028_Y);
  			}
  		}
    }
    
    public void onchbQI1028_AChangeValue() {
    	if (chbQI1028_A.isChecked()) {
    		Log.e("s","5");
    		Util.cleanAndLockView(getActivity(),chbQI1028_Y);
    		Util.lockView(getActivity(),false,lblP1030_1,chbQI1030A_A,chbQI1030A_B,chbQI1030A_C,chbQI1030A_D,chbQI1030A_E,chbQI1030A_F,chbQI1030A_G,chbQI1030A_H,chbQI1030A_I,chbQI1030A_J,chbQI1030A_K,chbQI1030A_L,chbQI1030A_M,chbQI1030A_X); 
    		lblP1030_1.setVisibility(View.VISIBLE);
    		chbQI1030A_A.setVisibility(View.VISIBLE);
    		chbQI1030A_B.setVisibility(View.VISIBLE);
    		chbQI1030A_C.setVisibility(View.VISIBLE);
    		chbQI1030A_D.setVisibility(View.VISIBLE);
    		chbQI1030A_E.setVisibility(View.VISIBLE);
    		chbQI1030A_F.setVisibility(View.VISIBLE);
    		chbQI1030A_G.setVisibility(View.VISIBLE);
    		chbQI1030A_H.setVisibility(View.VISIBLE);
    		chbQI1030A_I.setVisibility(View.VISIBLE);
    		chbQI1030A_J.setVisibility(View.VISIBLE);
    		chbQI1030A_K.setVisibility(View.VISIBLE);
    		chbQI1030A_L.setVisibility(View.VISIBLE);
    		chbQI1030A_M.setVisibility(View.VISIBLE);
    		chbQI1030A_X.setVisibility(View.VISIBLE);
    		txtQI1030A_XI.setVisibility(View.VISIBLE);
    		onctxtQI1030A_XIChangeValue();
    	}
    	else {		
    		Log.e("s","6");
    		Util.lockView(getActivity(),false,chbQI1028_Y);
    		Util.cleanAndLockView(getActivity(),lblP1030_1,chbQI1030A_A,chbQI1030A_B,chbQI1030A_C,chbQI1030A_D,chbQI1030A_E,chbQI1030A_F,chbQI1030A_G,chbQI1030A_H,chbQI1030A_I,chbQI1030A_J,chbQI1030A_K,chbQI1030A_L,chbQI1030A_M,chbQI1030A_X);
    		lblP1030_1.setVisibility(View.GONE);
    		chbQI1030A_A.setVisibility(View.GONE);
    		chbQI1030A_B.setVisibility(View.GONE);
    		chbQI1030A_C.setVisibility(View.GONE);
    		chbQI1030A_D.setVisibility(View.GONE);
    		chbQI1030A_E.setVisibility(View.GONE);
    		chbQI1030A_F.setVisibility(View.GONE);
    		chbQI1030A_G.setVisibility(View.GONE);
    		chbQI1030A_H.setVisibility(View.GONE);
    		chbQI1030A_I.setVisibility(View.GONE);
    		chbQI1030A_J.setVisibility(View.GONE);
    		chbQI1030A_K.setVisibility(View.GONE);
    		chbQI1030A_L.setVisibility(View.GONE);
    		chbQI1030A_M.setVisibility(View.GONE);
    		chbQI1030A_X.setVisibility(View.GONE);
    		txtQI1030A_XI.setVisibility(View.GONE);
    	}
    }
    
    public void onchbQI1028_BChangeValue() {
    	if (chbQI1028_B.isChecked()){
    		Log.e("s","7");
    		Util.cleanAndLockView(getActivity(),chbQI1028_Y);
    		Util.lockView(getActivity(),false,lblP1030_2,chbQI1030B_A,chbQI1030B_B,chbQI1030B_C,chbQI1030B_D,chbQI1030B_E,chbQI1030B_F,chbQI1030B_G,chbQI1030B_H,chbQI1030B_I,chbQI1030B_J,chbQI1030B_K,chbQI1030B_L,chbQI1030B_M,chbQI1030B_X);
    		lblP1030_2.setVisibility(View.VISIBLE);
       		chbQI1030B_A.setVisibility(View.VISIBLE);
    		chbQI1030B_B.setVisibility(View.VISIBLE);
    		chbQI1030B_C.setVisibility(View.VISIBLE);
    		chbQI1030B_D.setVisibility(View.VISIBLE);
    		chbQI1030B_E.setVisibility(View.VISIBLE);
    		chbQI1030B_F.setVisibility(View.VISIBLE);
    		chbQI1030B_G.setVisibility(View.VISIBLE);
    		chbQI1030B_H.setVisibility(View.VISIBLE);
    		chbQI1030B_I.setVisibility(View.VISIBLE);
    		chbQI1030B_J.setVisibility(View.VISIBLE);
    		chbQI1030B_K.setVisibility(View.VISIBLE);
    		chbQI1030B_L.setVisibility(View.VISIBLE);
    		chbQI1030B_M.setVisibility(View.VISIBLE);
    		chbQI1030B_X.setVisibility(View.VISIBLE);
    		txtQI1030B_XI.setVisibility(View.VISIBLE);
    		onctxtQI1030B_XIChangeValue();
    	}
    	else{	 
    		Log.e("s","8");
    		Util.lockView(getActivity(),false,chbQI1028_Y);
    		Util.cleanAndLockView(getActivity(),lblP1030_2,chbQI1030B_A,chbQI1030B_B,chbQI1030B_C,chbQI1030B_D,chbQI1030B_E,chbQI1030B_F,chbQI1030B_G,chbQI1030B_H,chbQI1030B_I,chbQI1030B_J,chbQI1030B_K,chbQI1030B_L,chbQI1030B_M,chbQI1030B_X);
    		lblP1030_2.setVisibility(View.GONE);
    		chbQI1030B_A.setVisibility(View.GONE);
    		chbQI1030B_B.setVisibility(View.GONE);
    		chbQI1030B_C.setVisibility(View.GONE);
    		chbQI1030B_D.setVisibility(View.GONE);
    		chbQI1030B_E.setVisibility(View.GONE);
    		chbQI1030B_F.setVisibility(View.GONE);
    		chbQI1030B_G.setVisibility(View.GONE);
    		chbQI1030B_H.setVisibility(View.GONE);
    		chbQI1030B_I.setVisibility(View.GONE);
    		chbQI1030B_J.setVisibility(View.GONE);
    		chbQI1030B_K.setVisibility(View.GONE);
    		chbQI1030B_L.setVisibility(View.GONE);
    		chbQI1030B_M.setVisibility(View.GONE);
    		chbQI1030B_X.setVisibility(View.GONE);
    		txtQI1030B_XI.setVisibility(View.GONE);
    	}
    }
    
    public void onchbQI1028_XChangeValue() {
    	if (chbQI1028_X.isChecked()){ 
    		Log.e("s","9");
    		Util.cleanAndLockView(getActivity(),chbQI1028_Y);			
			Util.lockView(getActivity(),false,txtQI1028_XI,lblP1030_3,chbQI1030C_A,chbQI1030C_B,chbQI1030C_C,chbQI1030C_D,chbQI1030C_E,chbQI1030C_F,chbQI1030C_G,chbQI1030C_H,chbQI1030C_I,chbQI1030C_J,chbQI1030C_K,chbQI1030C_L,chbQI1030C_M,chbQI1030C_X);
			lblP1030_3.setVisibility(View.VISIBLE);
    		chbQI1030C_A.setVisibility(View.VISIBLE);
    		chbQI1030C_B.setVisibility(View.VISIBLE);
    		chbQI1030C_C.setVisibility(View.VISIBLE);
    		chbQI1030C_D.setVisibility(View.VISIBLE);
    		chbQI1030C_E.setVisibility(View.VISIBLE);
    		chbQI1030C_F.setVisibility(View.VISIBLE);
    		chbQI1030C_G.setVisibility(View.VISIBLE);
    		chbQI1030C_H.setVisibility(View.VISIBLE);
    		chbQI1030C_I.setVisibility(View.VISIBLE);
    		chbQI1030C_J.setVisibility(View.VISIBLE);
    		chbQI1030C_K.setVisibility(View.VISIBLE);
    		chbQI1030C_L.setVisibility(View.VISIBLE);
    		chbQI1030C_M.setVisibility(View.VISIBLE);
    		chbQI1030C_X.setVisibility(View.VISIBLE);
    		txtQI1030C_XI.setVisibility(View.VISIBLE);
    		onctxtQI1030C_XIChangeValue();
			txtQI1028_XI.requestFocus();
    	}
    	else{	    		
    		Log.e("s","10");
    		Util.lockView(getActivity(),false,chbQI1028_Y);
    		Util.cleanAndLockView(getActivity(),txtQI1028_XI,lblP1030_3,chbQI1030C_A,chbQI1030C_B,chbQI1030C_C,chbQI1030C_D,chbQI1030C_E,chbQI1030C_F,chbQI1030C_G,chbQI1030C_H,chbQI1030C_I,chbQI1030C_J,chbQI1030C_K,chbQI1030C_L,chbQI1030C_M,chbQI1030C_X);
    		lblP1030_3.setVisibility(View.GONE);
    		chbQI1030C_A.setVisibility(View.GONE);
    		chbQI1030C_B.setVisibility(View.GONE);
    		chbQI1030C_C.setVisibility(View.GONE);
    		chbQI1030C_D.setVisibility(View.GONE);
    		chbQI1030C_E.setVisibility(View.GONE);
    		chbQI1030C_F.setVisibility(View.GONE);
    		chbQI1030C_G.setVisibility(View.GONE);
    		chbQI1030C_H.setVisibility(View.GONE);
    		chbQI1030C_I.setVisibility(View.GONE);
    		chbQI1030C_J.setVisibility(View.GONE);
    		chbQI1030C_K.setVisibility(View.GONE);
    		chbQI1030C_L.setVisibility(View.GONE);
    		chbQI1030C_M.setVisibility(View.GONE);
    		chbQI1030C_X.setVisibility(View.GONE);
    		txtQI1030C_XI.setVisibility(View.GONE);
    	}
   
    }
    
    
    public void onctxtQI1030A_XIChangeValue() {
    	if (chbQI1030A_X.isChecked()) {
    		Util.lockView(getActivity(),false,txtQI1030A_XI);
		}
    	else{
    		Util.cleanAndLockView(getActivity(),txtQI1030A_XI);
    	}
    }
    
    public void onctxtQI1030B_XIChangeValue() {
    	if (chbQI1030B_X.isChecked()) {
    		Util.lockView(getActivity(),false,txtQI1030B_XI);
		}
    	else{
    		Util.cleanAndLockView(getActivity(),txtQI1030B_XI);
    	}
    }
    
    public void onctxtQI1030C_XIChangeValue() {
    	if (chbQI1030C_X.isChecked()) {
    		Util.lockView(getActivity(),false,txtQI1030C_XI);
		}
    	else{
    		Util.cleanAndLockView(getActivity(),txtQI1030C_XI);
    	}
    }
    
    
    public boolean verificarCheck() {
  		if (chbQI1028_A.isChecked() || chbQI1028_B.isChecked() || chbQI1028_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1025.readOnly();
    		rgQI1026.readOnly();
    		
    		rgQI1026A_A.readOnly();
    		rgQI1026A_B.readOnly();
    		rgQI1026A_C.readOnly();
    		
    		txtQI1028_XI.readOnly();
    		txtQI1030A_XI.readOnly();
    		txtQI1030B_XI.readOnly();
    		txtQI1030C_XI.readOnly();
    		chbQI1028_A.readOnly();
    		chbQI1028_B.readOnly();
    		chbQI1028_X.readOnly();
    		chbQI1028_Y.readOnly();
    		chbQI1030A_A.readOnly();
    		chbQI1030A_B.readOnly();
    		chbQI1030A_C.readOnly();
    		chbQI1030A_D.readOnly();
    		chbQI1030A_E.readOnly();
    		chbQI1030A_F.readOnly();
    		chbQI1030A_G.readOnly();
    		chbQI1030A_H.readOnly();
    		chbQI1030A_I.readOnly();
    		chbQI1030A_J.readOnly();
    		chbQI1030A_K.readOnly();
    		chbQI1030A_L.readOnly();
    		chbQI1030A_M.readOnly();
    		chbQI1030A_X.readOnly();
    		chbQI1030B_A.readOnly();
    		chbQI1030B_B.readOnly();
    		chbQI1030B_C.readOnly();
    		chbQI1030B_D.readOnly();
    		chbQI1030B_E.readOnly();
    		chbQI1030B_F.readOnly();
    		chbQI1030B_G.readOnly();
    		chbQI1030B_H.readOnly();
    		chbQI1030B_I.readOnly();
    		chbQI1030B_J.readOnly();
    		chbQI1030B_K.readOnly();
    		chbQI1030B_L.readOnly();
    		chbQI1030B_M.readOnly();
    		chbQI1030B_X.readOnly();
    		chbQI1030C_A.readOnly();
    		chbQI1030C_B.readOnly();
    		chbQI1030C_C.readOnly();
    		chbQI1030C_D.readOnly();
    		chbQI1030C_E.readOnly();
    		chbQI1030C_F.readOnly();
    		chbQI1030C_G.readOnly();
    		chbQI1030C_H.readOnly();
    		chbQI1030C_I.readOnly();
    		chbQI1030C_J.readOnly();
    		chbQI1030C_K.readOnly();
    		chbQI1030C_L.readOnly();
    		chbQI1030C_M.readOnly();
    		chbQI1030C_X.readOnly();
    		
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		
		if (individual.qi1025!=null)
			individual.qi1025=individual.getConvertQi1025(individual.qi1025);
		if (individual.qi1026!=null)
			individual.qi1026=individual.getConvertQi1026(individual.qi1026);
		if (individual.qi1026a_a!=null)
			individual.qi1026a_a=individual.getConvertQi1026a_a(individual.qi1026a_a);
		if (individual.qi1026a_b!=null)
			individual.qi1026a_b=individual.getConvertQi1026a_b(individual.qi1026a_b);
		if (individual.qi1026a_c!=null)
			individual.qi1026a_c=individual.getConvertQi1026a_c(individual.qi1026a_c);
		if (individual.qi1031!=null)
			individual.qi1031=individual.getConvertQi1031(individual.qi1031);
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}