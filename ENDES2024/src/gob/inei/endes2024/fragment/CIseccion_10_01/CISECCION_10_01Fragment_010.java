package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_010 extends FragmentForm {
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI1031;
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI1032_A;
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI1032_B;
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI1032_C;
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI1032_D;
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI1032_E;
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI1032_F;
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI1032_G;
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI1032_H;
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI1032_I;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI1032_J;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI1032_K;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI1032_L;
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI1032_M;
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI1032_N;
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI1032_X;
	@FieldAnnotation(orderIndex=17) 
	public TextField txtQI1032_XI;
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI1032_Y;
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQI1033;
	
	CISECCION_10_01 individual;
	CISECCION_05_07 individualS5;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1031,lblpregunta1032,lblpregunta1033,lblpregunta1033_ind1,lblpregunta1033_ind2;
	public TextField txtCabecera;
	LinearLayout q0,q1,q2,q3;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5;
	private Calendar fecha_actual =null; 
	public CISECCION_10_01Fragment_010() {}
	public CISECCION_10_01Fragment_010 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1000A","QI1031","QI1032_A","QI1032_B","QI1032_C","QI1032_D","QI1032_E","QI1032_F","QI1032_G","QI1032_H","QI1032_I","QI1032_J","QI1032_K","QI1032_L","QI1032_M","QI1032_N","QI1032_X","QI1032_XI","QI1032_Y","QI1033","QI1028_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1031","QI1032_A","QI1032_B","QI1032_C","QI1032_D","QI1032_E","QI1032_F","QI1032_G","QI1032_H","QI1032_I","QI1032_J","QI1032_K","QI1032_L","QI1032_M","QI1032_N","QI1032_X","QI1032_XI","QI1032_Y","QI1033")};
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI707_CONS")};
		return rootView;
	}
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta1031 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1031);
		lblpregunta1032 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1032);
		lblpregunta1033 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1033);
		lblpregunta1033_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1033_ind1).negrita();
		lblpregunta1033_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1033_ind2);
		
		rgQI1031=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1031_1,R.string.ciseccion_10_01qi1031_2,R.string.ciseccion_10_01qi1031_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		chbQI1032_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_AChangeValue");
		chbQI1032_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_BChangeValue");
		chbQI1032_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_CChangeValue");
		chbQI1032_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_DChangeValue");
		chbQI1032_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_EChangeValue");
		chbQI1032_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_FChangeValue");
		chbQI1032_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_GChangeValue");
		chbQI1032_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_HChangeValue");
		chbQI1032_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_IChangeValue");
		chbQI1032_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_JChangeValue");
		chbQI1032_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_KChangeValue");
		chbQI1032_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_LChangeValue");
		chbQI1032_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_MhangeValue");
		chbQI1032_N=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_NChangeValue");
		chbQI1032_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_XChangeValue");
		txtQI1032_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		chbQI1032_Y=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1032_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1032_ZChangeValue");
		rgQI1033=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1033_1,R.string.ciseccion_10_01qi1033_2,R.string.ciseccion_10_01qi1033_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
   
    }
    
    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(lblpregunta1031,rgQI1031);
		LinearLayout ly1032 = new LinearLayout(getActivity());
		ly1032.addView(chbQI1032_X);
		ly1032.addView(txtQI1032_XI);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1032,chbQI1032_A,chbQI1032_B,chbQI1032_C,chbQI1032_D,chbQI1032_E,chbQI1032_F,chbQI1032_G,chbQI1032_H,chbQI1032_I,chbQI1032_J,chbQI1032_K,chbQI1032_L,chbQI1032_M,chbQI1032_N,ly1032,chbQI1032_Y);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1033,lblpregunta1033_ind1,lblpregunta1033_ind2,rgQI1033);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		 
		return contenedor;
    }
    @Override
    public boolean grabar() {

		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		if(App.getInstance().getCiseccion_10_01()!=null){
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);			
		}
		else{
			App.getInstance().setCiseccion_10_01(individual);
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);
		}
		
		
		
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1028Y=individual.qi1028_y==null?0:individual.qi1028_y;
		
		if (getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id) && in1028Y!=1 ) {
			if (Util.esVacio(individual.qi1031)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1031");
				view = rgQI1031;
				error = true;
				return false;
			}
		}
			if (!verificarCheck() && !chbQI1032_Y.isChecked()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1032_A");
				view = chbQI1032_A;
				error = true;
				return false;
			}
			if (chbQI1032_X.isChecked()) {
				if (Util.esVacio(individual.qi1032_xi)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1032_XI");
					view = txtQI1032_XI;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi1033)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1033");
				view = rgQI1033;
				error = true;
				return false;
			}

			return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
		if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if(individualS5!=null && individualS5.qi707_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individualS5.qi707_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		App.getInstance().getCiseccion_10_01().qi1000a=individual.qi1000a;

		entityToUI(individual);
		RenombrarEtiquetas();
		inicio();
    }
    
    
    public void validarPregunta1027(){
    	Integer in1028Y=individual.qi1028_y==null?0:individual.qi1028_y;
    	
    	if (!getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id) || in1028Y==1 ) {
			Util.cleanAndLockView(getActivity(),rgQI1031);
    		q1.setVisibility(View.GONE);
		}
    	else {
    		Util.lockView(getActivity(), false,rgQI1031);
			q1.setVisibility(View.VISIBLE);
    	}
    }
    
    
    private void inicio() {
    	validarPregunta1027();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    public void onchbQI1032_AChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_BChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_CChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_DChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_EChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_FChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_GChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_HChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_IChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_JChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_KChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_LChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_MChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    public void onchbQI1032_NChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    }
    
    
    public void onchbQI1032_XChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI1032_Y);
    	else				  Util.lockView(getActivity(),false,chbQI1032_Y);
    	if (chbQI1032_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI1032_XI);
  			txtQI1032_XI.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI1032_XI);
  		}
    }
    public void onchbQI1032_YChangeValue() {
    	if (chbQI1032_Y.isChecked()){
    		Util.cleanAndLockView(getActivity(),chbQI1032_A,chbQI1032_B,chbQI1032_C,chbQI1032_D,chbQI1032_E,chbQI1032_F,chbQI1032_G,chbQI1032_H,chbQI1032_I,chbQI1032_J,chbQI1032_K,chbQI1032_L,chbQI1032_M,chbQI1032_N,chbQI1032_X);
    	}
  		else{
  			Util.lockView(getActivity(),false,chbQI1032_A,chbQI1032_B,chbQI1032_C,chbQI1032_D,chbQI1032_E,chbQI1032_F,chbQI1032_G,chbQI1032_H,chbQI1032_I,chbQI1032_J,chbQI1032_K,chbQI1032_L,chbQI1032_M,chbQI1032_N,chbQI1032_X);
  			if (chbQI1032_X.isChecked())
  				Util.lockView(getActivity(),false,txtQI1032_XI);
  			else	Util.cleanAndLockView(getActivity(),txtQI1032_XI);
  		}
    }
    public boolean verificarCheck() {
  		if (chbQI1032_A.isChecked() || chbQI1032_B.isChecked() || chbQI1032_C.isChecked() || chbQI1032_D.isChecked() || chbQI1032_E.isChecked() || chbQI1032_F.isChecked() || chbQI1032_G.isChecked() || chbQI1032_H.isChecked() || chbQI1032_I.isChecked() || chbQI1032_J.isChecked() || chbQI1032_K.isChecked() || chbQI1032_L.isChecked() || chbQI1032_M.isChecked() || chbQI1032_N.isChecked() || chbQI1032_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    public void RenombrarEtiquetas() {
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fecha_actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecha_actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH)==0?11:fechados.get(Calendar.MONTH)-1);
    	lblpregunta1031.text(R.string.ciseccion_10_01qi1031);
    	lblpregunta1031.setText(lblpregunta1031.getText().toString().replace("#", F2));
    	//lblpregunta1031.setText(lblpregunta1031.getText().toString().replace("$", F1));
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1031.readOnly();
    		rgQI1033.readOnly();
    		txtQI1032_XI.readOnly();
    		chbQI1032_A.readOnly();
    		chbQI1032_B.readOnly();
    		chbQI1032_C.readOnly();
    		chbQI1032_D.readOnly();
    		chbQI1032_E.readOnly();
    		chbQI1032_F.readOnly();
    		chbQI1032_G.readOnly();
    		chbQI1032_H.readOnly();
    		chbQI1032_I.readOnly();
    		chbQI1032_J.readOnly();
    		chbQI1032_K.readOnly();
    		chbQI1032_L.readOnly();
    		chbQI1032_M.readOnly();
    		chbQI1032_N.readOnly();
    		chbQI1032_X.readOnly();
    		chbQI1032_Y.readOnly();
    	}
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		if(App.getInstance().getCiseccion_10_01()!=null){
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);			
		}
		else{
			App.getInstance().setCiseccion_10_01(individual);
			App.getInstance().getCiseccion_10_01().filtro1034 = getCuestionarioService().getNacimientosMayoresaunAnio(individual.id,individual.hogar_id, individual.persona_id);
		}
		return App.INDIVIDUAL;
	}
}