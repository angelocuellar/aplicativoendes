package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_008 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1022;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI1023_A;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI1023_B;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI1023_C;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI1023_D;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI1023_E;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI1023_F;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI1023_G;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI1023_H;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI1023_I;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI1023_J;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI1023_K;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI1023_L;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI1023_M;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI1023_X;
	@FieldAnnotation(orderIndex=16)
	public TextField txtQI1023_XI;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI1023A_A;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI1023A_B;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI1023A_C;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI1023A_D;
	@FieldAnnotation(orderIndex=21)
	public CheckBoxField chbQI1023A_E;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI1023A_F;
	@FieldAnnotation(orderIndex=23)
	public CheckBoxField chbQI1023A_G;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI1023A_H;
	@FieldAnnotation(orderIndex=25)
	public CheckBoxField chbQI1023A_X;
	@FieldAnnotation(orderIndex=26)
	public TextField txtQI1023A_XI;
	@FieldAnnotation(orderIndex=27)
	public CheckBoxField chbQI1023A_Z;
	@FieldAnnotation(orderIndex=28)
	public RadioGroupOtherField rgQI1024;
	@FieldAnnotation(orderIndex=29)
	public TextField txtQI1024X;
	
	CISECCION_10_01 individual;
	CISECCION_01_03 individualS1;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta1022,lblpregunta1023,lblpregunta1023A,lblpregunta1024;
	public TextField txtCabecera;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS1;

	public CISECCION_10_01Fragment_008() {}
	public CISECCION_10_01Fragment_008 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1022","QI1023_A","QI1023_B","QI1023_C","QI1023_D","QI1023_E","QI1023_F","QI1023_G","QI1023_H","QI1023_I","QI1023_J","QI1023_K","QI1023_L","QI1023_M","QI1023_X","QI1023_XI","QI1023A_A","QI1023A_B","QI1023A_C","QI1023A_D","QI1023A_E","QI1023A_F","QI1023A_G","QI1023A_H","QI1023A_X","QI1023A_XI","QI1023A_Z","QI1024","QI1024X","qi1013a","qi1013aa","qi1013b","QI1019","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1022","QI1023_A","QI1023_B","QI1023_C","QI1023_D","QI1023_E","QI1023_F","QI1023_G","QI1023_H","QI1023_I","QI1023_J","QI1023_K","QI1023_L","QI1023_M","QI1023_X","QI1023_XI","QI1023A_A","QI1023A_B","QI1023A_C","QI1023A_D","QI1023A_E","QI1023A_F","QI1023A_G","QI1023A_H","QI1023A_X","QI1023A_XI","QI1023A_Z","QI1024","QI1024X")};
		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI201","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView;
	}
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1022 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1022);
		lblpregunta1023 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1023);
		lblpregunta1023A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1023a);
		lblpregunta1024 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1024);
		
		rgQI1022=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1022_1,R.string.ciseccion_10_01qi1022_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI1022ChangeValue");
		
		chbQI1023_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI1023_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023_XChangeValue");
		txtQI1023_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		chbQI1023A_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_AChangeValue");
		chbQI1023A_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_BChangeValue");
		chbQI1023A_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_CChangeValue");
		chbQI1023A_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_DChangeValue");
		chbQI1023A_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_EChangeValue");
		chbQI1023A_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_FChangeValue");
		chbQI1023A_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_GChangeValue");
		chbQI1023A_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_HChangeValue");
		chbQI1023A_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_XChangeValue");
		chbQI1023A_Z=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1023a_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1023A_ZChangeValue");
		txtQI1023A_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		rgQI1024=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1024_1,R.string.ciseccion_10_01qi1024_2,R.string.ciseccion_10_01qi1024_3,R.string.ciseccion_10_01qi1024_4,R.string.ciseccion_10_01qi1024_5,R.string.ciseccion_10_01qi1024_6,R.string.ciseccion_10_01qi1024_7,R.string.ciseccion_10_01qi1024_8,R.string.ciseccion_10_01qi1024_9,R.string.ciseccion_10_01qi1024_11,R.string.ciseccion_10_01qi1024_12,R.string.ciseccion_10_01qi1024_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI1024X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI1024.agregarEspecifique(11,txtQI1024X);//9
    }
  
    @Override 
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1022,rgQI1022);
		
		LinearLayout ly1023 = new LinearLayout(getActivity());
		ly1023.addView(chbQI1023_X);
		ly1023.addView(txtQI1023_XI);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1023,chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,ly1023);
		
		LinearLayout ly1023A = new LinearLayout(getActivity());
		ly1023A.addView(chbQI1023A_X);
		ly1023A.addView(txtQI1023A_XI);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1023A,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,ly1023A,chbQI1023A_Z);
		
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta1024,rgQI1024);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		
		if (individual.qi1024!=null)
			individual.qi1024=individual.getConvertQi1024(individual.qi1024);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1013=individual.qi1013a==null?0:individual.qi1013a;
		
		Integer in1013aa=individual.qi1013aa==null?0:individual.qi1013aa;
		Integer in1013b=individual.qi1013b==null?0:individual.qi1013b;
		
    	Integer in1019=individual.qi1019==null?0:individual.qi1019;
    	Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
		
		if (in1013==1 || in1013aa ==1 || in1013b==1 || infiltro1005!=0 || in1019==1 ) {
			if (Util.esVacio(individual.qi1022)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1022");
				view = rgQI1022;
				error = true;
				return false;
			}
			if (!Util.esDiferente(individual.qi1022,1)) {
				if (!verificarCheck1023()) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1023_A");
					view = chbQI1023_A;
					error = true;
					return false;
				}
				if (chbQI1023_X.isChecked()) {
					if (Util.esVacio(individual.qi1023_xi)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1023_XI");
						view = txtQI1023_XI;
						error = true;
						return false;
					}
				}
			}
			
			if (!verificarCheck1023A() && !verificarCheck1023A_Z()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1023A_A");
				view = chbQI1023A_A;
				error = true;
				return false;
			}
			if (chbQI1023A_X.isChecked()) {
				if (Util.esVacio(individual.qi1023a_xi)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1023A_XI");
					view = txtQI1023A_XI;
					error = true;
					return false;
				}
			}
			
			if (chbQI1023A_Z.isChecked()) {
				if (Util.esVacio(individual.qi1024)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI1024");
					view = rgQI1024;
					error = true;
					return false;
				}					
				
				if (individual.qi1024==96) {
					if (Util.esVacio(individual.qi1024x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI1024X");
						view = txtQI1024X;
						error = true;
						return false;
					}
				}
						
			}
		}
		
			
		
		
		return true;
    }
    
    @Override 
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS1 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1);
		
    	
    	if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
		if(individual.qi1024!=null)
			individual.qi1024=individual.setConvertQi1024(individual.qi1024);
		
		App.getInstance().getCiseccion_10_01().qi1013a=individual.qi1013a;
		App.getInstance().getCiseccion_10_01().qi1013aa=individual.qi1013aa;
		App.getInstance().getCiseccion_10_01().qi1013b=individual.qi1013b;
		
		App.getInstance().getCiseccion_10_01().qi1019=individual.qi1019;
		App.getInstance().getPersonaCuestionarioIndividual().qi201=individualS1.qi201;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individualS1.qi226;
    	App.getInstance().getPersonaCuestionarioIndividual().qi230=individualS1.qi230;
		
		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta1022(){
    	Integer in1013=individual.qi1013a==null?0:individual.qi1013a;
    	Integer in1013aa=individual.qi1013aa==null?0:individual.qi1013aa;
    	Integer in1013b=individual.qi1013b==null?0:individual.qi1013b;
    	
    	Integer in1019=individual.qi1019==null?0:individual.qi1019;
    	Integer infiltro1005=App.getInstance().getCiseccion_10_01().filtro1005==null?0:App.getInstance().getCiseccion_10_01().filtro1005;
    	
    	
    	if( in1013 != 0){
    		if (((in1013==2 || in1013==3) && infiltro1005==0 && in1019==2)/*||((in1013aa==2 || in1013aa==3) && infiltro1005==0 && in1019==2)||((in1013b==2 || in1013b==3) && infiltro1005==0 && in1019==2) */) {
        		Util.cleanAndLockView(getActivity(),rgQI1022,chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,chbQI1023_X,txtQI1023_XI,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.GONE);
        		q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.GONE);
    			q4.setVisibility(View.GONE);
    		}
        	else{
        		Util.lockView(getActivity(), false,rgQI1022,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			onrgQI1022ChangeValue();
    			onchbQI1023A_AChangeValue();
    			onchbQI1023A_BChangeValue();
    			onchbQI1023A_CChangeValue();
    			onchbQI1023A_DChangeValue();
    			onchbQI1023A_EChangeValue();
    			onchbQI1023A_FChangeValue();
    			onchbQI1023A_GChangeValue();
    			onchbQI1023A_HChangeValue();
    			onchbQI1023A_XChangeValue();
    			onchbQI1023A_ZChangeValue();
        	}
    	}
    	
    	if( in1013aa != 0){
    		if (((in1013aa==2 || in1013aa==3) && infiltro1005==0 && in1019==2)/*||((in1013aa==2 || in1013aa==3) && infiltro1005==0 && in1019==2)||((in1013b==2 || in1013b==3) && infiltro1005==0 && in1019==2) */) {
        		Util.cleanAndLockView(getActivity(),rgQI1022,chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,chbQI1023_X,txtQI1023_XI,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.GONE);
        		q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.GONE);
    			q4.setVisibility(View.GONE);
    		}
        	else{
        		Util.lockView(getActivity(), false,rgQI1022,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			onrgQI1022ChangeValue();
    			onchbQI1023A_AChangeValue();
    			onchbQI1023A_BChangeValue();
    			onchbQI1023A_CChangeValue();
    			onchbQI1023A_DChangeValue();
    			onchbQI1023A_EChangeValue();
    			onchbQI1023A_FChangeValue();
    			onchbQI1023A_GChangeValue();
    			onchbQI1023A_HChangeValue();
    			onchbQI1023A_XChangeValue();
    			onchbQI1023A_ZChangeValue();
        	}
    	}
    	
    	
    	if( in1013b != 0){
    		if (((in1013b==2 || in1013b==3) && infiltro1005==0 && in1019==2)/*||((in1013aa==2 || in1013aa==3) && infiltro1005==0 && in1019==2)||((in1013b==2 || in1013b==3) && infiltro1005==0 && in1019==2) */) {
        		Util.cleanAndLockView(getActivity(),rgQI1022,chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,chbQI1023_X,txtQI1023_XI,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.GONE);
        		q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.GONE);
    			q4.setVisibility(View.GONE);
    		}
        	else{
        		Util.lockView(getActivity(), false,rgQI1022,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X,chbQI1023A_Z,rgQI1024,txtQI1024X);
        		q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			onrgQI1022ChangeValue();
    			onchbQI1023A_AChangeValue();
    			onchbQI1023A_BChangeValue();
    			onchbQI1023A_CChangeValue();
    			onchbQI1023A_DChangeValue();
    			onchbQI1023A_EChangeValue();
    			onchbQI1023A_FChangeValue();
    			onchbQI1023A_GChangeValue();
    			onchbQI1023A_HChangeValue();
    			onchbQI1023A_XChangeValue();
    			onchbQI1023A_ZChangeValue();
        	}
    	}
    	
    }
    
    
    private void inicio() {
    	validarPregunta1022();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    
    public void onrgQI1022ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI1022.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,chbQI1023_X);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,chbQI1023_A,chbQI1023_B,chbQI1023_C,chbQI1023_D,chbQI1023_E,chbQI1023_F,chbQI1023_G,chbQI1023_H,chbQI1023_I,chbQI1023_J,chbQI1023_K,chbQI1023_L,chbQI1023_M,chbQI1023_X);
			q2.setVisibility(View.VISIBLE);
			if (chbQI1023_X.isChecked())
				Util.lockView(getActivity(), false,txtQI1023_XI);
			else	Util.cleanAndLockView(getActivity(),txtQI1023_XI);
		}
    }
    
    public void onchbQI1023_XChangeValue() {
    	if (chbQI1023_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI1023_XI);
  			txtQI1023_XI.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI1023_XI);
  		}
    }
    
    public void onchbQI1023A_AChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_BChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_CChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_DChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_EChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_FChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_GChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_HChangeValue() {
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
    public void onchbQI1023A_XChangeValue() {
    	if(chbQI1023A_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI1023A_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI1023A_XI);
    	}
    	if (verificarCheck1023A()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(),false,chbQI1023A_Z,rgQI1024);
			q4.setVisibility(View.GONE);
    	}   
    }
    
   
    public void onchbQI1023A_ZChangeValue() {
    	if (chbQI1023A_Z.isChecked()){
    		Util.cleanAndLockView(getActivity(),chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X);
    		Util.lockView(getActivity(), false,rgQI1024);
    		q4.setVisibility(View.VISIBLE);
    	}
  		else{
  			Util.lockView(getActivity(),false,chbQI1023A_A,chbQI1023A_B,chbQI1023A_C,chbQI1023A_D,chbQI1023A_E,chbQI1023A_F,chbQI1023A_G,chbQI1023A_H,chbQI1023A_X);
  			Util.cleanAndLockView(getActivity(),rgQI1024);
			q4.setVisibility(View.GONE);
  		}
    }
    
    
    public boolean verificarCheck1023() {
  		if (chbQI1023_A.isChecked() || chbQI1023_B.isChecked() || chbQI1023_C.isChecked() || chbQI1023_D.isChecked() || chbQI1023_E.isChecked() || chbQI1023_F.isChecked() || chbQI1023_G.isChecked() || chbQI1023_H.isChecked() || chbQI1023_I.isChecked() || chbQI1023_J.isChecked() || chbQI1023_K.isChecked() || chbQI1023_L.isChecked() || chbQI1023_M.isChecked() || chbQI1023_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public boolean verificarCheck1023A() {
  		if (chbQI1023A_A.isChecked() || chbQI1023A_B.isChecked() || chbQI1023A_C.isChecked() || chbQI1023A_D.isChecked() || chbQI1023A_E.isChecked() || chbQI1023A_F.isChecked() || chbQI1023A_G.isChecked() || chbQI1023A_H.isChecked() || chbQI1023A_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public boolean verificarCheck1023A_Z() {
  		if (chbQI1023A_Z.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1022.readOnly();
    		rgQI1024.readOnly();
    		txtQI1023_XI.readOnly();
    		txtQI1023A_XI.readOnly();
    		txtQI1024X.readOnly();
    		chbQI1023_A.readOnly();
    		chbQI1023_B.readOnly();
    		chbQI1023_C.readOnly();
    		chbQI1023_D.readOnly();
    		chbQI1023_E.readOnly();
    		chbQI1023_F.readOnly();
    		chbQI1023_G.readOnly();
    		chbQI1023_H.readOnly();
    		chbQI1023_I.readOnly();
    		chbQI1023_J.readOnly();
    		chbQI1023_K.readOnly();
    		chbQI1023_L.readOnly();
    		chbQI1023_M.readOnly();
    		chbQI1023_X.readOnly();
    		chbQI1023A_A.readOnly();
    		chbQI1023A_B.readOnly();
    		chbQI1023A_C.readOnly();
    		chbQI1023A_D.readOnly();
    		chbQI1023A_E.readOnly();
    		chbQI1023A_F.readOnly();
    		chbQI1023A_G.readOnly();
    		chbQI1023A_H.readOnly();
    		chbQI1023A_X.readOnly();
    		chbQI1023A_Z.readOnly();
    		
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		
		if (individual.qi1024!=null)
			individual.qi1024=individual.getConvertQi1024(individual.qi1024);
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}