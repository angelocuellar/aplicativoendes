package gob.inei.endes2024.fragment.CIseccion_10_01;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.annotations.FieldEntity;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_012  extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI1041_1;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI1041_2;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI1042_1;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI1042_2;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI1042_3;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI1042A_1A;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI1042A_1B;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI1042A_2A;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI1042A_2B;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI1042A_3A;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI1042A_3B;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI1042A_4A;
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI1042A_4B;
	@FieldAnnotation(orderIndex=14)
	public RadioGroupOtherField rgQI1042A_5A;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI1042A_5B;
	@FieldAnnotation(orderIndex=16)
	public RadioGroupOtherField rgQI1042A_6A;
	@FieldAnnotation(orderIndex=17)
	public RadioGroupOtherField rgQI1042A_6B;
	@FieldAnnotation(orderIndex=18)
	public RadioGroupOtherField rgQI1042A_7A;
	@FieldAnnotation(orderIndex=19)
	public RadioGroupOtherField rgQI1042A_7B;
	@FieldAnnotation(orderIndex=20)
	public RadioGroupOtherField rgQI1042A_8A;
	@FieldAnnotation(orderIndex=21)
	public RadioGroupOtherField rgQI1042A_8B;
	
	
	CISECCION_10_01 individual;
	CISECCION_01_03 secccion1_3;
	CISECCION_08 secccion1_8;
	public TextField txtCabecera;
	public ButtonComponent btnHoraFin;
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	private List<CARATULA_INDIVIDUAL> visitas;
	private CARATULA_INDIVIDUAL ultimaVisita;
	private LabelComponent lblTitulo,lblhoras,lblpregunta1041,lblpregunta1042,lblpregunta1043,lblpreguntaInd;
	private LabelComponent lblblanco1,lblP1041_1,lblP1041_2,lblP1041_3,lblQI1041_1,lblQI1041_2,lblblanco2,lblP1042_1,lblP1042_2,lblP1042_3,lblQI1042_1,lblQI1042_2,lblQI1042_3,lblhora;
	private LabelComponent lblpregunta1042a,lblespacio,lblespacio1,lblpregunta1042a_a,lblpregunta1042a_b,lblpregunta1042a_1,lblpregunta1042a_2,lblpregunta1042a_3,lblpregunta1042a_4,lblpregunta1042a_5,lblpregunta1042a_6,lblpregunta1042a_7,lblpregunta1042a_8,lblpregunta1042a_indica1,lblpregunta1042a_indica2;
	public GridComponent2 gridPreguntas1041,gridPreguntas1042,gridPreguntas1043,gridPreguntas1042a;
	LinearLayout q0,q1,q2,q3,q4;
	public boolean dialogoAbierto = false;
	public int existeninio=0;

	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoVisita,seccionesCargadoS1_3,seccionesCargadoS8;

	public CISECCION_10_01Fragment_012() {}
	public CISECCION_10_01Fragment_012 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1041_1", "QI1041_2", "QI1042_1", "QI1042_2", "QI1042_3", "QI1043", "QI1043_1","QI1042A_1A","QI1042A_1B" ,"QI1042A_2A","QI1042A_2B" ,"QI1042A_3A","QI1042A_3B" ,"QI1042A_4A","QI1042A_4B" ,"QI1042A_5A","QI1042A_5B" ,"QI1042A_6A","QI1042A_6B" ,"QI1042A_7A","QI1042A_7B" ,"QI1042A_8A","QI1042A_8B","QI1000A","ID", "HOGAR_ID", "PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1041_1", "QI1041_2", "QI1042_1", "QI1042_2", "QI1042_3", "QI1043", "QI1043_1","QI1042A_1A","QI1042A_1B" ,"QI1042A_2A","QI1042A_2B" ,"QI1042A_3A","QI1042A_3B" ,"QI1042A_4A","QI1042A_4B" ,"QI1042A_5A","QI1042A_5B" ,"QI1042A_6A","QI1042A_6B" ,"QI1042A_7A","QI1042A_7B" ,"QI1042A_8A","QI1042A_8B")};
		seccionesCargadoVisita = new SeccionCapitulo[] {new SeccionCapitulo(0, -1,-1, "QIVDIA","QIVMES","QIVANIO","QIVHORA","QIVMIN","QIVHORA_FIN","QIVMIN_FIN","QIVENTREV","QIVRESUL","QIVRESUL_O","QIPROX_DIA","QIPROX_MES","QIPROX_ANIO","QIPROX_HORA","QIPROX_MINUTO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesCargadoS1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI201","QI206","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoS8 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI817B","QI817C","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
	}
    
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta1041 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1041);
		lblpregunta1042 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1042);
		lblpreguntaInd  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1042_indi).negrita();
		lblpregunta1043 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1043);
		
		lblblanco1 = new LabelComponent(getActivity()).textSize(15).size(60, 210).negrita().centrar();
		lblP1041_1 = new LabelComponent(getActivity()).textSize(15).size(60, 110).text(R.string.ciseccion_10_01qi1041_s1).centrar().negrita();
		lblP1041_2 = new LabelComponent(getActivity()).textSize(15).size(60, 110).text(R.string.ciseccion_10_01qi1041_s2).centrar().negrita();
		lblP1041_3 = new LabelComponent(getActivity()).textSize(15).size(60, 110).text(R.string.ciseccion_10_01qi1041_s3).centrar().negrita();
		
		lblQI1041_1 =new LabelComponent(getActivity()).textSize(15).size(130, 300).text(R.string.ciseccion_10_01qi1041_p1);
		lblQI1041_2 =new LabelComponent(getActivity()).textSize(15).size(130, 300).text(R.string.ciseccion_10_01qi1041_p2);
		
		rgQI1041_1=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1041_so1,R.string.ciseccion_10_01qi1041_so2,R.string.ciseccion_10_01qi1041_so3).size(60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1041_2=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1041_so1,R.string.ciseccion_10_01qi1041_so2,R.string.ciseccion_10_01qi1041_so3).size(60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		
		lblblanco2 = new LabelComponent(getActivity()).textSize(15).size(60, 270).negrita().centrar();
		lblP1042_1 = new LabelComponent(getActivity()).textSize(15).size(60, 130).text(R.string.ciseccion_10_01qi1042_s1).centrar().negrita();
		lblP1042_2 = new LabelComponent(getActivity()).textSize(15).size(60, 130).text(R.string.ciseccion_10_01qi1042_s2).centrar().negrita();
		lblP1042_3 = new LabelComponent(getActivity()).textSize(15).size(60, 130).text(R.string.ciseccion_10_01qi1042_s3).centrar().negrita();
		
		lblQI1042_1 =new LabelComponent(getActivity()).textSize(15).size(130, 300).text(R.string.ciseccion_10_01qi1042_p1);
		lblQI1042_2 =new LabelComponent(getActivity()).textSize(15).size(130, 300).text(R.string.ciseccion_10_01qi1042_p2);
		lblQI1042_3 =new LabelComponent(getActivity()).textSize(15).size(130, 300).text(R.string.ciseccion_10_01qi1042_p3);
	
		lblpregunta1042a =new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01qi1042a).textSize(17).alinearIzquierda();
		lblpregunta1042a_a =new LabelComponent(this.getActivity()).size(altoComponente, 240).text(R.string.ciseccion_10_01qi1042a_a).textSize(15).centrar();
		lblpregunta1042a_b =new LabelComponent(this.getActivity()).size(altoComponente, 240).text(R.string.ciseccion_10_01qi1042a_b).textSize(15).alinearIzquierda();
		
		lblpregunta1042a_1 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_1).textSize(15).alinearIzquierda();
		lblpregunta1042a_2 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_2).textSize(15).alinearIzquierda();
		lblpregunta1042a_3 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_3).textSize(15).alinearIzquierda();
		lblpregunta1042a_4 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_4).textSize(15).alinearIzquierda();
		lblpregunta1042a_5 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_5).textSize(15).alinearIzquierda();
		lblpregunta1042a_6 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_6).textSize(15).alinearIzquierda();
		lblpregunta1042a_7 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_7).textSize(15).alinearIzquierda();
		lblpregunta1042a_8 =new LabelComponent(this.getActivity()).size(130, 250).text(R.string.ciseccion_10_01qi1042a_8).textSize(15).alinearIzquierda();
		
		lblpregunta1042a_indica1 =new LabelComponent(this.getActivity()).size(altoComponente, 190).text(R.string.ciseccion_10_01qi1042a_indicacion).textSize(11).centrar().negrita();
		lblpregunta1042a_indica2 =new LabelComponent(this.getActivity()).size(altoComponente, 240).text(R.string.ciseccion_10_01qi1042a_indicacion).textSize(11).centrar().negrita();
		
		rgQI1042_1=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042_so1,R.string.ciseccion_10_01qi1042_so2,R.string.ciseccion_10_01qi1042_so3).size(60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042_2=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042_so1,R.string.ciseccion_10_01qi1042_so2,R.string.ciseccion_10_01qi1042_so3).size(60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042_3=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042_so1,R.string.ciseccion_10_01qi1042_so2,R.string.ciseccion_10_01qi1042_so3).size(60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQI1043  =new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).readOnly().centrar();
//		txtQI1043_1=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).readOnly().centrar();
		
		btnHoraFin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.hogarqh110_terminar).size(150, 55);
		lblhora  = new LabelComponent(this.getActivity()).size(altoComponente, 210).text(R.string.ciseccion_10_01qi1043_1).textSize(17).centrar();
		lblhoras = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 120).text("").centrar();

		btnHoraFin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
					Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
					if(
							
						( (in1000a==2 || App.getInstance().getQhviolen()!=App.getInstance().getPersonaCuestionarioIndividual().persona_id) 
					      || (in1000a==1 && App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id 
					      && MyUtil.incluyeRango(1,3,rgQI1041_1.getTagSelected("").toString())
					      && MyUtil.incluyeRango(1,3,rgQI1041_2.getTagSelected("").toString())
					      && MyUtil.incluyeRango(1,3,rgQI1042_1.getTagSelected("").toString())
					      && MyUtil.incluyeRango(1,3,rgQI1042_2.getTagSelected("").toString())
					      && MyUtil.incluyeRango(1,3,rgQI1042_3.getTagSelected("").toString())
					      
						))	
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_1A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_2A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_3A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_4A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_5A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_6A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_7A.getTagSelected("").toString())
						&&	MyUtil.incluyeRango(1,3,rgQI1042A_8A.getTagSelected("").toString())
						
						&& (existeninio==0 || (existeninio>0 && 
								                  MyUtil.incluyeRango(1,3,rgQI1042A_1B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_2B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_3B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_4B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_5B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_6B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_7B.getTagSelected("").toString()) 
								               && MyUtil.incluyeRango(1,3,rgQI1042A_8B.getTagSelected("").toString()) ))
					) {
						Calendar calendario = new GregorianCalendar();
						Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
						Integer minute= calendario.get(Calendar.MINUTE);
						String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
						String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
						lblhoras.setText(muestrahora+":"+muestraminuto);
						CompletarVisitaCuestionarioIndividual();
					}
					else {
						ToastMessage.msgBox(getActivity(), "Debe registar todas las preguntas", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					}
				}
		});
	
		rgQI1042A_1A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_1B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_2A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(80,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_2B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_3A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_3B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_4A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_4B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_5A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(80,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_5B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(80,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_6A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_6B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_7A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(80,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_7B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_8A= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(80,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI1042A_8B= new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1042a_1a_1,R.string.ciseccion_10_01qi1042a_1a_2,R.string.ciseccion_10_01qi1042a_1a_3).size(60,240).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		
		lblespacio=new LabelComponent(this.getActivity()).size(altoComponente, 250).text("").textSize(16).centrar();
		lblespacio1=new LabelComponent(this.getActivity()).size(altoComponente, 250).text("").textSize(16).centrar();
		gridPreguntas1041=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
//		gridPreguntas1041.addComponent(lblblanco1);
//		gridPreguntas1041.addComponent(lblP1041_1);
//		gridPreguntas1041.addComponent(lblP1041_2);
//		gridPreguntas1041.addComponent(lblP1041_3);
		gridPreguntas1041.addComponent(lblQI1041_1);
		gridPreguntas1041.addComponent(rgQI1041_1);
		gridPreguntas1041.addComponent(lblQI1041_2);
		gridPreguntas1041.addComponent(rgQI1041_2);
		
		gridPreguntas1042=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
//		gridPreguntas1042.addComponent(lblblanco2);
//		gridPreguntas1042.addComponent(lblP1042_1);
//		gridPreguntas1042.addComponent(lblP1042_2);
//		gridPreguntas1042.addComponent(lblP1042_3);
		gridPreguntas1042.addComponent(lblQI1042_1);
		gridPreguntas1042.addComponent(rgQI1042_1);
		gridPreguntas1042.addComponent(lblQI1042_2);
		gridPreguntas1042.addComponent(rgQI1042_2);
		gridPreguntas1042.addComponent(lblQI1042_3);
		gridPreguntas1042.addComponent(rgQI1042_3);
		
		gridPreguntas1043 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);		
		gridPreguntas1043.addComponent(btnHoraFin);
		gridPreguntas1043.addComponent(lblhora);
		gridPreguntas1043.addComponent(lblhoras);
		
		gridPreguntas1042a=new GridComponent2(this.getActivity(),App.ESTILO,3,1);
		gridPreguntas1042a.addComponent(lblespacio);
		gridPreguntas1042a.addComponent(lblpregunta1042a_a);
		gridPreguntas1042a.addComponent(lblpregunta1042a_b);
//		gridPreguntas1042a.addComponent(lblespacio1);
//		gridPreguntas1042a.addComponent(lblpregunta1042a_indica1);
//		gridPreguntas1042a.addComponent(lblpregunta1042a_indica2);
		
		gridPreguntas1042a.addComponent(lblpregunta1042a_1);
		gridPreguntas1042a.addComponent(rgQI1042A_1A);
		gridPreguntas1042a.addComponent(rgQI1042A_1B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_2);
		gridPreguntas1042a.addComponent(rgQI1042A_2A);
		gridPreguntas1042a.addComponent(rgQI1042A_2B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_3);
		gridPreguntas1042a.addComponent(rgQI1042A_3A);
		gridPreguntas1042a.addComponent(rgQI1042A_3B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_4);
		gridPreguntas1042a.addComponent(rgQI1042A_4A);
		gridPreguntas1042a.addComponent(rgQI1042A_4B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_5);
		gridPreguntas1042a.addComponent(rgQI1042A_5A);
		gridPreguntas1042a.addComponent(rgQI1042A_5B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_6);
		gridPreguntas1042a.addComponent(rgQI1042A_6A);
		gridPreguntas1042a.addComponent(rgQI1042A_6B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_7);
		gridPreguntas1042a.addComponent(rgQI1042A_7A);
		gridPreguntas1042a.addComponent(rgQI1042A_7B);
		gridPreguntas1042a.addComponent(lblpregunta1042a_8);
		gridPreguntas1042a.addComponent(rgQI1042A_8A);
		gridPreguntas1042a.addComponent(rgQI1042A_8B);
    }

    
    @Override
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta1041,gridPreguntas1041.component());
		q2 = createQuestionSection(lblpregunta1042,gridPreguntas1042.component());
		q3 = createQuestionSection(lblpregunta1042a,gridPreguntas1042a.component());
		q4 = createQuestionSection(lblpreguntaInd,lblpregunta1043,gridPreguntas1043.component());
		

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
    }
    
    
    @Override
    public boolean grabar() {

		uiToEntity(individual);
		
		individual.qi1043 = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		individual.qi1043_1 = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
			else {
				App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto10 = getCuestionarioService().getCompletadoSeccion10CI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
				
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		App.getInstance().getCiseccion_10_01().filtroSiNinio=getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id);
		App.getInstance().getCiseccion_10_01().filtroSiNinioS10=getCuestionarioService().getNiniosConCarnetVistabyPersonaId(individual.id, individual.hogar_id, individual.persona_id);
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
		App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto10=getCuestionarioService().getCompletadoSeccion10CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		
		}
		if(individual!=null && individual.id!=null){
			MensajeSinoTieneGps(individual.id);
		}
		return true;
    }
    
    private void MensajeSinoTieneGps(Integer id){
    	if(EndesCalendario.VerificarSitientePuntoGPS(id)==true){
    		MyUtil.MensajeGeneral(getActivity(),App.MENSAJE_GPS);
    	}
    }
    
    public void CompletarVisitaCuestionarioIndividual(){
    	try {
    		if (getCuestionarioService().getCompletadoSeccion0103CI(individual.id, individual.hogar_id, individual.persona_id)
    				&& getCuestionarioService().getCompletadoSeccion0507CI(individual.id, individual.hogar_id, individual.persona_id)
    				&& getCuestionarioService().getCompletadoSeccion008CI(individual.id, individual.hogar_id, individual.persona_id)
    				&& getCuestionarioService().getCompletadoSeccion0103CI(individual.id, individual.hogar_id, individual.persona_id)) {			
    				if(!getVisitaService().saveOrUpdate(RegistrarVisitaCompletada(),"ID","HOGAR_ID","PERSONA_ID","QIVRESUL","QIVHORA_FIN","QIVMIN_FIN","NRO_VISITA")){
    					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados visita", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
    					return; 
    				}
    			}
			
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return;
		}
    	
    }
    
    
    public CARATULA_INDIVIDUAL RegistrarVisitaCompletada(){
    	CARATULA_INDIVIDUAL visita= new CARATULA_INDIVIDUAL();
    	visita.id=individual.id;
    	visita.hogar_id=individual.hogar_id;
    	visita.persona_id=individual.persona_id;
    	
    	visitas = getCuestionarioService().getCAPVISITAsI(individual.id,individual.hogar_id,individual.persona_id, seccionesCargadoVisita);

    	Integer indice = 0;
		if (visitas.size() > 0) {
			if (visitas.size() > 1) {
				indice = visitas.size() - 1;
			} else {
				indice = 0;
			}

			ultimaVisita = getCuestionarioService().getVisitaI(
					App.getInstance().getPersonaCuestionarioIndividual().id,
					App.getInstance().getPersonaCuestionarioIndividual().hogar_id,
					App.getInstance().getPersonaCuestionarioIndividual().persona_id,
					visitas.get(indice).nro_visita,seccionesCargadoVisita);
			
			App.getInstance().setVisitaI(null);
			App.getInstance().setVisitaI(ultimaVisita);
			
		}
		
    	if(App.getInstance().getVisitaI()!=null){
    		visita.nro_visita=ultimaVisita.nro_visita;
    	}
//    	visita.qivhora_fin=Integer.parseInt(txtQI1043.getText().toString()); //individual.qi1043;
//    	visita.qivmin_fin= Integer.parseInt(txtQI1043_1.getText().toString());
    	visita.qivhora_fin=lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
    	visita.qivmin_fin=lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(3,5):null;
    	visita.qivresul=App.INDIVIDUAL_RESULTADO_COMPLETADO;
    	return visita;
    }
    
//    public CARATULA_INDIVIDUAL registrarVisitaCompletada(){
//		 return EndesCalendario.CuestionarioIndividualEstaCompleto(App.getInstance().getPersonaCuestionarioIndividual().id,  App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,App.getInstance().getVisitaI().nro_visita);
//	}
//    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
    	
		if (in1000a==1 && App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
			if (Util.esVacio(individual.qi1041_1)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1041_1");
				view = rgQI1041_1;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1041_2)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1041_2");
				view = rgQI1041_2;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042_1)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042_1");
				view = rgQI1042_1;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042_2)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042_2");
				view = rgQI1042_2;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042_3)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042_3");
				view = rgQI1042_3;
				error = true;
				return false;
			}
		}
		
	
		
		/*****preguntas agregadas FCP@*/
		if (Util.esVacio(individual.qi1042a_1a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_1A");
			view = rgQI1042A_1A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_2a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_2A");
			view = rgQI1042A_2A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_3a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_3A");
			view = rgQI1042A_3A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_4a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_4A");
			view = rgQI1042A_4A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_5a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_5A");
			view = rgQI1042A_5A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_6a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_6A");
			view = rgQI1042A_6A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_7a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_7A");
			view = rgQI1042A_7A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1042a_8a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_8A");
			view = rgQI1042A_8A;
			error = true;
			return false;
		}
		
		if(existeninio>0){
			if (Util.esVacio(individual.qi1042a_1b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_1B");
				view = rgQI1042A_1B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_2b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_2B");
				view = rgQI1042A_2B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_3b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_3B");
				view = rgQI1042A_3B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_4b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_4B");
				view = rgQI1042A_4B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_5b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_5B");
				view = rgQI1042A_5B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_6b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_6B");
				view = rgQI1042A_6B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_7b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_7B");
				view = rgQI1042A_7B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi1042a_8b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI1042A_8B");
				view = rgQI1042A_8B;
				error = true;
				return false;
			}
		}
		
		
		
		if (Util.esVacio(individual.qi1043)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1043");
			view = btnHoraFin;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi1043_1)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1043_1");
			view = btnHoraFin;
			error = true;
			return false;
		}
		
		return true;
    }
    
    
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_10_01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	secccion1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1_3);
    	secccion1_8 = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS8);
		
    	if(individual==null){
		  individual=new CISECCION_10_01();
		  individual.id=App.getInstance().getPersonaCuestionarioIndividual().id;
		  individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		  individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    }
    	
		if (App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			Log.e("11","11"+secccion1_3.qi106);
			App.getInstance().getPersonaCuestionarioIndividual().qi106=secccion1_3.qi106;
			App.getInstance().getPersonaCuestionarioIndividual().qi201=secccion1_3.qi201;
			App.getInstance().getPersonaCuestionarioIndividual().qi206=secccion1_3.qi206;
			App.getInstance().getPersonaCuestionarioIndividual().qi226=secccion1_3.qi226;
			App.getInstance().getPersonaCuestionarioIndividual().qi230=secccion1_3.qi230;
		}
		else{
//			Log.e("11","22");
			App.getInstance().setPersonaCuestionarioIndividual(secccion1_3);
		}
		
		
		if (App.getInstance().getCiseccion_08()!=null && secccion1_8!=null &&secccion1_8.qi817b!=null && secccion1_8.qi817c!=null) {
//			Log.e("11","33");
//			Log.e("11","33"+secccion1_8.qi817b+" "+ secccion1_8.qi817c);
			App.getInstance().getCiseccion_08().qi817b=secccion1_8.qi817b;
			App.getInstance().getCiseccion_08().qi817c=secccion1_8.qi817c;
		}
		else{
//			Log.e("11","44");
			App.getInstance().setCiseccion_08(secccion1_8);
		}
    	
		App.getInstance().getCiseccion_10_01().qi1000a=individual.qi1000a;
		/*if(individual.qi1043!=null && individual.qi1043_1!=null)	{
    		lblhoras.setText(individual.qi1043.toString()+":"+individual.qi1043_1.toString());
    	}*/
		if(individual.qi1043!=null && individual.qi1043_1!=null){
            String hora_=individual.qi1043.toString().length()==1?"0"+individual.qi1043.toString():individual.qi1043.toString();
            String minuto_=individual.qi1043_1.toString().length()==1?"0"+individual.qi1043_1.toString():individual.qi1043_1.toString();
            lblhoras.setText(hora_+":"+minuto_);
		}	
		entityToUI(individual);
		inicio();
    }
    

    private void inicio() {
    	existeninio=App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;
    	Log.e("existeninio: ",""+existeninio);
    	ValidarPregunta1000A();
    	TituloSiEsViolencia();
    	ValidarsiesSupervisora();
    	RenombrarEtiquetas();
    	ValidarPregunta1042A();
    	
    	txtCabecera.requestFocus();
    }
    

    
    public void ValidarPregunta1000A() {
    	if (App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
    		Integer in1000a = individual.qi1000a==null?0:individual.qi1000a;
        	if (in1000a==2) {
        		Util.cleanAndLockView(getActivity(),rgQI1041_1,rgQI1041_2,rgQI1042_1,rgQI1042_2,rgQI1042_3);  		
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
    		}
        	else{
        		Util.lockView(getActivity(),false,rgQI1041_1,rgQI1041_2,rgQI1042_1,rgQI1042_2,rgQI1042_3);  		
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
        		
        	}
		}
    	else {
			Util.cleanAndLockView(getActivity(),rgQI1041_1,rgQI1041_2,rgQI1042_1,rgQI1042_2,rgQI1042_3);  		
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
		}
	}
    public void RenombrarEtiquetas(){
    	lblpregunta1042a_b.text(R.string.ciseccion_10_01qi1042a_b);
    	lblpregunta1042a_b.setText(lblpregunta1042a_b.getText().toString().replace("#", App.ANIOPORDEFECTO+""));
    }
    public void TituloSiEsViolencia() {
    	if (App.getInstance().getQhviolen()==App.getInstance().getPersonaCuestionarioIndividual().persona_id) {
    		Log.e("ddd", "11");
    		q0.setVisibility(View.VISIBLE);    		
		}
    	else{
    		Log.e("ddd", "22");
    		q0.setVisibility(View.GONE);    		
    	}
    }
    
    public void ValidarPregunta1042A(){
    	if (existeninio>0){
    		Util.lockView(getActivity(),false,rgQI1042A_1B,rgQI1042A_2B,rgQI1042A_3B,rgQI1042A_4B,rgQI1042A_5B,rgQI1042A_6B,rgQI1042A_7B,rgQI1042A_8B);
    		lblpregunta1042a_b.setVisibility(View.VISIBLE);
    		lblpregunta1042a_indica1.setVisibility(View.VISIBLE);
    		rgQI1042A_1B.setVisibility(View.VISIBLE);
    		rgQI1042A_2B.setVisibility(View.VISIBLE);
    		rgQI1042A_3B.setVisibility(View.VISIBLE);
    		rgQI1042A_4B.setVisibility(View.VISIBLE);
    		rgQI1042A_5B.setVisibility(View.VISIBLE);
    		rgQI1042A_6B.setVisibility(View.VISIBLE);
    		rgQI1042A_7B.setVisibility(View.VISIBLE);
    		rgQI1042A_8B.setVisibility(View.VISIBLE);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(),rgQI1042A_1B,rgQI1042A_2B,rgQI1042A_3B,rgQI1042A_4B,rgQI1042A_5B,rgQI1042A_6B,rgQI1042A_7B,rgQI1042A_8B);
    		lblpregunta1042a_b.setVisibility(View.GONE);
    		lblpregunta1042a_indica2.setVisibility(View.GONE);
    		rgQI1042A_1B.setVisibility(View.GONE);
    		rgQI1042A_2B.setVisibility(View.GONE);
    		rgQI1042A_3B.setVisibility(View.GONE);
    		rgQI1042A_4B.setVisibility(View.GONE);
    		rgQI1042A_5B.setVisibility(View.GONE);
    		rgQI1042A_6B.setVisibility(View.GONE);
    		rgQI1042A_7B.setVisibility(View.GONE);
    		rgQI1042A_8B.setVisibility(View.GONE);
    	}
    }
    

    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI1041_1.readOnly();
    		rgQI1041_2.readOnly();
    		rgQI1042_1.readOnly();
    		rgQI1042_2.readOnly();
    		rgQI1042_3.readOnly();
    		
    		rgQI1042A_1A.readOnly();
    		rgQI1042A_1B.readOnly();
    		rgQI1042A_2A.readOnly();
    		rgQI1042A_2B.readOnly();
    		rgQI1042A_3A.readOnly();
    		rgQI1042A_3B.readOnly();
    		rgQI1042A_4A.readOnly();
    		rgQI1042A_4B.readOnly();
    		rgQI1042A_5A.readOnly();
    		rgQI1042A_5B.readOnly();
    		rgQI1042A_6A.readOnly();
    		rgQI1042A_6B.readOnly();
    		rgQI1042A_7A.readOnly();
    		rgQI1042A_7B.readOnly();
    		rgQI1042A_8A.readOnly();
    		rgQI1042A_8B.readOnly();
    		btnHoraFin.setEnabled(false);
    		Util.cleanAndLockView(getActivity(), btnHoraFin);
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		App.getInstance().getCiseccion_10_01().filtroSiNinio=getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id);
		
		return App.INDIVIDUAL;
	}
}