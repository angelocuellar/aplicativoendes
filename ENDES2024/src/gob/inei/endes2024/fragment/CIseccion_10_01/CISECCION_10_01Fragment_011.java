package gob.inei.endes2024.fragment.CIseccion_10_01;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_10_01.Dialog.CISECCION_10_01Fragment_009Dialog;
import gob.inei.endes2024.fragment.CIseccion_10_01.Dialog.CISECCION_10_01Fragment_009Dialog.ACTION_NACIMIENTO;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_011 extends FragmentForm implements Respondible {
	
	public List<CISECCION_02> detalles;
	Hogar bean;
	private CuestionarioService cuestionarioService;
	public TableComponent tcPersonas;
	public Integer completado;
	private DialogComponent dialog;
	Seccion01ClickListener adapter;
	
	public LabelComponent lblTitulo,lblpreguntaInd,lblpregunta211;
	public TextField txtCabecera;
	
	LinearLayout q0,q1;
	
	SeccionCapitulo[] seccionesPersonas;
	SeccionCapitulo[] seccionesCargado;
	
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;
	public ACTION_NACIMIENTO siguienteaccion;

	public CISECCION_10_01Fragment_011() {}
	public CISECCION_10_01Fragment_011 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","QI216","QI217","QI218","ID","HOGAR_ID","PERSONA_ID")};
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI1036_A","QI1036_B","QI1036_C","QI1036_D","QI1036_E","QI1036_F","QI1036_X","QI1036_Y","ID","HOGAR_ID","PERSONA_ID","NINIO_ID") };
		return rootView;
	}
    
    @Override
    protected void buildFields() {
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_10_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta211 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1035);
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 700).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 700).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcPersonas.addHeader(R.string.seccion01_nro_ordens2, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01nombress2, 1f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01_anioscumplidos, 0.6f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01_conviviendo, 0.6f,TableComponent.ALIGN.CENTER);
    }
  
    public void agregarPersona(Integer indice) {
    	CISECCION_02 bean = new CISECCION_02();
		bean.id = App.getInstance().getPersonaCuestionarioIndividual().id;
		bean.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
		bean.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		if(indice==null)
			bean.qi212 = detalles.size() + 1;
		else 
			bean.qi212 = indice;
		abrirDetalle(bean);
	}
  
    public void abrirDetalle(CISECCION_02 tmp) {
		FragmentManager fm = CISECCION_10_01Fragment_011.this.getFragmentManager();
		CISECCION_10_01Fragment_009Dialog aperturaDialog = CISECCION_10_01Fragment_009Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
  
    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta211,tcPersonas.getTableView());
				
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
    	if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			}
			return false; 
		}
    	return true;
    }
    
    private boolean validar() {
    	if(!isInRange()) return false;
    	if(Util.esDiferente(completado,1)){
			mensaje = "FALTA COMPLETAR REGISTROS"; 
			error = true;
			return false; 
		}
    	return true;
    }
    
    @Override
    public void cargarDatos() {
    	if(App.getInstance().getHogar()!=null){
			cargarTabla();
			inicio();
		}
    }
    
    public void refrescarPersonas(CISECCION_02 personas) {
    	if (detalles.contains(personas)) {
    		cargarTabla();
    		return;
		} else {
			detalles.add(personas);
			cargarTabla();
		}
	}
    
    private int obtenerEstado(CISECCION_02 detalle) {
    	CISECCION_10_02 entidad =  getCuestionarioService().getCISECCION_10_02(detalle.id,detalle.hogar_id,detalle.persona_id,detalle.qi212,seccionesPersonas);
    	
		if (entidad!=null && (!Util.esDiferente(entidad.qi1036_a, 1)||!Util.esDiferente(entidad.qi1036_b, 1)||!Util.esDiferente(entidad.qi1036_c, 1)||!Util.esDiferente(entidad.qi1036_d, 1)||
				!Util.esDiferente(entidad.qi1036_e, 1)||!Util.esDiferente(entidad.qi1036_f, 1)||!Util.esDiferente(entidad.qi1036_x, 1)||!Util.esDiferente(entidad.qi1036_y, 1))) {
			return 1 ;
		} else if (entidad!=null && (!Util.esDiferente(entidad.qi1036_a,0)&&!Util.esDiferente(entidad.qi1036_b,0)&&!Util.esDiferente(entidad.qi1036_c,0)&&!Util.esDiferente(entidad.qi1036_d,0)&&
				!Util.esDiferente(entidad.qi1036_e,0)&&!Util.esDiferente(entidad.qi1036_f,0)&&!Util.esDiferente(entidad.qi1036_x,0)&&!Util.esDiferente(entidad.qi1036_y,0))) {
			completado=0;
			return 2;
		}else {
			completado=0;
			return 2;
		}
	}
    
    public void cargarTabla() {
		detalles = getCuestionarioService().getNacimientosListbyPersonaMayor1Anio(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		tcPersonas.setData(detalles, "getQi212", "getQi212_nom", "getQi217", "getQi218");
		completado=1;
		for (int row = 0; row < detalles.size(); row++) {
			if (obtenerEstado(detalles.get(row)) == 1) {
				// borde de color azul
				tcPersonas.setBorderRow(row, true);
			} else if ( obtenerEstado(detalles.get(row))== 2) {
				// borde de color rojo
				tcPersonas.setBorderRow(row, true, R.color.red);
			} else {
				tcPersonas.setBorderRow(row, false);
			}
		}
		registerForContextMenu(tcPersonas.getListView());
	}
    
    private void ValidarFiltro1034() {
    	if (App.getInstance().getCiseccion_10_01().filtro1034==false) {
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
		}
    	else{
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    	}
    }
    
    
    private void inicio() {
    	ValidarFiltro1034();
//    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	RenombrarEtiquetas();
    }
    public void RenombrarEtiquetas(){
    	lblpregunta211.setText(getResources().getString(R.string.ciseccion_10_01qi1035));
    	lblpregunta211.setText(lblpregunta211.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    
    private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_02 c = (CISECCION_02) detalles.get(arg2);
			abrirDetalle(c);
		}
	}
    
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcPersonas.getListView())) {
			menu.setHeaderTitle("Opciones");
			menu.add(1, 0, 1, "Editar Registro");
			menu.add(1, 1, 1, "Eliminar Registro");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = detalles.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			menu.getItem(1).setVisible(false);
			CISECCION_02 seleccion = (CISECCION_02) info.targetView.getTag();
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				menu.getItem(0).setVisible(false);
				menu.getItem(1).setVisible(false);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalle((CISECCION_02) detalles.get(info.position));
				break;
			case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_02) detalles.get(info.position));
				dialog.showDialog();
				break;

			}
		}
		return super.onContextItemSelected(item);
	}
    
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		CISECCION_02 bean = (CISECCION_02) dialog.get("seleccion");
		try {
			detalles.remove(bean);
			boolean borrado = false;
			borrado = getCuestionarioService().BorrarNacimiento(bean.id, bean.hogar_id, bean.persona_id, bean.qi212);
			
			if(detalles.size()>0){
			}
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	}
}