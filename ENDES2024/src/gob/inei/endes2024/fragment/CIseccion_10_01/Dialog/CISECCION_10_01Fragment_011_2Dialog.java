package gob.inei.endes2024.fragment.CIseccion_10_01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_013;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.N_LITERAL;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.text.SimpleDateFormat;
import java.util.Date;


public class CISECCION_10_01Fragment_011_2Dialog extends DialogFragmentComponent {


	@FieldAnnotation(orderIndex = 1)
	public ButtonComponent btnAceptar,btnAgregarCero,btnAgregar;
	
	@FieldAnnotation(orderIndex = 2)
	public CheckBoxField chbQI466E_1A;
	@FieldAnnotation(orderIndex = 3)
	public CheckBoxField chbQI466E_1B;
	@FieldAnnotation(orderIndex = 4)
	public CheckBoxField chbQI466E_1X;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtQI466E_1O;
	
	
	public static boolean editar=false;
	public GridComponent2 gridBtnAgregar;
	public TableComponent tcN1_literal;
	public List<CISECCION_10_04> detalles;
	
	private static CISECCION_10_01Fragment_013 caller;
	private CuestionarioService service;
	private N_LITERAL seleccionado;
	public static Integer edad; 
	private LabelComponent lblvacio, lblTitulo4;
	CISECCION_02 bean;
	CISECCION_10_04 bean10_04;
	CISECCION_04B individual4B2;
	SeccionCapitulo[] seccionesCargado, seccionesCargado2, seccionesGrabado2;
	private DialogComponent dialog;
	private enum ACTION {
		ELIMINAR, MENSAJE
	}
	private ACTION action;
	
	LinearLayout q0, q1, q2, q3, q4;
	public String nombre="";
	private CuestionarioService cuestionarioService;

	TC_ClickListener listener;

	public static CISECCION_10_01Fragment_011_2Dialog newInstance(FragmentForm pagina, CISECCION_02 detalle) {
		caller = (CISECCION_10_01Fragment_013) pagina;
		edad = detalle.qi217;
		CISECCION_10_01Fragment_011_2Dialog f = new CISECCION_10_01Fragment_011_2Dialog();
		f.setParent(caller);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public CISECCION_10_01Fragment_011_2Dialog() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_02) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
			//getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
			Integer orden= bean.qi212;
			nombre = bean.qi212_nom==null?"":bean.qi212_nom;
			getDialog().setTitle("N�:  " + orden+"  Nom.: "+nombre+" F.Nac.: "+bean.getQi215());
			
			final View rootView = createUI();
			initObjectsWithoutXML(this, rootView);
		
			listener = new TC_ClickListener();
			tcN1_literal.getListView().setOnItemClickListener(listener);
			seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI_CTRL_DIA","QI_CTRL_MES","QI_CTRL_ANIO","QI_CTRL_PESO","QI_CTRL_TALLA","ID","HOGAR_ID","PERSONA_ID","NINIO_ID","QI_CTRL","QI_CTRL_DESC")};
			
			seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466E_1A","QI466E_1B","QI466E_1X","QI466E_1O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
			seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466E_1A","QI466E_1B","QI466E_1X","QI466E_1O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
			cargarDatos();
			enlazarCajas();
			listening();
			return rootView;
		}

	@Override
	public void onDismiss(DialogInterface dialog) {
	}

	@Override
	protected View createUI() {
		buildFields();

		q1 = createQuestionSection(gridBtnAgregar.component());
		
		q2 = createQuestionSection(tcN1_literal.getTableView());

		/////////////////tcN1_literal.setSelectorData(R.drawable.selector_sombra_lista);
		
		LinearLayout ly1033 = new LinearLayout(getActivity());
		ly1033.addView(chbQI466E_1X);
		ly1033.addView(txtQI466E_1O);
		//q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblTitulo3,chbQI456_1A,chbQI456_1B,ly1032);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblTitulo4,chbQI466E_1A,chbQI466E_1B,ly1033);
		

		LinearLayout botones = createButtonSection(btnAceptar);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(q2);
		
		form.addView(q4);

		form.addView(botones);

		return contenedor;
	}

	@Override
	protected void buildFields() {
		
		tcN1_literal = new TableComponent(getActivity(), this, App.ESTILO).size(800, 740).headerHeight(65).dataColumHeight(50);//.filter("n1_literal");
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_cod,1.5f,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_dia,1,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_mes,1,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_anio,1,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_peso,1,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466E_talla,1,TableComponent.ALIGN.LEFT);
		tcN1_literal.setColorCondition("COLOR", Util.getHMObject("1",R.color.celesteclarito,"0",R.color.default_color));
		
		
		lblTitulo4=new LabelComponent(this.getActivity()).text(R.string.c2seccion_04bqi466E_1).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		chbQI466E_1A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466E_1B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		
		txtQI466E_1O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar(); 
		chbQI466E_1X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI466E_XChangeValue");
		
		chbQI466E_1X.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked ){
					
					Util.lockView(getActivity(), false,txtQI466E_1O);
		  		}
				else {
					Util.cleanAndLockView(getActivity(),txtQI466E_1O);
		  		}
			}
		});
		
		
		btnAgregar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 60);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				agregarPersona(null,edad);
				Util.cleanAndLockView(getActivity(), btnAgregar);
			}
		});
		lblvacio = new LabelComponent(getActivity()).textSize(15).size(10, 10).text("").centrar();
		btnAgregarCero = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.btnAgregarcero).size(200, 60);
		btnAgregarCero.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				agregarControlCero(null,edad);
				Util.cleanAndLockView(getActivity(), btnAgregarCero);
			}
		});
		
		gridBtnAgregar = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridBtnAgregar.addComponent(btnAgregarCero);
		gridBtnAgregar.addComponent(lblvacio);
		gridBtnAgregar.addComponent(btnAgregar);
		
		btnAceptar = new ButtonComponent(getParent().getActivity(), App.ESTILO_BOTON).text(
				R.string.ciseccion_10_01qi466E_btnSalir).size(200, 60);
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//boolean flag = validar();
				boolean flag = grabar();
				if (!flag) {
					return;
				}
//				caller.seleccionar(seleccionado);
				CISECCION_10_01Fragment_011_2Dialog.this.dismiss();
			}
		});
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuItem editar = menu.add("Editar Registro");
        MenuItem eliminar = menu.add("Eliminar Registro");
        eliminar.setIcon(android.R.drawable.ic_menu_upload);
        editar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                	abrirDetalle((CISECCION_10_04) detalles.get(info.position), 100,true,nombre);
                    return true;
                }
        });
        eliminar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
            	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            	bean10_04 = detalles.get(info.position);
            	
            	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            	    @Override
            	    public void onClick(DialogInterface dialog, int which) {
            	        switch (which){
            	        case DialogInterface.BUTTON_POSITIVE:
            	            //Yes button clicked
            	        	try {
            	    			boolean borrado = false;
            	    			detalles.remove(bean10_04);
            	    			borrado = getCuestionarioService().BorrarControl(bean.id, bean.hogar_id, bean.persona_id, bean.qi212, bean10_04.qi_ctrl);
            	    			if (!borrado) {
            	    				throw new SQLException("Persona no pudo ser borrada.");
            	    			}
            	    		}catch (SQLException e) {
            	    			e.printStackTrace();
            	    			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
            	    					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            	    		}
            	        	cargarDatos();
            	            break;

            	        case DialogInterface.BUTTON_NEGATIVE:
            	            //No button clicked
            	            break;
            	        }
            	    }
            	};
            	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            	builder.setMessage("�Esta seguro que desea eliminar?").setPositiveButton("Si", dialogClickListener)
            	    .setNegativeButton("No", dialogClickListener).show();
            	return true;
            }
        });
      }
	
	public void agregarPersona(Integer indice, Integer edad) {
		CISECCION_10_04 beanControl = new CISECCION_10_04();
    	beanControl.id = App.getInstance().getPersonaCuestionarioIndividual().id;
    	beanControl.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    	beanControl.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	beanControl.ninio_id = bean.qi212;
    	int i=1;
    	for (CISECCION_10_04 item : detalles) {
			if(!item.qi_ctrl_desc.substring(0,1).equals("0")){
				i++;
			}
		}
    	beanControl.qi_ctrl_desc = i+"";
		if(indice==null){
			beanControl.qi_ctrl = detalles.size()+1;
			editar=false;
		}
		else {
			beanControl.qi_ctrl = indice;
			editar=false;
		}
		
		abrirDetalle(beanControl, edad, editar,nombre);
		
	}
	public void agregarControlCero(Integer indice, Integer edad) {
		CISECCION_10_04 beanControl = new CISECCION_10_04();
    	beanControl.id = App.getInstance().getPersonaCuestionarioIndividual().id;
    	beanControl.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    	beanControl.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	beanControl.ninio_id = bean.qi212;
    	beanControl.qi_ctrl_desc = "0";
		if(indice==null){
			beanControl.qi_ctrl = detalles.size()==0?1:detalles.size()+1;
			editar=false;
		}
		else {
			beanControl.qi_ctrl = indice;
			editar=false;
		}
		
		abrirDetalle(beanControl, edad,editar,nombre);
		
	}
	
	public void cargarDatos() {
		
		individual4B2 = getCuestionarioService().getCISECCION_04B(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,bean.qi212,seccionesCargado2);
         entityToUI(individual4B2);
		
         Log.e("Num de controles", "vvvvvvvvvvvv"+individual4B2.qi466e_1a+" "+individual4B2.qi466e_1b+" "+individual4B2.qi466e_1x+" "+individual4B2.qi466e_1o);
		
		
		
		detalles = getCuestionarioService().getCISECCION_10_04(bean.id,bean.hogar_id,bean.persona_id,bean.qi212, seccionesCargado);

		List<CISECCION_10_04> detallesa = new ArrayList();
		
		for (CISECCION_10_04 item : detalles) {
			Log.e("Pruebassss", "gg"+item.qi_ctrl_dia);
			if(item.qi_ctrl_dia != null){
				if( Integer.parseInt(item.qi_ctrl_dia) < 32 ){
					detallesa.add(item);
				}
			}
			
		}
		
		//Log.e("Num de controles", "size"+detallesa.size());
		
//		Integer caida1 = Integer.parseInt(detalles.get(0).qi_ctrl_dia);
//		Integer caida2 = Integer.parseInt(detalles.get(0).qi_ctrl_dia);

		if(detallesa.size() > 1 && detallesa.get(0).qi_ctrl_dia != null && detallesa.get(1).qi_ctrl_dia != null  ){
//            Log.e("Num de controles", "size"+detalles.size());
//            Log.e("penultima fecha", "size"+detalles.get(detalles.size()-2).qi_ctrl_dia+"-"+detalles.get(detalles.size()-2).qi_ctrl_mes+"-"+detalles.get(detalles.size()-2).qi_ctrl_anio);
//            Log.e("ultima fecha", "size"+detalles.get(detalles.size()-1).qi_ctrl_dia+"-"+detalles.get(detalles.size()-1).qi_ctrl_mes+"-"+detalles.get(detalles.size()-1).qi_ctrl_anio);
            
            SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
            Date d1, d2;
            try {
                  d1 = sdformat.parse(detallesa.get(detallesa.size()-1).qi_ctrl_dia.toString()+"-"+detallesa.get(detallesa.size()-1).qi_ctrl_mes.toString()+"-"+detallesa.get(detallesa.size()-1).qi_ctrl_anio.toString());
                  d2 = sdformat.parse(detallesa.get(detallesa.size()-2).qi_ctrl_dia.toString()+"-"+detallesa.get(detallesa.size()-2).qi_ctrl_mes.toString()+"-"+detallesa.get(detallesa.size()-2).qi_ctrl_anio.toString());
                  

                  if(d1.compareTo(d2) < 0) {
                        ToastMessage.msgBox(getActivity(), "la fecha anterior no puede ser mayor a la fecha actual", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
                  }
                  
            } catch (java.text.ParseException e) {
                  
            
                  // TODO Auto-generated catch block
                  e.printStackTrace();
            }
            
            
      }else{
//            Log.e("mensaje", "solo tiene un control");
      }

		
		
/////////////////////////////////////	
	
		tcN1_literal.setData(detalles, "getQi_ctrl_desc", "getQi_ctrl_dia","getQi_ctrl_mes","getQi_ctrl_anio","getPeso","getTalla");
		registerForContextMenu(tcN1_literal.getListView());
		int i=0;
		int j=0;
		int k=0;
		int l=0;
		if(detalles.size()>0){
			for (CISECCION_10_04 item : detalles) {
				if(item.qi_ctrl_desc.substring(0,1).equals("0")){
					i++;
				}
				else{
					j++;
				}
				if(item.qi_ctrl_desc.substring(0,1).equals("0") && item.qi_ctrl_dia==null){
					k=1;
				}
				if(item.qi_ctrl_desc.equals("1") && item.qi_ctrl_dia==null){
					l=1;
				}
			}
		}
		if(i==4 || j>=1 || k==1){
			Util.cleanAndLockView(getActivity(), btnAgregarCero);
		}
		else{
			Util.lockView(getActivity(), false,btnAgregarCero);
		}
		
		if (j>=12 || detalles.size()==0 || l==1){
			Log.e("","ssss ");
			Util.cleanAndLockView(getActivity(), btnAgregar);
		}
		else 
			Util.lockView(getActivity(), false,btnAgregar);
		inicio();
	}
	
	public void abrirDetalle(CISECCION_10_04 tmp, Integer edad,boolean edit,String nombre) {
		FragmentManager fm = CISECCION_10_01Fragment_011_2Dialog.this.getFragmentManager();
		CISECCION_10_01Fragment_011_3Dialog aperturaDialog = CISECCION_10_01Fragment_011_3Dialog.newInstance(caller, this, tmp, edad,edit,nombre);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}

	private CuestionarioService getService() {
		if (service == null) {
			service = CuestionarioService.getInstance(getActivity());
		}
		return service;
	}

	private boolean grabar() {
				
       uiToEntity(individual4B2);  
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) {
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				}
				if (view != null) {
					view.requestFocus();
				}
			}
			return false;
		}
		try {
			
			if( !getCuestionarioService().saveOrUpdate(individual4B2,seccionesGrabado2)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}

	private boolean validar() {

		Integer qi466a = individual4B2.qi466e_1a == null ? 0 : individual4B2.qi466e_1a;
		Integer qi466b = individual4B2.qi466e_1b == null ? 0 : individual4B2.qi466e_1b;
		Integer qi466x = individual4B2.qi466e_1x == null ? 0 : individual4B2.qi466e_1x;
		
		
		if(qi466a==0 && qi466b==0 && qi466x==0){
			mensaje = "Debe seleccionar al menos una alternativa 466E";
			view =chbQI466E_1A;
			error = true;
			return false;
		}
		
		
		if (chbQI466E_1X.isChecked()) {
			
			if (Util.esVacio(individual4B2.qi466e_1o)) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique 466E";
				view = txtQI466E_1O;
				error = true;
				return false;
			}
		}
		
		/*if (seleccionado == null) {
			return false;
		}*/
		return true;
	}

	private void inicio() {
		ValidarsiesSupervisora();
	}
	   public void ValidarsiesSupervisora(){
	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		btnAgregar.setEnabled(false);
	    	}
	    }
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;

	}
	
	private class TC_ClickListener implements OnItemClickListener {

		public TC_ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_10_04 c = (CISECCION_10_04) detalles.get(arg2);
			abrirDetalle(c, 100,true,nombre);			
		}
	}
}