package gob.inei.endes2024.fragment.CIseccion_10_01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_011;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_009Dialog  extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public CheckBoxField chbQI1036_A;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI1036_B;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI1036_C;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI1036_D;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI1036_E;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI1036_F;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI1036_X;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI1036_Y;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI1037_A_M;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI1037_B_M;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI1037_C_M;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI1037_D_M;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI1037_E_M;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI1037_F_M;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI1037_G_M;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI1037_H_M;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI1037_I_M;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI1037_J_M;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI1037_K_M;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI1037_X_M;	
	@FieldAnnotation(orderIndex=21)
	public TextField txtQI1037_X_M_O;	
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI1037_A_P;
	@FieldAnnotation(orderIndex=23)
	public CheckBoxField chbQI1037_B_P;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI1037_C_P;
	@FieldAnnotation(orderIndex=25)
	public CheckBoxField chbQI1037_D_P;
	@FieldAnnotation(orderIndex=26)
	public CheckBoxField chbQI1037_E_P;
	@FieldAnnotation(orderIndex=27)
	public CheckBoxField chbQI1037_F_P;
	@FieldAnnotation(orderIndex=28)
	public CheckBoxField chbQI1037_G_P;
	@FieldAnnotation(orderIndex=29)
	public CheckBoxField chbQI1037_H_P;
	@FieldAnnotation(orderIndex=30)
	public CheckBoxField chbQI1037_I_P;
	@FieldAnnotation(orderIndex=31)
	public CheckBoxField chbQI1037_J_P;
	@FieldAnnotation(orderIndex=32)
	public CheckBoxField chbQI1037_K_P;
	@FieldAnnotation(orderIndex=33)
	public CheckBoxField chbQI1037_X_P;
	@FieldAnnotation(orderIndex=34)
	public TextField txtQI1037_X_P_O;
	@FieldAnnotation(orderIndex=35)
	public CheckBoxField chbQI1037_A_O;
	@FieldAnnotation(orderIndex=36)
	public CheckBoxField chbQI1037_B_O;
	@FieldAnnotation(orderIndex=37)
	public CheckBoxField chbQI1037_C_O;
	@FieldAnnotation(orderIndex=38)
	public CheckBoxField chbQI1037_D_O;
	@FieldAnnotation(orderIndex=39)
	public CheckBoxField chbQI1037_E_O;
	@FieldAnnotation(orderIndex=40)
	public CheckBoxField chbQI1037_F_O;
	@FieldAnnotation(orderIndex=41)
	public CheckBoxField chbQI1037_G_O;
	@FieldAnnotation(orderIndex=42)
	public CheckBoxField chbQI1037_H_O;
	@FieldAnnotation(orderIndex=43)
	public CheckBoxField chbQI1037_I_O;
	@FieldAnnotation(orderIndex=44)
	public CheckBoxField chbQI1037_J_O;
	@FieldAnnotation(orderIndex=45)
	public CheckBoxField chbQI1037_K_O;
	@FieldAnnotation(orderIndex=46)
	public CheckBoxField chbQI1037_X_O;
	@FieldAnnotation(orderIndex=47)
	public TextField txtQI1037_X_O_O;
	@FieldAnnotation(orderIndex=48)
	public RadioGroupOtherField rgQI1040_A;
	@FieldAnnotation(orderIndex=49)
	public RadioGroupOtherField rgQI1040_B;
	@FieldAnnotation(orderIndex=50)
	public RadioGroupOtherField rgQI1040_C;
	@FieldAnnotation(orderIndex=51)
	public RadioGroupOtherField rgQI1040_D;
	@FieldAnnotation(orderIndex=52)
	public RadioGroupOtherField rgQI1040_E;
	@FieldAnnotation(orderIndex=53)
	public RadioGroupOtherField rgQI1040_F;
	@FieldAnnotation(orderIndex=54)
	public RadioGroupOtherField rgQI1040_G;
	@FieldAnnotation(orderIndex=55)
	public RadioGroupOtherField rgQI1040_H;
	@FieldAnnotation(orderIndex=56)
	public RadioGroupOtherField rgQI1040_I;

	public ButtonComponent btnAceptar;
	public ButtonComponent btnGrabadoParcial;
	public ButtonComponent btnCancelar;
	SeccionCapitulo[] seccionesCargado;
	LinearLayout q0,q1,q2;
	LinearLayout ly220;
	private boolean filtro1038= false;
	CISECCION_10_02 entidad=null;
	public TextField txtCabecera;
	CISECCION_10_01 individual;
	private LabelComponent lblp1036,lblp1037,lblp1040,lblp1036_ind,lblp1036_ind2;
	private LabelComponent lblP1037_0,lblP1037_1,lblP1037_2,lblP1037_3,lblQI1037A,lblQI1037B,lblQI1037C,lblQI1037D,lblQI1037E,lblQI1037F,lblQI1037G,lblQI1037H,lblQI1037I,lblQI1037J,lblQI1037K,lblQI1037X;
	private LabelComponent lblP1040_0,lblP1040_1,lblP1040_2,lblP1040_3,lblQI1040A,lblQI1040B,lblQI1040C,lblQI1040D,lblQI1040E,lblQI1040F,lblQI1040G,lblQI1040H,lblQI1040I,lblP1037_0_e1,lblP1037_0_e2,lblP1037_0_e3;
	CISECCION_02 bean;
	private static CISECCION_10_01Fragment_011 caller;
	
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	public GridComponent2 gridPregunta1037;
	public GridComponent2 gridPregunta1040;
	 
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco;
	public LabelComponent lblpregunta01,lblpregunta03;
	public IntegerField txtQI220C1,txtQI220C2,txtQI220C3;
	public String fechareferencia;

	public enum ACTION_NACIMIENTO {
		AGREGAR, EDITAR //, AGREGAR_INTERMEDIO
	}
	public ACTION_NACIMIENTO accion;

	public static CISECCION_10_01Fragment_009Dialog newInstance(FragmentForm pagina, CISECCION_02 detalle) {
		caller = (CISECCION_10_01Fragment_011) pagina;
		CISECCION_10_01Fragment_009Dialog f = new CISECCION_10_01Fragment_009Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_02) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= bean.qi212;
		String nombre = bean.qi212_nom==null?"":bean.qi212_nom;
		getDialog().setTitle("N� de Orden:  " + orden+"  Nombre: "+nombre);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}

	public CISECCION_10_01Fragment_009Dialog() {
		super();
		seccionesGrabado  = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI1036_A","QI1036_B","QI1036_C","QI1036_D","QI1036_E","QI1036_F","QI1036_X","QI1036_Y","QI1037_A_M","QI1037_B_M","QI1037_C_M","QI1037_D_M","QI1037_E_M","QI1037_F_M","QI1037_G_M","QI1037_H_M","QI1037_I_M","QI1037_J_M","QI1037_K_M","QI1037_X_M","QI1037_X_M_O","QI1037_A_P","QI1037_B_P","QI1037_C_P","QI1037_D_P","QI1037_E_P","QI1037_F_P","QI1037_G_P","QI1037_H_P","QI1037_I_P","QI1037_J_P","QI1037_K_P","QI1037_X_P","QI1037_X_P_O","QI1037_A_O","QI1037_B_O","QI1037_C_O","QI1037_D_O","QI1037_E_O","QI1037_F_O","QI1037_G_O","QI1037_H_O","QI1037_I_O","QI1037_J_O","QI1037_K_O","QI1037_X_O","QI1037_X_O_O","QI1040_A","QI1040_B","QI1040_C","QI1037_X_M_O","QI1037_X_P_O","QI1037_X_O_O","QI1040_D","QI1040_E","QI1040_F","QI1040_G","QI1040_H","QI1040_I","NINIO_ID")};
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI1036_A","QI1036_B","QI1036_C","QI1036_D","QI1036_E","QI1036_F","QI1036_X","QI1036_Y","QI1037_A_M","QI1037_B_M","QI1037_C_M","QI1037_D_M","QI1037_E_M","QI1037_F_M","QI1037_G_M","QI1037_H_M","QI1037_I_M","QI1037_J_M","QI1037_K_M","QI1037_X_M","QI1037_X_M_O","QI1037_A_P","QI1037_B_P","QI1037_C_P","QI1037_D_P","QI1037_E_P","QI1037_F_P","QI1037_G_P","QI1037_H_P","QI1037_I_P","QI1037_J_P","QI1037_K_P","QI1037_X_P","QI1037_X_P_O","QI1037_A_O","QI1037_B_O","QI1037_C_O","QI1037_D_O","QI1037_E_O","QI1037_F_O","QI1037_G_O","QI1037_H_O","QI1037_I_O","QI1037_J_O","QI1037_K_O","QI1037_X_O","QI1037_X_O_O","QI1040_A","QI1040_B","QI1040_C","QI1037_X_M_O","QI1037_X_P_O","QI1037_X_O_O","QI1040_D","QI1040_E","QI1040_F","QI1040_G","QI1040_H","QI1040_I","ID","HOGAR_ID","PERSONA_ID","NINIO_ID") };
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1005A","QI1005B","QI1005C","QI1005D","QI1005E","QI1005F","QI1005G","QI1005H","QI1005I","ID","HOGAR_ID","PERSONA_ID")};
	}
		
	@Override
	protected void buildFields() {
		chbQI1036_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_AChangeValue");
		chbQI1036_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_BChangeValue");
		chbQI1036_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_CChangeValue");
		chbQI1036_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_DChangeValue");
		chbQI1036_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_EChangeValue");
		chbQI1036_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_FChangeValue");
		chbQI1036_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_XChangeValue");
		chbQI1036_Y=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1036_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI1036_YChangeValue");
		
		chbQI1037_A_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_a, "1:0").size(50, 90);
		chbQI1037_B_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_b, "1:0").size(50, 90);
		chbQI1037_C_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_c, "1:0").size(50, 90);
		chbQI1037_D_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_d, "1:0").size(50, 90);
		chbQI1037_E_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_e, "1:0").size(50, 90);
		chbQI1037_F_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_f, "1:0").size(50, 90);
		chbQI1037_G_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_g, "1:0").size(50, 90);
		chbQI1037_H_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_h, "1:0").size(50, 90);
		chbQI1037_I_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_i, "1:0").size(50, 90);
		chbQI1037_J_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_j, "1:0").size(50, 90);
		chbQI1037_K_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_k, "1:0").size(50, 90);
		chbQI1037_X_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_x, "1:0").size(50, 90).callback("onchbQI1037_M_XChangeValue");
		
		chbQI1037_A_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_a, "1:0").size(50, 90);
		chbQI1037_B_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_b, "1:0").size(50, 90);
		chbQI1037_C_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_c, "1:0").size(50, 90);
		chbQI1037_D_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_d, "1:0").size(50, 90);
		chbQI1037_E_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_e, "1:0").size(50, 90);
		chbQI1037_F_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_f, "1:0").size(50, 90);
		chbQI1037_G_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_g, "1:0").size(50, 90);
		chbQI1037_H_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_h, "1:0").size(50, 80);
		chbQI1037_I_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_i, "1:0").size(50, 90);
		chbQI1037_J_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_j, "1:0").size(50, 90);
		chbQI1037_K_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_k, "1:0").size(50, 90);
		chbQI1037_X_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_x, "1:0").size(50, 90).callback("onchbQI1037_P_XChangeValue");
		
		chbQI1037_A_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_a, "1:0").size(50, 110);
		chbQI1037_B_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_b, "1:0").size(50, 110);
		chbQI1037_C_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_c, "1:0").size(50, 110);
		chbQI1037_D_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_d, "1:0").size(50, 110);
		chbQI1037_E_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_e, "1:0").size(50, 110);
		chbQI1037_F_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_f, "1:0").size(50, 110);
		chbQI1037_G_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_g, "1:0").size(50, 110);
		chbQI1037_H_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_h, "1:0").size(50, 110);
		chbQI1037_I_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_i, "1:0").size(50, 110);
		chbQI1037_J_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_j, "1:0").size(50, 110);
		chbQI1037_K_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_k, "1:0").size(50, 110);
		chbQI1037_X_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi1037_x, "1:0").size(50, 110).callback("onchbQI1037_O_XChangeValue");
		
		lblP1037_0_e1 = new LabelComponent(getActivity()).textSize(16).size(altoComponente, 410).text(R.string.ciseccion_10_01qi1037_e1);
		lblP1037_0_e2 = new LabelComponent(getActivity()).textSize(16).size(altoComponente, 410).text(R.string.ciseccion_10_01qi1037_e2);
		lblP1037_0_e3 = new LabelComponent(getActivity()).textSize(16).size(altoComponente, 410).text(R.string.ciseccion_10_01qi1037_e3);
		txtQI1037_X_M_O=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		txtQI1037_X_P_O=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		txtQI1037_X_O_O=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		
		lblP1037_0 = new LabelComponent(getActivity()).textSize(16).size(90, 410).text(R.string.ciseccion_10_01qi1037_0).centrar();
		lblP1037_1 = new LabelComponent(getActivity()).textSize(16).size(90, 90).text(R.string.ciseccion_10_01qi1037_1).centrar();
		lblP1037_2 = new LabelComponent(getActivity()).textSize(16).size(90, 90).text(R.string.ciseccion_10_01qi1037_2).centrar();
		lblP1037_3 = new LabelComponent(getActivity()).textSize(16).size(90, 110).text(R.string.ciseccion_10_01qi1037_3).centrar();
		
		lblQI1037A =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pa);
		lblQI1037B =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pb);
		lblQI1037C =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pc);
		lblQI1037D =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pd);
		lblQI1037E =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pe);
		lblQI1037F =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pf);
		lblQI1037G =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pg);
		lblQI1037H =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Ph);
		lblQI1037I =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pi);
		lblQI1037J =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pj);
		lblQI1037K =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Pk);
		lblQI1037X =new LabelComponent(getActivity()).textSize(16).size(50, 410).text(R.string.ciseccion_10_01qi1037_Px);
		
		gridPregunta1037=new GridComponent2(this.getActivity(),App.ESTILO,4,1);
		gridPregunta1037.addComponent(lblP1037_0);
		gridPregunta1037.addComponent(lblP1037_1);
		gridPregunta1037.addComponent(lblP1037_2);
		gridPregunta1037.addComponent(lblP1037_3);
		
		gridPregunta1037.addComponent(lblQI1037A);
		gridPregunta1037.addComponent(chbQI1037_A_M);
		gridPregunta1037.addComponent(chbQI1037_A_P);
		gridPregunta1037.addComponent(chbQI1037_A_O);
		
		gridPregunta1037.addComponent(lblQI1037B);
		gridPregunta1037.addComponent(chbQI1037_B_M);
		gridPregunta1037.addComponent(chbQI1037_B_P);
		gridPregunta1037.addComponent(chbQI1037_B_O);
		
		gridPregunta1037.addComponent(lblQI1037C);
		gridPregunta1037.addComponent(chbQI1037_C_M);
		gridPregunta1037.addComponent(chbQI1037_C_P);
		gridPregunta1037.addComponent(chbQI1037_C_O);
		
		gridPregunta1037.addComponent(lblQI1037D);
		gridPregunta1037.addComponent(chbQI1037_D_M);
		gridPregunta1037.addComponent(chbQI1037_D_P);
		gridPregunta1037.addComponent(chbQI1037_D_O);
		
		gridPregunta1037.addComponent(lblQI1037E);
		gridPregunta1037.addComponent(chbQI1037_E_M);
		gridPregunta1037.addComponent(chbQI1037_E_P);
		gridPregunta1037.addComponent(chbQI1037_E_O);
		
		gridPregunta1037.addComponent(lblQI1037F);
		gridPregunta1037.addComponent(chbQI1037_F_M);
		gridPregunta1037.addComponent(chbQI1037_F_P);
		gridPregunta1037.addComponent(chbQI1037_F_O);
		
		gridPregunta1037.addComponent(lblQI1037G);
		gridPregunta1037.addComponent(chbQI1037_G_M);
		gridPregunta1037.addComponent(chbQI1037_G_P);
		gridPregunta1037.addComponent(chbQI1037_G_O);
		
		gridPregunta1037.addComponent(lblQI1037H);
		gridPregunta1037.addComponent(chbQI1037_H_M);
		gridPregunta1037.addComponent(chbQI1037_H_P);
		gridPregunta1037.addComponent(chbQI1037_H_O);
		
		gridPregunta1037.addComponent(lblQI1037I);
		gridPregunta1037.addComponent(chbQI1037_I_M);
		gridPregunta1037.addComponent(chbQI1037_I_P);
		gridPregunta1037.addComponent(chbQI1037_I_O);
		
		gridPregunta1037.addComponent(lblQI1037J);
		gridPregunta1037.addComponent(chbQI1037_J_M);
		gridPregunta1037.addComponent(chbQI1037_J_P);
		gridPregunta1037.addComponent(chbQI1037_J_O);
		
		gridPregunta1037.addComponent(lblQI1037K);
		gridPregunta1037.addComponent(chbQI1037_K_M);
		gridPregunta1037.addComponent(chbQI1037_K_P);
		gridPregunta1037.addComponent(chbQI1037_K_O);
		
		gridPregunta1037.addComponent(lblQI1037X);
		gridPregunta1037.addComponent(chbQI1037_X_M);
		gridPregunta1037.addComponent(chbQI1037_X_P);
		gridPregunta1037.addComponent(chbQI1037_X_O);
		
		gridPregunta1037.addComponent(lblP1037_0_e1);
		gridPregunta1037.addComponent(txtQI1037_X_M_O,3);
		gridPregunta1037.addComponent(lblP1037_0_e2);
		gridPregunta1037.addComponent(txtQI1037_X_P_O,3);
		gridPregunta1037.addComponent(lblP1037_0_e3);
		gridPregunta1037.addComponent(txtQI1037_X_O_O,3);
		
		rgQI1040_A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(altoComponente,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(altoComponente,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(70,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(altoComponente,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(altoComponente,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_F=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(70,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_G=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(70,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_H=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(90,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQI1040_I=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_10_01qi1040_so1,R.string.ciseccion_10_01qi1040_so2,R.string.ciseccion_10_01qi1040_so3).size(70,255).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblP1040_0 = new LabelComponent(getActivity()).textSize(18).size(70, 435).negrita().centrar();
		lblP1040_1 = new LabelComponent(getActivity()).textSize(16).size(70, 85).text(R.string.ciseccion_10_01qi1040_1).centrar();
		lblP1040_2 = new LabelComponent(getActivity()).textSize(16).size(70, 85).text(R.string.ciseccion_10_01qi1040_2).centrar();
		lblP1040_3 = new LabelComponent(getActivity()).textSize(16).size(70, 85).text(R.string.ciseccion_10_01qi1040_3).centrar();
		
		lblQI1040A =new LabelComponent(getActivity()).textSize(17).size(altoComponente, 435).text(R.string.ciseccion_10_01qi1040_Pa);
		lblQI1040B =new LabelComponent(getActivity()).textSize(17).size(altoComponente, 435).text(R.string.ciseccion_10_01qi1040_Pb);
		lblQI1040C =new LabelComponent(getActivity()).textSize(17).size(70, 435).text(R.string.ciseccion_10_01qi1040_Pc);
		lblQI1040D =new LabelComponent(getActivity()).textSize(17).size(altoComponente, 435).text(R.string.ciseccion_10_01qi1040_Pd);
		lblQI1040E =new LabelComponent(getActivity()).textSize(17).size(altoComponente, 435).text(R.string.ciseccion_10_01qi1040_Pe);
		lblQI1040F =new LabelComponent(getActivity()).textSize(17).size(70, 435).text(R.string.ciseccion_10_01qi1040_Pf);
		lblQI1040G =new LabelComponent(getActivity()).textSize(17).size(70, 435).text(R.string.ciseccion_10_01qi1040_Pg);
		lblQI1040H =new LabelComponent(getActivity()).textSize(17).size(90, 435).text(R.string.ciseccion_10_01qi1040_Ph);
		lblQI1040I =new LabelComponent(getActivity()).textSize(17).size(70, 435).text(R.string.ciseccion_10_01qi1040_Pi);
		
		gridPregunta1040=new GridComponent2(this.getActivity(),App.ESTILO,4,1);
		gridPregunta1040.addComponent(lblP1040_0);
		gridPregunta1040.addComponent(lblP1040_1);
		gridPregunta1040.addComponent(lblP1040_2);
		gridPregunta1040.addComponent(lblP1040_3);

		gridPregunta1040.addComponent(lblQI1040A);
		gridPregunta1040.addComponent(rgQI1040_A,3);
	
		gridPregunta1040.addComponent(lblQI1040B);
		gridPregunta1040.addComponent(rgQI1040_B,3);
		
		gridPregunta1040.addComponent(lblQI1040C);
		gridPregunta1040.addComponent(rgQI1040_C,3);
		
		gridPregunta1040.addComponent(lblQI1040D);
		gridPregunta1040.addComponent(rgQI1040_D,3);
		
		gridPregunta1040.addComponent(lblQI1040E);
		gridPregunta1040.addComponent(rgQI1040_E,3);
		
		gridPregunta1040.addComponent(lblQI1040F);
		gridPregunta1040.addComponent(rgQI1040_F,3);
		
		gridPregunta1040.addComponent(lblQI1040G);
		gridPregunta1040.addComponent(rgQI1040_G,3);
		
		gridPregunta1040.addComponent(lblQI1040H);
		gridPregunta1040.addComponent(rgQI1040_H,3);
		
		gridPregunta1040.addComponent(lblQI1040I);
		gridPregunta1040.addComponent(rgQI1040_I,3);

		lblp1036 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1036);
		lblp1036_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1036_ind).negrita();
		lblp1036_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_10_01qi1036_ind2);
		lblp1037 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1037);
		lblp1040 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi1040);

		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);

		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CISECCION_10_01Fragment_009Dialog.this.dismiss();
			}
		});

		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarPersonas(bean);
				CISECCION_10_01Fragment_009Dialog.this.dismiss();
				if(bean.qi221!=null) {
					if( accion == ACTION_NACIMIENTO.AGREGAR){
						caller.agregarPersona(null);
					}
				}
				else {
					if(accion == ACTION_NACIMIENTO.AGREGAR) {
						caller.agregarPersona(null);
					}
				}
			}
		});

		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag=grabadoParcial();
				boolean flagdetalle=grabadoParcial();
				if(!flagdetalle){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), CISECCION_10_01Fragment_009Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
	}
	
	@Override
	protected View createUI() {
		buildFields();
		LinearLayout botones = createButtonSection(btnAceptar, btnGrabadoParcial ,btnCancelar);
		q0 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblp1036,lblp1036_ind,lblp1036_ind2,chbQI1036_A,chbQI1036_B,chbQI1036_C,chbQI1036_D,chbQI1036_E,chbQI1036_F,chbQI1036_X,chbQI1036_Y);
		q1 = createQuestionSection(lblp1037,gridPregunta1037.component());
		q2 = createQuestionSection(lblp1040,gridPregunta1040.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		
		form.addView(botones);
		return contenedor;
	}
	
	
	public boolean grabar(){
		uiToEntity(entidad);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			if(!getCuestionarioService().saveOrUpdate(entidad, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return flag;
	}

	
	public boolean grabadoParcial(){
		uiToEntity(entidad);
		if (!validarparcial()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			if(!getCuestionarioService().saveOrUpdate(entidad, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return flag;
	}
	
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in1005a=individual.qi1005a==null?0:individual.qi1005a;
		Integer in1005b=individual.qi1005b==null?0:individual.qi1005b;
		Integer in1005c=individual.qi1005c==null?0:individual.qi1005c;
		Integer in1005d=individual.qi1005d==null?0:individual.qi1005d;
		Integer in1005e=individual.qi1005e==null?0:individual.qi1005e;
		Integer in1005f=individual.qi1005f==null?0:individual.qi1005f;
		Integer in1005g=individual.qi1005g==null?0:individual.qi1005g;
		Integer in1005h=individual.qi1005h==null?0:individual.qi1005h;
		Integer in1005i=individual.qi1005i==null?0:individual.qi1005i;
	
		if (!verificarCheck136() && !chbQI1036_Y.isChecked()) {
			mensaje = preguntaVacia.replace("$", "La pregunta 1036");
			view = chbQI1036_A;
			error = true;
			return false;
		}
		
		if (chbQI1036_A.isChecked() && !verificarCheck137_M()) {
			mensaje = preguntaVacia.replace("$", "La pregunta 1037");
			view = chbQI1037_A_M;
			error = true;
			return false;
		}
		
		if (chbQI1036_B.isChecked() && !verificarCheck137_P()) {
			mensaje = preguntaVacia.replace("$", "La pregunta 1037");
			view = chbQI1037_A_P;
			error = true;
			return false;
		}
		
		if ((chbQI1036_C.isChecked()||chbQI1036_D.isChecked()||chbQI1036_E.isChecked()||chbQI1036_F.isChecked()||chbQI1036_X.isChecked()) && !verificarCheck137_O()) {
			mensaje = preguntaVacia.replace("$", "La pregunta 1037");
			view = chbQI1037_A_O;
			error = true;
			return false;
		}

		if (in1005a==1 && Util.esVacio(entidad.qi1040_a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_A");
			view = rgQI1040_A;
			error = true;
			return false;
		}
		if (in1005b==1 && Util.esVacio(entidad.qi1040_b)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_B");
			view = rgQI1040_B;
			error = true;
			return false;
		}
		if (in1005c==1 && Util.esVacio(entidad.qi1040_c)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_C");
			view = rgQI1040_C;
			error = true;
			return false;
		}
		if (in1005d==1 && Util.esVacio(entidad.qi1040_d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_D");
			view = rgQI1040_D;
			error = true;
			return false;
		}
		if (in1005e==1 && Util.esVacio(entidad.qi1040_e)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_E");
			view = rgQI1040_E;
			error = true;
			return false;
		}
		if (in1005f==1 && Util.esVacio(entidad.qi1040_f)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_F");
			view = rgQI1040_F;
			error = true;
			return false;
		}
		if (in1005g==1 && Util.esVacio(entidad.qi1040_g)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_G");
			view = rgQI1040_G;
			error = true;
			return false;
		}
		if (in1005h==1 && Util.esVacio(entidad.qi1040_h)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_H");
			view = rgQI1040_H;
			error = true;
			return false;
		}
		if (in1005i==1 && Util.esVacio(entidad.qi1040_i)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_I");
			view = rgQI1040_I;
			error = true;
			return false;
		}
		
	    return true;
	}
	
	public boolean validarparcial(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (!verificarCheck136() && !chbQI1036_Y.isChecked()) {
			mensaje = preguntaVacia.replace("$", "La pregunta 1036");
			view = chbQI1036_A;
			error = true;
			return false;
		}
		return true;
	}
	
	public void resetearLabels() {
		lblp1036.setText(lblp1036.getText().toString().replace("$",bean.qi212_nom));
		lblp1037.setText(lblp1037.getText().toString().replace("$",bean.qi212_nom));
		lblp1040.setText(lblp1040.getText().toString().replace("$",bean.qi212_nom));
	}
	
	public void esconderPreg1040() {
		Integer in1005a=individual.qi1005a==null?0:individual.qi1005a;
		Integer in1005b=individual.qi1005b==null?0:individual.qi1005b;
		Integer in1005c=individual.qi1005c==null?0:individual.qi1005c;
		Integer in1005d=individual.qi1005d==null?0:individual.qi1005d;
		Integer in1005e=individual.qi1005e==null?0:individual.qi1005e;
		Integer in1005f=individual.qi1005f==null?0:individual.qi1005f;
		Integer in1005g=individual.qi1005g==null?0:individual.qi1005g;
		Integer in1005h=individual.qi1005h==null?0:individual.qi1005h;
		Integer in1005i=individual.qi1005i==null?0:individual.qi1005i;
		
		if (in1005a!=1 && in1005b!=1 && in1005c!=1 && in1005d!=1 && in1005e!=1 && in1005e!=1 && in1005f!=1 && in1005g!=1 && in1005h!=1 && in1005i!=1) {
			q2.setVisibility(View.GONE);
		}
		else{
			q2.setVisibility(View.VISIBLE);
		}
		
		if (in1005a!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_A);
			gridPregunta1040.getFila(1).setVisibility(view.GONE);
		}
		if (in1005b!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_B);
			gridPregunta1040.getFila(2).setVisibility(view.GONE);
		}
		if (in1005c!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_C);
			gridPregunta1040.getFila(3).setVisibility(view.GONE);
		}
		if (in1005d!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_D);
			gridPregunta1040.getFila(4).setVisibility(view.GONE);
		}
		if (in1005e!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_E);
			gridPregunta1040.getFila(5).setVisibility(view.GONE);
		}
		if (in1005f!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_F);
			gridPregunta1040.getFila(6).setVisibility(view.GONE);
		}
		if (in1005g!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_G);
			gridPregunta1040.getFila(7).setVisibility(view.GONE);
		}
		if (in1005h!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_H);
			gridPregunta1040.getFila(8).setVisibility(view.GONE);
		}
		if (in1005i!=1) {
			Util.cleanAndLockView(getActivity(),rgQI1040_I);
			gridPregunta1040.getFila(9).setVisibility(view.GONE);
		}
		
	}
	
	private void cargarDatos() {
		entidad =  getCuestionarioService().getCISECCION_10_02(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesPersonas);
		individual = getCuestionarioService().getCISECCION_10_01(bean.id,bean.hogar_id,bean.persona_id,seccionesCargado);
		filtro1038 = getCuestionarioService().getFiltro1038byPersona(bean.id, bean.hogar_id, bean.persona_id);
		if(entidad==null){
			  entidad=new CISECCION_10_02();
			  entidad.id=bean.id;
			  entidad.hogar_id=bean.hogar_id;
			  entidad.persona_id=bean.persona_id;
			  entidad.ninio_id=bean.qi212;
		}
		entityToUI(entidad);
		inicio();
	}
	
	private void inicio() {
		onchbQI1036_YChangeValue();
		verificarCheck136Y();
		esconderPreg1040();
		resetearLabels();
		ValidarsiesSupervisora();
	}
	
	public void onchbQI1037_M_XChangeValue() {
		if (chbQI1037_X_M.isChecked()) {
			Util.lockView(getActivity(),false,txtQI1037_X_M_O);
		}
		else{
			Util.cleanAndLockView(getActivity(),txtQI1037_X_M_O);			
		}
	}
	
	public void onchbQI1037_P_XChangeValue() {
		if (chbQI1037_X_P.isChecked()) {
			Util.lockView(getActivity(),false,txtQI1037_X_P_O);
		}
		else{
			Util.cleanAndLockView(getActivity(),txtQI1037_X_P_O);
		}
	}
	
	public void onchbQI1037_O_XChangeValue() {
		if (chbQI1037_X_O.isChecked()) {
			Util.lockView(getActivity(),false,txtQI1037_X_O_O);
		}
		else{
			Util.cleanAndLockView(getActivity(),txtQI1037_X_O_O);
		}
	}
	

	public void onchbQI1036_AChangeValue() {
		if (chbQI1036_A.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_1,chbQI1037_A_M,chbQI1037_B_M,chbQI1037_C_M,chbQI1037_D_M,chbQI1037_E_M,chbQI1037_F_M,chbQI1037_G_M,chbQI1037_H_M,chbQI1037_I_M,chbQI1037_J_M,chbQI1037_K_M,chbQI1037_X_M); 
    		lblP1037_1.setVisibility(View.VISIBLE);
    		chbQI1037_A_M.setVisibility(View.VISIBLE);
    		chbQI1037_B_M.setVisibility(View.VISIBLE);
    		chbQI1037_C_M.setVisibility(View.VISIBLE);
    		chbQI1037_D_M.setVisibility(View.VISIBLE);
    		chbQI1037_E_M.setVisibility(View.VISIBLE);
    		chbQI1037_F_M.setVisibility(View.VISIBLE);
    		chbQI1037_G_M.setVisibility(View.VISIBLE);
    		chbQI1037_H_M.setVisibility(View.VISIBLE);
    		chbQI1037_I_M.setVisibility(View.VISIBLE);
    		chbQI1037_J_M.setVisibility(View.VISIBLE);
    		chbQI1037_K_M.setVisibility(View.VISIBLE);
    		chbQI1037_X_M.setVisibility(View.VISIBLE);
    		txtQI1037_X_M_O.setVisibility(View.VISIBLE);
    		onchbQI1037_M_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_1,chbQI1037_A_M,chbQI1037_B_M,chbQI1037_C_M,chbQI1037_D_M,chbQI1037_E_M,chbQI1037_F_M,chbQI1037_G_M,chbQI1037_H_M,chbQI1037_I_M,chbQI1037_J_M,chbQI1037_K_M,chbQI1037_X_M);
    		lblP1037_1.setVisibility(View.GONE);
    		chbQI1037_A_M.setVisibility(View.GONE);
    		chbQI1037_B_M.setVisibility(View.GONE);
    		chbQI1037_C_M.setVisibility(View.GONE);
    		chbQI1037_D_M.setVisibility(View.GONE);
    		chbQI1037_E_M.setVisibility(View.GONE);
    		chbQI1037_F_M.setVisibility(View.GONE);
    		chbQI1037_G_M.setVisibility(View.GONE);
    		chbQI1037_H_M.setVisibility(View.GONE);
    		chbQI1037_I_M.setVisibility(View.GONE);
    		chbQI1037_J_M.setVisibility(View.GONE);
    		chbQI1037_K_M.setVisibility(View.GONE);
    		chbQI1037_X_M.setVisibility(View.GONE);
    		txtQI1037_X_M_O.setVisibility(View.GONE);
    	}
	}
	
	public void onchbQI1036_BChangeValue() {
		if (chbQI1036_B.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_2,chbQI1037_A_P,chbQI1037_B_P,chbQI1037_C_P,chbQI1037_D_P,chbQI1037_E_P,chbQI1037_F_P,chbQI1037_G_P,chbQI1037_H_P,chbQI1037_I_P,chbQI1037_J_P,chbQI1037_K_P,chbQI1037_X_P); 
    		lblP1037_2.setVisibility(View.VISIBLE);
    		chbQI1037_A_P.setVisibility(View.VISIBLE);
    		chbQI1037_B_P.setVisibility(View.VISIBLE);
    		chbQI1037_C_P.setVisibility(View.VISIBLE);
    		chbQI1037_D_P.setVisibility(View.VISIBLE);
    		chbQI1037_E_P.setVisibility(View.VISIBLE);
    		chbQI1037_F_P.setVisibility(View.VISIBLE);
    		chbQI1037_G_P.setVisibility(View.VISIBLE);
    		chbQI1037_H_P.setVisibility(View.VISIBLE);
    		chbQI1037_I_P.setVisibility(View.VISIBLE);
    		chbQI1037_J_P.setVisibility(View.VISIBLE);
    		chbQI1037_K_P.setVisibility(View.VISIBLE);
    		chbQI1037_X_P.setVisibility(View.VISIBLE);
    		txtQI1037_X_P_O.setVisibility(View.VISIBLE);
    		onchbQI1037_P_XChangeValue();
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_2,chbQI1037_A_P,chbQI1037_B_P,chbQI1037_C_P,chbQI1037_D_P,chbQI1037_E_P,chbQI1037_F_P,chbQI1037_G_P,chbQI1037_H_P,chbQI1037_I_P,chbQI1037_J_P,chbQI1037_K_P,chbQI1037_X_P);
    		lblP1037_2.setVisibility(View.GONE);
    		chbQI1037_A_P.setVisibility(View.GONE);
    		chbQI1037_B_P.setVisibility(View.GONE);
    		chbQI1037_C_P.setVisibility(View.GONE);
    		chbQI1037_D_P.setVisibility(View.GONE);
    		chbQI1037_E_P.setVisibility(View.GONE);
    		chbQI1037_F_P.setVisibility(View.GONE);
    		chbQI1037_G_P.setVisibility(View.GONE);
    		chbQI1037_H_P.setVisibility(View.GONE);
    		chbQI1037_I_P.setVisibility(View.GONE);
    		chbQI1037_J_P.setVisibility(View.GONE);
    		chbQI1037_K_P.setVisibility(View.GONE);
    		chbQI1037_X_P.setVisibility(View.GONE);
    		txtQI1037_X_P_O.setVisibility(View.GONE);
    	}
	}
	
	public void onchbQI1036_CChangeValue() {
		if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
    		lblP1037_3.setVisibility(View.VISIBLE);
    		chbQI1037_A_O.setVisibility(View.VISIBLE);
    		chbQI1037_B_O.setVisibility(View.VISIBLE);
    		chbQI1037_C_O.setVisibility(View.VISIBLE);
    		chbQI1037_D_O.setVisibility(View.VISIBLE);
    		chbQI1037_E_O.setVisibility(View.VISIBLE);
    		chbQI1037_F_O.setVisibility(View.VISIBLE);
    		chbQI1037_G_O.setVisibility(View.VISIBLE);
    		chbQI1037_H_O.setVisibility(View.VISIBLE);
    		chbQI1037_I_O.setVisibility(View.VISIBLE);
    		chbQI1037_J_O.setVisibility(View.VISIBLE);
    		chbQI1037_K_O.setVisibility(View.VISIBLE);
    		chbQI1037_X_O.setVisibility(View.VISIBLE);
    		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
    		onchbQI1037_O_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		lblP1037_3.setVisibility(View.GONE);
    		chbQI1037_A_O.setVisibility(View.GONE);
    		chbQI1037_B_O.setVisibility(View.GONE);
    		chbQI1037_C_O.setVisibility(View.GONE);
    		chbQI1037_D_O.setVisibility(View.GONE);
    		chbQI1037_E_O.setVisibility(View.GONE);
    		chbQI1037_F_O.setVisibility(View.GONE);
    		chbQI1037_G_O.setVisibility(View.GONE);
    		chbQI1037_H_O.setVisibility(View.GONE);
    		chbQI1037_I_O.setVisibility(View.GONE);
    		chbQI1037_J_O.setVisibility(View.GONE);
    		chbQI1037_K_O.setVisibility(View.GONE);
    		chbQI1037_X_O.setVisibility(View.GONE);
    		txtQI1037_X_O_O.setVisibility(View.GONE);
    	}
	}
	
	public void onchbQI1036_DChangeValue() {
		if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
    		lblP1037_3.setVisibility(View.VISIBLE);
    		chbQI1037_A_O.setVisibility(View.VISIBLE);
    		chbQI1037_B_O.setVisibility(View.VISIBLE);
    		chbQI1037_C_O.setVisibility(View.VISIBLE);
    		chbQI1037_D_O.setVisibility(View.VISIBLE);
    		chbQI1037_E_O.setVisibility(View.VISIBLE);
    		chbQI1037_F_O.setVisibility(View.VISIBLE);
    		chbQI1037_G_O.setVisibility(View.VISIBLE);
    		chbQI1037_H_O.setVisibility(View.VISIBLE);
    		chbQI1037_I_O.setVisibility(View.VISIBLE);
    		chbQI1037_J_O.setVisibility(View.VISIBLE);
    		chbQI1037_K_O.setVisibility(View.VISIBLE);
    		chbQI1037_X_O.setVisibility(View.VISIBLE);
    		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
    		onchbQI1037_O_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		lblP1037_3.setVisibility(View.GONE);
    		chbQI1037_A_O.setVisibility(View.GONE);
    		chbQI1037_B_O.setVisibility(View.GONE);
    		chbQI1037_C_O.setVisibility(View.GONE);
    		chbQI1037_D_O.setVisibility(View.GONE);
    		chbQI1037_E_O.setVisibility(View.GONE);
    		chbQI1037_F_O.setVisibility(View.GONE);
    		chbQI1037_G_O.setVisibility(View.GONE);
    		chbQI1037_H_O.setVisibility(View.GONE);
    		chbQI1037_I_O.setVisibility(View.GONE);
    		chbQI1037_J_O.setVisibility(View.GONE);
    		chbQI1037_K_O.setVisibility(View.GONE);
    		chbQI1037_X_O.setVisibility(View.GONE);
    		txtQI1037_X_O_O.setVisibility(View.GONE);
    	}
	}
	
	public void onchbQI1036_EChangeValue() {
		if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
    		lblP1037_3.setVisibility(View.VISIBLE);
    		chbQI1037_A_O.setVisibility(View.VISIBLE);
    		chbQI1037_B_O.setVisibility(View.VISIBLE);
    		chbQI1037_C_O.setVisibility(View.VISIBLE);
    		chbQI1037_D_O.setVisibility(View.VISIBLE);
    		chbQI1037_E_O.setVisibility(View.VISIBLE);
    		chbQI1037_F_O.setVisibility(View.VISIBLE);
    		chbQI1037_G_O.setVisibility(View.VISIBLE);
    		chbQI1037_H_O.setVisibility(View.VISIBLE);
    		chbQI1037_I_O.setVisibility(View.VISIBLE);
    		chbQI1037_J_O.setVisibility(View.VISIBLE);
    		chbQI1037_K_O.setVisibility(View.VISIBLE);
    		chbQI1037_X_O.setVisibility(View.VISIBLE);
    		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
    		onchbQI1037_O_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		lblP1037_3.setVisibility(View.GONE);
    		chbQI1037_A_O.setVisibility(View.GONE);
    		chbQI1037_B_O.setVisibility(View.GONE);
    		chbQI1037_C_O.setVisibility(View.GONE);
    		chbQI1037_D_O.setVisibility(View.GONE);
    		chbQI1037_E_O.setVisibility(View.GONE);
    		chbQI1037_F_O.setVisibility(View.GONE);
    		chbQI1037_G_O.setVisibility(View.GONE);
    		chbQI1037_H_O.setVisibility(View.GONE);
    		chbQI1037_I_O.setVisibility(View.GONE);
    		chbQI1037_J_O.setVisibility(View.GONE);
    		chbQI1037_K_O.setVisibility(View.GONE);
    		chbQI1037_X_O.setVisibility(View.GONE);
    		txtQI1037_X_O_O.setVisibility(View.GONE);
    	}
    }
	
	public void onchbQI1036_FChangeValue() {
		if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
    		lblP1037_3.setVisibility(View.VISIBLE);
    		chbQI1037_A_O.setVisibility(View.VISIBLE);
    		chbQI1037_B_O.setVisibility(View.VISIBLE);
    		chbQI1037_C_O.setVisibility(View.VISIBLE);
    		chbQI1037_D_O.setVisibility(View.VISIBLE);
    		chbQI1037_E_O.setVisibility(View.VISIBLE);
    		chbQI1037_F_O.setVisibility(View.VISIBLE);
    		chbQI1037_G_O.setVisibility(View.VISIBLE);
    		chbQI1037_H_O.setVisibility(View.VISIBLE);
    		chbQI1037_I_O.setVisibility(View.VISIBLE);
    		chbQI1037_J_O.setVisibility(View.VISIBLE);
    		chbQI1037_K_O.setVisibility(View.VISIBLE);
    		chbQI1037_X_O.setVisibility(View.VISIBLE);
    		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
    		onchbQI1037_O_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		lblP1037_3.setVisibility(View.GONE);
    		chbQI1037_A_O.setVisibility(View.GONE);
    		chbQI1037_B_O.setVisibility(View.GONE);
    		chbQI1037_C_O.setVisibility(View.GONE);
    		chbQI1037_D_O.setVisibility(View.GONE);
    		chbQI1037_E_O.setVisibility(View.GONE);
    		chbQI1037_F_O.setVisibility(View.GONE);
    		chbQI1037_G_O.setVisibility(View.GONE);
    		chbQI1037_H_O.setVisibility(View.GONE);
    		chbQI1037_I_O.setVisibility(View.GONE);
    		chbQI1037_J_O.setVisibility(View.GONE);
    		chbQI1037_K_O.setVisibility(View.GONE);
    		chbQI1037_X_O.setVisibility(View.GONE);
    		txtQI1037_X_O_O.setVisibility(View.GONE);
    	}
	}
	
    public void onchbQI1036_XChangeValue() {
    	if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_Y);
    		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
    		lblP1037_3.setVisibility(View.VISIBLE);
    		chbQI1037_A_O.setVisibility(View.VISIBLE);
    		chbQI1037_B_O.setVisibility(View.VISIBLE);
    		chbQI1037_C_O.setVisibility(View.VISIBLE);
    		chbQI1037_D_O.setVisibility(View.VISIBLE);
    		chbQI1037_E_O.setVisibility(View.VISIBLE);
    		chbQI1037_F_O.setVisibility(View.VISIBLE);
    		chbQI1037_G_O.setVisibility(View.VISIBLE);
    		chbQI1037_H_O.setVisibility(View.VISIBLE);
    		chbQI1037_I_O.setVisibility(View.VISIBLE);
    		chbQI1037_J_O.setVisibility(View.VISIBLE);
    		chbQI1037_K_O.setVisibility(View.VISIBLE);
    		chbQI1037_X_O.setVisibility(View.VISIBLE);
    		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
    		onchbQI1037_O_XChangeValue();
    	}
    	else {		
    		Util.lockView(getActivity(),false,chbQI1036_Y);
    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		lblP1037_3.setVisibility(View.GONE);
    		chbQI1037_A_O.setVisibility(View.GONE);
    		chbQI1037_B_O.setVisibility(View.GONE);
    		chbQI1037_C_O.setVisibility(View.GONE);
    		chbQI1037_D_O.setVisibility(View.GONE);
    		chbQI1037_E_O.setVisibility(View.GONE);
    		chbQI1037_F_O.setVisibility(View.GONE);
    		chbQI1037_G_O.setVisibility(View.GONE);
    		chbQI1037_H_O.setVisibility(View.GONE);
    		chbQI1037_I_O.setVisibility(View.GONE);
    		chbQI1037_J_O.setVisibility(View.GONE);
    		chbQI1037_K_O.setVisibility(View.GONE);
    		chbQI1037_X_O.setVisibility(View.GONE);
    		txtQI1037_X_O_O.setVisibility(View.GONE);
    	}
    }
    
    public void onchbQI1036_YChangeValue() {
    	if (chbQI1036_Y.isChecked()) {
    		Util.cleanAndLockView(getActivity(),chbQI1036_A,chbQI1036_B,chbQI1036_C,chbQI1036_D,chbQI1036_E,chbQI1036_F,chbQI1036_X);
    		Util.cleanAndLockView(getActivity(),chbQI1037_B_M,chbQI1037_C_M,chbQI1037_D_M,chbQI1037_E_M,chbQI1037_F_M,chbQI1037_G_M,chbQI1037_H_M,chbQI1037_I_M,chbQI1037_J_M,chbQI1037_K_M,chbQI1037_X_M,chbQI1037_A_P,chbQI1037_B_P,chbQI1037_C_P,chbQI1037_D_P,chbQI1037_E_P,chbQI1037_F_P,chbQI1037_G_P,chbQI1037_H_P,chbQI1037_I_P,chbQI1037_J_P,chbQI1037_K_P,chbQI1037_X_P,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		q1.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(), false,chbQI1036_A,chbQI1036_B,chbQI1036_C,chbQI1036_D,chbQI1036_E,chbQI1036_F,chbQI1036_X);
    		Util.lockView(getActivity(), false,chbQI1037_B_M,chbQI1037_C_M,chbQI1037_D_M,chbQI1037_E_M,chbQI1037_F_M,chbQI1037_G_M,chbQI1037_H_M,chbQI1037_I_M,chbQI1037_J_M,chbQI1037_K_M,chbQI1037_X_M,chbQI1037_A_P,chbQI1037_B_P,chbQI1037_C_P,chbQI1037_D_P,chbQI1037_E_P,chbQI1037_F_P,chbQI1037_G_P,chbQI1037_H_P,chbQI1037_I_P,chbQI1037_J_P,chbQI1037_K_P,chbQI1037_X_P,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
    		q1.setVisibility(View.VISIBLE);
    		
    		onchbQI1036_AChangeValue();
    		onchbQI1036_BChangeValue();
    		if (chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
        		Util.lockView(getActivity(),false,lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O); 
        		lblP1037_3.setVisibility(View.VISIBLE);
        		chbQI1037_A_O.setVisibility(View.VISIBLE);
        		chbQI1037_B_O.setVisibility(View.VISIBLE);
        		chbQI1037_C_O.setVisibility(View.VISIBLE);
        		chbQI1037_D_O.setVisibility(View.VISIBLE);
        		chbQI1037_E_O.setVisibility(View.VISIBLE);
        		chbQI1037_F_O.setVisibility(View.VISIBLE);
        		chbQI1037_G_O.setVisibility(View.VISIBLE);
        		chbQI1037_H_O.setVisibility(View.VISIBLE);
        		chbQI1037_I_O.setVisibility(View.VISIBLE);
        		chbQI1037_J_O.setVisibility(View.VISIBLE);
        		chbQI1037_K_O.setVisibility(View.VISIBLE);
        		chbQI1037_X_O.setVisibility(View.VISIBLE);
        		txtQI1037_X_O_O.setVisibility(View.VISIBLE);
        		onchbQI1037_O_XChangeValue();
			}else {
	    		Util.cleanAndLockView(getActivity(),lblP1037_3,chbQI1037_A_O,chbQI1037_B_O,chbQI1037_C_O,chbQI1037_D_O,chbQI1037_E_O,chbQI1037_F_O,chbQI1037_G_O,chbQI1037_H_O,chbQI1037_I_O,chbQI1037_J_O,chbQI1037_K_O,chbQI1037_X_O);
	    		lblP1037_3.setVisibility(View.GONE);
	    		chbQI1037_A_O.setVisibility(View.GONE);
	    		chbQI1037_B_O.setVisibility(View.GONE);
	    		chbQI1037_C_O.setVisibility(View.GONE);
	    		chbQI1037_D_O.setVisibility(View.GONE);
	    		chbQI1037_E_O.setVisibility(View.GONE);
	    		chbQI1037_F_O.setVisibility(View.GONE);
	    		chbQI1037_G_O.setVisibility(View.GONE);
	    		chbQI1037_H_O.setVisibility(View.GONE);
	    		chbQI1037_I_O.setVisibility(View.GONE);
	    		chbQI1037_J_O.setVisibility(View.GONE);
	    		chbQI1037_K_O.setVisibility(View.GONE);
	    		chbQI1037_X_O.setVisibility(View.GONE);
	    		txtQI1037_X_O_O.setVisibility(View.GONE);
			}
    		/*onchbQI1036_CChangeValue();
    		onchbQI1036_DChangeValue();
    		onchbQI1036_EChangeValue();
    		onchbQI1036_FChangeValue();
    		onchbQI1036_XChangeValue();*/
    	}
    }
  
    
	public boolean verificarCheck136() {
  		if (chbQI1036_A.isChecked() || chbQI1036_B.isChecked() || chbQI1036_C.isChecked() || chbQI1036_D.isChecked() || chbQI1036_E.isChecked() || chbQI1036_F.isChecked() || chbQI1036_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
	public boolean verificarCheck137_M() {
  		if (chbQI1037_A_M.isChecked() || chbQI1037_B_M.isChecked() || chbQI1037_C_M.isChecked() || chbQI1037_D_M.isChecked() || chbQI1037_E_M.isChecked() || chbQI1037_F_M.isChecked() || chbQI1037_G_M.isChecked() || chbQI1037_H_M.isChecked() || chbQI1037_I_M.isChecked() || chbQI1037_J_M.isChecked() || chbQI1037_K_M.isChecked() || chbQI1037_X_M.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
	public boolean verificarCheck137_P() {
  		if (chbQI1037_A_P.isChecked() || chbQI1037_B_P.isChecked() || chbQI1037_C_P.isChecked() || chbQI1037_D_P.isChecked() || chbQI1037_E_P.isChecked() || chbQI1037_F_P.isChecked() || chbQI1037_G_P.isChecked() || chbQI1037_H_P.isChecked() || chbQI1037_I_P.isChecked() || chbQI1037_J_P.isChecked() || chbQI1037_K_P.isChecked() || chbQI1037_X_P.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
	public boolean verificarCheck137_O() {
  		if (chbQI1037_A_O.isChecked() || chbQI1037_B_O.isChecked() || chbQI1037_C_O.isChecked() || chbQI1037_D_O.isChecked() || chbQI1037_E_O.isChecked() || chbQI1037_F_O.isChecked() || chbQI1037_G_O.isChecked() || chbQI1037_H_O.isChecked() || chbQI1037_I_O.isChecked() || chbQI1037_J_O.isChecked() || chbQI1037_K_O.isChecked() || chbQI1037_X_O.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
	public void verificarCheck136Y() {
		if(verificarCheck136()) Util.cleanAndLockView(getActivity(),chbQI1036_Y);
		else Util.lockView(getActivity(),false,chbQI1036_Y);
  	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	 public void ValidarsiesSupervisora(){
		  Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		 chbQI1036_A.readOnly();
	    		chbQI1036_B.readOnly();
	    		chbQI1036_C.readOnly();
	    		chbQI1036_D.readOnly();
	    		chbQI1036_E.readOnly();
	    		chbQI1036_F.readOnly();
	    		chbQI1036_X.readOnly();
	    		chbQI1036_Y.readOnly();
	    		chbQI1037_A_M.readOnly();
	    		chbQI1037_B_M.readOnly();
	    		chbQI1037_C_M.readOnly();
	    		chbQI1037_D_M.readOnly();
	    		chbQI1037_E_M.readOnly();
	    		chbQI1037_F_M.readOnly();
	    		chbQI1037_G_M.readOnly();
	    		chbQI1037_H_M.readOnly();
	    		chbQI1037_I_M.readOnly();
	    		chbQI1037_J_M.readOnly();
	    		chbQI1037_K_M.readOnly();
	    		chbQI1037_X_M.readOnly();	
	    		txtQI1037_X_M_O.readOnly();	
	    		chbQI1037_A_P.readOnly();
	    		chbQI1037_B_P.readOnly();
	    		chbQI1037_C_P.readOnly();
	    		chbQI1037_D_P.readOnly();
	    		chbQI1037_E_P.readOnly();
	    		chbQI1037_F_P.readOnly();
	    		chbQI1037_G_P.readOnly();
	    		chbQI1037_H_P.readOnly();
	    		chbQI1037_I_P.readOnly();
	    		chbQI1037_J_P.readOnly();
	    		chbQI1037_K_P.readOnly();
	    		chbQI1037_X_P.readOnly();
	    		txtQI1037_X_P_O.readOnly();
	    		chbQI1037_A_O.readOnly();
	    		chbQI1037_B_O.readOnly();
	    		chbQI1037_C_O.readOnly();
	    		chbQI1037_D_O.readOnly();
	    		chbQI1037_E_O.readOnly();
	    		chbQI1037_F_O.readOnly();
	    		chbQI1037_G_O.readOnly();
	    		chbQI1037_H_O.readOnly();
	    		chbQI1037_I_O.readOnly();
	    		chbQI1037_J_O.readOnly();
	    		chbQI1037_K_O.readOnly();
	    		chbQI1037_X_O.readOnly();
	    		txtQI1037_X_O_O.readOnly();
	    		rgQI1040_A.readOnly();
	    		rgQI1040_B.readOnly();
	    		rgQI1040_C.readOnly();
	    		rgQI1040_D.readOnly();
	    		rgQI1040_E.readOnly();
	    		rgQI1040_F.readOnly();
	    		rgQI1040_G.readOnly();
	    		rgQI1040_H.readOnly();
	    		rgQI1040_I.readOnly();
	    		btnAceptar.setEnabled(false);
	    		btnGrabadoParcial.setEnabled(false);
	    	}
	  }

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}
	
	

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_10_01Fragment_009Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
}