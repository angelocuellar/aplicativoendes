package gob.inei.endes2024.fragment.CIseccion_10_01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_013;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_10_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.time.LocalDate;






















//import android.graphics.AvoidXfermode;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CISECCION_10_01Fragment_011_4Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI456_VAC_G_D1;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI456_VAC_G_M1;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI456_VAC_G_A1;
	
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI456_VAC_G_D2;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQI456_VAC_G_M2;
	@FieldAnnotation(orderIndex=6)
	public IntegerField txtQI456_VAC_G_A2;
	@FieldAnnotation(orderIndex=7)
	public IntegerField txtQI456_VAC_G_D3;
	@FieldAnnotation(orderIndex=8)
	public IntegerField txtQI456_VAC_G_M3;
	@FieldAnnotation(orderIndex=9)
	public IntegerField txtQI456_VAC_G_A3;
	
	public ButtonComponent btnAceptar;
	public ButtonComponent btnGrabadoParcial;
	public ButtonComponent btnCancelar;
	
	LinearLayout q0;
	
	CISECCION_10_03 vacuna=null;
	public TextField txtCabecera;
	private LabelComponent lblP456_1,lblP456_11,lblP456_22,lblP456_33;
	private LabelComponent lblblanco1,lblblanco2,lblblanco3,lblblanco4;
	
	CISECCION_02 bean;
	CARATULA_INDIVIDUAL visita;
	CISECCION_02 individualmef;
	private static CISECCION_10_01Fragment_013 caller;
	private static CISECCION_10_01Fragment_011_1Dialog dialog1;
	private static Integer stOrden;
	private static String stNombVacuna;
	private static String stDosis;
	
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesNinio;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	public GridComponent2 grid456_1;
	public GridComponent2 grid456_1_1,grid456_1_2,grid456_1_3;
	 
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco;
	public LabelComponent lblpregunta01,lblpregunta03;
	public IntegerField txtQI220C1,txtQI220C2,txtQI220C3;
	public String fechareferencia;
	
	public boolean tienevalor=true;

	public enum ACTION_NACIMIENTO {
		AGREGAR, EDITAR //, AGREGAR_INTERMEDIO
	}
	public ACTION_NACIMIENTO accion;

	public static CISECCION_10_01Fragment_011_4Dialog newInstance(FragmentForm pagina, CISECCION_10_01Fragment_011_1Dialog Dialogo1, CISECCION_02 detalle, Integer orden,
																																	String nombVacuna,String nombDosisVacuna) {
		caller = (CISECCION_10_01Fragment_013) pagina;
		stNombVacuna=nombVacuna;
		stDosis=nombDosisVacuna;
		stOrden=orden;
		dialog1=Dialogo1;
		CISECCION_10_01Fragment_011_4Dialog f = new CISECCION_10_01Fragment_011_4Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_02) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= bean.qi212;
		String nombre = bean.qi212_nom==null?"":bean.qi212_nom;
		getDialog().setTitle("N� de Orden:  " + orden+"  Nombre: "+nombre);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}

	public CISECCION_10_01Fragment_011_4Dialog() {
		super();
		
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1,"QI456_VAC_G_D1","QI456_VAC_G_M1","QI456_VAC_G_A1","QI456_VAC_G_D2","QI456_VAC_G_M2","QI456_VAC_G_A2","QI456_VAC_G_D3","QI456_VAC_G_M3","QI456_VAC_G_A3","ID","HOGAR_ID","PERSONA_ID","NINIO_ID","VACUNA_ID") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1,"QI456_VAC_G_D1","QI456_VAC_G_M1","QI456_VAC_G_A1","QI456_VAC_G_D2","QI456_VAC_G_M2","QI456_VAC_G_A2","QI456_VAC_G_D3","QI456_VAC_G_M3","QI456_VAC_G_A3") };
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0, -1,-1, "QIVDIA","QIVMES","QIVANIO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesNinio = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
	}
		
	@Override
	protected View createUI() {
		buildFields();
		
		LinearLayout botones = createButtonSection(btnAceptar, btnGrabadoParcial ,btnCancelar);
		q0 = createQuestionSection(lblblanco1,grid456_1.component(),lblblanco2,grid456_1_1.component(),lblblanco3,grid456_1_2.component(),lblblanco4,grid456_1_3.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		
		form.addView(botones);
		return contenedor;
	}

	@Override
	protected void buildFields() {
		
		lblP456_1 = new LabelComponent(getActivity()).textSize(16).size(40, 400).text(R.string.ciseccion_10_01qi456_1).centrar();
		lblP456_11 = new LabelComponent(getActivity()).textSize(16).size(40, 80);
		lblP456_22 = new LabelComponent(getActivity()).textSize(16).size(40, 80);
		lblP456_33 = new LabelComponent(getActivity()).textSize(16).size(40, 80);
		
		lblblanco1 = new LabelComponent(getActivity()).textSize(18).size(53, 112).negrita().centrar();
		lblblanco2 = new LabelComponent(getActivity()).textSize(18).size(33, 112).negrita().centrar();
		lblblanco3 = new LabelComponent(getActivity()).textSize(18).size(13, 112).negrita().centrar();
		lblblanco4 = new LabelComponent(getActivity()).textSize(18).size(13, 112).negrita().centrar();

		txtQI456_VAC_G_D1=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_D1.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44 || inDia==66 || inDia==77){
			    		Util.cleanAndLockView(getActivity(),txtQI456_VAC_G_M1,txtQI456_VAC_G_A1);
				    	Util.lockView(getActivity(), false,txtQI456_VAC_G_D2,txtQI456_VAC_G_M2,txtQI456_VAC_G_A2);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQI456_VAC_G_M1,txtQI456_VAC_G_A1);
				}
			}
		});
		txtQI456_VAC_G_M1=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_A1=new IntegerField(this.getActivity()).size(50, 120).maxLength(4).callback("ontxtQI456_VAC_1_AChangeValue");
		
		txtQI456_VAC_G_D2=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_D2.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44 || inDia==66 || inDia==77){
			    		Util.cleanAndLockView(getActivity(),txtQI456_VAC_G_M2,txtQI456_VAC_G_A2);
			    		Util.lockView(getActivity(), false,txtQI456_VAC_G_D3,txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQI456_VAC_G_M2,txtQI456_VAC_G_A2);
				}
			}
		});
		txtQI456_VAC_G_M2=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_A2=new IntegerField(this.getActivity()).size(50, 120).maxLength(4).callback("ontxtQI456_VAC_2_AChangeValue");
		
		txtQI456_VAC_G_D3=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_D3.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
					tienevalor = true;
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44 || inDia==66 || inDia==77)
			    		Util.cleanAndLockView(getActivity(),txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
			    	else
			    		
			    		Util.lockView(getActivity(), false,txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
				}else{
					Log.e("ffff", "ff"+s.toString());
					tienevalor = false;
				}
			}
		});
		txtQI456_VAC_G_M3=new IntegerField(this.getActivity()).size(50, 60).maxLength(2);
		txtQI456_VAC_G_A3=new IntegerField(this.getActivity()).size(50, 120).maxLength(4);
		
		grid456_1=new GridComponent2(this.getActivity(),App.ESTILO,1);
		grid456_1.addComponent(lblP456_1);
		
		grid456_1_1=new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		grid456_1_1.addComponent(lblP456_11);
		grid456_1_1.addComponent(txtQI456_VAC_G_D1);
		grid456_1_1.addComponent(txtQI456_VAC_G_M1);
		grid456_1_1.addComponent(txtQI456_VAC_G_A1);
		
		grid456_1_2=new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		grid456_1_2.addComponent(lblP456_22);
		grid456_1_2.addComponent(txtQI456_VAC_G_D2);
		grid456_1_2.addComponent(txtQI456_VAC_G_M2);
		grid456_1_2.addComponent(txtQI456_VAC_G_A2);
		
		
		grid456_1_3=new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		grid456_1_3.addComponent(lblP456_33);
		grid456_1_3.addComponent(txtQI456_VAC_G_D3);
		grid456_1_3.addComponent(txtQI456_VAC_G_M3);
		grid456_1_3.addComponent(txtQI456_VAC_G_A3);
		
		
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);

		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CISECCION_10_01Fragment_011_4Dialog.this.dismiss();
			}
		});

		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
//////////NICOLAS INI		01/01/2024		
				//ToastMessage.msgBox(getActivity(), "aceptar", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);	
				vacuna = getCuestionarioService().getCISECCION_10_03(bean.id, bean.hogar_id,bean.persona_id,bean.qi212,stOrden, seccionesPersonas);
				if(!Util.esDiferente(vacuna.vacuna_id, 3) || !Util.esDiferente(vacuna.vacuna_id, 4) || !Util.esDiferente(vacuna.vacuna_id, 5 )|| !Util.esDiferente(vacuna.vacuna_id, 7)|| !Util.esDiferente(vacuna.vacuna_id, 8) || !Util.esDiferente(vacuna.vacuna_id, 10 ) 
						|| !Util.esDiferente(vacuna.vacuna_id, 12 )){
					SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
			   	 	Date d1, d2, d3;
					try {
						d1 = sdformat.parse(txtQI456_VAC_G_A1.getText().toString()+"-"+txtQI456_VAC_G_M1.getText().toString()+"-"+txtQI456_VAC_G_D1.getText().toString());
						d2 = sdformat.parse(txtQI456_VAC_G_A2.getText().toString()+"-"+txtQI456_VAC_G_M2.getText().toString()+"-"+txtQI456_VAC_G_D2.getText().toString());
						d3 = sdformat.parse(txtQI456_VAC_G_A3.getText().toString()+"-"+txtQI456_VAC_G_M3.getText().toString()+"-"+txtQI456_VAC_G_D3.getText().toString());
												
						Log.e("d1",txtQI456_VAC_G_A1.getText().toString()+"-"+txtQI456_VAC_G_M1.getText().toString()+"-"+txtQI456_VAC_G_D1.getText().toString());
						Log.e("d2",txtQI456_VAC_G_A2.getText().toString()+"-"+txtQI456_VAC_G_M2.getText().toString()+"-"+txtQI456_VAC_G_D2.getText().toString());
						Log.e("d3",txtQI456_VAC_G_A3.getText().toString()+"-"+txtQI456_VAC_G_M3.getText().toString()+"-"+txtQI456_VAC_G_D3.getText().toString());

						
//					if( d1.compareTo(d2) > 0 && d1.compareTo(d3) > 0) {
//						ToastMessage.msgBox(getActivity(), "La fecha 1 no puede ser mayor a fecha 2 o fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//					}else if(d2.compareTo(d3) > 0) {
//					    ToastMessage.msgBox(getActivity(), "La fecha 2 no puede ser mayor a fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//					}else if(d1.compareTo(d2) == 0 || d1.compareTo(d3) == 0 || d2.compareTo(d3) == 0) {
//					    ToastMessage.msgBox(getActivity(), "fecha no pueden ser iguales", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//					 }        
					
					if(d1.before(d2) && d1.before(d3)){
						if (d2.after(d3)) {
							ToastMessage.msgBox(getActivity(), "La fecha 2 no puede ser mayor a fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
						}

						if (d2.before(d3)) {
						    
						}

						if (d2.equals(d3)) {
							ToastMessage.msgBox(getActivity(), "La fecha 2 no puede ser igual a la fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
						}
					}else if(d1.after(d2) && d1.after(d3)){
						ToastMessage.msgBox(getActivity(), "La fecha 1 no puede ser mayor a la fecha 2 y a la fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					}else if(d2.before(d1) && d2.after(d3)){
						ToastMessage.msgBox(getActivity(), "La fecha 2 no puede ser menor a la fecha 1 y mayor a la fecha 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					}else if(d3.before(d1) && d3.before(d2)){
						ToastMessage.msgBox(getActivity(), "La fecha 3 no puede ser menor a la fecha 1 y menor a la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					}else if(d1.equals(d2) && d2.equals(d3)){
						ToastMessage.msgBox(getActivity(), "Las tres fechas no pueden ser iguales", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					}
					
					

					} catch (java.text.ParseException e) {					
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
				
////////NICOLAS FIN				
//				caller.refrescarPersonas(bean);
				dialog1.refrescarPersonas();
				CISECCION_10_01Fragment_011_4Dialog.this.dismiss();
			}
		});		
		

		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag=grabadoParcial();
				boolean flagdetalle=grabadoParcial();
				if(!flagdetalle){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), CISECCION_10_01Fragment_011_4Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
	}
	
	public boolean grabar(){
		uiToEntity(vacuna);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			if(!getCuestionarioService().saveOrUpdate(vacuna, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
				
			if(dialog1.chbEDAD.isChecked()){
					bean.qiedad_gest=0;
			}
			else{
				 if(dialog1.txtQIEDAD_GEST.getText()!= null && dialog1.txtQIEDAD_GEST.getText().toString().trim().length()!=0) {
					 bean.qiedad_gest=Integer.parseInt(dialog1.txtQIEDAD_GEST.getText().toString());
				 }
				 else{
					 bean.qiedad_gest=null;
				 }
			}

			if(!getCuestionarioService().saveOrUpdate(bean,null,dialog1.seccionesgrabadoseccion2)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}

		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return flag;
	}

	public boolean grabadoParcial(){
		//uiToEntity(entidad);
		if (!validarparcial()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		/*try {
			if(!getCuestionarioService().saveOrUpdate(entidad, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}*/
		return flag;
	}
	
	public boolean validar(){
///////////////////////// NICOLAS INI	
			if(txtQI456_VAC_G_D3.getText()==null || txtQI456_VAC_G_D3.getText().toString().equals("")){

			SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
	   	 	Date d1, d2;
			try {
				d1 = sdformat.parse(txtQI456_VAC_G_A1.getText().toString()+"-"+txtQI456_VAC_G_M1.getText().toString()+"-"+txtQI456_VAC_G_D1.getText().toString());
				d2 = sdformat.parse(txtQI456_VAC_G_A2.getText().toString()+"-"+txtQI456_VAC_G_M2.getText().toString()+"-"+txtQI456_VAC_G_D2.getText().toString());				

				if(d1.compareTo(d2) > 0) {
					ToastMessage.msgBox(getActivity(), "La fecha 1 no puede ser mayor a la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }else if(d1.compareTo(d2) < 0) {
//		        	 ToastMessage.msgBox(getActivity(), "La fecha 1 ocurre después de la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         } else if(d1.compareTo(d2) == 0) {
		        	 ToastMessage.msgBox(getActivity(), "las fechas no pueden ser iguales", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }
				
			} catch (java.text.ParseException e) {				
			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			mensaje ="el dia 3 no existe";
			view = txtQI456_VAC_G_D3;
			error=true;
			//return false;			
		}		
//////////////CASO QUE TENGAN 2 VACUNAS DT=6//////////////
			
			if(!Util.esDiferente(vacuna.vacuna_id, 6)){
			Log.e("aaaa","vacuna_id");
			if(txtQI456_VAC_G_D2.getText()!=null && !txtQI456_VAC_G_D2.getText().toString().equals("")){
			Log.e("bbbb","entro");
			SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
	   	 	Date d2, d3;
			try {
				d2 = sdformat.parse(txtQI456_VAC_G_A2.getText().toString()+"-"+txtQI456_VAC_G_M2.getText().toString()+"-"+txtQI456_VAC_G_D2.getText().toString());
				d3 = sdformat.parse(txtQI456_VAC_G_A3.getText().toString()+"-"+txtQI456_VAC_G_M3.getText().toString()+"-"+txtQI456_VAC_G_D3.getText().toString());				

				if(d2.compareTo(d3) > 0) {
					ToastMessage.msgBox(getActivity(), "La fecha 1 no puede ser mayor a la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }else if(d2.compareTo(d3) < 0) {
//		        	 ToastMessage.msgBox(getActivity(), "La fecha 1 ocurre después de la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         } else if(d2.compareTo(d3) == 0) {
		        	 ToastMessage.msgBox(getActivity(), "las fechas no pueden ser iguales", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }
				
			} catch (java.text.ParseException e) {				
			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			mensaje ="el dia 3 no existe";
			view = txtQI456_VAC_G_D2;
			error=true;
			//return false;			
			}
		}
			
//////////////CASO QUE TENGAN 2 VACUNAS CASO 11,13,14,17,18,19//////////////
			
			
			if(!Util.esDiferente(vacuna.vacuna_id, 11) || !Util.esDiferente(vacuna.vacuna_id, 13) || !Util.esDiferente(vacuna.vacuna_id, 14)|| !Util.esDiferente(vacuna.vacuna_id, 17)
					|| !Util.esDiferente(vacuna.vacuna_id, 18)|| !Util.esDiferente(vacuna.vacuna_id, 19)){			
			if(txtQI456_VAC_G_D2.getText()!=null && !txtQI456_VAC_G_D2.getText().toString().equals("")){			
			SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
			
			
	   	 	Date d1, d2;
			try {
				d1 = sdformat.parse(txtQI456_VAC_G_A1.getText().toString()+"-"+txtQI456_VAC_G_M1.getText().toString()+"-"+txtQI456_VAC_G_D1.getText().toString());
				d2 = sdformat.parse(txtQI456_VAC_G_A2.getText().toString()+"-"+txtQI456_VAC_G_M2.getText().toString()+"-"+txtQI456_VAC_G_D2.getText().toString());				

				if(d1.compareTo(d2) > 0) {
					ToastMessage.msgBox(getActivity(), "La fecha 1 no puede ser mayor a la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }else if(d1.compareTo(d2) < 0) {
//		        	 ToastMessage.msgBox(getActivity(), "La fecha 1 ocurre después de la fecha 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         } else if(d1.compareTo(d2) == 0) {
		        	 ToastMessage.msgBox(getActivity(), "las fechas no pueden ser iguales", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		         }
				
			} catch (java.text.ParseException e) {				
			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			mensaje ="el dia 3 no existe";
			view = txtQI456_VAC_G_D2;
			error=true;
			//return false;			
				
			}
		}
		
		
///////////////////////// NICOLAS FIN		
		if(txtQI456_VAC_G_D1.getText()!=null && !txtQI456_VAC_G_D1.getText().toString().equals("")){
	    	if(Util.esMayor(Integer.parseInt(txtQI456_VAC_G_D1.getText().toString()), 31) && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=44
	    	   && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=66 && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=77
	    	   && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=98){
	    		mensaje ="dia fuera de rango";
				view = txtQI456_VAC_G_D1;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_M1.getText()!=null && !txtQI456_VAC_G_M1.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M1.getText().toString()),1) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_M1.getText().toString()),12))) {
	    		mensaje ="Mes fuera de rango";
				view = txtQI456_VAC_G_M1;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_A1.getText()!=null && !txtQI456_VAC_G_A1.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_A1.getText().toString()),2000) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_A1.getText().toString()),App.ANIOPORDEFECTOSUPERIOR))) {
	    		mensaje ="A�o fuera de rango";
				view = txtQI456_VAC_G_A1;
				error=true;
				return false;
	    	}
    	}
    	if(txtQI456_VAC_G_D1.getText()!=null && txtQI456_VAC_G_M1.getText()!=null && !txtQI456_VAC_G_D1.getText().toString().equals("") && !txtQI456_VAC_G_M1.getText().toString().equals("")
    			 && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=44 && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=66
    			 && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=77 && Integer.parseInt(txtQI456_VAC_G_D1.getText().toString())!=98){
	    	if(Util.esMenor(Integer.parseInt(txtQI456_VAC_G_D1.getText().toString()), 32) && Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M1.getText().toString()), 13)){
	    		Integer dia=Integer.parseInt(txtQI456_VAC_G_D1.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M1.getText().toString());    		
	    		
	    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
	    			mensaje ="Dia no corresponde al mes que eligi�";
	    			view = txtQI456_VAC_G_D1;
	    			error=true;
	    			return false;
	    		}
	    	}
    	}
    	
    	if(txtQI456_VAC_G_D2.getText()!=null && !txtQI456_VAC_G_D2.getText().toString().equals("") && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=44
 	    	   && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=66 && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=77
 	    	   && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=98){
	    	if(Util.esMayor(Integer.parseInt(txtQI456_VAC_G_D2.getText().toString()), 31)){
	    		mensaje ="dia fuera de rango";
				view = txtQI456_VAC_G_D2;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_M2.getText()!=null  && !txtQI456_VAC_G_M2.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M2.getText().toString()),1) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_M2.getText().toString()),12))) {
	    		mensaje ="Mes fuera de rango";
				view = txtQI456_VAC_G_M2;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_A2.getText()!=null  && !txtQI456_VAC_G_A2.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_A2.getText().toString()),2000) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_A2.getText().toString()),App.ANIOPORDEFECTOSUPERIOR))) {
	    		mensaje ="A�o fuera de rango";
				view = txtQI456_VAC_G_A2;
				error=true;
				return false;
	    	}
    	}
    	if(txtQI456_VAC_G_D2.getText()!=null && txtQI456_VAC_G_M2.getText()!=null && !txtQI456_VAC_G_D2.getText().toString().equals("")  && !txtQI456_VAC_G_M2.getText().toString().equals("")
    		&& Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=44 && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=66
   			 && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=77 && Integer.parseInt(txtQI456_VAC_G_D2.getText().toString())!=98){
	    	if(Util.esMenor(Integer.parseInt(txtQI456_VAC_G_D2.getText().toString()), 32) && Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M2.getText().toString()), 13)){
	    		Integer dia=Integer.parseInt(txtQI456_VAC_G_D2.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M2.getText().toString());    		
	    		
	    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
	    			mensaje ="Dia no corresponde al mes que eligi�";
	    			view = txtQI456_VAC_G_D2;
	    			error=true;
	    			return false;
	    		}
	    	}
    	}
    	
    	if(txtQI456_VAC_G_D3.getText()!=null && !txtQI456_VAC_G_D3.getText().toString().equals("") && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=44
  	    	   && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=66 && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=77
  	    	   && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=98){
	    	if(Util.esMayor(Integer.parseInt(txtQI456_VAC_G_D3.getText().toString()), 31)){
	    		mensaje ="dia fuera de rango";
				view = txtQI456_VAC_G_D3;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_M3.getText()!=null && !txtQI456_VAC_G_M3.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M3.getText().toString()),1) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_M3.getText().toString()),12))) {
	    		mensaje ="Mes fuera de rango";
				view = txtQI456_VAC_G_M3;
				error=true;
				return false;
	    	}
    	}
		if(txtQI456_VAC_G_A3.getText()!=null && !txtQI456_VAC_G_A3.getText().toString().equals("")){
	    	if ((Util.esMenor(Integer.parseInt(txtQI456_VAC_G_A3.getText().toString()),2000) ||  Util.esMayor(Integer.parseInt(txtQI456_VAC_G_A3.getText().toString()),App.ANIOPORDEFECTOSUPERIOR))) {
	    		mensaje ="A�o fuera de rango";
				view = txtQI456_VAC_G_A3;
				error=true;
				return false;
	    	}
    	}
    	if(txtQI456_VAC_G_D3.getText()!=null && txtQI456_VAC_G_M3.getText()!=null && !txtQI456_VAC_G_D3.getText().toString().equals("") && !txtQI456_VAC_G_M3.getText().toString().equals("")
    		 && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=44 && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=66
   			 && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=77 && Integer.parseInt(txtQI456_VAC_G_D3.getText().toString())!=98){
	    	if(Util.esMenor(Integer.parseInt(txtQI456_VAC_G_D3.getText().toString()), 32) && Util.esMenor(Integer.parseInt(txtQI456_VAC_G_M3.getText().toString()), 13)){
	    		Integer dia=Integer.parseInt(txtQI456_VAC_G_D3.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M3.getText().toString());
	    		
	    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
	    			mensaje ="Dia no corresponde al mes que eligi�";
	    			view = txtQI456_VAC_G_D3;
	    			error=true;
	    			return false;
	    		}
	    	}
    	}
    	
    	if(txtQI456_VAC_G_D1.getText()!=null && txtQI456_VAC_G_M1.getText()!=null && txtQI456_VAC_G_A1.getText()!=null &&
  			  !txtQI456_VAC_G_D1.getText().toString().equals("") && !txtQI456_VAC_G_M1.getText().toString().equals("") && !txtQI456_VAC_G_A1.getText().toString().equals("") ){
    		
			Integer numOrdenMax= getCuestionarioService().getnumOrdenMaxVisitaCI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			visita = getCuestionarioService().getVisitaI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,numOrdenMax,seccionesCargado);
			individualmef = getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesNinio); 
			if (visita!=null && individualmef!=null) {
				Integer diaMin  = Integer.parseInt(individualmef.qi215d);
				Integer mesMin  = Integer.parseInt(individualmef.qi215m);
				Integer anioMin = individualmef.qi215y;
				Integer edad = individualmef.qi217;
				if (mesMin==98 || anioMin==9998) {
					diaMin  = Integer.parseInt(visita.qivdia);
					mesMin  = Integer.parseInt(visita.qivmes);
					anioMin = visita.qivanio-edad;
				}
				
				Integer diaMax  = Integer.parseInt(visita.qivdia);
				Integer mesMax  = Integer.parseInt(visita.qivmes);
				Integer anioMax = visita.qivanio;
				
				Integer dia=Integer.parseInt(txtQI456_VAC_G_D1.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M1.getText().toString()); 
	    		Integer anio=Integer.parseInt(txtQI456_VAC_G_A1.getText().toString());
				
				Calendar fechamin = new GregorianCalendar(anioMin , mesMin-1, diaMin-1);
				Calendar fechavacuna = new GregorianCalendar(anio, mes-1, dia);
				Calendar fechamax = new GregorianCalendar(anioMax,mesMax -1, diaMax+1 );
				
			    if(!(fechamin.compareTo(fechavacuna)==-1 && fechamax.compareTo(fechavacuna)==1)){ 
			    	Log.e("","AQUI");
		    			mensaje ="Fecha fuera de rango";
		    			view = txtQI456_VAC_G_D1;
		    			error=true;
		    			return false;
			    }
			}
		}
    	
    	if(txtQI456_VAC_G_D2.getText()!=null && txtQI456_VAC_G_M2.getText()!=null && txtQI456_VAC_G_A2.getText()!=null &&
    			  !txtQI456_VAC_G_D2.getText().toString().equals("") && !txtQI456_VAC_G_M2.getText().toString().equals("") && !txtQI456_VAC_G_A2.getText().toString().equals("") ){
      		
  			Integer numOrdenMax= getCuestionarioService().getnumOrdenMaxVisitaCI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
  			visita = getCuestionarioService().getVisitaI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,numOrdenMax,seccionesCargado);
  			individualmef = getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesNinio);
  			
  			if (visita!=null && individualmef!=null) {
  				Integer diaMin  = Integer.parseInt(individualmef.qi215d);
  				Integer mesMin  = Integer.parseInt(individualmef.qi215m);
  				Integer anioMin = individualmef.qi215y;
  				Integer edad = individualmef.qi217;
  				if (mesMin==98 || anioMin==9998) {
  					diaMin  = Integer.parseInt(visita.qivdia);
  					mesMin  = Integer.parseInt(visita.qivmes);
  					anioMin = visita.qivanio-edad;
  				}
  				
  				Integer diaMax  = Integer.parseInt(visita.qivdia);
  				Integer mesMax  = Integer.parseInt(visita.qivmes);
  				Integer anioMax = visita.qivanio;
  				
  				Integer dia=Integer.parseInt(txtQI456_VAC_G_D2.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M2.getText().toString()); 
	    		Integer anio=Integer.parseInt(txtQI456_VAC_G_A2.getText().toString());
  				
	    		Calendar fechamin = new GregorianCalendar(anioMin , mesMin-1, diaMin-1);
				Calendar fechavacuna = new GregorianCalendar(anio, mes-1, dia);
				Calendar fechamax = new GregorianCalendar(anioMax,mesMax -1, diaMax+1 );
  				
  				
				if(!(fechamin.compareTo(fechavacuna)==-1 && fechamax.compareTo(fechavacuna)==1)){
					Log.e("","AQUI OTRO");
  		    			mensaje ="Fecha fuera de rango";
  		    			view = txtQI456_VAC_G_D2;
  		    			error=true;
  		    			return false;
  			    }
  			}
  		}
    	if(txtQI456_VAC_G_D3.getText()!=null && txtQI456_VAC_G_M3.getText()!=null && txtQI456_VAC_G_A3.getText()!=null &&
  			  !txtQI456_VAC_G_D3.getText().toString().equals("") && !txtQI456_VAC_G_M3.getText().toString().equals("") && !txtQI456_VAC_G_A3.getText().toString().equals("") ){
    		
			Integer numOrdenMax= getCuestionarioService().getnumOrdenMaxVisitaCI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			visita = getCuestionarioService().getVisitaI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,numOrdenMax,seccionesCargado);
			individualmef = getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesNinio);
			
			if (visita!=null && individualmef!=null) {
				Integer diaMin  = Integer.parseInt(individualmef.qi215d);
				Integer mesMin  = Integer.parseInt(individualmef.qi215m);
				Integer anioMin = individualmef.qi215y;
				Integer edad = individualmef.qi217;
				if (mesMin==98 || anioMin==9998) {
					diaMin  = Integer.parseInt(visita.qivdia);
					mesMin  = Integer.parseInt(visita.qivmes);
					anioMin = visita.qivanio-edad;
				}
				
				Integer diaMax  = Integer.parseInt(visita.qivdia);
				Integer mesMax  = Integer.parseInt(visita.qivmes);
				Integer anioMax = visita.qivanio;
				
				Integer dia=Integer.parseInt(txtQI456_VAC_G_D3.getText().toString());
	    		Integer mes=Integer.parseInt(txtQI456_VAC_G_M3.getText().toString()); 
	    		Integer anio=Integer.parseInt(txtQI456_VAC_G_A3.getText().toString());
				
	    		Calendar fechamin = new GregorianCalendar(anioMin , mesMin-1, diaMin-1);
				Calendar fechavacuna = new GregorianCalendar(anio, mes-1, dia);
				Calendar fechamax = new GregorianCalendar(anioMax,mesMax -1, diaMax+1 );
				
				
				if(!(fechamin.compareTo(fechavacuna)==-1 && fechamax.compareTo(fechavacuna)==1)){
					Log.e("","ULTIMO");
		    			mensaje ="Fecha fuera de rango";
		    			view = txtQI456_VAC_G_D3;
		    			error=true;
		    			return false;
			    }
			}
		}
	    return true;
	}
	
	public boolean validarparcial(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		return true;
	}
	
	private void cargarDatos() {
		vacuna = getCuestionarioService().getCISECCION_10_03(bean.id, bean.hogar_id,bean.persona_id,bean.qi212,stOrden, seccionesPersonas);
		if(vacuna==null){
			vacuna = new CISECCION_10_03();
			vacuna.id = bean.id;
			vacuna.hogar_id= bean.hogar_id;
			vacuna.persona_id= bean.persona_id;
			vacuna.ninio_id = bean.qi212;
			vacuna.vacuna_id = stOrden;
		}
		entityToUI(vacuna);
		RenombrarEtiquetas(stOrden);
		inicio();
	}
	
	private void inicio() {
		txtQI456_VAC_G_D1.requestFocus();
		EsconderoMostrarSegunvacuna(stOrden);
		ValidarsiesSupervisora();
	}
	public void EsconderoMostrarSegunvacuna(Integer vacuna){
		if(!Util.esDiferente(vacuna, 1,2,9,15,16)){
			Util.cleanAndLockView(getActivity(), txtQI456_VAC_G_D2,txtQI456_VAC_G_M2,txtQI456_VAC_G_A2,txtQI456_VAC_G_D3,txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
			grid456_1_2.setVisibility(View.GONE);
			grid456_1_3.setVisibility(View.GONE);
		}
		if(!Util.esDiferente(vacuna, 11,13,14,17,18,19)){
			Util.cleanAndLockView(getActivity(),txtQI456_VAC_G_D3,txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
			grid456_1_3.setVisibility(View.GONE);
		}
		if(!Util.esDiferente(vacuna,6)){
			Util.cleanAndLockView(getActivity(), txtQI456_VAC_G_D1,txtQI456_VAC_G_M1,txtQI456_VAC_G_A1);
			grid456_1_1.setVisibility(View.GONE);
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	 public void ValidarsiesSupervisora(){
	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		txtQI456_VAC_G_D1.readOnly();
	    		txtQI456_VAC_G_M1.readOnly();
	    		txtQI456_VAC_G_A1.readOnly();
	    		txtQI456_VAC_G_D2.readOnly();
	    		txtQI456_VAC_G_M2.readOnly();
	    		txtQI456_VAC_G_A2.readOnly();
	    		txtQI456_VAC_G_D3.readOnly();
	    		txtQI456_VAC_G_M3.readOnly();
	    		txtQI456_VAC_G_A3.readOnly();
	    		btnAceptar.setEnabled(false);
	    		btnGrabadoParcial.setEnabled(false);
	    	}
	    }
	public void RenombrarEtiquetas(Integer vacuna_id) {
		if(!Util.esDiferente(vacuna_id, 1)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_1);
			lblP456_11.text(R.string.ciseccion_10_01qi456_1_1);
		}
		if(!Util.esDiferente(vacuna_id, 2)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_2);
			lblP456_11.text(R.string.ciseccion_10_01qi456_2_1);
		}
		if(!Util.esDiferente(vacuna_id, 3)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_3);
			lblP456_11.text(R.string.ciseccion_10_01qi456_3_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_3_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_3_3);
		}
		if(!Util.esDiferente(vacuna_id, 4)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_4);
			lblP456_11.text(R.string.ciseccion_10_01qi456_4_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_4_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_4_3);
		}
		if(!Util.esDiferente(vacuna_id, 5)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_5);
			lblP456_11.text(R.string.ciseccion_10_01qi456_5_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_5_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_5_3);
		}
		if(!Util.esDiferente(vacuna_id, 6)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_6);
			lblP456_22.text(R.string.ciseccion_10_01qi456_6_1);
			lblP456_33.text(R.string.ciseccion_10_01qi456_6_2);
		}
		if(!Util.esDiferente(vacuna_id, 7)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_7);
			lblP456_11.text(R.string.ciseccion_10_01qi456_7_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_7_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_7_3);
		}
		if(!Util.esDiferente(vacuna_id, 8)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_8);
			lblP456_11.text(R.string.ciseccion_10_01qi456_8_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_8_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_8_3);
		}
		if(!Util.esDiferente(vacuna_id, 9)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_9);
			lblP456_11.text(R.string.ciseccion_10_01qi456_9_1);
		}
		if(!Util.esDiferente(vacuna_id, 10)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_10);
			lblP456_11.text(R.string.ciseccion_10_01qi456_10_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_10_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_10_3);
		}
		if(!Util.esDiferente(vacuna_id, 11)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_11);
			lblP456_11.text(R.string.ciseccion_10_01qi456_11_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_11_2);
		}
		if(!Util.esDiferente(vacuna_id, 12)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_12);
			lblP456_11.text(R.string.ciseccion_10_01qi456_12_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_12_2);
			lblP456_33.text(R.string.ciseccion_10_01qi456_12_3);
		}
		if(!Util.esDiferente(vacuna_id, 13)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_13);
			lblP456_11.text(R.string.ciseccion_10_01qi456_13_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_13_2);
		}
		if(!Util.esDiferente(vacuna_id, 14)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_14);
			lblP456_11.text(R.string.ciseccion_10_01qi456_14_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_14_2);
		}
		if(!Util.esDiferente(vacuna_id, 15)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_15);
			lblP456_11.text(R.string.ciseccion_10_01qi456_15_1);
		}
		if(!Util.esDiferente(vacuna_id, 16)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_19);
			lblP456_11.text(R.string.ciseccion_10_01qi456_19_1);
		}
		if(!Util.esDiferente(vacuna_id, 17)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_16);
			lblP456_11.text(R.string.ciseccion_10_01qi456_16_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_16_2);
		}
		if(!Util.esDiferente(vacuna_id, 18)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_17);
			lblP456_11.text(R.string.ciseccion_10_01qi456_17_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_17_2);
		}
		if(!Util.esDiferente(vacuna_id, 19)){
			lblP456_1.text(R.string.ciseccion_10_01qi456_18);
			lblP456_11.text(R.string.ciseccion_10_01qi456_18_1);
			lblP456_22.text(R.string.ciseccion_10_01qi456_18_2);
		}
		
    }
	public void ontxtQI456_VAC_1_AChangeValue() {
		Util.lockView(getActivity(), false,txtQI456_VAC_G_D2,txtQI456_VAC_G_M2,txtQI456_VAC_G_A2);
		txtQI456_VAC_G_D2.requestFocus();
	}
	public void ontxtQI456_VAC_2_AChangeValue() {
		Util.lockView(getActivity(), false,txtQI456_VAC_G_D3,txtQI456_VAC_G_M3,txtQI456_VAC_G_A3);
		txtQI456_VAC_G_D3.requestFocus();
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_10_01Fragment_011_4Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
}