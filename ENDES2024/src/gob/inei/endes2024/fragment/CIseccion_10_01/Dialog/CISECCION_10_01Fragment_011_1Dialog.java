package gob.inei.endes2024.fragment.CIseccion_10_01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_013;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_03;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.N_LITERAL;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.w3c.dom.ls.LSInput;

import android.view.Gravity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_011_1Dialog  extends DialogFragmentComponent {


	@FieldAnnotation(orderIndex = 1)
	public IntegerField txtQIEDAD_GEST;
	
	@FieldAnnotation(orderIndex = 2)
	public CheckBoxField chbEDAD;
	@FieldAnnotation(orderIndex = 3)
	public CheckBoxField chbQI456_1A;
	@FieldAnnotation(orderIndex = 4)
	public CheckBoxField chbQI456_1B;
	@FieldAnnotation(orderIndex = 5)
	public CheckBoxField chbQI456_1X;
	@FieldAnnotation(orderIndex = 6)
	public TextField txtQI456_1O;
	
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnAceptar;

	public GridComponent2 gridEdadGestacional;
	public TableComponent tcN1_literal;
	LabelComponent lbledadgestacional,lbledadgestacionalNoReg,lblblanco,lblpregunta456,lblpregunta456_ind2,lblpregunta456_ind,lblTitulo3;
	private static CISECCION_10_01Fragment_013 caller;
	private CuestionarioService service;
	private N_LITERAL seleccionado;
	private List<N_LITERAL> lstN1_Literal;
	public List<CISECCION_10_03> detalles;
	CISECCION_10_03 entidad=null;
	CISECCION_02 bean;
	CISECCION_10_04 bean10_04;
	CISECCION_04B individual4B2;
	List<CISECCION_10_03> Listadogeneral = new ArrayList<CISECCION_10_03>();
	SeccionCapitulo[] seccionesCargado,seccionescargadoseccion2,seccionesgrabadoseccion2, seccionesCargado2,seccionesGrabado2;
	private DialogComponent dialog;
	
	
	
	private enum ACTION {
		ELIMINAR, MENSAJE
	}
	private ACTION action;
	
	LinearLayout q0, q1,q2,q3,q4;

	private CuestionarioService cuestionarioService;

	TC_ClickListener listener;

	public static CISECCION_10_01Fragment_011_1Dialog newInstance(FragmentForm pagina, CISECCION_02 detalle) {
		caller = (CISECCION_10_01Fragment_013) pagina;
		CISECCION_10_01Fragment_011_1Dialog f = new CISECCION_10_01Fragment_011_1Dialog();
		f.setParent(caller);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public CISECCION_10_01Fragment_011_1Dialog() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_02) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
			Integer orden= bean.qi212;
			String nombre = bean.qi212_nom==null?"":bean.qi212_nom;
			String fecha=bean.getQi215();
			getDialog().setTitle("N�:  " + orden+"  Nom.: "+nombre+" F.Nac.: "+fecha);
			
			
			
			final View rootView = createUI();
			initObjectsWithoutXML(this, rootView);
		
			listener = new TC_ClickListener();
			tcN1_literal.getListView().setOnItemClickListener(listener);
			seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI456_VAC_G_D1","QI456_VAC_G_M1","QI456_VAC_G_A1","QI456_VAC_G_D2","QI456_VAC_G_M2","QI456_VAC_G_A2","QI456_VAC_G_D3","QI456_VAC_G_M3","QI456_VAC_G_A3","ID","HOGAR_ID","PERSONA_ID","NINIO_ID","VACUNA_ID") };
			seccionescargadoseccion2=new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "ID","HOGAR_ID","PERSONA_ID","QI212","QI212_NOM","QIEDAD_GEST") };
			seccionesgrabadoseccion2=new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QIEDAD_GEST") };
			
			seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI456_1A","QI456_1B","QI456_1X","QI456_1O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
			seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI454","QI456_1A","QI456_1B","QI456_1X","QI456_1O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
			cargarDatos();
			enlazarCajas();
			listening();
			return rootView;
		}

	@Override
	public void onDismiss(DialogInterface dialog) {
	}
	
	@Override
	protected void buildFields() {
		lbledadgestacional=new LabelComponent(getActivity()).size(altoComponente, 200).text("Edad Gestacional: ").negrita();
		lbledadgestacionalNoReg=new LabelComponent(getActivity()).size(altoComponente, 200).text("No Registra Edad Gestacional");
		lblblanco=new LabelComponent(getActivity()).size(altoComponente, 50).text("");
		txtQIEDAD_GEST=new IntegerField(getActivity()).size(altoComponente, 80).centrar().maxLength(2);
		
		
		lblTitulo3=new LabelComponent(this.getActivity()).text(R.string.c2seccion_04bqi456_1).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		
		chbQI456_1A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI456_1B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		txtQI456_1O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar(); 
		chbQI456_1X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456_1x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
 
		chbQI456_1X.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked ){
					
					Util.lockView(getActivity(), false,txtQI456_1O);
		  		}
				else {
					Util.cleanAndLockView(getActivity(),txtQI456_1O);
		  		}
			}
		});
		
		
		tcN1_literal = new TableComponent(getActivity(), this, App.ESTILO).size(900, 740).headerHeight(65).dataColumHeight(60);		
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466_cod,0.5f,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466_vac,3,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466_fec1,1.8f,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466_fec2,1.8f,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.ciseccion_10_01qi466_fec3,1.8f,TableComponent.ALIGN.LEFT);
		tcN1_literal.setColorCondition("n1", Util.getHMObject("16",R.color.dark_pink_agua,"1",R.color.azulmarino_claro));
		tcN1_literal.setColorCondition("n1", Util.getHMObject("17",R.color.dark_pink_agua,"1",R.color.azulmarino_claro));
		tcN1_literal.setColorCondition("n1", Util.getHMObject("18",R.color.dark_pink_agua,"1",R.color.azulmarino_claro));
		
		chbEDAD=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 50).callback("onchbEDADChangeValue");
			
		btnAceptar = new ButtonComponent(getParent().getActivity(), App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				CISECCION_10_01Fragment_011_1Dialog.this.dismiss();
			}
		});
		gridEdadGestacional= new GridComponent2(getActivity(),App.ESTILO,5,0);
		gridEdadGestacional.addComponent(lbledadgestacional);
		gridEdadGestacional.addComponent(txtQIEDAD_GEST);		
		gridEdadGestacional.addComponent(lblblanco);
		gridEdadGestacional.addComponent(chbEDAD);
		gridEdadGestacional.addComponent(lbledadgestacionalNoReg);
		
	}

	@Override
	protected View createUI() {
		buildFields();
		q1=createQuestionSection(gridEdadGestacional.component());
		q2 = createQuestionSection(tcN1_literal.getTableView());

		LinearLayout ly1032 = new LinearLayout(getActivity());
				ly1032.addView(chbQI456_1X);
				ly1032.addView(txtQI456_1O);


		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblTitulo3,chbQI456_1A,chbQI456_1B,ly1032);

		
		LinearLayout botones = createButtonSection(btnAceptar);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(q2);
		form.addView(q3);

		form.addView(botones);

		return contenedor;
	}


	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuItem editar = menu.add("Editar Registro");
        editar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                	CISECCION_10_03 c = (CISECCION_10_03) Listadogeneral.get((info.position));
                	abrirDetalle(bean, c.vacuna_id, "","");//(CISECCION_10_03) detalles.get(info.position)
                    return true;
                }
        });
   
      }
	
	public void agregarPersona(Integer indice) {
		CISECCION_10_04 beanControl = new CISECCION_10_04();
    	beanControl.id = App.getInstance().getPersonaCuestionarioIndividual().id;
    	beanControl.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    	beanControl.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	beanControl.ninio_id = bean.qi212;
		/*if(indice==null)
			beanControl.qi_ctrl = detalles.size();
		else 
			beanControl.qi_ctrl = indice;
		
		abrirDetalle(beanControl);*/
    	
//    	individual4B2 = getCuestionarioService().getCISECCION_04B(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,bean.qi212,seccionesCargado2);
//    	
//    	if(individual4B2==null){
//    		individual4B2=new CISECCION_04B();
//    		individual4B2.id=App.getInstance().getPersonaCuestionarioIndividual().id;
//    		individual4B2.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
//    		individual4B2.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
//  	    }
		
	}
	
	public void cargarDatos() {
		
//        individual4B2 = getCuestionarioService().getCISECCION_04B(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,bean.qi212,seccionesCargado2);
//        entityToUI(individual4B2);
//        Log.e("Num de controles", "vvvvvvvvvvvv"+individual4B2.qi456_1o+" "+individual4B2.qi456_1a+" "+individual4B2.qi456_1b+" "+individual4B2.qi456_1x);
//        
//        Integer pr = individual4B2.qi456_1a == null ? 0 : individual4B2.qi456_1a;
//        
//    	if(pr >0){
//            Log.e("foco", "vvvvvvvvvvvv"+individual4B2.qi456_1o+" "+individual4B2.qi456_1a+" "+individual4B2.qi456_1b+" "+individual4B2.qi456_1x);
//
//            
//    		chbQI456_1A.setChecked(true);
//    		txtQI456_1O.setText("dddd");
//    	}
//    	if(individual4B2==null){
//    		individual4B2=new CISECCION_04B();
//    		individual4B2.id=App.getInstance().getPersonaCuestionarioIndividual().id;
//    		individual4B2.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
//    		individual4B2.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
//  	    }
    	
    	
		
		bean=getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionescargadoseccion2);
		entityToUI(bean);
		if(bean.qiedad_gest!=null && bean.qiedad_gest==0){
			chbEDAD.setChecked(true);
		}
		cargarTabla();
		inicio();
		
	}
	public void onchbEDADChangeValue(){
		if(chbEDAD.isChecked()){
			Util.cleanAndLockView(getActivity(), txtQIEDAD_GEST);
//			txtQIEDAD_GEST.setText("0");
		}
		else{
			Util.lockView(getActivity(), false,txtQIEDAD_GEST);
		}
	}
	public void cargarTabla(){
		List<CISECCION_10_03> ListadodeVacunas = new ArrayList<CISECCION_10_03>();
		List<CISECCION_10_03> Listita = new ArrayList<CISECCION_10_03>();
		Listadogeneral=new ArrayList<CISECCION_10_03>();
		ListadodeVacunas = getCuestionarioService().getCISECCION_10_03All(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, bean.qi212, seccionesCargado);
		if(ListadodeVacunas.size()>0){
			Listita = TodoLasVacunas();
			for(int i=1; i<=19 ; i++){
				boolean entro=false;
				for(CISECCION_10_03 v: ListadodeVacunas){
					if(v.vacuna_id==i){
						entro=true;
						Listadogeneral.add(v);
					}
				}
				if(!entro){
					Listadogeneral.add(Listita.get(i-1));
				}
			}
		}
		else{
			Listadogeneral = TodoLasVacunas();
		}
		tcN1_literal.setData(Listadogeneral, "getCodigodeVacuna","getNombredeVacuna","PrimeraFecha","SegundaFecha","TerceraFecha");
		registerForContextMenu(tcN1_literal.getListView());
		
		
		
		individual4B2 = getCuestionarioService().getCISECCION_04B(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,bean.qi212,seccionesCargado2);
        //entityToUI(individual4B2);
        Log.e("Num de controles", "vvvvvvvvvvvv"+individual4B2.qi456_1o+" "+individual4B2.qi456_1a+" "+individual4B2.qi456_1b+" "+individual4B2.qi456_1x);
        
        Integer pr1a = individual4B2.qi456_1a == null ? 0 : individual4B2.qi456_1a;
        Integer pr1b = individual4B2.qi456_1b == null ? 0 : individual4B2.qi456_1b;
        Integer pr1x = individual4B2.qi456_1x == null ? 0 : individual4B2.qi456_1x;
        String pr1xt = individual4B2.qi456_1o == null ?  " " : individual4B2.qi456_1o.toString();
        
		//txtQI456_1O.setText("dddd");
        
    	if(pr1a >0){
    		chbQI456_1A.setChecked(true);
    	}
    	if(pr1b >0){
    		chbQI456_1B.setChecked(true);
    	}
    	if(pr1x >0){
    		chbQI456_1X.setChecked(true);
    		txtQI456_1O.setText(pr1xt);
    	}
		
	}
	public List<CISECCION_10_03> TodoLasVacunas(){
		List<CISECCION_10_03> todo = new ArrayList<CISECCION_10_03>();
		for(int i=1; i<=19 ; i++){
			CISECCION_10_03 uno= new CISECCION_10_03();
			uno.vacuna_id=i;
			todo.add(uno);
		}
		return todo;
	}
	
	public void abrirDetalle(CISECCION_02 tmp,Integer orden, String nomb,String dosis) {
		FragmentManager fm = CISECCION_10_01Fragment_011_1Dialog.this.getFragmentManager();
		CISECCION_10_01Fragment_011_4Dialog aperturaDialog = CISECCION_10_01Fragment_011_4Dialog.newInstance(caller, this, tmp, orden, nomb,dosis);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}

	private CuestionarioService getService() {
		if (service == null) {
			service = CuestionarioService.getInstance(getActivity());
		}
		return service;
	}

	private boolean grabar() {
		uiToEntity(bean);
		uiToEntity(individual4B2);  
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) {
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				}
				if (view != null) {
					view.requestFocus();
				}
			}
			return false;
		}
		try {
			if(chbEDAD.isChecked()){
				bean.qiedad_gest=0;
			}
			if(!getCuestionarioService().saveOrUpdate(bean,null,seccionesgrabadoseccion2) || !getCuestionarioService().saveOrUpdate(individual4B2,seccionesGrabado2)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}

	private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		Integer qi456a = individual4B2.qi456_1a == null ? 0 : individual4B2.qi456_1a;
		Integer qi456b = individual4B2.qi456_1b == null ? 0 : individual4B2.qi456_1b;
		Integer qi456x = individual4B2.qi456_1x == null ? 0 : individual4B2.qi456_1x;
		
		if (!chbEDAD.isChecked() && Util.esVacio(bean.qiedad_gest)) {
			mensaje = preguntaVacia.replace("$", "La pregunta EDAD");
			view = txtQIEDAD_GEST;
			error = true;
			return false;
		}
		
//		if (bean.qiedad_gest!=null && (bean.qiedad_gest<0 || bean.qiedad_gest>45)) {
//			mensaje = "Edad Gestacional Fuera de Rango";
//			view = txtQIEDAD_GEST;
//			error = true;
//			return false;
//		}
		if(Util.esMenor(bean.qiedad_gest, 22) && !chbEDAD.isChecked()){
			mensaje = "Edad Gestacional Fuera de Rango";
			view = txtQIEDAD_GEST;
			error = true;
			return false;
		}
		if(Util.between(bean.qiedad_gest,22,28)){
			MyUtil.MensajeGeneral(getActivity(), "Verifique Edad gestacional");
		}
		if(Util.between(bean.qiedad_gest,42,44)){
			MyUtil.MensajeGeneral(getActivity(), "Verifique Edad gestacional");
		}
		if(Util.esMayor(bean.qiedad_gest, 44) && !chbEDAD.isChecked()){
			mensaje = "Edad Gestacional Fuera de Rango";
			view = txtQIEDAD_GEST;
			error = true;
			return false;
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////
		
		if(qi456a==0 && qi456b==0 && qi456x==0){
			mensaje = "Debe seleccionar al menos una alternativa 456";
			view =chbQI456_1A;
			error = true;
			return false;
		}
		
		
		if (chbQI456_1X.isChecked() ) {
			if (Util.esVacio(individual4B2.qi456_1o)) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique 456";
				view = txtQI456_1O;
				error = true;
				return false;
			}
		}
		

		
		
		return true;
	}

	private void inicio() {
		onchbEDADChangeValue();
		btnAceptar.requestFocus();
	}
	public void refrescarPersonas() {
			cargarDatos();
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	private class TC_ClickListener implements OnItemClickListener {

		public TC_ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_10_03 c = (CISECCION_10_03) Listadogeneral.get(arg2);
			abrirDetalle(bean, c.vacuna_id, "","");
		}
	}
}