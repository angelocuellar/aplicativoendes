package gob.inei.endes2024.fragment.CIseccion_10_01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_10_01.CISECCION_10_01Fragment_013;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_10_01Fragment_011_3Dialog  extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI_CTRL_DIA;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI_CTRL_MES;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI_CTRL_ANIO;
	@FieldAnnotation(orderIndex=4)
	public DecimalField txtQI_CTRL_PESO;
	@FieldAnnotation(orderIndex=5)
	public DecimalField txtQI_CTRL_TALLA;
	
	public ButtonComponent btnAceptar;
	public ButtonComponent btnCancelar;
	
	LinearLayout q0,q1,q2;
	LinearLayout ly220;
	
	CISECCION_10_04 entidad=null;
	CARATULA_INDIVIDUAL visita;
	CISECCION_02 individualmef;
	public CheckBoxField chbP105_m;
	public TextField txtCabecera;
	public static Integer inEdad;
	private LabelComponent lblBlanco,lblBlanco1,lblBlanco2,lbl466E_1,lbl466E_2,lbl466E_3,lbl466E_4,lbl466E_5;
	CISECCION_10_04 bean;
	private static CISECCION_10_01Fragment_013 caller;
	private static CISECCION_10_01Fragment_011_2Dialog dialogo2;
	
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesNinio;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	public GridComponent2 grid466E_1,grid466E_2,grid466E_3;
	public static boolean editable=false;
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco;
	public LabelComponent lblpregunta01,lblpregunta03;
	public IntegerField txtQI220C1,txtQI220C2,txtQI220C3;
	public String fechareferencia;
	public static String nombres="";

	public enum ACTION_NACIMIENTO {
		AGREGAR, EDITAR
	}
	public ACTION_NACIMIENTO accion;

	public static CISECCION_10_01Fragment_011_3Dialog newInstance(FragmentForm pagina, CISECCION_10_01Fragment_011_2Dialog Dialogo2, CISECCION_10_04 detalle, Integer edad, boolean editar, String nombre) {
		caller = (CISECCION_10_01Fragment_013) pagina;
		dialogo2 = (CISECCION_10_01Fragment_011_2Dialog) Dialogo2;
		editable=editar;
		inEdad= edad;
		nombres=nombre;
		CISECCION_10_01Fragment_011_3Dialog f = new CISECCION_10_01Fragment_011_3Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_10_04) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		//Integer control= bean.qi_ctrl;
		if(bean.qi_ctrl_desc.equals("0")){
			bean.qi_ctrl_desc=bean.qi_ctrl_desc+bean.qi_ctrl;
		}
//		getDialog().setTitle("Control:  " + bean.qi_ctrl_desc);
		getDialog().setTitle("Control:  " + bean.qi_ctrl_desc+" Nombre: "+nombres);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		enlazarCajas();
		listening();
		return rootView;
	}

	public CISECCION_10_01Fragment_011_3Dialog() {
		super();
		seccionesGrabado  = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI_CTRL_DIA","QI_CTRL_DESC","QI_CTRL_MES","QI_CTRL_ANIO","QI_CTRL_PESO","QI_CTRL_TALLA")};
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI_CTRL_DIA","QI_CTRL_MES","QI_CTRL_ANIO","QI_CTRL_PESO","QI_CTRL_TALLA","ID","HOGAR_ID","PERSONA_ID","NINIO_ID","QI_CTRL","QI_CTRL_DESC") };
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0, -1,-1, "QIVDIA","QIVMES","QIVANIO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesNinio = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
	}

	@Override
	protected View createUI() {
		buildFields();
		
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		q0 = createQuestionSection(lblBlanco,grid466E_1.component(),lblBlanco1,grid466E_2.component(),lblBlanco2,grid466E_3.component(),chbP105_m);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);

		form.addView(botones);
		return contenedor;
	}

	@Override
	protected void buildFields() {
		
		lblBlanco = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_10_01qi466E_b);
		lblBlanco1 = new LabelComponent(this.getActivity()).size(20, 80).textSize(19).text(R.string.ciseccion_10_01qi466E_b);
		lblBlanco2 = new LabelComponent(this.getActivity()).size(20, 80).textSize(19).text(R.string.ciseccion_10_01qi466E_b);

		lbl466E_1 = new LabelComponent(getActivity()).textSize(15).size(50, 80).text(R.string.ciseccion_10_01qi466E_fech).alinearIzquierda();
		lbl466E_2 = new LabelComponent(getActivity()).textSize(15).size(50, 80).text(R.string.ciseccion_10_01qi466E_peso).alinearIzquierda();
		lbl466E_3 = new LabelComponent(getActivity()).textSize(15).size(50, 80).text(R.string.ciseccion_10_01qi466E_talla).alinearIzquierda();
		lbl466E_4 = new LabelComponent(getActivity()).textSize(15).size(50, 50).text(R.string.ciseccion_10_01qi466E_kg).alinearIzquierda();
		lbl466E_5 = new LabelComponent(getActivity()).textSize(15).size(50, 50).text(R.string.ciseccion_10_01qi466E_cm).alinearIzquierda();
		
		txtQI_CTRL_DIA  =new IntegerField(this.getActivity()).size(50, 55).maxLength(2);
		txtQI_CTRL_DIA.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44 || inDia==66 || inDia==77 ){
			    		Util.cleanAndLockView(getActivity(),txtQI_CTRL_MES,txtQI_CTRL_ANIO);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQI_CTRL_MES,txtQI_CTRL_ANIO);
				}
			}
		});
		txtQI_CTRL_MES  =new IntegerField(this.getActivity()).size(50, 55).maxLength(2);
		txtQI_CTRL_ANIO =new IntegerField(this.getActivity()).size(50, 85).maxLength(4);
		
		txtQI_CTRL_PESO  =new DecimalField(this.getActivity()).size(50, 80).maxLength(2).completarCaracteres(2, ".").decimales(2);
		txtQI_CTRL_TALLA =new DecimalField(this.getActivity()).size(50, 80).maxLength(4).decimales(1);
		
		chbP105_m=new CheckBoxField(this.getActivity(), R.string.ciseccion_10_01qi466E_sc, "1:0").size(60, 200);
		chbP105_m.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI_CTRL_DIA,txtQI_CTRL_MES, txtQI_CTRL_ANIO,txtQI_CTRL_PESO,txtQI_CTRL_TALLA);
		  		}else {
		  			MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI_CTRL_DIA,txtQI_CTRL_MES, txtQI_CTRL_ANIO,txtQI_CTRL_PESO,txtQI_CTRL_TALLA);
					}
				}
		});
		
		grid466E_1=new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		grid466E_1.addComponent(lbl466E_1);
		grid466E_1.addComponent(txtQI_CTRL_DIA);
		grid466E_1.addComponent(txtQI_CTRL_MES);
		grid466E_1.addComponent(txtQI_CTRL_ANIO);
		
		grid466E_2=new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		grid466E_2.addComponent(lbl466E_2);
		grid466E_2.addComponent(txtQI_CTRL_PESO);
		grid466E_2.addComponent(lbl466E_4);
		
		grid466E_3=new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		grid466E_3.addComponent(lbl466E_3);
		grid466E_3.addComponent(txtQI_CTRL_TALLA);
		grid466E_3.addComponent(lbl466E_5);

		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);

		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CISECCION_10_01Fragment_011_3Dialog.this.dismiss();
				Util.lockView(getParentActivity(), false,dialogo2.btnAgregar,dialogo2.btnAgregarCero);
				dialogo2.cargarDatos();
			}
		});

		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				CISECCION_10_01Fragment_011_3Dialog.this.dismiss();
				dialogo2.cargarDatos();
				Integer i=0;
				Integer j=0;
				if(chbP105_m.isChecked()){
					if(dialogo2.detalles.size()>0){
						for (CISECCION_10_04 item : dialogo2.detalles) {
							if(item.qi_ctrl_desc.substring(0,1).equals("0") && item.qi_ctrl_dia==null){
								i=1;
							}
							if(item.qi_ctrl_desc.equals("1") && item.qi_ctrl_dia==null){
								j=1;
							}
						}
					}
				
					if(bean.qi_ctrl==1 || i==1){
						Util.cleanAndLockView(getParentActivity(), dialogo2.btnAgregarCero);
					}
					if(bean.qi_ctrl_desc.equals("1") || j==1){
						Util.cleanAndLockView(getParentActivity(), dialogo2.btnAgregar);
					}
				}
				else{
					//Util.lockView(getParentActivity(), false,dialogo2.btnAgregar,dialogo2.btnAgregarCero);
					if(bean.qi_ctrl_desc.substring(0, 1).equals("0") && (bean.qi_ctrl+1)<=4 && editable==false){
	//					Log.e("","11111 ");
						dialogo2.agregarControlCero(bean.qi_ctrl+1, inEdad);
					}
					if ((Integer.parseInt(bean.qi_ctrl_desc)+1) <13 && (bean.qi_ctrl+1)<17 && !bean.qi_ctrl_desc.substring(0, 1).equals("0") && editable==false) {
						Log.e("","33333 ");
						dialogo2.agregarPersona(bean.qi_ctrl+1, inEdad);
					}
				}
				
			}
		});
	}
	
	public boolean grabar(){
		uiToEntity(entidad);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		
		if (chbP105_m.isChecked()) {
			entidad.qi_ctrl_dia=null;
			entidad.qi_ctrl_mes=null;
			entidad.qi_ctrl_anio=null;
			entidad.qi_ctrl_peso=null;
			entidad.qi_ctrl_talla=null;
			
		}
		boolean flag = true;
		try {
			if(!getCuestionarioService().saveOrUpdate(entidad, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		return flag;
	}

	public boolean grabadoParcial(){
		uiToEntity(entidad);
		if (!validarparcial()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			if(!getCuestionarioService().saveOrUpdate(entidad, seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		int year = Calendar.getInstance().get(Calendar.YEAR);
		if (!chbP105_m.isChecked()) {
			Log.e("",entidad.qi_ctrl_dia+"-"+entidad.qi_ctrl_mes+"-"+entidad.qi_ctrl_anio);
			if (Util.esVacio(entidad.qi_ctrl_dia)) {
				mensaje = preguntaVacia.replace("$", "La pregunta txtQI_CTRL_DIA");
				view = txtQI_CTRL_DIA;
				error = true;
				return false;
			}
			
			if (!txtQI_CTRL_DIA.getText().toString().equals("44") && !txtQI_CTRL_DIA.getText().toString().equals("98") && !txtQI_CTRL_DIA.getText().toString().equals("66") ) {
				
				if(txtQI_CTRL_DIA.getText().toString().equals("") || txtQI_CTRL_MES.getText().toString().equals("") || txtQI_CTRL_ANIO.getText().toString().equals("")){
					mensaje = "Falta informaci�n en el mes y/o a�o";
					//view = txtQI_CTRL_DIA;
					error = true;
					return false;
				}
			}
			
			if(txtQI_CTRL_MES.getText()!=null && !txtQI_CTRL_MES.getText().toString().equals("") && !txtQI_CTRL_MES.getText().toString().equals("98")){
		    	if ((Util.esMenor(Integer.parseInt(txtQI_CTRL_MES.getText().toString()),1) ||  Util.esMayor(Integer.parseInt(txtQI_CTRL_MES.getText().toString()),12))) {
		    		mensaje ="Mes fuera de rango";
					view = txtQI_CTRL_MES;
					error=true;
					return false;
		    	}
	    	}
			
			if (!Util.esVacio(entidad.qi_ctrl_anio) && (entidad.qi_ctrl_anio>year || entidad.qi_ctrl_anio<year-15)) {
				mensaje = "A�o fuera de rango";
				view = txtQI_CTRL_ANIO;
				error = true;
				return false;
			}
			
			
			if(txtQI_CTRL_DIA.getText()!=null && txtQI_CTRL_MES.getText()!=null && txtQI_CTRL_ANIO.getText()!=null &&
		  			  !txtQI_CTRL_DIA.getText().toString().equals("") && !txtQI_CTRL_MES.getText().toString().equals("") && !txtQI_CTRL_ANIO.getText().toString().equals("") ){
		    		
					Integer numOrdenMax= getCuestionarioService().getnumOrdenMaxVisitaCI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
					visita = getCuestionarioService().getVisitaI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,numOrdenMax,seccionesCargado);
					individualmef = getCuestionarioService().getSeccion01_03Nacimiento(entidad.id,entidad.hogar_id,entidad.persona_id,entidad.ninio_id,seccionesNinio); 
					if (visita!=null && individualmef!=null) {
						Integer diaMin  = Integer.parseInt(individualmef.qi215d);
						Integer mesMin  = Integer.parseInt(individualmef.qi215m);
						Integer anioMin = individualmef.qi215y;
						Integer edad = individualmef.qi217;
						if (mesMin==98 || anioMin==9998) {
							diaMin  = Integer.parseInt(visita.qivdia);
							mesMin  = Integer.parseInt(visita.qivmes);
							anioMin = visita.qivanio-edad;
						}
						
						Integer diaMax  = Integer.parseInt(visita.qivdia);
						Integer mesMax  = Integer.parseInt(visita.qivmes);
						Integer anioMax = visita.qivanio;
						
						Integer dia=Integer.parseInt(txtQI_CTRL_DIA.getText().toString());
			    		Integer mes=Integer.parseInt(txtQI_CTRL_MES.getText().toString()); 
			    		Integer anio=Integer.parseInt(txtQI_CTRL_ANIO.getText().toString());
						
						Calendar fechamin = new GregorianCalendar(anioMin , mesMin-1, diaMin-1);
						Calendar fechavacuna = new GregorianCalendar(anio, mes-1, dia);
						Calendar fechamax = new GregorianCalendar(anioMax,mesMax -1, diaMax+1 );
						
					    if( !txtQI_CTRL_DIA.getText().equals("98") && !(fechamin.compareTo(fechavacuna)==-1 && fechamax.compareTo(fechavacuna)==1)){ 
				    			mensaje ="Verifique Fecha";
				    			MyUtil.MensajeGeneral(getActivity(), mensaje);
//				    			view = txtQI_CTRL_DIA;
//				    			error=true;
//				    			return false;
				    			
				    			
					    }
					}
				}
			if(entidad.qi_ctrl_dia!=null && entidad.qi_ctrl_mes!=null && entidad.qi_ctrl_anio!=null){
				Integer dia= Integer.parseInt(entidad.qi_ctrl_dia.toString());
				Integer mes = Integer.parseInt(entidad.qi_ctrl_mes.toString());
				CISECCION_10_04 control = getCuestionarioService().getCICONTROL_BYFECHA(entidad.id, entidad.hogar_id, entidad.persona_id,entidad.ninio_id,dia, mes,  entidad.qi_ctrl_anio, seccionesPersonas);
				if(control!=null && control.qi_ctrl!=null && control.qi_ctrl!=entidad.qi_ctrl){
					mensaje ="Existe control con la misma fecha";
					ValidarMensaje(mensaje);
	    		}
				CISECCION_10_04 controlmes= getCuestionarioService().getCICONTROL_BYFECHASOLOHASTAMES(entidad.id, entidad.hogar_id, entidad.persona_id,entidad.ninio_id, mes,  entidad.qi_ctrl_anio, seccionesPersonas);
				if(controlmes!=null  && controlmes.qi_ctrl!=null &&  controlmes.qi_ctrl!=entidad.qi_ctrl){
					mensaje ="Existe control con el mismo mes";
					ValidarMensaje(mensaje);
	    		}
			}
			/*if (Util.esVacio(entidad.qi_ctrl_mes)) {
				mensaje = preguntaVacia.replace("$", "La pregunta txtQI_CTRL_MES");
				view = txtQI_CTRL_MES;
				error = true;
				return false;
			}
			if (Util.esVacio(entidad.qi_ctrl_anio)) {
				mensaje = preguntaVacia.replace("$", "La pregunta txtQI_CTRL_ANIO");
				view = txtQI_CTRL_ANIO;
				error = true;
				return false;
			}
			if (Util.esVacio(entidad.qi_ctrl_peso)) {
				mensaje = preguntaVacia.replace("$", "La pregunta txtQI_CTRL_PESO");
				view = txtQI_CTRL_PESO;
				error = true;
				return false;
			}
			if (Util.esVacio(entidad.qi_ctrl_talla)) {
				mensaje = preguntaVacia.replace("$", "La pregunta txtQI_CTRL_TALLA");
				view = txtQI_CTRL_TALLA;
				error = true;
				return false;
			}
			if(entidad.qi_ctrl_dia!=null){
		    	if(Util.esMayor(Integer.parseInt(entidad.qi_ctrl_dia), 31)){
		    		mensaje ="dia fuera de rango";
					view = txtQI_CTRL_DIA;
					error=true;
					return false;
		    	}
	    	}
			if(entidad.qi_ctrl_mes!=null){
		    	if ((Util.esMenor(Integer.parseInt(entidad.qi_ctrl_mes),1) ||  Util.esMayor(Integer.parseInt(entidad.qi_ctrl_mes),12))) {
		    		mensaje ="Mes fuera de rango";
					view = txtQI_CTRL_MES;
					error=true;
					return false;
		    	}
	    	}
	    	if(entidad.qi_ctrl_dia!=null && entidad.qi_ctrl_mes!=null){
		    	if(Util.esMenor(Integer.parseInt(entidad.qi_ctrl_dia), 32) && Util.esMenor(Integer.parseInt(entidad.qi_ctrl_mes), 13)){
		    		Integer dia=Integer.parseInt(entidad.qi_ctrl_dia.toString());
		    		Integer mes=Integer.parseInt(entidad.qi_ctrl_mes.toString());    		
		    		
		    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
		    			mensaje ="Dia no corresponde al mes que eligi�";
		    			view = txtQI_CTRL_DIA;
		    			error=true;
		    			return false;
		    		}
		    	}
	    	}*/
		}
		
		
	    return true;
	}
	
	public boolean validarparcial(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		return true;
	}
	public void ValidarMensaje(String mensaje){
		ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	
	private void cargarDatos() {
		entidad =  getCuestionarioService().getCISECCION_10_04(bean.id,bean.hogar_id,bean.persona_id,bean.ninio_id,bean.qi_ctrl,seccionesPersonas);
		if(entidad==null){
			  entidad=new CISECCION_10_04();
			  entidad.id=bean.id;
			  entidad.hogar_id=bean.hogar_id;
			  entidad.persona_id=bean.persona_id;
			  entidad.ninio_id=bean.ninio_id;
			  entidad.qi_ctrl=bean.qi_ctrl;
			  entidad.qi_ctrl_desc=bean.qi_ctrl_desc;
		}else {
			entityToUI(entidad);
			txtQI_CTRL_PESO.setText(entidad.getPeso());
			txtQI_CTRL_TALLA.setText(entidad.getTalla());
		
		}
		if(entidad!=null && entidad.qi_ctrl_dia==null && entidad.qi_ctrl!=null && editable==true){
			chbP105_m.setChecked(true);
		}
		inicio();
	}
	
	private void inicio() {
		if (bean.qi_ctrl==1 || bean.qi_ctrl_desc.equals("1")) {
			Util.lockView(getActivity(), false,chbP105_m);
  		}else {
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),chbP105_m);
		}
		ValidarsiesSupervisora();
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	   public void ValidarsiesSupervisora(){
	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		txtQI_CTRL_DIA.readOnly();
	    		txtQI_CTRL_MES.readOnly();
	    		txtQI_CTRL_ANIO.readOnly();
	    		txtQI_CTRL_PESO.readOnly();
	    		txtQI_CTRL_TALLA.readOnly();
	    		chbP105_m.readOnly();
	    		btnAceptar.setEnabled(false);
	    	}
	    }
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_10_01Fragment_011_3Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
}