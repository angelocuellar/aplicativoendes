package gob.inei.endes2024.fragment.CIseccion_10_01; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CISECCION_10_02Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI1040_A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI1040_B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI1040_C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI1040_D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI1040_E; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI1040_F; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI1040_G; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI1040_H; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI1040_I; 
	/*@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI1041_1; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI1041_2; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI1042_1; 
	@FieldAnnotation(orderIndex=13) 
	public IntegerField txtQI1043; 
	@FieldAnnotation(orderIndex=14) 
	public IntegerField txtQI1043_1; */
	CISECCION_10_02 c2seccion_10_02; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
// 
	public CISECCION_10_02Fragment_003() {} 
	public CISECCION_10_02Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_10_02).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		rgQI1040_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_a_1,R.string.c2seccion_10_02qi1040_a_2,R.string.c2seccion_10_02qi1040_a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_b_1,R.string.c2seccion_10_02qi1040_b_2,R.string.c2seccion_10_02qi1040_b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_c_1,R.string.c2seccion_10_02qi1040_c_2,R.string.c2seccion_10_02qi1040_c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_d_1,R.string.c2seccion_10_02qi1040_d_2,R.string.c2seccion_10_02qi1040_d_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_e_1,R.string.c2seccion_10_02qi1040_e_2,R.string.c2seccion_10_02qi1040_e_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_f_1,R.string.c2seccion_10_02qi1040_f_2,R.string.c2seccion_10_02qi1040_f_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_g_1,R.string.c2seccion_10_02qi1040_g_2,R.string.c2seccion_10_02qi1040_g_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_H=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_h_1,R.string.c2seccion_10_02qi1040_h_2,R.string.c2seccion_10_02qi1040_h_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1040_I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1040_i_1,R.string.c2seccion_10_02qi1040_i_2,R.string.c2seccion_10_02qi1040_i_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		/*rgQI1041_1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1041_1_1,R.string.c2seccion_10_02qi1041_1_2,R.string.c2seccion_10_02qi1041_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1041_2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1041_2_1,R.string.c2seccion_10_02qi1041_2_2,R.string.c2seccion_10_02qi1041_2_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI1042_1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_10_02qi1042_1_1,R.string.c2seccion_10_02qi1042_1_2,R.string.c2seccion_10_02qi1042_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI1043=new IntegerField(this.getActivity()).size(altoComponente, 40).maxLength(2); 
		txtQI1043_1=new IntegerField(this.getActivity()).size(altoComponente, 40).maxLength(2);*/ 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(lblTitulo); 
		q1 = createQuestionSection(R.string.c2seccion_10_02qi1040_a,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_A); 
		q2 = createQuestionSection(R.string.c2seccion_10_02qi1040_b,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_B); 
		q3 = createQuestionSection(R.string.c2seccion_10_02qi1040_c,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_C); 
		q4 = createQuestionSection(R.string.c2seccion_10_02qi1040_d,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_D); 
		q5 = createQuestionSection(R.string.c2seccion_10_02qi1040_e,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_E); 
		q6 = createQuestionSection(R.string.c2seccion_10_02qi1040_f,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_F); 
		q7 = createQuestionSection(R.string.c2seccion_10_02qi1040_g,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_G); 
		q8 = createQuestionSection(R.string.c2seccion_10_02qi1040_h,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_H); 
		q9 = createQuestionSection(R.string.c2seccion_10_02qi1040_i,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1040_I); 
		/*q10 = createQuestionSection(R.string.c2seccion_10_02qi1041_1,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1041_1); 
		q11 = createQuestionSection(R.string.c2seccion_10_02qi1041_2,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1041_2); 
		q12 = createQuestionSection(R.string.c2seccion_10_02qi1042_1,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI1042_1); 
		q13 = createQuestionSection(R.string.c2seccion_10_02qi1043,txtQI1043); 
		q14 = createQuestionSection(R.string.c2seccion_10_02qi1043_1,txtQI1043_1); */
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
		form.addView(q9); 
		form.addView(q10); 
		form.addView(q11); 
		form.addView(q12); 
		form.addView(q13); 
		form.addView(q14); 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_10_02); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_10_02,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(c2seccion_10_02.qi1040_a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_A"); 
			view = rgQI1040_A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_B"); 
			view = rgQI1040_B; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_C"); 
			view = rgQI1040_C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_D"); 
			view = rgQI1040_D; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_E"); 
			view = rgQI1040_E; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_f)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_F"); 
			view = rgQI1040_F; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_g)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_G"); 
			view = rgQI1040_G; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_h)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_H"); 
			view = rgQI1040_H; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1040_i)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1040_I"); 
			view = rgQI1040_I; 
			error = true; 
			return false; 
		} 
		/*if (Util.esVacio(c2seccion_10_02.qi1041_1)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1041_1"); 
			view = rgQI1041_1; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1041_2)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1041_2"); 
			view = rgQI1041_2; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1042_1)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1042_1"); 
			view = rgQI1042_1; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1043)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1043"); 
			view = txtQI1043; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_10_02.qi1043_1)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI1043_1"); 
			view = txtQI1043_1; 
			error = true; 
			return false; 
		} */
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_10_02 = getCuestionarioService().getCISECCION_10_02(App.getInstance().getPersona().id, App.getInstance().getPersona().hogar_id, App.getInstance().getPersona().persona_id,1,seccionesCargado); 
		if(c2seccion_10_02==null){ 
		  c2seccion_10_02=new CISECCION_10_02(); 
		  c2seccion_10_02.id=App.getInstance().getPersona().id; 
		  c2seccion_10_02.hogar_id=App.getInstance().getPersona().hogar_id; 
		  c2seccion_10_02.persona_id=App.getInstance().getPersona().persona_id; 
	    } 
		entityToUI(c2seccion_10_02); 
		inicio(); 
    } 
    private void inicio() { 
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	} 
} 
