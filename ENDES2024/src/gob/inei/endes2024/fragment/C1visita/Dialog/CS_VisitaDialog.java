package gob.inei.endes2024.fragment.C1visita.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.C1visita.CS_VISITAFragment_001;
import gob.inei.endes2024.fragment.visita.Dialog.VisitaDialog;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.mapswithme.maps.api.MapsWithMeApi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_VisitaDialog extends DialogFragmentComponent {
	@FieldAnnotation(orderIndex = 1)
	public DateTimeField txtFECHA_INICIO;
	@FieldAnnotation(orderIndex = 2)
	public DateTimeField txtHORA_INICIO;
	@FieldAnnotation(orderIndex = 3)
	public DateTimeField txtHORA_FIN;
	@FieldAnnotation(orderIndex = 4)
	public SpinnerField spnQSVRESUL;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtQSVRESUL_O;
	@FieldAnnotation(orderIndex = 6)
	public DateTimeField txtFECHA_PROXIMA;
	@FieldAnnotation(orderIndex = 7)
	public DateTimeField txtHORA_PROXIMA;
	
	@FieldAnnotation(orderIndex = 8)	
	public TextField txtQSGPS_LAT;
	@FieldAnnotation(orderIndex = 9)
	public TextField txtQSGPS_LONG;
	@FieldAnnotation(orderIndex = 10)
	public TextField txtQSGPS_ALT;
	

	CSVISITA bean;
	
	
	public List<Integer> resul_1;
	public List<Integer> resul_fila;
	private CSVISITA ultimaVisita;

	public ButtonComponent btnAceptar,btnCancelar,btnGPS,btnVerenMapa;
	private CuestionarioService service;
		
	private LinearLayout q1,q2,q3,q4;
	private LabelComponent lblResultado, lblFechaFinal, lblResultadoFinal,lblFecha,lblHoraIni,lbltitulo_proxima,lblBlanco3, lblCordenadas1,lblCordenadas2,lblCordenadas3,lblblanco,lblblanco1,lblHoraFin,lblResutado,lblfechaproxima,lblhoraproxima;

	private static FragmentForm caller;
	
	private GridComponent grid1,grid2, grproxvisita;
	private GridComponent2 tblGps;
	
	private List<CSVISITA> visitas;
	private boolean esResultado3 = false;
	SeccionCapitulo[] seccionesGrabado,seccionesCargado;

	public static enum ACTION {
		INICIAR, EDITAR, FINALIZAR
	};

	private ACTION action;
	private VisitaService visitaService;

	public CS_VisitaDialog() {
	}
	
	public interface C1_Cap00Fragment_001_01Listener {
		void onFinishEditDialog(String inputText);
	}

	public static CS_VisitaDialog newInstance(FragmentForm pagina, CSVISITA bean) {
		return newInstance(pagina, bean, ACTION.INICIAR);
	}

	public static CS_VisitaDialog newInstance(FragmentForm pagina, CSVISITA bean,ACTION action) {
		caller = pagina;
		CS_VisitaDialog f = new CS_VisitaDialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("bean", bean);
		args.putSerializable("action", action);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CSVISITA) getArguments().getSerializable("bean");
		action = (ACTION) getArguments().getSerializable("action");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas(this);
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINTUO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","QSRESULT","QSRESULT_O","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };

         seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINTUO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","QSRESULT","QSRESULT_O","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		return rootView;
	}



	@Override
	protected void buildFields() {
		txtFECHA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 200).readOnly();
		txtFECHA_INICIO.setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
		txtHORA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 200).readOnly();
		txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 200).readOnly();
		txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 200);
		lblResultado = new LabelComponent(this.getActivity(),R.style.styleHeaderAzulpurpura).size(altoComponente, 170).text(R.string.v_resultadoinf).textSize(21).centrar().negrita();
		lblFechaFinal = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.v_fecha).textSize(21).centrar().negrita();
		lblResultadoFinal = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.v_resultadoinf).textSize(15).centrar().negrita();
		txtQSVRESUL_O = new TextField(getActivity()).size(altoComponente, 400).maxLength(100).hint(R.string.especifique).soloTexto();
		txtFECHA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 180).setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
		txtHORA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100);
		spnQSVRESUL = new SpinnerField(getActivity()).size(altoComponente + 15,350).callback("onC1_RVISITAChangeValue");
		cargarSpinner();

		
		lblFecha = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_fecha_ini);
		lblHoraIni = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_ini);
		lblHoraFin = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_fin);
		lblResutado = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_resultado);
		lbltitulo_proxima = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.v_l_proxima_visita).textSize(22).centrar().negrita().colorFondo(R.color.griscabece);
		lblfechaproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_fecha_prox);
		lblhoraproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_hora_prox);
		
		grid1 = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
		grid1.addComponent(lblFecha);
		grid1.addComponent(txtFECHA_INICIO);
		grid1.addComponent(lblHoraIni);
		grid1.addComponent(txtHORA_INICIO);
		
		grid2 = new GridComponent(getActivity(), Gravity.CENTER, 2, 0);
		grid2.addComponent(lblHoraFin);
		grid2.addComponent(txtHORA_FIN);
		grid2.addComponent(lblResutado);
		grid2.addComponent(spnQSVRESUL);
		grid2.addComponent(txtQSVRESUL_O, 2);

		grproxvisita = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
		grproxvisita.addComponent(lblfechaproxima);
		grproxvisita.addComponent(txtFECHA_PROXIMA);
		grproxvisita.addComponent(lblhoraproxima);
		grproxvisita.addComponent(txtHORA_PROXIMA);
		
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				CS_VisitaDialog.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				 App.getInstance().setC1Visita(bean);
				((CS_VISITAFragment_001) caller).recargarLista();
				 CS_VisitaDialog.this.dismiss();
			}
		});

	}

	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(R.string.pregunta01, grid1.component());
		q2 = createQuestionSection(grid2.component());
		q3 = createQuestionSection(R.string.v_l_proxima_visita,grproxvisita.component());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(botones);
		return contenedor;
	}
	

	private boolean grabar() {
		uiToEntity(bean);
		if (action == ACTION.INICIAR) {
			bean.qsvanio = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
			bean.qsvdia = Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "dd");
			bean.qsvmes = Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "MM");
			bean.qsvhora_ini = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "HH");
			bean.qsvminuto_ini = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "mm");
			
		} else if (action == ACTION.FINALIZAR) {
			if (spnQSVRESUL.getSelectedItemKey() == null || spnQSVRESUL.getSelectedItemKey().toString().equals("0")) {
				spnQSVRESUL.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Seleccione un resultado.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				return false;
			}
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("7")) {
				if (Util.esVacio(txtQSVRESUL_O)) {
					txtQSVRESUL_O.requestFocus();
					ToastMessage.msgBox(this.getActivity(),"Otro no puede estar vacia.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return false;
				} 
				bean.qsvresul_o=txtQSVRESUL_O.getText().toString();
			 }
			
			
			bean.qsvresul = spnQSVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQSVRESUL.getSelectedItemKey();
			bean.qsvhora_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
			bean.qsvminuto_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");
			
			bean.qsvhora_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qsvhora_fin);
			bean.qsvminuto_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qsvminuto_fin);
	
			
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("2")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")) {
				if (!validarProximaVisita(Integer.parseInt(spnQSVRESUL.getSelectedItemKey().toString()))) {
					return false;
				}
				if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
					bean.qsprox_hora =Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
					bean.qsprox_minuto = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
					bean.qsprox_dia = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
					bean.qsprox_mes = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
					bean.qsprox_anio = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "yyyy");
				}
			}else {
//				bean.qhvmesp = null;
			}
			
		} else if (action == ACTION.EDITAR) {
			if (spnQSVRESUL.getSelectedItemKey() == null || spnQSVRESUL.getSelectedItemKey().toString().equals("0")) {
				spnQSVRESUL.requestFocus();
				ToastMessage.msgBox(this.getActivity(), "Seleccione un resultado.", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return false;
			}
			
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("7")) {
				if (Util.esVacio(txtQSVRESUL_O)) {
					txtQSVRESUL_O.requestFocus();
					ToastMessage.msgBox(this.getActivity(),"Otro no puede estar vacia.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					return false;
				}
				bean.qsvresul_o=txtQSVRESUL_O.getText().toString();
			}
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("2")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")) {
				
				if (!validarProximaVisita(Integer.parseInt(spnQSVRESUL.getSelectedItemKey().toString()))) {
					return false;
				}
				if (txtFECHA_PROXIMA.getValue() != null&& txtHORA_PROXIMA.getValue() != null) {
					bean.qsprox_hora = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
					bean.qsprox_minuto = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
					bean.qsprox_dia = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
					bean.qsprox_mes = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
					bean.qsprox_anio =Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "yyyy");
				}
			} else {
			}
			bean.qsvresul = spnQSVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQSVRESUL.getSelectedItemKey();
			bean.qsvhora_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
			bean.qsvminuto_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");

		}
		try {
			if (!getVisitaService().saveOrUpdate(bean,"QSVDIA", "QSVMES", "QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINUTO_FIN","NRO_VISITA","QSVRESUL","QSVRESUL_O","QSPROX_DIA","QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID","PERSONA_ID")) {
				ToastMessage.msgBox(this.getActivity(),"Los datos no fueron grabados",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		return true;
	}

	private boolean validarProximaVisita(Integer item) {

		if (txtFECHA_PROXIMA.getValue() == null || txtHORA_PROXIMA.getValue() == null && Util.esDiferente(item, 4)) {		
			return true;
		}
		if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) > 0) {
			txtFECHA_PROXIMA.requestFocus();
			ToastMessage.msgBox(this.getActivity(),"Fecha Proxima no puede ser menor.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} else if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) == 0) {
			if (Util.compareTime((Date) txtHORA_FIN.getValue(),(Date) txtHORA_PROXIMA.getValue()) >= 0) {
				txtHORA_PROXIMA.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Hora Proxima no puede ser menor o igual.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return false;
			}
		}
		return true;
	}

	private void cargarDatos() {
		getDialog().setTitle("Cuestionario de Salud - Visita N� " + bean.nro_visita);
		entityToUI(bean);

		caretaker.addMemento("antes", bean.saveToMemento(CSVISITA.class));
		
			if (action == ACTION.INICIAR) {
				txtFECHA_INICIO.setValue(Calendar.getInstance().getTime());
				txtHORA_INICIO.setValue(Calendar.getInstance().getTime());
			} else if (action == ACTION.FINALIZAR) {
				if (bean.qsvminuto_ini != null && bean.qsvhora_ini!=null) {
					Date fecha = Util.getFechaHora(bean.qsvanio+"", bean.qsvmes,bean.qsvdia, bean.qsvhora_ini , bean.qsvminuto_ini, 0+"");
					txtFECHA_INICIO.setValue(fecha);
					txtHORA_INICIO.setValue(fecha);
					txtHORA_FIN.setValue(Calendar.getInstance().getTime());
				}
				if(spnQSVRESUL.getSelectedItemKey()!=null){
					if (spnQSVRESUL.getSelectedItemKey().toString().equals("2") || spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					 || spnQSVRESUL.getSelectedItemKey().toString().equals("4") || spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					  || spnQSVRESUL.getSelectedItemKey().toString().equals("6")
					  ) {
						if (bean.qsprox_dia != null && bean.qsprox_hora != null) {
							int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
							Date fechaProx = Util.getFechaHora(anio+"", bean.qsprox_mes,bean.qsprox_dia, bean.qsprox_hora,bean.qsprox_minuto, 0+"");
							txtFECHA_PROXIMA.setValue(fechaProx);
							txtHORA_PROXIMA.setValue(fechaProx);
						}
					}
				
				}
				
			} else if (action == ACTION.EDITAR) {
			
				if(spnQSVRESUL.getSelectedItemKey()!=null){
					if (spnQSVRESUL.getSelectedItemKey().toString().equals("2") || spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					 || spnQSVRESUL.getSelectedItemKey().toString().equals("4") || spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					  || spnQSVRESUL.getSelectedItemKey().toString().equals("6")) {
						if (bean.qsprox_dia != null && bean.qsprox_hora != null) {
							int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
							Date fechaProx = Util.getFechaHora(anio+"", bean.qsprox_mes,bean.qsprox_dia, bean.qsprox_hora,bean.qsprox_minuto, 0+"");
							txtFECHA_PROXIMA.setValue(fechaProx);
							txtHORA_PROXIMA.setValue(fechaProx);
						}
					}
					
				}
				
				if (bean.qsvminuto_ini != null && bean.qsvhora_ini!=null && bean.qsvhora_fin != null && bean.qsvminuto_fin!=null) {
					Date fechaIni = Util.getFechaHora(bean.qsvanio+"", bean.qsvmes,bean.qsvdia, bean.qsvhora_ini , bean.qsvminuto_ini, 0+"");
					Date fechaFin = Util.getFechaHora(bean.qsvanio+"", bean.qsvmes,bean.qsvdia, bean.qsvhora_fin , bean.qsvminuto_fin, 0+"");
					txtFECHA_INICIO.setValue(fechaIni);
					txtHORA_INICIO.setValue(fechaIni);
					txtHORA_FIN.setValue(fechaFin);
				}
				
			 
			}
			inicio();
		
	}

	private void inicio() {
		if (action == ACTION.INICIAR) {
			btnAceptar.requestFocus();
			q3.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		} else if (action == ACTION.FINALIZAR) {
			q3.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			spnQSVRESUL.requestFocus();
		}
		onC1_RVISITAChangeValue(spnQSVRESUL);
	}

	public void onC1_RVISITAChangeValue(FieldComponent component) {
		String resultadoStr = (String) component.getValue();

		if (resultadoStr.substring(0, 1).equals("9")) {
			Util.lockView(getActivity(), false, txtQSVRESUL_O);
			txtQSVRESUL_O.requestFocus();
		} else {
			Util.cleanAndLockView(getActivity(), txtQSVRESUL_O);
		}
       
		if (resultadoStr.substring(0, 1).equals("2")|| resultadoStr.substring(0, 1).equals("3")	|| resultadoStr.substring(0, 1).equals("4") || resultadoStr.substring(0, 1).equals("5")) {
			Util.lockView(getActivity(), false, txtFECHA_PROXIMA,txtHORA_PROXIMA);
			txtFECHA_PROXIMA.requestFocus();
		} else {
			Util.cleanAndLockView(getActivity(), txtFECHA_PROXIMA,txtHORA_PROXIMA);
		}
	}


	private void cargarSpinner() {
		if (esResultado3) {
			List<Object> keys = new ArrayList<Object>();
			keys.add(null);
			keys.add(1);
			keys.add(5);
			String[] items1 = new String[] { " --- Seleccione --- ","1. COMPLETO", "5. INCOMPLETO" };
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
			spnQSVRESUL.setAdapterWithKey(adapter, keys);
		} else {
			List<Object> keys = new ArrayList<Object>();
			keys.add(null);
			keys.add(2);
			keys.add(3);
			keys.add(4);
			keys.add(5);
			keys.add(6);
			keys.add(7);
			String[] items1 = new String[] { " --- Seleccione --- ",
					"2. AUSENTE",
					"3. APLAZADA", 
					"4. RECHAZADA", 
					"5. INCOMPLETA",
					"6. DISCAPACITADA(O)",
					"9. OTRO"
			};
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
			spnQSVRESUL.setAdapterWithKey(adapter, keys);
		}
	}
		
	private VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	
	public CuestionarioService getService() {
		if (service == null) {
			service = CuestionarioService.getInstance(getActivity());
		}
		return service;
	}

}
