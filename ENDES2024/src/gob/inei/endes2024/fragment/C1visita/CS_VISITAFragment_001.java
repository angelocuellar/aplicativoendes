package gob.inei.endes2024.fragment.C1visita;

import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.C1visita.Dialog.CS_VisitaDialog;
import gob.inei.endes2024.fragment.C1visita.Dialog.CS_VisitaDialog.ACTION;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

// 
public class CS_VISITAFragment_001 extends FragmentForm implements Respondible {
	private TableComponent tcVisita;
	@FieldAnnotation(orderIndex = 1)
	public ButtonComponent btnIniciar;
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnFinalizar;
	
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQD333_1;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQD333_2;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQD333_3;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQD333_4;
	@FieldAnnotation(orderIndex = 7)
	public RadioGroupOtherField rgQD333_5;
	@FieldAnnotation(orderIndex = 8)
	public RadioGroupOtherField rgQD333_6;
	
	
	
	@FieldAnnotation(orderIndex = 9)
	public SpinnerField spnQSRESULT;
	@FieldAnnotation(orderIndex = 10)
	public TextField txtQSRESULT_O;
	
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnBorrarCuestionario;
	public ButtonComponent btnRecuperarInformacion;
	boolean btniniciar=false;
	
	public TextField txtOrden;
	private List<CSVISITA> visitas,visitasUpdate;
	private CSVISITA ultimaVisita, menorVisita,visitacalculada;
	Hogar hogar;
	public List<CSSECCION_08> detallesUpdateseccion8;
	private GridComponent grid;
	private GridComponent2 resultadoDetallado;
	private GridComponent2 gridresultado;
	private LabelComponent dummy;
	private LabelComponent dummy1;
	private RadioGroupOtherField rgOther;
	private LabelComponent lblResultado, lblfecha, lblFechaFinal,lblResultadoFinal, lblSubTituloresultado, lblGridcabecera,lblGridcargo, lblGriddni, lblGridNombres, lblGridencuestador,lblGridSupervision, lblGridCoordinador, lblGridSupervisor;
	
	private enum ACTION {
		ELIMINARCS,RECUPERARCS,ELIMINARVISITA, MENSAJE
	}

	private ACTION action;
	
	CSVISITA visita;
	DISCAPACIDAD persona_con_capacidadesdiferentes;
	private CuestionarioService cuestionarioService;
	private UsuarioService usuarioservice;
	private LabelComponent lblTitulo;
	private LabelComponent lblResultadoFinalDetallado,lblResultadoFinalDetallado2,IDinformante,Nombreinformante;
	private LabelComponent lblPresentacion,lblPresentacion_descrip;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	
	private DialogComponent dialog;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	VisitaService visitaService;
	private Seccion01Service personaService;
	public List<CSSECCION_08> detallesseccion08;
	public TableComponent tcNinios;
	SeccionCapitulo[] seccionesCargado08,seccionesCargadoInd;
	SeccionCapitulo[] seccionesGrabadoSeccion08; 
	SeccionCapitulo[] seccionesCargadoSeccion01,seccionescargado9;
	SeccionCapitulo[] seccionesCargadoDiscapacidad;
	CSSECCION_08 salud; 
	public List<Seccion01> detalles;
	public String[] items1={};
	CAP04_07 cap08_09; 
	boolean band=false;
	boolean s9=false;
	boolean hora=false;
	Integer result=0;
	private boolean seccion01_03=false;
	private boolean seccion04_07=false;

	public LabelComponent lbl26a1, lbl26a2, lbl26a3, lbl26a4, lbl26a5, lbl26a6,lbllimitaciones;
	public GridComponent2 gridPreguntas26;
	
	public CS_VISITAFragment_001() {
	}

	public CS_VISITAFragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);

		enlazarCajas();
		listening();
		seccionesCargado08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","QS802A","QS802D","PERSONA_ID_NINIO","ESTADO")}; 
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","ID","HOGAR_ID")};
		seccionesGrabadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO" )};
		seccionescargado9= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS731AH","QS731AM","QS902","QS906","HOGAR_ID","ID","PERSONA_ID")};
		
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,
				-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI",
				"QSVHORA_FIN","QSVMINUTO_FIN","QSVRESUL","QSVRESUL_O","QSRESULT", "QSRESULT_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO",
				"ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,
				-1, "QSVDIA", "QSVMES", "QSVANIO", "QSVENTREV", "QSVRESUL",
				"QSVRESUL_O", "QSPROX_DIA", "QSPROX_MES", "QSPROX_ANIO",
				"QSPROX_HORA", "QSPROX_MINUTO","QSRESULT", "QSRESULT_O") };
		
		seccionesCargadoInd= new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QIVRESUL")};
		
		seccionesCargadoDiscapacidad = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };

		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.visita).textSize(21).centrar().negrita();

//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcVisita = new TableComponent(getActivity(), this, App.ESTILO).size(250, 770).dataColumHeight(60).headerHeight(60).headerTextSize(17);
//		}
//		else{
			tcVisita = new TableComponent(getActivity(), this, App.ESTILO).size(350, 770).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcVisita.addHeader(R.string.v_nro, 0.4f);
		tcVisita.addHeader(R.string.v_fecha, 0.6f);
		tcVisita.addHeader(R.string.v_hora_ini, 0.8f);
		tcVisita.addHeader(R.string.v_hora_fin, 0.8f);
		tcVisita.addHeader(R.string.v_fecha_prox, 0.8f);
		tcVisita.addHeader(R.string.v_hora_prox, 0.8f);
		tcVisita.addHeader(R.string.v_resultado, 1.0f);

		btnIniciar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(150, 55);
		
		btnIniciar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					if (visitas.size() > 0) {
						
						if (!Util.esDiferente(visita.qsvresul, 1)) {
							ToastMessage.msgBox(getActivity(),"Visita Completa",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
							return;
						}				

						if (!Util.esDiferente(
								visitas.get(visitas.size() - 1).qsvresul, 1)) {
							ToastMessage.msgBox(getActivity(),"Cuestionarios finalizados",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
							return;
						}
						
						if (visitas.get(visitas.size() - 1).qsvresul == null) {
							ToastMessage.msgBox(getActivity(),"Finalice la visita anterior",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
							return;
						}					
						
						if (Util.esDiferente(visita.qsvresul, 1)) {
							crearVisita();
						}
					}
//					crearVisita();
					Calendar fechaactual = new GregorianCalendar();
					visita = new CSVISITA();
					visita.id = App.getInstance().getPersonaSeccion01().id;
					visita.hogar_id = App.getInstance().getPersonaSeccion01().hogar_id ;
					visita.persona_id = App.getInstance().getPersonaSeccion01().persona_id;
					visita.nro_visita = visitas.size() + 1;
					visita.qsvhora_ini = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
					visita.qsvminuto_ini = fechaactual.get(Calendar.MINUTE)+"";
					visita.qsvdia = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
					visita.qsvmes = (fechaactual.get(Calendar.MONTH)+1)+"";
					visita.qsvanio= fechaactual.get(Calendar.YEAR);
					visita.qsresult=null;
					btniniciar=true;
					
					visita.qsvhora_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvhora_ini);
					visita.qsvminuto_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvminuto_ini);
					visita.qsvdia=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvdia);
					visita.qsvmes=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvmes);
					
					if( visita.nro_visita >getCuestionarioService().getUltima_visitaVivienda(visita.id).nro_visita){
						ToastMessage.msgBox(getActivity(),"Debe iniciar visita en la vivienda",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						btniniciar=false;
						return;
					}
					
				boolean flag=grabar();
				if(flag){
					recargarLista();
					cargarSpinner();
				}
			}
		});
		
		btnFinalizar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.v_l_finalizar).size(150, 55);
		btnFinalizar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CSVISITA bean = (CSVISITA) tcVisita.getSelectedItem();
				if (bean == null) {
					ToastMessage.msgBox(getActivity(),"Seleccione una visita primero.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return;
				}
				 if (bean.qsvminuto_fin != null) {
				 ToastMessage.msgBox(getActivity(),"CAPVISITA ya fue finalizada.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				 return;
				 }
				abrirDialogo(bean, CS_VisitaDialog.ACTION.FINALIZAR);
			}
		});
		btnBorrarCuestionario = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnborrarsalud).size(150, 55);
		btnBorrarCuestionario.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				BorrarCuestionarioSalud();				
			}
		});
		btnRecuperarInformacion= new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnrecuperarsalud).size(150, 55);
		btnRecuperarInformacion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RecuperarCuestionarioSalud();				
			}
		});
		spnQSRESULT = new SpinnerField(getActivity()).size(altoComponente + 15,350).callback("onSpinerChangeValue");
//		cargarSpinner();
		txtQSRESULT_O = new TextField(getActivity()).size(altoComponente, MATCH_PARENT).maxLength(100).hint(R.string.especifique).soloTexto();
		
		dummy = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		dummy1 = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		grid = new GridComponent(getActivity(), Gravity.CENTER, 5, 0);
		grid.addComponent(btnIniciar);
		grid.addComponent(dummy);
		grid.addComponent(btnBorrarCuestionario);
		grid.addComponent(dummy1);
		grid.addComponent(btnRecuperarInformacion);
				
		lblSubTituloresultado = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vresultcabecera).textSize(21).centrar().negrita();

		lblfecha = new LabelComponent(this.getActivity()).size(altoComponente, 170).text(R.string.v_fecha).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblResultado = new LabelComponent(this.getActivity()).text(R.string.v_resultado).size(altoComponente+10, 170).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblFechaFinal = new LabelComponent(this.getActivity()).size(altoComponente, 250).textSize(18).centrar().negrita();
		lblResultadoFinal = new LabelComponent(this.getActivity()).size(altoComponente, 350).textSize(18).centrar().negrita();
		
		gridresultado = new GridComponent2(this.getActivity(), 2);
		gridresultado.addComponent(lblfecha);
		gridresultado.addComponent(lblFechaFinal);
		gridresultado.addComponent(lblResultado);
		gridresultado.addComponent(lblResultadoFinal);
		
		tcNinios = new TableComponent(getActivity(), this,App.ESTILO).size(400, 770).headerHeight(altoComponente + 10).dataColumHeight(50);
		
		tcNinios.addHeader(R.string.visitaresul_nombrenino, 1f,TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.visitaresul_nro, 0.3f,TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.visitaresul_resul,0.9f, TableComponent.ALIGN.CENTER);
//		tcNinios.addHeader(R.string.seccion01_sexo,1.2f, TableComponent.ALIGN.CENTER);
		
		
		lblResultadoFinalDetallado = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.visitaresul_detallado).textSize(21).centrar().negrita();
		lblResultadoFinalDetallado2 = new LabelComponent(this.getActivity()).size(60, MATCH_PARENT).text(R.string.visitaresul_detallado2).textSize(17).centrar().negrita();
		Nombreinformante = new LabelComponent(this.getActivity()).size(60, 400).textSize(18).centrar();
		IDinformante = new LabelComponent(this.getActivity()).size(60, 30).textSize(18);
		
		if(App.getInstance().getPersonaSeccion01()!=null){
			if(App.getInstance().getPersonaSeccion01().qh02_2==null){
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" ");
			}else{
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" "+App.getInstance().getPersonaSeccion01().qh02_2);
			}
			
			IDinformante.setText(""+App.getInstance().getPersonaSeccion01().persona_id);
		}
		
		
		resultadoDetallado = new GridComponent2(this.getActivity(), 3);
		resultadoDetallado.addComponent(lblResultadoFinalDetallado2,3,1);
		resultadoDetallado.addComponent(Nombreinformante);
		resultadoDetallado.addComponent(IDinformante);
		resultadoDetallado.addComponent(spnQSRESULT);
		resultadoDetallado.addComponent(txtQSRESULT_O, 3,1);
		
//		Spanned texto2 = Html.fromHtml("<p align=\"justify\">Se�or / Se�ora / Se�orita,  mi nombre es F1 y soy trabajadora del  Instituto Nacional de Estad�stica e Inform�tica,  instituci�n  que por especial encargo del  Ministerio de Salud est� realizando un estudio sobre la salud de las personas de 15 a�os a m�s y de los ni�os menores de 12 a�os, a nivel nacional; con el objeto de  evaluar y orientar la futura  implementaci�n de  los diversos programas de salud, dirigidos a mejorar las condiciones de salud de la poblaci�n en el pa�s. La informaci�n  que nos  brinde es estrictamente confidencial y permanecer� en absoluta reserva.</p>");

		Spanned texto2=Html.fromHtml("<div style=\"text-align:justify\">"+
"Se�or / Se�ora / <b>Se�orita</b>,  mi nombre es F1 y soy trabajadora del  Instituto Nacional de Estad�stica e Inform�tica,  instituci�n  que por especial encargo del  Ministerio de Salud est� realizando un estudio sobre la salud de las personas de 15 a�os a m�s y de los ni�os menores de 12 a�os, a nivel nacional; con el objeto de  evaluar y orientar la futura  implementaci�n de  los diversos programas de salud, dirigidos a mejorar las condiciones de salud de la poblaci�n en el pa�s. La informaci�n  que nos  brinde es estrictamente confidencial y permanecer� en absoluta reserva."+"</div>");
		
		lblPresentacion = new LabelComponent(this.getActivity()).size(60, 550).text(R.string.visita_presentacion).textSize(18).negrita().centrar();
//		lblPresentacion_descrip = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.visita_presentacion_desc).textSize(17);
		lblPresentacion_descrip = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17); lblPresentacion_descrip.setText(texto2);
		
		if(App.getInstance().getMarco().usu_id!=null){
			Usuario usuario = getUsuarioService().getUsuarioById(App.getInstance().getMarco().usu_id);
			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1",usuario.nombres+" "+obtenerPrimerApellido(usuario.apellidos)));
		}else{
			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1","..."));
		}
		
		/***AGREGANDO DISCAPACIDAD CARATULA SALUD**/
		lbl26a1 = new LabelComponent(this.getActivity()).size(altoComponente, 450).text(R.string.seccion01qh26a1).textSize(18).alinearIzquierda();
		rgQD333_1 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a1_1, R.string.seccion01qh26a1_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a2 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a2).textSize(18).alinearIzquierda();
		rgQD333_2 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a2_1, R.string.seccion01qh26a2_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a3 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a3).textSize(18).alinearIzquierda();
		rgQD333_3 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a3_1, R.string.seccion01qh26a3_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a4 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a4).textSize(18).alinearIzquierda();
		rgQD333_4 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a4_1, R.string.seccion01qh26a4_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a5 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a5).textSize(18).alinearIzquierda();
		rgQD333_5 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a5_1, R.string.seccion01qh26a5_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a6 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a6).textSize(18).alinearIzquierda();
		rgQD333_6 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a6_1, R.string.seccion01qh26a6_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		lbllimitaciones = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh26);
		
		gridPreguntas26 = new GridComponent2(this.getActivity(), App.ESTILO, 2);
		gridPreguntas26.addComponent(lbl26a1);
		gridPreguntas26.addComponent(rgQD333_1);
		gridPreguntas26.addComponent(lbl26a2);
		gridPreguntas26.addComponent(rgQD333_2);
		gridPreguntas26.addComponent(lbl26a3);
		gridPreguntas26.addComponent(rgQD333_3);
		gridPreguntas26.addComponent(lbl26a4);
		gridPreguntas26.addComponent(rgQD333_4);
		gridPreguntas26.addComponent(lbl26a5);
		gridPreguntas26.addComponent(rgQD333_5);
		gridPreguntas26.addComponent(lbl26a6);
		gridPreguntas26.addComponent(rgQD333_6);
	}
	public void crearVisita(){

		Calendar fechaactual = new GregorianCalendar();
		visita = new CSVISITA();
		visita.id = App.getInstance().getPersonaSeccion01().id;
		visita.hogar_id = App.getInstance().getPersonaSeccion01().hogar_id ;
		visita.persona_id = App.getInstance().getPersonaSeccion01().persona_id;
		visita.nro_visita = visitas.size() + 1;
		visita.qsvhora_ini = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
		visita.qsvminuto_ini = fechaactual.get(Calendar.MINUTE)+"";
		visita.qsvdia = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
		visita.qsvmes = (fechaactual.get(Calendar.MONTH)+1)+"";
		visita.qsvanio= fechaactual.get(Calendar.YEAR);
		
		visita.qsvhora_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvhora_ini);
		visita.qsvminuto_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvminuto_ini);
		visita.qsvdia=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvdia);
		visita.qsvmes=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita.qsvmes);
	}
	
	public String obtenerPrimerApellido(String apellidos){

		Integer pos=0;
		if(apellidos!=null){
			for(int i=0;i<apellidos.length();i++){
				if(apellidos.charAt(i)==' '){
					pos=i;
					break;
				}
			}
			return apellidos.substring(0, pos);
		}else{
			return "";
		}
		
	}
    
	private void abrirDialogo(CSVISITA bean, CS_VisitaDialog.ACTION action) {
		FragmentManager fm = this.getFragmentManager();
		CS_VisitaDialog aperturaDialog = CS_VisitaDialog.newInstance(this, bean,
				action);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
		recargarLista();
	}

	@Override
	protected View createUI() {
		buildFields();
		/* Aca creamos las preguntas */
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(tcVisita.getTableView(), grid.component());
		tcVisita.setSelectorData(R.drawable.selector_sombra_lista);
		q2 = createQuestionSection(lblSubTituloresultado,gridresultado.component());
		q3 = createQuestionSection(lblResultadoFinalDetallado,resultadoDetallado.component());
		q5 = createQuestionSection(lbllimitaciones,gridPreguntas26.component());
		q6 =createQuestionSection(tcNinios.getTableView());
		q4 = createQuestionSection(lblPresentacion,lblPresentacion_descrip);
//		q4 = createQuestionSection(0,Gravity.CENTER_HORIZONTAL|Gravity.CENTER,lblPresentacion_descrip);
		// ///////////////////////////
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		/* Aca agregamos las preguntas a la pantalla */
		form.addView(q0);
		form.addView(q4);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q5);
		form.addView(q6);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(visita);
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		}
		if(btniniciar){
		GrabarVisitaCalculada(visita,"QSVDIA", "QSVMES", "QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA","ID", "HOGAR_ID","PERSONA_ID");

		
		btniniciar=false;
		}
		else{
		btniniciar=false;
//		Log.e("","QSRESULT G: "+visita.qsresult);
		if(visita!=null && visita.qsresult!=null){
			GrabarVisitaCalculada(visita,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA","QSRESULT_O");
//			}
		///AGREGANDO GRABADO DE DISCAPACIDAD DEL CUESTIONARIO DE SALUD
//				Log.e("","RESULTADO: "+visita.qsresult);
				if(!Util.esDiferente(visita.qsresult, 6)){
//					Log.e("","INGRESO SE VA A GRABAR");
					EndesCalendario.GrabadoGeneralDiscapacidad(ObtenerDatosParaGrabarDiscapacidad());
				}
				else{
					getCuestionarioService().BorrarDiscapacidad(visita.id, visita.hogar_id, visita.persona_id, App.CUEST_ID_CARA_SALUD, 0);			
				}
			}
		}
		visita = getCuestionarioService().getCAPVISITASALUD(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
		App.getInstance().setC1Visita(visita);
		cargarTabla();
//			try {	
//				if(!getVisitaService().saveOrUpdate(visita,"QSVDIA", "QSVMES", "QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINUTO_FIN","NRO_VISITA","QSVRESUL","QSVRESUL_O","QSRESULT","QSRESULT_O","QSPROX_DIA","QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID","PERSONA_ID")){
//						 ToastMessage.msgBox(getActivity(), "Los datos no fueron guardados.",
//		                         ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//		         	}
//			} catch (SQLException e) {
//				 ToastMessage.msgBox(getActivity(),e.getMessage()+"error visita",
//                         ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//			}
		return true;
	}

	public DISCAPACIDAD ObtenerDatosParaGrabarDiscapacidad(){
		persona_con_capacidadesdiferentes= new DISCAPACIDAD();
		persona_con_capacidadesdiferentes.id=visita.id;
		persona_con_capacidadesdiferentes.hogar_id=visita.hogar_id;
		persona_con_capacidadesdiferentes.cuestionario_id=App.CUEST_ID_CARA_SALUD;
		persona_con_capacidadesdiferentes.persona_id=visita.persona_id;
		persona_con_capacidadesdiferentes.ninio_id=0;
		persona_con_capacidadesdiferentes.qd333_1=Integer.parseInt(rgQD333_1.getTagSelected("0").toString());
		persona_con_capacidadesdiferentes.qd333_2=Integer.parseInt(rgQD333_2.getTagSelected("0").toString());
		persona_con_capacidadesdiferentes.qd333_3=Integer.parseInt(rgQD333_3.getTagSelected("0").toString());
		persona_con_capacidadesdiferentes.qd333_4=Integer.parseInt(rgQD333_4.getTagSelected("0").toString());
		persona_con_capacidadesdiferentes.qd333_5=Integer.parseInt(rgQD333_5.getTagSelected("0").toString());
		persona_con_capacidadesdiferentes.qd333_6=Integer.parseInt(rgQD333_6.getTagSelected("0").toString());
		return persona_con_capacidadesdiferentes;
	}
	private boolean validar() {
		if (!isInRange())
			return false;
		
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		boolean HabilitarCuandoesMef=false;
		if(App.getInstance().getPersonaSeccion01().qh06==2 && App.getInstance().getPersonaSeccion01().qh07<50){
			HabilitarCuandoesMef= getCuestionarioService().getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id);
//			if(spnQSRESULT.getSelectedItemKey()!=null){
//				if(!Util.esDiferente(visita.qsresult,9)){
//					mensaje = "Debe Ingresar Especifique "; 
//					view = txtQSRESULT_O; 
//					error = true; 
//					return false; 
//				}
//				
//			}
			if (!HabilitarCuandoesMef) {
				mensaje = "Debe cambiar de resultado en el C.I"; 
				error = true; 
				return false; 
			}
		}
		else{
			if(spnQSRESULT.getSelectedItemKey()!=null){
					if(!Util.esDiferente(visita.qsresult,9)){
						if(Util.esVacio(visita.qsresult_o)){
							mensaje = "Debe Ingresar Especifique "; 
							view = txtQSRESULT_O; 
							error = true; 
							return false; 
						}
					}
					if(!Util.esDiferente(visita.qsresult,6)){
						Integer valor=Integer.parseInt(rgQD333_1.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P01 Discapacidad");
							view = rgQD333_1;
							error = true;
							return false;	
						}
						valor=Integer.parseInt(rgQD333_2.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P02 Discapacidad");
							view = rgQD333_2;
							error = true;
							return false;	
						}
						valor=Integer.parseInt(rgQD333_3.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P03 Discapacidad");
							view = rgQD333_3;
							error = true;
							return false;	
						}
						valor=Integer.parseInt(rgQD333_4.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P04 Discapacidad");
							view = rgQD333_4;
							error = true;
							return false;	
						}
						valor=Integer.parseInt(rgQD333_5.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P05 Discapacidad");
							view = rgQD333_5;
							error = true;
							return false;	
						}
						valor=Integer.parseInt(rgQD333_6.getTagSelected("0").toString());
						if(!Util.esDiferente(valor, 0)){
							mensaje = preguntaVacia.replace("$", "P06 Discapacidad");
							view = rgQD333_6;
							error = true;
							return false;	
						}
					}
			}
		}
		return true;
	}

	public void individual(){
		if(App.getInstance().getPersonaSeccion01().qh06==2 && App.getInstance().getPersonaSeccion01().qh07<50){
			List<Individual> indResultado=getCuestionarioService().getListadoIndividual(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoInd);
			Integer resultado=0;
			for (Individual id:indResultado) {
				if(!Util.esDiferente(id.persona_id, App.getInstance().getPersonaSeccion01().persona_id)){
					if(id.qivresul!=null){
						resultado=id.qivresul;
					}
				}
			}		
			if (Util.esDiferente(resultado, 1)) {
	             ultimaVisita.qsresult=resultado;
//	             Log.e("resultado: ",""+resultado);	             
					ultimaVisita.qsresult = ultimaVisita.getConvertResult(ultimaVisita.qsresult);
					entityToUI(ultimaVisita);
			}
		}
	}
	@Override
	public void cargarDatos() {
		if(App.getInstance().getPersonaSeccion01()!=null){
			visita = getCuestionarioService().getCAPVISITASALUD(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
			seccion01_03 = getCuestionarioService().getExisteSeccion01_03(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id);
			seccion04_07 = getCuestionarioService().getExisteSeccion04_07(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id);
			persona_con_capacidadesdiferentes = getCuestionarioService().getSeccion02IndividualParaDiscapacidad(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,Integer.valueOf(App.CUEST_ID_CARA_SALUD), App.getInstance().getPersonaSeccion01().persona_id,0, seccionesCargadoDiscapacidad);
			if(App.getInstance().getPersonaSeccion01()!=null){
			if(App.getInstance().getPersonaSeccion01().qh02_2==null){
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" ");
			}else{
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" "+App.getInstance().getPersonaSeccion01().qh02_2);
			}
			
			IDinformante.setText(""+App.getInstance().getPersonaSeccion01().persona_id);
		}
		cargarTabla();
		calcularCombobox();
		cargarSpinner();
		recargarLista();
			if(visita!=null){
				if(visita.qsresult==null){
					visita.qsresult=spnQSRESULT.getSelectedItemKey()!=null?(Integer) spnQSRESULT.getSelectedItemKey():null;
				}
				if(visita.qsresult!=null || seccion01_03 || seccion04_07){
				GrabarVisitaCalculada(visita,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA","QSRESULT_O");
				}
//				visita = getCuestionarioService().getCAPVISITASALUD(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
				entityToUI(visita);
			}
		}
		recargarLista();
		inicio();
		/*AGREGADO PARA DISCAPACIDAD*/
		
		if(visita!=null && !Util.esDiferente(visita.qsresult,6)){
//			persona_con_capacidadesdiferentes = getCuestionarioService().getSeccion02IndividualParaDiscapacidad(visita.id,visita.hogar_id, visita.persona_id,Integer.valueOf(App.CUEST_ID_CARA_SALUD),0, seccionesCargadoDiscapacidad);
//			Log.e("","TODO: "+persona_con_capacidadesdiferentes);
			if(persona_con_capacidadesdiferentes!=null){
				rgQD333_1.setTagSelected(persona_con_capacidadesdiferentes.qd333_1);
				rgQD333_2.setTagSelected(persona_con_capacidadesdiferentes.qd333_2);
				rgQD333_3.setTagSelected(persona_con_capacidadesdiferentes.qd333_3);
				rgQD333_4.setTagSelected(persona_con_capacidadesdiferentes.qd333_4);
				rgQD333_5.setTagSelected(persona_con_capacidadesdiferentes.qd333_5);
				rgQD333_6.setTagSelected(persona_con_capacidadesdiferentes.qd333_6);
			}
		}
	}
	public void GrabarVisitaCalculada(CSVISITA visita,String...campos){
		visitacalculada=MyUtil.VerificarCompletadoVisitaDeSalud(visita,seccionesCargado08,getCuestionarioService(),seccionescargado9);
		if(visitacalculada!=null){
			try {
//				getVisitaService().saveOrUpdate(visitacalculada,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA");
				getVisitaService().saveOrUpdate(visitacalculada,campos);
			} catch (SQLException e) {
				ToastMessage.msgBox(getActivity(),e.getMessage()+"error visita calculada",
	                    ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	}
	public void calcularCombobox(){
         Integer cont=0;
		if(detallesseccion08.size()!=0){
		    cont=0;
			for(int i=0;i<detallesseccion08.size();i++){
				if(detallesseccion08.get(i).qs802a!=null){
					if(detallesseccion08.get(i).qs802a==1){
						cont++;
					}
				}
			}
		}
		if(cont==detallesseccion08.size()) 
			band=true;
		else band=false;
		recargarLista();
//		SeccionCapitulo[] seccionescargado9= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS731AH","QS731AM","QS902","QS906","HOGAR_ID","ID","PERSONA_ID")};
//		cap08_09 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionescargado9); 
//		
//		if(cap08_09==null) {
//			s9=false;
//		}
//		if(cap08_09!=null){
//			if(cap08_09.qs902!=null && cap08_09.qs906!=null){
//				if( (cap08_09.qs902==1 || cap08_09.qs902==4) && cap08_09.qs906==1){
//					s9=true;
//				}else{
//					s9=false;
//				}
//			}else {
//				 s9=false;  
//			}
//		}
	}
	public boolean registrarComboCompletado(CuestionarioService cuestionarioService, Integer id, Integer  hogar_id,Integer persona_id){
		boolean band=false;
		if(cuestionarioService.getCuestionarioDelSaludCompletado(id,hogar_id,persona_id)){
			band=true;
		}else band=false;
		
		return band;
	}
//	public CSVISITA registrarVisitaSaludCompletada(){		
//    	return	MyUtil.RegistrarVisitaSaludCompletada(getCuestionarioService(), App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,App.getInstance().getC1Visita().nro_visita, ultimaVisita.qsvresul);
//    }
//	public void grabarVisita(){
//		boolean resultado=false;
//		Integer resul=0;
//		resul=ultimaVisita.qsvresul;
//		Log.e("","RESULT: "+resul);
//		if(s9 && cap08_09!=null){
//			if(cap08_09.qs731ah!=null && cap08_09.qs731am!=null){
//				ultimaVisita.qsvhora_fin=Integer.parseInt(cap08_09.qs731ah);
//		    	ultimaVisita.qsvminuto_fin=Integer.parseInt(cap08_09.qs731am);
//			}
//		}
//		boolean existeninios=getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, 0, 11);
//			if(band && result==1 && s9){
//				Log.e("1","1");
//				ultimaVisita.qsvresul=1;
//				ultimaVisita.qsresult=1;
//			}
//			else{
//				Log.e("","ENTRO AQUI: ");
//				if(result==2 && !existeninios && band){
//					Log.e("1","2");
//					ultimaVisita.qsvresul=2;
//					ultimaVisita.qsresult=2;
//				}else if(result==3 && !existeninios && band){
//					Log.e("1","3");
//					ultimaVisita.qsvresul=3;
//					ultimaVisita.qsresult=3;
//				}
//				else if(result==4 && !existeninios && band){
//					Log.e("1","4");
//					ultimaVisita.qsvresul=4;
//					ultimaVisita.qsresult=4;
//				}
//				else if(result==6 && !existeninios && band){
//					Log.e("1","5");
//					ultimaVisita.qsvresul=6;
//					ultimaVisita.qsresult=6;
//				}
//				else if(result==7 && !existeninios && band){
//					Log.e("1","6");
//					ultimaVisita.qsvresul=9;
//					ultimaVisita.qsresult=9;
//				}
//				else{
//					Log.e("1","7 DATA: "+ result);
//					Log.e("","ENTRO AQUI: CAMBIO");
//					ultimaVisita.qsvresul=5;
//					ultimaVisita.qsresult=5;
//				}
//			}
//			
//			if(ultimaVisita.qsvresul!=resul){
//				Log.e("1","9");
//				resultado=true;
//			}
//			
//			if (resultado && result!=1 || !resultado && ultimaVisita.qsvhora_fin==null && ultimaVisita.qsvresul!=null ) {
//				Log.e("1","10");
//				Calendar calendario = new GregorianCalendar();
//				ultimaVisita.qsvhora_fin=calendario.get(Calendar.HOUR_OF_DAY);
//		    	ultimaVisita.qsvminuto_fin=calendario.get(Calendar.MINUTE);
//			}
//			
//		
//		try {	
//			if(ultimaVisita.id!=null){
//			 getVisitaService().saveOrUpdate(ultimaVisita,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN");
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		recargarLista();
//	}
	
	
	private void cargarTabla(){
		detallesseccion08=new ArrayList<CSSECCION_08>();
		detallesseccion08= getCuestionarioService().getListadoNiniosSeccion08(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,seccionesCargado08);
		tcNinios.setData(detallesseccion08 ,"getNombre","persona_id_ninio","get802a");
		registerForContextMenu(tcNinios.getListView());
	}
    public void blanquearPantalla(){
		if(CuestionarioFragmentActivity.blanquear){
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		}
    }
	private void inicio() {
		onSpinerChangeValue();
		ValidarsiesSupervisora();
		individual();
		blanquearPantalla();
		renombrarTexto();
		validarBotones();
	}
	private void validarBotones(){
		if(visitas.size()>0 && visitas.get(visitas.size()-1).qsvresul!=null && Util.esDiferente(visitas.get(visitas.size()-1).qsvresul,1)){
			Util.lockView(getActivity(), false,btnBorrarCuestionario,btnRecuperarInformacion);
			btnBorrarCuestionario.setVisibility(View.VISIBLE);
			btnRecuperarInformacion.setVisibility(View.VISIBLE);
		}
		else{
			btnBorrarCuestionario.setVisibility(View.GONE);
			btnRecuperarInformacion.setVisibility(View.GONE);
			Util.cleanAndLockView(getActivity(),btnBorrarCuestionario,btnRecuperarInformacion);
		}
	}
	public void renombrarTexto(){
		lbllimitaciones.text(R.string.seccion01qh26);
		lbllimitaciones.setText(lbllimitaciones.getText().toString().replace("(NOMBRE)",App.getInstance().getPersonaSeccion01().qh02_1));
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			spnQSRESULT.readOnly();
			Util.cleanAndLockView(getActivity(), btnIniciar,btnFinalizar);
			rgQD333_1.readOnly();
			rgQD333_2.readOnly();
			rgQD333_3.readOnly();
			rgQD333_4.readOnly();
			rgQD333_5.readOnly();
			rgQD333_6.readOnly();
			
		}
	}

	public void recargarLista() {
		
		visitas = getCuestionarioService().getCAPVISITAs2(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,getInformanteSalud(), seccionesCargado);
		if(visitas.size()==0){
			lblFechaFinal.setText("...");
    		lblResultadoFinal.setText("...");
        	App.getInstance().setC1Visita(null);
        }  

		ultimaVisita = new CSVISITA();
		menorVisita = new CSVISITA();
		tcVisita.setData(visitas, "nro_visita", "getFechainicio","getHoraInicio", "getHoraFin", "getFechaProxima","getHoraProxima", "getResultado");
		registerForContextMenu(tcVisita.getListView());
		Integer indice = 0;
		if (visitas.size() > 0) {
			if (visitas.size() > 1) {
				indice = visitas.size() - 1;
			} else {
				indice = 0;
			}
			
			ultimaVisita = getCuestionarioService().getCAPVISITA(
					App.getInstance().getPersonaSeccion01().id,
					App.getInstance().getPersonaSeccion01().hogar_id,
					visitas.get(indice).nro_visita,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
			
			App.getInstance().setC1Visita(null);
			App.getInstance().setC1Visita(ultimaVisita);
			
			menorVisita = getCuestionarioService().getCAPVISITA(
					App.getInstance().getPersonaSeccion01().id,
					App.getInstance().getPersonaSeccion01().hogar_id,
					visitas.get(0).nro_visita,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
		}
		
		 if(menorVisita==null){
				lblFechaFinal.setText("...");
	    		lblResultadoFinal.setText("...");
	        }
			if(ultimaVisita==null){
				lblFechaFinal.setText("...");
	    		lblResultadoFinal.setText("...");
	        }
		if (menorVisita != null && ultimaVisita != null) {
			lblFechaFinal.setText(Util.completarCadena(Util.getText(ultimaVisita.qsvdia), "0", 2,COMPLETAR.IZQUIERDA)
					+ "/"+ Util.completarCadena(Util.getText(ultimaVisita.qsvmes),"0", 2, COMPLETAR.IZQUIERDA)+"/"+Util.getText(ultimaVisita.qsvanio));
			if (!Util.esVacio(menorVisita.qsvresul)) {
				if (!Util.esDiferente(ultimaVisita.qsvresul, 1)) {
					lblResultadoFinal.setText("1. COMPLETA");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 2)) {
					lblResultadoFinal.setText("2. AUSENTE");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 3)) {
					lblResultadoFinal.setText("3. APLAZADA");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 4)) {
					lblResultadoFinal.setText("4. RECHAZADA");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 5)) {
					lblResultadoFinal.setText("5. INCOMPLETA");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 6)) {
					lblResultadoFinal.setText("6. DISCAPACITADA(O)");
				}
				if (!Util.esDiferente(ultimaVisita.qsvresul, 9)) {
					lblResultadoFinal.setText("9. OTRO");
				}
			} 
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcVisita.getListView())) {
			menu.setHeaderTitle("Opciones de las Visitas");
			menu.add(0, 0, 1, "Editar");
			menu.add(0, 1, 1, "Eliminar");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			
			menu.getItem(0).setVisible(false);
			menu.getItem(1).setVisible(false);
			if (info.position == visitas.size() - 1) {
				if (visitas.get(info.position).qsvresul == null) {
					menu.getItem(0).setVisible(false);
				}
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
			case 0:
				editarVisita(info.position);
				break;
			case 1:
				eliminarVisita(info.position);
				break;
			}
		}
		return super.onContextItemSelected(item);
	}

	private void editarVisita(int position) {
//		abrirDialogo(visitas.get(position), ACTION.EDITAR);
////		 recargarLista();
	}

	private void eliminarVisita(int position) {
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,
				getResources().getString(R.string.app_name),
				"Desea borrar la visita?");
		dialog.put("visita", visitas.get(position));
		dialog.showDialog();
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
		
	
	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	public UsuarioService getUsuarioService() {
		if (usuarioservice == null) {
			usuarioservice = UsuarioService.getInstance(getActivity());
		}
		return usuarioservice;
	}
	 public Seccion01Service getPersonaService() {
   	  if (personaService == null) {
   		  personaService = Seccion01Service.getInstance(getActivity());
   	  }
   	  return personaService;
     }

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccept() {
		if (dialog == null) {
			return;
		}
		if(action==ACTION.ELIMINARCS){
			SQLiteDatabase tran= getCuestionarioService().startTX();
			try {
				getCuestionarioService().BorrarCuestionarioDeSalud(App.getInstance().getPersonaSeccion01(),null);
			} catch (SQLException e) {
	            e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
			finally{
				getCuestionarioService().endTX(tran);
			}
		}
		if(action==ACTION.RECUPERARCS){
			SQLiteDatabase tran= getCuestionarioService().startTX();
			try {
				getCuestionarioService().ModificarRecuperacionDeCuestionarioSalud(App.getInstance().getPersonaSeccion01());
			}
			finally{
				getCuestionarioService().endTX(tran);
			}
		}
		if(action==ACTION.ELIMINARVISITA){
			CSVISITA bean = (CSVISITA) dialog.get("visita");
			if (bean == null) {
				ToastMessage.msgBox(getActivity(), "Error al eliminar visita.",
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return;
			}
			if (!getVisitaService().eliminarSalud(null,bean.id, bean.hogar_id,  bean.nro_visita)) {
				ToastMessage.msgBox(getActivity(),
						"Los datos no fueron eliminados.",
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return;
			}
			recargarLista();
		}
	}
	private void cargarSpinner() {
			List<Object> keys = new ArrayList<Object>();
			if(App.getInstance().getPersonaSeccion01()!=null && App.getInstance().getPersonaSeccion01().persona_id!=null){
			if(getCuestionarioService().getCuestionarioDelSaludComboCompletado(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getPersonaSeccion01().persona_id)){
//				if(s9 && band){
				if(band){
					keys.add(1);
					items1 = new String[] {
							"1. COMPLETO"
					};
					result=1;
				}else{
					keys.add(5);
					items1 = new String[] {
							"5. INCOMPLETA"
					};
					result=5;
				}
				
			}else {
				keys.add(null);
				keys.add(2);
				keys.add(3);
				keys.add(4);
				keys.add(5);
				keys.add(6);
				keys.add(9);
				items1 = new String[] { 
						" -- Iniciar Entrevista -- ",
						"2. AUSENTE",
						"3. APLAZADA", 
						"4. RECHAZADA", 
						"5. INCOMPLETA",
						"6. DISCAPACITADA(O)",
						"9. OTRO"

				};
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), R.layout.spinner_item, R.id.textview, items1);
			spnQSRESULT.setAdapterWithKey(adapter, keys);
			}
		}

	
	public void onSpinerChangeValue() {
		result=(int) spnQSRESULT.getSelectedItemId();
		String texto =(String) spnQSRESULT.getSelectedItem();
		texto=texto.substring(3, texto.length());
		if(getCuestionarioService().getCuestionarioDelSaludComboCompletado(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getPersonaSeccion01().persona_id)){
			Util.cleanAndLockView(getActivity(), txtQSRESULT_O);
			if(spnQSRESULT.getSelectedItemId()==5 && texto.trim().equals("DISCAPACITADA(O)")){
				q5.setVisibility(View.VISIBLE);
				Util.lockView(getActivity(), false,rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
			else{
				q5.setVisibility(View.GONE);
				Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
		}else{
			if(spnQSRESULT.getSelectedItemId()==5 && texto.trim().equals("DISCAPACITADA(O)")){
				q5.setVisibility(View.VISIBLE);
				Util.lockView(getActivity(), false,rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
			else{
				q5.setVisibility(View.GONE);
				Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
//				Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
		    if (spnQSRESULT.getSelectedItemId()==6) {
						Util.lockView(getActivity(), false, txtQSRESULT_O);
						txtQSRESULT_O.requestFocus();
			} else {
						Util.cleanAndLockView(getActivity(), txtQSRESULT_O);
					}
		    result++;
		    if (result==1) {
		    	ultimaVisita.qsresult=null;
		    }
		    try {	
		    	if(ultimaVisita.id!=null){
						getVisitaService().saveOrUpdate(ultimaVisita,"QSRESULT","QSRESULT_O");	
		    	}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
        
	}
	public void BorrarCuestionarioSalud(){
		action = ACTION.ELIMINARCS;
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name), "�Est� seguro que desea eliminar?"); 
		dialog.showDialog();
	}
	public void RecuperarCuestionarioSalud(){
		action = ACTION.RECUPERARCS;
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name), "�Est� seguro que desea recuperar informaci�n?"); 
		dialog.showDialog();
	}
	public Integer getInformanteSalud(){
		Integer info_salud=-1;
		Integer cantidad = getPersonaService().TotalDePersonas(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		List<CSVISITA> visitas2 = getCuestionarioService().getCAPVISITAs(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, seccionesCargado);
		if(cantidad>0 && visitas2.size()>0){
			info_salud= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,getVisitaService(),getPersonaService());
		}
		return info_salud;
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}

