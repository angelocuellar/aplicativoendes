package gob.inei.endes2024.fragment.CIseccion_05_07;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_06Fragment_003 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI610;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI611;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQI611X;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI612;
	@FieldAnnotation(orderIndex=5)
	public TextField txtQI612X;
	
	public TextField txtCabecera;
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta610,lblpregunta611,lblpregunta612;
	LinearLayout q0,q1,q2,q3;

	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01;

	public CISECCION_06Fragment_003() {}
	public CISECCION_06Fragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI610","QI611","QI611X","QI612","QI612X","QI602","QI603U","QI603N","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI610","QI611","QI611X","QI612","QI612X")};
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI310","QI320","QI226")};
		return rootView;
  }
  
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_06).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta610 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi610);
		lblpregunta611 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi611);
		lblpregunta612 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi612);
		rgQI610=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi610_1,R.string.ciseccion_06qi610_2,R.string.ciseccion_06qi610_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI610ChangeValue");
		rgQI611=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi611_1,R.string.ciseccion_06qi611_2,R.string.ciseccion_06qi611_3,R.string.ciseccion_06qi611_4,R.string.ciseccion_06qi611_5,R.string.ciseccion_06qi611_6,R.string.ciseccion_06qi611_7,R.string.ciseccion_06qi611_8,R.string.ciseccion_06qi611_9,R.string.ciseccion_06qi611_10,R.string.ciseccion_06qi611_11,R.string.ciseccion_06qi611_12,R.string.ciseccion_06qi611_13,R.string.ciseccion_06qi611_14,R.string.ciseccion_06qi611_15).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI611ChangeValue");
		txtQI611X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI611.agregarEspecifique(13,txtQI611X);
		
		rgQI612=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi612_1,R.string.ciseccion_06qi612_2,R.string.ciseccion_06qi612_3,R.string.ciseccion_06qi612_4,R.string.ciseccion_06qi612_5,R.string.ciseccion_06qi612_6,R.string.ciseccion_06qi612_7,R.string.ciseccion_06qi612_8,R.string.ciseccion_06qi612_9,R.string.ciseccion_06qi612_10,R.string.ciseccion_06qi612_11,R.string.ciseccion_06qi612_12,R.string.ciseccion_06qi612_13,R.string.ciseccion_06qi612_14,R.string.ciseccion_06qi612_15,R.string.ciseccion_06qi612_16,R.string.ciseccion_06qi612_17,R.string.ciseccion_06qi612_18,R.string.ciseccion_06qi612_19,R.string.ciseccion_06qi612_20).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI612X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI612.agregarEspecifique(18,txtQI612X);
    }
  
    @Override
    protected View createUI() {
		buildFields();
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta610,rgQI610);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta611,rgQI611);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta612,rgQI612);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi610!=null)
			individual.qi610=individual.getConvertQi610(individual.qi610);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
        
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in310 = individuals01.qi310==null?0:individuals01.qi310;  
    	Integer in603u = individual.qi603u==null?0:individual.qi603u;
    	Integer in603n = individual.qi603n==null?0:individual.qi603n;
    	Integer in226 = individuals01.qi226==null?0:individuals01.qi226;
    	
    	if (in310!=1) {
    		if ((in310==2 || in310==0) || in226==1 ||  ((in603u==1 && in603n>0 && in603n<24) || (in603u==2 && in603n>0 && in603n<2))) {
        		if (Util.esVacio(individual.qi610)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI610");
        			view = rgQI610;
        			error = true;
        			return false;
        		}
        		if (Util.esDiferente(individual.qi610,2,8)) {
        			if (Util.esVacio(individual.qi611)) {
            			mensaje = preguntaVacia.replace("$", "La pregunta QI611");
            			view = rgQI611;
            			error = true;
            			return false;
            		}
        			if (individual.qi611==14) {
            			if (Util.esVacio(individual.qi611x)) {
                			mensaje = preguntaVacia.replace("$", "La pregunta QI611X");
                			view = txtQI611X;
                			error = true;
                			return false;
                		}
        			}
        		}
        		
        		if (!Util.esDiferente(individual.qi610,2,8)) {
        			if (Util.esVacio(individual.qi612)) {
            			mensaje = preguntaVacia.replace("$", "La pregunta QI612");
            			view = rgQI612;
            			error = true;
            			return false;
            		}
            		if (individual.qi612==19) {
            			if (Util.esVacio(individual.qi612x)) {
                			mensaje = preguntaVacia.replace("$", "La pregunta QI612X");
                			view = txtQI612X;
                			error = true;
                			return false;
                		}
        			}
        		}
        	}
		}
    	
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individuals01.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi310=individuals01.qi310;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individuals01.qi226;
		App.getInstance().getCiseccion05_07().qi602=individual.qi602;
		App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
		App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
    	
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi610!=null)
			individual.qi610=individual.setConvertQi610(individual.qi610);
		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta320(){
    	Integer in310 = individuals01.qi310==null?0:individuals01.qi310;  
    	Integer in603u = individual.qi603u==null?0:individual.qi603u;
    	Integer in603n = individual.qi603n==null?0:individual.qi603n;
    	Integer in226 = individuals01.qi226==null?0:individuals01.qi226;
//    	Log.e("in310 ="," "+in310);
//    	Log.e("in226 ="," "+in226);
//    	Log.e("in603u ="," "+in603u);
//    	Log.e("in603n ="," "+in603n);
//    	Log.e("in603u ="," "+in603u);
    	
    	if (in310==1) {
//    		Log.e("in310 ="," "+in310);
    		Util.cleanAndLockView(getActivity(),rgQI610,rgQI611,rgQI612);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		}
    	else{
    		if ((in310==2 ||in310==0) || in226==1 || ((in603u==1 && in603n>0 && in603n<24) || (in603u==2 && in603n>0 && in603n<2))) {
        		Util.lockView(getActivity(),false,rgQI610,rgQI611,rgQI612);
    			q0.setVisibility(View.VISIBLE);
    			q1.setVisibility(View.VISIBLE);
    			q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			onrgQI610ChangeValue(); 
    		}   
    	}
    }
    
    private void inicio() {
    	validarPregunta320();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    public void onrgQI610ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQI610.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI611,txtQI611X);
			Util.lockView(getActivity(), false,rgQI612);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.VISIBLE);
		} 
		else {
			Util.lockView(getActivity(), false,rgQI611,rgQI612);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			onrgQI611ChangeValue();
		}
    }
    
    public void onrgQI611ChangeValue() {
		if (MyUtil.incluyeRango(1,15,rgQI611.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI612,txtQI612X);
			q3.setVisibility(View.GONE);
		} 
		else {
			Util.lockView(getActivity(), false,rgQI612);
			q3.setVisibility(View.VISIBLE);
		}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI610.readOnly();
    		rgQI611.readOnly();
    		rgQI612.readOnly();
    		txtQI611X.readOnly();
    		txtQI612X.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi610!=null)
			individual.qi610=individual.getConvertQi610(individual.qi610);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}