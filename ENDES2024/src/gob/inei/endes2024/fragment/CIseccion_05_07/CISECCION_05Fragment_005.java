package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_05Fragment_005 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI522; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI523; 	
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI523_A; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI523_B; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI523_C; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI523_D; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI523_E; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI523_F; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI523_G; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI523_H; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI523_I; 
	@FieldAnnotation(orderIndex=12) 
	public TextField txtQI523_IX; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI523_J; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI523_K; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI523_L; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI523_M; 
	@FieldAnnotation(orderIndex=17) 
	public TextField txtQI523_MX; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI523_N; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI523_O; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI523_P; 
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI523_Q; 
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI523_R; 
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI523_X; 
	@FieldAnnotation(orderIndex=24) 
	public TextField txtQI523_XI; 
	@FieldAnnotation(orderIndex=25) 
	public RadioGroupOtherField rgQI524; 
	@FieldAnnotation(orderIndex=26) 
	public TextAreaField txtQIOBS_S5;
	
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSP,lblSPR,lblONG,lblOtro,lblpregunta522,lblpregunta523,lblpregunta524,lblEspacio,lblNro,lblpregunta521A_ind1,lblpregunta521A_ind2,lblpregunta521A_ind3,lblpregunta521A_ind4,lblobs_s5;
	private GridComponent2 gridPreguntas522,gridPreguntas522N;
	public TextField txtCabecera;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01; 

	public CISECCION_05Fragment_005() {} 
	public CISECCION_05Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI522","QI523","QI523_A","QI523_B","QI523_C","QI523_D","QI523_E","QI523_F","QI523_G","QI523_H","QI523_I","QI523_IX","QI523_J","QI523_K","QI523_L","QI523_M","QI523_MX","QI523_N","QI523_O","QI523_P","QI523_Q","QI523_R","QI523_X","QI523_XI","QI524","QIOBS_S5","QI512","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI522","QI523","QI523_A","QI523_B","QI523_C","QI523_D","QI523_E","QI523_F","QI523_G","QI523_H","QI523_I","QI523_IX","QI523_J","QI523_K","QI523_L","QI523_M","QI523_MX","QI523_N","QI523_O","QI523_P","QI523_Q","QI523_R","QI523_X","QI523_XI","QI524","QIOBS_S5")}; 
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI320","QI328")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07).textSize(21).centrar().negrita(); 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	
		rgQI522=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi522_1,R.string.c2seccion_05_07qi522_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS522BChangeValue");
		txtQI523=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 400).alfanumerico();
		
		lblSP  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_05_07qi523_sp).textSize(16).negrita();
		chbQI523_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_a, "1:0").size(WRAP_CONTENT, 300); 
		chbQI523_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_b, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_c, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_d, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_e, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_f, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_g, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_h, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_i, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426IChangeValue");
		txtQI523_IX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		lblSPR  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_05_07qi523_spr).textSize(16).negrita();
		chbQI523_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_j, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_k, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_l, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_m, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426MChangeValue");
		txtQI523_MX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		lblONG  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_05_07qi523_sng).textSize(16).negrita();
		chbQI523_N=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_n, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_O=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_o, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_P=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_p, "1:0").size(WRAP_CONTENT, 300);
		
		lblOtro  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_05_07qi523_Ot).textSize(16).negrita();
		chbQI523_Q=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_q, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_R=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_r, "1:0").size(WRAP_CONTENT, 300);
		chbQI523_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi523_x, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426XChangeValue");
		txtQI523_XI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		rgQI524=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi524_1,R.string.c2seccion_05_07qi524_2,R.string.c2seccion_05_07qi524_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta521A_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi523_ind1);
		lblpregunta521A_ind2 = new LabelComponent(this.getActivity()).size(altoComponente, 350).textSize(16).text(R.string.c2seccion_05_07qi523_nom);
		lblpregunta521A_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi523_ind2);
		lblpregunta521A_ind4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi523_ind3);
		lblpregunta522 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi522);
		lblpregunta523 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi523);
		lblpregunta524 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi524);
		
		lblobs_s5 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08obs_s5);
		txtQIOBS_S5 = new TextAreaField(getActivity()).maxLength(1000).size(200, 750).alfanumerico();
		
		gridPreguntas522N = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas522N.addComponent(lblpregunta521A_ind2);
		gridPreguntas522N.addComponent(txtQI523);
		
		
		gridPreguntas522 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas522.addComponent(lblSP,2);
		gridPreguntas522.addComponent(chbQI523_A,2);
		gridPreguntas522.addComponent(chbQI523_B,2);
		gridPreguntas522.addComponent(chbQI523_C,2);
		gridPreguntas522.addComponent(chbQI523_D,2);
		gridPreguntas522.addComponent(chbQI523_E,2);
		gridPreguntas522.addComponent(chbQI523_F,2);
		gridPreguntas522.addComponent(chbQI523_G,2);
		gridPreguntas522.addComponent(chbQI523_H,2);
		gridPreguntas522.addComponent(chbQI523_I);
		gridPreguntas522.addComponent(txtQI523_IX);
		gridPreguntas522.addComponent(lblSPR,2);
		gridPreguntas522.addComponent(chbQI523_J,2);
		gridPreguntas522.addComponent(chbQI523_K,2);
		gridPreguntas522.addComponent(chbQI523_L,2);
		gridPreguntas522.addComponent(chbQI523_M);
		gridPreguntas522.addComponent(txtQI523_MX);
		gridPreguntas522.addComponent(lblONG,2);
		gridPreguntas522.addComponent(chbQI523_N,2);
		gridPreguntas522.addComponent(chbQI523_O,2);
		gridPreguntas522.addComponent(chbQI523_P,2);
		gridPreguntas522.addComponent(lblOtro,2);
		gridPreguntas522.addComponent(chbQI523_Q,2);
		gridPreguntas522.addComponent(chbQI523_R,2);
		gridPreguntas522.addComponent(chbQI523_X);
		gridPreguntas522.addComponent(txtQI523_XI);
		  
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta522,rgQI522); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta523,lblpregunta521A_ind1,gridPreguntas522N.component(),lblpregunta521A_ind3,lblpregunta521A_ind4,gridPreguntas522.component()); 
		q3 = createQuestionSection(lblpregunta524,rgQI524); 
		q4 = createQuestionSection(lblobs_s5,txtQIOBS_S5); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
	
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
    	uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 

		if (Util.esVacio(individual.qi522)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI522"); 
			view = rgQI522; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi522,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi523_a,individual.qi523_b,individual.qi523_c,individual.qi523_d,individual.qi523_e,individual.qi523_f,individual.qi523_g,individual.qi523_h,individual.qi523_i,individual.qi523_j,individual.qi523_k,individual.qi523_l,individual.qi523_m,individual.qi523_o,individual.qi523_p,individual.qi523_q,individual.qi523_r,individual.qi523_x)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI523_A; 
				error = true; 
				return false; 
			}
			if (chbQI523_I.isChecked()) {
				if (Util.esVacio(individual.qi523_ix)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI523_IX; 
					error = true; 
					return false; 
				}
			}
			if (chbQI523_M.isChecked()) {
				if (Util.esVacio(individual.qi523_mx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI523_MX; 
					error = true; 
					return false; 
				}
			}
			if (chbQI523_X.isChecked()) {
				if (Util.esVacio(individual.qi523_xi)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI523_XI; 
					error = true; 
					return false; 
				}
			}
			if (Util.esVacio(individual.qi524)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI524"); 
				view = rgQI524; 
				error = true; 
				return false; 
			}
		}
		if ( individuals01.qi328!=null  && individual.qi522!=null && !Util.esDiferente(individuals01.qi328,2) && !Util.esDiferente(individual.qi522,1) ) {
			validarMensaje("Verifique la pregunta QI328, Respondi� No");
		}
		return true; 
    } 
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
		
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
    	
    	App.getInstance().getPersonaCuestionarioIndividual().qi106=individuals01.qi106;
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individuals01.qi320;
    	App.getInstance().getCiseccion05_07().qi512=individual.qi512;
    	
		entityToUI(individual); 
		inicio(); 
    } 
    
    private void inicio() { 
    	onqrgQS522BChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
	public void onqrgQS522BChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI522.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),txtQI523,txtQI523_IX,txtQI523_MX,txtQI523_XI,chbQI523_A,chbQI523_B,chbQI523_C,chbQI523_D,chbQI523_E,chbQI523_F,chbQI523_G,chbQI523_H,chbQI523_I,chbQI523_J,chbQI523_K,chbQI523_L,chbQI523_M,chbQI523_O,chbQI523_P,chbQI523_Q,chbQI523_R,chbQI523_X,rgQI524);  		
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		} 
		else {
			
			Util.lockView(getActivity(),false,txtQI523,chbQI523_A,chbQI523_B,chbQI523_C,chbQI523_D,chbQI523_E,chbQI523_F,chbQI523_G,chbQI523_H,chbQI523_I,chbQI523_J,chbQI523_K,chbQI523_L,chbQI523_M,chbQI523_O,chbQI523_P,chbQI523_Q,chbQI523_R,chbQI523_X,rgQI524); 
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			onqrgQI426IChangeValue();
			onqrgQI426MChangeValue();
			onqrgQI426XChangeValue();
			if (individuals01.qi328!=null && individual.qi522!=null  && !Util.esDiferente(individuals01.qi328,2) && MyUtil.incluyeRango(1,1,rgQI522.getTagSelected("").toString())) {
				ToastMessage.msgBox(this.getActivity(), "Verifique la pregunta QI328, Respondi� No", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			}
			txtQI523.requestFocus(); 
		}
	}
	
	public void onqrgQI426IChangeValue() {  	
		if (chbQI523_I.isChecked()) {
			Util.lockView(getActivity(),false,txtQI523_IX);  
			txtQI523_IX.requestFocus();
		} 
		else {	
			Util.cleanAndLockView(getActivity(),txtQI523_IX);			
		}	
	}
	public void onqrgQI426MChangeValue() {  	
		if (chbQI523_M.isChecked()) {
			Util.lockView(getActivity(),false,txtQI523_MX);
			txtQI523_MX.requestFocus();				
		} 
		else {
			Util.cleanAndLockView(getActivity(),txtQI523_MX);
		}	
	}
	public void onqrgQI426XChangeValue() {  	
		if (chbQI523_X.isChecked()) {
			Util.lockView(getActivity(),false,txtQI523_XI);
			txtQI523_XI.requestFocus();				
		} 
		else {
			Util.cleanAndLockView(getActivity(),txtQI523_XI);
		}	
	}
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQI523.readOnly();
			txtQI523_IX.readOnly();
			txtQI523_MX.readOnly();
			txtQI523_XI.readOnly();
			txtQIOBS_S5.setEnabled(false);
			rgQI522.readOnly();
			rgQI524.readOnly();
			chbQI523_A.readOnly();
			chbQI523_B.readOnly();
			chbQI523_C.readOnly();
			chbQI523_D.readOnly();
			chbQI523_E.readOnly();
			chbQI523_F.readOnly();
			chbQI523_G.readOnly();
			chbQI523_H.readOnly();
			chbQI523_I.readOnly();
			chbQI523_J.readOnly();
			chbQI523_K.readOnly();
			chbQI523_L.readOnly();
			chbQI523_M.readOnly();
			chbQI523_N.readOnly();
			chbQI523_O.readOnly();
			chbQI523_P.readOnly();
			chbQI523_Q.readOnly();
			chbQI523_R.readOnly();
			chbQI523_X.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
