package gob.inei.endes2024.fragment.CIseccion_05_07;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_06Fragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI602;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI603U;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI603N;
	@FieldAnnotation(orderIndex=4)
	public TextField txtQI603NX;
	
	public TextField txtCabecera;
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	
	public GridComponent2 gdEmbarazo;
	public ButtonComponent btnPronto,btnNoPuede,btnDespues,btnOtra,btnNoSabe;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta602_p1,lblpregunta602_ind,lblpregunta602_p2,lblpregunta603_p1,lblpregunta603_p2,lblmeses,lblanios,lblEspacio1,lblEspacio2,lblEspacio3,lblEspacio4,lblindica;
	public IntegerField txtQI603N_M,txtQI603N_A;
	LinearLayout q0,q1,q2;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01;

	public CISECCION_06Fragment_001() {}
	public CISECCION_06Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI602","QI603U","QI603N","QI603NX","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI602","QI603U","QI603N","QI603NX")};
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI310","QI320","QI226")};
		return rootView;
	}
    
    @Override
    protected void buildFields() {
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_06).textSize(21).centrar().negrita();
		lblpregunta602_p1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi602_p1);
		lblpregunta602_p2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi602_p2);
		lblpregunta603_p1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi603_p1);
		lblpregunta603_p2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi603_p2);
		lblpregunta602_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_06qi602_IND).negrita();
		
		txtQI603N_M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI603N_A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);		
		txtQI603NX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 400);
		
	
		rgQI602=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi602_1,R.string.ciseccion_06qi602_2,R.string.ciseccion_06qi602_3,R.string.ciseccion_06qi602_4,R.string.ciseccion_06qi602_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI602ChangeValue");
		rgQI603U=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi603_1,R.string.ciseccion_06qi603_2,R.string.ciseccion_06qi603_3,R.string.ciseccion_06qi603_4,R.string.ciseccion_06qi603_5 ,R.string.ciseccion_06qi603_6,R.string.ciseccion_06qi603_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI602ChangeValue");
		rgQI603U.agregarEspecifique(0,txtQI603N_M);
		rgQI603U.agregarEspecifique(1,txtQI603N_A);
		rgQI603U.agregarEspecifique(5,txtQI603NX);
		
		rgQI602.addView(lblpregunta602_ind,3);
		
		lblindica  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).text(R.string.ciseccion_06qi603_ind).textSize(16).negrita();
		lblmeses  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.ciseccion_06qi603_1).textSize(16);
		lblanios  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.ciseccion_06qi603_2).textSize(16);
		lblEspacio1 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		lblEspacio2 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		lblEspacio3 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		lblEspacio4 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
    }
    
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta602_p1,lblpregunta602_p2,rgQI602);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta603_p1,lblpregunta603_p2,lblindica,rgQI603U);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
    return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi603u!=null) {
			if (!Util.esDiferente(individual.qi603u,1) && txtQI603N_M.getText().toString().trim().length()!=0 ) {
				individual.qi603n=Integer.parseInt(txtQI603N_M.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi603u,2) && txtQI603N_A.getText().toString().trim().length()!=0) {
				individual.qi603n=Integer.parseInt(txtQI603N_A.getText().toString().trim());
			}
			else {
				individual.qi603n=null;
			}
		}
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().qi602=individual.qi602;
			App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
			App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
    	Integer in320 = individuals01.qi320==null?0:individuals01.qi320;
    	
    	if (in320!=1 && in320!=2) {
    		if (Util.esVacio(individual.qi602)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI602");
    			view = rgQI602;
    			error = true;
    			return false;
    		}
    		if (!Util.esDiferente(individual.qi602,1)) {    			
    			if (Util.esVacio(individual.qi603u)) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI603U");
    				view = rgQI603U;
    				error = true;
    				return false;
    			}
    			if (!Util.esDiferente(individual.qi603u,1) || !Util.esDiferente(individual.qi603u,2)) {
    				if (Util.esVacio(individual.qi603n)) {
    					mensaje = preguntaVacia.replace("$", "La pregunta QI603N");
    					view = txtQI603N;
    					error = true;
    					return false;
    				}
    				if(!Util.esDiferente(individual.qi603u,1) && individual.qi603n!=null  && (Util.esMenor(individual.qi603n,1) || Util.esMayor(individual.qi603n, 23)) ){
    					mensaje = "Valor en meses esta fuera de rango pregunta 603N"; 
    					view = txtQI603N;
    					error = true;
    					return false;
    				}
    				if(!Util.esDiferente(individual.qi603u,2) && individual.qi603n!=null  && (Util.esMenor(individual.qi603n,2) || Util.esMayor(individual.qi603n, 40)) ){
    					mensaje = "Valor en a�os esta fuera de rango pregunta 603N"; 
    					view = txtQI603N;
    					error = true;
    					return false;
    				}    		
    			}
    			if (!Util.esDiferente(individual.qi603u,6)) {
    				if (Util.esVacio(individual.qi603nx)) {
    					mensaje = preguntaVacia.replace("$", "La pregunta QI603NX");
    					view = txtQI603NX;
    					error = true;
    					return false;
    				}
    			}
    		}
			
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
		
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		
	 	App.getInstance().getPersonaCuestionarioIndividual().qi320=individuals01.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi310=individuals01.qi310;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individuals01.qi226;
		App.getInstance().getCiseccion05_07().qi602=individual.qi602;
		App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
		App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
		
		entityToUI(individual);
		
		if (individual.qi603n!=null) {	
    		if (!Util.esDiferente(individual.qi603u,1)) {
    			txtQI603N_M.setText(individual.qi603n.toString());
    		}
    		if (!Util.esDiferente(individual.qi603u,2)) {
    			txtQI603N_A.setText(individual.qi603n.toString());
    		}       	
    	}
		
		inicio();
    }
    
    public void validarPregunta320(){
    	Integer in320 = individuals01.qi320==null?0:individuals01.qi320;
    	Integer in226 = individuals01.qi226==null?0:individuals01.qi226;
    	
    	if (in320==1 || in320==2) {
    		Util.cleanAndLockView(getActivity(),rgQI602,txtQI603N,rgQI603U);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		}
    	else{
    		if (in226==1) {
    			Util.lockView(getActivity(),false,rgQI602,txtQI603N,rgQI603U);
    			q0.setVisibility(View.VISIBLE);
    			lblpregunta602_p1.setVisibility(View.GONE);
    			lblpregunta602_p2.setVisibility(View.VISIBLE);
    			lblpregunta603_p1.setVisibility(View.GONE);
    			lblpregunta603_p2.setVisibility(View.VISIBLE);
    			Util.lockView(getActivity(), false,rgQI602);
	    		rgQI602.lockButtons(true,3);
	    		onrgQI602ChangeValue();
			}
    		else{
    			Util.lockView(getActivity(),false,rgQI602,txtQI603N,rgQI603U);
    			q0.setVisibility(View.VISIBLE);
    			lblpregunta602_p1.setVisibility(View.VISIBLE);
    			lblpregunta602_p2.setVisibility(View.GONE);
    			lblpregunta603_p1.setVisibility(View.VISIBLE);
    			lblpregunta603_p2.setVisibility(View.GONE);
    			Util.lockView(getActivity(), false,rgQI602);
	    		rgQI602.lockButtons(true,4);
    			onrgQI602ChangeValue();
    		}
    	}
    }
    
    private void inicio() {
    	validarPregunta320();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    

    
    public void onrgQI602ChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,5,rgQI602.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI603U,txtQI603N,txtQI603NX);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI603U,txtQI603N);
			q2.setVisibility(View.VISIBLE);
		}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI602.readOnly();
    		rgQI603U.readOnly();
    		txtQI603N_A.readOnly();
    		txtQI603N_M.readOnly();
    		txtQI603NX.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }

	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi603u!=null) {
			if (!Util.esDiferente(individual.qi603u,1) && txtQI603N_M.getText().toString().trim().length()!=0 ) {
				individual.qi603n=Integer.parseInt(txtQI603N_M.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi603u,2) && txtQI603N_A.getText().toString().trim().length()!=0) {
				individual.qi603n=Integer.parseInt(txtQI603N_A.getText().toString().trim());
			}
			else {
				individual.qi603n=null;
			}
		}
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().qi602=individual.qi602;
			App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
			App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return App.INDIVIDUAL;
	}
}