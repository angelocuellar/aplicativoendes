package gob.inei.endes2024.fragment.CIseccion_05_07;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_06Fragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public CheckBoxField chbQI607_A;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI607_B;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI607_C;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI607_D;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI607_E;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI607_F;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI607_G;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI607_H;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI607_I;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI607_J;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI607_K;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI607_L;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI607_M;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI607_N;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI607_O;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI607_P;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI607_Q;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI607_R;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI607_S;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI607_T;
	@FieldAnnotation(orderIndex=21)
	public CheckBoxField chbQI607_U;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI607_X;
	@FieldAnnotation(orderIndex=23)
	public TextField txtQI607_X_I;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI607_Z;
	@FieldAnnotation(orderIndex=25)
	public RadioGroupOtherField rgQI608;
	
	public TextField txtCabecera;
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta607_p1,lblpregunta607_p2,lblpregunta608,lblpregunta607_ind;
	LinearLayout q0,q1,q2;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01;

	public CISECCION_06Fragment_002() {}
	public CISECCION_06Fragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI602","QI603U","QI603N","QI607_A","QI607_B","QI607_C","QI607_D","QI607_E","QI607_F","QI607_G","QI607_H","QI607_I","QI607_J","QI607_K","QI607_L","QI607_M","QI607_N","QI607_O","QI607_P","QI607_Q","QI607_R","QI607_S","QI607_T","QI607_U","QI607_X","QI607_X_I","QI607_Z","QI608","QI602","QI603U","QI603N","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI607_A","QI607_B","QI607_C","QI607_D","QI607_E","QI607_F","QI607_G","QI607_H","QI607_I","QI607_J","QI607_K","QI607_L","QI607_M","QI607_N","QI607_O","QI607_P","QI607_Q","QI607_R","QI607_S","QI607_T","QI607_U","QI607_X","QI607_X_I","QI607_Z","QI608")};
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI320","QI226","QI310")};
		return rootView;
	}
    
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_06).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta607_p1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi607_p1);
		lblpregunta607_p2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi607_p2);
		lblpregunta607_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi607_ind);		
		lblpregunta608 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi608);
		
		chbQI607_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_AChangeValue");
		chbQI607_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_BChangeValue");
		chbQI607_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_CChangeValue");
		chbQI607_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_DChangeValue");
		chbQI607_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_EChangeValue");
		chbQI607_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_FChangeValue");
		chbQI607_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_GChangeValue");
		chbQI607_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_HChangeValue");
		chbQI607_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_IChangeValue");
		chbQI607_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607JChangeValue");
		chbQI607_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_KChangeValue");
		chbQI607_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_LChangeValue");
		chbQI607_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_MChangeValue");
		chbQI607_N=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_NChangeValue");
		chbQI607_O=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_o, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_OChangeValue");
		chbQI607_P=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_p, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_PChangeValue");
		chbQI607_Q=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_q, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_QChangeValue");
		chbQI607_R=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_r, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_RChangeValue");
		chbQI607_S=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_s, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_SChangeValue");
		chbQI607_T=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_t, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_TChangeValue");
		chbQI607_U=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_u, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_UChangeValue");
		chbQI607_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_XChangeValue");
		chbQI607_Z=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi607_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI607_ZChangeValue");
		txtQI607_X_I=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI608=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi608_1,R.string.ciseccion_06qi608_2,R.string.ciseccion_06qi608_3,R.string.ciseccion_06qi608_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
    }
    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		LinearLayout ly607 = new LinearLayout(getActivity());
		ly607.addView(chbQI607_X);
		ly607.addView(txtQI607_X_I);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta607_p1,lblpregunta607_p2,chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,ly607,chbQI607_Z);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta608,rgQI608);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
 
    return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
    	Integer in602 = individual.qi602==null?0:individual.qi602;
    	Integer in603u = individual.qi603u==null?0:individual.qi603u;
    	Integer in603n = individual.qi603n==null?0:individual.qi603n;
    	Integer in310 = individuals01.qi310==null?0:individuals01.qi310;
    	Integer in226 = individuals01.qi226==null?0:individuals01.qi226;
    	
    	if( ((in602==1 || in602==2) && (in226==2 || in226==8) && (in310==2 ||in310==0) && ((in603u==2 && in603n>=2) || in603u==0 )) ){
    		if (!Util.alMenosUnoEsDiferenteA(0,individual.qi607_a,individual.qi607_b,individual.qi607_c,individual.qi607_d,individual.qi607_e,individual.qi607_f,individual.qi607_g,individual.qi607_h,individual.qi607_i,individual.qi607_j,individual.qi607_k,individual.qi607_l,individual.qi607_m,individual.qi607_n,individual.qi607_o,individual.qi607_p,individual.qi607_q,individual.qi607_r,individual.qi607_s,individual.qi607_t,individual.qi607_u,individual.qi607_x,individual.qi607_z)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI607_A");
    			view = chbQI607_A;
    			error = true;
    			return false;
    		}
    		if (!Util.esDiferente(individual.qi607_x,1)){
				if (Util.esVacio(individual.qi607_x_i)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI607_X_I");
        			view = txtQI607_X_I;
        			error = true;
        			return false;
				}
    		}
    	}
    	
    	if (		 in602==4 
    			|| ((in602==1 || in602==2) && (in226==2 || in226==8) && in310==1  && (in603u==1 || in603u==2 || in603u==0))
    			|| ((in602==1 || in602==2) && (in226==2 || in226==8) && (in310==2 ||in310==0) && ((in603u==2 && in603n>=2) || in603u==0 ) )
    			
    			) {
    	  	if (Util.esVacio(individual.qi608)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI608");
    			view = rgQI608;
    			error = true;
    			return false;
    		}
		}


		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individuals01.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi310=individuals01.qi310;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individuals01.qi226;
		App.getInstance().getCiseccion05_07().qi602=individual.qi602;
		App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
		App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
    	
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}    	
		entityToUI(individual);
		inicio();
    }
    
    
    public void validarPregunta226(){
    	Integer in602 = individual.qi602==null?0:individual.qi602;
    	Integer in603u = individual.qi603u==null?0:individual.qi603u;
    	Integer in603n = individual.qi603n==null?0:individual.qi603n;
    	Integer in310 = individuals01.qi310==null?0:individuals01.qi310;
    	Integer in226 = individuals01.qi226==null?0:individuals01.qi226;
    	
    	if (in602==2) {
			lblpregunta607_p1.setVisibility(View.GONE);
			lblpregunta607_p2.setVisibility(View.VISIBLE);
		}
    	else{
    		lblpregunta607_p1.setVisibility(View.VISIBLE);
    		lblpregunta607_p2.setVisibility(View.GONE);
    	}
    	
    	
    	if(      (in602==1 && (in603u!=1 && in603u!=2))   
    		 ||  (in602==1 && in226==1)    
    		 || ((in602==1 || in602==2) && (in226==2  || in226==8) && (in310==2 ||in310==0) && ((in603u==1 && in603n>0 && in603n<24) || (in603u==2 && in603n>0 && in603n<2)))
    		 || ((in602==2 && in226==1) || in602==3 || in602==5)	){
				Util.cleanAndLockView(getActivity(),rgQI608,chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,chbQI607_X);
	    		Log.e("caso 1",""+"1");
	    		q0.setVisibility(View.GONE);
				q1.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);     			
		}
		
		if(     (in602==1 && (in603u==1 || in603u==2) && (in226==2 || in226==8) && in310==1)
		     || (in602==2 && (in226==2 || in226==8) && in310==1) 
		     ||  in602==4  ){
				Log.e("caso 1",""+"2");
				Util.cleanAndLockView(getActivity(),chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,chbQI607_X);
				Util.lockView(getActivity(),false,rgQI608);
				q0.setVisibility(View.VISIBLE);
				q1.setVisibility(View.GONE);
				q2.setVisibility(View.VISIBLE);
		}    	
		if(      ((in602==1 || in602==2) && (in226==2 || in226==8) && (in310==2 ||in310==0) && ((in603u==2 && in603n>=2) || in603u==0 ))   ){
				Log.e("caso 1",""+"3");
				Util.lockView(getActivity(),false,rgQI608,chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,chbQI607_X);
				q0.setVisibility(View.VISIBLE);
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);  
		}
		
    }   	
    
    private void inicio() {
    	validarPregunta226();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    public boolean verificarCheck() {
  		if (chbQI607_A.isChecked() || chbQI607_B.isChecked() || chbQI607_C.isChecked() || chbQI607_D.isChecked() || chbQI607_E.isChecked() || chbQI607_F.isChecked() || chbQI607_G.isChecked() || chbQI607_H.isChecked() || chbQI607_I.isChecked() || chbQI607_J.isChecked() || chbQI607_K.isChecked() || chbQI607_L.isChecked() || chbQI607_M.isChecked() || chbQI607_N.isChecked() || chbQI607_O.isChecked() || chbQI607_P.isChecked() || chbQI607_Q.isChecked() || chbQI607_R.isChecked() || chbQI607_S.isChecked() || chbQI607_T.isChecked() || chbQI607_U.isChecked() || chbQI607_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void onchbQI607_AChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_BChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_CChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_DChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_EChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_FChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_GChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_HChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_IChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607JChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_KChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_LChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_MChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_NChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_OChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_PChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_QChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_RChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_SChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_TChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_UChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    }
    public void onchbQI607_XChangeValue() {
    	if (verificarCheck()) Util.cleanAndLockView(getActivity(),chbQI607_Z);
    	else				  Util.lockView(getActivity(),false,chbQI607_Z);
    	
    	if (chbQI607_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI607_X_I);
  			txtQI607_X_I.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI607_X_I);
  		}
    }
    
    public void onchbQI607_ZChangeValue() {
    	if (chbQI607_Z.isChecked())
  			Util.cleanAndLockView(getActivity(),chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,chbQI607_X);
  		else
  			Util.lockView(getActivity(),false,chbQI607_A,chbQI607_B,chbQI607_C,chbQI607_D,chbQI607_E,chbQI607_F,chbQI607_G,chbQI607_H,chbQI607_I,chbQI607_J,chbQI607_K,chbQI607_L,chbQI607_M,chbQI607_N,chbQI607_O,chbQI607_P,chbQI607_Q,chbQI607_R,chbQI607_S,chbQI607_T,chbQI607_U,chbQI607_X);
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI608.readOnly();
    		txtQI607_X_I.readOnly();
    		chbQI607_A.readOnly();
    		chbQI607_B.readOnly();
    		chbQI607_C.readOnly();
    		chbQI607_D.readOnly();
    		chbQI607_E.readOnly();
    		chbQI607_F.readOnly();
    		chbQI607_G.readOnly();
    		chbQI607_H.readOnly();
    		chbQI607_I.readOnly();
    		chbQI607_J.readOnly();
    		chbQI607_K.readOnly();
    		chbQI607_L.readOnly();
    		chbQI607_M.readOnly();
    		chbQI607_N.readOnly();
    		chbQI607_O.readOnly();
    		chbQI607_P.readOnly();
    		chbQI607_Q.readOnly();
    		chbQI607_R.readOnly();
    		chbQI607_S.readOnly();
    		chbQI607_T.readOnly();
    		chbQI607_U.readOnly();
    		chbQI607_X.readOnly();
    		chbQI607_Z.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}