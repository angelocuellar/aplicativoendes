package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_05Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI512B; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI512BA; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI512BB; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI512BC; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI512C; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI512CX; 

	
	CISECCION_05_07 individual;
	public CheckBoxField chbP512BA;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta512B,lblpregunta512BA,lblpregunta512BB,lblpregunta512BC,lblpregunta512C,lblEdad,lblEspacio; 
	private GridComponent2 gridPreguntas512BA;
	private ButtonComponent btnOmisionEdad;
	public TextField txtCabecera;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CISECCION_05Fragment_003() {} 
	public CISECCION_05Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512B","QI512BA","QI512BB","QI512BC","QI512C","QI512CX","QI512AB","QI512","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512B","QI512BA","QI512BB","QI512BC","QI512C","QI512CX")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07).textSize(21).centrar().negrita(); 
	  rgQI512B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512b_1,R.string.c2seccion_05_07qi512b_2,R.string.c2seccion_05_07qi512b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	  lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEdad  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_05_07qi512ba_1).textSize(16).centrar();
	  txtQI512BA=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqrgQS512BAChangeValue");
	  chbP512BA=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512ba_2, "1:0").size(60, 200);

	  chbP512BA.setOnCheckedChangeListener(new OnCheckedChangeListener() {			
		  @Override
		  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked){
				MyUtil.LiberarMemoria();
	    		Util.cleanAndLockView(getActivity(),txtQI512BA);
	    		Util.lockView(getActivity(),false,rgQI512BB,rgQI512BC); 
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    	  	onqrgQS512BBChangeValue();
	    		rgQI512BB.requestFocus(); 
	  		}
			else {
	  			MyUtil.LiberarMemoria();
				Util.lockView(getActivity(), false,txtQI512BA);
				txtQI512BA.setVisibility(View.VISIBLE);
				Util.cleanAndLockView(getActivity(),rgQI512BB,rgQI512BC);  		
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		txtQI512BA.requestFocus();
				}
			}
	  });
	  
	  rgQI512BB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512bb_1,R.string.c2seccion_05_07qi512bb_2,R.string.c2seccion_05_07qi512bb_3,R.string.c2seccion_05_07qi512bb_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS512BBChangeValue"); 
	  rgQI512BC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512bc_1,R.string.c2seccion_05_07qi512bc_2,R.string.c2seccion_05_07qi512bc_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  rgQI512C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512c_1,R.string.c2seccion_05_07qi512c_2,R.string.c2seccion_05_07qi512c_3,R.string.c2seccion_05_07qi512c_4,R.string.c2seccion_05_07qi512c_5,R.string.c2seccion_05_07qi512c_6,R.string.c2seccion_05_07qi512c_7,R.string.c2seccion_05_07qi512c_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  txtQI512CX=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
	  rgQI512C.agregarEspecifique(7, txtQI512CX);
	  
	  lblpregunta512B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512b);
	  lblpregunta512BA = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512ba);
	  lblpregunta512BB = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512bb);
	  lblpregunta512BC = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512bc);
	  lblpregunta512C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512c);
  
	  gridPreguntas512BA = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
	  gridPreguntas512BA.addComponent(lblEdad);
	  gridPreguntas512BA.addComponent(txtQI512BA);
	  gridPreguntas512BA.addComponent(lblEspacio);
	  gridPreguntas512BA.addComponent(chbP512BA);
	  

	  
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta512B,rgQI512B); 
		q2 = createQuestionSection(lblpregunta512BA,gridPreguntas512BA.component()); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta512BB,rgQI512BB); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta512BC,rgQI512BC); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta512C,rgQI512C); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 		
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual);
		if (chbP512BA.isChecked())
			individual.qi512ba=98;
		individual.qi512c=individual.qi512c!=null?individual.getConvertQi512(individual.qi512c):null;
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in512 = individual.qi512==null?0:individual.qi512;
		
		if(Util.esMenor(App.getInstance().getPersonaCuestionarioIndividual().qi106,25) && in512!=0){
			if (Util.esVacio(individual.qi512b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI512B"); 
				view = rgQI512B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi512ba)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI512BA"); 
				view = txtQI512BA; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi512ba, 98)) {
				if (Util.esVacio(individual.qi512bb)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI512BB"); 
					view = rgQI512BB; 
					error = true; 
					return false; 
				}
			}
			if (!Util.esDiferente(individual.qi512bb,1)) {
				if (Util.esVacio(individual.qi512bc)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI512BC"); 
					view = rgQI512BC; 
					error = true; 
					return false; 
				}
			}
			if (Util.esVacio(individual.qi512c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI512C"); 
				view = rgQI512C; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi512c,96)) {
				if (Util.esVacio(individual.qi512cx)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI512CX"); 
					view = txtQI512CX; 
					error = true; 
					return false; 
				} 
			}
			
			if (MyUtil.incluyeRango(0, 9, individual.qi512ba) ||  MyUtil.incluyeRango(99, 99, individual.qi512ba) ) {
	    		mensaje ="Edad fuera de rango";
				view = txtQI512BA;
				error=true;
				return false;
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		App.getInstance().getCiseccion05_07().qi512=individual.qi512;
		
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		} 
		individual.qi512c=individual.qi512c!=null?individual.setConvertQi512(individual.qi512c):null;
		entityToUI(individual); 
		inicio(); 
    } 
    
    private void esconderpreguntasEdadMayora24(){
    	
    	Integer in106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
    	Integer in512 = individual.qi512==null?0:individual.qi512;
    	
    	if(Util.esMayor(in106, 24) ||  (Util.esMenor(in106, 25) && in512==0 )){
//    		Log.e("111","edad"+App.getInstance().getPersonaCuestionarioIndividual().qi106);
    		Util.cleanAndLockView(getActivity(),rgQI512B,rgQI512BB,rgQI512BC,rgQI512C);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    	}
    	else{
    		Log.e("222","else edad");
    		Util.lockView(getActivity(), false,rgQI512B,rgQI512BB,rgQI512BC,rgQI512C);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		onqrgQS512BAChangeValue();
    		
    		if (individual.qi512ba!=null && individual.qi512ba==98) {
        		chbP512BA.setChecked(true);
        		Util.cleanAndLockView(getActivity(),txtQI512BA);
        		txtQI512BA.setVisibility(View.GONE);
    		}
    	}
    }
    
    
    private void inicio() { 
    	esconderpreguntasEdadMayora24();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
 
    
    public void onqrgQS512BAChangeValue() {
    	if (MyUtil.incluyeRango(0,97,txtQI512BA.getText().toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI512BB,rgQI512BC);  		
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		rgQI512C.requestFocus();
    	} 
    	else {
    		MyUtil.LiberarMemoria();
    	  	onqrgQS512BBChangeValue();
    		rgQI512BB.requestFocus(); 
    	}
    }
    
    public void onqrgQS512BBChangeValue() {
      	if (MyUtil.incluyeRango(2,8,rgQI512BB.getTagSelected("").toString())) {
      		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI512BC);  		
    		q4.setVisibility(View.GONE);
    		rgQI512C.requestFocus();
    	} 
    	else {
    		MyUtil.LiberarMemoria();
    		Util.lockView(getActivity(),false,rgQI512BC); 
    		q4.setVisibility(View.VISIBLE);
    		rgQI512BC.requestFocus(); 
    	}    	
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI512B.readOnly();
    		rgQI512BB.readOnly();
    		rgQI512BC.readOnly();
    		rgQI512C.readOnly();
    		chbP512BA.readOnly();
    		txtQI512BA.readOnly();
    		txtQI512CX.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (chbP512BA.isChecked())
			individual.qi512ba=98;
		individual.qi512c=individual.qi512c!=null?individual.getConvertQi512(individual.qi512c):null;
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
