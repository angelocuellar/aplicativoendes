package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_07Fragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI719A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI719B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI719C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI719D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI719E; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI720A; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQI720AX; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI721A; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI721B; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI721C; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI721D; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI721E;
	
	private boolean filtro720=false; 
	CISECCION_02 individualS2;
	CISECCION_05_07 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblvacio,lblTitulo,lblpregunta719,lblpregunta720A,lblpregunta721,lbltitulo719_1,lbltitulo719_2,lbltitulo719_3,lbltitulo719_4,lbltitulo719_5,lbltitulo719_6,lblpregunta719_a,lblpregunta719_b,lblpregunta719_c,lblpregunta719_d,lblpregunta719_e,lblpregunta721_a,lblpregunta721_b,lblpregunta721_c,lblpregunta721_d,lblpregunta721_e; 
	public TextField txtCabecera;
	GridComponent2 gridPreguntas719,gridPreguntas721;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS1; 

	public CISECCION_07Fragment_005() {} 
	public CISECCION_07Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI719A","QI719B","QI719C","QI719D","QI719E","QI720A","QI720AX","QI721A","QI721B","QI721C","QI721D","QI721E","QI501","QI709","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI719A","QI719B","QI719C","QI719D","QI719E","QI720A","QI720AX","QI721A","QI721B","QI721C","QI721D","QI721E")};
//		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI217","QI218","ID","HOGAR_ID","PERSONA_ID","QI212")};
		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07qi_titulo).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI719A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi719_1,R.string.c2seccion_05_07qi719_2,R.string.c2seccion_05_07qi719_3,R.string.c2seccion_05_07qi719_4,R.string.c2seccion_05_07qi719_5,R.string.c2seccion_05_07qi719_6).size(260,400 ).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI719B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi719_1,R.string.c2seccion_05_07qi719_2,R.string.c2seccion_05_07qi719_3,R.string.c2seccion_05_07qi719_4,R.string.c2seccion_05_07qi719_5,R.string.c2seccion_05_07qi719_6).size(260,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI719C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi719_1,R.string.c2seccion_05_07qi719_2,R.string.c2seccion_05_07qi719_3,R.string.c2seccion_05_07qi719_4,R.string.c2seccion_05_07qi719_5,R.string.c2seccion_05_07qi719_6).size(260,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI719D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi719_1,R.string.c2seccion_05_07qi719_2,R.string.c2seccion_05_07qi719_3,R.string.c2seccion_05_07qi719_4,R.string.c2seccion_05_07qi719_5,R.string.c2seccion_05_07qi719_6).size(260,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI719E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi719_1,R.string.c2seccion_05_07qi719_2,R.string.c2seccion_05_07qi719_3,R.string.c2seccion_05_07qi719_4,R.string.c2seccion_05_07qi719_5,R.string.c2seccion_05_07qi719_6).size(260,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
   
		txtQI720AX = new TextField(getActivity()).size(altoComponente,450).maxLength(1000);
		rgQI720A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi720a_1,R.string.c2seccion_05_07qi720a_2,R.string.c2seccion_05_07qi720a_3,R.string.c2seccion_05_07qi720a_4,R.string.c2seccion_05_07qi720a_5,R.string.c2seccion_05_07qi720a_6,R.string.c2seccion_05_07qi720a_7,R.string.c2seccion_05_07qi720a_8,R.string.c2seccion_05_07qi720a_9,R.string.c2seccion_05_07qi720a_10,R.string.c2seccion_05_07qi720a_11).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI720A.agregarEspecifique(10,txtQI720AX);
		
		rgQI721A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi721_1,R.string.c2seccion_05_07qi721_2,R.string.c2seccion_05_07qi721_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI721B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi721_1,R.string.c2seccion_05_07qi721_2,R.string.c2seccion_05_07qi721_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI721C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi721_1,R.string.c2seccion_05_07qi721_2,R.string.c2seccion_05_07qi721_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI721D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi721_1,R.string.c2seccion_05_07qi721_2,R.string.c2seccion_05_07qi721_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI721E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi721_1,R.string.c2seccion_05_07qi721_2,R.string.c2seccion_05_07qi721_3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblpregunta719 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi719);
		lblpregunta719_a = new LabelComponent(this.getActivity()).size(260, 360).textSize(17).text(R.string.c2seccion_05_07qi719a);
		lblpregunta719_b = new LabelComponent(this.getActivity()).size(260, 360).textSize(17).text(R.string.c2seccion_05_07qi719b);
		lblpregunta719_c = new LabelComponent(this.getActivity()).size(260, 360).textSize(17).text(R.string.c2seccion_05_07qi719c);
		lblpregunta719_d = new LabelComponent(this.getActivity()).size(260, 360).textSize(17).text(R.string.c2seccion_05_07qi719d);
		lblpregunta719_e = new LabelComponent(this.getActivity()).size(260, 360).textSize(17).text(R.string.c2seccion_05_07qi719e);
		
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente, 180).textSize(17);
		lbltitulo719_1 = new LabelComponent(this.getActivity()).size(altoComponente, 640).textSize(13).text(R.string.c2seccion_05_07qi719t_1).negrita();
		
		lblpregunta720A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi720a);
		lblpregunta721 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi721);
		
		lblpregunta721_a = new LabelComponent(this.getActivity()).size(altoComponente+10, 460).textSize(17).text(R.string.c2seccion_05_07qi721a);
		lblpregunta721_b = new LabelComponent(this.getActivity()).size(altoComponente, 460).textSize(17).text(R.string.c2seccion_05_07qi721b);
		lblpregunta721_c = new LabelComponent(this.getActivity()).size(altoComponente, 460).textSize(17).text(R.string.c2seccion_05_07qi721c);
		lblpregunta721_d = new LabelComponent(this.getActivity()).size(altoComponente+10, 460).textSize(17).text(R.string.c2seccion_05_07qi721d);
		lblpregunta721_e = new LabelComponent(this.getActivity()).size(altoComponente, 460).textSize(17).text(R.string.c2seccion_05_07qi721e);
		
		gridPreguntas719 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
//		gridPreguntas719.addComponent(lblvacio);
//		gridPreguntas719.addComponent(lbltitulo719_1);
		gridPreguntas719.addComponent(lblpregunta719_a);
		gridPreguntas719.addComponent(rgQI719A);
		gridPreguntas719.addComponent(lblpregunta719_b);
		gridPreguntas719.addComponent(rgQI719B);
		gridPreguntas719.addComponent(lblpregunta719_c);
		gridPreguntas719.addComponent(rgQI719C);
		gridPreguntas719.addComponent(lblpregunta719_d);
		gridPreguntas719.addComponent(rgQI719D);
		gridPreguntas719.addComponent(lblpregunta719_e);
		gridPreguntas719.addComponent(rgQI719E);
		
		gridPreguntas721 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas721.addComponent(lblpregunta721_a);
		gridPreguntas721.addComponent(rgQI721A);
		gridPreguntas721.addComponent(lblpregunta721_b);
		gridPreguntas721.addComponent(rgQI721B);
		gridPreguntas721.addComponent(lblpregunta721_c);
		gridPreguntas721.addComponent(rgQI721C);
		gridPreguntas721.addComponent(lblpregunta721_d);
		gridPreguntas721.addComponent(rgQI721D);
		gridPreguntas721.addComponent(lblpregunta721_e);
		gridPreguntas721.addComponent(rgQI721E);
  } 
  
  @Override 
  protected View createUI() { 
	  buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta719,gridPreguntas719.component()); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta720A,rgQI720A); 
		q3 = createQuestionSection(lblpregunta721,gridPreguntas721.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
    return contenedor; 
    } 
  
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 = getCuestionarioService().getCompletadoSeccion0507CI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if(Util.esDiferente(individual.qi501, 3)){
			if (Util.esVacio(individual.qi719a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI719A"); 
				view = rgQI719A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi719b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI719B"); 
				view = rgQI719B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi719c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI719C"); 
				view = rgQI719C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi719d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI719D"); 
				view = rgQI719D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi719e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI719E"); 
				view = rgQI719E; 
				error = true; 
				return false; 
			}
		}
 
		if(filtro720){
			if (Util.esVacio(individual.qi720a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI720A"); 
				view = rgQI720A; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi720a,11)) {
				if (Util.esVacio(individual.qi720ax)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI720AX"); 
					view = txtQI720AX; 
					error = true; 
					return false; 
				} 
			}
		}
		if (Util.esVacio(individual.qi721a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI721A"); 
			view = rgQI721A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi721b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI721B"); 
			view = rgQI721B; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi721c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI721C"); 
			view = rgQI721C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi721d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI721D"); 
			view = rgQI721D; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi721e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI721E"); 
			view = rgQI721E; 
			error = true; 
			return false; 
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() {
    	filtro720= getCuestionarioService().VerificarFiltro720(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,6);
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getCiseccion05_07().qi501=individual.qi501;
    	App.getInstance().getCiseccion05_07().qi709=individual.qi709;
    	
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual); 
		inicio();  
    } 
    
    public void ValidaPregunta719(){
    	if (!Util.esDiferente(individual.qi501,3)) {
    		Log.e("","1");
    		Util.cleanAndLockView(getActivity(),rgQI719A,rgQI719B,rgQI719C,rgQI719D,rgQI719E);  		
    		q1.setVisibility(View.GONE);
    	}  
    	else{
    		Log.e("","2");
    		Util.lockView(getActivity(),false,rgQI719A,rgQI719B,rgQI719C,rgQI719D,rgQI719E);  		
    		q1.setVisibility(View.VISIBLE);
    	}
    }
    public void VerificarFiltro720(){
    	if(!filtro720){
    		Util.cleanAndLockView(getActivity(), rgQI720A);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI720A);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    
    private void inicio() {
    	RenombrarEtiquetas();
    	ValidaPregunta719();
    	VerificarFiltro720();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI719A.readOnly();
    		rgQI719B.readOnly();
    		rgQI719C.readOnly();
    		rgQI719D.readOnly();
    		rgQI719E.readOnly();
    		rgQI720A.readOnly();
    		rgQI721A.readOnly();
    		rgQI721B.readOnly();
    		rgQI721C.readOnly();
    		rgQI721D.readOnly();
    		rgQI721E.readOnly();
    		txtQI720AX.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public void RenombrarEtiquetas() {
    	
//    	String nombre= getCuestionarioService().PrimerNombreCap4aVivo(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	lblpregunta720A.text(R.string.c2seccion_05_07qi720a);
//    	lblpregunta720A.setText(lblpregunta720A.getText().toString().replace("#", nombre));
//    	
//    	CISECCION_02 ninio= getCuestionarioService().getUltimoNacimientoidMayorVivo(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	
    	CISECCION_02 ninio=getCuestionarioService().getUltimoNacimientoidMayorVivo(individual.id,individual.hogar_id,individual.persona_id,seccionesCargadoS1);
		if (ninio!=null && ninio.qi212_nom!=null) {
			lblpregunta720A.setText(lblpregunta720A.getText().toString().replace("#", ninio.qi212_nom));	
		} 
		
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
