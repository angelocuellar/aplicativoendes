package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_03Fragment_003_2Dialog;
import gob.inei.endes2024.fragment.CIseccion_05_07.Dialog.CISECCION_05Fragment_002Dialog;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.R.integer;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CISECCION_05Fragment_002 extends FragmentForm  implements Respondible{ 
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI509M; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI509Y; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI510;
	@FieldAnnotation(orderIndex=4)
	public ButtonComponent btnIniciar;
	@FieldAnnotation(orderIndex=5)
	public ButtonComponent btnMostrar;
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI512;
	@FieldAnnotation(orderIndex=7) 
	public TextAreaField txtQIOBS512; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI512AB;
	
	public TableComponent tctramoscolumna04;
	CISECCION_05_07 individual; 
	CISECCION_01_03 seccion01_03;
	CISECCION_02 seccion2;
	CISECCION_02T seccion2t;
	public List<CICALENDARIO_TRAMO_COL4> detalletramos; 
	public CheckBoxField chbP509_m,chbP509_y,chbP512_1,chbP512_2;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta509_11,lblpregunta509_2,lblpregunta509_22,lblpregunta510,lblpregunta512,lblpregunta512AB,lbl509_mes,lbl509_anio,lblEspacio,lblEspacio1,lbl510_edad,
	lbl512nunca,lblEspacio2,lbledadenios,lblcuando,lblEspacio3,lblEspacio4,lblEspacio5,lblEspacio6,lblEspacio7,lblmensaje511,lblobs512;
	private GridComponent2 gridPreguntas509,gridPreguntas510,gridPreguntas512;
	public ButtonComponent btnOmisionMes, btnOmisionAnio,btnnunca,btncuando;
	public TextField txtCabecera;
	
	
	public List<CICALENDARIO_TRAMO_COL01_03> lista;
	public List<CISECCION_02> ninios;
	public CICALENDARIO_TRAMO_COL01_03 tramo1=null;
	public CICALENDARIO_TRAMO_COL01_03 tramo2=null;
	public CICALENDARIO_TRAMO_COL01_03 tramo;
	private boolean tienenacimientoenfechaactual=false;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6; 
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoFechas,seccionesGrabadoCalendario,seccionesgrabado,seccionesGrabadoTramosCol04,seccionesGrabadoTramosCol01_03; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoTramosCalendario,SeccionescargadoSeccion01_03,seccionesCargadoTramosCol01_03,seccionesCargadoTerminaciones,seccionesCargadoNinio; 
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;
	private DialogComponent dialog;
	private Calendar fecha_actual = null;//Calendar.getInstance();
	public CISECCION_05Fragment_002() {}
	public CISECCION_05Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargadoNinio = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI509M","QI509Y","QI510","QI512","QIOBS512","QI512AB","QI508","QI502","QI501","QI508","QIMES_CAL","QIANIO_CAL","QI501","QI502","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI509M","QI509Y","QI510","QI512","QIOBS512","QI512AB")};
		seccionesGrabadoFechas = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIMES_CAL","QIANIO_CAL","QI510")};
		seccionesCargadoTramosCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QITRAMO_ID","QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QICANTIDAD","CONVIVIENTE_ID")};
		seccionesGrabadoCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIANIO","QIMES_ID","QIINDICE","QICOL4","QITRAMO_ID")};
		seccionesGrabadoTramosCol04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QITRAMO_ID","QICANTIDAD","CONVIVIENTE_ID")};
		SeccionescargadoSeccion01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI105D","QI105M","QI105Y","QI105CONS","QI208","QI226","QI227","QI226CONS","QI230","QI231_M","QI231_Y","QI233","QI234","QI235A","QI235B_M","QI235B_Y","QI302_01","QI302_02","QI302_03","QI302_04","QI302_06","QI302_05","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI230","QI231_M","QI231_Y","QI233","QI310","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X","QI311_O","QI312","QI316M","QI316Y","QI226","QI227","QI234","QI320","QI315M","QI315Y","QICAL_CONS","QIOBS_CAL","QI315Y","QI316Y","QI_FANT","QI_FANT_O","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoTramosCol01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","ID","QICOL2","QICOL3","HOGAR_ID","PERSONA_ID","QICANTIDAD")};
		seccionesGrabadoTramosCol01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","QICANTIDAD","QICOL3")};
		seccionesCargadoTerminaciones = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"TERMINACION_ID","QI235_M","QI235_Y","QI235_D","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07).textSize(21).centrar().negrita(); 
	  txtQI509M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	  txtQI509Y=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(4).callback("onqi509ChangeValue");
	  
	  lblobs512 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07obs512);
	  txtQIOBS512 = new TextAreaField(getActivity()).maxLength(1000).size(100, 750).alfanumerico();
		
	  chbP509_m=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi509mn, "1:0").size(60, 250);	  
	  chbP509_m.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
		    		Util.cleanAndLockView(getActivity(),txtQI509M);
		  		}
				else {
					Util.lockView(getActivity(), false,txtQI509M);
					txtQI509M.setVisibility(View.VISIBLE);
				}
			}
	   });
	  
	  chbP509_y=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi509yn, "1:0").size(60,250);
	  chbP509_y.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
		    		Util.cleanAndLockView(getActivity(),txtQI509Y);
		    		Util.lockView(getActivity(), false,txtQI510);
		    		q2.setVisibility(View.VISIBLE);		    	
		  		}
				else {
					MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI509Y);
					txtQI509Y.setVisibility(View.VISIBLE);
					Util.cleanAndLockView(getActivity(), txtQI510);
		    		q2.setVisibility(View.GONE);
				}
			}
	  });
	  
	  txtQI510=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onQI510ChangeValue"); 
	  txtQI512=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqi512TextoChangeValue");
	  
	  chbP512_1=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512_4, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
	  chbP512_1.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked ){
				if(seccion01_03.qi208>0 || !Util.esDiferente(seccion01_03.qi226, 1) || !Util.esDiferente(seccion01_03.qi230, 1)){
					ToastMessage.msgBox(getActivity(), "VERIFICAR  \"Nunca relaciones y embarazo/perdida previa\"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
	  			}				
				else if(( !Util.esDiferente(seccion01_03.qi302_01,1) || !Util.esDiferente(seccion01_03.qi302_02,1) || !Util.esDiferente(seccion01_03.qi302_04,1) || !Util.esDiferente(seccion01_03.qi302_06,1) || !Util.esDiferente(seccion01_03.qi302_07,1) || !Util.esDiferente(seccion01_03.qi302_08,1) || !Util.esDiferente(seccion01_03.qi302_09,1) || !Util.esDiferente(seccion01_03.qi302_10,1) || !Util.esDiferente(seccion01_03.qi302_11,1) || !Util.esDiferente(seccion01_03.qi302_12,1) || !Util.esDiferente(seccion01_03.qi302_13,1) || !Util.esDiferente(seccion01_03.qi302_14,1))){
					ToastMessage.msgBox(getActivity(), "VERIFICAR  \"Nunca relaciones y uso de m�todo anticonceptivo\"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				}
				
				if (App.getInstance().getPersonaCuestionarioIndividual().qi106>=25) {
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),txtQI512,chbP512_2,rgQI512AB);
		    		q7.setVisibility(View.GONE);
				}
				else {
					Util.cleanAndLockView(getActivity(),txtQI512,chbP512_2);
					Util.lockView(getActivity(), false,rgQI512AB);
		    		q7.setVisibility(View.VISIBLE);
				}
	    		
	  		}
			else {
				MyUtil.LiberarMemoria();
				Util.lockView(getActivity(), false,txtQI512,chbP512_2);
				Util.cleanAndLockView(getActivity(),rgQI512AB);
				q7.setVisibility(View.GONE);
	  		}
		}
	  });
//	  Integer d=getResources().getDisplayMetrics().densityDpi;
//	  if(!Util.esDiferente(d,App.YOGA8)){
//		  tctramoscolumna04= new TableComponent(getActivity(), this, App.ESTILO).size(300, 750).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tctramoscolumna04= new TableComponent(getActivity(), this, App.ESTILO).size(400, 750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
	  
//	  tctramoscolumna04.addHeader(R.string.cicalendario_tramonro, 0.25f,TableComponent.ALIGN.CENTER);
	  tctramoscolumna04.addHeader(R.string.cicalendario_tramodesde, 0.25f,TableComponent.ALIGN.LEFT);
	  tctramoscolumna04.addHeader(R.string.cicalendario_tramohasta, 0.25f,TableComponent.ALIGN.CENTER);
	  tctramoscolumna04.addHeader(R.string.cicalendario_tramoresultadohasta, 0.18f,TableComponent.ALIGN.CENTER);
	  tctramoscolumna04.addHeader(R.string.cicalendario_tramocantidad, 0.18f,TableComponent.ALIGN.CENTER);
	  chbP512_2=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512_5, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
	  chbP512_2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked){
				MyUtil.LiberarMemoria();
	    		Util.cleanAndLockView(getActivity(),txtQI512,chbP512_1,rgQI512AB);
	    		q7.setVisibility(View.GONE);
	  		}
			else {
				Util.lockView(getActivity(), false,txtQI512,chbP512_1);
				Util.cleanAndLockView(getActivity(),rgQI512AB);
				q7.setVisibility(View.GONE);
	  		}
				if(!chbP509_m.isChecked() && !chbP509_y.isChecked()){
					if(txtQI509M.getText().toString().trim().length()!=0 && txtQI509Y.getText().toString().trim().length()!=0){
						Integer anio= Integer.parseInt(txtQI509Y.getText().toString());
						Integer mes = Integer.parseInt(txtQI509M.getText().toString())-1;
					if(seccion01_03.qi208>0) {
			        	if(seccion2!=null) {
			        		Integer mesesEmbarazo = 0;
			        		if(seccion2.qi220a!=null) {
			        			mesesEmbarazo = seccion2.qi220a; 
			        		}
			        		Calendar fechaNacPrimer = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1 - mesesEmbarazo, Integer.parseInt(seccion2.qi215d));
			        		if(Util.esMayor(anio, seccion2.qi215y)	||(!Util.esDiferente(anio, seccion2.qi215y) && Util.esMayor(fechaNacPrimer.get(Calendar.MONTH),mes))) {
			        			 ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que cuando tuvo primer nacimiento \"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
			        		}        		
			        	}          	  
			          }
					  if(seccion01_03.qi235a!=null) {
				          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==2) {
				        	  if(seccion2t!=null) {
					        	  Calendar fechaPrimTerm = new GregorianCalendar(seccion2t.qi235_y, seccion2t.qi235_m - 1 - seccion2t.qi235_d, 1);
					        	  if(Util.esMayor(anio, seccion2t.qi235_y) || (!Util.esDiferente(anio, seccion2t.qi235_y)  && Util.esMayor(fechaPrimTerm.get(Calendar.MONTH), mes))) {
					        			mensaje = "VERIFICAR  \"edad ingresada es mayor que terminaci�n \""; 
					        			ToastMessage.msgBox(getActivity(), mensaje, ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					        		}
				        	    }
				          } 		
				          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==1) {
				        	  Calendar fechaAnt2011 = new GregorianCalendar(seccion01_03.qi235b_y, Integer.parseInt(seccion01_03.qi235b_m)-1, 1);
				        	 
				        		if(Util.esMayor(anio, seccion2t.qi235_y) || (!Util.esDiferente(anio, seccion2t.qi235_y)  && Util.esMayor(fechaAnt2011.get(Calendar.MONTH), mes))) {
				        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando fue la terminaci�n anterior del "+App.ANIOPORDEFECTO+" \""; 
				        			ToastMessage.msgBox(getActivity(), mensaje, ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				        		}
				          	}           
			          	}
					}
			}
		}
	  });
	  
	  rgQI512AB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512ab_1,R.string.c2seccion_05_07qi512ab_2,R.string.c2seccion_05_07qi512ab_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	  
	  lbl509_mes  = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.c2seccion_05_07qi509m).textSize(16).centrar();
	  lbl509_anio = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.c2seccion_05_07qi509y).textSize(16).centrar();
	  lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEspacio1  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEspacio2  = new LabelComponent(this.getActivity()).size(8, MATCH_PARENT);
	  
	  lbl510_edad  = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.c2seccion_05_07qi510_e).textSize(16).centrar();
	  lblmensaje511 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07qi511).textSize(16);
	  lbl512nunca  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.c2seccion_05_07qi512_1).textSize(16);
	  lbledadenios  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.c2seccion_05_07qi512_2).textSize(16);
	  lblcuando  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.c2seccion_05_07qi512_3).textSize(16);
	  lblEspacio5  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEspacio6  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEspacio7  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEspacio3  = new LabelComponent(this.getActivity()).size(8, MATCH_PARENT);
	  lblEspacio4  = new LabelComponent(this.getActivity()).size(8, MATCH_PARENT);
	   
	  lblpregunta509_11 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi509_11);
	  lblpregunta509_22 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi509_22);
	  lblpregunta510 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi510);
	  lblpregunta512 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512);
	  lblpregunta512AB = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512ab);
	 
	  btnMostrar= new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(250, 55).text(R.string.cibtnMostrar);
	  btnMostrar.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
//			VerificarsiexisteCalendarioCol01_03();
			MostrarCalendario();
		}
	  });
	  btnIniciar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(250, 55).text(R.string.v_l_iniciar);
	  btnIniciar.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			boolean flag = grabarFechasdeValidacion();
			if(!flag){
				return;
			}
			else{
				AbrirPreguntas(null);	
			}
		}
	  });

		
		
	  gridPreguntas509 = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
	  gridPreguntas509.addComponent(lbl509_mes);
	  gridPreguntas509.addComponent(txtQI509M);
	  gridPreguntas509.addComponent(lblEspacio);
	  gridPreguntas509.addComponent(chbP509_m);
	  gridPreguntas509.addComponent(lblEspacio2,4);
	  gridPreguntas509.addComponent(lbl509_anio);
	  gridPreguntas509.addComponent(txtQI509Y);
	  gridPreguntas509.addComponent(lblEspacio1);
	  gridPreguntas509.addComponent(chbP509_y);
	  
	  gridPreguntas510 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
	  gridPreguntas510.addComponent(lbl510_edad);
	  gridPreguntas510.addComponent(txtQI510);
	  
	  gridPreguntas512 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
	  gridPreguntas512.addComponent(lbl512nunca);
	  gridPreguntas512.addComponent(lblEspacio5);
	  gridPreguntas512.addComponent(chbP512_1);
	  gridPreguntas512.addComponent(lblEspacio3,3);
	  gridPreguntas512.addComponent(lbledadenios);
	  gridPreguntas512.addComponent(lblEspacio6);
	  gridPreguntas512.addComponent(txtQI512);
	  gridPreguntas512.addComponent(lblEspacio4,3);
	  gridPreguntas512.addComponent(lblcuando);
	  gridPreguntas512.addComponent(lblEspacio7);
	  gridPreguntas512.addComponent(chbP512_2);
	

  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta509_11,lblpregunta509_22,gridPreguntas509.component());
		q2 = createQuestionSection(lblpregunta510,gridPreguntas510.component());
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblmensaje511);
		q4 = createButtonSection(btnIniciar,btnMostrar);
		q5 = createQuestionSection(q4,tctramoscolumna04.getTableView());
		q6 = createQuestionSection(lblpregunta512,gridPreguntas512.component(),lblobs512,txtQIOBS512); 
		q7 = createQuestionSection(lblpregunta512AB,rgQI512AB); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q5);
		form.addView(q6);
		form.addView(q7); 
    return contenedor; 
    } 
    
    public boolean validarMesyAnio(){
    	if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(individual.qi510==null){
			if (Util.esVacio(individual.qi509m)  && !chbP509_m.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI509M"); 
				view = txtQI509M; 
				error = true; 
				return false; 
			}
			Integer mes=individual.qi509m!=null?Integer.parseInt(individual.qi509m):2;
			if((mes<=0 || mes>12) && !chbP509_m.isChecked()){
				mensaje = "Mes no corresponde"; 
				view = txtQI509M; 
				error = true; 
				return false;
			}
			if (Util.esVacio(individual.qi509y)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI509Y"); 
				view = txtQI509Y; 
				error = true; 
				return false; 
			}
			if(individual.qi509y.toString().length()<4){
				mensaje = "El a�o debe ser de 4 digitos"; 
				view = txtQI509Y; 
				error = true; 
				return false; 
			}
			if(chbP509_m.isChecked() && individual.qi509y>=App.ANIOPORDEFECTO){
				mensaje = "El mes no puede ser No sabe"; 
				view = txtQI509Y; 
				error = true; 
				return false; 
			}
			if(Util.esMenor(fecha_actual.get(Calendar.YEAR), individual.qi509y)){
				mensaje = "La fecha ingresada debe ser menor a la fecha actual"; 
				view = txtQI509Y; 
				error = true; 
				return false; 
			}
		}
		else{
			if(Util.esVacio(individual.qi510)){
				mensaje = preguntaVacia.replace("$", "La pregunta QI510"); 
				view = txtQI510; 
				error = true; 
				return false;
			}
			if(Util.esMenor(individual.qi510, 12)){
				mensaje = "A�o de inicio de uni�n no puede ser menor a 12"; 
				view = txtQI510; 
				error = true; 
				return false;
			}
			if(Util.esMayor(individual.qi510, seccion01_03.qi106)){
				mensaje = "A�o de inicio de uni�n no puede ser mayor que le edad de la mef"; 
				view = txtQI510; 
				error = true; 
				return false;
			}
		}
		
		return true;
    }

    
    public boolean grabarFechasdeValidacion(){
    	uiToEntity(individual);
    	if (!validarMesyAnio()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
    	try {
    		if(individual.qi510==null){
    			individual.qimes_cal=individual.qi509m!=null?Integer.parseInt(individual.qi509m):13;
    			individual.qianio_cal = individual.qi509y;
    		}
    		else{
    			individual.qimes_cal=13;
    			Integer anio=0;
    			anio=fecha_actual.get(Calendar.YEAR)-seccion01_03.qi106;
    			anio=anio+individual.qi510;
    			individual.qianio_cal = anio;
    		}
//    		VerificarsiexisteCalendarioCol01_03();
    		getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoFechas);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false;
		}
    	return true;
    }
    public void onQI510ChangeValue(){
    	Integer valor = txtQI510.getValue()!=null?Integer.parseInt(txtQI510.getText().toString()):0;
    	Integer tramo_id=-1;
		 Integer mes=-1;
		 Integer anio=-1;
		 mes = chbP509_m.isChecked()?13:Integer.parseInt(txtQI509M.getText().toString());
		 anio = fecha_actual.get(Calendar.YEAR)-seccion01_03.qi106;
		 anio =anio+valor;
    	if(valor<=12){
    		MyUtil.MensajeGeneral(this.getActivity(), "No puede haber tenido 12 a�os");
    		return;
    	}
    	if(anio>=App.ANIOPORDEFECTO){
    		MyUtil.MensajeGeneral(this.getActivity(), "Debe Ingresar mes y a�o ");
    		return;
    	}
    	List<CICALENDARIO_TRAMO_COL4> todo = getCuestionarioService().getTramoscol04ByPersona(individual.id, individual.hogar_id, individual.persona_id, seccionesCargadoTramosCalendario);
   	    
   	if(todo.size()>0){
   		for(CICALENDARIO_TRAMO_COL4 tramo:todo){
   				getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, individual.id,individual.hogar_id, individual.persona_id, tramo.qitramo_id);
   		}
   		todo.clear();
   	}
		 CICALENDARIO_TRAMO_COL4 tramoagrabar= new CICALENDARIO_TRAMO_COL4();
		 if(!Util.esDiferente(individual.qi508, 1)){
			if(!Util.esDiferente(individual.qi501, 1,2)){
				if(anio>=App.ANIOPORDEFECTO){
					tramoagrabar.id=individual.id;
					tramoagrabar.hogar_id = individual.hogar_id;
					tramoagrabar.persona_id=individual.persona_id;
					tramoagrabar.qimes_ini=App.MESPORDEFECTO;
					tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
					if(mes-1==0){
						tramoagrabar.qimes_fin = 12;
						tramoagrabar.qianio_fin = anio-1;
					}else{
						tramoagrabar.qimes_fin=mes-1;
						tramoagrabar.qianio_fin = anio;
					}
					tramoagrabar.qiresult="0";
					tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
					tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
					try {
						tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
					} catch (SQLException e) {
						ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
					tramoagrabar = new CICALENDARIO_TRAMO_COL4();
					tramoagrabar.id=individual.id;
					tramoagrabar.hogar_id = individual.hogar_id;
					tramoagrabar.persona_id=individual.persona_id;
					tramoagrabar.qimes_ini=mes;
					tramoagrabar.qianio_ini = anio;
					tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
					tramoagrabar.qianio_fin = fecha_actual.get(Calendar.YEAR);
					tramoagrabar.qiresult="X";
					tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
					tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
					try {
						tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
					} catch (SQLException e) {
						ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
				}
				else{
					tramoagrabar = new CICALENDARIO_TRAMO_COL4();
					tramoagrabar.id=individual.id;
					tramoagrabar.hogar_id = individual.hogar_id;
					tramoagrabar.persona_id=individual.persona_id;
					tramoagrabar.qimes_ini=App.MESPORDEFECTO;
					tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
					tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
					tramoagrabar.qianio_fin = fecha_actual.get(Calendar.YEAR);
					tramoagrabar.qiresult="X";
					tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
					tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
					try {
						tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
					} catch (SQLException e) {
						ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
					
				}
			}
			else{
				if(!Util.esDiferente(individual.qi502, 1,2)){
					if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
						tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					}
				}
			}
		 }
		 
		 
		 /**** CASO 508 = 2 ***/
		 if(!Util.esDiferente(individual.qi508, 2)){
			 if(!Util.esDiferente(individual.qi501, 1,2)){
				 if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
						tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
				 }
				 if(anio<App.ANIOPORDEFECTO){
					 
				 }
			 }
			 if(!Util.esDiferente(individual.qi501, 3) && !Util.esDiferente(individual.qi502, 1,2)  ){
				 if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
					 tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
				 }
			 }
		 }
		 cargarTabla();
		 if(ExisteEspaciosEnblanco()){
				btnIniciar.setEnabled(false);
			}
			else{
				btnIniciar.setEnabled(true);
			}
    }
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (chbP509_m.isChecked())
			individual.qi509m="98";
		if (chbP509_y.isChecked())
			individual.qi509y=9998;
		if (chbP512_1.isChecked())
			individual.qi512=0;
		if (chbP512_2.isChecked())
				individual.qi512=95;
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().qi512 = individual.qi512;
			App.getInstance().getCiseccion05_07().qi512ab = individual.qi512ab;
			App.getInstance().getCiseccion05_07().qi509m = individual.qi509m;
			App.getInstance().getCiseccion05_07().qi509y = individual.qi509y;
			App.getInstance().getCiseccion05_07().qi512 = individual.qi512;
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
//		VerificarsiexisteCalendarioCol01_03();
		return true; 
    } 
    /*FCP 31/03/2017  Todos estos metodos se pusieron en una clase EndesCalendario */
   /* public void VerificarsiexisteCalendarioCol01_03(){
    	lista= getCuestionarioService().getTramosdelcalendario(individual.id,individual.hogar_id,individual.persona_id,seccionesCargadoTramosCol01_03);
    	if((!Util.esDiferente(seccion01_03.qi302_01,2) &&  
    	   !Util.esDiferente(seccion01_03.qi302_02, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_03, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_04, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_05, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_06, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_07, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_08, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_09, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_10, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_11, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_12, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_13, 2) &&
    	   !Util.esDiferente(seccion01_03.qi302_14, 2) && lista.size()>0) ||  ((seccion01_03.qi315y!=null && seccion01_03.qi315y<App.ANIOPORDEFECTO) 	|| (seccion01_03.qi316y!=null &&  seccion01_03.qi316y<App.ANIOPORDEFECTO))){
   		if(lista.size()>0){
    			List<CICALENDARIO_TRAMO_COL01_03> copia= new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
    			copia.addAll(lista);
	    		for(CICALENDARIO_TRAMO_COL01_03 tramo: copia){
	    			Log.e("","ENTRO: ");
	    			lista.remove(tramo);
	    			EliminacionDeTablas(tramo.id, tramo.hogar_id, tramo.persona_id, tramo.qitramo_id);
	    		}
    		}
       	}
    	if(lista.size()==0){
    		RegistrarNacimientosPrimeroenTramos();
	    	Registrarterminaciones();
	    	TerminacionesSiesqueExistiera();
	    	RegistrarEmbarazoOMetodoAnticonceptivoONinguno();
	    	tramo1=null;
			tramo2=null;
			tramo=null;
	    	if(!CalendarioCompletado()){
	    	ExisteEspaciosEnBlancoEnElCalendarioTramo();
	    	}
    	}
    }
    public void ExisteEspaciosEnBlancoEnElCalendarioTramo(){
		List<CICALENDARIO_TRAMO_COL01_03> todoslostramos = getCuestionarioService().getTramosdelcalendario(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoTramosCol01_03);
		List<CICALENDARIO_TRAMO_COL01_03> todossegundorecorrido = new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
		
		Collections.reverse(todoslostramos);
		todossegundorecorrido.addAll(todoslostramos);
		for(CICALENDARIO_TRAMO_COL01_03 item: todoslostramos){ 
			todossegundorecorrido.remove(item);
			if(tramo2!=null){
				break;
			}
			tramo1=null;
			tramo1=item;
			for(CICALENDARIO_TRAMO_COL01_03 tramo:todossegundorecorrido){
				Integer Mesesfin=tramo.qimes_fin;
				Integer AnioFin= tramo.qianio_fin;
				if(Mesesfin==12){
						Mesesfin=0;
					AnioFin++;
				}
				Mesesfin=Mesesfin+1;
				if(!Util.esDiferente(item.qianio_ini, AnioFin) && !Util.esDiferente(item.qimes_ini, Mesesfin) && Util.esDiferente(item.qitramo_id,tramo.qitramo_id)){
					break;
				}
				else{
					tramo2=tramo;
					break;
				}
			}
		}
		if(tramo2==null){
			tramo2= new CICALENDARIO_TRAMO_COL01_03();
			tramo2.qianio_fin=App.ANIOPORDEFECTO;
			tramo2.qimes_fin = App.MESPORDEFECTO;
			tramo2.qir_final="";
		}
		if(tramo==null){
			tramo = new CICALENDARIO_TRAMO_COL01_03();
			tramo.id=App.getInstance().getPersonaCuestionarioIndividual().id;
			tramo.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			tramo.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			tramo.qitramo_id=getCuestionarioService().getUltimotramoidRegistrado(tramo.id, tramo.hogar_id, tramo.persona_id)+1;
		}
		if(!CalendarioCompletado()){
			RegistrarCuandoNoUsoNingunMetodoTramo();
			tramo1=null;
			tramo2=null;
			tramo=null;
			ExisteEspaciosEnBlancoEnElCalendarioTramo();
		}
	}
    public boolean CalendarioCompletado(){
    	boolean calendariocompleto= false;
    	if(getCuestionarioService().CalendarioCompletado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,fecha_actual)){
    		calendariocompleto=true;
    	}
    	return calendariocompleto;
    }
    
    public void RegistrarCuandoNoUsoNingunMetodoTramo(){
    	Integer Mesinicial=0;
    	Integer Mesfinal =0;
    	if(tramo2.qianio_fin!=App.ANIOPORDEFECTO && tramo2.qimes_fin!=App.MESPORDEFECTO){
    		Mesinicial = tramo2.qimes_fin+1;
    		if(Mesinicial==13){
    			Mesinicial=1;
    			tramo2.qianio_fin++;
    		}
    		Mesfinal = tramo1.qimes_ini-1;
    		if(Mesfinal==0){
    			Mesfinal = 12;
    			tramo1.qianio_ini--;
    		}
    	}
    	else{
//    		if(!Util.esDiferente(tramo2.qimes_fin, App.MESPORDEFECTO) && !Util.esDiferente(tramo2.qianio_fin, App.ANIOPORDEFECTO)){
//    		Mesinicial= tramo2.qimes_fin;                                                                                                                                                                                       
//    		}else{
//    			Mesinicial=Mesinicial==0?tramo2.qimes_fin+1:Mesinicial;
//    		}
//    			Mesfinal = tramo1.qimes_ini-1;
//        		if(Mesfinal==0){
//        			Mesfinal = 12;
//        			tramo1.qianio_ini--;
//        		}
    		if(!Util.esDiferente(tramo2.qimes_fin, App.MESPORDEFECTO) && !Util.esDiferente(tramo2.qianio_fin, App.ANIOPORDEFECTO)){
        		Mesinicial= !tramo2.qir_final.equals("")?tramo2.qimes_fin+1:tramo2.qimes_fin;
        		}else{
        			Mesinicial=Mesinicial==0?tramo2.qimes_fin+1:Mesinicial;
        		}
        			Mesfinal = tramo1.qimes_ini-1;
            		if(Mesfinal==0){
            			Mesfinal = 12;
            			tramo1.qianio_ini--;
            		}
            		if(Mesinicial==13){
            			Mesinicial=1;
            			tramo2.qianio_fin++;
            		}
    	}
    	tramo.qianio_ini = tramo2.qianio_fin;
    	tramo.qimes_ini = Mesinicial;
    	tramo.qimes_fin = Mesfinal;
    	tramo.qianio_fin = tramo1.qianio_ini;
    	tramo.qir_inicial ="0";
    	tramo.qir_final = "0";
    	tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,Mesinicial)-MyUtil.DeterminarIndice(tramo.qianio_fin, Mesfinal);
    	tramo.qicantidad++;
    	try {
			getCuestionarioService().saveOrUpdateTRAMO(tramo, null, seccionesGrabadoTramosCol01_03);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	tramo=null;
    }*/
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
	 	Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in502 = individual.qi502==null?0:individual.qi502;

    	if (in501!=3 || in502!=3) {
    	 	if (Util.esVacio(individual.qi509m)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta QI509M"); 
    			view = txtQI509M; 
    			error = true; 
    			return false; 
    		} 
    		Integer mes=individual.qi509m!=null?Integer.parseInt(individual.qi509m):-1;
			if((mes<=0 || mes>12) && !chbP509_m.isChecked()){
				mensaje = "Mes no corresponde"; 
				view = txtQI509M; 
				error = true; 
				return false;
			}
    		if (Util.esVacio(individual.qi509y)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta QI509Y"); 
    			view = txtQI509Y; 
    			error = true; 
    			return false; 
    		}
    		
    		if (individual.qi509y.toString().length()<4) { 
    			mensaje = "La longitud m�nima del a�o es 4 digitos"; 
    			view = txtQI509Y; 
    			error = true; 
    			return false; 
    		}
    		if(Util.esDiferente(individual.qi509y, 9998)){    			
    			Integer resta=App.ANIOPORDEFECTOSUPERIOR-39;
    			if (individual.qi509y!=null  && (individual.qi509y>App.ANIOPORDEFECTOSUPERIOR || individual.qi509y<resta)) {
					mensaje = "Valor fuera de rango Pregunta QI509 A�O"; 
					view = txtQI509Y; 
					error = true; 
					return false; 
				}
    		}
    		
    		if(!Util.esDiferente(individual.qi509y, 9998)){
    			if (Util.esVacio(individual.qi510)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta QI510"); 
    				view = txtQI510; 
    				error = true; 
    				return false; 
    			} 
    			if (individual.qi510!=null &&  (individual.qi510>=0 && individual.qi510<=11) ) {
					mensaje = "Valor fuera de rango Pregunta QI510"; 
					view = txtQI510; 
					error = true; 
					return false; 
				}
    		}
		}
    	
   
		

		if (Util.esVacio(individual.qi512)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI512"); 
			view = txtQI512; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(individual.qi512, 0)) {
//			Log.e("","QI208: "+seccion01_03.qi208);
//			Log.e("","QI226: "+seccion01_03.qi226);
//			Log.e("","QI230: "+seccion01_03.qi230);
			if((seccion01_03.qi208>0 || !Util.esDiferente(seccion01_03.qi226, 1) || !Util.esDiferente(seccion01_03.qi230, 1)) && !Util.esDiferente(individual.qi512, 0)){
				mensaje = "VERIFICAR  \"Nunca relaciones y embarazo/perdida previa\"";
    			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
			}
			
			if(( !Util.esDiferente(seccion01_03.qi302_01,1) || !Util.esDiferente(seccion01_03.qi302_02,1) || !Util.esDiferente(seccion01_03.qi302_04,1) || !Util.esDiferente(seccion01_03.qi302_06,1) || !Util.esDiferente(seccion01_03.qi302_07,1) || !Util.esDiferente(seccion01_03.qi302_08,1) || !Util.esDiferente(seccion01_03.qi302_09,1) || !Util.esDiferente(seccion01_03.qi302_10,1) || !Util.esDiferente(seccion01_03.qi302_11,1) || !Util.esDiferente(seccion01_03.qi302_12,1) || !Util.esDiferente(seccion01_03.qi302_13,1) || !Util.esDiferente(seccion01_03.qi302_14,1)) && !Util.esDiferente(individual.qi512, 0)){
				mensaje = "VERIFICAR  \"Nunca relaciones y uso de m�todo anticonceptivo\""; 
    			MyUtil.MensajeGeneral(this.getActivity(), mensaje); 
			}
		}
		
		if (Util.esMayor(individual.qi512, 0) && Util.esDiferente(individual.qi512, 95)) {
						
			Integer i512 = individual.qi512;
        	
			if(i512> 	App.getInstance().getPersonaCuestionarioIndividual().qi106) {
				mensaje = "VERIFICAR  \"edad ingresada es mayor que edad mef\""; 
	  			view = txtQI512; 
	  			error = true; 
	  			return false; 
	          } 
			if(i512<10) {
				mensaje = "VERIFICAR  \"edad ingresada debe ser mayor a 10 \""; 
	  			view = txtQI512; 
	  			error = true; 
	  			return false; 
	          } 
			Calendar fecharef = new GregorianCalendar();
	  		fecharef = new GregorianCalendar(seccion01_03.qi105y,Integer.parseInt(seccion01_03.qi105m)-1, Integer.parseInt(seccion01_03.qi105d));
	          if(seccion01_03.qi208>0) {
	        	if(seccion2!=null) {
	        		Integer mesesEmbarazo = 0;
	        		if(seccion2.qi220a!=null) {
	        			mesesEmbarazo = seccion2.qi220a; 
	        		}
	        		Calendar fechaNacPrimer = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1 - mesesEmbarazo, Integer.parseInt(seccion2.qi215d));
	        		Integer edadhastaprimernacimiento=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaNacPrimer);
	        		if(Util.esMayor(individual.qi512, edadhastaprimernacimiento)){
	        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando tuvo primer nacimiento \"";
	        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
	        		}        		
	        	}          	  
	          }
	          if(seccion01_03.qi230!=null && seccion01_03.qi234!=null) {          
		          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==2) {
		        	  Calendar fechaUltNac = new GregorianCalendar(seccion01_03.qi231_y, Integer.parseInt(seccion01_03.qi231_m)-1 - seccion01_03.qi233, 1);
		        	  Integer edadhastaprimernacimiento=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaUltNac);
		        		if(Util.esMayor(individual.qi512, edadhastaprimernacimiento)) {	        			
		        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando fue su ultima terminaci�n \""; 
		        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
		        		}
		          }    
		          
		          if(seccion01_03.qi235a!=null) {
			          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==2) {
			        	  if(seccion2t!=null) {
				        	  Calendar fechaPrimTerm = new GregorianCalendar(seccion2t.qi235_y, seccion2t.qi235_m - 1 - seccion2t.qi235_d, 1);
				        	  Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaPrimTerm);
				        	  if(Util.esMayor(individual.qi512, edadcalculada)) {
				        			mensaje = "VERIFICAR  \"edad ingresada es mayor que terminaci�n \""; 
				        			MyUtil.MensajeGeneral(this.getActivity(), mensaje); 
				        		}
			        	  }
			          } 		
			          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==1) {
			        	  Calendar fechaAnt2011 = new GregorianCalendar(seccion01_03.qi235b_y, Integer.parseInt(seccion01_03.qi235b_m)-1, 1);
			        	 Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaAnt2011);
			        		if(Util.esMayor(individual.qi512, edadcalculada)) {
			        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando fue la terminaci�n anterior del "+App.ANIOPORDEFECTO+" \""; 
			        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
			        		}
			        	  
			          }           
		          }
	          }
		}
		if(!Util.esDiferente(individual.qi512, 0) && App.getInstance().getPersonaCuestionarioIndividual().qi106<25){
			if (Util.esVacio(individual.qi512ab)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI512AB"); 
				view = rgQI512AB; 
				error = true; 
				return false; 
			} 
		}
		if(individual.qi509m!=null && individual.qi509y!=null && !individual.qi509m.equals("98")  && Util.esDiferente(individual.qi509y, 9998) && chbP512_2.isChecked()){
			Integer anio= Integer.parseInt(txtQI509Y.getText().toString());
			Integer mes = Integer.parseInt(txtQI509M.getText().toString())-1;
		if(seccion01_03.qi208>0) {
        	if(seccion2!=null) {
        		Integer mesesEmbarazo = 0;
        		if(seccion2.qi220a!=null) {
        			mesesEmbarazo = seccion2.qi220a; 
        		}
        		Calendar fechaNacPrimer = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1 - mesesEmbarazo, Integer.parseInt(seccion2.qi215d));
        		if(Util.esMayor(anio, seccion2.qi215y)	||(!Util.esDiferente(anio, seccion2.qi215y) && Util.esMayor(fechaNacPrimer.get(Calendar.MONTH),mes))) {
        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando tuvo primer nacimiento \""; 
        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
        		}        		
        	}          	  
          }
		  if(seccion01_03.qi235a!=null) {
	          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==2) {
	        	  if(seccion2t!=null) {
		        	  Calendar fechaPrimTerm = new GregorianCalendar(seccion2t.qi235_y, seccion2t.qi235_m - 1 - seccion2t.qi235_d, 1);
		        	  if(Util.esMayor(anio, seccion2t.qi235_y) || (!Util.esDiferente(anio, seccion2t.qi235_y)  && Util.esMayor(fechaPrimTerm.get(Calendar.MONTH), mes))) {
		        			mensaje = "VERIFICAR  \"edad ingresada es mayor que terminaci�n \""; 
		        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
		        		}
	        	    }
	          } 		
	          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==1) {
	        	  Calendar fechaAnt2011 = new GregorianCalendar(seccion01_03.qi235b_y, Integer.parseInt(seccion01_03.qi235b_m)-1, 1);
	        	 
	        		if(Util.esMayor(anio, seccion2t.qi235_y) || (!Util.esDiferente(anio, seccion2t.qi235_y)  && Util.esMayor(fechaAnt2011.get(Calendar.MONTH), mes))) {
	        			mensaje = "VERIFICAR  \"edad ingresada es mayor que cuando fue la terminaci�n anterior del "+App.ANIOPORDEFECTO+" \""; 
	        			MyUtil.MensajeGeneral(this.getActivity(), mensaje);
	        		}
	          	}           
          	}
		}
		
		if(!Util.esDiferente(individual.qi501, 3) && !Util.esDiferente(individual.qi502, 3) && chbP512_2.isChecked()){
			mensaje = "No esta en union y no conviv�o "; 
			view = chbP512_2; 
			error = true; 
			return false;
		}
		if(VerificacionDeDatos512()){
			mensaje = "Edad de su primera relaci�n sexual no puede ser mayor a su edad primera uni�n";
			MyUtil.MensajeGeneral(getActivity(), mensaje);
		}
		if(VerificacionDeDatos509()){
			mensaje = "Su primera union no puede ser menor a 12 a�os";
			MyUtil.MensajeGeneral(getActivity(), mensaje);
		}
//		if(seccion01_03.qi226!=null && seccion01_03.qi226==1){
//			Calendar fecharefembarazo = new GregorianCalendar();
//			Calendar embarazo= new GregorianCalendar();
//		  		
//			if(seccion01_03.qi226cons!=null){
//				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//				Date date2 = null;
//				Date date1 = null;
//				try {
//					date2 = df.parse(seccion01_03.qi226cons);
//					date1 = df.parse(seccion01_03.qi226cons);
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}		
//				Calendar cal = Calendar.getInstance();
//				Calendar cal1 = Calendar.getInstance();
//				cal.setTime(date2);
//				cal1.setTime(date1);
//				fecharefembarazo=cal;
//				embarazo=cal1;
//			}
//	  		embarazo.add(Calendar.MONTH, -seccion01_03.qi227);
//	  		Integer edad=MyUtil.CalcularEdadByFechaNacimiento(embarazo, fecharefembarazo);
//	  		Log.e("EDAD: "," "+edad);
//	  		Log.e("Mensaje","test fecharefEMBARAZO "+fecharefembarazo.get(Calendar.DAY_OF_MONTH)+"-"+fecharefembarazo.get(Calendar.MONTH)+"-"+fecharefembarazo.get(Calendar.YEAR));
//	  		Log.e("Mensaje","test fechaProbable "+embarazo.get(Calendar.DAY_OF_MONTH)+"-"+embarazo.get(Calendar.MONTH)+"-"+embarazo.get(Calendar.YEAR));
//			if(fecharefembarazo.before(embarazo)) {
//	  			mensaje = "VERIFICAR  \"Fecha de su primera relacion no puede ser menor al dia de su embarazo \"";
//	  			MyUtil.MensajeGeneral(getActivity(), mensaje);
//	    	}        		
//	  	} 
		
//		Log.e("detalletramos.size(): ",""+detalletramos.size());
		if(detalletramos.size()==0 ){
			mensaje = "Debe completar calendario";
			view = tctramoscolumna04;
			error = true;
			return false;
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	seccion01_03 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,SeccionescargadoSeccion01_03);
    	if(seccion01_03.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(seccion01_03.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
    	ninios = getCuestionarioService().getNacimientosparacalendario(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha_actual.get(Calendar.YEAR)-5, seccionesCargadoNinio);
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		} 
		
		seccion2 = getCuestionarioService().primerNacimientoCap2(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		seccion2t = getCuestionarioService().primeraTerminacionCap1_3(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		
		entityToUI(individual); 
		inicio(); 
		cargarTabla();
		if(ExisteEspaciosEnblanco()){
			btnIniciar.setEnabled(false);
		}
		else{
			btnIniciar.setEnabled(true);
		}
		
		Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in502 = individual.qi502==null?0:individual.qi502;
    	
    	if (in501!=3 && in502!=3 && !chbP509_y.isChecked()) {
			Integer anio= txtQI509Y.getValue()!=null?Integer.parseInt(txtQI509Y.getText().toString()):0;
	    	if(Util.esDiferente(anio, 0,9998)){
	    		Util.cleanAndLockView(getActivity(), txtQI510);
	    		q2.setVisibility(View.GONE);
	    	}
	    	else{
	    		Util.lockView(getActivity(), false,txtQI510);
	    		q2.setVisibility(View.VISIBLE);
	    	}
    	}
    	EndesCalendario.VerificacionCalendarioColumnas_01_03(individual.id,individual.hogar_id,individual.persona_id, fecha_actual);
	
    }
    /*FCP 31/03/2017 se comento el m�todo debido a que no se utiliza en este fragmento*/
    /* 
    public void GrabarCalendario(){
    	CICALENDARIO_TRAMO_COL4 tramo = null;
    	CICALENDARIO_TRAMO_COL4 tramodato = null;
    	tramo = getCuestionarioService().getTramoCasoUnion(individual.id, individual.hogar_id, individual.persona_id, App.ACTUALMENTE_EN_UNION, seccionesCargadoTramosCalendario);
    	if(tramo==null){
	    	if(!Util.esDiferente(individual.qi501, 1,2)){
	    		tramodato = getCuestionarioService().getTramoCasoUnion(individual.id, individual.hogar_id, individual.persona_id, App.ACTUALMENTE_NO_EN_UNION, seccionesCargadoTramosCalendario);
	    		if(tramodato!=null){
	    			getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, tramodato.id, tramodato.hogar_id, tramodato.persona_id, tramodato.qitramo_id);
	    		}
	    		tramo= new CICALENDARIO_TRAMO_COL4();
	    		tramo.id= individual.id;
	    		tramo.hogar_id =individual.hogar_id;
	    		tramo.persona_id = individual.persona_id;
	    		tramo.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
	    		tramo.qianio_ini = fecha_actual.get(Calendar.YEAR);
	    		tramo.qianio_fin = tramo.qianio_ini;
	    		tramo.qimes_ini = tramo.qimes_fin;
	    		tramo.qicantidad = 1;
	    		tramo.qiresult = "X";
	    		tramo.conviviente_id = App.ACTUALMENTE_EN_UNION;
	    		try {
					getCuestionarioService().saveOrUpdate(tramo ,null, seccionesGrabadoTramosCol04);
				} catch (Exception e) {
					// TODO: handle exception
				}
	    	}
    	}
    	tramo = null;
    	tramo = getCuestionarioService().getTramoCasoUnion(individual.id, individual.hogar_id, individual.persona_id, App.ACTUALMENTE_NO_EN_UNION, seccionesCargadoTramosCalendario);
    	if(tramo==null){
	    	if(!Util.esDiferente(individual.qi501, 3) && Util.esDiferente(individual.qi502, 3)){
	    		tramodato = getCuestionarioService().getTramoCasoUnion(individual.id, individual.hogar_id, individual.persona_id, App.ACTUALMENTE_EN_UNION, seccionesCargadoTramosCalendario);
	    		if(tramodato!=null){
	    			getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, tramodato.id, tramodato.hogar_id, tramodato.persona_id, tramodato.qitramo_id);
	    		}
	    		tramo = new CICALENDARIO_TRAMO_COL4();
	    		tramo.id = individual.id;
	    		tramo.hogar_id = individual.hogar_id;
	    		tramo.persona_id = individual.persona_id;
	    		tramo.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
	    		tramo.qianio_ini = fecha_actual.get(Calendar.YEAR);
	    		tramo.qianio_fin = tramo.qianio_ini;
	    		tramo.qimes_ini = tramo.qimes_fin;
	    		tramo.qicantidad = 1;
	    		tramo.qiresult = "0";
	    		tramo.conviviente_id = App.ACTUALMENTE_NO_EN_UNION;
	    		try {
					getCuestionarioService().saveOrUpdate(tramo ,null, seccionesGrabadoTramosCol04);
				} catch (Exception e) {
					// TODO: handle exception
				}
	    	}
    	}
    }*/
    public void cargarTabla(){
    	detalletramos = getCuestionarioService().getTramoscol04ByPersona(individual.id,individual.hogar_id,individual.persona_id, seccionesCargadoTramosCalendario);
    	Collections.reverse(detalletramos);
    	tctramoscolumna04.setData(detalletramos, "getfechaInicio","getFechaFinal","qiresult","qicantidad");
    	registerForContextMenu(tctramoscolumna04.getListView());
    }
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tctramoscolumna04.getListView())) {
			menu.setHeaderTitle("Opciones del Calendario");
			menu.add(1, 0, 1, "Editar tramo");
			menu.add(1, 1, 1, "Eliminar tramo");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			CICALENDARIO_TRAMO_COL4 seleccion = (CICALENDARIO_TRAMO_COL4) info.targetView.getTag();
			menu.getItem(0).setVisible(false);
			Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		menu.getItem(1).setVisible(false);
	    	}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		CICALENDARIO_TRAMO_COL4 seleccion = (CICALENDARIO_TRAMO_COL4) info.targetView.getTag();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				AbrirPreguntas((CICALENDARIO_TRAMO_COL4) detalletramos.get(info.position));
				break;
			case 1:
				action=ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea eliminar?");
				dialog.put("seleccion", (CICALENDARIO_TRAMO_COL4) detalletramos.get(info.position));
				dialog.showDialog();
				break;
			}
		}
		return super.onContextItemSelected(item);
	}
	private class CalendarioClickListener implements OnItemClickListener {
		public CalendarioClickListener() {
		}
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CICALENDARIO_TRAMO_COL4 c = (CICALENDARIO_TRAMO_COL4) detalletramos.get(arg2);
			Integer indiceCol1=MyUtil.DeterminarIndice(fecha_actual.get(Calendar.YEAR), fecha_actual.get(Calendar.MONTH)+1);
			String col1="";
//			for(CICALENDARIO_TRAMO_COL4  item: detalletramos){
//				if(!Util.esDiferente(item.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(item.qimes_fin,fecha_actual.get(Calendar.MONTH)+1)){
//					col1=item.qir_final;
//					break;
//				}
//			}
//			Log.e("","RESULTADO FINAL: "+c.qir_final);
//			if(!"N".equals(c.qir_final) && !"E".equals(c.qir_final) && !"T".equals(c.qir_final) && !col1.equals(c.qir_final)){
//				abrirDetalle(c,ACTION_CALENDARIO.EDITAR);
//			}
		}
	}
    private void ValidaPregunta500(){
    	ValidaTextoPregunta509();    	
    	Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in502 = individual.qi502==null?0:individual.qi502;
    	Integer in508 = individual.qi508==null?0:individual.qi508;
    	
    	if (in501==3 && in502==3) {
//    		Log.e(" 11"," in501==3 && in502==3  ->"+in501);
    		Util.cleanAndLockView(getActivity(),txtQI509M,txtQI509Y,txtQI510);
    		Util.lockView(getActivity(), false,txtQI512,rgQI512AB);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		btnIniciar.setVisibility(View.GONE);
    		onqi512ChangeValue();
		}
    	else{   
//    		Log.e(" 33"," else "+in508);
    		Util.lockView(getActivity(), false,txtQI509M,txtQI509Y,txtQI510,txtQI512,rgQI512AB);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		btnIniciar.setVisibility(View.VISIBLE);
    		
    		Integer anio= txtQI509Y.getValue()!=null?Integer.parseInt(txtQI509Y.getText().toString()):0;
        	if(Util.esDiferente(anio, 0,9998)){
        	if(((individual.qianio_cal!=null && !Util.esDiferente(individual.qianio_cal, anio)) || (individual.qi509y!=null && Util.esDiferente(individual.qi509y, anio)))&& ! EndesCalendario.CalendarioCompletado(individual.id,individual.hogar_id,individual.persona_id,fecha_actual)){
        		ConvivienteyunaSolapareja();
        		}
        		Util.cleanAndLockView(getActivity(), txtQI510);
        		q2.setVisibility(View.GONE);
        	}
        	else{
        		Util.lockView(getActivity(), false,txtQI510);
        		q2.setVisibility(View.VISIBLE);
        	}
    		
    		onqi512ChangeValue();
    		
    		if (individual.qi509m!=null && individual.qi509m.equals("98")) {
        		chbP509_m.setChecked(true);
        		Util.cleanAndLockView(getActivity(),txtQI509M);
        		txtQI509M.setVisibility(View.GONE);
    		}
        	if (individual.qi509y!=null && individual.qi509y==9998) {
        		chbP509_y.setChecked(true);
        		Util.cleanAndLockView(getActivity(),txtQI509Y);
        		txtQI509Y.setVisibility(View.GONE);
    		}
    	}
    	
    }
        
    public void onqi512TextoChangeValue() {
    	
        if(txtQI512.getText()!=null) { 
    	
        Integer i512 = Integer.parseInt(txtQI512.getText().toString().trim());
        	
          if(i512> 	App.getInstance().getPersonaCuestionarioIndividual().qi106) {
       	  
        	  ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que edad mef\"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
          } 
          
          Calendar fecharef = new GregorianCalendar();
          fecharef = new GregorianCalendar(seccion01_03.qi105y,Integer.parseInt(seccion01_03.qi105m)-1, Integer.parseInt(seccion01_03.qi105d));
          if(seccion01_03.qi208>0) {
        	if(seccion2!=null) {
        		Integer mesesEmbarazo = 0;
        		if(seccion2.qi220a!=null) {
        			mesesEmbarazo = seccion2.qi220a; 
        		}
        		Calendar fechaNacPrimer = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-(mesesEmbarazo+1), Integer.parseInt(seccion2.qi215d));
        		Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaNacPrimer);
        		if(Util.esMayor(i512, edadcalculada)) {
        			 ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que cuando tuvo primer nacimiento \"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
        		}        		
        	}          	  
          }
                    
          if(seccion01_03.qi230!=null && seccion01_03.qi234!=null) {          
	          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==2) {
	        	  Calendar fechaUltNac = new GregorianCalendar(seccion01_03.qi231_y, Integer.parseInt(seccion01_03.qi231_m)-(1+ seccion01_03.qi233), 1);
	        	  Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaUltNac);
	        		if(Util.esMayor(i512, edadcalculada)) {
	        			 ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que cuando fue su ultima terminaci�n \"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
	        		}
	          
	          }    
	          
	          if(seccion01_03.qi235a!=null) {
		          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==2) {
		        	  if(seccion2t!=null) {
			        	  Calendar fechaPrimTerm = new GregorianCalendar(seccion2t.qi235_y, seccion2t.qi235_m - 1 - seccion2t.qi235_d, 1);
			        	  Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaPrimTerm);
			        	  if(Util.esMayor(i512, edadcalculada)) {
			        			 ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que terminaci�n \"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
			        		}
		        	  }
		          } 		
		          if(seccion01_03.qi230 == 1 && seccion01_03.qi234==1 &&  seccion01_03.qi235a==1) {
		        	  Calendar fechaAnt2011 = new GregorianCalendar(seccion01_03.qi235b_y, Integer.parseInt(seccion01_03.qi235b_m)-1, 1);
		        	  Integer edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fecharef, fechaAnt2011);
		        		if(Util.esMayor(i512, edadcalculada)) {
		        			 ToastMessage.msgBox(getActivity(), "VERIFICAR  \"edad ingresada es mayor que cuando fue la terminaci�n anterior del "+App.ANIOPORDEFECTO+" \"", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);  
		        		}
		          }           
	          }
          }
        }
        txtQIOBS512.requestFocus();
    }
    
    public void onqi512ChangeValue(){
    	if (individual.qi512==null) {
    		Util.cleanAndLockView(getActivity(), rgQI512AB);
    		q7.setVisibility(View.GONE);
		}
    
  		if (App.getInstance().getPersonaCuestionarioIndividual().qi106>=25) {
			Log.e("sss","2");
			Util.cleanAndLockView(getActivity(), rgQI512AB);
    		q7.setVisibility(View.GONE);
		}
		else{
			if (individual.qi512!=null &&individual.qi512==0) {
				Log.e("sss","2");
    			Util.lockView(getActivity(),false, rgQI512AB);
        		q7.setVisibility(View.VISIBLE);
			}
			else{
				Log.e("sss","2");
    			Util.cleanAndLockView(getActivity(),rgQI512AB);
        		q7.setVisibility(View.GONE);
			}
		}
		
	 	if (individual.qi512!=null && individual.qi512==0) {
    		chbP512_1.setChecked(true);
    		chbP512_2.setChecked(false);      
    		Util.cleanAndLockView(getActivity(),txtQI512);
		}
    	if (individual.qi512!=null&&individual.qi512==95) {
			chbP512_1.setChecked(false);
			chbP512_2.setChecked(true);        
			Util.cleanAndLockView(getActivity(),txtQI512);
		}
        	
    	}
    
    
    public void ValidaTextoPregunta509(){
    	if (!Util.esDiferente(individual.qi508,2)) {
    		q1.setVisibility(View.VISIBLE);
			lblpregunta509_11.setVisibility(View.GONE);
			lblpregunta509_22.setVisibility(View.VISIBLE);
		}
    	else{
			q1.setVisibility(View.GONE);
			lblpregunta509_11.setVisibility(View.VISIBLE);
			lblpregunta509_22.setVisibility(View.GONE);
    	}
    }

    private void inicio() { 
    	ValidaPregunta500();
    	txtCabecera.requestFocus();
    	ValidarsiesSupervisora();
    	RenombrarEtiquetas();
    }
    public void RenombrarEtiquetas(){
    	lblmensaje511.setText(getResources().getString(R.string.c2seccion_05_07qi511));
    	lblmensaje511.setText(lblmensaje511.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    }
    
    public void onqi509ChangeValue(){
    	Integer anio= txtQI509Y.getValue()!=null?Integer.parseInt(txtQI509Y.getText().toString()):0;
    	if(Util.esDiferente(anio, 0,9998)){
    			ConvivienteyunaSolapareja();
    		Util.cleanAndLockView(getActivity(), txtQI510);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI510);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    
    public void ConvivienteyunaSolapareja(){
    	List<CICALENDARIO_TRAMO_COL4> todo = getCuestionarioService().getTramoscol04ByPersona(individual.id, individual.hogar_id, individual.persona_id, seccionesCargadoTramosCalendario);
    	 Integer tramo_id=-1;
		 Integer mes=-1;
		 Integer anio=-1;

		 mes = chbP509_m.isChecked()?2:Integer.parseInt(txtQI509M.getText().toString());
		 anio = Integer.parseInt(txtQI509Y.getText().toString());
    	if(todo.size()>0){
    		for(CICALENDARIO_TRAMO_COL4 tramo:todo){
    				getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, individual.id,individual.hogar_id, individual.persona_id, tramo.qitramo_id);
    		}
    		todo.clear();
    	}
		 CICALENDARIO_TRAMO_COL4 tramoagrabar= new CICALENDARIO_TRAMO_COL4();
		
		 if(!Util.esDiferente(individual.qi508, 1)){
			if(!Util.esDiferente(individual.qi501, 1,2)){
				if(anio>=App.ANIOPORDEFECTO){
					if((App.MESPORDEFECTO!=mes && anio==App.ANIOPORDEFECTO) || (anio>App.ANIOPORDEFECTO)){
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							tramoagrabar.qimes_fin = 12;
							tramoagrabar.qianio_fin =anio-1<App.ANIOPORDEFECTO?anio:anio-1;
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					}
					tramoagrabar = new CICALENDARIO_TRAMO_COL4();
					tramoagrabar.id=individual.id;
					tramoagrabar.hogar_id = individual.hogar_id;
					tramoagrabar.persona_id=individual.persona_id;
					tramoagrabar.qimes_ini=mes;
					tramoagrabar.qianio_ini = anio;
					tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
					tramoagrabar.qianio_fin = fecha_actual.get(Calendar.YEAR);
					tramoagrabar.qiresult="X";
					tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
					tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
					try {
						tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
					} catch (SQLException e) {
						ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
				}
				else{
					tramoagrabar = new CICALENDARIO_TRAMO_COL4();
					tramoagrabar.id=individual.id;
					tramoagrabar.hogar_id = individual.hogar_id;
					tramoagrabar.persona_id=individual.persona_id;
					tramoagrabar.qimes_ini=App.MESPORDEFECTO;
					tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
					tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
					tramoagrabar.qianio_fin = fecha_actual.get(Calendar.YEAR);
					tramoagrabar.qiresult="X";
					tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
					tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
					try {
						tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
					} catch (SQLException e) {
						ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
					
				}
			}
			else{
				if(!Util.esDiferente(individual.qi502, 1,2)){
					if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
						tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1<App.ANIOPORDEFECTO?anio:anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					}
				}
			}
		 }
		 
		 
		 /**** CASO 508 = 2 ***/
		 if(!Util.esDiferente(individual.qi508, 2)){
			 if(!Util.esDiferente(individual.qi501, 1,2)){
				 if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
						tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
				 }
				 if(anio<App.ANIOPORDEFECTO){
					 
				 }
			 }
			 if(!Util.esDiferente(individual.qi501, 3) && !Util.esDiferente(individual.qi502, 1,2)  ){
				 if(anio>=App.ANIOPORDEFECTO && ((!Util.esDiferente(anio, App.ANIOPORDEFECTO) && Util.esDiferente(mes, App.MESPORDEFECTO)) || Util.esDiferente(anio, App.ANIOPORDEFECTO))){
					 tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						if(mes-1==0){
							if(anio-1<App.ANIOPORDEFECTO){
								tramoagrabar.qianio_fin = anio;
								tramoagrabar.qimes_fin = 1;
							}
							else{
								tramoagrabar.qianio_fin = anio-1;
								tramoagrabar.qimes_fin = 12;
							}
						}else{
							tramoagrabar.qimes_fin=mes-1;
							tramoagrabar.qianio_fin = anio;
						}
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
				 }
			 }
		 }
		 cargarTabla();
		 if(ExisteEspaciosEnblanco()){
				btnIniciar.setEnabled(false);
			}
			else{
				btnIniciar.setEnabled(true);
			}
    }
     
    public void MostrarCalendario(){
    	FragmentManager fm = CISECCION_05Fragment_002.this.getFragmentManager();
    	CISECCION_03Fragment_003_2Dialog aperturaDialog = CISECCION_03Fragment_003_2Dialog.newInstance(this);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
    }
    
    public void AbrirPreguntas(CICALENDARIO_TRAMO_COL4 tramo){
    	if(tramo==null){
    		tramo= new CICALENDARIO_TRAMO_COL4();
    		tramo.id=App.getInstance().getPersonaCuestionarioIndividual().id;
    		tramo.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		tramo.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    		tramo.qitramo_id = detalletramos.size()+1;
    	}
    	FragmentManager fm = CISECCION_05Fragment_002.this.getFragmentManager();
    	CISECCION_05Fragment_002Dialog aperturaDialog = CISECCION_05Fragment_002Dialog.newInstance(this,tramo);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		CICALENDARIO_TRAMO_COL4 bean = (CICALENDARIO_TRAMO_COL4) dialog.get("seleccion");
		try {
			detalletramos.remove(bean);
			boolean borrado = false;
			borrado=getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, bean.id, bean.hogar_id, bean.persona_id, bean.qitramo_id);
			if(borrado &&  !ExisteEspaciosEnblanco()){
				btnIniciar.setEnabled(true);
			}
			else{
				btnIniciar.setEnabled(false);
			}
			if (!borrado) {
				throw new SQLException("Tramo no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	} 
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI512AB.readOnly();
			chbP509_m.readOnly();
			chbP509_y.readOnly();
			chbP512_1.readOnly();
			chbP512_2.readOnly();
			txtQI509M.readOnly();
			txtQI509Y.readOnly();
			txtQI510.readOnly();
			txtQI512.readOnly();
			btnIniciar.setEnabled(false);
			txtQIOBS512.setEnabled(false);
		}
	}
	 public boolean ExisteEspaciosEnblanco(){
		 boolean flag = getCuestionarioService().getCompletadoTramosCol4(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,fecha_actual);
		 boolean completado=false;
		 if(!flag){
			 completado = false;
		 }
		 else{
			 completado=true;
		 }
		 return completado;
	 }
	 
	 ///*METODOS DEL CALENDARIO*/
	 /*
	 public void RegistrarNacimientosPrimeroenTramos(){
		 Integer tramoinicial=-1;
	    	boolean multiple=true;
	    	Calendar fechadenacimientoanterior= null;
	    	Calendar fechadenacimientoactual = null;
	    	Integer id=null;
//	    	Log.e("","CANTIDAD: "+ninios.size());
	    	if(ninios.size()>0){
	    	fechadenacimientoanterior= new GregorianCalendar(ninios.get(0).qi215y,Integer.parseInt(ninios.get(0).qi215m)-1,Integer.parseInt(ninios.get(0).qi215d));
	    	id=ninios.get(0).qi212;
	    	for(CISECCION_02 ninio:ninios){
	    		Integer mes = Integer.parseInt(ninio.qi215m.toString());
	    		if(!Util.esDiferente(mes, fecha_actual.get(Calendar.MONTH)) && !Util.esDiferente(ninio.qi215y, fecha_actual.get(Calendar.YEAR))){
	    			tienenacimientoenfechaactual=true;
	    		}
	    		CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
	    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    		fechadenacimientoactual = new GregorianCalendar(ninio.qi215y,Integer.parseInt(ninio.qi215m.toString())-1,Integer.parseInt(ninio.qi215d.toString()));
	    		if(fechadenacimientoanterior.compareTo(fechadenacimientoactual)==0 && id!=ninio.qi212){
	    			fechadenacimientoanterior=fechadenacimientoactual;
	    			continue;
	    		}
	    		else{
	    			fechadenacimientoanterior=fechadenacimientoactual;
	    		}
	    		tramo.id=ninio.id;
	    		verificacion_tramo = getCuestionarioService().getTramoparaNacimientos(ninio.id, ninio.hogar_id, ninio.persona_id,ninio.qi212, seccionesCargadoTramosCol01_03);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(ninio.id,ninio.hogar_id,ninio.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
	    		tramo.hogar_id=ninio.hogar_id;
	    		tramo.persona_id=ninio.persona_id;
	    		tramo.ninio_id=ninio.qi212;
	    		tramo.terminacion_id=null;
	    		Integer indice=MyUtil.DeterminarIndice(ninio.qi215y, Integer.parseInt(ninio.qi215m.toString()))+ninio.qi220a;
	    		indice=indice-1;
	    		indice=indice>72?72:indice;
	    		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
	    		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
	    		tramo.qimes_fin=Integer.parseInt(ninio.qi215m.toString());
	    		tramo.qianio_fin=ninio.qi215y;
	    		tramo.qir_inicial=App.CALENDARIOEMBARAZO;
	    		tramo.qir_final=App.CALENDARIONACIMIENTO;
	    		tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)+1;
	    		try {
	    		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
				} catch (Exception e) {
					// TODO: handle exception
				}
	    	}
	      }
	    }
	 public void Registrarterminaciones(){
	    	Integer tramoinicial=-1;
	    	CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
 		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    	if(seccion01_03!=null &&  seccion01_03.qi230!=null && !Util.esDiferente(seccion01_03.qi230, 1) && seccion01_03.qi231_y!=null && seccion01_03.qi231_y>=App.ANIOPORDEFECTO){
	    		verificacion_tramo = getCuestionarioService().getTramoparaTerminaciones(seccion01_03.id, seccion01_03.hogar_id, seccion01_03.persona_id,App.TERMINACION_IDDEFAULT, seccionesCargadoTramosCol01_03);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(seccion01_03.id,seccion01_03.hogar_id,seccion01_03.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
	    	tramo.id=seccion01_03.id;
	    	tramo.hogar_id=seccion01_03.hogar_id;
	    	tramo.persona_id=seccion01_03.persona_id;
	    	Integer indice=MyUtil.DeterminarIndice(seccion01_03.qi231_y, Integer.parseInt(seccion01_03.qi231_m.toString()))+seccion01_03.qi233;
 		indice=indice-1;
 		indice=indice>72?72:indice;
 		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
 		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
 		tramo.qimes_fin=Integer.parseInt(seccion01_03.qi231_m.toString());
 		tramo.qianio_fin=seccion01_03.qi231_y;
	    	tramo.terminacion_id=App.TERMINACION_IDDEFAULT;	
	    	tramo.qir_inicial= seccion01_03.qi233>1?App.CALENDARIOEMBARAZO:App.CALENDARIOTERMINACION;
	    	tramo.qir_final=App.CALENDARIOTERMINACION;
	    	tramo.qicantidad=seccion01_03.qi233;
	    	try {
 		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
			} catch (Exception e) {
				// TODO: handle exception
			}    
	    }
	   }
	 public void RegistrarEmbarazoOMetodoAnticonceptivoONinguno(){
		 Log.e("","ENTRO ");
	    	Integer tramoinicial=-1;
	    	CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
 		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
 		CICALENDARIO_COL01_03 verificarmetodoactual= new CICALENDARIO_COL01_03();
 		Integer mesactual=fecha_actual.get(Calendar.MONTH)+1;
 		Integer anioactual=fecha_actual.get(Calendar.YEAR);
 		if(seccion01_03!=null && seccion01_03.qi226!=null && !Util.esDiferente(seccion01_03.qi226, 1)){//embarazo
 			Log.e("","IF EMBARAZO");
 			verificacion_tramo = getCuestionarioService().getTramoparaEmbarazo(seccion01_03.id, seccion01_03.hogar_id, seccion01_03.persona_id,App.EMBARAZO_IDDEFAULT, seccionesCargadoTramosCol01_03);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(seccion01_03.id,seccion01_03.hogar_id,seccion01_03.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
 			tramo.id=seccion01_03.id;
 			tramo.hogar_id=seccion01_03.hogar_id;
 			tramo.persona_id=seccion01_03.persona_id;
 	    	Integer indice=MyUtil.DeterminarIndice(anioactual, mesactual)+seccion01_03.qi227;
     		indice=indice-1;
     		indice=indice>72?72:indice;
     		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
     		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
 			tramo.qimes_fin=mesactual;
 			tramo.qianio_fin=anioactual;
 			tramo.embarazo_id=App.EMBARAZO_IDDEFAULT;
 			tramo.qir_inicial=App.CALENDARIOEMBARAZO;
 			tramo.qir_final=App.CALENDARIOEMBARAZO;
 			tramo.qicantidad=seccion01_03.qi227;
 			try {
     		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
 			} catch (Exception e) {
 				// TODO: handle exception
 			}
 		}
 		else if(seccion01_03!=null && !tienenacimientoenfechaactual && (seccion01_03.qi310!=null && !Util.esDiferente(seccion01_03.qi310,1))||(seccion01_03.qi311_a!=0 && !Util.esDiferente(seccion01_03.qi311_a, 1))){//Metodo actual
 			Log.e("","ELSE  IF");
 			Integer mesmetodo =seccion01_03.qi316m==null?Integer.parseInt(seccion01_03.qi315m):Integer.parseInt(seccion01_03.qi316m);
	    		Integer aniometodo = seccion01_03.qi316y==null?seccion01_03.qi315y:seccion01_03.qi316y;
	    		if(aniometodo<App.ANIOPORDEFECTO){
	    			mesmetodo=App.MESPORDEFECTO;
	    			aniometodo=App.ANIOPORDEFECTO;
	    		}
	    		verificacion_tramo = getCuestionarioService().getTramoparaMetodoActual(seccion01_03.id, seccion01_03.hogar_id, seccion01_03.persona_id,App.METODO_IDDEFAULT, seccionesCargadoTramosCol01_03);
	    		boolean mesesigualalnacimiento=false;
	    		if(ninios.size()>0 && CodigoCorrecto().equals("1") || CodigoCorrecto().equals("2") || CodigoCorrecto().equals("5")){
		    		for(CISECCION_02 n: ninios){
		    			if(n.qi215m!=null){
		    			Integer mes= Integer.parseInt(n.qi215m.toString());
			    			if(!Util.esDiferente(mesmetodo, mes) && !Util.esDiferente(n.qi215y, aniometodo)){
			    				mesesigualalnacimiento=true;
			    				break;	
			    			}
		    			}
		    		}
	    		}
	    		if(mesesigualalnacimiento){
	    			mesmetodo=mesmetodo+1;
	    		}
	    		Integer cantidad=MyUtil.DeterminarIndice(aniometodo, mesmetodo)-MyUtil.DeterminarIndice(anioactual, mesactual);
 	    	Integer indice=MyUtil.DeterminarIndice(anioactual, mesactual)+cantidad;
	    		if(verificacion_tramo!=null && verificacion_tramo.qicol3==null ){ //&& verificacion_tramo.qimes_ini!=null && verificacion_tramo.qianio_ini!=null && (verificacion_tramo.qimes_ini+1)!=mesmetodo || verificacion_tramo.qianio_ini!=aniometodo){
	    			EliminacionDeTablas(seccion01_03.id,seccion01_03.hogar_id,seccion01_03.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			if(verificacion_tramo!=null){
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id!=null?verificacion_tramo.qitramo_id:null;
	    			tramo.qicol3 = verificacion_tramo.qicol3!=null?verificacion_tramo.qicol3:null;
	    			}
	    		}
 			tramo.id=seccion01_03.id;
 			tramo.hogar_id=seccion01_03.hogar_id;
 			tramo.persona_id=seccion01_03.persona_id;
     		indice=indice>72?72:indice;
     		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
     		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
 			tramo.qimes_fin=mesactual;
 			tramo.qianio_fin=anioactual;
 			tramo.metodo_id=App.METODO_IDDEFAULT;
 			tramo.qir_inicial=CodigoCorrecto();
 			tramo.qir_final=CodigoCorrecto();
 			if("1".equals(tramo.qir_final) || "2".equals(tramo.qir_final)){
 				tramo.qicol3=ObtenerCol3SiesEsterilizacion();
 			}
 			else{
 				tramo.qicol3=seccion01_03.qi_fant;
 				tramo.qicol3_o=seccion01_03.qi_fant_o;
 			}
 			tramo.qicantidad=cantidad+1;
 			try {
     		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
 			} catch (Exception e) {
 				// TODO: handle exception
 			}
 	    }
 		else{
 			Log.e("","ELSE ");
 			if(seccion01_03!=null && seccion01_03.qi226!=null && Util.esDiferente(seccion01_03.qi226, 1) && Util.esDiferente(seccion01_03.qi310,1) && !tienenacimientoenfechaactual){
 				lista= getCuestionarioService().getTramosdelcalendario(individual.id,individual.hogar_id,individual.persona_id,seccionesCargadoTramosCol01_03);
 				
 				tramo.id=seccion01_03.id;
     			tramo.hogar_id=seccion01_03.hogar_id;
     			tramo.persona_id=seccion01_03.persona_id;
     			verificacion_tramo = getCuestionarioService().getTramoparaNingunMetodo(seccion01_03.id, seccion01_03.hogar_id, seccion01_03.persona_id,App.NINGUNMETODO_IDDEFAULT, seccionesCargadoTramosCol01_03);
     			if(verificacion_tramo!=null){
 	    			EliminacionDeTablas(seccion01_03.id,seccion01_03.hogar_id,seccion01_03.persona_id,verificacion_tramo.qitramo_id);
 	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
 	    			tramoinicial=verificacion_tramo.qitramo_id;
 	    		}
 	    		else{
 	    			tramo.qitramo_id=null;
 	    		}
     			if(lista.size()==0){
     				tramo.qimes_ini=App.MESPORDEFECTO;
     				tramo.qianio_ini=App.ANIOPORDEFECTO;
     			}
     			else{
     				tramo.qimes_ini=mesactual;
             		tramo.qianio_ini=anioactual;	
     			}
     			tramo.qimes_fin=mesactual;
     			tramo.qianio_fin=anioactual;
     			tramo.ninguno_id=App.NINGUNMETODO_IDDEFAULT;
     			tramo.qir_inicial=App.CALENDARIONINGUNMETODO;
     			tramo.qir_final=App.CALENDARIONINGUNMETODO;
     			tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(anioactual, mesactual)+1;
     			try {
         		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
     			} catch (Exception e) {
     				// TODO: handle exception
     			}
 			}
 		}
	  }
	 public void TerminacionesSiesqueExistiera(){
	    	Integer tramoinicial=-1;
	    	if(seccion01_03!=null && seccion01_03.qi234!=null && !Util.esDiferente(seccion01_03.qi234,1)){
	    		List<CISECCION_02T> embarazoentermino= getCuestionarioService().getTerminacionesParaCalendario(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, App.ANIOPORDEFECTO, seccionesCargadoTerminaciones);
	    		if(embarazoentermino.size()>0){
	    			for(CISECCION_02T termino:embarazoentermino){
	    				CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
	    	    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    				verificacion_tramo = getCuestionarioService().getTramoparaTerminaciones(termino.id, termino.hogar_id, termino.persona_id,termino.terminacion_id, seccionesCargadoTramosCol01_03);
	    	    		if(verificacion_tramo!=null){
	    	    			EliminacionDeTablas(termino.id,termino.hogar_id,termino.persona_id,verificacion_tramo.qitramo_id);
	    	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    	    			tramoinicial=verificacion_tramo.qitramo_id;
	    	    		}
	    	    		else{
	    	    			tramo.qitramo_id=null;
	    	    		}
	    				tramo.id=termino.id;
	    		    	tramo.hogar_id=termino.hogar_id;
	    		    	tramo.persona_id=termino.persona_id;
	    		    	Integer indice=MyUtil.DeterminarIndice(termino.qi235_y, termino.qi235_m)+termino.qi235_d;
	    		    	indice=indice-1;
	    	    		indice=indice>72?72:indice;
	    	    		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
	    	    		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
	    	    		tramo.qimes_fin=termino.qi235_m;
	    	    		tramo.qianio_fin=termino.qi235_y;
	    		    	tramo.terminacion_id=termino.terminacion_id;	
	    		    	tramo.qir_inicial= termino.qi235_d>1?App.CALENDARIOEMBARAZO:App.CALENDARIOTERMINACION;
	    		    	tramo.qir_final=App.CALENDARIOTERMINACION;
	    		    	tramo.qicantidad=termino.qi235_d;
	    		    	try {
	    	    		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramosCol01_03);
	    				} catch (Exception e) {
	    					// TODO: handle exception
	    				}
	    			}
	    		}
	    	}
	 }
	 public String CodigoCorrecto(){
	    	if(seccion01_03.qi311_a!=null && !Util.esDiferente(seccion01_03.qi311_a,1))
	    		return "1";
	    	if(seccion01_03.qi311_b!=null && !Util.esDiferente(seccion01_03.qi311_b,1))
	    		return "2";
	    	if(seccion01_03.qi311_c!=null && !Util.esDiferente(seccion01_03.qi311_c,1))
	    		return "3";
	    	if(seccion01_03.qi311_d!=null && !Util.esDiferente(seccion01_03.qi311_d,1))
		    	return "4";
	    	if(seccion01_03.qi311_e!=null && !Util.esDiferente(seccion01_03.qi311_e,1))
		    	return "5";
	    	if(seccion01_03.qi311_f!=null && !Util.esDiferente(seccion01_03.qi311_f,1))
		    	return "6";
	    	if(seccion01_03.qi311_g!=null && !Util.esDiferente(seccion01_03.qi311_g,1))
		    	return "7";
	    	if(seccion01_03.qi311_h!=null && !Util.esDiferente(seccion01_03.qi311_h,1))
		    	return "8";
	    	if(seccion01_03.qi311_i!=null && !Util.esDiferente(seccion01_03.qi311_i,1))
		    	return "9";
	    	if(seccion01_03.qi311_j!=null && !Util.esDiferente(seccion01_03.qi311_j,1))
		    	return "J";
	    	if(seccion01_03.qi311_k!=null && !Util.esDiferente(seccion01_03.qi311_k,1))
		    	return "K";
	    	if(seccion01_03.qi311_l!=null && !Util.esDiferente(seccion01_03.qi311_l,1))
		    	return "L";
	    	if(seccion01_03.qi311_m!=null && !Util.esDiferente(seccion01_03.qi311_m,1))
		    	return "M";
	    	if(seccion01_03.qi311_x!=null && !Util.esDiferente(seccion01_03.qi311_x,1))
		    	return "X";
	    	else
		    	return "";
	    	
	    }
	 public String ObtenerCol3SiesEsterilizacion(){
	    	String retorna="";
	    	if(!Util.esDiferente(seccion01_03.qi312, 10)){
	    		retorna="1";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 11)){
	    		retorna="2";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 12)){
	    		retorna="5";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 13)){
	    		retorna="6";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 14)){
	    		retorna="7";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 15)){
	    		retorna="8";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 16)){
	    		retorna="A";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 20)){
	    		retorna="B";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 21)){
	    		retorna="D";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 31)){
	    		retorna="F";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 32)){
	    		retorna="H";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 96)){
	    		retorna="X";
	    	}
	    	if(!Util.esDiferente(seccion01_03.qi312, 98)){
	    		retorna="9";
	    	}
	    	return retorna;
	    }
	 public void EliminacionDeTablas(Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id){
			getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, id, hogar_id, persona_id, tramo_id);
		}*/
	 
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		if (chbP509_m.isChecked())
			individual.qi509m="98";
		if (chbP509_y.isChecked())
			individual.qi509y=9998;
		if (chbP512_1.isChecked())
			individual.qi512=0;
		if (chbP512_2.isChecked())
				individual.qi512=95;
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().qi512 = individual.qi512;
			App.getInstance().getCiseccion05_07().qi512ab = individual.qi512ab;
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return App.INDIVIDUAL; 
	}
	
	public boolean VerificacionDeDatos512(){
		Integer edadPrimeraunion=0;
		Integer mes=null;
		Calendar fecha_ingresada=null;
		Calendar fecha_nacimiento=null;
		boolean iserror=false; 
		 if(individual!=null && individual.qi509m!=null 
				 && individual.qi509y!=null
				 && !individual.qi509m.equals("98") 
				 && individual.qi509y!=9998 
				 && seccion01_03!= null 
				 && seccion01_03.qi105m!=null 
				 && seccion01_03.qi105y!=null  
				 && !seccion01_03.qi105m.equals("98") 
				 && seccion01_03.qi105y!=9998
				 && seccion01_03.qi106!=null){
			 fecha_nacimiento = new GregorianCalendar(seccion01_03.qi105y, Integer.parseInt(seccion01_03.qi105m)-1, 1);
			 fecha_ingresada = new GregorianCalendar(individual.qi509y, Integer.parseInt(individual.qi509m)-1, 1);
			 edadPrimeraunion=fecha_ingresada.get(Calendar.YEAR)-fecha_nacimiento.get(Calendar.YEAR);
			 mes=fecha_ingresada.get(Calendar.MONTH)-fecha_nacimiento.get(Calendar.MONTH);
			 if(mes<0){
				 edadPrimeraunion--;
			 }
		 }
		 else{
			 edadPrimeraunion=individual.qi510==null?120:individual.qi510;
		 }
//		 Log.e("ss",""+edadPrimeraunion);
		 if(individual.qi512!=null && individual.qi512!=0 && individual.qi512!=95 && individual.qi512>edadPrimeraunion){
			 iserror=true;
		 }
		return iserror;
	}
	
	
	public boolean VerificacionDeDatos509(){
		Integer edadPrimeraunion=0;
		Integer mes=null;
		Calendar fecha_ingresada=null;
		Calendar fecha_nacimiento=null;
		boolean iserror=false; 
		 if(individual!=null && individual.qi509m!=null 
				 && individual.qi509y!=null
				 && !individual.qi509m.equals("98") 
				 && individual.qi509y!=9998 
				 && seccion01_03!= null 
				 && seccion01_03.qi105m!=null 
				 && seccion01_03.qi105y!=null  
				 && !seccion01_03.qi105m.equals("98") 
				 && seccion01_03.qi105y!=9998
				 && seccion01_03.qi106!=null){
			 fecha_nacimiento = new GregorianCalendar(seccion01_03.qi105y, Integer.parseInt(seccion01_03.qi105m)-1, 1);
			 fecha_ingresada = new GregorianCalendar(individual.qi509y, Integer.parseInt(individual.qi509m)-1, 1);
			 edadPrimeraunion=fecha_ingresada.get(Calendar.YEAR)-fecha_nacimiento.get(Calendar.YEAR);
			 mes=fecha_ingresada.get(Calendar.MONTH)-fecha_nacimiento.get(Calendar.MONTH);
			 if(mes<0){
				 edadPrimeraunion--;
			 }
		 }
		 else{
			 edadPrimeraunion=individual.qi510==null?120:individual.qi510;
		 }
//		 Log.e("ss",""+edadPrimeraunion);
		 if(individual.qi512!=null && individual.qi512!=0 && individual.qi512!=95 && edadPrimeraunion<12){
			 iserror=true;
		 }
		return iserror;
	}
} 
