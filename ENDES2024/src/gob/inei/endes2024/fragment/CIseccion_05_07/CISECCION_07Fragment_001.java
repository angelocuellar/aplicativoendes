package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.IFormComponent;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.opengl.Visibility;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_07Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI702; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI703; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI704N; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI704Y; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI704G; 
	@FieldAnnotation(orderIndex=6) 
	public TextAreaField txtQI706A; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQI706_COD; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI707; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI708; 
	
	CISECCION_05_07 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta702,lblpregunta703,lblpregunta704,lblpregunta706_a,lblpregunta706_a1,lblpregunta706_b,lblpregunta706_b1,lblpregunta707,lblpregunta708,lbledad702,lblnivel,lblanio,lblgrado,lblpregunta704_ind;
	public TextField txtCabecera;
	public GridComponent2 gridPreguntas702,gridPreguntas704;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	String qi707_cons;
	public CISECCION_07Fragment_001() {} 
	public CISECCION_07Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI702","QI703","QI704N","QI704Y","QI704G","QI706A","QI706_COD","QI707","QI708","QI501","QI505","QI502","QI707_CONS","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI702","QI703","QI704N","QI704Y","QI704G","QI706A","QI706_COD","QI707","QI708")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07qi_titulo).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
	  	lbledad702 = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.c2seccion_05_07qi702_edad).textSize(16).centrar();
	  	txtQI702=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
	  	
		rgQI703=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi703_1,R.string.c2seccion_05_07qi703_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS703ChangeValue"); 
		
		lblnivel = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.c2seccion_05_07qi704n).textSize(16).centrar().negrita();	
		lblanio = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.c2seccion_05_07qi704y).textSize(16).centrar().negrita();
		lblgrado = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.c2seccion_05_07qi704g).textSize(16).centrar().negrita();
		rgQI704N=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi704n_0,R.string.c2seccion_05_07qi704n_1,R.string.c2seccion_05_07qi704n_2,R.string.c2seccion_05_07qi704n_3,R.string.c2seccion_05_07qi704n_4,R.string.c2seccion_05_07qi704n_5,R.string.c2seccion_05_07qi704n_6).size(350,380).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI704NChangeValue"); 
		rgQI704Y=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi704y_0,R.string.c2seccion_05_07qi704y_1,R.string.c2seccion_05_07qi704y_2,R.string.c2seccion_05_07qi704y_3,R.string.c2seccion_05_07qi704y_4,R.string.c2seccion_05_07qi704y_5,R.string.c2seccion_05_07qi704y_6,R.string.c2seccion_05_07qi704n_6).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI704YChangeValue");
		rgQI704G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi704g_1,R.string.c2seccion_05_07qi704g_2,R.string.c2seccion_05_07qi704g_3,R.string.c2seccion_05_07qi704g_4,R.string.c2seccion_05_07qi704g_5,R.string.c2seccion_05_07qi704g_6,R.string.c2seccion_05_07qi704n_6).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI704GChangeValue");
		
		txtQI706A = new TextAreaField(getActivity()).maxLength(500).size(100, 650).soloTextoNumero();
		rgQI707=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi707_1,R.string.c2seccion_05_07qi707_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS707ChangeValue");
		rgQI708=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi708_1,R.string.c2seccion_05_07qi708_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta702 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi702);
		lblpregunta703 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi703);
		lblpregunta704 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi704);
		lblpregunta704_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi704_ind);
		lblpregunta706_a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi706_a);
		lblpregunta706_a1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi706_a1);
		lblpregunta706_b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi706_b);
		lblpregunta706_b1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi706_b1);
		lblpregunta707 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi707);
		lblpregunta708 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi708);
		
		gridPreguntas702 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas702.addComponent(lbledad702);
		gridPreguntas702.addComponent(txtQI702);
		
		gridPreguntas704=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas704.addComponent(lblnivel);		
		gridPreguntas704.addComponent(lblanio);
		gridPreguntas704.addComponent(lblgrado);
		gridPreguntas704.addComponent(rgQI704N);
		gridPreguntas704.addComponent(rgQI704Y);	
		gridPreguntas704.addComponent(rgQI704G);
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta702,gridPreguntas702.component()); 
		q2 = createQuestionSection(lblpregunta703,rgQI703); 
		q3 = createQuestionSection(lblpregunta704,lblpregunta704_ind,gridPreguntas704.component()); 
		q4 = createQuestionSection(lblpregunta706_a,lblpregunta706_a1,lblpregunta706_b,lblpregunta706_b1,txtQI706A); 
		q5 = createQuestionSection(lblpregunta707,rgQI707);
		q6 = createQuestionSection(lblpregunta708,rgQI708); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
//		if(qi707_cons == null) {
//			individual.qi707_cons =  Util.getFechaActualToString();
//			}
//		else {
//			individual.qi707_cons = qi707_cons;
//			}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) { 
			App.getInstance().getCiseccion05_07().qi707=individual.qi707;
		}
		
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (!Util.esDiferente(individual.qi501,1,2)) {
			if (Util.esVacio(individual.qi702)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI702"); 
				view = txtQI702; 
				error = true; 
				return false; 
			}
			if (Util.esMenor(individual.qi702,15) || Util.esMayor(individual.qi702, 98)) { 
				mensaje = "Valor fuera de rango en la pregunta QI702"; 
				view = txtQI702; 
				error = true; 
				return false; 
			}
		}
		if (!Util.esDiferente(individual.qi505,1,2,3) || !Util.esDiferente(individual.qi501,1,2)) {
			if (Util.esVacio(individual.qi703)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI703"); 
				view = rgQI703; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi703,1)) {
				if (Util.esVacio(individual.qi704n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI704N"); 
					view = rgQI704N; 
					error = true; 
					return false; 
				} 
				if (Util.esMenor(individual.qi704n,7)) {
					if (Util.esVacio(individual.qi704y) && Util.esVacio(individual.qi704g)) { 
						if (Util.esVacio(individual.qi704y)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI704Y"); 
							view = rgQI704Y; 
							error = true; 
							return false; 
						}
					}
					if (Util.esVacio(individual.qi704g) && Util.esVacio(individual.qi704y)) { 
						if (Util.esVacio(individual.qi704g)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI704G"); 
							view = rgQI704G; 
							error = true; 
							return false; 
						} 
					}
				}
			}
			if (Util.esVacio(individual.qi706a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI706A"); 
				view = txtQI706A; 
				error = true; 
				return false; 
			} 
		}
		if (!Util.esDiferente(individual.qi502,3) || !Util.esDiferente(individual.qi505,1,2,3) || !Util.esDiferente(individual.qi501,1,2)) {
			if (Util.esVacio(individual.qi707)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI707"); 
				view = rgQI707; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi707,2)) {
				if (Util.esVacio(individual.qi708)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI708"); 
					view = rgQI708; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual); 
		qi707_cons= individual.qi707_cons;
		inicio(); 
    } 
    
    public void validarPregunta501() {
    	if (!Util.esDiferente(individual.qi501,1,2)) {
    		Log.e("1",""+"1");
    		Util.lockView(getActivity(), false,txtQI702,rgQI703,rgQI704N,rgQI704Y,rgQI704G,txtQI706A,rgQI707,rgQI708);
    		Util.cleanAndLockView(getActivity(),lblpregunta706_b,lblpregunta706_b1);
    		q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			txtQI706A.setVisibility(View.VISIBLE);
			lblpregunta706_a.setVisibility(View.VISIBLE);
			lblpregunta706_a1.setVisibility(View.VISIBLE);
			lblpregunta706_b.setVisibility(View.GONE);
			lblpregunta706_b1.setVisibility(View.GONE);
			onqrgQS703ChangeValue();
			
		}
    	if (!Util.esDiferente(individual.qi505,1,2,3)) {
    		Log.e("2",""+"2");
    		Util.lockView(getActivity(), false,rgQI703,rgQI704N,rgQI704Y,rgQI704G,lblpregunta706_b,lblpregunta706_b1,txtQI706A,rgQI707,rgQI708);
    		Util.cleanAndLockView(getActivity(), txtQI702,lblpregunta706_a,lblpregunta706_a1);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			txtQI706A.setVisibility(View.VISIBLE);
			lblpregunta706_b.setVisibility(View.VISIBLE);
			lblpregunta706_b1.setVisibility(View.VISIBLE);
			lblpregunta706_a.setVisibility(View.GONE);
			lblpregunta706_a1.setVisibility(View.GONE);
			onqrgQS703ChangeValue();
		}
    	if (!Util.esDiferente(individual.qi502,3)) {
    		Log.e("3",""+"3");
    		Util.lockView(getActivity(), false,rgQI707,rgQI708);
    		Util.cleanAndLockView(getActivity(), txtQI702,rgQI703,rgQI704N,rgQI704Y,rgQI704G,txtQI706A);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
		}
	}
    
    private void inicio() { 
    	validarPregunta501();
    	onqrgQS707ChangeValue();
    	RenombrarEtiqueta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void RenombrarEtiqueta(){
    	
    	lblpregunta707.text(R.string.c2seccion_05_07qi707);
    	
    	Calendar Fecha_Actual = new GregorianCalendar();    	
    	if(qi707_cons!=null){			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qi707_cons);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			Fecha_Actual=cal;			
		}
   		int dias=Fecha_Actual.get(Calendar.DAY_OF_WEEK);
   		String primero="";
   		String segundo="";
   		Calendar segunda_fecha;
   		if(dias==Fecha_Actual.SATURDAY && Fecha_Actual.get(Calendar.AM_PM) ==1){
   			primero=Fecha_Actual.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(Fecha_Actual.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(Fecha_Actual, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		else{
   			Calendar primera_fecha = MyUtil.PrimeraFecha(Fecha_Actual, dias);
   			primero=primera_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(primera_fecha.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(primera_fecha, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		
   		lblpregunta707.setText(lblpregunta707.getText().toString().replace("#",segundo));
   		lblpregunta707.setText(lblpregunta707.getText().toString().replace("$",primero));
    }

    public void onqrgQS703ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI703.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI704N,rgQI704Y,rgQI704G);
    		MyUtil.LiberarMemoria();
    		q3.setVisibility(View.GONE);
    		txtQI706A.requestFocus();
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI704N,rgQI704Y,rgQI704G); 
    		q3.setVisibility(View.VISIBLE);
    		onrgQI704NChangeValue();
    		rgQI704N.requestFocus(); 
    	}
    }
    public void onqrgQS707ChangeValue() {
    	if (MyUtil.incluyeRango(1,1,rgQI707.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI708);  		
    		q6.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI708); 
    		q6.setVisibility(View.VISIBLE);
    		rgQI708.requestFocus(); 
    	}
    }

	public void onrgQI704NChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQI704G.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1,1,rgQI704N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQI704G);			
				Util.lockView(getActivity(), false,rgQI704Y);
				rgQI704Y.lockButtons(true,1,2,3,4,5,6);
				rgQI704Y.requestFocus();
			}	
			if (MyUtil.incluyeRango(3,4,rgQI704N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQI704G);			
				Util.lockView(getActivity(), false,rgQI704Y);	
				rgQI704Y.lockButtons(true,0,6);
				rgQI704G.requestFocus();
			}
			if (MyUtil.incluyeRango(5,5,rgQI704N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQI704G);			
				Util.lockView(getActivity(), false,rgQI704Y);	
				rgQI704Y.lockButtons(true,0);
				rgQI704G.requestFocus();
			}
			if (MyUtil.incluyeRango(6,6,rgQI704N.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),rgQI704G);			
				Util.lockView(getActivity(), false,rgQI704Y);	
				rgQI704Y.lockButtons(true,0,3,4,5,6);
				rgQI704G.requestFocus();
			}
			if (MyUtil.incluyeRango(7,7,rgQI704N.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),rgQI704G,rgQI704Y);			
				txtQI706A.requestFocus();
			}
			if (MyUtil.incluyeRango(15,46,txtQI702.getText().toString())) {
				if (MyUtil.incluyeRango(2,2,rgQI704N.getTagSelected("").toString())) {				
					Util.cleanAndLockView(getActivity(),rgQI704Y);	
					Util.lockView(getActivity(), false,rgQI704G);				
					rgQI704G.requestFocus();
				}	
			}
			else{
				if (MyUtil.incluyeRango(2,2,rgQI704N.getTagSelected("").toString())) {		
					Util.lockView(getActivity(), false,rgQI704Y,rgQI704G);
					rgQI704Y.lockButtons(true,6);
					onrgQI704YChangeValue();
					onrgQI704GChangeValue();
					rgQI704G.requestFocus();
				}
			}			
		}
	}
	public void onrgQI704YChangeValue() {
		if (MyUtil.incluyeRango(47,97,txtQI702.getText().toString()) && MyUtil.incluyeRango(1,5,rgQI704Y.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI704N.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI704G);
			Util.lockView(getActivity(), false,rgQI704G);
		}				
	}
	
	public void onrgQI704GChangeValue() {
		if (MyUtil.incluyeRango(47,97,txtQI702.getText().toString()) && MyUtil.incluyeRango(1,6,rgQI704G.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI704N.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI704Y);
			Util.lockView(getActivity(), false,rgQI704Y);
			rgQI704Y.lockButtons(true,0,6);
		}				
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI703.readOnly();
			rgQI704G.readOnly();
			rgQI704N.readOnly();
			rgQI704Y.readOnly();
			rgQI707.readOnly();
			rgQI708.readOnly();
			txtQI702.readOnly();
			txtQI706A.setEnabled(false);
		}
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		if(qi707_cons == null) {
			individual.qi707_cons =  Util.getFechaActualToString();
			}
		else {
			individual.qi707_cons = qi707_cons;
			}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) { 
			App.getInstance().getCiseccion05_07().qi707=individual.qi707;
		}
		
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return App.INDIVIDUAL; 
	} 
} 
