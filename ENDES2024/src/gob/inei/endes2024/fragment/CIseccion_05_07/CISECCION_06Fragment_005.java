package gob.inei.endes2024.fragment.CIseccion_05_07;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_06Fragment_005 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI617;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI618_A;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI618_B;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI618_C;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI618_D;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI618_E;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI618_F;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI618_G;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI618_H;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI618_I;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI618_J;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI618_K;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI618_L;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI618_M;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI618_X;
	@FieldAnnotation(orderIndex=16)
	public TextField txtQI618_X_I;
	@FieldAnnotation(orderIndex=17)
	public RadioGroupOtherField rgQI619B;
	@FieldAnnotation(orderIndex=18)
	public RadioGroupOtherField rgQI620;
	@FieldAnnotation(orderIndex=19)
	public TextField txtQI620X;
	@FieldAnnotation(orderIndex=20)
	public RadioGroupOtherField rgQI621;
	@FieldAnnotation(orderIndex=21)
	public RadioGroupOtherField rgQI621A;
	@FieldAnnotation(orderIndex=22)
	public RadioGroupOtherField rgQI623;
	@FieldAnnotation(orderIndex=23)
	public RadioGroupOtherField rgQI624A;
	@FieldAnnotation(orderIndex=24)
	public RadioGroupOtherField rgQI624B;
	@FieldAnnotation(orderIndex=25)
	public RadioGroupOtherField rgQI624C;
	@FieldAnnotation(orderIndex=26)
	public RadioGroupOtherField rgQI624D;
	@FieldAnnotation(orderIndex=27)
	public RadioGroupOtherField rgQI624E;
	@FieldAnnotation(orderIndex=28)
	public RadioGroupOtherField rgQI624F;
	
	public TextField txtCabecera;
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta619B,lblpregunta620,lblpregunta621,lblpregunta621A,lblpregunta623,lblpregunta624,lblpregunta618,lblpregunta617;
	private LabelComponent lblblanco,lblP624_1,lblP624_2,lblP624_3,lblQI624A,lblQI624A2,lblQI624B,lblQI624B2,lblQI624C,lblQI624C2,lblQI624D,lblQI624E,lblQI624F,lblQI624D2;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01;
	GridComponent2 grid1,grid2,grid3,grid4,grid5;
	
	public CISECCION_06Fragment_005() {}
	public CISECCION_06Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI617","QI618_A","QI618_B","QI618_C","QI618_D","QI618_E","QI618_F","QI618_G","QI618_H","QI618_I","QI618_J","QI618_K","QI618_L","QI618_M","QI618_X","QI618_X_I","QI619B","QI620","QI620X","QI621","QI621A","QI623","QI624A","QI624B","QI624C","QI624D","QI624E","QI624F","QI501","QI617","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI617","QI618_A","QI618_B","QI618_C","QI618_D","QI618_E","QI618_F","QI618_G","QI618_H","QI618_I","QI618_J","QI618_K","QI618_L","QI618_M","QI618_X","QI618_X_I","QI619B","QI620","QI620X","QI621","QI621A","QI623","QI624A","QI624B","QI624C","QI624D","QI624E","QI624F")};
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI320","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_06).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta618 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi618);
		lblpregunta619B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi619b);
		lblpregunta620 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi620);
		lblpregunta621 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi621);
		lblpregunta621A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi621a);
		lblpregunta623 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi623);
		lblpregunta624 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi624);
		lblpregunta617 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi617);

		rgQI617=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi617_1,R.string.ciseccion_06qi617_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI617ChangeValue");
		chbQI618_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI618_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi618_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI618_XChangeValue");
		txtQI618_X_I=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);

		
		rgQI619B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi619b_1,R.string.ciseccion_06qi619b_2,R.string.ciseccion_06qi619b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI619BChangeValue");
		rgQI620=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi620_1,R.string.ciseccion_06qi620_2,R.string.ciseccion_06qi620_3,R.string.ciseccion_06qi620_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI620X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI620.agregarEspecifique(3,txtQI620X);
		rgQI621=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi621_1,R.string.ciseccion_06qi621_2,R.string.ciseccion_06qi621_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI621A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi621a_1,R.string.ciseccion_06qi621a_2,R.string.ciseccion_06qi621a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI621AChangeValue");
		rgQI623=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi623_1,R.string.ciseccion_06qi623_2,R.string.ciseccion_06qi623_3,R.string.ciseccion_06qi623_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);

		
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP624_1 = new LabelComponent(getActivity()).textSize(16).size(50, 99).text(R.string.ciseccion_06qi624_si).centrar();
		lblP624_2 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.ciseccion_06qi624_no).centrar();
		lblP624_3 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.ciseccion_06qi624_ns).centrar();

		lblQI624A=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pa);
		rgQI624A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
				
		lblQI624B=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pb);
		rgQI624B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblQI624C=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pc);
		rgQI624C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
				
		lblQI624D=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pd);
		rgQI624D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblQI624E=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pe);
		rgQI624E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblQI624F=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.ciseccion_06qi624_pf);
		rgQI624F=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi624_o1,R.string.ciseccion_06qi624_o2,R.string.ciseccion_06qi624_o3).size(altoComponente+10,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI624A);
		grid1.addComponent(rgQI624A);
		grid1.addComponent(lblQI624B);
		grid1.addComponent(rgQI624B);
		grid1.addComponent(lblQI624C);
		grid1.addComponent(rgQI624C);
		grid1.addComponent(lblQI624D);
		grid1.addComponent(rgQI624D);
		grid1.addComponent(lblQI624E);
		grid1.addComponent(rgQI624E);
		grid1.addComponent(lblQI624F);
		grid1.addComponent(rgQI624F);
    }
  
    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q8 = createQuestionSection(lblpregunta617,rgQI617);
		LinearLayout ly618 = new LinearLayout(getActivity());
		ly618.addView(chbQI618_X);
		ly618.addView(txtQI618_X_I);
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta618,chbQI618_A,chbQI618_B,chbQI618_C,chbQI618_D,chbQI618_E,chbQI618_F,chbQI618_G,chbQI618_H,chbQI618_I,chbQI618_J,chbQI618_K,chbQI618_L,chbQI618_M,ly618);
		q1 = createQuestionSection(lblpregunta619B,rgQI619B);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta620,rgQI620);
		q3 = createQuestionSection(lblpregunta621,rgQI621);
		q4= createQuestionSection(lblpregunta621A,rgQI621A);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta623,rgQI623);
		q6 = createQuestionSection(lblpregunta624,grid1.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q8);
		form.addView(q7);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
    return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi619b!=null)
			individual.qi619b=individual.getConvertQi619B(individual.qi619b);
		if (individual.qi620!=null)
			individual.qi620=individual.getConvertQi620(individual.qi620);
		if (individual.qi621!=null)
			individual.qi621=individual.getConvertQi621(individual.qi621);
		if (individual.qi623!=null)
			individual.qi623=individual.getConvertQi623(individual.qi623);
		if (individual.qi624a!=null)
			individual.qi624a=individual.getConvertQi624A(individual.qi624a);
		if (individual.qi624b!=null)
			individual.qi624b=individual.getConvertQi624B(individual.qi624b);
		if (individual.qi624c!=null)
			individual.qi624c=individual.getConvertQi624C(individual.qi624c);
		if (individual.qi624d!=null)
			individual.qi624d=individual.getConvertQi624D(individual.qi624d);
		
		if (individual.qi624e!=null)
			individual.qi624e=individual.getConvertQi624D(individual.qi624e);
		if (individual.qi624f!=null)
			individual.qi624f=individual.getConvertQi624D(individual.qi624f);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
    	if(!isInRange()) return false;
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
    	Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in320 = individuals01.qi320==null?0:individuals01.qi320;
    	Integer in617 = individual.qi617==null?0:individual.qi617;
    	Integer in311A = individuals01.qi311_a==null?0:individuals01.qi311_a;
    	Integer in311B = individuals01.qi311_b==null?0:individuals01.qi311_b;
    	Integer in311C = individuals01.qi311_c==null?0:individuals01.qi311_c;
    	Integer in311D = individuals01.qi311_d==null?0:individuals01.qi311_d;
    	Integer in311E = individuals01.qi311_e==null?0:individuals01.qi311_e;
    	Integer in311F = individuals01.qi311_f==null?0:individuals01.qi311_f;
    	Integer in311G = individuals01.qi311_g==null?0:individuals01.qi311_g;
    	Integer in311H = individuals01.qi311_h==null?0:individuals01.qi311_h;
    	Integer in311I = individuals01.qi311_i==null?0:individuals01.qi311_i;
    	Integer in311J = individuals01.qi311_j==null?0:individuals01.qi311_j;
    	Integer in311K = individuals01.qi311_k==null?0:individuals01.qi311_k;
    	Integer in311L = individuals01.qi311_l==null?0:individuals01.qi311_l;
    	Integer in311M = individuals01.qi311_m==null?0:individuals01.qi311_m;
    	Integer in311X = individuals01.qi311_x==null?0:individuals01.qi311_x;
    	
	    
    	if (Util.esVacio(individual.qi617)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI617");
			view = rgQI617;
			error = true;
			return false;
		}
    	
		if (in617==1) {
			if (!verificarCheck()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI618_A");
				view = chbQI618_A;
				error = true;
				return false;
			}
			if (chbQI618_X.isChecked()){
				if (Util.esVacio(individual.qi618_x_i)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI618_X_I");
					view = txtQI618_X_I;
					error = true;
					return false;
				}
			}
		}
    	//if (in501!=3 &&  (in320!=0 && in320!=2 && in320!=7 && in320!=12)) {	
		if (in501!=3 &&  in320!=0 && (in311A==1 || in311C==1 && in311D==1 || in311E==1 || in311F==1 || in311H==1 || in311I==1 || in311J==1 || in311K==1 || in311M==1 || in311X==1)) {
    		if (Util.esVacio(individual.qi619b)) {
    			MyUtil.MensajeGeneral(getActivity(), "la pregunta QI619B no puede estar vacia");
//				mensaje = preguntaVacia.replace("$", "La pregunta QI619B");
//				view = rgQI619B;
//				error = true;
//				return false;
			}
    	}
    	
    	if (in501!=3 && in320!=0) {
    		if (Util.esDiferente(individual.qi619b,2,8)) {
				if (Util.esVacio(individual.qi620)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI620");
					view = rgQI620;
					error = true;
					return false;
				}
				if (individual.qi620==6) {
					if (Util.esVacio(individual.qi620x)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI620X");
						view = txtQI620X;
						error = true;
						return false;
					}
				}
			}	
    	}

		if (in501!=3) {
			if (Util.esVacio(individual.qi621)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI621");
				view = rgQI621;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi621a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI621A");
				view = rgQI621A;
				error = true;
				return false;
			}			
			
	    	
	    	if (chbQI618_A.isChecked() && in617==1 && !Util.esDiferente(individual.qi621a,1) ) {
	    		mensaje = "Verificar la pregunta QI618";
				view = rgQI621A;
				error = true;
				return false;
			}
	    	
	    	if (!chbQI618_A.isChecked() && in617==1 && Util.esDiferente(individual.qi621a,1) ) {
	    		mensaje = "Verificar la pregunta QI618";
				view = rgQI621A;
				error = true;
				return false;
			}
	    	
	    	if (in617==2 && Util.esDiferente(individual.qi621a,1) ) {
	    		mensaje = "Verificar la pregunta QI617";
				view = rgQI621A;
				error = true;
				return false;
			}	    	
		}
	
		if (in501!=3 && (in320!=1 && in320!=2)) {
			if (Util.esVacio(individual.qi623)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI623");
				view = rgQI623;
				error = true;
				return false;
			}
		}		
		
		if (Util.esVacio(individual.qi624a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624A");
			view = rgQI624A;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi624b)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624B");
			view = rgQI624B;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi624c)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624C");
			view = rgQI624C;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi624d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624D");
			view = rgQI624D;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi624e)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624D");
			view = rgQI624E;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi624f)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI624D");
			view = rgQI624F;
			error = true;
			return false;
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
//    	Log.e("320","320"+individuals01.qi320);
    	
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi619b!=null)
			individual.qi619b=individual.setConvertQi619B(individual.qi619b);
		if(individual.qi620!=null)
			individual.qi620=individual.setConvertQi620(individual.qi620);
		if(individual.qi621!=null)
			individual.qi621=individual.setConvertQi621(individual.qi621);
		if(individual.qi623!=null)
			individual.qi623=individual.setConvertQi623(individual.qi623);
		if(individual.qi624a!=null)
			individual.qi624a=individual.setConvertQi624A(individual.qi624a);
		if(individual.qi624b!=null)
			individual.qi624b=individual.setConvertQi624B(individual.qi624b);
		if(individual.qi624c!=null)
			individual.qi624c=individual.setConvertQi624C(individual.qi624c);
		if(individual.qi624d!=null)
			individual.qi624d=individual.setConvertQi624D(individual.qi624d);
		
		if(individual.qi624e!=null)
			individual.qi624e=individual.setConvertQi624D(individual.qi624e);
		if(individual.qi624f!=null)
			individual.qi624f=individual.setConvertQi624D(individual.qi624f);
		
		entityToUI(individual);
		inicio();
    }
    
    
    public void  validarPregunta320(){
    	Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in320 = individuals01.qi320==null?0:individuals01.qi320;
    	Integer in311A = individuals01.qi311_a==null?0:individuals01.qi311_a;
    	Integer in311B = individuals01.qi311_b==null?0:individuals01.qi311_b;
    	Integer in311C = individuals01.qi311_c==null?0:individuals01.qi311_c;
    	Integer in311D = individuals01.qi311_d==null?0:individuals01.qi311_d;
    	Integer in311E = individuals01.qi311_e==null?0:individuals01.qi311_e;
    	Integer in311F = individuals01.qi311_f==null?0:individuals01.qi311_f;
    	Integer in311G = individuals01.qi311_g==null?0:individuals01.qi311_g;
    	Integer in311H = individuals01.qi311_h==null?0:individuals01.qi311_h;
    	Integer in311I = individuals01.qi311_i==null?0:individuals01.qi311_i;
    	Integer in311J = individuals01.qi311_j==null?0:individuals01.qi311_j;
    	Integer in311K = individuals01.qi311_k==null?0:individuals01.qi311_k;
    	Integer in311L = individuals01.qi311_l==null?0:individuals01.qi311_l;
    	Integer in311M = individuals01.qi311_m==null?0:individuals01.qi311_m;
    	Integer in311X = individuals01.qi311_x==null?0:individuals01.qi311_x;
    	
    	
    	if (in501==3) {
//    		Log.e("caso 1",""+"in501==3 ");
    		Util.cleanAndLockView(getActivity(),rgQI619B,rgQI620,txtQI620X,rgQI621,rgQI621A,rgQI623);
    		Util.lockView(getActivity(), false,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.VISIBLE);
		}
    	else{
    		if (in320==0) {
//    			Log.e("caso 2",""+"in320==0"+in320);
    			Util.cleanAndLockView(getActivity(),rgQI619B,rgQI620,txtQI620X);
        		Util.lockView(getActivity(), false,rgQI621,rgQI621A,rgQI623,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
    			q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			q5.setVisibility(View.VISIBLE);
    			q6.setVisibility(View.VISIBLE);
			}
    		else{
    			if ((in311B==1 || in311G==1 || in311L==1) && (in311A!=1 && in311C!=1 && in311D!=1 && in311E!=1 && in311F!=1 && in311H!=1 && in311I!=1 && in311J!=1 && in311K!=1 && in311M!=1 && in311X!=1)) {    		
        			if (in311B==1) {
//        				Log.e("caso 3",""+"in320==2"+in320);
        				Util.cleanAndLockView(getActivity(),rgQI619B,rgQI623);
                		Util.lockView(getActivity(), false,rgQI620,rgQI621,rgQI621A,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
            			q1.setVisibility(View.GONE);
            			q2.setVisibility(View.VISIBLE);
            			q3.setVisibility(View.VISIBLE);
            			q4.setVisibility(View.VISIBLE);
            			q5.setVisibility(View.GONE);
            			q6.setVisibility(View.VISIBLE);
					}
        			else{
//        				Log.e("caso 4",""+"in320==7 || in320==12 "+in320);
        				Util.cleanAndLockView(getActivity(),rgQI619B);
                		Util.lockView(getActivity(), false,rgQI620,rgQI621,rgQI621A,rgQI623,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
            			q1.setVisibility(View.GONE);
            			q2.setVisibility(View.VISIBLE);
            			q3.setVisibility(View.VISIBLE);
            			q4.setVisibility(View.VISIBLE);
            			q5.setVisibility(View.VISIBLE);
            			q6.setVisibility(View.VISIBLE);
        			}
				}
    			else{
    				if (in311A==1 || in311B==1) {
//    					Log.e("caso 5",""+"in320==1"+in311A);
        				Util.lockView(getActivity(), false,rgQI619B,rgQI620,rgQI621,rgQI621A,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
        				Util.cleanAndLockView(getActivity(),rgQI623);                    		
            			q1.setVisibility(View.VISIBLE);
            			q2.setVisibility(View.VISIBLE);
            			q3.setVisibility(View.VISIBLE);
            			q4.setVisibility(View.VISIBLE);
            			q5.setVisibility(View.GONE);
            			q6.setVisibility(View.VISIBLE);
            			onrgQI619BChangeValue();
    				}
    				else{
//    					Log.e("caso 6",""+"otros  "+in320);
                		Util.lockView(getActivity(), false,rgQI619B,rgQI620,rgQI621,rgQI621A,rgQI623,rgQI624A,rgQI624B,rgQI624C,rgQI624D,rgQI624E,rgQI624F);
            			q1.setVisibility(View.VISIBLE);
            			q2.setVisibility(View.VISIBLE);
            			q3.setVisibility(View.VISIBLE);
            			q4.setVisibility(View.VISIBLE);
            			q5.setVisibility(View.VISIBLE);
            			q6.setVisibility(View.VISIBLE);
            			onrgQI619BChangeValue();
    				}
    			}
    		}
    	}
    }
    
    private void inicio() {
    	onrgQI617ChangeValue();
    	validarPregunta320();    	
		onchbQI618_XChangeValue();
		ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void onrgQI617ChangeValue() {
  		if (MyUtil.incluyeRango(2,2,rgQI617.getTagSelected("").toString())) {
  			Util.cleanAndLockView(getActivity(),chbQI618_A,chbQI618_B,chbQI618_C,chbQI618_D,chbQI618_E,chbQI618_F,chbQI618_G,chbQI618_H,chbQI618_I,chbQI618_J,chbQI618_K,chbQI618_L,chbQI618_M,chbQI618_X);
  			MyUtil.LiberarMemoria();
  			q7.setVisibility(View.GONE);
  		} else {
  			Util.lockView(getActivity(), false,chbQI618_A,chbQI618_B,chbQI618_C,chbQI618_D,chbQI618_E,chbQI618_F,chbQI618_G,chbQI618_H,chbQI618_I,chbQI618_J,chbQI618_K,chbQI618_L,chbQI618_M,chbQI618_X);
  			q7.setVisibility(View.VISIBLE);
  		}
    }

    public boolean verificarCheck() {
  		if (chbQI618_A.isChecked() || chbQI618_B.isChecked() || chbQI618_C.isChecked() || chbQI618_D.isChecked() || chbQI618_E.isChecked() || chbQI618_F.isChecked() || chbQI618_G.isChecked() || chbQI618_H.isChecked() || chbQI618_I.isChecked() || chbQI618_J.isChecked() || chbQI618_K.isChecked() || chbQI618_L.isChecked() || chbQI618_M.isChecked() || chbQI618_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void onchbQI618_XChangeValue() {
    	if (chbQI618_X.isChecked()){
    		Util.lockView(getActivity(),false,txtQI618_X_I);
  			txtQI618_X_I.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI618_X_I);
  		}
    }
    
    public void onrgQI621AChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQI621A.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI617.getTagSelected("").toString())   ) {
			ToastMessage.msgBox(this.getActivity(), "Validar con la pregunta 617", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return;
		} 
		if (MyUtil.incluyeRango(1,1,rgQI621A.getTagSelected("").toString()) && chbQI618_A.isChecked()) {
			ToastMessage.msgBox(this.getActivity(), "Validar con la pregunta 618", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return;
		} 
    }

    
    public void onrgQI619BChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQI619B.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI620);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			rgQI621.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI620);
			q2.setVisibility(View.VISIBLE);
			rgQI620.requestFocus();
		}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI617.readOnly();
    		rgQI619B.readOnly();
    		rgQI620.readOnly();
    		rgQI621.readOnly();
    		rgQI621A.readOnly();
    		rgQI623.readOnly();
    		rgQI624A.readOnly();
    		rgQI624B.readOnly();
    		rgQI624C.readOnly();
    		rgQI624D.readOnly();
    		rgQI624E.readOnly();
    		rgQI624F.readOnly();
    		txtQI618_X_I.readOnly();
    		txtQI620X.readOnly();
    		chbQI618_A.readOnly();
    		chbQI618_B.readOnly();
    		chbQI618_C.readOnly();
    		chbQI618_D.readOnly();
    		chbQI618_E.readOnly();
    		chbQI618_F.readOnly();
    		chbQI618_G.readOnly();
    		chbQI618_H.readOnly();
    		chbQI618_I.readOnly();
    		chbQI618_J.readOnly();
    		chbQI618_K.readOnly();
    		chbQI618_L.readOnly();
    		chbQI618_M.readOnly();
    		chbQI618_X.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
 
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi619b!=null)
			individual.qi619b=individual.getConvertQi619B(individual.qi619b);
		if (individual.qi620!=null)
			individual.qi620=individual.getConvertQi620(individual.qi620);
		if (individual.qi621!=null)
			individual.qi621=individual.getConvertQi621(individual.qi621);
		if (individual.qi623!=null)
			individual.qi623=individual.getConvertQi623(individual.qi623);
		if (individual.qi624a!=null)
			individual.qi624a=individual.getConvertQi624A(individual.qi624a);
		if (individual.qi624b!=null)
			individual.qi624b=individual.getConvertQi624B(individual.qi624b);
		if (individual.qi624c!=null)
			individual.qi624c=individual.getConvertQi624C(individual.qi624c);
		if (individual.qi624d!=null)
			individual.qi624d=individual.getConvertQi624D(individual.qi624d);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}