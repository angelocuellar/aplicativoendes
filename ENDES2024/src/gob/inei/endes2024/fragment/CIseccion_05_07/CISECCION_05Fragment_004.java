package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_05_07.Dialog.CISECCION_05Fragment_004Dialog;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_05;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_05Fragment_004 extends FragmentForm implements Respondible { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI513U; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI513N; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI521A; 
	@FieldAnnotation(orderIndex=4) 
	public TextAreaField txtQI521A_O;
	
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI512D_O; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI512D; 
	
	CISECCION_05 pareja;
	public TableComponent tcPersonas;
	public List<CISECCION_05> detalles;
	private DialogComponent dialog;
	Seccion01ClickListener adapter;
	CISECCION_05_07 individual;
	
	CISECCION_01_03 seccion01_03;
	CISECCION_02 seccion2;
	CISECCION_02T seccion2t;
	
	public CheckBoxField chbP512BA;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta512D,lblpregunta512D_1, lblpregunta513,lblpregunta513_ind,lblpregunta513A,lblobsqi521a,lblpregunta513_Ultima,lblpregunta513_PUltima,lblpregunta513_AUltima,lblEspacio_1,lblEspacio_2,lblpregunta521A,lblEspacio,lblnro; 
	private IntegerField txtQI513N1,txtQI513N2,txtQI513N3,txtQI513N4;
	private GridComponent2 gridPreguntas513,gridPreguntas521A, grid212D;
	public TextField txtCabecera;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 

	SeccionCapitulo[] seccionesGrabado,seccionesGrabado3; 
	SeccionCapitulo[] seccionesCargado,seccionesCargado2,seccionesCargado3,SeccionescargadoSeccion01_03; 
	private enum ACTION {
		ELIMINAR, DATA
	}
	private ACTION action;
	public CISECCION_05Fragment_004() {} 
	public CISECCION_05Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas(); 
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512D","QI512D_O","QI513U","QI513N","QI521A","QI521A_O","QI508","ID","HOGAR_ID","QI512","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512D","QI512D_O","QI513U","QI513N","QI521A","QI521A_O")};
		seccionesGrabado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI513BU","QI513BN","QI514","QI514A","QI515","QI515X","QI516U","QI516N","QI516B","QI516C","QI516D","QI516E","QI516F","QI517","QI521")};
		
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI513BU","QI513BN","QI514","QI514A","QI515","QI515X","QI516U","QI516N","QI516B","QI516C","QI516D","QI516E","QI516F","QI517","QI521","ID","HOGAR_ID","PERSONA_ID","PAREJA_ID")};
		seccionesCargado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PAREJA_ID","ID","HOGAR_ID","PERSONA_ID","ESTADO")};

		SeccionescargadoSeccion01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI105D","QI105M","QI105Y","QI105CONS","QI208","QI226","QI226CONS","QI227")};
		
		return rootView; 
  	} 
  
  	@Override 
  	protected void buildFields() {
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07).textSize(21).centrar().negrita(); 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  
	  txtQI513N1=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
	  txtQI513N2=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	  txtQI513N3=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
	  txtQI513N4=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
	  
	  rgQI513U=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi513u_1,R.string.c2seccion_05_07qi513u_2,R.string.c2seccion_05_07qi513u_3,R.string.c2seccion_05_07qi513u_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI513ChangeValue");
	  rgQI513U.agregarEspecifique(0,txtQI513N1);
	  rgQI513U.agregarEspecifique(1,txtQI513N2);
	  rgQI513U.agregarEspecifique(2,txtQI513N3);
	  rgQI513U.agregarEspecifique(3,txtQI513N4);
	  


	  txtQI521A_O = new TextAreaField(getActivity()).maxLength(1000).size(100, 750).alfanumerico();
	  txtQI521A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	  chbP512BA=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512ba_2, "1:0").size(60, 200);
	  chbP512BA.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked){
				MyUtil.LiberarMemoria();
	    		Util.cleanAndLockView(getActivity(),txtQI521A);
	    		txtQI521A_O.requestFocus();
	  		}else {
	  			MyUtil.LiberarMemoria();
				Util.lockView(getActivity(), false,txtQI521A);
				txtQI521A.setVisibility(View.VISIBLE);
				txtQI521A_O.requestFocus();
				}
		}
	  });
	  
	  lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblnro  = new LabelComponent(this.getActivity()).size(altoComponente, 270).text(R.string.c2seccion_05_07qi521a_1).textSize(16).centrar();
	  	
	  lblpregunta513A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi513a_ind);
	  lblpregunta513_Ultima = new LabelComponent(this.getActivity()).size(60, 350).textSize(16).text(R.string.c2seccion_05_07qi513atit_1).alinearIzquierda();
	  lblpregunta513_PUltima = new LabelComponent(this.getActivity()).size(60, 350).textSize(16).text(R.string.c2seccion_05_07qi513atit_2).alinearIzquierda();
	  lblpregunta513_AUltima = new LabelComponent(this.getActivity()).size(60, 350).textSize(16).text(R.string.c2seccion_05_07qi513atit_3).alinearIzquierda();
	  lblEspacio_1  = new LabelComponent(this.getActivity()).size(5, 550);
	  lblEspacio_2  = new LabelComponent(this.getActivity()).size(5, 550);
	  
	  lblpregunta513 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi513);
	  lblpregunta513_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi513_ind);
	  lblpregunta521A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi521a);
	  lblobsqi521a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi521a_obs);
	 
	  lblpregunta512D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi512d);
	  
	  //lblpregunta512D_1 = new LabelComponent(this.getActivity()).size(60, 300).text(R.string.c2seccion_05_07qi512d_1);
	  txtQI512D_O=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	  rgQI512D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi512d_1,R.string.c2seccion_05_07qi512d_2,R.string.c2seccion_05_07qi512d_8).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI512DChangeValue");
	  rgQI512D.agregarEspecifique(0, txtQI512D_O);
	  
//	  grid212D = new GridComponent2(this.getActivity(),Gravity.CENTER, 2, 0);
//	  grid212D.addComponent(lblpregunta512D_1);
//	  grid212D.addComponent(txtQI512D_O);
//	  grid212D.addComponent(rgQI512D,2);
	  
//	  Integer d=getResources().getDisplayMetrics().densityDpi;
//	  if(!Util.esDiferente(d,App.YOGA8)){
//		  tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(225, 600).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(325, 400).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		} 
	  
	  tcPersonas.addHeader(R.string.seccion01_nro_orden, 0.25f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.c2seccion_05_07qi513atit_0, 1f,TableComponent.ALIGN.LEFT);
	  
//	  gridPreguntas513 = new GridComponent2(this.getActivity(),App.ESTILO,2,4);
//	  gridPreguntas513.addComponent(rgQI513U,1,4);
//	  gridPreguntas513.addComponent(txtQI513N1);
//	  gridPreguntas513.addComponent(txtQI513N2);
//	  gridPreguntas513.addComponent(txtQI513N3);
//	  gridPreguntas513.addComponent(txtQI513N4);
//	  
	  
	  gridPreguntas521A = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
	  gridPreguntas521A.addComponent(lblnro);
	  gridPreguntas521A.addComponent(txtQI521A);
	  gridPreguntas521A.addComponent(lblEspacio);
	  gridPreguntas521A.addComponent(chbP512BA);
		
	} 
	
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta513,lblpregunta513_ind,rgQI513U); 
		q2 = createQuestionSection(lblpregunta513A,tcPersonas.getTableView());
		q3 = createQuestionSection(lblpregunta521A,gridPreguntas521A.component(),lblobsqi521a,txtQI521A_O);
		
		q4 = createQuestionSection(lblpregunta512D, rgQI512D);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q4);
		form.addView(q1);
		form.addView(q2); 
		form.addView(q3);

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual);
		
		if (individual.qi512d!=null)
			individual.qi512d=individual.getConvertQi512d(individual.qi512d);
		
		if (chbP512BA.isChecked())
			individual.qi521a=98;
		if (individual.qi513u!=null) {
			if (!Util.esDiferente(individual.qi513u,1) && txtQI513N1.getText().toString().trim().length()!=0 ) {
				individual.qi513n=Integer.parseInt(txtQI513N1.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,2) && txtQI513N2.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N2.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,3) && txtQI513N3.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N3.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,4) && txtQI513N4.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N4.getText().toString().trim());
			}
			else {
				individual.qi513n=null;
			}
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		BorrarParejasSiesQueexiste();
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		Integer in512 = individual.qi512==null?0:individual.qi512;
		
		if (Util.esVacio(individual.qi513u)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI513U"); 
			view = rgQI513U; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi513n)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI513N"); 
			view = txtQI513N; 
			error = true; 
			return false; 
		} 
		
		if(!Util.esDiferente(individual.qi513u,1) && (Util.esMenor(individual.qi513n, 0)|| Util.esMayor(individual.qi513n , 90))){
			mensaje = "valor fuera de rango solo permite de 0 a 90";
			view = txtQI513N;
			error = true;
			return false;
		}		
		
		if(!Util.esDiferente(individual.qi513u,2) && (Util.esMenor(individual.qi513n, 1)|| Util.esMayor(individual.qi513n , 30))){
			mensaje = "valor fuera de rango solo permite de 1 a 30";
			view = txtQI513N;
			error = true;
			return false;
		}
		if(!Util.esDiferente(individual.qi513u,3) && (Util.esMenor(individual.qi513n, 1)|| Util.esMayor(individual.qi513n , 11))){
			mensaje = "valor fuera de rango solo permite de 1 a 11";
			view = txtQI513N;
			error = true;
			return false;
		}
		if(!Util.esDiferente(individual.qi513u,4) && (Util.esMenor(individual.qi513n, 1)|| Util.esMayor(individual.qi513n , 40))){
			mensaje = "valor fuera de rango solo permite de 1 a edad de Mef - 10";
			view = txtQI513N;
			error = true;
			return false;
		}
					
		 Calendar fecharef = new GregorianCalendar();
	  			  		
			if(seccion01_03.qi105cons!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(seccion01_03.qi105cons);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}		  
	
			Integer tipo_periodo=0;
			Integer cantidad = 0;
			
			if(individual.qi513u == 1) {
				tipo_periodo = Calendar.DAY_OF_YEAR;
				cantidad=individual.qi513n;
			}
			else if(individual.qi513u == 2) {
				tipo_periodo = Calendar.DAY_OF_YEAR;
				cantidad=individual.qi513n*7;
				
			}
			else if(individual.qi513u == 3) {
				tipo_periodo = Calendar.MONTH;
				cantidad=individual.qi513n-1;
			}
			else if(individual.qi513u == 4) {
				tipo_periodo = Calendar.YEAR;
				cantidad=individual.qi513n;
			}
			
			fecharef.add(tipo_periodo, -(cantidad) );		
			
    if(seccion01_03.qi208>0) {
  	  
  	if(seccion2!=null) {
  		
  		Integer mesesEmbarazo = 0;
  		if(seccion2.qi220a!=null) {
  			mesesEmbarazo = seccion2.qi220a; 
  		}
  		
  		Calendar fechaNacUltimo = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1 - mesesEmbarazo, Integer.parseInt(seccion2.qi215d));
//  		Log.d("Mensaje","test meses "+mesesEmbarazo);
//  		Log.d("Mensaje","test fecharef "+fecharef.get(Calendar.DAY_OF_MONTH)+"-"+fecharef.get(Calendar.MONTH)+"-"+fecharef.get(Calendar.YEAR));
//  		Log.d("Mensaje","test fechaUltimoNac "+fechaNacUltimo.get(Calendar.DAY_OF_MONTH)+"-"+fechaNacUltimo.get(Calendar.MONTH)+"-"+fechaNacUltimo.get(Calendar.YEAR));

  		if(fecharef.before(fechaNacUltimo)) {
  			
  			mensaje = "VERIFICAR  \"Fecha de �ltima relaci�n sexual no puede ser menor que cuando tuvo ultimo embarazo \""; 
    			view = txtQI513N; 
    			error = true; 
    			return false; 
  			}        		
  		}          	  
    }
	if(seccion01_03.qi226!=null && seccion01_03.qi226==1){
		Calendar fecharefembarazo = new GregorianCalendar();
		Calendar embarazo= new GregorianCalendar();
	  		
		if(seccion01_03.qi226cons!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			Date date1 = null;
			try {
				date2 = df.parse(seccion01_03.qi226cons);
				date1 = df.parse(seccion01_03.qi226cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			Calendar cal1 = Calendar.getInstance();
			cal.setTime(date2);
			cal1.setTime(date1);
			fecharefembarazo=cal;
			embarazo=cal1;
		}
		fecharefembarazo.add(tipo_periodo, -(cantidad));
  		embarazo.add(Calendar.MONTH, -seccion01_03.qi227);

//  		Log.e("Mensaje","test fecharefEMBARAZO "+fecharefembarazo.get(Calendar.DAY_OF_MONTH)+"-"+fecharefembarazo.get(Calendar.MONTH)+"-"+fecharefembarazo.get(Calendar.YEAR));
//  		Log.e("Mensaje","test fechaProbable "+embarazo.get(Calendar.DAY_OF_MONTH)+"-"+embarazo.get(Calendar.MONTH)+"-"+embarazo.get(Calendar.YEAR));
		if(fecharefembarazo.before(embarazo)) {
  			mensaje = "VERIFICAR  \"Fecha de �ltima relaci�n sexual no puede ser menor al dia de su embarazo \""; 
    			view = txtQI513N; 
    			error = true; 
    			return false; 
  			}        		
  	}  
	
    
		if (Util.esVacio(individual.qi521a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI521A"); 
			view = txtQI521A; 
			error = true; 
			return false; 
		} 
		if (MyUtil.incluyeRango(99, 99, individual.qi521a) ) {
    		mensaje ="El N�. de compa�eros no puede ser mayor a 97";
			view = txtQI521A;
			error=true;
			return false;
		}
		if (!Util.esDiferente(individual.qi521a,0) ) {
    		mensaje ="El No. de compa�eros no puede ser 0";
			view = txtQI521A;
			error=true;
			return false;
		}
		
		if(detalles.size()>0 && Util.esMenor(individual.qi521a,detalles.size()) && Util.esDiferente(individual.qi521a, 98)){
			mensaje ="Como minimo puede tener: "+detalles.size()+" parejas";
			view = txtQI521A;
			error=true;
			return false;
		}
		
		if(Util.esMenor(individual.qi521a,2) && !Util.esDiferente(individual.qi508, 2) && individual.qi521a!=98 && individual.qi508!=null){
			MyUtil.MensajeGeneral(this.getActivity(), "Verifique pregunta QI508 tiene m�s de dos convivencias");
		}
		
		if(Util.esDiferente(individual.qi513u, 4) && !getCuestionarioService().getCompletadoSeccion05(individual.id,individual.hogar_id,individual.persona_id)){
			mensaje ="Debe Completar todos los registros de parejas";
			view = tcPersonas;
			error=true;
			return false;
		}
		
		if (Util.esVacio(individual.qi512d)) {
			mensaje = "Debe ingresar informaci�n en 512D";
			view = rgQI512D;
			error = true;
			return false;
		}
		
		if (!Util.esDiferente(individual.qi512d, 1)) {
			if (Util.esVacio(individual.qi512d_o)) {
				mensaje = "Debe ingresar informaci�n en especifique 512D";
				view = txtQI512D_O;
				error = true;
				return false;
			}
			
			if (!Util.esDiferente(individual.qi512d_o,"0") ) {
	    		mensaje ="Seleccionar c�digo 2";
				view = txtQI512D_O;
				error=true;
				return false;
			}
		}
		
		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	
//    	Log.e("333","in512==0 entroo");
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	seccion01_03 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,SeccionescargadoSeccion01_03);
    	seccion2 = getCuestionarioService().PrimerRegistroCap2(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	App.getInstance().getCiseccion05_07().qi512=individual.qi512;
    	
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		entityToUI(individual); 
		
		if(individual.qi512d!=null)
			individual.qi512d=individual.setConvertQi512d(individual.qi512d);
		
		if (individual.qi513n!=null) {	
    		if (!Util.esDiferente(individual.qi513u,1)) {
    			txtQI513N1.setText(individual.qi513n.toString());
    		}
    		if (!Util.esDiferente(individual.qi513u,2)) {
    			txtQI513N2.setText(individual.qi513n.toString());
    		}  
    		if (!Util.esDiferente(individual.qi513u,3)) {
    			txtQI513N3.setText(individual.qi513n.toString());
    		}   
    		if (!Util.esDiferente(individual.qi513u,4)) {
    			txtQI513N4.setText(individual.qi513n.toString());
    		}   
    	} 
		cargarTabla();
		if((detalles.size()==0 && individual.qi513u==null) ||(detalles.size()==0 && individual.qi513u!=null && !Util.esDiferente(individual.qi513u, 1,2,3))){
		agregarPareja(detalles.size()+1);
		}
		inicio(); 
    } 
    
    private void validarPregunta512(){     	
    	Integer in512 = individual.qi512==null?0:individual.qi512;    	
    	if(in512==0){
//    		Log.e("333","in512==0"+in512);
    		Util.cleanAndLockView(getActivity(),rgQI513U,txtQI513N,txtQI521A);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	}
    	else{
//    		Log.e("444","else edad");
    		Util.lockView(getActivity(), false,rgQI513U,txtQI513N,txtQI521A);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		onqrgQI513ChangeValue();
    	}
    }
    
    private void inicio() {
    	renombrarEtiquetas();
    	validarPregunta512();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
	public void onqrgQI513ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI513U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N2,txtQI513N3,txtQI513N4);
			individual.qi513n=null;
			Util.lockView(getActivity(),false,txtQI513N1);		
			if(detalles.size()==0){
				agregarPareja(detalles.size()+1);
			}
			txtQI513N1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI513U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N1,txtQI513N3,txtQI513N4);	
			individual.qi513n=null;
			Util.lockView(getActivity(),false,txtQI513N2);	
			if(detalles.size()==0){
				agregarPareja(detalles.size()+1);
			}
			txtQI513N2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI513U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N1,txtQI513N2,txtQI513N4);	
			individual.qi513n=null;
			Util.lockView(getActivity(),false,txtQI513N3);	
			if(detalles.size()==0){
				agregarPareja(detalles.size()+1);
			}
			txtQI513N3.requestFocus();				
		}
		else if (MyUtil.incluyeRango(4,4,rgQI513U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N1,txtQI513N2,txtQI513N3);	
			individual.qi513n=null;
			Util.lockView(getActivity(),false,txtQI513N4);			
			txtQI513N4.requestFocus();				
		}
		Integer valor=Integer.parseInt(rgQI513U.getTagSelected("0").toString());
		if(!Util.esDiferente(valor, 4)){
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),lblpregunta513A);
			q2.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,lblpregunta513A);
			q2.setVisibility(View.VISIBLE);
		}
		if (individual.qi521a!=null && individual.qi521a==98) {
			MyUtil.LiberarMemoria();
    		chbP512BA.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI521A);
    		txtQI521A.setVisibility(View.GONE);
		}
	}
	public void BorrarParejasSiesQueexiste(){
		if(!Util.esDiferente(individual.qi513u, 4)){
			detalles=null;
			getCuestionarioService().BorrarTodasLasparejas(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			cargarTabla();
//			if(detalles.size()>0){
//				action=action.DATA;
//				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Se eliminar� todas las parejas desea borrar todo?");
//				dialog.showDialog();
//		    }
		}
		
	}
	 @Override
		public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
			super.onCreateContextMenu(menu, v, menuInfo);
			if (v.equals(tcPersonas.getListView())) {
				menu.setHeaderTitle("Opciones");
				menu.add(1, 0, 1, "Editar Registro");
				menu.add(1, 1, 1, "Eliminar Registro");
				
				AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
				Integer posicion = 0;
				posicion = detalles.get(info.position).pareja_id;
				if (posicion > 1) {
					posicion = posicion - 1;
				}
				
				CISECCION_05 seleccion = (CISECCION_05) info.targetView.getTag();
				if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
					menu.getItem(0).setVisible(false);
					menu.getItem(1).setVisible(false);
				}
			}
	}
	 @Override
		public boolean onContextItemSelected(MenuItem item) {
			if (!getUserVisibleHint())
				return false;
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			if (item.getGroupId() == 1) {
				switch (item.getItemId()) {
				case 0:
					abrirDetalle((CISECCION_05) detalles.get(info.position),detalles.get(info.position).pareja_id);
					break;
				case 1:
					action=action.ELIMINAR;
					dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea eliminar?");
					dialog.put("seleccion", (CISECCION_05) detalles.get(info.position));
					dialog.showDialog();
					break;

				}
			}
			return super.onContextItemSelected(item);
	}
	private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_05 c = (CISECCION_05) detalles.get(arg2);
			abrirDetalle(c,c.pareja_id);
		}
	}
    public void agregarPareja(Integer indice) {
    	
    	if(detalles.size()<3) {
    		CISECCION_05 pareja = new CISECCION_05();
    		pareja.id = App.getInstance().getPersonaCuestionarioIndividual().id;
    		pareja.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		pareja.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    		if(indice==null)
    			pareja.pareja_id = detalles.size() + 1;
    		else 
    			pareja.pareja_id = indice;
    		boolean flag=false;
    		try {		
    			flag= getCuestionarioService().saveOrUpdate(pareja,seccionesGrabado3);
    			if(!flag){
    				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);} 
    		} catch (Exception e) { 
    			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    		}
    		cargarTabla();

    	}
		else {
			ToastMessage.msgBox(getActivity(), "No se puede agregar mas parejas", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
		}
		
	 }
	 public void cargarTabla() {
			detalles = getCuestionarioService().getParejasListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
			tcPersonas.setData(detalles, "pareja_id", "getOrdenPareja");
			for (int row = 0; row < detalles.size(); row++) {
				if (obtenerEstado(detalles.get(row)) == 1) {
					// borde de color azul
					tcPersonas.setBorderRow(row, true);
				} else if (obtenerEstado(detalles.get(row)) == 2) {
					// borde de color rojo
					tcPersonas.setBorderRow(row, true, R.color.red);
				} else {
					tcPersonas.setBorderRow(row, false);
				}
			}

			tcPersonas.reloadData();

			registerForContextMenu(tcPersonas.getListView());
	}
	 private int obtenerEstado(CISECCION_05 detalle) {
			if (!Util.esDiferente(detalle.estado, 0)) {
				return 1 ;
			} else if (!Util.esDiferente(detalle.estado,1)) {
				return 2;
			}
			return 0;
		}
	public void abrirDetalle(CISECCION_05 tmp, Integer pareja_id) {
		FragmentManager fm = CISECCION_05Fragment_004.this.getFragmentManager();
		CISECCION_05Fragment_004Dialog aperturaDialog = CISECCION_05Fragment_004Dialog.newInstance(this, tmp, pareja_id);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
   
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		if(action==action.DATA){
			rgQI513U.setTagSelected(individual.qi513u!=null?individual.qi513u:null);
			action=null;
		}
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		if (dialog == null) {
			return;
		}
		if(action==action.ELIMINAR){
		CISECCION_05 bean = (CISECCION_05) dialog.get("seleccion");
		try {
			detalles.remove(bean);
			boolean borrado = false;
			borrado = getCuestionarioService().BorrarPareja(bean.id, bean.hogar_id, bean.persona_id, bean.pareja_id);
			if (!borrado) {
				throw new SQLException("Pareja no pudo ser borrada.");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		action=null;
		}
		if(action==action.DATA){
			detalles.clear();
			getCuestionarioService().BorrarTodasLasparejas(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			action=null;
		}
		cargarTabla();
	} 
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI513U.readOnly();
			chbP512BA.readOnly();
			txtQI513N1.readOnly();
			txtQI513N2.readOnly();
			txtQI513N3.readOnly();
			txtQI513N4.readOnly();
			txtQI512D_O.readOnly();
			rgQI512D.readOnly();
			txtQI521A.readOnly();
			txtQI521A_O.setEnabled(false);
		}
	}
	
	
	public void renombrarEtiquetas(){
		Calendar fecharef = new GregorianCalendar();
    	
    	//ToastMessage.msgBox(this.getActivity(), fechareferencia.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG); 
    	
//    	if(fechareferencia!=null){
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fechareferencia);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fecharef=cal;
//		}
    	Calendar fechainicio = MyUtil.restarDias(fecharef,30);
    	Integer f1 =fechainicio.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio.get(Calendar.MONTH)) ;
    	
       	
    	lblpregunta512D.setText(lblpregunta512D.getText().toString().replace("#", f1.toString()+" de "+mes1));
	}
	
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (chbP512BA.isChecked())
			individual.qi521a=98;
		if (individual.qi513u!=null) {
			if (!Util.esDiferente(individual.qi513u,1) && txtQI513N1.getText().toString().trim().length()!=0 ) {
				individual.qi513n=Integer.parseInt(txtQI513N1.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,2) && txtQI513N2.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N2.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,3) && txtQI513N3.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N3.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513u,4) && txtQI513N4.getText().toString().trim().length()!=0) {
				individual.qi513n=Integer.parseInt(txtQI513N4.getText().toString().trim());
			}
			else {
				individual.qi513n=null;
			}
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
