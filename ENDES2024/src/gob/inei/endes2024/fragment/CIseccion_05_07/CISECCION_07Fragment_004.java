package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_07Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI715; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI716; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI717; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI718A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI718AA; 

	
	CISECCION_05_07 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta715,lblpregunta716,lblpregunta717,lblpregunta718A,lblpregunta718AA; 
	public TextField txtCabecera;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CISECCION_07Fragment_004() {} 
	public CISECCION_07Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI715","QI716","QI717","QI718A","QI718AA","QI709","QI501","QI707","QI708","QI708A","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI715","QI716","QI717","QI718A","QI718AA")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07qi_titulo).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI715=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi715_1,R.string.c2seccion_05_07qi715_2,R.string.c2seccion_05_07qi715_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI716=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi716_1,R.string.c2seccion_05_07qi716_2,R.string.c2seccion_05_07qi716_3,R.string.c2seccion_05_07qi716_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS716ChangeValue"); 
		rgQI717=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi717_1,R.string.c2seccion_05_07qi717_2,R.string.c2seccion_05_07qi717_3,R.string.c2seccion_05_07qi717_4,R.string.c2seccion_05_07qi717_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI718A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi718a_1,R.string.c2seccion_05_07qi718a_2,R.string.c2seccion_05_07qi718a_3,R.string.c2seccion_05_07qi718a_4,R.string.c2seccion_05_07qi718a_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS718AChangeValue"); 
		rgQI718AA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi718aa_1,R.string.c2seccion_05_07qi718aa_2,R.string.c2seccion_05_07qi718aa_3,R.string.c2seccion_05_07qi718aa_4,R.string.c2seccion_05_07qi718aa_5,R.string.c2seccion_05_07qi718aa_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		
		lblpregunta715 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi715);
		lblpregunta716 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi716);
		lblpregunta717 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi717);
		lblpregunta718A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi718a);
		lblpregunta718AA = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi718aa);
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta715,rgQI715); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta716,rgQI716); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta717,rgQI717); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta718A,rgQI718A); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta718AA,rgQI718AA); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
    return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esDiferente(individual.qi709,2)) {
			if (Util.esVacio(individual.qi715)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI715"); 
				view = rgQI715; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi716)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI716"); 
				view = rgQI716; 
				error = true; 
				return false; 
			}
		}
		if (!Util.esDiferente(individual.qi716,1,2)) {
			if (Util.esDiferente(individual.qi501,3) && (!Util.esDiferente(individual.qi707,1) || !Util.esDiferente(individual.qi708,1) || !Util.esDiferente(individual.qi708a,1))) {
				if (Util.esVacio(individual.qi717)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI717"); 
					view = rgQI717; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(individual.qi718a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI718A"); 
					view = rgQI718A; 
					error = true; 
					return false; 
				}
			}
		}
		if (Util.esDiferente(individual.qi501,3) && Util.esDiferente(individual.qi718a,4)) {
			if (Util.esVacio(individual.qi718aa)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI718AA"); 
				view = rgQI718AA; 
				error = true; 
				return false; 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getCiseccion05_07().qi501=individual.qi501;
    	App.getInstance().getCiseccion05_07().qi709=individual.qi709;
    	
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual); 
		inicio();  
    } 
    
    public void ValidaPregunta709(){
    	if (!Util.esDiferente(individual.qi709,2)) {
    		Log.e("","11");
    		Util.cleanAndLockView(getActivity(),rgQI715,rgQI716);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			ValidaPregunta709dd();
			onqrgQS716ChangeValue();
		}
    	else{
    		Log.e("","22");
    		Util.lockView(getActivity(),false,rgQI715,rgQI716);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		ValidaPregunta709dd();
    		onqrgQS716ChangeValue();
    	}
    }
    
    public void ValidaPregunta709dd(){
    	Integer in501 = individual.qi501==null?0:individual.qi501;
    	Integer in707 = individual.qi707==null?0:individual.qi707;
    	Integer in708 = individual.qi708==null?0:individual.qi708;
    	Integer in708a = individual.qi708a==null?0:individual.qi708a;
		if (in501==3) {
			Log.e("","33");
    		Util.cleanAndLockView(getActivity(),rgQI717,rgQI718A,rgQI718AA);  		
    		q0.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
		}
		else{
			q0.setVisibility(View.VISIBLE);
			if ((in501==1 || in501==2) && in707==2 && in708a==2 && in708a==2) {
				Log.e("","44");
				Util.cleanAndLockView(getActivity(),rgQI717,rgQI718A);  	
				Util.lockView(getActivity(),false,rgQI718AA);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		q5.setVisibility(View.VISIBLE);
			}
			else{
				Log.e("","55");
	    		Util.lockView(getActivity(),false,rgQI717,rgQI718A,rgQI718AA);
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
	    		onqrgQS718AChangeValue();
			}
		}
	  }
      

    
    
    private void inicio() { 
    	ValidaPregunta709();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void onqrgQS716ChangeValue() {
    	
      	if (MyUtil.incluyeRango(3,4,rgQI716.getTagSelected("").toString()) && !Util.esDiferente(individual.qi501,1,2)) {
    		Log.e("","1");
    		Util.cleanAndLockView(getActivity(),rgQI717,rgQI718A);  	
    		Util.lockView(getActivity(),false,rgQI718AA);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.VISIBLE);
    		rgQI718AA.requestFocus();
    	}
      	else{
      		ValidaPregunta709dd();
      	}
    }
    
    public void onqrgQS718AChangeValue() {
    	if (!MyUtil.incluyeRango(3,4,rgQI716.getTagSelected("").toString())) {
        	if (MyUtil.incluyeRango(4,4,rgQI718A.getTagSelected("").toString())) {
        		Log.e("","33");
        		Util.cleanAndLockView(getActivity(),rgQI718AA);  		
        		q5.setVisibility(View.GONE);
        	} 
        	else {
        		if(!MyUtil.incluyeRango(3,4,rgQI716.getTagSelected("").toString())  && rgQI717.getTagSelected()==null){
        			rgQI717.requestFocus();
        		}
        		else{
        			rgQI718AA.requestFocus();
        		}
        		Log.e("","44");
        		Util.lockView(getActivity(),false,rgQI718AA); 
        		q5.setVisibility(View.VISIBLE);
        	}
        	
		}
    	   	
    	
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI715.readOnly();
    		rgQI716.readOnly();
    		rgQI717.readOnly();
    		rgQI718A.readOnly();
    		rgQI718AA.readOnly();
    	}
    }
    
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
