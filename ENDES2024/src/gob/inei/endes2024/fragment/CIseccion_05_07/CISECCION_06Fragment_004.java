package gob.inei.endes2024.fragment.CIseccion_05_07;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_06Fragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI614;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQI614X;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI615A;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI615B;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQI615C;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI616AA;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI616AB;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI616AC;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI616AD;
	
	public TextField txtCabecera;
	CISECCION_05_07 individual;
	CISECCION_01_03 individuals01;
	public CheckBoxField chbP614_0,chbP614_96;
	
	private CuestionarioService cuestionarioService;
	public GridComponent2 gridPreguntas614,gridPreguntas615;
	public ButtonComponent btnNinguno,btnOtro;
	private LabelComponent lblTitulo,lblvacio,lblpregunta614_p1,lblpregunta614_p2,lblpregunta615,lblpregunta616A,lblnumero,lblninguno;
	private LabelComponent lblvacio1,lblvacio2,lblvacio3,lblvacio4,lblblanco,lblP624_1,lblP624_2,lblQI472aA,lblQI472aA2,lblQI472aB,lblQI472aB2,lblQI472aC,lblQI472aC2,lblQI472aD,lblQI472aD2,lblotrares,lblninias,lblninios,lblcualquiera,lblnumero1,lblpregunta614_ind;
	GridComponent2 grid1,grid2,grid3,grid4;
	LinearLayout q0,q1,q2,q3,q4,q5;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01;
	private String qi707_cons;
	public CISECCION_06Fragment_004() {}
	public CISECCION_06Fragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
  }
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI614","QI614X","QI615A","QI615B","QI615C","QI616AA","QI616AB","QI616AC","QI616AD","QI602","QI603U","QI603N","QI707_CONS","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI614","QI614X","QI615A","QI615B","QI615C","QI616AA","QI616AB","QI616AC","QI616AD","QI707_CONS")};
		seccionesCargadoCIS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI310","QI320","QI226","QI106")};
		
		
		return rootView;
  }
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_06).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta614_p1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi614_p1);
		lblpregunta614_p2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi614_p2);
		lblpregunta614_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_06qi614_ind).negrita();
		lblpregunta615 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi615);
		lblpregunta616A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_06qi616a);
		
		txtQI614=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqrgQS614ChangeValue");
		
		chbP614_0=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi614_0, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbP614_0.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),chbP614_96,txtQI614,txtQI614X,txtQI615A,txtQI615B,txtQI615C);
					q2.setVisibility(View.GONE);
		  		}
				else {
		  			Util.lockView(getActivity(), false,chbP614_96,txtQI614,txtQI615A,txtQI615B,txtQI615C);
		  			Util.cleanAndLockView(getActivity(),txtQI614X);
					q2.setVisibility(View.VISIBLE);
					txtQI614.setText("");
				}
			}
		});
		
		chbP614_96=new CheckBoxField(this.getActivity(), R.string.ciseccion_06qi614_96, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbP614_96.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
		    		Util.cleanAndLockView(getActivity(),chbP614_0,txtQI614,txtQI615A,txtQI615B,txtQI615C);
		    		Util.lockView(getActivity(), false,txtQI614X);
		    		MyUtil.LiberarMemoria();
					q2.setVisibility(View.GONE);
		  		}
				else {
		  			Util.lockView(getActivity(), false,chbP614_0,txtQI614,txtQI615A,txtQI615B,txtQI615C);
		  			Util.cleanAndLockView(getActivity(),txtQI614X);
		  			q2.setVisibility(View.VISIBLE);
		  			txtQI614.setText(null);
		  			txtQI614X.setText(null);
				}
			}
		});
		  
		txtQI614X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400).centrar();
		txtQI615A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI615B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI615C=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		/**********/
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP624_1 = new LabelComponent(getActivity()).textSize(16).size(50, 99).text(R.string.ciseccion_06qi616a_si).centrar();
		lblP624_2 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.ciseccion_06qi616a_no).centrar();

		lblQI472aA=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 370).text(R.string.ciseccion_06qi616a_pa);
		lblQI472aA2=new LabelComponent(getActivity()).textSize(17).size(55, 200).text(R.string.ciseccion_06qi616a_a);
		rgQI616AA=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi616a_o1,R.string.ciseccion_06qi616a_o2).size(altoComponente+10,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
				
		lblQI472aB=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 370).text(R.string.ciseccion_06qi616a_pb);
		lblQI472aB2=new LabelComponent(getActivity()).textSize(17).size(50, 200).text(R.string.ciseccion_06qi616a_b);
		rgQI616AB=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi616a_o1,R.string.ciseccion_06qi616a_o2).size(altoComponente+10,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblQI472aC=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 370).text(R.string.ciseccion_06qi616a_pc);
		lblQI472aC2=new LabelComponent(getActivity()).textSize(17).size(60, 200).text(R.string.ciseccion_06qi616a_c);
		rgQI616AC=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi616a_o1,R.string.ciseccion_06qi616a_o2).size(altoComponente+10,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblQI472aD=new LabelComponent(getActivity()).textSize(17).size(altoComponente+65, 370).text(R.string.ciseccion_06qi616a_pd);
		lblQI472aD2=new LabelComponent(getActivity()).textSize(17).size(60, 200).text(R.string.ciseccion_06qi616a_d);
		rgQI616AD=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_06qi616a_o1,R.string.ciseccion_06qi616a_o2).size(altoComponente+10,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI472aA);
		grid1.addComponent(rgQI616AA.centrar());
		grid1.addComponent(lblQI472aB);
		grid1.addComponent(rgQI616AB.centrar());
		grid1.addComponent(lblQI472aC);
		grid1.addComponent(rgQI616AC.centrar());
		grid1.addComponent(lblQI472aD);
		grid1.addComponent(rgQI616AD.centrar());

		lblninguno = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_06qi614nin).textSize(16);
		lblnumero  = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_06qi614n).textSize(16);
		lblotrares  = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_06qi614otr).textSize(16);
		
		lblvacio  = new LabelComponent(this.getActivity()).size(altoComponente, 250);
		lblninias  = new LabelComponent(this.getActivity()).size(altoComponente, 160).text(R.string.ciseccion_06qi615a).textSize(14).negrita();
		lblninios  = new LabelComponent(this.getActivity()).size(altoComponente, 160).text(R.string.ciseccion_06qi615b).textSize(14).negrita();
		lblcualquiera  = new LabelComponent(this.getActivity()).size(altoComponente, 180).text(R.string.ciseccion_06qi615c).textSize(14).negrita().centrar();
		lblnumero1  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_06qi615n).textSize(16).centrar();
		
		lblvacio1 = new LabelComponent(this.getActivity()).size(altoComponente, 40);
		lblvacio2 = new LabelComponent(this.getActivity()).size(altoComponente, 40);
		lblvacio3 = new LabelComponent(this.getActivity()).size(altoComponente, 20);
		
		gridPreguntas614 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas614.addComponent(lblninguno);
		gridPreguntas614.addComponent(chbP614_0);
		gridPreguntas614.addComponent(lblnumero);
		gridPreguntas614.addComponent(txtQI614);
		gridPreguntas614.addComponent(lblotrares);
		gridPreguntas614.addComponent(chbP614_96);
		gridPreguntas614.addComponent(txtQI614X,2);
		
		
		gridPreguntas615 = new GridComponent2(this.getActivity(),App.ESTILO,7,0);
		gridPreguntas615.addComponent(lblvacio);
		gridPreguntas615.addComponent(lblninias,2);
		gridPreguntas615.addComponent(lblninios,2);
		gridPreguntas615.addComponent(lblcualquiera,2);
		gridPreguntas615.addComponent(lblnumero1);
		gridPreguntas615.addComponent(txtQI615A);
		gridPreguntas615.addComponent(lblvacio1);
		gridPreguntas615.addComponent(txtQI615B);
		gridPreguntas615.addComponent(lblvacio2);
		gridPreguntas615.addComponent(txtQI615C);
		gridPreguntas615.addComponent(lblvacio3);
    }
  
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta614_p1,lblpregunta614_p2,lblpregunta614_ind,gridPreguntas614.component());
		q2 = createQuestionSection(lblpregunta615,gridPreguntas615.component());
		q3 = createQuestionSection(lblpregunta616A,grid1.component());

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
    return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if(qi707_cons == null) {
			individual.qi707_cons =  Util.getFechaActualToString();
			}
		else {
			individual.qi707_cons = qi707_cons;
			}
		if (chbP614_0.isChecked())
			individual.qi614=0;
		if (chbP614_96.isChecked())
				individual.qi614=96;
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		if(App.getInstance().getCiseccion05_07()!=null) 
			App.getInstance().getCiseccion05_07().qi614=individual.qi614;		
		else 
			App.getInstance().setCiseccion05_07(individual);
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		
		if (Util.esVacio(individual.qi614)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI614");
			view = txtQI614;
			error = true;
			return false;
		}
		if (individual.qi614==96) {
			if (Util.esVacio(individual.qi614x)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI614X");
				view = txtQI614X;
				error = true;
				return false;
			}
		}
		
		if (Util.esDiferente(individual.qi614, 0,96)) {
			if (Util.esVacio(individual.qi615a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI615A");
				view = txtQI615A;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi615b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI615B");
				view = txtQI615B;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi615c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI615C");
				view = txtQI615C;
				error = true;
				return false;
			}
			if(Util.esDiferente(individual.qi614, (individual.qi615a+individual.qi615b+individual.qi615c))){
				mensaje = "La cantidad de hijo(a)s deben ser iguales";
				view = txtQI614;
				error = true;
				return false;
			}
		}

		if (Util.esVacio(individual.qi616aa)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI616AA");
			view = rgQI616AA;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi616ab)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI616AB");
			view = rgQI616AB;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi616ac)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI616AC");
			view = rgQI616AC;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi616ad)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI616AD");
			view = rgQI616AD;
			error = true;
			return false;
		}
		
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals01 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoCIS01);
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individuals01.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi310=individuals01.qi310;
    	App.getInstance().getPersonaCuestionarioIndividual().qi226=individuals01.qi226;
    	App.getInstance().getPersonaCuestionarioIndividual().qi106=individuals01.qi106;
		App.getInstance().getCiseccion05_07().qi602=individual.qi602;
		App.getInstance().getCiseccion05_07().qi603u=individual.qi603u;
		App.getInstance().getCiseccion05_07().qi603n=individual.qi603n;
       	
    	if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual);
		qi707_cons=individual.qi707_cons;
		inicio();
    }
    
    public void lblPregunta614() {
//    	Log.e("total de hijos vivios",""+getCuestionarioService().getAlgunaHijaoHijoQueViveEnelHogar(individual.id, individual.hogar_id, individual.persona_id));
    	if (getCuestionarioService().getAlgunNacidoVivo(individual.id, individual.hogar_id, individual.persona_id)) {
			Log.e("ali111","ali111");
			lblpregunta614_p1.setVisibility(View.VISIBLE);
			lblpregunta614_p2.setVisibility(View.GONE);
		}
		else{
			Log.e("ali111","2222");
			lblpregunta614_p1.setVisibility(View.GONE);
			lblpregunta614_p2.setVisibility(View.VISIBLE);
		}
	}
    
    public void ValidarCheckP614() {
    	if (individual.qi614!=null) {
    	  	if (individual.qi614==0) {
        		chbP614_0.setChecked(true);
        		chbP614_96.setChecked(false);
        		Util.cleanAndLockView(getActivity(), txtQI614X,txtQI614);
    		}
        	if (individual.qi614==96) {
        		chbP614_0.setChecked(false);
    			chbP614_96.setChecked(true);
    			Util.cleanAndLockView(getActivity(), txtQI614);
    		}
        	else{
        		Util.cleanAndLockView(getActivity(), txtQI614X);
        	}
		}
	}
    
    private void inicio() {
    	lblPregunta614();
    	ValidarCheckP614();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
		txtCabecera.requestFocus();
    }
    
    public void onqrgQS614ChangeValue() {
    	if (MyUtil.incluyeRango(1,95,txtQI614.getText().toString())) {
    		Util.cleanAndLockView(getActivity(), txtQI614X);
    		txtQI615A.requestFocus();
  		} 
	}
    
    public void RenombrarEtiquetas() {
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar Fecha_Actual = new GregorianCalendar();    	
    	if(qi707_cons!=null){			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qi707_cons);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			Fecha_Actual=cal;			
		}
    	Calendar fechainicio = MyUtil.RestarMeses(Fecha_Actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(Fecha_Actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta616A.text(R.string.ciseccion_06qi616a);
    	lblpregunta616A.setText(lblpregunta616A.getText().toString().replace("#", F2));
    	lblpregunta616A.setText(lblpregunta616A.getText().toString().replace("$", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta616A.setText(lblpregunta616A.getText().toString().replace("%", F3)); 
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI616AA.readOnly();
    		rgQI616AB.readOnly();
    		rgQI616AC.readOnly();
    		rgQI616AD.readOnly();
    		chbP614_0.readOnly();
    		chbP614_96.readOnly();
    		txtQI614.readOnly();
    		txtQI614X.readOnly();
    		txtQI615A.readOnly();
    		txtQI615B.readOnly();
    		txtQI615C.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (chbP614_0.isChecked())
			individual.qi614=0;
		if (chbP614_96.isChecked())
				individual.qi614=96;
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		}catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		if(App.getInstance().getCiseccion05_07()!=null) 
			App.getInstance().getCiseccion05_07().qi614=individual.qi614;		
		else 
			App.getInstance().setCiseccion05_07(individual);
		return App.INDIVIDUAL;
	}
}