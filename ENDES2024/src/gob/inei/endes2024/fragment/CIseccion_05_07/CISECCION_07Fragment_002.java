package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_07Fragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI708A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI709; 
	@FieldAnnotation(orderIndex=3) 
	public TextAreaField txtQI710A; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI711; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI712; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI713; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI714; 
	
	CISECCION_05_07 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta708A,lblpregunta709,lblpregunta710,lblpregunta712,lblpregunta713,lblpregunta714,lblpregunta711;
	public TextField txtCabecera;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6;
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CISECCION_07Fragment_002() {} 
	public CISECCION_07Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  }
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI708A","QI709","QI710A","QI710_COD","QI711","QI712","QI713","QI714","QI707","QI708","QI709","QI501","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI708A","QI709","QI710A","QI710_COD","QI711","QI712","QI713","QI714")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07qi_titulo).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI708A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi708a_1,R.string.c2seccion_05_07qi708a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS708AChangeValue"); 
		rgQI709=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi709_1,R.string.c2seccion_05_07qi709_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS709ChangeValue"); 
		txtQI710A = new TextAreaField(getActivity()).maxLength(500).size(100, 650).soloTextoNumero();
//		txtQI710A.setCallback("OcultarTecla");
		rgQI711=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi711_1,R.string.c2seccion_05_07qi711_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS711ChangeValue");
		rgQI712=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi712_1,R.string.c2seccion_05_07qi712_2,R.string.c2seccion_05_07qi712_3,R.string.c2seccion_05_07qi712_4,R.string.c2seccion_05_07qi712_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI713=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi713_1,R.string.c2seccion_05_07qi713_2,R.string.c2seccion_05_07qi713_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI714=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi714_1,R.string.c2seccion_05_07qi714_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta708A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi708a);
		lblpregunta709 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi709);
		lblpregunta710 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi710);
		lblpregunta711 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_05_07qi711);
		lblpregunta712 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi712);
		lblpregunta713 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi713);
		lblpregunta714 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi714);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta708A,rgQI708A); 
		q2 = createQuestionSection(lblpregunta709,rgQI709); 
		q3 = createQuestionSection(lblpregunta710,txtQI710A);
		q4 = createQuestionSection(lblpregunta711,rgQI711); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta712,rgQI712); 
		q6 = createQuestionSection(lblpregunta713,rgQI713); 
		q7 = createQuestionSection(lblpregunta714,rgQI714);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6);
		form.addView(q7); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().filtro720= getCuestionarioService().VerificarFiltro720(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,6);
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (!Util.esDiferente(individual.qi707,2) && !Util.esDiferente(individual.qi708,2)) {
			if (Util.esVacio(individual.qi708a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI708A"); 
				view = rgQI708A; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi708a,2)) {
				if (Util.esVacio(individual.qi709)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI709"); 
					view = rgQI709; 
					error = true; 
					return false; 
				}
			}
		}
		if (Util.esDiferente(individual.qi709,2)) {
			if (Util.esVacio(individual.qi710a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI710A"); 
				view = txtQI710A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi711)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI711"); 
				view = rgQI711; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi711,1)) {
				if (Util.esVacio(individual.qi712)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI712"); 
					view = rgQI712; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(individual.qi713)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI713"); 
					view = rgQI713; 
					error = true; 
					return false; 
				}
			}
			else{
				if (Util.esVacio(individual.qi713)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI713"); 
					view = rgQI713; 
					error = true; 
					return false; 
				}
			}
			if (Util.esVacio(individual.qi714)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI714"); 
				view = rgQI714; 
				error = true; 
				return false; 
			} 
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getCiseccion05_07().qi501=individual.qi501;
    	App.getInstance().getCiseccion05_07().qi709=individual.qi709;
    	
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual); 
		inicio(); 
    } 
    
    public void ValidaPregunta707(){
    	if (!Util.esDiferente(individual.qi707,1) || !Util.esDiferente(individual.qi708,1)) {
    		Log.e("","1");
    		Util.cleanAndLockView(getActivity(),rgQI708A,rgQI709);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			onqrgQS711ChangeValue();
		}
    	else{
    		Log.e("","2");
    		Util.lockView(getActivity(),false,rgQI708A,rgQI709);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		onqrgQS708AChangeValue();
    		
    	}
    }
    
    private void inicio() {
    	ValidaPregunta707();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void onqrgQS708AChangeValue() {
    	if (MyUtil.incluyeRango(1,1,rgQI708A.getTagSelected("").toString())) {
    		Log.e("","3");
    		Util.cleanAndLockView(getActivity(),rgQI709);  		
    		q2.setVisibility(View.GONE);
    		onqrgQS709ChangeValue();
    		txtQI710A.requestFocus();
    	} 
    	else {
    		Log.e("","4");
    		Util.lockView(getActivity(),false,rgQI709); 
    		q2.setVisibility(View.VISIBLE);
    		onqrgQS709ChangeValue();
    		rgQI709.requestFocus(); 
    	}
    }
    
    public void onqrgQS709ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI709.getTagSelected("").toString())) {
    		Log.e("","4");
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),txtQI710A,rgQI711,rgQI712,rgQI713,rgQI714);  		
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		txtQI710A.requestFocus();
    	} 
    	else {
    		Log.e("","5");
    		Util.lockView(getActivity(),false,txtQI710A,rgQI711,rgQI712,rgQI713,rgQI714); 
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		onqrgQS711ChangeValue();
    		txtQI710A.requestFocus();
    	}
    }
    public void onqrgQS711ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI711.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Log.e("","6");
    		Util.cleanAndLockView(getActivity(),rgQI712);  		
    		q5.setVisibility(View.GONE);
    		rgQI713.requestFocus();
    	} 
    	else {
    		Log.e("sss","7");
    		Util.lockView(getActivity(),false,rgQI712); 
    		q5.setVisibility(View.VISIBLE);
    		rgQI712.requestFocus();
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI708A.readOnly();
    		rgQI709.readOnly();
    		rgQI711.readOnly();
    		rgQI712.readOnly();
    		rgQI713.readOnly();
    		rgQI714.readOnly();
    		txtQI710A.setEnabled(false);
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().filtro720= getCuestionarioService().VerificarFiltro720(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,6);
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		return App.NODEFINIDO; 
	} 
} 
