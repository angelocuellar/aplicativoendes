package gob.inei.endes2024.fragment.CIseccion_05_07.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_002;
import gob.inei.endes2024.model.CICALENDARIO_COL04;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;











































import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_05Fragment_002Dialog extends DialogFragmentComponent  {
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbSINUNION;
	@FieldAnnotation(orderIndex=2) 
	public SpinnerField spnQIMES_INI;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQIANIO_INI;
	@FieldAnnotation(orderIndex=4) 
	public SpinnerField spnQIMES_FIN;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQIANIO_FIN;
	@FieldAnnotation(orderIndex=6)
	public SpinnerField spnQICOL4;
	@FieldAnnotation(orderIndex=7)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=8)
	public ButtonComponent btnCancelar;
	
	public static CISECCION_05Fragment_002 caller;
	public GridComponent2 gridcalendario;
	public LabelComponent lblpregunta01,lblpregunta02;
	public SeccionCapitulo[] seccionesCargado,seccionesCargadoCalendario,seccionesCargadoTramosCalendario,seccionsecargados01_s03;
	public SeccionCapitulo[] seccionesgrabado,seccionesGrabadoCalendario,seccionesGrabadoTramosCol04;
	private LinearLayout q0;
	private LinearLayout q1;
	private LinearLayout q2;
	CISECCION_05_07 individual;
	private CICALENDARIO_TRAMO_COL4 tramocol4;
	private CISECCION_01_03 seccion_01_03;
	private CuestionarioService cuestionarioService;
	private Calendar fecha_actual = null;//Calendar.getInstance();
	public static CISECCION_05Fragment_002Dialog newInstance(FragmentForm pagina,CICALENDARIO_TRAMO_COL4 detalle) {
		caller=(CISECCION_05Fragment_002)(pagina);
		CISECCION_05Fragment_002Dialog f = new CISECCION_05Fragment_002Dialog();
        f.setParent(pagina);
        Bundle args = new Bundle();
    	args.putSerializable("detalle", detalle);
         f.setArguments(args);
        return f;	
	}
	
	  @Override 
	  public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
			tramocol4 = (CICALENDARIO_TRAMO_COL4) getArguments().getSerializable("detalle");
			caretaker = new Caretaker<Entity>();
		} 
	
	  @Override 
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		  	getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
			rootView = createUI(); 
			initObjectsWithoutXML(this, rootView); 
			enlazarCajas(); 
			listening(); 
			cargarDatos();
			return rootView; 
	  } 
	public CISECCION_05Fragment_002Dialog() {
		  super();
		  seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI510","QIMES_CAL","QIANIO_CAL","QI501","QI502","QI508","ID","HOGAR_ID","PERSONA_ID")};
		  seccionesgrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIANIO_CAL","QIMES_CAL")};
		  seccionesCargadoCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIINDICE","QIMES_ID","QIANIO","QICOL4")};
		  seccionesGrabadoCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIANIO","QIMES_ID","QIINDICE","QICOL4","QITRAMO_ID")};
		  seccionesGrabadoTramosCol04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QITRAMO_ID","QICANTIDAD")};
		  seccionesCargadoTramosCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QITRAMO_ID","QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QICANTIDAD")};
		  seccionsecargados01_s03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QICAL_CONS","QI106")};
	}
	@Override
	protected View createUI() {
		buildFields(); 
		q0= createQuestionSection(0, chbSINUNION);
		q1 = createQuestionSection(lblpregunta01,lblpregunta02,gridcalendario.component());
		q2 = createButtonSection(btnAceptar,btnCancelar);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2); 
		return contenedor;
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		spnQIMES_INI = new SpinnerField(getActivity()).size(altoComponente+15, 400);
		llenarMes(spnQIMES_INI);
		spnQICOL4 = new SpinnerField(getActivity()).size(altoComponente+15, 700);
		spnQIMES_FIN = new SpinnerField(getActivity()).size(altoComponente+15, 400);
		llenarMes(spnQIMES_FIN);
		llenarSpinnerColumna4();
		txtQIANIO_FIN = new IntegerField(getActivity()).size(altoComponente, 300).maxLength(4);
		txtQIANIO_INI = new IntegerField(getActivity()).size(altoComponente, 300).maxLength(4);
		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(250, 55).text(R.string.btnAceptar);
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean flag=false;
				flag=grabar();
				if(!flag){
					return;
				}
				CISECCION_05Fragment_002Dialog.this.dismiss();
				caller.cargarTabla();
				if(!ExisteEspaciosEnblanco()){
					caller.btnIniciar.setEnabled(true);
					caller.AbrirPreguntas(null);
				}
				else{
					caller.btnIniciar.setEnabled(false);
				}
			}
		});
		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(250, 55).text(R.string.btnCancelar);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CISECCION_05Fragment_002Dialog.this.dismiss();
			}
		});
		chbSINUNION=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qinouniones,"1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQICalendario");
		lblpregunta01 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qipregunta01);
		lblpregunta02 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qipregunta02); 
		gridcalendario = new GridComponent2(getActivity(), 2);
		gridcalendario.addComponent(spnQIMES_INI);
		gridcalendario.addComponent(txtQIANIO_INI);
		gridcalendario.addComponent(spnQIMES_FIN);
		gridcalendario.addComponent(txtQIANIO_FIN);
		gridcalendario.addComponent(spnQICOL4,2);
	}
	
	 public void llenarMes(SpinnerField spiner){
	    	List<Object> keys = new ArrayList<Object>();
	    	keys.add(1);
	    	keys.add(2);
	    	keys.add(3);
	    	keys.add(4);
	    	keys.add(5);
	    	keys.add(6);
	    	keys.add(7);
	    	keys.add(8);
	    	keys.add(9);
	    	keys.add(10);
	    	keys.add(11);
	    	keys.add(12);
	    	keys.add(13);
	    	String[] items = new String[]{ "ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE","NO SABE"};
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(
	                getActivity(), R.layout.spinner_item, R.id.textview, items);
	    	spiner.setAdapterWithKey(adapter, keys);
	    }
	 public void llenarSpinnerColumna4(){
		 List<Object> keys = new ArrayList<Object>();
		 keys.add("X");
		 String[] items =  new String[]{"EN UNION(CASADA O CONVIVIENTE)"};
		 ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,R.id.textview,items);
		 spnQICOL4.setAdapterWithKey(adapter,keys);
	 }
	 public void cargarDatos() {
		 individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		 tramocol4 = getCuestionarioService().getTramoByTramoId(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, tramocol4.qitramo_id, seccionesCargadoTramosCalendario);
		 seccion_01_03 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionsecargados01_s03);
		 if(seccion_01_03.qical_cons==null){
				fecha_actual=Calendar.getInstance();
			}
			else{
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(seccion_01_03.qical_cons);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecha_actual = cal;
			}
		 if(tramocol4==null){
			 tramocol4= new CICALENDARIO_TRAMO_COL4();
			 tramocol4.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			 tramocol4.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			 tramocol4.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			 tramocol4.qitramo_id = caller.detalletramos.size()+1; 
		 }
		 entityToUI(tramocol4);
		 inicio();
	 }
	 public void MostrarTexto(){
		 Integer nosabe=individual.qimes_cal;
		 Integer anionosabe=individual.qianio_cal;
		 if(individual.qi510!=null){
			 anionosabe=fecha_actual.get(Calendar.YEAR)-seccion_01_03.qi106;
			 anionosabe=anionosabe+ Integer.parseInt(caller.txtQI510.getText().toString());
		 }
		 if(!caller.chbP509_m.isChecked()){
			 nosabe=caller.txtQI509M.getValue()!=null?Integer.parseInt(caller.txtQI509M.getText().toString()):individual.qimes_cal;	 
		 }
		 if(!caller.chbP509_y.isChecked()){
			 anionosabe=caller.txtQI509Y.getValue()!=null?Integer.parseInt(caller.txtQI509Y.getText().toString()):individual.qianio_cal;
		 }
//		 Log.e("", "M: CAL "+individual.qimes_cal);
//		 Log.e("", "A: CAL "+individual.qianio_cal);
//		 Log.e("", "M: NS "+nosabe);
//		 Log.e("", "A: NS "+anionosabe);
		 if(individual.qimes_cal!=null  && !Util.esDiferente(individual.qimes_cal,nosabe) &&  individual.qianio_cal!=null && !Util.esDiferente(individual.qianio_cal,anionosabe)){
			 lblpregunta02.setVisibility(View.GONE);
			 lblpregunta01.setVisibility(View.VISIBLE);
			 spnQIMES_INI.setVisibility(View.GONE); 
			 txtQIANIO_INI.setVisibility(View.GONE);
			 spnQICOL4.setVisibility(View.GONE);
		 }
		 else{
			 lblpregunta02.setVisibility(View.VISIBLE); 
			 lblpregunta01.setVisibility(View.GONE);
			 spnQIMES_INI.setVisibility(View.VISIBLE); 
			 txtQIANIO_INI.setVisibility(View.VISIBLE);
			 spnQICOL4.setVisibility(View.VISIBLE);
		 }
		 
	 }
	 private void inicio(){
		 RenombrarEtiquetas();
		 MostrarTexto();
		 if(!Util.esDiferente(individual.qi508, 1)){
			 Util.cleanAndLockView(getActivity(),chbSINUNION);
			 q0.setVisibility(View.GONE);
		 }
		 else{
			 Util.lockView(getActivity(),false,chbSINUNION);
			 q0.setVisibility(View.VISIBLE);
		 }
		 ValidarsiesSupervisora();
	 }
	 public void onchbQICalendario(){
		 if(chbSINUNION.isChecked()){
			 q1.setVisibility(View.GONE);
		 }
		 else{
			 q1.setVisibility(View.VISIBLE);
		 }
	 }
	 public void RenombrarEtiquetas(){
		 String data="";
		 if(individual.qimes_cal!=null && individual.qimes_cal!=13){
			 data=MyUtil.Mes(individual.qimes_cal-1);
		 }
		 else{
			 data=" NS ";
		 }
		 lblpregunta01.setText(lblpregunta01.getText().toString().replace("#",data));
		 lblpregunta01.setText(lblpregunta01.getText().toString().replace("$",individual.qianio_cal+""));
		 lblpregunta02.setText(lblpregunta02.getText().toString().replace("#",data));
		 lblpregunta02.setText(lblpregunta02.getText().toString().replace("$",individual.qianio_cal+""));
	 }
	 
	 public boolean grabar(){
		 uiToEntity(tramocol4); 
		 if (!validar()) { 
				if (error) { 
					if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
					if (view != null) view.requestFocus(); 
				} 
				return false; 
			} 
		 try {
			 ConvivienteyunaSolapareja();
			 individual.qimes_cal = tramocol4.qimes_fin;
			 individual.qianio_cal = tramocol4.qianio_fin; 
			getCuestionarioService().saveOrUpdate(individual,seccionesgrabado);
		} catch (SQLException e) {
			// TODO: handle exception
		}
		 
		 return true;
	 }
	 public boolean validar(){
		 if(!isInRange()) return false; 
		 if(!chbSINUNION.isChecked()){
			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
			if (Util.esDiferente(individual.qimes_cal, 13) && Util.esMayor(individual.qimes_cal,tramocol4.qianio_fin)) { 
				mensaje = "El a�o final no puede ser menor"; 
				view = txtQIANIO_FIN; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(individual.qianio_cal, tramocol4.qianio_fin) && Util.esDiferente(individual.qimes_cal, 13) && individual.qimes_cal>tramocol4.qimes_fin) { 
				mensaje = "El mes de termino no puede ser menor al de inicio"; 
				view = spnQIMES_FIN; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(tramocol4.qimes_ini, 13) && tramocol4.qianio_ini>=App.ANIOPORDEFECTO){
				mensaje = "El mes de no puede ser no sabe si es menor a: "+App.ANIOPORDEFECTO; 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
			if(tramocol4.qimes_fin!=null && tramocol4.qianio_fin!=null && !Util.esDiferente(tramocol4.qimes_fin, 13) && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
				mensaje = "El mes de no puede ser no sabe si es menor a: "+App.ANIOPORDEFECTO; 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
			if(tramocol4.qimes_fin!=null && individual.qimes_cal!=13 && tramocol4.qimes_fin<individual.qimes_cal && !Util.esDiferente(tramocol4.qianio_fin, individual.qianio_cal)){
				mensaje = "El mes no puede ser menor a "+ MyUtil.Mes(individual.qimes_cal-1); 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
			if(tramocol4.qianio_fin!=null && Util.esMenor(tramocol4.qianio_fin, individual.qianio_cal)){
				mensaje = "El a�o no debe ser menor a "+ individual.qianio_cal; 
				view = txtQIANIO_FIN; 
				error = true; 
				return false;
			}
			if(tramocol4.qianio_ini!=null && Util.esMenor(tramocol4.qianio_ini, individual.qianio_cal)){
				mensaje = "El a�o no debe ser menor a "+ individual.qianio_cal; 
				view = txtQIANIO_INI; 
				error = true; 
				return false;
			}
			if(tramocol4.qimes_ini!=null && individual.qimes_cal!=13 && tramocol4.qimes_ini<=individual.qimes_cal && !Util.esDiferente(tramocol4.qianio_ini, individual.qianio_cal)){
				mensaje = "El mes no puede ser menor a "+ MyUtil.Mes(individual.qimes_cal-1); 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
			if(!Util.esDiferente(individual.qi508, 1) && tramocol4.qianio_fin==fecha_actual.get(Calendar.YEAR) && tramocol4.qimes_fin==fecha_actual.get(Calendar.MONTH)+1){
					mensaje = "No puede registrar fecha actual porque no esta unida actualmente"; 
					view = spnQIMES_FIN; 
					error = true; 
					return false;
			 }
			if(!Util.esDiferente(tramocol4.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(tramocol4.qimes_fin, fecha_actual.get(Calendar.MONTH)+1) && !Util.esDiferente(individual.qi501, 3)){
				mensaje = "Actualmente no est� unida"; 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
			if(Util.esMayor(tramocol4.qianio_fin,fecha_actual.get(Calendar.YEAR))){
				mensaje = "El a�o no puede ser mayor que la fecha actual"; 
				view = txtQIANIO_FIN; 
				error = true; 
				return false;
			}
			if(!Util.esDiferente(tramocol4.qianio_fin, fecha_actual.get(Calendar.YEAR)) &&  Util.esMayor(tramocol4.qimes_fin,fecha_actual.get(Calendar.MONTH)+1)){
				mensaje = "El mes no puede ser mayor que el mes actual"; 
				view = spnQIMES_FIN; 
				error = true; 
				return false;
			}
		 }
		 else{
			 if(!Util.esDiferente(individual.qi501, 1,2)){
					mensaje = "No puede completarse con \"no\" uni�n porque actualmente est� en uni�n"; 
					view = chbSINUNION; 
					error = true; 
					return false;
			}
		 }
	 return true;
	 }
	 public void ConvivienteyunaSolapareja(){
		 if(chbSINUNION.isChecked()){
			 RegistrartramosDeNoUnion();
		 }
		 else{
			 CICALENDARIO_TRAMO_COL4 tramoagrabar= null;
			 Integer tramo_id=-1;
			 if(!Util.esDiferente(individual.qi508, 1)){
				 if(individual.qianio_cal>=App.ANIOPORDEFECTO){
					 	tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=individual.qimes_cal;
						tramoagrabar.qianio_ini = individual.qianio_cal;
						tramoagrabar.qimes_fin=tramocol4.qimes_fin;
						tramoagrabar.qianio_fin = tramocol4.qianio_fin;
						tramoagrabar.qiresult="X";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					RegistrartramosDeNoUnion();
				 }
				 if(individual.qianio_cal<App.ANIOPORDEFECTO && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
					 	tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						tramoagrabar.qimes_fin=tramocol4.qimes_fin;
						tramoagrabar.qianio_fin = tramocol4.qianio_fin;
						tramoagrabar.qiresult="X";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					RegistrartramosDeNoUnion();
				 }
				 if(individual.qianio_cal<App.ANIOPORDEFECTO && tramocol4.qianio_fin<App.ANIOPORDEFECTO){
						tramoagrabar = new CICALENDARIO_TRAMO_COL4();
						tramoagrabar.id=individual.id;
						tramoagrabar.hogar_id = individual.hogar_id;
						tramoagrabar.persona_id=individual.persona_id;
						tramoagrabar.qimes_ini=App.MESPORDEFECTO;
						tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
						tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
						tramoagrabar.qianio_fin =fecha_actual.get(Calendar.YEAR);
						tramoagrabar.qiresult="0";
						tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
						tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
						try {
							tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
						} catch (SQLException e) {
							ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
				 }
			 }
			 
			 
			 
			 boolean entro=false;
			 
			 if(!Util.esDiferente(individual.qi508, 2)){
				 Log.e("","DATOS 508= 2");
				 if(!Util.esDiferente(individual.qi501, 1,2)){
					 Log.e("","DATOS 501= (1,2)");
						 if(individual.qianio_cal>=App.ANIOPORDEFECTO){
//							 Log.e("","ANIO CAL > "+individual.qianio_cal);
									 	tramoagrabar = new CICALENDARIO_TRAMO_COL4();
										tramoagrabar.id=individual.id;
										tramoagrabar.hogar_id = individual.hogar_id;
										tramoagrabar.persona_id=individual.persona_id;
										tramoagrabar.qimes_ini=individual.qimes_cal;
										tramoagrabar.qianio_ini = individual.qianio_cal;
										tramoagrabar.qimes_fin=tramocol4.qimes_fin;
										tramoagrabar.qianio_fin = tramocol4.qianio_fin;
										tramoagrabar.qiresult="X";
										tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
										tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
										try {
											tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
										} catch (SQLException e) {
											ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
									}
							 }
						 if(tramocol4.qianio_ini!=null && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
							 entro=true;
//							 Log.e("","GRABADO CUANDO INI != NULL");
							 if(tramocol4.qianio_ini>=App.ANIOPORDEFECTO || tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
								 if(tramocol4.qianio_ini<App.ANIOPORDEFECTO){
									 tramocol4.qianio_ini=App.ANIOPORDEFECTO;
									 tramocol4.qimes_ini=App.MESPORDEFECTO;
								 }
									 tramocol4.qiresult="X";
									 tramocol4.qicantidad = MyUtil.DeterminarIndice(tramocol4.qianio_ini, tramocol4.qimes_ini)-MyUtil.DeterminarIndice(tramocol4.qianio_fin, tramocol4.qimes_fin);
									 tramocol4.qicantidad = tramocol4.qicantidad+1; 
										try {
											tramo_id = getCuestionarioService().saveOrUpdate(tramocol4, null, seccionesGrabadoTramosCol04);
										} catch (SQLException e) {
											ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
										}
									Integer mesfinal=tramocol4.qimes_ini-1;
									if(mesfinal==0){
										mesfinal=12;
									}
								if((Util.esDiferente(individual.qimes_cal, mesfinal) && !Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)|| 
								   (!Util.esDiferente(individual.qimes_cal, mesfinal) && Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)||
								   (Util.esDiferente(individual.qimes_cal, mesfinal) && Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)))) && 
								   (!Util.esDiferente(tramocol4.qianio_ini, App.ANIOPORDEFECTO)  && Util.esDiferente(tramocol4.qimes_ini, App.MESPORDEFECTO) || 
								   ( Util.esDiferente(tramocol4.qianio_ini, App.ANIOPORDEFECTO)  && Util.esMayor(tramocol4.qianio_ini,App.ANIOPORDEFECTO)))){
									boolean data=false;
									tramoagrabar = new CICALENDARIO_TRAMO_COL4();
									tramoagrabar.id=individual.id;
									tramoagrabar.hogar_id = individual.hogar_id;
									tramoagrabar.persona_id=individual.persona_id;
									if(tramocol4.qianio_ini>=App.ANIOPORDEFECTO && individual.qianio_cal<App.ANIOPORDEFECTO){
										tramoagrabar.qimes_ini=App.MESPORDEFECTO;
										tramoagrabar.qianio_ini=App.ANIOPORDEFECTO;
									}
									else{	
										if(individual.qimes_cal+1==13){
											tramoagrabar.qimes_ini=1;
											tramoagrabar.qianio_ini=individual.qianio_cal+1;
										}
										else{
											tramoagrabar.qimes_ini=individual.qimes_cal+1;
											tramoagrabar.qianio_ini = individual.qianio_cal;
										}
										mesfinal=tramocol4.qimes_ini-1;
										if(mesfinal==0){
											data=true;
											mesfinal=12;
											tramocol4.qianio_ini=tramocol4.qianio_ini-1<App.ANIOPORDEFECTO?tramocol4.qianio_ini:tramocol4.qianio_ini-1;
										}
									}
									if(mesfinal==12 && !data){
										tramocol4.qianio_ini=tramocol4.qianio_ini-1<App.ANIOPORDEFECTO?tramocol4.qianio_ini:tramocol4.qianio_ini-1;
									}
										tramoagrabar.qimes_fin=mesfinal;
										tramoagrabar.qianio_fin = tramocol4.qianio_ini;
										tramoagrabar.qiresult="0";
										tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
										tramoagrabar.qicantidad = tramoagrabar.qicantidad+1;
										if(tramoagrabar.qicantidad>0){
											try {
												tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
											} catch (SQLException e) {
												ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
											}
										}
							}
						 }
						}
					if(individual.qianio_cal<App.ANIOPORDEFECTO && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
//						Log.e("","ANIO CAL < "+App.ANIOPORDEFECTO);
							if(tramocol4.qianio_ini!=null && tramocol4.qianio_ini<App.ANIOPORDEFECTO && !entro){
//								Log.e("","TRAMO.QIANIOFIN >=" +App.ANIOPORDEFECTO);
								tramoagrabar = new CICALENDARIO_TRAMO_COL4();
								tramoagrabar.id=individual.id;
								tramoagrabar.hogar_id = individual.hogar_id;
								tramoagrabar.persona_id=individual.persona_id;
								tramoagrabar.qimes_ini=App.MESPORDEFECTO;
								tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
								tramoagrabar.qimes_fin=tramocol4.qimes_fin;
								tramoagrabar.qianio_fin = tramocol4.qianio_fin;
								tramoagrabar.qiresult="X";
								tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
								tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
								try {
									tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
								} catch (SQLException e) {
									ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
								}
							}
							if(!entro){
//								Log.e("ELSE","#");
								tramoagrabar = new CICALENDARIO_TRAMO_COL4();
								tramoagrabar.id=individual.id;
								tramoagrabar.hogar_id = individual.hogar_id;
								tramoagrabar.persona_id=individual.persona_id;
								tramoagrabar.qimes_ini=App.MESPORDEFECTO;
								tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
								tramoagrabar.qimes_fin=tramocol4.qimes_fin;
								tramoagrabar.qianio_fin = tramocol4.qianio_fin;
								tramoagrabar.qiresult="X";
								tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
								tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
								try {
									tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
								} catch (SQLException e) {
									ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
							}
						}
					}
				 }
				 entro=false;
				 if(!Util.esDiferente(individual.qi501, 3)){
					 if(!Util.esDiferente(individual.qi502, 1,2)){
						 if(individual.qianio_cal>=App.ANIOPORDEFECTO){
							 	tramoagrabar = new CICALENDARIO_TRAMO_COL4();
								tramoagrabar.id=individual.id;
								tramoagrabar.hogar_id = individual.hogar_id;
								tramoagrabar.persona_id=individual.persona_id;
								tramoagrabar.qimes_ini=individual.qimes_cal;
								tramoagrabar.qianio_ini = individual.qianio_cal;
								tramoagrabar.qimes_fin=tramocol4.qimes_fin;
								tramoagrabar.qianio_fin = tramocol4.qianio_fin;
								tramoagrabar.qiresult="X";
								tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
								tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
								try {
									tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
								} catch (SQLException e) {
									ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
							}
						 }
						 if(tramocol4.qianio_ini!=null && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
							 entro=true;
//							 Log.e(" G. NORMAL ","GRABADO CUANDO INI != NULL");
							 if(tramocol4.qianio_ini>=App.ANIOPORDEFECTO || tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
								 if(tramocol4.qianio_ini<App.ANIOPORDEFECTO){
									 tramocol4.qianio_ini=App.ANIOPORDEFECTO;
									 tramocol4.qimes_ini=App.MESPORDEFECTO;
								 }
									 tramocol4.qiresult="X";
									 tramocol4.qicantidad = MyUtil.DeterminarIndice(tramocol4.qianio_ini, tramocol4.qimes_ini)-MyUtil.DeterminarIndice(tramocol4.qianio_fin, tramocol4.qimes_fin);
									 tramocol4.qicantidad = tramocol4.qicantidad+1; 
										try {
											tramo_id = getCuestionarioService().saveOrUpdate(tramocol4, null, seccionesGrabadoTramosCol04);
										} catch (SQLException e) {
											ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
										}
									Integer mesfinal=tramocol4.qimes_ini-1;
									if(mesfinal==0){
										mesfinal=12;
									}
								
								if((Util.esDiferente(individual.qimes_cal, mesfinal) && !Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)|| 
								   (!Util.esDiferente(individual.qimes_cal, mesfinal) && Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)||
								   (Util.esDiferente(individual.qimes_cal, mesfinal) && Util.esDiferente(individual.qianio_cal, tramocol4.qianio_ini)))) && 
								   (!Util.esDiferente(tramocol4.qianio_ini, App.ANIOPORDEFECTO)  && Util.esDiferente(tramocol4.qimes_ini, App.MESPORDEFECTO) || 
								   ( Util.esDiferente(tramocol4.qianio_ini, App.ANIOPORDEFECTO)  && Util.esMayor(tramocol4.qianio_ini,App.ANIOPORDEFECTO)))){
									boolean data=false;
									tramoagrabar = new CICALENDARIO_TRAMO_COL4();
									tramoagrabar.id=individual.id;
									tramoagrabar.hogar_id = individual.hogar_id;
									tramoagrabar.persona_id=individual.persona_id;
									if(tramocol4.qianio_ini>=App.ANIOPORDEFECTO && individual.qianio_cal<App.ANIOPORDEFECTO){
										tramoagrabar.qimes_ini=App.MESPORDEFECTO;
										tramoagrabar.qianio_ini=App.ANIOPORDEFECTO;
									}
									else{	
										if(individual.qimes_cal+1==13){
											tramoagrabar.qimes_ini=1;
											tramoagrabar.qianio_ini=individual.qianio_cal+1;
										}
										else{
											tramoagrabar.qimes_ini=individual.qimes_cal+1;
											tramoagrabar.qianio_ini = individual.qianio_cal;
										}
										 mesfinal=tramocol4.qimes_ini-1;
										if(mesfinal==0){
											data=true;
											mesfinal=12;
											tramocol4.qianio_ini=tramocol4.qianio_ini-1<App.ANIOPORDEFECTO?tramocol4.qianio_ini:tramocol4.qianio_ini-1;
										}
									}
									if(mesfinal==12 && !data){
										tramocol4.qianio_ini=tramocol4.qianio_ini-1<App.ANIOPORDEFECTO?tramocol4.qianio_ini:tramocol4.qianio_ini-1;
									}
										tramoagrabar.qimes_fin=mesfinal;
										tramoagrabar.qianio_fin = tramocol4.qianio_ini;
										tramoagrabar.qiresult="0";
										tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
										tramoagrabar.qicantidad = tramoagrabar.qicantidad+1;
										if(tramoagrabar.qicantidad>0){
											try {
												tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
											} catch (SQLException e) {
												ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
											}
										}
							}
						}
					}
					if(individual.qianio_cal<App.ANIOPORDEFECTO){
							 if(tramocol4.qianio_ini!=null && tramocol4.qianio_ini<App.ANIOPORDEFECTO && !entro && tramocol4.qianio_fin>=App.ANIOPORDEFECTO){
//									Log.e("1111 ","TRAMO.QIANIOFIN >=" +App.ANIOPORDEFECTO);
									tramoagrabar = new CICALENDARIO_TRAMO_COL4();
									tramoagrabar.id=individual.id;
									tramoagrabar.hogar_id = individual.hogar_id;
									tramoagrabar.persona_id=individual.persona_id;
									tramoagrabar.qimes_ini=App.MESPORDEFECTO;
									tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
									tramoagrabar.qimes_fin=tramocol4.qimes_fin;
									tramoagrabar.qianio_fin = tramocol4.qianio_fin;
									tramoagrabar.qiresult="X";
									tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
									tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
									try {
										tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
									} catch (SQLException e) {
										ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
									}
								}
								if(tramocol4.qianio_fin>=App.ANIOPORDEFECTO && !entro){
									Log.e("ELSE 2222","#");
									tramoagrabar = new CICALENDARIO_TRAMO_COL4();
									tramoagrabar.id=individual.id;
									tramoagrabar.hogar_id = individual.hogar_id;
									tramoagrabar.persona_id=individual.persona_id;
									tramoagrabar.qimes_ini=App.MESPORDEFECTO;
									tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
									tramoagrabar.qimes_fin=tramocol4.qimes_fin;
									tramoagrabar.qianio_fin = tramocol4.qianio_fin;
									tramoagrabar.qiresult="X";
									tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
									tramoagrabar.qicantidad = tramoagrabar.qicantidad+1; 
									try {
										tramo_id = getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
									} catch (SQLException e) {
										ToastMessage.msgBox(getActivity(), e.getMessage()+" Error Calendario", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
								}
							}
						}
					}
				 }
			 }
		 }
	 }
	 public boolean ExisteEspaciosEnblanco(){
		 boolean flag = getCuestionarioService().getCompletadoTramosCol4(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,fecha_actual);
		 boolean completado=false;
		 if(!flag){
			 completado = false;
		 }
		 else{
			 completado=true;
		 }
		 return completado;
	 }
	 public void RegistrartramosDeNoUnion(){
		 List<CICALENDARIO_TRAMO_COL4> todo = new ArrayList<CICALENDARIO_TRAMO_COL4>();
		 List<CICALENDARIO_TRAMO_COL4> todocopia = new ArrayList<CICALENDARIO_TRAMO_COL4>();
		 
		 CICALENDARIO_TRAMO_COL4 tramoinf = new CICALENDARIO_TRAMO_COL4();
		 CICALENDARIO_TRAMO_COL4 tramosup = null;
		 CICALENDARIO_TRAMO_COL4 tramoagrabar=null;
		 CICALENDARIO_TRAMO_COL4  item=null;
		 todo = getCuestionarioService().getTramoscol04ByPersona(individual.id,individual.hogar_id,individual.persona_id, seccionesCargadoTramosCalendario);
		 if(todo.size()==0){
			 tramoagrabar = new CICALENDARIO_TRAMO_COL4();
			 tramoagrabar.id = individual.id;
			 tramoagrabar.hogar_id= individual.hogar_id;
			 tramoagrabar.persona_id= individual.persona_id;
			 tramoagrabar.qianio_ini = App.ANIOPORDEFECTO;
			 tramoagrabar.qimes_ini=App.MESPORDEFECTO;
			 tramoagrabar.qianio_fin=fecha_actual.get(Calendar.YEAR);
			 tramoagrabar.qimes_fin=fecha_actual.get(Calendar.MONTH)+1;
			 tramoagrabar.qiresult="0";
			 tramoagrabar.qicantidad=MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
			 tramoagrabar.qicantidad++;
		 }
		 else{
		 Collections.reverse(todo);
		 todocopia.addAll(todo);
		 item = new CICALENDARIO_TRAMO_COL4();
		 item.qianio_ini =fecha_actual.get(Calendar.YEAR);
		 item.qimes_ini = fecha_actual.get(Calendar.MONTH)+1;
		 item.qitramo_id = todo.size()+1;
			 tramoinf = item;
			 for(CICALENDARIO_TRAMO_COL4 tramo: todocopia){
				 if(!Util.esDiferente(item.qianio_ini, tramo.qianio_fin) && !Util.esDiferente(item.qimes_ini, tramo.qimes_fin+1) && Util.esDiferente(item.qitramo_id,tramo.qitramo_id)){
						break;
					}
					else{
						Log.e("","ELSE");
						tramosup=tramo;
						break;
					}
			 }
//		 }
			if(tramosup==null){
				tramosup = new CICALENDARIO_TRAMO_COL4();
				tramosup.id=individual.id;
				tramosup.hogar_id=individual.hogar_id;
				tramosup.persona_id=individual.persona_id;
				tramosup.qianio_fin=fecha_actual.get(Calendar.YEAR);
				tramosup.qimes_fin = fecha_actual.get(Calendar.MONTH)+1;
				tramosup.qianio_ini=fecha_actual.get(Calendar.YEAR);
				tramosup.qimes_ini = fecha_actual.get(Calendar.MONTH)+1;
				tramosup.qiresult =null;
			}
//			Log.e("","SUP: A"+tramosup.qianio_fin+" M: "+tramosup.qimes_fin);
//			Log.e("","INF: A"+tramoinf.qianio_ini+" M: "+tramoinf.qimes_ini);
			
			 Integer Mesinicial=0;
			 if(tramosup.qimes_fin==fecha_actual.get(Calendar.MONTH)+1 && tramosup.qianio_fin==fecha_actual.get(Calendar.YEAR)){
				 Mesinicial=fecha_actual.get(Calendar.MONTH)+1;
			 }
			 else{
				 Mesinicial =tramosup.qimes_fin+1;
			 }
			 Integer Mesfinal = tramoinf.qimes_ini;
			 if(Mesinicial==13){
				 Mesinicial=1;
				 tramosup.qianio_fin++;
			 }
			 if(Mesfinal==0){
				 Mesfinal=12;
				 tramoinf.qianio_ini--;
			 }
			 tramoagrabar = new CICALENDARIO_TRAMO_COL4();
			 tramoagrabar.id=tramosup.id;
			 tramoagrabar.hogar_id = tramosup.hogar_id;
			 tramoagrabar.persona_id = tramosup.persona_id;
			 tramoagrabar.qimes_ini =   Mesinicial;
			 tramoagrabar.qianio_ini =  tramosup.qianio_fin;
			 tramoagrabar.qimes_fin =  Mesfinal;
			 tramoagrabar.qianio_fin = tramoinf.qianio_ini;
			 tramoagrabar.qicantidad = MyUtil.DeterminarIndice(tramoagrabar.qianio_ini, tramoagrabar.qimes_ini)-MyUtil.DeterminarIndice(tramoagrabar.qianio_fin, tramoagrabar.qimes_fin);
			 tramoagrabar.qicantidad=tramoagrabar.qicantidad+1;
			 tramoagrabar.qiresult = "0";
		 }
		 try {
				getCuestionarioService().saveOrUpdate(tramoagrabar, null, seccionesGrabadoTramosCol04);
			} catch (SQLException e) {
				// TODO: handle exception
			}
//		 }
		 
	}
		public void ValidarsiesSupervisora(){
			Integer codigo=App.getInstance().getUsuario().cargo_id;
			if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
				chbSINUNION.readOnly();
				spnQIMES_INI.readOnly();
				txtQIANIO_INI.readOnly();
				spnQIMES_FIN.readOnly();
				txtQIANIO_FIN.readOnly();
				spnQICOL4.readOnly();
				btnAceptar.setEnabled(false);
			}
		}
	 public CuestionarioService getCuestionarioService() { 
			if(cuestionarioService==null){ 
				cuestionarioService = CuestionarioService.getInstance(getActivity()); 
			} 
			return cuestionarioService; 
	    } 
}
