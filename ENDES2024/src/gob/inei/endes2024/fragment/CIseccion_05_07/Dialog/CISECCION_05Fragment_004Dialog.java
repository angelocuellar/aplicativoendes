package gob.inei.endes2024.fragment.CIseccion_05_07.Dialog;
import java.util.Calendar;
import java.util.GregorianCalendar;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_004;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CISECCION_05Fragment_004Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI513BU; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI513BN; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI514; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI514A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI515; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI515X; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI516U; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI516N; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI516B; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI516C; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI516D; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI516E; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI516F; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI517; 
	@FieldAnnotation(orderIndex=15) 
	public IntegerField txtQI521;
	@FieldAnnotation(orderIndex=16) 
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=17) 
	public ButtonComponent btnCancelar;
		
	///jjjjj ffffggggjjjjjjvvvvffff
	private static CISECCION_05Fragment_004 caller;
	public CheckBoxField chbP516B;
	public CheckBoxField chbP521;
	
	CISECCION_05 individual; 
	CISECCION_01_03 individualS1; 
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblpregunta513B,lblpregunta514,lblpregunta514A,lblpregunta515,lblpregunta516,lblpregunta516_ind,lblpregunta516B,lblpregunta516C,lblpregunta516D,lblpregunta516E,lblpregunta516F,lblpregunta517,lblpregunta521,lblEspacio,lblEdad,lblnrocom,lblEspacio1,lblpregunta515_ind1,lblpregunta515_ind2,lblpregunta515_ind3,lblpregunta515_ind4,lblpregunta515_ind5;
	public ButtonComponent btnOmisionEdad,btnOmisionNro;
	private GridComponent2 gridPreguntas513B,gridPreguntas516,gridPreguntas516B,gridPreguntas521;
	private IntegerField txtQI513N1,txtQI513N2,txtQI513N3,txtQI516N1,txtQI516N2,txtQI516N3;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS1;
	public static Integer pareja_idd;
	private enum PROCCES {
		 GRABADOPARCIAL
	}

	public static CISECCION_05Fragment_004Dialog newInstance(FragmentForm pagina, CISECCION_05 detalle, Integer pareja_id) {
          CISECCION_05Fragment_004Dialog f = new CISECCION_05Fragment_004Dialog();
          caller = (CISECCION_05Fragment_004) pagina;
          f.setParent(pagina);
          Bundle args = new Bundle();
          args.putSerializable("detalle", (CISECCION_05) detalle);
          pareja_idd=pareja_id;
           f.setArguments(args);
          return f;
}
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
		individual = (CISECCION_05) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
	  	getDialog().setTitle("N� de Pareja:  "+individual.pareja_id+" : "+ individual.getOrdenPareja());
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		cargarDatos();
		return rootView; 
  } 
  
  public CISECCION_05Fragment_004Dialog() {
	  super();
	  seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI513BU","QI513BN","QI514","QI514A","QI515","QI515X","QI516U","QI516N","QI516B","QI516C","QI516D","QI516E","QI516F","QI517","QI521","ID","HOGAR_ID","PERSONA_ID","PAREJA_ID")}; 
	  seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI513BU","QI513BN","QI514","QI514A","QI515","QI515X","QI516U","QI516N","QI516B","QI516C","QI516D","QI516E","QI516F","QI517","QI521")};
	  seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106")};
		 
  }
  
  @Override 
  protected void buildFields() {
	  rgQI513BU=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi513bu_1,R.string.c2seccion_05_07qi513bu_2,R.string.c2seccion_05_07qi513bu_3).size(225,200).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI513BChangeValue"); 
	  txtQI513N1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  txtQI513N2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  txtQI513N3=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  
	  rgQI514=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi514_1,R.string.c2seccion_05_07qi514_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS514ChangeValue"); 
	  rgQI514A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi514a_1,R.string.c2seccion_05_07qi514a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  rgQI515=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi515_1,R.string.c2seccion_05_07qi515_2,R.string.c2seccion_05_07qi515_3,R.string.c2seccion_05_07qi515_4,R.string.c2seccion_05_07qi515_5,R.string.c2seccion_05_07qi515_6,R.string.c2seccion_05_07qi515_7,R.string.c2seccion_05_07qi515_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS515ChangeValue"); 
	  txtQI515X=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
	  rgQI515.agregarEspecifique(7, txtQI515X);
	  
	  rgQI516U=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi516u_1,R.string.c2seccion_05_07qi516u_2,R.string.c2seccion_05_07qi516u_3).size(225,200).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI516ChangeValue"); 
	  txtQI516N1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  txtQI516N2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  txtQI516N3=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  
	  txtQI516B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqrgQS516BChangeValue");
	  chbP516B=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512ba_2, "1:0").size(60, 200);
	  chbP516B.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked){
	    		Util.cleanAndLockView(getActivity(),txtQI516B);
	    		onqrgQS516BChangeValue();
	  		}else {
				Util.lockView(getActivity(), false,txtQI516B);
				txtQI516B.setVisibility(View.VISIBLE);
				}
			}
	  });
	  chbP521=new CheckBoxField(this.getActivity(), R.string.c2seccion_05_07qi512ba_2, "1:0").size(60, 200);
	  chbP521.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked){
	    		Util.cleanAndLockView(getActivity(),txtQI521);
	  		}else {
				Util.lockView(getActivity(), false,txtQI521);
				txtQI521.setVisibility(View.VISIBLE);
			}
		}
	  });
	  lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblEdad  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_05_07qi516b_1).textSize(16).centrar();
	  
	  rgQI516C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi516c_1,R.string.c2seccion_05_07qi516c_2,R.string.c2seccion_05_07qi516c_3,R.string.c2seccion_05_07qi516c_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS516CChangeValue"); 
	  rgQI516D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi516d_1,R.string.c2seccion_05_07qi516d_2,R.string.c2seccion_05_07qi516d_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  rgQI516E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi516e_1,R.string.c2seccion_05_07qi516e_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS516EChangeValue"); 
	  rgQI516F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi516f_1,R.string.c2seccion_05_07qi516f_2,R.string.c2seccion_05_07qi516f_3,R.string.c2seccion_05_07qi516f_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  rgQI517=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi517_1,R.string.c2seccion_05_07qi517_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  
	  txtQI521=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	  lblEspacio1  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
	  lblnrocom  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_05_07qi521_1).textSize(16).centrar();
	  
	  lblpregunta513B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi513bu);
	  lblpregunta514 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi514);
	  lblpregunta514A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi514a);
	  lblpregunta515 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi515);
	  lblpregunta515_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi515_ind1).negrita();
	  lblpregunta515_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi515_ind2);
	  lblpregunta515_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi515_ind3);
	  lblpregunta515_ind4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi515_ind4);
	  lblpregunta515_ind5 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi515_ind5);
	  lblpregunta516 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516);
	  lblpregunta516_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_05_07qi516_ind);
	  
	  lblpregunta516B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516b);
	  lblpregunta516C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516c);
	  lblpregunta516D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516d);
	  lblpregunta516E = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516e);
	  lblpregunta516F = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi516f);
	  lblpregunta517 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi517);
	  lblpregunta521 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi521);
	   
	  btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
	  btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
	  
	  btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {			
				CISECCION_05Fragment_004Dialog.this.dismiss();
			}
		});
	  btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = false;
				flag = grabar();
				if (!flag) {
					return;
				}
				Integer cantidad=caller.detalles.size();
				if(!Util.esDiferente(individual.qi517, 1)){
					if((!Util.esDiferente(individual.pareja_id,1) && (!Util.esDiferente(cantidad,1))) || (!Util.esDiferente(individual.pareja_id,2) && (!Util.esDiferente(cantidad,2)))){
					caller.agregarPareja(caller.detalles.size()+1);
					}
				}
				caller.cargarTabla();
				CISECCION_05Fragment_004Dialog.this.dismiss();
			}
		});	
	  
	  btnOmisionEdad = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.c2seccion_05_07qi516b_2).size(150, 55);
	  btnOmisionEdad.setOnClickListener(new View.OnClickListener() {
		  @Override
		  public void onClick(View v) {
			  txtQI516B.setText("98");
			  Util.lockView(getActivity(),false,rgQI516C,rgQI516D); 
			  q7.setVisibility(View.VISIBLE);
			  q8.setVisibility(View.VISIBLE);
			  onqrgQS516CChangeValue();
			  rgQI516C.requestFocus(); 
		  }
	  });
	  
	  btnOmisionNro = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.c2seccion_05_07qi521_2).size(150, 55);
	  btnOmisionNro.setOnClickListener(new View.OnClickListener() {
		  @Override
		  public void onClick(View v) {
			  txtQI521.setText("98");
		  }
	  });
	  
	  gridPreguntas513B = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
	  gridPreguntas513B.addComponent(rgQI513BU,1,3);
	  gridPreguntas513B.addComponent(txtQI513N1);
	  gridPreguntas513B.addComponent(txtQI513N2);
	  gridPreguntas513B.addComponent(txtQI513N3);
	  
	  gridPreguntas516 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
	  gridPreguntas516.addComponent(rgQI516U,1,3);
	  gridPreguntas516.addComponent(txtQI516N1);
	  gridPreguntas516.addComponent(txtQI516N2);
	  gridPreguntas516.addComponent(txtQI516N3);
	  
	  gridPreguntas516B = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
	  gridPreguntas516B.addComponent(lblEdad);
	  gridPreguntas516B.addComponent(txtQI516B);
	  gridPreguntas516B.addComponent(lblEspacio);
	  gridPreguntas516B.addComponent(chbP516B);
	  
	  gridPreguntas521 = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
	  gridPreguntas521.addComponent(lblnrocom);
	  gridPreguntas521.addComponent(txtQI521);
	  gridPreguntas521.addComponent(lblEspacio1);
	  gridPreguntas521.addComponent(chbP521);
	  
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
//		q0 = createQuestionSection(lblTitulo_1);
		q1 = createQuestionSection(lblpregunta513B,gridPreguntas513B.component()); 
		q2 = createQuestionSection(lblpregunta514,rgQI514); 
		q3 = createQuestionSection(lblpregunta514A,rgQI514A); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta515,lblpregunta515_ind1,lblpregunta515_ind2,lblpregunta515_ind3,lblpregunta515_ind4,lblpregunta515_ind5,rgQI515); 
		q5 = createQuestionSection(lblpregunta516,lblpregunta516_ind,gridPreguntas516.component()); 
		q6 = createQuestionSection(lblpregunta516B,gridPreguntas516B.component()); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta516C,rgQI516C); 
		q8 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta516D,rgQI516D); 
		q9 = createQuestionSection(lblpregunta516E,rgQI516E); 
		q10 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta516F,rgQI516F); 
		q11 = createQuestionSection(lblpregunta517,rgQI517); 
		q12 = createQuestionSection(lblpregunta521,gridPreguntas521.component());
		q13 = createButtonSection(btnAceptar,btnCancelar);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
//		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
		form.addView(q9); 
		form.addView(q10); 
		form.addView(q11); 
		form.addView(q12); 
		form.addView(q13); 
    return contenedor; 
    } 
    

    public boolean grabar() { 
    	uiToEntity(individual); 
    	if (chbP516B.isChecked())
			individual.qi516b=98;
    	if (chbP521.isChecked())
			individual.qi521=98;
    	if (individual.qi513bu!=null && pareja_idd!=1) {
			if (!Util.esDiferente(individual.qi513bu,1) && txtQI513N1.getText().toString().trim().length()!=0 ) {
				individual.qi513bn=Integer.parseInt(txtQI513N1.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513bu,2) && txtQI513N2.getText().toString().trim().length()!=0) {
				individual.qi513bn=Integer.parseInt(txtQI513N2.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi513bu,3) && txtQI513N3.getText().toString().trim().length()!=0) {
				individual.qi513bn=Integer.parseInt(txtQI513N3.getText().toString().trim());
			}
			else {
				individual.qi513bn=null;
			}
		}
    	
       	if (individual.qi516u!=null) {
    			if (!Util.esDiferente(individual.qi516u,1) && txtQI516N1.getText().toString().trim().length()!=0 ) {
    				individual.qi516n=Integer.parseInt(txtQI516N1.getText().toString().trim());
    			}
    			else if (!Util.esDiferente(individual.qi516u,2) && txtQI516N2.getText().toString().trim().length()!=0) {
    				individual.qi516n=Integer.parseInt(txtQI516N2.getText().toString().trim());
    			}
    			else if (!Util.esDiferente(individual.qi516u,3) && txtQI516N3.getText().toString().trim().length()!=0) {
    				individual.qi516n=Integer.parseInt(txtQI516N3.getText().toString().trim());
    			}
    			else {
    				individual.qi516n=null;
    			}
       	}
    	
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		boolean flag=false;
		try {		
			flag= getCuestionarioService().saveOrUpdate(individual,seccionesGrabado); 
			if(!flag){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);} 
		} catch (Exception e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return flag; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esDiferente(pareja_idd,1)) {
			if (Util.esVacio(individual.qi513bu)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI513BU"); 
				view = rgQI513BU; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi513bn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI513BN"); 
				view = txtQI513BN; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(individual.qi513bu,1) && (Util.esMenor(individual.qi513bn, 0)|| Util.esMayor(individual.qi513bn , 90))){
				mensaje = "valor fuera de rango solo permite de 0 a 90";
				view = txtQI513BN;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi513bu,2) && (Util.esMenor(individual.qi513bn, 1)|| Util.esMayor(individual.qi513bn , 30))){
				mensaje = "valor fuera de rango solo permite de 1 a 30";
				view = txtQI513BN;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi513bu,3) && (Util.esMenor(individual.qi513bn, 1)|| Util.esMayor(individual.qi513bn , 12))){
				mensaje = "valor fuera de rango solo permite de 1 a 12";
				view = txtQI513BN;
				error = true;
				return false;
			}
		}

		if (Util.esVacio(individual.qi514)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI514"); 
			view = rgQI514; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi514, 1)) {
			if (Util.esVacio(individual.qi514a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI514A"); 
				view = rgQI514A; 
				error = true; 
				return false; 
			}
		}
	 
		if (Util.esVacio(individual.qi515)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI515"); 
			view = rgQI515; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi515,96)) {
			if (Util.esVacio(individual.qi515x)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI515X"); 
				view = txtQI515X; 
				error = true; 
				return false; 
			} 
		}
		
		if (Util.esDiferente(individual.qi515,1)) {
			if (Util.esVacio(individual.qi516u)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI516U"); 
				view = rgQI516U; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi516n)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI516N"); 
				view = txtQI516N; 
				error = true; 
				return false; 
			} 
			
			if(!Util.esDiferente(individual.qi516u,1) && (Util.esMenor(individual.qi516n, 1)|| Util.esMayor(individual.qi516n , 90))){
				mensaje = "valor fuera de rango solo permite de 1 a 90";
				view = txtQI516N;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi516u,2) && (Util.esMenor(individual.qi516n, 1)|| Util.esMayor(individual.qi516n , 11))){
				mensaje = "valor fuera de rango solo permite de 1 a 11";
				view = txtQI516N;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi516u,3) && (Util.esMenor(individual.qi516n, 1)|| Util.esMayor(individual.qi516n , individualS1.qi106-10))){
				mensaje = "valor fuera de rango solo permite de 1 a edad de Mef - 10";
				view = txtQI516N;
				error = true;
				return false;
			}
			
			
			if (MyUtil.incluyeRango(12, 24, individualS1.qi106)) {
//			if (MyUtil.incluyeRango(15, 24, individualS1.qi106)) {
				if (Util.esVacio(individual.qi516b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI516B"); 
					view = txtQI516B; 
					error = true; 
					return false; 
				}
				if (MyUtil.incluyeRango(0, 9, individual.qi516b) ||  MyUtil.incluyeRango(99, 99, individual.qi516b) ) {
		    		mensaje ="Edad fuera de rango";
					view = txtQI516B;
					error=true;
					return false;
				}
				
				if (!Util.esDiferente(individual.qi516b,98)) {
					if (Util.esVacio(individual.qi516c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI516C"); 
						view = rgQI516C; 
						error = true; 
						return false; 
					} 
					if (!Util.esDiferente(individual.qi516c, 1)) {
						if (Util.esVacio(individual.qi516d)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI516D"); 
							view = rgQI516D; 
							error = true; 
							return false; 
						} 
					}
				}
			}
		}

		if (Util.esVacio(individual.qi516e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI516E"); 
			view = rgQI516E; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi516e,1)) {
			if (Util.esVacio(individual.qi516f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI516F"); 
				view = rgQI516F; 
				error = true; 
				return false; 
			} 
		}
	
		if (Util.esDiferente(pareja_idd,3)) {
			if (Util.esVacio(individual.qi517)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI517"); 
				view = rgQI517; 
				error = true; 
				return false; 
			} 
		}
		if (!Util.esDiferente(pareja_idd,3)) {
			if (Util.esVacio(individual.qi521)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI521"); 
				view = txtQI521; 
				error = true; 
				return false; 
			}
			if (MyUtil.incluyeRango(0, 0, individual.qi521) || MyUtil.incluyeRango(99, 99, individual.qi521) ) {
	    		mensaje ="Edad fuera de rango";
				view = txtQI521;
				error=true;
				return false;
			}
			
		}
	 
	
		return true; 
    } 
   
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_05(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,pareja_idd,seccionesCargado);
		
    	individualS1 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1);
//    	Log.e("edad mef","" +individualS1.qi106);
    	
    	if (individual == null) {
			individual = new CISECCION_05();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
			individual.pareja_id = pareja_idd;//1;
		}
		entityToUI(individual); 
		
		if (individual.qi513bn!=null) {	
    		if (!Util.esDiferente(individual.qi513bu,1)) {
    			txtQI513N1.setText(individual.qi513bn.toString());
    		}
    		if (!Util.esDiferente(individual.qi513bu,2)) {
    			txtQI513N2.setText(individual.qi513bn.toString());
    		}  
    		if (!Util.esDiferente(individual.qi513bu,3)) {
    			txtQI513N3.setText(individual.qi513bn.toString());
    		}   
    	} 
		
		if (individual.qi516n!=null) {	
    		if (!Util.esDiferente(individual.qi516u,1)) {
    			txtQI516N1.setText(individual.qi516n.toString());
    		}
    		if (!Util.esDiferente(individual.qi516u,2)) {
    			txtQI516N2.setText(individual.qi516n.toString());
    		}  
    		if (!Util.esDiferente(individual.qi516u,3)) {
    			txtQI516N3.setText(individual.qi516n.toString());
    		}   
    	} 
		RenombrarEtiquetas();
		inicio();
//		App.getInstance().setPersonaCuestionarioIndividual(individualS1);
    } 
    
    
    public void ValirParejaID(){
    	if (!Util.esDiferente(pareja_idd,1)) {
    		Util.cleanAndLockView(getActivity(),rgQI513BU,txtQI513BN);  		
			q1.setVisibility(View.GONE);
		}
    	else{
    		onqrgQI513BChangeValue();
    	}
    	if (!Util.esDiferente(pareja_idd,3)) {
    		Util.cleanAndLockView(getActivity(),rgQI517);  		
			q11.setVisibility(View.GONE);
		}
    	if (!Util.esDiferente(pareja_idd,1,2)) {
    		Util.cleanAndLockView(getActivity(),txtQI521);  		
			q12.setVisibility(View.GONE);
		}
    }
    public void ValirEdadMef(){
    	if (MyUtil.incluyeRango(25,49 , individualS1.qi106)) {		
    		Util.cleanAndLockView(getActivity(),txtQI516B,rgQI516C,rgQI516D);  		
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
		}
    	else{
    		Util.lockView(getActivity(),false,txtQI516B,rgQI516C,rgQI516D); 
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);	
    	}
  
    }
    private void inicio() { 
    	ValirParejaID();
    	onqrgQS514ChangeValue();	
    	ValirEdadMef();
    	onqrgQS515ChangeValue();
    	onqrgQS516EChangeValue();
    	onch521ChangeValue();
    	ValidarsiesSupervisora();
    	rgQI514.requestFocus();
    } 
    
    public void RenombrarEtiquetas() {
    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta514A.text(R.string.c2seccion_05_07qi514a);
    	lblpregunta514A.setText(lblpregunta514A.getText().toString().replace("#", F2));
    	lblpregunta514A.setText(lblpregunta514A.getText().toString().replace("$", F1));
    	    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta514A.setText(lblpregunta514A.getText().toString().replace("%", F3));    	
    	
    }

	public void onqrgQI513BChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI513BU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N2,txtQI513N3);
			individual.qi513bn=null;
			Util.lockView(getActivity(),false,txtQI513N1);				
			txtQI513N1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI513BU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N1,txtQI513N3);	
			individual.qi513bn=null;
			Util.lockView(getActivity(),false,txtQI513N2);			
			txtQI513N2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI513BU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI513N1,txtQI513N2);	
			individual.qi513bn=null;
			Util.lockView(getActivity(),false,txtQI513N3);			
			txtQI513N3.requestFocus();				
		}		
	}
	
	public void onqrgQS514ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI514.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI514A);  		
			q3.setVisibility(View.GONE);
			rgQI515.requestFocus();
		} 
		else {
			Util.lockView(getActivity(),false,rgQI514A); 
			q3.setVisibility(View.VISIBLE);
			rgQI514A.requestFocus(); 
		}
		if (individual.qi516b!=null && individual.qi516b==98) {
    		chbP516B.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI516B);
    		txtQI516B.setVisibility(View.GONE);
		}
	}
	
	public void onqrgQS515ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI515.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI516U,txtQI516N,txtQI516B,rgQI516C,rgQI516D);  		
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			rgQI516E.requestFocus();
		} 
		else {
			Util.lockView(getActivity(),false,rgQI516U,txtQI516N);
			q5.setVisibility(View.VISIBLE);
			
			if (MyUtil.incluyeRango(25,49 , individualS1.qi106)) {
				Util.cleanAndLockView(getActivity(),txtQI516B,rgQI516C,rgQI516D);  		
				q6.setVisibility(View.GONE);
				q7.setVisibility(View.GONE);
				q8.setVisibility(View.GONE);
			}
			else{
				Util.lockView(getActivity(),false,txtQI516B,rgQI516C,rgQI516D); 
				q6.setVisibility(View.VISIBLE);
				q7.setVisibility(View.VISIBLE);
				q8.setVisibility(View.VISIBLE);	
				onqrgQI516ChangeValue();
				onqrgQS516BChangeValue();	
				rgQI516U.requestFocus(); 
			}
	
		}
		if (individual.qi516b!=null && individual.qi516b==98) {
    		chbP516B.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI516B);
    		txtQI516B.setVisibility(View.GONE);
		}
	}
	
	public void onqrgQS516BChangeValue() {
		if (MyUtil.incluyeRango(10,97,txtQI516B.getText().toString())) {
			Util.cleanAndLockView(getActivity(),rgQI516C,rgQI516D);  		
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			rgQI516E.requestFocus();
		} 
		else {
			Util.lockView(getActivity(),false,rgQI516C,rgQI516D); 
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			onqrgQS516CChangeValue();
			rgQI516C.requestFocus(); 
		}
	}
	
	public void onqrgQS516CChangeValue() {
		if (MyUtil.incluyeRango(2,8,rgQI516C.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI516D);  		
			q8.setVisibility(View.GONE);
			rgQI516E.requestFocus();
		} 
		else {
			Util.lockView(getActivity(),false,rgQI516D); 
			q8.setVisibility(View.VISIBLE);
			rgQI516D.requestFocus(); 
		}
	}
	
	public void onqrgQS516EChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI516E.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI516F);  		
			q10.setVisibility(View.GONE);
			rgQI517.requestFocus();
		} 
		else {
			Util.lockView(getActivity(),false,rgQI516F); 
			q10.setVisibility(View.VISIBLE);
			rgQI516F.requestFocus(); 
		}
	}
	
	public void onch521ChangeValue() {
		if (individual.qi521!=null && individual.qi521==98) {
			chbP521.setChecked(true);
			Util.cleanAndLockView(getActivity(),txtQI521);
			txtQI521.setVisibility(View.GONE);
		}
	}
	
	
	public void onqrgQI516ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI516U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI516N2,txtQI516N3);
			individual.qi516n=null;
			Util.lockView(getActivity(),false,txtQI516N1);				
			txtQI516N1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI516U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI516N1,txtQI516N3);	
			individual.qi516n=null;
			Util.lockView(getActivity(),false,txtQI516N2);			
			txtQI516N2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI516U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI516N1,txtQI516N2);	
			individual.qi516n=null;
			Util.lockView(getActivity(),false,txtQI516N3);			
			txtQI516N3.requestFocus();				
		}		
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI513BU.readOnly(); 
			rgQI514.readOnly(); 
			rgQI514A.readOnly(); 
			rgQI515.readOnly(); 
			txtQI513N1.readOnly();
			txtQI513N2.readOnly();
			txtQI513N3.readOnly();
			txtQI516N1.readOnly();
			txtQI516N2.readOnly();
			txtQI516N3.readOnly();
			txtQI515X.readOnly(); 
			rgQI516U.readOnly(); 
			txtQI516B.readOnly(); 
			rgQI516C.readOnly(); 
			rgQI516D.readOnly(); 
			rgQI516E.readOnly(); 
			rgQI516F.readOnly(); 
			rgQI517.readOnly(); 
			txtQI521.readOnly();
			chbP516B.readOnly();
			chbP521.readOnly();
			btnAceptar.setEnabled(false);
		}
	}

    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		}
		return cuestionarioService; 
    } 
    

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_05Fragment_004Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.CSVISITA);
	}

}
