package gob.inei.endes2024.fragment.CIseccion_05_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_COL04;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.R.id;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;


public class CISECCION_05Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI500A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI500B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI500C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI500D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI501; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI502;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI505; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI506; 
//	@FieldAnnotation(orderIndex=9) 
	public SpinnerField spnQI507; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI508; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI508A; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI508AX;

	
	CISECCION_05_07 individual; 
	CISECCION_04B2 individual4B2;
	Seccion01 persona;
	CISECCION_01_03 seccion01_03;
	private CuestionarioService cuestionarioService; 
	private Seccion01Service personaservice;
	private LabelComponent lblTitulo,lblpregunta500,lblpregunta500A,lblpregunta500B,lblpregunta500C,lblpregunta500D,lblpregunta501,lblpregunta502,lblpregunta503,lblpregunta504,lblpregunta505,lblpregunta506,lblpregunta507,lblpregunta508,lblpregunta508A;
	private GridComponent2 gridPreguntas500;
	public TextField txtCabecera;
	
	private Calendar fecha_actual=null;//Calendar.getInstance();
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoCalendario,seccionesCargadoTramosCol04; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCalendario,seccionesGrabadoTramosCol04,seccionesCargado4B2,seccionesCargadoTramosCalendario,seccionesCargadoSeccion01CH,SeccionescargadoSeccion01_03; 

	public CISECCION_05Fragment_001() {} 
	public CISECCION_05Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI500A","QI500B","QI500C","QI500D","QI501","QI502","QI505","QI506","QI507","QI508","QI508A","QI508AX","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI500A","QI500B","QI500C","QI500D","QI501","QI502","QI505","QI506","QI507","QI508","QI508A","QI508AX")};
		seccionesCargadoCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIINDICE","QIMES_ID","QIANIO","QICOL4","QITRAMO_ID")};
		seccionesGrabadoCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIINDICE","QIMES_ID","QIANIO","QICOL4","QITRAMO_ID")};
		seccionesGrabadoTramosCol04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QITRAMO_ID","QICANTIDAD")};
		seccionesCargadoTramosCol04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QITRAMO_ID")};
		seccionesCargado4B2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI490","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoTramosCalendario = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QITRAMO_ID","QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QITRAMO_ID")};
		seccionesCargadoSeccion01CH = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH03", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","ID","HOGAR_ID","PERSONA_ID")};
		SeccionescargadoSeccion01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QICAL_CONS","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_05_07).textSize(21).centrar().negrita(); 
		rgQI500A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi500a_1,R.string.c2seccion_05_07qi500a_2).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI500B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi500b_1,R.string.c2seccion_05_07qi500b_2).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI500C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi500c_1,R.string.c2seccion_05_07qi500c_2).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI500D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi500d_1,R.string.c2seccion_05_07qi500d_2).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI501=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi501_1,R.string.c2seccion_05_07qi501_2,R.string.c2seccion_05_07qi501_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS501ChangeValue"); 
		rgQI502=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi502_1,R.string.c2seccion_05_07qi502_2,R.string.c2seccion_05_07qi502_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS502ChangeValue"); 
		rgQI505=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi505_1,R.string.c2seccion_05_07qi505_2,R.string.c2seccion_05_07qi505_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS505ChangeValue"); 
		rgQI506=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi506_1,R.string.c2seccion_05_07qi506_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS506ChangeValue"); 
		spnQI507 = new SpinnerField(getActivity()).size(altoComponente, 450);
		rgQI508=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi508_1,R.string.c2seccion_05_07qi508_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS508ChangeValue"); 
		rgQI508A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_05_07qi508a_1,R.string.c2seccion_05_07qi508a_2,R.string.c2seccion_05_07qi508a_3,R.string.c2seccion_05_07qi508a_4,R.string.c2seccion_05_07qi508a_5,R.string.c2seccion_05_07qi508a_6,R.string.c2seccion_05_07qi508a_7,R.string.c2seccion_05_07qi508a_8,R.string.c2seccion_05_07qi508a_9,R.string.c2seccion_05_07qi508a_10,R.string.c2seccion_05_07qi508a_11,R.string.c2seccion_05_07qi508a_12).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI508AX=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI508A.agregarEspecifique(11, txtQI508AX);
		
		
		lblpregunta500 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_05_07qi500);
		lblpregunta500A = new LabelComponent(this.getActivity()).size(altoComponente, 400).textSize(16).text(R.string.c2seccion_05_07qi500a);
		lblpregunta500B = new LabelComponent(this.getActivity()).size(altoComponente, 400).textSize(16).text(R.string.c2seccion_05_07qi500b);
		lblpregunta500C = new LabelComponent(this.getActivity()).size(altoComponente, 400).textSize(16).text(R.string.c2seccion_05_07qi500c);
		lblpregunta500D = new LabelComponent(this.getActivity()).size(altoComponente, 400).textSize(16).text(R.string.c2seccion_05_07qi500d);
		lblpregunta501 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi501);
		lblpregunta502 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi502);
		lblpregunta503 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_05_07qi503);
		lblpregunta504 = new LabelComponent(this.getActivity()).size(altoComponente, MATCH_PARENT).textSize(17).text(R.string.c2seccion_05_07qi504);
		lblpregunta505 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi505);
		lblpregunta506 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi506);
		lblpregunta507 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_05_07qi507);
		lblpregunta508 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi508);
		lblpregunta508A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_05_07qi508a);
		
		gridPreguntas500=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas500.addComponent(lblpregunta500A);	
		gridPreguntas500.addComponent(rgQI500A);		
		gridPreguntas500.addComponent(lblpregunta500B);
		gridPreguntas500.addComponent(rgQI500B);	
		gridPreguntas500.addComponent(lblpregunta500C);
		gridPreguntas500.addComponent(rgQI500C);
		gridPreguntas500.addComponent(lblpregunta500D);
		gridPreguntas500.addComponent(rgQI500D);
		
  } 
  
  @Override 
  protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta500,gridPreguntas500.component()); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta501,rgQI501); 
		q3 = createQuestionSection(lblpregunta502,rgQI502);
//		q4 = createQuestionSection(lblpregunta503,lblpregunta504);
		q5 = createQuestionSection(lblpregunta505,rgQI505);		
		q6 = createQuestionSection(lblpregunta506,rgQI506); 
		q7 = createQuestionSection(lblpregunta507,spnQI507); 
		q8 = createQuestionSection(lblpregunta508,rgQI508); 
		q9 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta508A,rgQI508A); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
//		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
    return contenedor; 
  } 
  
  @Override 
  public boolean grabar() { 
	  uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion05_07()!=null) {
			App.getInstance().getCiseccion05_07().qi501=individual.qi501;
			App.getInstance().getCiseccion05_07().qi502=individual.qi502;
		}
		else 
			App.getInstance().setCiseccion05_07(individual);
		
		try { 
			GrabarCalendario();
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(App.getInstance().getCiseccion05_07()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null){
			App.getInstance().getCiseccion05_07().qi501=individual.qi501;
			App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3=getCuestionarioService().getCompletadoSeccion0103CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		}
		else{
			App.getInstance().setCiseccion05_07(individual);
		}
		return true; 
    } 
  
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(individual.qi500a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI500A"); 
			view = rgQI500A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi500b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI500B"); 
			view = rgQI500B; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi500c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI500C"); 
			view = rgQI500C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi500d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI500D"); 
			view = rgQI500D; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi501)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI501"); 
			view = rgQI501; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi501,3)) {
			if (Util.esVacio(individual.qi502)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI502"); 
				view = rgQI502; 
				error = true; 
				return false; 
			}
		}
		if (!Util.esDiferente(individual.qi502,1,2)) {
			if (Util.esVacio(individual.qi505)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI505"); 
				view = rgQI505; 
				error = true; 
				return false; 
			} 
		}
		if (!Util.esDiferente(individual.qi501,1,2)) {
			if (Util.esVacio(individual.qi506)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI506"); 
				view = rgQI506; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi507)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI507"); 
				view = spnQI507; 
				error = true; 
				return false; 
			}
		}
		if (Util.esDiferente(individual.qi502,3)) {
			if (Util.esVacio(individual.qi508)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI508"); 
				view = rgQI508; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(individual.qi508,2) ) {
	    		if (Util.esDiferente(individual.qi508,1)) {
					if (Util.esVacio(individual.qi508a)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI508A"); 
						view = rgQI508A; 
						error = true; 
						return false; 
					} 
					if (!Util.esDiferente(individual.qi508,11)) {
						if (Util.esVacio(individual.qi508ax)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI508AX"); 
							view = txtQI508AX; 
							error = true; 
							return false; 
						}
					}
				}
			}
		}
		
		return true; 
    } 
    
    public void GrabarCalendario(){
    	CICALENDARIO_TRAMO_COL4 tramocol4 = new CICALENDARIO_TRAMO_COL4();
    	List<CICALENDARIO_TRAMO_COL4> tramoscol4 = getCuestionarioService().getTramoscol04ByPersona(individual.id, individual.hogar_id,individual.persona_id,seccionesCargadoTramosCol04);
    	Integer tramo_id=-1;
    	if(!Util.esDiferente(individual.qi501, 3) && !Util.esDiferente(individual.qi502, 3)){
        	if(tramoscol4.size()>0){
        		for(CICALENDARIO_TRAMO_COL4 tramo: tramoscol4){
            		getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, individual.id, individual.hogar_id, individual.persona_id, tramo.qitramo_id);
        		}
        	}
    		tramocol4.id=individual.id;
        	tramocol4.hogar_id=individual.hogar_id;
        	tramocol4.persona_id=individual.persona_id;
        	tramocol4.qianio_ini = App.ANIOPORDEFECTO;
        	tramocol4.qimes_ini = App.MESPORDEFECTO;
        	tramocol4.qimes_fin = fecha_actual.get(Calendar.MONTH)+1;
        	tramocol4.qianio_fin = fecha_actual.get(Calendar.YEAR);
        	tramocol4.qicantidad = MyUtil.DeterminarIndice(tramocol4.qianio_ini, tramocol4.qimes_ini)-MyUtil.DeterminarIndice(tramocol4.qianio_fin, tramocol4.qimes_fin);
        	tramocol4.qicantidad=tramocol4.qicantidad+1;
        	tramocol4.qiresult ="0";
        	try {
    			tramo_id = getCuestionarioService().saveOrUpdate(tramocol4, null, seccionesGrabadoTramosCol04);
    		} catch (SQLException e) {
    			// TODO: handle exception
    		}
    	}
    }
    
    @Override 
    public void cargarDatos() { 
		individual = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		MyUtil.llenarEspososParaMef(getActivity(), getPersonaService(), spnQI507, App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id);
		persona = getCuestionarioService().getSeccion01(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoSeccion01CH);
		seccion01_03 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,SeccionescargadoSeccion01_03);
		if (individual == null) {
			individual = new CISECCION_05_07();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(seccion01_03!=null && seccion01_03.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(seccion01_03.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		individual4B2 = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado4B2);
		App.getInstance().getSeccion04B2().qi490=individual4B2.qi490;
		
		entityToUI(individual); 
		inicio(); 
    } 
    
    private void inicio() { 
    	onqrgQS501ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	RenombrarEtiquetas();
    } 
    public void RenombrarEtiquetas(){
    	lblpregunta503.setText(getResources().getString(R.string.c2seccion_05_07qi503));
    	lblpregunta503.setText(lblpregunta503.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    }
    
    public void onqrgQS501ChangeValue() {
    	
    	if (MyUtil.incluyeRango(1,2,rgQI501.getTagSelected("").toString())) {
    		Log.e("","1");
    		Util.cleanAndLockView(getActivity(),rgQI502,rgQI505);
    		MyUtil.LiberarMemoria();
    		q3.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
			Util.lockView(getActivity(),false,rgQI506,spnQI507,rgQI508,rgQI508A);  
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			onqrgQS508ChangeValue();
    		rgQI506.requestFocus();
    	} 
    	else {
    		Log.e("","2");
    		Util.lockView(getActivity(),false,rgQI502,rgQI505,rgQI508A); 
    		q3.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		onqrgQS502ChangeValue();
    		onqrgQS505ChangeValue();
    		rgQI502.requestFocus(); 
    	}
    	Integer valor =Integer.parseInt(rgQI501.getTagSelected("0").toString());
    	if(!Util.esDiferente(persona.qh03, 1) && valor!=0){
    		if(!Util.esDiferente(persona.qh08, 2) && Util.esDiferente(valor, 1)){
    			Mensaje("Verifique C.H. Mef casada");
    		}
    		if(!Util.esDiferente(persona.qh08, 1) && Util.esDiferente(valor, 2)){
    			Mensaje("Verifique C.H. Mef conviviente");
    		}
    		if(Util.esDiferente(persona.qh08, 1,2) && Util.esDiferente(valor, 3)){
    			Mensaje("Verifique C.H. Mef no en Uni�n");
    		}
    	}
    } 
    public void Mensaje(String msj){
    	ToastMessage.msgBox(this.getActivity(), msj, ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    }
    public void onqrgQS502ChangeValue() {
    	if (!MyUtil.incluyeRango(1,2,rgQI501.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(3,3,rgQI502.getTagSelected("").toString())) {
    			Log.e("","3");
    			MyUtil.LiberarMemoria();
    			Util.cleanAndLockView(getActivity(),rgQI505,rgQI506,spnQI507,rgQI508,rgQI508A); 
    			q5.setVisibility(View.GONE);
        		q6.setVisibility(View.GONE);
        		q7.setVisibility(View.GONE);
        		q8.setVisibility(View.GONE);
        		q9.setVisibility(View.GONE);
    		}
    		else{
    			Log.e("","4");
    			Util.lockView(getActivity(),false,rgQI505,rgQI506,spnQI507,rgQI508,rgQI508A);  
    			q5.setVisibility(View.VISIBLE);
//        		q6.setVisibility(View.VISIBLE);
//        		q7.setVisibility(View.VISIBLE);
        		q8.setVisibility(View.VISIBLE);
        		q9.setVisibility(View.VISIBLE);
        		onqrgQS508ChangeValue();
        		rgQI505.requestFocus();
    		}
    	}  		
  	}
    
    

    public void onqrgQS505ChangeValue() {
    	if (MyUtil.incluyeRango(1,3,rgQI505.getTagSelected("").toString())) {
    		Log.e("","7");
			Util.cleanAndLockView(getActivity(),rgQI506,spnQI507);
			MyUtil.LiberarMemoria();
			MyUtil.LiberarMemoria();
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			rgQI508.requestFocus();
  		} 
	}  
    
    public void onqrgQS506ChangeValue() {
    	if (MyUtil.incluyeRango(1,2,rgQI506.getTagSelected("").toString())) {
    		Log.e("","77");
			spnQI507.requestFocus();
  		} 
	} 
    
//    public void onqrgQS508ChangeValue() {
//    	if (MyUtil.incluyeRango(1,1,rgQI508.getTagSelected("").toString())) {
//    		Log.e("","8");
//			Util.cleanAndLockView(getActivity(),rgQI508A);
//			MyUtil.LiberarMemoria();
//			q9.setVisibility(View.GONE);
//  		} 
//    	else{
//    		Util.lockView(getActivity(),false,rgQI508A);  
//			q9.setVisibility(View.VISIBLE);
//			rgQI508A.requestFocus();
//    	}
//	}
    
    public void onqrgQS508ChangeValue() {
    	if (MyUtil.incluyeRango(1,1,rgQI508.getTagSelected("").toString())) {
    		Log.e("","8");
			Util.cleanAndLockView(getActivity(),rgQI508A);
			MyUtil.LiberarMemoria();
			q9.setVisibility(View.GONE);
  		} 
    	else{
    		Util.lockView(getActivity(),false,rgQI508A);  
			q9.setVisibility(View.VISIBLE);
			rgQI508A.requestFocus();
    	}
	}
    
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public Seccion01Service getPersonaService(){
    	if(personaservice==null){
    		personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return personaservice;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI500A.readOnly();
    		rgQI500B.readOnly();
    		rgQI500C.readOnly();
    		rgQI500D.readOnly();
    		rgQI501.readOnly();
    		rgQI502.readOnly();
    		rgQI505.readOnly();
    		rgQI506.readOnly();
    		rgQI508.readOnly();
    		rgQI508A.readOnly();
    		txtQI508AX.readOnly();
    		spnQI507.readOnly();
    	}
    }
    
	@Override
	public Integer grabadoParcial() {
		  uiToEntity(individual); 
			
			if(App.getInstance().getCiseccion05_07()!=null) {
				App.getInstance().getCiseccion05_07().qi501=individual.qi501;
				App.getInstance().getCiseccion05_07().qi502=individual.qi502;
			}
			else 
				App.getInstance().setCiseccion05_07(individual);
			
			try { 
				GrabarCalendario();
				if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return App.NODEFINIDO; 
				} 
			} catch (SQLException e) { 
				ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			return App.INDIVIDUAL; 
		} 
	} 

