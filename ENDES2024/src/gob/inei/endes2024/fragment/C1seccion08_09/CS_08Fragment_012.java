package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_012 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQS838_A; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQS838_B; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQS838_C; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS838_D; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS838_E; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQS838_F; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQS838_G; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS838_H; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQS838_I; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQS838_J; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQS838_K; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQS838_L; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQS838_M; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQS838_N; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQS838_X; 
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQS838_O; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQS838_Y; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQS838_Z; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblhora,lblhoras,lblpregunta838_ind,lblhoratermino,lblSubtitulo; 
	public GridComponent2 gridPreguntas838A;
	public ButtonComponent btnHoraInicio;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2;
	LinearLayout q3;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoSeccion01;
	public List<Seccion01> detalles;
	public LabelComponent lblpregunta838;
	public CS_08Fragment_012() {} 
	public CS_08Fragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z","QS838A_H","QS838A_M","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS806","QS833")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z","QS838A_H","QS838A_M")};
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","ID","HOGAR_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_6_11_MENTAL).negrita().centrar().colorFondo(R.color.griscabece);
	  
	  chbQS838_A=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838aChangeValue"); 
	  chbQS838_B=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838bChangeValue"); 
	  chbQS838_C=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838cChangeValue"); 
	  chbQS838_D=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838dChangeValue"); 
	  chbQS838_E=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838eChangeValue"); 
	  chbQS838_F=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838fChangeValue"); 
	  chbQS838_G=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838gChangeValue"); 
	  chbQS838_H=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838hChangeValue"); 
	  chbQS838_I=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838iChangeValue"); 
	  chbQS838_J=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838jChangeValue"); 
	  chbQS838_K=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838kChangeValue"); 
	  chbQS838_L=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838lChangeValue"); 
	  chbQS838_M=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838mChangeValue"); 
	  chbQS838_N=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838nChangeValue"); 
	  chbQS838_X=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838xChangeValue"); 
	  txtQS838_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	  chbQS838_Y=new CheckBoxField(this.getActivity(), R.string.cap08_09qs838_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838yChangeValue"); 
	  chbQS838_Z=new CheckBoxField(this.getActivity(),R.string.cap08_09qs838_z,"1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS838ZChangeValue");
	  lblhora = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 300).text(R.string.cap08_09qs838a_m);
	  btnHoraInicio = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_finalizar).size(200, 55);	  
	  lblhoras = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 100).text("");
	  
	  lblpregunta838 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs838).textSize(19);
	  lblpregunta838_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs838_ind).textSize(18).negrita();
	  lblhoratermino = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs838a).textSize(18);
	  gridPreguntas838A = new GridComponent2(this.getActivity(),App.ESTILO,3);
	  gridPreguntas838A.addComponent(lblhora);		
	  gridPreguntas838A.addComponent(btnHoraInicio);
	  gridPreguntas838A.addComponent(lblhoras);
	
	  btnHoraInicio.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar calendario = new GregorianCalendar();
				Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
				Integer minute= calendario.get(Calendar.MINUTE);
				String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
				String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
				lblhoras.setText(muestrahora+":"+muestraminuto);
			}
		});
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 	
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta838,lblpregunta838_ind,chbQS838_A,chbQS838_B,chbQS838_C,chbQS838_D,chbQS838_E,chbQS838_F,chbQS838_G,chbQS838_H,chbQS838_I,chbQS838_J,chbQS838_K,chbQS838_L,chbQS838_M,chbQS838_N,chbQS838_X,txtQS838_O,chbQS838_Y,chbQS838_Z); 
		q3 = createQuestionSection(lblhoratermino,gridPreguntas838A.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);	 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);  
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		salud.qs838a_h = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		salud.qs838a_m = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 	
		
		
			if (Util.esMayor(salud.qs802d,5)) {
				if (!Util.esDiferente(salud.qs833,1)) {
					if (!Util.alMenosUnoEsDiferenteA(0,salud.qs838_a,salud.qs838_b,salud.qs838_c,salud.qs838_d,salud.qs838_e,salud.qs838_f,salud.qs838_g,salud.qs838_h,salud.qs838_i,salud.qs838_j,salud.qs838_k,salud.qs838_l,salud.qs838_m,salud.qs838_n,salud.qs838_x,salud.qs838_y,salud.qs838_z)) { 
						mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
						view = chbQS838_A; 
						error = true; 
						return false; 
					} 
					if (chbQS838_X.isChecked()) {
						if (Util.esVacio(salud.qs838_o)) { 
							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
							view = txtQS838_O; 
							error = true; 
							return false; 
						}
					}	
				}
			}
		

		if (Util.esVacio(salud.qs838a_h)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.838A"); 
			view = btnHoraInicio; 
			error = true; 
			return false; 
		}
  
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	if (App.getInstance().getPersonabySeccion8Salud()!=null) {
    	   	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);

        	if(salud==null){ 
        		salud=new CSSECCION_08(); 
        		salud.id=App.getInstance().getPersonaSeccion01().id; 
        		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
        		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
        	}
        	if(salud.qs838a_h!=null && salud.qs838a_m!=null)
        	{
        		lblhoras.setText(salud.qs838a_h.toString()+":"+salud.qs838a_m.toString());
        	}
        	
        	entityToUI(salud); 
        	inicio();
		}
   
    } 
    public void cargarpaseedad(Integer edad) {   
    	//if (Util.esMenor(edad,6) || Util.esDiferente(salud.qs806,1) || Util.esDiferente(salud.qs833,1)) {
    	if (Util.esMenor(edad,6) || Util.esDiferente(salud.qs833,1)) {
    		Util.cleanAndLockView(getActivity(),chbQS838_A,chbQS838_B,chbQS838_C,chbQS838_D,chbQS838_E,chbQS838_F,chbQS838_G,chbQS838_H,chbQS838_I,chbQS838_J,chbQS838_K,chbQS838_L,chbQS838_M,chbQS838_N,chbQS838_X,chbQS838_Y,txtQS838_O);
 			q2.setVisibility(View.GONE);
		}
 		else{
 			Util.lockView(getActivity(), false,chbQS838_A,chbQS838_B,chbQS838_C,chbQS838_D,chbQS838_E,chbQS838_F,chbQS838_G,chbQS838_H,chbQS838_I,chbQS838_J,chbQS838_K,chbQS838_L,chbQS838_M,chbQS838_N,chbQS838_X,chbQS838_Y);
 			q2.setVisibility(View.VISIBLE);
 			onqrgQS838aChangeValue();
 			onqrgQS838bChangeValue();
 			onqrgQS838cChangeValue();
 			onqrgQS838dChangeValue();
 			onqrgQS838eChangeValue();
 			onqrgQS838fChangeValue();
 			onqrgQS838gChangeValue();
 			onqrgQS838hChangeValue();
 			onqrgQS838iChangeValue();
 			onqrgQS838jChangeValue();
 			onqrgQS838kChangeValue();
 			onqrgQS838lChangeValue();
 			onqrgQS838mChangeValue();
 			onqrgQS838nChangeValue();
 			onqrgQS838xChangeValue();
 			onqrgQS838yChangeValue();  
 		} 
    }
 	
    private void inicio() { 
    	cargarpaseedad(salud.qs802d);
    	ValidarsiesSupervisora();
    	RenombrarEtiquetas();
    	txtCabecera.requestFocus();
    	
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQS838_A.readOnly();
			chbQS838_B.readOnly();
			chbQS838_C.readOnly();
			chbQS838_D.readOnly();
			chbQS838_E.readOnly();
			chbQS838_F.readOnly();
			chbQS838_G.readOnly();
			chbQS838_H.readOnly();
			chbQS838_I.readOnly();
			chbQS838_J.readOnly();
			chbQS838_K.readOnly();
			chbQS838_L.readOnly();
			chbQS838_M.readOnly();
			chbQS838_N.readOnly();
			chbQS838_X.readOnly();
			chbQS838_Y.readOnly();
			chbQS838_Z.readOnly();
			txtQS838_O.readOnly();
			Util.cleanAndLockView(getActivity(), btnHoraInicio);
		}
	}
    
    public void RenombrarEtiquetas()
    {
    	String replace="(NOMBRE)";
    	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	lblpregunta838.setText(lblpregunta838.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public boolean verificarCheck() {
  		if (chbQS838_A.isChecked() || chbQS838_B.isChecked() || chbQS838_C.isChecked() || chbQS838_D.isChecked() || chbQS838_E.isChecked() || chbQS838_F.isChecked()
  								   || chbQS838_G.isChecked() || chbQS838_H.isChecked() || chbQS838_I.isChecked() || chbQS838_J.isChecked() || chbQS838_K.isChecked()
  								   || chbQS838_L.isChecked() || chbQS838_M.isChecked() || chbQS838_N.isChecked() || chbQS838_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    
    public void onqrgQS838aChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838bChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838cChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838dChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838eChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838fChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838gChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838hChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838iChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838jChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838kChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838lChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838mChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    public void onqrgQS838nChangeValue() { if (verificarCheck()) {Util.cleanAndLockView(getActivity(),chbQS838_Y);} else {Util.lockView(getActivity(),false,chbQS838_Y); }}
    
    public void onqrgQS838xChangeValue() {  	
    	if (chbQS838_X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS838_O);
  			if (verificarCheck()) {  		
  				Util.cleanAndLockView(getActivity(),chbQS838_Y);
  			}
  			txtQS838_O.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQS838_O);  
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS838_Y);  				
			}
  		}	
    }
    public void onqrgQS838yChangeValue() {    	
  		if (chbQS838_Y.isChecked()){	  
  			Util.cleanAndLockView(getActivity(),chbQS838_A,chbQS838_B,chbQS838_C,chbQS838_D,chbQS838_E,chbQS838_F,chbQS838_G,chbQS838_H,chbQS838_I,chbQS838_J,chbQS838_K,chbQS838_L,chbQS838_M,chbQS838_N,chbQS838_X);  	
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS838_A,chbQS838_B,chbQS838_C,chbQS838_D,chbQS838_E,chbQS838_F,chbQS838_G,chbQS838_H,chbQS838_I,chbQS838_J,chbQS838_K,chbQS838_L,chbQS838_M,chbQS838_N,chbQS838_X);  					
  		}	
  	}
    public void onqrgQS838ZChangeValue(){
    	detalles = getCuestionarioService().getListadoSeccion8Salud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion01);
    	if(!Util.esDiferente(detalles.get(0).persona_id, salud.persona_id_ninio)){
      		Util.cleanAndLockView(getActivity(), chbQS838_Z);
    	}
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		salud.qs838a_h = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		salud.qs838a_m = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
