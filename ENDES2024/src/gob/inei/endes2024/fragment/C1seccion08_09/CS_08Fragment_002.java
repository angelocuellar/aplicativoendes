package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_002 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public SpinnerField spnQS802; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS802A; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQS802A_O; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS802CD; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQS802CM; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS802CA; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS802D; 
	
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblhora,lblfechanac,lbledad,lblhoras,lblpregunta802c,lblpregunta802d,lblpregunta802,lblpregunta802A,lblpregunta802B,lblfechanacDia,lblfechanacMes,lblfechanacAnio,lblEspacio; 
	public GridComponent2 gridPreguntas802B,gridPreguntas802C,gridPreguntas802D;
	public ButtonComponent btnHoraInicio;
	private Seccion01Service serviceSeccion01;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 

	String fechareferencia;
	
	SeccionCapitulo[] seccionesGrabado,seccionesGrabado0,seccionesGrabado1A2,seccionesGrabado3A5,seccionesGrabado802A; 
	SeccionCapitulo[] seccionesCargado; 

	public CS_08Fragment_002() {} 
	public CS_08Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
	  rootView = createUI(); 
	  initObjectsWithoutXML(this, rootView); 
	  enlazarCajas(); 
	  listening(); 
	  seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS802","QS802N","QS802A","QS802A_O","QS802BH","QS802BM","QS802CD","QS802CM","QS802CA","QS802D","QS802CONS","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO")}; 
	  seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS802","QS802N","QS802A","QS802A_O","QS802BH","QS802BM","QS802CD","QS802CM","QS802CA","QS802D","QS802CONS")}; 
	
	  seccionesGrabado802A = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS803","QS804","QS804C","QS805","QS805_O","QS806","QS807_A","QS807_B","QS807_C","QS807_D","QS807_E","QS807_F","QS807_G","QS807_H","QS807_I","QS807_J","QS807_X","QS807_O","QS807_Y",
																			   "QS809","QS810","QS811","QS812","QS812A","QS813","QS814",
																			   "QS817","QS818","QS818A","QS819","QS819_O","QS820","QS821A","QS821B","QS821C","QS821D","QS821X","QS821_O","QS821Y","QS822","QS822A","QS823","QS823_O","QS824","QS825","QS825_O","QS826","QS827","QS827_O","QS828","QS829",
																			   "QS831","QS832","QS833","QS834","QS835","QS836","QS837","QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z","QS838A_H","QS838A_M",
																			   "QS840A","QS840B","QS840_O")};
		
	  seccionesGrabado0 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS809","QS810","QS811","QS812","QS812A","QS813","QS814",
																			"QS817","QS818","QS818A","QS819","QS819_O","QS820","QS821A","QS821B","QS821C","QS821D","QS821X","QS821_O","QS821Y","QS822","QS822A","QS823","QS823_O","QS824","QS825","QS825_O","QS826","QS827","QS827_O","QS828","QS829",
																			"QS831","QS832","QS833","QS834","QS835","QS836","QS837","QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z",
																			"QS840A","QS840B","QS840_O")};
		
	  seccionesGrabado1A2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS817","QS818","QS818A","QS819","QS819_O","QS820","QS821A","QS821B","QS821C","QS821D","QS821X","QS821_O","QS821Y","QS822","QS822A","QS823","QS823_O","QS824","QS825","QS825_O","QS826","QS827","QS827_O","QS828","QS829",
																			  "QS831","QS832","QS833","QS834","QS835","QS836","QS837","QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z")};
		
	  seccionesGrabado3A5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS831","QS832","QS833","QS834","QS835","QS836","QS837","QS838_A","QS838_B","QS838_C","QS838_D","QS838_E","QS838_F","QS838_G","QS838_H","QS838_I","QS838_J","QS838_K","QS838_L","QS838_M","QS838_N","QS838_X","QS838_O","QS838_Y","QS838_Z")};

		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_0_11_BUCAL).negrita().centrar().colorFondo(R.color.griscabece);

		Spanned texto = Html.fromHtml("802. PREGUNTE POR LA PERSONA (DE 15 A�OS A M�S) <b>RESPONSABLE DE LA SALUD DE LA NI�A O NI�O DE 0 A 11 A�OS</b>. VERIFIQUE LAS PREGUNTAS: <b>2 Y 7</b> DEL CUESTIONARIO DEL HOGAR Y TRANSCRIBA EL N�MERO DE ORDEN Y NOMBRE DE ESTA PERSONA");
		lblpregunta802 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17);	
		lblpregunta802.setText(texto);
		spnQS802=new SpinnerField(getActivity()).size(altoComponente+15, 400);
				
		lblpregunta802A = new LabelComponent(getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs802a);
		rgQS802A=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs802a_1,R.string.cap08_09qs802a_2,R.string.cap08_09qs802a_3,R.string.cap08_09qs802a_4,R.string.cap08_09qs802a_5,R.string.cap08_09qs802a_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqs802ChangeValue"); 
		txtQS802A_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQS802A.agregarEspecifique(5,txtQS802A_O); 

		lblpregunta802B = new LabelComponent(getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs802b);
		lblhora = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 250).text(R.string.cap08_09qs802bh).centrar();
		btnHoraInicio = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(170, 55);
		lblhoras = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 100).text("").centrar();
				
		lblfechanac = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 280).text(R.string.cap08_09qs802cd).centrar();
		lblfechanacDia= new LabelComponent(getActivity()).textSize(17).size(altoComponente, 90).text(R.string.cap08_09qs802cdDia).centrar();
		lblfechanacMes = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 90).text(R.string.cap08_09qs802cdMes).centrar();
		lblfechanacAnio = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 150).text(R.string.cap08_09qs802cdAnio).centrar();
		lblEspacio = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 220);
		txtQS802CD=new IntegerField(this.getActivity()).size(altoComponente+10, 90).maxLength(2).centrar();
		txtQS802CM=new IntegerField(this.getActivity()).size(altoComponente+10, 90).maxLength(2).centrar();
		txtQS802CA=new IntegerField(this.getActivity()).size(altoComponente+10, 130).maxLength(4).centrar();
		
		lbledad = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 200).text(R.string.cap08_09qs802de);
		txtQS802D=new IntegerField(this.getActivity()).size(altoComponente+10, 90).maxLength(2);
		
		lblpregunta802c= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs802c);
		lblpregunta802d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs802d).textSize(19);
		
		gridPreguntas802B = new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas802B.addComponent(lblhora);		
		gridPreguntas802B.addComponent(btnHoraInicio);
		gridPreguntas802B.addComponent(lblhoras);
		
		gridPreguntas802C = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		gridPreguntas802C.addComponent(lblfechanac);		
		gridPreguntas802C.addComponent(txtQS802CD);
		gridPreguntas802C.addComponent(txtQS802CM);
		gridPreguntas802C.addComponent(txtQS802CA);
		gridPreguntas802C.addComponent(lblEspacio);
		gridPreguntas802C.addComponent(lblfechanacDia);
		gridPreguntas802C.addComponent(lblfechanacMes);
		gridPreguntas802C.addComponent(lblfechanacAnio);
		
		gridPreguntas802D = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas802D.addComponent(lbledad);		
		gridPreguntas802D.addComponent(txtQS802D);
		btnHoraInicio.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Calendar calendario = new GregorianCalendar();
			Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
			Integer minute= calendario.get(Calendar.MINUTE);
			String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
			String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
			lblhoras.setText(muestrahora+":"+muestraminuto);
			}
		});
			
	
  
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo); 
		q2 = createQuestionSection(lblpregunta802,spnQS802); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta802A,rgQS802A); 
		q4 = createQuestionSection(lblpregunta802B,gridPreguntas802B.component()); 
		q5 = createQuestionSection(lblpregunta802c,gridPreguntas802C.component());
		q6 = createQuestionSection(lblpregunta802d,gridPreguntas802D.component());

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {	
		
    	uiToEntity(salud); 
    	
    	if(salud!=null){
			salud.qs802bh = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
			salud.qs802bm = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
			if (salud.qs802a!=null) {
				salud.qs802a=salud.getConvertqs802a(salud.qs802a);
			}
		}
    		
		if (!validar() ) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs802cd == null && salud.qs802cm == null && salud.qs802ca == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia;
				}			
			}		
			
			
			if (salud.qs802d!=null) {
				if (salud.qs802d==0) {
					Log.e("1"," de 0 a�os");
					if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado) || !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado0)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 1.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					} 
				}
				else if (salud.qs802d>0 && salud.qs802d<3 ) {
					Log.e("1"," de 1 a 2 a�os");
					if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado) || !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado1A2)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 2.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					} 
				}
				else if (salud.qs802d>2 && salud.qs802d<6) {
					Log.e("1"," de 3 a 5 a�os");
					if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado) || !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado3A5)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 3.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					} 
				}
				else{
					Log.e("1"," de 6 a 11");
					if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 4.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}
			}
			Integer dato= Integer.parseInt(rgQS802A.getTagSelected("0").toString());
			if (!Util.esDiferente(dato,2,3,5,6)) {
				Log.e("1"," 802a = 2,3,5,6");
				
				
				if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado) || !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado802A)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 5.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				} 
			}			
			
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(salud.qs802d!=null){
			App.getInstance().getPersonabySeccion8Salud().qs802d=salud.qs802d;}
			App.getInstance().getPersonabySeccion8Salud().qs802a=salud.qs802a;
			return true; 
    } 
    
    @SuppressLint("SimpleDateFormat") 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if(salud==null){
			return false;
		}
		if(Util.esVacio(salud.qs802)){
			mensaje = preguntaVacia.replace("$", "la pregunta P.802"); 
			view = spnQS802; 
			error = true; 
			return false;
		}
		if(Util.esVacio(salud.qs802a)){
			mensaje = preguntaVacia.replace("$", "la pregunta P.802A"); 
			view = rgQS802A; 
			error = true; 
			return false;
		}
		if(!Util.esDiferente(salud.qs802a, 9)){
			if (Util.esVacio(salud.qs802a_o)) { 
				mensaje = "Especifique de P.802A es requerido"; 
				view = txtQS802A_O; 
				error = true; 
				return false; 
			} 
		}
		if(Util.esDiferente(salud.qs802a, 2,4,6,9)){
			if (Util.esVacio(salud.qs802bh)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.802B"); 
				view = btnHoraInicio; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs802cd)){ 
				mensaje = preguntaVacia.replace("$", "La pregunta P.802D"); 
				view = txtQS802CD; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(salud.qs802cm)){ 
				mensaje = preguntaVacia.replace("$", "La pregunta P.802M"); 
				view = txtQS802CM; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(salud.qs802ca)){ 
				mensaje = preguntaVacia.replace("$", "La pregunta P.802A"); 
				view = txtQS802CA; 
				error = true; 
				return false; 
			}			
			if (Util.esVacio(salud.qs802d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.802D"); 
				view = txtQS802D; 
				error = true; 
				return false; 
			}
			if(Util.esMenor(salud.qs802cd, 1) || Util.esMayor(salud.qs802cd, 31)){
				mensaje = "d�a fuera de rango"; 
				view = txtQS802CD; 
				error = true; 
				return false;
			}
			if (Util.esMenor(salud.qs802cm,1) || Util.esMayor(salud.qs802cm,12) ) { 
				mensaje = "Mes fuera de rango"; 
				view = txtQS802CM; 
				error = true; 
				return false; 
			}
			if(!MyUtil.DiaCorrespondeAlMes(salud.qs802cd, salud.qs802cm)){
				mensaje = "d�a no corresponde al mes"; 
				view = txtQS802CD; 
				error = true; 
				return false; 
			}
			if(Util.esMayor(salud.qs802d,11)){
				mensaje = "a�o fuera de rango"; 
				view = txtQS802D; 
				error = true; 
				return false;
			}
			
//			SeccionCapitulo[] seccionescargadovisita= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSVRESUL")};
//			CSVISITA visitacompletada = getCuestionarioService().getVisitaSaludCompletada(salud.id,salud.hogar_id, seccionescargadovisita);
//			if(visitacompletada==null || visitacompletada.qsvresul !=1){ 
				Calendar fechadenacimiento = new GregorianCalendar(salud.qs802ca, salud.qs802cm-1, salud.qs802cd);
				Calendar fecharef = new GregorianCalendar();
				if(fechareferencia!=null){
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date2 = null;
					try {
						date2 = df.parse(fechareferencia);
					} catch (ParseException e) {
						e.printStackTrace();
					}		
					Calendar cal = Calendar.getInstance();
					cal.setTime(date2);
					fecharef=cal;
				}
					
				if(Util.esDiferente(CalcularEdadbyFechaNacimiento(fechadenacimiento,fecharef), salud.qs802d)){
					mensaje = "Edad no corresponde con P.802C"; 
					view = txtQS802D; 
					error = true; 
					return false;
				}	
			}
		if(!validarSiexisteDiferenciaenfechaNacimientoS04_05(salud)){
			mensaje = "verifique fecha de nacimiento no es igual a la seccion 04"; 
			MyUtil.MensajeGeneral(getActivity(), mensaje); 
		}
		if(!validarSiexisteDiferenciaenfechaNacimientoCIS2(salud)){
			mensaje = "verifique fecha de nacimiento no es igual a la seccion 2 C. Individual"; 
			MyUtil.MensajeGeneral(getActivity(), mensaje);
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	if (App.getInstance().getPersonabySeccion8Salud()!=null){  	
    		salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	}
    	
    	MyUtil.llenarResponsableCuestionarioSalud(getActivity(),getServiceSeccion01() , spnQS802 ,App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id);
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	}
    	if (salud.qs802a!=null) {
			salud.qs802a=salud.setConvertqs802a(salud.qs802a);
		}
    	if(salud.qs802bh!=null && salud.qs802bm!=null){
    		lblhoras.setText(salud.qs802bh.toString()+":"+salud.qs802bm.toString());
    	}
    	
    	
       	entityToUI(salud);
       	fechareferencia = salud.qs802cons;
    	inicio();   
    } 
    
    private void inicio() { 
    	RenombrarEtiquetas();
    	onqs802ChangeValue();
    	ValidarsiesSupervisora();
//    	spnQS802.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			spnQS802.readOnly();
			rgQS802A.readOnly();
			txtQS802A_O.readOnly();
			Util.cleanAndLockView(getActivity(), btnHoraInicio);
			txtQS802CD.readOnly();
			txtQS802CM.readOnly();
			txtQS802CA.readOnly();
			txtQS802D.readOnly();
		}
	}
	
    public void RenombrarEtiquetas() {	    	
		if ( App.getInstance().getPersonabySeccion8Salud()!=null) {
			lblpregunta802c.setText(getResources().getString(R.string.cap08_09qs802c));
			lblpregunta802d.setText(getResources().getString(R.string.cap08_09qs802d));
    	
    		String replace="(NOMBRE)";
        	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
        	lblpregunta802c.setText(lblpregunta802c.getText().toString().replace(replace,nombre));
        	lblpregunta802d.setText(lblpregunta802d.getText().toString().replace(replace,nombre));
		}
    
    }
    
    public Integer CalcularEdadbyFechaNacimiento(Calendar fechadenacimiento) {
    	Calendar fechaactual=new GregorianCalendar();
    	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
    	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
    	if(mes==0){
    		if(Util.esMenor(fechaactual.get(Calendar.DAY_OF_MONTH),fechadenacimiento.get(Calendar.DAY_OF_MONTH))){
    			anio--;
    		}
    	}
    	else if(mes<0){
    		anio--;
    	}
    	return anio;
    }
    
    public Integer CalcularEdadbyFechaNacimiento(Calendar fechadenacimiento,Calendar fechareferencia) {
    	//Calendar fechaactual=new GregorianCalendar();
    	Calendar fechaactual=fechareferencia;
    	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
    	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
    	if(mes==0){
    		if(Util.esMenor(fechaactual.get(Calendar.DAY_OF_MONTH),fechadenacimiento.get(Calendar.DAY_OF_MONTH))){
    			anio--;
    		}
    	}
    	else if(mes<0){
    		anio--;
    	}
    	return anio;
    }
    
    public void onqs802ChangeValue(){
    	Integer dato= Integer.parseInt(rgQS802A.getTagSelected("0").toString());
    	if(!Util.esDiferente(dato, 2,3,5,6)){
    		lblhoras.setText("");
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), txtQS802D,txtQS802CD,txtQS802CM,txtQS802CA,lblhoras);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQS802D,txtQS802CD,txtQS802CM,txtQS802CA,lblhoras);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}
    
    public boolean validarSiexisteDiferenciaenfechaNacimientoS04_05(CSSECCION_08 bean){
		boolean valor_inicials4_05=true;
		  	Calendar F_nacimientoCS8=null;
			 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(bean!=null &&  bean.qs802cd!=null && bean.qs802cm!=null && bean.qs802ca!=null){
				Integer mescs8=Integer.parseInt(bean.qs802cm.toString());
				Integer diacs8=Integer.parseInt(bean.qs802cd.toString());
				F_nacimientoCS8 = new GregorianCalendar(bean.qs802ca, mescs8-1, diacs8);	
			}
		
			SeccionCapitulo[] cargadoseccion04_05= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","ID","HOGAR_ID")};
			Seccion04_05 persona= getCuestionarioService().getFecha_NacimientoNinioSeccion04_05(bean.id,bean.hogar_id,bean.persona_id_ninio, cargadoseccion04_05);
			Calendar F_nacimientoCH04_05= null;
			if(persona!=null && persona.qh203d!=null && persona.qh203m!=null && persona.qh203y!=null && F_nacimientoCS8!=null ){
				Integer mes04_05=Integer.parseInt(persona.qh203m);
				Integer dia_04_05 = Integer.parseInt(persona.qh203d);
				F_nacimientoCH04_05 = new GregorianCalendar(persona.qh203y, mes04_05-1,dia_04_05);
				valor_inicials4_05= (sdf.format(F_nacimientoCH04_05.getTime()).equals(sdf.format(F_nacimientoCS8.getTime())));
			}
		
		return valor_inicials4_05;
	}
	public boolean validarSiexisteDiferenciaenfechaNacimientoCIS2(CSSECCION_08 bean){
		boolean valor_inicialscis2=true;
		Calendar F_nacimientoCS8=null;
		 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		if(bean!=null &&  bean.qs802cd!=null && bean.qs802cm!=null && bean.qs802ca!=null){
			Integer mescs8=Integer.parseInt(bean.qs802cm.toString());
			Integer diacs8=Integer.parseInt(bean.qs802cd.toString());
			F_nacimientoCS8 = new GregorianCalendar(bean.qs802ca, mescs8-1, diacs8);	
		}
		SeccionCapitulo[] cargadoseccioncis2= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI215D","QI215M","QI215Y","ID","HOGAR_ID")};
		Calendar F_nacimientoCI2 = null;
			CISECCION_02 ninio=getCuestionarioService().getFecha_NacimientoNinioSeccionCIS2(bean.id,bean.hogar_id,bean.persona_id_ninio, cargadoseccioncis2);
			if(ninio!=null && ninio.qi215d!=null && ninio.qi215m!=null && ninio.qi215y!=null && F_nacimientoCS8!=null){
				Integer mesci2=Integer.parseInt(ninio.qi215m);
				Integer diaci2=Integer.parseInt(ninio.qi215d);
				F_nacimientoCI2 = new GregorianCalendar(ninio.qi215y, mesci2-1, diaci2);
				valor_inicialscis2= (sdf.format(F_nacimientoCS8.getTime()).equals(sdf.format(F_nacimientoCI2.getTime())));
			}
				
		
		return valor_inicialscis2;
	}
    
	@Override
	public Integer grabadoParcial() {
    	uiToEntity(salud); 
    	
    	if(salud!=null){
			salud.qs802bh = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
			salud.qs802bm = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
			salud.qs802cd = !txtQS802CD.getText().toString().equals("") ? Integer.parseInt(txtQS802CD.getText().toString()) : null;
			salud.qs802cm = !txtQS802CM.getText().toString().equals("") ? Integer.parseInt(txtQS802CM.getText().toString()) : null;
			salud.qs802ca = !txtQS802CA.getText().toString().equals("") ? Integer.parseInt(txtQS802CA.getText().toString()) : null;
			
			
			if (salud.qs802a!=null) {
				salud.qs802a=salud.getConvertqs802a(salud.qs802a);
			}				
			
			
			if(salud.qs802cd == null && salud.qs802cm == null && salud.qs802ca == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia;
				}			
			}		
			
								
		}
    	
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
