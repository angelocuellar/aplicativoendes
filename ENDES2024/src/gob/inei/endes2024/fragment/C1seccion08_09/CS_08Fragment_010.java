package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_010 extends FragmentForm {
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS831; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS832; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS833; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblSubtitulo2,lblpregunta833,lblpregunta832_ind; 
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta831,lblpregunta832;
	
	String fechareferencia;
	
	public CS_08Fragment_010() {} 
	public CS_08Fragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS831","QS832","QS833","QS802CONS","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS831","QS832","QS833")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_6_11_OCULAR).negrita().centrar().colorFondo(R.color.griscabece);
	  lblSubtitulo2 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_6_11_MENTAL).negrita().centrar().colorFondo(R.color.griscabece);
	  rgQS831=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs831_1,R.string.cap08_09qs831_2,R.string.cap08_09qs831_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS831ChangeValue"); 
	  rgQS832=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs832_1,R.string.cap08_09qs832_2,R.string.cap08_09qs832_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  
	  lblpregunta833 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs833).textSize(19);
	  lblpregunta831= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs831);
	  lblpregunta832= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs832);
	  lblpregunta832_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs832_ind).negrita();
	 
	  rgQS833=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs833_1,R.string.cap08_09qs833_2,R.string.cap08_09qs833_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo); 		 
		q2 = createQuestionSection(lblpregunta831,rgQS831); 
		q3 = createQuestionSection(lblpregunta832,lblpregunta832_ind,rgQS832); 
		q4 = createQuestionSection(lblSubtitulo2);
		q5 = createQuestionSection(lblpregunta833,rgQS833); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);  
		form.addView(q5);	
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 

		if (salud.qs832!=null) {
			salud.qs832=salud.getConvertqs804(salud.qs832);
		}		
		if (salud.qs833!=null) {
			salud.qs833=salud.getConvertqs804(salud.qs833);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs833 == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia;
				}			
			}		
			
			
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().getPersonabySeccion8Salud().qs833=salud.qs833;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
	
			if (Util.esMayor(salud.qs802d,5)) {
				if (Util.esVacio(salud.qs831)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QS831"); 
					view = rgQS831; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(salud.qs831,1,2)) {
					if (Util.esVacio(salud.qs832)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS832"); 
						view = rgQS832; 
						error = true; 
						return false; 
					}
				}	 
				if (Util.esVacio(salud.qs833)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QS833"); 
					view = rgQS833; 
					error = true; 
					return false; 
				}
			}
			 
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    
		if (salud.qs832!=null) {
			salud.qs832=salud.setConvertqs804(salud.qs832);
		}
		if (salud.qs833!=null) {
			salud.qs833=salud.setConvertqs804(salud.qs833);
		}
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	fechareferencia = salud.qs802cons;
    	inicio();  
    } 
    
    public void cargarpaseedad(Integer edad) {  
    	if (Util.esMenor(edad,6)) {
    		Util.cleanAndLockView(getActivity(),rgQS831,rgQS832,rgQS833);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQS831,rgQS832,rgQS833);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		RenombrarEtiquetas();
    		onqrgQS831ChangeValue();    		
    		ValidarsiesSupervisora();
        	rgQS831.requestFocus();
    	}
    } 	
 	
    private void inicio() {    	
    	cargarpaseedad(salud.qs802d); 
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS831.readOnly();
			rgQS832.readOnly();
			rgQS833.readOnly();			
		}
	}
	
    public void RenombrarEtiquetas()
    {	String replace="(NOMBRE)";
		String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	Calendar fechaactual = new GregorianCalendar();
    	if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;
		}    	
    	Calendar fechahacetreintadias;   		
    	fechahacetreintadias = MyUtil.PrimeraFecha(fechaactual, 30);
   		
   		lblpregunta833.setText(lblpregunta833.getText().toString().replace("F1", fechahacetreintadias.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias.get(Calendar.MONTH))));  		
   		lblpregunta833.setText(lblpregunta833.getText().toString().replace(replace,nombre));
   		lblpregunta831.setText(lblpregunta831.getText().toString().replace(replace,nombre));
   		lblpregunta832.setText(lblpregunta832.getText().toString().replace(replace,nombre));
   		
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
	public void onqrgQS831ChangeValue() {
		if (!MyUtil.incluyeRango(1,2,rgQS831.getTagSelected("").toString())) {			
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQS832);		
			q3.setVisibility(View.GONE);
			rgQS833.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,rgQS832);
			q3.setVisibility(View.VISIBLE);
			rgQS832.requestFocus();
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		if (salud.qs832!=null) {
			salud.qs832=salud.getConvertqs804(salud.qs832);
		}		
		if (salud.qs833!=null) {
			salud.qs833=salud.getConvertqs804(salud.qs833);
		}
		
		if(salud.qs833 == null) {
			salud.qs802cons = null;
		} 
		else {
			if(fechareferencia == null) {
				salud.qs802cons =  Util.getFechaActualToString();
			}
			else {
				salud.qs802cons = fechareferencia;
			}			
		}
		
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
