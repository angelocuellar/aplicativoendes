package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_009 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS826; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS827; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQS827_O; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS828; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS829; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta826,lblpregunta827,lblpregunta828,lblpregunta828_ind,lblpregunta829,lblpregunta829_ind;
	public Spanned texto828=null;
	public CS_08Fragment_009() {} 
	public CS_08Fragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS826","QS827","QS827_O","QS828","QS829","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS817","QS820","QS825","QS802D","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS826","QS827","QS827_O","QS828","QS829")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_3_11_OCULAR).negrita().centrar().colorFondo(R.color.griscabece);	 
	  rgQS826=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs826_1,R.string.cap08_09qs826_2,R.string.cap08_09qs826_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS826ChangeValue"); 
	  rgQS827=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs827_1,R.string.cap08_09qs827_2,R.string.cap08_09qs827_3,R.string.cap08_09qs827_4,R.string.cap08_09qs827_5,R.string.cap08_09qs827_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  txtQS827_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	  rgQS827.agregarEspecifique(4,txtQS827_O); 
	  rgQS828=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs828_1,R.string.cap08_09qs828_2,R.string.cap08_09qs828_3,R.string.cap08_09qs828_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  rgQS829=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs829_1,R.string.cap08_09qs829_2,R.string.cap08_09qs829_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  lblpregunta826 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs826);
	  lblpregunta827 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs827);
//	  lblpregunta828 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs828);

	  texto828 = Html.fromHtml("828. �(NOMBRE) ve televisi�n/ computadora/ laptop/ tablet muy de cerca, es decir,");
	  lblpregunta828 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);	
	  lblpregunta828.setText(texto828);
	  
	  lblpregunta829 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs829);
	  lblpregunta829_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs829_ind).negrita();
	  lblpregunta828_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs828_ind).negrita();
	  
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo); 
		q2 = createQuestionSection(lblpregunta826,rgQS826); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta827,rgQS827); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta828,lblpregunta828_ind,rgQS828); 
		q5 = createQuestionSection(lblpregunta829,lblpregunta829_ind,rgQS829);


		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);  
		form.addView(q5);	
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		if (salud.qs826!=null) {
			salud.qs826=salud.getConvertqs804(salud.qs826);
		}
		if (salud.qs827!=null) {
			salud.qs827=salud.getConvertqs825(salud.qs827);
		}
		if (salud.qs828!=null) {
			salud.qs828=salud.getConvertqs828(salud.qs828);
		}	
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		
			if (Util.esMayor(salud.qs802d,2)) {
				if (!Util.esDiferente(salud.qs817,1) && !Util.esDiferente(salud.qs820,1) && Util.esVacio(salud.qs825)) {
					if (Util.esVacio(salud.qs826)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS826"); 
						view = rgQS826; 
						error = true; 
						return false; 
					} 
					if(!Util.esDiferente(salud.qs826,1,2)){
						if (Util.esVacio(salud.qs827)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS827"); 
							view = rgQS827; 
							error = true; 
							return false; 
						} 
						if(!Util.esDiferente(salud.qs827,6)){ 
							if (Util.esVacio(salud.qs827_o)) { 
								mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
								view = txtQS827_O; 
								error = true; 
								return false; 
							} 
						}
					}
				}			
				if (Util.esVacio(salud.qs828)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QS828"); 
					view = rgQS828; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs829)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QS829"); 
					view = rgQS829; 
					error = true; 
					return false; 
				} 
			}
					
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
		
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	//MODIFICAR PARA EL A�O 2017-2 LA PREGUNTA 826 DEBE SER CODIFICADA EN 1,2,3
    	if (salud.qs826!=null) {
			salud.qs826=salud.setConvertqs804(salud.qs826);
		}
		if (salud.qs827!=null) {
			salud.qs827=salud.setConvertqs825(salud.qs827);
		}
		if (salud.qs828!=null) {
			salud.qs828=salud.setConvertqs828(salud.qs828);
		}	
    	entityToUI(salud); 
    	inicio();  
    } 
    public void cargarpase828(Integer p817, Integer p820, Integer p825) {
		if (Util.esDiferente(p817,1) || Util.esDiferente(p820,1) || !Util.esVacio(p825)) {		
			Util.cleanAndLockView(getActivity(), rgQS826,rgQS827,txtQS827_O);
			q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,rgQS826,rgQS827);	
			q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
		}
    }
		
    public void cargarpaseedad(Integer edad) {  
    	//if (Util.esDiferente(salud.qs806,1) || Util.esMenor(edad,3)) {
    	if (Util.esMenor(edad,3)) {    	
    		Util.cleanAndLockView(getActivity(),rgQS826,rgQS827,txtQS827_O,rgQS828,rgQS829); 
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQS826,rgQS827,rgQS828,rgQS829); 
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		cargarpase828(salud.qs817,salud.qs820,salud.qs825);
    		onqrgQS826ChangeValue();
    	}
    }    
    
    private void inicio() { 
    	cargarpaseedad(salud.qs802d);
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS826.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS826.readOnly();
			rgQS827.readOnly();
			rgQS828.readOnly();
			rgQS829.readOnly();
			txtQS827_O.readOnly();			
		}
		
	}
	
    public void RenombrarEtiquetas()
    {
    	String replace="(NOMBRE)";
    	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	lblpregunta826.setText(lblpregunta826.getText().toString().replace(replace,nombre));
    	lblpregunta827.setText(lblpregunta827.getText().toString().replace(replace,nombre));
    	
    	lblpregunta828.setText(texto828);
    	lblpregunta828.setText(lblpregunta828.getText().toString().replace(replace,nombre));    	
    	Spanned texto8281=Html.fromHtml(lblpregunta828.getText()+" <b>a menos de 30 cent�metros</b>?");
    	lblpregunta828.setText(texto8281);
    	
    	lblpregunta829.setText(lblpregunta829.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	public void onqrgQS826ChangeValue() {
		if (!MyUtil.incluyeRango(1,2,rgQS826.getTagSelected("").toString())) {			
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQS827,txtQS827_O);
			q3.setVisibility(View.GONE);
			rgQS827.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,rgQS827);
			q3.setVisibility(View.VISIBLE);
			rgQS827.requestFocus();
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		if (salud.qs826!=null) {
			salud.qs826=salud.getConvertqs804(salud.qs826);
		}
		if (salud.qs827!=null) {
			salud.qs827=salud.getConvertqs825(salud.qs827);
		}
		if (salud.qs828!=null) {
			salud.qs828=salud.getConvertqs828(salud.qs828);
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
