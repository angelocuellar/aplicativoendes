package gob.inei.endes2024.fragment.C1seccion08_09;
import java.sql.SQLException;
import java.util.List;

import android.os.Bundle;
import android.text.format.Time;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Endes_cs_nueve_validar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

public class CS_09Fragment_015 extends FragmentForm{
	
	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnQSCOD_ANT;	
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnQSCOD_AUX;
	@FieldAnnotation(orderIndex=3) 
	public TextAreaField txtQSOBS_ANTRO;
	
	CAP04_07 cap08_09; 
	public Endes_cs_nueve_validar valida=null;
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	
	private LabelComponent lblTitulo,lblDescripcion; 
//	private List<CSVISITA> visitas;
	 private UsuarioService serviceUsuario;
	 private Seccion04_05Service seccion04_05Service; 

	SeccionCapitulo[] seccionesCargadoVisitas;
	
	private LabelComponent lblAntro,lblAux;

	public Time hora_fin;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	LinearLayout q15; 
	LinearLayout q16; 
	LinearLayout q17; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionescargado9,seccionesCargado08,seccionesCargadoVisita; 
	private GridComponent2 grid1;
	private GridComponent2 grid2;
	private LabelComponent lblObservaciones,lblObs_titulo;

	public CS_09Fragment_015() {} 
	public CS_09Fragment_015 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
	
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS900","QS901","QS902","QS902_O","QS903","QS903_1","QS904H","QS904M","QS905","QS905_1","QS906","QS906_O","QSNOM_ANT","QSCOD_ANT","QSNOM_AUX","QSCOD_AUX","QSOBS_ANTRO","QSOBS_ENTREV","QSOBS_SUPER","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSNOM_ANT","QSCOD_ANT","QSNOM_AUX","QSCOD_AUX","QSOBS_ANTRO")}; 
		
		
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs900_titulo).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 

		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15); 
		lblObs_titulo=new LabelComponent(this.getActivity()).size(60, 600).text(R.string.secccion04_obs_titulo).textSize(15).alinearIzquierda().negrita(); 
		txtQSOBS_ANTRO= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		lblAntro=new LabelComponent(this.getActivity()).size(60,600).text(R.string.secccion04_nombreantro).textSize(15).centrar().negrita(); 
		lblAux=new LabelComponent(this.getActivity()).size(60,600).text(R.string.secccion04_nombreaux).textSize(15).centrar().negrita();
				
		spnQSCOD_ANT = new SpinnerField(getActivity()).size(altoComponente+15, 400);
		spnQSCOD_AUX = new SpinnerField(getActivity()).size(altoComponente+15, 400);
		
		grid1 = new GridComponent2(this.getActivity(),Gravity.CENTER, 1,0);
		grid1.addComponent(lblAntro,1);
		grid1.addComponent(spnQSCOD_ANT,1);
		grid2 = new GridComponent2(this.getActivity(),Gravity.CENTER, 1,0);
		grid2.addComponent(lblAux,1);
		grid2.addComponent(spnQSCOD_AUX,1);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(grid1.component());
		q2 = createQuestionSection(grid2.component()); 		
		q3 = createQuestionSection(R.string.secccion04_obs_titulo,lblObservaciones,txtQSOBS_ANTRO); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2);
		form.addView(q3);
		
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(cap08_09); 
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			Usuario antro = (Usuario) spnQSCOD_ANT.getValue();
			Usuario aux = (Usuario) spnQSCOD_AUX.getValue();
			if (!antro.nombres.equals("-- SELECCIONE --"))
				cap08_09.qsnom_ant=antro.nombres+" "+antro.apellidos;
			if (!aux.nombres.equals("-- SELECCIONE --"))
				cap08_09.qsnom_aux=aux.nombres+" "+aux.apellidos;
			if(valida.qscod_ant==null && valida.qscod_aux==null && valida.qsobs_antro==null){
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSCOD_ANT",cap08_09.qscod_ant==null?null:cap08_09.qscod_ant, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSCOD_AUX",cap08_09.qscod_aux==null?null:cap08_09.qscod_aux, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSOBS_ANTRO",cap08_09.qsobs_antro==null?null:cap08_09.qsobs_antro, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);
			}
			else{
				if(Util.esDiferente(valida.qscod_ant, cap08_09.qscod_ant)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSCOD_ANT",cap08_09.qscod_ant==null?null:cap08_09.qscod_ant, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qscod_aux, cap08_09.qscod_aux)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSCOD_AUX",cap08_09.qscod_aux==null?null:cap08_09.qscod_aux, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qsobs_antro, cap08_09.qsobs_antro)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSOBS_ANTRO",cap08_09.qsobs_antro==null?null:cap08_09.qsobs_antro, cap08_09.id,  cap08_09.hogar_id, cap08_09.persona_id), null);}
			}
			
			if(!getCuestionarioService().saveOrUpdate(cap08_09,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
	
//		if(!Util.esDiferente(cap08_09.qs902,1)){ 
			
		/*COMENTADO 12/09/2020 SOLICITADO POR CORREO DE METODOLOGIA FCP*/
			/*Usuario antro = (Usuario) spnQSCOD_ANT.getValue();
			if (antro.nombres.equals("-- SELECCIONE --")) {
				mensaje = "Debe seleccionar una Antropometrista";
				error = true;
				return false;
			}
			Usuario aux = (Usuario) spnQSCOD_AUX.getValue();
			if (aux.nombres.equals("-- SELECCIONE --")) {
				mensaje = "Debe seleccionar una Auxiliar";
				error = true;
				return false;
			}*/
			/*********** FIN 12/09/2020*****/
			
//    	}

		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
		cap08_09 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		MyUtil.llenarAntropo(this.getActivity() , getServiceUsuario(), spnQSCOD_ANT, App.getInstance().getUsuario().id, cap08_09.qscod_ant, cap08_09.qsnom_ant);
    	MyUtil.llenarAuxiliares(this.getActivity() , getServiceUsuario(), spnQSCOD_AUX, App.getInstance().getUsuario().id, cap08_09.qscod_aux, cap08_09.qsnom_aux);
    	
    	
		if(cap08_09==null){ 
		  cap08_09=new CAP04_07(); 
		  cap08_09.id=App.getInstance().getPersonaSeccion01().id; 
		  cap08_09.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap08_09.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		entityToUI(cap08_09); 
		CargaDeDatosParaValidar(cap08_09);
		inicio(); 
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void CargaDeDatosParaValidar(CAP04_07 datos){
    	valida = new Endes_cs_nueve_validar();
    	valida.id=datos.id;
    	valida.hogar_id=datos.hogar_id;
    	valida.persona_id=datos.persona_id;
    	valida.qscod_ant=datos.qscod_ant;
    	valida.qscod_aux=datos.qscod_aux;
    	valida.qsobs_antro=datos.qsobs_antro;
    }
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			spnQSCOD_ANT.readOnly();
			spnQSCOD_AUX.readOnly();
			txtQSOBS_ANTRO.setEnabled(false);
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public UsuarioService getServiceUsuario() {
		if (serviceUsuario == null) {
			serviceUsuario = UsuarioService.getInstance(getActivity());
		}
		return serviceUsuario;
	}
    public Seccion04_05Service getSeccion04_05Service() { 
		if(seccion04_05Service==null){ 
			seccion04_05Service = Seccion04_05Service.getInstance(getActivity()); 
		} 
		return seccion04_05Service; 
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(cap08_09); 
		try {
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap08_09,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
}
