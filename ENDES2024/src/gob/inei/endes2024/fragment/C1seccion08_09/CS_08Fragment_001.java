package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.C1visita.CS_VISITAFragment_001;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.AdapterView.OnItemClickListener;

public class CS_08Fragment_001 extends FragmentForm { 
	CSSECCION_08 salud; 
	CSVISITA visitacalculada,visita;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	public GridComponent2 gridPreguntas802B,gridPreguntas802C,gridPreguntas802D;
	public TableComponent tcNinios;
	public List<Seccion01> detalles;
	public List<CSSECCION_08> detallesseccion8;
	public List<CSSECCION_08> detallesUpdateseccion8;
	public ButtonComponent btnSeccion9,btnPregunta40;
	Seccion01ClickListener listener;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	public List<CSSECCION_08> detallesseccion08;
	public TextField txtCabecera;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoSeccion08; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoSeccion01,seccionescargado9; 
	private List<CSVISITA> visitas;
	SeccionCapitulo[] seccionesCargadoVisitas;
	SeccionCapitulo[] seccionesGrabadoVisitas;
	private VisitaService visitaService;
	public Seccion01Service seccion01service;
// 
	public CS_08Fragment_001() {} 
	public CS_08Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		listener = new Seccion01ClickListener();
		tcNinios.getListView().setOnItemClickListener(listener);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","QS802D","PERSONA_ID_NINIO","QS809","QS817","QS820","QS833","ESTADO")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS802","QS802N","QS802A","QS802A_O","QS802BH","QS802BM","QS802CD","QS802CM","QS802CA","QS802D")};
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","ID","HOGAR_ID")};
		seccionesGrabadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO" )};
		seccionesCargadoVisitas = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINUTO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesGrabadoVisitas = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINTUO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","QSRESULT","QSRESULT_O","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		
		seccionescargado9= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS731AH","QS731AM","QS902","QS906","HOGAR_ID","ID","PERSONA_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
		lblSubtitulo = new LabelComponent(getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_1);
		
		btnSeccion9 = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnSeccion9).size(200,55);
		btnPregunta40 = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnPregunta40).size(200,55);
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcNinios = new TableComponent(getActivity(), this,App.ESTILO).size(550, 750).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcNinios = new TableComponent(getActivity(), this,App.ESTILO).size(650, 750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcNinios.addHeader(R.string.seccion01_nro_orden, 0.8f,TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.seccion01nombres, 1.5f,TableComponent.ALIGN.LEFT);
		tcNinios.addHeader(R.string.seccion01_edad,0.7f, TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.seccion01_sexo,0.7f, TableComponent.ALIGN.CENTER);
		
		btnSeccion9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	parent.nextFragment(CuestionarioFragmentActivity.CSSECCION8f_13+1);
            }
		});
		
		btnPregunta40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	parent.nextFragment(CuestionarioFragmentActivity.CSSECCION8f_13);
            }
		});
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo,btnPregunta40,btnSeccion9,tcNinios.getTableView()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1); 

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
   	
//    	validar();
		return true; 
    } 
    private boolean validar() {  
//    	 App.getInstance().getSaludNavegacionS4_7().estadoseccion8Completo=getCuestionarioService().getRegistroCompletadoSaludSeccion8(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id);
//         Log.e("","COMPLETAD8888 "+App.getInstance().getSaludNavegacionS4_7().estadoseccion8Completo);    
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	App.getInstance().setPersonabySeccion8Salud(null);
    	if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null){
    		SeccionCapitulo[] seccionescargado8= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","PERSONA_ID_NINIO")};
    		SeccionCapitulo[] seccionesgrabado8= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","PERSONA_ID_NINIO")};
    		detallesUpdateseccion8 = getCuestionarioService().getListadoSeccion8(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, seccionescargado8);
    		if(detallesUpdateseccion8.size()>0){
    			for (CSSECCION_08 css8: detallesUpdateseccion8 ) {
					if(Util.esDiferente(css8.persona_id , App.getInstance().getPersonaSeccion01().persona_id)){
						css8.persona_id = App.getInstance().getPersonaSeccion01().persona_id;
						
						try {
							if(!getCuestionarioService().Update(css8,seccionesgrabado8)){ 
			    				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados!!!.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			    			} 
						} catch (Exception e) {
							ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}
					}
				}
    		}
    		if(salud== null){
    			salud = new CSSECCION_08();
    			salud.id = App.getInstance().getMarco().id;
    			salud.hogar_id=App.getInstance().getHogar().hogar_id;
    			salud.persona_id = App.getInstance().getPersonaSeccion01().persona_id;
    		}   	
    	mostrarBotones();
    	entityToUI(salud); 
    	cargarTabla();
    	
    	}
    	inicio();  
    	crearVisita();
    	visita = getCuestionarioService().getCAPVISITASALUD(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoVisitas);
    	//sino completa la s1-s7 nose habilita ir a s9
		if(getCuestionarioService().getCuestionarioDelSalud01_07Completado(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id)){
			Util.lockView(getActivity(), false,btnSeccion9);			
		}else{
			Util.cleanAndLockView(getActivity(),btnSeccion9);	
		}
//		GrabarVisitaCalculada(visita);
    } 
	public void GrabarVisitaCalculada(CSVISITA visita){
		visitacalculada=MyUtil.VerificarCompletadoVisitaDeSalud(visita,seccionesCargado,getCuestionarioService(),seccionescargado9);
		if(visitacalculada!=null){
			try {
				getVisitaService().saveOrUpdate(visitacalculada,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA");
			} catch (SQLException e) {
				ToastMessage.msgBox(getActivity(),e.getMessage()+"error visita calculada",
	                    ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	}
    public void crearVisita(){
    	visitas = getCuestionarioService().getCAPVISITAs(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, seccionesCargadoVisitas);
		if(visitas!=null){
			if(visitas.size()==0){
				CSVISITA bean = new CSVISITA();
				bean.id = App.getInstance().getPersonaSeccion01().id;
				bean.hogar_id = App.getInstance().getPersonaSeccion01().hogar_id ;
				bean.persona_id = App.getInstance().getPersonaSeccion01().persona_id;
				bean.nro_visita = 1;
				bean.qsvanio = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
				bean.qsvdia = Util.getFechaFormateada(new Date(), "dd");
				bean.qsvmes = Util.getFechaFormateada(new Date(), "MM");
				bean.qsvhora_ini = Util.getFechaFormateada(new Date(), "HH");
				bean.qsvminuto_ini = Util.getFechaFormateada(new Date(), "mm");
				try {
					if (!getVisitaService().saveOrUpdate(bean,"QSVDIA", "QSVMES", "QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINUTO_FIN","NRO_VISITA","QSVRESUL","QSVRESUL_O","QSPROX_DIA","QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID","PERSONA_ID")) {
						ToastMessage.msgBox(this.getActivity(),"Los datos no fueron grabados",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					}
				} catch (SQLException e) {
					ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				}
				App.getInstance().setC1Visita(null);
				App.getInstance().setC1Visita(bean);
			}
		}
    }
    
    public void cargarTabla() {
    	detallesseccion08=new ArrayList<CSSECCION_08>();
    	if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null ){
    		detallesseccion08= getCuestionarioService().getListadoNiniosSeccion08(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,seccionesCargado);
    		tcNinios.setData(detallesseccion08 ,"persona_id_ninio", "getNombre","qh07","getSexo");
    	    //tcNinios.setBorder("estado");
    	    for (int row = 0; row < detallesseccion08.size(); row++) {
    			if (obtenerEstado(detallesseccion08.get(row)) == 1) {
    				// borde de color azul
    				tcNinios.setBorderRow(row, true);
    			} else if (obtenerEstado(detallesseccion08.get(row)) == 2) {
    				// borde de color rojo
    				tcNinios.setBorderRow(row, true, R.color.red);
    			} else {
    				tcNinios.setBorderRow(row, false);
    			}
    		}
            registerForContextMenu(tcNinios.getListView());
    	}
    	    	
    }
    private int obtenerEstado(CSSECCION_08 detallesseccion08) {
//    	Log.e("detalle.estadocap4a:: "," "+detallesseccion08.estado);
    	
		if (!Util.esDiferente(detallesseccion08.estado, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detallesseccion08.estado,1)) {
			return 2;
		}
		return 0;
	}
    public void mostrarBotones(){
//    	Log.e("","EDAD: "+getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, 0, 11));
//    	Log.e("","EXISTE "+getCuestionarioService().getListadoNiniosSeccion08Botones(salud.id, salud.hogar_id));
//    	Log.e("", "SALUD: "+salud);
//    	Log.e("", "SALUD:ID "+salud.id);
//    	Log.e("", "SALUD:HOGAR "+salud.hogar_id);
    	if (getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, 0, 11)) {
    		if (getCuestionarioService().getListadoNiniosSeccion08Botones(App.getInstance().getMarco().id,  App.getInstance().getHogar().hogar_id)) {
        		btnPregunta40.setVisibility(View.VISIBLE);
        		btnSeccion9.setVisibility(View.GONE);
    		}
        	else{
        		btnSeccion9.setVisibility(View.VISIBLE);
        		btnPregunta40.setVisibility(View.GONE);
        	}	
		}
    	
    }
    
    private void inicio() { 
    	txtCabecera.requestFocus();
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public Seccion01Service getPersonaService() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
	private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CSSECCION_08 c = (CSSECCION_08) detallesseccion08.get(arg2);
			GoTo(c);	
		}
	}

	 public void GoTo(CSSECCION_08 bean){

		 App.getInstance().setPersonabySeccion8Salud(null);
		 App.getInstance().setPersonabySeccion8Salud(bean);
		 parent.nextFragment(CuestionarioFragmentActivity.CSSECCION8f_1+1);

	 }
	 private VisitaService getVisitaService() {
			if (visitaService == null) {
				visitaService = VisitaService.getInstance(getActivity());
			}
			return visitaService;
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.SALUD;
	}

//	 public void EliminarNiniosQueCambiaronEdad(){
//	   SeccionCapitulo[] seccionescargado8= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","PERSONA_ID_NINIO")};
// 	   detallesUpdateseccion8 = getCuestionarioService().getListadoSeccion8(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, seccionescargado8);
// 	   detalles = getCuestionarioService().getListadoSeccion8Salud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion01);
// 	   Integer contador=0;
// 	   List<Integer> ids=new ArrayList<Integer>();
// 	   Log.e("Hasta ","aqui11");
// 	    if(detallesUpdateseccion8.size()>0){
// 	     for(CSSECCION_08 cs: detallesUpdateseccion8){
// 	    	 for(Seccion01 chs01: detalles){
// 	    		 if(cs.persona_id_ninio!=chs01.persona_id){
//	 	    		ids.add(cs.persona_id_ninio);
//	 	    	 }
//	 	    	  	    	 }
// 	    	 contador=0;
// 	     }
// 	    }
// 	   Log.e("Hasta ","paso");
// 	     boolean flag= false;
// 	     if(ids.size()>0){
// 	    	 Log.e("Hasta ","aqui");
// 	    	Log.e("Hasta ","aqui total "+ids.size());
// 	    	 for(Integer ninio_id: ids){
// 	    		 flag= getCuestionarioService().BorrarPersona(App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, ninio_id);
// 	    	 }
// 	     }
//	 }
} 
