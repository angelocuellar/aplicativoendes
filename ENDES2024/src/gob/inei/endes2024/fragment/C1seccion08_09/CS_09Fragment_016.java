package gob.inei.endes2024.fragment.C1seccion08_09;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

public class CS_09Fragment_016 extends FragmentForm{
	@FieldAnnotation(orderIndex=1) 
	public TextAreaField txtQSOBS_ENTREV;
	@FieldAnnotation(orderIndex=2) 
	public TextAreaField txtQSOBS_SUPER;
	
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	private LabelComponent lblTitulo,lblDescripcion; 
	private List<CSVISITA> visitas;
	private CSVISITA ultimaVisita, menorVisita;
	SeccionCapitulo[] seccionesCargadoVisitas;

	public Time hora_fin;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	private LabelComponent lblObservacionesEntrevistadora,lblObservacionesSupervisora;

	public CS_09Fragment_016() {} 
	public CS_09Fragment_016 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
	
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS900","QS901","QS902","QS902_O","QS903","QS903_1","QS904H","QS904M","QS905","QS905_1","QS906","QS906_O","QSNOM_ANT","QSCOD_ANT","QSNOM_AUX","QSCOD_AUX","QSOBS_ANTRO","QSOBS_ENTREV","QSOBS_SUPER","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSOBS_ENTREV","QSOBS_SUPER")}; 
		seccionesCargadoVisitas = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINUTO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA","QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		return rootView; 
	}

  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs900_titulo).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		lblObservacionesEntrevistadora=new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap08_09qsobs_entrev).textSize(18).negrita(); 
		txtQSOBS_ENTREV= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		lblObservacionesSupervisora=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qsobs_super).textSize(18).negrita(); 
		txtQSOBS_SUPER= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();

    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblObservacionesEntrevistadora,txtQSOBS_ENTREV); 
		q2 = createQuestionSection(lblObservacionesSupervisora,txtQSOBS_SUPER);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(cap04_07); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		
//			if(getCuestionarioService().getCuestionarioDelSaludCompletado(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id)){
//				if(!getVisitaService().saveOrUpdate(RegistrarVisitaCompletada(), "NRO_VISITA","QSVHORA_FIN","QSVMINUTO_FIN","QSVRESUL","QSRESULT")){
//					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados visita.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//					return false; 
//				}
//			}
	
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	if(App.getInstance().getPersonaSeccion01()!=null){
    		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 

    		if(cap04_07==null){ 
    		  cap04_07=new CAP04_07(); 
    		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
    		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	    } 
    		entityToUI(cap04_07); 
    		inicio(); 
    	}
    } 
    
    private void inicio() { 
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQSOBS_ENTREV.setEnabled(false);
			txtQSOBS_SUPER.setEnabled(false);
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    private VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
    
    public CSVISITA RegistrarVisitaCompletada() {
    	CSVISITA visita= new CSVISITA();
    	visita.id=App.getInstance().getPersonaSeccion01().id;
    	visita.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id;
    	visita.persona_id=App.getInstance().getPersonaSeccion01().persona_id;
    	
    	visitas = getCuestionarioService().getCAPVISITAs(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, seccionesCargadoVisitas);

    	Integer indice = 0;
		if (visitas.size() > 0) {
			if (visitas.size() > 1) {
				indice = visitas.size() - 1;
			} else {
				indice = 0;
			}

			ultimaVisita = getCuestionarioService().getCAPVISITA(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,visitas.get(indice).nro_visita,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoVisitas);
			
			App.getInstance().setC1Visita(null);
			App.getInstance().setC1Visita(ultimaVisita);
		}
		
    	if(App.getInstance().getC1Visita()!=null){
    		visita.nro_visita=ultimaVisita.nro_visita;
    	}
    	
    	Calendar calendario = new GregorianCalendar();
    	visita.qsvhora_fin=calendario.get(Calendar.HOUR_OF_DAY)+"";
    	visita.qsvminuto_fin=calendario.get(Calendar.MINUTE)+"";
    	visita.qsvresul=App.SALUD_RESULTADO_COMPLETADO;
    	visita.qsresult=App.SALUD_RESULTADO_COMPLETADO;
    	return visita;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(cap04_07);
		try {
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
}
