package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_008 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS823; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQS823_O; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS824; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS825; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQS825_O; 	
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblsectorpublico,lblsectorprivado,lblong,lblcampa,lblhospital; 

	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
   public LabelComponent lblpregunta823,lblpregunta824,lblpregunta825;
	public CS_08Fragment_008() {} 
	public CS_08Fragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS823","QS823_O","QS824","QS825","QS825_O","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS817","QS820","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS823","QS823_O","QS824","QS825","QS825_O")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() {
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_3_11_OCULAR).negrita().centrar().colorFondo(R.color.griscabece);
	  rgQS823=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs823_1,R.string.cap08_09qs823_2,R.string.cap08_09qs823_3,R.string.cap08_09qs823_4,R.string.cap08_09qs823_5,R.string.cap08_09qs823_6,R.string.cap08_09qs823_7,R.string.cap08_09qs823_7_1,R.string.cap08_09qs823_8,R.string.cap08_09qs823_9,R.string.cap08_09qs823_10,R.string.cap08_09qs823_11,R.string.cap08_09qs823_12,R.string.cap08_09qs823_13,R.string.cap08_09qs823_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS823ChangeValue"); 
	  txtQS823_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	 
	  rgQS824=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS824ChangeValue");
	  rgQS825=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs825_1,R.string.cap08_09qs825_2,R.string.cap08_09qs825_3,R.string.cap08_09qs825_4,R.string.cap08_09qs825_5,R.string.cap08_09qs825_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  txtQS825_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	  rgQS825.agregarEspecifique(4,txtQS825_O); 
	 
	  lblpregunta823 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs823);
	  lblpregunta824 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs824);
	  lblpregunta825 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs825);
	  
	  lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_a).textSize(18).negrita();
	  lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
	  lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_b).textSize(18).negrita();
	  lblong = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_c).textSize(18).negrita();
	  lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_d).textSize(18).negrita();
	
	  rgQS823.agregarTitle(0,lblsectorpublico);
	  rgQS823.agregarTitle(1,lblhospital);
	  rgQS823.agregarTitle(8,lblsectorprivado);
	  rgQS823.agregarTitle(11,lblong);
	  rgQS823.agregarTitle(14,lblcampa);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta823,rgQS823,txtQS823_O); 
		q3 = createQuestionSection(lblpregunta824,rgQS824); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta825,rgQS825); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q4); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {	
		uiToEntity(salud); 
		if (salud.qs823!=null) {
			salud.qs823=salud.getConvertqs819(salud.qs823);
		}
		if (salud.qs825!=null) {
			salud.qs825=salud.getConvertqs825(salud.qs825);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 

	
			if (Util.esMayor(salud.qs802d,2)) {
				if (!Util.esDiferente(salud.qs817,1) && !Util.esDiferente(salud.qs820,1)) {
					if (Util.esVacio(salud.qs823)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS823"); 
						view = rgQS823; 
						error = true; 
						return false; 
					} 
					if(!Util.esDiferente(salud.qs823,42,96)){ 
						if (Util.esVacio(salud.qs823_o)) { 
							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
							view = txtQS823_O; 
							error = true; 
							return false; 
						} 
					} 
					if (Util.esVacio(salud.qs824)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS824"); 
						view = rgQS824; 
						error = true; 
						return false; 
					} 
					if(!Util.esDiferente(salud.qs824,2)){
						if (Util.esVacio(salud.qs825)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS825"); 
							view = rgQS825; 
							error = true; 
							return false; 
						} 
						if(!Util.esDiferente(salud.qs825,6)){ 
							if (Util.esVacio(salud.qs825_o)) { 
								mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
								view = txtQS825_O; 
								error = true; 
								return false; 
							} 
						}
					} 
				}			
			}
				
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	if (salud.qs823!=null) {
			salud.qs823=salud.setConvertqs819(salud.qs823);
		}
		if (salud.qs825!=null) {
			salud.qs825=salud.setConvertqs825(salud.qs825);
		}
		
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	inicio();  
    } 
    
    public void cargarpaseedad(Integer edad) { 
    		//if (Util.esDiferente(salud.qs806,1) || Util.esMenor(edad,3) || Util.esDiferente(salud.qs817,1) || Util.esDiferente(salud.qs820,1)) {    	
        	if (Util.esMenor(edad,3) || Util.esDiferente(salud.qs817,1) || Util.esDiferente(salud.qs820,1)) {
        	Util.cleanAndLockView(getActivity(),rgQS823,txtQS823_O,rgQS824,rgQS825,txtQS825_O);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
		}
    	else{
    		Util.lockView(getActivity(), false,rgQS823,txtQS823_O,rgQS824,rgQS825,txtQS825_O);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		onqrgQS823ChangeValue();
    		onqrgQS824ChangeValue();
    	}			
    }
    
    private void inicio() { 
    	cargarpaseedad(salud.qs802d);
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS823.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS823.readOnly();
			txtQS823_O.readOnly();
			rgQS824.readOnly();
			rgQS825.readOnly();
			txtQS825_O.readOnly();
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	String replace="(NOMBRE)";
    	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	lblpregunta823.setText(lblpregunta823.getText().toString().replace(replace,nombre));
    	lblpregunta824.setText(lblpregunta824.getText().toString().replace(replace,nombre));
    	lblpregunta825.setText(lblpregunta825.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
	public void onqrgQS823ChangeValue() {
		if (!MyUtil.incluyeRango(13,14,rgQS823.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQS823_O);	
			rgQS824.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,txtQS823_O);
			txtQS823_O.requestFocus();
		}
	}
	
	public void onqrgQS824ChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQS824.getTagSelected("").toString())) {			
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQS825,txtQS825_O);	
			q4.setVisibility(View.GONE);
		} 
		else {				
			Util.lockView(getActivity(), false,rgQS825,txtQS825_O);
			q4.setVisibility(View.VISIBLE);
			rgQS825.requestFocus();
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		if (salud.qs823!=null) {
			salud.qs823=salud.getConvertqs819(salud.qs823);
		}
		if (salud.qs825!=null) {
			salud.qs825=salud.getConvertqs825(salud.qs825);
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
