package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.C1seccion08_09.Dialog.Seccion08_P40Dialog;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_08Fragment_013 extends FragmentForm implements Respondible{ 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS840A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS840B; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQS840_O; 
	
	public TableComponent tcListadoNinios;
	
	public List<CSSECCION_08> detalles;
	public Integer contador=0;
	Seccion8ClickListener adapter;
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta840;
	public CS_08Fragment_013() {} 
	public CS_08Fragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		adapter = new Seccion8ClickListener();
        tcListadoNinios.getListView().setOnItemClickListener(adapter);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","QS802D","PERSONA_ID_NINIO","QS809","QS817","QS820","QS833","ESTADO")};		 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_1_11_CEPILLO).negrita().centrar().colorFondo(R.color.griscabece);
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcListadoNinios = new TableComponent(getActivity(), this,App.ESTILO).size(550, 750).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcListadoNinios = new TableComponent(getActivity(), this,App.ESTILO).size(650, 750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
	  
	  tcListadoNinios.addHeader(R.string.seccion01_nro_orden, 0.8f,TableComponent.ALIGN.CENTER);
	  tcListadoNinios.addHeader(R.string.seccion01nombres, 1.5f,TableComponent.ALIGN.LEFT);
	  tcListadoNinios.addHeader(R.string.seccion01_edad,0.7f, TableComponent.ALIGN.CENTER);
	  tcListadoNinios.addHeader(R.string.seccion01_sexo,0.7f, TableComponent.ALIGN.CENTER);
	  
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(tcListadoNinios.getTableView());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);	 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {
    	salud=new CSSECCION_08();
    	salud.id=App.getInstance().getMarco().id;
    	salud.hogar_id=App.getInstance().getHogar().hogar_id;
    	salud.estadoseccion1_7=getCuestionarioService().getCuestionarioDelSalud01_07Completado(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id);
    	App.getInstance().setPersonabySeccion8Salud(salud);

		return true; 
    } 
    
    private boolean validar() {     	
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (!getCuestionarioService().getCuestionarioDelSalud01_07Completado(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id)) {
			mensaje = preguntaVacia.replace("$", "Debe completar las preguntas del la Secci�n 1 al 7"); 			
			error = true; 
			return false; 
		}		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
      	recargartabla();    	
    } 
    
    public void recargartabla(){    	
    	detalles = new ArrayList<CSSECCION_08>();
    	detalles= getCuestionarioService().getListadoNiniosSeccion08Preg840(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,seccionesCargado);
    	tcListadoNinios.setData(detalles ,"persona_id_ninio", "getNombre","QS802D","getSexo");
//    	tcListadoNinios.setBorder("estado");
    	 for (int row = 0; row < detalles.size(); row++) {
 			if (obtenerEstado(detalles.get(row)) == 1) {
 				// borde de color azul
 				tcListadoNinios.setBorderRow(row, true);
 			} else if (obtenerEstado(detalles.get(row)) == 2) {
 				// borde de color rojo
 				tcListadoNinios.setBorderRow(row, true, R.color.red);
 			} else {
 				tcListadoNinios.setBorderRow(row, false);
 			}
 		}
    	registerForContextMenu(tcListadoNinios.getListView());    
    }
    private int obtenerEstado(CSSECCION_08 detalles) {
    	Log.e("detalle.estadocap4a:: "," "+detalles.estado);
    	
		if (!Util.esDiferente(detalles.estado, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalles.estado,1)) {
			return 2;
		}
		return 0;
	}
     	
    private void inicio(){  
    	txtCabecera.requestFocus();
    } 
 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void refrescarPersonas(CSSECCION_08 personas) {
    	if (detalles.contains(personas)) {
    		recargartabla();
    		return;
    	} else {
    		detalles.add(personas);
    		recargartabla();
    	}
    }
    public class Seccion8ClickListener implements OnItemClickListener {
    	public Seccion8ClickListener() {
    	}
    	@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
    		CSSECCION_08 c = (CSSECCION_08) detalles.get(arg2);
    		if(contador==0){
    		abrirDetalle(c, arg2, (List<CSSECCION_08>) detalles);
    		contador=1;
    		}
		}
	}
	
	public void abrirDetalle(CSSECCION_08 tmp, int index, List<CSSECCION_08> detalles) {     	
         FragmentManager fm = CS_08Fragment_013.this.getFragmentManager();
         Seccion08_P40Dialog aperturaDialog = Seccion08_P40Dialog.newInstance(CS_08Fragment_013.this, tmp, index, detalles);
         aperturaDialog.show(fm, "aperturaDialog");
     }
	@Override
	public Integer grabadoParcial() {
		return App.NODEFINIDO;
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
	}
} 
