package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Endes_cs_nueve_validar;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.format.Time;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_09Fragment_014 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public DecimalField txtQS900; 
	@FieldAnnotation(orderIndex=2) 
	public DecimalField txtQS901; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS902; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQS902_O; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQS903; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS903_1; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS904H; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQS904M; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQS905; 
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQS905_1; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQS906; 
	@FieldAnnotation(orderIndex=12) 
	public TextField txtQS906_O; 
	
	@FieldAnnotation(orderIndex=13) 
	public DecimalField txtQS907; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQS908; 
	@FieldAnnotation(orderIndex=15) 
	public TextField txtQS908_O; 
	
	
	@FieldAnnotation(orderIndex=16) 
	public TextAreaField txtQSOBS_ANTRO;
	
	
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQS908B_A; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQS908B_B; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQS908C; 
	@FieldAnnotation(orderIndex=20) 
	public IntegerField txtQS908C1; 
	@FieldAnnotation(orderIndex=21) 
	public IntegerField txtQS908C2; 
	@FieldAnnotation(orderIndex=22) 
	public IntegerField txtQS908C3;
	@FieldAnnotation(orderIndex=23) 
	public RadioGroupOtherField rgQS908D; 
	@FieldAnnotation(orderIndex=24) 
	public TextField txtQS908D1;
	@FieldAnnotation(orderIndex=25) 
	public RadioGroupOtherField rgQS908E;
	@FieldAnnotation(orderIndex = 26)
	public DateTimeField txtQS908E1;
	@FieldAnnotation(orderIndex=27) 
	public TextField txtQS908E_O;

	
	
//	@FieldAnnotation(orderIndex = 17)
//	public DateTimeField txtQS900_PT_INI;
//	@FieldAnnotation(orderIndex = 18)
//	public DateTimeField txtQS900_PAR_INI;
//	@FieldAnnotation(orderIndex = 19)
//	public DateTimeField txtQS900_PAB_INI;
	
	
	
//	public DateTimeField txtQS900_PT_INI_H, txtQS900_PT_FIN_H;
//	public DateTimeField txtQS900_PAR_INI_H, txtQS900_PAR_FIN_H;
//	public DateTimeField txtQS900_PAB_INI_H, txtQS900_PAB_FIN_H;
	
	public ButtonComponent btnqs900_pt_ini, btnqs900_pt_fin;
	public ButtonComponent btnqs900_par_ini, btnqs900_par_fin;
	public ButtonComponent btnqs900_pab_ini, btnqs900_pab_fin;
	
	private final static Integer r_minimo_sistolica=0;
	private final static Integer r_maximo_sistolica=299;
	private final static Integer r_minimo_diastolica=0;
	private final static Integer r_maximo_diastolica=299;
	
	public Seccion01Service seccion01;
	public Endes_cs_nueve_validar valida=null;
	Salud seccion1; 
	public boolean mef=false;
	public List<Seccion01> ListadoMef;
	public Seccion01 informantedesalud,mefdeviolenciafamiliar;
	public int qh06=0,hora=0, minutos=0;
	SeccionCapitulo[] seccionesCargadoS01; 
	private CSVISITA visita,visitacalculada;
	
	CAP04_07 cap08_09; 
	public TextField txtCabecera, txtanio, txtcodigo, txthogar;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion; 
	private LabelComponent lblPeso,lblTalla; 
	private LabelComponent lblSistolica,lblDiastolica,lblSistolica1,lblDiastolica1,lblhora,lblhoras, lblanio, lblcodigo, lblhogar, lblPreg908b_a, lblPreg908b_b, lblPreg908b_b1, lblPreg908c, lblPreg908d ,lblPreg908bitem, lblPreg908bitem1, lblPreg908bitem2, lblPreg908bitem3 ; 
	private LabelComponent lblpre900_pt_ini_t,lblpre900_pt_fin_t,lblpre900_pt_ini_f,lblpre900_pt_fin_f;
	private LabelComponent lblpre900_par_ini_t,lblpre900_par_fin_t,lblpre900_par_ini_f,lblpre900_par_fin_f, lblinformatesnegrita, lblvacio, lblvacio2;
	private LabelComponent lblpre900_pab_ini_t,lblpre900_pab_fin_t,lblpre900_pab_ini_f,lblpre900_pab_fin_f, lblea, lblEspacio, lblEspacio2;
	private LabelComponent lblPreg900,lblPreg901,lblPreg902,lblPreg903,lblPreg904,lblPreg905,lblPreg906,lblobs_antro,lblPreg907,lblPreg908, lblPreg908c_a, lblPreg908c_b,lblPreg908c_bF, lblPreg908c_c, lblPreg908e;
	private GridComponent2 gridHora, gridverificacion ; 
	public Time hora_inicio;
	public ButtonComponent btnHoraIniciar;
	public String peso,perimetro;
	public String talla;
	public String sistolica1,sistolica2,diastolica1,diastolica2;
	private LabelComponent lblminutos904,lblhora904;
	private GridComponent2 gridPreg904, gridcelular, grid908C; 
	
	Seccion04_05 bean; 
	private Seccion04_05Service seccion04_05Service; 
	SeccionCapitulo[] seccionesCargadoSeccion04,seccionesCargadoVisita;
	public Seccion01 informantehogar;
	 private Seccion01Service Personaservice;
	 private VisitaService visitaService;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	LinearLayout q15; 
	LinearLayout q16;
	LinearLayout q17; 
	LinearLayout q18; 
	LinearLayout q19;
	LinearLayout q20;
	LinearLayout q21;
	LinearLayout q22;
	LinearLayout q23;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionescargado9,seccionesCargado08; 
	private GridComponent2 grid_QS900,grid_QS901,grid_QS903,grid_QS905,grid_QS900_pt,grid_QS900_par,grid_QS900_pab,grid_QS900_pt_hf,grid_QS900_par_hf,grid_QS900_pab_hf;

	public CS_09Fragment_014() {} 
	public CS_09Fragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); //"QS908B_A", "QS908B_B", "QS908C", "QS908C1", "QS908C2", "QS908C3",
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS900_REF_S9", "QS900A", "QS902A", "QS903A", "QS906A", "QS907A", "QS908A",  "QS900","QS901","QS902","QS902_O","QS903","QS903_1","QS904H","QS904M","QS905","QS905_1","QS906","QS906_O","QS907","QS908","QS908_O",  "QS908B_A", "QS908B_B", "QS908C", "QS908C1", "QS908C2", "QS908C3","QS908D", "QS908D1", "QS908E", "QS908E1", "QS908E_O" ,"QSREC_09","QSFRC_FECHA09","QSNOM_ANT","QSCOD_ANT","QSNOM_AUX","QSCOD_AUX","QSOBS_ANTRO","QSOBS_ENTREV","QSOBS_SUPER","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS900_REF_S9", "QS900A", "QS902A", "QS903A", "QS906A", "QS907A", "QS908A",  "QS900","QS901","QS902","QS902_O","QS903","QS903_1","QS904H","QS904M","QS905","QS905_1","QS906","QS906_O","QS907","QS908","QS908_O", "QS908B_A", "QS908B_B", "QS908C", "QS908C1", "QS908C2", "QS908C3", "QS908D", "QS908D1","QS908E", "QS908E1", "QS908E_O" , "QSOBS_ANTRO","QSREC_09","QSFRC_FECHA09")}; //"QS908D", "QS908D1",
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23")};
		seccionesCargadoVisita = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI",	"QSVHORA_FIN","QSVMINUTO_FIN","QSVRESUL","QSVRESUL_O","QSRESULT", "QSRESULT_O","QSPROX_DIA","QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionescargado9= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS731AH","QS731AM","QS902","QS906","HOGAR_ID","ID","PERSONA_ID")};
		seccionesCargado08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","QS802A","QS802D","PERSONA_ID_NINIO","ESTADO")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs900_titulo).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		lblpre900_pt_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs900a).textSize(17);
		lblpre900_pt_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs902a).textSize(17);
	    lblpre900_pt_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    lblpre900_pt_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
	    lblpre900_par_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs903a).textSize(17);
	    lblpre900_par_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs906a).textSize(17);
	    lblpre900_par_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    lblpre900_par_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
	    lblpre900_pab_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs907a).textSize(17);
	    lblpre900_pab_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908a).textSize(17);
	    lblpre900_pab_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    lblpre900_pab_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
	    btnqs900_pt_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
	    btnqs900_pt_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
	    btnqs900_par_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
	    btnqs900_par_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
	    btnqs900_pab_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
	    btnqs900_pab_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
	    
	    
	    btnqs900_pt_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_pt_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				btnqs900_pt_fin.setEnabled(true);
			}
		});
	    btnqs900_pt_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_pt_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
	    btnqs900_par_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_par_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				btnqs900_par_fin.setEnabled(true);
			}
		});
	    btnqs900_par_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_par_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
	    btnqs900_pab_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_pab_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				btnqs900_pab_fin.setEnabled(true);
			}
		});
	    btnqs900_pab_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre900_pab_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
		
	    grid_QS900_pt = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_pt.addComponent(btnqs900_pt_ini);
	    grid_QS900_pt.addComponent(lblpre900_pt_ini_f);
	    
	    grid_QS900_pt_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_pt_hf.addComponent(btnqs900_pt_fin);
	    grid_QS900_pt_hf.addComponent(lblpre900_pt_fin_f);
	    
	    grid_QS900_par = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_par.addComponent(btnqs900_par_ini);
	    grid_QS900_par.addComponent(lblpre900_par_ini_f);
	    
	    grid_QS900_par_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_par_hf.addComponent(btnqs900_par_fin);
	    grid_QS900_par_hf.addComponent(lblpre900_par_fin_f);
	    
	    grid_QS900_pab = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_pab.addComponent(btnqs900_pab_ini);
	    grid_QS900_pab.addComponent(lblpre900_pab_ini_f);
	    
	    grid_QS900_pab_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QS900_pab_hf.addComponent(btnqs900_pab_fin);
	    grid_QS900_pab_hf.addComponent(lblpre900_pab_fin_f);
	    
	        
		
		lblPeso= new LabelComponent(this.getActivity()).size(60, 600).text(R.string.cap08_09qs900).textSize(17);
		lblTalla= new LabelComponent(this.getActivity()).size(60, 600).text(R.string.cap08_09qs901).textSize(17);
		txtQS900=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP900ChangeValue");
		txtQS901=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP901ChangeValue");
		
		grid_QS900 = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
        grid_QS900.addComponent(lblPeso);
        grid_QS900.addComponent(txtQS900);
        
        grid_QS901 = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
        grid_QS901.addComponent(lblTalla);
        grid_QS901.addComponent(txtQS901);
        lblPreg902 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs902);
        lblPreg903 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs903);
        lblPreg904 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs904);
        lblPreg905 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs905);
        lblPreg906 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs906);
        lblPreg907 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs907);
        lblPreg908 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908);
        
        rgQS902=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs902_1,R.string.cap08_09qs902_2,R.string.cap08_09qs902_3,R.string.cap08_09qs902_4,R.string.cap08_09qs902_5,R.string.cap08_09qs902_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP902ChangeValue"); 
		txtQS902_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQS902.agregarEspecifique(5,txtQS902_O); 
		
		rgQS906=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs906_1,R.string.cap08_09qs906_2,R.string.cap08_09qs906_3,R.string.cap08_09qs906_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP906ChangeValue"); 
		txtQS906_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQS906.agregarEspecifique(3,txtQS906_O); 
		
		lblSistolica= new LabelComponent(this.getActivity()).size(60, 250).text(R.string.cap08_09qs903_1).textSize(19);
		txtQS903=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(3).callback("onP903ChangeValue");   
		lblDiastolica= new LabelComponent(this.getActivity()).size(60, 250).text(R.string.cap08_09qs903_2).textSize(19);
		txtQS903_1=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(3).callback("onP903dChangeValue");  
		 
        grid_QS903 = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
        grid_QS903.addComponent(lblSistolica);
        grid_QS903.addComponent(txtQS903);
        grid_QS903.addComponent(lblDiastolica);
        grid_QS903.addComponent(txtQS903_1);
        
        lblhora904 = new LabelComponent(getActivity()).textSize(16).size(MATCH_PARENT, 80).text(R.string.cap08_09qs904h).alinearDerecha();
        lblminutos904 = new LabelComponent(getActivity()).textSize(16).size(MATCH_PARENT, 120).text(R.string.cap08_09qs904m).alinearDerecha();
        txtQS904H=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onP904hChangeValue");
        txtQS904M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onP904mChangeValue"); 
        
        gridPreg904 = new GridComponent2(this.getActivity(),Gravity.LEFT, 4,0);
        gridPreg904.addComponent(lblhora904);
        gridPreg904.addComponent(txtQS904H);
        gridPreg904.addComponent(lblminutos904);
        gridPreg904.addComponent(txtQS904M);
        
//      lblhora = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 300).text(R.string.cap08_09qs802bh);
//		btnHoraIniciar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(200, 55);
//		lblhoras = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 100).text("");
//		
//		gridHora = new GridComponent2(this.getActivity(),App.ESTILO,3,1);
//		gridHora.addComponent(lblhora);		
//		gridHora.addComponent(btnHoraIniciar);
//		gridHora.addComponent(lblhoras);
        
        lblSistolica1= new LabelComponent(this.getActivity()).size(60, 250).text(R.string.cap08_09qs905_1).textSize(19);
        lblobs_antro = new LabelComponent(getActivity()).size(60, 400).text(R.string.obs_generales).textSize(17).negrita();
        txtQS905=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(3).callback("onP905ChangeValue");   
		lblDiastolica1= new LabelComponent(this.getActivity()).size(60, 250).text(R.string.cap08_09qs905_2).textSize(19);
		txtQS905_1=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(3).callback("onP905_dChangeValue");  
		
        grid_QS905 = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
        grid_QS905.addComponent(lblSistolica1);
        grid_QS905.addComponent(txtQS905);
        grid_QS905.addComponent(lblDiastolica1);
        grid_QS905.addComponent(txtQS905_1);
        
        
        txtQS907=new DecimalField(this.getActivity()).maxLength(4).size(60, 100).decimales(1).callback("onP907ChangeValue");;
        rgQS908=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908_1,R.string.cap08_09qs908_2,R.string.cap08_09qs908_3,R.string.cap08_09qs908_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);//.callback("onP908_dChangeValue"); 
		txtQS908_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQS908.agregarEspecifique(3,txtQS908_O); 
		
        
        txtQSOBS_ANTRO= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
        
        ///////////////////////////
        lblvacio2 = new LabelComponent(this.getActivity()).textSize(17).size(50, MATCH_PARENT).text("");
        lblEspacio = new LabelComponent(this.getActivity()).textSize(17).size(250, MATCH_PARENT).text("");
        lblvacio = new LabelComponent(this.getActivity()).textSize(17).size(100, MATCH_PARENT).text("");
        lblEspacio2 = new LabelComponent(this.getActivity()).textSize(17).size(20, MATCH_PARENT).text("");
        
        lblPreg908b_a = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908c_a);
        rgQS908B_A=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908c_a1,R.string.cap08_09qs908c_a2).size(altoComponente, MATCH_PARENT).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP908b_aChangeValue"); 
        
        
        
        lblPreg908b_b = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908b_b); 
        lblea = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text("LEA:"); 
        lblinformatesnegrita = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908b_negrita).negrita(); 
        lblPreg908b_b1 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908c_bF);
        rgQS908B_B=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908c_c1,R.string.cap08_09qs908c_c2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onP908_dChangeValue"); 

        lblPreg908c = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908b); 
        rgQS908C=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908c_b1a,R.string.cap08_09qs908c_b2a,R.string.cap08_09qs908c_b3a).size(550,250).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP908cChangeValue"); 
        
        //rgQS908C.agregarTitle(0, lblvacio2);
        rgQS908C.agregarTitle(1, lblEspacio);
        //rgQS908C.setCustomHeight(position, customHeight);
        
        lblPreg908bitem1 = new LabelComponent(this.getActivity()).textSize(17).size(45, 320).text(R.string.cap08_09qs908bite1).centrar();
        txtQS908C1 =new IntegerField(this.getActivity()).size(altoComponente, 220).centrar().maxLength(9);  
        lblPreg908bitem2 = new LabelComponent(this.getActivity()).textSize(17).size(45, 320).text(R.string.cap08_09qs908bite2).centrar();
        txtQS908C2 =new IntegerField(this.getActivity()).size(altoComponente, 220).centrar().maxLength(9);  
        lblPreg908bitem3 = new LabelComponent(this.getActivity()).textSize(17).size(45, 320).text(R.string.cap08_09qs908bite3).centrar();
        txtQS908C3 =new IntegerField(this.getActivity()).size(altoComponente, 220).centrar().maxLength(7);  
        
        gridcelular = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
        gridcelular.addComponent(rgQS908C, 1, 7);
        gridcelular.addComponent(lblPreg908bitem1);
        gridcelular.addComponent(txtQS908C1);
        gridcelular.addComponent(lblPreg908bitem2);
        gridcelular.addComponent(txtQS908C2);
        gridcelular.addComponent(lblPreg908bitem3);
        gridcelular.addComponent(txtQS908C3);
        gridcelular.addComponent(lblvacio);
        
        lblPreg908d = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908d); 
        rgQS908D = new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908d1,R.string.cap08_09qs908d2,R.string.cap08_09qs908d3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
        txtQS908D1=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 300); 
        rgQS908D.agregarEspecifique(0,txtQS908D1);
        
        
        Spanned texto = Html.fromHtml("908E.<b> SI EN 908B.b. SI ACEPTA:</b> ENTREVISTADORA ENTREGAR EL FRASCO DE PL�STICO PARA MUESTRA DE ORINA <br> <br> TENER EN CUENTA QUE SI NO ENTREGO EN UN PRIMER MOMENTO, DEBERA RETORNAR A LA VIVIENDA A REALIZAR LA ENTREGA DEL FRASCO OPORTUNAMENTE PARA LA VISITA DEL INS<br><br>"); 
        lblPreg908e = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
        lblPreg908e.setText(texto);
        
        //lblPreg908e = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs908e); 
        
        
        rgQS908E = new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs908e1,R.string.cap08_09qs908e2, R.string.cap08_09qs908e3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP908eChangeValue");  
        txtQS908E_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 450); 
        txtQS908E1 = new DateTimeField(this.getActivity(), TIPO_DIALOGO.FECHA,	"dd/MM/yyyy").size(altoComponente, 300).setRangoYear(App.ANIOPORDEFECTOSUPERIOR,App.ANIOPORDEFECTOSUPERIOR);
        rgQS908E.agregarEspecifique(1,txtQS908E_O);
        rgQS908E.agregarTitle(1, lblEspacio2);
        rgQS908E.agregarEspecifique(0,txtQS908E1);
        
       
        
        
//        "QS908B_A", "QS908B_B", "QS908C", "QS908C1", "QS908C2", "QS908C3", "QS908D", "QS908D1"
        
//        grid908C = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
//        grid908C.addComponent(rgQS908B);
//        grid908C.addComponent(gridcelular.component(),1);
        
        //////////////////////////////////////////////////////

        
        
//        btnHoraIniciar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//					// TODO Auto-generated method stub
//					Calendar calendario = new GregorianCalendar();
//					Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
//					Integer minute= calendario.get(Calendar.MINUTE);
//					String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
//					String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
//					lblhoras.setText(muestrahora+":"+muestraminuto);
//				}
//			});
        
       
        
        
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion);
		
		q1 = createQuestionSection(lblpre900_pt_ini_t,grid_QS900_pt.component());
		q2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,grid_QS900.component()); 
		q3 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,grid_QS901.component()); 
		q4 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg902,rgQS902);
		q5 = createQuestionSection(lblpre900_pt_fin_t,grid_QS900_pt_hf.component());
		
		q6 = createQuestionSection(lblpre900_par_ini_t,grid_QS900_par.component());
		q7 = createQuestionSection(lblPreg903,grid_QS903.component()); 
		q8 = createQuestionSection(lblPreg904,gridPreg904.component());
		q9 = createQuestionSection(lblPreg905,grid_QS905.component()); 
		q10 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg906,rgQS906);
		q11 = createQuestionSection(lblpre900_par_fin_t,grid_QS900_par_hf.component());
		
		q12 = createQuestionSection(lblpre900_pab_ini_t,grid_QS900_pab.component());
		q13 = createQuestionSection(lblPreg907,txtQS907);
		q14 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg908,rgQS908);
		q15 = createQuestionSection(lblpre900_pab_fin_t,grid_QS900_pab_hf.component());
		
		q16 = createQuestionSection(lblobs_antro,txtQSOBS_ANTRO);
		
		
		q17 = createQuestionSection(lblPreg908b_a,rgQS908B_A);
		q18 = createQuestionSection(lblPreg908b_b,lblea, lblinformatesnegrita, lblPreg908b_b1, rgQS908B_B);
		
		q19 = createQuestionSection(lblPreg908c, gridcelular.component());
		//q19 = createQuestionSection(0, Gravity.CENTER, LinearLayout.VERTICAL,gridverificacion.component()); 
		q20 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg908d,rgQS908D);
		q21 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg908e,rgQS908E);

		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		form.addView(q11);
		form.addView(q12);
		form.addView(q13);
		form.addView(q14);
		form.addView(q15);
		form.addView(q16);
		
		form.addView(q17);
		form.addView(q18);
		form.addView(q19);
		form.addView(q20);
		form.addView(q21);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(cap08_09); 
		
		if (cap08_09.qs906 != null) {
			cap08_09.qs906 = cap08_09.getConvert906(cap08_09.qs906);
		}
		if (cap08_09.qs908 != null) {
			cap08_09.qs908 = cap08_09.getConvert906(cap08_09.qs908);
		}
		cap08_09.qs900a =lblpre900_pt_ini_f.getText().toString();
		cap08_09.qs902a =lblpre900_pt_fin_f.getText().toString();
		cap08_09.qs903a =lblpre900_par_ini_f.getText().toString();
		cap08_09.qs906a =lblpre900_par_fin_f.getText().toString();
		cap08_09.qs907a =lblpre900_pab_ini_f.getText().toString();
		cap08_09.qs908a =lblpre900_pab_fin_f.getText().toString();				
		if(cap08_09.qs900_ref_s9==null ){
			cap08_09.qs900_ref_s9=Util.getFechaActualToString();
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(valida.qs900==null && valida.qs901==null && valida.qs902==null &&
			   valida.qs902_o==null && valida.qs903==null && valida.qs903_1==null &&
			   valida.qs904h==null && valida.qs904m==null && valida.qs905==null &&
			   valida.qs906==null && valida.qs906_o==null && valida.qs907==null &&
			   valida.qs908==null && valida.qs908_o==null && valida.qsobs_antro==null){
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS900", cap08_09.qs900==null?null:cap08_09.qs900+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS901", cap08_09.qs901==null?null:cap08_09.qs901+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS902", cap08_09.qs902==null?null:cap08_09.qs902+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS902_O", cap08_09.qs902_o==null?null:cap08_09.qs902_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS903", cap08_09.qs903==null?null:cap08_09.qs903+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS903_1", cap08_09.qs903_1==null?null:cap08_09.qs903_1+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS904H", cap08_09.qs904h==null?null:cap08_09.qs904h+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS904M", cap08_09.qs904m==null?null:cap08_09.qs904m+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS905", cap08_09.qs905==null?null:cap08_09.qs905+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS906", cap08_09.qs906==null?null:cap08_09.qs906+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS906_O", cap08_09.qs906_o==null?null:cap08_09.qs906_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS907", cap08_09.qs907==null?null:cap08_09.qs907+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS908", cap08_09.qs908==null?null:cap08_09.qs908+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS908_O", cap08_09.qs908_o==null?null:cap08_09.qs908_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
				getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSOBS_ANTRO", cap08_09.qsobs_antro==null?null:cap08_09.qsobs_antro+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);
			}
			else{
				if(Util.esDiferente(valida.qs900, cap08_09.qs900)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS900", cap08_09.qs900==null?null:cap08_09.qs900+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs901, cap08_09.qs901)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS901", cap08_09.qs901==null?null:cap08_09.qs901+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs902, cap08_09.qs902)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS902", cap08_09.qs902==null?null:cap08_09.qs902+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs902_o, cap08_09.qs902_o)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS902_O", cap08_09.qs902_o==null?null:cap08_09.qs902_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs903, cap08_09.qs903)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS903", cap08_09.qs903==null?null:cap08_09.qs903+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs903_1, cap08_09.qs903_1)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS903_1", cap08_09.qs903_1==null?null:cap08_09.qs903_1+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs904h, cap08_09.qs904h)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS904H", cap08_09.qs904h==null?null:cap08_09.qs904h+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs904m, cap08_09.qs904m)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS904M", cap08_09.qs904m==null?null:cap08_09.qs904m+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs905, cap08_09.qs905)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS905", cap08_09.qs905==null?null:cap08_09.qs905+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs906, cap08_09.qs906)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS906", cap08_09.qs906==null?null:cap08_09.qs906+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs906_o, cap08_09.qs906_o)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS906_O", cap08_09.qs906_o==null?null:cap08_09.qs906_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs907, cap08_09.qs907)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS907", cap08_09.qs907==null?null:cap08_09.qs907+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs908, cap08_09.qs908)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS908", cap08_09.qs908==null?null:cap08_09.qs908+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qs908_o, cap08_09.qs908_o)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QS908_O", cap08_09.qs908_o==null?null:cap08_09.qs908_o+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
				if(Util.esDiferente(valida.qsobs_antro, cap08_09.qsobs_antro)){getSeccion04_05Service().saveOrUpdate(EndesCalendario.GuardarPreguntaDetalle("QSOBS_ANTRO", cap08_09.qsobs_antro==null?null:cap08_09.qsobs_antro+"", cap08_09.id, cap08_09.hogar_id,cap08_09.persona_id), null);}
			}
	
			if(!getCuestionarioService().saveOrUpdate(cap08_09,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		GrabarVisitaCalculada(visita);
		App.getInstance().setC1Visita(visita);
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		 App.getInstance().getSaludNavegacionS4_7().qs906=cap08_09.qs906;
		return true; 
    } 
    
    public void GrabarVisitaCalculada(CSVISITA visita){
    	visita.qsresult=App.SALUD_RESULTADO_COMPLETADO;
		visitacalculada=MyUtil.VerificarCompletadoVisitaDeSalud(visita,seccionesCargado08,getCuestionarioService(),seccionescargado9);
		if(visitacalculada!=null){
			try {
				getVisitaService().saveOrUpdate(visitacalculada,"QSRESULT","QSVRESUL","QSVHORA_FIN","QSVMINUTO_FIN","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","NRO_VISITA");
			} catch (SQLException e) {
				ToastMessage.msgBox(getActivity(),e.getMessage()+"error visita calculada",
	                    ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	}
    
    public boolean tieneDecimal(String numero){
    	boolean punto=false;
    	int pos=0;

		 for(int i=0;i<numero.length();i++){
				if(numero.charAt(i)=='.'){		
					 pos=i;
					 i=numero.length();
					 
				}
		}
		 if(pos==0) pos=9999;
		 if(pos+1<numero.length()){
			 punto=true;
		 }else{
			 punto=false;
		 }
		 return punto;
    }
    
    private boolean validar() { 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		sistolica1 = cap08_09.qs903!=null?cap08_09.qs903.toString():"0";
		diastolica1 = cap08_09.qs903_1!=null?cap08_09.qs903_1.toString():"0";
		sistolica2 = cap08_09.qs905!=null?cap08_09.qs905.toString():"0";
    	diastolica2 = cap08_09.qs905_1!=null?cap08_09.qs905_1.toString():"0";
    
    	
    	peso = cap08_09.qs900!=null?cap08_09.qs900.toString():"";
    	talla = cap08_09.qs901!=null?cap08_09.qs901.toString():"";
    	perimetro = cap08_09.qs907!=null?cap08_09.qs907.toString():"";
    	
    	if(talla.equals("")){
		
			talla="999";
		}
		if(peso.equals("")){
	
			peso="999";
		}
		
		if(perimetro.equals("")){
			
			perimetro="999";
		}
		
		if (!Util.esDiferente(cap08_09.qs906,1)) {
			if(!peso.equals("999")){
				if(!MyUtil.incluyeRango(23.0,200.0,peso)){
					mensaje = "La pregunta P.900 debe estar en el rango 23-200"; 
		  			view = txtQS900; 
		  			error = true; 
		  			return false; 
				}
			}
			if(!talla.equals("999")){
				if(!MyUtil.incluyeRango(110.0,200.0,talla)){
					mensaje = "La pregunta P.901 debe estar en el rango 110-200"; 
		  			view = txtQS901; 
		  			error = true; 
		  			return false; 
				}
			}
		}
		
		boolean punto=false;
		boolean punto2=false;
		boolean punto3=false;
		 punto=tieneDecimal(peso);
		 punto2=tieneDecimal(talla);
		 punto3=tieneDecimal(perimetro);	
		 
		 if(!peso.equals("999")){
			 if(!punto){
				 mensaje = "La pregunta P.900 debe tener . decimal"; 
	    			view = txtQS900; 
	    			error = true; 
	    			return false; 
			 }
		 }
		 
		 if(!talla.equals("999")){
			 if(!punto2){
				 mensaje = "La pregunta P.901 debe tener . decimal"; 
	    			view = txtQS901; 
	    			error = true; 
	    			return false; 
			 }
		 }
		 
		 if(!perimetro.equals("999")){
			 if(!punto3){
				 mensaje = "La pregunta P.907 debe tener . decimal"; 
	    			view = txtQS907; 
	    			error = true; 
	    			return false; 
			 }
		 }

		
		if(bean!=null){
			if(bean.qh207!=null){
				if(!mef || (mef && bean.qh207!=0 && bean!=null)){
					if (Util.esVacio(cap08_09.qs902)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.902"); 
		    			view = rgQS902; 
		    			error = true; 
		    			return false; 
		    		} 
		    		if(!Util.esDiferente(cap08_09.qs902,6)){ 
		    			if (Util.esVacio(cap08_09.qs902_o)) { 
		    				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
		    				view = txtQS902_O; 
		    				error = true; 
		    				return false; 
		    			} 
		    		}
		    		
		    	}
		    }
		}
	
	if (!Util.esDiferente(cap08_09.qs906,1)) {
		if(!sistolica1.equals("") || !diastolica1.equals("") || !sistolica2.equals("") || !diastolica2.equals("")){
			if(sistolica1.equals("")){
				if (Util.esVacio(cap08_09.qs903)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.903"); 
					view = txtQS903; 
					error = true; 
					return false; 
				} 
			}
			if (!MyUtil.incluyeRango(r_minimo_sistolica,r_maximo_sistolica,sistolica1)) { 
				mensaje = "La pregunta P.903 debe estar en el rango "+ r_minimo_sistolica+" - "+r_maximo_sistolica;
				view = txtQS903; 
				error = true; 
				return false; 
			} 
			if(diastolica1.equals("")){
				if (Util.esVacio(cap08_09.qs903_1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.903_1"); 
					view = txtQS903_1; 
					error = true; 
					return false; 
				} 
			}
			if (!MyUtil.incluyeRango(r_minimo_diastolica,r_maximo_diastolica,diastolica1)) { 
				mensaje = "La pregunta P.903_1 debe estar en el rango "+r_minimo_diastolica+" - "+ r_maximo_diastolica;
				view = txtQS903_1; 
				error = true; 
				return false; 
			} 
			if(sistolica2.equals("")){
				if (Util.esVacio(cap08_09.qs905)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.905"); 
					view = txtQS905; 
					error = true; 
					return false; 
				} 
			}
			if (!MyUtil.incluyeRango(r_minimo_sistolica,r_maximo_sistolica,sistolica2)) { 
				mensaje = "La pregunta P.905 debe estar en el rango "+r_minimo_sistolica+" - "+r_maximo_sistolica; 
				view = txtQS905; 
				error = true; 
				return false; 
			} 
			if(diastolica2.equals("")){
				if (Util.esVacio(cap08_09.qs905_1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.905_1"); 
					view = txtQS905_1; 
					error = true; 
					return false; 
				} 
			}
			if (!MyUtil.incluyeRango(r_minimo_diastolica,r_maximo_diastolica,diastolica2)) { 
				mensaje = "La pregunta P.905_1 debe estar en el rango "+r_minimo_diastolica+" - "+r_maximo_diastolica; 
				view = txtQS905_1; 
				error = true; 
				return false; 
			} 
		}
		
		
	    int s1,s2,d1,d2,varS,varD;
	    if(!sistolica1.equals("") && !diastolica1.equals("") && !sistolica2.equals("") && !diastolica2.equals("")){
	    	s1=Integer.parseInt(sistolica1);
		    s2=Integer.parseInt(sistolica2);
		    d1=Integer.parseInt(diastolica1);
		    d2=Integer.parseInt(diastolica2);
			if(Util.esMayor(s1,s2)){
				varS=s1-s2;
			}else varS=s2-s1;
			if(Util.esMayor(d1,d2)){
				varD=d1-d2;
			}else varD=d2-d1;
			if(varS>=20){
					mensaje = "Verifique diferencia entre Presiones Sist�licas"; 
					view = txtQS905; 
					error = true; 
					return false; 
			}
			if(varD>=10){
				mensaje = "Verifique diferencia entre Presiones Diast�licas"; 
				view = txtQS905_1; 
				error = true; 
				return false; 
		    }
	    }
	}
	
	

	
		if (Util.esVacio(cap08_09.qs906)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.906"); 
			view = rgQS906; 
			error = true; 
			return false; 
		} 
		if(!Util.esDiferente(cap08_09.qs906,6)){ 
			if (Util.esVacio(cap08_09.qs906_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQS906_O; 
				error = true; 
				return false; 
			} 
		}
	

		if(!txtQS904H.getText().toString().equals("")){
       	 hora=Integer.parseInt(txtQS904H.getText().toString());
        }
		
		if(!txtQS904M.getText().toString().equals("")){
	       	minutos=Integer.parseInt(txtQS904M.getText().toString());
	    }
		 
		if(!Util.esDiferente(cap08_09.qs906,1)){ 

    		if (Util.esVacio(cap08_09.qs904h)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.904H"); 
    			view = txtQS904H; 
    			error = true; 
    			return false; 
    		} 
    			if (!MyUtil.incluyeRango(0,23,hora)) { 
    				mensaje = "La pregunta P.904h debe estar en el rango 0-23"; 
    				view = txtQS904H; 
    				error = true; 
    				return false; 
    			} 
    		
    		if (Util.esVacio(cap08_09.qs904m)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.904M"); 
    			view = txtQS904M; 
    			error = true; 
    			return false; 
    		} 
    			if (!MyUtil.incluyeRango(0,59,minutos)) { 
    				mensaje = "La pregunta P.904m debe estar en el rango 0-59"; 
    				view = txtQS904M; 
    				error = true; 
    				return false; 
    			} 
    	}
		if(mef && bean!=null && bean.qh207==null){
			mensaje = "Debe completar primero la seccion4 del cuestionario del hogar\n (Se recomienda usar Grabado Parcial)"; 
			view = rgQS902; 
			error = true; 
			return false;
		}
		
		
		
    	if (!Util.esDiferente(cap08_09.qs908,1)) {
    		if (Util.esVacio(cap08_09.qs907)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.907"); 
    			view = txtQS907; 
    			error = true; 
    			return false; 
    		}
    		if(!perimetro.equals("999")){
    			double d = cap08_09.qs907.doubleValue(); 
    			long parteentera = (long) d;
    			Integer dato=(int)parteentera;
    			if(Util.esMenor(dato, 50)){
    				mensaje = "La pregunta P.908 esta fuera de rango";
    				view = txtQS907; 
		  			error = true; 
		  			return false; 
    			}
    			if(Util.between(dato, 51,80)){
    				mensaje = "verifique valor de la P.907";
    				MyUtil.MensajeGeneral(getActivity(), mensaje);
    			}
    			if(Util.between(dato, 109,199)){
    				mensaje = "verifique valor de la P.907 ";
    				MyUtil.MensajeGeneral(getActivity(), mensaje);
    			}
    			if(Util.esMayor(dato, 200)){
    				mensaje = "La pregunta P.908 esta fuera de rango";
    				view = txtQS907; 
		  			error = true; 
		  			return false; 
    			}
			}
    	}
    	

    	if (Util.esVacio(cap08_09.qs908)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.908"); 
			view = rgQS908; 
			error = true; 
			return false; 
		}
    	if (!Util.esDiferente(cap08_09.qs908,6)) {
    		if (Util.esVacio(cap08_09.qs908_o)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.908_o"); 
    			view = txtQS908_O; 
    			error = true; 
    			return false; 
    		}     		
    	}

    	
		
    	
    	if(!Util.esDiferente(cap08_09.qs902,1,5) && !mef){
    		 if(lblpre900_pt_ini_f.getText().toString().trim().length()==0){
    				mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 900A");
    				view = btnqs900_pt_ini;
    				error = true;
    				return false;
    		}
    		if(lblpre900_pt_fin_f.getText().toString().trim().length()==0){
 				mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 902A");
 				view = btnqs900_pt_fin	;
 				error = true;
 				return false;
    		}
    	}
    	if(lblpre900_pt_ini_f.getText().toString().trim().length()!=0 && lblpre900_pt_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_iniA = new GregorianCalendar();
    		if(lblpre900_pt_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_pt_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_iniA=cal;			
    		}
    		
    		Calendar fecha_finA = new GregorianCalendar();
        	if(lblpre900_pt_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_pt_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_finA=cal;			
    		}
        	if (Util.compare((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) < 0) {
        		mensaje = "Fecha final P902A no puede ser menor a la fecha inicial"; 
    			view = lblpre900_pt_fin_f; 
    			error = true; 
    			return false;
        	}
        	
        	if ( (Util.compare((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) == 0)  && (Util.compareTime((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) < 0)) {
        		mensaje = "Hora final P902A no puede ser menor a la hora inicial"; 
    			view = lblpre900_pt_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
    	
    	
    	if(!Util.esDiferente(cap08_09.qs902,1)){
    		if(lblpre900_par_ini_f.getText().toString().trim().length()==0){
				mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 903A");
				view = btnqs900_par_ini;
				error = true;
				return false;
			}
			if(lblpre900_par_fin_f.getText().toString().trim().length()==0){
					mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 906A");
					view = btnqs900_par_fin	;
					error = true;
					return false;
			}	
    	}
    	if(lblpre900_par_ini_f.getText().toString().trim().length()!=0 && lblpre900_par_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_iniB = new GregorianCalendar();
    		if(lblpre900_par_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_par_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_iniB=cal;			
    		}
    		
    		Calendar fecha_finB = new GregorianCalendar();
        	if(lblpre900_par_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_par_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_finB=cal;			
    		}
        	if (Util.compare((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) < 0) {
        		mensaje = "Fecha final P906A no puede ser menor a la fecha inicial"; 
    			view = lblpre900_par_fin_f; 
    			error = true; 
    			return false;
        	}	
        	if ( (Util.compare((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) == 0)  && (Util.compareTime((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) < 0)) {
        		mensaje = "Hora final P906A no puede ser menor a la hora inicial"; 
    			view = lblpre900_par_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
    	
    	
    	
    	if(!Util.esDiferente(cap08_09.qs908,1)){
        	if(lblpre900_pab_ini_f.getText().toString().trim().length()==0){
				mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 907A");
				view = btnqs900_pab_ini;
				error = true;
				return false;
			}
			if(lblpre900_pab_fin_f.getText().toString().trim().length()==0){
					mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 908A");
					view = btnqs900_pab_fin	;
					error = true;
					return false;
			}	
    	}
    	if(lblpre900_pab_ini_f.getText().toString().trim().length()!=0 && lblpre900_pab_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_iniC = new GregorianCalendar();
    		if(lblpre900_pab_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_pab_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_iniC=cal;			
    		}
    		
    		Calendar fecha_finC = new GregorianCalendar();
        	if(lblpre900_pab_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre900_pab_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_finC=cal;			
    		}
        	if (Util.compare((Date) fecha_finC.getTime(),(Date) fecha_iniC.getTime()) < 0) {
        		mensaje = "Fecha final P908A no puede ser menor a la fecha inicial"; 
    			view = lblpre900_pab_fin_f; 
    			error = true; 
    			return false;
        	}	
        	if ( (Util.compare((Date) fecha_finC.getTime(),(Date) fecha_iniC.getTime()) == 0)  && (Util.compareTime((Date) fecha_finC.getTime(),(Date) fecha_iniC.getTime()) < 0)) {
        		mensaje = "Hora final P908A no puede ser menor a la hora inicial"; 
    			view = lblpre900_pab_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
    	
    	
    	if (Util.esVacio(cap08_09.qs908b_a)){
			 mensaje = preguntaVacia.replace("$", "La pregunta P.908B_A"); 
			 view = rgQS908B_A; 
			 error = true; 
			 return false; 
		}
    	
    	
    	if (!Util.esDiferente(cap08_09.qs908b_a,1)) {
    		if (Util.esVacio(cap08_09.qs908b_b)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.908B_B"); 
				 view = rgQS908B_B; 
				 error = true; 
				 return false; 
    		}
    		if (Util.esVacio(cap08_09.qs908c)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.908C"); 
				 view = rgQS908C; 
				 error = true; 
				 return false; 
    		}
    		if (Util.esVacio(cap08_09.qs908d)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.908D"); 
				 view = rgQS908D; 
				 error = true; 
				 return false; 
    		}
    		if (Util.esVacio(cap08_09.qs908e)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.908E"); 
				 view = rgQS908E; 
				 error = true; 
				 return false; 
   		}
    	}
    	
    	
    	
    	if (!Util.esDiferente(cap08_09.qs908c,1)) {
    		
    		if (Util.esVacio(cap08_09.qs908c1) && Util.esVacio(cap08_09.qs908c2) && Util.esVacio(cap08_09.qs908c3)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta N�mero de tel�fono"); 
    			view = txtQS908C1; 
    			error = true; 
    			return false; 
    		}
    		
    		if (!Util.esVacio(cap08_09.qs908c1)){
    			String phonePattern = "^[0-9]{9}$";
    			 if(!cap08_09.qs908c1.matches(phonePattern)){
    				 mensaje = "N�mero de celular no v�lido"; 
    				 view = txtQS908C1; 
    				 error = true; 
    				 return false; 
    			 }
    		}
    		if (!Util.esVacio(cap08_09.qs908c2)){
    			String phonePattern = "^[0-9]{9}$";
	   			 if(!cap08_09.qs908c2.matches(phonePattern)){
	   				 mensaje = "N�mero de celular no v�lido"; 
	   				 view = txtQS908C2; 
	   				 error = true; 
	   				 return false; 
	   			 }
    		}
    		if (!Util.esVacio(cap08_09.qs908c3)){
    			String phonePattern = "^[0-9]{7}$";
	   			 if(!cap08_09.qs908c3.matches(phonePattern)){
	   				 mensaje = "N�mero de tel�fono no v�lido"; 
	   				 view = txtQS908C3; 
	   				 error = true; 
	   				 return false; 
	   			 }
    		 }
    		
    	}
    	
    	if (!Util.esDiferente(cap08_09.qs908d,1)) {
    		if (Util.esVacio(cap08_09.qs908d1)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta correo"); 
    			view = txtQS908D1; 
    			error = true; 
    			return false; 
    		}
    		
    		if(!Patterns.EMAIL_ADDRESS.matcher(cap08_09.qs908d1).matches()){
    			mensaje = "Correo electr�nico no v�lido"; 
    			view = txtQS908D1; 
    			error = true; 
    			return false; 
    		}
    		
    	}
    	
   	/////ddddd
    	if (!Util.esDiferente(cap08_09.qs908e,2)) {

    		if (Util.esVacio(cap08_09.qs908e_o)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.908E_O"); 
    			view = txtQS908E_O; 
    			error = true; 
    			return false; 
    		}
    		
    		
    	}
    	
    	if (!Util.esDiferente(cap08_09.qs908e,1)) {
    		if (Util.esVacio(cap08_09.qs908e1)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.908E1"); 
    			view = txtQS908E1; 
    			error = true; 
    			return false; 
    		}
    	}
    	
    	
    	
    	if (Util.esVacio(cap08_09.qs902)){
			 mensaje = preguntaVacia.replace("$", "La pregunta P.902"); 
			 view = rgQS902; 
			 error = true; 
			 return false; 
		}
    	
    	if(!Util.esDiferente(cap08_09.qs902,6)){ 
			if (Util.esVacio(cap08_09.qs902_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQS902_O; 
				error = true; 
				return false; 
			} 
		}
    	
    	

		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		App.getInstance().setMarco(App.getInstance().getMarco());
		
//		ToastMessage.msgBox(this.getActivity(),	App.getInstance().getMarco().s_orden.toString(),	
//				ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		
//		int anioActual = Calendar.getInstance().get(Calendar.YEAR);
//		txtanio.setText(String.valueOf(anioActual));
//		txtcodigo.setText(App.getInstance().getMarco().s_orden.toString());
		//txthogar.setText(perimetro);
		
		mef=false;
			
		cap08_09 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
		visita = getCuestionarioService().getCAPVISITASALUD(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoVisita);
		qh06=App.getInstance().getPersonaSeccion01().qh06;
		if(qh06==2 && MyUtil.incluyeRango(15, 49, seccion1.qs23)){
    		mef=true;
    	}
		if (cap08_09!=null) {
			cap08_09.MEF=mef;	
		}		
        if(mef){
        	informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, 1);
        	bean= getSeccion04_05Service().getSeccion04_05Tabla(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id,informantehogar.persona_id, App.getInstance().getPersonaSeccion01().persona_id, seccionesCargadoSeccion04);
		}
    	
    	
		if(cap08_09==null){ 
		  cap08_09=new CAP04_07(); 
		  cap08_09.id=App.getInstance().getPersonaSeccion01().id; 
		  cap08_09.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap08_09.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 

		if (cap08_09.qs906!= null) {
			cap08_09.qs906 = cap08_09.setConvert906(cap08_09.qs906);
		}
		if (cap08_09.qs908!= null) {
			cap08_09.qs908 = cap08_09.setConvert906(cap08_09.qs908);
		}
		
		if(cap08_09.qs900a!=null){lblpre900_pt_ini_f.setText(cap08_09.qs900a);}		
		if(cap08_09.qs902a!=null){lblpre900_pt_fin_f.setText(cap08_09.qs902a);}
		if(cap08_09.qs903a!=null){lblpre900_par_ini_f.setText(cap08_09.qs903a);}
		if(cap08_09.qs906a!=null){lblpre900_par_fin_f.setText(cap08_09.qs906a);}
		if(cap08_09.qs907a!=null){lblpre900_pab_ini_f.setText(cap08_09.qs907a);}
		if(cap08_09.qs908a!=null){lblpre900_pab_fin_f.setText(cap08_09.qs908a);}
		

		if(lblpre900_pt_ini_f.getText().toString().trim().length()!=0){btnqs900_pt_fin.setEnabled(true);}else{btnqs900_pt_fin.setEnabled(false);}
		if(lblpre900_par_ini_f.getText().toString().trim().length()!=0){btnqs900_par_fin.setEnabled(true);}else{btnqs900_par_fin.setEnabled(false);}
		if(lblpre900_pab_ini_f.getText().toString().trim().length()!=0){btnqs900_pab_fin.setEnabled(true);}else{btnqs900_pab_fin.setEnabled(false);}
		
		entityToUI(cap08_09); 
		CargaDeDatosParaValidar(cap08_09);
		if(cap08_09.qs900!=null){
			if(!tieneDecimal(cap08_09.qs900.toString())){
				txtQS900.setText(cap08_09.qs900+".0");
			}else{
				txtQS900.setText(cap08_09.getPeso().toString());
			}
			
		}
		if(cap08_09.qs901!=null){
			if(!tieneDecimal(cap08_09.qs901.toString())){
				txtQS901.setText(cap08_09.qs901+".0");
			}else{
				txtQS901.setText(cap08_09.getTalla().toString());
			}
		}
		
		inicio(); 
		
		
		
    } 
    
    
    private void inicio() {     	
    	entra_seccion9();    	
    	if(!mef){    		
        	onP900ChangeValue();
        	onP901ChangeValue();
    	}
    	
    	onP902ChangeValue();
    	if(cap08_09.qs904h!=null){
    		onP904hChangeValue();
    	}
    	if(cap08_09.qs904m!=null){
    		onP904mChangeValue();
    	}
    	
    	onP905_dChangeValue();
    	onP906ChangeValue();
    	
    	onP907ChangeValue();
    	onP908ChangeValue();
    	
    	txtQS900.requestFocus();
    	
    	ValidarsiesSupervisora();
    	App.getInstance().setPersona_04_07(null);
    	App.getInstance().setPersona_04_07(cap08_09);
    	App.getInstance().setPersonabySalud(null);
    	App.getInstance().setPersonabySalud(seccion1);
    	
    	onP908b_aChangeValue();
    	onP908cChangeValue(); 
    	onP908eChangeValue();
    	
    	if(Util.esDiferente(cap08_09.qs908d,1) && !Util.esVacio(cap08_09.qs908d) ){
    		Util.cleanAndLockView(this.getActivity(),txtQS908D1);
    	}
    	
    	
    	txtCabecera.requestFocus();
    } 
    public void CargaDeDatosParaValidar(CAP04_07 datos){
    	valida = new Endes_cs_nueve_validar();
    	valida.id=datos.id;
    	valida.hogar_id=datos.hogar_id;
    	valida.persona_id=datos.persona_id;
    	valida.qs900=datos.qs900;
    	valida.qs901=datos.qs901;
    	valida.qs902=datos.qs902;
    	valida.qs902_o=datos.qs902_o;
    	valida.qs903=datos.qs903;
    	valida.qs903_1=datos.qs903_1;
    	valida.qs904h=datos.qs904h;
    	valida.qs904m=datos.qs904m;
    	valida.qs905=datos.qs905;
    	valida.qs905_1=datos.qs905_1;
    	valida.qs906=datos.qs906;
    	valida.qs906_o=datos.qs906_o;
    	valida.qs907=datos.qs907;
    	valida.qs908=datos.qs908;
    	valida.qs908_o=datos.qs908_o;
    	valida.qsobs_antro=datos.qsobs_antro;
    }
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
				txtQS900.readOnly(); 
				txtQS901.readOnly();
				rgQS902.readOnly();
				txtQS902_O.readOnly(); 
				txtQS903.readOnly();
				txtQS903_1.readOnly();
				txtQS904H.readOnly();
				txtQS904M.readOnly();
				txtQS905.readOnly();
				txtQS905_1.readOnly();
				rgQS906.readOnly();
				txtQS906_O.readOnly();
				txtQS907.readOnly();
				rgQS908.readOnly();
				txtQS908_O.readOnly();
				rgQS908B_A.readOnly();
				rgQS908B_B.readOnly();
				rgQS908C.readOnly();
				rgQS908D.readOnly();
				rgQS908E.readOnly();
				txtQS908C1.readOnly();
				txtQS908C2.readOnly();
				txtQS908C3.readOnly();
				txtQS908D1.readOnly();
				txtQS908E1.readOnly();
				txtQS908E_O.readOnly();				
				
				txtQSOBS_ANTRO.setEnabled(false);
				btnqs900_pt_ini.setEnabled(false);
				btnqs900_pt_fin.setEnabled(false);
				btnqs900_par_ini.setEnabled(false);
				btnqs900_par_fin.setEnabled(false);
				btnqs900_pab_ini.setEnabled(false);
				btnqs900_pab_fin.setEnabled(false);
				
		}
	}
 
    
    public void entra_seccion9(){
    	if(mef){
    		Log.e("","MEF TRUE");
			Util.cleanAndLockView(this.getActivity(),txtQS900,txtQS901);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		q10.setVisibility(View.VISIBLE);
    		
    		
    		if(bean!=null){
    			if(bean.qh207!=null){
    				q4.setVisibility(View.VISIBLE);
    				Util.lockView(getActivity(), false, rgQS902);
            			if(bean.qh207==1){
                			rgQS902.lockButtons(false,3);
                			rgQS902.lockButtons(true,0,1,2,4,5);
                    		rgQS902.requestFocus();
                		}
                		if(bean.qh207==2){
                			rgQS902.lockButtons(false,1);
                			rgQS902.lockButtons(true,0,2,3,4,5);
                    		rgQS902.requestFocus();
                		}
                		if(bean.qh207==3){
                			rgQS902.lockButtons(false,2);
                			rgQS902.lockButtons(true,0,1,3,4,5);
                    		rgQS902.requestFocus();
                		}
                		if(bean.qh207==5){
                			rgQS902.lockButtons(false,4);
                			rgQS902.lockButtons(true,0,1,2,3,5);
                    		rgQS902.requestFocus();
                		}
                		if(bean.qh207==6){
                			rgQS902.lockButtons(false,5);
                			rgQS902.lockButtons(true,0,1,2,3,4);
                    		rgQS902.requestFocus();
                		}
        		}
    			else{
    				 Log.e("","BEAN ELSE 207!=0");
         			validarMensaje("Realizar Medicion de Peso y Talla en Hogar");
         			q4.setVisibility(View.GONE);
         			q7.setVisibility(View.VISIBLE);
             		q8.setVisibility(View.VISIBLE);
             		q9.setVisibility(View.VISIBLE);
             		q10.setVisibility(View.VISIBLE);
    			}
    		}
    		ValidarsiesSupervisora();
    		rgQS902.requestFocus();
		}else{
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		q10.setVisibility(View.VISIBLE);
    		
    		Util.lockView(getActivity(), false, txtQS900);	
    		ValidarsiesSupervisora();
    		txtQS900.requestFocus();
		}
    	
    }
    
    
    public void onP908cChangeValue() {
    	if (MyUtil.incluyeRango(2,3,rgQS908C.getTagSelected("").toString())) {   
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),txtQS908C1, txtQS908C2, txtQS908C3 );  		
    		//q19.setVisibility(View.GONE);
    		//q20.setVisibility(View.GONE);
    		
    	} 
    	else {
    		Util.lockView(getActivity(),false,txtQS908C1, txtQS908C2, txtQS908C3 ); 
    		//q19.setVisibility(View.VISIBLE);
    		//q20.setVisibility(View.VISIBLE);
    		
    	}
    }
    
    public void onP908eChangeValue() {
    	if (MyUtil.incluyeRango(1,1,rgQS908E.getTagSelected("").toString())) {   
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),txtQS908E_O );  		
    	} 
    	else {
    		Util.lockView(getActivity(),false,txtQS908E_O); 
    	}
    	
    	if (MyUtil.incluyeRango(2,2,rgQS908E.getTagSelected("").toString())) {   
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),txtQS908E1 );  		
    	} 
    	else {
    		Util.lockView(getActivity(),false,txtQS908E1); 
    	}
    	
    	if (MyUtil.incluyeRango(3,3,rgQS908E.getTagSelected("").toString())) {   
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),txtQS908E1 ); 
    		Util.cleanAndLockView(getActivity(),txtQS908E_O ); 
    	} 
    }
    
    
    
    public void onP908b_aChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQS908B_A.getTagSelected("").toString())) {
    		
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQS908B_B,rgQS908C, txtQS908C1, txtQS908C2, txtQS908C3, rgQS908D, txtQS908D1, rgQS908E, txtQS908E1, txtQS908E_O );
    		q18.setVisibility(View.GONE);
    		q19.setVisibility(View.GONE);
    		q20.setVisibility(View.GONE);
    		q21.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQS908B_B,rgQS908C, txtQS908C1, txtQS908C2, txtQS908C3 ,rgQS908D, txtQS908D1, rgQS908E, txtQS908E1, txtQS908E_O ); 
    		q18.setVisibility(View.VISIBLE);
    		q19.setVisibility(View.VISIBLE);
    		q20.setVisibility(View.VISIBLE);
    		q21.setVisibility(View.VISIBLE);
    	}
    }
    
    public void onP900ChangeValue() {
    	peso =txtQS900.getText().toString();
    	talla =(txtQS901.getText().toString());
    

    	if(!peso.equals("")){
    		if (!talla.equals("")) {
    			rgQS902.lockButtons(false,0);
    			rgQS902.lockButtons(true,1,2,3,4,5);
    			
    		}else{
    			rgQS902.lockButtons(false,4);
    			rgQS902.lockButtons(true,0,1,2,3,5);
    		}
    	}else{
    		onP901ChangeValue();
    	}
    	txtQS901.requestFocus();
    }
    
    public void onP901ChangeValue() {
    	peso =txtQS900.getText().toString();
    	talla =txtQS901.getText().toString();
 
    	if(!talla.equals("")){
    		if (!peso.equals("")) {
    			rgQS902.lockButtons(false,0);
    			rgQS902.lockButtons(true,1,2,3,4,5);
    		}else{
    			rgQS902.lockButtons(false,4);
    			rgQS902.lockButtons(true,0,1,2,3,5);
    		}
    	}else{
    		if (!peso.equals("")) {
    			rgQS902.lockButtons(false,4);
    			rgQS902.lockButtons(true,0,1,2,3,5);
    		}else{
    			rgQS902.lockButtons(false,1,2,5);
    			rgQS902.lockButtons(true,0,3,4);
    		}
    		
    	}
    	rgQS902.requestFocus();
    }
    
    public void onP902ChangeValue() {
    	if (MyUtil.incluyeRango(6,6, rgQS902.getTagSelected("").toString())) {
    		Util.lockView(getActivity(), false,txtQS902_O);
    	}
    	txtQS903.requestFocus();
    }
    
    public void onP907ChangeValue() {
    	perimetro =txtQS907.getText().toString();    	
    	if(!perimetro.equals("")){
    		rgQS908.lockButtons(false,0);
    		rgQS908.lockButtons(true,1,2,3,4);    	
    	}
    	else{
    		rgQS908.lockButtons(true,0);
    		rgQS908.lockButtons(false,1,2,3,4);  
    	}
    }
    
    public void onP908ChangeValue() {
    	if (MyUtil.incluyeRango(6,6, rgQS908.getTagSelected("").toString())) {
    		Util.lockView(getActivity(), false,txtQS908_O);
    	}
    }
    
    
    
    public void onP906ChangeValue() {
    	if (MyUtil.incluyeRango(4,4, rgQS906.getTagSelected("").toString())) {
    		Util.lockView(getActivity(), false,txtQS906_O);
    	}
    	if (MyUtil.incluyeRango(2,4, rgQS906.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),txtQS904H,txtQS904M);
    	}else{
    		Util.lockView(getActivity(), false,txtQS904H,txtQS904M);
    	}
    }
    
    public void onP903ChangeValue() {
    	onP905_dChangeValue();
    	txtQS903_1.requestFocus();
    }
    
    public void onP903dChangeValue() {
    	onP905_dChangeValue();
    	txtQS904H.requestFocus();
    	Util.lockView(getActivity(), false,txtQS904H,txtQS904M);
    }
    
    public void onP904hChangeValue() {
        if(!txtQS904H.getText().toString().equals("")){
        	 hora=Integer.parseInt(txtQS904H.getText().toString());
        }
    	if(hora<10){
        	txtQS904H.setText("0"+hora);
        }
    	txtQS904M.requestFocus();
    }
    
    public void onP904mChangeValue() {    
        if(!txtQS904M.getText().toString().equals("")){
        	minutos=Integer.parseInt(txtQS904M.getText().toString());
        }        
    	if(minutos<10){
        	txtQS904M.setText("0"+minutos);
        }
    	txtQS905.requestFocus();
    }
    
    public void onP905ChangeValue() {
    	onP905_dChangeValue();
    	txtQS905_1.requestFocus();
    }
    
    public void onP905_dChangeValue() {
    	sistolica1 =txtQS903.getText().toString();
    	diastolica1 =txtQS903_1.getText().toString();
    	sistolica2 =txtQS905.getText().toString();
    	diastolica2 =txtQS905_1.getText().toString();
    	if(!sistolica1.equals("") && !sistolica2.equals("") && !diastolica1.equals("") && !diastolica2.equals("")){
    		rgQS906.lockButtons(false,0);
			rgQS906.lockButtons(true,1,2,3);
    	}else{
    		if(sistolica1.equals("") && sistolica2.equals("") && diastolica1.equals("") && diastolica2.equals("")){
    			rgQS906.lockButtons(false,1,2,3);
    			rgQS906.lockButtons(true,0);
    		}
    	}
    	rgQS906.requestFocus();
    }
    
    
    public CuestionarioService getCuestionarioService() { 
 		if(cuestionarioService==null){ 
 			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
 		} 
 		return cuestionarioService; 
     } 
    
     private void validarMensaje(String msj) {
         ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
  	}
     
     
    private Seccion01Service getServiceSeccion01() {
    	if (seccion01 == null) {
    		seccion01 = Seccion01Service.getInstance(getActivity());
    	}
        return seccion01;
    }
    
       public Seccion04_05Service getSeccion04_05Service() { 
   		if(seccion04_05Service==null){ 
   			seccion04_05Service = Seccion04_05Service.getInstance(getActivity()); 
   		} 
   		return seccion04_05Service; 
       }
       public Seccion01Service getPersonaService()
       {
       	if(Personaservice == null)
       	{
       		Personaservice = Seccion01Service.getInstance(getActivity());
       	}
       	return Personaservice;
       }
       public VisitaService getVisitaService() {
   		if (visitaService == null) {
   			visitaService = VisitaService.getInstance(getActivity());
   		}
   		return visitaService;
   	}
       
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub		
		uiToEntity(cap08_09); 						
		try { 			
			if (cap08_09.qs906 != null) {
				cap08_09.qs906 = cap08_09.getConvert906(cap08_09.qs906);
			}
			if (cap08_09.qs908 != null) {
				cap08_09.qs908 = cap08_09.getConvert906(cap08_09.qs908);
			}
			cap08_09.qs900a =lblpre900_pt_ini_f.getText().toString();
			cap08_09.qs902a =lblpre900_pt_fin_f.getText().toString();
			cap08_09.qs903a =lblpre900_par_ini_f.getText().toString();
			cap08_09.qs906a =lblpre900_par_fin_f.getText().toString();
			cap08_09.qs907a =lblpre900_pab_ini_f.getText().toString();
			cap08_09.qs908a =lblpre900_pab_fin_f.getText().toString();				
			if(cap08_09.qs900_ref_s9==null ){
				cap08_09.qs900_ref_s9=Util.getFechaActualToString();
			}
			
			if(!getCuestionarioService().saveOrUpdate(cap08_09,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		return App.SALUD; 
	}
} 
