package gob.inei.endes2024.fragment.C1seccion08_09.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.C1seccion08_09.CS_08Fragment_013;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;

import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion08_P40Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS840A; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQS840PPM;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS840B;
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQS840_O; 
	private static CS_08Fragment_013 caller;
//	public TableComponent tcListadoNinios;
	
	List<CSSECCION_08> detallesq;
	private int iniPosition;
	CSSECCION_08 salud; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblcepillo,lblfluor,lblpregunta840_ind,lblmarca,lblvalor,lblotro,lbltextovalor;
	public ButtonComponent btnAceptar;
	public ButtonComponent btnCancelar,btnGrabadoParcial;
	public String nombres;
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3; 
	LinearLayout q4;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta840;
	public GridComponent2 grid;
	
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
   }

	  public static Seccion08_P40Dialog newInstance(FragmentForm pagina, CSSECCION_08 detalle, int position, List<CSSECCION_08> detalles) {
          caller = (CS_08Fragment_013) pagina;
          Seccion08_P40Dialog f = new Seccion08_P40Dialog();
          f.setParent(pagina);
          Bundle args = new Bundle();
          args.putSerializable("detalle", (CSSECCION_08) detalles.get(position));
          f.detallesq = detalles;
          f.iniPosition = position;
          f.setArguments(args);
          return f;
}
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
		salud = (CSSECCION_08) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
	  	getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		cargarDatos();
		return rootView; 
  } 
  
  public Seccion08_P40Dialog() {
	  super();
	  seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS840A","QS840B","QS840_O","QS840PPM","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS806","QS809")}; 
	  seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS840A","QS840B","QS840_O","QS840PPM")}; 
  }
  
  @Override 
  protected void buildFields() { 
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_1_11_CEPILLO).negrita().centrar().colorFondo(R.color.griscabece);
	  rgQS840A=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs840a_1,R.string.cap08_09qs840a_2,R.string.cap08_09qs840a_3,R.string.cap08_09qs840a_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	  lblcepillo = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs840_A).textSize(18).negrita();
	  rgQS840A.agregarTitle(0,lblcepillo);
	  
	  rgQS840B=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs840b_1,R.string.cap08_09qs840b_2,R.string.cap08_09qs840b_3,R.string.cap08_09qs840b_4,R.string.cap08_09qs840b_5,R.string.cap08_09qs840b_6,R.string.cap08_09qs840b_7,R.string.cap08_09qs840b_8,R.string.cap08_09qs840b_9).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS840bChangeValue");
//	  rgQS840B=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs840b_1,R.string.cap08_09qs840b_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS840bChangeValue");
	  txtQS840_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico().centrar();
	  txtQS840PPM=new IntegerField(getActivity()).maxLength(4).size(altoComponente, 80).centrar();

	  lblpregunta840= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs840);
	  lblpregunta840_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs840_ind).negrita();
	  lblfluor = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs840_B).textSize(18).negrita();	  
//	  rgQS840B.agregarTitle(0,lblfluor);
	  
	  lblmarca = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs840_B_1).textSize(17).centrar();	
	  lblvalor = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs840_B_2).textSize(17).centrar();	  
	  lblotro = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs840_B_3).textSize(17).centrar();
	  lbltextovalor = new LabelComponent(this.getActivity()).size(20,400).text(R.string.cap08_09qs840_B_4).textSize(17).centrar();
	  grid= new GridComponent2(this.getActivity(),App.ESTILO, 2,0);
	  grid.addComponent(lbltextovalor);
	  grid.addComponent(txtQS840PPM);
	  grid.addComponent(rgQS840B,2);
	  
	  grid.addComponent(txtQS840_O,2);
	  grid.addComponent(lblmarca,2);
	  grid.addComponent(lblvalor,2);
	  grid.addComponent(lblotro,2);
	  btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
	  btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
	  btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 60);
	  btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				caller.contador=0;
				Seccion08_P40Dialog.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = false;
				try {
					flag = grabar();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (!flag) {
					return;
				}
				caller.contador=0;
				caller.refrescarPersonas(salud);
				Seccion08_P40Dialog.this.dismiss();
			}
		});	
		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				action=PROCCES.GRABADOPARCIAL;
				boolean flag=grabadoParcial();
				if(!flag){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), Seccion08_P40Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo); 
		q1 = createQuestionSection(lblSubtitulo);
//		q4 = createQuestionSection(txtQS840_O,lblmarca,lblvalor,lblotro);
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblfluor);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta840,lblpregunta840_ind,rgQS840A,lblfluor,grid.component()); 
		q3 = createButtonSection(btnAceptar,btnGrabadoParcial,btnCancelar);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);	 
    return contenedor; 
    } 
    

    public boolean grabar() { 
    	uiToEntity(salud); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		boolean flag=false;
		try { 
			
			flag= getCuestionarioService().saveOrUpdate(salud,seccionesGrabado); 
			if(!flag)
					{ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);} 
			
			
		} catch (Exception e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return flag; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
			if (Util.esMayor(salud.qs802d,0)) {
				if (!Util.esDiferente(salud.qs809,1)) {
					if (Util.esVacio(salud.qs840a)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS840A"); 
						view = rgQS840A; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs840b)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS840B"); 
						view = rgQS840B; 
						error = true; 
						return false; 
					} 
					if(!Util.esDiferente(salud.qs840b, 1,2,3)){
						if (Util.esVacio(salud.qs840ppm)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS840PPM"); 
							view = txtQS840PPM; 
							error = true; 
							return false; 
						} 	
					}
					
					if(!Util.esDiferente(salud.qs840b,1) && !Util.between(salud.qs840ppm, 1000,5000)){
						mensaje = "Valor ppm no corresponde verifique."; 
						view = txtQS840PPM; 
						error = true; 
						return false; 
					}
					if(!Util.esDiferente(salud.qs840b,2) && !Util.between(salud.qs840ppm, 601,999)){
						mensaje = "Valor ppm no corresponde verifique.."; 
						view = txtQS840PPM; 
						error = true; 
						return false; 
					}
					if(!Util.esDiferente(salud.qs840b,3) && !Util.between(salud.qs840ppm, 50,600)){
						mensaje = "Valor ppm no corresponde verifique..."; 
						view = txtQS840PPM; 
						error = true; 
						return false; 
					}
							
					
					if (!Util.esDiferente(salud.qs840b,7,8,9)) {
						if (Util.esVacio(salud.qs840_o)) { 
							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
							view = txtQS840_O; 
							error = true; 
							return false; 
						}
					}
				}		
				
			}
		
		return true; 
    } 
   
    public void cargarDatos() { 
    	if(salud!=null){
    		nombres=salud.qh02_1;
    		salud = getCuestionarioService().getCAP08_09(salud.id, salud.hogar_id ,salud.persona_id,salud.persona_id_ninio,seccionesCargado);
    	}
        	if(salud==null){ 
        		salud=new CSSECCION_08(); 
        		salud.id=App.getInstance().getPersonabySeccion8Salud().id; 
        		salud.hogar_id=App.getInstance().getPersonabySeccion8Salud().hogar_id; 
        		salud.persona_id=App.getInstance().getPersonabySeccion8Salud().persona_id; 
        		salud.persona_id_ninio = App.getInstance().getPersonabySeccion8Salud().persona_id_ninio;
        	} 
        	entityToUI(salud); 
        	inicio();  
    } 

 	
    private void inicio() { 
    	onqrgQS840bChangeValue();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	rgQS840A.requestFocus();
    } 
    public boolean grabadoParcial(){
		uiToEntity(salud);
		boolean flag = true;		
		try {
			flag = getCuestionarioService().saveOrUpdate(salud,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de P.840");
			}
		}  catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS840A.readOnly();
			rgQS840B.readOnly();
			txtQS840PPM.readOnly();
		}
	}

    public void RenombrarEtiquetas(){
    	lblpregunta840.setText(getResources().getString(R.string.cap08_09qs840));
    	String replace="(NOMBRE)";
    	String nombre= nombres;
    	lblpregunta840.setText(lblpregunta840.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
   
	public void onqrgQS840bChangeValue() {
		if (MyUtil.incluyeRango(4,9,rgQS840B.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(), txtQS840PPM,lbltextovalor);
			lbltextovalor.setVisibility(View.GONE);
			txtQS840PPM.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,lbltextovalor,txtQS840PPM);
			lbltextovalor.setVisibility(View.VISIBLE);
			txtQS840PPM.setVisibility(View.VISIBLE);
		}
		if (MyUtil.incluyeRango(7,9,rgQS840B.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(7,7,rgQS840B.getTagSelected("").toString())) {			
				Util.lockView(getActivity(), false,txtQS840_O);
				Util.lockView(getActivity(), false,lblmarca);
				txtQS840_O.setVisibility(View.VISIBLE);
				lblmarca.setVisibility(View.VISIBLE);
				lblvalor.setVisibility(View.GONE);
				lblotro.setVisibility(View.GONE);
				txtQS840_O.requestFocus();
			} 
			if (MyUtil.incluyeRango(8,8,rgQS840B.getTagSelected("").toString())) {			
				Util.lockView(getActivity(), false,txtQS840_O);		
				Util.lockView(getActivity(), false,lblvalor);
				txtQS840_O.setVisibility(View.VISIBLE);
				lblvalor.setVisibility(View.VISIBLE);
				lblmarca.setVisibility(View.GONE);
				lblotro.setVisibility(View.GONE);
				 txtQS840_O.requestFocus();
			} 
			if (MyUtil.incluyeRango(9,9,rgQS840B.getTagSelected("").toString())) {				
				Util.lockView(getActivity(), false,txtQS840_O);
				Util.lockView(getActivity(), false,lblotro);
				txtQS840_O.setVisibility(View.VISIBLE);
				lblotro.setVisibility(View.VISIBLE);
				lblmarca.setVisibility(View.GONE);
				lblvalor.setVisibility(View.GONE);
				txtQS840_O.requestFocus();
			} 
		}
		else {				
			Util.cleanAndLockView(getActivity(),txtQS840_O);
			Util.cleanAndLockView(getActivity(),lblmarca,lblvalor,lblotro);
			txtQS840_O.setVisibility(View.GONE);
			lblmarca.setVisibility(View.GONE);
			lblvalor.setVisibility(View.GONE);
			lblotro.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		Seccion08_P40Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.CSVISITA);
	}

}
