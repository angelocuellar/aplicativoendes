package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_011 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS834; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS835; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS836; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS837; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblSubtitulo1; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	String fechareferencia834;
	String fechareferencia835;
	public LabelComponent lblpregunta834,lblpregunta835,lblpregunta836,lblpregunta837;
	public CS_08Fragment_011() {} 
	public CS_08Fragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS834","QS802CONS","QS835","QS836","QS837","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS833","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS834","QS835","QS836","QS837")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
	  	lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_6_11_MENTAL).negrita().centrar().colorFondo(R.color.griscabece);
	  	lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_6_11_MENTAL_indic).negrita();
	   	lblpregunta834 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs834).textSize(19);
	  	lblpregunta835 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs835).textSize(19);
	  	lblpregunta836 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs836).textSize(19);
	  	lblpregunta837 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs837).textSize(19);		 
		rgQS834=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs834_1,R.string.cap08_09qs834_2,R.string.cap08_09qs834_3,R.string.cap08_09qs834_4,R.string.cap08_09qs834_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQS835=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs835_1,R.string.cap08_09qs835_2,R.string.cap08_09qs835_3,R.string.cap08_09qs835_4,R.string.cap08_09qs835_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQS836=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs836_1,R.string.cap08_09qs836_2,R.string.cap08_09qs836_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS837=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs837_1,R.string.cap08_09qs837_2,R.string.cap08_09qs837_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblSubtitulo1); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta834,rgQS834); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta835,rgQS835); 
		q5 = createQuestionSection(lblpregunta836,rgQS836); 
		q6 = createQuestionSection(lblpregunta837,rgQS837); 
 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6);		 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		
		if (salud.qs834!=null) {
			salud.qs834=salud.getConvertqs834(salud.qs834);
		}
		if (salud.qs835!=null) {
			salud.qs835=salud.getConvertqs834(salud.qs835);
		}
		if (salud.qs836!=null) {
			salud.qs836=salud.getConvertqs804(salud.qs836);
		}
		if (salud.qs837!=null) {
			salud.qs837=salud.getConvertqs804(salud.qs837);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs834 == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia834 == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia834;
				}			
			}		
			

			if(salud.qs835 == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia835 == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia835;
				}			
			}		
			
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
	
	
			if (Util.esMayor(salud.qs802d,5)) {
				if (!Util.esDiferente(salud.qs833,1)) {
					if (Util.esVacio(salud.qs834)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS834"); 
						view = rgQS834; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs835)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS835"); 
						view = rgQS835; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs836)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS836"); 
						view = rgQS836; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs837)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS837"); 
						view = rgQS837; 
						error = true; 
						return false; 
					} 
				}
			}
		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
		
		if (salud.qs834!=null) {
			salud.qs834=salud.setConvertqs834(salud.qs834);
		}
		if (salud.qs835!=null) {
			salud.qs835=salud.setConvertqs834(salud.qs835);
		}
		if (salud.qs836!=null) {
			salud.qs836=salud.setConvertqs804(salud.qs836);
		}
		if (salud.qs837!=null) {
			salud.qs837=salud.setConvertqs804(salud.qs837);
		}
		
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	fechareferencia834 = salud.qs802cons;
    	fechareferencia835 = salud.qs802cons;
    	inicio();   
    } 
    
    public void cargarpaseedad(Integer edad) {   
    	//if (Util.esDiferente(salud.qs806,1) || Util.esMenor(edad,6) || Util.esDiferente(salud.qs833,1)) {
    	if (Util.esMenor(edad,6) || Util.esDiferente(salud.qs833,1)) {  
    		Util.cleanAndLockView(getActivity(),rgQS834,rgQS835,rgQS836,rgQS837);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQS834,rgQS835,rgQS836,rgQS837);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);    		
    		RenombrarEtiquetas();
        	ValidarsiesSupervisora();
        	rgQS834.requestFocus();
    	} 					 
    }
    
    private void inicio() {    
    	cargarpaseedad(salud.qs802d);  
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS834.readOnly();
			rgQS835.readOnly();
			rgQS836.readOnly();
			rgQS837.readOnly();			
		}
		
	}
	
    public void RenombrarEtiquetas(){
    	String replace="(NOMBRE)";
		String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	Calendar fechaactual834 = new GregorianCalendar();
    	if(fechareferencia834!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia835);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual834=cal;
		}
    	
    	Calendar fechaactual835 = new GregorianCalendar();
    	if(fechareferencia835!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia835);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual835=cal;
		}    	
   		Calendar fechahacetreintadias834;
   		Calendar fechahacetreintadias835;
   		fechahacetreintadias834 = MyUtil.PrimeraFecha(fechaactual834, 30);
   		fechahacetreintadias835 = MyUtil.PrimeraFecha(fechaactual835, 30);
   		
   		lblpregunta834.setText(lblpregunta834.getText().toString().replace("F1", fechahacetreintadias834.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias834.get(Calendar.MONTH))));
   		lblpregunta835.setText(lblpregunta835.getText().toString().replace("F1", fechahacetreintadias835.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias835.get(Calendar.MONTH))));
   		lblpregunta834.setText(lblpregunta834.getText().toString().replace(replace,nombre));
   		lblpregunta835.setText(lblpregunta835.getText().toString().replace(replace,nombre));
   		lblpregunta836.setText(lblpregunta836.getText().toString().replace(replace,nombre));
   		lblpregunta837.setText(lblpregunta837.getText().toString().replace(replace,nombre));
   		
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs834!=null) {
			salud.qs834=salud.getConvertqs834(salud.qs834);
		}
		if (salud.qs835!=null) {
			salud.qs835=salud.getConvertqs834(salud.qs835);
		}
		if (salud.qs836!=null) {
			salud.qs836=salud.getConvertqs804(salud.qs836);
		}
		if (salud.qs837!=null) {
			salud.qs837=salud.getConvertqs804(salud.qs837);
		}
		
		if(salud.qs834 == null) {
			salud.qs802cons = null;
		} 
		else {
			if(fechareferencia834 == null) {
				salud.qs802cons =  Util.getFechaActualToString();
			}
			else {
				salud.qs802cons = fechareferencia834;
			}			
		}
		
		if(salud.qs835 == null) {
			salud.qs802cons = null;
		} 
		else {
			if(fechareferencia835 == null) {
				salud.qs802cons =  Util.getFechaActualToString();
			}
			else {
				salud.qs802cons = fechareferencia835;
			}			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
