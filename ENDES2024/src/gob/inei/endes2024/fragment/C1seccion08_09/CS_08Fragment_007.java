package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_007 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS820; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQS821A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQS821B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS821C; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS821D; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQS821X; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQS821_O; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS821Y; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS822; 
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQS822A; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblvacio,lblpregunta821_ind,lblpregunta822_ind; 
	public GridComponent2 gridPreguntas822;
	public IntegerField txtQS822A1,txtQS822A2;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta820,lblpregunta821,lblpregunta822;
	public CS_08Fragment_007() {} 
	public CS_08Fragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS820","QS821A","QS821B","QS821C","QS821D","QS821X","QS821_O","QS821Y","QS822","QS822A","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS817","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS820","QS821A","QS821B","QS821C","QS821D","QS821X","QS821_O","QS821Y","QS822","QS822A")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_3_11_OCULAR).negrita().centrar().colorFondo(R.color.griscabece);
	  rgQS820=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs820_1,R.string.cap08_09qs820_2,R.string.cap08_09qs820_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS820ChangeValue"); 
	  chbQS821A=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821aChangeValue"); 
	  chbQS821B=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821bChangeValue"); 
	  chbQS821C=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821cChangeValue"); 
	  chbQS821D=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821dChangeValue"); 
	  chbQS821X=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821xChangeValue"); 
	  txtQS821_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	  chbQS821Y=new CheckBoxField(this.getActivity(), R.string.cap08_09qs821y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS821yChangeValue"); 
	 
	  rgQS822=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs822_1,R.string.cap08_09qs822_2,R.string.cap08_09qs822_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS822ChangeValue"); 
	  txtQS822A1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
	  txtQS822A2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
	  lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
		
	  lblpregunta820 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs820);
	  lblpregunta821 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs821);
	  lblpregunta821_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs821_ind).negrita();
	  lblpregunta822 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs822);
	  lblpregunta822_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs822_ind).negrita();
	 
	  gridPreguntas822 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
	  gridPreguntas822.addComponent(rgQS822,1,3);		
	  gridPreguntas822.addComponent(txtQS822A1);
	  gridPreguntas822.addComponent(txtQS822A2);
	  gridPreguntas822.addComponent(lblvacio);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblpregunta820,rgQS820); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta821,lblpregunta821_ind,chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X,txtQS821_O,chbQS821Y); 
		q4 = createQuestionSection(lblpregunta822,lblpregunta822_ind,gridPreguntas822.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
    return contenedor; 
    }     
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		if (salud.qs820!=null) {
			salud.qs820=salud.getConvertqs804(salud.qs820);
		}
		if (salud.qs822!=null) {
			salud.qs822=salud.getConvertqs804(salud.qs822);
		}
		
		if (!Util.esDiferente(salud.qs822,1) && txtQS822A1.getText().toString().trim().length()!=0 ) {
			salud.qs822a=Integer.parseInt(txtQS822A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs822,2) && txtQS822A2.getText().toString().trim().length()!=0) {
			salud.qs822a=Integer.parseInt(txtQS822A2.getText().toString().trim());
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().getPersonabySeccion8Salud().qs820=salud.qs820;
		return true; 
    } 
    
    private boolean validar() { 
    	if(!isInRange()) return false; 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
    	
    
    		if (Util.esMayor(salud.qs802d,2)) {
        		if (!Util.esDiferente(salud.qs817,1)) {
        			if (Util.esVacio(salud.qs820)) { 
            			mensaje = preguntaVacia.replace("$", "La pregunta QS820"); 
            			view = rgQS820; 
            			error = true; 
            			return false; 
            		}
            		if(!Util.esDiferente(salud.qs820,1)){
            			if (!Util.alMenosUnoEsDiferenteA(0,salud.qs821a,salud.qs821b,salud.qs821c,salud.qs821d,salud.qs821x,salud.qs821y)) { 
            				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
            				view = chbQS821A; 
            				error = true; 
            				return false; 
            			} 
            			if (chbQS821X.isChecked()) {
            				if (Util.esVacio(salud.qs821_o)) { 
            					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
            					view = txtQS821_O; 
            					error = true; 
            					return false; 
            				}
            			}
            			if (Util.esVacio(salud.qs822)) { 
            				mensaje = preguntaVacia.replace("$", "La pregunta QS822"); 
            				view = rgQS822; 
            				error = true; 
            				return false; 
            			} 
            			if(!Util.esDiferente(salud.qs822,1)){
            				if (Util.esVacio(salud.qs822a)) { 
            					mensaje = preguntaVacia.replace("$", "La pregunta QS822A1"); 
            					view = txtQS822A1; 
            					error = true; 
            					return false; 
            				}
            				if(Util.esMayor(salud.qs822a, 23)){
            					mensaje = "Opci�n meses no debe ser mayor a 23"; 
            					view = txtQS822A1; 
            					error = true; 
            					return false; 
            				}
            			}
            			if(!Util.esDiferente(salud.qs822,2)){
            				if (Util.esVacio(salud.qs822a)) { 
            					mensaje = preguntaVacia.replace("$", "La pregunta QS822A2"); 
            					view = txtQS822A2; 
            					error = true; 
            					return false; 
            				}
            				if(Util.esMenor(salud.qs822a,2) || Util.esMenor(salud.qs802d, salud.qs822a)){
            					mensaje = "A�os que le indicaron el uso de lentes no puede ser mayor a la edad"; 
            					view = txtQS822A2; 
            					error = true; 
            					return false; 
            				}
            			}
            		}
        		}
        	}
    	  		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	if (salud.qs820!=null) {
			salud.qs820=salud.setConvertqs804(salud.qs820);
		}
		if (salud.qs822!=null) {
			salud.qs822=salud.setConvertqs804(salud.qs822);
		}
		
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	
    	entityToUI(salud); 
    	
   	  if (salud.qs822a!=null) {	
  		  if (!Util.esDiferente(salud.qs822,1)) {
  			txtQS822A1.setText(salud.qs822a.toString());
  		  }
  		  if (!Util.esDiferente(salud.qs822,2)) {
  			txtQS822A2.setText(salud.qs822a.toString());
  		  }
  	  } 
    	
    	inicio();   
    } 
    
    public void cargarpaseedad(Integer edad) {
    	if (Util.esMenor(edad,3) || Util.esDiferente(salud.qs817,1)) {    	
    		Util.cleanAndLockView(getActivity(), rgQS820,chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X,chbQS821Y,txtQS821_O,rgQS822,txtQS822A1,txtQS822A2);
    		salud.qs822a=null;
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
		}
    	else{   				
    		Util.lockView(getActivity(), false,rgQS820,chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X,chbQS821Y,txtQS821_O,rgQS822);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		onqrgQS820ChangeValue();
    	}		
    }
    
    private void inicio() {    	
    	cargarpaseedad(salud.qs802d);
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS820.requestFocus();
    	txtCabecera.requestFocus();
    }
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS820.readOnly();
			chbQS821A.readOnly();
			chbQS821B.readOnly();
			chbQS821C.readOnly();
			chbQS821D.readOnly();
			chbQS821X.readOnly();
			chbQS821Y.readOnly();
			txtQS821_O.readOnly();
			rgQS822.readOnly();
//			txtQS822A.readOnly();
			txtQS822A1.readOnly();
			txtQS822A2.readOnly();
			
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	String replace="(NOMBRE)";
    	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	lblpregunta820.setText(lblpregunta820.getText().toString().replace(replace,nombre));
    	lblpregunta821.setText(lblpregunta821.getText().toString().replace(replace,nombre));
    	lblpregunta822.setText(lblpregunta822.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public boolean verificarCheck() {
  		if (chbQS821A.isChecked() || chbQS821B.isChecked() || chbQS821C.isChecked() || chbQS821D.isChecked() || chbQS821X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	public void onqrgQS820ChangeValue() {
  		if (MyUtil.incluyeRango(2,8,rgQS820.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X,chbQS821Y,txtQS821_O,rgQS822,txtQS822A1,txtQS822A2);
  			salud.qs822a=null;
  			q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X,chbQS821Y,txtQS821_O,rgQS822);
  			q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
			onqrgQS821aChangeValue();
  			onqrgQS821bChangeValue();
  			onqrgQS821cChangeValue(); 
  			onqrgQS821dChangeValue();   	
  			onqrgQS821xChangeValue(); 
  			if (!verificarCheck()) {
  				onqrgQS821yChangeValue();
			}
  			onqrgQS822ChangeValue();
  			chbQS821A.requestFocus();
  		}	
  	}

	public void onqrgQS821aChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS821Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS821Y);  				
    	}	
    }
	public void onqrgQS821bChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS821Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS821Y);  				
    	}	
    }
	public void onqrgQS821cChangeValue() {  	
    	if (verificarCheck()) {   
    		Util.cleanAndLockView(getActivity(),chbQS821Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS821Y);  				
    	}	
    }
	public void onqrgQS821dChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS821Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS821Y);  				
    	}	
    }
    public void onqrgQS821xChangeValue() {  	
    	if (chbQS821X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS821_O);
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS821Y);
  			}
  			txtQS821_O.requestFocus();
  		} 
  		else {  	
  			Util.cleanAndLockView(getActivity(),txtQS821_O);  
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS821Y);  				
			}
  		}	
    }
    public void onqrgQS821yChangeValue() {    	
  		if (chbQS821Y.isChecked()){	  			
  			Util.cleanAndLockView(getActivity(),chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X);  
  			rgQS822.requestFocus();
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS821A,chbQS821B,chbQS821C,chbQS821D,chbQS821X);
  			chbQS821A.requestFocus();
  		}	
  	}
	public void onqrgQS822ChangeValue() {
    	if (!MyUtil.incluyeRango(2,8,rgQS820.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(1,1,rgQS822.getTagSelected("").toString())) {    		
    			Util.cleanAndLockView(getActivity(),txtQS822A2);
				salud.qs822a=null;
				Util.lockView(getActivity(),false,txtQS822A1);				
				txtQS822A1.requestFocus();				
			} 
			if (MyUtil.incluyeRango(2,2,rgQS822.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQS822A1);	
				salud.qs822a=null;
				Util.lockView(getActivity(),false,txtQS822A2);			
				txtQS822A2.requestFocus();				
			}	
			if (MyUtil.incluyeRango(3,3,rgQS822.getTagSelected("").toString())) {
				salud.qs822a=null;			
				Util.cleanAndLockView(getActivity(),txtQS822A1,txtQS822A2);								
			}		
    	}  		
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		if (salud.qs820!=null) {
			salud.qs820=salud.getConvertqs804(salud.qs820);
		}
		if (salud.qs822!=null) {
			salud.qs822=salud.getConvertqs804(salud.qs822);
		}		
		if (!Util.esDiferente(salud.qs822,1) && txtQS822A1.getText().toString().trim().length()!=0 ) {
			salud.qs822a=Integer.parseInt(txtQS822A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs822,2) && txtQS822A2.getText().toString().trim().length()!=0) {
			salud.qs822a=Integer.parseInt(txtQS822A2.getText().toString().trim());
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
