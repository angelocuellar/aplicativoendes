package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_08Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS806; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQS807_A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQS807_B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS807_C; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS807_D; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQS807_E; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQS807_F; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS807_G; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQS807_H; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQS807_I; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQS807_J; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQS807_X; 
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQS807_O; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQS807_Y; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblpregunta806_ind,lblpregunta807; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoSeccion01;
	public LabelComponent lblpregunta806;
	public List<Seccion01> detalles;
	
	String fechareferencia;
	
	public CS_08Fragment_004() {} 
	public CS_08Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS806","QS802CONS","QS807_A","QS807_B","QS807_C","QS807_D","QS807_E","QS807_F","QS807_G","QS807_H","QS807_I","QS807_J","QS807_X","QS807_O","QS807_Y","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS806","QS807_A","QS807_B","QS807_C","QS807_D","QS807_E","QS807_F","QS807_G","QS807_H","QS807_I","QS807_J","QS807_X","QS807_O","QS807_Y")};
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","ID","HOGAR_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_0_11_BUCAL).negrita().centrar().colorFondo(R.color.griscabece);
	 
	  lblpregunta806 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs806).textSize(19);
	  lblpregunta806_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs806_ind).negrita();	
	  rgQS806=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs806_1,R.string.cap08_09qs806_2,R.string.cap08_09qs806_3,R.string.cap08_09qs806_4).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS806ChangeValue");
	 
	  lblpregunta807 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09qs807).textSize(19);
	  chbQS807_A=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807aChangeValue"); 
	  chbQS807_B=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807bChangeValue"); 
	  chbQS807_C=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807cChangeValue"); 
	  chbQS807_D=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807dChangeValue"); 
	  chbQS807_E=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807eChangeValue"); 
	  chbQS807_F=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807fChangeValue"); 
	  chbQS807_G=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807gChangeValue"); 
	  chbQS807_H=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807hChangeValue"); 
	  chbQS807_I=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807iChangeValue"); 
	  chbQS807_J=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807jChangeValue"); 
	  chbQS807_X=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807xChangeValue"); 
	  txtQS807_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
	  chbQS807_Y=new CheckBoxField(this.getActivity(), R.string.cap08_09qs807_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS807yChangeValue"); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta806,lblpregunta806_ind,rgQS806);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta807,chbQS807_A,chbQS807_B,chbQS807_C,chbQS807_D,chbQS807_E,chbQS807_F,chbQS807_G,chbQS807_H,chbQS807_I,chbQS807_J,chbQS807_X,txtQS807_O,chbQS807_Y); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);  
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 	
		uiToEntity(salud); 
		
		if (salud!=null && salud.qs806!=null) {
			salud.qs806=salud.getConvertqs806(salud.qs806);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs806 == null) {
				salud.qs802cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs802cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs802cons = fechareferencia;
				}			
			}		
			
			
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().getPersonabySeccion8Salud().qs806=salud.qs806;
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(salud==null){
			return false;
		}
		if (Util.esVacio(salud.qs806)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QS806"); 
			view = rgQS806; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(salud.qs806,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,salud.qs807_a,salud.qs807_b,salud.qs807_c,salud.qs807_d,salud.qs807_e,salud.qs807_f,salud.qs807_g,salud.qs807_h,salud.qs807_i,salud.qs807_j,salud.qs807_x,salud.qs807_y)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQS807_A; 
				error = true; 
				return false; 
			}
			if (chbQS807_X.isChecked()) {
				if (Util.esVacio(salud.qs807_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQS807_O; 
					error = true; 
					return false; 
				}
			}		
		}	
		return true; 
    } 
    @Override 
    public void cargarDatos() {
//    	MyUtil.LiberarMemoria();
    	if (App.getInstance().getPersonabySeccion8Salud()!=null){  	
    		salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	}
    	if (salud!=null && salud.qs806!=null) {
			salud.qs806=salud.setConvertqs806(salud.qs806);
		}    	
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	fechareferencia = salud.qs802cons;
    	inicio();   
    } 
    private void inicio() { 
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;
		}    	
    	Calendar fechainicio =	MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	
    	lblpregunta806.setText(lblpregunta806.getText().toString().replace("F1", F2)); 
    	lblpregunta806.setText(lblpregunta806.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta806.setText(lblpregunta806.getText().toString().replace("F3", F3));
    	
    	if (!chbQS807_X.isChecked()) {    	
    		Util.cleanAndLockView(getActivity(),txtQS807_O);	
		}
    	onqrgQS806ChangeValue(); 
    	ValidarsiesSupervisora();
//    	rgQS806.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS806.readOnly();
			chbQS807_A.readOnly();
			chbQS807_B.readOnly();
			chbQS807_C.readOnly();
			chbQS807_D.readOnly();
			chbQS807_E.readOnly();
			chbQS807_F.readOnly();
			chbQS807_G.readOnly();
			chbQS807_H.readOnly();
			chbQS807_I.readOnly();
			chbQS807_J.readOnly();
			chbQS807_X.readOnly();
			chbQS807_Y.readOnly();
			txtQS807_O.readOnly();			
		}
	}
   
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public boolean verificarCheck() {
  		if (chbQS807_A.isChecked() || chbQS807_B.isChecked() || chbQS807_C.isChecked() || chbQS807_D.isChecked() || chbQS807_E.isChecked() || chbQS807_F.isChecked() || chbQS807_G.isChecked() || chbQS807_H.isChecked() || chbQS807_I.isChecked() || chbQS807_J.isChecked() ||chbQS807_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public void onqrgQS806ChangeValue() {
    	detalles = getCuestionarioService().getListadoSeccion8Salud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion01);
    	if(!Util.esDiferente(detalles.get(0).persona_id, salud.persona_id_ninio)){
    		rgQS806.lockButtons(false, 0,1,2);
    		rgQS806.lockButtons(true,3);
    	}
    		
  		if (MyUtil.incluyeRango(2,4,rgQS806.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),chbQS807_A,chbQS807_B,chbQS807_C,chbQS807_D,chbQS807_E,chbQS807_F,chbQS807_G,chbQS807_H,chbQS807_I,chbQS807_J,chbQS807_X,chbQS807_Y,txtQS807_O);
  			q3.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS807_A,chbQS807_B,chbQS807_C,chbQS807_D,chbQS807_E,chbQS807_F,chbQS807_G,chbQS807_H,chbQS807_I,chbQS807_J,chbQS807_X,chbQS807_Y,txtQS807_O);
  			q3.setVisibility(View.VISIBLE);
  			onqrgQS807aChangeValue();
  			onqrgQS807bChangeValue();
  			onqrgQS807cChangeValue(); 
  			onqrgQS807dChangeValue(); 
  			onqrgQS807eChangeValue(); 
  			onqrgQS807fChangeValue(); 
  			onqrgQS807gChangeValue(); 
  			onqrgQS807hChangeValue(); 
  			onqrgQS807iChangeValue(); 
  			onqrgQS807jChangeValue(); 
  			onqrgQS807xChangeValue(); 
  			if (!verificarCheck()) {
  				onqrgQS807yChangeValue();
			}
  			chbQS807_A.requestFocus();
  		}	
  	}
    public void onqrgQS807aChangeValue() {  	
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807bChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807cChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807dChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807eChangeValue() {  	
    	if (verificarCheck()) {    		
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807fChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807gChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807hChangeValue() {  	
    	if (verificarCheck()) {    		
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807iChangeValue() {  	
    	if (verificarCheck()) {    	
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807jChangeValue() {  	
    	if (verificarCheck()) {    
    		Util.cleanAndLockView(getActivity(),chbQS807_Y);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQS807_Y);  				
    	}	
    }
    public void onqrgQS807xChangeValue() {  	
    	if (chbQS807_X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS807_O);
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS807_Y);
  			}
  			txtQS807_O.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQS807_O);  
  			if (!verificarCheck()) {  			
  				Util.lockView(getActivity(),false,chbQS807_Y);  				
			}
  		}	
    }
    public void onqrgQS807yChangeValue() {    	
  		if (chbQS807_Y.isChecked()){  		
  			Util.cleanAndLockView(getActivity(),chbQS807_A,chbQS807_B,chbQS807_C,chbQS807_D,chbQS807_E,chbQS807_F,chbQS807_G,chbQS807_H,chbQS807_I,chbQS807_J,chbQS807_X);  	
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS807_A,chbQS807_B,chbQS807_C,chbQS807_D,chbQS807_E,chbQS807_F,chbQS807_G,chbQS807_H,chbQS807_I,chbQS807_J,chbQS807_X);  					
  		}	
  	}
	@Override
	public Integer grabadoParcial() {
	uiToEntity(salud); 
		
	if (salud!=null ) {
		if( salud.qs806!=null) {
			salud.qs806=salud.getConvertqs804(salud.qs806);
		}
		
		if(salud.qs806 == null) {
			salud.qs802cons = null;
		} 
		else {
			if(fechareferencia == null) {
				salud.qs802cons =  Util.getFechaActualToString();
			}
			else {
				salud.qs802cons = fechareferencia;
			}			
		}
	}
		try { 
			
			
			
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
