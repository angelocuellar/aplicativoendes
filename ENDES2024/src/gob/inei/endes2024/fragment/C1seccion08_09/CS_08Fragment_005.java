package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS809; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS810; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS811; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS812; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQS812A; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS813; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS814; 
	
	public LabelComponent lblpregunta809,lblpregunta810,lblpregunta811,lblpregunta812,lblpregunta813,lblpregunta814,lblvacio,lblvacio1,lblpregunta812_ind;
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	public GridComponent2 gridPreguntas812;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6;
	LinearLayout q7;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesGrabadoP809; 
 
	public CS_08Fragment_005() {} 
	public CS_08Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS809","QS810","QS811","QS812","QS812A","QS813","QS814","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS806")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS809","QS810","QS811","QS812","QS812A","QS813","QS814")}; 
		seccionesGrabadoP809 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS809","QS810","QS811","QS812","QS812A","QS813","QS814","QS840A","QS840B","QS840_O")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_1_11_BUCAL).negrita().centrar().colorFondo(R.color.griscabece);
	  rgQS809=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS809ChangeValue"); 
	  rgQS810=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs810_1,R.string.cap08_09qs810_2,R.string.cap08_09qs810_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS810ChangeValue"); 
	  rgQS811=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs811_1,R.string.cap08_09qs811_2,R.string.cap08_09qs811_3,R.string.cap08_09qs811_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
	  rgQS812=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs812_1,R.string.cap08_09qs812_2,R.string.cap08_09qs812_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS812ChangeValue"); 
	  txtQS812A=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
	  lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
	  lblvacio1 = new LabelComponent(this.getActivity()).size(75, 100);
	  
	  rgQS813=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs813_1, R.string.cap08_09qs813_2).size(50,650).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  rgQS814=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  
	  gridPreguntas812 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);	 
	  gridPreguntas812.addComponent(rgQS812,1,3);		
	  gridPreguntas812.addComponent(txtQS812A);
	  gridPreguntas812.addComponent(lblvacio);
	  gridPreguntas812.addComponent(lblvacio1);
	 
	  lblpregunta809= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs809);
	  lblpregunta810= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs810);
	  lblpregunta811= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs811);
	  lblpregunta812= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs812);
	  lblpregunta812_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs812_ind).negrita();	  
	  lblpregunta813= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs813);
	  lblpregunta814= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs814);
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblSubtitulo); 
		q2 = createQuestionSection(lblpregunta809,rgQS809); 
		q3 = createQuestionSection(lblpregunta810,rgQS810); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta811,rgQS811); 
		q5 = createQuestionSection(0,lblpregunta812,lblpregunta812_ind,gridPreguntas812.component()); 
		q6 = createQuestionSection(lblpregunta813,rgQS813); 
		q7 = createQuestionSection(lblpregunta814,rgQS814); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud);
		if (salud.qs810!=null) {
			salud.qs810=salud.getConvertqs804(salud.qs810);
		}
		if (salud.qs812!=null) {
			salud.qs812=salud.getConvertqs804(salud.qs812);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!Util.esDiferente(salud.qs809,2)){
				if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoP809)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				} 
			}else{
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().getPersonabySeccion8Salud().qs809=salud.qs809;
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);		
		
			if (Util.esMayor(salud.qs802d,0)) {
				if (Util.esVacio(salud.qs809)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QS809"); 
					view = rgQS809; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(salud.qs809,1)) {
					if (Util.esVacio(salud.qs810)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS810"); 
						view = rgQS810; 
						error = true; 
						return false; 
					} 
					if (!Util.esDiferente(salud.qs810,1)) {
						if (Util.esVacio(salud.qs811)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS811"); 
							view = rgQS811; 
							error = true; 
							return false; 
						} 		
					}	
					if (Util.esVacio(salud.qs812)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS812"); 
						view = rgQS812; 
						error = true; 
						return false; 
					} 
					if (!Util.esDiferente(salud.qs812,1)) {
						if (Util.esVacio(salud.qs812a)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS812A"); 
							view = txtQS812A; 
							error = true; 
							return false; 
						}
						if(Util.esMayor(salud.qs812a, 11)){
							mensaje = "Opci�n meses no debe ser mayor a 11"; 
							view = txtQS812A; 
							error = true; 
							return false; 
						}
					}
				 
					if (Util.esVacio(salud.qs813)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS813"); 
						view = rgQS813; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs814)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QS814"); 
						view = rgQS814; 
						error = true; 
						return false; 
					}
				}
			}
			 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	if (salud.qs810!=null) {
			salud.qs810=salud.setConvertqs804(salud.qs810);
		}
    	if (salud.qs812!=null) {
			salud.qs812=salud.setConvertqs804(salud.qs812);
		}
    	
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	inicio();  
    } 
    public void cargarpaseedad(Integer edad) {   
		if (!Util.esDiferente(edad,0)) {		
			Util.cleanAndLockView(getActivity(), rgQS809,rgQS810,rgQS811,rgQS812,txtQS812A,rgQS813,rgQS814);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
  			q3.setVisibility(View.GONE);
  			q4.setVisibility(View.GONE);
  			q5.setVisibility(View.GONE);
  			q6.setVisibility(View.GONE);
  			q7.setVisibility(View.GONE);
		}
		else{		
			Util.lockView(getActivity(), false,rgQS809,rgQS810,rgQS811,rgQS812,txtQS812A,rgQS813,rgQS814);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
  			q3.setVisibility(View.VISIBLE);
  			q4.setVisibility(View.VISIBLE);
  			q5.setVisibility(View.VISIBLE);
  			q6.setVisibility(View.VISIBLE);
  			q7.setVisibility(View.VISIBLE);
			onqrgQS809ChangeValue();				
		}
	}
    
    private void inicio() {     	
    	cargarpaseedad(salud.qs802d);
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS809.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS809.readOnly();
			rgQS810.readOnly();
			rgQS811.readOnly();
			rgQS812.readOnly();
			txtQS812A.readOnly();
			rgQS813.readOnly();
			rgQS814.readOnly();			
		}
	}
    
    public void RenombrarEtiquetas(){
    	String replace="(NOMBRE)";
    	String nombre= App.getInstance().getPersonabySeccion8Salud().qh02_1;
    	lblpregunta809.setText(lblpregunta809.getText().toString().replace(replace,nombre));
    	lblpregunta810.setText(lblpregunta810.getText().toString().replace(replace,nombre));
    	lblpregunta811.setText(lblpregunta811.getText().toString().replace(replace,nombre));
    	lblpregunta812.setText(lblpregunta812.getText().toString().replace(replace,nombre));
    	lblpregunta813.setText(lblpregunta813.getText().toString().replace(replace,nombre));
    	lblpregunta814.setText(lblpregunta814.getText().toString().replace(replace,nombre));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS809ChangeValue() {
  		if (MyUtil.incluyeRango(2,2,rgQS809.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),rgQS810,rgQS811,rgQS812,txtQS812A,rgQS813,rgQS814);  	
  			q3.setVisibility(View.GONE);
  			q4.setVisibility(View.GONE);
  			q5.setVisibility(View.GONE);
  			q6.setVisibility(View.GONE);
  			q7.setVisibility(View.GONE);  			
  		} 
  		else {
  			Util.lockView(getActivity(),false,rgQS810,rgQS811,rgQS812,txtQS812A,rgQS813,rgQS814); 
  			q3.setVisibility(View.VISIBLE);
  			q4.setVisibility(View.VISIBLE);
  			q5.setVisibility(View.VISIBLE);
  			q6.setVisibility(View.VISIBLE);
  			q7.setVisibility(View.VISIBLE);
  			onqrgQS810ChangeValue();
  			onqrgQS812ChangeValue();
  			rgQS810.requestFocus();
  		}	
  	}
    public void onqrgQS810ChangeValue() {
    	if (!MyUtil.incluyeRango(2,2,rgQS809.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(2,3,rgQS810.getTagSelected("").toString())) {		
    			MyUtil.LiberarMemoria();
    			Util.cleanAndLockView(getActivity(),rgQS811);  	
      			q4.setVisibility(View.GONE);
      			rgQS812.requestFocus();
      		} 
      		else {
      			Util.lockView(getActivity(),false,rgQS811); 
      			q4.setVisibility(View.VISIBLE);
      			rgQS811.requestFocus();
      		}
    	}  		
  	}
    public void onqrgQS812ChangeValue() {
    	if (!MyUtil.incluyeRango(2,2,rgQS809.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(2,3,rgQS812.getTagSelected("").toString())) {		    	
    			Util.cleanAndLockView(getActivity(),txtQS812A);  
      			rgQS813.requestFocus();
      		} 
      		else {
      			Util.lockView(getActivity(),false,txtQS812A);  	
      			txtQS812A.requestFocus();
      		}
    	}  		
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);
		if (salud.qs810!=null) {
			salud.qs810=salud.getConvertqs804(salud.qs810);
		}
		if (salud.qs812!=null) {
			salud.qs812=salud.getConvertqs804(salud.qs812);
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 			
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		return App.SALUD;
	}
} 
