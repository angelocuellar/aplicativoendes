package gob.inei.endes2024.fragment.C1seccion08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_08Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS803; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS804; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS804C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS805; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQS805_O; 
	
	CSSECCION_08 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblsectorpublico,lblsectorprivado,lblong,lblcampa,lblpregunta803,lblSubtitulo,lblhospital,lblvacio,lblpregunta804,lblpregunta805,lblpregunta804_ind; 
	public GridComponent2 gridPreguntas804;
	public IntegerField txtQS804C1,txtQS804C2;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;  
	LinearLayout q4;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CS_08Fragment_003() {} 
	public CS_08Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		//rango(getActivity(), txtQS804C, 0, 23, null, 99); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS803","QS804","QS804C","QS805","QS805_O","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO","QS802D","QS802A")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS803","QS804","QS804C","QS805","QS805_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09).textSize(21).centrar();  
	  	lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap08_09_0_11_BUCAL).negrita().centrar().colorFondo(R.color.griscabece);
	  	lblpregunta803= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs803);
	  	rgQS803=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS803ChangeValue") ; 
		
	  	Spanned texto = Html.fromHtml("804. �Hace cu�nto tiempo fue la �ltima atenci�n? <b><br><br>REGISTRE:<br>EN \"MESES\", SI ES < DE 2 A�OS<br>EN \"A�OS\", SI ES DE 2 � M�S A�OS</b>");
	  	lblpregunta804 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);	
	  	lblpregunta804.setText(texto);
		
	  	lblpregunta804= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs804);
	  	lblpregunta804_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap08_09qs804_ind).negrita();
	  	rgQS804=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs804_1,R.string.cap08_09qs804_2,R.string.cap08_09qs804_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS804ChangeValue");
		txtQS804C1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQS804C2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
		
		lblpregunta805= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap08_09qs805);
		rgQS805=new RadioGroupOtherField(this.getActivity(),R.string.cap08_09qs805_1,R.string.cap08_09qs805_2,R.string.cap08_09qs805_3,R.string.cap08_09qs805_4,R.string.cap08_09qs805_5,R.string.cap08_09qs805_6,R.string.cap08_09qs805_7,R.string.cap08_09qs805_8,R.string.cap08_09qs805_9,R.string.cap08_09qs805_10,R.string.cap08_09qs805_11,R.string.cap08_09qs805_12,R.string.cap08_09qs805_13,R.string.cap08_09qs805_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS805ChangeValue"); 
		txtQS805_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_a).textSize(18).negrita();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_b).textSize(18).negrita();
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_c).textSize(18).negrita();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap08_09qs805_d).textSize(18).negrita();
		
		rgQS805.agregarTitle(0,lblsectorpublico);
		rgQS805.agregarTitle(1,lblhospital);
		rgQS805.agregarTitle(8,lblsectorprivado);
		rgQS805.agregarTitle(10,lblong);
		rgQS805.agregarTitle(13,lblcampa);
		
		gridPreguntas804 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas804.addComponent(rgQS804,1,3);		
		gridPreguntas804.addComponent(txtQS804C1);
		gridPreguntas804.addComponent(txtQS804C2);
		gridPreguntas804.addComponent(lblvacio);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
	 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblpregunta803,rgQS803); 
		q3 = createQuestionSection(lblpregunta804,lblpregunta804_ind,gridPreguntas804.component());
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta805,rgQS805,txtQS805_O); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q4);

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 	
		uiToEntity(salud);
		if(salud!=null){		
			if (salud.qs804!=null) {
				salud.qs804=salud.getConvertqs804(salud.qs804);
			}
			if (salud.qs805!=null) {
				salud.qs805=salud.getConvertqs805(salud.qs805);
			}
			if (salud.qs804!=null) {
				if (!Util.esDiferente(salud.qs804,1) && txtQS804C1.getText().toString().trim().length()!=0 ) {
					salud.qs804c=Integer.parseInt(txtQS804C1.getText().toString().trim());
				}
			}
			if (salud.qs804!=null) {
				if (!Util.esDiferente(salud.qs804,2) && txtQS804C2.getText().toString().trim().length()!=0) {
					salud.qs804c=Integer.parseInt(txtQS804C2.getText().toString().trim());
				}		
			}
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(salud==null){
			return false;
		}
		if (Util.esVacio(salud.qs803)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.803"); 
			view = rgQS803; 
			error = true; 
			return false; 
		} 
		if(!Util.esDiferente(salud.qs803, 1))
		{
			if (Util.esVacio(salud.qs804)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.804"); 
				view = rgQS804; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs804,1)) {
				if (Util.esVacio(salud.qs804c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.804C1"); 
					view = txtQS804C1; 
					error = true; 
					return false; 
				}
				if(Util.esMayor(salud.qs804c, 23)){
					mensaje = "Opci�n meses no debe ser mayo a 23"; 
					view = txtQS804C1; 
					error = true; 
					return false; 
				}
			}	
			if (!Util.esDiferente(salud.qs804,2)) {
				if (Util.esVacio(salud.qs804c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.804C2"); 
					view = txtQS804C2; 
					error = true; 
					return false; 
				}
				if(Util.esMenor(salud.qs804c,2) || Util.esMayor(salud.qs804c,salud.qs802d)){
					mensaje = "A�os del diagn�stico no puede ser mayor a la edad"; 
					view = txtQS804C2; 
					error = true; 
					return false;
				}
			}
			if (Util.esVacio(salud.qs805)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.805"); 
				view = rgQS805; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(salud.qs805,42,96)){ 
				if (Util.esVacio(salud.qs805_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQS805_O; 
					error = true; 
					return false; 
				} 
			}
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//    	MyUtil.LiberarMemoria();
    	if (App.getInstance().getPersonabySeccion8Salud()!=null){  	
    		salud = getCuestionarioService().getCAP08_09(App.getInstance().getPersonabySeccion8Salud().id, App.getInstance().getPersonabySeccion8Salud().hogar_id ,App.getInstance().getPersonaSeccion01().persona_id ,App.getInstance().getPersonabySeccion8Salud().persona_id_ninio,seccionesCargado);
    	}
    	if (salud!=null && salud.qs804!=null) {
			salud.qs804=salud.setConvertqs804(salud.qs804);
		}
		if (salud!=null && salud.qs805!=null) {
			salud.qs805=salud.setConvertqs805(salud.qs805);
		}
		
    	if(salud==null){ 
    		salud=new CSSECCION_08(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud);
    	
    	if (salud.qs804c!=null) {	
    		if (!Util.esDiferente(salud.qs804,1)) {
    			txtQS804C1.setText(salud.qs804c.toString());
    		}
    		if (!Util.esDiferente(salud.qs804,2)) {
    			txtQS804C2.setText(salud.qs804c.toString());
    		}
    	}      	
    	inicio();  
    } 
    
    public void cargarsegunp802(Integer p802a){
    	if (!Util.esDiferente(p802a,2,4,6)) {
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
		}
    	else{    
    		q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			
			RenombrarEtiquetas();    		
	    	onqrgQS803ChangeValue();
	    	ValidarsiesSupervisora();
	    	rgQS803.requestFocus();
    	}
    }
    
    private void inicio() {
    	cargarsegunp802(salud.qs802a);    
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS803.readOnly();
			rgQS804.readOnly();
			txtQS804C1.readOnly();
			txtQS804C2.readOnly();
			rgQS805.readOnly();
			txtQS805_O.readOnly();
		}
	}
    
    public void RenombrarEtiquetas() {	
    	if ( App.getInstance().getPersonabySeccion8Salud()!=null) {
    		String replace="(NOMBRE)";
        	String nombre=App.getInstance().getPersonabySeccion8Salud().qh02_1;
        	lblpregunta803.setText(lblpregunta803.getText().toString().replace(replace, nombre));
    	}
    
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public void onqrgQS803ChangeValue() {
  		if (MyUtil.incluyeRango(2,2,rgQS803.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),rgQS804,txtQS804C1,txtQS804C2,rgQS805,txtQS805_O);  
  			salud.qs804c=null;
  			q3.setVisibility(View.GONE);
  			q4.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,rgQS804,rgQS805);   			
  			q3.setVisibility(View.VISIBLE);
  			q4.setVisibility(View.VISIBLE);
  			onqrgQS804ChangeValue();  	
  			onqrgQS805ChangeValue();
  			rgQS804.requestFocus();
  		}	
  	}
    
    
	public void onqrgQS804ChangeValue() {
		if (!MyUtil.incluyeRango(3,3,rgQS803.getTagSelected("").toString())) {			
			if (MyUtil.incluyeRango(1,1,rgQS804.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQS804C2);
				salud.qs804c=null;
				Util.lockView(getActivity(),false,txtQS804C1);				
				txtQS804C1.requestFocus();				
			} 
			if (MyUtil.incluyeRango(2,2,rgQS804.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQS804C1);	
				salud.qs804c=null;
				Util.lockView(getActivity(),false,txtQS804C2);			
				txtQS804C2.requestFocus();				
			}	
			if (MyUtil.incluyeRango(3,3,rgQS804.getTagSelected("").toString())) {
				salud.qs804c=null;		
				Util.cleanAndLockView(getActivity(),txtQS804C1,txtQS804C2);			
				rgQS805.requestFocus();				
			}
		}	
	}

	public void onqrgQS805ChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQS803.getTagSelected("").toString())) {
			if (!MyUtil.incluyeRango(12,13,rgQS805.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),txtQS805_O);				
			} 
			else {			
				Util.lockView(getActivity(), false,txtQS805_O);
				txtQS805_O.requestFocus();
			}
		}		
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);
		if(salud!=null){		
			if (salud.qs804!=null) {
				salud.qs804=salud.getConvertqs804(salud.qs804);
			}
			if (salud.qs805!=null) {
				salud.qs805=salud.getConvertqs805(salud.qs805);
			}
			if (salud.qs804!=null) {
				if (!Util.esDiferente(salud.qs804,1) && txtQS804C1.getText().toString().trim().length()!=0 ) {
					salud.qs804c=Integer.parseInt(txtQS804C1.getText().toString().trim());
				}
			}
			if (salud.qs804!=null) {
				if (!Util.esDiferente(salud.qs804,2) && txtQS804C2.getText().toString().trim().length()!=0) {
					salud.qs804c=Integer.parseInt(txtQS804C2.getText().toString().trim());
				}		
			}
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}

} 
