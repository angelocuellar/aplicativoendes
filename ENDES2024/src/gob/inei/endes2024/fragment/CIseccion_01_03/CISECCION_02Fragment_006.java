package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.R.integer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_006 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI229A;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI229AA;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI229B;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI229C_A;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI229C_B;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI229C_C;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI229C_D;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI229C_E;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI229C_X;
	@FieldAnnotation(orderIndex=10)
	public TextField txtQI229C_O;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQH12;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI230;
	@FieldAnnotation(orderIndex=13) 
	public TextAreaField txtQI230_O;
	@FieldAnnotation(orderIndex=14)
	public IntegerField txtQI231_M;
	@FieldAnnotation(orderIndex=15)
	public IntegerField txtQI231_Y;
	@FieldAnnotation(orderIndex=16)
	public IntegerField txtQI233;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	Seccion01 persona=null;
	public GridComponent2 gdMesesEmbarazo,gdEmbarazo,gdMeses;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta229A,lblpregunta229AA,lblobsqi230,lblpregunta229B,lblpregunta229C,lblpregunta230,lblmeses,lblmeses2,lblpregunta231,lblpregunta233,lblmes,lblanio,lblPregunta12;
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7,q8;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabado0A2,seccionesCargado0A2;
	SeccionCapitulo[] seccionesCargado,seccionesCargado2;
	List<CISECCION_02> listados2;
	
	
	
	public CISECCION_02Fragment_006() {}
	public CISECCION_02Fragment_006 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo( 0,-1,-1,"QI229A","QI229AA","QI229B","QI229C_A","QI229C_B","QI229C_C","QI229C_D","QI229C_E","QI229C_X","QI229C_O","QI230","QI230_O","QI231_M","QI231_Y","QI233","QI226","QI227","QI229_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargado2 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI215D","QI215M","QI215Y","QI220A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM") };
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI229A","QI229AA","QI229B","QI229C_A","QI229C_B","QI229C_C","QI229C_D","QI229C_E","QI229C_X","QI229C_O","QI230","QI230_O","QI231_M","QI231_Y","QI233")};
		
		seccionesGrabado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12") };
		seccionesCargado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","ID","HOGAR_ID","PERSONA_ID") };
		
		return rootView;
  }
  
  @Override
  protected void buildFields() {

		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta229A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229a);
		lblpregunta229AA = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229aa);
		lblpregunta229B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229b);
		lblpregunta229C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229c);
		lblpregunta230 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi230);
		lblpregunta231 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi231);
		lblpregunta233 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi233);
		lblPregunta12 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.seccion01qh12_ind);
		
		txtQI229A=new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(1);
		lblmeses  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi229am).textSize(16).centrar();
		rgQI229AA =new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi229aa_1,R.string.ciseccion_02qi229aa_2, R.string.ciseccion_02qi229aa_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI229B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi229b_1,R.string.ciseccion_02qi229b_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI229BChangeValue");
		lblmeses2  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi233_m).textSize(16).centrar();
		lblmes  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi231_m).textSize(16).centrar();
		lblanio  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi231_y).textSize(16).centrar();
		
		
		rgQH12=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh12_1,R.string.seccion01qh12_2,R.string.seccion01qh12_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		chbQI229C_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI229C_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI229C_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI229C_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI229C_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI229C_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229c_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229C_XChangeValue");
		txtQI229C_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI230=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi230_1,R.string.ciseccion_02qi230_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI230ChangeValue");
		lblobsqi230 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi230_o);
		txtQI230_O = new TextAreaField(getActivity()).maxLength(1000).size(100, 750).alfanumerico();
		
		txtQI231_M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQI231_Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).callback("onFechaChangea");	
		
		
		gdMesesEmbarazo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdMesesEmbarazo.addComponent(lblmeses);
		gdMesesEmbarazo.addComponent(txtQI229A);
		
		gdEmbarazo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdEmbarazo.addComponent(lblmes);
		gdEmbarazo.addComponent(txtQI231_M);
		gdEmbarazo.addComponent(lblanio);
		gdEmbarazo.addComponent(txtQI231_Y);	
		
		txtQI233=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(1);
    
		gdMeses = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdMeses.addComponent(lblmeses2);
		gdMeses.addComponent(txtQI233);
  }
  
    @Override
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta229A,gdMesesEmbarazo.component());
		q2 = createQuestionSection(lblpregunta229AA,rgQI229AA);
		q3 = createQuestionSection(lblpregunta229B,rgQI229B);
		LinearLayout ly229c = new LinearLayout(getActivity());
		ly229c.addView(chbQI229C_X);
		ly229c.addView(txtQI229C_O);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta229C,chbQI229C_A,chbQI229C_B,chbQI229C_C,chbQI229C_D,chbQI229C_E,ly229c);
		q5 = createQuestionSection(lblPregunta12,rgQH12); 
		q6 = createQuestionSection(lblpregunta230,rgQI230,lblobsqi230,txtQI230_O);
		q7 = createQuestionSection(lblpregunta231,gdEmbarazo.component());
		q8 = createQuestionSection(lblpregunta233,gdMeses.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		
    return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (persona.qh12!=null) {
			persona.qh12=persona.getConvertqh12(persona.qh12);
		}
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
			if(individual.qi230!=null && individual.qi231_m!=null && individual.qi231_y!=null) {          
		          if(individual.qi230 == 1) {
		        	  App.getInstance().getPersonaCuestionarioIndividual().fechaPrimerAborto = individual.qi231_m+"/"+ individual.qi231_y;
		            }
	    	}
			if(VerificarsiCambiodeRespuestasSalud(persona,individual)){
				if(individual.qi229b==1){
				persona.id=individual.id;
				persona.hogar_id=individual.hogar_id;
				persona.persona_id=individual.persona_id;
				persona.qh11_a=individual.qi229c_b;
				persona.qh11_b=individual.qi229c_c;
				persona.qh11_c=individual.qi229c_a;
				persona.qh11_d=individual.qi229c_d;
				persona.qh11_e=individual.qi229c_e;
				persona.qh11_y=0;
				persona.qh11_z=0;
				persona.qh12=Integer.parseInt(rgQH12.getTagSelected().toString());
				}
				if(individual.qi229b==2){
					persona.id=individual.id;
					persona.hogar_id=individual.hogar_id;
					persona.persona_id=individual.persona_id;
					persona.qh11_a=0;
					persona.qh11_b=0;
					persona.qh11_c=0;
					persona.qh11_d=0;
					persona.qh11_e=0;
					persona.qh11_y=0;
					persona.qh11_z=1;
					persona.qh12=null;
				}
				if(!getCuestionarioService().saveOrUpdate(persona, null,seccionesGrabado0A2)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados. S01", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return false;
				}
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi230 = individual.qi230;
			App.getInstance().getPersonaCuestionarioIndividual().qi231_y = individual.qi231_y;
		} 
		else{ 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
		return true;
    }
    
    public boolean VerificarsiCambiodeRespuestasSalud(Seccion01 bean, CISECCION_01_03 individual) {
    	if(bean.qh11_a!=individual.qi229c_b  || bean.qh11_b!=individual.qi229c_c  || bean.qh11_c!=individual.qi229c_a  || bean.qh11_d!=individual.qi229c_d  || bean.qh11_e!=individual.qi229c_e){
    		MyUtil.MensajeGeneral(getActivity(), "Se actualizo datos de seguro de salud en el cuestionario de Hogar");
    		return true;    		
    	}
    	else{
    		return false;
    	}
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		Integer p226 =individual.qi226==null?0:individual.qi226;
		Integer in231_y=individual.qi231_y==null?0:individual.qi231_y;
		Integer p229_y =individual.qi229_y==null?0:individual.qi229_y;
		
		if (p229_y!=1 && p226!=2 && p226!=8) {
			if (Util.esVacio(individual.qi229a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI229A");
				view = txtQI229A;
				error = true;
				return false;
			}
			if (Util.esMayor(individual.qi229a,individual.qi227)) {
				mensaje = "La cantidad de meses de control no puede ser mayor a la cantidad de meses de embarazo";
				view = txtQI229A;
				error = true;
				return false;
			}
			if (!MyUtil.incluyeRango(0,9,individual.qi229a)) {
				mensaje = "La cantidad de meses de control esta fuera de rango";
				view = txtQI229A;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi229aa)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI229AA");
				view = rgQI229AA;
				error = true;
				return false;
			}
		}
		if (Util.esVacio(individual.qi229b)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI229B");
			view = rgQI229B;
			error = true;
			return false;
		}
		if (Util.esDiferente(individual.qi229b,2)) {
			if (!verificarCheck()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI229C_A");
				view = chbQI229C_A;
				error = true;
				return false;
			}
			if (chbQI229C_X.isChecked()) {
				if (Util.esVacio(individual.qi229c_o)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQI229C_O;
					error = true;
					return false;
				}
			}
			Integer qh12 =Integer.parseInt(rgQH12.getTagSelected("0").toString());
			if (!Util.esDiferente(qh12,0) && !Util.esDiferente(individual.qi229b,1)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QH12");
				view = rgQH12;
				error = true;
				return false;
			}
			
	    	if(!Util.esDiferente(individual.qi229c_b, 1) && !Util.esDiferente(individual.qi229c_a, 1)){
	    		mensaje ="Entidades no compatibles 1";
				view = chbQI229C_A;
				error=true;
				return false;
	    	}
	     	
	    	if(!Util.esDiferente(individual.qi229c_b, 0) && !Util.esDiferente(individual.qi229c_c, 1) && (!Util.esDiferente(individual.qi229c_a, 1) || !Util.esDiferente(individual.qi229c_d, 1))){
	    		mensaje ="Entidades no compatibles";
	    		view = chbQI229C_C;
	    		error=true;
	    		return false;
	    	}
	     	
	    	
	    	if(!Util.esDiferente(individual.qi229c_a, 1) && (!Util.esDiferente(individual.qi229c_b, 1) || !Util.esDiferente(individual.qi229c_c, 1) || !Util.esDiferente(individual.qi229c_d, 1) || !Util.esDiferente(individual.qi229c_e,1))){
	    		mensaje ="Entidades no compatibles";
				view = chbQI229C_B;
				error=true;
				return false;
	    	}	
	    	
	    	if(!Util.esDiferente(individual.qi229c_a, 1) && !Util.esDiferente(individual.qi229c_d, 1) && !Util.esDiferente(individual.qi229c_a, 1)){
	    		mensaje ="Entidades no compatibles";
				view = chbQI229C_D;
				error=true;
				return false;
	    	}
	    	
	    	if(!Util.esDiferente(individual.qi229c_b, 0) && !Util.esDiferente(individual.qi229c_d, 1) && (!Util.esDiferente(individual.qi229c_c, 1) || !Util.esDiferente(individual.qi229c_a, 1))){
	    		mensaje ="Entidades no compatibles";
				view = chbQI229C_D;
				error=true;
				return false;
	    	}
	    	if(!Util.esDiferente(individual.qi229c_e, 1) && !Util.esDiferente(individual.qi229c_a, 1)){
	    		mensaje ="Entidades no compatibles";
				view = chbQI229C_A;
				error=true;
				return false;
	    	}
		}
		if (Util.esVacio(individual.qi230)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI230");
			view = rgQI230;
			error = true;
			return false;
		}
		if (!Util.esDiferente(individual.qi230,1)) {
			if (Util.esVacio(individual.qi231_m)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI231_M");
				view = txtQI231_M;
				error = true;
				return false;
			}
			
			if (MyUtil.incluyeRango(13,99,individual.qi231_m)) {
				mensaje = "La cantidad de meses esta fuera de rango.";
				view = txtQI231_M;
				error = true;
				return false;
			}
			
			if (MyUtil.incluyeRango(1,9,individual.qi231_m)) {
				mensaje = "Los meses deben considerarse a 2 d�gitos: de 01 a 12 meses";
				view = txtQI231_M;
				error = true;
				return false;
			}
			
//			if (!MyUtil.incluyeRango(1,12,individual.qi231_m) && Util.esMayor(individual.qi231_y,App.ANIOPORDEFECTO-1)) {
//				mensaje = "La cantidad de meses esta fuera de rango.";
//				view = txtQI231_M;
//				error = true;
//				return false;
//			}
//			if (!MyUtil.incluyeRango(1,12,individual.qi231_m) && Util.esMenor(individual.qi231_y,App.ANIOPORDEFECTO) && !individual.qi231_m.equals("98")) {
//				mensaje = "La cantidad de meses esta fuera de rango";
//				view = txtQI231_M;
//				error = true;
//				return false;
//			}
			
			if (Util.esVacio(in231_y)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI231_Y");
				view = txtQI231_Y;
				error = true;
				return false;
			}
			if (in231_y.toString().length()<4) {
				mensaje = "La longitud debe ser de 4 digitos";
				view = txtQI231_Y;
				error = true;
				return false;
			}
			if(in231_y>=App.ANIOPORDEFECTO ) {
				if (Util.esVacio(individual.qi233)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI233");
					view = txtQI233;
					error = true;
					return false;
				}
				if (!MyUtil.incluyeRango(0,9,individual.qi233)) {
					mensaje = "La cantidad de meses esta fuera de rango";
					view = txtQI233;
					error = true;
					return false;
				}
			}
			
			boolean retorno=true;
			for(CISECCION_02 cs2 : listados2) {
				
				if(cs2.qi215y!=null && cs2.qi215m!=null && cs2.qi215d!=null) {
				Integer meses = 6;
				if(cs2.qi220a!=null)
					meses = cs2.qi220a; 
				
				Integer mes_fin = Integer.parseInt(cs2.qi215m);
				Integer dia_fin = Integer.parseInt(cs2.qi215d);
				
				Calendar fecha_fin = new GregorianCalendar(cs2.qi215y,mes_fin-1,dia_fin);
				Calendar fecha_ini = new GregorianCalendar(cs2.qi215y,mes_fin-1-meses,dia_fin);
				
				Integer meses2 = 0;
				if(individual.qi233!=null)
					meses2 = individual.qi233; 
				
				Integer mes_eva = Integer.parseInt(individual.qi231_m);
				Calendar fecha_eva = new GregorianCalendar(in231_y,mes_eva-1,1);
				Calendar fecha_eva_ini = new GregorianCalendar(in231_y,mes_eva-1-meses2,1);
//
//				Log.e("retorno",""+retorno);
//				
//				Log.e("mes_fin",""+mes_fin);
//				Log.e("fecha_ini",""+fecha_ini);
//				
//				Log.e("mes_eva",""+mes_eva);
//				Log.e("fecha_eva",""+fecha_eva);
//				Log.e("fecha_eva_ini",""+fecha_eva_ini);
//				
				
				if((fecha_ini.compareTo(fecha_eva)<=0 && fecha_fin.compareTo(fecha_eva)>=0) || 
				   (fecha_ini.compareTo(fecha_eva_ini)<=0 && fecha_fin.compareTo(fecha_eva_ini)>=0)) {
					retorno = false;
					break; 
				}
				
	
				}
			}
			
			if (!retorno) {
				mensaje = "La fecha ingresada no es valida.";
				view = txtQI231_Y;
				error = true;
				return retorno;
			}					
			
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
    	persona=null;
    	persona = getCuestionarioService().getSeccion01(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado0A2);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		
		if(persona.qh12!=null)	{
			persona.qh12=persona.setConvertqh12(persona.qh12);
		}
		
		listados2=getCuestionarioService().getNacimientosListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
		
		entityToUI(individual);
		rgQH12.setTagSelected(persona.qh12);

		inicio();
    }
    
    
    private void inicio() {
    	validarPregunta229B();
    	onrgQI229BChangeValue();
    	onrgQI230ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void validarPregunta229B() {
    	Integer p226 =individual.qi226==null?0:individual.qi226;
    	Integer p229_y =individual.qi229_y==null?0:individual.qi229_y;
    	
    	if (p229_y==1) {
			Util.cleanAndLockView(getActivity(),txtQI229A, rgQI229AA);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		}    	
    	else {
			if (p226==2 || p226==8) {
				Util.cleanAndLockView(getActivity(),txtQI229A, rgQI229AA);
				q1.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);
			}
			else {
				Util.lockView(getActivity(), false,txtQI229A, rgQI229AA);
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
			}
		}		
	}
    
    public void onrgQI229BChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI229B.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI229C_A,chbQI229C_B,chbQI229C_C,chbQI229C_D,chbQI229C_E,chbQI229C_X);
			MyUtil.LiberarMemoria();
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			rgQI230.requestFocus();
		} else {
			Util.lockView(getActivity(), false,chbQI229C_A,chbQI229C_B,chbQI229C_C,chbQI229C_D,chbQI229C_E,chbQI229C_X,rgQH12);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onqrgQI229C_XChangeValue();
			ValidarPreguntaQH12();
			chbQI229C_A.requestFocus();
		}		
    }
    
    public void ValidarPreguntaQH12() {
    	Integer p11y=persona.qh11_y==null?0:persona.qh11_y;
    	Integer p11x =persona.qh11_z==null?0:persona.qh11_z;
    	
    	if (p11y==1 || p11x==1) {
    		Util.lockView(getActivity(), false,rgQH12);
			q5.setVisibility(View.VISIBLE);
		}
    	else{
     		q5.setVisibility(View.GONE);
    	}
    }
    
    
    public void onqrgQI229C_XChangeValue() {
    	if (chbQI229C_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI229C_O);
  			txtQI229C_O.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI229C_O);
  		}
    }
    
    public boolean verificarCheck() {
  		if (chbQI229C_A.isChecked() || chbQI229C_B.isChecked() || chbQI229C_C.isChecked() || chbQI229C_D.isChecked() || chbQI229C_E.isChecked() || chbQI229C_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    
    public void onrgQI230ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI230.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI231_M,txtQI231_Y,txtQI233);
			MyUtil.LiberarMemoria();
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,txtQI231_M,txtQI231_Y,txtQI233);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			onFechaChangea();
		}
    }
    
    
    public void onFechaChangea() {    	
		if(txtQI231_Y.getValue()!=null && !txtQI231_Y.getText().equals(""))  {
    		Integer anio = Integer.parseInt(txtQI231_Y.getText().toString());
	    	if (anio>=App.ANIOPORDEFECTO) {
	    		Util.lockView(getActivity(), false,txtQI233);
				q8.setVisibility(View.VISIBLE);
				txtQI233.requestFocus();
			} else {
				Util.cleanAndLockView(getActivity(),txtQI233);
				MyUtil.LiberarMemoria();
				q8.setVisibility(View.GONE);
			}
		}
		else{
			txtQI231_M.requestFocus();
		}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI229B.readOnly();
    		rgQI230.readOnly();
    		txtQI230_O.setEnabled(false);
    		txtQI229A.readOnly();
    		rgQI229AA.readOnly();
    		txtQI229C_O.readOnly();
    		txtQI231_M.readOnly();
    		txtQI231_Y.readOnly();
    		chbQI229C_A.readOnly();
    		chbQI229C_B.readOnly();
    		chbQI229C_C.readOnly();
    		chbQI229C_D.readOnly();
    		chbQI229C_E.readOnly();
    		chbQI229C_X.readOnly();
    		txtQI233.readOnly();
    		rgQH12.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return App.NODEFINIDO;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
			if(VerificarsiCambiodeRespuestasSalud(persona,individual)){
				if(individual.qi229b==1){
				persona.id=individual.id;
				persona.hogar_id=individual.hogar_id;
				persona.persona_id=individual.persona_id;
				persona.qh11_a=individual.qi229c_b;
				persona.qh11_b=individual.qi229c_c;
				persona.qh11_c=individual.qi229c_a;
				persona.qh11_d=individual.qi229c_d;
				persona.qh11_e=individual.qi229c_e;
				persona.qh11_y=0;
				persona.qh11_z=0;
				persona.qh12=Integer.parseInt(rgQH12.getTagSelected().toString());
				}
				if(individual.qi229b==2){
					persona.id=individual.id;
					persona.hogar_id=individual.hogar_id;
					persona.persona_id=individual.persona_id;
					persona.qh11_a=0;
					persona.qh11_b=0;
					persona.qh11_c=0;
					persona.qh11_d=0;
					persona.qh11_e=0;
					persona.qh11_y=0;
					persona.qh11_z=1;
					persona.qh12=null;
				}
				if(!getCuestionarioService().saveOrUpdate(persona, null,seccionesGrabado0A2)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados. S01", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return App.NODEFINIDO;
				}
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi230 = individual.qi230;
			App.getInstance().getPersonaCuestionarioIndividual().qi231_y = individual.qi231_y;
		} 
		else{ 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
		return App.INDIVIDUAL;
	}
}