package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.common.MyUtil.Personas;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI102;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI103;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI104;
	
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI101A;
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	Seccion01 persona;
	
	public ButtonComponent btnOmisionMes, btnOmisionAnio;
	private CuestionarioService cuestionarioService;
 LabelComponent lblTitulo,lblpregunta102,lblpregunta103,lblpregunta104,lblpregunta102_Ind,lblpregunta104_Ind,lblpregunta103anios;//,lblpregunta104a,lblpregunta104a_indi_1,lblpregunta104a_indi_2
	public GridComponent2 gridPregunta103;
	public CheckBoxField chbP103_1;
	public CheckBoxField chbP103_2;
	
	LinearLayout q0,q1,q2,q3;//,q4
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoPersona;

	public CISECCION_01Fragment_001() {}
	public CISECCION_01Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI102","QI103","QI104","ID","HOGAR_ID","PERSONA_ID","QI101A")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI102","QI103","QI104","QI101A")};
		seccionesCargadoPersona = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH04","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
  }
  
  @Override 
  protected void buildFields() {
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_01).textSize(21).centrar().negrita();
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblpregunta102 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi102);
	  lblpregunta103 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi103);
	  lblpregunta104 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi104);
//	  lblpregunta104a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi104a);
	  
	  lblpregunta103anios = new LabelComponent(this.getActivity()).size(60, 100).text(R.string.ciseccion_01qi103_a).textSize(16).alinearIzquierda();
	  txtQI103=new IntegerField(this.getActivity()).size(60, 80).maxLength(2).centrar();  
	  
	  Spanned texto102 =Html.fromHtml("<b>SI ES CIUDAD, SONDEE:</b> �Era la capital del departamento?");
	  lblpregunta102_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
	  lblpregunta102_Ind.setText(texto102);		
	  lblpregunta104_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
	  lblpregunta104_Ind.setText(texto102);
//	  lblpregunta104a_indi_1=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.ciseccion_01qi104a_indi_1);
//	  lblpregunta104a_indi_2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.ciseccion_01qi104a_indi_2);
	  //lblpregunta104a_indi_2.setText(texto102);
		
	  rgQI102=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi102_1,R.string.ciseccion_01qi102_2,R.string.ciseccion_01qi102_3,R.string.ciseccion_01qi102_4,R.string.ciseccion_01qi102_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	  chbP103_1=new CheckBoxField(this.getActivity(), R.string.ciseccion_01qi103_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
	  chbP103_1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked){
				MyUtil.LiberarMemoria();
	    		chbP103_2.setChecked(false);
	    		Util.cleanAndLockView(getActivity(),txtQI103);
	    		Util.cleanAndLockView(getActivity(),rgQI104,rgQI101A);
				q3.setVisibility(View.GONE);
//				q4.setVisibility(View.GONE);
				if ( persona.qh04!=null  && individual.qi103!=null && !Util.esDiferente(persona.qh04,2)) {
					validarMensaje("Verificar Pregunta QI103 con pregunta QH04 Residencia .");
				}
	  		}else {
	  			MyUtil.LiberarMemoria();
				if (chbP103_2.isChecked()) {
		    		Util.cleanAndLockView(getActivity(),txtQI103);
					txtQI103.setVisibility(View.GONE);
					Util.cleanAndLockView(getActivity(),rgQI104);
					q3.setVisibility(View.GONE);
					Util.lockView(getActivity(), false,rgQI101A);
//					q4.setVisibility(View.VISIBLE);
				}else {
					Util.lockView(getActivity(), false,txtQI103);
					txtQI103.setVisibility(View.VISIBLE);
					Util.lockView(getActivity(), false,rgQI104);
					q3.setVisibility(View.VISIBLE);
					Util.lockView(getActivity(),false,rgQI101A);
//					q4.setVisibility(View.VISIBLE);
				}
			}
		}
	  });
	  chbP103_2=new CheckBoxField(this.getActivity(), R.string.ciseccion_01qi103_2, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
	  chbP103_2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked){
				MyUtil.LiberarMemoria();
	    		chbP103_1.setChecked(false);
	    		Util.cleanAndLockView(getActivity(),txtQI103,rgQI104);
				txtQI103.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
//				q4.setVisibility(View.VISIBLE);
				Util.lockView(getActivity(), false,rgQI101A);
				if ( persona.qh04!=null  && individual.qi103!=null && !Util.esDiferente(persona.qh04,1)) {
					validarMensaje("Verificar Pregunta QI103 con pregunta QH04 Residencia .");
				}
				
	  		}else {
	  			MyUtil.LiberarMemoria();
				if (chbP103_1.isChecked()) {
		    		Util.cleanAndLockView(getActivity(),txtQI103,rgQI104,rgQI101A);
		    		txtQI103.setVisibility(View.GONE);
					q3.setVisibility(View.GONE);
//					q4.setVisibility(View.GONE);
				}else {
					Util.lockView(getActivity(), false,txtQI103);
					txtQI103.setVisibility(View.VISIBLE);
					Util.lockView(getActivity(), false,rgQI104);
					q3.setVisibility(View.VISIBLE);
					Util.lockView(getActivity(),false,rgQI101A);
//					q4.setVisibility(View.VISIBLE);
				}
			}
		}
	  });
	  rgQI104=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi104_1,R.string.ciseccion_01qi104_2,R.string.ciseccion_01qi104_3,R.string.ciseccion_01qi104_4,R.string.ciseccion_01qi104_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	  rgQI101A =new RadioGroupOtherField(this.getActivity()).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);//R.string.ciseccion_01qi104a_1,R.string.ciseccion_01qi104a_2,R.string.ciseccion_01qi104a_3,R.string.ciseccion_01qi104a_4,R.string.ciseccion_01qi104a_5,R.string.ciseccion_01qi104a_6
	  gridPregunta103=new GridComponent2(this.getActivity(),App.ESTILO,2,0);
	  gridPregunta103.addComponent(lblpregunta103anios);
	  gridPregunta103.addComponent(txtQI103);
  	}
  
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta102,lblpregunta102_Ind,rgQI102);
		q2 = createQuestionSection(lblpregunta103,gridPregunta103.component(),chbP103_1,chbP103_2);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta104,lblpregunta104_Ind,rgQI104);
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,rgQI101A);//lblpregunta104a,lblpregunta104a_indi_1,lblpregunta104a_indi_2,
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
//		form.addView(q4);
		return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (chbP103_1.isChecked())
			individual.qi103=95;
		if (chbP103_2.isChecked())
				individual.qi103=96;
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi102 = individual.qi102;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(individual==null){
			return false;
		}
		
		if (Util.esVacio(individual.qi102)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI102");
			view = rgQI102;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi103)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI103");
			view = txtQI103;
			error = true;
			return false;
		}
		if (individual.qi103!=95 && individual.qi103!=96) {
			if (Util.esVacio(individual.qi104)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI104");
				view = rgQI104;
				error = true;
				return false;
			}
		}
//		if (Util.esVacio(individual.qi101a) && (!Util.esDiferente(individual.qi103,96) || Util.esDiferente(individual.qi103, 95))) {
//			mensaje = preguntaVacia.replace("$", "La pregunta QI104A");
//			view = rgQI101A;
//			error = true;
//			return false;
//		}
		/*if(!Util.esDiferente(individual.qi103,96)){
			if (Util.esVacio(individual.qi101a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI104A");
				view = rgQI101A;
				error = true;
				return false;
			}
		}*/
		if (!MyUtil.incluyeRango(0,49,individual.qi103) && !MyUtil.incluyeRango(95,96,individual.qi103)) {
			mensaje = "Dato fuera de rango";
			view = txtQI103;
			error = true;
			return false;
		}
		
		if ( persona.qh04!=null  && individual.qi103!=null && (!Util.esDiferente(persona.qh04,1) && !Util.esDiferente(individual.qi103,96))) {
			validarMensaje("Verificar Pregunta QI103 con pregunta QH04 Residencia .");
		}
		
		if ( persona.qh04!=null  && individual.qi103!=null && (!Util.esDiferente(persona.qh04,2) && Util.esDiferente(individual.qi103,96))) {
			validarMensaje("Verificar Pregunta QI103 con pregunta QH04 Residencia");
		}
		
		return true;
    }

	@Override
	public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id ,seccionesCargado);

		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		persona= getCuestionarioService().getSeccion01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoPersona);
		
		entityToUI(individual);
		inicio();
	}
	
	private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

    private void inicio() {
    	validarPregunta103();
    	ValidarsiesSupervisora();
  	    txtCabecera.requestFocus();
    }
       
    public void validarPregunta103() {
    	String strQI103 =txtQI103.getValue() == null ? "":txtQI103.getValue().toString();
    	MyUtil.LiberarMemoria();
    	if (strQI103.equals("95")) {
    		chbP103_1.setChecked(true);
    		chbP103_2.setChecked(false);
    		Util.cleanAndLockView(getActivity(),txtQI103,rgQI104);
			txtQI103.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		}else if (strQI103.equals("96")) {
			chbP103_1.setChecked(false);
			chbP103_2.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI103,rgQI104);
			txtQI103.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		}
    	else {
			Util.lockView(getActivity(), false,rgQI104);
			q3.setVisibility(View.VISIBLE);		
		}
    }
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI102.readOnly();
			txtQI103.readOnly();
			chbP103_1.readOnly();
			chbP103_2.readOnly();
			rgQI104.readOnly();
			rgQI104.readOnly();
		}
	}
    
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (chbP103_1.isChecked())
			individual.qi103=95;
		if (chbP103_2.isChecked())
				individual.qi103=96;
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return App.NODEFINIDO;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi102 = individual.qi102;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return App.INDIVIDUAL;
	}
}