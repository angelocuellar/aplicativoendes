package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_007 extends FragmentForm {
//	@FieldAnnotation(orderIndex=1)
//	public RadioGroupOtherField rgQI326;
	@FieldAnnotation(orderIndex=1)
	public TextField txtQI327_NOM;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI327;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI327B;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI327C;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta327,lblpregunta327_ind,lblpregunta327B,lblpregunta327C,lbllugar,lblcontrol,lblpregunta321A_1_Ind,lblpregunta321A_2_Ind,lblpregunta321A_3_Ind,lblpregunta321A_4_Ind;
	public TextField txtQI327_1,txtQI327_2;
	LinearLayout q0,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCarga;
	List<CISECCION_02> detalles=null;
	public GridComponent2 gdLugarSalud,gdControles;
	private Calendar fecha_actual =null; 
	public CISECCION_03Fragment_007() {}
	public CISECCION_03Fragment_007 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI327C, 0, 8, null, 9);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI321_NOM","QI321A","QI321A_O","QI322","QI323","QI324","QI324A","QI325","QI325A","QI325A_O","QI325B","QI325B_O","QI325C_A","QI325C_B","QI325C_X","QI325C_O","QI325D","QI325D_O","QI325E","QI325E_O","QI326", "QI327_NOM","QI327","QI327_O","QI327B","QI327C","QI320","QI320_O","QI316Y","QI315Y","QI226","QI230","QI304","QI320","QI326","QI325A","QICAL_CONS","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI321_NOM","QI321A","QI321A_O","QI322","QI323","QI324","QI324A","QI325","QI325A","QI325A_O","QI325B","QI325B_O","QI325C_A","QI325C_B","QI325C_X","QI325C_O","QI325D","QI325D_O","QI325E","QI325E_O","QI326", "QI327_NOM","QI327","QI327_O","QI327B","QI327C")};
		
		seccionesCarga = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
    }
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta327  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi327);
		lblpregunta327_ind  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_03qi327_ind);
		lblpregunta327B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi327b);
		lblpregunta327C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi327c);
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_03qi327_nom).textSize(16);
		lblcontrol = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_03qi327_cont).textSize(16);
		Spanned texto321A_1 =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta321A_1_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_1_Ind.setText(texto321A_1);
		Spanned texto321A_2 =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta321A_2_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_2_Ind.setText(texto321A_2);
		Spanned texto321A_3 =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta321A_3_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_3_Ind.setText(texto321A_3);
		Spanned texto321A_4 =Html.fromHtml("<b>OTRO</b>");
		lblpregunta321A_4_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_4_Ind.setText(texto321A_4);
		
//		rgQI326=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi326_1,R.string.ciseccion_03qi326_2,R.string.ciseccion_03qi326_3,R.string.ciseccion_03qi326_4,R.string.ciseccion_03qi326_5,R.string.ciseccion_03qi326_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI326ChangeValue");
		rgQI327=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi327_1,R.string.ciseccion_03qi327_2,R.string.ciseccion_03qi327_3,R.string.ciseccion_03qi327_4,R.string.ciseccion_03qi327_5,R.string.ciseccion_03qi327_6,R.string.ciseccion_03qi327_7,R.string.ciseccion_03qi327_8,R.string.ciseccion_03qi327_9,R.string.ciseccion_03qi327_10,R.string.ciseccion_03qi327_11,R.string.ciseccion_03qi327_12,R.string.ciseccion_03qi327_13,R.string.ciseccion_03qi327_14,R.string.ciseccion_03qi327_15,R.string.ciseccion_03qi327_16,R.string.ciseccion_03qi327_17,R.string.ciseccion_03qi327_18).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI327_1=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		txtQI327_2=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		rgQI327.agregarEspecifique(8,txtQI327_1);
		rgQI327.agregarEspecifique(17,txtQI327_2);
		rgQI327.addView(lblpregunta321A_1_Ind,0);
		rgQI327.addView(lblpregunta321A_2_Ind,10);
		rgQI327.addView(lblpregunta321A_3_Ind,14);
		rgQI327.addView(lblpregunta321A_4_Ind,18);
		
		/*txtQI327_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI327.agregarEspecifique(17,txtQI327_O);*/
		txtQI327_NOM=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 300).alfanumerico();
		rgQI327B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi327b_1,R.string.ciseccion_03qi327b_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI327BChangeValue");
		txtQI327C=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(1);

		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI327_NOM);
		
		gdControles = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdControles.addComponent(lblcontrol);
		gdControles.addComponent(txtQI327C);
    }
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
//		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta326,rgQI326);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta327,lblpregunta327_ind,gdLugarSalud.component(),rgQI327);
		q3 = createQuestionSection(lblpregunta327B,rgQI327B);
		q4 = createQuestionSection(lblpregunta327C,gdControles.component());
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
//		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi327!=null)
			individual.qi327=individual.getConvertQi327(individual.qi327);
		
		if (individual.qi327!=null) {
			if (individual.qi327==18 && txtQI327_1.getText().toString().trim().length()!=0 ) {
				individual.qi327_o=txtQI327_1.getText().toString().trim();
			}
			else if (individual.qi327==96 && txtQI327_2.getText().toString().trim().length()!=0) {
				individual.qi327_o=txtQI327_2.getText().toString().trim();
			}
			else {
				individual.qi327_o=null;
			}
		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
		detalles = getCuestionarioService().getNacimientosMayor2011ListbyPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id, seccionesCarga);
		App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a   = detalles.size();
		App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 =  getCuestionarioService().getCompletadoSeccion0103CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		}else{
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
			
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in327b=individual.qi327b==null?0:individual.qi327b;
		Integer in320 = individual.qi320==null?0:individual.qi320;
		Integer in326 = individual.qi326==null?0:individual.qi326;
		Integer int304 = individual.qi304==null?0:individual.qi304;
		
    	Integer int315 = individual.qi315y==null?0:individual.qi315y;
    	Integer int316 = individual.qi316y==null?0:individual.qi316y;
    	Integer mayor=int315>int316?int315:int316;
    	
		if (int304!=2) {
    		if ((in320==3 || in320==5 || in320==7 || in320==8 || in320==9) || (mayor<App.ANIOPORDEFECTO && in320!=1)) {
				if (Util.esVacio(individual.qi327)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI327");
					view = rgQI327;
					error = true;
					return false;
				}
				if (individual.qi327==18) {
					if (Util.esVacio(individual.qi327_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI327_1; 
						error = true; 
						return false; 
					}			
				}	
				if (individual.qi327==96) {
					if (Util.esVacio(individual.qi327_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI327_2; 
						error = true; 
						return false; 
					}				
				}
    		}
    		
			if (mayor>=App.ANIOPORDEFECTO && (in320==4 || in320==6)) {
				if (Util.esVacio(individual.qi327b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI327B");
					view = rgQI327B;
					error = true;
					return false;
    			}
				if (individual.qi327b==1) {
					if (Util.esVacio(individual.qi327c)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI327C");
						view = txtQI327C;
						error = true;
						return false;
					}
				}
			}
		}
    	return true;
    }	
    
		
    @Override
    public void cargarDatos() {
    	
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	   	
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individual.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individual.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi315y=individual.qi315y;
    	App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
    	App.getInstance().getPersonaCuestionarioIndividual().qi325a=individual.qi325a;
    	App.getInstance().getPersonaCuestionarioIndividual().qi304=individual.qi304;

		if(individual.qi327!=null)
			individual.qi327=individual.setConvertQi327(individual.qi327);
		entityToUI(individual);
		if (individual.qi327!=null) {
    		if (individual.qi327==9) {
    			txtQI327_1.setText(individual.qi327_o);
    		}
    		if (individual.qi327==18) {
    			txtQI327_2.setText(individual.qi327_o);
    		}
    	}
		inicio();
		
		/********************************************************************************************/
		
		
		
	     Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;
	   	 Integer int320 = App.getInstance().getPersonaCuestionarioIndividual().qi320==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi320;
	   	 Integer int315 = App.getInstance().getPersonaCuestionarioIndividual().qi315y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi315y;
	   	 Integer int316 = App.getInstance().getPersonaCuestionarioIndividual().qi316y==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi316y;
	   	 Log.e("","int320: "+int320+ " int315: "+int315+" int316: "+int316);
	   	 Integer mayor=int315>int316?int315:int316;
	  		
	   	 if ((mayor>100 && mayor<App.ANIOPORDEFECTO) && int226!=1) {
		   		
		   		if(individual.qi321_nom != null){
					individual.qi321_nom = null;
				}
	   		 	if(individual.qi321a != null){
					individual.qi321a = null;
				}
	   		 	if(individual.qi321a_o != null){
					individual.qi321a_o = null;
				}
		   		if(individual.qi322 != null){
					individual.qi322 = null;
				}
		   		if(individual.qi323 != null){
					individual.qi323 = null;
				}
		   		if(individual.qi324 != null){
					individual.qi324 = null;
				}
		   		if(individual.qi324a != null){
					individual.qi324a = null;
				}
		   		if(individual.qi325 != null){
					individual.qi325 = null;
				}
		   		if(individual.qi325a != null){
					individual.qi325a = null;
				}
		   		if(individual.qi325a_o != null){
					individual.qi325a_o = null;
				}
		   		if(individual.qi325b != null){
					individual.qi325b = null;
				}
		   		if(individual.qi325b_o != null){
					individual.qi325b_o = null;
				}
		   		if(individual.qi325c_a != null){
					individual.qi325c_a = null;
				}
		   		if(individual.qi325c_b != null){
					individual.qi325c_b = null;
				}
		   		if(individual.qi325c_o != null){
					individual.qi325c_o = null;
				}
		   		if(individual.qi325c_x != null){
					individual.qi325c_x = null;
				}
		   		if(individual.qi325d != null){
					individual.qi325d = null;
				}
		   		if(individual.qi325d_o != null){
					individual.qi325d_o = null;
				}
		   		if(individual.qi325e != null){
					individual.qi325e = null;
				}
		   		if(individual.qi325e_o != null){
					individual.qi325e_o = null;
				}
		   		if(individual.qi326 != null){
					individual.qi326 = null;
				}
		   		
		   		
	   	 }else{
	   	
	   		 	//ToastMessage.msgBox(this.getActivity(), "fuera de rango", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
	   		

	   	 }
		
		
		
		
		
		/********************************************************************************************/
    }
    
    public void validarPreguntas(){
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	Integer in320 = individual.qi320==null?0:individual.qi320;
    	
    	Integer int315 = individual.qi315y==null?0:individual.qi315y;
    	Integer int316 = individual.qi316y==null?0:individual.qi316y;
    	Integer mayor=int315>int316?int315:int316;
    	
    	if (int304==2 ) {
    		Util.cleanAndLockView(getActivity(),txtQI327_NOM,rgQI327,rgQI327B,txtQI327C);
//    		Log.e("sss 304 =2 0",""+in320);
    		q0.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
		}
    	else{
    		if (mayor>=App.ANIOPORDEFECTO) {
    	      	if (in320==0 || in320==1 || in320==2 || in320==10 || in320==11 || in320==12 || in320==13 || in320==96 ) {
//            		Log.e("sss 320 1",""+in320);
            		Util.cleanAndLockView(getActivity(),txtQI327_NOM,rgQI327,rgQI327B,txtQI327C);
            		q0.setVisibility(View.GONE);
            		q2.setVisibility(View.GONE);
        			q3.setVisibility(View.GONE);
        			q4.setVisibility(View.GONE);
        		}
            	if (in320==3 || in320==5 || in320==9 || in320==7 || in320==8) {
//            		Log.e("sss 320 2",""+in320);
            		Util.cleanAndLockView(getActivity(),rgQI327B,txtQI327C);
            		Util.lockView(getActivity(),false,txtQI327_NOM,rgQI327);
            		q0.setVisibility(View.VISIBLE);
            		q2.setVisibility(View.VISIBLE);
        			q3.setVisibility(View.GONE);
        			q4.setVisibility(View.GONE);
        		}
            	if (in320==4 || in320==6) {
//            		Log.e("sss 320 3",""+in320);
            		Util.cleanAndLockView(getActivity(),txtQI327_NOM,rgQI327);
            		Util.lockView(getActivity(),false,rgQI327B,txtQI327C);
            		q0.setVisibility(View.VISIBLE);
            		q2.setVisibility(View.GONE);
        			q3.setVisibility(View.VISIBLE);
        			q4.setVisibility(View.VISIBLE);
        			onrgQI327BChangeValue();
            	} 
			}
    		else{
    			if ( in320==0 || in320==1 || in320==2 || in320==10 || in320==11 || in320==12 || in320==13 || in320==96 ) {
//            		Log.e("fd 320 1",""+in320);
            		Util.cleanAndLockView(getActivity(),txtQI327_NOM,rgQI327,rgQI327B,txtQI327C);
            		q0.setVisibility(View.GONE);
            		q2.setVisibility(View.GONE);
        			q3.setVisibility(View.GONE);
        			q4.setVisibility(View.GONE);
        		}
    			else{
//    				Log.e("dgdf 320 3",""+in320);
            		Util.lockView(getActivity(),false,txtQI327_NOM,rgQI327);
            		Util.cleanAndLockView(getActivity(),rgQI327B,txtQI327C);
            		q0.setVisibility(View.VISIBLE);
            		q2.setVisibility(View.VISIBLE);
        			q3.setVisibility(View.GONE);
        			q4.setVisibility(View.GONE);
    			}				
    			
    		}
       
    	}

    }
    	
    private void inicio() {
    	validarPreguntas();
    	RenombrarEtiquetas(); 
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    
    public void onrgQI327BChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,2,rgQI327B.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI327C);
			q4.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,txtQI327C);
			q4.setVisibility(View.VISIBLE);
			txtQI327C.requestFocus();
		}
    }
    
    public void RenombrarEtiquetas(){
    	
    	lblpregunta327.text(R.string.ciseccion_03qi327); 
    	lblpregunta327C.text(R.string.ciseccion_03qi327c);
    	
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fecha_actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecha_actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	
    	
    	lblpregunta327C.setText(lblpregunta327C.getText().toString().replace("#", F2));
    	lblpregunta327C.setText(lblpregunta327C.getText().toString().replace("$", F1));
    	lblpregunta327C.setText(lblpregunta327C.getText().toString().replace("%", F3));
    	
    	lblpregunta327.setText(lblpregunta327.getText().toString().replace("(METODO)", CodigoCorrecto()));
    	
    }
    public String CodigoCorrecto(){
    	String nombre="";
    	if(!Util.esDiferente(individual.qi320, 1)){
    		nombre="ESTERILIZACI�N FEMENINA";
    	}
    	if(!Util.esDiferente(individual.qi320, 3)){
    		nombre="P�LDORA";
    	}
    	if(!Util.esDiferente(individual.qi320, 4)){
    		nombre="DIU";
    	}
    	if(!Util.esDiferente(individual.qi320, 5)){
    		nombre="INYECCI�N";
    	}
    	if(!Util.esDiferente(individual.qi320, 6)){
    		nombre="IMPLANTES";
    	}
    	if(!Util.esDiferente(individual.qi320, 7)){
    		nombre="COND�N";
    	}
    	if(!Util.esDiferente(individual.qi320, 8)){
    		nombre="COND�N FEMENINO";
    	}
    	if(!Util.esDiferente(individual.qi320, 9)){
    		nombre="ESPUMA, JALEA, �VULOS(VAGINALES)";
    	}
    	return nombre;
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI327.readOnly();
    		rgQI327B.readOnly();
    		txtQI327_1.readOnly();
    		txtQI327_2.readOnly();
    		txtQI327_NOM.readOnly();
    		txtQI327C.readOnly();
    	}
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi327!=null)
			individual.qi327=individual.getConvertQi327(individual.qi327);
		
		if (individual.qi327!=null) {
			if (individual.qi327==18 && txtQI327_1.getText().toString().trim().length()!=0 ) {
				individual.qi327_o=txtQI327_1.getText().toString().trim();
			}
			else if (individual.qi327==96 && txtQI327_2.getText().toString().trim().length()!=0) {
				individual.qi327_o=txtQI327_2.getText().toString().trim();
			}
			else {
				individual.qi327_o=null;
			}
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()==null) 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		 
			
		return App.INDIVIDUAL;
	}
}