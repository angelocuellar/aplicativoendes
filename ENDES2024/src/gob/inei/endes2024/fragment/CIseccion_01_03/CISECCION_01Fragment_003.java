package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_003 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI111;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI112;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQI112_O;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI114;
	@FieldAnnotation(orderIndex=5)
	public TextField txtQI114_O;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI114A;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	Seccion01 personavalida;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta111,lblpregunta112,lblpregunta114,lblpregunta114A,lblpregunta114_ind1,lblpregunta114_ind2;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoPersona;

	public CISECCION_01Fragment_003() {}
	public CISECCION_01Fragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI111","QI112","QI112_O","QI114","QI114_O","QI114A","QI106","QI107","QI108N","QI108Y","QI108G","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI111","QI112","QI112_O","QI114","QI114_O","QI114A")};
		seccionesCargadoPersona = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH7DD","QH7MM","QH14","QH15N","QH15Y","QH15G","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
  }
  
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta111 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi111);
		lblpregunta112 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi112);
		lblpregunta114 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi114);
		lblpregunta114_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_01qi114_ind1);
		lblpregunta114_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi114_ind2);
		lblpregunta114A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi114a);
		
		rgQI111=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi111_1,R.string.ciseccion_01qi111_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI111ChangeValue");
		rgQI112=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi112_1,R.string.ciseccion_01qi112_2,R.string.ciseccion_01qi112_3,R.string.ciseccion_01qi112_4,R.string.ciseccion_01qi112_5,R.string.ciseccion_01qi112_6,R.string.ciseccion_01qi112_7,R.string.ciseccion_01qi112_8,R.string.ciseccion_01qi112_9,R.string.ciseccion_01qi112_10,R.string.ciseccion_01qi112_11,R.string.ciseccion_01qi112_12,R.string.ciseccion_01qi112_13,R.string.ciseccion_01qi112_14,R.string.ciseccion_01qi112_15).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI112_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI112.agregarEspecifique(13,txtQI112_O);
		rgQI114=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi114_1,R.string.ciseccion_01qi114_2,R.string.ciseccion_01qi114_3,R.string.ciseccion_01qi114_4,R.string.ciseccion_01qi114_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI114_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico();
		rgQI114.agregarEspecifique(3,txtQI114_O);
		rgQI114A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi114a_1,R.string.ciseccion_01qi114a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
    }
  
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta111,rgQI111);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta112,rgQI112);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta114,lblpregunta114_ind1,lblpregunta114_ind2,rgQI114);
		q4 = createQuestionSection(lblpregunta114A,rgQI114A);
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
    return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			App.getInstance().getPersonaCuestionarioIndividual().qi106=individual.qi106;
			App.getInstance().getPersonaCuestionarioIndividual().qi107=individual.qi107;
			App.getInstance().getPersonaCuestionarioIndividual().qi108n=individual.qi108n;
			App.getInstance().getPersonaCuestionarioIndividual().qi108y=individual.qi108y;
			App.getInstance().getPersonaCuestionarioIndividual().qi108g=individual.qi108g;
		}
		else{
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
			
		
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer st106 = individual.qi106==null?0:individual.qi106;
		String st107 = individual.qi107==null?"":individual.qi107.toString();
		String st111= individual.qi111==null?"":individual.qi111.toString();
		Integer int108n = individual.qi108n==null?0:individual.qi108n;
		
		if (st107.equals("1")) {
			if (st106<=24) {
				if (Util.esVacio(individual.qi111)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI111");
					view = rgQI111;
					error = true;
					return false;
				}
				if (st111.equals("2")) {
					if (Util.esVacio(individual.qi112)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI112");
						view = rgQI112;
						error = true;
						return false;
					}
					if(!Util.esDiferente(individual.qi112,14)){
						if (Util.esVacio(individual.qi112_o)) {
							mensaje = "Debe ingresar informaci\u00f3n en Especifique";
							view = txtQI112_O;
							error = true;
							return false;
						}
					}
				}
			}
			if (int108n==0 || int108n==1 || st107.equals("2")) {
				if (Util.esVacio(individual.qi114)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI114");
					view = rgQI114;
					error = true;
					return false;
				}
				if(!Util.esDiferente(individual.qi114,4)){
					if (Util.esVacio(individual.qi114_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique";
						view = txtQI114_O;
						error = true;
						return false;
					}
				}
			}
		}
		if (int108n==0 || int108n==1) {
			if (Util.esVacio(individual.qi114a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI114A");
				view = rgQI114A;
				error = true;
				return false;
			}
		}
		
		return true;
    }
 
	@Override
	public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		personavalida = getCuestionarioService().getSeccion01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoPersona);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		entityToUI(individual);
		inicio();
		App.getInstance().setEditarPersonaSeccion01(personavalida);
	}
    private void inicio() {
    	validarPregunta107();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void validarPregunta107() {
    	String st107 = individual.qi107==null?"":individual.qi107.toString();
    	Integer st106 = individual.qi106==null?0:individual.qi106;
		Integer int108n = individual.qi108n==null?0:individual.qi108n;
		
    	if (st107.equals("2")) {
    		Log.e("1","1");
    		Util.cleanAndLockView(getActivity(),rgQI111,rgQI112);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		if (st106<=24) {
    			Log.e("1","2");
    			Util.lockView(getActivity(), false,rgQI111);
    			q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.VISIBLE);        		
        		if (rgQI111.getTagSelected("").toString().equals("1")) {
        			Log.e("1","3");
					Util.cleanAndLockView(getActivity(),rgQI112);
	        		q2.setVisibility(View.GONE);
	        		if (int108n==0 ||int108n==1) {
	        			Util.lockView(getActivity(), false,rgQI114,rgQI114A);
						q3.setVisibility(View.VISIBLE);
						q4.setVisibility(View.VISIBLE);
	        		}else if (int108n==2 || int108n==3||int108n==4 ||int108n==5 ||int108n==6) {
	        			Log.e("1","5");
	        			Util.cleanAndLockView(getActivity(),rgQI114,rgQI114A);
		        		q3.setVisibility(View.GONE);
		        		q4.setVisibility(View.GONE);
					}
	        		
				}else {
					Log.e("1","6");
					Util.lockView(getActivity(), false,rgQI112);
					q0.setVisibility(View.VISIBLE);
	        		q2.setVisibility(View.VISIBLE);
	        		if (int108n==0||int108n==1) {
	        			Log.e("1","7");
	        			Util.lockView(getActivity(), false,rgQI114,rgQI114A);
						q3.setVisibility(View.VISIBLE);
						q4.setVisibility(View.VISIBLE);
	        		}else if (int108n==2 || int108n==3||int108n==4 ||int108n==5 ||int108n==6) {
	        			Log.e("1","8");
	        			Util.cleanAndLockView(getActivity(),rgQI114,rgQI114A);
		        		q3.setVisibility(View.GONE);
		        		q4.setVisibility(View.GONE);
					}
				}

			}else if (st106>24) {
				if (int108n==0||int108n==1) {
					Log.e("1","9");
					Util.cleanAndLockView(getActivity(),rgQI111,rgQI112);
					q0.setVisibility(View.VISIBLE);
					q1.setVisibility(View.GONE);
		    		q2.setVisibility(View.GONE);
					Util.lockView(getActivity(), false,rgQI114,rgQI114A);
					q3.setVisibility(View.VISIBLE);
					q4.setVisibility(View.VISIBLE);
				}else if (int108n==2 || int108n==3 || int108n==4 ||int108n==5 ||int108n==6) {
					Log.e("1","10");
					Util.cleanAndLockView(getActivity(),rgQI111,rgQI112,rgQI114,rgQI114A);
					q0.setVisibility(View.GONE);
					q1.setVisibility(View.GONE);
		    		q2.setVisibility(View.GONE);
	        		q3.setVisibility(View.GONE);
	        		q4.setVisibility(View.GONE);
				}
			}
    	}
    }
    
    public void onrgQI111ChangeValue() {
    	Integer int108n = individual.qi108n;    	
			if (MyUtil.incluyeRango(2,2,rgQI111.getTagSelected("").toString())) {
				Util.lockView(getActivity(), false,rgQI112,rgQI114,rgQI114A);
				q2.setVisibility(View.VISIBLE);
				rgQI112.requestFocus();
			} else {
				if (int108n>1 && int108n<7) {
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),rgQI112,txtQI112_O,rgQI114,rgQI114A);
					MyUtil.LiberarMemoria();
					q2.setVisibility(View.GONE);
					q3.setVisibility(View.GONE);
					q4.setVisibility(View.GONE);
				}else {
					Util.cleanAndLockView(getActivity(),rgQI112,txtQI112_O);
					q2.setVisibility(View.GONE);
					Util.lockView(getActivity(), false,rgQI114,rgQI114A);
					q3.setVisibility(View.VISIBLE);
					q4.setVisibility(View.VISIBLE);
					rgQI114.requestFocus();
				}
			}
	}
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI111.readOnly();
			rgQI112.readOnly();
			rgQI114.readOnly();
			rgQI114A.readOnly();
			txtQI112_O.readOnly();
			txtQI114_O.readOnly();
		}
	}
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}