package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_006 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI325B;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQI325B_O;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI325C_A;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI325C_B;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI325C_X;
	@FieldAnnotation(orderIndex=6)
	public TextField txtQI325C_O;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI325D;
	@FieldAnnotation(orderIndex=7)
	public TextField txtQI325D_O;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI325E;
	@FieldAnnotation(orderIndex=9)
	public TextField txtQI325E_O;

	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta325B,lblpregunta325C,lblpregunta325D,lblpregunta325E, lblpregunta325CX;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_03Fragment_006() {}
	public CISECCION_03Fragment_006 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI325B","QI325B_O","QI325C_A","QI325C_B","QI325C_X","QI325C_O","QI325D","QI325D_O","QI325E","QI325E_O","QI226","QI230","QI304","QI320","QI325A","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI325B","QI325B_O","QI325C_A","QI325C_B","QI325C_X","QI325C_O","QI325D","QI325D_O","QI325E","QI325E_O")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta325B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325b);
		lblpregunta325C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325c);
		lblpregunta325D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325d);
		lblpregunta325E = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325e);
		
		lblpregunta325CX = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03qi325cx);
		
		rgQI325B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi325b_1,R.string.ciseccion_03qi325b_2,R.string.ciseccion_03qi325b_3,R.string.ciseccion_03qi325b_4,R.string.ciseccion_03qi325b_5,R.string.ciseccion_03qi325b_6,R.string.ciseccion_03qi325b_7,R.string.ciseccion_03qi325b_8,R.string.ciseccion_03qi325b_9,R.string.ciseccion_03qi325b_10,R.string.ciseccion_03qi325b_11,R.string.ciseccion_03qi325b_12,R.string.ciseccion_03qi325b_13,R.string.ciseccion_03qi325b_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI325B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI325B.agregarEspecifique(13,txtQI325B_O);
		chbQI325C_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi325c_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI325C_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi325c_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI325C_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi325c_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI325C_XChangeValue");
		txtQI325C_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI325D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi325d_1,R.string.ciseccion_03qi325d_2,R.string.ciseccion_03qi325d_3,R.string.ciseccion_03qi325d_4,R.string.ciseccion_03qi325d_5,R.string.ciseccion_03qi325d_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI325D_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI325D.agregarEspecifique(5,txtQI325D_O);
		rgQI325E=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi325e_1,R.string.ciseccion_03qi325e_2,R.string.ciseccion_03qi325e_3,R.string.ciseccion_03qi325e_4,R.string.ciseccion_03qi325e_5,R.string.ciseccion_03qi325e_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI325E_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI325E.agregarEspecifique(5,txtQI325E_O);
    }
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta325B,rgQI325B);
		LinearLayout ly325C = new LinearLayout(getActivity());
		ly325C.addView(chbQI325C_X);
		ly325C.addView(txtQI325C_O);
		ly325C.addView(lblpregunta325CX);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta325C,chbQI325C_A,chbQI325C_B,ly325C);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta325D,rgQI325D);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta325E,rgQI325E);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
 
    return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi325b!=null)
			individual.qi325b=individual.getConvertQi325b(individual.qi325b);
		if (individual.qi325d!=null)
			individual.qi325d=individual.getConvertQi325d(individual.qi325d);
		if (individual.qi325e!=null)
			individual.qi325e=individual.getConvertQi325e(individual.qi325e);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()==null) 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in325a = individual.qi325a==null?0:individual.qi325a;
    	Integer in325b = individual.qi325b==null?0:individual.qi325b;
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	if (int304!=2) {
    		if (in325a==2) {
        		if (Util.esVacio(individual.qi325b)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI325B");
        			view = rgQI325B;
        			error = true;
        			return false;
        		}
        		if(in325b==96){
        			if (Util.esVacio(individual.qi325b_o)) {
        				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
        				view = txtQI325B_O;
        				error = true;
        				return false;
        			}
        		}
        		if (!verificarCheck()) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI325C");
        			view = chbQI325C_A;
        			error = true;
        			return false;
    			}
        		if (chbQI325C_X.isChecked()) {
    				if (Util.esVacio(individual.qi325c_o)) {
    					mensaje = preguntaVacia.replace("$", "La pregunta QI325C_O");
    					view = txtQI325C_O;
    					error = true;
    					return false;
    				}
    			}
    		}
    		
    		if (in325a==2 || in325a==3 || in325a==4 || in325a==6) {
        		if (Util.esVacio(individual.qi325d)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI325D");
        			view = rgQI325D;
        			error = true;
        			return false;
        		}
        		if(individual.qi325d==96){
        			if (Util.esVacio(individual.qi325d_o)) {
        				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
        				view = txtQI325D_O;
        				error = true;
        				return false;
        			}
        		}
        		if (Util.esVacio(individual.qi325e)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI325E");
        			view = rgQI325E;
        			error = true;
        			return false;
        		}
        		if(individual.qi325e==96){
        			if (Util.esVacio(individual.qi325e_o)) {
        				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
        				view = txtQI325E_O;
        				error = true;
        				return false;
        			}
        		}
			}

    	}
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getPersonaCuestionarioIndividual().qi325a=individual.qi325a;
    	
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi325b!=null)
			individual.qi325b=individual.setConvertQi325b(individual.qi325b);
		if(individual.qi325d!=null)
			individual.qi325d=individual.setConvertQi325d(individual.qi325d);
		if(individual.qi325e!=null)
			individual.qi325e=individual.setConvertQi325e(individual.qi325e);
		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta304(){
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	Integer in320 = individual.qi320==null?0:individual.qi320;
    	Integer in325a = individual.qi325a==null?0:individual.qi325a;
    	
    	if (in325a==1 || int304==2 || in320 ==7 || in320 ==8 ) {
    		Util.cleanAndLockView(getActivity(),rgQI325B,txtQI325B_O,chbQI325C_A,chbQI325C_B,chbQI325C_X,txtQI325C_O,rgQI325D,txtQI325D_O,rgQI325E,txtQI325E_O);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
		}else {
	    	Integer in325b = individual.qi325b==null?0:individual.qi325b;
	    	if (in325a==3 || in325a==4 || in325a==6) {
	    		Util.cleanAndLockView(getActivity(),rgQI325B,txtQI325B_O,chbQI325C_A,chbQI325C_B,chbQI325C_X,txtQI325C_O);
	    		q0.setVisibility(View.VISIBLE);
	    		q1.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);
				Util.lockView(getActivity(), false,rgQI325D,rgQI325E);
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
			}else {
				Util.lockView(getActivity(), false,rgQI325B,chbQI325C_A,chbQI325C_B,chbQI325C_X,rgQI325D,rgQI325E);
				q0.setVisibility(View.VISIBLE);
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
				if (in325b==14)
					Util.lockView(getActivity(), false,txtQI325B_O);
				else
					Util.cleanAndLockView(getActivity(),txtQI325B_O);
					onchbQI325C_XChangeValue();
			}
		}
    }
    
    private void inicio() {
    	validarPregunta304();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    public boolean verificarCheck() {
  		if (chbQI325C_A.isChecked() || chbQI325C_B.isChecked() || chbQI325C_X.isChecked()) {
  			return true;
  		}else{
  			return false;
  		}
  	}
    public void onchbQI325C_XChangeValue() {
    	if (chbQI325C_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI325C_O);
  			txtQI325C_O.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI325C_O);
  		}
    }
    
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI325B.readOnly();
    		rgQI325D.readOnly();
    		rgQI325E.readOnly();
    		txtQI325B_O.readOnly();
    		txtQI325C_O.readOnly();
    		txtQI325D_O.readOnly();
    		txtQI325E_O.readOnly();
    		chbQI325C_A.readOnly();
    		chbQI325C_B.readOnly();
    		chbQI325C_X.readOnly();
    	}
    }

	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi325b!=null)
			individual.qi325b=individual.getConvertQi325b(individual.qi325b);
		if (individual.qi325d!=null)
			individual.qi325d=individual.getConvertQi325d(individual.qi325d);
		if (individual.qi325e!=null)
			individual.qi325e=individual.getConvertQi325e(individual.qi325e);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()==null) 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return App.INDIVIDUAL;
	}
}