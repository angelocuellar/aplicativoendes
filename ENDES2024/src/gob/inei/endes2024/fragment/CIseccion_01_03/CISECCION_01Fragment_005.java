package gob.inei.endes2024.fragment.CIseccion_01_03;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_005 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI119A;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQI119A_1;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQI119A_2;
	
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI119B;
	@FieldAnnotation(orderIndex=5)
	public TextField txtQI119B_1;
	@FieldAnnotation(orderIndex=6)
	public TextField txtQI119B_2;
	
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI119C;
	@FieldAnnotation(orderIndex=8)
	public TextField txtQI119C_1;
	@FieldAnnotation(orderIndex=9)
	public TextField txtQI119C_2;
	
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI119D;
	@FieldAnnotation(orderIndex=11)
	public TextField txtQI119D_1;
	@FieldAnnotation(orderIndex=12)
	public TextField txtQI119D_2;
	@FieldAnnotation(orderIndex=13)
	public TextField txtQI119D_3;
//	@FieldAnnotation(orderIndex=14)
//	public TextField txtQI119D_O;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta119A,lblpregunta119B,lblpregunta119C,lblpregunta119D;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_01Fragment_005() {}
	public CISECCION_01Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI119","QI115","QI116","QI117","QI119A","QI119A_O","QI119B","QI119B_O","QI119C","QI119C_O","QI119D","QI119D_O","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI115","QI116","QI117","QI119","QI119A","QI119A_O","QI119B","QI119B_O","QI119C","QI119C_O","QI119D","QI119D_O")};
		return rootView;
	}
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta119A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi119a);
		lblpregunta119B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi119b);
		lblpregunta119C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi119c);
		lblpregunta119D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi119d);
		rgQI119A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi119a_1,R.string.ciseccion_01qi119a_2,R.string.ciseccion_01qi119a_3,R.string.ciseccion_01qi119a_4,R.string.ciseccion_01qi119a_5,R.string.ciseccion_01qi119a_6,R.string.ciseccion_01qi119a_7,R.string.ciseccion_01qi119a_8,R.string.ciseccion_01qi119a_9,R.string.ciseccion_01qi119a_10,R.string.ciseccion_01qi119a_11,R.string.ciseccion_01qi119a_12,R.string.ciseccion_01qi119a_13,R.string.ciseccion_01qi119a_98).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI119A_1=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400).alfanumerico();
		txtQI119A_2=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		rgQI119A.agregarEspecifique(8,txtQI119A_1);
		rgQI119A.agregarEspecifique(11,txtQI119A_2);
		
		rgQI119B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi119b_1,R.string.ciseccion_01qi119b_2,R.string.ciseccion_01qi119b_3,R.string.ciseccion_01qi119b_4,R.string.ciseccion_01qi119b_5,R.string.ciseccion_01qi119b_6,R.string.ciseccion_01qi119b_7,R.string.ciseccion_01qi119b_8,R.string.ciseccion_01qi119b_9,R.string.ciseccion_01qi119b_10,R.string.ciseccion_01qi119b_11,R.string.ciseccion_01qi119b_12,R.string.ciseccion_01qi119b_13,R.string.ciseccion_01qi119b_98).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI119B_1=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		txtQI119B_2=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		rgQI119B.agregarEspecifique(8,txtQI119B_1);
		rgQI119B.agregarEspecifique(11,txtQI119B_2);
		
		rgQI119C=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi119c_1,R.string.ciseccion_01qi119c_2,R.string.ciseccion_01qi119c_3,R.string.ciseccion_01qi119c_4,R.string.ciseccion_01qi119c_5,R.string.ciseccion_01qi119c_6,R.string.ciseccion_01qi119c_7,R.string.ciseccion_01qi119c_8,R.string.ciseccion_01qi119c_9,R.string.ciseccion_01qi119c_10,R.string.ciseccion_01qi119c_11,R.string.ciseccion_01qi119c_12).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI119C_1=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		txtQI119C_2=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		rgQI119C.agregarEspecifique(8,txtQI119C_1);
		rgQI119C.agregarEspecifique(11,txtQI119C_2);
		
		rgQI119D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi119d_1,R.string.ciseccion_01qi119d_2,R.string.ciseccion_01qi119d_3,R.string.ciseccion_01qi119d_4,R.string.ciseccion_01qi119d_5,R.string.ciseccion_01qi119d_6,R.string.ciseccion_01qi119d_7,R.string.ciseccion_01qi119d_8,R.string.ciseccion_01qi119d_98).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI119D_1=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 380);
		txtQI119D_2=new TextField(this.getActivity()).maxLength(500).size(altoComponente+20, 360);
		txtQI119D_3=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		rgQI119D.agregarEspecifique(2,txtQI119D_1);
		rgQI119D.agregarEspecifique(3,txtQI119D_2);
		rgQI119D.agregarEspecifique(7,txtQI119D_3);
		
    }
    @Override 
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta119A,rgQI119A);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta119B,rgQI119B);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta119C,rgQI119C);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta119D,rgQI119D);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
    return contenedor;
    }
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			ModificarvariableparaBD();
			if (individual!=null) {			
				if (individual.qi119a!=null) {
					individual.qi119a=individual.setConvertqi119ab(individual.qi119a);
				}
				if (individual.qi119b!=null) {
					individual.qi119b=individual.setConvertqi119ab(individual.qi119b);
				}
				if(individual.qi119d!=null){
					individual.qi119d=individual.setConvertqi119d(individual.qi119d);
				}
			}			
			
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		String st119 = individual.qi119==null?"":individual.qi119.toString();
    	if (st119.equals("10") || st119.equals("11") || st119.equals("12")) {
    		if (Util.esVacio(individual.qi119a)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI119A");
    			view = rgQI119A;
    			error = true;
    			return false;
    		}
    		if(!Util.esDiferente(individual.qi119a,9)){
    			if (txtQI119A_1.getText().toString().isEmpty()) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI119A_1;
    				error = true;
    				return false;
    			}
    		}
    		
    		if(!Util.esDiferente(individual.qi119a,12)){
    			if (txtQI119A_2.getText().toString().isEmpty()) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI119A_2;
    				error = true;
    				return false;
    			}
    		}
    		if (Util.esVacio(individual.qi119b)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI119B");
    			view = rgQI119B;
    			error = true;
    			return false;
    		}
    		if(!Util.esDiferente(individual.qi119b,9)){
    			if (txtQI119B_1.getText().toString().isEmpty()) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI119B_1;
    				error = true;
    				return false;
    			}
    		}
    		if(!Util.esDiferente(individual.qi119b,12)){
    			if (txtQI119B_2.getText().toString().isEmpty()) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI119B_2;
    				error = true;
    				return false;
    			}
    		}
    	}
		if (Util.esVacio(individual.qi119c)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI119C");
			view = rgQI119C;
			error = true;
			return false;
		}
		if(!Util.esDiferente(individual.qi119c,9)){
			if (txtQI119C_1.getText().toString().isEmpty()) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQI119C_1;
				error = true;
				return false;
			}
		}
		if(!Util.esDiferente(individual.qi119c,12)){
			if (txtQI119C_2.getText().toString().isEmpty()) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQI119C_2;
				error = true;
				return false;
			}
		}
		if (Util.esVacio(individual.qi119d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI119D");
			view = rgQI119D;
			error = true;
			return false;
		}
		if(!Util.esDiferente(individual.qi119d,3)){
			if (txtQI119D_1.getText().toString().isEmpty()) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQI119D_2;
				error = true;
				return false;
			}
		}
		if(!Util.esDiferente(individual.qi119d,4)){
			if (txtQI119D_2.getText().toString().isEmpty()) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQI119D_2;
				error = true;
				return false;
			}
		}
		if(!Util.esDiferente(individual.qi119d,8)){
			if (txtQI119D_3.getText().toString().isEmpty()) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQI119D_3;
				error = true;
				return false;
			}
		}
		return true;
    }
    
	@Override
	public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		if(individual.qi119a!=null){
			individual.qi119a=individual.getConvertqi119ab(individual.qi119a);
		}
		if(individual.qi119b!=null){
			individual.qi119b=individual.getConvertqi119ab(individual.qi119b);
		}
		if(individual.qi119d!=null){
			individual.qi119d=individual.getConvertqi119d(individual.qi119d);
		}
		entityToUI(individual);
		ModificarvariableparaUsuario();
		inicio();
	}
    private void inicio() {
    	validarPregunta119();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void validarPregunta119(){
    	String st119 = individual.qi119==null?"":individual.qi119.toString();
    	if (st119.equals("1") || st119.equals("2") || st119.equals("3") || st119.equals("4") ||  st119.equals("5") || st119.equals("6") ||  st119.equals("7") ||  st119.equals("8") ||  st119.equals("9") ) {
    		Util.cleanAndLockView(getActivity(),rgQI119A,rgQI119B);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}else{
    		MyUtil.LiberarMemoria();
			Util.lockView(getActivity(), false,rgQI119A,rgQI119B);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    public void ModificarvariableparaBD(){
    	if(!Util.esDiferente(individual.qi119a,9)){
    		individual.qi119a_o=txtQI119A_1.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119a,12)){
    		individual.qi119a_o=txtQI119A_2.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119b,9)){
    		individual.qi119b_o=txtQI119B_1.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119b,12)){
    		individual.qi119b_o=txtQI119B_2.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119c,9)){
    		individual.qi119c_o=txtQI119C_1.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119c,12)){
    		individual.qi119c_o=txtQI119C_2.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119d,3)){
    		individual.qi119d_o=txtQI119D_1.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119d,4)){
    		individual.qi119d_o=txtQI119D_2.getText().toString().trim();
    	}
    	if(!Util.esDiferente(individual.qi119d,8)){
    		individual.qi119d_o=txtQI119D_3.getText().toString().trim();
    	}
    	
    }
    
    public void ModificarvariableparaUsuario(){
    	if(!Util.esDiferente(individual.qi119a,9)){
    		txtQI119A_1.setText(individual.qi119a_o);
    	}
    	if(!Util.esDiferente(individual.qi119a,12)){
    		txtQI119A_2.setText(individual.qi119a_o);
    	}
    	if(!Util.esDiferente(individual.qi119b,9)){
    		txtQI119B_1.setText(individual.qi119b_o);
    	}
    	if(!Util.esDiferente(individual.qi119b,12)){
    		txtQI119B_2.setText(individual.qi119b_o);
    	}
    	if(!Util.esDiferente(individual.qi119c,9)){
    		txtQI119C_1.setText(individual.qi119c_o);
    	}
    	if(!Util.esDiferente(individual.qi119c,12)){
    		txtQI119C_2.setText(individual.qi119c_o);
    	}
    	if(!Util.esDiferente(individual.qi119d,3)){
    		txtQI119D_1.setText(individual.qi119d_o);
    	}
    	if(!Util.esDiferente(individual.qi119d,4)){
    		txtQI119D_2.setText(individual.qi119d_o);
    	}
    	if(!Util.esDiferente(individual.qi119d,8)){
    		txtQI119D_3.setText(individual.qi119d_o);
    	}
    	
    }
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI119A.readOnly();
    		rgQI119B.readOnly();
    		rgQI119C.readOnly();
    		rgQI119D.readOnly();
//    		txtQI119D_O.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
	
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}