package gob.inei.endes2024.fragment.CIseccion_01_03;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_004  extends FragmentForm {

	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI222;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI226;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI227;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI228;

	
	public String fechareferencia;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	public GridComponent2 gdMesesEmbarazo;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta221A,lblpregunta222,lblpregunta222_0,lblpregunta224,lblpregunta226,lblpregunta227,lblpregunta227_ind,lblmeses,lblpregunta228;
	LinearLayout q0,q1,q2,q3,q5;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargado2;
	public CISECCION_02Fragment_004() {}
	public CISECCION_02Fragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI227, 1, 9, null, 9);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI221A","QI222","QI224","QI226","QI226CONS","QI227","QI228","QI208","QI325A","QI320","QI316Y","QI315Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI221A","QI222","QI224","QI226","QI226CONS","QI227","QI228")};
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		return rootView;
  }
  
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta221A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_02qi221a);
		lblpregunta222 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi222);
		lblpregunta222_0 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_02qi222_0);
		lblpregunta224 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_02qi224);
		lblpregunta226 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi226);
		lblpregunta227 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi227);
		lblpregunta227_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_02qi227_ind);
		lblpregunta228 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi228);
		
		rgQI222=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi222_1,R.string.ciseccion_02qi222_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI226=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi226_1,R.string.ciseccion_02qi226_2,R.string.ciseccion_02qi226_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI226ChangeValue");
		txtQI227=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(1);
		lblmeses  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi227m).textSize(16).centrar();
		rgQI228=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi228_1,R.string.ciseccion_02qi228_2,R.string.ciseccion_02qi228_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		gdMesesEmbarazo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdMesesEmbarazo.addComponent(lblmeses);
		gdMesesEmbarazo.addComponent(txtQI227);
    }
  
    @Override
    protected View createUI(){
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta222,lblpregunta222_0,rgQI222);
		q2 = createQuestionSection(lblpregunta226,rgQI226);
		q3 = createQuestionSection(lblpregunta227,lblpregunta227_ind,gdMesesEmbarazo.component());
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta228,rgQI228);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q5);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi226!=null) {
			individual.qi226=individual.getConvert226(individual.qi226);
		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			
			if(individual.qi226 == null) {
				individual.qi226cons = null;
			} 
			else {
				if(fechareferencia == null) {
					individual.qi226cons =  Util.getFechaActualToString();
				}
				else {
					individual.qi226cons = fechareferencia;
				}			
			}
	
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi226 = individual.qi226;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (Util.esDiferente(individual.qi208,0)) {
			if (Util.esDiferente(individual.qi221a,2)) {
				if (Util.esVacio(individual.qi222)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI222");
					view = rgQI222;
					error = true;
					return false;
				}
				if (!Util.esDiferente(individual.qi222,1)) {
					mensaje = "INDAGUE Y CORRIJA SI ES NECESARIO";
					view = rgQI222;
					error = true;
					return false;
				}
			}
		}
		
		if (Util.esVacio(individual.qi226)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI226");
			view = rgQI226;
			error = true;
			return false;
		}
		if (individual.qi226==1) {
			if (Util.esVacio(individual.qi227)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI227");
				view = txtQI227;
				error = true;
				return false;
			}
			
			CISECCION_02 seccion2 =   getCuestionarioService().PrimerRegistroCap2(individual.id,individual.hogar_id,individual.persona_id);
			Integer meses=null;
			
			if (seccion2!=null) {			
			
		        Calendar fechadenacimiento = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1, Integer.parseInt(seccion2.qi215d));
				Calendar fecharef = new GregorianCalendar();
				
				if(fechareferencia!=null){
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date2 = null;
					try {
						date2 = df.parse(fechareferencia);
					} catch (ParseException e) {
						e.printStackTrace();
					}		
					Calendar cal = Calendar.getInstance();
					cal.setTime(date2);
					fecharef=cal;
				}	
		    	
				meses = MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef);
	    	}
			if(meses!=null && individual.qi227 >= meses) {
				mensaje = " Verificar, los meses ingresados no corresponden";
				view = txtQI227;
				error = true;
				return false;
			}
			
			if (Util.esVacio(individual.qi228)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI228");
				view = rgQI228;
				error = true;
				return false;
			}
		}
		return true;
    }
    @Override
    public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individual.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi315y=individual.qi315y;
    	App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
    	App.getInstance().getPersonaCuestionarioIndividual().qi325a=individual.qi325a;
    	
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi226!=null)	{
			individual.qi226=individual.setConvert226(individual.qi226);
		}
		
//		if(individual.qi208>0) {
//			detallevivos = getCuestionarioService().getListaNacimientos4abyPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado2);
//		} 
		entityToUI(individual);
		fechareferencia = individual.qi226cons;
		inicio();
    }
    
    public void validarPregunta208() {
 		if (!Util.esDiferente(individual.qi208,0)) {
 			Util.cleanAndLockView(getActivity(),rgQI222);
 			q1.setVisibility(View.GONE);
 		} else {
 			Util.lockView(getActivity(), false,rgQI222);
 			q1.setVisibility(View.VISIBLE);
 			onrgQI221AChangeValue();
 	    
 		}
     }
    
    private void inicio() {
    	validarPregunta208();
    	onrgQI226ChangeValue(); 
    	ValidarsiesSupervisora();
    	renombrarEtiquetaIntermedio();
    	txtCabecera.requestFocus();
    }
    public void renombrarEtiquetaIntermedio(){
    	lblpregunta224.setText(getResources().getString(R.string.ciseccion_02qi224));
    	lblpregunta224.setText(lblpregunta224.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
      	CISECCION_02 ninio=getCuestionarioService().getUltimoNacimientoidMayor(individual.id,individual.hogar_id,individual.persona_id,seccionesCargado2);
    	if (ninio!=null && ninio.qi212_nom!=null) {
    		lblpregunta222.setText(lblpregunta222.getText().toString().replace("(NOMBRE DEL �LTIMO NACIMIENTO)", ninio.qi212_nom));	
		}    	 
    }
    
    public void onrgQI221AChangeValue() {
		if (!Util.esDiferente(individual.qi221a,2)) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI222);
			q1.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI222);
			q1.setVisibility(View.VISIBLE);
			rgQI222.requestFocus();
		}
    }
    
    public void onrgQI226ChangeValue() {
    	Integer meses =0;
        CISECCION_02 seccion2 =   getCuestionarioService().PrimerRegistroCap2(individual.id,individual.hogar_id,individual.persona_id);
    	if(seccion2!=null){
        Calendar fechadenacimiento = new GregorianCalendar(seccion2.qi215y, Integer.parseInt(seccion2.qi215m)-1, Integer.parseInt(seccion2.qi215d));
		Calendar fecharef = new GregorianCalendar();
		
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}	
		meses = MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef);
		 	
    	}
		
		if (MyUtil.incluyeRango(2,8,rgQI226.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI227,rgQI228);
			q3.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			MyUtil.LiberarMemoria();
		} else {
			if(seccion2!=null){
				Integer data=Integer.parseInt(rgQI226.getTagSelected("0").toString());
				if(meses<1 && !Util.esDiferente(data, 1)) {
					String msg="Validar si esta embarazada, tiene menos de un mes de haber dado a luz";
					ToastMessage.msgBox(this.getActivity(), msg, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				}
			}	
			Util.lockView(getActivity(), false,txtQI227,rgQI228);
			q3.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			txtQI227.requestFocus();
		}
		
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI222.readOnly();
    		rgQI226.readOnly();
    		rgQI228.readOnly();
    		txtQI227.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi226!=null) {
			individual.qi226=individual.getConvert226(individual.qi226);
		}
	
		try {
			if(individual.qi226 == null) {
				individual.qi226cons = null;
			} 
			else {
				if(fechareferencia == null) {
					individual.qi226cons =  Util.getFechaActualToString();
				}
				else {
					individual.qi226cons = fechareferencia;
				}			
			}			
			
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi226 = individual.qi226;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return App.INDIVIDUAL;
	}
}