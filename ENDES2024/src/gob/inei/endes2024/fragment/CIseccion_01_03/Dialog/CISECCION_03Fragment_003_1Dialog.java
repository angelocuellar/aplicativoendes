package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_003;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;













































import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_003_1Dialog extends DialogFragmentComponent implements Respondible {
	
	@FieldAnnotation(orderIndex=1)
	public SpinnerField spnQIR_INICIAL;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQICOL1_O;
	@FieldAnnotation(orderIndex=3)
	public SpinnerField spnQIMES_INI;
	@FieldAnnotation(orderIndex=4)
	public SpinnerField spnQIANIO_INI;
	@FieldAnnotation(orderIndex=5)
	public SpinnerField spnQIMES_FIN;
	@FieldAnnotation(orderIndex=6)
	public SpinnerField spnQIANIO_FIN;
	@FieldAnnotation(orderIndex=7)
	public SpinnerField spnQICOL2;
	@FieldAnnotation(orderIndex=8)
	public TextField txtQICOL2_O;
	@FieldAnnotation(orderIndex=9)
	public SpinnerField spnQICOL3;
	@FieldAnnotation(orderIndex=10)
	public TextField txtQICOL3_O;
	@FieldAnnotation(orderIndex=11)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=12)
	public ButtonComponent btnCancelar;
	
	String resultadofinal="";
	public Integer tramometodouso=-1;
	
	private static CISECCION_03Fragment_003 caller;
	public CICALENDARIO_TRAMO_COL01_03 tramo;
	public GridComponent2 gridtodo;
	public CICALENDARIO_TRAMO_COL01_03 tramo1=null;
	public CICALENDARIO_TRAMO_COL01_03 tramo2=null;
	
	public CISECCION_01_03 individual=null;
	
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesCargadoTramos;
	private SeccionCapitulo[] seccionesGrabadoTramos,SeccionescargadoCI01_03,seccionesGrabadoCI01_03;
	LinearLayout q1;
	LinearLayout q2;
	private Calendar fecha_actual= null;//Calendar.getInstance();

	public LabelComponent lblpreguntareferencial1,lblpreguntareferencial2,lblpreguntareferencial3,lblpreguntareferencialcolumna2,lblpreguntareferencialcolumna3;
	
	private Integer mesfin=0;
	private Integer aniofinal=0;
	
	public static CISECCION_03Fragment_003_1Dialog newInstance(FragmentForm pagina,CICALENDARIO_TRAMO_COL01_03 tramoinicial, CICALENDARIO_TRAMO_COL01_03 detalle,CICALENDARIO_TRAMO_COL01_03 tramofinal) {
		caller = (CISECCION_03Fragment_003) pagina;
		CISECCION_03Fragment_003_1Dialog f = new CISECCION_03Fragment_003_1Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("tramo1", tramoinicial);
		args.putSerializable("detalle", detalle);
		args.putSerializable("tramo2", tramofinal);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tramo = (CICALENDARIO_TRAMO_COL01_03) getArguments().getSerializable("detalle");
		tramo1 = (CICALENDARIO_TRAMO_COL01_03) getArguments().getSerializable("tramo1");
		tramo2 = (CICALENDARIO_TRAMO_COL01_03) getArguments().getSerializable("tramo2");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().setTitle("CALENDARIO");
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	public CISECCION_03Fragment_003_1Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
		seccionesCargadoTramos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","QICOL1_O","QICOL2","QICOL2_O","QICOL3","QICOL3_O","QICANTIDAD","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabadoTramos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","QICOL1_O","QICOL2","QICOL2_O","QICOL3","QICOL3_O","QICANTIDAD")};
		SeccionescargadoCI01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QICAL_CONS","QI302_01","QI302_02","QI302_03","QI302_04","QI302_05","QI302_06","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI311_O","ID","HOGAR_ID","PERSONA_ID")}; 
		
	}
	protected View createUI() {
		buildFields();
		
		q1 = createQuestionSection(0,lblpreguntareferencial1,gridtodo.component());
		q2 = createButtonSection(btnAceptar,btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}
	@Override
	protected void buildFields() {
		  spnQIR_INICIAL = new SpinnerField(getActivity()).size(altoComponente+15, 750).callback("onQICOL1ChangeValue");
		  lblpreguntareferencial1 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.cicalendario_pregunta1);
		  lblpreguntareferencial2 = new LabelComponent(getActivity()).size(altoComponente,750).textSize(19).text(R.string.cicalendario_pregunta1_2);
		  lblpreguntareferencial3 = new LabelComponent(getActivity()).size(altoComponente,750).textSize(19).text(R.string.cicalendario_pregunta1_3);
		  lblpreguntareferencialcolumna2 = new LabelComponent(getActivity()).size(altoComponente+65,750).textSize(19);
		  lblpreguntareferencialcolumna3 = new LabelComponent(getActivity()).size(altoComponente+15,750).textSize(19);
		  spnQIMES_INI = new SpinnerField(getActivity()).size(altoComponente+15, 500);
		  spnQIANIO_INI = new SpinnerField(getActivity()).size(altoComponente+15, 250);
		  spnQIMES_FIN = new SpinnerField(getActivity()).size(altoComponente+15, 500);
		  spnQIANIO_FIN = new SpinnerField(getActivity()).size(altoComponente+15, 250);
		  txtQICOL1_O= new TextField(getActivity()).size(altoComponente, 750).callback("onQICOL1txtChangeValue");
		  spnQICOL2 = new SpinnerField(getActivity()).size(altoComponente+15, 750).callback("onQICOL2ChangeValue");
		  spnQICOL3 = new SpinnerField(getActivity()).size(altoComponente+15, 750).callback("onQICOL3ChangeValue");
		  btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(150, 55).text(R.string.btnGuardar);
		  btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(150, 55).text(R.string.btnCancelar);
		  btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CISECCION_03Fragment_003_1Dialog.this.dismiss();
			if(caller.CalendarioCompletado()){
				caller.btnIniciar.setEnabled(false);
				}
			else{
				caller.btnIniciar.setEnabled(true);
				}
			}
		});
		  btnAceptar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					boolean flag;
						flag=grabar();
						if(!flag){
							return;
						}
					caller.cargarTabla();
					CISECCION_03Fragment_003_1Dialog.this.dismiss();
					if(!caller.CalendarioCompletado()){
						caller.ExisteEspaciosEnBlancoEnElCalendarioTramo();
					}else{
						caller.btnIniciar.setEnabled(false);
					}
				}
				
			});
		  LlenarSpinerColumna02();
		  txtQICOL2_O= new TextField(getActivity()).size(altoComponente, 750).maxLength(1000);
		  txtQICOL3_O= new TextField(getActivity()).size(altoComponente, 750);
		  LlenarSpinerColumna03();
		  gridtodo = new GridComponent2(getActivity(), 2);
		  gridtodo.addComponent(spnQIR_INICIAL,2);
		  gridtodo.addComponent(txtQICOL1_O,2);
		  gridtodo.addComponent(lblpreguntareferencial2,2);
		  gridtodo.addComponent(spnQIMES_INI);
		  gridtodo.addComponent(spnQIANIO_INI);
		  gridtodo.addComponent(lblpreguntareferencial3,2);
		  gridtodo.addComponent(spnQIMES_FIN);
		  gridtodo.addComponent(spnQIANIO_FIN);
		  gridtodo.addComponent(lblpreguntareferencialcolumna2,2);
		  gridtodo.addComponent(spnQICOL2,2);
		  gridtodo.addComponent(txtQICOL2_O,2);
		  gridtodo.addComponent(lblpreguntareferencialcolumna3,2);
		  gridtodo.addComponent(spnQICOL3,2);
		  gridtodo.addComponent(txtQICOL3_O,2);
		 
	}

    public void RegistrarCuandoNoUsoNingunMetodoTramo(){
    	Integer Mesinicial=0;
    	Integer Mesfinal =0;
    	if(tramo2.qianio_fin!=App.ANIOPORDEFECTO && tramo2.qimes_fin!=App.MESPORDEFECTO){
    		Mesinicial = tramo2.qimes_fin+1;
    		if(Mesinicial==13){
    			Mesinicial=1;
    			tramo2.qianio_fin++;
    		}
    		Mesfinal = tramo1.qimes_ini-1;
    		if(Mesfinal==0){
    			Mesfinal = 12;
    			tramo1.qianio_ini--;
    		}
    	}
    	else{
    		if(!Util.esDiferente(tramo2.qimes_fin, App.MESPORDEFECTO) && !Util.esDiferente(tramo2.qianio_fin, App.ANIOPORDEFECTO)){
    		Mesinicial= !tramo2.qir_final.equals("")?tramo2.qimes_fin+1:tramo2.qimes_fin;
    		}else{
    			Mesinicial=Mesinicial==0?tramo2.qimes_fin+1:Mesinicial;
    		}
    			Mesfinal = tramo1.qimes_ini-1;
        		if(Mesfinal==0){
        			Mesfinal = 12;
        			tramo1.qianio_ini--;
        		}
        		if(Mesinicial==13){
        			Mesinicial=1;
        			tramo2.qianio_fin++;
        		}
    	}
    	tramo.qianio_ini = tramo2.qianio_fin;
    	tramo.qimes_ini = Mesinicial;
    	tramo.qimes_fin = Mesfinal;
    	tramo.qianio_fin = tramo1.qianio_ini;
    	tramo.qir_final = tramo. qir_inicial;
    	tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,Mesinicial)-MyUtil.DeterminarIndice(tramo.qianio_fin, Mesfinal);
    	tramo.qicantidad++;
    	try {
			getCuestionarioService().saveOrUpdateTRAMO(tramo, null, seccionesGrabadoTramos);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
	public void VerificarTramosAfectados(){
	List<CICALENDARIO_TRAMO_COL01_03> datos= new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
	datos= getCuestionarioService().getTramosAfectados(tramo,seccionesCargadoTramos);
		if(datos.size()>0){
			for(CICALENDARIO_TRAMO_COL01_03 t: datos){
			caller.detalles.remove(t);
			getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, tramo.id,tramo.hogar_id,tramo.persona_id, t.qitramo_id);
			}
		}
	}
    public void LimpiarEtiquetas(){
    	lblpreguntareferencial1.setText("");
    	lblpreguntareferencial2.setText("");
    	lblpreguntareferencial3.setText("");
    	lblpreguntareferencialcolumna2.setText("");
    	lblpreguntareferencialcolumna3.setText("");
    }
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
	public void cargarDatos() {
		tramo = getCuestionarioService().getTramoparaporTramoId(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, tramo.qitramo_id, seccionesCargadoTramos);
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, SeccionescargadoCI01_03);
		if(individual.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individual.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		if(tramo==null){
			tramo = new CICALENDARIO_TRAMO_COL01_03();
			tramo.id=App.getInstance().getPersonaCuestionarioIndividual().id;
			tramo.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			tramo.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			tramo.qitramo_id=getCuestionarioService().getUltimotramoidRegistrado(tramo.id, tramo.hogar_id, tramo.persona_id)+1;
		}
		aniofinal=tramo.qianio_fin;
		mesfin=tramo.qimes_fin;
		llenarMes(spnQIMES_INI);
    	llenarAnio(spnQIANIO_INI);
    	llenarMes(spnQIMES_FIN);
    	llenarAnio(spnQIANIO_FIN);
    	LlenarSpinerColumna01();
    	entityToUI(tramo);
    	inicio();
	}
	public void inicio(){
		onQICOL1ChangeValue();
    	onQICOL2ChangeValue();
    	onQICOL3ChangeValue();
		RenombrarPreguntas();
		if(ActualmenteUsoalgunMetodo()){
			EsconderPreguntasSiesqueUsaalgunMetodo();
		}
		ValidarsiesSupervisora();
	}

	public void onQICOL1txtChangeValue(){
		lblpreguntareferencialcolumna2.setText("�Por qu� dej� de usar (EL M�TODO)?�Dej� de utilizar (EL M�TODO) porque qued� embarazada mientras lo usaba, o dej� de usarlo para quedar embarazada, o por alguna otra raz�n?");
		lblpreguntareferencialcolumna2.setText(lblpreguntareferencialcolumna2.getText().toString().replace("(EL M�TODO)",getMethodSelected()));
		lblpreguntareferencialcolumna3.setText("�D�nde le recomendaron o informaron como usar m�todo?");
		lblpreguntareferencialcolumna3.setText(lblpreguntareferencialcolumna3.getText().toString().replace("m�todo",getMethodSelected()));
	}
	
	public boolean ActualmenteUsoalgunMetodo(){
		boolean retorna=false;
//		Log.e("","COL3: "+tramo.qicol3);
//		Log.e("","ANIO: "+tramo.qianio_fin);
//		Log.e("","MES: "+tramo.qimes_fin);
		if((tramo1!=null && !tramo1.qir_final.equals("0") && !tramo1.qir_final.equals("E") && !tramo1.qir_final.equals("T") && !tramo1.qir_final.equals("N")  && tramo1.qicol3==null && !Util.esDiferente(tramo1.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(tramo1.qimes_fin, fecha_actual.get(Calendar.MONTH)+1)) ||  (!Util.esDiferente(aniofinal, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(mesfin, fecha_actual.get(Calendar.MONTH)+1) && tramo.qicol3!=null)){
//		if(tramo1!=null && !tramo1.qir_final.equals("0") && !tramo1.qir_final.equals("E")  && !Util.esDiferente(tramo1.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(tramo1.qimes_fin, fecha_actual.get(Calendar.MONTH)+1)){
//			Log.e("","ENTRO: ");
		retorna=true;
		}
//		Log.e("","ENTRO: "+retorna);
		return retorna;
	}
	public void RenombrarPreguntas(){
		String cadena="";
//		if(tramo2.qianio_ini!=null && tramo2.qimes_ini!=null &&  !Util.esDiferente(tramo2.qianio_ini, App.ANIOPORDEFECTO) && !Util.esDiferente(tramo2.qimes_ini, App.MESPORDEFECTO) && tramo2.qir_final.equals("") && caller.CalendarioCompletado()){
		if(tramo2.qianio_ini!=null && tramo2.qimes_ini!=null &&  !Util.esDiferente(tramo2.qianio_ini, App.ANIOPORDEFECTO) && !Util.esDiferente(tramo2.qimes_ini, App.MESPORDEFECTO) && caller.CalendarioCompletado()){
			Log.e("","ENTRO: ");
			cadena = PreguntaIlustrativaPrimeraParte(tramo2.qir_final)+"("+MyUtil.Mes(tramo2.qimes_ini-1)+" , "+tramo2.qianio_ini +")";	
		}
		else{
			Log.e("","ENTRO ELSE: ");
			cadena = PreguntaIlustrativaPrimeraParte(tramo2.qir_final)+"("+MyUtil.Mes(tramo2.qimes_fin-1)+" , "+tramo2.qianio_fin +")";
		}
		lblpreguntareferencial1.setText(cadena+" y "+PreguntaIlustrativaSegundaParte(tramo1.qir_final)+"("+MyUtil.Mes(tramo1.qimes_ini-1)+" , "+tramo1.qianio_ini+" ) "+"�Us� o hizo algo para evitar salir embarazada?\n"+"�Qu� fue lo que hizo? �Qu� m�todo utiliz�?");
	}

	public void EsconderPreguntasSiesqueUsaalgunMetodo(){
		if(tramo1!=null && tramo1.qicol3==null && !Util.esDiferente(tramo1.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(tramo1.qimes_fin, fecha_actual.get(Calendar.MONTH)+1) ||  (!Util.esDiferente(tramo.qianio_fin, fecha_actual.get(Calendar.YEAR)) && !Util.esDiferente(tramo.qimes_fin, fecha_actual.get(Calendar.MONTH)+1) && tramo.qicol3!=null) ){
   		Util.cleanAndLockView(getActivity(), spnQIMES_INI,spnQIANIO_INI,spnQIMES_FIN,spnQIANIO_FIN,spnQIR_INICIAL,spnQICOL2,lblpreguntareferencialcolumna2);
		spnQIMES_INI.setVisibility(View.GONE);
		spnQIANIO_INI.setVisibility(View.GONE);
		spnQIMES_FIN.setVisibility(View.GONE);
		spnQIANIO_FIN.setVisibility(View.GONE);
		spnQIR_INICIAL.setVisibility(View.GONE);
		spnQICOL2.setVisibility(View.GONE);
		lblpreguntareferencialcolumna2.setVisibility(View.GONE);
		lblpreguntareferencial1.setVisibility(View.GONE);
		lblpreguntareferencial2.setVisibility(View.GONE);
		lblpreguntareferencial3.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(),false, spnQIMES_INI,spnQIANIO_INI,spnQIMES_FIN,spnQIANIO_FIN,spnQIR_INICIAL,spnQICOL2);
			spnQIMES_INI.setVisibility(View.VISIBLE);
			spnQIANIO_INI.setVisibility(View.VISIBLE);
			spnQIMES_FIN.setVisibility(View.VISIBLE);
			spnQIANIO_FIN.setVisibility(View.VISIBLE);
			spnQIR_INICIAL.setVisibility(View.VISIBLE);
			spnQICOL2.setVisibility(View.VISIBLE);
			lblpreguntareferencialcolumna2.setVisibility(View.VISIBLE);
			lblpreguntareferencial1.setVisibility(View.VISIBLE);
			lblpreguntareferencial2.setVisibility(View.VISIBLE);
			lblpreguntareferencial3.setVisibility(View.VISIBLE);
		}
	}
	
	public boolean validar(){
		if(!ActualmenteUsoalgunMetodo()){
			if(Util.esVacio(tramo.qir_inicial)){
					mensaje = "Debe seleccionar al menos un metodo";
					view = spnQIR_INICIAL;
					error = true;
					return false;
			}
		if(!"0".equals(tramo.qir_inicial)){
		if (Util.esVacio(tramo.qir_inicial)) {
			mensaje = "Debe seleccionar al menos un metodo";
			view = spnQIR_INICIAL;
			error = true;
			return false;
		}
		if("X".equals(tramo.qir_inicial)){
			if(Util.esVacio(tramo.qicol1_o)){
				mensaje = "Debe Ingresar especifique";
				view = txtQICOL1_O;
				error = true;
				return false;	
			}
		}
		if(Util.esVacio(tramo.qimes_ini)){
			mensaje = "Debe ingresar mes de inicio";
			view = spnQIMES_INI;
			error = true;
			return false;
		}
		if(Util.esVacio(tramo.qianio_ini)){
			mensaje = "Debe ingresar anio de inicio";
			view = spnQIANIO_INI;
			error = true;
			return false;
		}
		if(Util.esVacio(tramo.qimes_fin)){
			mensaje = "Debe ingresar mes final";
			view = spnQIMES_FIN;
			error = true;
			return false;
		}
		if(Util.esVacio(tramo.qianio_fin)){
			mensaje = "Debe ingresar anio final";
			view = spnQIANIO_FIN;
			error = true;
			return false;
		}
		
		if(Util.esVacio(tramo.qicol2)){
			mensaje = "Debe ingresar por qu� dej� de usarlo";
			view = spnQICOL2;
			error = true;
			return false;
		}
		if("X".equals(tramo.qicol2) || "4".equals(tramo.qicol2) || "5".equals(tramo.qicol2)){
			if(Util.esVacio(tramo.qicol2_o)){
				mensaje = "Debe Ingresar especifique";
				view = txtQICOL2_O;
				error = true;
				return false;
			}
		}
		if(Util.esVacio(tramo.qicol3)){
			mensaje = "Debe ingresar de d�nde obtuvo el m�todo";
			view = spnQICOL3;
			error = true;
			return false;
		}
		if("A".equals(tramo.qicol3) || "E".equals(tramo.qicol3) || "X".equals(tramo.qicol3)){
			if(Util.esVacio(tramo.qicol3_o)){
				mensaje = "Debe Ingresar especifique";
				view = txtQICOL3_O;
				error = true;
				return false;
			}
		}
	
//		if(tramo.qianio_ini<tramo2.qianio_fin){
//			mensaje = "El a�o de inicio no debe ser Menor a ==>"+tramo2.qianio_fin;
//			view = spnQIANIO_INI;
//			error = true;
//			return false;
//		}
		if(!Util.esDiferente(tramo.qianio_ini, tramo2.qianio_fin) && (tramo.qimes_ini<=tramo2.qimes_fin) && Util.esDiferente(tramo.qianio_ini, App.ANIOPORDEFECTO) && Util.esDiferente(tramo.qimes_ini, App.MESPORDEFECTO)){
			mensaje = "El mes de inicio no puede ser Menor o igual a "+MyUtil.Mes(tramo2.qimes_fin-1);
			view = spnQIMES_INI;
			error = true;
			return false;
		}
		if(tramo.qianio_ini>tramo1.qianio_ini){
			mensaje = "El a�o de inicio no debe ser Mayor a "+tramo1.qianio_ini;
			view = spnQIANIO_INI;
			error = true;
			return false;
		}
		if(!Util.esDiferente(tramo.qianio_ini, tramo1.qianio_ini) && (tramo.qimes_ini>=tramo1.qimes_ini)){
			mensaje ="El mes de inicio no puede ser Mayor o igual "+MyUtil.Mes(tramo1.qimes_ini-1);
			view = spnQIMES_INI;
			error = true;
			return false;
			}
		
//		if(tramo.qianio_fin<tramo2.qianio_fin){
//			mensaje = "El a�o final no debe ser Menor a "+tramo2.qianio_fin;
//			view = spnQIANIO_FIN;
//			error = true;
//			return false;
//		}
//		if(!Util.esDiferente(tramo.qianio_fin, tramo2.qianio_fin) && (tramo.qimes_fin<=tramo2.qimes_fin) && Util.esDiferente(tramo.qianio_fin, App.ANIOPORDEFECTO) && Util.esDiferente(tramo.qimes_fin, App.MESPORDEFECTO)){
		if(!Util.esDiferente(tramo.qianio_fin, tramo2.qianio_fin) && (tramo.qimes_fin<=tramo2.qimes_fin) && Util.esDiferente(tramo.qianio_fin, App.ANIOPORDEFECTO) && Util.esDiferente(tramo.qimes_fin, App.MESPORDEFECTO) &&  Util.esDiferente(tramo.qianio_ini, App.ANIOPORDEFECTO) && Util.esDiferente(tramo.qimes_ini, App.MESPORDEFECTO)){
			mensaje ="El mes final no puede ser Menor o igual "+MyUtil.Mes(tramo2.qimes_fin-1);
			view = spnQIMES_FIN;
			error = true;
			return false;
		}
		
		if(tramo.qianio_fin>tramo1.qianio_ini){
			mensaje = "El a�o final no debe ser Mayor a "+tramo1.qianio_ini;
			view = spnQIANIO_FIN;
			error = true;
			return false;
		}
		if(!Util.esDiferente(tramo.qianio_fin, tramo1.qianio_ini) && (tramo.qimes_fin>=tramo1.qimes_ini)){
			mensaje = "El mes final no puede ser Mayor o igual a "+MyUtil.Mes(tramo1.qimes_ini-1);
			view = spnQIMES_FIN;
			error = true;
			return false;
	    }
		if(tramo.qianio_ini>tramo.qianio_fin){
			mensaje = "El a�o de inicio no puede ser Mayor a "+tramo.qianio_fin;
			view = spnQIANIO_INI;
			error = true;
			return false;
		}
		if(!Util.esDiferente(tramo.qianio_ini, tramo.qianio_fin) && (tramo.qimes_fin<tramo.qimes_ini)){
			mensaje = "El mes de inicio no puede ser mayor al mes de inicio "+MyUtil.Mes(tramo.qimes_fin-1);
			view = spnQIMES_INI;
			error = true;
			return false;
		}
	 }
	}
		else{
			if(Util.esVacio(tramo.qicol3)){
				mensaje = "Debe ingresar de d�nde obtuvo el m�todo";
				view = spnQICOL3;
				error = true;
				return false;
			}
			if("A".equals(tramo.qicol3) || "E".equals(tramo.qicol3) || "X".equals(tramo.qicol3)){
				if(Util.esVacio(tramo.qicol3_o)){
					mensaje = "Debe Ingresar especifique";
					view = txtQICOL3_O;
					error = true;
					return false;
				}
			}
		}
		return true;
	}
	public String PreguntaIlustrativaSegundaParte(String columnasuperior){
    	String texto ="";
    	if(columnasuperior.equals("N")){
    		if(tramo1!=null){
    		texto=" Antes de quedar Embarazada de "+GetNombreDelNacido(tramo1.qitramo_id).toString()+" : ";
    		}
    		else{
    		texto=" Antes de quedar Embarazada de "+GetNombreDelNacido(tramo1.qitramo_id).toString()+" : ";
    		}
    	}
    	else if(columnasuperior.equals("0"))
    		texto="Antes de este mes ";
    	else if(columnasuperior.equals("T"))
    		texto=" Antes del Embarazo que termin� en p�rdida ";
    	else if(columnasuperior.equals("E"))
    		texto=" Antes del Embarazo";
    	else if(   columnasuperior.equals("1") || columnasuperior.equals("2") || columnasuperior.equals("3") || columnasuperior.equals("4")||columnasuperior.equals("5") ||columnasuperior.equals("6") ||columnasuperior.equals("7")
    			|| columnasuperior.equals("8") || columnasuperior.equals("9") || columnasuperior.equals("J") || columnasuperior.equals("K")||columnasuperior.equals("L") || columnasuperior.equals("M")||columnasuperior.equals("X"))
    		texto=" Antes de Usar "+CodigodeMetodo(columnasuperior);
    	return texto;
    }
    public String PreguntaIlustrativaPrimeraParte(String columnainferior){
    	String texto="";
    	if(columnainferior.equals("N")){
    		if(tramo2!=null){
    		texto="Despu�s del Nacimiento de "+ GetNombreDelNacido(tramo2.qitramo_id).toString()+" : ";
    		}
    		else{
    			texto="Despu�s del Nacimiento de "+ GetNombreDelNacido(tramo2.ninio_id).toString()+" : ";
    		}
    	}
    	else if(columnainferior.equals("0"))
    		texto="Desde caso(0)";
    	else if(columnainferior.equals("T"))
    		texto="Despu�s de la terminaci�n";
    	else if(columnainferior.equals("E"))
    		texto="Despu�s del Embarazo";
    	else if(   columnainferior.equals("1")||columnainferior.equals("2") ||columnainferior.equals("3")||columnainferior.equals("4") || columnainferior.equals("5")||columnainferior.equals("6") || columnainferior.equals("7")
    			||columnainferior.equals("8") || columnainferior.equals("9")||columnainferior.equals("J")|| columnainferior.equals("K")||columnainferior.equals("L") || columnainferior.equals("M")||columnainferior.equals("X"))
    		texto="Despu�s de Usar "+CodigodeMetodo(columnainferior);
    	else 
    		texto=" Desde ";
    	return texto;
    }
    public String GetNombreDelNacido(Integer tramo_id){
    	CICALENDARIO_TRAMO_COL01_03 tramoninio=new CICALENDARIO_TRAMO_COL01_03();
    	tramoninio=getCuestionarioService().getTramoparaporTramoId(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, tramo_id, seccionesCargadoTramos);
    	 return getCuestionarioService().getNombreDelNacido(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, tramoninio.ninio_id!=null?tramoninio.ninio_id:-1 ,seccionesCargado);
    }
    public String CodigodeMetodo(String metodo){
    	String metododescriptivo="";
    	if(metodo.equals("1"))
    		metododescriptivo="la ESTERILIZACI�N FEMENINA";
    	else if(metodo.equals("2"))
    		metododescriptivo="la ESTERILIZACI�N MASCULINA";
    	else if(metodo.equals("3"))
    		metododescriptivo="las P�LDORAS";
    	else if(metodo.equals("4"))
    		metododescriptivo="el DIU";
    	else if(metodo.equals("5"))
    		metododescriptivo="las INYECCIONES";
    	else if(metodo.equals("6"))
    		metododescriptivo="los IMPLANTES";
    	else if(metodo.equals("7"))
    		metododescriptivo="el CONDON";
    	else if(metodo.equals("8"))
    		metododescriptivo="el CONDON FEMENINO";
    	else if(metodo.equals("9"))
    		metododescriptivo="el ESPUMA/JALEA/�VULOS (VAGINALES)";
    	else  if(metodo.equals("J"))
    		metododescriptivo="la AMENORREA POR LACTANCIA (MELA)";
    	else if(metodo.equals("K"))
    		metododescriptivo="ABSTINENCIA PERI�DICA";
    	else if(metodo.equals("L"))
    		metododescriptivo="el RETIRO";
    	else if(metodo.equals("M"))
    		metododescriptivo="la ANTICONCEPCI�N ORAL DE EMERGENCIA";
    	else if(metodo.equals("X"))
    		metododescriptivo="OTRO";
    	
    	return metododescriptivo;
    }

	public boolean grabar(){
		uiToEntity(tramo);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!ActualmenteUsoalgunMetodo()){
				if(tramo.qir_inicial.equals("0")){
					RegistrarCuandoNoUsoNingunMetodoTramo();
				}
				else{
					tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin,tramo.qimes_fin);
					tramo.qicantidad++;
					tramo.qir_final=tramo.qir_inicial;
					if(!getCuestionarioService().saveOrUpdateTRAMO(tramo, null,seccionesGrabadoTramos)){
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					}
					if(tramo.qir_inicial.equals("1") && !Util.esDiferente(individual.qi302_01, 2)){
						individual.qi302_01=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_01")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos. 1", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("2") && !Util.esDiferente(individual.qi302_02, 2)){
						individual.qi302_02=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_02")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos. 2", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("3") && !Util.esDiferente(individual.qi302_03, 2)){
						individual.qi302_03=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_03")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos. 3", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("4") && !Util.esDiferente(individual.qi302_04, 2)){
						individual.qi302_04=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_04")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos. 4", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					
					/*if(tramo.qir_inicial.equals("5") && !Util.esDiferente(individual.qi302_05, 2)){
						individual.qi302_05=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_05")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos. 5", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}*/
					if(tramo.qir_inicial.equals("6") && !Util.esDiferente(individual.qi302_06, 2)){
						individual.qi302_06=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_06")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.6", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("7") && !Util.esDiferente(individual.qi302_07, 2)){
						individual.qi302_07=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_07")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.7", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("8") && !Util.esDiferente(individual.qi302_08, 2)){
						individual.qi302_08=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_08")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.8", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("9") && !Util.esDiferente(individual.qi302_09, 2)){
						individual.qi302_09=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_09")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.9", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("J") && !Util.esDiferente(individual.qi302_10, 2)){
						individual.qi302_10=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_10")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.10", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("K") && !Util.esDiferente(individual.qi302_11, 2)){
						individual.qi302_11=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_11")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.11", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("L") && !Util.esDiferente(individual.qi302_12, 2)){
						individual.qi302_12=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_12")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.12", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("M") && !Util.esDiferente(individual.qi302_13, 2)){
						individual.qi302_13=1;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_13")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.13", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					if(tramo.qir_inicial.equals("X") && !Util.esDiferente(individual.qi302_14, 2)){
						individual.qi302_14=1;
						individual.qi302o=tramo.qicol1_o;
						seccionesGrabadoCI01_03= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_14","QI302O")};
						if(!getCuestionarioService().saveOrUpdate(individual, seccionesGrabadoCI01_03)){
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados Metodos.14", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
						}	
					}
					
				}
			}
		else{
				tramo1.qicol3=spnQICOL3.getSelectedItemKey()+"";
				tramo1.qicol3_o = txtQICOL3_O.getValue()!=null?txtQICOL3_O.getText().toString():null;
				if(!getCuestionarioService().saveOrUpdateTRAMO(tramo1, null,seccionesGrabadoTramos)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				}
			}
		} catch (SQLException e) {
			// TODO: handle exception
		}
		mesfin=0;
		aniofinal=0;
		return true;
	}

   
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
	}
	 public void LlenarSpinerColumna01(){
    	 List<Object> keys = new ArrayList<Object>();
    	 String[] items1=null;
    	 if(!Util.esDiferente(individual.qi302_01, 2)){
	         keys.add(null);
	         keys.add("0");
	         keys.add("2");
	         keys.add("3");
	         keys.add("4");
	         keys.add("5");
	         keys.add("6");
	         keys.add("7");
	         keys.add("8");
	         keys.add("9");
	         keys.add("J");
	         keys.add("K");
	         keys.add("L");
	         keys.add("M");
	         keys.add("X");
	         items1 = new String[] { " --- Seleccione --- ","0 NING�N M�TODO","2 ESTERILIZACI�N MASCULINA","3 P�LDORAS","4 DIU","5 INYECCIONES",
	                         "6 IMPLANTES",
	                         "7 CONDON",
	                         "8 CONDON FEMENINO",
	                         "9 ESPUMA/JALEA/�VULOS (VAGINALES)",
	                         "J AMENORREA POR LACTANCIA (MELA)",
	                         "K ABSTINENCIA PERI�DICA",
	                         "L RETIRO",
	                         "M ANTICONCEPCI�N ORAL DE EMERGENCIA",
	                         "X OTRO:"
	         
	         };
    	 }
    	 else{
    		 keys.add(null);
	         keys.add("0");
	         keys.add("1");
	         keys.add("2");
	         keys.add("3");
	         keys.add("4");
	         keys.add("5");
	         keys.add("6");
	         keys.add("7");
	         keys.add("8");
	         keys.add("9");
	         keys.add("J");
	         keys.add("K");
	         keys.add("L");
	         keys.add("M");
	         keys.add("X");
	         items1 = new String[] { " --- Seleccione --- ","0 NING�N M�TODO","1 ESTERILIZACI�N FEMENINA","2 ESTERILIZACI�N MASCULINA","3 P�LDORAS","4 DIU","5 INYECCIONES",
	                         "6 IMPLANTES",
	                         "7 CONDON",
	                         "8 CONDON FEMENINO",
	                         "9 ESPUMA/JALEA/�VULOS (VAGINALES)",
	                         "J AMENORREA POR LACTANCIA (MELA)",
	                         "K ABSTINENCIA PERI�DICA",
	                         "L RETIRO",
	                         "M ANTICONCEPCI�N ORAL DE EMERGENCIA",
	                         "X OTRO:"
	         
	         };
    	 }
         ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                         getActivity(), R.layout.spinner_item, R.id.textview, items1);
         spnQIR_INICIAL.setAdapterWithKey(adapter, keys);
    }
    public void LlenarSpinerColumna03(){
    	 List<Object> keys = new ArrayList<Object>();
    	 keys.add(null);
    	 keys.add("1");
    	 keys.add("2");
    	 keys.add("3");
    	 keys.add("4");
    	 keys.add("5");
    	 keys.add("6");
    	 keys.add("7");
    	 keys.add("8");
    	 keys.add("A");
    	 keys.add("B");
    	 keys.add("C");
    	 keys.add("D");
    	 keys.add("E");
    	 keys.add("F");
    	 keys.add("G");
    	 keys.add("H");
    	 keys.add("I");
    	 keys.add("J");
    	 keys.add("K");
    	 keys.add("X");
    	 String[] items = new String[]{" ---Seleccione --- ",
    			 "1 HOSPITAL MINSA",
    			 "2 CENTRO DE SALUD MINSA",
    			 "3 PUESTO DE SALUD MINSA",
    			 "4 PROMOTOR DE SALUD MINSA",
    			 "5 HOSPITAL DE ESSALUD",
    			 "6 POLICLINICO/CENTRO/POSTA ESSALUD",
    			 "7 HOSPITAL/OTRO DE LAS FFAA Y PNP",
    			 "8 HOSPITAL /OTRO DE LA MUNICIPALIDAD",
    			 "A OTRO GOBIERNO",
    			 "B CL�NICA PARTICULAR",
    			 "C FARMACIA / BOTICA",
    			 "D CONSULTORIO M�DICO PARTICULAR",
    			 "E OTRO SECTOR PRIVADO",
    			 "F CL�NICA/POSTA DE ONG",
    			 "G PROMOTORES ONG",
    			 "H HOSPITAL/OTRO DE LA IGLESIA",
    			 "I TIENDA / SUPERMERCADO /HOSTAL",
    			 "J AMIGOS / PARIENTES",
    			 "K NADIE/SE AUTORECETO",
    			 "X OTRO"
    	 };
    	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                 getActivity(), R.layout.spinner_item, R.id.textview, items);
    	 spnQICOL3.setAdapterWithKey(adapter, keys);
    	 
    }
    public void LlenarSpinerColumna02(){
    	 List<Object> keys = new ArrayList<Object>();
    	 keys.add(null);
    	 keys.add("1");
    	 keys.add("2");
    	 keys.add("3");
    	 keys.add("4");
    	 keys.add("5");
    	 keys.add("6");
    	 keys.add("7");
    	 keys.add("8");
    	 keys.add("9");
    	 keys.add("C");
    	 keys.add("F");
    	 keys.add("A");
    	 keys.add("D");
    	 keys.add("X");
    	 keys.add("Z");
    	 String[] items = new String[]{" --- Seleccione ---","1 QU�DO EMBARAZADA MIENTRAS LO USABA","2 DESEABA QUEDAR EMBARAZADA","3 MARIDO DESAPROBO","4 EFECTOS COLATERALES",
    			 		"5 MOTIVOS DE SALUD",
    			 		"6 ACCESO/DISPONIBILIDAD",
    			 		"7 DESEABA M�TODO M�S EFECTIVO",
    			 		"8 USO INCONVENIENTE",
    			 		"9 SEXO POCO FRECUENTE/MARIDO AUSENTE",
    			 		"C COSTO",
    			 		"F FATALISTA",
    			 		"A DIFICIL QUEDAR EMBARAZADA/MENOPAUSIA",
    			 		"D DISOLUCI�N MARITAL/SEPARACI�N",
    			 		"X OTRO",
    			 		"Z NO SABE"
    			 		};
    	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                 getActivity(), R.layout.spinner_item, R.id.textview, items);
 spnQICOL2.setAdapterWithKey(adapter, keys);
    }
    public void llenarMes(SpinnerField spiner){
    	List<Object> keys = new ArrayList<Object>();
    	keys.add(null);
    	keys.add(1);
    	keys.add(2);
    	keys.add(3);
    	keys.add(4);
    	keys.add(5);
    	keys.add(6);
    	keys.add(7);
    	keys.add(8);
    	keys.add(9);
    	keys.add(10);
    	keys.add(11);
    	keys.add(12);
    	String[] items = new String[]{ "-- SELECCIONE --","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, R.id.textview, items);
    	spiner.setAdapterWithKey(adapter, keys);
    }
    public void llenarAnio(SpinnerField spineranio){
    	Calendar fecha = Calendar.getInstance();
    	List<Object> keys = new ArrayList<Object>();
    	keys.add(null);
    	keys.add(fecha.get(Calendar.YEAR));
    	keys.add(fecha.get(Calendar.YEAR)-1);
    	keys.add(fecha.get(Calendar.YEAR)-2);
    	keys.add(fecha.get(Calendar.YEAR)-3);
    	keys.add(fecha.get(Calendar.YEAR)-4);
    	keys.add(fecha.get(Calendar.YEAR)-5);
    	String[] items = new String[]{ "-- SELECCIONE-- ",fecha.get(Calendar.YEAR)+"",fecha.get(Calendar.YEAR)-1+"",fecha.get(Calendar.YEAR)-2+"",fecha.get(Calendar.YEAR)-3+"",fecha.get(Calendar.YEAR)-4+"",fecha.get(Calendar.YEAR)-5+""};
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, R.id.textview, items);
    	spineranio.setAdapterWithKey(adapter, keys);
    }
    public void onQICOL1ChangeValue(){
    	String codigo=spnQIR_INICIAL.getSelectedItemKey()!=null?spnQIR_INICIAL.getSelectedItemKey().toString():"";
    	lblpreguntareferencialcolumna2.setText("�Por qu� dej� de usar (EL M�TODO)?�Dej� de utilizar (EL M�TODO) porque qued� embarazada mientras lo usaba, o dej� de usarlo para quedar embarazada, o por alguna otra raz�n?");
    	if(ActualmenteUsoalgunMetodo()){
    		if(tramo1.qir_final.equals("J") || tramo1.qir_final.equals("K") || tramo1.qir_final.equals("L") || tramo1.qir_final.equals("X")){
				lblpreguntareferencialcolumna3.setText("�D�nde le recomendaron o informaron como usar m�todo?");
				lblpreguntareferencialcolumna3.setText(lblpreguntareferencialcolumna3.getText().toString().replace("m�todo",MetodoSegunCodigoMarcado()));
			} 
			else{
				lblpreguntareferencialcolumna3.setText("�D�nde obtuvo m�todo cu�ndo Ud. empez� a usarlo?");
				lblpreguntareferencialcolumna3.setText(lblpreguntareferencialcolumna3.getText().toString().replace("m�todo",MetodoSegunCodigoMarcado()));
			}
    	}else{
    		lblpreguntareferencialcolumna3.setText("�D�nde obtuvo el m�todo cu�ndo Ud. empez� a usarlo?�D�nde le recomendaron o informaron como usar el m�todo (ritmo,retiro,etc)?");
    	}
    	if("0".equals(codigo)){
    		Util.cleanAndLockView(getActivity(), lblpreguntareferencial2,lblpreguntareferencial3,lblpreguntareferencialcolumna2,lblpreguntareferencialcolumna3,spnQIMES_INI,spnQIMES_FIN,spnQIANIO_FIN,spnQIANIO_INI,spnQICOL2,spnQICOL3);
    		lblpreguntareferencial2.setVisibility(View.GONE);
    		lblpreguntareferencial3.setVisibility(View.GONE);
    		lblpreguntareferencialcolumna2.setVisibility(View.GONE);
    		lblpreguntareferencialcolumna3.setVisibility(View.GONE);
    		spnQIMES_INI.setVisibility(View.GONE);
    		spnQIMES_FIN.setVisibility(View.GONE);
    		spnQIANIO_FIN.setVisibility(View.GONE);
    		spnQIANIO_INI.setVisibility(View.GONE);
    		spnQICOL2.setVisibility(View.GONE);
    		spnQIMES_FIN.setVisibility(View.GONE);
    		spnQICOL3.setVisibility(View.GONE);
    		btnAceptar.requestFocus();
    	}
    	else {
    		Util.lockView(getActivity(), false,lblpreguntareferencial2,lblpreguntareferencial3,lblpreguntareferencialcolumna2,lblpreguntareferencialcolumna3,spnQIMES_INI,spnQIMES_FIN,spnQIANIO_FIN,spnQIANIO_INI,spnQICOL2,spnQICOL3);
    		lblpreguntareferencial2.setVisibility(View.VISIBLE);
    		lblpreguntareferencial3.setVisibility(View.VISIBLE);
    		lblpreguntareferencialcolumna2.setVisibility(View.VISIBLE);
    		lblpreguntareferencialcolumna3.setVisibility(View.VISIBLE);
    		spnQIMES_INI.setVisibility(View.VISIBLE);
    		spnQIMES_FIN.setVisibility(View.VISIBLE);
    		spnQIANIO_FIN.setVisibility(View.VISIBLE);
    		spnQIANIO_INI.setVisibility(View.VISIBLE);
    		spnQICOL2.setVisibility(View.VISIBLE);
    		spnQIMES_FIN.setVisibility(View.VISIBLE);
    		spnQICOL3.setVisibility(View.VISIBLE);
    		spnQIMES_INI.requestFocus();
    		if(!"".equals(codigo)){
    			lblpreguntareferencialcolumna2.setText(lblpreguntareferencialcolumna2.getText().toString().replace("(EL M�TODO)",getMethodSelected()));
    			if(codigo.equals("J") || codigo.equals("K") || codigo.equals("L") || codigo.equals("X")){
    				lblpreguntareferencialcolumna3.setText("�D�nde le recomendaron o informaron como usar m�todo?");
    				lblpreguntareferencialcolumna3.setText(lblpreguntareferencialcolumna3.getText().toString().replace("m�todo",getMethodSelected()));
    			}
    			else{
    				lblpreguntareferencialcolumna3.setText("�D�nde obtuvo m�todo cu�ndo Ud. empez� a usarlo?");
    				lblpreguntareferencialcolumna3.setText(lblpreguntareferencialcolumna3.getText().toString().replace("m�todo",getMethodSelected()));
    			}
    		}
    	}
    	if(!codigo.equals("X")){
    		Util.cleanAndLockView(getActivity(), txtQICOL1_O);
    		txtQICOL1_O.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(), false,txtQICOL1_O);
    		txtQICOL1_O.setVisibility(View.VISIBLE);
    	}
    }
    public String getMethodSelected(){
    	String method="";
    	String articulo="";
    	if(spnQIR_INICIAL.getSelectedItemKey().equals("X")){
    		method=txtQICOL1_O.getText().toString();
    	}
    	else{
    		if(spnQIR_INICIAL.getSelectedItemKey().equals("1")|| spnQIR_INICIAL.getSelectedItemKey().equals("2")|| 
    		   spnQIR_INICIAL.getSelectedItemKey().equals("9")|| spnQIR_INICIAL.getSelectedItemKey().equals("J")||
    		   spnQIR_INICIAL.getSelectedItemKey().equals("K")|| spnQIR_INICIAL.getSelectedItemKey().equals("M"))   			
    		 articulo="la ";
    		if(spnQIR_INICIAL.getSelectedItemKey().equals("3"))    			
       		 articulo="las ";
    		if(spnQIR_INICIAL.getSelectedItemKey().equals("4")|| spnQIR_INICIAL.getSelectedItemKey().equals("7")|| 
    		   spnQIR_INICIAL.getSelectedItemKey().equals("8")|| spnQIR_INICIAL.getSelectedItemKey().equals("L"))    			
       		 articulo="el ";
    		if(spnQIR_INICIAL.getSelectedItemKey().equals("5"))    			
       		 articulo="las ";
    		if(spnQIR_INICIAL.getSelectedItemKey().equals("6"))    			
       		 articulo="los ";
    		
    		method=articulo+spnQIR_INICIAL.getSelectedItem().toString().substring(1,spnQIR_INICIAL.getSelectedItem().toString().length());	
    	}
    	return method;
    }
    
    public String MetodoSegunCodigoMarcado(){
    	if(tramo1.qir_final.equals("1")){
    		return " la ESTERILIZACI�N FEMENINA";
    	}
    	if(tramo1.qir_final.equals("2")){
    		return "la ESTERILIZACI�N MASCULINA";
    	}
    	if(tramo1.qir_final.equals("3")){
    		return "las P�LDORAS";
    	}
    	if(tramo1.qir_final.equals("4")){
    		return 	"el DIU";
    	}
    	if(tramo1.qir_final.equals("5")){
    		return "las INYECCIONES";
    	}
    	if(tramo1.qir_final.equals("6")){
    		return "los IMPLANTES";
    	}
    	if(tramo1.qir_final.equals("7")){
    		return "el CONDON";
    	}
    	if(tramo1.qir_final.equals("8")){
    		   return "el CONDON FEMENINO";
    	}
    	if(tramo1.qir_final.equals("9")){
    		return "la ESPUMA/JALEA/�VULOS (VAGINALES)";
    	}
    	if(tramo1.qir_final.equals("J")){
    		return "la AMENORREA POR LACTANCIA (MELA)";
    	}
    	if(tramo1.qir_final.equals("K")){
    		return "la ABSTINENCIA PERI�DICA";
    	}
    	if(tramo1.qir_final.equals("L")){
    		return "el RETIRO"; 
    	}
    	if(tramo1.qir_final.equals("M")){
    		return "la ANTICONCEPCI�N ORAL DE EMERGENCIA";
    	}
    	if(tramo1.qir_final.equals("X")){
    		return individual.qi311_o;
    	}
    	else{
            return "";
    	}
    }
    public void onQICOL2ChangeValue(){
    	String codigo=spnQICOL2.getSelectedItemKey()!=null?spnQICOL2.getSelectedItemKey().toString():"";
    	if(!codigo.equals("X") && !codigo.equals("4") && !codigo.equals("5")){
    		Util.cleanAndLockView(getActivity(), txtQICOL2_O);
    		txtQICOL2_O.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(), false,txtQICOL2_O);
    		txtQICOL2_O.setVisibility(View.VISIBLE);
    	}
    }
    public void onQICOL3ChangeValue(){
    	String codigo =spnQICOL3.getSelectedItemKey()!=null?spnQICOL3.getSelectedItemKey().toString():"";
    	if(!codigo.equals("A") && !codigo.equals("E") && !codigo.equals("X")){
    		Util.cleanAndLockView(getActivity(), txtQICOL3_O);
    		txtQICOL3_O.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(), false,txtQICOL3_O);
    		txtQICOL3_O.setVisibility(View.VISIBLE);
    	}
    }
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		spnQIR_INICIAL.readOnly();
    		spnQIMES_INI.readOnly();
    		spnQIANIO_INI.readOnly();
    		spnQIMES_FIN.readOnly();
    		spnQIANIO_FIN.readOnly();
    		spnQICOL2.readOnly();
    		spnQICOL3.readOnly();
    		btnAceptar.setEnabled(false);
    	}
    }
}
