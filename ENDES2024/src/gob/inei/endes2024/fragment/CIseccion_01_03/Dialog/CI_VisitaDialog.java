package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_000;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
//aaa bbb
public class CI_VisitaDialog extends DialogFragmentComponent {
	@FieldAnnotation(orderIndex = 1)
	public DateTimeField txtFECHA_INICIO;
	@FieldAnnotation(orderIndex = 2)
	public DateTimeField txtHORA_INICIO;
	@FieldAnnotation(orderIndex = 3)
	public DateTimeField txtHORA_FIN;
	@FieldAnnotation(orderIndex = 4)
	public SpinnerField spnQSVRESUL;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtQSVRESUL_O;
	
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQD333_1;
	@FieldAnnotation(orderIndex = 7)
	public RadioGroupOtherField rgQD333_2;
	@FieldAnnotation(orderIndex = 8)
	public RadioGroupOtherField rgQD333_3;
	@FieldAnnotation(orderIndex = 9)
	public RadioGroupOtherField rgQD333_4;
	@FieldAnnotation(orderIndex = 10)
	public RadioGroupOtherField rgQD333_5;
	@FieldAnnotation(orderIndex = 11)
	public RadioGroupOtherField rgQD333_6;
		
	@FieldAnnotation(orderIndex = 12)
	public DateTimeField txtFECHA_PROXIMA;
	@FieldAnnotation(orderIndex = 13)
	public DateTimeField txtHORA_PROXIMA;
	
	public LabelComponent lbl26a1, lbl26a2, lbl26a3, lbl26a4, lbl26a5, lbl26a6,lbllimitaciones;
	public GridComponent2 gridPreguntas26;
	
	public List<Integer> resul_1;
	public List<Integer> resul_fila;

	public ButtonComponent btnCancelar,btnAceptar,btnGPS,btnVerenMapa;
	private CuestionarioService service;
	CARATULA_INDIVIDUAL bean;
	private LinearLayout q1,q2,q3,q4;
	private LabelComponent lblblanco,lblblanco1,lblBlanco3, lblCordenadas1,lblCordenadas2,lblCordenadas3,lblFecha,lblHoraIni,lblHoraFin,lblResutado,lbltitulo_proxima,lblfechaproxima,lblhoraproxima;
	
	private static FragmentForm caller;
	private GridComponent grid1,grid2, grproxvisita;
	private GridComponent2 tblGps;
	private boolean esResultado3 = false;
	private DISCAPACIDAD modelo;
	SeccionCapitulo[] seccionesGrabado,seccionesCargado,seccionesGrabadoVisitaSalud,seccionesGrabadoDiscapacidad;

	public Seccion01 persona; 
	public static enum ACTION {
		INICIAR, EDITAR, FINALIZAR
	};

	private ACTION action;
	private VisitaService visitaService;
	
	public CI_VisitaDialog() {
	}

	public interface C1_Cap00Fragment_001_01Listener {
		void onFinishEditDialog(String inputText);
	}

	public static CI_VisitaDialog newInstance(FragmentForm pagina, CARATULA_INDIVIDUAL bean) {
		return newInstance(pagina, bean, ACTION.INICIAR);
	}

	public static CI_VisitaDialog newInstance(FragmentForm pagina, CARATULA_INDIVIDUAL bean,ACTION action) {
		caller = pagina;
		CI_VisitaDialog f = new CI_VisitaDialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("bean", bean);
		args.putSerializable("action", action);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CARATULA_INDIVIDUAL) getArguments().getSerializable("bean");
		action = (ACTION) getArguments().getSerializable("action");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas(this);
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINTUO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","QSRESULT","QSRESULT_O","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };

         seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI","QSVHORA_FIN","QSVMINTUO_FIN","QSVRESUL","QSVRESUL_O","QSPROX_DIA",
				"QSPROX_MES","QSPROX_ANIO","QSPROX_HORA","QSPROX_MINUTO","QSRESULT","QSRESULT_O","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
         
         seccionesGrabadoVisitaSalud = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"QSVRESUL","QSRESULT","QSRESULT_O","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI")};
//         SeccionesCargadoPersona= new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QH02_1","QH07")};
         seccionesGrabadoDiscapacidad = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
		return rootView;
	}


	@Override
	protected void buildFields() {		
		
		lblFecha = new LabelComponent(getActivity()).size(altoComponente, 100).negrita().text(R.string.v_l_fecha_ini);
		lblHoraIni = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_ini);
		txtFECHA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 180).readOnly();
		txtFECHA_INICIO.setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
		txtHORA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();
		
		grid1 = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
		grid1.addComponent(lblFecha);
		grid1.addComponent(txtFECHA_INICIO);
		grid1.addComponent(lblHoraIni);
		grid1.addComponent(txtHORA_INICIO);
		
		lblHoraFin = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_fin);
		lblResutado = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_resultado);		
		lblblanco = new LabelComponent(this.getActivity()).size(altoComponente, 150);
        lblblanco1 = new LabelComponent(this.getActivity()).size(altoComponente, 450);
        txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();
		txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100);
		txtQSVRESUL_O = new TextField(getActivity()).size(altoComponente, 550).maxLength(100).hint(R.string.especifique).soloTexto();
		spnQSVRESUL = new SpinnerField(getActivity()).size(altoComponente + 15,550).callback("onC1_RVISITAChangeValue");
		cargarSpinner();
		
		grid2 = new GridComponent(getActivity(), Gravity.CENTER, 3, 0);
		grid2.addComponent(lblHoraFin);
		grid2.addComponent(txtHORA_FIN);
		grid2.addComponent(lblblanco1);
		grid2.addComponent(lblResutado);
		grid2.addComponent(spnQSVRESUL,2);
		grid2.addComponent(lblblanco);
		grid2.addComponent(txtQSVRESUL_O, 2);
		
	
		lbl26a1 = new LabelComponent(this.getActivity()).size(altoComponente, 450).text(R.string.seccion01qh26a1).textSize(18).alinearIzquierda();
		rgQD333_1 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a1_1, R.string.seccion01qh26a1_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a2 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a2).textSize(18).alinearIzquierda();
		rgQD333_2 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a2_1, R.string.seccion01qh26a2_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a3 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a3).textSize(18).alinearIzquierda();
		rgQD333_3 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a3_1, R.string.seccion01qh26a3_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a4 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a4).textSize(18).alinearIzquierda();
		rgQD333_4 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a4_1, R.string.seccion01qh26a4_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a5 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a5).textSize(18).alinearIzquierda();
		rgQD333_5 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a5_1, R.string.seccion01qh26a5_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a6 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a6).textSize(18).alinearIzquierda();
		rgQD333_6 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a6_1, R.string.seccion01qh26a6_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		lbllimitaciones = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh26);
		
		gridPreguntas26 = new GridComponent2(this.getActivity(), App.ESTILO, 2);
		gridPreguntas26.addComponent(lbl26a1);
		gridPreguntas26.addComponent(rgQD333_1);
		gridPreguntas26.addComponent(lbl26a2);
		gridPreguntas26.addComponent(rgQD333_2);
		gridPreguntas26.addComponent(lbl26a3);
		gridPreguntas26.addComponent(rgQD333_3);
		gridPreguntas26.addComponent(lbl26a4);
		gridPreguntas26.addComponent(rgQD333_4);
		gridPreguntas26.addComponent(lbl26a5);
		gridPreguntas26.addComponent(rgQD333_5);
		gridPreguntas26.addComponent(lbl26a6);
		gridPreguntas26.addComponent(rgQD333_6);
		
			
		
		
		lblfechaproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_fecha_prox);
		lblhoraproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_hora_prox);
		txtFECHA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 180).setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
		txtHORA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100);
		
		grproxvisita = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
		grproxvisita.addComponent(lblfechaproxima);
		grproxvisita.addComponent(txtFECHA_PROXIMA);
		grproxvisita.addComponent(lblhoraproxima);
		grproxvisita.addComponent(txtHORA_PROXIMA);
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				CI_VisitaDialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				 App.getInstance().setC1VisitaI(bean);
				((CISECCION_01Fragment_000) caller).recargarLista();
				 CI_VisitaDialog.this.dismiss();
			}
		});
		

	}
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(R.string.pregunta01, grid1.component());
		q2 = createQuestionSection(grid2.component());
		q3 = createQuestionSection(lbllimitaciones,gridPreguntas26.component());
		q4 = createQuestionSection(R.string.v_l_proxima_visita, grproxvisita.component());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(botones);
		return contenedor;
	}
	
	private boolean grabar() {
		uiToEntity(bean);
		if (action == ACTION.INICIAR) {
			bean.qivanio = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
			bean.qivdia = Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "dd");
			bean.qivmes = Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "MM");
			bean.qivhora = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "HH");
			bean.qivmin = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "mm");
			
		} 
		else if (action == ACTION.FINALIZAR) {
			if (spnQSVRESUL.getSelectedItemKey() == null || spnQSVRESUL.getSelectedItemKey().toString().equals("0")) {
				spnQSVRESUL.requestFocus();
				ToastMessage.msgBox(this.getActivity(), "Seleccione un resultado.", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return false;
			}
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("7")) {
				if (Util.esVacio(txtQSVRESUL_O)) {
					txtQSVRESUL_O.requestFocus();
					ToastMessage.msgBox(this.getActivity(), "Otro no puede estar vacia.", ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					return false;
				} 
				bean.qivresul_o=txtQSVRESUL_O.getText().toString();
			}
			
			bean.qivresul = spnQSVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQSVRESUL.getSelectedItemKey();
			bean.qivhora_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
			bean.qivmin_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");
			
			bean.qivhora_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivhora_fin);
			bean.qivmin_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivmin_fin);
			
			if (	   spnQSVRESUL.getSelectedItemKey().toString().equals("2")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")) {
				if (!validarProximaVisita(Integer.parseInt(spnQSVRESUL.getSelectedItemKey().toString()))) {
					return false;
				}
				if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
					bean.qiprox_hora = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
					bean.qiprox_minuto = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
					bean.qiprox_dia = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
					bean.qiprox_mes = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
					bean.qiprox_anio = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "yyyy");
				}
			}
		}
		
		else if (action == ACTION.EDITAR) {
			if (spnQSVRESUL.getSelectedItemKey() == null || spnQSVRESUL.getSelectedItemKey().toString().equals("0")) {
				spnQSVRESUL.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Seleccione un resultado.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				return false;
			}
			bean.qivresul = spnQSVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQSVRESUL.getSelectedItemKey();
			if (spnQSVRESUL.getSelectedItemKey().toString().equals("7")) {
				if (Util.esVacio(txtQSVRESUL_O)) {
					txtQSVRESUL_O.requestFocus();
					ToastMessage.msgBox(this.getActivity(), "Otro no puede estar vacia.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return false;
				}
				bean.qivresul_o=txtQSVRESUL_O.getText().toString();
			}
			if (	   spnQSVRESUL.getSelectedItemKey().toString().equals("2")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
					|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")) {
				
				if (!validarProximaVisita(Integer.parseInt(spnQSVRESUL.getSelectedItemKey().toString()))) {
					return false;
				}
				if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
					bean.qiprox_hora = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
					bean.qiprox_minuto = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
					bean.qiprox_dia = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
					bean.qiprox_mes = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
					bean.qiprox_anio = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "yyyy");
				}
			}
			if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
				bean.qivresul = spnQSVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQSVRESUL.getSelectedItemKey();
				bean.qiprox_hora =Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
				bean.qiprox_minuto = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");
			}
			

		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if (!getVisitaService().saveOrUpdate(bean,"QIVDIA","QIVMES","QIVANIO","QIVHORA","QIVMIN","QIVHORA_FIN","QIVMIN_FIN","QIVENTREV","QIVRESUL","QIVRESUL_O","QIPROX_DIA","QIPROX_MES","QIPROX_ANIO","QIPROX_HORA","QIPROX_MINUTO","ID", "HOGAR_ID","PERSONA_ID")) {
				ToastMessage.msgBox(this.getActivity(), "Los datos no fueron grabados",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
			if(App.getInstance().getPersonaSeccion01()!= null && App.getInstance().getPersonaSeccion01().persona_id!=null && !Util.esDiferente(App.getInstance().getPersonaSeccion01().persona_id,bean.persona_id)){
				if(Util.esDiferente(bean.qivresul, 1,3,5)){
					if(!getVisitaService().saveOrUpdate(CrearVisitaSalud(),seccionesGrabadoVisitaSalud)){
						ToastMessage.msgBox(this.getActivity(), "Los datos no fueron grabados visita salud",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					}
				}
			}
			
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(), ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		try {
			if(bean!=null && bean.qivresul==6){
				getService().saveOrUpdate_Discapacidad(Discapacidad(), seccionesGrabadoDiscapacidad);
				getService().saveOrUpdate_Discapacidad(DiscapacidadSalud(), seccionesGrabadoDiscapacidad);
			}
			else{
				getService().BorrarDiscapacidad(bean.id, bean.hogar_id, bean.persona_id, App.CUEST_ID_CARA_INDIVIDUAL, 0);
				getService().BorrarDiscapacidad(bean.id, bean.hogar_id, bean.persona_id, App.CUEST_ID_CARA_SALUD, 0);
			}
		} catch (SQLException e) {
			// TODO: handle exception
		}
		return true;
	}
	private DISCAPACIDAD  Discapacidad(){
		DISCAPACIDAD nuevadiscapacidad;
		nuevadiscapacidad= new DISCAPACIDAD();
		nuevadiscapacidad.id=bean.id;
		nuevadiscapacidad.hogar_id=bean.hogar_id;
		nuevadiscapacidad.cuestionario_id=App.CUEST_ID_CARA_INDIVIDUAL;
		nuevadiscapacidad.persona_id=bean.persona_id;
		nuevadiscapacidad.ninio_id=0;
		nuevadiscapacidad.qd333_1=Integer.parseInt(rgQD333_1.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_2=Integer.parseInt(rgQD333_2.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_3=Integer.parseInt(rgQD333_3.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_4=Integer.parseInt(rgQD333_4.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_5=Integer.parseInt(rgQD333_5.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_6=Integer.parseInt(rgQD333_6.getTagSelected("0").toString());
		return nuevadiscapacidad;
	}
	private DISCAPACIDAD  DiscapacidadSalud(){
		DISCAPACIDAD nuevadiscapacidad;
		nuevadiscapacidad= new DISCAPACIDAD();
		nuevadiscapacidad.id=bean.id;
		nuevadiscapacidad.hogar_id=bean.hogar_id;
		nuevadiscapacidad.cuestionario_id=App.CUEST_ID_CARA_SALUD;
		nuevadiscapacidad.persona_id=bean.persona_id;
		nuevadiscapacidad.ninio_id=0;
		nuevadiscapacidad.qd333_1=Integer.parseInt(rgQD333_1.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_2=Integer.parseInt(rgQD333_2.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_3=Integer.parseInt(rgQD333_3.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_4=Integer.parseInt(rgQD333_4.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_5=Integer.parseInt(rgQD333_5.getTagSelected("0").toString());
		nuevadiscapacidad.qd333_6=Integer.parseInt(rgQD333_6.getTagSelected("0").toString());
		return nuevadiscapacidad;
	}
	private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		Log.e("","BEAN: "+bean.qivresul);
		if (bean.qivresul!=null && bean.qivresul==3 && Util.esVacio(bean.qiprox_dia)) {
			mensaje = preguntaVacia.replace("$", "Fecha prox. ");
			view = txtFECHA_PROXIMA;
			error = true;
			return false;
		}
		if (bean.qivresul!=null && bean.qivresul==3 && Util.esVacio(bean.qiprox_hora)) {
			mensaje = preguntaVacia.replace("$", "Hora prox. ");
			view = txtHORA_PROXIMA;
			error = true;
			return false;
		}
		if(bean.qivresul!=null && bean.qivresul==6){
			Integer valor=Integer.parseInt(rgQD333_1.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P01 Discapacidad");
				view = rgQD333_1;
				error = true;
				return false;	
			}
			valor=Integer.parseInt(rgQD333_2.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P02 Discapacidad");
				view = rgQD333_2;
				error = true;
				return false;	
			}
			valor=Integer.parseInt(rgQD333_3.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P03 Discapacidad");
				view = rgQD333_3;
				error = true;
				return false;	
			}
			valor=Integer.parseInt(rgQD333_4.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P04 Discapacidad");
				view = rgQD333_4;
				error = true;
				return false;	
			}
			valor=Integer.parseInt(rgQD333_5.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P05 Discapacidad");
				view = rgQD333_5;
				error = true;
				return false;	
			}
			valor=Integer.parseInt(rgQD333_6.getTagSelected("0").toString());
			if(!Util.esDiferente(valor, 0)){
				mensaje = preguntaVacia.replace("$", "P06 Discapacidad");
				view = rgQD333_6;
				error = true;
				return false;	
			}
		}
		return true;
	}
	
	private boolean validarProximaVisita(Integer item) {
		if (txtFECHA_PROXIMA.getValue() == null || txtHORA_PROXIMA.getValue() == null && Util.esDiferente(item, 3)) {
			return true;
		}
		if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) > 0) {
			txtFECHA_PROXIMA.requestFocus();
			ToastMessage.msgBox(this.getActivity(),"Fecha Proxima no puede ser menor.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} else if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) == 0) {
			if (Util.compareTime((Date) txtHORA_FIN.getValue(),(Date) txtHORA_PROXIMA.getValue()) >= 0) {
				txtHORA_PROXIMA.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Hora Proxima no puede ser menor o igual.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return false;
			}
		}
		return true;
	}

	private void cargarDatos() {
		getDialog().setTitle("Cuestionario Individual - Visita N� " + bean.nro_visita);
		entityToUI(bean);
		modelo = getService().getSeccion02IndividualParaDiscapacidad(bean.id,bean.hogar_id, Integer.valueOf(App.CUEST_ID_CARA_INDIVIDUAL),bean.persona_id,0, seccionesCargado);
		if(modelo!=null && bean!=null && bean.qivresul!=null && bean.qivresul==6){
			rgQD333_1.setTagSelected(modelo.qd333_1);
			rgQD333_2.setTagSelected(modelo.qd333_2);
			rgQD333_3.setTagSelected(modelo.qd333_3);
			rgQD333_4.setTagSelected(modelo.qd333_4);
			rgQD333_5.setTagSelected(modelo.qd333_5);
			rgQD333_6.setTagSelected(modelo.qd333_6);
		}
		caretaker.addMemento("antes", bean.saveToMemento(CARATULA_INDIVIDUAL.class));
		
			if (action == ACTION.INICIAR) {
				txtFECHA_INICIO.setValue(Calendar.getInstance().getTime());
				txtHORA_INICIO.setValue(Calendar.getInstance().getTime());
			} else if (action == ACTION.FINALIZAR) {
				if (bean.qivmin != null && bean.qivhora!=null) {
					Date fecha = Util.getFechaHora(bean.qivanio+"", bean.qivmes,bean.qivdia, bean.qivhora , bean.qivmin, 0+"");
					txtFECHA_INICIO.setValue(fecha);
					txtHORA_INICIO.setValue(fecha);
					txtHORA_FIN.setValue(Calendar.getInstance().getTime());
				}
				if(spnQSVRESUL.getSelectedItemKey()!=null){
					if (	   spnQSVRESUL.getSelectedItemKey().toString().equals("2")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")
					) {
						if (bean.qiprox_dia != null && bean.qiprox_hora != null) {
							int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
							Date fechaProx = Util.getFechaHora(anio+"", bean.qiprox_mes,bean.qiprox_dia, bean.qiprox_hora,bean.qiprox_minuto, 0+"");
							txtFECHA_PROXIMA.setValue(fechaProx);
							txtHORA_PROXIMA.setValue(fechaProx);
						}
					}
				
				}
			} else if (action == ACTION.EDITAR) {
			if(bean.qivresul!=null){
				spnQSVRESUL.setSelectionKey(bean.qivresul);
			}
			if (bean.qivmin != null && bean.qivhora!=null) {
				Date fecha = Util.getFechaHora(bean.qivanio+"", bean.qivmes,bean.qivdia, bean.qivhora , bean.qivmin, 0+"");
				txtFECHA_INICIO.setValue(fecha);
				txtHORA_INICIO.setValue(fecha);
				if(bean.qivhora_fin!=null && bean.qivmin_fin!=null){
				txtHORA_FIN.setValue(Util.getHora(bean.qivhora_fin, bean.qivmin_fin, ""+0));
				}
			}
				if(spnQSVRESUL.getSelectedItemKey()!=null){
					if (	   spnQSVRESUL.getSelectedItemKey().toString().equals("2")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("3")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("4")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("5")
							|| spnQSVRESUL.getSelectedItemKey().toString().equals("6")
					) {
						if (bean.qiprox_dia != null && bean.qiprox_hora != null) {
							int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
							Date fechaProx = Util.getFechaHora(anio+"", bean.qiprox_mes,bean.qiprox_dia, bean.qiprox_hora,bean.qiprox_minuto, 0+"");
							txtFECHA_PROXIMA.setValue(fechaProx);
							txtHORA_PROXIMA.setValue(fechaProx);
						}
					}
					
				}
				if (bean.qivmin != null && bean.qivhora!=null && bean.qiprox_hora != null && bean.qiprox_minuto!=null) {
					Date fechaIni = Util.getFechaHora(bean.qivanio+"", bean.qivmes,bean.qivdia, bean.qivhora , bean.qivmin, 0+"");
					Date fechaFin = Util.getFechaHora(bean.qivanio+"", bean.qivmes,bean.qivdia, bean.qiprox_hora , bean.qiprox_minuto, 0+"");
					txtFECHA_INICIO.setValue(fechaIni);
					txtHORA_INICIO.setValue(fechaIni);
					txtHORA_FIN.setValue(fechaFin);
				}
			}
			inicio();
	}

	private void inicio() {
		if (action == ACTION.INICIAR) {
			btnAceptar.requestFocus();
			q4.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		} else if (action == ACTION.FINALIZAR) {
			q4.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			spnQSVRESUL.requestFocus();
		}
		onC1_RVISITAChangeValue(spnQSVRESUL);
		RenombrarEtiquetas();
	}
	public void RenombrarEtiquetas(){
		lbllimitaciones.text(R.string.seccion01qh26);
		lbllimitaciones.setText(lbllimitaciones.getText().toString().replace("(NOMBRE)", App.getInstance().getPersonaCuestionarioIndividual().qh02_1!=null?App.getInstance().getPersonaCuestionarioIndividual().qh02_1:""));
	}
	public void onC1_RVISITAChangeValue(FieldComponent component) {
		String resultadoStr = (String) component.getValue();
//		Log.e("","Resultado: "+resultadoStr.substring(0,1));
//		if (!resultadoStr.substring(0, 1).equals("6")) {
//			q3.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
//		}else{
//			q3.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(), false,rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
//		}
		
		if (resultadoStr.substring(0, 1).equals("7")) {
				Util.lockView(getActivity(), false, txtQSVRESUL_O);
				txtQSVRESUL_O.requestFocus();
			} else {
				Util.cleanAndLockView(getActivity(), txtQSVRESUL_O);
			}
		if (       resultadoStr.substring(0, 1).equals("2")
				|| resultadoStr.substring(0, 1).equals("3")
				|| resultadoStr.substring(0, 1).equals("4")
				|| resultadoStr.substring(0, 1).equals("5")) {
			Util.lockView(getActivity(), false, txtFECHA_PROXIMA,txtHORA_PROXIMA);
			q3.setVisibility(View.GONE);
			Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			txtFECHA_PROXIMA.requestFocus();
			
		} else {
			Util.cleanAndLockView(getActivity(), txtFECHA_PROXIMA,txtHORA_PROXIMA);
			if(resultadoStr.substring(0, 1).equals("6")){
				q3.setVisibility(View.VISIBLE);
				Util.lockView(getActivity(), false,rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
			else{
				q3.setVisibility(View.GONE);
				Util.cleanAndLockView(getActivity(), rgQD333_1,rgQD333_2,rgQD333_3,rgQD333_4,rgQD333_5,rgQD333_6);
			}
		}
	}
	/*BORRAR ESTE COMENTADO
	private void abrirDialogo(Seccion01 mef) {
		FragmentManager fm = this.getFragmentManager();
		CISECCION_01Fragment_000Dialog aperturaDialog = CISECCION_01Fragment_000Dialog.newInstance(caller ,mef);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");		
	}*/
	public CSVISITA CrearVisitaSalud(){
    	CSVISITA visita;
    	visita= new CSVISITA();
    	visita.id=App.getInstance().getMarco().id;
    	visita.hogar_id=App.getInstance().getHogar().hogar_id;
    	visita.persona_id=bean.persona_id;
    	visita.qsvdia=bean.qivdia.toString();
    	visita.qsvmes=bean.qivmes.toString();
    	visita.qsvanio=bean.qivanio;
    	visita.qsresult=(bean.qivresul==7?9:bean.qivresul);
    	visita.qsvresul=bean.qivresul;
    	visita.qsresult_o=bean.qivresul_o;
    	visita.nro_visita=1;
    	Calendar calendario=new GregorianCalendar();
    	visita.qsvhora_ini=calendario.get(Calendar.HOUR_OF_DAY)+"";
    	visita.qsvminuto_ini=calendario.get(Calendar.MINUTE)+"";
    	return visita;
    }
	


	private void cargarSpinner() {
		if (esResultado3) {
			List<Object> keys = new ArrayList<Object>();
			keys.add(null);
			keys.add(1);
			keys.add(2);
			String[] items1 = new String[] { " --- Seleccione --- ","1. Completa", "5. Incompleta" };
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
			spnQSVRESUL.setAdapterWithKey(adapter, keys);
		} else {
			List<Object> keys = new ArrayList<Object>();
			keys.add(null);
			keys.add(2);
			keys.add(3);
			keys.add(4);
			keys.add(5);
			keys.add(6);
			keys.add(7);
			String[] items1 = new String[] { " --- Seleccione --- ",
					"2. AUSENTE",
					"3. APLAZADA", 
					"4. RECHAZADA", 
					"5. INCOMPLETA",
					"6. DISCAPACITADA",
					"7. OTRO"
			};
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
			spnQSVRESUL.setAdapterWithKey(adapter, keys);
		}
	}
	
	private VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	
	public CuestionarioService getService() {
		if (service == null) {
			service = CuestionarioService.getInstance(getActivity());
		}
		return service;
	}
}