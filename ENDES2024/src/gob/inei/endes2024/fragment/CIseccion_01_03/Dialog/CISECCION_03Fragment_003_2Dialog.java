package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;

import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.util.Caretaker;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_003;
import gob.inei.endes2024.fragment.CIseccion_05_07.CISECCION_05Fragment_002;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_003_2Dialog extends DialogFragmentComponent {
	public TableComponent tcCalendario;
	public ButtonComponent btnCerrar;
	private static CISECCION_03Fragment_003 caller;
	private static CISECCION_05Fragment_002 caller1;
	//public List<CICALENDARIO_COL01_03> detalles;
	public List<CICALENDARIO_TRAMO_COL01_03> detalles=null;
	public List<CICALENDARIO_TRAMO_COL4> detallescol4=null;
	public CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesCargadoCalendario,seccionesCargadoCalendarioCol4;
	LinearLayout q1;
	LinearLayout q2;
	public static CISECCION_03Fragment_003_2Dialog newInstance(FragmentForm pagina) {
			CISECCION_03Fragment_003_2Dialog f = new CISECCION_03Fragment_003_2Dialog();
			f.setParent(pagina);
			Bundle args = new Bundle();
			f.setArguments(args);
			return f;
	}
	public CISECCION_03Fragment_003_2Dialog() {
		super();
		//seccionesCargadoCalendario = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIINDICE","QIMES_ID","QIANIO","QICOL1","QICOL1_O","QICOL2","QICOL2_O","QICOL3","QICOL3_O","QICOL4")};
		seccionesCargadoCalendario = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","QICOL2","QICOL3","QICANTIDAD")};
		seccionesCargadoCalendarioCol4 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIMES_INI","QIANIO_INI","QIRESULT","QIMES_FIN","QIANIO_FIN","QICANTIDAD")};
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().setTitle("CALENDARIO");
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	
	}

	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(tcCalendario.getTableView());
		q2 = createButtonSection(btnCerrar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}

	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		tcCalendario = new TableComponent(getActivity(), this, App.ESTILO).size(900, 750).headerHeight(altoComponente).dataColumHeight(35);
		tcCalendario.addHeader(R.string.cianiocalendario, 0.15f,TableComponent.ALIGN.CENTER);
		tcCalendario.addHeader(R.string.cimescalendario, 0.15f,TableComponent.ALIGN.LEFT);
		tcCalendario.addHeader(R.string.cicol1calendario,0.09f,TableComponent.ALIGN.CENTER);
		tcCalendario.addHeader(R.string.cicol2calendario, 0.09f,TableComponent.ALIGN.CENTER);
		tcCalendario.addHeader(R.string.cicol3calendario, 0.09f,TableComponent.ALIGN.CENTER);
		tcCalendario.addHeader(R.string.cicol4calendario, 0.09f,TableComponent.ALIGN.CENTER);
		btnCerrar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(150, 55).text(R.string.btnCerrar);
		btnCerrar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CISECCION_03Fragment_003_2Dialog.this.dismiss();
			}
		});
	}
	public void cargarDatos() {
		MyUtil.LiberarMemoria();
		detalles=null;
		detallescol4=null;
		detalles = getCuestionarioService().getTramosdelcalendario(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoCalendario);
		detallescol4 = getCuestionarioService().getTramoscol04ByPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoCalendarioCol4);
		List<CICALENDARIO_COL01_03> listacolumnas = new ArrayList<CICALENDARIO_COL01_03>();
		
	    Integer indicei=MyUtil.DeterminarIndice(App.ANIOPORDEFECTO, 1);
	    Integer indicef=MyUtil.DeterminarIndice(App.ANIOPORDEFECTOSUPERIOR, 12);
	    	    
	    Integer itramo1_3=0;
	    for(int i=indicei;i>=indicef;i--) {
	    	CICALENDARIO_COL01_03 fila = new CICALENDARIO_COL01_03();	    		    	
	    	
	    	CICALENDARIO_TRAMO_COL01_03 tramo = detalles.get(itramo1_3);
	    	Integer itramo = MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini);

	    	if(itramo == i) {
	    		Integer contador=0;
	    		
	    		for(int j=1;j<=tramo.qicantidad;j++) {
//	    			Log.e("","DATA_INDICE "+(i-(j-1)));
	    			fila = new CICALENDARIO_COL01_03();
	    			fila.qiindice=i-(j-1);
	    			
					fila.qimes_id = tramo.qimes_ini + contador;
					
					if(fila.qimes_id==13) {
						fila.qimes_id=1;
						tramo.qimes_ini=1;
						contador = 0;
					}
					
					if(j==1)
						fila.qicol3 = tramo.qicol3;

						fila.qicol1 = tramo.qir_inicial;
					if(j==tramo.qicantidad) {	
						fila.qicol1 = tramo.qir_final;
						fila.qicol2 = tramo.qicol2;
					}
					
					if(j>1) {
						if(fila.qimes_id == 1) {
							tramo.qianio_ini++;
							fila.qianio = tramo.qianio_ini;
						}
						else {
							fila.qianio = tramo.qianio_ini;
						}
					}
					else 					
						fila.qianio = tramo.qianio_ini;
					
					listacolumnas.add(fila);
					contador++;
				}
	    		i-=(tramo.qicantidad-1);
	    		
	    		if(detalles.size()>(itramo1_3+1))
	    			itramo1_3++;	    		
	    	}
	    	
	    	else {
//	    		Log.e("","DATA_INDICE  I: "+i);
	    		fila.qiindice=i;
	    	    ArrayMap<String,Integer> valores= MyUtil.determinarMesAnio(i);
	    		fila.qimes_id = valores.get("mes");
	    		fila.qianio = valores.get("anio");
	    		listacolumnas.add(fila);
	    	}
	    }	    
	    
//	    for(int k = 0 ; k< listacolumnas.size();k++ ) {
//	    	Log.e("","INDICE: "+listacolumnas.get(k).qiindice);
//	    }
//	    
	    CICALENDARIO_TRAMO_COL4 tramo4=null; 
		if(detallescol4!=null && detallescol4.size()>0) {
			
			Integer itramo4=0;
		    for(int k = 0 ; k< listacolumnas.size();k++ ) {
		    	
		    	CICALENDARIO_COL01_03 fila = listacolumnas.get(k);
		    	
		    	CICALENDARIO_TRAMO_COL4 tramo = detallescol4.get(itramo4);
		    	Integer itramo = MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini);

		    	if(fila.qiindice!=null && itramo == fila.qiindice) {
		    		Integer contador=0;
		    		for(int j=1;j<=tramo.qicantidad;j++) {
		    			CICALENDARIO_COL01_03 fila2 = listacolumnas.get(k+(j-1));
		    			fila2.qicol4 = tramo.qiresult;
					}
		    		k+=(tramo.qicantidad-1);
		    		
		    		if(detallescol4.size()>(itramo4+1))
		    			itramo4++;	    		
		    	}
		    	
		    }
	    }
		Collections.reverse(listacolumnas);
		tcCalendario.setData(listacolumnas, "qianio","getMes","qicol1","qicol2","qicol3","qicol4");
	}
  public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
}
