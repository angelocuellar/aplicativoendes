package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_007;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_007Dialog extends DialogFragmentComponent implements Respondible {
	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI235_M;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI235_Y;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI235_D;
		
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;

	public ButtonComponent btnAceptar,btnCancelar; 	
	public GridComponent2 gridFechaTerminacion; 
	public ButtonComponent btnGrabadoParcial;
	public TextField txtCabecera;
	private LabelComponent lblmes,lblanio,lblduracion;
	List<CISECCION_02> listados2;
	CISECCION_01_03 individual1_3;
	CISECCION_02T individual; 
	private static CISECCION_02Fragment_007 caller;
	
	private SeccionCapitulo[] seccionesGrabado, seccionesCargado,seccionesCargado2,seccionesCargado3;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	 
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
    }
	
	public enum ACTION_TERMINACION {
		AGREGAR, EDITAR 
	}

	public ACTION_TERMINACION accion;
	
	
	public static CISECCION_02Fragment_007Dialog newInstance(FragmentForm pagina, CISECCION_02T detalle) {
		caller = (CISECCION_02Fragment_007) pagina;
		CISECCION_02Fragment_007Dialog f = new CISECCION_02Fragment_007Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		individual = (CISECCION_02T) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= individual.terminacion_id;
		getDialog().setTitle("N� de Orden:  " + orden);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}

	public CISECCION_02Fragment_007Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1,"QI235_M","QI235_Y","QI235_D","ID","HOGAR_ID","PERSONA_ID","TERMINACION_ID") }; 
		seccionesCargado2 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI215D","QI215M","QI215Y","QI220A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM") };
		seccionesCargado3= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI233","QI231_M","QI231_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QI235_M","QI235_Y","QI235_D","TERMINACION_ID") };		
	}

	@Override
	protected void buildFields() {
		lblduracion = new LabelComponent(getActivity()).size(altoComponente, 80).text(R.string.ciseccion_02qi235_d).textSize(16).centrar();
		lblmes = new LabelComponent(getActivity()).size(altoComponente, 80).text(R.string.ciseccion_02qi235_m).textSize(16).centrar();
		lblanio = new LabelComponent(getActivity()).size(altoComponente, 80).text(R.string.ciseccion_02qi235_y).textSize(16).centrar();
		txtQI235_M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI235_Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
		txtQI235_D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1);
		
		gridFechaTerminacion = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridFechaTerminacion.addComponent(lblmes);
		gridFechaTerminacion.addComponent(lblanio);
		gridFechaTerminacion.addComponent(lblduracion);
		gridFechaTerminacion.addComponent(txtQI235_M);		
		gridFechaTerminacion.addComponent(txtQI235_Y);				
		gridFechaTerminacion.addComponent(txtQI235_D);
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CISECCION_02Fragment_007Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.cargarTabla();
				CISECCION_02Fragment_007Dialog.this.dismiss();
			}
		});		
	}
	
	@Override
	protected View createUI() {
		buildFields();		 
		q1 = createQuestionSection(gridFechaTerminacion.component());
		q2 = createButtonSection(btnAceptar, btnCancelar);
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}
	
	public boolean grabar(){
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}
	
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (Util.esVacio(individual.qi235_m)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI2235_M");
			view = txtQI235_M;
			error = true;
			return false;
		}
		if (Util.esMayor(individual.qi235_m,13) || Util.esMenor(individual.qi235_m, 1)) {
			mensaje = "Mes fuera de Rango";
			view = txtQI235_M;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi235_y)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI235_Y");
			view = txtQI235_Y;
			error = true;
			return false;
		}
		
		if (individual.qi235_y.toString().length()<4) {
			mensaje = "El an�o debe ser de longitud cuatro";
			view = txtQI235_Y;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi235_d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI235_D");
			view = txtQI235_D;
			error = true;
			return false;
		}
		
		boolean retorno=true;
		for(CISECCION_02 cs2 : listados2) {
			
			if(cs2.qi215y!=null && cs2.qi215m!=null && cs2.qi215d!=null) {
			Integer meses = 6;
			if(cs2.qi220a!=null)
				meses = cs2.qi220a; 
			
			Integer mes_fin = Integer.parseInt(cs2.qi215m);
			Integer dia_fin = Integer.parseInt(cs2.qi215d);
			
			Calendar fecha_fin = new GregorianCalendar(cs2.qi215y,mes_fin-1,dia_fin);
			Calendar fecha_ini = new GregorianCalendar(cs2.qi215y,mes_fin-1-meses,dia_fin);
			Integer mes_eva = individual.qi235_m;
			Calendar fecha_eva = new GregorianCalendar(individual.qi235_y,mes_eva-1,dia_fin);
			Calendar fecha_eva_ini = new GregorianCalendar(individual.qi235_y,mes_eva-1-individual.qi235_d,dia_fin);
			
			if((fecha_ini.compareTo(fecha_eva)<=0 && fecha_fin.compareTo(fecha_eva)>=0) || (fecha_ini.compareTo(fecha_eva_ini)<=0 && fecha_fin.compareTo(fecha_eva_ini)>=0)) {
			retorno = false;
				break; 
			}
		}
		}
		
		if (!retorno) {
			mensaje = "La fecha ingresada no es valida";
			view = txtQI235_Y;
			error = true;
			return retorno;
		}	
		
		if(individual1_3.qi231_m!=null && individual1_3.qi231_y!=null) {
			Integer meses = 6;
			if(individual1_3.qi233!=null)
				meses = individual1_3.qi233; 
			
			Integer mes_fin = Integer.parseInt(individual1_3.qi231_m);
			Integer dia_fin = 1;
			
			Calendar fecha_fin = new GregorianCalendar(individual1_3.qi231_y,mes_fin-1,dia_fin);
			Calendar fecha_ini = new GregorianCalendar(individual1_3.qi231_y,mes_fin-1-meses,dia_fin);
			
			Integer mes_eva = individual.qi235_m;
			Calendar fecha_eva = new GregorianCalendar(individual.qi235_y,mes_eva-1,dia_fin);
			Calendar fecha_eva_ini = new GregorianCalendar(individual.qi235_y,mes_eva-1-individual.qi235_d,dia_fin);
			
			if((fecha_ini.compareTo(fecha_eva)<=0 && fecha_fin.compareTo(fecha_eva)>=0) || (fecha_ini.compareTo(fecha_eva_ini)<=0 && fecha_fin.compareTo(fecha_eva_ini)>=0)) {
				mensaje = "La fecha ingresada no es valida";
				view = txtQI235_Y;
				error = true;
				return false;
			}
		}					
		
	    return true;
	}
	
		

	private void cargarDatos() {
//		Log.e("ss",""+individual.terminacion_id);
		individual = getCuestionarioService().getCISECCION_02T(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, individual.terminacion_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_02T();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			individual.terminacion_id =caller.terminaciones.size()+1;
		}
		
		individual1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
		listados2=getCuestionarioService().getNacimientosListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
		entityToUI(individual);
		inicio();
	}
	
	private void inicio() {
		ValidarsiesSupervisora();
	}
		

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	} 


	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_02Fragment_007Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQI235_D.readOnly();
			txtQI235_M.readOnly();
			txtQI235_Y.readOnly();
			btnAceptar.setEnabled(false);
		}
	}
}
