package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_01Fragment_000;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_000Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQD333_1;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQD333_2;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQD333_3;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQD333_4;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQD333_5;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQD333_6;
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnAceptar;
	
	Seccion01 bean;
	DISCAPACIDAD modelo;
//	private static CISECCION_01Fragment_000 caller;
	 private static FragmentForm caller;  
	public ButtonComponent btnCancelar,btnGrabadoParcial;
	private SeccionCapitulo[] seccionesGrabado;
	private SeccionCapitulo[] seccionesCargado;
	private CuestionarioService cuestionarioService;
	private Seccion01Service serviceSeccion01;
	private CuestionarioService hogarService;
	public GridComponent2 gridPreguntas26;
	public LabelComponent lbl26a1, lbl26a2, lbl26a3, lbl26a4, lbl26a5, lbl26a6;
	public List<Seccion01> listaMadres;
	public LabelComponent lbllimitaciones;
	private int iniPosition;
	public List<Seccion01> detallesq;
	public Integer contador = 0;
	public TableComponent tcPersonasDialog;
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
    }

	public static CISECCION_01Fragment_000Dialog newInstance(FragmentForm pagina,Seccion01 bean) {
		caller = pagina;
		CISECCION_01Fragment_000Dialog f = new CISECCION_01Fragment_000Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("bean", bean);
		f.setArguments(args);
		return f;
	}

	public interface Seccion01Fragment_002_BListener {
		void onFinishEditDialog(String inputText);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion01) getArguments().getSerializable("bean");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().setTitle("NOMBRE: "+bean.qh02_1+"     N� DE ORDEN:  " +bean.persona_id);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	public CISECCION_01Fragment_000Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
	}

	@Override
	protected void buildFields() {

		lbl26a1 = new LabelComponent(this.getActivity()).size(altoComponente, 450).text(R.string.seccion01qh26a1).textSize(18).alinearIzquierda();
		rgQD333_1 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a1_1, R.string.seccion01qh26a1_2,R.string.seccion01qh26a1_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a2 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a2).textSize(18).alinearIzquierda();
		rgQD333_2 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a2_1, R.string.seccion01qh26a2_2,R.string.seccion01qh26a2_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a3 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a3).textSize(18).alinearIzquierda();
		rgQD333_3 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a3_1, R.string.seccion01qh26a3_2,R.string.seccion01qh26a3_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a4 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a4).textSize(18).alinearIzquierda();
		rgQD333_4 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a4_1, R.string.seccion01qh26a4_2,R.string.seccion01qh26a4_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a5 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a5).textSize(18).alinearIzquierda();
		rgQD333_5 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a5_1, R.string.seccion01qh26a5_2,R.string.seccion01qh26a5_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a6 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a6).textSize(18).alinearIzquierda();
		rgQD333_6 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a6_1, R.string.seccion01qh26a6_2,R.string.seccion01qh26a6_3).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 60);

		lbllimitaciones = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh26);
		
		gridPreguntas26 = new GridComponent2(this.getActivity(), App.ESTILO, 2);
		gridPreguntas26.addComponent(lbl26a1);
		gridPreguntas26.addComponent(rgQD333_1);
		gridPreguntas26.addComponent(lbl26a2);
		gridPreguntas26.addComponent(rgQD333_2);
		gridPreguntas26.addComponent(lbl26a3);
		gridPreguntas26.addComponent(rgQD333_3);
		gridPreguntas26.addComponent(lbl26a4);
		gridPreguntas26.addComponent(rgQD333_4);
		gridPreguntas26.addComponent(lbl26a5);
		gridPreguntas26.addComponent(rgQD333_5);
		gridPreguntas26.addComponent(lbl26a6);
		gridPreguntas26.addComponent(rgQD333_6);

		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
//				caller.cargarTabla();
//				caller.contador=0;
				CISECCION_01Fragment_000Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
//				caller.cargarTabla();
//				caller.contador=0;
//				if (caller.detalles.size() > bean.persona_id) {
//					caller.abrirDetalle(caller.detalles.get(bean.persona_id),bean.persona_id, caller.detalles);
//				}
				CISECCION_01Fragment_000Dialog.this.dismiss();
			}
		});
		
		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				action=PROCCES.GRABADOPARCIAL;
				boolean flag=grabadoParcial();
				if(!flag){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), CISECCION_01Fragment_000Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q1 = createQuestionSection(lbllimitaciones,gridPreguntas26.component());
		LinearLayout botones = createButtonSection(btnAceptar,btnGrabadoParcial, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(botones);
		return contenedor;
	}

	public boolean grabar() {
		uiToEntity(modelo);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
		boolean flag = true;
//		SQLiteDatabase dbTX = getService().startTX();
		
		try {
//			Log.e("","M. ID: "+modelo.id);
//			Log.e("","M. HOGAR_ID: "+modelo.hogar_id);
//			Log.e("","M. PERSONA_ID: "+modelo.persona_id);
//			Log.e("","M. CUESTIONARIO_ID: "+modelo.cuestionario_id);
//			Log.e("","M. NINIO_ID: "+modelo.ninio_id);
			modelo.cuestionario_id=modelo.cuestionario_id==null?App.CUEST_ID_INDIVIDUAL:modelo.cuestionario_id;
			flag = getCuestionarioService().saveOrUpdate_Discapacidad(modelo, seccionesGrabado);

//			getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		}
		/*catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}*/
		return flag;
	}

	public boolean validar() {
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);

		if (Util.esVacio(modelo.qd333_1)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A1");
			view = rgQD333_1;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_2)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A2");
			view = rgQD333_2;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_3)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A3");
			view = rgQD333_3;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_4)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A4");
			view = rgQD333_4;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_5)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A5");
			view = rgQD333_5;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_6)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A6");
			view = rgQD333_6;
			error = true;
			return false;
		}
		return true;
	}

	private void cargarDatos() {
		Log.e("","ID: "+bean.id);
		Log.e("","HOGAR_ID: "+bean.hogar_id);
		Log.e("","PERSONA_ID: "+bean.persona_id);
		Log.e("","QI212: "+bean.qi212);
		Log.e("","CUESTIONARIO: "+bean.cuestionario_id);
		
		if (bean != null) {
			modelo = getCuestionarioService().getSeccion02IndividualParaDiscapacidad(bean.id,bean.hogar_id, bean.persona_id,Integer.valueOf(App.CUEST_ID_CARA_INDIVIDUAL),0, seccionesCargado);
		}
		Log.e("","MODELO: "+modelo);
		if (modelo == null) {
			modelo = new DISCAPACIDAD();
			modelo.id = App.getInstance().getHogar().id;
			modelo.hogar_id = App.getInstance().getHogar().hogar_id;
			modelo.persona_id = bean.persona_id;
			modelo.cuestionario_id=App.CUEST_ID_CARA_INDIVIDUAL;
			modelo.ninio_id=0;
			
		}
		entityToUI(modelo);
		caretaker.addMemento("antes", bean.saveToMemento(Seccion01.class));
		inicio();
	}
/*  public void refrescarTabla() {
		tcPersonasDialog.setData(detallesq, "label");

	}*/

	private void inicio() {
		rgQD333_1.requestFocus();
		ValidarsiesSupervisora();
		RenombrarLabels();
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQD333_1.readOnly();
			rgQD333_2.readOnly();
			rgQD333_3.readOnly();
			rgQD333_4.readOnly();
			rgQD333_5.readOnly();
			rgQD333_6.readOnly();			
		}
	}

	private void RenombrarLabels() {
		String replace = "(NOMBRE)";
		String Ud = "Usted";
		bean.qhinfo = bean.qhinfo == null ? 0 : bean.qhinfo;
		lbllimitaciones.setText(lbllimitaciones.getText().toString().replace(replace, bean.qhinfo == 1 ? Ud : bean.qh02_1));
		Spanned texto828 = Html.fromHtml(lbllimitaciones.getText()+ " <b>permanente:</b>");
		lbllimitaciones.setText(texto828);
	}
	
	public boolean grabadoParcial(){
		uiToEntity(bean);
		boolean flag = true;		
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de discapacidad");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}

	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_01Fragment_000Dialog.this.dismiss();
//		caller.contador=0;
//		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
//		caller.nextFragment(CuestionarioFragmentActivity.CHSECCION1f_2);
	}

}
