package gob.inei.endes2024.fragment.CIseccion_01_03.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_02Fragment_003;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_003Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public TextField txtQI212_NOM;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI213;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI214;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI215D;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQI215M;
	/*@FieldAnnotation(orderIndex=6)*/
	public IntegerField txtQI215Y;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI216;
	@FieldAnnotation(orderIndex=7)
	public IntegerField txtQI217;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI218;
	@FieldAnnotation(orderIndex=9)
	public SpinnerField spnQI219;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI220U;
	@FieldAnnotation(orderIndex=11)
	public IntegerField txtQI220A;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI221;
	@FieldAnnotation(orderIndex=13) 
	public TextAreaField txtQI200_OBS;
	@FieldAnnotation(orderIndex=14)	
	public ButtonComponent btnAceptar; 
	
	public List<CISECCION_02> detalles;
	public List<CISECCION_02> detallesActual;
	public GridComponent2 gdfechnac;
	public ButtonComponent btnGrabadoParcial;
	public ButtonComponent btnCancelar;
	
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10,q11;
	LinearLayout ly220;
	
	public boolean existeGemelos = false;
	public Integer actual215y=null;
	public String actual215m=null;
	public String actual215d=null;

	private static Integer MINIMA_EDAD_EMBARAZO=10;
	
	public TextField txtCabecera;
	private LabelComponent lll3,lll2,lll,lblp212,lblp213,lblp214,lblp215,lblp216,lblp217,lblp218,lblp219,lblp220,lblp225,lblp221,lblp220_Ind,lblp215_ind1,lblp215_ind2,lblp215_ind3, lblp217_ind,lblp220_Ind1,lblp220_Ind2,lblp220_Ind3,lblp220_Ind4;
	
	CISECCION_02 bean; 
	CISECCION_01_03 individualmef;
	private static CISECCION_02Fragment_003 caller;
	
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas, seccionesMenores,seccionesCargadomef,seccionesCargadoPersonaS1,seccionesGrabadoPersonaS1,seccionesCargadoSeccion04,seccionesGrabadoSeccion04;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	private Seccion01Service personaService;
	private Seccion04_05Service seccion04_05Service;
	 
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco;
	public LabelComponent lblpregunta01,lblpregunta03,lbl200_obs;
	public String nombre_anterior;
	
	CISECCION_02 entidadAnterior;
	CISECCION_02 reemplazar;
	
	public IntegerField txtQI220C1,txtQI220C2,txtQI220C3;
	public TextField txtvacio;
	public String fechareferencia;
	public GridComponent2 gridPreguntas220,gridPreguntas217;
	
	public Integer hvivos,hmuertos,mvivas,mmuertas;
	
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
    }
	
	public enum ACTION_NACIMIENTO {
		AGREGAR, EDITAR , AGREGAR_INTERMEDIO
	}

	public ACTION_NACIMIENTO accion;
	
	
	public static CISECCION_02Fragment_003Dialog newInstance(FragmentForm pagina, CISECCION_02 detalle,ACTION_NACIMIENTO accion,List<CISECCION_02> detalles) {
		caller = (CISECCION_02Fragment_003) pagina;
		CISECCION_02Fragment_003Dialog f = new CISECCION_02Fragment_003Dialog();
		f.accion = accion;
		f.setParent(pagina);
		f.detalles = detalles;
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);		
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (CISECCION_02) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= bean.qi212;
		getDialog().setTitle("N� de Orden:  " + orden);
		
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		Calendar cal = Calendar.getInstance();
		rango(getActivity(), txtQI215Y, 1975,cal.get(Calendar.YEAR), null, 9999);
		rango(getActivity(), txtQI217, 0, 38, null, 99);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}

	public CISECCION_02Fragment_003Dialog() {
		super();
		seccionesGrabado  = new SeccionCapitulo[] { new SeccionCapitulo( 0, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","QI200_OBS","QIEDAD_MESES","ID","HOGAR_ID","PERSONA_ID") };
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","QI200_OBS","ID","HOGAR_ID","PERSONA_ID") };
		
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","ID","HOGAR_ID","PERSONA_ID")};
		
		seccionesCargadoPersonaS1 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH07","QH06","QH7DD", "QH7MM","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabadoPersonaS1 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH07","QH7DD", "QH7MM","ID","HOGAR_ID","PERSONA_ID")};
		seccionesMenores = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH01", "QH02_1", "QH02_2","QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD","QH7MM", "QHINFO", "estado") };
		seccionesCargadomef = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI203_A","QI203_B","QI205_A","QI205_B","QI207_A","QI207_B")};
	}
		
	@Override
	protected void buildFields() {
//		LabelComponent lblvalor = new LabelComponent(getActivity()).size(altoComponente, 120).text(R.string.c2seccion_01_03qi220n).textSize(16).centrar();
		
		
		lblp212 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi212);
		lblp213 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi213);
		lblp214 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi214);
		lblp215 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi215);
		lblp215_ind1 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(15).text(R.string.c2seccion_01_03qi215_ind1).negrita();
		lblp215_ind2 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(16).text(R.string.c2seccion_01_03qi215_ind2);
		lblp215_ind3 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(16).text(R.string.c2seccion_01_03qi215_ind3).negrita();
		lblp216 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi216);
		lblp217 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi217);
		lblp217_ind = new LabelComponent(this.getActivity()).size(50, 350).textSize(15).text(R.string.c2seccion_01_03qi217_ind);
		lblp218 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi218);
		lblp219 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_01_03qi219);
		lblp220 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi220);
		lblp220_Ind1 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(15).text(R.string.c2seccion_01_03qi220_ind1).negrita();
		lblp220_Ind2 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(17).text(R.string.c2seccion_01_03qi220_ind2);
		lblp220_Ind3 = new LabelComponent(this.getActivity()).size(35, MATCH_PARENT).textSize(15).text(R.string.c2seccion_01_03qi220_ind3).negrita();
		lblp220_Ind4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.c2seccion_01_03qi220_ind4);
		lblp225 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi220a);
		lblp221 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_01_03qi221);
		lll = new LabelComponent(this.getActivity()).size(200, MATCH_PARENT).textSize(19).text("");
//		lll2 = new LabelComponent(this.getActivity()).size(100, MATCH_PARENT).textSize(19).text("");
//		lll3 = new LabelComponent(this.getActivity()).size(120, MATCH_PARENT).textSize(19).text("");
		
		txtvacio = new TextField(this.getActivity()).size(40, 400).maxLength(100).alinearIzquierda();
		txtQI212_NOM = new TextField(this.getActivity()).size(altoComponente, 730).maxLength(100).alinearIzquierda().soloTexto().callback("ontxtQI212_NOMChangeValue").alfanumerico();
		txtQI212_NOM.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0)
					ontxtQI212_NOMChangeValue();
				else 
					resetearLabels();
			}
		});
		rgQI213=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi213_1,R.string.c2seccion_01_03qi213_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI214=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi214_1,R.string.c2seccion_01_03qi214_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI214ChangeValue");
	
		txtQI215D=new IntegerField(this.getActivity()).size(altoComponente, 70).maxLength(2).callback("onFechaChanged").alinearDerecha();		
		txtQI215M=new IntegerField(this.getActivity()).size(altoComponente, 70).maxLength(2).callback("onFechaChangem").alinearDerecha();
		txtQI215Y=new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(4).alinearDerecha();
		txtQI215Y.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQI215Y.getText().toString().trim().length()==4)
					onFechaChangea();
			}
		});
		txtQI215Y.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(txtQI215Y.getText().toString().trim().length()==4)
					onFechaChangea();
			}
		}); 
		LabelComponent lbldia = new LabelComponent(getActivity()).size(35, 50).text(R.string.c2seccion_01_03qi215d).textSize(14).centrar();
		LabelComponent lblmes = new LabelComponent(getActivity()).size(35, 50).text(R.string.c2seccion_01_03qi215m).textSize(14).centrar();
		LabelComponent lblanio = new LabelComponent(getActivity()).size(35, 50).text(R.string.c2seccion_01_03qi215y).textSize(14).centrar();
		gdfechnac = new GridComponent2(this.getActivity(),App.ESTILO,6,0);
		gdfechnac.addComponent(lbldia);
		gdfechnac.addComponent(txtQI215D);
		gdfechnac.addComponent(lblmes);
		gdfechnac.addComponent(txtQI215M);
		gdfechnac.addComponent(lblanio);
		gdfechnac.addComponent(txtQI215Y);
		
				
		rgQI216=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi216_1,R.string.c2seccion_01_03qi216_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI216ChangeValue");
		txtQI217=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onp217ChangeValue");
		gridPreguntas217 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas217.addComponent(lblp217_ind);
		gridPreguntas217.addComponent(txtQI217);
		
		rgQI218=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi218_1,R.string.c2seccion_01_03qi218_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI218ChangeValue");
		spnQI219=new SpinnerField(this.getActivity()).size(altoComponente, 400).callback("onQI219ChangeValue");
		
		rgQI220U=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi220u_1,R.string.c2seccion_01_03qi220u_2,R.string.c2seccion_01_03qi220u_3).size(225,250).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI220ChangeValue");				
		txtQI220C1=new IntegerField(this.getActivity()).size(50, 100).maxLength(2); 
		txtQI220C2=new IntegerField(this.getActivity()).size(50, 100).maxLength(2);
		txtQI220C3=new IntegerField(this.getActivity()).size(50, 100).maxLength(2);
		txtQI220C2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onp220ChangeValue();
			}
		});
		gridPreguntas220 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas220.addComponent(rgQI220U,1,3);		
		gridPreguntas220.addComponent(txtQI220C1);
		gridPreguntas220.addComponent(txtQI220C2);
		gridPreguntas220.addComponent(txtQI220C3);
		
		txtQI220A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1).callback("onQI220aChangeValue");
		rgQI221=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_01_03qi221_1,R.string.c2seccion_01_03qi221_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("OcultarTecla");
			
		lbl200_obs    = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2obsxx);
		txtQI200_OBS = new TextAreaField(getActivity()).maxLength(1000).size(200, 600).alfanumerico();
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 70);
		btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 70);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 70);
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				CISECCION_02Fragment_003Dialog.this.dismiss();
				caller.dialogoAbierto = false;
				Util.lockView(getParentActivity(), false,caller.btnAgregar);
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}				
				
					reordenarLista(bean);
//					ModificarEnSeccion01delHogarSiesqueEstaListado(bean);
				
				caller.refrescarPersonas(bean);
				CISECCION_02Fragment_003Dialog.this.dismiss();
				caller.dialogoAbierto = false;
				if(bean.qi221!=null) {
					if( (accion == ACTION_NACIMIENTO.AGREGAR || accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO)  && bean.qi221==2){
						caller.agregarPersona(null);						
					}
					else if((accion == ACTION_NACIMIENTO.AGREGAR || accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO) && bean.qi221==1){
						caller.siguienteaccion = ACTION_NACIMIENTO.AGREGAR_INTERMEDIO;
						caller.agregarPersona(bean.qi212);
					}
					else if(accion == ACTION_NACIMIENTO.EDITAR && bean.qi221==1){
						caller.siguienteaccion = ACTION_NACIMIENTO.AGREGAR_INTERMEDIO;
						caller.agregarPersona(bean.qi212);					
					}
				}
				else {
					if(accion == ACTION_NACIMIENTO.AGREGAR) {
						caller.agregarPersona(null);
					}					
				}
			}
		});
		
		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean flag=grabadoParcial();
				action=PROCCES.GRABADOPARCIAL;
				boolean flagdetalle=grabadoParcial();
				if(!flagdetalle){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), CISECCION_02Fragment_003Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
	}
	
	
	@Override
	protected View createUI() {
		buildFields();		
		LinearLayout botones = createButtonSection(btnAceptar, btnGrabadoParcial ,btnCancelar);
		q0 = createQuestionSection(lblp212,txtQI212_NOM);
		q1 = createQuestionSection(lblp213,rgQI213);
		q2 = createQuestionSection(lblp214,rgQI214);
		q3 = createQuestionSection(lblp215,lblp215_ind1,lblp215_ind2,lblp215_ind3,gdfechnac.component());
		q4 = createQuestionSection(lblp216,rgQI216);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblp217,gridPreguntas217.component());
		q6 = createQuestionSection(lblp218,rgQI218);
		q7 = createQuestionSection(lblp219,spnQI219);
		q8 = createQuestionSection(lblp220,lblp220_Ind1,lblp220_Ind2,lblp220_Ind3,lblp220_Ind4,gridPreguntas220.component());
		q9 = createQuestionSection(lblp225,txtQI220A);
		q10 = createQuestionSection(lblp221,rgQI221);
		q11 = createQuestionSection(lbl200_obs,txtQI200_OBS);
		
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10); 			
		form.addView(q11);
		form.addView(botones);		 
		q8.setVisibility(View.GONE);
		
		return contenedor;
	}
	
	public boolean grabar(){
		uiToEntity(bean);
		
		if (bean.qi220u!=null) {
			if (!Util.esDiferente(bean.qi220u,1) && txtQI220C1.getText().toString().trim().length()!=0 ) {
				bean.qi220n=Integer.parseInt(txtQI220C1.getText().toString().trim());
			}
			else if (!Util.esDiferente(bean.qi220u,2) && txtQI220C2.getText().toString().trim().length()!=0) {
				bean.qi220n=Integer.parseInt(txtQI220C2.getText().toString().trim());
			}
			else if (!Util.esDiferente(bean.qi220u,3) && txtQI220C3.getText().toString().trim().length()!=0) {
				bean.qi220n=Integer.parseInt(txtQI220C3.getText().toString().trim());
			}
		}
		
		if(bean.qi220u==null)
			bean.qi220n=null;
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			if(bean.qi215d == null && bean.qi215m == null && bean.qi215y == null) {
				bean.qi217cons = null;
			} 
			else {
				if(fechareferencia == null) {
					bean.qi217cons =  Util.getFechaActualToString();
				}
				else {
					bean.qi217cons = fechareferencia;
				}			
			}
			
//			if(bean.qi217==null)
//				bean.qi217cons = null;
			if(bean.qi215y!=null && bean.qi215m.toString()!=null && bean.qi215d!=null) {
	    		Calendar fn =new GregorianCalendar(bean.qi215y, Integer.parseInt(bean.qi215m.toString())-1,Integer.parseInt(bean.qi215d.toString()));
	    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    		java.util.Date fechaactual=null;
	    		try {
					fechaactual=df.parse(bean.qi217cons);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
	    		Calendar cal = Calendar.getInstance();
				cal.setTime(fechaactual);
	    		bean.qiedad_meses=MyUtil.CalcularEdadEnMesesFelix(fn, cal);
//	    		Log.e("","MESES: "+bean.qiedad_meses);
			}
			if(accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO) {
				getCuestionarioService().reordenarIntermedioNumerodeOrdenNacimiento(reemplazar);
				//reordenarLista(reemplazar);					
			}
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);
			
			notificacion();
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de nacimiento.");
			}
			else {
				
				if(existeGemelos && bean.qi213==2) {
					boolean flag2 = getCuestionarioService().actualizarFechasGemelos(bean.id,bean.hogar_id,bean.persona_id,actual215y,actual215m,actual215d,bean.qi215y,bean.qi215m,bean.qi215d,bean.qi217,bean.qi220a);
					if (!flag2) {
						throw new Exception("Ocurri� un problema al grabar los datos.");
					}	
				}
				
				/*if(bean.qi215y<App.ANIOPORDEFECTO){	
				   getCuestionarioService().borrarEnlaceNacimiento(bean);	
				}*/
				if(bean.qiedad_meses!=null && bean.qiedad_meses>71){
					getCuestionarioService().borrarEnlaceNacimiento(bean);
				}
				if((bean.qi216!=null && bean.qi216==2) || (bean.qi218!=null && bean.qi218==2)){
					Log.e("muerto  o  no vive con la madre", "eliminar");
					   getCuestionarioService().borrarTablaDiscapacidad(bean);	
				}
				
			}
			
		getService().commitTX(dbTX);
		
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(),"ERROR1: " +e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			e.printStackTrace();
//			ToastMessage.msgBox(this.getActivity(),"ERROR2: " +e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		
		
		
		return flag;
	}
	

	public boolean validarSiexisteDiferenciaenfechaNacimientoS04_05(CISECCION_02 bean){
		boolean valor_inicials4_05=true;
		if(bean.qi219 != null && bean.qi219!=0){
			Calendar F_nacimientoCI2=null;
			 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(bean!=null &&  bean.qi215d!=null && bean.qi215m!=null && bean.qi215y!=null ){
				Integer mci2=Integer.parseInt(bean.qi215m.toString());
				Integer dci2=Integer.parseInt(bean.qi215d.toString());
				F_nacimientoCI2 = new GregorianCalendar(bean.qi215y, mci2-1, dci2);	
			}
		
			SeccionCapitulo[] cargadoseccion04_05= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","ID","HOGAR_ID")};
			Seccion04_05 persona= getCuestionarioService().getFecha_NacimientoNinioSeccion04_05(bean.id,bean.hogar_id,bean.qi219, cargadoseccion04_05);
			Calendar F_nacimientoCH04_05= null;
			if(persona!=null && persona.qh203d!=null && persona.qh203m!=null && persona.qh203y!=null &&  F_nacimientoCI2!=null){
				Integer mes04_05=Integer.parseInt(persona.qh203m);
				Integer dia_04_05 = Integer.parseInt(persona.qh203d);
				F_nacimientoCH04_05 = new GregorianCalendar(persona.qh203y, mes04_05-1,dia_04_05);
				valor_inicials4_05= (sdf.format(F_nacimientoCH04_05.getTime()).equals(sdf.format(F_nacimientoCI2.getTime())));
			}
		}
		return valor_inicials4_05;
	}
	public boolean validarSiexisteDiferenciaenfechaNacimientoS8(CISECCION_02 bean){
		boolean valor_inicials8=true;

		if(bean.qi219!=null && bean.qi219!=0){
			SeccionCapitulo[] cargadoseccion8= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS802CD","QS802CM","QS802CA","ID","HOGAR_ID")};
			Calendar F_nacimientoCI2=null;
			Calendar F_nacimientoCS8= null;
			 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(bean!=null &&  bean.qi215d!=null && bean.qi215m!=null && bean.qi215y!=null){
				Integer mci2=Integer.parseInt(bean.qi215m.toString());
				Integer dci2=Integer.parseInt(bean.qi215d.toString());
				F_nacimientoCI2 = new GregorianCalendar(bean.qi215y, mci2-1, dci2);	
			}
	
			CSSECCION_08 ninio=getCuestionarioService().getFecha_NacimientoNinioSeccion08(bean.id,bean.hogar_id,bean.qi219, cargadoseccion8);
			if(ninio!=null && ninio.qs802cd!=null && ninio.qs802cm!=null && ninio.qs802ca!=null && F_nacimientoCI2!=null ){
				F_nacimientoCS8 = new GregorianCalendar(ninio.qs802ca, ninio.qs802cm-1, ninio.qs802cd);
				valor_inicials8= (sdf.format(F_nacimientoCS8.getTime()).equals(sdf.format(F_nacimientoCI2.getTime())));
			}
				
		}
		return valor_inicials8;
	}
	
	public void reordenarLista(CISECCION_02 seccion2) {
		
		detallesActual = getCuestionarioService().getListaNacimientosCompletobyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesPersonas);
		
		Integer orden_inicial = seccion2.qi212;
		
		
		if(seccion2.qi215y!=null && seccion2.qi215m!=null && seccion2.qi215d!=null) {
		
		Calendar cal_inicial = new GregorianCalendar(seccion2.qi215y,Integer.parseInt(seccion2.qi215m)-1,Integer.parseInt(seccion2.qi215d)); 
		
		Integer orden_intercambio=null;
		
		Calendar cal_previo=null;
		
		HashMap<String,Integer> mapa = null;
		
		Integer indice_mayor = 0;
		Integer indice_menor = 0;
		
		if(existeGemelos) {
			cal_previo = new GregorianCalendar(actual215y,Integer.parseInt(actual215m)-1,Integer.parseInt(actual215d));
			mapa = getCuestionarioService().getExtremosGemelos(seccion2.id,seccion2.hogar_id,seccion2.persona_id,seccion2.qi215y,seccion2.qi215m,seccion2.qi215d);
			indice_mayor = mapa.get("mayor");
			indice_menor = mapa.get("menor");
		}		
		
		
		
		Calendar calAnt=null;
		Log.d("Mensaje","test detalles "+detallesActual.size());	
			for(CISECCION_02 cs2 :  detallesActual) {
				if(cs2.qi212!=seccion2.qi212) {
					if(cs2.qi215y!=null && cs2.qi215m!=null && cs2.qi215d!=null) {
						Calendar cal = new GregorianCalendar(cs2.qi215y,Integer.parseInt(cs2.qi215m)-1,Integer.parseInt(cs2.qi215d));
					   	
						if(accion == ACTION_NACIMIENTO.EDITAR) //&& !existeGemelos) 
						{							
							
							Log.d("Mensaje","test cal_inicial "+cal_inicial.get(Calendar.DAY_OF_MONTH)+" - "+cal_inicial.get(Calendar.MONTH)+" - "+cal_inicial.get(Calendar.YEAR));
							Log.d("Mensaje","test cal "+cal.get(Calendar.DAY_OF_MONTH)+" - "+cal.get(Calendar.MONTH)+" - "+cal.get(Calendar.YEAR));
							
							if(seccion2.qi212 < cs2.qi212) { 
								
								if(cal_inicial.compareTo(cal)>0) {
									orden_intercambio = cs2.qi212;
									Log.d("Mensaje","test orden_intercambio < "+orden_intercambio);
									//break;									
								}							
							}
							
							else if(seccion2.qi212 > cs2.qi212) { 
								
								if(cal_inicial.compareTo(cal)<0) {
									orden_intercambio = cs2.qi212;
									Log.d("Mensaje","test orden_intercambio > "+orden_intercambio);
									break;
								}
							}
							/*if(orden_intercambio == null && cs2.qi212 == detallesActual.size()) {
								orden_intercambio = cs2.qi212;
							}*/
						}
						else if(accion == ACTION_NACIMIENTO.EDITAR && existeGemelos) 
						{
							
							
						}
						
						
						calAnt = cal;
						
					}
				}
			}
//			Log.e("","ORDEN_I: "+orden_intercambio);
			Log.d("Mensaje","test indice inter "+orden_intercambio+" registro "+seccion2.qi212 );
			if(seccion2!=null && orden_intercambio!=null && !existeGemelos) //&& !(orden_inicial==(orden_intercambio-1)))
				getCuestionarioService().reordenarSeccion2(seccion2,orden_intercambio);
			else if(seccion2!=null && orden_intercambio!=null && existeGemelos) {
				getCuestionarioService().reordenarSeccion2Gemelos(seccion2,orden_intercambio,indice_menor,indice_mayor);
			}
		}		
	}
	
	public void notificacion() {
		String msg="";
		
		if(bean.qi212>1) {
			
			Calendar fechaNacAnterior = new GregorianCalendar(entidadAnterior.qi215y, Integer.parseInt(entidadAnterior.qi215m)-1, Integer.parseInt(entidadAnterior.qi215d));
			Calendar fechadenacimiento = new GregorianCalendar(bean.qi215y, Integer.parseInt(bean.qi215m)-1, Integer.parseInt(bean.qi215d));
			
			Integer meses =MyUtil.CalcularEdadEnMesesFelix(fechaNacAnterior, fechadenacimiento);
			 
			if(meses<10) {
				msg = "Verificar fecha de nacimiento entre hijos";
				ToastMessage.msgBox(this.getActivity(), msg,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			}
		}		
	}
	public void onQI219ChangeValue(){
		Integer value=spnQI219.getValue()!=null?Integer.parseInt(spnQI219.getSelectedItemKey().toString()):-1;
		Integer cisexo = rgQI214.getValue()!=null? Integer.parseInt(rgQI214.getTagSelected().toString()):-1;
		Integer ciedad = txtQI217.getValue()!=null?Integer.parseInt(txtQI217.getText().toString()):-1;
		Seccion01 persona=null;
		if(value!=-1 && value!=0){
			persona=getCuestionarioService().getSeccion01(bean.id, bean.hogar_id, value,seccionesCargadoPersonaS1);
			if( cisexo!=-1 && ciedad!=-1 && persona.qh06!=cisexo && persona!=null){
				MyUtil.MensajeGeneral(getActivity(), "Verificar sexo del ni�o(a)");
			}
			if( cisexo!=-1 && ciedad!=-1 && persona.qh07!=ciedad && persona!=null){
				MyUtil.MensajeGeneral(getActivity(), "Verificar edad del ni�o(a)");
			}
		}
		OcultarTecla();
		txtQI220A.requestFocus();
	}
	
	public void onrgQI218ChangeValue(){
		OcultarTecla();
		spnQI219.requestFocus();
	}
	public void onQI220aChangeValue(){
		OcultarTecla();
		btnAceptar.requestFocus();
	}
	
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQI213.getWindowToken(), 0);
	}
	
	
	public boolean grabadoParcial(){
		uiToEntity(bean);
		
		if (bean.qi220u!=null) {
			if (!Util.esDiferente(bean.qi220u,1) && txtQI220C1.getText().toString().trim().length()!=0 ) {
				bean.qi220n=Integer.parseInt(txtQI220C1.getText().toString().trim());
			}
			else if (!Util.esDiferente(bean.qi220u,2) && txtQI220C2.getText().toString().trim().length()!=0) {
				bean.qi220n=Integer.parseInt(txtQI220C2.getText().toString().trim());
			}
			else if (!Util.esDiferente(bean.qi220u,3) && txtQI220C3.getText().toString().trim().length()!=0) {
				bean.qi220n=Integer.parseInt(txtQI220C3.getText().toString().trim());
			}
		}
		
		if(bean.qi220u==null)
			bean.qi220n=null;
		
		if (!validarparcial()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		
		SQLiteDatabase dbTX = getService().startTX();
		try {
			
			if(bean.qi215d == null && bean.qi215m == null && bean.qi215y == null) {
				bean.qi217cons = null;
			} 
			else {
				if(fechareferencia == null) {
					bean.qi217cons =  Util.getFechaActualToString();
				}
				else {
					bean.qi217cons = fechareferencia;
				}			
			}
			
//			if(bean.qi217==null)
//				bean.qi217cons = null;
			
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);
		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos listado de personas");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (bean.qi212_nom== null) {
			error = true;
			view = txtQI212_NOM;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI212");
			return false;
		}
		if (Util.esVacio(bean.qi213)) {
			error = true;
			view = rgQI213;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI213");
			return false;
		}
		
		if(!(accion == ACTION_NACIMIENTO.AGREGAR || accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO)) {
			
		if (Util.esVacio(bean.qi214)) {
				error = true;
				view = rgQI214;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI214");
				return false;
		}			
		/*
		if(bean.qi214 ==1) {
			hvivos = hvivos == null ? 0 : hvivos;
			hmuertos = hmuertos == null ? 0 : hmuertos;
			
			Integer hijos_nac = hvivos+hmuertos;
			
			Integer i203b = individualmef.qi203_b == null ? 0 : individualmef.qi203_b;
			Integer i205b = individualmef.qi205_b == null ? 0 : individualmef.qi205_b;
			Integer i207b = individualmef.qi207_b == null ? 0 : individualmef.qi207_b;
			
			Integer hijos_dec = i203b+i205b+i207b;
			if((hijos_nac+1) > hijos_dec) {
				error = true;
				view = rgQI214;
				mensaje = "No puede ingresar mas hijos que los declarados";
				return false;
			}
		}
		
		if(bean.qi214==2) {
			mvivas = mvivas == null ? 0 : mvivas;
			mmuertas = mmuertas == null ? 0 : mmuertas;
			
			Integer hijas_nac = mvivas+mmuertas;
			
			Integer i203a = individualmef.qi203_a == null ? 0 : individualmef.qi203_a;
			Integer i205a = individualmef.qi205_a == null ? 0 : individualmef.qi205_a;
			Integer i207a = individualmef.qi207_a == null ? 0 : individualmef.qi207_a;
			
			Integer hijas_dec = i203a+i205a+i207a;
			if((hijas_nac+1) > hijas_dec) {
				error = true;
				view = rgQI214;
				mensaje = "No puede ingresar mas hijas que las declaradas";
				return false;
			}
		}*/
			
		if (Util.esVacio(bean.qi215d)) {
			error = true;
			view = txtQI215D;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI215D");
			return false;
		}
		

		int i215d = bean.qi215d == null ? 0 : Integer.parseInt(bean.qi215d);
		if (!(i215d>=1 && i215d<=31) && bean.qi215y>=App.ANIOPORDEFECTO) {
			error = true;
			view = txtQI215D;
			mensaje = "Valor del campo invalido";
			return false;		
		}
		if (!(i215d>=1 && i215d<=31) && (i215d!=98 && bean.qi215y<App.ANIOPORDEFECTO)) {
			error = true;
			view = txtQI215D;
			mensaje = "Valor del campo invalido";
			return false;		
		}
		
		if (Util.esVacio(bean.qi215m)) {
			error = true;
			view = txtQI215M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI215M");
			return false;
		}
		

		if (MyUtil.incluyeRango(1,9,bean.qi215m)) {
			mensaje = "Los meses deben considerarse a 2 d�gitos: de 01 a 12 meses";
			view = txtQI215M;
			error = true;
			return false;
		}
		

		int i215m = bean.qi215m == null ? 0 : Integer.parseInt(bean.qi215m);
		if (!(i215m>=1 && i215m<=12)) {
			error = true;
			view = txtQI215M;
			mensaje = "Valor del campo invalido";
			return false;
		}

		
		
		if (Util.esVacio(bean.qi215y)) {
			error = true;
			view = txtQI215Y;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI215Y");
			return false;
		}
		
		if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(bean.qi215d), Integer.parseInt(bean.qi215m)) && Integer.parseInt(bean.qi215d)!=98){
			mensaje = "d�a no corresponde al mes"; 
			view = txtQI215D; 
			error = true; 
			return false; 
		}
		
		Calendar cal2 = Calendar.getInstance();
		if(!(bean.qi215y>=1975 && bean.qi215y<=cal2.get(Calendar.YEAR)) ) {
			error = true;
			view = txtQI215Y;
			mensaje = "Valor del campo fuera de rango";
			return false;
		}  
		
		/*
		if(bean.qi212>1) {
			
			Calendar fechaNacAnterior = new GregorianCalendar(entidadAnterior.qi215y, Integer.parseInt(entidadAnterior.qi215m)-1, Integer.parseInt(entidadAnterior.qi215d));
			Calendar fechadenacimiento = new GregorianCalendar(bean.qi215y, Integer.parseInt(bean.qi215m)-1, Integer.parseInt(bean.qi215d));
			
			if(!(fechaNacAnterior.before(fechadenacimiento) || fechaNacAnterior.compareTo(fechadenacimiento)==0) ) {
				error = true;
				view = txtQI215Y;
				mensaje = "La fecha ingresada debe ser mayor que el nacimiento anterior";
				return false;
			}

		} */
		
		
		
		boolean existeGemelos2 = getCuestionarioService().getExisteGemelos(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,bean.qi215y,bean.qi215m,bean.qi215d);
		if (bean.qi213 ==1 && existeGemelos2) {
			error = true;
			view = txtQI215D;
			mensaje = "La fecha ingresada pertenece a otro nacimiento";
			return false;
		}
		
		
		
		
		if (Util.esVacio(bean.qi216)) {
			error = true;
			view = rgQI216;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI216");
			return false;
		}	
		
		if(bean.qi216==1) {
			/*
			if(bean.qi214 ==1) {
				hvivos = hvivos == null ? 0 : hvivos;
				
				
				Integer hijos_viv = hvivos;
				
				Integer i203b = individualmef.qi203_b == null ? 0 : individualmef.qi203_b;
				Integer i205b = individualmef.qi205_b == null ? 0 : individualmef.qi205_b;
				
				Integer hijos_dec = i203b+i205b;
				if((hijos_viv+1) > hijos_dec) {
					error = true;
					view = rgQI216;
					mensaje = "No puede ingresar mas hijos vivos que los declarados";
					return false;
				}
			}
			
			if(bean.qi214==2) {
				mvivas = mvivas == null ? 0 : mvivas;
				
				Integer hijas_viv = mvivas;
				
				Integer i203a = individualmef.qi203_a == null ? 0 : individualmef.qi203_a;
				Integer i205a = individualmef.qi205_a == null ? 0 : individualmef.qi205_a;
				
				Integer hijas_dec = i203a+i205a;
				if((hijas_viv+1) > hijas_dec) {
					error = true;
					view = rgQI216;
					mensaje = "No puede ingresar mas hijas vivas que las declaradas";
					return false;
				}
			} */
			
			
			if (Util.esVacio(bean.qi217)) {
				error = true;
				view = txtQI217;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI217");
				return false;
			}				
			
			int edad_permitida=individualmef.qi106-MINIMA_EDAD_EMBARAZO;
			if(bean.qi217>(edad_permitida)) {
				error = true;
				view = txtQI217;
				mensaje = "La edad es mayor que la permitida "+edad_permitida+" a�os ";
				return false;			
			}
			
			
			Calendar fechadenacimiento = new GregorianCalendar(bean.qi215y, Integer.parseInt(bean.qi215m)-1, Integer.parseInt(bean.qi215d));
			Calendar fecharef = new GregorianCalendar();
			
			if(fechareferencia!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(fechareferencia);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}			
			Integer edadcalculada =MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef);
			Integer edadcalculadamenosuno = edadcalculada+1;
			if(Util.esDiferente(edadcalculada, bean.qi217) && Util.esDiferente(edadcalculadamenosuno, bean.qi217) && Integer.parseInt(bean.qi215d)==98){
				mensaje = "Edad no corresponde con P.QI217"; 
				view = txtQI217; 
				error = true; 
				return false;
			}
			else if(Util.esDiferente(MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef), bean.qi217) && Integer.parseInt(bean.qi215d)!=98){
				mensaje = "Edad no corresponde con P.QI217"; 
				view = txtQI217; 
				error = true; 
				return false;
			}	
			
			if (Util.esVacio(bean.qi218)) {
				error = true;
				view = rgQI218;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI218");
				return false;
			}
			
			if (Util.esVacio(bean.qi219)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI219");
				view = spnQI219;
				error = true;
				return false;
			}
		}

		else if(bean.qi216==2) {
			/*
			if(bean.qi214 ==1) {
				
				hmuertos = hmuertos == null ? 0 : hmuertos;
				
				Integer hijos_mue = hmuertos;
				
				Integer i207b = individualmef.qi207_b == null ? 0 : individualmef.qi207_b;
				
				Integer hijos_dec = i207b;
				if((hijos_mue+1) > hijos_dec) {
					error = true;
					view = rgQI216;
					mensaje = "No puede ingresar mas hijos muertos que los declarados";
					return false;
				}
			}
			
			if(bean.qi214==2) {
				
				mmuertas = mmuertas == null ? 0 : mmuertas;
				
				Integer hijas_mue = mmuertas;
				
				Integer i207a = individualmef.qi207_a == null ? 0 : individualmef.qi207_a;
				
				Integer hijas_dec = i207a;
				if((hijas_mue+1) > hijas_dec) {
					error = true;
					view = rgQI216;
					mensaje = "No puede ingresar mas hijas muertas que las declaradas";
					return false;
				}
			}
			*/
			
			if (bean.qi220u == null) {
				error = true;
				view = rgQI220U;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI220");
				return false;
			}
			
			if (!Util.esDiferente(bean.qi220u,1)) {
				if (Util.esVacio(bean.qi220n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.220C1"); 
					view = txtQI220C1; 
					error = true; 
					return false; 
				}		
				
				if (!(bean.qi220n>=0 && bean.qi220n<=29)) { 
					mensaje = "Valor incorrecto en P.220C1"; 
					view = txtQI220C1; 
					error = true; 
					return false; 
				}		
			}	
			
			if (!Util.esDiferente(bean.qi220u,2)) {
				if (Util.esVacio(bean.qi220n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.220C2"); 
					view = txtQI220C2; 
					error = true; 
					return false; 
				}	
				
				if (!(bean.qi220n>=1 && bean.qi220n<=23)) { 
					mensaje = "Valor incorrecto en P.220C2"; 
					view = txtQI220C2; 
					error = true; 
					return false; 
				}	
			}
			
			if (!Util.esDiferente(bean.qi220u,3)) {
				if (Util.esVacio(bean.qi220n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.220C3"); 
					view = txtQI220C3; 
					error = true; 
					return false; 
				}	
				
				if (!(bean.qi220n>=2 && bean.qi220n<=40)) { 
					mensaje = "Valor incorrecto en P.220C3"; 
					view = txtQI220C3; 
					error = true; 
					return false; 
				}	
			}			
		}				
		
		if(bean.qi215y!=null && bean.qi215m!=null && bean.qi215d!=null) {
		
			Calendar fechadenacimiento = new GregorianCalendar(bean.qi215y,Integer.parseInt(bean.qi215m)-1,Integer.parseInt(bean.qi215d));
			//Calendar fecharef = new GregorianCalendar();
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(App.FECHA_MINIMA_P220);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			
			if(cal.before(fechadenacimiento) || cal.compareTo(fechadenacimiento)==0) {
				if (Util.esVacio(bean.qi220a)) {
					error = true;
					view = txtQI220A;
					mensaje = preguntaVacia.replace("$", "La pregunta P.QI225");
					return false;
				}	
				
				int i220a = bean.qi220a == null ? 0 :bean.qi220a;
				if (!(i220a>=5 &&i220a<=9)) {
					error = true;
					view = txtQI220A;
					mensaje = "Valor del campo invalido";
					return false;
				}
			}
		}
		
		if(bean.qi212!=1){
			if (Util.esVacio(bean.qi221)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.221"); 
				view = rgQI221; 
				error = true; 
				return false; 
			}	
		}
		
		}
		if(!validarSiexisteDiferenciaenfechaNacimientoS8(bean)){
			mensaje = "La fecha de nacimiento es inconsistente con la seccion08"; 
			MyUtil.MensajeGeneral(getActivity(), mensaje);
		}
		if(!validarSiexisteDiferenciaenfechaNacimientoS04_05(bean)){
			mensaje = "La fecha de nacimiento no es inconsistente con la seccion 04"; 
			MyUtil.MensajeGeneral(getActivity(), mensaje);
		}
	    return true;
	}
	
	public boolean validarparcial(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (bean.qi212_nom == null) {
			error = true;
			view = txtQI212_NOM;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI212");
			return false;
		}
		return true;
	}
	
	public void resetearLabels() {
		lblp213.setText(getResources().getString(R.string.c2seccion_01_03qi213));
		lblp214.setText(getResources().getString(R.string.c2seccion_01_03qi214));
		lblp215.setText(getResources().getString(R.string.c2seccion_01_03qi215));
		lblp216.setText(getResources().getString(R.string.c2seccion_01_03qi216));
		lblp218.setText(getResources().getString(R.string.c2seccion_01_03qi218));
		lblp220.setText(getResources().getString(R.string.c2seccion_01_03qi220));
		lblp220_Ind2.setText(getResources().getString(R.string.c2seccion_01_03qi220_ind2));
		lblp225.setText(getResources().getString(R.string.c2seccion_01_03qi220a));
		lblp221.setText(getResources().getString(R.string.c2seccion_01_03qi221));
	}
	
	public void ontxtQI212_NOMChangeValue(){
		if(txtQI212_NOM.getText().toString().length()>0){
			resetearLabels();
			lblp213.setText(lblp213.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp214.setText(lblp214.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp215.setText(lblp215.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp216.setText(lblp216.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp218.setText(lblp218.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp220.setText(lblp220.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp220_Ind2.setText(lblp220_Ind2.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp225.setText(lblp225.getText().toString().replace("(NOMBRE)",txtQI212_NOM.getText().toString()));
			lblp221.setText(lblp221.getText().toString().replace("#NOMBRE#",txtQI212_NOM.getText().toString()));
			if(bean.qi212>1) {
			renombrarEtiquetaIntermedio();
			}
		}
//		OcultarTecla();
	}
	
	
	public void onrgQI214ChangeValue() {
		if(rgQI214.getValue()!=null) {
			if (MyUtil.incluyeRango(2,2,rgQI214.getTagSelected("").toString())) {
				MyUtil.llenarMenoresHogar(getActivity() ,getService() , spnQI219 ,App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,  bean.qi219,2, seccionesMenores);
				
			} 
			else {
				MyUtil.llenarMenoresHogar(getActivity() ,getService() , spnQI219 ,App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,  bean.qi219,1, seccionesMenores);
				
			}	
			txtQI215D.requestFocus();
		}
		
	} 
	
	public void onrgQI216ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI216.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.lockView(getActivity(),false,rgQI220U,txtQI220C1,txtQI220C2,txtQI220C3);
    		Util.cleanAndLockView(getActivity(),txtQI217,rgQI218,spnQI219);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.VISIBLE);
    		rgQI220U.requestFocus();
    	} 
		else {
			Util.cleanAndLockView(getActivity(),rgQI220U,txtQI220C1,txtQI220C2,txtQI220C3);
			Util.lockView(getActivity(),false,txtQI217,rgQI218,spnQI219);
			q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.GONE);
    		txtQI217.requestFocus();
		}	
		
	} 
	
	private void cargarDatos() {
		
		CISECCION_02 entidad=null;
		
		individualmef = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadomef);
		
		if(accion != ACTION_NACIMIENTO.AGREGAR_INTERMEDIO) 
			entidad =  getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesPersonas);
		else {
			caller.siguienteaccion = null;
			reemplazar =  getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesPersonas);
			}
		if(entidad != null) {
			bean = entidad;
		}
		
		if(bean.qi212>1) {
			 entidadAnterior =  getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212-1,seccionesPersonas);
			if(entidadAnterior!=null) {
				nombre_anterior = entidadAnterior.qi212_nom;
			}
		}
		
		actual215d = bean.qi215d;
		actual215m = bean.qi215m;
		actual215y = bean.qi215y;
		
		if(actual215d!=null && actual215m!=null && actual215y!=null) {
			existeGemelos = getCuestionarioService().getExisteGemelos(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,actual215y,actual215m,actual215d);
		}
		
		MyUtil.llenarMenoresHogar(getActivity() ,getService() , spnQI219 ,App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,  bean.qi219,bean.qi214, seccionesMenores);

		entityToUI(bean);

		if (bean.qi220n!=null) {	
    		if (!Util.esDiferente(bean.qi220u,1)) {
    			txtQI220C1.setText(bean.qi220n.toString());
    		}
    		if (!Util.esDiferente(bean.qi220u,2)) {
    			txtQI220C2.setText(bean.qi220n.toString());
    		}
    		if (!Util.esDiferente(bean.qi220u,3)) {
    			txtQI220C3.setText(bean.qi220n.toString());
    		}
    	}
		
		/*
		HashMap<String,Integer> valores = getCuestionarioService().getValoresNacimmientos(App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, bean.qi212);
		if(valores!=null) {
			hvivos = valores.get("HVIVOS");
			hmuertos = valores.get("HMUERTOS");
			mvivas = valores.get("MVIVAS");
			mmuertas = valores.get("MMUERTAS"); 
		}*/		
		
		fechareferencia = bean.qi217cons;
		
		caretaker.addMemento("antes", bean.saveToMemento(CISECCION_02.class));
		inicio();
	}
	
	private void inicio() {
		esconderpregunta();
		renombrarEtiquetas();
		ontxtQI212_NOMChangeValue();
		verificarNuevo();
		ValidarsiesSupervisora();
		rgQI214.requestFocus();
	}
	
	public void verificarNuevo() {
		if(accion == ACTION_NACIMIENTO.AGREGAR || accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO) {
		 Util.cleanAndLockView(getActivity(),  rgQI214,	txtQI215D, txtQI215M, txtQI215Y, rgQI216, txtQI217, rgQI218, 
				 spnQI219, rgQI220U,txtQI220C1,txtQI220C2,txtQI220C3, txtQI220A, rgQI221);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
		}
		else {
			if(accion == ACTION_NACIMIENTO.AGREGAR || accion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO) {
				 Util.lockView(getActivity(),false, rgQI214,txtQI215D, txtQI215M, txtQI215Y, rgQI216, txtQI217, rgQI218, 
						 spnQI219, rgQI220U,txtQI220C1,txtQI220C2,txtQI220C3, txtQI220A, rgQI221);
					q2.setVisibility(View.VISIBLE);
					q3.setVisibility(View.VISIBLE);
					q4.setVisibility(View.VISIBLE);
					q5.setVisibility(View.VISIBLE);
					q6.setVisibility(View.VISIBLE);
					q7.setVisibility(View.VISIBLE);
					q8.setVisibility(View.VISIBLE);
					q9.setVisibility(View.VISIBLE);
					q10.setVisibility(View.VISIBLE);

			}
			onrgQI216ChangeValue();
			if(bean.qi212>1) {
				renombrarEtiquetaIntermedio();
			}
			onqrgQI220ChangeValue();
			txtQI212_NOM.requestFocus();
			
			evaluaFecha();
		}
	}
	
	public void renombrarEtiquetas()
    {	
    	lblp212.setText(lblp212.getText().toString().replace("$", MyUtil.ordinal(bean.qi212))); 
    }
	
	public void renombrarEtiquetaIntermedio()
    {	
    	lblp221.setText(lblp221.getText().toString().replace("(NOMBRE)", nombre_anterior)); 
    }
	
	private void esconderpregunta(){
		Integer ii212 = bean.qi212 == null ? 0 : bean.qi212;
		if(ii212==1){
			q10.setVisibility(View.GONE);
		}
		else{
			q10.setVisibility(View.VISIBLE);
		}
	}

	public void onqrgQI220ChangeValue() {
		if(rgQI220U.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI220U.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI220C2,txtQI220C3);
				bean.qi220n=null;
				Util.lockView(getActivity(),false,txtQI220C1);				
				txtQI220C1.requestFocus();				
			} 
			else if (MyUtil.incluyeRango(2,2,rgQI220U.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI220C1,txtQI220C3);	
				bean.qi220n=null;
				Util.lockView(getActivity(),false,txtQI220C2);			
				txtQI220C2.requestFocus();	
				onp220ChangeValue();
			}	
			else if (MyUtil.incluyeRango(3,3,rgQI220U.getTagSelected("").toString())) {
				Util.cleanAndLockView(getActivity(),txtQI220C1,txtQI220C2);	
				bean.qi220n=null;
				Util.lockView(getActivity(),false,txtQI220C3);			
				txtQI220C3.requestFocus();				
			}
		}
	
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	} 
	
	public void onp217ChangeValue() {
		
		boolean valido = true;
		String msg = "";
		int edad_permitida=individualmef.qi106-MINIMA_EDAD_EMBARAZO;
		String s217 =txtQI217.getValue() == null ? "0" : txtQI217.getValue().toString().trim() ;
		
		Integer i217 = Integer.parseInt(s217);
		
		if(i217>(edad_permitida)) {
			msg = "La edad es mayor que la permitida "+edad_permitida+" a�os ";
			valido = false;
		}
		
		String s215y = txtQI215Y.getValue() == null ? null : txtQI215Y.getValue().toString();
		String s215m = txtQI215M.getValue() == null ? null : txtQI215M.getValue().toString();
		String s215d = txtQI215D.getValue() == null ? null : txtQI215D.getValue().toString();
		
		if(s215y!=null && s215m!=null && s215d!=null && valido) {
		
			Calendar fechadenacimiento = new GregorianCalendar(Integer.parseInt(s215y), Integer.parseInt(s215m)-1, Integer.parseInt(s215d));
			Calendar fecharef = new GregorianCalendar();
			
			if(fechareferencia!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(fechareferencia);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}			
			Integer edadcalculada=0;
			Integer edadcalculadamenosuno=0;
			edadcalculada=MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento, fecharef);
			edadcalculadamenosuno=edadcalculada+1;
			if(Util.esDiferente(edadcalculada, i217) && Util.esDiferente(edadcalculadamenosuno, i217) && Integer.parseInt(s215d)==98){
				msg = "Edad no corresponde en P.QI217"; 
				valido = false;
			}
			else if(Util.esDiferente(MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef), i217)&& Integer.parseInt(s215d)!=98){
				msg = "Edad no corresponde en P.QI217"; 
				valido = false;
			}		
		
		}
		if(!valido)
			ToastMessage.msgBox(this.getActivity(), msg,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	
	public void onp220ChangeValue() {
		Integer meses=0;
		if(txtQI220C2.getText()!=null && txtQI220C2.getText().toString().length()>0) {
			meses = Integer.parseInt(txtQI220C2.getText().toString());
			if(meses==12) {				
			String str ="Si la edad al morir del ni�o es 12 meses verifique si es necesario";	
			ToastMessage.msgBox(this.getActivity(), str,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);							
			}
		
		}
		txtQI220A.requestFocus();
	}
	
	public void evaluaFecha() {
		
		Integer y=null,m=null,d=null;
		if(txtQI215Y.getText()!=null && !txtQI215Y.getText().toString().isEmpty())
			y = Integer.parseInt(txtQI215Y.getText().toString());
		if(txtQI215M.getText()!=null && !txtQI215M.getText().toString().isEmpty())
			m = Integer.parseInt(txtQI215M.getText().toString());
		if(txtQI215D.getText()!=null && !txtQI215D.getText().toString().isEmpty())
			d = Integer.parseInt(txtQI215D.getText().toString());
		
		if(y!=null && m!=null && d!=null) {
		
			Calendar fechadenacimiento = new GregorianCalendar(y,m-1,d);
			//Calendar fecharef = new GregorianCalendar();
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(App.FECHA_MINIMA_P220);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			
			if(!cal.before(fechadenacimiento) && !(cal.compareTo(fechadenacimiento)==0)
					) {
				Util.cleanAndLockView(getActivity(), txtQI220A);
				q9.setVisibility(View.GONE);
			} else {
				Util.lockView(getActivity(),false, txtQI220A);
				q9.setVisibility(View.VISIBLE);
			}
		}
	}
	
	public void onFechaChanged() {
		evaluaFecha();
		txtQI215M.requestFocus();
	}
	
	public void onFechaChangem() {
		evaluaFecha();
		txtQI215Y.requestFocus();
	}
	
	public void onFechaChangea() {
		evaluaFecha();
		rgQI216.requestFocus();
	}
	public void ModificarEnSeccion01delHogarSiesqueEstaListado(CISECCION_02 bean){
		Seccion01 persona=null;
		Seccion04_05 personaseccion04=null;
		if(bean.qi219!=null && Util.esDiferente(bean.qi219, 0)){
			persona=getCuestionarioService().getSeccion01(bean.id, bean.hogar_id, bean.qi219,seccionesCargadoPersonaS1);
			if(persona!=null ) {
						try {
							persona.qh07=bean.qi217;
							if(Util.esMayor(bean.qi217, 14)){
							persona.qh7dd=bean.qi215d;
							persona.qh7mm=bean.qi215m;
							}
							else{
								persona.qh7dd=null;
								persona.qh7mm=null;
							}
							getPersonaService().saveOrUpdate(persona, seccionesGrabadoPersonaS1);
						} catch (SQLException e) {
							ToastMessage.msgBox(this.getActivity(), e.getMessage()+" Hogar S1",
									ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
						}
				}
		personaseccion04 = getCuestionarioService().getSeccion04_05(bean.id, bean.hogar_id, bean.qi219, seccionesCargadoSeccion04);
			if(personaseccion04!=null && personaseccion04.qh203d!=null && personaseccion04.qh203m!=null && personaseccion04.qh203y!=null){
				Integer dias4ch=Integer.parseInt(personaseccion04.qh203d.toString());
				Integer dias2ci=Integer.parseInt(bean.qi215d.toString());
				Integer mess4ch= Integer.parseInt(personaseccion04.qh203m.toString());
				Integer mess2ci = Integer.parseInt(bean.qi215m.toString());
				if(Util.esDiferente(dias4ch, dias2ci) || Util.esDiferente(mess4ch, mess2ci) || Util.esDiferente(bean.qi215y, personaseccion04.qh203y)){
					try {
						personaseccion04.qh203d=bean.qi215d;
						personaseccion04.qh203m = bean.qi215m;
						personaseccion04.qh203y= bean.qi215y;
						getSeccion04_05Service().saveOrUpdate(personaseccion04, seccionesGrabadoSeccion04);
					} catch (SQLException e) {
						ToastMessage.msgBox(this.getActivity(), e.getMessage()+" Hogar S4",
								ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
					}
				}
			}
		}
	}
	
	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}
	
  public Seccion04_05Service getSeccion04_05Service() { 
			if(seccion04_05Service==null){ 
				seccion04_05Service = Seccion04_05Service.getInstance(getActivity()); 
			} 
			return seccion04_05Service; 
	    }
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		CISECCION_02Fragment_003Dialog.this.dismiss();
		caller.dialogoAbierto = false;
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI213.readOnly();
			rgQI214.readOnly();
			rgQI216.readOnly();
			rgQI218.readOnly();
			rgQI220U.readOnly();
			rgQI221.readOnly();
			txtQI212_NOM.readOnly();
			txtQI215D.readOnly();
			txtQI215M.readOnly();
			txtQI215Y.readOnly();
			txtQI217.readOnly();
			txtQI220A.readOnly();
			txtQI220C1.readOnly();
			txtQI220C2.readOnly();
			txtQI220C3.readOnly();
			spnQI219.readOnly();
			btnAceptar.setEnabled(false);
			btnGrabadoParcial.setEnabled(false);
		}
	}
}
