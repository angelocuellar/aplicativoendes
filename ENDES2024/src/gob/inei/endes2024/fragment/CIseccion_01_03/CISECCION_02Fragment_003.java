package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_02Fragment_003Dialog;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_02Fragment_003Dialog.ACTION_NACIMIENTO;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.DTDHandler;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.AdapterView.OnItemClickListener;

public class CISECCION_02Fragment_003 extends FragmentForm implements Respondible {
	
	public List<CISECCION_02> detalles;
//	Hogar bean;
	private CuestionarioService cuestionarioService;
	public ButtonComponent btnAgregar;
	public TableComponent tcPersonas;
	private DialogComponent dialog;
	Seccion01ClickListener adapter;
	public LabelComponent lblTitulo;
	
	CISECCION_01_03 individual;
	CISECCION_01_03 individualmef;
	LinearLayout q0,q1,q2;
	
	public TextField txtCabecera;	
	public LabelComponent lblpreguntaInd,lblespacio1,lblpregunta211,lblpregunta211_ind;
	
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoS1;
	SeccionCapitulo[] seccionesCargado,seccionesCargados2,seccionesCargadoS1,seccionesCargado2,seccionesCargado3,seccionesCargadomef,SeccionesCargadogemelos;
	public GridComponent2 botones; 
	
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;
	public ACTION_NACIMIENTO accion;
	public ACTION_NACIMIENTO siguienteaccion;
	public boolean dialogoAbierto = false;
	
	public CISECCION_02Fragment_003() {}
	public CISECCION_02Fragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218","ESTADOCAP2")};
		seccionesCargados2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"NRO_ORDEN_NINIO","QI478")};
		seccionesCargado2 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI218","QI219","QI220U","QI220N","QI220A","QI221","QI217CONS","ID","HOGAR_ID","PERSONA_ID","QI212_NOM") };
		seccionesCargado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		seccionesCargadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI208","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabadoS1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI221A","QI224")};
		seccionesCargadomef = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI203_A","QI203_B","QI205_A","QI205_B","QI207_A","QI207_B")};
		SeccionesCargadogemelos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y")};
		return rootView;
	}
  
  @Override 
  protected void buildFields() {
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_01_03qi_nacimiento).textSize(21).centrar().negrita();
		btnAgregar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 60);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(individual!=null && !dialogoAbierto) {
					//if(individual.qi208>detalles.size()) {
						agregarPersona(null);
						Util.cleanAndLockView(getActivity(), btnAgregar);
					/*}
					else {
						ToastMessage.msgBox(getActivity(), "No se puede agregar mas ni�os", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					}*/
				}
			}
		});
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta211 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_01_03qi211);
		lblpregunta211_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_01_03qi212_IND).negrita();
		
		
//		Spanned texto211 =Html.fromHtml("<b>ANOTE EL NOMBRE DE TODOS LOS HIJOS EN 212: LOS MELLIZOS Y TRILLIZOS A&Ntilde;ADELOS EN LINEAS SEPARADAS.\n SONDEE PARA DETERMINAR SI LA SE&Ntilde;ORA HA TENIDO MELLIZOS Y TRILLIZOS Y, DE SER EL CASO, SELECCIONE 2 EN 213 PARA FUTURA REFERENCIA</b>");
//		lblpregunta211_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
//		lblpregunta211_ind.setText(texto211);
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 750).headerHeight(altoComponente + 40).dataColumHeight(50);
//			
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(700, 790).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//			tcPersonas.addHeader(R.string.seccion01_estavivo, 0.53f,TableComponent.ALIGN.CENTER);
//		}
		
		tcPersonas.addHeader(R.string.seccion01_nro_ordens2, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01nombress2, 1f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01_fechanacimiento, 0.6f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01_estavivo, 0.45f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01_anioscumplidos, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01_conviviendo, 0.5f,TableComponent.ALIGN.CENTER);
		
		botones = new GridComponent2(this.getActivity(),App.ESTILO,1,0);
		botones.addComponent(btnAgregar);
		lblespacio1 = new LabelComponent(this.getActivity()).size(10,MATCH_PARENT);
    }
  

  
  @Override 
  protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT,lblpregunta211,lblpregunta211_ind);
		q2 = createQuestionSection(botones.component(), lblespacio1,tcPersonas.getTableView());
				
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		
		return contenedor;
  }
  
  
    public void agregarPersona(Integer indice) {
    	
    	//if(individual.qi208>detalles.size()) {
    		CISECCION_02 bean = new CISECCION_02();
    		bean.id = App.getInstance().getPersonaCuestionarioIndividual().id;
    		bean.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		bean.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    		if(indice==null)
    			bean.qi212 = detalles.size() + 1;
    		else 
    			bean.qi212 = indice;
    		
    		if(siguienteaccion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO)
    			abrirDetalle(bean,ACTION_NACIMIENTO.AGREGAR_INTERMEDIO);
    		else 
    			abrirDetalle(bean,ACTION_NACIMIENTO.AGREGAR);  
    	/*}
		else {
			siguienteaccion=null;
			ToastMessage.msgBox(getActivity(), "No se puede agregar mas ni�os", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
		}*/
    	
    	
    	Util.lockView(getActivity(), false, btnAgregar);
		
	}
  
    public void abrirDetalle(CISECCION_02 tmp,ACTION_NACIMIENTO accion) {
    	if(!dialogoAbierto) {
			FragmentManager fm = CISECCION_02Fragment_003.this.getFragmentManager();
			CISECCION_02Fragment_003Dialog aperturaDialog = CISECCION_02Fragment_003Dialog.newInstance(this, tmp,accion,detalles);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
			dialogoAbierto=true;
    	}
	}
      

    
    @Override 
    public boolean grabar() {
    	if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
    	}
    	try {
    		LLenarPreguntas221ay224();
			getCuestionarioService().saveOrUpdate(individual,seccionesGrabadoS1);
			//notificacion();
		} catch (SQLException e) {
			// TODO: handle exception
		}
    	CISECCION_02 seccion2 = getCuestionarioService().primerNacimientoCap2(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
    	if(seccion2!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null){
    		App.getInstance().getPersonaCuestionarioIndividual().qi215=seccion2.getQi215();
    	}
    return true;
    }
    public void LLenarPreguntas221ay224(){
    	
   	 CISECCION_02 ninio= getCuestionarioService().getUltimoNacimiento(individual.id,individual.hogar_id, individual.persona_id, seccionesCargados2);
   	 if (ninio!=null) {
   		individual.qi221a = (App.getInstance().getVisitaI().qivanio-ninio.qi215y)>=4?1:2;	
   		individual.qi224 = getCuestionarioService().getListaNacimientos4abyPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado2).size();
    	 
   	 }  
   }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esDiferente(individual.qi208,0)) {
			if (Util.esDiferente(individual.qi208,detalles.size())) {
				mensaje ="La cantidad de ni�os debe ser igual a total de la pregunta QI208";
				view = btnAgregar;
				error = true;
				return false;
			}
		}
		
		if(!getCuestionarioService().getSeccion2Completado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id) ) {
			 
			mensaje ="Debe completar todos los registros";
			view = btnAgregar;
			error = true;
			return false;
		}
		
		
		/*
		if (!registrosLlenos()) {
			mensaje ="Debe de ingresar el registro de todos los ni�os para continuar";
			view = btnAgregar;
			error = true;
			return false;
		}*/
		
		
		Integer hvivos=0,hmuertos=0,mvivas=0,mmuertas=0;
		HashMap<String,Integer> valores = getCuestionarioService().getValoresNacimmientos(App.getInstance().getMarco().id , App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		if(valores!=null) {
			hvivos = valores.get("HVIVOS");
			hmuertos = valores.get("HMUERTOS");
			mvivas = valores.get("MVIVAS");
			mmuertas = valores.get("MMUERTAS"); 
		}		
			
			Integer hijos_nac = hvivos+hmuertos;
			
			Integer i203b = individualmef.qi203_b == null ? 0 : individualmef.qi203_b;
			Integer i205b = individualmef.qi205_b == null ? 0 : individualmef.qi205_b;
			Integer i207b = individualmef.qi207_b == null ? 0 : individualmef.qi207_b;
			
			Integer hijos_dec = i203b+i205b+i207b;
			
			/*
			if(!(hijos_nac == hijos_dec)) {
				mensaje = "La cantidad de hijos declarados no coincide o falta completar datos";
				error = true;
				view = btnAgregar;
				
				return false;
			}*/

			mvivas = mvivas == null ? 0 : mvivas;
			mmuertas = mmuertas == null ? 0 : mmuertas;
			
			Integer hijas_nac = mvivas+mmuertas;
			
			Integer i203a = individualmef.qi203_a == null ? 0 : individualmef.qi203_a;
			Integer i205a = individualmef.qi205_a == null ? 0 : individualmef.qi205_a;
			Integer i207a = individualmef.qi207_a == null ? 0 : individualmef.qi207_a;
			
			Integer hijas_dec = i203a+i205a+i207a;
			
			/*
			if(!(hijas_nac == hijas_dec)) {
				mensaje = "La cantidad de hijas declaradas no coincide";
				error = true;
				view = btnAgregar;				
				return false;
			}*/
				
			Integer hijos_viv = hvivos;
			
			hijos_dec = i203b+i205b;
			if(!(hijos_viv == hijos_dec)) {
				mensaje = "La cantidad de hijos vivos declarados "+hijos_dec+" no coincide con "+hijos_viv;
				error = true;
				view = btnAgregar;				
				return false;
			}
			
			Integer hijos_mue = hmuertos;
			
			hijos_dec = i207b;
			if(!(hijos_mue == hijos_dec)) {
				mensaje = "La cantidad de hijos muertos declarados "+ hijos_dec+" no coincide con "+hijos_mue;
				error = true;
				view = btnAgregar;					
				return false;
			}			
			
			Integer hijas_viv = mvivas;
					
			hijas_dec = i203a+i205a;
			if(!(hijas_viv == hijas_dec)) {
				mensaje = "La cantidad de hijas vivas declaradas "+ hijas_dec+ "no coincide con "+hijas_viv;
				error = true;
				view = btnAgregar;				
				return false;
			}						
				
			Integer hijas_mue = mmuertas;
												
			hijas_dec = i207a;
			if(!(hijas_mue == hijas_dec)) {
				mensaje = "La cantidad de hijas muertas declaradas "+hijas_dec+"no coincide con "+hijas_mue;
				error = true;
				view = btnAgregar;					
				return false;
			}
			List<CISECCION_02> gemelos= getCuestionarioService().getFechasdegemelos(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, SeccionesCargadogemelos);
//			
//			if(gemelos.size()>0){
//				for(CISECCION_02 ninio: gemelos){
//				}
//			}
		return true;
    }
    
   /* public void notificacion() { 
    	if(! getCuestionarioService().getSeccion2Completado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id) ) {
			 
			String msg = "Debe completar todos los registros";
			ToastMessage.msgBox(this.getActivity(), msg, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			 
		}
			
    }*/
    
   /* public boolean registrosLlenos() {
    	boolean valor =false;
    	
    	for(CISECCION_02 entidad:detalles) {
    		if (entidad.getQi215().isEmpty()) {
    			valor = false;
    			break;
    		}
    		else {
    			valor = true;
    		}
    	}
    	
    	return valor;
    }*/
    
    @Override 
    public void cargarDatos() {
    	if(App.getInstance().getHogar()!=null){
    		
    		individualmef = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadomef);
    		
			cargarTabla();
			inicio();
		}	
    }
    
    public void refrescarPersonas(CISECCION_02 personas) {
		//if (detalles.contains(personas)) {
			cargarTabla();
			InsertarRegistros();
		/*	return;
		} else {
			detalles.add(personas);
			cargarTabla();
			InsertarRegistros();
		}	*/
	}    
    
    public void cargarTabla() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1);
 		detalles = getCuestionarioService().getListaNacimientosCompletobyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		App.getInstance().getPersonaCuestionarioIndividual().total_ninios = detalles.size();
		Log.d("Mensaje","total ninios "+App.getInstance().getPersonaCuestionarioIndividual().total_ninios);
		List<CISECCION_02> detalles2=null;
		detalles2 = getCuestionarioService().getNacimientosVivoListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
		App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos = detalles2.size();
		Log.d("Mensaje","total ninios vivos "+App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos);
		
		SeccionCapitulo[] seccionesCargadet3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID")};
		List<CISECCION_02> detalles3=null;
		detalles3 = getCuestionarioService().getNacimientosMayor2011ListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadet3);
			
		App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a = detalles3.size();
		
		tcPersonas.setData(detalles, "getQi212", "getQi212_nom","getQi215","getQi216","getQi217","getQi218"); 
		
		for (int row = 0; row < detalles.size(); row++) {
			if (obtenerEstado(detalles.get(row)) == 1) {
				// borde de color azul
				tcPersonas.setBorderRow(row, true);
			} else if (obtenerEstado(detalles.get(row)) == 2) {
				// borde de color rojo
				tcPersonas.setBorderRow(row, true, R.color.red);
			} else {
				tcPersonas.setBorderRow(row, false);
			}
		}

		tcPersonas.reloadData();
		
		registerForContextMenu(tcPersonas.getListView());
	}
    
    private int obtenerEstado(CISECCION_02 detalle) {
		if (!Util.esDiferente(detalle.estadocap2, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalle.estadocap2,1)) {
			return 2;
		}
		return 0;
	}
    
    public void validarPregunta208() {
 		if (!Util.esDiferente(individual.qi208,0)) {
 			q0.setVisibility(View.GONE);
 			q1.setVisibility(View.GONE);
 			q2.setVisibility(View.GONE);
 		} else {
 			q0.setVisibility(View.VISIBLE);
 			q1.setVisibility(View.VISIBLE);
 			q2.setVisibility(View.VISIBLE);
 		}
     }

    
    private void inicio() {
    	validarPregunta208();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_02 c = (CISECCION_02) detalles.get(arg2);
			if(siguienteaccion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO)
				abrirDetalle(c,ACTION_NACIMIENTO.AGREGAR_INTERMEDIO);
			else 
			{				
				if (Util.esDiferente(individual.qi208,0)) {
					/*if (Util.esDiferente(individual.qi208,detalles.size())) {
						ToastMessage.msgBox(getActivity(), "La cantidad de ni�os debe ser igual a total de la pregunta QI208", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					}
					else { */
						if(c.getQi212()!=1) {
							Integer indice = c.getQi212()-2;
							CISECCION_02 entidad = detalles.get(indice);
							//CISECCION_02 entidad =  getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesPersonas);
							if (entidad.getQi215().isEmpty()) {
								ToastMessage.msgBox(getActivity(), "Primero debe ingresar el registro anterior", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
							}
							else {
								abrirDetalle(c,ACTION_NACIMIENTO.EDITAR);
							}
						}
						else {
							abrirDetalle(c,ACTION_NACIMIENTO.EDITAR);
						}
						//abrirDetalle(c,ACTION_NACIMIENTO.EDITAR);
					//}
				}	
				
				
				
			}
		}
	}
    
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcPersonas.getListView())) {
			menu.setHeaderTitle("Opciones");
			menu.add(1, 0, 1, "Editar Registro");
			menu.add(1, 1, 1, "Eliminar Registro");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = detalles.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			
			CISECCION_02 seleccion = (CISECCION_02) info.targetView.getTag();
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				menu.getItem(0).setVisible(false);
				menu.getItem(1).setVisible(false);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				if(siguienteaccion == ACTION_NACIMIENTO.AGREGAR_INTERMEDIO)
					abrirDetalle((CISECCION_02) detalles.get(info.position),ACTION_NACIMIENTO.AGREGAR_INTERMEDIO);					
				else  {
					if (Util.esDiferente(individual.qi208,0)) {
						/*if (Util.esDiferente(individual.qi208,detalles.size())) {
							ToastMessage.msgBox(getActivity(), "La cantidad de ni�os debe ser igual a total de la pregunta QI208", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						}
						else {*/
							CISECCION_02 c = (CISECCION_02) detalles.get(info.position);
							if(c.getQi212()!=1) {
								Integer indice = c.getQi212()-2;
								CISECCION_02 entidad = detalles.get(indice);
								//CISECCION_02 entidad =  getCuestionarioService().getSeccion01_03Nacimiento(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,seccionesPersonas);
								if (entidad.getQi215().isEmpty()) {
									ToastMessage.msgBox(getActivity(), "Primero debe ingresar el registro anterior", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
								}
								else {
									abrirDetalle(c,ACTION_NACIMIENTO.EDITAR);
								}
							}
							else {
								abrirDetalle(c,ACTION_NACIMIENTO.EDITAR);
							}
							
						//}
					}	
					
				}
				break;
			case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_02) detalles.get(info.position));
				dialog.showDialog();
				break;

			}
		}
		return super.onContextItemSelected(item);
	}
    
    
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub

		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		CISECCION_02 bean = (CISECCION_02) dialog.get("seleccion");
		try {
			detalles.remove(bean);
			boolean borrado = false;
//			borrado = getCuestionarioService().BorrarNacimiento(bean.id, bean.hogar_id, bean.persona_id, bean.qi212);
			borrado = getCuestionarioService().BorrarNacimiento(bean);
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	}
	
	public void InsertarRegistros(){
		Log.e("Entro d registro","ddd");
      List<CISECCION_02> listado=getCuestionarioService().getNacimientosListbyPersonaDIT(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
     
      Integer contador=1;
    	CISECCION_04DIT_02 ninioingresa=null;
    	
    	List<CISECCION_04DIT_02> listadit=new ArrayList<CISECCION_04DIT_02>();
    	
    	for(CISECCION_02 ninio:listado){
    		if(ninio.qi215y!=null && ninio.qi215m.toString()!=null && ninio.qi215d!=null) {
	    		Calendar fn =new GregorianCalendar(ninio.qi215y, Integer.parseInt(ninio.qi215m.toString())-1,Integer.parseInt(ninio.qi215d.toString()));
	    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    		java.util.Date fechaactual=null;
	    		try {
					fechaactual=df.parse(ninio.qi217cons);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
	    		Calendar cal = Calendar.getInstance();
				cal.setTime(fechaactual);
				
	    		Integer meses=  MyUtil.CalcularEdadEnMesesFelix(fn, cal); 
	    		
	    		Log.e("","NINIO: "+ninio.qi212+" EDAD EN MESESss: "+meses);
	    		if((meses>=9 && meses<=12) ||
	    				(meses>=13 && meses<=18)||
	    				(meses>=19 && meses<=23)||
	    				(meses>=24 && meses<=36)||
	    				(meses>=37 && meses<=54)||
	    				(meses>=55 && meses<=71)
	    				){
	    			
	    			Log.e("","NINIO INGRESO: "+ninio.qi212+" INGRESO CON MESES: "+meses);
	    			
	    			
	    			ninioingresa=new CISECCION_04DIT_02();
	    			ninioingresa.id=App.getInstance().getPersonaCuestionarioIndividual().id;
	    			ninioingresa.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
	    			ninioingresa.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    			ninioingresa.nro_orden_ninio=ninio.qi212;
	    				
	    			Integer rultimo = MyUtil.getRangoDit(meses);
	    			SeccionCapitulo[] seccionesCargadoDit_02 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","QI478","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
	    			CISECCION_04DIT_02 ninioDit_02 = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,ninio.qi212,seccionesCargadoDit_02);
	    			
	    			  	
	    			Integer ranterior = 0;
	    			if(ninioDit_02!=null && ninioDit_02.qi478!=null) 
	    			 ranterior = MyUtil.getRangoDit(ninioDit_02.qi478);
	    			
	    			if(rultimo!=ranterior) {
	    				seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"NRO_ORDEN_NINIO","QI478","QI478E1", "QI478E2", "QI478E3", "QI478E4", "QI478E5", "QI478E6", "QI478E7", "QI478E8", "QI478E9", "QI478E10", "QI478F1", "QI478F2_A", "QI478F2_B", "QI478F2_C", "QI478F2_D", "QI478F2_E", "QI478F3", "QI478F4", "QI478F5", "QI478F6", "QI478G1", "QI478G2_A", "QI478G2_B", "QI478G2_C", "QI478G3", "QI478G4","QI478H1", "QI478H2", "QI478H3", "QI478H4", "QI478H5", "QI478H6", "QI478H7", "QI478H8_A", "QI478H8_B", "QI478H9","QI478H10","QI478H11","QI478H12","QI478I1", "QI478I2", "QI478I3", "QI478I4_A", "QI478I4_B", "QI478I5", "QI478I6", "QI478I7", "QI478I8", "QI478J1", "QI478J2", "QI478J3", "QI478J4_A", "QI478J4_B", "QI478J5", "QI478J6", "QI478J7", "QI478J8")};
	    			}
	    			else{
	    				seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"NRO_ORDEN_NINIO","QI478")};
	    			}
	    			
	    			ninioingresa.qi478 =meses;
	    			Log.e("","NINIO INGRESO: "+ninio.qi212+" ninioingresa.qi478: "+ninioingresa.qi478);
	    			try {
						getCuestionarioService().saveOrUpdate(ninioingresa, seccionesGrabado);
						listadit.add(ninioingresa);
					} catch (SQLException e) {
						// TODO: handle exception
						e.printStackTrace();	
						ToastMessage.msgBox(getActivity(), "ERROR: SECCIONDIT " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
					}
	
	    		}
	    		meses=0;
        	}
        }
        	
       SeccionCapitulo[] seccionesCargadoDIT_02 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","QI212_NOM","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO","QI478") };
            
       List<CISECCION_04DIT_02> listadit2_02 = getCuestionarioService().getListadoDit_02(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoDIT_02);
            
       for(CISECCION_04DIT_02 niniodit_02: listadit2_02) {
    	   boolean existe=false;
           for(CISECCION_04DIT_02 ninio:listadit){
        	   if(niniodit_02.nro_orden_ninio == ninio.nro_orden_ninio) {
        		   existe=true;
            		break;
    	        	}
            	}
            	if(!existe) {
            		getCuestionarioService().borrar04bDit_02(niniodit_02.id, niniodit_02.hogar_id, niniodit_02.persona_id, niniodit_02.nro_orden_ninio);
            	}	
            }
            
    	List<CISECCION_04DIT_02> detalles4=null;
    	detalles4 = getCuestionarioService().getListadoDit_02(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoDIT_02);
    	App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4dit = detalles4.size();	
    	
    		
    		/*
    		Log.e("","NINIO: "+ninio.qi212+" EDAD EN MESES: "+meses);
    		if((meses>=9 && meses<=12) ||
    				(meses>=15 && meses<=18)||
    				(meses>=30 && meses<=36)||
    				(meses>=53 && meses<=59)){
    			ninioingresa=new CISECCION_04DIT();
    			ninioingresa.id=App.getInstance().getPersonaCuestionarioIndividual().id;
    			ninioingresa.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    			ninioingresa.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    			ninioingresa.nro_orden_ninio=ninio.qi212;
    				
    			Integer rultimo = MyUtil.getRangoDit(meses);
    			SeccionCapitulo[] seccionesCargadoDit = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","QI478","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
    			CISECCION_04DIT ninioDit = getCuestionarioService().getCISECCION_04B_2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,ninio.qi212,seccionesCargadoDit);
    			
    			  	
    			Integer ranterior = 0;
    			if(ninioDit!=null && ninioDit.qi478!=null) 
    			 ranterior = MyUtil.getRangoDit(ninioDit.qi478);
    			
    			if(rultimo!=ranterior) {
    				seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"NRO_ORDEN_NINIO","QI478","QI478A1","QI478A2","QI478A3","QI478A4","QI478A5","QI478A6","QI478A7","QI478A8","QI478A9A","QI478A9B","QI478A10","QI478B1","QI478B2","QI478B3","QI478B4","QI478B5","QI478B6","QI478B7","QI478B8","QI478B9A","QI478B9B","QI478B9C","QI478B9D","QI478B9E","QI478B10","QI478C1","QI478C2","QI478C3","QI478C4","QI478C5","QI478C6","QI478C7","QI478C8","QI478C9","QI478C10","QI478D1","QI478D2","QI478D3","QI478D4","QI478D5","QI478D6","QI478D7","QI478D8","QI478D9A","QI478D9B","QI478D10")};
    			}
    			else{
    				seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"NRO_ORDEN_NINIO","QI478")};
    			}
    			
    			ninioingresa.qi478 =meses;
    			try {
					getCuestionarioService().saveOrUpdate(ninioingresa, seccionesGrabado);
					listadit.add(ninioingresa);
				} catch (SQLException e) {
					// TODO: handle exception
					e.printStackTrace();	
					ToastMessage.msgBox(getActivity(), "ERROR: SECCIONDIT " + e.getMessage(),
							ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				}

    		}
    		meses=0;
    		
    	}
    	}
    	
        SeccionCapitulo[] seccionesCargadoDIT = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","QI212_NOM","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO","QI478") };
        
        List<CISECCION_04DIT> listadit2 = getCuestionarioService().getListadoDit(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoDIT);
        
        for(CISECCION_04DIT niniodit: listadit2) {
        	boolean existe=false;
        	for(CISECCION_04DIT ninio:listadit){
        		
        		if(niniodit.nro_orden_ninio == ninio.nro_orden_ninio) {
	        		existe=true;
        			break;
	        	}
        	}
        	if(!existe) {
        		getCuestionarioService().borrar04bDit(niniodit.id, niniodit.hogar_id, niniodit.persona_id, niniodit.nro_orden_ninio);
        	}	
        	
        }
        
	List<CISECCION_04DIT> detalles4=null;
	detalles4 = getCuestionarioService().getListadoDit(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoDIT);
	App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4dit = detalles4.size();	
	
    		*/
    		
    	
    
	}	
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		Util.cleanAndLockView(getActivity(), btnAgregar);
    	}
    }
    
    
	@Override
	public Integer grabadoParcial() {		
    	try {
    		LLenarPreguntas221ay224();
			getCuestionarioService().saveOrUpdate(individual,seccionesGrabadoS1);
		} catch (SQLException e) {
			// TODO: handle exception
		}
    return App.INDIVIDUAL;
    }

}