package gob.inei.endes2024.fragment.CIseccion_01_03;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_008 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI327D;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQI327D_O;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI328;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI330;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI331;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI332;
	@FieldAnnotation(orderIndex=7)
	public TextAreaField txtQIOBS_S3;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta327D,lblpregunta328,lblpregunta330,lblpregunta331,lblpregunta332,lblpregunta321A_1_Ind,lblpregunta321A_2_Ind,lblpregunta321A_3_Ind,lblpregunta321A_4_Ind,lblpregunta321A_5_Ind,lblobs_s3;
	LinearLayout q0,q1,q2,q3,q4,q5,q6;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCarga;
	List<CISECCION_02> detalles=null;
	private Calendar fecha_actual =null; 
	public CISECCION_03Fragment_008() {}
	public CISECCION_03Fragment_008 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI327D","QI327D_O","QI328","QI330","QI331","QI332","QIOBS_S3","QI106","QI226","QI230","QI304","QI316Y","QI315Y","QI320","QI325A","QI326","QI327B","QI327C","QICAL_CONS","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI327D","QI327D_O","QI328","QI330","QI331","QI332","QIOBS_S3")};
		
		seccionesCarga = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
    }
    @Override
    protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta327D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi327d);
		lblpregunta328 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi328);
		lblpregunta330 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi330);
		lblpregunta331 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi331);
		lblpregunta332 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi332);
		
		Spanned texto321A_1 =Html.fromHtml("<b>RAZONES RELACIONADAS CON FECUNDIDAD</b>");
		lblpregunta321A_1_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_1_Ind.setText(texto321A_1);
		Spanned texto321A_2 =Html.fromHtml("<b>OPOSICI�N A USAR</b>");
		lblpregunta321A_2_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_2_Ind.setText(texto321A_2);
		Spanned texto321A_3 =Html.fromHtml("<b>FALTA DE CONOCIMIENTO</b>");
		lblpregunta321A_3_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_3_Ind.setText(texto321A_3);
		Spanned texto321A_4 =Html.fromHtml("<b>RAZONES RELACIONADAS CON EL M�TODO</b>");
		lblpregunta321A_4_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_4_Ind.setText(texto321A_4);
		Spanned texto321A_5 =Html.fromHtml("<b>RAZONES DE ACCESO/COSTO</b>");
		lblpregunta321A_5_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_5_Ind.setText(texto321A_5);
		
		rgQI327D=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi327d_1,R.string.ciseccion_03qi327d_2,R.string.ciseccion_03qi327d_3,R.string.ciseccion_03qi327d_4,R.string.ciseccion_03qi327d_5,R.string.ciseccion_03qi327d_6,R.string.ciseccion_03qi327d_7,R.string.ciseccion_03qi327d_8,R.string.ciseccion_03qi327d_9,R.string.ciseccion_03qi327d_10,R.string.ciseccion_03qi327d_11,R.string.ciseccion_03qi327d_12,R.string.ciseccion_03qi327d_13,R.string.ciseccion_03qi327d_14,R.string.ciseccion_03qi327d_15,R.string.ciseccion_03qi327d_16,R.string.ciseccion_03qi327d_17,R.string.ciseccion_03qi327d_18,R.string.ciseccion_03qi327d_19,R.string.ciseccion_03qi327d_20,R.string.ciseccion_03qi327d_21,R.string.ciseccion_03qi327d_22,R.string.ciseccion_03qi327d_23).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI327DChangeValue");
		txtQI327D_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI327D.agregarEspecifique(21,txtQI327D_O);
		rgQI327D.addView(lblpregunta321A_1_Ind,1);
		rgQI327D.addView(lblpregunta321A_2_Ind,10);
		rgQI327D.addView(lblpregunta321A_3_Ind,15);
		rgQI327D.addView(lblpregunta321A_4_Ind,18);
		rgQI327D.addView(lblpregunta321A_5_Ind,23);
		
		rgQI328=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi328_1,R.string.ciseccion_03qi328_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI330=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi330_1,R.string.ciseccion_03qi330_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI331=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi331_1,R.string.ciseccion_03qi331_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI331ChangeValue");
		rgQI332=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi332_1,R.string.ciseccion_03qi332_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblobs_s3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.obs_s3);
		txtQIOBS_S3 = new TextAreaField(getActivity()).maxLength(1000).size(200, 750).alfanumerico();
		
    }
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta327D,rgQI327D);
		q2 = createQuestionSection(lblpregunta328,rgQI328);
		q3 = createQuestionSection(lblpregunta330,rgQI330);
		q4 = createQuestionSection(lblpregunta331,rgQI331);
		q5 = createQuestionSection(lblpregunta332,rgQI332);
		q6 = createQuestionSection(lblobs_s3,txtQIOBS_S3);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi327d!=null)
			individual.qi327d=individual.getConvertQi327d(individual.qi327d);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
		App.getInstance().getPersonaCuestionarioIndividual().qi106=individual.qi106;
		detalles = getCuestionarioService().getNacimientosMayor2011ListbyPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id, seccionesCarga);
		App.getInstance().getPersonaCuestionarioIndividual().total_ninios_4a   = detalles.size();
		App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 =  getCuestionarioService().getCompletadoSeccion0103CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit = getCuestionarioService().ExisteninioDe61Hasta71ParaDit(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		//		Log.e("ExisteninioDe61Hasta71ParaDit "," "+App.getInstance().getPersonaCuestionarioIndividual().ExisteninioDe61Hasta71ParaDit);
		}
		else{
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer int304 = individual.qi304==null?0:individual.qi304;
		Integer int320 = individual.qi320==null?0:individual.qi320;
    	Integer int326 = individual.qi326==null?0:individual.qi326;
    	Integer int327b = individual.qi327b==null?0:individual.qi327b;
    	Integer int327c = individual.qi327c==null?0:individual.qi327c;
    	Integer int327d = individual.qi327d==null?0:individual.qi327d;
    	
       	if (int320==0) {
    		if (Util.esVacio(individual.qi327d)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI327D");
    			view = rgQI327D;
    			error = true;
    			return false;
    		}
    		if(int327d==96){
    			if (Util.esVacio(individual.qi327d_o)) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI327D_O;
    				error = true;
    				return false;
    			}
    		}
    	}
       	
       	if (Util.esDiferente(individual.qi327d,42)) {
       		if (int320==0 || int320==10 || int320==11 || int320==12 || int320==13 || int320==96) {
    			if (Util.esVacio(individual.qi328)) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI328");
    				view = rgQI328;
    				error = true;
    				return false;
    			}
    		}
		}
 		
		if (Util.esVacio(individual.qi330)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI330");
			view = rgQI330;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi331)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI331");
			view = rgQI331;
			error = true;
			return false;
		}
		if (individual.qi331==1) {
			if (Util.esVacio(individual.qi332)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI332");
				view = rgQI332;
				error = true;
				return false;
			}
		}


		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		
    	if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		
    	if(individual.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individual.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
    	
    	App.getInstance().getPersonaCuestionarioIndividual().qi304=individual.qi304;
    	App.getInstance().getPersonaCuestionarioIndividual().qi325a=individual.qi325a;
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individual.qi320;
    	App.getInstance().getPersonaCuestionarioIndividual().qi315y=individual.qi315y;
    	App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
    	
    	if(individual.qi327d!=null)
			individual.qi327d=individual.setConvertQi327d(individual.qi327d);
		
		
		entityToUI(individual);
		inicio();
    }
    
    
    public void validarPreguntas(){
    	Integer in320 = individual.qi320==null?0:individual.qi320;
    	Integer in326 = individual.qi326==null?0:individual.qi326;
    	Integer in327b = individual.qi327b==null?0:individual.qi327b;
    	Integer in327c = individual.qi327c==null?-1:individual.qi327c;
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	if (int304==2 || in320==0) {
    		Log.e("sss","3");
			Util.lockView(getActivity(), false,rgQI327D,rgQI328,rgQI330,rgQI331,rgQI332);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onrgQI327DChangeValue();
			onrgQI331ChangeValue();
		}
    	else{
    		if ((in320==1 || in320==2 || in320==3 || in320==4 || in320==5 || in320==6 || in320==7 || in320==8 || in320==9) && (in327b!=1 || in327c >= 0)) {
        		Log.e("sss","1");
        		Util.cleanAndLockView(getActivity(),rgQI327D,rgQI328);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
        		Util.lockView(getActivity(), false,rgQI330,rgQI331,rgQI332);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			q5.setVisibility(View.VISIBLE);
    			onrgQI331ChangeValue();
        	}
    		if (in320==10 || in320==11 || in320==12 || in320==13 || in320==96) {
    			Log.e("sss","2");
        		Util.cleanAndLockView(getActivity(),rgQI327D);
        		q1.setVisibility(View.GONE);
        		Util.lockView(getActivity(), false,rgQI328,rgQI330,rgQI331,rgQI332);
    			q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			q5.setVisibility(View.VISIBLE);
    			onrgQI331ChangeValue();
    		}
    		
    	}
    }
    	
    
    private void inicio() {
    	validarPreguntas();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

 
    public void RenombrarEtiquetas() {
    	
    	lblpregunta330.text(R.string.ciseccion_03qi330);
    	
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fecha_actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecha_actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	
    	lblpregunta330.setText(lblpregunta330.getText().toString().replace("#", F2));
    	lblpregunta330.setText(lblpregunta330.getText().toString().replace("$", F1));
    	lblpregunta330.setText(lblpregunta330.getText().toString().replace("%", F3));
    }
    
    public void onrgQI327DChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(15,15,rgQI327D.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI328);
			q2.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI328);
			q2.setVisibility(View.VISIBLE);
			rgQI328.requestFocus();
		}
    }
    
    public void onrgQI331ChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,2,rgQI331.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI332);
			q5.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI332);
			q5.setVisibility(View.VISIBLE);
		}
    }

    
    public CuestionarioService getCuestionarioService() {
 		if(cuestionarioService==null){
 			cuestionarioService = CuestionarioService.getInstance(getActivity());
 		}
 		return cuestionarioService;
     }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI327D.readOnly();
    		rgQI328.readOnly();
    		rgQI330.readOnly();
    		rgQI331.readOnly();
    		rgQI332.readOnly();
    		txtQI327D_O.readOnly();
    		txtQIOBS_S3.setEnabled(false);
    	}
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi327d!=null)
			individual.qi327d=individual.getConvertQi327d(individual.qi327d);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}		
		App.getInstance().getPersonaCuestionarioIndividual().qi106=individual.qi106;
		return App.INDIVIDUAL;
	}
}