package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI304;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI307;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI310;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI311_A;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI311_B;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI311_C;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI311_D;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI311_E;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI311_F;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI311_G;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI311_H;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI311_I;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI311_J;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI311_K;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI311_L;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI311_M;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI311_X;
	@FieldAnnotation(orderIndex=18)
	public TextField txtQI311_O;
	@FieldAnnotation(orderIndex=19)
	public TextField txtQI312_NOM;
	@FieldAnnotation(orderIndex=20)
	public RadioGroupOtherField rgQI312;
	@FieldAnnotation(orderIndex=21)
	public TextField txtQI312_O;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	public GridComponent2 gdHijos,gdLugarSalud;
	Spanned texto311A,texto311;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta304,lblpregunta307,lblpregunta310,lblpregunta311,lblpregunta312,lblhijos,lbllugar,lblpregunta312_Pu_Ind,lblpregunta312_Pri_Ind,lblpregunta312_Org_Ind,lblpregunta312_ind;
	LinearLayout q0,q1,q2,q3,q4,q5,q6;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	private Calendar fecha_actual =null; 
	public CISECCION_03Fragment_001() {}
	public CISECCION_03Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI307, 0, 24, null, 99);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226","QI230","QI302_01","QI302_02","QI302_03","QI302_04","QI302_05A","QI302_05B","QI302_06","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI304","QI307","QI310","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X","QI311_O","QI312_NOM","QI312","QI312_O","QICAL_CONS","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI304","QI307","QI310","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X","QI311_O","QI312_NOM","QI312","QI312_O","QI320","QI320_O")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta304 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi304);
		lblpregunta307 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi307);
		lblpregunta310 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi310);
		lblpregunta311 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi311);
		lblpregunta312 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi312);
		lblpregunta312_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_03qi312_ind);
		
		Spanned texto312Pu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta312_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta312_Pu_Ind.setText(texto312Pu);
		Spanned texto312Pri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta312_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta312_Pri_Ind.setText(texto312Pri);
		Spanned texto312Org =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta312_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta312_Org_Ind.setText(texto312Org);
		
		texto311 =Html.fromHtml("311. �Qu� est�n haciendo o usando para evitar quedar embarazada?");
		texto311A =Html.fromHtml("311A. SELECCIONE EN \"A\" PARA LA ESTERILIZACI�N FEMENINA.");
		
		
		rgQI304=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi304_1,R.string.ciseccion_03qi304_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI304ChangeValue");
		txtQI307=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		rgQI310=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi310_1,R.string.ciseccion_03qi310_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI310ChangeValue");
		chbQI311_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_AChangeValue");
		chbQI311_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_BChangeValue");
		chbQI311_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_CChangeValue");
		chbQI311_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_DChangeValue");
		chbQI311_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_EChangeValue");
		chbQI311_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_FChangeValue");
		chbQI311_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_GChangeValue");
		chbQI311_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_HChangeValue");
		chbQI311_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_IChangeValue");
		chbQI311_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_JChangeValue");
		chbQI311_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_KChangeValue");
		chbQI311_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_LChangeValue");
		chbQI311_M=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_MChangeValue");
		chbQI311_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi311_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI311_XChangeValue");
		txtQI311_O=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 500);
		txtQI312_NOM=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 440);
	
		rgQI312=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi312_1,R.string.ciseccion_03qi312_2,R.string.ciseccion_03qi312_3,R.string.ciseccion_03qi312_4,R.string.ciseccion_03qi312_5,R.string.ciseccion_03qi312_6,R.string.ciseccion_03qi312_7,R.string.ciseccion_03qi312_8,R.string.ciseccion_03qi312_9,R.string.ciseccion_03qi312_10,R.string.ciseccion_03qi312_11,R.string.ciseccion_03qi312_12,R.string.ciseccion_03qi312_13).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI312_O=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 500).alfanumerico();
		rgQI312.agregarEspecifique(11,txtQI312_O);
		rgQI312.addView(lblpregunta312_Pu_Ind,0);
		rgQI312.addView(lblpregunta312_Pri_Ind,8);
		rgQI312.addView(lblpregunta312_Org_Ind,11);
		
		lblhijos  = new LabelComponent(this.getActivity()).size(altoComponente, 200).text(R.string.ciseccion_03qi307_hijos).textSize(16).centrar();
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.ciseccion_03qi312_nom).textSize(16);
		
		
		gdHijos = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdHijos.addComponent(lblhijos);
		gdHijos.addComponent(txtQI307);
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI312_NOM);
		
    }
  
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta304,rgQI304);
		q2 = createQuestionSection(lblpregunta307,gdHijos.component());
		q3 = createQuestionSection(lblpregunta310,rgQI310);
		LinearLayout ly311 = new LinearLayout(getActivity());
		ly311.addView(chbQI311_X);
		ly311.addView(txtQI311_O);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta311,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,ly311);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta312,lblpregunta312_ind,gdLugarSalud.component(),rgQI312);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			individual.qi312=individual.qi312!=null?individual.getConvertqi312(individual.qi312):null;
			grabarPregunta320();
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi310 = individual.qi310;
			App.getInstance().getPersonaCuestionarioIndividual().qi320 = individual.qi320;
			App.getInstance().getPersonaCuestionarioIndividual().qi304 = individual.qi304;
			App.getInstance().getPersonaCuestionarioIndividual().qi307 = individual.qi307;
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		if(individual!=null && individual.id!=null && individual.hogar_id!=null && individual.persona_id!=null && fecha_actual!=null){
			Log.e("DATA: ","DE");
			EndesCalendario.VerificacionCalendarioColumnas_01_03(individual.id, individual.hogar_id, individual.persona_id, fecha_actual);
		}
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer int302_1=individual.qi302_01==null?0:individual.qi302_01;
    	Integer int302_2=individual.qi302_02==null?0:individual.qi302_02;
    	Integer int302_3=individual.qi302_03==null?0:individual.qi302_03;
    	Integer int302_4=individual.qi302_04==null?0:individual.qi302_04;
//    	Integer int302_5=individual.qi302_05==null?0:individual.qi302_05;
    	Integer int302_5=0;
    	if(individual.qi302_05a!=null && individual.qi302_05b!=null){
    		if(!Util.esDiferente(individual.qi302_05a, 1) || !Util.esDiferente(individual.qi302_05b, 1)){
    			int302_5=1;
    		}
    		else{
    			int302_5=2;
    		}
    	}
    	else{
    		int302_5=0;
    	}
//    	Integer int302_5=individual.qi302_05a==null?0:individual.qi302_05a;
    	Integer int302_6=individual.qi302_06==null?0:individual.qi302_06;
    	Integer int302_7=individual.qi302_07==null?0:individual.qi302_07;
    	Integer int302_8=individual.qi302_08==null?0:individual.qi302_08;
    	Integer int302_9=individual.qi302_09==null?0:individual.qi302_09;
    	Integer int302_10=individual.qi302_10==null?0:individual.qi302_10;
    	Integer int302_11=individual.qi302_11==null?0:individual.qi302_11;
    	Integer int302_12=individual.qi302_12==null?0:individual.qi302_12;
    	Integer int302_13=individual.qi302_13==null?0:individual.qi302_13;
    	Integer int302_14=individual.qi302_14==null?0:individual.qi302_14;
    	Integer int312=individual.qi312==null?0:individual.qi312;
    	Integer int304=individual.qi304==null?0:individual.qi304;
    	Integer int310=individual.qi310==null?0:individual.qi310;
    	if (int302_1==2&&int302_2==2&&int302_3==2&&int302_4==2&&int302_5==2&&int302_6==2&&int302_7==2&&int302_8==2&&int302_9==2&&int302_10==2&&int302_11==2&&int302_12==2&&int302_13==2&&int302_14==2) {
    		if (Util.esVacio(individual.qi304)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI304");
			view = rgQI304;
			error = true;
			return false;
			}
    		if (individual.qi304==1) {
    			mensaje = "�Qu� ha(n) usado o qu� ha(n) hecho?\nCORRIJA 302 Y 303";
				view = rgQI304;
				error = true;
				return false;
    		}
		}
    	if (int304!=2) {
    		if (Util.esVacio(individual.qi307)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI307");
    			view = txtQI307;
    			error = true;
    			return false;
    		}
    		if(Util.esMayor(individual.qi307,App.getInstance().getPersonaCuestionarioIndividual().total_ninios)){
    			mensaje = "El valor no puede ser mayor al total de ni�os: "+App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
    			view = txtQI307;
    			error = true;
    			return false;
    		}
    		Integer int226=individual.qi226==null?0:individual.qi226;
    		if (int302_1!=1 && int226!=1) {
    			if (Util.esVacio(individual.qi310)) {
        			mensaje = preguntaVacia.replace("$", "La pregunta QI310");
        			view = rgQI310;
        			error = true;
        			return false;
        		}
			}
    		if (int226!=1 && int310!=2) {
    			if (!verificarCheck311()) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI311");
    				view = chbQI311_A;
    				error = true;
    				return false;
    			}
    			if (chbQI311_X.isChecked()) {
    				if (Util.esVacio(individual.qi311_o)) {
    					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    					view = txtQI311_O;
    					error = true;
    					return false;
    				}
    			}
    			if(!Util.esDiferente(individual.qi311_j, 1) && !Util.esDiferente(App.getInstance().getPersonaCuestionarioIndividual().total_ninios, 0)){
    				mensaje = "M�todo no pudo ser usado porque no tuvo hijos";
					view = txtQI311_O;
					error = true;
					return false;
    			}
    				
    			if (verificarCheck311_AB()) {
//    				if (Util.esVacio(individual.qi312_nom)) {
//    					mensaje = preguntaVacia.replace("$", "La pregunta QI312_NOM");
//    					view = txtQI312_NOM;
//    					error = true;
//    					return false;
//    				}
    				if (int312==0) {
    					mensaje = preguntaVacia.replace("$", "La pregunta QI312");
    					view = rgQI312;
    					error = true;
    					return false;
    				}
    				if(int312==12){
    					if (Util.esVacio(individual.qi312_o)) {
    						mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    						view = txtQI312_O;
    						error = true;
    						return false;
    					}
    				}
    			}
    		}
		}
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		individual.qi312=individual.qi312!=null?individual.setConvertqi312(individual.qi312):null;
		if(individual.qical_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individual.qical_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		entityToUI(individual);
		inicio();
    }
    private void inicio() {
    	validarPregunta302();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
//    public String Listado(){
//    	String data ="";
//    	String salto="\n";
//    	if(!Util.esDiferente(individual.qi302_01, 1))
//    		data = salto+data+getString(R.string.ciseccion_03qi302_1)+salto;
//    	if(!Util.esDiferente(individual.qi302_02, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_2)+salto;
//    	if(!Util.esDiferente(individual.qi302_03, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_3)+salto;
//    	if(!Util.esDiferente(individual.qi302_04, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_4)+salto;
////    	if(!Util.esDiferente(individual.qi302_05, 1))
////    		data=data+getString(R.string.ciseccion_03qi302_5)+salto;
//    	if(!Util.esDiferente(individual.qi302_06, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_6)+salto;
//    	if(!Util.esDiferente(individual.qi302_07, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_7)+salto;
//    	if(!Util.esDiferente(individual.qi302_08, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_8)+salto;
//    	if(!Util.esDiferente(individual.qi302_09, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_9)+salto;
//    	if(!Util.esDiferente(individual.qi302_10, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_10t)+salto;
//    	if(!Util.esDiferente(individual.qi302_11, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_11t)+salto;
//    	if(!Util.esDiferente(individual.qi302_12, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_12t)+salto;
//    	if(!Util.esDiferente(individual.qi302_13, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_13t)+salto;
//    	if(!Util.esDiferente(individual.qi302_14, 1))
//    		data=data+getString(R.string.ciseccion_03qi302_14t)+salto;
//    	return data;
//    }
    
    public void validarPregunta302(){
    	Integer int302_1=individual.qi302_01==null?0:individual.qi302_01;
    	Integer int302_2=individual.qi302_02==null?0:individual.qi302_02;
    	Integer int302_3=individual.qi302_03==null?0:individual.qi302_03;
    	Integer int302_4=individual.qi302_04==null?0:individual.qi302_04;
//    	Integer int302_5=individual.qi302_05==null?0:individual.qi302_05;
    	Integer int302_5=0;
    	if(individual.qi302_05a!=null && individual.qi302_05b!=null){
    		if(!Util.esDiferente(individual.qi302_05a, 1) || !Util.esDiferente(individual.qi302_05b, 1)){
    			int302_5=1;
    		}
    		else{
    			int302_5=2;
    		}
    	}
    	else{
    		int302_5=0;
    	}
    	Integer int302_6=individual.qi302_06==null?0:individual.qi302_06;
    	Integer int302_7=individual.qi302_07==null?0:individual.qi302_07;
    	Integer int302_8=individual.qi302_08==null?0:individual.qi302_08;
    	Integer int302_9=individual.qi302_09==null?0:individual.qi302_09;
    	Integer int302_10=individual.qi302_10==null?0:individual.qi302_10;
    	Integer int302_11=individual.qi302_11==null?0:individual.qi302_11;
    	Integer int302_12=individual.qi302_12==null?0:individual.qi302_12;
    	Integer int302_13=individual.qi302_13==null?0:individual.qi302_13;
    	Integer int302_14=individual.qi302_14==null?0:individual.qi302_14;
    	Integer int310=individual.qi310==null?0:individual.qi310;
    	Integer int312=individual.qi312==null?0:individual.qi312;
    	Integer int226=individual.qi226==null?0:individual.qi226;
    	lblpregunta311.setText(texto311);
    	if (int302_1==2&&int302_2==2&&int302_3==2&&int302_4==2&&int302_5==2&&int302_6==2&&int302_7==2&&int302_8==2&&int302_9==2&&int302_10==2&&int302_11==2&&int302_12==2&&int302_13==2&&int302_14==2) {
    		Util.lockView(getActivity(), false,rgQI304);
			q1.setVisibility(View.VISIBLE);
			Util.cleanAndLockView(getActivity(),txtQI307,rgQI310,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O,txtQI312_NOM,rgQI312,txtQI312_O);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		}else {
			Util.cleanAndLockView(getActivity(),rgQI304);
    		q1.setVisibility(View.GONE);
    		Util.lockView(getActivity(), false,txtQI307);
			q2.setVisibility(View.VISIBLE);
			if (int302_1==2) {
				if (int226==2 || int226==8) {
					Util.lockView(getActivity(), false,rgQI310);
					q3.setVisibility(View.VISIBLE);
					if (int310==2) {
						Util.cleanAndLockView(getActivity(),chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O,txtQI312_NOM,rgQI312,txtQI312_O);
						q4.setVisibility(View.GONE);
						q5.setVisibility(View.GONE);
					} else {
						Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
						q4.setVisibility(View.VISIBLE);
						q5.setVisibility(View.VISIBLE);
			    		if (int312==12)
							Util.lockView(getActivity(),false,txtQI312_O);
						else
							Util.cleanAndLockView(getActivity(),txtQI312_O);
			    		if (int302_1==2)
			    			Util.cleanAndLockView(getActivity(),chbQI311_A);
						else
							Util.lockView(getActivity(),false,chbQI311_A);
			    		
						if (int302_2==2)
							Util.cleanAndLockView(getActivity(),chbQI311_B);
						else
							Util.lockView(getActivity(),false,chbQI311_B);
						if (int302_3==2)
							Util.cleanAndLockView(getActivity(),chbQI311_C);
						else
							Util.lockView(getActivity(),false,chbQI311_C);
						if (int302_4==2)
							Util.cleanAndLockView(getActivity(),chbQI311_D);
						else
							Util.lockView(getActivity(),false,chbQI311_D);
						if (int302_5==2)
							Util.cleanAndLockView(getActivity(),chbQI311_E);
						else
							Util.lockView(getActivity(),false,chbQI311_E);
						if (int302_6==2)
							Util.cleanAndLockView(getActivity(),chbQI311_F);
						else
							Util.lockView(getActivity(),false,chbQI311_F);
						if (int302_7==2)
							Util.cleanAndLockView(getActivity(),chbQI311_G);
						else
							Util.lockView(getActivity(),false,chbQI311_G);
						if (int302_8==2)
							Util.cleanAndLockView(getActivity(),chbQI311_H);
						else
							Util.lockView(getActivity(),false,chbQI311_H);
						if (int302_9==2)
							Util.cleanAndLockView(getActivity(),chbQI311_I);
						else
							Util.lockView(getActivity(),false,chbQI311_I);
						if (int302_10==2)
							Util.cleanAndLockView(getActivity(),chbQI311_J);
						else
							Util.lockView(getActivity(),false,chbQI311_J);
						if (int302_11==2)
							Util.cleanAndLockView(getActivity(),chbQI311_K);
						else
							Util.lockView(getActivity(),false,chbQI311_K);
						if (int302_12==2)
							Util.cleanAndLockView(getActivity(),chbQI311_L);
						else
							Util.lockView(getActivity(),false,chbQI311_L);
						if (int302_13==2)
							Util.cleanAndLockView(getActivity(),chbQI311_M);
						else
							Util.lockView(getActivity(),false,chbQI311_M);
						if (int302_14==2)
							Util.cleanAndLockView(getActivity(),chbQI311_X);
						else
							Util.lockView(getActivity(),false,chbQI311_X);
						
						if (!chbQI311_X.isChecked()) {
				    		Util.cleanAndLockView(getActivity(),txtQI311_O);	
						}else {
							Util.lockView(getActivity(),false,txtQI311_O);
						}
					}
					if (!verificarCheck311_AB()) {
			        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
			    		q5.setVisibility(View.GONE);
			    	}else {
						Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
			    		q5.setVisibility(View.VISIBLE);
			    		if (int312==12)
			    			Util.lockView(getActivity(),false,txtQI312_O);
			    		else
			    			Util.cleanAndLockView(getActivity(),txtQI312_O);
					}
				}else {
					Util.cleanAndLockView(getActivity(),rgQI310,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O,txtQI312_NOM,rgQI312,txtQI312_O);
					q3.setVisibility(View.GONE);
					q4.setVisibility(View.GONE);
					q5.setVisibility(View.GONE);
				}
			}else {
				RenombrarLabels();
				Util.cleanAndLockView(getActivity(),rgQI310);
				q3.setVisibility(View.GONE);
				Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
				Util.cleanAndLockView(getActivity(),chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O);
				Util.lockView(getActivity(),false,chbQI311_A);
				if (int312==12)
					Util.lockView(getActivity(),false,txtQI312_O);
				else
					Util.cleanAndLockView(getActivity(),txtQI312_O);
			}
		}
    }
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    public void RenombrarLabels(){
    		lblpregunta311.setText(texto311A);
	}
    public void onrgQI310ChangeValue() {
    	Integer int302_1=individual.qi302_01==null?0:individual.qi302_01;
    	Integer int302_2=individual.qi302_02==null?0:individual.qi302_02;
    	Integer int302_3=individual.qi302_03==null?0:individual.qi302_03;
    	Integer int302_4=individual.qi302_04==null?0:individual.qi302_04;
//    	Integer int302_5=individual.qi302_05==null?0:individual.qi302_05;
    	Integer int302_5=0;
    	if(individual.qi302_05a!=null && individual.qi302_05b!=null){
    		if(!Util.esDiferente(individual.qi302_05a, 1) || !Util.esDiferente(individual.qi302_05b, 1)){
    			int302_5=1;
    		}
    		else{
    			int302_5=2;
    		}
    	}
    	else{
    		int302_5=0;
    	}
    	Integer int302_6=individual.qi302_06==null?0:individual.qi302_06;
    	Integer int302_7=individual.qi302_07==null?0:individual.qi302_07;
    	Integer int302_8=individual.qi302_08==null?0:individual.qi302_08;
    	Integer int302_9=individual.qi302_09==null?0:individual.qi302_09;
    	Integer int302_10=individual.qi302_10==null?0:individual.qi302_10;
    	Integer int302_11=individual.qi302_11==null?0:individual.qi302_11;
    	Integer int302_12=individual.qi302_12==null?0:individual.qi302_12;
    	Integer int302_13=individual.qi302_13==null?0:individual.qi302_13;
    	Integer int302_14=individual.qi302_14==null?0:individual.qi302_14;
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,2,rgQI310.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O,txtQI312_NOM,rgQI312,txtQI312_O);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		} else {
			Util.cleanAndLockView(getActivity(),txtQI311_O);
			if (int302_1==1) {
				Util.cleanAndLockView(getActivity(),chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI311_O);
				Util.lockView(getActivity(), false,chbQI311_A,txtQI312_NOM,rgQI312,txtQI312_O);
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
			}else {
				
				if (int302_1==2)
					Util.cleanAndLockView(getActivity(),chbQI311_A);
				else
					Util.lockView(getActivity(),false,chbQI311_A);
				if (int302_2==2)
					Util.cleanAndLockView(getActivity(),chbQI311_B);
				else
					Util.lockView(getActivity(),false,chbQI311_B);
				if (int302_3==2)
					Util.cleanAndLockView(getActivity(),chbQI311_C);
				else
					Util.lockView(getActivity(),false,chbQI311_C);
				if (int302_4==2)
					Util.cleanAndLockView(getActivity(),chbQI311_D);
				else
					Util.lockView(getActivity(),false,chbQI311_D);
				if (int302_5==2)
					Util.cleanAndLockView(getActivity(),chbQI311_E);
				else
					Util.lockView(getActivity(),false,chbQI311_E);
				if (int302_6==2)
					Util.cleanAndLockView(getActivity(),chbQI311_F);
				else
					Util.lockView(getActivity(),false,chbQI311_F);
				if (int302_7==2)
					Util.cleanAndLockView(getActivity(),chbQI311_G);
				else
					Util.lockView(getActivity(),false,chbQI311_G);
				if (int302_8==2)
					Util.cleanAndLockView(getActivity(),chbQI311_H);
				else
					Util.lockView(getActivity(),false,chbQI311_H);
				if (int302_9==2)
					Util.cleanAndLockView(getActivity(),chbQI311_I);
				else
					Util.lockView(getActivity(),false,chbQI311_I);
				if (int302_10==2)
					Util.cleanAndLockView(getActivity(),chbQI311_J);
				else
					Util.lockView(getActivity(),false,chbQI311_J);
				if (int302_11==2)
					Util.cleanAndLockView(getActivity(),chbQI311_K);
				else
					Util.lockView(getActivity(),false,chbQI311_K);
				if (int302_12==2)
					Util.cleanAndLockView(getActivity(),chbQI311_L);
				else
					Util.lockView(getActivity(),false,chbQI311_L);
				if (int302_13==2)
					Util.cleanAndLockView(getActivity(),chbQI311_M);
				else
					Util.lockView(getActivity(),false,chbQI311_M);
				if (int302_14==2)
					Util.cleanAndLockView(getActivity(),chbQI311_X);
				else
					Util.lockView(getActivity(),false,chbQI311_X);
			}
			q4.setVisibility(View.VISIBLE);
			if (int302_1==1 || int302_2==1) {
				Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
				q5.setVisibility(View.VISIBLE);
				if (rgQI312.getValue().toString().equals("12"))
					Util.lockView(getActivity(),false,txtQI312_O);
				else
					Util.cleanAndLockView(getActivity(),txtQI312_O);
			}else {
				Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
				q5.setVisibility(View.GONE);
			}
			

		}
    }
    public boolean verificarCheck311() {
    	  if (chbQI311_A.isChecked() || chbQI311_B.isChecked() || chbQI311_C.isChecked() || chbQI311_D.isChecked() || chbQI311_E.isChecked() || chbQI311_F.isChecked() ||chbQI311_G.isChecked() || chbQI311_H.isChecked() || chbQI311_I.isChecked() || chbQI311_J.isChecked() || chbQI311_K.isChecked() || chbQI311_L.isChecked() ||chbQI311_M.isChecked() ||chbQI311_X.isChecked()) {
    		  return true;
    	  }else{ 
    		  return false;
    	  }
    }
    public boolean verificarCheck311_AB() {
  	  if (chbQI311_A.isChecked() || chbQI311_B.isChecked()) {
  		  return true;
  	  }else{
  		  return false;
  	  }
    }
    public boolean verificarCheck311_CDEFGHIJKLMX() {
  	  if (chbQI311_C.isChecked() || chbQI311_D.isChecked() || chbQI311_E.isChecked() || chbQI311_F.isChecked() ||chbQI311_G.isChecked() || chbQI311_H.isChecked() || chbQI311_I.isChecked() || chbQI311_J.isChecked() || chbQI311_K.isChecked() || chbQI311_L.isChecked() ||chbQI311_M.isChecked() ||chbQI311_X.isChecked()) {
  		  return true;
  	  }else{
  		  return false;
  	  }
    }
    public void onchbQI311_AChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
		}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_BChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
		}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		String st312=rgQI312.getValue()==null?"0":rgQI312.getValue().toString();
    		Integer int312=Integer.valueOf(st312);
    		if (int312==12)
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
    			Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_CChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
		}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_DChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {    		
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
    		Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_EChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_FChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_GChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_HChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_IChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_JChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    	
    }
    public void onchbQI311_KChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_LChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
			if (rgQI312.getValue().toString().equals("12"))
				Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		q5.setVisibility(View.VISIBLE);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_MChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onchbQI311_XChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (chbQI311_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI311_O);
  			txtQI311_O.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI311_O);
  		}
    	if (!verificarCheck311_AB()) {
        	Util.cleanAndLockView(getActivity(),txtQI312_NOM,rgQI312,txtQI312_O);
    		q5.setVisibility(View.GONE);
    	}else {
			Util.lockView(getActivity(), false,txtQI312_NOM,rgQI312);
    		q5.setVisibility(View.VISIBLE);
    		if (rgQI312.getValue().toString().equals("12"))
    			Util.lockView(getActivity(),false,txtQI312_O);
			else
				Util.cleanAndLockView(getActivity(),txtQI312_O);
    		txtQI312_NOM.requestFocus();
		}
    }
    public void onrgQI304ChangeValue() {
    	MyUtil.LiberarMemoria();
    	if (MyUtil.incluyeRango(2,2,rgQI304.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI307,rgQI310,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI312_NOM,rgQI312,txtQI312_O);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,txtQI307,rgQI310,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI312_NOM,rgQI312);
			Util.cleanAndLockView(getActivity(),txtQI307,rgQI310,chbQI311_A,chbQI311_B,chbQI311_C,chbQI311_D,chbQI311_E,chbQI311_F,chbQI311_G,chbQI311_H,chbQI311_I,chbQI311_J,chbQI311_K,chbQI311_L,chbQI311_M,chbQI311_X,txtQI312_NOM,rgQI312,txtQI312_O);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		}
    }
    public void grabarPregunta320(){
    	individual.qi320=CodigoCorrecto();
    	if(!Util.esDiferente(individual.qi320, 96)){
    		individual.qi320_o=individual.qi311_o;
    	}
    }
    public Integer CodigoCorrecto(){
    	if(individual.qi311_a!=null && !Util.esDiferente(individual.qi311_a,1))
    		return 1;
    	if(individual.qi311_b!=null && !Util.esDiferente(individual.qi311_b,1))
    		return 2;
    	if(individual.qi311_c!=null && !Util.esDiferente(individual.qi311_c,1))
    		return 3;
    	if(individual.qi311_d!=null && !Util.esDiferente(individual.qi311_d,1))
	    	return 4; 
    	if(individual.qi311_e!=null && !Util.esDiferente(individual.qi311_e,1))
	    	return 5; 
    	if(individual.qi311_f!=null && !Util.esDiferente(individual.qi311_f,1))
	    	return 6; 
    	if(individual.qi311_g!=null && !Util.esDiferente(individual.qi311_g,1))
	    	return 7; 
    	if(individual.qi311_h!=null && !Util.esDiferente(individual.qi311_h,1))
	    	return 8; 
    	if(individual.qi311_i!=null && !Util.esDiferente(individual.qi311_i,1))
	    	return 9; 
    	if(individual.qi311_j!=null && !Util.esDiferente(individual.qi311_j,1))
	    	return 10; 
    	if(individual.qi311_k!=null && !Util.esDiferente(individual.qi311_k,1))
	    	return 11; 
    	if(individual.qi311_l!=null && !Util.esDiferente(individual.qi311_l,1))
	    	return 12; 
    	if(individual.qi311_m!=null && !Util.esDiferente(individual.qi311_m,1))
	    	return 13; 
    	if(individual.qi311_x!=null && !Util.esDiferente(individual.qi311_x,1))
	    	return 96; 
    	else 
    		return 0;
    		
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI304.readOnly();
    		rgQI310.readOnly();
    		rgQI312.readOnly();
    		txtQI307.readOnly();
    		txtQI311_O.readOnly();
    		txtQI312_NOM.readOnly();
    		txtQI312_O.readOnly();
    		chbQI311_A.readOnly();
    		chbQI311_B.readOnly();
    		chbQI311_C.readOnly();
    		chbQI311_D.readOnly();
    		chbQI311_E.readOnly();
    		chbQI311_F.readOnly();
    		chbQI311_G.readOnly();
    		chbQI311_H.readOnly();
    		chbQI311_I.readOnly();
    		chbQI311_J.readOnly();
    		chbQI311_K.readOnly();
    		chbQI311_L.readOnly();
    		chbQI311_M.readOnly();
    		chbQI311_X.readOnly();
    	}
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
	
		try {
			individual.qi312=individual.qi312!=null?individual.getConvertqi312(individual.qi312):null;
			grabarPregunta320();
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi310 = individual.qi310;
			App.getInstance().getPersonaCuestionarioIndividual().qi304 = individual.qi304;
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return App.INDIVIDUAL;
	}
}