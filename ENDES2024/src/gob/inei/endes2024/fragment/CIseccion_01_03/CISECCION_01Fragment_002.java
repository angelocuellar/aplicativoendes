package gob.inei.endes2024.fragment.CIseccion_01_03; 
import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public IntegerField txtQI105D;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI105M;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI105Y;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI106;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI107;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI108N;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI108Y;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI108G;

	public TextField txtCabecera;
	CISECCION_01_03 individual;
	public CheckBoxField chbP105_d,chbP105_m,chbP105_y;
	public Seccion01 persona;
	public Seccion01 personavalida;
	public String fechareferencia;
	public GridComponent2 gdFechaNacimiento, gridmesHogar, gdAnios, gridAnios, gridPreguntas108,gridEscuela,gridlbl108;
	private CuestionarioService cuestionarioService;
	private Seccion01Service seccion01service;
	private VisitaService visitaService;
	private LabelComponent lblTitulo, lblpregunta105, lblpregunta106, lblpregunta106_Ind, lblpregunta107, lblpregunta108, lbldia, lblmes, lblanio, lblEspacio, lblEspacio2, lblanioedad; 
	private LabelComponent lblEspacio1, lbldiaCH, lbldiaCH2, lblmesCH, lblmesCH2, lblanioCH, lblanioCH2, lblnivel0,lblanio0,lblgrado0,lblpregunta108_ind;
	LinearLayout q0,q1,q2,q3,q4,q5;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoPersona;
	SeccionCapitulo[] seccionesGrabadoSec1_Hog;

	public CISECCION_01Fragment_002() {}
	public CISECCION_01Fragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		int year = Calendar.getInstance().get(Calendar.YEAR);
//		rango(getActivity(), txtQI106, 15, 49);
//		rango(getActivity(), txtQI105M, 1, 12, null, 98);
//		rango(getActivity(), txtQI105Y, year-49, year-15, null, 9998);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI105D","QI105M","QI105Y","QI105CONS","QI106","QI107","QI108N","QI108Y","QI108G","QI103","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoPersona = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH7DD","QH7MM","QH14","QH15N","QH15Y","QH15G","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI105D","QI105M","QI105Y","QI105CONS","QI106","QI107","QI108N","QI108Y","QI108G")};
		seccionesGrabadoSec1_Hog = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH7DD","QH7MM","QH14","QH15N","QH15Y","QH15G")};
		return rootView; 
	}
  @Override 
  protected void buildFields() {
	  	
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_01).textSize(21).centrar().negrita();//.colorFondo(R.color.griscabece)
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta105 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi105);
		lblpregunta106 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi106);
		Spanned texto106 =Html.fromHtml("<b>COMPARE 105 Y 106 Y CORRIJA SI SON INCONSISTENTES</b>");
		lblpregunta106_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpregunta106_Ind.setText(texto106);
		lblpregunta107 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi107);
		lblpregunta108 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi108);
		lblpregunta108_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_01qi108_ind);
		
		lbldiaCH  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_01qi105texto1).textSize(16).colorTexto(color.red).centrar();
		lbldiaCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		lblmesCH  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_01qi105texto2).textSize(16).colorTexto(color.red).centrar();
		lblmesCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		lblanioCH = new LabelComponent(this.getActivity()).size(altoComponente, 420).text(R.string.ciseccion_01qi106texto1).textSize(16).colorTexto(color.red).centrar();
		lblanioCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
				
		lbldia  = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.ciseccion_01qi105qs22d).textSize(16).centrar();
		lblmes  = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.ciseccion_01qi105qs22m).textSize(16).centrar();
		lblanio = new LabelComponent(this.getActivity()).size(85, 450).text(R.string.ciseccion_01qi106qs22a).textSize(16).centrar();
		lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		lblanioedad = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.ciseccion_01qi105qs22a).textSize(16).centrar();
		lblEspacio1 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		lblEspacio2 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		
		txtQI105D = new IntegerField(this.getActivity()).size(altoComponente,100).maxLength(2) ;
		chbP105_d=new CheckBoxField(this.getActivity(), R.string.ciseccion_01qi105nosabe98, "1:0").size(60, 200);
		chbP105_d.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI105D);
		  		}else {
		  			MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI105D);
					txtQI105D.setVisibility(View.VISIBLE);
					}
				}
		});
		
		txtQI105M = new IntegerField(this.getActivity()).size(altoComponente,100).maxLength(2) ;
		chbP105_m=new CheckBoxField(this.getActivity(), R.string.ciseccion_01qi105nosabe98, "1:0").size(60, 200);
		chbP105_m.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI105M);
		  		}else {
		  			MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI105M);
					txtQI105M.setVisibility(View.VISIBLE);
					}
				}
		});
		chbP105_y=new CheckBoxField(this.getActivity(), R.string.ciseccion_01qi105nosabe9998, "1:0").size(60,200);
		chbP105_y.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI105Y);
		  		}else {
		  			MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI105Y);
					txtQI105Y.setVisibility(View.VISIBLE);
					}
				}
			});
		txtQI105Y = new IntegerField(this.getActivity()).size(altoComponente,100).maxLength(4);
		txtQI106  = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).callback("onQI106ChangeValue");
		rgQI107   = new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi107_1,R.string.ciseccion_01qi107_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI107ChangeValue");
		
		lblnivel0 = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.ciseccion_01qi108n).textSize(18).centrar().negrita();
		rgQI108N  = new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi108n_1,R.string.ciseccion_01qi108n_2,R.string.ciseccion_01qi108n_3,R.string.ciseccion_01qi108n_4,R.string.ciseccion_01qi108n_5,R.string.ciseccion_01qi108n_6).size(350,380).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI108NChangeValue");
		lblanio0  = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.ciseccion_01qi108y).textSize(18).centrar().negrita();
		rgQI108Y  = new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi108y_0,R.string.ciseccion_01qi108y_1,R.string.ciseccion_01qi108y_2,R.string.ciseccion_01qi108y_3,R.string.ciseccion_01qi108y_4,R.string.ciseccion_01qi108y_5,R.string.ciseccion_01qi108y_6).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH108AChangeValue");	
		lblgrado0 = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.ciseccion_01qi108g).textSize(18).centrar().negrita();
		rgQI108G  = new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi108g_1,R.string.ciseccion_01qi108g_2,R.string.ciseccion_01qi108g_3,R.string.ciseccion_01qi108g_4,R.string.ciseccion_01qi108g_5,R.string.ciseccion_01qi108g_6).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH108GChangeValue");
	
		gridmesHogar = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridmesHogar.addComponent(lbldiaCH);
		gridmesHogar.addComponent(lbldiaCH2);
		gridmesHogar.addComponent(lblmesCH);
		gridmesHogar.addComponent(lblmesCH2);
		
		gdFechaNacimiento = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		gdFechaNacimiento.addComponent(lbldia);
		gdFechaNacimiento.addComponent(txtQI105D);
		gdFechaNacimiento.addComponent(lblEspacio);
		gdFechaNacimiento.addComponent(chbP105_d);
		
		gdFechaNacimiento.addComponent(lblmes);
		gdFechaNacimiento.addComponent(txtQI105M);
		gdFechaNacimiento.addComponent(lblEspacio1);
		gdFechaNacimiento.addComponent(chbP105_m);
		
		gdFechaNacimiento.addComponent(lblanioedad);
		gdFechaNacimiento.addComponent(txtQI105Y);
		gdFechaNacimiento.addComponent(lblEspacio2);
		gdFechaNacimiento.addComponent(chbP105_y);
		
		gridAnios = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridAnios.addComponent(lblanioCH);
		gridAnios.addComponent(lblanioCH2);
		
		gdAnios = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdAnios.addComponent(lblanio);
		gdAnios.addComponent(txtQI106);
		
		gridPreguntas108=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas108.addComponent(lblnivel0);		
		gridPreguntas108.addComponent(lblanio0);
		gridPreguntas108.addComponent(lblgrado0);
		gridPreguntas108.addComponent(rgQI108N);
		gridPreguntas108.addComponent(rgQI108Y);	
		gridPreguntas108.addComponent(rgQI108G); 
    }
    @Override 
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera, lblTitulo);
		q2 = createQuestionSection(lblpregunta105, gridmesHogar.component(), gdFechaNacimiento.component());
		q3 = createQuestionSection(lblpregunta106, lblpregunta106_Ind, gridAnios.component(), gdAnios.component());
		q4 = createQuestionSection(lblpregunta107, rgQI107);
		q5 = createQuestionSection(lblpregunta108, lblpregunta108_ind,gridPreguntas108.component());

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
    return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi108n!=null)
			individual.qi108n=individual.setConvertQi108n(individual.qi108n);
		if (individual.qi108y!=null)
			individual.qi108y=individual.setConvertQi108y(individual.qi108y);
		if (chbP105_d.isChecked())
			individual.qi105d="98";
		if (chbP105_m.isChecked())
			individual.qi105m="98";
		if (chbP105_y.isChecked())
			individual.qi105y=9998;
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		
		persona = new Seccion01();
    	persona.id = individual.id;
    	persona.hogar_id = individual.hogar_id;
    	persona.persona_id = individual.persona_id;
    	persona.qh07 = individual.qi106;
    	persona.qh7dd = individual.qi105d;
    	persona.qh7mm = individual.qi105m;
    	persona.qh14  = individual.qi107;
    	persona.qh15n = individual.qi108n;
    	persona.qh15y = individual.qi108y;
    	persona.qh15g = individual.qi108g;

		try {
			if(individual.qi105d == null && individual.qi105m== null && individual.qi105y == null) {
				individual.qi105cons = null;
			}
			else {
				if(fechareferencia == null) {
					individual.qi105cons =  Util.getFechaActualToString();
				}
				else {
					individual.qi105cons = fechareferencia;
				}
			}
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}else {
				getCuestionarioService().saveOrUpdate(persona,seccionesGrabadoSec1_Hog);
			}
			
			 
//			if (individual.qi105d != null && individual.qi105m != null && individual.qi105y != null) {
//				Integer diaMin  = Integer.parseInt(individual.qi105d);
//				Integer mesMin  = Integer.parseInt(individual.qi105m);
//				Integer anioMin = individual.qi105y-49;
//				
//				Integer diaMax  = Integer.parseInt(individual.qi105d);
//				Integer mesMax  = Integer.parseInt(individual.qi105m);
//				Integer anioMax = individual.qi105y-15;
//				
//				Integer dia=Integer.parseInt(individual.qi105d);
//	    		Integer mes=Integer.parseInt(individual.qi105m);
//	    		Integer anio=individual.qi105y-15;
//				
//				Calendar fechamin = new GregorianCalendar(anioMin , mesMin-1, diaMin);
//				Calendar fechavacuna = new GregorianCalendar(anio, mes-1, dia);
//				Calendar fechamax = new GregorianCalendar(anioMax,mesMax -1, diaMax );
//				
//				Log.e("sss","entrDDDo aki");
//				
//			    if(fechamin.compareTo(fechavacuna)==-1 && fechamax.compareTo(fechavacuna)==1){
//			    	Log.e("sss","entro aki");
//			    	ToastMessage.msgBox(this.getActivity(), "Persona no es MEF", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//			    	parent.nextFragment(CuestionarioFragmentActivity.CISECCION_f_carat);
//			    	
//			    }
//			}
			
			
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		ActulializaPersona_idInformantedeSalud();
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi106 = individual.qi106;
			App.getInstance().getPersonaCuestionarioIndividual().qi107 = individual.qi107;
			App.getInstance().getPersonaCuestionarioIndividual().qi108n = individual.qi108n;
			App.getInstance().getPersonaCuestionarioIndividual().qi108y = individual.qi108y;
			App.getInstance().getPersonaCuestionarioIndividual().qi108g = individual.qi108g;
			App.getInstance().getPersonaCuestionarioIndividual().qi105d=individual.qi105d;
			App.getInstance().getPersonaCuestionarioIndividual().qi105m=individual.qi105m;
			App.getInstance().getPersonaCuestionarioIndividual().qi105y=individual.qi105y;
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return true;
    }
    
    public void ActulializaPersona_idInformantedeSalud(){
    	if(individual!=null && individual.id!=null && individual.hogar_id!=null)
			getSeccion01Service().ModificarEnlaceInformanteDeSalud(individual.id,individual.hogar_id, SeleccionarInformantedeSalud());
	}
    public Integer SeleccionarInformantedeSalud(){
  		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getSeccion01Service());
  		return persona_id;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (individual.qi105d!=null &&individual.qi105m!=null && individual.qi105m!=null) {
			if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(individual.qi105d), Integer.parseInt(individual.qi105m)) && !individual.qi105d.equals("98") && !individual.qi105m.equals("98")){
				 mensaje = "d�a no corresponde al mes";
				 view = txtQI105D;
				 error = true;
				 return false;
			}
		}
		if (Util.esVacio(individual.qi105d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI105D");
			view = txtQI105D;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi105m)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI105M");
			view = txtQI105M;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi105y)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI105Y");
			view = txtQI105Y;
			error = true;
			return false;
		}
    	if(individual.qi105d!=null){
	    	if(Util.esMayor(Integer.parseInt(individual.qi105d), 31) && Util.esDiferente(Integer.parseInt(individual.qi105d), 98,98)){
	    		mensaje ="dia fuera de rango";
				view = txtQI105D;
				error=true;
				return false;
	    	}
    	}

    	if (MyUtil.incluyeRango(1,9,individual.qi105m)) {
				mensaje = "Los meses deben considerarse a 2 d�gitos: de 01 a 12 meses";
				view = txtQI105M;
				error = true;
				return false;
		}
		if(individual.qi105m!=null){
	    	if ((Util.esMenor(Integer.parseInt(individual.qi105m),1) ||  Util.esMayor(Integer.parseInt(individual.qi105m),12)) && Util.esDiferente(Integer.parseInt(individual.qi105m), 98,98)) {
	    		mensaje ="Mes fuera de rango";
				view = txtQI105M;
				error=true;
				return false;
	    	}
    	}
    	if(individual.qi105d!=null && individual.qi105m!=null){
	    	if(Util.esMenor(Integer.parseInt(individual.qi105d), 32) && Util.esMenor(Integer.parseInt(individual.qi105m), 13)){
	    		Integer dia=Integer.parseInt(individual.qi105d.toString());
	    		Integer mes=Integer.parseInt(individual.qi105m.toString());
	    		
	    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
	    			mensaje ="Dia no corresponde al mes que eligi�";
	    			view = txtQI105D;
	    			error=true;
	    			return false;
	    		}
	    	}
    	}
    	
		if (Util.esVacio(individual.qi106)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI106");
			view = txtQI106;
			error = true;
			return false;
		}
		Calendar fechadenacimiento = new GregorianCalendar(individual.qi105y, Integer.parseInt(individual.qi105m)-1, Integer.parseInt(individual.qi105d));
		Calendar fecharef = new GregorianCalendar();

		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}
 		if(Util.esDiferente(MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef), individual.qi106) && !individual.qi105d.equals("98") && !individual.qi105m.equals("98") && individual.qi105y!=9998){
			mensaje = "Edad no corresponde con Fecha de Nacimiento"; 
			view = txtQI106; 
			error = true; 
			return false;
		}
 		if(Util.esMayor(individual.qi103,individual.qi106) && Util.esDiferente(individual.qi103,95) && Util.esDiferente(individual.qi103,96)){
			mensaje = "Verificar Pregunta QI103, No puede ser Mayor a la edad de la Mef"; 
			view = txtQI106; 
			error = true; 
			return false;
		}
 		
 		
 		
		if (Util.esVacio(individual.qi107)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI107");
			view = rgQI107;
			error = true; 
			return false; 
		}
		if (Util.esDiferente(individual.qi107,2)) {
			if (Util.esDiferente(individual.qi108n,7)) {
				if (Util.esVacio(individual.qi108n)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.108N");
					view = rgQI108N;
					error = true;
					return false;
				}					
				if (Util.esDiferente(individual.qi108n,2)) {
					if (Util.esVacio(individual.qi108y)  && Util.esVacio(individual.qi108g)) {
						mensaje = preguntaVacia.replace("$", "La pregunta P.108Y");
						view = rgQI108Y;
						error = true;
						return false;
					}
				}
				if (!Util.esDiferente(individual.qi108n,2)) {
					if (Util.esVacio(individual.qi108g)  && Util.esVacio(individual.qi108y)) {
						mensaje = preguntaVacia.replace("$", "La pregunta P.108G");
						view = rgQI108G;
						error = true;
						return false;
					}
				}
			}
		}
		return true;
    }
    
	@Override
	public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		if(individual.qi108n!=null)
			individual.qi108n=individual.getConvertQi108n(individual.qi108n);
		if(individual.qi108y!=null)
			individual.qi108y=individual.getConvertQi108y(individual.qi108y);
		
		entityToUI(individual);
		fechareferencia = individual.qi105cons;
		inicio();
	}
 
    private void inicio() {
    	renombrar();
    	validarPregunta103();
    	onrgQI107ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void onQI106ChangeValue(){
    	boolean valido=true;
    	String msg="";
    	String s115y = txtQI105Y.getValue() == null ? null : txtQI105Y.getValue().toString();
		String s115m = txtQI105M.getValue() == null ? null : txtQI105M.getValue().toString();
		String s115d = txtQI105D.getValue() == null ? null : txtQI105D.getValue().toString();
		
		String s1106 =txtQI106.getValue() == null ? "0" : txtQI106.getValue().toString().trim() ;
		
		Integer i106 = Integer.parseInt(s1106);
		if(s115y!=null && s115m!=null && s115d!=null && valido) {
		
			Calendar fechadenacimiento = new GregorianCalendar(Integer.parseInt(s115y), Integer.parseInt(s115m)-1, Integer.parseInt(s115d));
			Calendar fecharef = new GregorianCalendar();
			
			if(fechareferencia!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(fechareferencia);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}			
				
			if(Util.esDiferente(MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef), i106)){
				msg = "Edad no corresponde con Fecha de Nacimiento"; 
				valido = false;
			}		
		
		}
    	if(txtQI106.getValue().toString().trim().length()>0 && Integer.parseInt(txtQI106.getValue().toString())!=App.getInstance().getPersonaCuestionarioIndividual().qh07){
    		msg="La edad es diferente al del hogar";
    		valido=false;
		}
    	
		if(!valido){
			ToastMessage.msgBox(this.getActivity(), msg,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
		}
		
		rgQI107.requestFocus();
    }
    
    private void renombrar() {
    	personavalida = getCuestionarioService().getSeccion01(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoPersona);
//    	Log.e("","CH: 14 "+personavalida.qh14); 
    	lbldiaCH2.setText(personavalida.qh7dd);
    	lblmesCH2.setText(personavalida.qh7mm);
    	lblanioCH2.setText(personavalida.qh07.toString());
    	App.getInstance().setEditarPersonaSeccion01(personavalida);
    }
    
    /*public void MefCorrecto() {
		
		if (personaId!=App.getInstance().getPersonaSeccion01().persona_id) {
			validarMensaje("Existe otro seleccionado del Informate");
			parent.nextFragment(CuestionarioFragmentActivity.CARATULA);
			return;
		}
	}*/
    
    public void validarPregunta103() {
    	String st107 = individual.qi107==null?"":individual.qi107.toString();
    	if (st107.equals("2")) {
    		q5.setVisibility(View.GONE);
    	}else{
    		q5.setVisibility(View.VISIBLE);
    	}
    	if (individual.qi105d!=null && individual.qi105d.equals("98")) {
    		chbP105_d.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI105D);
    		txtQI105D.setVisibility(View.GONE);
		}
    	if (individual.qi105m!=null && individual.qi105m.equals("98")) {
    		chbP105_m.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI105M);
    		txtQI105M.setVisibility(View.GONE);
		}
    	if (individual.qi105y!=null && individual.qi105y==9998) {
    		chbP105_y.setChecked(true);
    		Util.cleanAndLockView(getActivity(),txtQI105Y);
    		txtQI105Y.setVisibility(View.GONE);
		}
    }
    	
	 public void onrgQI107ChangeValue() {
		 Integer value=rgQI107.getTagSelected()!=null?Integer.parseInt(rgQI107.getTagSelected("").toString()):0;
		 if(Util.esDiferente(value, 0) && Util.esDiferente(personavalida.qh14, value)){
			 ToastMessage.msgBox(this.getActivity(), "La respuesta difiere a la del cuestionario del hogar", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		 }
		 if (MyUtil.incluyeRango(2,2,rgQI107.getTagSelected("").toString())) {
			 MyUtil.LiberarMemoria();
			 Util.cleanAndLockView(getActivity(),rgQI108N,rgQI108Y,rgQI108G);
			 q5.setVisibility(View.GONE);
		 } 
		 else {
			 Util.lockView(getActivity(), false,rgQI108N,rgQI108Y,rgQI108G);
			 q5.setVisibility(View.VISIBLE);
			 onrgQI108NChangeValue();
			 rgQI108N.requestFocus();
		 }	
	 }

		
	 public void onrgQI108NChangeValue() {
		 Integer nivel=rgQI108N.getTagSelected()!=null?Integer.parseInt(rgQI108N.getTagSelected("0").toString()):-1;
		 if(nivel!=-1){
			 nivel=nivel-1;
			 nivel=nivel==0?1:nivel;
		 }
		 if(Util.esDiferente(nivel,-1) &&  Util.esDiferente(personavalida.qh15n,nivel)){
			 ToastMessage.msgBox(this.getActivity(), "El Nivel difiere con el cuestionario del hogar", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		 }
		 
		 if (!MyUtil.incluyeRango(2,2,rgQI107.getTagSelected("").toString())) {
			 if (MyUtil.incluyeRango(1,1,rgQI108N.getTagSelected("").toString())) {		
				 Util.cleanAndLockView(getActivity(),rgQI108G);			
				 Util.lockView(getActivity(), false,rgQI108Y);
				 rgQI108Y.lockButtons(true,1,2,3,4,5,6);
				 rgQI108Y.requestFocus();
			 }	
			 if (MyUtil.incluyeRango(3,4,rgQI108N.getTagSelected("").toString())) {		
				 Util.cleanAndLockView(getActivity(),rgQI108G);			
				 Util.lockView(getActivity(), false,rgQI108Y);	
				 rgQI108Y.lockButtons(true,0,6);
				 rgQI108G.requestFocus();
			 }
			 if (MyUtil.incluyeRango(5,5,rgQI108N.getTagSelected("").toString())) {		
				 Util.cleanAndLockView(getActivity(),rgQI108G);			
				 Util.lockView(getActivity(), false,rgQI108Y);	
				 rgQI108Y.lockButtons(true,0);
				 rgQI108G.requestFocus();
			 }
			 if (MyUtil.incluyeRango(6,6,rgQI108N.getTagSelected("").toString())) {			
				 Util.cleanAndLockView(getActivity(),rgQI108G);			
				 Util.lockView(getActivity(), false,rgQI108Y);	
				 rgQI108Y.lockButtons(true,0,3,4,5,6);
				 rgQI108G.requestFocus();
			 }
			 if (MyUtil.incluyeRango(15,46,txtQI106.getText().toString())) {
				 if (MyUtil.incluyeRango(2,2,rgQI108N.getTagSelected("").toString())) {				
					 Util.cleanAndLockView(getActivity(),rgQI108Y);	
					 Util.lockView(getActivity(), false,rgQI108G);				
					 rgQI108G.requestFocus();
				 }	
			 }
			 else{
				 if (MyUtil.incluyeRango(2,2,rgQI108N.getTagSelected("").toString())) {		
					 Util.lockView(getActivity(), false,rgQI108Y,rgQI108G);
					 rgQI108Y.lockButtons(true,6);
					 onrgQH108AChangeValue();
					 onrgQH108GChangeValue();
					 rgQI108G.requestFocus();
				 }
			 }			
		 }
	 }
		
	 public void onrgQH108AChangeValue() {
		 Integer valor = rgQI108Y.getTagSelected()!=null?Integer.parseInt(rgQI108Y.getTagSelected("0").toString()):-1;
		 if(valor!=-1){
			 valor=valor-1;
			 valor=valor==0?1:valor;	 
		 }		 
//		 Log.e("wsss","sss"+valor);
		 if(Util.esDiferente(valor, -1) && Util.esDiferente(personavalida.qh15y,valor)){
			 ToastMessage.msgBox(this.getActivity(), "El A�o difiere con el cuestionario del hogar", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		 }
		 if (MyUtil.incluyeRango(47,97,txtQI106.getText().toString()) && MyUtil.incluyeRango(1,5,rgQI108Y.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI108N.getTagSelected("").toString())) {
			 Util.cleanAndLockView(getActivity(),rgQI108G);
			 Util.lockView(getActivity(), false,rgQI108G);
		 }				
	 }
	 public void onrgQH108GChangeValue() {
		 Integer valor = rgQI108Y.getTagSelected()!=null?Integer.parseInt(rgQI108G.getTagSelected("0").toString()):0;
		 if( Util.esDiferente(valor, 0) &&  Util.esDiferente(personavalida.qh15g, valor)){
			 ToastMessage.msgBox(this.getActivity(), "El Grado difiere con el cuestionario del hogar", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		 }
		 if (MyUtil.incluyeRango(47,97,txtQI106.getText().toString()) && MyUtil.incluyeRango(1,6,rgQI108G.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI108N.getTagSelected("").toString())) {
			 Util.cleanAndLockView(getActivity(),rgQI108Y);
			 Util.lockView(getActivity(), false,rgQI108Y);
			 rgQI108Y.lockButtons(true,0,6);
		 }				
	 }
		
	 public void ValidarsiesSupervisora(){
		 Integer codigo=App.getInstance().getUsuario().cargo_id;
			if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
				rgQI107.readOnly();
				rgQI108G.readOnly();
				rgQI108N.readOnly();
				rgQI108Y.readOnly();
				txtQI105D.readOnly();
				txtQI105M.readOnly();
				txtQI105Y.readOnly();
				txtQI106.readOnly();
				chbP105_d.readOnly();
				chbP105_m.readOnly();
				chbP105_y.readOnly();
			}
	 }
		
	 public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		} 
		return cuestionarioService; 
	 }
	 
	 public Seccion01Service getSeccion01Service(){
	    	if(seccion01service==null){
	    		seccion01service =  Seccion01Service.getInstance(getActivity());
	    	}
	    return seccion01service;
	 }
	 
	 public VisitaService getVisitaService(){
			if(visitaService==null)
			{
				visitaService = VisitaService.getInstance(getActivity());
			}
			return visitaService;
		}
	 
	 @Override
	 public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi108n!=null)
			individual.qi108n=individual.getConvertQi108n(individual.qi108n);
		if (individual.qi108y!=null)
			individual.qi108y=individual.getConvertQi108y(individual.qi108y);
		if (chbP105_d.isChecked())
			individual.qi105d="98";
		if (chbP105_m.isChecked())
			individual.qi105m="98";
		if (chbP105_y.isChecked())
			individual.qi105y=9998;
			
		persona = new Seccion01();
    	persona.id = individual.id;
    	persona.hogar_id = individual.hogar_id;
    	persona.persona_id = individual.persona_id;
    	persona.qh07 = individual.qi106;
    	persona.qh7dd = individual.qi105d;
    	persona.qh7mm = individual.qi105m;
    	persona.qh14  = individual.qi107;
    	persona.qh15n = individual.qi108n;
    	persona.qh15y = individual.qi108y;
    	persona.qh15g = individual.qi108g;

		try {
			if(individual.qi105d == null && individual.qi105m== null && individual.qi105y == null) {
				individual.qi105cons = null;
			}
			else {
				if(fechareferencia == null) {
					individual.qi105cons =  Util.getFechaActualToString();
				}
				else {
					individual.qi105cons = fechareferencia;
				}
			}
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}else {
				getCuestionarioService().saveOrUpdate(persona,seccionesGrabadoSec1_Hog);
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi106 = individual.qi106;
			App.getInstance().getPersonaCuestionarioIndividual().qi108n = individual.qi108n;
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return App.INDIVIDUAL;
	}
}