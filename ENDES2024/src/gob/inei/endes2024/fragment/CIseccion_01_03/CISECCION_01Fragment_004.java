package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI115;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI116;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI117;
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI119;
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQI119_1;
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI119_2;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta115,lblpregunta116,lblpregunta117,lblpregunta119;
	LinearLayout q0,q1,q2,q3,q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_01Fragment_004() {}
	public CISECCION_01Fragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI114","QI115","QI116","QI117","QI119","QI119_O","QI119A","QI119B","QI119C","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI115","QI116","QI117","QI119","QI119_O","QI119A","QI119B","QI119C")};
		return rootView;
	}
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_01).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta115 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi115);
		lblpregunta116 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi116);
		lblpregunta117 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi117);
		lblpregunta119 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_01qi119);
		rgQI115=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi115_1,R.string.ciseccion_01qi115_2,R.string.ciseccion_01qi115_3,
				R.string.ciseccion_01qi115_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI116=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi116_1,R.string.ciseccion_01qi116_2,R.string.ciseccion_01qi116_3,
				R.string.ciseccion_01qi116_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI117=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi117_1,R.string.ciseccion_01qi117_2,R.string.ciseccion_01qi117_3,
				R.string.ciseccion_01qi117_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI119=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_01qi119_1,R.string.ciseccion_01qi119_2,R.string.ciseccion_01qi119_3,
				R.string.ciseccion_01qi119_4,R.string.ciseccion_01qi119_5,R.string.ciseccion_01qi119_6,R.string.ciseccion_01qi119_7,
				R.string.ciseccion_01qi119_8,R.string.ciseccion_01qi119_9,R.string.ciseccion_01qi119_10,R.string.ciseccion_01qi119_11,
				R.string.ciseccion_01qi119_12).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI119_1 = new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		txtQI119_2 = new TextField(this.getActivity()).maxLength(500).size(altoComponente, 400);
		rgQI119.agregarEspecifique(8, txtQI119_1);
		rgQI119.agregarEspecifique(11, txtQI119_2);
		
    }
    @Override 
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta115,rgQI115);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta116,rgQI116);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta117,rgQI117);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta119,rgQI119);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
    return contenedor;
    }
    
    @Override 
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			ModificarVariablesparaBD();
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		String st114 = individual.qi114==null?"0":individual.qi114.toString();
    	if (st114.equals("0") || st114.equals("2") || st114.equals("3") || st114.equals("4")) {
    		if (Util.esVacio(individual.qi115)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI115");
    			view = rgQI115;
    			error = true;
    			return false;
    		}
    	}
		if (Util.esVacio(individual.qi116)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI116");
			view = rgQI116;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi117)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI117");
			view = rgQI117;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi119)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI119");
			view = rgQI119;
			error = true;
			return false;
		}
		if (!Util.esDiferente(individual.qi119,9)) {
			if(txtQI119_1.getText().toString().isEmpty()){
			mensaje = "Debe ingresar informaci\u00f3n en Especifique";
			view = txtQI119_1;
			error = true;
			return false;
			}
		}
		if (!Util.esDiferente(individual.qi119,12)) {
			if(txtQI119_2.getText().toString().isEmpty()){
			mensaje = "Debe ingresar informaci\u00f3n en Especifique";
			view = txtQI119_2;
			error = true;
			return false;
			}
		}
		return true;
    }
    
	@Override
	public void cargarDatos() {

		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		entityToUI(individual);
		ModificarVariableparaUsuario();
		inicio();
	}
    private void inicio() {
    	validarPregunta114();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void validarPregunta114(){
    	String st114 = individual.qi114==null?"":individual.qi114.toString();
    	if (st114.equals("1") || st114.equals("5")) {
    		Util.cleanAndLockView(getActivity(),rgQI115);
    		q1.setVisibility(View.GONE);
    	}else{
    		MyUtil.LiberarMemoria();
			Util.lockView(getActivity(), false,rgQI115,rgQI116,rgQI117,rgQI119);
    		q1.setVisibility(View.VISIBLE);
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI115.readOnly();
    		rgQI116.readOnly();
    		rgQI117.readOnly();
    		rgQI119.readOnly();    		
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ModificarVariablesparaBD(){
	    if(!Util.esDiferente(individual.qi119, 9)){
	    	individual.qi119_o=txtQI119_1.getText().toString().trim();
	    }
	    if(!Util.esDiferente(individual.qi119, 12)){
	    	individual.qi119_o=txtQI119_2.getText().toString().trim();
	    }
    }
    public void ModificarVariableparaUsuario(){
    	if(!Util.esDiferente(individual.qi119, 9)){
	    	txtQI119_1.setText(individual.qi119_o);
	    }
	    if(!Util.esDiferente(individual.qi119, 12)){
	    	txtQI119_2.setText(individual.qi119_o);
	    }
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
	
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}