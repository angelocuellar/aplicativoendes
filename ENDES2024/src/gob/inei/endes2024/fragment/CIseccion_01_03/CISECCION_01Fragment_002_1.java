package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_002_1 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH14; 	
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH15N; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH15Y;
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQH15G; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH16; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH17;	
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQH18N;
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQH18Y; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQH18G;	
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQH19;	
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQH20N;	
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH20Y; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQH20G;	
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQH21; 	
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQH21A;	
	@FieldAnnotation(orderIndex = 16)
	public TextAreaField txtQHOBSPERSONA;
	
	Seccion01 bean; 
	private SeccionCapitulo[] seccionesGrabado;
	private SeccionCapitulo[] seccionesCargado; 
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	public GridComponent2 gridPreguntas18,gridPreguntas20;
	public LabelComponent lblnivel,lblnivel2,lblanio,lblgrado,lblanio2,lblgrado2;
	public LabelComponent lblmatriculado,lblasiste,lbltituloanio,lblnivelanioygrado2,lblnivelanioygrado3,lblnivelanioygrado4,lblestuvomatriculado,lblgradonombre,lblgradoaniopasado,lblcolegioestatal,lblTitulo1;
	public GridComponent2 gridPreguntas15;
	public LabelComponent lblnivel0,lblanio0,lblgrado0;	
	public LabelComponent lblafiliado,lbltitular,lblpregunta13,lblasistioalaescuela,lblnivelanioygrado;
	
	public boolean validaredadmayor24 = false;
	
	LinearLayout q0;
	LinearLayout q1; 		
	LinearLayout q2;
	LinearLayout q3; 
	LinearLayout q4; 		
	LinearLayout q5;
	LinearLayout q6; 		
	LinearLayout q7; 		
	LinearLayout q8; 
	LinearLayout q9;
	LinearLayout q10;
	
	private Seccion03Service seccion03service;

	public CISECCION_01Fragment_002_1() {} 
	public CISECCION_01Fragment_002_1 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","ID","HOGAR_ID","PERSONA_ID","QH07","QH13","QH02_1","QHINFO","QHOBSPERSONA")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","QHOBSPERSONA")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	
	  lblTitulo1 = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_tituloNroOrden).textSize(21).centrar().negrita();
	  
	  rgQH14=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh14_1,R.string.seccion01qh14_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH14ChangeValue");
		
		lblasistioalaescuela = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh14);
		lblnivelanioygrado= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh15n);
		lblnivelanioygrado2= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.seccion01qh15n_ind);
		lblnivelanioygrado3= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.seccion01qh15n_ind);
		lblnivelanioygrado4= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.seccion01qh20n_ind);
		
		lblnivel0 = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.seccion01qh15).textSize(18).centrar().negrita();
		rgQH15N=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15n_1,R.string.seccion01qh15n_2,R.string.seccion01qh15n_3,R.string.seccion01qh15n_4,R.string.seccion01qh15n_5,R.string.seccion01qh15n_6,R.string.seccion01qh15n_7).size(350,380).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH15nChangeValue"); 
		
		lblanio0 = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15y).textSize(18).centrar().negrita();
		rgQH15Y=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15y_0,R.string.seccion01qh15y_1,R.string.seccion01qh15y_2,R.string.seccion01qh15y_3,R.string.seccion01qh15y_4,R.string.seccion01qh15y_5,R.string.seccion01qh15y_6,R.string.seccion01qh15y_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH15YChangeValue");
	
		lblgrado0 = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15g).textSize(18).centrar().negrita();
		rgQH15G=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15g_1,R.string.seccion01qh15g_2,R.string.seccion01qh15g_3,R.string.seccion01qh15g_4,R.string.seccion01qh15g_5,R.string.seccion01qh15g_6,R.string.seccion01qh15g_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH15GChangeValue");
			
		rgQH16=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh16_1,R.string.seccion01qh16_2).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH16ChangeValue");
		rgQH17=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh17_1,R.string.seccion01qh17_2).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH17ChangeValue");
		
		lblnivel = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.seccion01qh15).textSize(18).centrar().negrita();
		rgQH18N=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh18n_1,R.string.seccion01qh18n_2,R.string.seccion01qh18n_3,R.string.seccion01qh18n_4,R.string.seccion01qh18n_5,R.string.seccion01qh18n_6,R.string.seccion01qh18n_7).size(300,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH18nChangeValue"); 
		
		lblanio = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15y).textSize(18).centrar().negrita();
		rgQH18Y=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15y_0,R.string.seccion01qh15y_1,R.string.seccion01qh15y_2,R.string.seccion01qh15y_3,R.string.seccion01qh15y_4,R.string.seccion01qh15y_5,R.string.seccion01qh15y_6,R.string.seccion01qh15y_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
				
		lblgrado = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15g).textSize(18).centrar().negrita();
		rgQH18G=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15g_1,R.string.seccion01qh15g_2,R.string.seccion01qh15g_3,R.string.seccion01qh15g_4,R.string.seccion01qh15g_5,R.string.seccion01qh15g_6,R.string.seccion01qh15g_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
						
		rgQH19=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh19_1,R.string.seccion01qh19_2).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH19ChangeValue");
		
		lblnivel2 = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.seccion01qh15).textSize(18).centrar().negrita();
		rgQH20N=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh20n_1,R.string.seccion01qh20n_2,R.string.seccion01qh20n_3,R.string.seccion01qh20n_4,R.string.seccion01qh20n_5,R.string.seccion01qh20n_6,R.string.seccion01qh20n_7).size(300,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH20nChangeValue"); 
		
		lblanio2 = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15y).textSize(18).centrar().negrita();
		rgQH20Y=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15y_0,R.string.seccion01qh15y_1,R.string.seccion01qh15y_2,R.string.seccion01qh15y_3,R.string.seccion01qh15y_4,R.string.seccion01qh15y_5,R.string.seccion01qh15y_6,R.string.seccion01qh15y_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		lblgrado2 = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15g).textSize(18).centrar().negrita();
		rgQH20G=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15g_1,R.string.seccion01qh15g_2,R.string.seccion01qh15g_3,R.string.seccion01qh15g_4,R.string.seccion01qh15g_5,R.string.seccion01qh15g_6,R.string.seccion01qh15g_7).size(350,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
			
		rgQH21=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh21_1,R.string.seccion01qh21_2,R.string.seccion01qh21_3,R.string.seccion01qh21_4).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();		
		rgQH21A=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh21a_1,R.string.seccion01qh21a_2).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
			
		lblmatriculado= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh16);
		lblasiste= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh17);
		lbltituloanio = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh18n);
		lblestuvomatriculado = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh19);
		lblgradonombre = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh20n);
		lblgradoaniopasado =new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(20).text(R.string.seccion01qh21); 
		lblcolegioestatal = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(20).text(R.string.seccion01qh21a);

		txtQHOBSPERSONA = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
		txtQHOBSPERSONA.setCallback("OcultarTecla");
		
		gridPreguntas15=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas15.addComponent(lblnivel0);		
		gridPreguntas15.addComponent(lblanio0);
		gridPreguntas15.addComponent(lblgrado0);
		gridPreguntas15.addComponent(rgQH15N);
		gridPreguntas15.addComponent(rgQH15Y);	
		gridPreguntas15.addComponent(rgQH15G);	
		
		gridPreguntas18=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas18.addComponent(lblnivel);		
		gridPreguntas18.addComponent(lblanio);
		gridPreguntas18.addComponent(lblgrado);
		gridPreguntas18.addComponent(rgQH18N);
		gridPreguntas18.addComponent(rgQH18Y);	
		gridPreguntas18.addComponent(rgQH18G);
		
		gridPreguntas20=new GridComponent2(this.getActivity(),3);
		gridPreguntas20.addComponent(lblnivel2);		
		gridPreguntas20.addComponent(lblanio2);
		gridPreguntas20.addComponent(lblgrado2);
		gridPreguntas20.addComponent(rgQH20N);
		gridPreguntas20.addComponent(rgQH20Y);	
		gridPreguntas20.addComponent(rgQH20G);  
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo1);
		q1 = createQuestionSection(lblasistioalaescuela,rgQH14); 		
		q2 = createQuestionSection(lblnivelanioygrado,lblnivelanioygrado2,gridPreguntas15.component());	
		q3 = createQuestionSection(lblmatriculado,rgQH16); 
		q4 = createQuestionSection(lblasiste,rgQH17); 		
		q5 = createQuestionSection(lbltituloanio,lblnivelanioygrado3,gridPreguntas18.component());
		q6 = createQuestionSection(lblestuvomatriculado,rgQH19); 		
		q7 = createQuestionSection(lblgradonombre,lblnivelanioygrado4,gridPreguntas20.component()); 		
		q8 = createQuestionSection(lblgradoaniopasado,rgQH21); 
		q9 = createQuestionSection(lblcolegioestatal,rgQH21A);
		q10 = createQuestionSection(R.string.seccion01qh_obs_0,txtQHOBSPERSONA);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {	
		
    	uiToEntity(bean);
		
		if (bean.qh15n!=null) {
			bean.qh15n=bean.getConvertqh15n(bean.qh15n);
		}			
		if (bean.qh15y!=null) {
			bean.qh15y=bean.getConvertqh15_y(bean.qh15y);
		}
		if (bean.qh15g!=null) {
			bean.qh15g=bean.getConvertqh15_g(bean.qh15g);
		}
		if (bean.qh18n!=null) {
			bean.qh18n=bean.getConvertqh15n(bean.qh18n);
		}
		if (bean.qh18y!=null) {
			bean.qh18y=bean.getConvertqh15_y(bean.qh18y);
		}
		if (bean.qh18g!=null) {
			bean.qh18g=bean.getConvertqh15_g(bean.qh18g);
		}
		if (bean.qh20n!=null) {
			bean.qh20n=bean.getConvertqh15n(bean.qh20n);
		}		
		if (bean.qh20y!=null) {
			bean.qh20y=bean.getConvertqh15_y(bean.qh20y);
		}	
		if (bean.qh20g!=null) {
			bean.qh20g=bean.getConvertqh15_g(bean.qh20g);
		}
		

		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
				ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {	
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		getService().commitTX(dbTX);
		EliminarSeccion03();
		
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return true; 
    } 
    
    private boolean validar() { 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);

    	if (Util.esMayor(bean.qh07,2)) {
    		if (Util.esVacio(bean.qh14)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.14"); 
    			view = rgQH14; 
    			error = true; 
    			return false; 
    		} 
    		if (Util.esDiferente(bean.qh14,2)) {    		
    			if (Util.esDiferente(bean.qh15n,8)) {    			
    				if (Util.esVacio(bean.qh15n)) { 
    					mensaje = preguntaVacia.replace("$", "La pregunta P.15N"); 
    					view = rgQH15N; 
    					error = true; 
    					return false; 
    				}	
    					
    				if (Util.esDiferente(bean.qh15n,1)) {
    					if (Util.esVacio(bean.qh15y)  && Util.esVacio(bean.qh15g)) {
    						mensaje = preguntaVacia.replace("$", "La pregunta P.15Y"); 
    						view = rgQH15Y; 
    						error = true; 
    						return false;
    					}
    				}
    				if (!Util.esDiferente(bean.qh15n,1)) {
    					if (Util.esVacio(bean.qh15g)  && Util.esVacio(bean.qh15y)) { 
    						mensaje = preguntaVacia.replace("$", "La pregunta P.15G"); 
    						view = rgQH15G; 
    						error = true; 
    						return false; 
    					}
    				}
    			}
    			if (Util.esMenor(bean.qh07,25)) {
    				if (Util.esVacio(bean.qh16)) { 
    					mensaje = preguntaVacia.replace("$", "La pregunta P.16"); 
    					view = rgQH16; 
    					error = true; 
    					return false; 
    				} 
    				if (Util.esDiferente(bean.qh16, 2)) {
    					if (Util.esVacio(bean.qh17)) { 
    						mensaje = preguntaVacia.replace("$", "La pregunta P.17"); 
    						view = rgQH17; 
    						error = true; 
    						return false; 
    					} 
    					if (Util.esVacio(bean.qh18n)) { 
    						mensaje = preguntaVacia.replace("$", "La pregunta P.18N"); 
    						view = rgQH18N; 
    						error = true; 
    						return false; 
    					} 
    					if (!Util.esDiferente(bean.qh18n, 0,1,2,3,4,5)) {
    						if (Util.esVacio(bean.qh18y) && Util.esVacio(bean.qh18g)) { 
    							mensaje = preguntaVacia.replace("$", "La pregunta P.18Y"); 
    							view = rgQH18Y; 
    							error = true; 
    							return false; 
    						} 
    						if (Util.esVacio(bean.qh18g) && Util.esVacio(bean.qh18y)) { 
    							mensaje = preguntaVacia.replace("$", "La pregunta P.18G"); 
    							view = rgQH18G; 
    							error = true; 
    							return false; 
    						}
    					}				 
    				}
    					
    				if (Util.esVacio(bean.qh19)) { 
    					mensaje = preguntaVacia.replace("$", "La pregunta P.19"); 
    					view = rgQH19; 
    					error = true; 
    					return false; 
    				} 
    				if (Util.esDiferente(bean.qh19,2)) {
    					if (Util.esVacio(bean.qh20n)) { 
    						mensaje = preguntaVacia.replace("$", "La pregunta P.20N"); 
    						view = rgQH20N; 
    						error = true; 
    						return false; 
    					} 
    					if (!Util.esDiferente(bean.qh20n, 0,1,2,3,4,5)) {
    						if (Util.esVacio(bean.qh20y) && Util.esVacio(bean.qh20g)) { 
    							mensaje = preguntaVacia.replace("$", "La pregunta P.20Y"); 
    							view = rgQH20Y; 
    							error = true; 
    							return false; 
    						} 
    						if (Util.esVacio(bean.qh20g) && Util.esVacio(bean.qh20y)) { 
    							mensaje = preguntaVacia.replace("$", "La pregunta P.20G"); 
    							view = rgQH20G; 
    							error = true; 
    							return false; 
    						} 
    					}			
    									
    					if (!Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh20n,0)) {
    						if (Util.esVacio(bean.qh21)) { 
    							mensaje = preguntaVacia.replace("$", "La pregunta P.21"); 
    							view = rgQH21; 
    							error = true; 
    							return false; 
    						} 
    					}														
    				}
    					
    				if (!Util.esDiferente(bean.qh17,1) && !Util.esDiferente(bean.qh18n,0,1)) {
    					if (Util.esVacio(bean.qh21a)) { 
    						mensaje = preguntaVacia.replace("$", "La pregunta P.21A"); 
    						view = rgQH21A; 
    						error = true; 
    						return false; 
    					}
    				}
    			}		
    		}
    		
    		if (!Util.esDiferente(bean.qh13,6) && !Util.esDiferente(bean.qh14,2)) {
    			mensaje = "Marco la alternativa 6.Estudiaba en la P.13"; 
    			view = rgQH14; 
    			error = true; 
    			return false;	
    		}

    		
    	}
    		  			

    	if (!Util.esDiferente(bean.qh15n,1,2) && !Util.esDiferente(bean.qh18n,1,2))  {    		
    		if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh18n,1) && Util.esDiferente(bean.qh15g,8) && Util.esDiferente(bean.qh18g,8) &&  (Util.esMenor(bean.qh18g,bean.qh15g) || !Util.esDiferente(bean.qh18g,bean.qh15g) || Util.esDiferente(bean.qh18g,bean.qh15g+1))){
    			mensaje = "Error 1"; 
    			view = rgQH18G; 
    			error = true; 
    			return false;			
    		}
    		if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh18n,2) && Util.esDiferente(bean.qh15y,8) && Util.esDiferente(bean.qh18y,8) && (Util.esMenor(bean.qh18y,bean.qh15y) || !Util.esDiferente(bean.qh18y,bean.qh15y) || Util.esDiferente(bean.qh18y,bean.qh15y+1))){
    			mensaje = "Error 2"; 
    			view = rgQH18Y; 
    			error = true; 
    			return false;			
    		}    				
    		if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,1,2,3,4,5) && !Util.esDiferente(bean.qh18n,2)){
    			mensaje = "Error 3"; 
    			view = rgQH18N; 
    			error = true; 
    			return false;			
    		}
    		if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,1,2,3,4)  && !Util.esDiferente(bean.qh18n,1)){
    			mensaje = "Error 4"; 
    			view = rgQH18N; 
    			error = true; 
    			return false;			
    		}    				
    		if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,6) && !Util.esDiferente(bean.qh18n,2) && Util.esDiferente(bean.qh18y,1,8)){
    			mensaje = "Error 5"; 
    			view = rgQH18Y; 
    			error = true; 
    			return false;			
    		}    				
    	}
    			
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,5) && Util.esDiferente(bean.qh18n,3,4)){
    				mensaje = "Error 6"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,5) && !Util.esDiferente(bean.qh18n,3,4) && Util.esDiferente(bean.qh18y,1,8)){
    				mensaje = "Error 7"; 
    				view = rgQH18Y; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,6) && Util.esDiferente(bean.qh18n,2,8)){
    				mensaje = "Error 8"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}

    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,1) && Util.esDiferente(bean.qh15g,6,8) && Util.esDiferente(bean.qh18n,1,8)){
    				mensaje = "Error 9"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}

    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,2) && Util.esDiferente(bean.qh15y,5,8) && Util.esDiferente(bean.qh18n,2,8)){
    				mensaje = "Error 10"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,3,4,5) && Util.esDiferente(bean.qh18n,3,4,5,8)){
    				mensaje = "Error 11"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh18n,4) && Util.esDiferente(bean.qh18y,1,8)  && Util.esDiferente(bean.qh15n,4)){
    				mensaje = "Error 12"; 
    				view = rgQH15N; 
    				error = true; 
    				return false;			
    			}
    			

    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh18n,3) && !Util.esDiferente(bean.qh15n,3) && Util.esMayor(bean.qh18y,bean.qh15y) && Util.esDiferente(bean.qh18y,bean.qh15y+1)){
    				mensaje = "Error 13"; 
    				view = rgQH15N; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh18n,4) && !Util.esDiferente(bean.qh15n,4) && Util.esMayor(bean.qh18y,bean.qh15y) && Util.esDiferente(bean.qh18y,bean.qh15y+1)){
    				mensaje = "Error 14"; 
    				view = rgQH15N; 
    				error = true; 
    				return false;			
    			}
    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,4) && !Util.esDiferente(bean.qh21,1) && Util.esDiferente(bean.qh15n,4,5)){
    				mensaje = "Error 15"; 
    				view = rgQH15N; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,4) && !Util.esDiferente(bean.qh15n,4) && !Util.esDiferente(bean.qh21,1) && Util.esMayor(bean.qh20y,bean.qh15y)){
    				mensaje = "Error 16"; 
    				view = rgQH15Y; 
    				error = true; 
    				return false;			
    			}
    			
    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,3) && !Util.esDiferente(bean.qh21,1) && Util.esDiferente(bean.qh15n,3,4,5)){
    				mensaje = "Error 15_1"; 
    				view = rgQH15N; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,3) && !Util.esDiferente(bean.qh15n,3) && !Util.esDiferente(bean.qh21, 1) && Util.esMayor(bean.qh20y,bean.qh15y)){
    				mensaje = "Error 16_1"; 
    				view = rgQH15Y; 
    				error = true; 
    				return false;			
    			}
    			
    			
    			
    			
    			if (!Util.esDiferente(bean.qh18n,1,2) && !Util.esDiferente(bean.qh20n,1,2))  {			
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh18n,1) && Util.esDiferente(bean.qh20g,8) && Util.esDiferente(bean.qh18g,8) && (Util.esMenor(bean.qh18g,bean.qh20g) || !Util.esDiferente(bean.qh18g,bean.qh20g) || Util.esDiferente(bean.qh18g,bean.qh20g+1))){
    					mensaje = "Error 20"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh18n,1) && Util.esDiferente(bean.qh20g,8) && Util.esDiferente(bean.qh18g,8) && Util.esDiferente(bean.qh18g,bean.qh20g)){
    					mensaje = "Error 21"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}
    				
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh18n,2) && Util.esDiferente(bean.qh20y,8) && Util.esDiferente(bean.qh18y,8) && (Util.esMenor(bean.qh18y,bean.qh20y) || !Util.esDiferente(bean.qh18y,bean.qh20y) || Util.esDiferente(bean.qh18y,bean.qh20y+1))){
    					mensaje = "Error 22"; 
    					view = rgQH20Y; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh18n,2) && Util.esDiferente(bean.qh20y,8) && Util.esDiferente(bean.qh18y,8) && Util.esDiferente(bean.qh18y,bean.qh20y)){
    					mensaje = "Error 23"; 
    					view = rgQH20Y; 
    					error = true; 
    					return false;			
    				}
    				
    				
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh20g,1,2,3,4,5) && !Util.esDiferente(bean.qh18n,2)){
    					mensaje = "Error 24"; 
    					view = rgQH20N; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh18n,2)){
    					mensaje = "Error 25"; 
    					view = rgQH20N; 
    					error = true; 
    					return false;			
    				}
    				
    				
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,1,2,3,4)  && !Util.esDiferente(bean.qh18n,1)){
    					mensaje = "Error 26"; 
    					view = rgQH18N; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh18n,1)){
    					mensaje = "Error 27"; 
    					view = rgQH20N; 
    					error = true; 
    					return false;			
    				}
    				
    				if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh20g,6) && !Util.esDiferente(bean.qh18n,2) && Util.esDiferente(bean.qh18y,1,8)){
    					mensaje = "Error 28"; 
    					view = rgQH18Y; 
    					error = true; 
    					return false;			
    				}
    			}
    			/**************mod*/
    			if(!Util.esDiferente(bean.qh16,1)  && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,5) && Util.esDiferente(bean.qh18n,3,4)){
    				mensaje = "Error 29"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			
//    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,5) && Util.esDiferente(bean.qh18n,3,4)){
//    				mensaje = "Error 30"; 
//    				view = rgQH18G; 
//    				error = true; 
//    				return false;			
//    			}
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,5) && !Util.esDiferente(bean.qh18n,3,4) && Util.esDiferente(bean.qh18y,1,8)){
    				mensaje = "Error 31"; 
    				view = rgQH18Y; 
    				error = true; 
    				return false;			
    			}
//    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,5) && !Util.esDiferente(bean.qh18n,3,4) && Util.esDiferente(bean.qh18y,1,8)){
//    				mensaje = "Error 32"; 
//    				view = rgQH18Y; 
//    				error = true; 
//    				return false;			
//    			}
    			
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh18n,1) && Util.esDiferente(bean.qh18g,6,8) && Util.esDiferente(bean.qh20n,0,1,8)){
    				mensaje = "Error 33"; 
    				view = rgQH18Y; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh18n,1) && !Util.esDiferente(bean.qh18g,6) && Util.esDiferente(bean.qh20n,1,8)){
    				mensaje = "Error 34"; 
    				view = rgQH18Y; 
    				error = true; 
    				return false;			
    			}
    		

    			
    			
    			if (!Util.esDiferente(bean.qh15n,1,2) && !Util.esDiferente(bean.qh20n,1,2))  {	
    				if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1)  && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh20n,1) && Util.esDiferente(bean.qh20g,8) && Util.esDiferente(bean.qh15g,8) && Util.esDiferente(bean.qh20g,bean.qh15g)){
    					mensaje = "Error 50"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1)  && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh20n,1) && Util.esDiferente(bean.qh20g,8) && Util.esDiferente(bean.qh15g,8) && Util.esDiferente(bean.qh20g, bean.qh15g+1)){
    					mensaje = "Error 51"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh21,1)  && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh20n,2) && Util.esDiferente(bean.qh20y,8) && Util.esDiferente(bean.qh15y,8)&& Util.esDiferente(bean.qh20y,bean.qh15y)){
    					mensaje = "Error 52"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}
    				if(!Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh21,1)  && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh20n,2) && Util.esDiferente(bean.qh20y,8) && Util.esDiferente(bean.qh15y,8) && Util.esDiferente(bean.qh20y, bean.qh15y+1)){
    					mensaje = "Error 53"; 
    					view = rgQH20G; 
    					error = true; 
    					return false;			
    				}  	    				
    				
    			}
    			
    			
    			
    			
//    	
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,1) && Util.esDiferente(bean.qh15g,6) && Util.esDiferente(bean.qh20n,1,8)){
					mensaje = "Error 53_1"; 
					view = rgQH20N; 
					error = true; 
					return false;			
				}    		
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,6) && Util.esDiferente(bean.qh20n,1,2,8)){
					mensaje = "Error 53_2"; 
					view = rgQH20N; 
					error = true; 
					return false;			
				}    		    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,6) && !Util.esDiferente(bean.qh20n,2) && Util.esDiferente(bean.qh20y,1,8)){
					mensaje = "Error 53_3"; 
					view = rgQH20Y; 
					error = true; 
					return false;			
				}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,1) && !Util.esDiferente(bean.qh15g,6) && !Util.esDiferente(bean.qh20n,2) && !Util.esDiferente(bean.qh20y,1) && !Util.esDiferente(bean.qh21,1) ){
					mensaje = "Error 53_4"; 
					view = rgQH21; 
					error = true; 
					return false;			
				}  			
    			
    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,2) && Util.esDiferente(bean.qh15y,5) && Util.esDiferente(bean.qh20n,2,8)){
					mensaje = "Error 54_1"; 
					view = rgQH20N; 
					error = true; 
					return false;			
				}  
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,5) && Util.esDiferente(bean.qh20n,2,3,4,8)){
					mensaje = "Error 54_2"; 
					view = rgQH20N; 
					error = true; 
					return false;			
				}	
    			
    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,3) && !Util.esDiferente(bean.qh20n,3) && !Util.esDiferente(bean.qh15y,5) && Util.esDiferente(bean.qh20n,2,3,4,8)){
					mensaje = "Error 54_2"; 
					view = rgQH20N; 
					error = true; 
					return false;			
				}
    			   	
    			
    			
    			
    			
    			
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,0,1,2) && Util.esDiferente(bean.qh15y,5,8) && !Util.esDiferente(bean.qh20n,3,4,5)){
    				mensaje = "Error 54"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,5) && !Util.esDiferente(bean.qh20n,3,4) && Util.esDiferente(bean.qh20y,1,8)){
    				mensaje = "Error 55"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,2) && !Util.esDiferente(bean.qh15y,5) && !Util.esDiferente(bean.qh20n,3,4) && !Util.esDiferente(bean.qh20y,1,8) && !Util.esDiferente(bean.qh21,1) ){
    				mensaje = "Error 56"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			if(!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,0,1,2) && !Util.esDiferente(bean.qh15n,3,4,5)){
    				mensaje = "Error 56_1"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;			
    			}
    			
    			
    			if (!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,3) && !Util.esDiferente(bean.qh18n,5)) {
    				mensaje = "Error 57"; 
    				view = rgQH18N; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,5) && Util.esDiferente(bean.qh20n,3,4,5,8)) {
    				mensaje = "Error 58"; 
    				view = rgQH20N; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh15n,5) && !Util.esDiferente(bean.qh20n,5)  && !Util.esDiferente(bean.qh21,1) && Util.esMayor(bean.qh20y,bean.qh15y)) {
    				mensaje = "Error 59"; 
    				view = rgQH21; 
    				error = true; 
    				return false;
    			}
    		
    			
    			if (!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,4) && Util.esDiferente(bean.qh15y,5,6) && !Util.esDiferente(bean.qh18n,5)) {
    				mensaje = "Error 60"; 
    				view = rgQH18N; 
    				error = true; 
    				return false;
    			}	
    			if (!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,4) && !Util.esDiferente(bean.qh15y,5,6) && !Util.esDiferente(bean.qh18n,5) && Util.esDiferente(bean.qh18y,1) ) {
    				mensaje = "Error 61"; 
    				view = rgQH18N; 
    				error = true; 
    				return false;
    			}	
    			
    			
    			
    			
    			if (!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,0) && !Util.esDiferente(bean.qh16,1) && Util.esMayor(bean.qh18n,1)) {
    				mensaje = "Error 0_1"; 
    				view = rgQH18G; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh16,1) && !Util.esDiferente(bean.qh15n,0) && !Util.esDiferente(bean.qh18n,1) && Util.esDiferente(bean.qh18g,1,8)) {
    				mensaje = "Error 0_2"; 
    				view = rgQH18Y; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh15n,0) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh20n,0,1,8)) {
    				mensaje = "Error 0_3"; 
    				view = rgQH20N; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh15n,0) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,1) && Util.esDiferente(bean.qh20g,1,8)) {
    				mensaje = "Error 0_4"; 
    				view = rgQH20G; 
    				error = true; 
    				return false;
    			}
    			if (!Util.esDiferente(bean.qh15n,0) && !Util.esDiferente(bean.qh19,1) && !Util.esDiferente(bean.qh20n,1) && !Util.esDiferente(bean.qh20g,1,8) && !Util.esDiferente(bean.qh21,1)) {
    				mensaje = "Error 0_5"; 
    				view = rgQH20G; 
    				error = true; 
    				return false;
    			}
    			
    			if (!Util.esDiferente(bean.qh18n,0) && !Util.esDiferente(bean.qh19,1) && Util.esDiferente(bean.qh20n,0,8)) {
    				mensaje = "Error 0_6"; 
    				view = rgQH20N; 
    				error = true; 
    				return false;
    			}
    			
    			/**** por edades cuando la 19=2*/
    			if (!Util.esDiferente(bean.qh07,5,6)   && !Util.esDiferente(bean.qh19,2) &&  Util.esDiferente(bean.qh15n,8)   && Util.esMayor(bean.qh15n,0)) {mensaje = "Edad no corresponde 1";view = rgQH15N;error = true; return false;}    		
    			if (!Util.esDiferente(bean.qh07,7)     && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,1)) {mensaje = "Edad no corresponde 2";view = rgQH15G;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,8)     && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,2)) {mensaje = "Edad no corresponde 3";view = rgQH15G;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,9)     && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,3)) {mensaje = "Edad no corresponde 4";view = rgQH15G;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,10)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,4)) {mensaje = "Edad no corresponde 5";view = rgQH15G;error = true; return false;}
    			
    			if (!Util.esDiferente(bean.qh07,11,12) && !Util.esDiferente(bean.qh19,2) &&  Util.esDiferente(bean.qh15n,8)   && Util.esMayor(bean.qh15n,1)) {mensaje = "Edad no corresponde 16";view = rgQH15N;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,11)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,5)) {mensaje = "Edad no corresponde 7";view = rgQH15G;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,12)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,1)   && Util.esDiferente(bean.qh15g,8) && Util.esMayor(bean.qh15g,6)) {mensaje = "Edad no corresponde 8";view = rgQH15G;error = true; return false;}
    			    			
    			if (!Util.esDiferente(bean.qh07,13)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,2)   && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,1)) {mensaje = "Edad no corresponde 9";view = rgQH15Y;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,14)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,2)   && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,2)) {mensaje = "Edad no corresponde 10";view = rgQH15Y;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,15)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,2)   && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,3)) {mensaje = "Edad no corresponde 11";view = rgQH15Y;error = true; return false;}
    			
    			if (!Util.esDiferente(bean.qh07,16,17) && !Util.esDiferente(bean.qh19,2) &&  Util.esDiferente(bean.qh15n,8)   && Util.esMayor(bean.qh15n,2)) {mensaje = "Edad no corresponde 12";view = rgQH15N;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,16)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,2)   && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,4)) {mensaje = "Edad no corresponde 13";view = rgQH15Y;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,17)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,2)   && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,5)) {mensaje = "Edad no corresponde 14";view = rgQH15Y;error = true; return false;}
    			
    			if (!Util.esDiferente(bean.qh07,18)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,3,4) && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,1)) {mensaje = "Edad no corresponde 15";view = rgQH15Y;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,19)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,3,4) && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,2)) {mensaje = "Edad no corresponde 16";view = rgQH15Y;error = true; return false;}
    			
    			if (!Util.esDiferente(bean.qh07,20,21) && !Util.esDiferente(bean.qh19,2) &&  Util.esDiferente(bean.qh15n,8)   && Util.esMayor(bean.qh15n,4)) {mensaje = "Edad no corresponde 17";view = rgQH15N;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,20)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,3,4) && Util.esDiferente(bean.qh15y,8)  && Util.esMayor(bean.qh15y,3)) {mensaje = "Edad no corresponde 18";view = rgQH15Y;error = true; return false;}
    			if (!Util.esDiferente(bean.qh07,21)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,3,4) && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,4)) {mensaje = "Edad no corresponde 19";view = rgQH15Y;error = true; return false;}
    			
    			if (!Util.esDiferente(bean.qh07,22)    && !Util.esDiferente(bean.qh19,2) && !Util.esDiferente(bean.qh15n,5) && Util.esDiferente(bean.qh15y,8) && Util.esMayor(bean.qh15y,1)) {mensaje = "Edad no corresponde 20";view = rgQH15Y;error = true; return false;}
    			    			
    			return true; 
    } 
    
    public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH14.getWindowToken(), 0);
	}
    
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    @Override 
    public void cargarDatos() {   	
    	MyUtil.LiberarMemoria();	
		bean = getCuestionarioService().getSeccion01(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getEditarPersonaSeccion01().persona_id,seccionesCargado);
		
		if (bean == null) {		
			bean = new Seccion01();
			bean.id = App.getInstance().getHogar().id;
			bean.hogar_id =App.getInstance().getHogar().hogar_id;
			bean.persona_id = App.getInstance().getEditarPersonaSeccion01().persona_id+1;
			bean.qh01 = App.getInstance().getEditarPersonaSeccion01().persona_id+1;
		}
			
		if (bean.qh15n!=null) {
			bean.qh15n=bean.setConvertqh15n(bean.qh15n);
		}
		if (bean.qh15y!=null) {
				bean.qh15y=bean.setConvertqh15_y(bean.qh15y);
		}
		if (bean.qh15g!=null) {
				bean.qh15g=bean.setConvertqh15_g(bean.qh15g);
		}
		if (bean.qh18n!=null) {
			bean.qh18n=bean.setConvertqh15n(bean.qh18n);
		}
		if (bean.qh18y!=null) {
			bean.qh18y=bean.setConvertqh15_y(bean.qh18y);
		}
		if (bean.qh18g!=null) {
			bean.qh18g=bean.setConvertqh15_g(bean.qh18g);
		}
		if (bean.qh20n!=null) {
			bean.qh20n=bean.setConvertqh15n(bean.qh20n);
		}		
		if (bean.qh20y!=null) {
			bean.qh20y=bean.setConvertqh15_y(bean.qh20y);
		}
		if (bean.qh20g!=null) {
			bean.qh20g=bean.setConvertqh15_g(bean.qh20g);
		}
		entityToUI(bean);
		inicio();     	
    } 
    
    
    public void validarrgQH0724(Integer edad) {
		if (Util.esMayor(bean.qh07,24)) {
			validaredadmayor24=true;
			Util.cleanAndLockView(getActivity(),rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);	
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);	
			onrgQH14ChangeValue();	
		}
		else{			
			Util.lockView(getActivity(), false,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);		
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);		
			onrgQH14ChangeValue();			
			
		}		
	}
	
	public void validarrgQH18rgQH20(Integer edad) {
		if (MyUtil.incluyeRango(3, 9, edad)) {
			if (MyUtil.incluyeRango(3, 3, edad)) {
				rgQH15N.lockButtons(true,1,2,3,4,5);
				rgQH18N.lockButtons(true,1,2,3,4,5);
				rgQH20N.lockButtons(true,1,2,3,4,5); 
			}
			if (MyUtil.incluyeRango(4, 4, edad)) {
				rgQH15N.lockButtons(true,1,2,3,4,5);
				rgQH18N.lockButtons(true,2,3,4,5);
				rgQH20N.lockButtons(true,1,2,3,4,5); 
			}
			if (MyUtil.incluyeRango(5, 9, edad)) {
				rgQH15N.lockButtons(true,2,3,4,5);
				rgQH18N.lockButtons(true,2,3,4,5);
				rgQH20N.lockButtons(true,2,3,4,5); 
			}		 
        }
		if (MyUtil.incluyeRango(10, 14, edad)) {				
			if (MyUtil.incluyeRango(10, 10, edad)) {
				rgQH15N.lockButtons(true,2,3,4,5);
				rgQH18N.lockButtons(true,3,4,5);
				rgQH20N.lockButtons(true,2,3,4,5);
			}		
			else{
				rgQH15N.lockButtons(true,3,4,5);				
				rgQH18N.lockButtons(true,3,4,5);
				rgQH20N.lockButtons(true,3,4,5); 
			}
        }
		if (MyUtil.incluyeRango(15, 19, edad)) {
			if (MyUtil.incluyeRango(15, 15, edad)) {
				rgQH15N.lockButtons(true,3,4,5);
				rgQH18N.lockButtons(true,5);
				rgQH20N.lockButtons(true,3,4,5);
			}
			else{
			rgQH15N.lockButtons(true,5);
			rgQH18N.lockButtons(true,5);
			rgQH20N.lockButtons(true,5);
			}
        }
	}
//	public void validarEstudiante(Integer p13) {
//		if (!Util.esDiferente(p13,6)) {
//			rgQH14.lockButtons(true,1);			
//			rgQH14.requestFocus();		
//		}
//		else{
//			rgQH14.lockButtons(false,1);	
//		}
//	}
    
    private void inicio() { 
    	if (Util.esMenor(bean.qh07,3)) {
    		Util.cleanAndLockView(getActivity(),rgQH14,rgQH15G,rgQH15N,rgQH15Y,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
    		q10.setVisibility(View.GONE);	
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQH14,rgQH15G,rgQH15N,rgQH15Y,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		q10.setVisibility(View.VISIBLE);	
    		
    		RenombrarLabels();
//    		validarEstudiante(bean.qh13);
    		validarrgQH0724(bean.qh07);			
    		rgQH14.requestFocus();
    		ValidarsiesSupervisora();
    	}
    
    }
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH14.readOnly();
			rgQH15G.readOnly();
			rgQH15N.readOnly();
			rgQH15Y.readOnly();
			rgQH16.readOnly();
			rgQH17.readOnly();
			rgQH18G.readOnly();
			rgQH18N.readOnly();
			rgQH18Y.readOnly();
			rgQH19.readOnly();
			rgQH20G.readOnly();
			rgQH20N.readOnly();
			rgQH20Y.readOnly();
			rgQH21.readOnly();
			rgQH21A.readOnly();
			txtQHOBSPERSONA.setEnabled(false);
		}
	}
    
    public void RenombrarLabels(){
		String replace= "(NOMBRE)";
		String Ud="Usted";
    	
		bean.qhinfo=bean.qhinfo==null?0:bean.qhinfo;
		lblTitulo1.setText(getResources().getString(R.string.seccion01_tituloNroOrden));
		lblasistioalaescuela.setText(lblasistioalaescuela.getText().toString().replace(replace, bean.qhinfo==1?Ud:bean.qh02_1));
		lblnivelanioygrado.setText(lblnivelanioygrado.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));	
		lblmatriculado.setText(lblmatriculado.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblasiste.setText(lblasiste.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lbltituloanio.setText(lbltituloanio.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblestuvomatriculado.setText(lblestuvomatriculado.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblgradonombre.setText(lblgradonombre.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblgradoaniopasado.setText(lblgradoaniopasado.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblcolegioestatal.setText(lblcolegioestatal.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D1",bean.persona_id.toString()));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D2",bean.qh07.toString()));
		
	}
   
    public void onrgQH14ChangeValue() {
		if (Util.esMayor(bean.qh07,24)) {
			if (MyUtil.incluyeRango(2,2,rgQH14.getTagSelected("").toString())) {	
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),rgQH15N,rgQH15Y,rgQH15G,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);
				q2.setVisibility(View.GONE);	
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);
				q7.setVisibility(View.GONE);
				q8.setVisibility(View.GONE);
				q9.setVisibility(View.GONE);
			} else {
				Util.lockView(getActivity(), false,rgQH15N,rgQH15Y,rgQH15G);	
				q2.setVisibility(View.VISIBLE);						
				validarrgQH18rgQH20(bean.qh07);			
				onrgQH15nChangeValue();		
				rgQH15N.requestFocus();
			}
		}
		else{
			if (MyUtil.incluyeRango(2,2,rgQH14.getTagSelected("").toString())) {	
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),rgQH15N,rgQH15Y,rgQH15G,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);
				q2.setVisibility(View.GONE);	
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);
				q7.setVisibility(View.GONE);
				q8.setVisibility(View.GONE);
				q9.setVisibility(View.GONE);			
				rgQH16.requestFocus();			
			} else {
				Util.lockView(getActivity(), false,rgQH15N,rgQH15Y,rgQH15G,rgQH16,rgQH17,rgQH18N,rgQH18Y,rgQH18G,rgQH19,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);	
				q2.setVisibility(View.VISIBLE);	
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
				q6.setVisibility(View.VISIBLE);
				q7.setVisibility(View.VISIBLE);
				q8.setVisibility(View.VISIBLE);
				q9.setVisibility(View.VISIBLE);							
				onrgQH15nChangeValue();				
				onrgQH16ChangeValue();
				onrgQH19ChangeValue();	
				validarrgQH18rgQH20(bean.qh07);	
				rgQH15N.requestFocus();
			}
		}
	}
	
	public void onrgQH15YChangeValue() {
		if (MyUtil.incluyeRango(47, 97, bean.qh07)  && MyUtil.incluyeRango(2,8,rgQH15Y.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQH15N.getTagSelected("").toString())) {		
			Util.cleanAndLockView(getActivity(),rgQH15G);
			Util.lockView(getActivity(), false,rgQH15G);			
			rgQH16.requestFocus();				
		} 			
	}
	public void onrgQH15GChangeValue() {		
		if (MyUtil.incluyeRango(47, 97, bean.qh07) && MyUtil.incluyeRango(1,7,rgQH15G.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQH15N.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQH15Y);
			Util.lockView(getActivity(), false,rgQH15Y);
			rgQH15Y.lockButtons(true,0,6);
			rgQH16.requestFocus();				
		} 			
	}		
	
	
	public void onrgQH16ChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQH14.getTagSelected("").toString())) {	
			if (MyUtil.incluyeRango(2,2,rgQH16.getTagSelected("").toString())) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),rgQH17,rgQH18N,rgQH18Y,rgQH18G);		
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);					
				rgQH19.requestFocus();
			} 			
			else{
				Util.lockView(getActivity(), false,rgQH17,rgQH18N,rgQH18Y,rgQH18G);				
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);		
				validarrgQH18rgQH20(bean.qh07);
				onrgQH18nChangeValue();	
				rgQH17.requestFocus();
			}
		}
	}
	
	public void onrgQH17ChangeValue() {
		if (MyUtil.incluyeRango(1,2,rgQH17.getTagSelected("").toString())) {
			validarrgQH18rgQH20(bean.qh07);	
			onrgQH19ChangeValue();			
			rgQH18N.requestFocus();
		}	
	}
	
	
	public void onrgQH19ChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQH14.getTagSelected("").toString())) {	
			if (MyUtil.incluyeRango(2,2,rgQH19.getTagSelected("").toString())) {
				if (MyUtil.incluyeRango(1,1,rgQH17.getTagSelected("").toString()) && MyUtil.incluyeRango(1,2,rgQH18N.getTagSelected("").toString())) {
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),rgQH20N,rgQH20Y,rgQH20G,rgQH21);				
					Util.lockView(getActivity(), false,rgQH21A);	
					q7.setVisibility(View.GONE);
					q8.setVisibility(View.GONE);
					q9.setVisibility(View.VISIBLE);	
					rgQH21A.requestFocus();
				}
				else{				
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);		
					q7.setVisibility(View.GONE);
					q8.setVisibility(View.GONE);
					q9.setVisibility(View.GONE);					
				}
			}
			if (MyUtil.incluyeRango(1,1,rgQH19.getTagSelected("").toString())) {
				if (MyUtil.incluyeRango(1,1,rgQH17.getTagSelected("").toString()) && MyUtil.incluyeRango(1,2,rgQH18N.getTagSelected("").toString())) {								
					Util.lockView(getActivity(), false,rgQH20N,rgQH20Y,rgQH20G,rgQH21,rgQH21A);			
					q7.setVisibility(View.VISIBLE);
					q8.setVisibility(View.VISIBLE);
					q9.setVisibility(View.VISIBLE);	
					validarrgQH18rgQH20(bean.qh07);
					onrgQH20nChangeValue();
					rgQH20N.requestFocus();
				}
				else{
					Util.lockView(getActivity(), false,rgQH20N,rgQH20Y,rgQH20G,rgQH21);			
					q7.setVisibility(View.VISIBLE);
					q8.setVisibility(View.VISIBLE);		
					validarrgQH18rgQH20(bean.qh07);	
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),rgQH21A);					
					q9.setVisibility(View.GONE);
					onrgQH20nChangeValue();
					rgQH20N.requestFocus();
				}				
			}					
		}
	}
	
	
	public void onrgQH15nChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQH15N.getTagSelected("").toString())) {	
			Log.e("15n","15n");
			Util.cleanAndLockView(getActivity(),rgQH15G);			
			Util.lockView(getActivity(), false,rgQH15Y);
			rgQH15Y.lockButtons(true,1,2,3,4,5,6,7);
			rgQH15Y.requestFocus();
		}
		if (MyUtil.incluyeRango(2,2,rgQH15N.getTagSelected("").toString())) {
			Log.e("222","222");
			if (Util.esMenor(bean.qh07,47)) {	
				if (!Util.esDiferente(bean.qh07,5)) {
					Log.e("es igual a 5","primaria 1");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);
					rgQH15G.lockButtons(true,1,2,3,4,5);
					rgQH15G.requestFocus();
				}
				if (!Util.esDiferente(bean.qh07,6)) {
					Log.e("es igual a 6","primaria 1");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);
					rgQH15G.lockButtons(true,2,3,4,5);
					rgQH15G.requestFocus();
				}
				if (!Util.esDiferente(bean.qh07,7)) {
					Log.e("es igual a 7","primaria 1,2");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);
					rgQH15G.lockButtons(true,3,4,5);
					rgQH15G.requestFocus();
				}
				if (!Util.esDiferente(bean.qh07,8)) {
					Log.e("es igual 8","primaria 1,2,3");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);
					rgQH15G.lockButtons(true,4,5);
					rgQH15G.requestFocus();
				}	
				if (!Util.esDiferente(bean.qh07,9)) {
					Log.e("es igual 8","primaria 1,2,3");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);
					rgQH15G.lockButtons(true,5);
					rgQH15G.requestFocus();
				}
				if (MyUtil.incluyeRango(10, 46, bean.qh07)) {
					Log.e("es igual de 10 a 47"," de 10 y menor a 48");
					Util.cleanAndLockView(getActivity(),rgQH15Y);
					Util.lockView(getActivity(), false,rgQH15G);				
					rgQH15G.requestFocus();
				}
			}
			else{
				Log.e("de 47"," mas a 47");
				Util.lockView(getActivity(), false,rgQH15Y,rgQH15G);
				rgQH15Y.lockButtons(true,6);
				onrgQH15YChangeValue();
				onrgQH15GChangeValue();
				rgQH15Y.requestFocus();
			}				
		}
		if (MyUtil.incluyeRango(3,3,rgQH15N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,11)) {
				Log.e("es igual a 11","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,2,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,12)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,13)) {
				Log.e("es igual a 13","secundaria 1,2");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,14)) {
				Log.e("es igual a 14","secundaria 1,2,3");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,5,6);
				rgQH15Y.requestFocus();
			}	
			if (Util.esMayor(bean.qh07,14)) {
				Log.e("es mayor a 15","secundaria todos");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,6);
				rgQH15Y.requestFocus();
			}			
		}
		if (MyUtil.incluyeRango(4,4,rgQH15N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 16","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,2,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 18","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,19)) {
				Log.e("es igual a 19","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,5,6);
				rgQH15Y.requestFocus();
			}		
			if (Util.esMayor(bean.qh07,19)) {
				Log.e("es mayor a 20","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,6);
				rgQH15Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(5,5,rgQH15N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 16","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,2,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 18","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,19)) {
				Log.e("es igual a 19","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,5,6);
				rgQH15Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,20)) {
				Log.e("es igual a 20","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,6);
				rgQH15Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,20)) {
				Log.e("es mayor a 20","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0);
				rgQH15Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(6,6,rgQH15N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,21)) {
				Log.e("es igual a 21","superior posgrado");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,2,3,4,5,6);
				rgQH15Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,21)) {
				Log.e("es mayor a 21","pos grado");
				Util.cleanAndLockView(getActivity(),rgQH15G);
				Util.lockView(getActivity(), false,rgQH15Y);
				rgQH15Y.lockButtons(true,0,3,4,5,6);
				rgQH15Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(7,7,rgQH15N.getTagSelected("").toString())) {
			Log.e("no sabe ","bloquea a�o y grado");
			Util.cleanAndLockView(getActivity(),rgQH15Y,rgQH15G);
			rgQH16.requestFocus();
		}	
	}
	



	public void onrgQH18nChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQH18N.getTagSelected("").toString())) {	
			Log.e("18n","18n");
			Util.cleanAndLockView(getActivity(),rgQH18G);
			Util.lockView(getActivity(), false,rgQH18Y);
			rgQH18Y.lockButtons(true,1,2,3,4,5,6,7);
			onrgQH19ChangeValue();
			rgQH18Y.requestFocus();
		}
		if (MyUtil.incluyeRango(2,2,rgQH18N.getTagSelected("").toString())) {		
			if (!Util.esDiferente(bean.qh07,4)) {
				Log.e("es igual a 6","primaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				rgQH18G.lockButtons(true,1,2,3,4,5);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,5)) {
				Log.e("es igual a 6","primaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				rgQH18G.lockButtons(true,2,3,4,5);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,6)) {
				Log.e("es igual a 6","primaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				rgQH18G.lockButtons(true,3,4,5);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,7)) {
				Log.e("es igual a 7","primaria 1,2");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				rgQH18G.lockButtons(true,4,5);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,8)) {
				Log.e("es igual 8","primaria 1,2,3");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				rgQH18G.lockButtons(true,5);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}									
			if (Util.esMayor(bean.qh07,8)) {
				Log.e("es mayor a 9","primaria todos");
				Util.cleanAndLockView(getActivity(),rgQH18Y);
				Util.lockView(getActivity(), false,rgQH18G);
				onrgQH19ChangeValue();
				rgQH18G.requestFocus();
			}							
		}		
		if (MyUtil.incluyeRango(3,3,rgQH18N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,10)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,2,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,11)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,12)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}	
			if (!Util.esDiferente(bean.qh07,13)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,13)) {
				Log.e("es mayor a 15","secundaria todos");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			
		}
		if (MyUtil.incluyeRango(4,4,rgQH18N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,15)) {
				Log.e("es igual a 16","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,2,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,18)) {
				Log.e("es mayor a 20","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(5,5,rgQH18N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,15)) {
				Log.e("es igual a 15","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,2,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 16","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}	
			if (!Util.esDiferente(bean.qh07,19)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}	
			if (Util.esMayor(bean.qh07,19)) {
				Log.e("es mayor a 19","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0);	
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
		}
		
		if (MyUtil.incluyeRango(6,6,rgQH18N.getTagSelected("").toString())) {
			if (!Util.esDiferente(bean.qh07,20)) {
				Log.e("es igual a 21 y 22","superior posgrado");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,2,3,4,5,6);	
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}	
			if (Util.esMayor(bean.qh07,20)) {
				Log.e("es mayor a 22","pos grado");
				Util.cleanAndLockView(getActivity(),rgQH18G);
				Util.lockView(getActivity(), false,rgQH18Y);
				rgQH18Y.lockButtons(true,0,3,4,5,6);
				onrgQH19ChangeValue();
				rgQH18Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(7,7,rgQH18N.getTagSelected("").toString())) {
			Log.e("no sabe ","bloquea a�o y grado");
			Util.cleanAndLockView(getActivity(),rgQH18Y,rgQH18G);	
			onrgQH19ChangeValue();
			rgQH19.requestFocus();
		}			
	
	}
	
	
	
	public void onrgQH20nChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQH20N.getTagSelected("").toString())) {	
			Log.e("15n","15n");
			Util.cleanAndLockView(getActivity(),rgQH20G);			
			Util.lockView(getActivity(), false,rgQH20Y);
			rgQH20Y.lockButtons(true,1,2,3,4,5,6,7);
			rgQH20Y.requestFocus();
			
			
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {
				Log.e("aaa","sssdfe");
				Util.cleanAndLockView(getActivity(),rgQH21);
				q8.setVisibility(View.GONE);
			}		
			
			
		}
		if (MyUtil.incluyeRango(2,2,rgQH20N.getTagSelected("").toString())) {	
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			
			if (!Util.esDiferente(bean.qh07,5)) {
				Log.e("es igual a 5","primaria 1");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);
				rgQH20G.lockButtons(true,1,2,3,4,5);
				rgQH20G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,6)) {
				Log.e("es igual a 6","primaria 1");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);
				rgQH20G.lockButtons(true,2,3,4,5);
				rgQH20G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,7)) {
				Log.e("es igual a 7","primaria 1,2");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);
				rgQH20G.lockButtons(true,3,4,5);
				rgQH20G.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,8)) {
				Log.e("es igual 8","primaria 1,2,3");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);
				rgQH20G.lockButtons(true,4,5);
				rgQH20G.requestFocus();
			}	
			if (!Util.esDiferente(bean.qh07,9)) {
				Log.e("es igual 8","primaria 1,2,3");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);
				rgQH20G.lockButtons(true,5);
				rgQH20G.requestFocus();
			}
			if (Util.esMayor(bean.qh07,9)) {
				Log.e("es igual de 10"," de 10 y menor a 47");
				Util.cleanAndLockView(getActivity(),rgQH20Y);
				Util.lockView(getActivity(), false,rgQH20G);				
				rgQH20G.requestFocus();
			}
		}		
		if (MyUtil.incluyeRango(3,3,rgQH20N.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			if (!Util.esDiferente(bean.qh07,11)) {
				Log.e("es igual a 11","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,2,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,12)) {
				Log.e("es igual a 12","secundaria 1");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,13)) {
				Log.e("es igual a 13","secundaria 1,2");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,14)) {
				Log.e("es igual a 14","secundaria 1,2,3");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,5,6);
				rgQH20Y.requestFocus();
			}	
			if (Util.esMayor(bean.qh07,14)) {
				Log.e("es mayor a 15","secundaria todos");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,6);
				rgQH20Y.requestFocus();
			}
			
		}
		if (MyUtil.incluyeRango(4,4,rgQH20N.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 15","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,2,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 18","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,19)) {
				Log.e("es igual a 19","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,5,6);
				rgQH20Y.requestFocus();
			}		
			if (Util.esMayor(bean.qh07,19)) {
				Log.e("es mayor a 20","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,6);
				rgQH20Y.requestFocus();
			}
			
			if (MyUtil.incluyeRango(5,5,rgQH15N.getTagSelected("").toString()) && MyUtil.incluyeRango(1,1,rgQH19.getTagSelected("").toString()) && MyUtil.incluyeRango(4,4,rgQH20N.getTagSelected("").toString())) {
				validarMensaje("VERIFICAR P.20");
				error=true;
			}
		}
		if (MyUtil.incluyeRango(5,5,rgQH20N.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			if (!Util.esDiferente(bean.qh07,16)) {
				Log.e("es igual a 16","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,2,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,17)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,18)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,19)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,5,6);
				rgQH20Y.requestFocus();
			}
			if (!Util.esDiferente(bean.qh07,20)) {
				Log.e("es igual a 17","superior no uni 1 y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,6);
				rgQH20Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,20)) {
				Log.e("es mayor a 20","superior no uni y univer");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0);
				rgQH20Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(6,6,rgQH20N.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			if (!Util.esDiferente(bean.qh07,21)) {
				Log.e("es igual a 22","superior posgrado");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,2,3,4,5,6);
				rgQH20Y.requestFocus();
			}
			if (Util.esMayor(bean.qh07,21)) {
				Log.e("es mayor a 22","pos grado");
				Util.cleanAndLockView(getActivity(),rgQH20G);
				Util.lockView(getActivity(), false,rgQH20Y);
				rgQH20Y.lockButtons(true,0,3,4,5,6);
				rgQH20Y.requestFocus();
			}
		}
		if (MyUtil.incluyeRango(7,7,rgQH20N.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1, 1, rgQH19.getTagSelected("").toString())) {		
				Util.lockView(getActivity(), false,rgQH21);
				q8.setVisibility(View.VISIBLE);
			}
			Log.e("no sabe ","bloquea a�o y grado");
			Util.cleanAndLockView(getActivity(),rgQH20Y,rgQH20G);			
		}	

	}
  	
  	
  	
  	
    
  	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}	 
	
	public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
	
	public void EliminarSeccion03(){
		SeccionCapitulo[] seccionesCargadoseccion3 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"ID", "HOGAR_ID", "PERSONA_ID", "PERSONA_ID_ORDEN","PREGUNTA_ID") };
		Seccion03 beneficiado= getSeccion03Service().getBeneficiario(bean.id,bean.hogar_id,bean.persona_id, App.QALIWARMA, seccionesCargadoseccion3);
		if(beneficiado!=null){
			if(!MyUtil.incluyeRango(App.EDADMINIMAQALIWARMA, App.EDADMAXIMAQALIWARMA, bean.qh07) || !MyUtil.incluyeRango(0, 1, bean.qh18n) || !MyUtil.incluyeRango(1, 1, bean.qh21a)){
				try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiado);
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        }
			}
		}
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(bean);
		if (bean.qh15n!=null) {
			bean.qh15n=bean.getConvertqh15n(bean.qh15n);
		}			
		if (bean.qh15y!=null) {
			bean.qh15y=bean.getConvertqh15_y(bean.qh15y);
		}
		if (bean.qh15g!=null) {
			bean.qh15g=bean.getConvertqh15_g(bean.qh15g);
		}
		if (bean.qh18n!=null) {
			bean.qh18n=bean.getConvertqh15n(bean.qh18n);
		}
		if (bean.qh18y!=null) {
			bean.qh18y=bean.getConvertqh15_y(bean.qh18y);
		}
		if (bean.qh18g!=null) {
			bean.qh18g=bean.getConvertqh15_g(bean.qh18g);
		}
		if (bean.qh20n!=null) {
			bean.qh20n=bean.getConvertqh15n(bean.qh20n);
		}		
		if (bean.qh20y!=null) {
			bean.qh20y=bean.getConvertqh15_y(bean.qh20y);
		}	
		if (bean.qh20g!=null) {
			bean.qh20g=bean.getConvertqh15_g(bean.qh20g);
		}
		boolean flag=false;
		try {	
			flag = getCuestionarioService().saveOrUpdate(bean,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return App.NODEFINIDO;
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
