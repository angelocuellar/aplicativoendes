package gob.inei.endes2024.fragment.CIseccion_01_03;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_005 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public TextField txtQI229_NOM;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI229_A;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI229_B;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI229_C;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI229_D;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI229_E;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI229_F;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI229_G;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI229_H;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI229_I;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI229_J;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQI229_K;
	@FieldAnnotation(orderIndex=13)
	public CheckBoxField chbQI229_L;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI229_X;
	@FieldAnnotation(orderIndex=15)
	public TextField txtQI229_O;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI229_Y;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	Seccion01 bean;
	Seccion01 seccion01;
	public GridComponent2 gdLugarSalud;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta229,lblpregunta229_1,lblpregunta229lug,lblpregunta229lug_1,lblpregunta229hos,lblpregunta229_Pu_Ind,lblpregunta229_Pri_Ind,lblpregunta229_Org_Ind,lbllugar;
	LinearLayout q0,q1,q2;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	private CuestionarioService hogarService;
	
	public CISECCION_02Fragment_005() {}
	public CISECCION_02Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226","QI229_NOM","QI229_A","QI229_B","QI229_C","QI229_D","QI229_E","QI229_F","QI229_G","QI229_H","QI229_I","QI229_J","QI229_K","QI229_L","QI229_X","QI229_O","QI229_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI229_NOM","QI229_A","QI229_B","QI229_C","QI229_D","QI229_E","QI229_F","QI229_G","QI229_H","QI229_I","QI229_J","QI229_K","QI229_L","QI229_X","QI229_O","QI229_Y")};
//		seccionesGrabado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z") };
//		seccionesCargado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","ID","HOGAR_ID","PERSONA_ID") };
		return rootView;
    }
    @Override
    protected void buildFields() {

		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta229 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229);
		lblpregunta229_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_02qi229_1);
		lblpregunta229lug = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229lug);
		lblpregunta229lug_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_02qi229lug_2);
		lblpregunta229hos = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi229hos);
		Spanned texto229Pu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta229_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta229_Pu_Ind.setText(texto229Pu);
		Spanned texto229Pri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta229_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta229_Pri_Ind.setText(texto229Pri);
		Spanned texto229Org =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta229_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta229_Org_Ind.setText(texto229Org);
		
		txtQI229_NOM=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 300).alfanumerico();
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 450).text(R.string.ciseccion_02qi229nom).textSize(16);
		chbQI229_A=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_AChangeValue");
		chbQI229_B=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_BChangeValue");
		chbQI229_C=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_CChangeValue");
		chbQI229_D=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_DChangeValue");
		chbQI229_E=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_EChangeValue");
		chbQI229_F=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_FChangeValue");
		chbQI229_G=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_GChangeValue");
		chbQI229_H=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_HChangeValue");
		chbQI229_I=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_IChangeValue");
		chbQI229_J=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_JChangeValue");
		chbQI229_K=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_KChangeValue");
		chbQI229_L=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_LChangeValue");
		chbQI229_X=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_XChangeValue");
		txtQI229_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico();
		chbQI229_Y=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi229_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI229_YChangeValue");

		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI229_NOM);
    }
    
    @Override
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,lblpregunta229,lblpregunta229_1);
		LinearLayout ly229 = new LinearLayout(getActivity());
		ly229.addView(chbQI229_X);
		ly229.addView(txtQI229_O);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,q1,gdLugarSalud.component(),lblpregunta229lug,lblpregunta229lug_1,lblpregunta229_Pu_Ind,lblpregunta229hos,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,lblpregunta229_Pri_Ind,chbQI229_H,chbQI229_I,chbQI229_J,lblpregunta229_Org_Ind,chbQI229_K,chbQI229_L,ly229,chbQI229_Y);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q2);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
      
    private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
    
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer p226 =individual.qi226==null?0:individual.qi226;
		
		if (p226!=2 && p226!=8) {
			if (!verificarCheck() && !chbQI229_Y.isChecked()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI229");
				view = chbQI229_A;
				error = true;
				return false;
			}
			if (chbQI229_X.isChecked()) {
				if (Util.esVacio(individual.qi229_o)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQI229_O;
					error = true;
					return false;
				}
			}
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual);
		inicio();
    }
    
    public void validarPregunta226() {
    	Integer p226 =individual.qi226==null?0:individual.qi226;
    	if (p226==2 || p226==8  ) {
    		Util.cleanAndLockView(getActivity(),txtQI229_NOM,txtQI229_O,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
		}
    	else{
    		Util.lockView(getActivity(),false,txtQI229_NOM,txtQI229_O,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		verificarPregunta229();
    	}
    }
    
    
    private void inicio() {
    	validarPregunta226();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void verificarPregunta229(){
    	if (!chbQI229_X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),txtQI229_O);	
		}else {
			Util.lockView(getActivity(),false,txtQI229_O);
		}
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    	if (chbQI229_Y.isChecked()){
  			Util.cleanAndLockView(getActivity(),txtQI229_NOM,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
  		}
  		else {
  			Util.lockView(getActivity(),false,txtQI229_NOM,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
  		}
    }
    
    
    public boolean verificarCheck() {
  		if (chbQI229_A.isChecked() || chbQI229_B.isChecked() || chbQI229_C.isChecked() || chbQI229_D.isChecked() || chbQI229_E.isChecked() || chbQI229_F.isChecked() || chbQI229_G.isChecked() || chbQI229_H.isChecked() || chbQI229_I.isChecked() || chbQI229_J.isChecked() || chbQI229_K.isChecked() || chbQI229_L.isChecked() ||chbQI229_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public void onqrgQI229_AChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_BChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_CChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_DChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_EChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_FChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_GChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_HChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_IChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_AJChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_KChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_LChangeValue() {
    	if (verificarCheck()) {
    		Util.cleanAndLockView(getActivity(),chbQI229_Y);
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI229_Y);
    	}
    }
    public void onqrgQI229_XChangeValue() {
    	if (chbQI229_X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI229_O);
  			if (verificarCheck()) {
  				Util.cleanAndLockView(getActivity(),chbQI229_Y);
  			}
  			txtQI229_O.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI229_O);  
  			if (!verificarCheck()) {  			
  				Util.lockView(getActivity(),false,chbQI229_Y);  				
			}
  		}
    }
    public void onqrgQI229_YChangeValue() {
  		if (chbQI229_Y.isChecked()){  		
  			Util.cleanAndLockView(getActivity(),txtQI229_NOM,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
  		}
  		else {
  			Util.lockView(getActivity(),false,txtQI229_NOM,chbQI229_A,chbQI229_B,chbQI229_C,chbQI229_D,chbQI229_E,chbQI229_F,chbQI229_G,chbQI229_H,chbQI229_I,chbQI229_J,chbQI229_K,chbQI229_L,chbQI229_X);
  		}
  	}
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		txtQI229_NOM.readOnly();
    		txtQI229_O.readOnly();
    		chbQI229_A.readOnly();
    		chbQI229_B.readOnly();
    		chbQI229_C.readOnly();
    		chbQI229_D.readOnly();
    		chbQI229_E.readOnly();
    		chbQI229_F.readOnly();
    		chbQI229_G.readOnly();
    		chbQI229_H.readOnly();
    		chbQI229_I.readOnly();
    		chbQI229_J.readOnly();
    		chbQI229_K.readOnly();
    		chbQI229_L.readOnly();
    		chbQI229_X.readOnly();
    		chbQI229_Y.readOnly();
    	}
    } 	

    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}