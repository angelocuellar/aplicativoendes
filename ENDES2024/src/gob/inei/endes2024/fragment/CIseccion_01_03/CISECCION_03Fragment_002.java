package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI313;
//	@FieldAnnotation(orderIndex=2)
//	public IntegerField txtQI314;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI315M;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI315Y;
//	@FieldAnnotation(orderIndex=4)
//	public RadioGroupOtherField rgQI315B;
//	@FieldAnnotation(orderIndex=5)
//	public TextField txtQI315B_O;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI316M;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQI316Y;
	@FieldAnnotation(orderIndex=6)
	public DecimalField txtQI317A;
	@FieldAnnotation(orderIndex=7)
	public SpinnerField spnQI_FANT;
	@FieldAnnotation(orderIndex=8)
	public TextField txtQI_FANT_O;

	public TextField txtCabecera;
	CISECCION_01_03 individual;
	
	Spanned texto313Hombre,texto313Mujer;
	public GridComponent2 gdCostoEsteril,gdEmbarazo,gdEmbarazoUltimo,gdCostoEsteril1er,gdLugarSalud;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta314,lblpregunta315,lblpregunta315B,lblpregunta316,lblpregunta317A,lblpregunta317A_ind,lblcostoEsteril,lblcostoEsteril1er,lblmes,lblanio,lblmes1,lblanio1,lblpregunta313_Ind,lblfanticoncepcion,lblfanticoncepcioninf;
	public CheckBoxField  /*#retiro de pregruntas# //chbP314_1,chbP314_2,*/chbP317A_1,chbP317A_2;
	LinearLayout q0,q1,q3,q5,q6,q7;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoNinio;

  public CISECCION_03Fragment_002() {}
  public CISECCION_03Fragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
  }
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
//		rango(getActivity(), txtQI315M, 1, 12);
//		rango(getActivity(), txtQI315Y, 1900, 2016);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI313","QI314","QI315M","QI315Y","QI315B","QI315B_O","QI316M","QI316Y","QI317A","QI226","QI230","QI304","QI310","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X","QI311_O","QI_FANT","QI_FANT_O","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoNinio = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI215Y","QI215M")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI313","QI314","QI315M","QI315Y","QI315B","QI315B_O","QI316M","QI316Y","QI317A","QI_FANT","QI_FANT_O")};
		return rootView;   
  }
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta314 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi314);
		lblpregunta315 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi315);
//		lblpregunta315B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi315b);
		lblpregunta316 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi316);
		lblpregunta317A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi317a);
		lblpregunta317A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_03qi317a_ind);
		lblfanticoncepcion = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi_fantanticoncepcion);
		lblfanticoncepcioninf = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi_fantanticoncepcioninf);
		spnQI_FANT = new SpinnerField(getActivity()).size(altoComponente+15, 750).callback("onQICOL2ChangeValue");
		LlenarSpinerColumna03();
		txtQI_FANT_O=new TextField(getActivity()).size(altoComponente, 750).maxLength(1000);
		rgQI313=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi313_1,R.string.ciseccion_03qi313_2,R.string.ciseccion_03qi313_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);

//		txtQI314=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
//		chbP314_1=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi314_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbP314_1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				// TODO Auto-generated method stub
//				if (isChecked){
//					MyUtil.LiberarMemoria();
//		    		chbP314_2.setChecked(false);
//		    		Util.cleanAndLockView(getActivity(),txtQI314);
//		  		}else {
//		  			MyUtil.LiberarMemoria();
//					if (chbP314_2.isChecked()) {
//			    		Util.cleanAndLockView(getActivity(),txtQI314);
//						txtQI314.setVisibility(View.GONE);
//					}else {
//						Util.lockView(getActivity(), false,txtQI314);
//						txtQI314.setVisibility(View.VISIBLE);
//					}
//				}
//			}
//		  });
		
		
//		chbP314_2=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi314_2, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbP314_2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				// TODO Auto-generated method stub
//				if (isChecked){
//					MyUtil.LiberarMemoria();
//		    		chbP314_1.setChecked(false);
//		    		Util.cleanAndLockView(getActivity(),txtQI314);
//					txtQI314.setVisibility(View.GONE);
//		  		}else {
//		  			MyUtil.LiberarMemoria();
//					if (chbP314_1.isChecked()) {
//			    		Util.cleanAndLockView(getActivity(),txtQI314);
//			    		txtQI314.setVisibility(View.GONE);
//					}else {
//						Util.lockView(getActivity(), false,txtQI314);
//						txtQI314.setVisibility(View.VISIBLE);
//						Util.lockView(getActivity(), false);
//					}
//				}
//			}
//		});
		
		chbP317A_1=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi317a_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbP317A_1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		chbP317A_2.setChecked(false);
		    		Util.cleanAndLockView(getActivity(),txtQI317A);
		  		}else {
		  			MyUtil.LiberarMemoria();
					if (chbP317A_2.isChecked()) {
			    		Util.cleanAndLockView(getActivity(),txtQI317A);
			    		txtQI317A.setVisibility(View.GONE);
					}else {
						Util.lockView(getActivity(), false,txtQI317A);
						txtQI317A.setVisibility(View.VISIBLE);
					}
				}
			}
		  });
			
		  chbP317A_2=new CheckBoxField(this.getActivity(), R.string.ciseccion_03qi317a_2, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		  chbP317A_2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		chbP317A_1.setChecked(false);
		    		Util.cleanAndLockView(getActivity(),txtQI317A);
		    		txtQI317A.setVisibility(View.GONE);
		  		}else {
		  			MyUtil.LiberarMemoria();
					if (chbP317A_1.isChecked()) {
			    		Util.cleanAndLockView(getActivity(),txtQI317A);
			    		txtQI317A.setVisibility(View.GONE);
					}else {
						Util.lockView(getActivity(), false,txtQI317A);
						txtQI317A.setVisibility(View.VISIBLE);
						Util.lockView(getActivity(), false);
					}
				}
			}
		});
		txtQI315M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQI315Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
//		rgQI315B=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi315b_1,R.string.ciseccion_03qi315b_2,R.string.ciseccion_03qi315b_3,R.string.ciseccion_03qi315b_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQI315B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
//		rgQI315B.agregarEspecifique(3,txtQI315B_O);
		txtQI316M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQI316Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).callback("onQI316YChangeValue");
		txtQI317A=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1);
		
		texto313Hombre =Html.fromHtml("313. �Antes de su operaci�n de esterilizaci�n le dijeron que usted no podr�a tener (m�s) hijas e hijos a causa de esta operaci�n?");
		texto313Mujer =Html.fromHtml("313. �Antes de la operaci�n de esterilizaci�n le dijeron a su esposo/ compa�ero que no podr�a tener (m�s) hijas e hijos a causa de esta operaci�n?");
		lblpregunta313_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);

		lblcostoEsteril  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi314_c).textSize(16).centrar();
		lblcostoEsteril1er  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi317a_c).textSize(16).centrar();
		lblmes  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi315m).textSize(16).centrar();
		lblanio  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi315y).textSize(16).centrar();
		lblmes1  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi316m).textSize(16).centrar();
		lblanio1  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_03qi316y).textSize(16).centrar();
		
		
//		gdCostoEsteril = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
//		gdCostoEsteril.addComponent(lblcostoEsteril);
//		gdCostoEsteril.addComponent(txtQI314);
//		gdCostoEsteril.addComponent(chbP314_1,2);
//		gdCostoEsteril.addComponent(chbP314_2,2);
		
		gdEmbarazo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdEmbarazo.addComponent(lblmes);
		gdEmbarazo.addComponent(txtQI315M);
		gdEmbarazo.addComponent(lblanio);
		gdEmbarazo.addComponent(txtQI315Y);
		
		gdEmbarazoUltimo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdEmbarazoUltimo.addComponent(lblmes1);
		gdEmbarazoUltimo.addComponent(txtQI316M);
		gdEmbarazoUltimo.addComponent(lblanio1);
		gdEmbarazoUltimo.addComponent(txtQI316Y);
		
		gdCostoEsteril1er = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdCostoEsteril1er.addComponent(lblcostoEsteril1er);
		gdCostoEsteril1er.addComponent(txtQI317A);
		gdCostoEsteril1er.addComponent(chbP317A_1,2);
		gdCostoEsteril1er.addComponent(chbP317A_2,2);
    }
    @Override
    protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta313_Ind,rgQI313);
//		q2 = createQuestionSection(lblpregunta314,gdCostoEsteril.component());
		q3 = createQuestionSection(lblpregunta315,gdEmbarazo.component());
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta315B,rgQI315B);
		q5 = createQuestionSection(lblpregunta316,gdEmbarazoUltimo.component());
		q6 = createQuestionSection(lblpregunta317A,lblpregunta317A_ind,gdCostoEsteril1er.component());
		q7 = createQuestionSection(lblfanticoncepcion,lblfanticoncepcioninf,spnQI_FANT,txtQI_FANT_O);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
//		form.addView(q2);
		form.addView(q3);
//		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		
//		if (chbP314_1.isChecked())
//			individual.qi314=9995;
//		if (chbP314_2.isChecked())
//			individual.qi314=9998;
		
		if (chbP317A_1.isChecked()){
			BigDecimal data= new BigDecimal(995);
			individual.qi317a=data;
		}
		if (chbP317A_2.isChecked()){
			BigDecimal data= new BigDecimal(998);
			individual.qi317a=data;
		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		App.getInstance().getPersonaCuestionarioIndividual().qi315y=individual.qi315y;
		App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer int311_a = individual.qi311_a==null?0:individual.qi311_a;
    	Integer int311_b = individual.qi311_b==null?0:individual.qi311_b;
    	Integer int311_c = individual.qi311_c==null?0:individual.qi311_c;
    	Integer int311_d = individual.qi311_d==null?0:individual.qi311_d;
    	Integer int311_e = individual.qi311_e==null?0:individual.qi311_e;
    	Integer int311_f = individual.qi311_f==null?0:individual.qi311_f;
    	Integer int311_g = individual.qi311_g==null?0:individual.qi311_g;
    	Integer int311_h = individual.qi311_h==null?0:individual.qi311_h;
    	Integer int311_i = individual.qi311_i==null?0:individual.qi311_i;
    	Integer int311_j = individual.qi311_j==null?0:individual.qi311_j;
    	Integer int311_k = individual.qi311_k==null?0:individual.qi311_k;
    	Integer int311_l = individual.qi311_l==null?0:individual.qi311_l;
    	Integer int311_m = individual.qi311_m==null?0:individual.qi311_m;
    	Integer int311_x = individual.qi311_x==null?0:individual.qi311_x;
//    	Integer int315b = individual.qi315b==null?0:individual.qi315b;
    	Integer int310 = individual.qi310==null?0:individual.qi310;
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	Integer int226 = individual.qi226==null?0:individual.qi226;
    	
		CISECCION_02 ninio=getCuestionarioService().getUltimoNacimiento(individual.id,individual.hogar_id,individual.persona_id,seccionesCargadoNinio);
		Integer anio=0,mes=0;
		
		if (ninio!=null) {
			 anio=ninio.qi215y==null?0:ninio.qi215y;
			 mes = ninio.qi215m==null?0:Integer.parseInt(ninio.qi215m);
		}
		
		if (int311_a==1  || int311_b==1 ) {
			if (Util.esVacio(individual.qi313)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI313");
				view = rgQI313;
				error = true;
				return false;
			}
//			if (Util.esVacio(individual.qi314)) {
//				mensaje = preguntaVacia.replace("$", "La pregunta QI314");
//				view = txtQI314;
//				error = true;
//				return false;
//			}
			if (Util.esVacio(individual.qi315m)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI315M");
				view = txtQI315M;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi315y)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI315Y");
				view = txtQI315Y;
				error = true;
				return false;
			}

			if (!MyUtil.incluyeRango(1900, App.ANIOPORDEFECTOSUPERIOR, individual.qi315y)) {
				mensaje = "El a�o fuera de rango";
				view = txtQI315Y;
				error = true;
				return false;
			}
			
			if (!MyUtil.incluyeRango(1,12,individual.qi315m) && Util.esMayor(individual.qi315y,App.ANIOPORDEFECTO-1)) {
				mensaje = "La cantidad de meses esta fuera de rango.";
				view = txtQI315M;
				error = true;
				return false;
			}
			if (!MyUtil.incluyeRango(1,12,individual.qi315m) && Util.esMenor(individual.qi315y,App.ANIOPORDEFECTO) && !individual.qi315m.equals("98")) {
				mensaje = "La cantidad de meses esta fuera de rango";
				view = txtQI315M;
				error = true;
				return false;
			}
			
			if (Util.esMenor(individual.qi315y, anio)) {
				mensaje = "El a�o no puede ser menor al ultimo nacimiento del ni�o";
				view = rgQI313;
				error = true;
				return false;
			}
			if (!Util.esDiferente(individual.qi315y,anio)) {
				if (Util.esMenor(Integer.parseInt(individual.qi315m), mes)) {
					mensaje = "El mes no puede ser menor al ultimo nacimiento del ni�o";
					view = rgQI313;
					error = true;
					return false;
				}
			}
		}
		
//		if (int311_a==1){
//			if (Util.esVacio(individual.qi315b)) {
//				mensaje = preguntaVacia.replace("$", "La pregunta QI315B");
//				view = rgQI315B;
//				error = true;
//				return false;
//			}		
//			if(individual.qi315b==4){
//				if (Util.esVacio(individual.qi315b_o)) {
//					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
//					view = txtQI315B_O;
//					error = true;
//					return false;
//				}
//			}			
//		}
		
		if (int311_b!=1 && (int311_c==1||int311_d==1||int311_e==1||int311_f==1||int311_g==1||int311_h==1||int311_i==1||int311_j==1||int311_k==1||int311_l==1||int311_m==1||int311_x==1)) {
			if (Util.esVacio(individual.qi316m)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI316M sss");
				view = txtQI316M;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi316y)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI316Y");
				view = txtQI316Y;
				error = true;
				return false;
			}
			
			
			if(Util.esVacio(individual.qi_fant) && Util.esMenor(individual.qi316y, App.ANIOPORDEFECTO)){
				mensaje = preguntaVacia.replace("$", "La pregunta QI_FANT");
				view = spnQI_FANT;
				error = true;
				return false;
			}
			if(individual.qi_fant!=null && (individual.qi_fant.equals("A") || individual.qi_fant.equals("E") || individual.qi_fant.equals("X")) && Util.esMenor(individual.qi316y, App.ANIOPORDEFECTO)){
				if(Util.esVacio(individual.qi_fant_o)){
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQI_FANT_O;
					error = true;
					return false;
				}
			}
			
			if (!MyUtil.incluyeRango(1900, App.ANIOPORDEFECTOSUPERIOR, individual.qi316y)) {
				mensaje = "El a�o fuera de rango";
				view = txtQI316Y;
				error = true;
				return false;
			}
			
			if (MyUtil.incluyeRango(13,99,individual.qi316m)) {
				mensaje = "La cantidad de meses esta fuera de rango.";
				view = txtQI316M;
				error = true;
				return false;
			}
			
			if (MyUtil.incluyeRango(1,9,individual.qi316m)) {
				mensaje = "Los meses deben considerarse a 2 d�gitos: de 01 a 12 meses";
				view = txtQI316M;
				error = true;
				return false;
			}
			
//			if (!MyUtil.incluyeRango(1,12,individual.qi316m) && Util.esMayor(individual.qi316y,App.ANIOPORDEFECTO-1)) {
//				mensaje = "La cantidad de meses esta fuera de rango.";
//				view = txtQI316M;
//				error = true;
//				return false;
//			}
//			if (!MyUtil.incluyeRango(1,12,individual.qi316m) && Util.esMenor(individual.qi316y,App.ANIOPORDEFECTO) && !individual.qi316m.equals("98")) {
//				mensaje = "La cantidad de meses esta fuera de rango";
//				view = txtQI316M;
//				error = true;
//				return false;
//			}
			
			if (Util.esMenor(individual.qi316y,anio)) {
				mensaje = "El a�o no puede ser menor al ultimo nacimiento del ni�o";
				view = rgQI313;
				error = true;
				return false;
			}
			if (!Util.esDiferente(individual.qi316y,anio)) {
				if (Util.esMenor(Integer.parseInt(individual.qi316m),mes)) {
					mensaje = "El mes no puede ser menor al ultimo nacimiento del ni�o";
					view = rgQI313;
					error = true;
					return false;
				}
			}
			
		}
		
		if (ObtenerCodigodeMetodo().equals("C") || ObtenerCodigodeMetodo().equals("D") || ObtenerCodigodeMetodo().equals("E") ||ObtenerCodigodeMetodo().equals("F") || ObtenerCodigodeMetodo().equals("G") || ObtenerCodigodeMetodo().equals("I") || ObtenerCodigodeMetodo().equals("M") ) {
			if (Util.esVacio(individual.qi317a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI317A");
				view = txtQI317A;
				error = true;
				return false;
			}
		}	
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual);
		inicio();
    }
    private void inicio() {
    	validarPreguntas();
    	RenombrarEtiqueta();
    	onQICOL2ChangeValue();
    	onQI316YChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void RenombrarEtiqueta(){
    	lblpregunta316.text(R.string.ciseccion_03qi316);
    	lblpregunta317A.text(R.string.ciseccion_03qi317a);
    	lblpregunta316.setText(lblpregunta316.getText().toString().replace("#", UsodeMetodoActual()));
    	lblpregunta317A.setText(lblpregunta317A.getText().toString().replace("#", UsodeMetodoActual()));
    	lblfanticoncepcioninf.setText(lblfanticoncepcioninf.getText().toString().replace("m�todo", UsodeMetodoActual()));
    	lblfanticoncepcion.setText(lblfanticoncepcion.getText().toString().replace("m�todo", UsodeMetodoActual()));
    }
    public String UsodeMetodoActual(){
    	if(!Util.esDiferente(individual.qi311_a, 1)){
    		return getString(R.string.ciseccion_03qi311_a).substring(3, getString(R.string.ciseccion_03qi311_a).length());
    	}
    	if(!Util.esDiferente(individual.qi311_b, 1)){
    		return getString(R.string.ciseccion_03qi311_b).substring(3, getString(R.string.ciseccion_03qi311_b).length());
    	}
    	if(!Util.esDiferente(individual.qi311_c, 1)){
    		return getString(R.string.ciseccion_03qi311_c).substring(3, getString(R.string.ciseccion_03qi311_c).length());
    	}
    	if(!Util.esDiferente(individual.qi311_d, 1)){
    		return getString(R.string.ciseccion_03qi311_d).substring(3, getString(R.string.ciseccion_03qi311_d).length());
    	}
    	if(!Util.esDiferente(individual.qi311_e, 1)){
    		return getString(R.string.ciseccion_03qi311_e).substring(3, getString(R.string.ciseccion_03qi311_e).length());
    	}
    	if(!Util.esDiferente(individual.qi311_f, 1)){
    		return getString(R.string.ciseccion_03qi311_f).substring(3, getString(R.string.ciseccion_03qi311_f).length());
    	}
    	if(!Util.esDiferente(individual.qi311_g, 1)){
    		return getString(R.string.ciseccion_03qi311_g).substring(3, getString(R.string.ciseccion_03qi311_g).length());
    	}
    	if(!Util.esDiferente(individual.qi311_h, 1)){
    		return getString(R.string.ciseccion_03qi311_h).substring(3, getString(R.string.ciseccion_03qi311_h).length());
    	}
    	if(!Util.esDiferente(individual.qi311_i, 1)){
    		return getString(R.string.ciseccion_03qi311_i).substring(3, getString(R.string.ciseccion_03qi311_i).length());
    	}
    	if(!Util.esDiferente(individual.qi311_j, 1)){
    		return getString(R.string.ciseccion_03qi311_j).substring(3, getString(R.string.ciseccion_03qi311_j).length());
    	}
    	if(!Util.esDiferente(individual.qi311_k, 1)){
    		return getString(R.string.ciseccion_03qi311_k).substring(3, getString(R.string.ciseccion_03qi311_k).length());
    	}
    	if(!Util.esDiferente(individual.qi311_l, 1)){
    		return getString(R.string.ciseccion_03qi311_l).substring(3, getString(R.string.ciseccion_03qi311_l).length());
    	}
    	if(!Util.esDiferente(individual.qi311_m, 1)){
    		return getString(R.string.ciseccion_03qi311_m).substring(3, getString(R.string.ciseccion_03qi311_m).length());
    	}
    	if(!Util.esDiferente(individual.qi311_x, 1)){
    		return individual.qi311_o;
    	}
    	else{
    		return "";
    	}
    }
    public String ObtenerCodigodeMetodo(){
    	if(!Util.esDiferente(individual.qi311_a, 1)){
    		return "A";
    	}
    	if(!Util.esDiferente(individual.qi311_b, 1)){
    		return "B";
    	}
    	if(!Util.esDiferente(individual.qi311_c, 1)){
    		return "C";
    	}
    	if(!Util.esDiferente(individual.qi311_d, 1)){
    		return "D";
    	}
    	if(!Util.esDiferente(individual.qi311_e, 1)){
    		return "E";
    	}
    	if(!Util.esDiferente(individual.qi311_f, 1)){
    		return "F";
    	}
    	if(!Util.esDiferente(individual.qi311_g, 1)){
    		return "G";
    	}
    	if(!Util.esDiferente(individual.qi311_h, 1)){
    		return "H";
    	}
    	if(!Util.esDiferente(individual.qi311_i, 1)){
    		return "I";
    	}
    	if(!Util.esDiferente(individual.qi311_j, 1)){
    		return "J";
    	}
    	if(!Util.esDiferente(individual.qi311_k, 1)){
    		return "K";
    	}
    	if(!Util.esDiferente(individual.qi311_l, 1)){
    		return "L";
    	}
    	if(!Util.esDiferente(individual.qi311_m, 1)){
    		return "M";
    	}
    	if(!Util.esDiferente(individual.qi311_x, 1)){
    		return "X";
    	}
    	else{
    		return "";
    	}
    }
    
    public void validarPreguntas(){
    	Integer int311_a = individual.qi311_a==null?0:individual.qi311_a;
    	Integer int311_b = individual.qi311_b==null?0:individual.qi311_b;
    	Integer int310 = individual.qi310==null?0:individual.qi310;
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	Integer int226 = individual.qi226==null?0:individual.qi226;
    	
    	if (int304==2 || (int310==2 || int226==1) ) {
    		Log.e("caso 0","int304==2 || (int310==2 || int226==1 ");
    		Util.cleanAndLockView(getActivity(),rgQI313/*,txtQI314*/,txtQI315M,txtQI315Y/*,rgQI315B*/,txtQI316M,txtQI316Y,txtQI317A);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
//			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
//			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		}
    	else{
    		RenombrarLabels(int311_a,int311_b);
    		q0.setVisibility(View.VISIBLE);
    		
    		if (int311_a==1  || int311_b==1 ) { 
    			if (int311_a==1 ) {   		
    				Log.e("caso 1","int311_a==1 ");
        			Util.lockView(getActivity(),false,rgQI313/*,txtQI314*/,txtQI315M,txtQI315Y/*,rgQI315B*/);
        			Util.cleanAndLockView(getActivity(),txtQI316M,txtQI316Y,txtQI317A);        		
            		q1.setVisibility(View.VISIBLE);
//        			q2.setVisibility(View.VISIBLE);
        			q3.setVisibility(View.VISIBLE);
//        			q4.setVisibility(View.VISIBLE);
        			q5.setVisibility(View.GONE);
        			q6.setVisibility(View.GONE);
    			}
        		if (int311_b==1 ) {  
    				Log.e("caso 2 ","int311_a==2 ");
        			Util.lockView(getActivity(),false,rgQI313/*,txtQI314*/,txtQI315M,txtQI315Y);
        			Util.cleanAndLockView(getActivity()/*,rgQI315B*/,txtQI316M,txtQI316Y,txtQI317A);        		
            		q1.setVisibility(View.VISIBLE);
//        			q2.setVisibility(View.VISIBLE);
        			q3.setVisibility(View.VISIBLE);
//        			q4.setVisibility(View.GONE);
        			q5.setVisibility(View.GONE);
        			q6.setVisibility(View.GONE);
        		} 
    		}
    		else{
    			if (ObtenerCodigodeMetodo().equals("C") || ObtenerCodigodeMetodo().equals("D") || ObtenerCodigodeMetodo().equals("E") ||ObtenerCodigodeMetodo().equals("F") || ObtenerCodigodeMetodo().equals("G") || ObtenerCodigodeMetodo().equals("I") || ObtenerCodigodeMetodo().equals("M") ) {
    				Log.e("caso 3","int311_c==1||int311_d==1||int311_e==1||int311_f==1||int311_g==1||int311_i==1||int311_m==1 ");
    				Util.cleanAndLockView(getActivity(),rgQI313/*,txtQI314*/,txtQI315M,txtQI315Y/*,rgQI315B*/);  
    				Util.lockView(getActivity(),false,txtQI316M,txtQI316Y,txtQI317A);        			      		
            		q1.setVisibility(View.GONE);
//        			q2.setVisibility(View.GONE);
        			q3.setVisibility(View.GONE);
//        			q4.setVisibility(View.GONE);
        			q5.setVisibility(View.VISIBLE);
        			q6.setVisibility(View.VISIBLE);
				}
    			else{
    				Log.e("caso 4","otros casos ");
    				Util.cleanAndLockView(getActivity(),rgQI313/*,txtQI314*/,txtQI315M,txtQI315Y/*,rgQI315B*/,txtQI317A);  
    				Util.lockView(getActivity(),false,txtQI316M,txtQI316Y);        			      		
            		q1.setVisibility(View.GONE);
//        			q2.setVisibility(View.GONE);
        			q3.setVisibility(View.GONE);
//        			q4.setVisibility(View.GONE);
        			q5.setVisibility(View.VISIBLE);
        			q6.setVisibility(View.GONE);
    			}
    		}
    	}
//    	if (individual.qi314!=null) {
//    		if (individual.qi314==9995) {
//        		chbP314_1.setChecked(true);
//        		chbP314_2.setChecked(false);
//        		Util.cleanAndLockView(getActivity(),txtQI314);
//        		txtQI314.setVisibility(View.GONE);
//    		}else if (individual.qi314==9998) {
//    			chbP314_1.setChecked(false);
//    			chbP314_2.setChecked(true);
//        		Util.cleanAndLockView(getActivity(),txtQI314);
//        		txtQI314.setVisibility(View.GONE);
//    		}
//		}
    	
		
		
    	if (individual.qi317a!=null) {
    		BigDecimal data= new BigDecimal(995);
    		BigDecimal data1= new BigDecimal(998);
    		if (individual.qi317a.equals(data)) {
        		chbP317A_1.setChecked(true);
        		chbP317A_2.setChecked(false);
        		Util.cleanAndLockView(getActivity(),txtQI317A);
        		txtQI317A.setVisibility(View.GONE);
    		}
    		else if (individual.qi317a.equals(data1)) {
    			chbP317A_1.setChecked(false);
    			chbP317A_2.setChecked(true);
        		Util.cleanAndLockView(getActivity(),txtQI317A);
        		txtQI317A.setVisibility(View.GONE);
    		}
		}
    }
    
    
    
    public void RenombrarLabels(Integer in311_a,Integer in311_b){
    	if (in311_a==1) {
    		lblpregunta313_Ind.setText(texto313Hombre);
		}
    	if (in311_b==1) {
    		lblpregunta313_Ind.setText(texto313Mujer);
		}
	}
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI313.readOnly();
//    		rgQI315B.readOnly();
//    		txtQI314.readOnly();
//    		txtQI315B_O.readOnly();
    		txtQI315M.readOnly();
    		txtQI315Y.readOnly();
    		txtQI316M.readOnly();
    		txtQI316Y.readOnly();
    		txtQI317A.readOnly();
//    		chbP314_1.readOnly();
//    		chbP314_2.readOnly();
    		chbP317A_1.readOnly();
    		chbP317A_2.readOnly();
    	}
    }
    public void LlenarSpinerColumna03(){
   	 List<Object> keys = new ArrayList<Object>();
   	 keys.add(null);
   	 keys.add("1");
   	 keys.add("2");
   	 keys.add("3");
   	 keys.add("4");
   	 keys.add("5");
   	 keys.add("6");
   	 keys.add("7");
   	 keys.add("8");
   	 keys.add("A");
   	 keys.add("B");
   	 keys.add("C");
   	 keys.add("D");
   	 keys.add("E");
   	 keys.add("F");
   	 keys.add("G");
   	 keys.add("H");
   	 keys.add("I");
   	 keys.add("J");
   	 keys.add("K");
   	 keys.add("X");
   	 String[] items = new String[]{" ---Seleccione --- ",
   			 "1 HOSPITAL MINSA",
   			 "2 CENTRO DE SALUD MINSA",
   			 "3 PUESTO DE SALUD MINSA",
   			 "4 PROMOTOR DE SALUD MINSA",
   			 "5 HOSPITAL DE ESSALUD",
   			 "6 POLICLINICO/CENTRO/POSTA ESSALUD",
   			 "7 HOSPITAL/OTRO DE LAS FFAA Y PNP",
   			 "8 HOSPITAL /OTRO DE LA MUNICIPALIDAD",
   			 "A OTRO GOBIERNO",
   			 "B CL�NICA PARTICULAR",
   			 "C FARMACIA / BOTICA",
   			 "D CONSULTORIO M�DICO PARTICULAR",
   			 "E OTRO SECTOR PRIVADO",
   			 "F CL�NICA/POSTA DE ONG",
   			 "G PROMOTORES ONG",
   			 "H HOSPITAL/OTRO DE LA IGLESIA",
   			 "I TIENDA / SUPERMERCADO /HOSTAL",
   			 "J AMIGOS / PARIENTES",
   			 "K NADIE/SE AUTORECETO",
   			 "X OTRO"
   	 };
   	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items);
   	 spnQI_FANT.setAdapterWithKey(adapter, keys);
   }
    public void onQICOL2ChangeValue(){
    	String codigo=spnQI_FANT.getSelectedItemKey()!=null?spnQI_FANT.getSelectedItemKey().toString():"";
    	if(!codigo.equals("A") && !codigo.equals("E") && !codigo.equals("X")){
    		Util.cleanAndLockView(getActivity(), txtQI_FANT_O);
    		txtQI_FANT_O.setVisibility(View.GONE);
    	}else{
    		Util.lockView(getActivity(), false,txtQI_FANT_O);
    		txtQI_FANT_O.setVisibility(View.VISIBLE);
    	}
    }
    public void onQI316YChangeValue(){
    	if(txtQI316Y.getText().toString().trim().length()>0 && Integer.parseInt(txtQI316Y.getText().toString())<App.ANIOPORDEFECTO){
    		Util.lockView(getActivity(), false,spnQI_FANT,txtQI_FANT_O);
    		q7.setVisibility(View.VISIBLE);
//    		Log.e("","CODIGO: "+ObtenerCodigodeMetodo());
    		if(ObtenerCodigodeMetodo().equals("J") || ObtenerCodigodeMetodo().equals("K") || ObtenerCodigodeMetodo().equals("L")){
    			MyUtil.LiberarMemoria();
    			lblfanticoncepcion.setVisibility(View.GONE);
    			lblfanticoncepcioninf.setVisibility(View.VISIBLE);
    		}
    		else{
    			MyUtil.LiberarMemoria();
    			lblfanticoncepcion.setVisibility(View.VISIBLE);
    			lblfanticoncepcioninf.setVisibility(View.GONE);
    		}
    	}
    	else{
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), spnQI_FANT,txtQI_FANT_O);
    		q7.setVisibility(View.GONE);
    	}
    	txtQI317A.requestFocus();
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
//		if (chbP314_1.isChecked())
//			individual.qi314=9995;
//		if (chbP314_2.isChecked())
//			individual.qi314=9998;
		
		if (chbP317A_1.isChecked()){
			BigDecimal data= new BigDecimal(995);
			individual.qi317a=data;
		}
		if (chbP317A_2.isChecked()){
			BigDecimal data1= new BigDecimal(998);
			individual.qi317a=data1;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
//		App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
//		App.getInstance().getPersonaCuestionarioIndividual().qi316y=individual.qi316y;
		return App.INDIVIDUAL;
	}
}