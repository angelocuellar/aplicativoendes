package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_008 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI236;
	@FieldAnnotation(orderIndex=2)
	public TextAreaField txtQI236_O;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI237;
	@FieldAnnotation(orderIndex=4)
	public TextAreaField txtQI238_TEX;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI238;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI239A;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI239B;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI239C;
	@FieldAnnotation(orderIndex=9)
	public CheckBoxField chbQI239E;
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQI239F;
	@FieldAnnotation(orderIndex=11)
	public CheckBoxField chbQI239X;
	@FieldAnnotation(orderIndex=12)
	public TextField txtQI239O;
	@FieldAnnotation(orderIndex=13)
	public TextAreaField txtQIOBS_S2;
//	@FieldAnnotation(orderIndex=14)
//	public IntegerField txtQI236_1;
	

	public TextField txtCabecera;
	CISECCION_01_03 individual;

	public GridComponent2 gdEmbarazo;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta236,lblpregunta237,lblpregunta238,lblpregunta239,lblobs_s1,lblpregunta236_o;
	public IntegerField txtQI236_1,txtQI236_2,txtQI236_3,txtQI236_4;
	
	LinearLayout q0,q1,q2,q3,q4,q5;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_02Fragment_008() {}
	public CISECCION_02Fragment_008 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI236","QI236_A","QI236_O","QI237","QI238","QI238_TEX","QI239A","QI239B","QI239C","QI239E","QI239F","QI239X","QI239O","QIOBS_S2","QI230","QI231_Y","QI234","QI235A","QI226","QI106","QI201","QI206","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI236","QI236_A","QI236_O","QI237","QI238","QI238_TEX","QI239A","QI239B","QI239C","QI239E","QI239F","QI239X","QI239O","QIOBS_S2")};
		return rootView;
  }
  
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta236 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi236);
		lblpregunta237 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi237);

		txtQI236_1=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
		txtQI236_2=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI236_3=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
		txtQI236_4=new IntegerField(this.getActivity()).size(altoComponente, 83).maxLength(2);
	
		rgQI236=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi236_1,R.string.ciseccion_02qi236_2,R.string.ciseccion_02qi236_3,R.string.ciseccion_02qi236_4,R.string.ciseccion_02qi236_5,R.string.ciseccion_02qi236_6,R.string.ciseccion_02qi236_7,R.string.ciseccion_02qi236_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQI236ChangeValue");
		rgQI236.agregarEspecifique(0,txtQI236_1);
		rgQI236.agregarEspecifique(1,txtQI236_2);
		rgQI236.agregarEspecifique(2,txtQI236_3);
		rgQI236.agregarEspecifique(3,txtQI236_4);
		txtQI236_O = new TextAreaField(getActivity()).size(90, 750).alfanumerico().maxLength(500);
		lblpregunta236_o =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi236_o);
		rgQI237=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi237_1,R.string.ciseccion_02qi237_2,R.string.ciseccion_02qi237_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI237ChangeValue");
		
		lblpregunta238 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi238);
		lblpregunta239 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi239);
		rgQI238=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi238_1,R.string.ciseccion_02qi238_2,R.string.ciseccion_02qi238_3,R.string.ciseccion_02qi238_4,R.string.ciseccion_02qi238_5,R.string.ciseccion_02qi238_6,R.string.ciseccion_02qi238_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI238_TEX = new TextAreaField(getActivity()).maxLength(500).size(100, 750).alfanumerico();
		chbQI239A=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI239B=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI239C=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI239E=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI239F=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI239X=new CheckBoxField(this.getActivity(), R.string.ciseccion_02qi239x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQI239XChangeValue");
		txtQI239O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		lblobs_s1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.obs_s1);
		txtQIOBS_S2 = new TextAreaField(getActivity()).maxLength(1000).size(200, 750).alfanumerico();
		
		
    }
  
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta236,rgQI236,lblpregunta236_o,txtQI236_O);
		q2 = createQuestionSection(lblpregunta237,rgQI237);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta238,txtQI238_TEX,rgQI238);
		LinearLayout ly239 = new LinearLayout(getActivity());
		ly239.addView(chbQI239X);
		ly239.addView(txtQI239O);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta239,chbQI239A,chbQI239C,chbQI239F,ly239);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblobs_s1,txtQIOBS_S2);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		return contenedor;
    }
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		
		if (individual.qi236!=null) {
			if (!Util.esDiferente(individual.qi236,1) && txtQI236_1.getText().toString().trim().length()!=0 ) {
				individual.qi236_a=Integer.parseInt(txtQI236_1.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,2) && txtQI236_2.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_2.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,3) && txtQI236_3.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_3.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,4) && txtQI236_4.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_4.getText().toString().trim());
			}
			else {
				individual.qi236_a=null;
			}
		}
		
//		if (individual.qi236!=null) {
//			individual.qi236=individual.getConvertQi236(individual.qi236);
//		}
		if (individual.qi237!=null) {
			individual.qi237=individual.getConvertQi237(individual.qi237);
		}
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
    }
    
    private boolean validar() {
			if(!isInRange()) return false;
			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
	    	Integer int236=individual.qi236==null?0:individual.qi236;
  
			if (Util.esVacio(individual.qi236)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI236");
				view = rgQI236;
				error = true;
				return false;
			}
			if (int236==1 || int236==2 || int236==3 || int236==4) {
				if (Util.esVacio(individual.qi236_a)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI236_A");
					view = txtQI236_2;
					error = true;
					return false;
				}
				if(!Util.esDiferente(int236,1) && (Util.esMenor(individual.qi236_a, 0)|| Util.esMayor(individual.qi236_a , 29))){
					mensaje = "valor fuera de rango solo permite de 1 a 29";
					view = txtQI236_1;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(individual.qi237)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI237");
				view = rgQI237;
				error = true;
				return false;
			}
			if (individual.qi237!=2 && individual.qi237!=8) {
				if (Util.esVacio(individual.qi238)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI238");
					view = rgQI238;
					error = true;
					return false;
				}
				if (Util.esDiferente(individual.qi238,7)) {
					if (Util.esVacio(individual.qi238_tex)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI238_TEX");
						view = txtQI238_TEX;
						error = true;
						return false;
					}
				}
			}
			if (!verificarCheck239()) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI239A");
				view = chbQI239A;
				error = true;
				return false;
			}
			if (chbQI239X.isChecked()) {
				if (Util.esVacio(individual.qi239o)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI239O");
					view = txtQI239O;
					error = true;
					return false;
				}
			}
			
			if(App.getInstance().getPersonaCuestionarioIndividual()!=null 
				    && App.getInstance().getPersonaCuestionarioIndividual().total_ninios==0 
				    && !Util.esDiferente(individual.qi226,2)
				    && !Util.esDiferente(individual.qi236, 995)){
				mensaje = "debe cambiar de opcion no tiene ni�os y no esta embarazada";
				view = rgQI236;
				error = true;
				return false;
			}
		    return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
//		if(individual.qi236!=null)	{
//			individual.qi236=individual.setConvertQi236(individual.qi236);
//		}
		if(individual.qi237!=null)	{
			individual.qi237=individual.setConvertQi237(individual.qi237);
		}
		entityToUI(individual);
		
		if (individual.qi236_a!=null) {	
    		if (!Util.esDiferente(individual.qi236,1)) {
    			txtQI236_1.setText(individual.qi236_a.toString());
    		}
    		if (!Util.esDiferente(individual.qi236,2)) {
    			txtQI236_2.setText(individual.qi236_a.toString());
    		}  
    		if (!Util.esDiferente(individual.qi236,3)) {
    			txtQI236_3.setText(individual.qi236_a.toString());
    		}  
    		if (!Util.esDiferente(individual.qi236,4)) {
    			txtQI236_4.setText(individual.qi236_a.toString());
    		}  
    	}
		
		inicio();
       	
    }
    
    private void ValidarPregunta226() {
    	Integer in226 = individual.qi226==null?0:individual.qi226;
    	if (in226==1) {
    		rgQI236.lockButtons(true,4);
		}
    	else{
    		rgQI236.lockButtons(false,4);
    	}
    }

    private void inicio() {
    	onrgQI237ChangeValue();
    	onchbQI239XChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    
    public void onrgQI236ChangeValue() {
    	Integer radio=Integer.parseInt(rgQI236.getTagSelected("0").toString());
	    if(App.getInstance().getPersonaCuestionarioIndividual()!=null 
	    && App.getInstance().getPersonaCuestionarioIndividual().total_ninios==0 
	    && !Util.esDiferente(individual.qi226,2)
	    && !Util.esDiferente(radio, 6)){
	    	ToastMessage.msgBox(this.getActivity(), "no puede marcar esta opcion porque no tiene ni�os y no est� embaraza", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
	    }
    }
    
	  public void onrgQI237ChangeValue() {
		  if (MyUtil.incluyeRango(2,8,rgQI237.getTagSelected("").toString())) {
			  Util.cleanAndLockView(getActivity(),txtQI238_TEX,rgQI238);
			  MyUtil.LiberarMemoria();
			  q3.setVisibility(View.GONE);
			  chbQI239A.requestFocus();
		  } else {
			  Util.lockView(getActivity(), false,txtQI238_TEX,rgQI238);
			  q3.setVisibility(View.VISIBLE);
			  txtQI238_TEX.requestFocus();
		  }
	  }
    
    
    
    public void onchbQI239XChangeValue() {
    	if (chbQI239X.isChecked()){
  			Util.lockView(getActivity(),false,txtQI239O);
  			txtQI239O.requestFocus();
  		}
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI239O);
  		}
    }
    
    public boolean verificarCheck239() {
    	  if (chbQI239A.isChecked() || chbQI239B.isChecked() || chbQI239C.isChecked() || chbQI239E.isChecked() || chbQI239F.isChecked() || chbQI239X.isChecked()) {
    		  return true;
    	  }else{
    		  return false;
    	  }
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		txtQI236_1.readOnly();
    		txtQI236_2.readOnly();
    		txtQI236_3.readOnly();
    		txtQI236_4.readOnly();
    		txtQI238_TEX.setEnabled(false);
    		txtQI239O.readOnly();
    		txtQIOBS_S2.setEnabled(false);
    		rgQI236.readOnly();
    		txtQI236_O.setEnabled(false);
    		rgQI237.readOnly();
    		rgQI238.readOnly();
    		chbQI239A.readOnly();
    		chbQI239B.readOnly();
    		chbQI239C.readOnly();
    		chbQI239E.readOnly();
    		chbQI239F.readOnly();
    		chbQI239X.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
 		if(cuestionarioService==null){
 			cuestionarioService = CuestionarioService.getInstance(getActivity());
 		}
 		return cuestionarioService;
     }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		
		if (individual.qi236!=null) {
			if (!Util.esDiferente(individual.qi236,1) && txtQI236_1.getText().toString().trim().length()!=0 ) {
				individual.qi236_a=Integer.parseInt(txtQI236_1.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,2) && txtQI236_2.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_2.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,3) && txtQI236_3.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_3.getText().toString().trim());
			}
			else if (!Util.esDiferente(individual.qi236,4) && txtQI236_4.getText().toString().trim().length()!=0) {
				individual.qi236_a=Integer.parseInt(txtQI236_4.getText().toString().trim());
			}
			else {
				individual.qi236_a=null;
			}
		}
		
//		if (individual.qi236!=null) {
//			individual.qi236=individual.getConvertQi236(individual.qi236);
//		}
		if (individual.qi237!=null) {
			individual.qi237=individual.getConvertQi237(individual.qi237);
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.INDIVIDUAL;
	}
}