package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CI_VisitaDialog;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CI_VisitaDialog.ACTION;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_01Fragment_000  extends FragmentForm implements Respondible {
	private TableComponent tcVisita;
	@FieldAnnotation(orderIndex = 1)
	public ButtonComponent btnIniciar;
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnFinalizar;
	@FieldAnnotation(orderIndex = 3)
	public TextField txtQSRESULT_O;
	//HOLAaafff
	
	CISECCION_01_03 individual;
	public TextField txtOrden;
	private List<CARATULA_INDIVIDUAL> visitas;
	public TextField txtnombre;
	public IntegerField txtnumero;
	public TextField txtQHVIOLEN;
	public IntegerField txtQITOTALN;
	public IntegerField txtQITOTALC;
	public GridComponent2 gridNombre,gridResultado,gridResultadovisita;
	public CARATULA_INDIVIDUAL ultimaVisita, menorVisita,bean;
	Hogar hogar;
	SeccionCapitulo[] seccionesCargadoHogar;
	private GridComponent grid;
	private GridComponent2 resultadoDetallado;
	private LabelComponent lblResultado, lblfecha, lblFechaFinal, lblResultadoFinal;
	CARATULA_INDIVIDUAL visita;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblResultadoFinalDetallado2,IDinformante,Nombreinformante,lblPresentacion,lblPresentacion_descrip,lblnombreMef,lblTitulo,lblNombre,lblViolencia,lblNinio5,lblNinioCarne,dummy,lblSubTituloresultado;
	
	LinearLayout q0,q1,q2,q3,q4;
	
	DialogComponent dialog;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoCIS01_03;
	VisitaService visitaService;
	
	private Seccion01Service personaService;
	private UsuarioService usuarioservice;
	public TableComponent tcNinios;
	SeccionCapitulo[] seccionesCargadoInd;
	SeccionCapitulo[] seccionesCargadoSeccion01;
	
	public String[] items1={};
	Integer result=0;

	public CISECCION_01Fragment_000() {
	}

	public CISECCION_01Fragment_000 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);

		enlazarCajas();
		listening();		
		
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","ID","HOGAR_ID")};		
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0, -1,-1, "QIVDIA","QIVMES","QIVANIO","QIVHORA","QIVMIN","QIVHORA_FIN","QIVMIN_FIN","QIVENTREV","QIVRESUL","QIVRESUL_O","QIPROX_DIA","QIPROX_MES","QIPROX_ANIO","QIPROX_HORA","QIPROX_MINUTO","ID", "HOGAR_ID", "PERSONA_ID", "NRO_VISITA") };
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QITOTALN","QITOTALC")};
		seccionesCargadoCIS01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QITOTALN","QITOTALC")};
		seccionesCargadoInd= new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QIVRESUL")};
		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "QHVIOLEN") };

		return rootView;
	}

	@Override
	protected void buildFields() {
		
		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.visita).textSize(21).centrar().negrita();
		
		lblNombre = new LabelComponent(this.getActivity()).size(altoComponente+40, 500).text(R.string.ciseccion_00nomb).textSize(15).centrar();
		lblnombreMef = new LabelComponent(this.getActivity()).size(altoComponente, 400).textSize(15);
		txtnumero = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).readOnly().alinearIzquierda();
		lblViolencia = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_00vio).textSize(15);
		txtQHVIOLEN = new TextField(this.getActivity()).size(altoComponente, 100).readOnly();		
		
		txtQITOTALN = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).readOnly().centrar();
		txtQITOTALC = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).readOnly().centrar();
		lblNinio5 = new LabelComponent(this.getActivity()).size(altoComponente, 300).text(R.string.ciseccion_00ninio5).textSize(16).alinearIzquierda();
		lblNinioCarne = new LabelComponent(this.getActivity()).size(altoComponente, 300).text(R.string.ciseccion_00niniocrane).textSize(16).alinearIzquierda();
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcVisita = new TableComponent(getActivity(), this, App.ESTILO).size(250, 770).dataColumHeight(60).headerHeight(60).headerTextSize(17);
//		}
//		else{
			tcVisita = new TableComponent(getActivity(), this, App.ESTILO).size(350, 770).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcVisita.addHeader(R.string.v_nro, 0.4f);
		tcVisita.addHeader(R.string.v_fecha, 0.6f);
		tcVisita.addHeader(R.string.v_hora_ini, 0.8f);
		tcVisita.addHeader(R.string.v_hora_fin, 0.8f);
		tcVisita.addHeader(R.string.v_fecha_prox, 0.8f);
		tcVisita.addHeader(R.string.v_hora_prox, 0.8f);
		tcVisita.addHeader(R.string.v_resultado, 1.0f);
		
		gridNombre=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridNombre.addComponent(lblnombreMef);
		gridNombre.addComponent(txtnumero.centrar());
		gridNombre.addComponent(lblViolencia);
		gridNombre.addComponent(txtQHVIOLEN.centrar());
		
		gridResultado=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridResultado.addComponent(lblNinio5);
		gridResultado.addComponent(txtQITOTALN);		
		gridResultado.addComponent(lblNinioCarne);
		gridResultado.addComponent(txtQITOTALC);
		
		btnIniciar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(150, 55);
		
		btnIniciar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (visitas.size() > 0) {					
					
					if (!Util.esDiferente(ultimaVisita.qivresul, 1)) {
						ToastMessage.msgBox(getActivity(),"Visita Completa",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}						
					
					if (!Util.esDiferente(
							visitas.get(visitas.size() - 1).qivresul, 1)) {
						ToastMessage.msgBox(getActivity(),"Cuestionarios finalizados",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					if (visitas.get(visitas.size() - 1).qivresul == null) {
						ToastMessage.msgBox(getActivity(),"Finalice la visita anterior",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					
					if (Util.esDiferente(ultimaVisita.qivresul, 1)) {
						crearVisita();
					}					
				}
				
				
				Calendar fechaactual = new GregorianCalendar();
				bean = new CARATULA_INDIVIDUAL();
				bean.id = App.getInstance().getPersonaSeccion01().id;
				bean.hogar_id = App.getInstance().getPersonaSeccion01().hogar_id ;
				bean.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
				bean.nro_visita = visitas.size() + 1;
				bean.qivhora = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
				bean.qivmin = fechaactual.get(Calendar.MINUTE)+"";
				bean.qivdia = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
				bean.qivmes = (fechaactual.get(Calendar.MONTH)+1)+"";
				bean.qivanio= fechaactual.get(Calendar.YEAR);
				
				bean.qivhora=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivhora);
				bean.qivmin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivmin);
				bean.qivdia=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivdia);
				bean.qivmes=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qivmes);
				
				if( bean.nro_visita > getCuestionarioService().getUltima_visitaVivienda(visita.id).nro_visita){
					ToastMessage.msgBox(getActivity(),"Debe iniciar visita en la vivienda",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return;
				}
				boolean flag=grabarVisita();
				if(flag){
					recargarLista();
				}
			}
		});
		
		btnFinalizar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.v_l_finalizar).size(150, 55);
		btnFinalizar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CARATULA_INDIVIDUAL bean = (CARATULA_INDIVIDUAL) tcVisita.getSelectedItem();
				if (bean == null) {
					ToastMessage.msgBox(getActivity(),"Seleccione una visita primero.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return;
				}
				 if (bean.qivresul != null) {
					 ToastMessage.msgBox(getActivity(),"VISITA ya fue finalizada.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					 return;
				 }
				abrirDialogo(bean, CI_VisitaDialog.ACTION.FINALIZAR);
			}
		});
		
		txtQSRESULT_O = new TextField(getActivity()).size(altoComponente, MATCH_PARENT).maxLength(100).hint(R.string.especifique).soloTexto();
		
		dummy = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		
		grid = new GridComponent(getActivity(), Gravity.CENTER, 3, 0);
		grid.addComponent(btnIniciar);
		grid.addComponent(dummy);
		grid.addComponent(btnFinalizar);

		lblfecha = new LabelComponent(this.getActivity()).size(altoComponente, 170).text(R.string.v_fecha).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblResultado = new LabelComponent(this.getActivity()).text(R.string.v_resultado).size(altoComponente+10, 170).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblFechaFinal = new LabelComponent(this.getActivity()).size(altoComponente, 250).textSize(18).centrar().negrita();
		lblResultadoFinal = new LabelComponent(this.getActivity()).size(altoComponente, 350).textSize(18).centrar().negrita();
	    lblSubTituloresultado = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vresultcabecera).textSize(21).centrar().negrita();
		gridResultadovisita = new GridComponent2(this.getActivity(), 2);
		gridResultadovisita.addComponent(lblfecha);
		gridResultadovisita.addComponent(lblFechaFinal);
		gridResultadovisita.addComponent(lblResultado);
		gridResultadovisita.addComponent(lblResultadoFinal);
		
		tcNinios = new TableComponent(getActivity(), this,App.ESTILO).size(400, 770).headerHeight(altoComponente + 10).dataColumHeight(50);
		
		tcNinios.addHeader(R.string.visitaresul_nombrenino, 1f,TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.visitaresul_nro, 0.3f,TableComponent.ALIGN.CENTER);
		tcNinios.addHeader(R.string.visitaresul_resul,0.9f, TableComponent.ALIGN.CENTER);
		

		lblResultadoFinalDetallado2 = new LabelComponent(this.getActivity()).size(60, MATCH_PARENT).text(R.string.visitaresul_detallado2).textSize(17).centrar().negrita();
		Nombreinformante = new LabelComponent(this.getActivity()).size(60, 400).textSize(18).centrar();
		IDinformante = new LabelComponent(this.getActivity()).size(60, 30).textSize(18);
		
		if(App.getInstance().getPersonaSeccion01()!=null){
			if(App.getInstance().getPersonaSeccion01().qh02_2==null){
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" ");
			}else{
				Nombreinformante.setText(""+App.getInstance().getPersonaSeccion01().qh02_1+" "+App.getInstance().getPersonaSeccion01().qh02_2);
			}
			
			IDinformante.setText(""+App.getInstance().getPersonaSeccion01().persona_id);
		}
		
		
		resultadoDetallado = new GridComponent2(this.getActivity(), 3);
		resultadoDetallado.addComponent(lblResultadoFinalDetallado2,3,1);
		resultadoDetallado.addComponent(Nombreinformante);
		resultadoDetallado.addComponent(IDinformante);
		resultadoDetallado.addComponent(txtQSRESULT_O, 3,1);

		Spanned texto2=Html.fromHtml("<div style=\"text-align:justify\">"+
"Se�ora (Se�orita), mi nombre es F1 y estoy trabajando para el Instituto Nacional de Estad�stica e Inform�tica, instituci�n que por especial encargo del Ministerio de Salud est� realizando un estudio sobre la salud de las mujeres, las ni�as y los ni�os menores de seis a�os, a nivel nacional y en cada uno de los departamentos del pa�s, con el objeto de evaluar y orientar la futura implementaci�n de los programas de salud materno infantil, orientados a elevar las condiciones de salud de la poblaci�n en el pa�s. <br>Con tal motivo, me gustar�a hacerle algunas preguntas sobre su salud y la salud de sus hijas e hijos. La informaci�n que nos brinde es estrictamente confidencial y permanecer� en absoluta reserva. <br>En este momento, �Usted desea preguntarme algo acerca de esta investigaci�n o estudio? �Puedo iniciar la entrevista ahora?"+"</div>");
		
		lblPresentacion = new LabelComponent(this.getActivity()).size(60, 550).text(R.string.ciseccion_00texto1).textSize(18).negrita().centrar();
		lblPresentacion_descrip = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17);lblPresentacion_descrip.setText(texto2);
		
//		if(App.getInstance().getUsuario().nombres!=null){
//			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1",App.getInstance().getUsuario().nombres+" "+obtenerPrimerApellido(App.getInstance().getUsuario().apellidos)));
//		}else{
//			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1","..."));
// 
//		}
		if(App.getInstance().getMarco().usu_id!=null){
			Usuario usuario = getUsuarioService().getUsuarioById(App.getInstance().getMarco().usu_id);
			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1",usuario.nombres+" "+obtenerPrimerApellido(usuario.apellidos)));
		}else{
			lblPresentacion_descrip.setText(lblPresentacion_descrip.getText().toString().replace("F1","..."));
		}

	}
	public void crearVisita(){

		Calendar fechaactual = new GregorianCalendar();
		ultimaVisita = new CARATULA_INDIVIDUAL();
		ultimaVisita.id = App.getInstance().getPersonaSeccion01().id;
		ultimaVisita.hogar_id = App.getInstance().getPersonaSeccion01().hogar_id ;
		ultimaVisita.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		ultimaVisita.nro_visita = visitas.size() + 1;
		ultimaVisita.qivhora = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
		ultimaVisita.qivmin = fechaactual.get(Calendar.MINUTE)+"";
		ultimaVisita.qivdia = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
		ultimaVisita.qivmes = (fechaactual.get(Calendar.MONTH)+1)+"";
		ultimaVisita.qivanio= fechaactual.get(Calendar.YEAR);
		
		ultimaVisita.qivhora=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(ultimaVisita.qivhora);
		ultimaVisita.qivmin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(ultimaVisita.qivmin);
		ultimaVisita.qivdia=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(ultimaVisita.qivdia);
		ultimaVisita.qivmes=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(ultimaVisita.qivmes);
	}
	
	public String obtenerPrimerApellido(String apellidos){

		Integer pos=0;
		if(apellidos!=null){
			for(int i=0;i<apellidos.length();i++){
				if(apellidos.charAt(i)==' '){
					pos=i;
					break;
				}
			}
			return apellidos.substring(0, pos);
		}else{
			return "";
		}	
	}
    
	private void abrirDialogo(CARATULA_INDIVIDUAL bean, CI_VisitaDialog.ACTION action) {
		FragmentManager fm = this.getFragmentManager();
		CI_VisitaDialog aperturaDialog = CI_VisitaDialog.newInstance(this, bean, action);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
		recargarLista();
	}

	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(lblPresentacion,lblPresentacion_descrip,lblNombre,gridNombre.component());
		q2 = createQuestionSection(tcVisita.getTableView(), grid.component());
		tcVisita.setSelectorData(R.drawable.selector_sombra_lista);
		q3 = createQuestionSection(lblSubTituloresultado,gridResultadovisita.component());
		q4 = createQuestionSection(gridResultado.component());
		
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		
		return contenedor;
	}

	@Override
	public boolean grabar() {
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
    			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
    				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados!!!.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    			}
    		}catch (SQLException e) {
    			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    			
    		}
		return true;
	}

	private boolean validar() {
		if(!isInRange()) return false;
		return true;
	}

	public boolean grabarVisita() {
		if(bean!=null){
			try {
				if(!getVisitaService().saveOrUpdate(bean,"QIVDIA","QIVMES","QIVANIO","QIVHORA","QIVMIN","NRO_VISITA")){
					 ToastMessage.msgBox(getActivity(), "Los datos no fueron guardados.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	         			}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return true;
	}
	
	public void individual(){
	}
	
	@Override
	public void cargarDatos() {
		App.getInstance().setVisitaI(null);
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoCIS01_03);
			visita = getCuestionarioService().getCAPIVISITA(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
			
			if (visita == null) {
				visita = new CARATULA_INDIVIDUAL();
				visita.id = App.getInstance().getPersonaCuestionarioIndividual().id;
				visita.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
				visita.persona_id =App.getInstance().getPersonaCuestionarioIndividual().persona_id;
				visitas = new ArrayList<CARATULA_INDIVIDUAL>();
			}
			if(individual==null){
				individual = new CISECCION_01_03();
				individual.id =App.getInstance().getPersonaCuestionarioIndividual().id;
				individual.hogar_id =App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
				individual.persona_id =App.getInstance().getPersonaCuestionarioIndividual().persona_id;
				individual.qitotaln = 0;
				individual.qitotalc =0;
			}

			if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
				if(App.getInstance().getPersonaCuestionarioIndividual().qh02_2==null){
					Nombreinformante.setText(""+App.getInstance().getPersonaCuestionarioIndividual().qh02_1+" ");
				}else{
					Nombreinformante.setText(""+App.getInstance().getPersonaCuestionarioIndividual().qh02_1+" "+App.getInstance().getPersonaCuestionarioIndividual().qh02_2);
				}
				IDinformante.setText(""+App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			}
			
			recargarLista();
			try {	
				if (Util.esDiferente(ultimaVisita.qivresul,2,3,4,5,6,7)) {			
					getVisitaService().saveOrUpdate(registrarVisitaCompletada(),"QIVRESUL");	
				}			
			} catch (Exception e) {
				// TODO: handle exception
			}
			recargarLista();
			entityToUI(individual);
			inicio();
		}
	}
	
	public CARATULA_INDIVIDUAL registrarVisitaCompletada(){
		 return EndesCalendario.CuestionarioIndividualEstaCompleto(App.getInstance().getPersonaCuestionarioIndividual().id,  App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,App.getInstance().getVisitaI().nro_visita);
//	    	return	MyUtil.CuestionarioIndividualEstaCompleto(getCuestionarioService(), App.getInstance().getPersonaCuestionarioIndividual().id,  App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,App.getInstance().getVisitaI().nro_visita);
	}
	
    public void blanquearPantalla(){
		if(CuestionarioFragmentActivity.blanquear){
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		}
    }
    
	private void inicio() {
		
		txtQHVIOLEN.setText("");
		txtQITOTALC.setText("");
		txtQITOTALN.setText("");
		
		lblnombreMef.setText(App.getInstance().getPersonaCuestionarioIndividual().qh02_1);
		txtnumero.setText(App.getInstance().getPersonaCuestionarioIndividual().qh01);
		
		Integer violencia_dom1;
		String persona_id= App.getInstance().getPersonaCuestionarioIndividual().qh01==null?"0":App.getInstance().getPersonaCuestionarioIndividual().qh01;
		Hogar hogar = new Hogar();
		hogar = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
		
		violencia_dom1=hogar.qhviolen==null?-1:hogar.qhviolen;
		if(persona_id.equals(String.valueOf(violencia_dom1)))
			txtQHVIOLEN.setText("SI");
		else if(!persona_id.equals(String.valueOf(violencia_dom1)))
			txtQHVIOLEN.setText("NO");
		ValidarsiesSupervisora();
		individual();
		blanquearPantalla();
		EscribirCantidadesdeNinios();
	}
	public void Limpiartextos(){
		txtQITOTALN.setText("");
		txtQITOTALC.setText("");
	}
	public void EscribirCantidadesdeNinios(){
		Limpiartextos();
		Integer ninios5=getCuestionarioService().getNiniosMenoresACincoAniosporMef(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		Integer concarnet =getCuestionarioService().getNiniosConCarnetbyPersonaId(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id);
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			App.getInstance().getPersonaCuestionarioIndividual().viotartarjeta=concarnet>0?true:false;
		}
		txtQITOTALN.setText(""+ninios5);
		txtQITOTALC.setText(""+concarnet);
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			Util.cleanAndLockView(getActivity(), btnIniciar,btnFinalizar);//,spnQSRESULT
			
		}
		else{
			Util.lockView(getActivity(),false ,btnIniciar,btnFinalizar);//,spnQSRESULT
		}
	}

	public void recargarLista() {
		
		ultimaVisita=null;
		menorVisita=null;
		visitas = getCuestionarioService().getCAPVISITAsI(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		tcVisita.setData(visitas, "nro_visita", "getFechainicio","getHoraInicio", "getHoraFin", "getFechaProxima","getHoraProxima", "getResultado");
		registerForContextMenu(tcVisita.getListView());
		Integer indice=0;
		if(visitas.size()>0){
			if(visitas.size()>1){
				indice = visitas.size()-1;
			}
			else{
				indice=0;
			}
				
		ultimaVisita = getCuestionarioService().getVisitaI(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,visitas.get(indice).nro_visita, seccionesCargado);
		
		App.getInstance().setVisitaI(ultimaVisita);
		menorVisita = getCuestionarioService().getVisitaI(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id,visitas.get(0).nro_visita, seccionesCargado);
		
		}
		if (menorVisita != null && ultimaVisita != null) {
			if(ultimaVisita.qivdia!=null){
				lblFechaFinal.setText(Util.completarCadena(Util.getText(ultimaVisita.qivdia), "0", 2, COMPLETAR.IZQUIERDA) + "/"
                            + Util.completarCadena(Util.getText(ultimaVisita.qivmes), "0", 2, COMPLETAR.IZQUIERDA)+"/"+Util.getText(ultimaVisita.qivanio));
			}
			else{
				lblFechaFinal.setText("....");
				lblResultadoFinal.setText("...");
			}
			
            if (!Util.esVacio(menorVisita.qivresul)) {
                    if (!Util.esDiferente(ultimaVisita.qivresul, 1)) {
                    	lblResultadoFinal.setText("1. COMPLETA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qivresul, 2)) {
                    	lblResultadoFinal.setText("2. AUSENTE");
                    }
                    if (!Util.esDiferente(ultimaVisita.qivresul, 3)) {
                    	lblResultadoFinal.setText("3. APLAZADA");
                    }                  
                    if (!Util.esDiferente(ultimaVisita.qivresul, 4)) {
                    	lblResultadoFinal.setText("4. RECHAZADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qivresul, 5)) {
                    	lblResultadoFinal.setText("5. INCOMPLETA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qivresul, 6)) {
                    	lblResultadoFinal.setText("6. DISCAPACITADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qivresul, 7)) {
                    	lblResultadoFinal.setText("7. OTRO");
                    }
            } else {
            	lblResultadoFinal.setText("....");
            }
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcVisita.getListView())) {
			menu.setHeaderTitle("Opciones de las Visitas");
			menu.add(0, 0, 1, "Editar");
			menu.add(0, 1, 1, "Eliminar");
			menu.getItem(0).setVisible(false);
			menu.getItem(1).setVisible(false);
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			if (info.position == visitas.size() - 1) {
				if(visitas.get(info.position).qivresul!=null && !Util.esDiferente(visitas.get(info.position).qivresul, 6)){
					menu.getItem(0).setVisible(true);
				}
				else{
					menu.getItem(0).setVisible(false);
				}
//				if (visitas.get(info.position).qivresul == null) {
//					menu.getItem(0).setVisible(false);
//				}
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
			case 0:
				editarVisita(info.position);
				break;
			case 1:
				eliminarVisita(info.position);
				break;
			}
		}
		return super.onContextItemSelected(item);
	}

	private void editarVisita(int position) {
		abrirDialogo(visitas.get(position), ACTION.EDITAR);
	}

	private void eliminarVisita(int position) {
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Desea borrar la visita?");
		dialog.put("visita", visitas.get(position));
		dialog.showDialog();
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	
	public Seccion01Service getPersonaService() {
   	  if (personaService == null) {
   		  personaService = Seccion01Service.getInstance(getActivity());
   	  }
   	  return personaService;
    }
	public UsuarioService getUsuarioService() {
		if (usuarioservice == null) {
			usuarioservice = UsuarioService.getInstance(getActivity());
		}
		return usuarioservice;
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAccept() {
		if (dialog == null) {
			return;
		}
		CARATULA_INDIVIDUAL bean = (CARATULA_INDIVIDUAL) dialog.get("visita");
		if (bean == null) {
			ToastMessage.msgBox(getActivity(), "Error al eliminar visita.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return;
		}
		if (!getVisitaService().EliminarVisitaIndividual(null, bean.id, bean.hogar_id, bean.persona_id, bean.nro_visita)){
			ToastMessage.msgBox(getActivity(),"Los datos no fueron eliminados.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return;
		}
		recargarLista();
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}