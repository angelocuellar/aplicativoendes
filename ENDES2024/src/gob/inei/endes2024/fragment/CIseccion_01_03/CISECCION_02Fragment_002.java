package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI206;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI207_A;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI207_B;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI208;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI209;
	
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	public GridComponent2 gdHijosMuertos,gdTotalHijos;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta206,lblpregunta207,lblpregunta207_Ind,lblpregunta208,lblpregunta209,lblpregunta209_Ind,lblpregunta209_total_Ind,lblhijasmuertas,lblhijosmuertos,lbltotalhijos;
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7;
	Spanned texto209Total;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_02Fragment_002() {}
	public CISECCION_02Fragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI207_A, 0, 20, null, 99);
		rango(getActivity(), txtQI207_B, 0, 20, null, 99);
		rango(getActivity(), txtQI208, 0, 20, null, 99);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI203_A","QI203_B","QI205_A","QI205_B","QI206","QI207_A","QI207_B","QI208","QI209","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI206","QI207_A","QI207_B","QI208","QI209")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta206 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi206);
		lblpregunta207 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi207_a);
		Spanned texto207 =Html.fromHtml("<b>SI DIJO NINGUNO, ANOTE \"0\"</b>");
		lblpregunta207_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpregunta207_Ind.setText(texto207);
		lblpregunta208 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.ciseccion_02qi208);
		lblpregunta209 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi209);
		Spanned texto209 =Html.fromHtml("209. <b>VERIFIQUE 208:</b>");
		lblpregunta209_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17);
		lblpregunta209_Ind.setText(texto209);
		lblpregunta209 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi209);
		texto209Total =Html.fromHtml(" \u00BFUsted ha tenido en <b>total</b> $ hijo(s) nacido(s) vivo(s) durante toda su vida?\n\u00BFEs correcto?");
		lblpregunta209_total_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta209_total_Ind.setText(texto209Total);
		rgQI206=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi206_1,R.string.ciseccion_02qi206_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI206ChangeValue");
		txtQI207_A=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).callback("ontxtQI207_AChangeValue");
//		txtQI207_A.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//					Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
//			    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
//			    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
//			    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
//			    	Integer int207Hijo = txtQI207_B.getValue()==null?0:Integer.valueOf(txtQI207_B.getValue().toString());
//			    	Integer int207Hija = Integer.valueOf(s.toString());
//			    	Integer total = 0;
//			    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
//			    	
//			    	txtQI208.setValue(total.toString());
//			    	RenombrarLabels(total.toString());
//				}
//			}
//		});
		txtQI207_B=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).callback("ontxtQI207_BChangeValue");
//		txtQI207_B.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
//			    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
//			    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
//			    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
//			    	Integer int207Hijo = Integer.valueOf(s.toString());
//			    	Integer int207Hija = txtQI207_A.getValue()==null?0:Integer.valueOf(txtQI207_A.getValue().toString());
//			    	Integer total = 0;
//			    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
//			    	
//			    	txtQI208.setValue(total.toString());
//			    	RenombrarLabels(total.toString());
//				}
//			}
//		});
		lblhijasmuertas  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_02qi207_a_1).textSize(19).centrar();
		lblhijosmuertos  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_02qi207_a_2).textSize(19).centrar();
		lbltotalhijos  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi208_1).textSize(16).centrar();
		
		txtQI208=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).readOnly();
		rgQI209=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi209_1,R.string.ciseccion_02qi209_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		gdHijosMuertos = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdHijosMuertos.addComponent(lblhijasmuertas);
		gdHijosMuertos.addComponent(txtQI207_A);
		gdHijosMuertos.addComponent(lblhijosmuertos);
		gdHijosMuertos.addComponent(txtQI207_B);
		
		gdTotalHijos = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdTotalHijos.addComponent(lbltotalhijos);
		gdTotalHijos.addComponent(txtQI208);
    }
    @Override
    protected View createUI() {
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta206,rgQI206);
		q2 = createQuestionSection(lblpregunta207,gdHijosMuertos.component(),lblpregunta207_Ind);
		q3 = createQuestionSection(lblpregunta208,gdTotalHijos.component());
		q4 = createQuestionSection(lblpregunta209_Ind,lblpregunta209,lblpregunta209_total_Ind,rgQI209);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
    return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
			else {
				if(individual.qi208==0) {
					
				if(!getCuestionarioService().borrarNacimientos(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id)) {
					ToastMessage.msgBox(this.getActivity(), "Los nacimientos no pudieron ser eliminados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return false;
				}
				
				else {
					
					SeccionCapitulo[]seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218","ESTADOCAP2")};
					List<CISECCION_02> detalles=null;
					detalles = getCuestionarioService().getListaNacimientosCompletobyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
					App.getInstance().getPersonaCuestionarioIndividual().total_ninios = detalles.size();
					SeccionCapitulo[] seccionesCargado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
					List<CISECCION_02> detalles2=null;
					detalles2 = getCuestionarioService().getNacimientosVivoListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
					App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos = detalles2.size();
				}
					
				}
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		App.getInstance().getPersonaCuestionarioIndividual().qi208=individual.qi208;
		App.getInstance().getPersonaCuestionarioIndividual().qi206=individual.qi206;
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (Util.esVacio(individual.qi206)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI206");
			view = rgQI206;
			error = true;
			return false;
		}
		if (Util.esDiferente(individual.qi206,2)) {
			if (Util.esVacio(individual.qi207_a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI207_A");
				view = txtQI207_A;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi207_b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI207_B");
				view = txtQI207_B;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi207_b,individual.qi207_a) && !Util.esDiferente(individual.qi207_a,0)){
				mensaje = "Ambos valores no pueden ser cero";
				view = txtQI207_B;
				error = true;
				return false;
			}
		}
		if (Util.esVacio(individual.qi208)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI208");
			view = txtQI208;
			error = true;
			return false;
		}
		if (Util.esVacio(individual.qi209)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI209");
			view = rgQI209;
			error = true;
			return false;
		}else if (individual.qi209==2) {
			mensaje = "209. INDAGUE Y CORRIJA 201-208 SI ES NECESARIO";
			view = rgQI209;
			error = true;
			return false;
		}
		return true;
    }
    @Override
    public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual);
		inicio();
    }
    
    private void inicio() {
    	validarPregunta206();
    	renombrar();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void validarPregunta206(){
    	if (MyUtil.incluyeRango(2,2,rgQI206.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI207_A,txtQI207_B);
			q2.setVisibility(View.GONE);
		} else {
			MyUtil.LiberarMemoria();
			Util.lockView(getActivity(), false,txtQI207_A,txtQI207_B);
			q2.setVisibility(View.VISIBLE);
		}
    }
    public void renombrar(){
    	Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
    	Integer int207Hijo = individual.qi207_a==null?0:individual.qi207_a;
    	Integer int207Hija = individual.qi207_b==null?0:individual.qi207_b;
    	Integer total = 0;
    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
    	txtQI208.setText(total.toString());
    	RenombrarLabels(total.toString());
    }
    public void ontxtQI207_AChangeValue() {
    	Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
    	Integer int207Hijo = txtQI207_B.getValue()==null?0:Integer.valueOf(txtQI207_B.getValue().toString());
    	Integer int207Hija = txtQI207_A.getValue()==null?0:Integer.valueOf(txtQI207_A.getValue().toString());
    	Integer total = 0;
    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
    	
    	txtQI208.setValue(total.toString());
    	RenombrarLabels(total.toString());
    	txtQI207_B.requestFocus();
    }
    
    public void ontxtQI207_BChangeValue() {
    	Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
    	Integer int207Hijo = txtQI207_B.getValue()==null?0:Integer.valueOf(txtQI207_B.getValue().toString());
    	Integer int207Hija = txtQI207_A.getValue()==null?0:Integer.valueOf(txtQI207_A.getValue().toString());
    	Integer total = 0;
    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
    	txtQI208.setValue(total.toString());
    	RenombrarLabels(total.toString());
    	txtQI207_B.requestFocus();
    	rgQI209.requestFocus();
    }
    
    public void onrgQI206ChangeValue() {
    	Integer int203Hijo = individual.qi203_a==null?0:individual.qi203_a;
    	Integer int203Hija = individual.qi203_b==null?0:individual.qi203_b;
    	Integer int205Hijo = individual.qi205_a==null?0:individual.qi205_a;
    	Integer int205Hija = individual.qi205_b==null?0:individual.qi205_b;
		if (MyUtil.incluyeRango(2,2,rgQI206.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),txtQI207_A,txtQI207_B);
			q2.setVisibility(View.GONE);
			Integer int207Hijo = 0;
	    	Integer int207Hija = 0;
	    	Integer total = 0;
	    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
	    	txtQI208.setValue(total.toString());
	    	RenombrarLabels(total.toString());
			txtQI208.requestFocus();
		} else {
			MyUtil.LiberarMemoria();
			Util.lockView(getActivity(), false,txtQI207_A,txtQI207_B);
			q2.setVisibility(View.VISIBLE);
			Integer int207Hijo = txtQI207_B.getValue()==null?0:Integer.valueOf(txtQI207_B.getValue().toString());
	    	Integer int207Hija = txtQI207_A.getValue()==null?0:Integer.valueOf(txtQI207_A.getValue().toString());
	    	Integer total = 0;
	    	total=int203Hija+int203Hijo+int205Hija+int205Hijo+int207Hija+int207Hijo;
	    	txtQI208.setValue(total.toString());
	    	RenombrarLabels(total.toString());
			txtQI207_A.requestFocus();
		}
    }
    
    public void RenombrarLabels(String nueRee){
    	lblpregunta209_total_Ind.setText(texto209Total);
    	lblpregunta209_total_Ind.setText(lblpregunta209_total_Ind.getText().toString().replace("$", nueRee));
	}
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI206.readOnly();
    		rgQI209.readOnly();
    		txtQI207_A.readOnly();
    		txtQI207_B.readOnly();
    		txtQI208.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
			else {
				if(individual.qi208==0) {
					
				if(!getCuestionarioService().borrarNacimientos(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id)) {
					ToastMessage.msgBox(this.getActivity(), "Los nacimientos no pudieron ser eliminados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return App.NODEFINIDO;
				}
				
				else {
					
					SeccionCapitulo[]seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218","ESTADOCAP2")};
					List<CISECCION_02> detalles=null;
					detalles = getCuestionarioService().getListaNacimientosCompletobyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
					App.getInstance().getPersonaCuestionarioIndividual().total_ninios = detalles.size();
					SeccionCapitulo[] seccionesCargado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
					List<CISECCION_02> detalles2=null;
					detalles2 = getCuestionarioService().getNacimientosVivoListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
					App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos = detalles2.size();
				}
					
				}
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		App.getInstance().getPersonaCuestionarioIndividual().qi208=individual.qi208;
		return App.INDIVIDUAL;
	}
}