package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_03Fragment_009Dialog;
import gob.inei.endes2024.fragment.seccion01.Dialog.Seccion01_001_4Dialog;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_009 extends FragmentForm implements Respondible {
	
	public List<Seccion01> detalles;
	Hogar bean;
	CISECCION_01_03 individual;
	
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo, lblintro;
	public TableComponent tcPersonas;
	Seccion01ClickListener adapter;
	SeccionCapitulo[] seccionesGrabado, seccionesCargado,seccionesCargados1_3,seccionesCargadoHogar,seccionesGrabadoHogar ;
	public TextField txtCabecera;

	private HogarService hogarService;
	private Seccion01Service personaService;
	
	public List<Seccion01> listado=new ArrayList<Seccion01>();
	public Integer contador=0;
	int dato=-17;
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;

	public CISECCION_02Fragment_009() {
	}

	public CISECCION_02Fragment_009 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "PERSONA_INFORMANTE_ID","QH_OBS_SECCION01","QH110H","QH93")};
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QH_OBS_SECCION01") };
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH02_1", "QH02_2","QH02_3","QH07", "ESTADO","CUESTIONARIO_ID","QI212") };
		seccionesCargados1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI201","QI206","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_titulo2).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		Spanned textointro = Html.fromHtml("240. A continuaci�n le har� algunas preguntas para saber si Usted (o alguna de sus hijas o hijos, menores de 6 a�os) presenta alguna dificultad o limitaci�n PERMANENTE, que le impida desarrollarse normalmente en sus actividades diarias.");
		lblintro = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblintro.setText(textointro);
		
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 770).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(700, 770).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcPersonas.addHeader(R.string.seccionDiscapacidad_NroMef, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccionDiscapacidad_NroPersona, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccionDiscapacidad_Nombres, 1.3f,TableComponent.ALIGN.LEFT);
	}

	@Override
	protected View createUI() {
		buildFields();

		LinearLayout q0 = createQuestionSection(txtCabecera,lblTitulo);
		LinearLayout q1 = createQuestionSection(lblintro);
		LinearLayout q2 = createQuestionSection(tcPersonas.getTableView());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(bean);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = false;

		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(bean, dbTX, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		if(App.getInstance().getHogar()!=null){
			App.getInstance().getHogar().qh110h=bean.qh110h;
		}
		return flag;
	}



	@Override
	public void cargarDatos() {
		bean = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
//		detalles = getCuestionarioService().getSeccion01ListDiscapacidad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		if (bean == null) {
			bean = new Hogar();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id = App.getInstance().getHogar().hogar_id;
			detalles = new ArrayList<Seccion01>();
		}
		
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargados1_3);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		
		entityToUI(bean);
		cargarTabla();
		inicio();
		
		if (App.getInstance().getPersonaCuestionarioIndividual()!=null ) {
			App.getInstance().getPersonaCuestionarioIndividual().qi106=individual.qi106;
			App.getInstance().getPersonaCuestionarioIndividual().qi201=individual.qi201;
			App.getInstance().getPersonaCuestionarioIndividual().qi206=individual.qi206;
			App.getInstance().getPersonaCuestionarioIndividual().qi226=individual.qi226;
			App.getInstance().getPersonaCuestionarioIndividual().qi230=individual.qi230;
		}
		else{
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		}
	}

	private boolean validar() {
		Integer suma=0;
		if(detalles.size()>0){
			for (Seccion01 corre: detalles) {
				suma+=corre.estado;
			}
		}
		if(suma>0){
			mensaje = "Debe completar las preguntas de la Seccion Discapacidad";
			view = tcPersonas;
			error = true;
			return false;
		}
		return true;
	}

	
	
	public void refrescarPersonas(Seccion01 personas) {
		if (detalles.contains(personas)) {
			cargarTabla();
			return;
		} else {
			detalles.add(personas);
			cargarTabla();
		}
	}

	public void cargarTabla() {
//		detalles = getCuestionarioService().getSeccion01ListDiscapacidad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
//		Log.e("ID: ",""+App.getInstance().getPersonaCuestionarioIndividual().id);
//		Log.e("HOGAR_ID: ",""+App.getInstance().getPersonaCuestionarioIndividual().hogar_id);
//		Log.e("PERSONA_ID: ",""+App.getInstance().getPersonaCuestionarioIndividual().persona_id);
//		detalles.clear();
		detalles = getCuestionarioService().getDiscapacidadParaIndividual(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,App.CUEST_ID_INDIVIDUAL, seccionesCargado);
		listado.clear();
//		Log.e("Cantidad: ",""+detalles.size());
		int valor=1;
		for(Seccion01 persona:detalles){
			persona.ordenado=valor;
			listado.add(persona);
			valor++;
		}
		tcPersonas.setData(listado, "persona_id","qi212", "getNombre");
		
		for (int row = 0; row < listado.size(); row++) {
			if (obtenerEstado(listado.get(row)) == 1) {
				// borde de color azul
				tcPersonas.setBorderRow(row, true);
			} else if (obtenerEstado(listado.get(row)) == 2) {
				// borde de color rojo
				tcPersonas.setBorderRow(row, true, R.color.red);
			} else {
				tcPersonas.setBorderRow(row, false);
			}
		}

		registerForContextMenu(tcPersonas.getListView());
	}
    private int obtenerEstado(Seccion01 detalle) {
		if (!Util.esDiferente(detalle.estado, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalle.estado,1)) {
			return 2;
		}
		return 0;
	}
    


	private void inicio() {
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
		}
	}
	
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(tcPersonas.getWindowToken(), 0);
	}
	
	public class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//			arg0.setEnabled(false);
//			arg1.setEnabled(false);
			
			Seccion01 c = (Seccion01) detalles.get(arg2);
/*			Integer posicionpersona = -1;
			Integer estado = -1;
			for (int i = 0; i < detalles.size(); i++) {
				if (c.persona_id == detalles.get(i).persona_id) {
					posicionpersona = i;
					break;
				}
			}
			posicionpersona = posicionpersona == 0 ? 0 : posicionpersona - 1;
			for (int i = 0; i < detalles.size(); i++) {
				if (posicionpersona == i) {
					estado = detalles.get(i).estado;
					break;
				}
			}*/
//			if (c.persona_id == App.JEFE && contador==0) {
//				abrirDetalle(c, arg2, (List<Seccion01>) detalles);
//				contador++;
//			} else {
				if (contador==0) {
					abrirDetalle(c, arg2, (List<Seccion01>) detalles);
					contador++;
				}
//			}
		}
	}
	
	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}

	public void abrirDetalle(Seccion01 tmp, int index, List<Seccion01> detalles) {
//		if(Util.esDiferente(dato, index,index)){
//		dato=index;
		
		FragmentManager fm = CISECCION_02Fragment_009.this.getFragmentManager();
		CISECCION_03Fragment_009Dialog aperturaDialog = CISECCION_03Fragment_009Dialog.newInstance(CISECCION_02Fragment_009.this, tmp, index, detalles);
		if(aperturaDialog.getShowsDialog()){
			aperturaDialog.show(fm, "aperturaDialog");
			}
//		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		Log.e("AQUIII ","EEEE");
		
	}
}