package gob.inei.endes2024.fragment.CIseccion_01_03;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_03Fragment_003_1Dialog;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_03Fragment_003_2Dialog;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_003 extends FragmentForm implements Respondible {
	
	@FieldAnnotation(orderIndex=1)
	public ButtonComponent btnIniciar;
	@FieldAnnotation(orderIndex=2)
	public ButtonComponent btnMostrar;
	@FieldAnnotation(orderIndex=3)
	public TextAreaField txtQIOBS_CAL;  
	
	
	public TableComponent tctramos;
	public CICALENDARIO_COL01_03 calendarioinicial,calendario; 
	public List<CICALENDARIO_TRAMO_COL01_03> detalles;
	public List<CISECCION_02> ninios;
	public LabelComponent lblpreguntareferencial1,lblpreguntareferencial2,lblpreguntareferencial3,lblpreguntareferencialcolumna2,lblpreguntareferencialcolumna3,lblpreguntageneral,lblpreguntageneral_ind,lbltitulo,lblobservacioncalendario;
	
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesCargadoCalendario,capitulo01_03cargado,seccionesCargadoTramos,seccionesCargadoTerminaciones;
	private SeccionCapitulo[] seccionesGrabado01_03,seccionesGrabadoTramos;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	CICALENDARIO_COL01_03 calendariorecorridoinicio,calendariorecorridofin;
	CISECCION_01_03 terminaciones;
	private Calendar fecha_actual =null; 
	private DialogComponent dialog;
	public CICALENDARIO_TRAMO_COL01_03 tramo1=null;
	public CICALENDARIO_TRAMO_COL01_03 tramo2=null;
	private boolean nacimientoenfechaactual=false; 
	private boolean terminacionenfechaactual=false;
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;
	public CISECCION_03Fragment_003() {}
	public CISECCION_03Fragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	  @Override 
	  public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		}
	  @Override  
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			rootView = createUI();
			initObjectsWithoutXML(this, rootView);
			enlazarCajas();
			listening();
			seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
			capitulo01_03cargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1,"QI230","QI231_M","QI231_Y","QI233","QI310","QI311_A","QI311_B","QI311_C","QI311_D","QI311_E","QI311_F","QI311_G","QI311_H","QI311_I","QI311_J","QI311_K","QI311_L","QI311_M","QI311_X","QI311_O","QI312","QI316M","QI316Y","QI226","QI227","QI234","QI320","QI315M","QI315Y","QICAL_CONS","QIOBS_CAL","ID","HOGAR_ID","PERSONA_ID") };
			seccionesCargadoCalendario = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","QIINDICE","QIMES_ID","QIANIO","QICOL1","QICOL1_O","QITRAMO_ID","QICOL2","QICOL3")};
			seccionesCargadoTramos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","ID","QICOL2","QICOL3","HOGAR_ID","PERSONA_ID","QICANTIDAD")};
			seccionesGrabadoTramos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","QICANTIDAD","QICOL3")};
			seccionesCargadoTerminaciones = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"TERMINACION_ID","QI235_M","QI235_Y","QI235_D","ID","HOGAR_ID","PERSONA_ID")};
			seccionesGrabado01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QIOBS_CAL")};
			return rootView;
	  }
	  @Override 
	  protected void buildFields() {
		  lbltitulo = new LabelComponent(getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cicalendario).textSize(20).centrar();
		  lblpreguntageneral = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cicalendarioqi319).textSize(19);
		  lblpreguntageneral_ind = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cicalendarioqi319_ind).textSize(17);
		  
		  btnIniciar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(150, 55).text(R.string.v_l_iniciar);
		  btnIniciar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(detalles.size()==0){
					return;
				}
				ExisteEspaciosEnBlancoEnElCalendarioTramo();
			}
		});
//		  Integer d=getResources().getDisplayMetrics().densityDpi;
//		  if(!Util.esDiferente(d,App.YOGA8)){
//			  tctramos = new TableComponent(getActivity(), this, App.ESTILO).size(450, 750).headerHeight(altoComponente + 10).dataColumHeight(50);
//			}
//			else{
				tctramos = new TableComponent(getActivity(), this, App.ESTILO).size(550, 750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//			}
		  tctramos.addHeader(R.string.cicalendario_tramodesde, 0.25f,TableComponent.ALIGN.LEFT);
		  tctramos.addHeader(R.string.cicalendario_tramoresultadodesde,0.18f,TableComponent.ALIGN.CENTER);
		  tctramos.addHeader(R.string.cicalendario_tramohasta, 0.25f,TableComponent.ALIGN.CENTER);
		  tctramos.addHeader(R.string.cicalendario_tramoresultadohasta, 0.18f,TableComponent.ALIGN.CENTER);
		  tctramos.addHeader(R.string.cicalendario_tramocantidad, 0.18f,TableComponent.ALIGN.CENTER);
		  tctramos.addHeader(R.string.cicalendario_tramoCol2, 0.15f,TableComponent.ALIGN.CENTER);
		  tctramos.addHeader(R.string.cicalendario_tramoCol3, 0.15f,TableComponent.ALIGN.CENTER);
		  
		  btnMostrar= new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(150, 55).text(R.string.cibtnMostrar);
		  btnMostrar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			 Integer total=	getCuestionarioService().CalendarioCompletadoPorcantidad(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha_actual);
			 Integer completo= 73-MyUtil.DeterminarIndice(fecha_actual.get(Calendar.YEAR), fecha_actual.get(Calendar.MONTH)+1);
				if(detalles.size()==0  || total>completo){
					ToastMessage.msgBox(getActivity(), "Verifique Tabla Existe Eventos demas. ", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return;
				}
				MostrarCalendario();
			}
		});
		  txtQIOBS_CAL= new TextAreaField(getActivity()).size(250, 750).maxLength(1000).alfanumerico();
		  lblobservacioncalendario = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.obs_cal).textSize(16);
	  }
	  
	    @Override 
	    protected View createUI() {
			buildFields();
			q0=createQuestionSection(lbltitulo);
			q1=createQuestionSection(lblpreguntageneral,lblpreguntageneral_ind);
			q2=createButtonSection(btnIniciar,btnMostrar); 
			q3 = createQuestionSection(0,tctramos.getTableView());
			q4 = createQuestionSection(lblobservacioncalendario,txtQIOBS_CAL);
			ScrollView contenedor = createForm();
			LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
			form.addView(q0);
			form.addView(q1);
			form.addView(q2);
			form.addView(q3);
			form.addView(q4);
			return contenedor;
	    }
	    public void abrirDetalle(CICALENDARIO_TRAMO_COL01_03 tramoinicial, CICALENDARIO_TRAMO_COL01_03 tramo,CICALENDARIO_TRAMO_COL01_03 tramofinal){
	    	if(tramo==null){
	    		tramo = new CICALENDARIO_TRAMO_COL01_03();
	    		tramo.id=App.getInstance().getPersonaCuestionarioIndividual().id;
	    		tramo.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
	    		tramo.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id;
	    		Integer cantidad = getCuestionarioService().getUltimotramoidRegistrado(tramo.id, tramo.hogar_id, tramo.persona_id);
	    		tramo.qitramo_id=cantidad+1;
	    	}
	    	FragmentManager fm = CISECCION_03Fragment_003.this.getFragmentManager();
	    	CISECCION_03Fragment_003_1Dialog aperturaDialog = CISECCION_03Fragment_003_1Dialog.newInstance(this,tramoinicial, tramo,tramofinal);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
	    }
	    
	    public void MostrarCalendario(){
	    	FragmentManager fm = CISECCION_03Fragment_003.this.getFragmentManager();
	    	CISECCION_03Fragment_003_2Dialog aperturaDialog = CISECCION_03Fragment_003_2Dialog.newInstance(this);
			aperturaDialog.setAncho(MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
	    }
	    
	    @Override 
	    public boolean grabar() {
	    	uiToEntity(terminaciones);
			if (!validar()) {
				if (error) {
					if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					if (view != null) view.requestFocus();
				}
				return false;
			}
			try {
				if(!getCuestionarioService().saveOrUpdate(terminaciones,seccionesGrabado01_03)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return false;
				}
			} catch (SQLException e) {
				ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
	    	App.getInstance().getPersonaCuestionarioIndividual().qi320=terminaciones.qi320;
	    	App.getInstance().getPersonaCuestionarioIndividual().qi316y=terminaciones.qi316y;
	    	App.getInstance().getPersonaCuestionarioIndividual().qi315y=terminaciones.qi315y;
			return true;
	    }
	    private boolean validar() {
			if(!isInRange()) return false;
//			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
			if (!CalendarioCompletado()) {
				validarMensaje("FALTA COMPLETAR CALENDARIO!!");
				}
			return true;
	    }
	    private void validarMensaje(String msj) {
	        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	    }
	    @Override
		public void cargarDatos() {
	    	terminaciones=getCuestionarioService().getTerminaciones(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, capitulo01_03cargado);
	    	if(terminaciones.qical_cons==null){
				fecha_actual=Calendar.getInstance();
			}
			else{
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(terminaciones.qical_cons);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecha_actual = cal;
			}
	    	ninios = getCuestionarioService().getNacimientosparacalendario(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha_actual.get(Calendar.YEAR)-5, seccionesCargado);
	    	if(ninios.size()>0){
	    		for(CISECCION_02 n: ninios){
	    			Integer mes=Integer.parseInt(n.qi215m.toString());
	    			if(!Util.esDiferente(mes, fecha_actual.get(Calendar.MONTH)+1) && !Util.esDiferente(n.qi215y, fecha_actual.get(Calendar.YEAR))){
	    				nacimientoenfechaactual=true;
	    				break;
	    			}
	    			else{
	    				nacimientoenfechaactual=false;
	    			}
	    		}
	    	}
	    	if(terminaciones!=null &&  terminaciones.qi230!=null && !Util.esDiferente(terminaciones.qi230, 1) && terminaciones.qi231_y!=null && terminaciones.qi231_y>=App.ANIOPORDEFECTO){
	    		Integer mes= Integer.parseInt(terminaciones.qi231_m.toString());
	    		if(!Util.esDiferente(mes, fecha_actual.get(Calendar.MONTH)+1) && !Util.esDiferente(terminaciones.qi231_y, fecha_actual.get(Calendar.YEAR))){
	    			terminacionenfechaactual=true;
	    		}
	    		else{
	    			terminacionenfechaactual=false;
	    		}
	    		
	    	}
	    	RegistrarEmbarazoOMetodoAnticonceptivoONinguno();
	    	RegistrarNacimientosPrimeroenTramos();
	    	Registrarterminaciones();
	    	TerminacionesSiesqueExistiera();
			cargarTabla();
			if(CalendarioCompletado()){
	    		btnIniciar.setEnabled(false);
	    	}
	    	else{
	    		btnIniciar.setEnabled(true);
	    	}
			
			entityToUI(terminaciones);
			inicio();
		}
	    public void ExisteEspaciosEnBlancoEnElCalendarioTramo(){
			List<CICALENDARIO_TRAMO_COL01_03> todoslostramos = getCuestionarioService().getTramosdelcalendario(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoTramos);
			List<CICALENDARIO_TRAMO_COL01_03> todossegundorecorrido = new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
			boolean utilizaalgunmetodo=false;
			
			Collections.reverse(todoslostramos);
			todossegundorecorrido.addAll(todoslostramos);
			for(CICALENDARIO_TRAMO_COL01_03 item: todoslostramos){ 
				todossegundorecorrido.remove(item);
				if(tramo2!=null){
					break;
				}
				tramo1=null;
				tramo1=item;
				if(item.qimes_fin==fecha_actual.get(Calendar.MONTH)+1 && item.qianio_fin==fecha_actual.get(Calendar.YEAR) && !item.qir_final.equals("0") && !item.qir_final.equals("E") && !item.qir_final.equals("T") && !item.qir_final.equals("N") && item.qicol3==null){
					utilizaalgunmetodo=true;
				}
				for(CICALENDARIO_TRAMO_COL01_03 tramo:todossegundorecorrido){
					Integer Mesesfin=tramo.qimes_fin;
					Integer AnioFin= tramo.qianio_fin;
					if(Mesesfin==12){
							Mesesfin=0;
						AnioFin++;
					}
					Mesesfin=Mesesfin+1;
					if(!Util.esDiferente(item.qianio_ini, AnioFin) && !Util.esDiferente(item.qimes_ini, Mesesfin) && Util.esDiferente(item.qitramo_id,tramo.qitramo_id) && utilizaalgunmetodo==false){
						break;
					}
					else{
						utilizaalgunmetodo=false;
						tramo2=tramo;
						break;
					}
				}
			}
			if(tramo2==null){
				tramo2= new CICALENDARIO_TRAMO_COL01_03();
				tramo2.qianio_fin=App.ANIOPORDEFECTO;
				tramo2.qimes_fin = App.MESPORDEFECTO;
				tramo2.qir_final="";
			}
			abrirDetalle(tramo1,null,tramo2);
			tramo1=null;
			tramo2=null;
		}
	    public void inicio(){
	    	String texto=lblpreguntageneral_ind.getText().toString();
	    	texto=texto.replace("#;", App.ANIOPORDEFECTO+";");
	    	lblpreguntageneral_ind.setText(texto);
	    	Integer int315 = terminaciones.qi315y==null?0:terminaciones.qi315y;
	    	Integer int316 = terminaciones.qi316y==null?0:terminaciones.qi316y;
	    	Integer mayor=int315>int316?int315:int316;
	    	Log.e("sds","1");
	    	if (mayor>=App.ANIOPORDEFECTO || mayor==0) {
	    		Log.e("sds","2");
				q0.setVisibility(View.VISIBLE);
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
			}
	    	else{
	    		Log.e("sds","3");
	    		q0.setVisibility(View.GONE);
				q1.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
	    	}
	    	ValidarsiesSupervisora();
	    }
	    public void cargarTabla(){
	    	detalles=getCuestionarioService().getTramosdelcalendario(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoTramos);
	    	Collections.reverse(detalles);
	    	tctramos.setData(detalles, "getInicio","qir_inicial","getFin","qir_final","qicantidad","qicol2","qicol3");
	    	registerForContextMenu(tctramos.getListView());
	    }
	  public void ValidarsiesSupervisora(){
		  Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		btnIniciar.setEnabled(false);
	    		txtQIOBS_CAL.setEnabled(false);
	    	}
		  
	  }

	  public String ObtenerCol3SiesEsterilizacion(){
	    	String retorna="";
	    	if(!Util.esDiferente(terminaciones.qi312, 10)){
	    		retorna="1";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 11)){
	    		retorna="2";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 12)){
	    		retorna="5";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 13)){
	    		retorna="6";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 14)){
	    		retorna="7";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 15)){
	    		retorna="8";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 16)){
	    		retorna="A";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 20)){
	    		retorna="B";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 21)){
	    		retorna="D";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 31)){
	    		retorna="F";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 32)){
	    		retorna="H";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 96)){
	    		retorna="X";
	    	}
	    	if(!Util.esDiferente(terminaciones.qi312, 98)){
	    		retorna="9";
	    	}
	    	return retorna;
	    }
	    public void RegistrarEmbarazoOMetodoAnticonceptivoONinguno(){
	    	Integer tramoinicial=-1;
	    	CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
    		CICALENDARIO_COL01_03 verificarmetodoactual= new CICALENDARIO_COL01_03();
    		Integer mesactual=fecha_actual.get(Calendar.MONTH)+1;
    		Integer anioactual=fecha_actual.get(Calendar.YEAR);
    		if(terminaciones!=null && terminaciones.qi226!=null && !Util.esDiferente(terminaciones.qi226, 1)){//embarazo
    			verificacion_tramo = getCuestionarioService().getTramoparaEmbarazo(terminaciones.id, terminaciones.hogar_id, terminaciones.persona_id,App.EMBARAZO_IDDEFAULT, seccionesCargadoTramos);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(terminaciones.id,terminaciones.hogar_id,terminaciones.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
    			tramo.id=terminaciones.id;
    			tramo.hogar_id=terminaciones.hogar_id;
    			tramo.persona_id=terminaciones.persona_id;
    	    	Integer indice= MyUtil.DeterminarIndice(anioactual, mesactual)+terminaciones.qi227;
        		indice=indice-1;
        		indice=indice>72?72:indice;
        		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
        		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
    			tramo.qimes_fin=mesactual;
    			tramo.qianio_fin=anioactual;
    			tramo.embarazo_id=App.EMBARAZO_IDDEFAULT;
    			tramo.qir_inicial=App.CALENDARIOEMBARAZO;
    			tramo.qir_final=App.CALENDARIOEMBARAZO;
    			tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)+1;
    			try {
        		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
    			} catch (Exception e) {
    				// TODO: handle exception
    			}
    		}
    		else if(terminaciones!=null && !nacimientoenfechaactual &&  !terminacionenfechaactual && (terminaciones.qi310!=null && !Util.esDiferente(terminaciones.qi310,1))||(terminaciones.qi311_a!=0 && !Util.esDiferente(terminaciones.qi311_a, 1))){//Metodo actual
    			Integer mesmetodo =terminaciones.qi316m==null?Integer.parseInt(terminaciones.qi315m):Integer.parseInt(terminaciones.qi316m);
	    		Integer aniometodo = terminaciones.qi316y==null?terminaciones.qi315y:terminaciones.qi316y;
	    		if(aniometodo<App.ANIOPORDEFECTO){
	    			mesmetodo=App.MESPORDEFECTO;
	    			aniometodo=App.ANIOPORDEFECTO;
	    		}
	    		verificacion_tramo = getCuestionarioService().getTramoparaMetodoActual(terminaciones.id, terminaciones.hogar_id, terminaciones.persona_id,App.METODO_IDDEFAULT, seccionesCargadoTramos);
	    		boolean mesesigualalnacimiento=false;
	    		if(ninios.size()>0 && CodigoCorrecto().equals("1") || CodigoCorrecto().equals("2") || CodigoCorrecto().equals("5")){
		    		for(CISECCION_02 n: ninios){
		    			if(n.qi215m!=null){
		    			Integer mes= Integer.parseInt(n.qi215m.toString());
			    			if(!Util.esDiferente(mesmetodo, mes) && !Util.esDiferente(n.qi215y, aniometodo)){
			    				mesesigualalnacimiento=true;
			    				break;	
			    			}
		    			}
		    		}
	    		}
	    		if(mesesigualalnacimiento){
	    			mesmetodo=mesmetodo+1;
	    		}
	    		Integer cantidad=MyUtil.DeterminarIndice(aniometodo, mesmetodo)-MyUtil.DeterminarIndice(anioactual, mesactual);
    	    	Integer indice=MyUtil.DeterminarIndice(anioactual, mesactual)+cantidad;
	    		if(verificacion_tramo!=null && verificacion_tramo.qicol3==null ){ 
	    			EliminacionDeTablas(terminaciones.id,terminaciones.hogar_id,terminaciones.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			if(verificacion_tramo!=null){
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id!=null?verificacion_tramo.qitramo_id:null;
	    			tramo.qicol3 = verificacion_tramo.qicol3!=null?verificacion_tramo.qicol3:null;
	    			}
	    		}
    			tramo.id=terminaciones.id;
    			tramo.hogar_id=terminaciones.hogar_id;
    			tramo.persona_id=terminaciones.persona_id;
        		indice=indice>72?72:indice;
        		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
        		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
    			tramo.qimes_fin=mesactual;
    			tramo.qianio_fin=anioactual;
    			tramo.metodo_id=App.METODO_IDDEFAULT;
    			tramo.qir_inicial=CodigoCorrecto();
    			tramo.qir_final=CodigoCorrecto();
    			if("1".equals(tramo.qir_final) || "2".equals(tramo.qir_final)){
    				tramo.qicol3=ObtenerCol3SiesEsterilizacion();
    			}
    			tramo.qicantidad=cantidad+1;
    			try {
        		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
    			} catch (Exception e) {
    				// TODO: handle exception
    			}
    	    }
    		else{
    			if(terminaciones!=null && terminaciones.qi226!=null && Util.esDiferente(terminaciones.qi226, 1) && Util.esDiferente(terminaciones.qi310,1) && !nacimientoenfechaactual && !terminacionenfechaactual){
    				tramo.id=terminaciones.id;
        			tramo.hogar_id=terminaciones.hogar_id;
        			tramo.persona_id=terminaciones.persona_id;
        			verificacion_tramo = getCuestionarioService().getTramoparaNingunMetodo(terminaciones.id, terminaciones.hogar_id, terminaciones.persona_id,App.NINGUNMETODO_IDDEFAULT, seccionesCargadoTramos);
        			if(verificacion_tramo!=null){
    	    			EliminacionDeTablas(terminaciones.id,terminaciones.hogar_id,terminaciones.persona_id,verificacion_tramo.qitramo_id);
    	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
    	    			tramoinicial=verificacion_tramo.qitramo_id;
    	    		}
    	    		else{
    	    			tramo.qitramo_id=null;
    	    		}
            		tramo.qimes_ini=mesactual;
            		tramo.qianio_ini=anioactual;
        			tramo.qimes_fin=mesactual;
        			tramo.qianio_fin=anioactual;
        			tramo.ninguno_id=App.NINGUNMETODO_IDDEFAULT;
        			tramo.qir_inicial=App.CALENDARIONINGUNMETODO;
        			tramo.qir_final=App.CALENDARIONINGUNMETODO;
        			tramo.qicantidad=1;
        			try {
            		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
        			} catch (Exception e) {
        				// TODO: handle exception
        			}
    			}
    		}
	    }
	    public String CodigoCorrecto(){
	    	if(terminaciones.qi311_a!=null && !Util.esDiferente(terminaciones.qi311_a,1))
	    		return "1";
	    	if(terminaciones.qi311_b!=null && !Util.esDiferente(terminaciones.qi311_b,1))
	    		return "2";
	    	if(terminaciones.qi311_c!=null && !Util.esDiferente(terminaciones.qi311_c,1))
	    		return "3";
	    	if(terminaciones.qi311_d!=null && !Util.esDiferente(terminaciones.qi311_d,1))
		    	return "4";
	    	if(terminaciones.qi311_e!=null && !Util.esDiferente(terminaciones.qi311_e,1))
		    	return "5";
	    	if(terminaciones.qi311_f!=null && !Util.esDiferente(terminaciones.qi311_f,1))
		    	return "6";
	    	if(terminaciones.qi311_g!=null && !Util.esDiferente(terminaciones.qi311_g,1))
		    	return "7";
	    	if(terminaciones.qi311_h!=null && !Util.esDiferente(terminaciones.qi311_h,1))
		    	return "8";
	    	if(terminaciones.qi311_i!=null && !Util.esDiferente(terminaciones.qi311_i,1))
		    	return "9";
	    	if(terminaciones.qi311_j!=null && !Util.esDiferente(terminaciones.qi311_j,1))
		    	return "J";
	    	if(terminaciones.qi311_k!=null && !Util.esDiferente(terminaciones.qi311_k,1))
		    	return "K";
	    	if(terminaciones.qi311_l!=null && !Util.esDiferente(terminaciones.qi311_l,1))
		    	return "L";
	    	if(terminaciones.qi311_m!=null && !Util.esDiferente(terminaciones.qi311_m,1))
		    	return "M";
	    	if(terminaciones.qi311_x!=null && !Util.esDiferente(terminaciones.qi311_x,1))
		    	return "X";
	    	else
		    	return "";
	    	
	    }
	    public boolean CalendarioCompletado(){
	    	boolean calendariocompleto= false;
	    	if(getCuestionarioService().CalendarioCompletado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,fecha_actual)){
	    		calendariocompleto=true;
	    	}
	    	return calendariocompleto;
	    }

	    public String CodigodeMetodo(String metodo){
	    	String metododescriptivo="";
	    	if(metodo.equals("1"))
	    		metododescriptivo="la ESTERILIZACIÓN FEMENINA";
	    	else if(metodo.equals("2"))
	    		metododescriptivo="la ESTERILIZACIÓN MASCULINA";
	    	else if(metodo.equals("3"))
	    		metododescriptivo="las P�?LDORAS";
	    	else if(metodo.equals("4"))
	    		metododescriptivo="el DIU";
	    	else if(metodo.equals("5"))
	    		metododescriptivo="las INYECCIONES";
	    	else if(metodo.equals("6"))
	    		metododescriptivo="los IMPLANTES";
	    	else if(metodo.equals("7"))
	    		metododescriptivo="el CONDON";
	    	else if(metodo.equals("8"))
	    		metododescriptivo="el CONDON FEMENINO";
	    	else if(metodo.equals("9"))
	    		metododescriptivo="el ESPUMA/JALEA/ÓVULOS (VAGINALES)";
	    	else  if(metodo.equals("J"))
	    		metododescriptivo="la AMENORREA POR LACTANCIA (MELA)";
	    	else if(metodo.equals("K"))
	    		metododescriptivo="ABSTINENCIA PERIÓDICA";
	    	else if(metodo.equals("L"))
	    		metododescriptivo="el RETIRO";
	    	else if(metodo.equals("M"))
	    		metododescriptivo="la ANTICONCEPCIÓN ORAL DE EMERGENCIA";
	    	else if(metodo.equals("X"))
	    		metododescriptivo="OTRO";
	    	
	    	return metododescriptivo;
	    }
	    public void Registrarterminaciones(){
	    	Integer tramoinicial=-1;
	    	CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    	if(terminaciones!=null &&  terminaciones.qi230!=null && !Util.esDiferente(terminaciones.qi230, 1) && terminaciones.qi231_y!=null && terminaciones.qi231_y>=App.ANIOPORDEFECTO){
	    		verificacion_tramo = getCuestionarioService().getTramoparaTerminaciones(terminaciones.id, terminaciones.hogar_id, terminaciones.persona_id,App.TERMINACION_IDDEFAULT, seccionesCargadoTramos);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(terminaciones.id,terminaciones.hogar_id,terminaciones.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
	    	tramo.id=terminaciones.id;
	    	tramo.hogar_id=terminaciones.hogar_id;
	    	tramo.persona_id=terminaciones.persona_id;
	    	Integer indice=MyUtil.DeterminarIndice(terminaciones.qi231_y, Integer.parseInt(terminaciones.qi231_m.toString()))+terminaciones.qi233;
    		indice=indice-1;
    		indice=indice>72?72:indice;
    		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
    		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
    		tramo.qimes_fin=Integer.parseInt(terminaciones.qi231_m.toString());
    		tramo.qianio_fin=terminaciones.qi231_y;
	    	tramo.terminacion_id=App.TERMINACION_IDDEFAULT;	
	    	tramo.qir_inicial= terminaciones.qi233>1?App.CALENDARIOEMBARAZO:App.CALENDARIOTERMINACION;
	    	tramo.qir_final=App.CALENDARIOTERMINACION;
	    	tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)+1;
	    	try {
    		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
			} catch (Exception e) {
				// TODO: handle exception
			}    
	    }
	   }
	    public void TerminacionesSiesqueExistiera(){
	    	Integer tramoinicial=-1;
	    	if(terminaciones!=null && terminaciones.qi234!=null && !Util.esDiferente(terminaciones.qi234,1)){
	    		List<CISECCION_02T> embarazoentermino= getCuestionarioService().getTerminacionesParaCalendario(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, App.ANIOPORDEFECTO, seccionesCargadoTerminaciones);
	    		if(embarazoentermino.size()>0){
	    			for(CISECCION_02T termino:embarazoentermino){
	    				CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
	    	    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    				verificacion_tramo = getCuestionarioService().getTramoparaTerminaciones(termino.id, termino.hogar_id, termino.persona_id,termino.terminacion_id, seccionesCargadoTramos);
	    	    		if(verificacion_tramo!=null){
	    	    			EliminacionDeTablas(termino.id,termino.hogar_id,termino.persona_id,verificacion_tramo.qitramo_id);
	    	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    	    			tramoinicial=verificacion_tramo.qitramo_id;
	    	    		}
	    	    		else{
	    	    			tramo.qitramo_id=null;
	    	    		}
	    				tramo.id=termino.id;
	    		    	tramo.hogar_id=termino.hogar_id;
	    		    	tramo.persona_id=termino.persona_id;
	    		    	Integer indice=MyUtil.DeterminarIndice(termino.qi235_y, termino.qi235_m)+termino.qi235_d;
	    		    	indice=indice-1;
	    	    		indice=indice>72?72:indice;
	    	    		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
	    	    		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
	    	    		tramo.qimes_fin=termino.qi235_m;
	    	    		tramo.qianio_fin=termino.qi235_y;
	    		    	tramo.terminacion_id=termino.terminacion_id;	
	    		    	tramo.qir_inicial= termino.qi235_d>1?App.CALENDARIOEMBARAZO:App.CALENDARIOTERMINACION;
	    		    	tramo.qir_final=App.CALENDARIOTERMINACION;
//	    		    	tramo.qicantidad=termino.qi235_d;
	    		    	tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)+1;
	    		    	try {
	    	    		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
	    				} catch (Exception e) {
	    					// TODO: handle exception
	    				}
	    			}
	    		}
	    	}
	 }
	public void RenombrarEtiquetas(){
	    	
	}
	    public void RegistrarNacimientosPrimeroenTramos(){
	    	Integer tramoinicial=-1;
	    	boolean multiple=true;
	    	Calendar fechadenacimientoanterior= null;
	    	Calendar fechadenacimientoactual = null;
	    	Integer id=null;
	    	if(ninios.size()>0){
	    	fechadenacimientoanterior= new GregorianCalendar(ninios.get(0).qi215y,Integer.parseInt(ninios.get(0).qi215m)-1,Integer.parseInt(ninios.get(0).qi215d));
	    	id=ninios.get(0).qi212;
	    	for(CISECCION_02 ninio:ninios){
	    		CICALENDARIO_TRAMO_COL01_03 tramo= new CICALENDARIO_TRAMO_COL01_03();
	    		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo= new CICALENDARIO_TRAMO_COL01_03();
	    		fechadenacimientoactual = new GregorianCalendar(ninio.qi215y,Integer.parseInt(ninio.qi215m.toString())-1,Integer.parseInt(ninio.qi215d.toString()));
	    		if(fechadenacimientoanterior.compareTo(fechadenacimientoactual)==0 && id!=ninio.qi212){
	    			fechadenacimientoanterior=fechadenacimientoactual;
	    			continue;
	    		}
	    		else{
	    			fechadenacimientoanterior=fechadenacimientoactual;
	    		}
	    		tramo.id=ninio.id;
	    		verificacion_tramo = getCuestionarioService().getTramoparaNacimientos(ninio.id, ninio.hogar_id, ninio.persona_id,ninio.qi212, seccionesCargadoTramos);
	    		if(verificacion_tramo!=null){
	    			EliminacionDeTablas(ninio.id,ninio.hogar_id,ninio.persona_id,verificacion_tramo.qitramo_id);
	    			tramo.qitramo_id=verificacion_tramo.qitramo_id;
	    			tramoinicial=verificacion_tramo.qitramo_id;
	    		}
	    		else{
	    			tramo.qitramo_id=null;
	    		}
	    		tramo.hogar_id=ninio.hogar_id;
	    		tramo.persona_id=ninio.persona_id;
	    		tramo.ninio_id=ninio.qi212;
	    		tramo.terminacion_id=null;
	    		Integer indice=MyUtil.DeterminarIndice(ninio.qi215y, Integer.parseInt(ninio.qi215m.toString()))+ninio.qi220a;
	    		indice=indice-1;
	    		indice=indice>72?72:indice;
	    		tramo.qimes_ini=MyUtil.DeterminarMesInicial(indice);
	    		tramo.qianio_ini=MyUtil.DeterminarAnioInicial(indice);
	    		tramo.qimes_fin=Integer.parseInt(ninio.qi215m.toString());
	    		tramo.qianio_fin=ninio.qi215y;
	    		tramo.qir_inicial=App.CALENDARIOEMBARAZO;
	    		tramo.qir_final=App.CALENDARIONACIMIENTO;
	    		tramo.qicantidad=MyUtil.DeterminarIndice(tramo.qianio_ini, tramo.qimes_ini)-MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)+1;
	    		try {
	    		 	tramoinicial=getCuestionarioService().saveOrUpdate(tramo, null,seccionesGrabadoTramos);
				} catch (Exception e) {
					// TODO: handle exception
				}
	    	}
	      }
	    }
	    
	    @Override
		public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
			super.onCreateContextMenu(menu, v, menuInfo);
			if (v.equals(tctramos.getListView())) {
				menu.setHeaderTitle("Opciones del Calendario");
				menu.add(1, 0, 1, "Editar tramo");
				menu.add(1, 1, 1, "Eliminar tramo");
				AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
				CICALENDARIO_TRAMO_COL01_03 seleccion = (CICALENDARIO_TRAMO_COL01_03) info.targetView.getTag();
//				if((seleccion.qir_final.equals("N") || seleccion.qir_final.equals("T") ||  seleccion.qir_final.equals("E") || (!Util.esDiferente(seleccion.qimes_fin , fecha_actual.get(Calendar.MONTH)+1) && !Util.esDiferente(seleccion.qianio_fin, fecha_actual.get(Calendar.YEAR)) || seleccion.qir_final.equals("N") || seleccion.qir_final.equals("T") ||  seleccion.qir_final.equals("E") && seleccion.qir_final.equals("0")))){
				if((seleccion.qir_final.equals("N") || seleccion.qir_final.equals("T") ||  seleccion.qir_final.equals("E") || (seleccion.qir_final.equals("N") || seleccion.qir_final.equals("T") ||  seleccion.qir_final.equals("E") && seleccion.qir_final.equals("0")))){
					menu.getItem(0).setVisible(false);
				}
				else{
					menu.getItem(0).setVisible(true);
				}
				Integer codigo=App.getInstance().getUsuario().cargo_id;
		    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
		    		menu.getItem(1).setVisible(false);
		    	}
			}
		}

		@Override
		public boolean onContextItemSelected(MenuItem item) {
			if (!getUserVisibleHint())
				return false;
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			CICALENDARIO_TRAMO_COL01_03 seleccion = (CICALENDARIO_TRAMO_COL01_03) info.targetView.getTag();
			Integer cantidad=0;
			Integer posicionfinal=0;
			Integer posicioninicial=0;
			if (item.getGroupId() == 1) {
				switch (item.getItemId()) {
				case 0:
					cantidad = detalles.size();
//					Log.e("","cantidad: "+cantidad);
					posicionfinal = info.position+1;
					if(!Util.esDiferente(posicionfinal, cantidad)){
						posicionfinal=info.position;
					}
					if(!Util.esDiferente(info.position, 0,0)){
						posicioninicial=0;
					}
					else{
						posicioninicial=info.position-1;
					}
//					Log.e("","P.I. "+posicioninicial);
//					Log.e("","P.F. "+posicionfinal);
//					Log.e("","P.A. "+info.position);
					abrirDetalle((CICALENDARIO_TRAMO_COL01_03) detalles.get(posicioninicial),(CICALENDARIO_TRAMO_COL01_03) detalles.get(info.position),(CICALENDARIO_TRAMO_COL01_03) detalles.get(posicionfinal));
					break;
				case 1:
					action=ACTION.ELIMINAR;
					dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro que desea eliminar?");
					dialog.put("seleccion", (CICALENDARIO_TRAMO_COL01_03) detalles.get(info.position));
					dialog.showDialog();
					break;
				}
			}
			return super.onContextItemSelected(item);
		}
		
		@Override
		public Integer grabadoParcial() {
			uiToEntity(terminaciones);
			try {
				if(!getCuestionarioService().saveOrUpdate(terminaciones,seccionesGrabado01_03)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return App.NODEFINIDO;
				}
			} catch (SQLException e) {
				ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
	    	App.getInstance().getPersonaCuestionarioIndividual().qi320=terminaciones.qi320;
	    	App.getInstance().getPersonaCuestionarioIndividual().qi316y=terminaciones.qi316y;
	    	App.getInstance().getPersonaCuestionarioIndividual().qi315y=terminaciones.qi315y;
			return App.INDIVIDUAL;
		}
		
		   public CuestionarioService getCuestionarioService() {
				if(cuestionarioService==null){
					cuestionarioService = CuestionarioService.getInstance(getActivity());
				}
				return cuestionarioService;
		    }
		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onAccept() {
			if (action == ACTION.MENSAJE) {
				return;
			}
			if (dialog == null) {
				return;
			}
			CICALENDARIO_TRAMO_COL01_03 bean = (CICALENDARIO_TRAMO_COL01_03) dialog.get("seleccion");
			try {
				detalles.remove(bean);
				boolean borrado = false;
				borrado=getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, bean.id, bean.hogar_id, bean.persona_id, bean.qitramo_id);
				if(borrado && !CalendarioCompletado()){
					btnIniciar.setEnabled(true);
				}
				else{
					btnIniciar.setEnabled(false);
				}
				tramo1=null;
				tramo2=null;
				if (!borrado) {
					throw new SQLException("Tramo no pudo ser borrada.");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
			cargarTabla();
		}
		public void EliminacionDeTablas(Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id){
			getCuestionarioService().Deletetramo(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, id, hogar_id, persona_id, tramo_id);
		}
		
		
}
