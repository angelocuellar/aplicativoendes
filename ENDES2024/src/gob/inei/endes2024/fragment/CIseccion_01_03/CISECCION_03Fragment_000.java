package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_000 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI302_01;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI302_02;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI302_03;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI302_04;
//	@FieldAnnotation(orderIndex=5)
//	public RadioGroupOtherField rgQI302_05;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI302_05a;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI302_05b;
	@FieldAnnotation(orderIndex=7)
	public RadioGroupOtherField rgQI302_06;
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI302_07;
	@FieldAnnotation(orderIndex=9)
	public RadioGroupOtherField rgQI302_08;
	@FieldAnnotation(orderIndex=10)
	public RadioGroupOtherField rgQI302_09;
	@FieldAnnotation(orderIndex=11)
	public RadioGroupOtherField rgQI302_10;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI302_11;
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI302_12;
	@FieldAnnotation(orderIndex=14)
	public RadioGroupOtherField rgQI302_13;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI302_14;
	@FieldAnnotation(orderIndex=16)
	public TextField txtQI302O;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta300,lblpregunta302,lblpregunta302_01,lblpregunta302_02,lblpregunta302_03,lblpregunta302_04,lblpregunta302_05,lblpregunta302_06;
	private LabelComponent lblpregunta302_07,lblpregunta302_08,lblpregunta302_09,lblpregunta302_10,lblpregunta302_11,lblpregunta302_12,lblpregunta302_13,lblpregunta302_14;
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,q15,q16;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	private LabelComponent lblpregunta305_a,lblpregunta305_b;
	private GridComponent2 gridpregunta305;

	public CISECCION_03Fragment_000() {}
	public CISECCION_03Fragment_000 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
  }
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_01","QI302_02","QI302_03","QI302_04","QI302_05A","QI302_05B","QI302_06","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI302O","QI237","QI238","QI201","QI226","QI230","QI206","QI106","QICAL_CONS","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI302_01","QI302_02","QI302_03","QI302_04","QI302_05A","QI302_05B","QI302_06","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI302O","QICAL_CONS")};
		return rootView;
  }
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta300 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi300);
		lblpregunta302 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302);
		lblpregunta302_01 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_01);
		lblpregunta302_02 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_02);
		lblpregunta302_03 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_03);
		lblpregunta302_04 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_04);
		lblpregunta302_05 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_05);
		lblpregunta302_06 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_06);
		lblpregunta302_07 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_07);
		lblpregunta302_08 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_08);
		lblpregunta302_09 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_09);
		lblpregunta302_10 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_10);
		lblpregunta302_11 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_11);
		lblpregunta302_12 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_12);
		lblpregunta302_13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi302_13);
		lblpregunta302_14 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03q302_14);
		lblpregunta305_a = new LabelComponent(this.getActivity()).size(altoComponente+40, 540).textSize(19).text(R.string.ciseccion_03qi302_05a);
		lblpregunta305_b = new LabelComponent(this.getActivity()).size(altoComponente+40, 540).textSize(19).text(R.string.ciseccion_03qi302_05b);
		rgQI302_01=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_01_1,R.string.ciseccion_03qi302_01_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_02=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_02_1,R.string.ciseccion_03qi302_02_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_03=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_03_1,R.string.ciseccion_03qi302_03_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_04=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_04_1,R.string.ciseccion_03qi302_04_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		rgQI302_05=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_05_1,R.string.ciseccion_03qi302_05_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		gridpregunta305= new GridComponent2()
	
		rgQI302_05a=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_05_1,R.string.ciseccion_03qi302_05_2).size(altoComponente+40,200).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI302_05b=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_05_1,R.string.ciseccion_03qi302_05_2).size(altoComponente+40,200).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI302_06=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_06_1,R.string.ciseccion_03qi302_06_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_07=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_07_1,R.string.ciseccion_03qi302_07_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_08=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_08_1,R.string.ciseccion_03qi302_08_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_09=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_09_1,R.string.ciseccion_03qi302_09_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_10=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_10_1,R.string.ciseccion_03qi302_10_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_11=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_11_1,R.string.ciseccion_03qi302_11_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS302_11ChangeValue");
		rgQI302_12=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_12_1,R.string.ciseccion_03qi302_12_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_13=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi302_13_1,R.string.ciseccion_03qi302_13_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI302_14=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03q302_14_1,R.string.ciseccion_03q302_14_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI302O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400);
		rgQI302_14.agregarEspecifique(0,txtQI302O);
		
		gridpregunta305 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta305.addComponent(lblpregunta305_a);
		gridpregunta305.addComponent(rgQI302_05a);
		gridpregunta305.addComponent(lblpregunta305_b);
		gridpregunta305.addComponent(rgQI302_05b);
    }
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta300);
		q2 = createQuestionSection(lblpregunta302);
		q3 = createQuestionSection(lblpregunta302_01,rgQI302_01);
		q4 = createQuestionSection(lblpregunta302_02,rgQI302_02);
		q5 = createQuestionSection(lblpregunta302_03,rgQI302_03);
		q6 = createQuestionSection(lblpregunta302_04,rgQI302_04);
		q7 = createQuestionSection(lblpregunta302_05,gridpregunta305.component());
		q8 = createQuestionSection(lblpregunta302_06,rgQI302_06);
		q9 = createQuestionSection(lblpregunta302_07,rgQI302_07);
		q10 = createQuestionSection(lblpregunta302_08,rgQI302_08);
		q11 = createQuestionSection(lblpregunta302_09,rgQI302_09);
		q12 = createQuestionSection(lblpregunta302_10,rgQI302_10);
		q13 = createQuestionSection(lblpregunta302_11,rgQI302_11);
		q14 = createQuestionSection(lblpregunta302_12,rgQI302_12);
		q15 = createQuestionSection(lblpregunta302_13,rgQI302_13);
		q16 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta302_14,rgQI302_14);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		form.addView(q11);
		form.addView(q12);
		form.addView(q13);
		form.addView(q14);
		form.addView(q15);
		form.addView(q16);
 
    return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		/*Integer int226=individual.qi226==null?0:individual.qi226;
    	if (int226==1 && individual.qi302_01==null)
    		individual.qi302_01=2;*/
			
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(individual.qical_cons==null){
				individual.qical_cons=Util.getFechaActualToString();
			}
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi302_01 = individual.qi302_01;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_02 = individual.qi302_02;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_03 = individual.qi302_03;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_04 = individual.qi302_04;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_05a = individual.qi302_05a;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_05b = individual.qi302_05b;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_06 = individual.qi302_06;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_07 = individual.qi302_07;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_08 = individual.qi302_08;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_09 = individual.qi302_09;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_10 = individual.qi302_10;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_11 = individual.qi302_11;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_12 = individual.qi302_12;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_13 = individual.qi302_13;
			App.getInstance().getPersonaCuestionarioIndividual().qi302_14 = individual.qi302_14;
			App.getInstance().getPersonaCuestionarioIndividual().qi302o = individual.qi302o;
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//       	Integer int201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
//       	Integer int206 = App.getInstance().getPersonaCuestionarioIndividual().qi206==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi206;
//       	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;	           	
//       	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
//       	Log.e("int106 s", " " + int106);
//       	Log.e("int201 s", " " + int201);
//       	Log.e("int206 s", " " + int206);
//       	Log.e("int226 s", " " + int226);
//       	Log.e("int230 s", " " + int230);
       	
//     	if (!(int106>=12 && int106<=14  && int201==2 && int206==2 && int226==2 &&int230==2)) {
     		
     		if (Util.esVacio(individual.qi302_01)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_01");
    			view = rgQI302_01;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_02)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_02");
    			view = rgQI302_02;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_03)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_03");
    			view = rgQI302_03;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_04)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_04");
    			view = rgQI302_04;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_05a)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_05");
    			view = rgQI302_05a;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_05b)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_05");
    			view = rgQI302_05b;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_06)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_06");
    			view = rgQI302_06;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_07)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_07");
    			view = rgQI302_07;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_08)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_08");
    			view = rgQI302_08;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_09)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_09");
    			view = rgQI302_09;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_10)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_10");
    			view = rgQI302_10;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_11)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_11");
    			view = rgQI302_11;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_12)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_12");
    			view = rgQI302_12;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_13)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI302_13");
    			view = rgQI302_13;
    			error = true;
    			return false;
    		}
    		if (Util.esVacio(individual.qi302_14)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta Q302_14");
    			view = rgQI302_14;
    			error = true;
    			return false;
    		}
    		if(individual.qi302_14==1){
    			if (Util.esVacio(individual.qi302o)) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI302O;
    				error = true;
    				return false;
    			}
    		}
    		
    		if ( individual.qi237!=null && Util.esDiferente(individual.qi237,1) && !Util.esDiferente(individual.qi302_11,1) ) {
    			validarMensaje("Verifique la pregunta QI237, Respondi� No o No sabe");
    		}
    		if ( individual.qi238!=null && individual.qi237!=null && !Util.esDiferente(individual.qi237,1) && !Util.esDiferente(individual.qi238,7) && !Util.esDiferente(individual.qi302_11,1) ) {
    			validarMensaje("Verifique la pregunta QI238");
    		}
//     	}
		
		
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		entityToUI(individual);
		inicio();
    }
    
    
    private void inicio() {
    	ValidarMef_12a14();
    	ValidarsiesSupervisora();
    	RenombrarTexto();
    	txtCabecera.requestFocus();
    }
    private void RenombrarTexto(){
    	lblpregunta305_a.setText(R.string.ciseccion_03qi302_05a);
    	lblpregunta305_b.setText(R.string.ciseccion_03qi302_05b);
    	lblpregunta305_a.setText(Html.fromHtml("Algunas mujeres se hacen aplicar una inyección <b>cada mes</b> para evitar quedar embarazadas."));
    	lblpregunta305_b.setText(Html.fromHtml("Algunas mujeres se hacen aplicar una inyección <b>cada 3 meses</b> para evitar quedar embarazadas."));
    }
    
    public void ValidarMef_12a14() {
//	    Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//	   	Integer int201 = App.getInstance().getPersonaCuestionarioIndividual().qi201==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi201;
//	   	Integer int206 = App.getInstance().getPersonaCuestionarioIndividual().qi206==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi206;
	   	Integer int226 = App.getInstance().getPersonaCuestionarioIndividual().qi226==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi226;	           	
//	   	Integer int230 = App.getInstance().getPersonaCuestionarioIndividual().qi230==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi230;
//	   	
////	   	Log.e("int106 s", " " + int106);
////	   	Log.e("int201 s", " " + int201);
////	   	Log.e("int206 s", " " + int206);
////	   	Log.e("int226 s", " " + int226);
////	   	Log.e("int230 s", " " + int230);
//	   	
//	 	if (int106>=12 && int106<=14  && int201==2 && int206==2 && int226==2 &&int230==2) {
////	 		Log.e("aaaaaaaaa", " aaaaaaaaa" );
//	 		Util.cleanAndLockView(getActivity(),rgQI302_01, rgQI302_02, rgQI302_03, rgQI302_04, rgQI302_05a, rgQI302_05b, rgQI302_06, rgQI302_07, rgQI302_09, rgQI302_10, rgQI302_11, rgQI302_12, rgQI302_13, rgQI302_14);
//	 		q0.setVisibility(View.GONE);
//	 		q1.setVisibility(View.GONE);
//	 		q2.setVisibility(View.GONE);
//	 		q3.setVisibility(View.GONE);
//	 		q4.setVisibility(View.GONE);
//	 		q5.setVisibility(View.GONE);
//	 		q6.setVisibility(View.GONE);
//	 		q7.setVisibility(View.GONE);
//	 		q8.setVisibility(View.GONE);
//	 		q9.setVisibility(View.GONE);
//	 		q10.setVisibility(View.GONE);
//	 		q11.setVisibility(View.GONE);
//	 		q12.setVisibility(View.GONE);
//	 		q13.setVisibility(View.GONE);
//	 		q14.setVisibility(View.GONE);
//	 		q15.setVisibility(View.GONE);
//	 		q16.setVisibility(View.GONE);
//	 		
//	 	}
//	 	else{
////	 		Log.e("bbbbbbb", " bbbbbbb" );
//	 		Util.lockView(getActivity(),false,rgQI302_01, rgQI302_02, rgQI302_03, rgQI302_04, rgQI302_05a, rgQI302_05b, rgQI302_06, rgQI302_07, rgQI302_09, rgQI302_10, rgQI302_11, rgQI302_12, rgQI302_13, rgQI302_14);
//	 		q0.setVisibility(View.VISIBLE);
//	 		q1.setVisibility(View.VISIBLE);
//	 		q2.setVisibility(View.VISIBLE);
//	 		q3.setVisibility(View.VISIBLE);
//	 		q4.setVisibility(View.VISIBLE);
//	 		q5.setVisibility(View.VISIBLE);
//	 		q6.setVisibility(View.VISIBLE);
//	 		q7.setVisibility(View.VISIBLE);
//	 		q8.setVisibility(View.VISIBLE);
//	 		q9.setVisibility(View.VISIBLE);
//	 		q10.setVisibility(View.VISIBLE);
//	 		q11.setVisibility(View.VISIBLE);
//	 		q12.setVisibility(View.VISIBLE);
//	 		q13.setVisibility(View.VISIBLE);
//	 		q14.setVisibility(View.VISIBLE);
//	 		q15.setVisibility(View.VISIBLE);
//	 		q16.setVisibility(View.VISIBLE);
	 		
//	 		Integer int226=individual.qi226==null?0:individual.qi226;
	    	if (int226==1) {
	    		Util.lockView(getActivity(), false,rgQI302_01);
	    		rgQI302_01.lockButtons(true,0);
			}else {
				Util.lockView(getActivity(), false,rgQI302_01);
			}
	    	onqrgQS302_11ChangeValue();
//	 	}
	 	
    }
    
	public void onqrgQS302_11ChangeValue() {	
		if ( individual.qi237!=null && Util.esDiferente(individual.qi237,1) && MyUtil.incluyeRango(1,1,rgQI302_11.getTagSelected("").toString())) {
			ToastMessage.msgBox(this.getActivity(), "Verifique la pregunta QI237, Respondió No o No sabe", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			
		}
		if ( individual.qi238!=null && individual.qi237!=null && !Util.esDiferente(individual.qi237,1) && !Util.esDiferente(individual.qi238,7) && MyUtil.incluyeRango(1,1,rgQI302_11.getTagSelected("").toString()) ) {
			ToastMessage.msgBox(this.getActivity(), "Verifique la pregunta QI238", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
		}
		rgQI302_12.requestFocus();
	}
	
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI302_01.readOnly();
    		rgQI302_02.readOnly();
    		rgQI302_03.readOnly();
    		rgQI302_04.readOnly();
    		rgQI302_05a.readOnly();
    		rgQI302_05b.readOnly();
    		rgQI302_06.readOnly();
    		rgQI302_07.readOnly();
    		rgQI302_08.readOnly();
    		rgQI302_09.readOnly();
    		rgQI302_10.readOnly();
    		rgQI302_11.readOnly();
    		rgQI302_12.readOnly();
    		rgQI302_13.readOnly();
    		rgQI302_14.readOnly();
    		txtQI302O.readOnly();
    	}
    }
    
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		/*Integer int226=individual.qi226==null?0:individual.qi226;
    	if (int226==1 && individual.qi302_01==null)
    		individual.qi302_01=2;*/
	
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi302_01 = individual.qi302_01;			
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return App.INDIVIDUAL;
	}
}