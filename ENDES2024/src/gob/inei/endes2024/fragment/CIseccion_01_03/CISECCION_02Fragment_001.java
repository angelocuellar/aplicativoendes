package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_02Fragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI201;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI202;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI203_A;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI203_B;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI204;
	@FieldAnnotation(orderIndex=6)
	public IntegerField txtQI205_A;
	@FieldAnnotation(orderIndex=7)
	public IntegerField txtQI205_B;
	
	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private Seccion01Service personaService;
	public GridComponent2 gdHijosViven,gdHijosNoViven;
	private LabelComponent lblTitulo,lblpregunta201,lblpregunta202,lblpregunta203,lblpregunta203_Ind,lblpregunta204,lblpregunta205,lblpregunta205_Ind,lblhijasviven,lblhijosviven,lblhijasnoviven,lblhijosnoviven;
	LinearLayout q0,q1,q2,q3,q4,q5;

	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_02Fragment_001() {}
	public CISECCION_02Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI203_A, 0, 20, null, 99);
		rango(getActivity(), txtQI203_B, 0, 20, null, 99);
		rango(getActivity(), txtQI205_A, 0, 20, null, 99);
		rango(getActivity(), txtQI205_B, 0, 20, null, 99);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI201","QI202","QI203_A","QI203_B","QI204","QI205_A","QI205_B","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI201","QI202","QI203_A","QI203_B","QI204","QI205_A","QI205_B")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta201 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi201);
		lblpregunta202 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi202);
		lblpregunta203 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi203_a);
		Spanned texto203 =Html.fromHtml("<b>SI DIJO NINGUNO, ANOTE \"0\"</b>");
		lblpregunta203_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpregunta203_Ind.setText(texto203);
		lblpregunta204 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi204);
		Spanned texto205 =Html.fromHtml("<b>SI DIJO NINGUNO, ANOTE \"0\"</b>");
		lblpregunta205_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpregunta205_Ind.setText(texto205);
		lblpregunta205 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi205_a);
		rgQI201=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi201_1,R.string.ciseccion_02qi201_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI201ChangeValue");
		rgQI202=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi202_1,R.string.ciseccion_02qi202_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI202ChangeValue");
		txtQI203_A = new IntegerField(this.getActivity()).size(altoComponente,100).maxLength(2);
		txtQI203_B = new IntegerField(this.getActivity()).size(altoComponente,100).maxLength(2);
		lblhijasviven  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_02qi203_a_1).textSize(19).centrar();
		lblhijosviven  = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.ciseccion_02qi203_a_2).textSize(19).centrar();
		
		rgQI204=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi204_1,R.string.ciseccion_02qi204_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI204ChangeValue");
		txtQI205_A=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQI205_B=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		lblhijasnoviven  = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.ciseccion_02qi205_a_1).textSize(19).centrar();
		lblhijosnoviven  = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.ciseccion_02qi205_a_2).textSize(19).centrar();
		
		gdHijosViven = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdHijosViven.addComponent(lblhijasviven);
		gdHijosViven.addComponent(txtQI203_A);
		gdHijosViven.addComponent(lblhijosviven);
		gdHijosViven.addComponent(txtQI203_B);
		
		gdHijosNoViven = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdHijosNoViven.addComponent(lblhijasnoviven);
		gdHijosNoViven.addComponent(txtQI205_A);
		gdHijosNoViven.addComponent(lblhijosnoviven);
		gdHijosNoViven.addComponent(txtQI205_B);		
    }
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta201,rgQI201);
		q2 = createQuestionSection(lblpregunta202,rgQI202);
		q3 = createQuestionSection(lblpregunta203,gdHijosViven.component(),lblpregunta203_Ind);
		q4 = createQuestionSection(lblpregunta204,rgQI204);
		q5 = createQuestionSection(lblpregunta205,gdHijosNoViven.component(),lblpregunta205_Ind);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
    return contenedor;
    }
    @Override
    public boolean grabar() {

		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi201 = individual.qi201;			
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (Util.esVacio(individual.qi201)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QI201");
			view = rgQI201;
			error = true;
			return false;
		}
		if (Util.esDiferente(individual.qi201,2)) {
			if (Util.esVacio(individual.qi202)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI202");
				view = rgQI202;
				error = true;
				return false;
			}
		}
		if (Util.esDiferente(individual.qi201,2) && Util.esDiferente(individual.qi202,2)) {
			if (Util.esVacio(individual.qi203_a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI203_A");
				view = txtQI203_A;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi203_b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI203_B");
				view = txtQI203_B;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi203_b,individual.qi203_a) && !Util.esDiferente(individual.qi203_a,0)){
				mensaje = "Ambos valores no pueden ser cero";
				view = txtQI203_B;
				error = true;
				return false;
			}
		}
		if (Util.esDiferente(individual.qi201,2)) {
			if (Util.esVacio(individual.qi204)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI204");
				view = rgQI204;
				error = true;
				return false;
			}
		}
		if (Util.esDiferente(individual.qi201,2) && Util.esDiferente(individual.qi204,2)) {
			if (Util.esVacio(individual.qi205_a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI205_A");
				view = txtQI205_A;
				error = true;
				return false;
			}
			if (Util.esVacio(individual.qi205_b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta QI205_B");
				view = txtQI205_B;
				error = true;
				return false;
			}
			if(!Util.esDiferente(individual.qi205_b,individual.qi205_a) && !Util.esDiferente(individual.qi205_a,0)){
				mensaje = "Ambos valores no pueden ser cero";
				view = txtQI205_B;
				error = true;
				return false;
			}
		}
		Integer totalhijosH=getPersonaService().getTotalDehijosDelaMef(individual.id, individual.hogar_id,individual.persona_id);
		Integer totalhijasM=getPersonaService().getTotalDehijasDelaMef(individual.id, individual.hogar_id,individual.persona_id);
		Integer total=totalhijosH+totalhijasM;
		if(Util.esDiferente(total, 0) && !Util.esDiferente(individual.qi201, 2)){
			mensaje = "Existen hijos(as) en el C. Hogar";
			view = rgQI201;
			error = true;
			return false;
		}
		if(Util.esDiferente(total, 0) && !Util.esDiferente(individual.qi202, 2)){
			mensaje = "Existen hijos(as) que viven con la Mef en el C. Hogar";
			view = rgQI202;
			error = true;
			return false;
		}
		if(Util.esDiferente(totalhijasM, 0) && Util.esMenor(individual.qi203_a, totalhijasM)){
			mensaje = "En el C. Hogar se registr� que tiene: "+totalhijasM+ " hijas";
			view = txtQI203_A;
			error = true;
			return false;
		}
		if(Util.esDiferente(totalhijosH, 0) && Util.esMenor(individual.qi203_b, totalhijosH)){
			mensaje = "En el C. Hogar se registr� que tiene: "+totalhijosH+ " hijos";
			view = txtQI203_B;
			error = true;
			return false;
		}
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; //1266;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;//1;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;//1;
		}
		entityToUI(individual);
		inicio();
    }
    
    private void inicio() {
    	validarPregunta202();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    public void validarPregunta202(){
    	if (MyUtil.incluyeRango(2,2,rgQI201.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI202,txtQI203_A,txtQI203_B,rgQI204,txtQI205_A,txtQI205_B);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			
		} else {
			if (MyUtil.incluyeRango(2,2,rgQI202.getTagSelected("").toString())) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),txtQI203_A,txtQI203_B);
				q3.setVisibility(View.GONE);
				if (MyUtil.incluyeRango(2,2,rgQI204.getTagSelected("").toString())) {
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),txtQI205_A,txtQI205_B);
					q5.setVisibility(View.GONE);
				} else {
					Util.lockView(getActivity(), false,txtQI205_A,txtQI205_B);
					q5.setVisibility(View.VISIBLE);
				}
			} else {
				if (MyUtil.incluyeRango(2,2,rgQI204.getTagSelected("").toString())) {
					MyUtil.LiberarMemoria();
					Util.cleanAndLockView(getActivity(),txtQI205_A,txtQI205_B);
					q5.setVisibility(View.GONE);
				} else {
					Util.lockView(getActivity(), false,txtQI205_A,txtQI205_B);
					q5.setVisibility(View.VISIBLE);
				}
			}
		}
    }
    public void onrgQI201ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI201.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI202,txtQI203_A,txtQI203_B,rgQI204,txtQI205_A,txtQI205_B);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			
		} else {
			Util.lockView(getActivity(), false,rgQI202,txtQI203_A,txtQI203_B,rgQI204,txtQI205_A,txtQI205_B);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			rgQI202.requestFocus();
		}
    }
    
    public void onrgQI202ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI202.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),txtQI203_A,txtQI203_B);
			q3.setVisibility(View.GONE);
			rgQI204.requestFocus();
		} else {
			Util.lockView(getActivity(), false,txtQI203_A,txtQI203_B);
			q3.setVisibility(View.VISIBLE);
			txtQI203_A.requestFocus();
		}
    }
    
    public void onrgQI204ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQI204.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),txtQI205_A,txtQI205_B);
			q5.setVisibility(View.GONE);
		} else {		
			Util.lockView(getActivity(), false,txtQI205_A,txtQI205_B);
			q5.setVisibility(View.VISIBLE);
			txtQI205_A.requestFocus();
		}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI201.readOnly();
    		rgQI202.readOnly();
    		rgQI204.readOnly();
    		txtQI203_A.readOnly();
    		txtQI203_B.readOnly();
    		txtQI205_A.readOnly();
    		txtQI205_B.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
	
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) {
			App.getInstance().getPersonaCuestionarioIndividual().qi201 = individual.qi201;			
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return App.INDIVIDUAL;
	}
}