package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_005 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI322;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI323;
	@FieldAnnotation(orderIndex=3)
	public RadioGroupOtherField rgQI324;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQI324A;
	@FieldAnnotation(orderIndex=5)
	public RadioGroupOtherField rgQI325;
	@FieldAnnotation(orderIndex=6)
	public RadioGroupOtherField rgQI325A;
	@FieldAnnotation(orderIndex=7)
	public TextField txtQI325A_O;

	public TextField txtCabecera;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta322,lblpregunta323,lblpregunta324,lblpregunta324A1,lblpregunta324A2,lblpregunta325,lblpregunta325A;
	LinearLayout q0,q1,q2,q3,q4,q5,q6;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_03Fragment_005() {}
	public CISECCION_03Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226","QI230","QI304","QI322","QI323","QI324","QI324A","QI325","QI325A","QI325A_O","QI320","QI320","QI320_O","QI316Y","QI316M","QI315Y","QI315M","QI312","QI312_O","QI321A","QI321A_O","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI322","QI323","QI324","QI324A","QI325","QI325A","QI325A_O","QI326")};
		return rootView;
	}
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta322 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
//	  	Spanned texto=Html.fromHtml("322. A Ud. <b>le prescribieron</b> (M�TODO ACTUAL EN 320) de (FUENTE DE M�TODO EN 312/321A) en (FECHA EN 321). �En ese momento le dijeron a Ud. de los efectos secundarios o problemas que Ud. podr�a tener con este m�todo?");
//    	lblpregunta322.setText(texto); 
		lblpregunta323 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi323);
		lblpregunta324 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi324);
		lblpregunta324A1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi324a1);
		lblpregunta324A2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi324a2);
		lblpregunta325 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325);
		lblpregunta325A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi325a);
		
		rgQI322=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi322_1,R.string.ciseccion_03qi322_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI322ChangeValue");
		rgQI323=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi323_1,R.string.ciseccion_03qi323_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI323ChangeValue");
		rgQI324=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi324_1,R.string.ciseccion_03qi324_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI324A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi324a_1,R.string.ciseccion_03qi324a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI324AChangeValue");
		rgQI325=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi325_1,R.string.ciseccion_03qi325_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI325A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi325a_1,R.string.ciseccion_03qi325a_2,R.string.ciseccion_03qi325a_3,R.string.ciseccion_03qi325a_4,R.string.ciseccion_03qi325a_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI325A_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI325A.agregarEspecifique(4,txtQI325A_O);
    }
    @Override
    protected View createUI() {
		buildFields();
 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta322,rgQI322);
		q2 = createQuestionSection(lblpregunta323,rgQI323);
		q3 = createQuestionSection(lblpregunta324,rgQI324);
		q4 = createQuestionSection(lblpregunta324A1,lblpregunta324A2,rgQI324A);
		q5 = createQuestionSection(lblpregunta325,rgQI325);
		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta325A,rgQI325A);
 
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
 
    return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi325a!=null)
			individual.qi325a=individual.getConvertQi325a(individual.qi325a);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			individual.qi326=CalcularPregunta326();
//			Log.d("pre326", "preg326-  "+individual.qi326);
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi325a = individual.qi325a;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in322=individual.qi322==null?0:individual.qi322;
		Integer in323=individual.qi323==null?0:individual.qi323;
		Integer in324a=individual.qi324a==null?0:individual.qi324a;
		Integer int304 = individual.qi304==null?0:individual.qi304;
    	if (int304!=2) {
    		if (Util.esVacio(individual.qi322)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI322");
    			view = rgQI322;
    			error = true;
    			return false;
    		}
    		if (individual.qi322!=1) {
    			if (Util.esVacio(individual.qi323)) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI323");
    				view = rgQI323;
    				error = true;
    				return false;
    			}
    		}
    		if (in322==1 || in323==1) {
    			if (rgQI324.getValue()==null) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI324");
    				view = rgQI324;
    				error = true;
    				return false;
    			}
    		}
    		if (Util.esVacio(individual.qi324a)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI324A");
    			view = rgQI324A;
    			error = true;
    			return false;
    		}
    		if (in324a!=1) {
    			if (Util.esVacio(individual.qi325)) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI325");
    				view = rgQI325;
    				error = true;
    				return false;
    			}
    		}
    		if (Util.esVacio(individual.qi325a)) {
    			mensaje = preguntaVacia.replace("$", "La pregunta QI325A");
    			view = rgQI325A;
    			error = true;
    			return false;
    		}
    		if(individual.qi325a==6){
    			if (Util.esVacio(individual.qi325a_o)) {
    				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
    				view = txtQI325A_O;
    				error = true;
    				return false;
    			}
    		}
    	}
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi325a!=null)
			individual.qi325a=individual.setConvertQi325a(individual.qi325a);
		entityToUI(individual);
		inicio();
    }
    public void validarPregunta304(){
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	if (int304==2) {
    		Util.cleanAndLockView(getActivity(),rgQI322,rgQI323,rgQI324,rgQI324A,rgQI325,rgQI325A,txtQI325A_O);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		}else {
			onrgQI322ChangeValue();
	    	onrgQI324AChangeValue();
		}
    }
    
    private void inicio() {
    	validarPregunta304();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void RenombrarEtiquetas(){    	
    	Spanned texto=Html.fromHtml("(M�TODO ACTUAL EN 320) de (FUENTE DE M�TODO EN 312/321A) en (FECHA EN 321). �En ese momento le dijeron a Ud. de los efectos secundarios o problemas que Ud. podr�a tener con este m�todo?");
    	lblpregunta322.setText(texto);
    	lblpregunta322.setText(lblpregunta322.getText().toString().replace("M�TODO ACTUAL EN 320", CodigoCorrecto()));
    	lblpregunta322.setText(lblpregunta322.getText().toString().replace("FUENTE DE M�TODO EN 312/321A", Codigo312()+codigo321()));
    	lblpregunta322.setText(lblpregunta322.getText().toString().replace("FECHA EN 321", DevolverFechade321()));
    	Spanned texto1=Html.fromHtml("322. A Ud. <b>le prescribieron</b> "+lblpregunta322.getText().toString());
    	lblpregunta322.setText(texto1); 
	}
    
    public void onrgQI322ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI322.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI323);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			Util.lockView(getActivity(), false,rgQI324);
			q3.setVisibility(View.VISIBLE);
			lblpregunta324A2.setVisibility(View.VISIBLE);
			lblpregunta324A1.setVisibility(View.GONE);
			rgQI324.requestFocus();
		} else {
			MyUtil.LiberarMemoria();
			Util.lockView(getActivity(), false,rgQI323);
			q2.setVisibility(View.VISIBLE);
//			q3.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),rgQI324);
			lblpregunta324A2.setVisibility(View.GONE);
			lblpregunta324A1.setVisibility(View.VISIBLE);
			onrgQI323ChangeValue();
			rgQI323.requestFocus();
		}
    }
    
    public void onrgQI323ChangeValue() {
    	if (!MyUtil.incluyeRango(1,1,rgQI322.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		if (MyUtil.incluyeRango(2,2,rgQI323.getTagSelected("").toString())) {
    			Util.cleanAndLockView(getActivity(),rgQI324);
    			MyUtil.LiberarMemoria();
    			q3.setVisibility(View.GONE);
    			lblpregunta324A1.setVisibility(View.GONE);
    			lblpregunta324A2.setVisibility(View.VISIBLE);
    			rgQI324A.requestFocus();
    		} else {
    			Util.lockView(getActivity(), false,rgQI324);
    			q3.setVisibility(View.VISIBLE);
    			lblpregunta324A1.setVisibility(View.VISIBLE);
    			lblpregunta324A2.setVisibility(View.GONE);
    			rgQI324.requestFocus();
    		}
    	}
	
	}
    
    public void onrgQI324AChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI324A.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQI325);
			q5.setVisibility(View.GONE);
			rgQI325A.requestFocus();
		} else {
			Util.lockView(getActivity(), false,rgQI325);
			q5.setVisibility(View.VISIBLE);
			rgQI325.requestFocus();
		}
	}
    public Integer CalcularPregunta326(){
    	Integer qi326=null;
    	if(!Util.esDiferente(individual.qi320,1,3,4,5,6,9)){
//    		Log.d("pre320", "preg320 "+individual.qi320);
    		qi326=individual.qi320;
    	}
    	return qi326;
    }
    	
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    public String CodigoCorrecto(){
    	String nombre="";
    	if(!Util.esDiferente(individual.qi320, 1)){
    		nombre="ESTERILIZACI�N FEMENINA";
    	}
    	if(!Util.esDiferente(individual.qi320, 3)){
    		nombre="P�LDORA";
    	}
    	if(!Util.esDiferente(individual.qi320, 4)){
    		nombre="DIU";
    	}
    	if(!Util.esDiferente(individual.qi320, 5)){
    		nombre="INYECCI�N";
    	}
    	if(!Util.esDiferente(individual.qi320, 6)){
    		nombre="IMPLANTES";
    	}
    	if(!Util.esDiferente(individual.qi320, 9)){
    		nombre="ESPUMA, JALEA, �VULOS(VAGINALES)";
    	}
    	return nombre;
    }
    public String DevolverFechade321(){
    	String texto="";
    	if(individual.qi316y!=null){
    		texto=MyUtil.Mes(Integer.parseInt(individual.qi316m)-1)+" "+individual.qi316y;
    	}
    	if(individual.qi315y!=null){
    		texto=MyUtil.Mes(Integer.parseInt(individual.qi315m)-1)+" "+individual.qi315y;
    	}
    	return texto;
    }
    public String Codigo312(){
    	String Textoaretornar="";
    	if(!Util.esDiferente(individual.qi312,10)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_1).substring(3,getString(R.string.ciseccion_03qi312_1).length());
    	}
    	if(!Util.esDiferente(individual.qi312,11)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_2).substring(3,getString(R.string.ciseccion_03qi312_2).length());
    	}
    	if(!Util.esDiferente(individual.qi312,12)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_3).substring(3,getString(R.string.ciseccion_03qi312_3).length());
    	}
    	if(!Util.esDiferente(individual.qi312,13)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_4).substring(3,getString(R.string.ciseccion_03qi312_4).length());
    	}
    	if(!Util.esDiferente(individual.qi312,14)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_5).substring(3,getString(R.string.ciseccion_03qi312_5).length());
    	}
    	if(!Util.esDiferente(individual.qi312,15)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_6).substring(3,getString(R.string.ciseccion_03qi312_6).length());
    	}
    	if(!Util.esDiferente(individual.qi312,16)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_7).substring(3,getString(R.string.ciseccion_03qi312_7).length());
    	}
    	if(!Util.esDiferente(individual.qi312,20)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_8).substring(3,getString(R.string.ciseccion_03qi312_8).length());
    	}
    	if(!Util.esDiferente(individual.qi312,21)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_9).substring(3,getString(R.string.ciseccion_03qi312_9).length());
    	}
    	if(!Util.esDiferente(individual.qi312,31)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_10).substring(3,getString(R.string.ciseccion_03qi312_10).length());
    	}
    	if(!Util.esDiferente(individual.qi312,32)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_11).substring(3,getString(R.string.ciseccion_03qi312_11).length());
    	}
    	if(!Util.esDiferente(individual.qi312,96)){
    		Textoaretornar=individual.qi312_o;
    	}
    	if(!Util.esDiferente(individual.qi312,98)){
    		Textoaretornar= getString(R.string.ciseccion_03qi312_13).substring(3,getString(R.string.ciseccion_03qi312_13).length());
    	}
    	return Textoaretornar;
    }
    public String codigo321(){
    	String texto321="";
    	if(!Util.esDiferente(individual.qi321a,10)){
    		texto321=getString(R.string.ciseccion_03qi321a_1).substring(3,getString(R.string.ciseccion_03qi321a_1).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,11)){
    		texto321=getString(R.string.ciseccion_03qi321a_2).substring(3,getString(R.string.ciseccion_03qi321a_2).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,12)){
    		texto321=getString(R.string.ciseccion_03qi321a_3).substring(3,getString(R.string.ciseccion_03qi321a_3).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,13)){
    		texto321=getString(R.string.ciseccion_03qi321a_4).substring(3,getString(R.string.ciseccion_03qi321a_4).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,14)){
    		texto321=getString(R.string.ciseccion_03qi321a_5).substring(3,getString(R.string.ciseccion_03qi321a_5).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,15)){
    		texto321=getString(R.string.ciseccion_03qi321a_6).substring(3,getString(R.string.ciseccion_03qi321a_6).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,16)){
    		texto321=getString(R.string.ciseccion_03qi321a_7).substring(3,getString(R.string.ciseccion_03qi321a_7).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,17)){
    		texto321=getString(R.string.ciseccion_03qi321a_8).substring(3,getString(R.string.ciseccion_03qi321a_8).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,18)){
    		texto321=individual.qi321a_o;
    	}
    	if(!Util.esDiferente(individual.qi321a,20)){
    		texto321=getString(R.string.ciseccion_03qi321a_10).substring(3,getString(R.string.ciseccion_03qi321a_10).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,21)){
    		texto321=getString(R.string.ciseccion_03qi321a_11).substring(3,getString(R.string.ciseccion_03qi321a_11).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,22)){
    		texto321=getString(R.string.ciseccion_03qi321a_12).substring(3,getString(R.string.ciseccion_03qi321a_12).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,31)){
    		texto321=getString(R.string.ciseccion_03qi321a_13).substring(3,getString(R.string.ciseccion_03qi321a_13).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,32)){
    		texto321=getString(R.string.ciseccion_03qi321a_14).substring(3,getString(R.string.ciseccion_03qi321a_14).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,33)){
    		texto321=getString(R.string.ciseccion_03qi321a_15).substring(3,getString(R.string.ciseccion_03qi321a_15).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,41)){
    		texto321=getString(R.string.ciseccion_03qi321a_16).substring(3,getString(R.string.ciseccion_03qi321a_16).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,42)){
    		texto321=getString(R.string.ciseccion_03qi321a_17).substring(3,getString(R.string.ciseccion_03qi321a_17).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,95)){
    		texto321=getString(R.string.ciseccion_03qi321a_18).substring(3,getString(R.string.ciseccion_03qi321a_18).length());
    	}
    	if(!Util.esDiferente(individual.qi321a,96)){
    		texto321=individual.qi321a_o;
    	}
    	return texto321;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI322.readOnly();
    		rgQI323.readOnly();
    		rgQI324.readOnly();
    		rgQI324A.readOnly();
    		rgQI325.readOnly();
    		rgQI325A.readOnly();
    		txtQI325A_O.readOnly();
    	}
    }
    

	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi325a!=null)
			individual.qi325a=individual.getConvertQi325a(individual.qi325a);
	
		try {
			individual.qi326=CalcularPregunta326();
			Log.d("pre326", "preg326-  "+individual.qi326);
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi325a = individual.qi325a;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		
		return App.INDIVIDUAL;
	}
}