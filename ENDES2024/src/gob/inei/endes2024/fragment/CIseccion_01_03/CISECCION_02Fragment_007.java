package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_02Fragment_007Dialog;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_02Fragment_007Dialog.ACTION_TERMINACION;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.AdapterView.OnItemClickListener;

public class CISECCION_02Fragment_007 extends FragmentForm implements Respondible {
	
	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQI234;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI235A;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI235B_M;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI235B_Y;
	

	LinearLayout q0,q3,q4,q5,q6;
	
	CISECCION_01_03 individual;
	CISECCION_01_03 individual1_3;
	public TextField txtCabecera;
	public GridComponent2 gdEmbarazoUltimo;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta234,lblpregunta235A,lblmeses,lblmes1,lblanio1,lblpregunta235B,lblpregunta235;
	public ButtonComponent btnAgregar;
	public TableComponent tcTerminaciones;
	public List <CISECCION_02T> terminaciones;
	Seccion01ClickListener adapter;
	private DialogComponent dialog;

	
	List<CISECCION_02> listados2;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado,seccionesCargado2,seccionesCargado3,seccionesCargadoTerminaciones;

	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;
	public ACTION_TERMINACION accion;

	public CISECCION_02Fragment_007() {}
	public CISECCION_02Fragment_007 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcTerminaciones.getListView().setOnItemClickListener(adapter);
		
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI230","QI233","QI234","QI235A","QI235B_M","QI235B_Y","QI236","QI236_A","QI237","QI230","QI231_M","QI231_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargado2 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI215D","QI215M","QI215Y","QI220A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM") };
		seccionesCargado3= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI233","QI231_M","QI231_Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI233","QI234","QI235A","QI235B_M","QI235B_Y","QI236","QI236_A","QI237")};
		seccionesCargadoTerminaciones = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"TERMINACION_ID","QI235_M","QI235_Y","QI235_D","ID","HOGAR_ID","PERSONA_ID")};
		return rootView;
  }
  
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_02).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		rgQI234=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi234_1,R.string.ciseccion_02qi234_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI234ChangeValue");
		rgQI235A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_02qi235a_1,R.string.ciseccion_02qi235a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI235AChangeValue");
		lblmeses  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi233_m).textSize(16).centrar();
		
		txtQI235B_M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQI235B_Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
		
		
		lblpregunta234 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi234);
		lblpregunta235 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_02qi235);
		lblpregunta235A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi235a);
		lblpregunta235B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_02qi235b);
		lblmes1  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi231_m).textSize(16).centrar();
		lblanio1  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.ciseccion_02qi231_y).textSize(16).centrar();
		
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcTerminaciones = new TableComponent(getActivity(), this, App.ESTILO).size(250, 600).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcTerminaciones = new TableComponent(getActivity(), this, App.ESTILO).size(350, 600).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcTerminaciones.addHeader(R.string.ciseccion_02qi235_n, 0.5f,TableComponent.ALIGN.CENTER);
		tcTerminaciones.addHeader(R.string.ciseccion_02qi235_m, 0.5f,TableComponent.ALIGN.LEFT);
		tcTerminaciones.addHeader(R.string.ciseccion_02qi235_y, 0.5f,TableComponent.ALIGN.LEFT);
		tcTerminaciones.addHeader(R.string.ciseccion_02qi235_d, 0.5f,TableComponent.ALIGN.LEFT);
		
		btnAgregar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.ciseccion_02qi235_b).size(200, 65);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
		    	CISECCION_02T bean = new CISECCION_02T();
				bean.id = App.getInstance().getPersonaCuestionarioIndividual().id;
				bean.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
				bean.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
				bean.terminacion_id = terminaciones.size()+1;
				abrirDetalle(bean);
			}
		});
		
		
		gdEmbarazoUltimo = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdEmbarazoUltimo.addComponent(lblmes1);
		gdEmbarazoUltimo.addComponent(txtQI235B_M);
		gdEmbarazoUltimo.addComponent(lblanio1);
		gdEmbarazoUltimo.addComponent(txtQI235B_Y);
    }
  
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		
		q3 = createQuestionSection(lblpregunta234,rgQI234);
		q4 = createQuestionSection(lblpregunta235,btnAgregar,tcTerminaciones.getTableView());
		q5 = createQuestionSection(lblpregunta235A,rgQI235A);
		q6 = createQuestionSection(lblpregunta235B,gdEmbarazoUltimo.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		return contenedor;
    }
    
    
    public void cargarTabla() {
		terminaciones = getCuestionarioService().getListarTerminaciones(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargadoTerminaciones);
		tcTerminaciones.setData(terminaciones, "terminacion_id", "qi235_m","qi235_y","qi235_d");
		registerForContextMenu(tcTerminaciones.getListView());
	}
    
    public void abrirDetalle(CISECCION_02T tmp) {
		FragmentManager fm = CISECCION_02Fragment_007.this.getFragmentManager();
		CISECCION_02Fragment_007Dialog aperturaDialog = CISECCION_02Fragment_007Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){ 
			App.getInstance().getPersonaCuestionarioIndividual().qi233 = individual.qi233;
			LlenarDataParaelMenu();
		}
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return true;
    }
    
    public void LlenarDataParaelMenu(){
    	CISECCION_02T seccion2t = getCuestionarioService().primeraTerminacionCap1_3(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
    /*Se cambia el condiciones iniciales de muestra de la ultima terminacion  20/01/2017*/
    	 if(individual.qi230!=null && individual.qi234!=null) {          
	          if(individual.qi230 == 1 && individual.qi234==2) {
	        	  App.getInstance().getPersonaCuestionarioIndividual().fechaPrimerAborto = individual.qi231_m+"/"+ individual.qi231_y;
	            }    
	          else  if(individual.qi235a!=null) {
		          if(individual.qi230 == 1 && individual.qi234==1 &&  individual.qi235a==2) {
		        	  if(seccion2t!=null) {
		        		  App.getInstance().getPersonaCuestionarioIndividual().fechaPrimerAborto = seccion2t.qi235_m+"/"+seccion2t.qi235_y;
		        	  	}
		          } 		
		          else if(individual.qi230 == 1 && individual.qi234==1 &&  individual.qi235a==1) {
		        	  App.getInstance().getPersonaCuestionarioIndividual().fechaPrimerAborto = individual.qi235b_m+"/"+individual.qi235b_y;
		          }           
	          }
         }
    	
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer in230=individual.qi230==null?0:individual.qi230;
		Integer in231_y=individual.qi231_y==null?0:individual.qi231_y;
		Integer int234=individual.qi234==null?0:individual.qi234;		
		Integer int230=individual.qi230==null?0:individual.qi230;
    	Integer int235a=individual.qi235a==null?0:individual.qi235a;
    	
		if (in230!=2) {
			if (in231_y>=App.ANIOPORDEFECTO) {
				
				if (Util.esVacio(int234)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI234");
					view = rgQI234;
					error = true;
					return false;
				}  
				if (int234!=2) {
					if(terminaciones.size()<=0){
						mensaje = "Si tuvo terminaciones deben ser registradas";
						view = btnAgregar;
						error = true;
						return false;
					}
					if (Util.esVacio(individual.qi235a)) {
						mensaje = preguntaVacia.replace("$", "La pregunta QI235A");
						view = rgQI235A;
						error = true;
						return false;
					}
			  		if (int235a!=2) {
						if (Util.esVacio(individual.qi235b_m)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI235B_M");
							view = txtQI235B_M;
							error = true;
							return false;
						}
						

						if (MyUtil.incluyeRango(1,9,individual.qi235b_m)) {
							mensaje = "Los meses deben considerarse a 2 d�gitos: de 01 a 12 meses";
							view = txtQI235B_M;
							error = true;
							return false;
						}
						
						//ToastMessage.msgBox(this.getActivity(), individual.qi235b_m , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
						//(individual.qi235b_m.charAt(0) == '0') ? String.valueOf(individual.qi235b_m.charAt(1)) : individual.qi235b_m;
						
						if (!MyUtil.incluyeRango(1,12,(individual.qi235b_m.charAt(0) == '0') ? String.valueOf(individual.qi235b_m.charAt(1)) : individual.qi235b_m) && Util.esMenor(individual.qi235b_y,App.ANIOPORDEFECTO) && !individual.qi235b_m.equals("98")) {
							mensaje = "La cantidad de meses esta fuera de rango";
							view = txtQI235B_M;
							error = true;
							return false;
						}
						
						if (Util.esVacio(individual.qi235b_y)) {
							mensaje = preguntaVacia.replace("$", "La pregunta QI235B_Y");
							view = txtQI235B_Y;
							error = true;
							return false;
						}
						
						if (individual.qi235b_y>App.ANIOPORDEFECTO-1) {
							mensaje = "El a�o ingresado no corresponde";
							view = txtQI235B_Y;
							error = true;
							return false;
						}
						
						boolean retorno=true;
						for(CISECCION_02 cs2 : listados2) {
							
							if(cs2.qi215y!=null && cs2.qi215m!=null && cs2.qi215d!=null) {
							Integer meses = 6;
							if(cs2.qi220a!=null)
								meses = cs2.qi220a; 
							
							Integer mes_fin = Integer.parseInt(cs2.qi215m);
							Integer dia_fin = Integer.parseInt(cs2.qi215d);
							
							Calendar fecha_fin = new GregorianCalendar(cs2.qi215y,mes_fin-1,dia_fin);
							Calendar fecha_ini = new GregorianCalendar(cs2.qi215y,mes_fin-1-meses,dia_fin);
							
							Integer meses2 = 0;
							
							Integer mes_eva = Integer.parseInt(individual.qi235b_m);
							Calendar fecha_eva = new GregorianCalendar(individual.qi235b_y,mes_eva-1,dia_fin);
							Calendar fecha_eva_ini = new GregorianCalendar(individual.qi235b_y,mes_eva-1-meses2,dia_fin);

							
							if((fecha_ini.compareTo(fecha_eva)<=0 && fecha_fin.compareTo(fecha_eva)>=0) || 
							   (fecha_ini.compareTo(fecha_eva_ini)<=0 && fecha_fin.compareTo(fecha_eva_ini)>=0)) {
								retorno = false;
								break; 
							}
						}
						}
						
						if (!retorno) {
							mensaje = "La fecha ingresada no es valida";
							view = txtQI235B_Y;
							error = true;
							return retorno;
						}	
						
						if(individual1_3.qi231_m!=null && individual1_3.qi231_y!=null) {
							Integer meses = 6;
							if(individual1_3.qi233!=null)
								meses = individual1_3.qi233; 
							
							Integer mes_fin = Integer.parseInt(individual1_3.qi231_m);
							Integer dia_fin = 1;
							
							Calendar fecha_fin = new GregorianCalendar(individual1_3.qi231_y,mes_fin-1,dia_fin);
							Calendar fecha_ini = new GregorianCalendar(individual1_3.qi231_y,mes_fin-1-meses,dia_fin);
							
							Integer mes_eva =  Integer.parseInt(individual.qi235b_m);
							Calendar fecha_eva = new GregorianCalendar(individual.qi235b_y,mes_eva-1,dia_fin);
							Calendar fecha_eva_ini = new GregorianCalendar(individual.qi235b_y,mes_eva-1-meses,dia_fin);
							
							if((fecha_ini.compareTo(fecha_eva)<=0 && fecha_fin.compareTo(fecha_eva)>=0) || (fecha_ini.compareTo(fecha_eva_ini)<=0 && fecha_fin.compareTo(fecha_eva_ini)>=0)) {
								mensaje = "La fecha ingresada no es valida";
								view = txtQI235B_Y;
								error = true;
								return false;
							}
						}
						
						
					}
				}
			}
		}
		return true;
    }
    
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
		if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		individual1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado3);
		listados2=getCuestionarioService().getNacimientosListbyPersona(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado2);
		entityToUI(individual);	
		inicio();
		cargarTabla(); 
    }
    
    public void validarPregunta236() {
    	Integer in230=individual.qi230==null?0:individual.qi230;
    	Integer in231_y=individual.qi231_y==null?0:individual.qi231_y;
    	if (in230==2 || in231_y<App.ANIOPORDEFECTO) {
    		Util.cleanAndLockView(getActivity(),rgQI234,rgQI235A,txtQI235B_M,txtQI235B_Y);
    		q0.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
		}
    	else {	
    		Util.lockView(getActivity(),false,rgQI234,rgQI235A,txtQI235B_M,txtQI235B_Y);
    		q0.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		onrgQI234ChangeValue();
    	}
	}
    
    private void inicio() {
    	validarPregunta236();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	RenombrarEtiquetas();
    }
    public void RenombrarEtiquetas(){
    	lblpregunta234.setText(getResources().getString(R.string.ciseccion_02qi234));
    	lblpregunta234.setText(lblpregunta234.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    	lblpregunta235.setText(getResources().getString(R.string.ciseccion_02qi235));
    	lblpregunta235.setText(lblpregunta235.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    	lblpregunta235A.setText(getResources().getString(R.string.ciseccion_02qi235a));
    	lblpregunta235A.setText(lblpregunta235A.getText().toString().replace("##", App.ANIOPORDEFECTO+""));
    }
    public void onrgQI234ChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,2,rgQI234.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQI235A,txtQI235B_M,txtQI235B_Y);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,rgQI235A,txtQI235B_M,txtQI235B_Y);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			onrgQI235AChangeValue();
			rgQI235A.requestFocus();
		}
    }
    public void onrgQI235AChangeValue() {
    	MyUtil.LiberarMemoria();
		if (MyUtil.incluyeRango(2,2,rgQI235A.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI235B_M,txtQI235B_Y);
			q6.setVisibility(View.GONE);
		} else {
			Util.lockView(getActivity(), false,txtQI235B_M,txtQI235B_Y);
			q6.setVisibility(View.VISIBLE);
			txtQI235B_M.requestFocus();
		}
    }
    
    private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_02T c = (CISECCION_02T) terminaciones.get(arg2);		
			abrirDetalle(c);
		}
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcTerminaciones.getListView())) {
			menu.setHeaderTitle("Opciones del Residente");
			menu.add(1, 0, 1, "Editar Residente");
			menu.add(1, 1, 1, "Eliminar Residente");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			CISECCION_02T seleccion = (CISECCION_02T) info.targetView.getTag();
	
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		CISECCION_02T seleccion = (CISECCION_02T) info.targetView.getTag();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalle((CISECCION_02T) terminaciones.get(info.position));
				break;
			case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"Å¼Esta seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_02T) terminaciones.get(info.position));
				dialog.showDialog();
				break;
			}
		}
		return super.onContextItemSelected(item);
	}
	
	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		CISECCION_02T bean = (CISECCION_02T) dialog.get("seleccion");
		try {
			terminaciones.remove(bean);
			boolean borrado = false;
			borrado = getCuestionarioService().BorrarTerminaciones(bean.id, bean.hogar_id,bean.persona_id, bean.terminacion_id);
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	}

	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI234.readOnly();
			rgQI235A.readOnly();
			txtQI235B_M.readOnly();
			txtQI235B_Y.readOnly();
		}
	}	
	
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null) 
			App.getInstance().getPersonaCuestionarioIndividual().qi233 = individual.qi233;
		else 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
		return App.INDIVIDUAL;
	}
}