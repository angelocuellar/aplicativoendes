package gob.inei.endes2024.fragment.CIseccion_01_03;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_03Fragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex=1)
	public TextField txtQI321_NOM;
	@FieldAnnotation(orderIndex=2)
	public RadioGroupOtherField rgQI321A;

	public TextField txtCabecera;
	CISECCION_01_03 individual;
	public ButtonComponent btnNoPagoEsteril, btnNoSabeEsteril,btnNoPagoEsteril1er,btnNoSabeEsteril1er;
	public GridComponent2 gdCostoEsteril,gdEmbarazo,gdEmbarazoUltimo,gdCostoEsteril1er,gdLugarSalud;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblpregunta320,lblpregunta321A,lblpregunta321A_1_Ind,lblpregunta321A_2_Ind,lblpregunta321A_3_Ind,lblpregunta321A_4_Ind,lbllugar,lblpregunta321A_ind;
	public TextField txtQI321A1,txtQI321A2;
	LinearLayout q0,q1;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	public CISECCION_03Fragment_004() {}
	public CISECCION_03Fragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
  }
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI321_NOM","QI321A","QI321A_O","QI315Y","QI316Y","QI226","QI230","QI304","QI320","QI315M","QI315Y","QI316M","QI316Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI321_NOM","QI321A","QI321A_O")};
		return rootView;
  }
  
  @Override
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_03).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta320 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi320);
		lblpregunta321A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.ciseccion_03qi321a);
		lblpregunta321A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.ciseccion_03qi321a_ind);
		
		Spanned texto321A_1 =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta321A_1_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_1_Ind.setText(texto321A_1);
		Spanned texto321A_2 =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta321A_2_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_2_Ind.setText(texto321A_2);
		Spanned texto321A_3 =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta321A_3_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_3_Ind.setText(texto321A_3);
		Spanned texto321A_4 =Html.fromHtml("<b>OTRO</b>");
		lblpregunta321A_4_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta321A_4_Ind.setText(texto321A_4);
		
		rgQI321A=new RadioGroupOtherField(this.getActivity(),R.string.ciseccion_03qi321a_1,R.string.ciseccion_03qi321a_2,R.string.ciseccion_03qi321a_3,R.string.ciseccion_03qi321a_4,R.string.ciseccion_03qi321a_5,R.string.ciseccion_03qi321a_6,R.string.ciseccion_03qi321a_7,R.string.ciseccion_03qi321a_8,R.string.ciseccion_03qi321a_9,R.string.ciseccion_03qi321a_10,R.string.ciseccion_03qi321a_11,R.string.ciseccion_03qi321a_12,R.string.ciseccion_03qi321a_13,R.string.ciseccion_03qi321a_14,R.string.ciseccion_03qi321a_15,R.string.ciseccion_03qi321a_16,R.string.ciseccion_03qi321a_17,R.string.ciseccion_03qi321a_18,R.string.ciseccion_03qi321a_19).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI321A1=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		txtQI321A2=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		rgQI321A.agregarEspecifique(8,txtQI321A1);
		rgQI321A.agregarEspecifique(18,txtQI321A2);
		rgQI321A.addView(lblpregunta321A_1_Ind,0);
		rgQI321A.addView(lblpregunta321A_2_Ind,10);
		rgQI321A.addView(lblpregunta321A_3_Ind,14);
		rgQI321A.addView(lblpregunta321A_4_Ind,18);
		
		txtQI321_NOM=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 350).alfanumerico();
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.ciseccion_03qi321_nom).textSize(16);
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI321_NOM);
    }
  
    @Override
    protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta321A,lblpregunta321A_ind,gdLugarSalud.component(),rgQI321A);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
 
		return contenedor;
    }
    @Override
    public boolean grabar() {
		uiToEntity(individual);
		if (individual.qi321a!=null)
			individual.qi321a=individual.getConvertQi321a(individual.qi321a);
		
		if (individual.qi321a!=null) {
			if (individual.qi321a==18 && txtQI321A1.getText().toString().trim().length()!=0 ) {
				individual.qi321a_o=txtQI321A1.getText().toString().trim();
			}
			else if (individual.qi321a==96 && txtQI321A2.getText().toString().trim().length()!=0) {
				individual.qi321a_o=txtQI321A2.getText().toString().trim();
			}
			else {
				individual.qi321a_o=null;
			}
		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()==null) 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
			
		return true;
    }
    private boolean validar() {
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		Integer int304 = individual.qi304==null?0:individual.qi304;
		Integer int315 = individual.qi315y==null?0:individual.qi315y;
    	Integer int316 = individual.qi316y==null?0:individual.qi316y;
    	if (int304!=2) {
    		if ((individual.qi320==3 || individual.qi320==4 || individual.qi320==5 || individual.qi320==6 || individual.qi320==9) && (int315>2010 || int316>2010)) {
//    			if (Util.esVacio(individual.qi321_nom)) {
//    				mensaje = preguntaVacia.replace("$", "La pregunta QI321_NOM");
//    				view = txtQI321_NOM;
//    				error = true;
//    				return false;
//    			}
    			if (Util.esVacio(individual.qi321a)) {
    				mensaje = preguntaVacia.replace("$", "La pregunta QI321A");
    				view = rgQI321A;
    				error = true;
    				return false;
    			}
    			if (individual.qi321a==18) {
    				if (Util.esVacio(individual.qi321a_o)) {
    					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
    					view = txtQI321A1; 
    					error = true; 
    					return false; 
    				}			
    			}	
    			if (individual.qi321a==96) {
    				if (Util.esVacio(individual.qi321a_o)) { 
    					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
    					view = txtQI321A2; 
    					error = true; 
    					return false; 
    				}				
    			}
    		}
    	}
		return true;
    }
    @Override
    public void cargarDatos() {
    	individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getPersonaCuestionarioIndividual().qi320=individual.qi320;
    	
    	if (individual == null) {
			individual = new CISECCION_01_03();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id;
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
		if(individual.qi321a!=null)
			individual.qi321a=individual.setConvertQi321a(individual.qi321a);
		entityToUI(individual);
		if (individual.qi321a!=null) {
    		if (individual.qi321a==9) {
    			txtQI321A1.setText(individual.qi321a_o);
    		}
    		if (individual.qi321a==19) {
    			txtQI321A2.setText(individual.qi321a_o);
    		}
    	}
		inicio();
    }
    
    public void validarPreguntas(){
    	Integer in321a = individual.qi321a==null?0:individual.qi321a;
    	Integer in320 = individual.qi320==null?0:individual.qi320;
    	Integer int304 = individual.qi304==null?0:individual.qi304;
    	Integer int315 = individual.qi315y==null?0:individual.qi315y;
    	Integer int316 = individual.qi316y==null?0:individual.qi316y;
    	if (int304==2) {
    		Util.cleanAndLockView(getActivity(),txtQI321_NOM,rgQI321A);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
		}else {
			if (in320==1||in320==2||in320==7||in320==8||in320==10||in320==11||in320==12||in320==13||in320==96) {
				Util.cleanAndLockView(getActivity(),txtQI321_NOM,rgQI321A,txtQI321A1,txtQI321A2);
				q0.setVisibility(View.GONE);
				q1.setVisibility(View.GONE);
			} else {
				if (int315>2010 || int316>2010) {
					Util.lockView(getActivity(), false,txtQI321_NOM,rgQI321A);
					q0.setVisibility(View.VISIBLE);
					q1.setVisibility(View.VISIBLE);
					if (in321a==9  ) {
						Util.lockView(getActivity(), false,txtQI321A1);
					}
					else if (in321a==19) {
						Util.lockView(getActivity(), false,txtQI321A2);
					}
					else {
						Util.cleanAndLockView(getActivity(),txtQI321A1,txtQI321A2);
					}
				}else {
					Util.cleanAndLockView(getActivity(),txtQI321_NOM,rgQI321A,txtQI321A1,txtQI321A2);
					q0.setVisibility(View.GONE);
					q1.setVisibility(View.GONE);
				}
			}
		}
    }
    

    
    private void inicio() {
    	validarPreguntas();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void RenombrarEtiquetas(){    	
    	lblpregunta321A.text(R.string.ciseccion_03qi321a);    	
    	lblpregunta321A.setText(lblpregunta321A.getText().toString().replace("M�TODO ACTUAL EN 320", CodigoCorrecto()));
    	lblpregunta321A.setText(lblpregunta321A.getText().toString().replace("FECHA EN 321", DevolverFechade321()));
    }
    
    public String CodigoCorrecto(){
    	String nombre="";
    	if(!Util.esDiferente(individual.qi320, 3)){
    		nombre="P�LDORA";
    	}
    	if(!Util.esDiferente(individual.qi320, 4)){
    		nombre="DIU";
    	}
    	if(!Util.esDiferente(individual.qi320, 5)){
    		nombre="INYECCI�N";
    	}
    	if(!Util.esDiferente(individual.qi320, 6)){
    		nombre="IMPLANTES";
    	}
    	if(!Util.esDiferente(individual.qi320, 9)){
    		nombre="ESPUMA, JALEA, �VULOS(VAGINALES)";
    	}
    	return nombre;
    }
    public String DevolverFechade321(){
    	String texto="";
    	if(individual.qi316y!=null){
    		texto=MyUtil.Mes(Integer.parseInt(individual.qi316m)-1)+" "+individual.qi316y;
    	}
    	if(individual.qi315y!=null){
    		texto=MyUtil.Mes(Integer.parseInt(individual.qi315m)-1)+" "+individual.qi315y;
    	}
    	return texto;
    }
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI321A.readOnly();
    		txtQI321_NOM.readOnly();
    		txtQI321A1.readOnly();
    		txtQI321A2.readOnly();    		
    	}
    }
    
        
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual);
		if (individual.qi321a!=null)
			individual.qi321a=individual.getConvertQi321a(individual.qi321a);
		
		if (individual.qi321a!=null) {
			if (individual.qi321a==18 && txtQI321A1.getText().toString().trim().length()!=0 ) {
				individual.qi321a_o=txtQI321A1.getText().toString().trim();
			}
			else if (individual.qi321a==96 && txtQI321A2.getText().toString().trim().length()!=0) {
				individual.qi321a_o=txtQI321A2.getText().toString().trim();
			}
			else {
				individual.qi321a_o=null;
			}
		}
		try {
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		/*individual.total_ninios =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios;
		individual.total_ninios_vivos =  App.getInstance().getPersonaCuestionarioIndividual().total_ninios_vivos;*/
		if(App.getInstance().getPersonaCuestionarioIndividual()==null) 
			App.getInstance().setPersonaCuestionarioIndividual(individual);
			
		return App.INDIVIDUAL;
	}
}