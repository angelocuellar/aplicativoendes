package gob.inei.endes2024.fragment.visita.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.SHOW_HIDE;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.ui.GPSDialog;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.IGPSDialog;
import gob.inei.dnce.util.CapturadorGPS;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.caratula.CARATULAFragment_002;
import gob.inei.endes2024.fragment.visita.VisitaFragment_001;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.VisitaService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.content.QuickViewConstants;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mapswithme.maps.api.MapsWithMeApi;


public class Viv_VisitaDialog extends DialogFragmentComponent implements IGPSDialog {
    @FieldAnnotation(orderIndex = 1)
    public DateTimeField txtFECHA_INICIO;
    @FieldAnnotation(orderIndex = 2)
    public DateTimeField txtHORA_INICIO;
    @FieldAnnotation(orderIndex = 3)
    public DateTimeField txtHORA_FIN;
    @FieldAnnotation(orderIndex = 4)
    public SpinnerField spnQVVRESUL;
    @FieldAnnotation(orderIndex = 5)
    public SpinnerField spnQVVRESUL_R;
    @FieldAnnotation(orderIndex = 6)
    public TextField txtQVVRESUL_O;
    @FieldAnnotation(orderIndex = 7)
    public DateTimeField txtFECHA_PROXIMA;
    @FieldAnnotation(orderIndex = 8)
    public DateTimeField txtHORA_PROXIMA;
    
	
	
	@FieldAnnotation(orderIndex = 9)	
  	public RadioGroupOtherField rgQVCONDICION;
//    @FieldAnnotation(orderIndex = 10)	
//    public DateTimeField txtQVCONDICION_F;
//	@FieldAnnotation(orderIndex = 10)
//	public TextField txtCN_DIA;
//	@FieldAnnotation(orderIndex = 11)
//	public TextField txtCN_MES;
//	@FieldAnnotation(orderIndex = 12)
//	public TextField txtCN_ANIO;
    @FieldAnnotation(orderIndex = 10)	
  	public CheckBoxField chbQVCONDICION_FN;
    
    @FieldAnnotation(orderIndex = 11)	
  	public RadioGroupOtherField rgQVINFORMANTE;
    @FieldAnnotation(orderIndex = 12)	
  	public TextField txtQVINFORMANTE_O;
    
    @FieldAnnotation(orderIndex = 13)	
	public TextField txtQVGPS_LAT;
	@FieldAnnotation(orderIndex = 14)
	public TextField txtQVGPS_LONG;
	@FieldAnnotation(orderIndex = 15)
	public TextField txtQVGPS_ALT;
    
	
	public TextField txtCN_DIA, txtCN_MES;	
	public IntegerField txtCN_ANIO;
	public TextField txtCabecera;
//	public TextField txtQVCONDICION_F;
    public LabelComponent lblv01_viv_t, lblv01_viv_cond_fs, lblv02_viv_inf;
    public GridComponent2 gridCOND;
	public ButtonComponent btnIniciar,btnGPS,btnVerenMapa,btnAceptar,btnCancelar;	
		
    public List<Integer> resul_1;
    public List<Integer> resul_fila;
        
    private CuestionarioService service;
    public CheckBoxField chbResul_o1;
    
    private LabelComponent lblBlanco4,lblpregunta1,lblBlanco3, lblCordenadas1,lblCordenadas2,lblCordenadas3,lblblanco,lblblanco1,lblFecha,lblHoraIni,lblHoraFin,lblResutado,lblfechaproxima,lblhoraproxima;
    private SeccionCapitulo[] seccionesGrabado,seccionesGrabadohogar,seccionesCargado_viv,seccionesGrabado_viv;
    private GridComponent2 tblGps;
    private GridComponent grid1,grid2, grproxvisita;
    
    private static FragmentForm caller;    
    private CapturadorGPS tracker;  
    public List<Hogar> detalles;
    private boolean esResultado3 = false;
    public boolean grabarcantidad=false;
    
    LinearLayout q1, q2, q3, q4, q5, q6;
    Visita_Viv bean;
    Hogar hogar;
    
    public static enum ACTION {
       INICIAR, EDITAR, FINALIZAR
    };

    private ACTION action;
    private VisitaService visitaService;

    public Viv_VisitaDialog() {
    }
    
    public Viv_VisitaDialog tracker(CapturadorGPS tracker) {
    	this.tracker =tracker;
    	return this;		
	}

    public interface C1_Cap00Fragment_001_01Listener {
            void onFinishEditDialog(String inputText);
    }

    public static Viv_VisitaDialog newInstance(FragmentForm pagina, Visita_Viv bean) {
            return newInstance(pagina, bean, ACTION.INICIAR);
    }

    public static Viv_VisitaDialog newInstance(FragmentForm pagina, Visita_Viv bean,ACTION action) {
            caller = pagina;
            Viv_VisitaDialog f = new Viv_VisitaDialog();
            f.setParent(pagina);
            Bundle args = new Bundle();
            args.putSerializable("bean", bean);
            args.putSerializable("action", action);
            f.setArguments(args);
            return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            bean = (Visita_Viv) getArguments().getSerializable("bean");
            action = (ACTION) getArguments().getSerializable("action");
            caretaker = new Caretaker<Entity>();
            
    
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            final View rootView = createUI();
            initObjectsWithoutXML(this, rootView);
            cargarDatos();
            enlazarCajas(this);
            listening();
            seccionesCargado_viv = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QVVDIA_INI", "QVVMES_INI", "QVVANIO_INI", "QVHORA_INI","QVMIN_INI", "QVVRESUL", "QVVRESUL_R", "QVVHORA_FIN", "QVVMIN_FIN","QVVDIAP", "QVVMESP", "QVVHORAP", "QVVMINUP", "NRO_VISITA","ID","QVVRESUL_O","QVGPS_LONG","QVGPS_LAT","QVGPS_ALT","QVCONDICION", "QVCONDICION_F", "QVCONDICION_FN", "QVINFORMANTE", "QVINFORMANTE_O") };
            seccionesGrabadohogar = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID")};
    
            return rootView;
            
    }


    @Override
    protected void buildFields() {
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
    	lblpregunta1 = new LabelComponent(getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).negrita().text(R.string.pregunta01);
    	
    	lblFecha = new LabelComponent(getActivity()).size(altoComponente, 100).negrita().text(R.string.v_l_fecha_ini);
        lblHoraIni = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_ini);        
        txtFECHA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM/yyyy").size(altoComponente, 180).readOnly();
        txtFECHA_INICIO.setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);            
        txtHORA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();
        
        chbResul_o1=new CheckBoxField(this.getActivity(), R.string.v_vivienda_result_01, "1:0").size(altoComponente, 700);
        chbResul_o1.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chbResul_o1.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQVVRESUL_O);
			}
			else {
				Util.lockView(getActivity(),false, txtQVVRESUL_O);	
			}
		}
		});
        
        grid1 = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
        grid1.addComponent(lblFecha);
        grid1.addComponent(txtFECHA_INICIO);
        grid1.addComponent(lblHoraIni);
        grid1.addComponent(txtHORA_INICIO);
        
        
        
        
        lblHoraFin = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_fin);
        lblResutado = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_resultado);
        
        lblBlanco4 = new LabelComponent(this.getActivity()).size(altoComponente, 150);
        lblblanco1 = new LabelComponent(this.getActivity()).size(altoComponente, 450);        
        txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();             
        spnQVVRESUL 	= new SpinnerField(getActivity()).size(altoComponente + 15,550).callback("onC1_RVISITAChangeValue");
        spnQVVRESUL_R 	= new SpinnerField(getActivity()).size(altoComponente + 15,700);
        txtQVVRESUL_O = new TextField(this.getActivity()).size(altoComponente, 700).maxLength(500).alinearIzquierda().soloTexto();
        
        grid2 = new GridComponent(getActivity(), Gravity.CENTER, 3, 0);
        grid2.addComponent(lblHoraFin);            
        grid2.addComponent(txtHORA_FIN);
        grid2.addComponent(lblblanco1);
        grid2.addComponent(lblResutado);
        grid2.addComponent(spnQVVRESUL,2);
        

        Spanned textot =Html.fromHtml("<b>ENTREVISTADORA:</b>SI EL C�DIGO DE RESULTADO DE LA �LTIMA VISITA ES C�DIGO 6, 7, 8 o 9, PREGUNTE:<br><br> 1. �La vivienda se encuentra (CONDICI�N ACTUAL DE LA VIVIENDA) desde antes o despu�s de la visita del registrador de la ENDES? <b>INDAGUE:</b>");
        lblv01_viv_t = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17);
        lblv01_viv_t.setText(textot);
		
        lblv01_viv_cond_fs =  new LabelComponent(getActivity()).size(altoComponente, 550).text(R.string.v_vivienda_cond_fs);
        lblv02_viv_inf =  new LabelComponent(getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).text(R.string.v_vivienda_inf).textSize(17);
        
        rgQVCONDICION=new RadioGroupOtherField(this.getActivity(),R.string.v_vivienda_cond_1, R.string.v_vivienda_cond_2, R.string.v_vivienda_cond_8).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();            
        chbQVCONDICION_FN =new CheckBoxField(this.getActivity(), R.string.v_vivienda_cond_ns, "1:0").size(altoComponente, 400).callback("validarEspfueCondicion");
        
        txtCN_DIA=new TextField(this.getActivity()).size(altoComponente, 70).maxLength(2).alinearDerecha().centrar().esTelefono();//.callback("onFechaChanged")
		txtCN_MES=new TextField(this.getActivity()).size(altoComponente, 70).maxLength(2).alinearDerecha().centrar().esTelefono();//.callback("onFechaChangem")
		txtCN_ANIO=new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(4).alinearDerecha().centrar();
		LabelComponent lbldia = new LabelComponent(getActivity()).size(35, 50).text(R.string.c2seccion_01_03qi215d).textSize(14).centrar();
		LabelComponent lblmes = new LabelComponent(getActivity()).size(35, 60).text(R.string.c2seccion_01_03qi215m).textSize(14).centrar();
		LabelComponent lblanio = new LabelComponent(getActivity()).size(35, 60).text(R.string.c2seccion_01_03qi215y).textSize(14).centrar();
        gridCOND = new GridComponent2(this.getActivity(),App.ESTILO,6,0);  
		gridCOND.addComponent(lblv01_viv_cond_fs,6);
		gridCOND.addComponent(lbldia);
		gridCOND.addComponent(txtCN_DIA);
		gridCOND.addComponent(lblmes);
		gridCOND.addComponent(txtCN_MES);
		gridCOND.addComponent(lblanio);
		gridCOND.addComponent(txtCN_ANIO);
		gridCOND.addComponent(chbQVCONDICION_FN,6);
		
		
        rgQVINFORMANTE=new RadioGroupOtherField(this.getActivity(),R.string.v_vivienda_inf_1, R.string.v_vivienda_inf_2, R.string.v_vivienda_inf_3, R.string.v_vivienda_inf_4, R.string.v_vivienda_inf_5, R.string.v_vivienda_inf_6, R.string.v_vivienda_inf_7, R.string.v_vivienda_inf_8, R.string.v_vivienda_inf_96).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQVINFORMANTE_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 450); 
		rgQVINFORMANTE.agregarEspecifique(8,txtQVINFORMANTE_O); 
				
		
	
		
        
        cargarSpinner();
        cargarSpinnerRRechazo();
            
        
            lblfechaproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_fecha_prox);        
            lblhoraproxima =  new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_hora_prox);
            txtFECHA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 180).showObject(SHOW_HIDE.CALENDAR).setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
            txtHORA_PROXIMA  = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA, "HH:mm").size(altoComponente, 100);
                        
            grproxvisita = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
            grproxvisita.addComponent(lblfechaproxima);
            grproxvisita.addComponent(txtFECHA_PROXIMA);
            grproxvisita.addComponent(lblhoraproxima);
            grproxvisita.addComponent(txtHORA_PROXIMA);
            
            
            lblBlanco3 = new LabelComponent(this.getActivity()).size(altoComponente+5, 200);
    		lblCordenadas1 = new LabelComponent(this.getActivity()).size(altoComponente+5, 150).text(R.string.c_latitud).textSize(16).negrita().alinearDerecha();
    		lblCordenadas2 = new LabelComponent(this.getActivity()).size(altoComponente+5, 150).text(R.string.c_longitud).textSize(16).negrita().alinearDerecha();
    		lblCordenadas3 = new LabelComponent(this.getActivity()).size(altoComponente+5, 150).text(R.string.c_altitud).textSize(16).negrita().alinearDerecha();
       
    		txtQVGPS_LAT  = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 240).alfanumerico().hint(R.string.c_latitud).readOnly();
    		txtQVGPS_LONG = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 240).alfanumerico().hint(R.string.c_longitud).readOnly();
    		txtQVGPS_ALT  = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 240).alfanumerico().hint(R.string.c_altitud).readOnly();
    				    		

    		
    		btnGPS = new ButtonComponent(getActivity(), App.ESTILO_BOTON).size(180, altoComponente).text(R.string.c_gps);
    		btnVerenMapa = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(180, 55).text(R.string.btnMapa);
    		
    		btnGPS.setOnClickListener(new View.OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				capturarGPS();
    			}});
    		
    		btnVerenMapa.setOnClickListener(new View.OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				if(txtQVGPS_ALT.getText().toString().length()<=0 || !Util.esDiferente(txtQVGPS_ALT.getText().toString(), App.GPS_OMISION)){
    				ToastMessage.msgBox(getActivity(), "Aun no hay un punto v�lido",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
    				}
    				else{
    				double latitud= Double.parseDouble(txtQVGPS_LAT.getText().toString());
    				double longitud = Double.parseDouble(txtQVGPS_LONG.getText().toString());
    				cargarMapa(latitud, longitud, "Coordenadas del hogar.");
    				}
    			}
    		});
    		
    		tblGps = new GridComponent2(this.getActivity(),App.ESTILO,3,0);    		
    		tblGps.addComponent(btnGPS);
    		tblGps.addComponent(lblCordenadas1);
    		tblGps.addComponent(txtQVGPS_LAT);    		
    		tblGps.addComponent(lblBlanco3);
    		tblGps.addComponent(lblCordenadas2);
    		tblGps.addComponent(txtQVGPS_LONG);
    		tblGps.addComponent(btnVerenMapa);    		
    		tblGps.addComponent(lblCordenadas3);
    		tblGps.addComponent(txtQVGPS_ALT);
    		
    		
            btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
            btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
            
            btnCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                          Viv_VisitaDialog.this.dismiss();
                    }
            });
            
            btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            boolean flag = grabar();
                            if (!flag) {
                                    return;
                            }
                            ((CARATULAFragment_002) caller).recargarLista();
                            App.getInstance().setVisita_Viv(bean);
                            Viv_VisitaDialog.this.dismiss();                           
                    }
            });

    }
    
    @Override
    protected View createUI() {
            buildFields();
            q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,txtCabecera,lblpregunta1, grid1.component());
            q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,grid2.component(),spnQVVRESUL_R,chbResul_o1, txtQVVRESUL_O);
            q3 = createQuestionSection(R.string.v_l_proxima_visita, grproxvisita.component());
            
            q5 = createQuestionSection(lblv01_viv_t, rgQVCONDICION, gridCOND.component());
    		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblv02_viv_inf, rgQVINFORMANTE);
            q4 = createQuestionSection(R.string.pgps,tblGps.component());
            
            LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
            ScrollView contenedor = createForm();
            LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
            
            form.addView(q1);
            form.addView(q2);
            form.addView(q3);
            form.addView(q5);
            form.addView(q6);
            form.addView(q4);
            form.addView(botones);            
            return contenedor;
    }
    




    private boolean grabar() {    	
            
			uiToEntity(bean);			
			if(chbResul_o1.isChecked() ) {
    			bean.qvvresul_o="TRANSITORIA";			
    		}
			if (bean!=null) {
				if (bean.qvcondicion!=null) {
					bean.qvcondicion=bean.setValue3a8(bean.qvcondicion);
				}
				
				if (bean.qvinformante!=null) {
					bean.qvinformante=bean.setValue9a96(bean.qvinformante);
				}
				if(txtCN_DIA.getText().toString().length()!=0 && txtCN_MES.getText().toString().length()!=0 && txtCN_ANIO.getText().toString().length()!=0){
					bean.qvcondicion_f=((txtCN_DIA.getText().toString().length()>1?txtCN_DIA.getText().toString():"0"+txtCN_DIA.getText().toString())
										+"/"+(txtCN_MES.getText().toString().length()>1?txtCN_MES.getText().toString():"0"+txtCN_MES.getText().toString())
										+"/"+txtCN_ANIO.getText().toString().trim()
										);	
				}
				
			}
			
            
			if (action == ACTION.INICIAR) {
            		bean.qvvanio_ini = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
                    bean.qvvdia_ini=Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "dd");
                    bean.qvvmes_ini =Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "MM");
                    bean.qvhora_ini  = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "HH");
                    bean.qvmin_ini = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "mm");
                   
                   
                    if(txtQVGPS_LAT.getValue() == "9999999999" || txtQVGPS_LAT.getValue() == null || txtQVGPS_ALT.getValue() == null || txtQVGPS_LONG.getValue() == null){
                    	txtQVGPS_LAT.requestFocus();
        				ToastMessage.msgBox(this.getActivity(),"GPS es requerido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                		return false;
        			}                   
                    
            }
            else if (action == ACTION.FINALIZAR) {
            	
    		
    			
                    if (spnQVVRESUL.getSelectedItemKey() == null || spnQVVRESUL.getSelectedItemKey().toString().equals("0")) {
                    	spnQVVRESUL.requestFocus();
                    	ToastMessage.msgBox(this.getActivity(),"Seleccione un resultado.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                    	return false;
                    }                    
                    
                    if (spnQVVRESUL.getSelectedItemKey().toString().equals("6") 
                    		|| spnQVVRESUL.getSelectedItemKey().toString().equals("7") 
                    		|| spnQVVRESUL.getSelectedItemKey().toString().equals("8") 
                    		|| (!chbResul_o1.isChecked() && spnQVVRESUL.getSelectedItemKey().toString().equals("9"))) {
                    	if (Util.esVacio(txtQVVRESUL_O)) {
                    		txtQVVRESUL_O.requestFocus();
                    		ToastMessage.msgBox(this.getActivity(),"Especifique no puede estar vacia.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                    		return false;
                    	}
                    }
                    
                    
                    if (spnQVVRESUL.getSelectedItemKey().toString().equals("5")) {
                        if (spnQVVRESUL_R.getSelectedItemKey() == null || spnQVVRESUL_R.getSelectedItemKey().toString().equals("0")) {
                        	spnQVVRESUL_R.requestFocus();
                        	ToastMessage.msgBox(this.getActivity(),"Especifique de Rechazo no puede estar vacia.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                        	return false;
                        }	
                    }
                    
                    bean.qvvresul = spnQVVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQVVRESUL.getSelectedItemKey();
                    bean.qvvhora_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
                    bean.qvvmin_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");
                                    
                    
                    if (spnQVVRESUL.getSelectedItemKey().toString().equals("2") || spnQVVRESUL.getSelectedItemKey().toString().equals("3") || spnQVVRESUL.getSelectedItemKey().toString().equals("4") || spnQVVRESUL.getSelectedItemKey().toString().equals("5")) {
                    	if (!validarProximaVisita(Integer.parseInt(spnQVVRESUL.getSelectedItemKey().toString()))) {
                    		return false;
                    	}
                            
                    	bean.qvvhorap=null;
                    	bean.qvvminup=null;
                    	bean.qvvdiap=null;
                    	bean.qvvmesp=null;
                    	if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
                    		bean.qvvhorap  = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
                    		bean.qvvminup = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
                    		bean.qvvdiap = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
                    		bean.qvvmesp = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
                    	}
                    }
                    
                    if (spnQVVRESUL.getSelectedItemKey() != null  && !validarPreguntasNuevas(Integer.parseInt(spnQVVRESUL.getSelectedItemKey().toString()))) {
                		return false;
                	}
                    
                    
            } 
            else if (action == ACTION.EDITAR) {

            	
            	if(bean.qvgps_lat == null || bean.qvgps_alt == null || bean.qvgps_long == null){
                	txtQVGPS_LAT.requestFocus();
    				ToastMessage.msgBox(this.getActivity(),"GPS es requerido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            		return false;
    			}
            	
            	if(bean.qvgps_alt.equals(App.GPS_OMISION)){
                	txtQVGPS_LAT.requestFocus();
    				MyUtil.MensajeGeneral(this.getActivity(),"GPS es requerido");
    			}
            	
            	if (spnQVVRESUL.getSelectedItemKey() != null  && !validarPreguntasNuevas(Integer.parseInt(spnQVVRESUL.getSelectedItemKey().toString()))) {
            		return false;
            	}
            	      
            }
			
            try {
                    if (!getVisitaService().saveOrUpdate(bean,"QVVDIA_INI","QVVMES_INI","QVVANIO_INI","QVHORA_INI","QVMIN_INI","QVVHORA_FIN","QVVMIN_FIN" ,"QVVRESUL","QVVRESUL_R","QVVRESUL_O","QVVDIAP","QVVMESP","QVVHORAP","QVVMINUP","QVGPS_LONG","QVGPS_LAT","QVGPS_ALT","QVCONDICION", "QVCONDICION_F", "QVCONDICION_FN", "QVINFORMANTE", "QVINFORMANTE_O")) {
                            ToastMessage.msgBox(this.getActivity(),"Los datos no fueron grabados",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                    }
            } catch (SQLException e) {
                    ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            }
            if(action==ACTION.EDITAR){
            	EndesCalendario.ModificaVisitaVivienda_final(bean.id);
            }
            
//            Log.e("gggbean.qvcondicion: ",""+bean.qvcondicion);
//        	Log.e("gggbean.qvcondicion_f: ",""+bean.qvcondicion_f);
        	
            return true;
    }

    
	private boolean validarPreguntasNuevas(Integer item) {
		if(!Util.esDiferente(item, 6) || !Util.esDiferente(item, 7) || !Util.esDiferente(item, 8) || !Util.esDiferente(item, 9)){
			
			if (Util.esVacio(bean.qvcondicion)) {
				rgQVCONDICION.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Condicion actual de la vivienda es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
        		return false;
			}
			if(!chbQVCONDICION_FN.isChecked() && (txtCN_DIA.getText().toString().length()==0 || txtCN_MES.getText().toString().length()==0 || txtCN_ANIO.getText().toString().length()==0) ){
				txtCN_DIA.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Fecha es requerido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
        		return false;						
			}
			
			int dia  = bean.qvcondicion_f == null ? 0 :Integer.parseInt(bean.qvcondicion_f.substring(0,2));
			int mes  = bean.qvcondicion_f == null ? 0 :Integer.parseInt(bean.qvcondicion_f.substring(3,5));
			int anio = bean.qvcondicion_f == null ? 0 :Integer.parseInt(bean.qvcondicion_f.substring(6,10));
            
			if (!chbQVCONDICION_FN.isChecked() && !(dia>=1 && dia<=31) && dia!=98) {				
				txtCN_DIA.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Valor del campo dia es invalido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);				
				return false;		
			}
			if (!chbQVCONDICION_FN.isChecked() && !(mes>=1 && mes<=12)) {				
				txtCN_MES.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Valor del campo mes invalido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);				
				return false;
			}
			if (!chbQVCONDICION_FN.isChecked() && anio<App.ANIOPORDEFECTOSUPERIOR-3 || anio>App.ANIOPORDEFECTOSUPERIOR) {
				txtCN_ANIO.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Valor del campo a�o es invalido",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return false;		
			}			
			
			
			
			if (Util.esVacio(bean.qvinformante)) {
				rgQVINFORMANTE.requestFocus();
				ToastMessage.msgBox(this.getActivity(),"Informante de la vivienda es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
        		return false;
			}
			if (!Util.esDiferente(bean.qvinformante,96)) {
				if(Util.esVacio(txtQVINFORMANTE_O)){
					txtQVINFORMANTE_O.requestFocus();
					ToastMessage.msgBox(this.getActivity(),"Especifique es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        		return false;
				}
			}
		}
       
        return true;
	}

	
	private boolean validarProximaVisita(Integer item) {
    		if(!Util.esDiferente(item, 4)){
    			if(txtFECHA_PROXIMA.getValue() == null){
    				txtFECHA_PROXIMA.requestFocus();
    				ToastMessage.msgBox(this.getActivity(),"Proxima Fecha es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            		return false;
    			}
    			if(txtHORA_PROXIMA.getValue() == null){
    				txtHORA_PROXIMA.requestFocus();
    				ToastMessage.msgBox(this.getActivity(),"proxima hora es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                	return false;
        		}
            
    		}
            if (txtFECHA_PROXIMA.getValue() == null || txtHORA_PROXIMA.getValue() == null &&  Util.esDiferente(item, 4)) {
               return true;
            }
            
            if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) > 0) {
                    txtFECHA_PROXIMA.requestFocus();
                    ToastMessage.msgBox(this.getActivity(),"Fecha Proxima no puede ser menor.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                    return false;
            } else if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) == 0) {
                    if (Util.compareTime((Date) txtHORA_FIN.getValue(),(Date) txtHORA_PROXIMA.getValue()) >= 0) {
                    	txtHORA_PROXIMA.requestFocus();
                    	ToastMessage.msgBox(this.getActivity(),"Hora Proxima no puede ser menor o igual.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                    	return false;
                    }
            }
           
            return true;
    }

    private void cargarDatos() {
    	getDialog().setTitle("Vivienda - Visita N� " + bean.nro_visita);
    	tracker = new CapturadorGPS(getActivity());

    	if (bean!=null) {
    		if (bean.qvcondicion!=null) {
				bean.qvcondicion=bean.getValue8a3(bean.qvcondicion);
			}
			if (bean.qvinformante!=null) {
				bean.qvinformante=bean.getValue96a9(bean.qvinformante);
			}
			
			if(bean.qvcondicion_f!=null){
				txtCN_DIA.setValue(bean.qvcondicion_f.substring(0,2));
	            txtCN_MES.setValue(bean.qvcondicion_f.substring(3,5));
	            txtCN_ANIO.setValue(bean.qvcondicion_f.substring(6,10));	
			}
			
		}
    	
    	entityToUI(bean);
            
    	caretaker.addMemento("antes", bean.saveToMemento(Visita_Viv.class));
    	
//    	Log.e("ini bean.qvvresul: ",""+bean.qvvresul);
    	
    	if(bean.qvvresul_o!=null && bean.qvvresul_o.equals("TRANSITORIA")) {
			chbResul_o1.setChecked(true);
			txtQVVRESUL_O.setText("");
		}
    	
    	if (action == ACTION.INICIAR) {
    		txtFECHA_INICIO.setValue(Calendar.getInstance().getTime());
    		txtHORA_INICIO.setValue(Calendar.getInstance().getTime());
    		
    	} 
    	else if (action == ACTION.FINALIZAR) {
    		Date fecha = Util.getFechaHora(bean.qvvanio_ini+"", bean.qvvmes_ini, bean.qvvdia_ini, bean.qvhora_ini, bean.qvmin_ini,  0+"");
    		txtFECHA_INICIO.setValue(fecha);
    		txtHORA_INICIO.setValue(fecha);
    		txtHORA_FIN.setValue(Calendar.getInstance().getTime());
                    
    	} 
    	else if (action == ACTION.EDITAR) {
    		spnQVVRESUL.readOnly();
    		spnQVVRESUL_R.readOnly();
    		chbResul_o1.readOnly();
    		txtQVVRESUL_O.readOnly();
    		
    		if(spnQVVRESUL.getSelectedItemKey()!=null){
    			if (spnQVVRESUL.getSelectedItemKey().toString().equals("2") || spnQVVRESUL.getSelectedItemKey().toString().equals("3") || spnQVVRESUL.getSelectedItemKey().toString().equals("4") || spnQVVRESUL.getSelectedItemKey().toString().equals("5")) {
    				if (bean.qvvhorap != null && bean.qvvhorap!=null) {
    					int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
    					Date fechaProx = Util.getFechaHora(anio+"", bean.qvvmesp,bean.qvvdiap , bean.qvvhorap , bean.qvvminup, 0+"");
    					txtFECHA_PROXIMA.setValue(fechaProx);
    					txtHORA_PROXIMA.setValue(fechaProx);    
    				}
    			}
    		}
                
    		Date fechaIni = Util.getFechaHora(App.ANIOPORDEFECTOSUPERIOR+"", bean.qvvmes_ini,bean.qvvdia_ini, bean.qvhora_ini,bean.qvmin_ini, 0+"");
    		Date fechaFin = Util.getFechaHora(App.ANIOPORDEFECTOSUPERIOR+"", bean.qvvmes_ini, bean.qvvdia_ini, bean.qvhora_ini, bean.qvmin_ini, 0+"");

    		txtFECHA_INICIO.setValue(fechaIni);
    		txtHORA_INICIO.setValue(fechaIni);
    		txtHORA_FIN.setValue(fechaFin);
    		
    	}  
//    	Log.e("fin bean.qvvresul: ",""+bean.qvvresul);
//    	Log.e("bean.qvcondicion_f: ",""+bean.qvcondicion_f);
    	
    	
    	inicio();
    }

    private void inicio() {
    	Hogar hogar= new Hogar();		
            if (action == ACTION.INICIAR) {
                    btnAceptar.requestFocus();
                    q2.setVisibility(View.GONE);
                    q3.setVisibility(View.GONE);
                    q5.setVisibility(View.GONE);
                    q6.setVisibility(View.GONE);
                    
            } 
            else 
            	if (action == ACTION.FINALIZAR) {
                    q2.setVisibility(View.VISIBLE);
                    q3.setVisibility(View.VISIBLE);
                    q5.setVisibility(View.VISIBLE);
                    q6.setVisibility(View.VISIBLE);                    
                    spnQVVRESUL.requestFocus();
            }
            onC1_RVISITAChangeValue(spnQVVRESUL);
            validarSiesSupervisora();
            txtCabecera.requestFocus();
    }

	public void validarSiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			Util.cleanAndLockView(getActivity(), btnGPS,btnAceptar);
			txtFECHA_INICIO.readOnly();
			txtHORA_INICIO.readOnly();
			txtHORA_FIN.readOnly();
			spnQVVRESUL.readOnly();
			spnQVVRESUL_R.readOnly();
			txtFECHA_PROXIMA.readOnly();
			txtHORA_PROXIMA.readOnly();
			rgQVCONDICION.readOnly();
			chbQVCONDICION_FN.readOnly();
			txtCN_DIA.readOnly();
			txtCN_MES.readOnly();
			txtCN_ANIO.readOnly();
			rgQVINFORMANTE.readOnly();
			txtQVINFORMANTE_O.readOnly();
		}
	}
	
	
	
    public void onC1_RVISITAChangeValue(FieldComponent component) {
        String resultadoStr = (String) component.getValue();
        if (resultadoStr.substring(0, 1).equals("5")) {
        	Util.lockView(getActivity(), false, spnQVVRESUL_R);
        	Util.cleanAndLockView(getActivity(), txtQVVRESUL_O, chbResul_o1);
        	spnQVVRESUL_R.setVisibility(View.VISIBLE);
        	txtQVVRESUL_O.setVisibility(View.GONE);
        	chbResul_o1.setVisibility(View.GONE);
        	spnQVVRESUL_R.requestFocus();
        	
        	Util.cleanAndLockView(getActivity(), rgQVCONDICION, rgQVINFORMANTE, txtQVINFORMANTE_O, txtCN_DIA, txtCN_MES, txtCN_ANIO,chbQVCONDICION_FN);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
        } 
        else {
        	Util.cleanAndLockView(getActivity(), spnQVVRESUL_R);
        	spnQVVRESUL_R.setVisibility(View.GONE);
        	if(resultadoStr.substring(0, 1).equals("9")){
        		Util.lockView(getActivity(), false, txtQVVRESUL_O, chbResul_o1);
        		txtQVVRESUL_O.setVisibility(View.VISIBLE);
        		chbResul_o1.setVisibility(View.VISIBLE);
        		
        		Util.lockView(getActivity(), false, rgQVCONDICION, rgQVINFORMANTE, txtCN_DIA, txtCN_MES, txtCN_ANIO,chbQVCONDICION_FN);
        		q5.setVisibility(View.VISIBLE);
        		q6.setVisibility(View.VISIBLE);
        		
            	validarEspecifique();
            	validarEspfueCondicion();
            	txtQVVRESUL_O.requestFocus();
        	}
        	else if(resultadoStr.substring(0, 1).equals("6") ||resultadoStr.substring(0, 1).equals("7")||resultadoStr.substring(0, 1).equals("8")){
        		Util.lockView(getActivity(), false, txtQVVRESUL_O);
        		Util.cleanAndLockView(getActivity(), chbResul_o1);
        		txtQVVRESUL_O.setVisibility(View.VISIBLE);
        		chbResul_o1.setVisibility(View.GONE);
        		
        		Util.lockView(getActivity(), false, rgQVCONDICION, rgQVINFORMANTE, txtCN_DIA, txtCN_MES, txtCN_ANIO,chbQVCONDICION_FN);
        		q5.setVisibility(View.VISIBLE);
        		q6.setVisibility(View.VISIBLE);
            	validarEspecifique();
            	validarEspfueCondicion();
            	txtQVVRESUL_O.requestFocus();
        	}
        	else{            	
        		Util.cleanAndLockView(getActivity(), txtQVVRESUL_O, chbResul_o1);
        		txtQVVRESUL_O.setVisibility(View.GONE);
        		chbResul_o1.setVisibility(View.GONE);
        		
        		Util.cleanAndLockView(getActivity(), rgQVCONDICION, rgQVINFORMANTE, txtQVINFORMANTE_O, txtCN_DIA, txtCN_MES, txtCN_ANIO,chbQVCONDICION_FN);
        		q5.setVisibility(View.GONE);
        		q6.setVisibility(View.GONE);
        		
        		
        	}
        }
        
        if (resultadoStr.substring(0, 1).equals("2")|| resultadoStr.substring(0, 1).equals("3")|| resultadoStr.substring(0, 1).equals("4")|| resultadoStr.substring(0, 1).equals("5")) {
        	Util.lockView(getActivity(), false, txtFECHA_PROXIMA,txtHORA_PROXIMA);
        	txtFECHA_PROXIMA.requestFocus();
        } 
        else {
        	Util.cleanAndLockView(getActivity(), txtFECHA_PROXIMA,txtHORA_PROXIMA);
        }
    }
    
    
    public void validarEspecifique(){
    	if(chbResul_o1.isChecked()){
			Util.cleanAndLockView(getActivity(), txtQVVRESUL_O);
		}
		else{
			Util.lockView(getActivity(), false, spnQVVRESUL_R);
		}
    }
    public void validarEspfueCondicion(){
    	if(chbQVCONDICION_FN.isChecked()){
			Util.cleanAndLockView(getActivity(), txtCN_DIA, txtCN_MES, txtCN_ANIO);
			bean.qvcondicion_f=null;
			rgQVINFORMANTE.requestFocus();
		}
		else{
			Util.lockView(getActivity(), false, txtCN_DIA, txtCN_MES, txtCN_ANIO);
			rgQVINFORMANTE.requestFocus();
		}
    }
    

    private void cargarSpinner() {

            if (esResultado3) {
                    List<Object> keys = new ArrayList<Object>();
                    keys.add(null);
                    keys.add(1);
                    keys.add(10);
                    String[] items1 = new String[] { " --- Seleccione --- ","1. VIVIENDA COMPLETA", "10. VIVIENDA INCOMPLETA" };
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                    spnQVVRESUL.setAdapterWithKey(adapter, keys);
            } 
            else {
//            	Log.e("action: ",""+action);
            	if(action == ACTION.EDITAR){
            		List<Object> keys = new ArrayList<Object>();
                    keys.add(null);
                    keys.add(1);
                    keys.add(2);
                    keys.add(3);
                    keys.add(4);
                    keys.add(5);
                    keys.add(6);
                    keys.add(7);
                    keys.add(8);
                    keys.add(9);
                    keys.add(10);
                    String[] items1 = new String[] { " --- Seleccione --- ",
                    				"1. VIVIENDA COMPLETA",
                                    "2. VIVIENDA OCUPADA PERO CON ENTREVISTADO COMPETENTE AUSENTE", 
                                    "3. VIVIENDA AUSENTE",
                                    "4. VIVIENDA APLAZADA",
                                    "5. VIVIENDA RECHAZADA", 
                                    "6. VIVIENDA DESOCUPADA O NO ES VIVIENDA",
                                    "7. VIVIENDA DESTRUIDA",
                                    "8. VIVIENDA NO EXISTE",
                                    "9. OTRO",
                                    "10. VIVIENDA INCOMPLETA" 
                    };
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                    spnQVVRESUL.setAdapterWithKey(adapter, keys);
            	}
            	else{
                    List<Object> keys = new ArrayList<Object>();
                    keys.add(null);
//                    keys.add(1);
                    keys.add(2);
                    keys.add(3);
                    keys.add(4);
                    keys.add(5);
                    keys.add(6);
                    keys.add(7);
                    keys.add(8);
                    keys.add(9);
                    String[] items1 = new String[] { " --- Seleccione --- ",
//                    				"1. VIVIENDA COMPLETA",
                                    "2. VIVIENDA OCUPADA PERO CON ENTREVISTADO COMPETENTE AUSENTE", 
                                    "3. VIVIENDA AUSENTE",
                                    "4. VIVIENDA APLAZADA",
                                    "5. VIVIENDA RECHAZADA", 
                                    "6. VIVIENDA DESOCUPADA O NO ES VIVIENDA",
                                    "7. VIVIENDA DESTRUIDA",
                                    "8. VIVIENDA NO EXISTE",
                                    "9. OTRO",
//                                    "10. VIVIENDA INCOMPLETA" 
                    };
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                    spnQVVRESUL.setAdapterWithKey(adapter, keys);
            	}
            }
    }
    
    
    private void cargarSpinnerRRechazo() { 
                List<Object> keys = new ArrayList<Object>();
                keys.add(null);
                keys.add(1);
                keys.add(2);
                keys.add(3);

                String[] items1 = new String[] { " --- Seleccione --- ",
                                "1. NO DESEAN LA ENTREVISTA", 
                                "2. YA FUE ENTREVISTADA POR LA ENDES",
                                "3. YA FUE ENTREVISTADA POR OTRA ENCUESTA DEL INEI",                
                };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                spnQVVRESUL_R.setAdapterWithKey(adapter, keys);        
    }
    
	private void capturarGPS() {   
		if (tracker == null) {
			ToastMessage.msgBox(this.getActivity(), "No se puede capturar GPS.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			return;
		}
		
		FragmentManager fm = this.getFragmentManager();
		GPSDialog gps = GPSDialog.newInstance((IGPSDialog) this, tracker).estilo(App.ESTILO).estiloBoton(App.ESTILO_BOTON).property(IGPSDialog.ACCURACY);
		gps.setAncho(MATCH_PARENT);
		gps.show(fm, "gpsDialog");
	}
	
	private void cargarMapa(Double lati, Double longi, String msj) {
		final double lat = lati;
		final double lon = longi;
		final String name = msj;
		MapsWithMeApi.showPointOnMap(getActivity(), lat, lon, name);
	}
    
    
	public void postShow(Map<String, String> properties) {
		
		String longitud;
		String latitud;
		String altitud;
		if (properties != null) {
			longitud = properties.get(IGPSDialog.LONGITUD);
			latitud = properties.get(IGPSDialog.LATITUD);
			altitud = properties.get(IGPSDialog.ALTURA);
		} else {
			return;
		}

		txtQVGPS_LONG.setText(longitud);
		txtQVGPS_LAT.setText(latitud);
		txtQVGPS_ALT.setText(altitud);
	}
	
    public CuestionarioService getService() {
        if (service == null) {
            service = CuestionarioService.getInstance(getActivity());
        }
        return service;
    }
	
    private VisitaService getVisitaService() {
        if (visitaService == null) {
                visitaService = VisitaService.getInstance(getActivity());
        }
        return visitaService;
    }

	private CuestionarioService getCuestionarioService() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private HogarService getHogarService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Fragment getForm() {
		// TODO Auto-generated method stub
		return this.getParent();
	}
}
