package gob.inei.endes2024.fragment.visita.Dialog;

import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.caratula.CaratulaFragmentCI;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CI_VisitaDialog extends DialogFragmentComponent {
    @FieldAnnotation(orderIndex = 1)
    public DateTimeField txtFECHA_INICIO;
    @FieldAnnotation(orderIndex = 2)
    public SpinnerField spnQIVRESUL;
    @FieldAnnotation(orderIndex = 3)
    public TextField txtQIVRESUL_O;
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQITOTALN;
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQITOTALC;
    @FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI106;
    @FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS28; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS29A; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS29B;

    
    public List<Integer> resul_1;
    public List<Integer> resul_fila;
    public Individual detallesindividual;

    public ButtonComponent btnAceptar,btnCancelar;
    private CuestionarioService service;
    Visita bean;    
    private LabelComponent lblvih,lblsida,lblenanios,lblpregunta106,lblpregunta28,lblpregunta29,lbledadCH,lbledadCH2,lblFecha,lblResutado,lblEspe,lblvisitafinal,lblTotalN,lblTotalC;
    private static CaratulaFragmentCI caller;    
    private GridComponent grid1,grid2,grid11,gridTextoedad;
    public GridComponent2 gridPregunta106, gridPreguntas29;
    private VisitaService visitaService;
    private SeccionCapitulo[] seccionesCargado,seccionesGrabado,seccionesCargadoS,seccionesGrabadoS,seccionesGrabadoSec1_MA24,seccionesGrabadoSec1_ME24,seccionesGrabadoVisitaSalud;
    
    public Individual bean_i;
    private CuestionarioService cuestionarioService; 
    boolean solo_una=false;
	Salud salud; 
	
	public boolean es_informante;
	public Seccion01 informantedesalud;
	public Seccion01Service seccion01;
	public VisitaService visita;
	public Seccion01 persona;
    
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
    LinearLayout q3;
    LinearLayout q4;
	LinearLayout q5;
    
    public CI_VisitaDialog() {
    	super();
    	solo_una=false;
    	seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH02_1","QH02_2","QH02_3","QH06","QH07","QIVDIA","QIVMES","QIVANIO","QIVRESUL","QIVHORA","QIVMIN","QIVENTREV","QIVRESUL_O","QI106","QITOTALN","QITOTALC")};
    	
    	seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QIVDIA","QIVMES","QIVANIO","QIVHORA","QIVMIN","QIVENTREV","QIVRESUL","QIVRESUL_O","QI106","QITOTALN","QITOTALC") };
    	seccionesCargadoS = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS28", "QS29A", "QS29B")};
    	seccionesGrabadoS = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1, "QS28", "QS29A", "QS29B") };
    	
    	seccionesGrabadoSec1_MA24 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A")};
    	seccionesGrabadoSec1_ME24 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07")};
    	
    	seccionesGrabadoVisitaSalud = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"QSRESULT","QSRESULT_O","QSVDIA","QSVMES","QSVANIO","QSVHORA_INI","QSVMINUTO_INI")};
			
    }
    
    public static CI_VisitaDialog newInstance(FragmentForm pagina, Individual detalle) {
		caller = (CaratulaFragmentCI)pagina;
		CI_VisitaDialog f = new CI_VisitaDialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            bean = (Visita) getArguments().getSerializable("bean");
            caretaker = new Caretaker<Entity>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            final View rootView = createUI();
            initObjectsWithoutXML(this, rootView);
            cargarDatos();
            enlazarCajas(this);
            listening();
            return rootView;
    }

    @Override
    protected void buildFields() {
    	
		lblvisitafinal = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.v_l_VF).textSize(17).centrar().negrita();
		lblFecha = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.v_l_fecha_ini).textSize(17);
		lblResutado = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.v_l_resultado).textSize(17);
		lblEspe = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.v_l_especifique).textSize(17);
		
		txtFECHA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM/yyyy").size(altoComponente + 20, 200);
		txtFECHA_INICIO.setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
		
		spnQIVRESUL = new SpinnerField(getActivity()).size(altoComponente + 15,350).callback("onC1_RVISITAChangeValue");
		cargarSpinner();
		txtQIVRESUL_O = new TextField(getActivity()).size(altoComponente, 350).maxLength(100).hint(R.string.especifique).soloTexto();     
		
		lbledadCH = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.cap01_03qstexto2).textSize(16).colorTexto(color.red);
		lbledadCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();		
	
		lblpregunta106 = new LabelComponent(this.getActivity()).text(R.string.vi_qi106).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblenanios = new LabelComponent(this.getActivity()).size(60, 300).text(R.string.vi_qiedad).textSize(16).centrar();
		txtQI106=new IntegerField(this.getActivity()).size(60, 80).maxLength(2);
		
		lblTotalN = new LabelComponent(this.getActivity()).size(60, 300).text(R.string.v_l_TotalN).textSize(16).centrar();
		txtQITOTALN=new IntegerField(this.getActivity()).size(60, 80).maxLength(1);
		lblTotalC = new LabelComponent(this.getActivity()).size(60, 300).text(R.string.v_l_TotalC).textSize(16).centrar();
		txtQITOTALC=new IntegerField(this.getActivity()).size(60, 80).maxLength(1);
            			
		lblpregunta28 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap4_qi489);
		rgQS28=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS28ChangeValue");
            
		lblpregunta29 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap8_qi801);
		lblvih = new LabelComponent(this.getActivity()).size(altoComponente, 380).text(R.string.cap8_qi801_vih).textSize(19).centrar();
		rgQS29A=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQS29aChangeValue");; 
		lblsida = new LabelComponent(this.getActivity()).size(altoComponente,380).text(R.string.cap8_qi801_sida).textSize(19).centrar();
		rgQS29B=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(altoComponente,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQS29bChangeValue");;
               		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
       				
		grid1 = new GridComponent(getActivity(), Gravity.CENTER, 2, 0);
		grid1.addComponent(lblvisitafinal,2);
		grid1.addComponent(lblFecha);
		grid1.addComponent(txtFECHA_INICIO);
		grid1.addComponent(lblResutado);
		grid1.addComponent(spnQIVRESUL);
		
		grid11 = new GridComponent(getActivity(), Gravity.CENTER, 2, 0);	
		grid11.addComponent(lblEspe);
		grid11.addComponent(txtQIVRESUL_O);

		grid2 = new GridComponent(getActivity(), Gravity.CENTER, 2, 0);
		grid2.addComponent(lblTotalN);
		grid2.addComponent(txtQITOTALN);
		grid2.addComponent(lblTotalC);
		grid2.addComponent(txtQITOTALC);
            
		gridTextoedad = new GridComponent(getActivity(),Gravity.CENTER, 2, 0);
		gridTextoedad.addComponent(lbledadCH);		
		gridTextoedad.addComponent(lbledadCH2);	
		
		gridPregunta106=new GridComponent2(this.getActivity(),Gravity.CENTER,2,0);
		gridPregunta106.addComponent(lblenanios);
		gridPregunta106.addComponent(txtQI106);
		
		gridPreguntas29=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas29.addComponent(lblvih);		
		gridPreguntas29.addComponent(rgQS29A);
		gridPreguntas29.addComponent(lblsida);
		gridPreguntas29.addComponent(rgQS29B);
		
        
	btnCancelar.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			bean_i = new Individual();
			CI_VisitaDialog.this.dismiss();
		}
	});
	btnAceptar.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			boolean flag = grabar();
			if (!flag) {
				return;
			}
			caller.cargarDatos();
			CI_VisitaDialog.this.dismiss();
		}
	});  
    }	
    
    @Override
    protected View createUI() {
    	buildFields();
    	q0 = createQuestionSection(grid1.component(),grid11.component());
    	q1 = createQuestionSection(grid2.component());
    	q2 = createQuestionSection(lblpregunta106,gridTextoedad.component(),gridPregunta106.component()); 
    	q3 = createQuestionSection(lblpregunta28,rgQS28); 
    	q4 = createQuestionSection(lblpregunta29,gridPreguntas29.component()); 
    	
    	LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
    	ScrollView contenedor = createForm();
    	LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
    	
    	form.addView(q0);
    	form.addView(q1);
    	form.addView(q2);
    	form.addView(q3); 
    	form.addView(q4); 
    	form.addView(botones);
    	return contenedor;
    }

    private VisitaService getVisitaService() {
    	if (visitaService == null) {
    		visitaService = VisitaService.getInstance(getActivity());
    	}
    	return visitaService;
    }
    
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
    }
    
    private VisitaService getServiceVisita() {
        if (visita == null) {
        	visita = VisitaService.getInstance(getActivity());
        }
        return visita;
    }

    private boolean grabar() {
    	
    	uiToEntity(bean_i);
    	if(txtFECHA_INICIO.length()>0){
		bean_i.qivanio = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
    	bean_i.qivdia=Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "dd");
    	bean_i.qivmes =Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "MM");
    	}

    	//Log.e("","DIAS  "+bean_i.qivmes);
    	grabarResultado();
    	
    	if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
      
    	boolean flag = true;
    	
    	persona = new Seccion01();
    	persona.id = caller.mef.id;
    	persona.hogar_id = caller.mef.hogar_id;
    	persona.persona_id = caller.mef.persona_id;
//    	persona.qh07 = bean_i.qi106;
    	    	

    	try { 
    		
    		flag = getCuestionarioService().saveOrUpdate(bean_i,seccionesGrabado);
    		
    		if (!flag) {
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados!!!.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}
    		
//    		if(!Util.esDiferente(bean_i.qivresul,1)){
//    			if (Util.esMenor(bean_i.qi106,25)) {
//        			Log.e("","Menor a 25");
//        			flag = getCuestionarioService().saveOrUpdate(persona,seccionesGrabadoSec1_ME24);	
//    			}
//        		else{
//         			Log.e("","Mayor a 24");
//        			flag = getCuestionarioService().saveOrUpdate(persona,seccionesGrabadoSec1_MA24);
//        		}        		  
//    		}
    	  		
    		
    		if (!flag) {
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados!!!.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados!!!.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}
			if(es_informante){
				flag = getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoS);
				if(Util.esDiferente(bean_i.qivresul, 1)){
//				Log.e("sss","ddd");
				flag = getVisitaService().saveOrUpdate(CrearVisitaSalud(),seccionesGrabadoVisitaSalud);
				}
				if(!flag){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			
		}
    	return flag;
    }
    
    private boolean validar(){
    	String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
    	if (Util.esVacio(bean_i.qivanio)) {
			error = true;
			view = txtFECHA_INICIO;
			mensaje = "Fecha Inicio vacia!!!";
			return false;
		}
    	if (Util.esVacio(bean_i.qivresul)) {
			error = true;
			view = spnQIVRESUL;
			mensaje = "Seleccione Resultado!!!";
			return false;
		}
    	if(!Util.esDiferente(bean_i.qivresul, 7)){    		
    		if (Util.esVacio(bean_i.qivresul_o)) {
    			error = true;
    			view = txtQIVRESUL_O;
    			mensaje = "Ingrese especifique!!!";
    			return false;
    		}	
    	} 
    	if (!Util.esDiferente(bean_i.qivresul,1)) {
//    		if(Util.esVacio(bean_i.qitotaln)){
//        		mensaje = preguntaVacia.replace("$", "La pregunta total de ni�os < 5 a�os"); 
//    			view = txtQITOTALN; 
//    			error = true; 
//    			return false; 
//        	}
//    		if(Util.esVacio(bean_i.qitotalc)){
//        		mensaje = preguntaVacia.replace("$", "La pregunta total ni�os sin carn�"); 
//    			view = txtQITOTALC; 
//    			error = true; 
//    			return false;
//        	}    		
//    		if(Util.esVacio(bean_i.qi106)){
//        		mensaje = preguntaVacia.replace("$", "La pregunta P.106"); 
//    			view = txtQI106; 
//    			error = true; 
//    			return false; 
//        	}
//        	if(!MyUtil.incluyeRango(15,49,bean_i.qi106)){
//        		mensaje = "La edad no corresponde...Ingrese valor ente 15 y 49!!!";
//    			view = txtQI106; 
//    			error = true; 
//    			return false; 
//        	}
        	if(es_informante){
        		if (Util.esVacio(salud.qs28)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta P.489"); 
    				view = rgQS28; 
    				error = true; 
    				return false; 
    			} 
    			if (Util.esVacio(salud.qs29a)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta P.801A"); 
    				view = rgQS29A; 
    				error = true; 
    				return false; 
    			} 
    			if (Util.esVacio(salud.qs29b)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta P.801B"); 
    				view = rgQS29B; 
    				error = true; 
    				return false; 
    			}
        	}
        	
//        	if(!Util.esDiferente(bean_i.qitotalc,1)){
//    			if (Util.esMayor(bean_i.qitotaln,4)) {
//    				mensaje = preguntaVacia.replace("$", "Fuera de rango"); 
//        			view = txtQITOTALN; 
//        			error = true; 
//        			return false; 	
//				}        		
//        	}
		}
    	Calendar date = new GregorianCalendar();
    	if (Util.compare((Date) txtFECHA_INICIO.getValue(),
                 date.getTime()) > 0){
    		mensaje ="Fecha no puede ser Mayor al d�a de Hoy";
			view = txtFECHA_INICIO; 
			error = true; 
			return false; 
    	}
    	return true;
    }    

    public CSVISITA CrearVisitaSalud(){
    	CSVISITA visita;
    	visita= new CSVISITA();
    	visita.id=App.getInstance().getMarco().id;
    	visita.hogar_id=App.getInstance().getHogar().hogar_id;
    	visita.persona_id=bean_i.persona_id;
    	visita.qsvdia=bean_i.qivdia;
    	visita.qsvmes=bean_i.qivmes;
    	visita.qsvanio=bean_i.qivanio;
    	visita.qsresult=bean_i.qivresul;
    	//Log.e("","RESULTADO "+visita.qsresult);
    	visita.qsresult_o=bean_i.qivresul_o;
    	visita.nro_visita=1;
    	Calendar calendario=new GregorianCalendar();
    	visita.qsvhora_ini=calendario.get(Calendar.HOUR_OF_DAY)+"";
    	visita.qsvminuto_ini=calendario.get(Calendar.MINUTE)+"";
    	return visita;
    }
    private void cargarDatos() {
    	MyUtil.LiberarMemoria();
    	getDialog().setTitle("Visita Cuestionario Individual");
    	antes();
    	cargarResultado();
    	informanteSalud();
    	entityToUI(bean_i);
    	if(bean_i.qivdia!=null){
    		Date fecha = Util.getFecha(bean_i.qivanio, Integer.parseInt(bean_i.qivmes),
    				Integer.parseInt(bean_i.qivdia));
		    txtFECHA_INICIO.setValue(fecha);
    	}
        inicio();
    }

    private void inicio() {
    	onC1_RVISITAChangeValue();
    	lbledadCH2.setText(caller.mef.qh07.toString());    	
    	ValidarsiesSupervisora();
    }
    
	public void onC1_RVISITAChangeValue(){
		MyUtil.LiberarMemoria();
		if ( spnQIVRESUL.getSelectedItemKey()!=null && !spnQIVRESUL.getSelectedItemKey().toString().equals("1")) {
			if (spnQIVRESUL.getSelectedItemKey().toString().equals("6")) {	
//				Log.e("aaa","1");			
				q1.setVisibility(View.GONE);				
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);				
				grid11.setVisibility(View.VISIBLE);				
				Util.lockView(this.getActivity(),false,lblEspe,txtQIVRESUL_O);
				Util.cleanAndLockView(this.getActivity(), txtQITOTALN,txtQITOTALC,txtQI106,rgQS28,rgQS29A,rgQS29A,rgQS29B);		
			}
			else{	
//				Log.e("aaa","2");				
				Util.cleanAndLockView(this.getActivity(), lblEspe,txtQIVRESUL_O);				
				q1.setVisibility(View.GONE);				
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				grid11.setVisibility(View.GONE);
				Util.cleanAndLockView(this.getActivity(), txtQITOTALN,txtQITOTALC,txtQI106,rgQS28,rgQS29A,rgQS29A,rgQS29B);	
			}			
		}
		else{		
//			Log.e("aaa","3");
			Util.cleanAndLockView(this.getActivity(),lblEspe,txtQIVRESUL_O);
			Util.lockView(this.getActivity(),false,txtQITOTALN,txtQITOTALC,txtQI106,rgQS28,rgQS29A,rgQS29A,rgQS29B);
			q1.setVisibility(View.VISIBLE);				
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			grid11.setVisibility(View.GONE);
			informanteSalud();
		}	
	}

    public CuestionarioService getService() {
            if (service == null) {
                    service = CuestionarioService.getInstance(getActivity());
            }
            return service;
    }

    private void cargarSpinner() {
    	List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        keys.add(1);
        keys.add(2);
        keys.add(3);
        keys.add(4);
        keys.add(5);
        keys.add(6);
        String[] items1 = new String[] { " --- Seleccione --- ",
                        "1. COMPLETA", 
                        "2. AUSENTE",
                        "3. APLAZADA", 
                        "4. RECHAZADA",                       
                        "6. DISCAPACITADA",
                        "7. OTRA"        
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQIVRESUL.setAdapterWithKey(adapter, keys);
    }
    
    public void fecha_hora_hoy(){    	  
    	txtFECHA_INICIO.setValue(Calendar.getInstance().getTime());
    	
    	Calendar calendario = new GregorianCalendar();
    	bean_i.qivdia = "" + calendario.get(Calendar.DAY_OF_MONTH);
    	bean_i.qivmes = "" + (calendario.get(Calendar.MONTH)+1);
    	bean_i.qivanio = calendario.get(Calendar.YEAR);
    }   
    
    public void antes(){
    	bean_i = new Individual();
    	
    	detallesindividual= getCuestionarioService().getListadoIndividualPorPersona(App.getInstance().getMarco().id,
    																				App.getInstance().getHogar().hogar_id,
    																	  caller.mef.persona_id,
    																	  seccionesCargado);
    	
    	bean_i.id = App.getInstance().getMarco().id;
    	bean_i.hogar_id = App.getInstance().getHogar().hogar_id;
    	//Log.e("persona_id","persona_id"+detallesindividual.persona_id);
    	//Log.e("persona_id","persona_id"+App.getInstance().getMarco().id+" - "+ App.getInstance().getHogar().hogar_id+" - "+ caller.mef.persona_id);
    	bean_i.persona_id = detallesindividual.persona_id;
    	bean_i.qivdia = detallesindividual.qivdia;
    	bean_i.qivmes = detallesindividual.qivmes;
    	bean_i.qivanio = detallesindividual.qivanio;
    	bean_i.qivresul = detallesindividual.qivresul;
    	bean_i.qivresul_o = detallesindividual.qivresul_o;
//    	bean_i.qi106 = detallesindividual.qi106;
//    	bean_i.qitotaln = detallesindividual.qitotaln;
//    	bean_i.qitotalc = detallesindividual.qitotalc;
    	
    	//bean_i.id = caller.mef.id;
    	//bean_i.hogar_id = caller.mef.hogar_id;
    	/*bean_i.persona_id = caller.mef.persona_id;
    	bean_i.qivdia = caller.mef.qivdia;
    	bean_i.qivmes = caller.mef.qivmes;
    	bean_i.qivanio = caller.mef.qivanio;
    	bean_i.qivresul = caller.mef.qivresul;
    	bean_i.qivresul_o = caller.mef.qivresul_o;
    	bean_i.qi106 = caller.mef.qi106;
    	bean_i.qitotaln = caller.mef.qitotaln;
    	bean_i.qitotalc = caller.mef.qitotalc;*/
    	
    }
    
    public void cargarResultado(){
    	if(bean_i.qivresul!=null){
	    	switch(bean_i.qivresul){
	    		case 6 : bean_i.qivresul = 5; break;
	    		case 7 : bean_i.qivresul = 6; break;    		
	    	}
    	}
    }
    
    public void grabarResultado(){
    	if(bean_i.qivresul!=null){
	    	switch(bean_i.qivresul){
	    		case 5 : bean_i.qivresul = 6; break;
	    		case 6 : bean_i.qivresul = 7; break;    		
	    	}
    	}
    }
    
    public void informanteSalud(){    	
    	Integer persona_id=MyUtil.SeleccionarInformantedeSalud(caller.mef.id, caller.mef.hogar_id, getServiceVisita(), getServiceSeccion01());
    	
    	if(persona_id.equals(caller.mef.persona_id)){
    		es_informante=true;
	    	
	    	salud = getCuestionarioService().getCAP01_03(caller.mef.id, caller.mef.hogar_id, caller.mef.persona_id,seccionesCargadoS);
//	    	Log.e("",""+salud);
	    	if(salud==null){ 
	    		salud=new Salud(); 
	    		salud.id=bean_i.id; 
	    		salud.hogar_id=bean_i.hogar_id; 
	    		salud.persona_id=bean_i.persona_id; 
	    	}
	    	
	    	if(salud.qs28!=null){
	    		bean_i.qs28 = salud.qs28; 
	    	}
	    	if(salud.qs29a!=null){
	    		bean_i.qs29a = salud.qs29a; 
	    	}
	    	if(salud.qs29b!=null){
	    		bean_i.qs29b = salud.qs29b; 
	    	}
    	}else{
    		es_informante=false;
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	}
    }
    
    public void onqrgQS28ChangeValue(){
    	if (MyUtil.incluyeRango(1,2,rgQS28.getTagSelected("").toString())) {
    		salud.qs28 = Integer.parseInt(rgQS28.getTagSelected("").toString());
    	}
    	rgQS29A.requestFocus();
    }
    
    public void onqrgQS29aChangeValue(){
    	if (MyUtil.incluyeRango(1,2,rgQS29A.getTagSelected("").toString())) {
    		salud.qs29a = Integer.parseInt(rgQS29A.getTagSelected("").toString());
    	}
    	rgQS29B.requestFocus();
    }
    
    public void onqrgQS29bChangeValue(){
    	if (MyUtil.incluyeRango(1,2,rgQS29B.getTagSelected("").toString())) {
    		salud.qs29b = Integer.parseInt(rgQS29B.getTagSelected("").toString());
    	}
    	btnAceptar.requestFocus();
    }
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtFECHA_INICIO.readOnly();
			spnQIVRESUL.readOnly();
			txtQIVRESUL_O.readOnly();
			txtQITOTALC.readOnly();
			txtQITOTALN.readOnly();
			txtQI106.readOnly();
			rgQS28.readOnly();
			rgQS29A.readOnly();
			rgQS29B.readOnly();
		}
	}
}