package gob.inei.endes2024.fragment.visita.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.SHOW_HIDE;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.visita.VisitaFragment_001;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.mapswithme.maps.api.MapsWithMeApi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;


public class VisitaDialog extends DialogFragmentComponent {
    @FieldAnnotation(orderIndex = 1)
    public DateTimeField txtFECHA_INICIO;
    @FieldAnnotation(orderIndex = 2)
    public DateTimeField txtHORA_INICIO;
    @FieldAnnotation(orderIndex = 3)
    public DateTimeField txtHORA_FIN;
    @FieldAnnotation(orderIndex = 4)
    public SpinnerField spnQHVRESUL;
    @FieldAnnotation(orderIndex = 5)
    public SpinnerField spnQHVRESUL_R;
    @FieldAnnotation(orderIndex = 6)
//    public TextField txtQHVRESUL_O;
//    @FieldAnnotation(orderIndex = 7)
    public DateTimeField txtFECHA_PROXIMA;
    @FieldAnnotation(orderIndex = 7)
    public DateTimeField txtHORA_PROXIMA;
    
    
	
	public ButtonComponent btnIniciar,btnGPS,btnVerenMapa,btnAceptar,btnCancelar;	
		
    public List<Integer> resul_1;
    public List<Integer> resul_fila;
    
    private CuestionarioService service;
    
    private LabelComponent lblBlanco3, lblCordenadas1,lblCordenadas2,lblCordenadas3,lblblanco,lblblanco1,lblFecha,lblHoraIni,lblHoraFin,lblResutado,lblfechaproxima,lblhoraproxima;
    private GridComponent2 tblGps;
    private GridComponent grid1,grid2, grproxvisita;
    
    private static FragmentForm caller;    
    private boolean esResultado3 = false;
    
    LinearLayout q1, q2, q3, q4;
    Visita bean;
    
    public static enum ACTION {
       INICIAR, EDITAR, FINALIZAR
    };

    private ACTION action;
    private VisitaService visitaService;

    public VisitaDialog() {
    }
    
    public interface C1_Cap00Fragment_001_01Listener {
            void onFinishEditDialog(String inputText);
    }

    public static VisitaDialog newInstance(FragmentForm pagina, Visita bean) {
            return newInstance(pagina, bean, ACTION.INICIAR);
    }

    public static VisitaDialog newInstance(FragmentForm pagina, Visita bean,ACTION action) {
            caller = pagina;
            VisitaDialog f = new VisitaDialog();
            f.setParent(pagina);
            Bundle args = new Bundle();
            args.putSerializable("bean", bean);
            args.putSerializable("action", action);
            f.setArguments(args);
            return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            bean = (Visita) getArguments().getSerializable("bean");
            action = (ACTION) getArguments().getSerializable("action");
            caretaker = new Caretaker<Entity>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            final View rootView = createUI();
            initObjectsWithoutXML(this, rootView);
            cargarDatos();
            enlazarCajas(this);
            listening();
            return rootView;
    }


    @Override
    protected void buildFields() {
    	lblFecha = new LabelComponent(getActivity()).size(altoComponente, 100).negrita().text(R.string.v_l_fecha_ini);
        lblHoraIni = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_ini);        
        txtFECHA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM/yyyy").size(altoComponente, 180).readOnly();
        txtFECHA_INICIO.setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);            
        txtHORA_INICIO = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();
        
        grid1 = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
        grid1.addComponent(lblFecha);
        grid1.addComponent(txtFECHA_INICIO);
        grid1.addComponent(lblHoraIni);
        grid1.addComponent(txtHORA_INICIO);
        
        lblHoraFin = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_hora_fin);
        lblResutado = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_l_resultado);
        lblblanco = new LabelComponent(this.getActivity()).size(altoComponente, 150);
        lblblanco1 = new LabelComponent(this.getActivity()).size(altoComponente, 450);        
        txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100).readOnly();             
        txtHORA_FIN = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA,"HH:mm").size(altoComponente, 100);        
        spnQHVRESUL 	= new SpinnerField(getActivity()).size(altoComponente + 15,550).callback("onC1_RVISITAChangeValue");
        spnQHVRESUL_R 	= new SpinnerField(getActivity()).size(altoComponente + 15,550);

        grid2 = new GridComponent(getActivity(), Gravity.CENTER, 3, 0);
        grid2.addComponent(lblHoraFin);            
        grid2.addComponent(txtHORA_FIN);
        grid2.addComponent(lblblanco1);
        grid2.addComponent(lblResutado);
        grid2.addComponent(spnQHVRESUL,2);
        grid2.addComponent(lblblanco);
        grid2.addComponent(spnQHVRESUL_R,2);
        
        cargarSpinner();
        cargarSpinnerRRechazo();
        
            lblfechaproxima = new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_fecha_prox);        
            lblhoraproxima =  new LabelComponent(getActivity()).size(altoComponente, 150).negrita().text(R.string.v_hora_prox);
            txtFECHA_PROXIMA = new DateTimeField(getActivity(), TIPO_DIALOGO.FECHA,"dd/MM").size(altoComponente, 180).showObject(SHOW_HIDE.CALENDAR).setRangoYear(App.ANIOPORDEFECTOSUPERIOR, App.ANIOPORDEFECTOSUPERIOR);
            txtHORA_PROXIMA  = new DateTimeField(getActivity(), TIPO_DIALOGO.HORA, "HH:mm").size(altoComponente, 100);
                        
            grproxvisita = new GridComponent(getActivity(), Gravity.CENTER, 4, 0);
            grproxvisita.addComponent(lblfechaproxima);
            grproxvisita.addComponent(txtFECHA_PROXIMA);
            grproxvisita.addComponent(lblhoraproxima);
            grproxvisita.addComponent(txtHORA_PROXIMA);
            
           
    		
            btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
            btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
            
            btnCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                          VisitaDialog.this.dismiss();
                    }
            });
            
            btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            boolean flag = grabar();
                            if (!flag) {
                                    return;
                            }
                            ((VisitaFragment_001) caller).recargarLista();
                            App.getInstance().setVisita(bean);
                            VisitaDialog.this.dismiss();                           
                    }
            });

    }
    
    @Override
    protected View createUI() {
            buildFields();
            q1 = createQuestionSection(R.string.pregunta01, grid1.component());
            q2 = createQuestionSection(grid2.component());
            q3 = createQuestionSection(R.string.v_l_proxima_visita, grproxvisita.component());            
            LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
            ScrollView contenedor = createForm();
            LinearLayout form = (LinearLayout) contenedor.getChildAt(0);            
            form.addView(q1);
            form.addView(q2);
            form.addView(q3);
            form.addView(botones);            
            return contenedor;
    }
    

    private boolean grabar() {
			uiToEntity(bean);
			if (action == ACTION.INICIAR) {
            		bean.qhvanio_ini = Integer.parseInt(Util.getFechaFormateada(new Date(), "yyyy"));
                    bean.qhvdia_ini=Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "dd");
                    bean.qhvmes_ini =Util.getFechaFormateada((Date) txtFECHA_INICIO.getValue(), "MM");
                    bean.qhhora_ini  = Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "HH");
                    bean.qhmin_ini =Util.getFechaFormateada((Date) txtHORA_INICIO.getValue(), "mm");
//                    Log.e("DIA: ",""+bean.qhvdia_ini);
//                    Log.e("HORA: ",""+bean.qhvmes_ini);
            }
            else if (action == ACTION.FINALIZAR) {
                    if (spnQHVRESUL.getSelectedItemKey() == null || spnQHVRESUL.getSelectedItemKey().toString().equals("0")) {
                    	spnQHVRESUL.requestFocus();
                            ToastMessage.msgBox(this.getActivity(),"Seleccione un resultado.", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                            return false;
                    }                    
                    
                    if (spnQHVRESUL.getSelectedItemKey().toString().equals("5")) {
                        if (spnQHVRESUL_R.getSelectedItemKey() == null || spnQHVRESUL_R.getSelectedItemKey().toString().equals("0")) {
                        	spnQHVRESUL_R.requestFocus();
                            ToastMessage.msgBox(this.getActivity(),"Especifique de Rechazo no puede estar vacia.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                            return false;
                        }	
                    }
                    
                    bean.qhvresul = spnQHVRESUL.getSelectedItemKey().toString().equals("0") ? null : (Integer) spnQHVRESUL.getSelectedItemKey();
                    bean.qhvhora_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "HH");
                    bean.qhvmin_fin = Util.getFechaFormateada((Date) txtHORA_FIN.getValue(), "mm");
                    
                    bean.qhvhora_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhvhora_fin);
    				bean.qhvmin_fin=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhvmin_fin);
                    
                    if (spnQHVRESUL.getSelectedItemKey().toString().equals("2") || spnQHVRESUL.getSelectedItemKey().toString().equals("3") || spnQHVRESUL.getSelectedItemKey().toString().equals("4") || spnQHVRESUL.getSelectedItemKey().toString().equals("5")
                    ) {
                            if (!validarProximaVisita(Integer.parseInt(spnQHVRESUL.getSelectedItemKey().toString()))) {
                                    return false;
                            }
                            
                            bean.qhvhorap=null;
                            bean.qhvminup=null;
                            bean.qhvdiap=null;
                            bean.qhvmesp=null;
                            if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
                                    bean.qhvhorap  = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
                                    bean.qhvminup = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
                                    bean.qhvdiap = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
                                    bean.qhvmesp = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
                            }
                    }
                    
            } else if (action == ACTION.EDITAR) {
            	if(spnQHVRESUL.getSelectedItemKey() ==null){
            		spnQHVRESUL.requestFocus();
            		ToastMessage.msgBox(this.getActivity(),"Seleccione un resultado.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
            		return false;
            	}
            	
            	
            	if (spnQHVRESUL.getSelectedItemKey().toString().equals("5")) {
                    if (spnQHVRESUL_R.getSelectedItemKey() == null || spnQHVRESUL_R.getSelectedItemKey().toString().equals("0")) {
                    	spnQHVRESUL_R.requestFocus();
                        ToastMessage.msgBox(this.getActivity(),"Especifique de Rechazo no puede estar vacia.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
                        return false;
                    }	
                }
            
                if (spnQHVRESUL.getSelectedItemKey().toString().equals("3") || spnQHVRESUL.getSelectedItemKey().toString().equals("4") || spnQHVRESUL.getSelectedItemKey().toString().equals("5") || spnQHVRESUL.getSelectedItemKey().toString().equals("2")) {
                        if (!validarProximaVisita(Integer.parseInt(spnQHVRESUL.getSelectedItemKey().toString()))) {
                                return false;
                        }
                        bean.qhvhorap=null;
                        bean.qhvminup=null;
                        bean.qhvdiap=null;
                        bean.qhvmesp=null;
                        if (txtFECHA_PROXIMA.getValue() != null && txtHORA_PROXIMA.getValue() != null) {
                                bean.qhvhorap = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "HH");
                                bean.qhvminup = Util.getFechaFormateada((Date) txtHORA_PROXIMA.getValue(), "mm");
                                bean.qhvdiap = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "dd");
                                bean.qhvmesp = Util.getFechaFormateada((Date) txtFECHA_PROXIMA.getValue(), "MM");
                        }
                } else {
                	bean.qhvhorap=null;
                    bean.qhvminup=null;
                    bean.qhvdiap=null;
                    bean.qhvmesp=null;
                }
                                  
                    
            }
            try {
            	if (!getVisitaService().saveOrUpdate(bean,"QHVDIA_INI","QHVMES_INI","QHVANIO_INI","QHHORA_INI","QHMIN_INI","QHVHORA_FIN","QHVMIN_FIN" ,"QHVRESUL","QHVRESUL_R","QHVRESUL_O","QHVDIAP","QHVMESP","QHVHORAP","QHVMINUP")) {
            		ToastMessage.msgBox(this.getActivity(),"Los datos no fueron grabados",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            	}
            } catch (SQLException e) {
            	ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            }
            return true;
    }

    
    private boolean validarProximaVisita(Integer item) {
    		if(!Util.esDiferente(item, 4)){
    			if(txtFECHA_PROXIMA.getValue() == null){
    				txtFECHA_PROXIMA.requestFocus();
    				ToastMessage.msgBox(this.getActivity(),"Proxima Fecha es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            		return false;
    			}
    			if(txtHORA_PROXIMA.getValue() == null){
    				txtHORA_PROXIMA.requestFocus();
    				ToastMessage.msgBox(this.getActivity(),"proxima hora es requerido.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
                	return false;
        		}
            
    		}
            if (txtFECHA_PROXIMA.getValue() == null || txtHORA_PROXIMA.getValue() == null &&  Util.esDiferente(item, 4)) {
            	return true;
            }
            
            if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) > 0) {
            	txtFECHA_PROXIMA.requestFocus();
            	ToastMessage.msgBox(this.getActivity(),"Fecha Proxima no puede ser menor.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            	return false;
            } else if (Util.compare((Date) txtFECHA_INICIO.getValue(),(Date) txtFECHA_PROXIMA.getValue()) == 0) {
            	if (Util.compareTime((Date) txtHORA_FIN.getValue(),(Date) txtHORA_PROXIMA.getValue()) >= 0) {
            		txtHORA_PROXIMA.requestFocus();
            		ToastMessage.msgBox(this.getActivity(),"Hora Proxima no puede ser menor o igual.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
            		return false;
            	}
            }
            return true;
    }

    private void cargarDatos() {    	
            getDialog().setTitle("Cuestionario de Hogar - Visita N� " + bean.nro_visita);
            entityToUI(bean);
            
            caretaker.addMemento("antes", bean.saveToMemento(Visita.class));

            if (action == ACTION.INICIAR) {
                    txtFECHA_INICIO.setValue(Calendar.getInstance().getTime());
                    txtHORA_INICIO.setValue(Calendar.getInstance().getTime());
                    
            } else if (action == ACTION.FINALIZAR) {
                    Date fecha = Util.getFechaHora(bean.qhvanio_ini+"", bean.qhvmes_ini, bean.qhvdia_ini, bean.qhhora_ini, bean.qhmin_ini,  0+"");
                    txtFECHA_INICIO.setValue(fecha);
                    txtHORA_INICIO.setValue(fecha);
                    txtHORA_FIN.setValue(Calendar.getInstance().getTime());
                    
            } else if (action == ACTION.EDITAR) {
                    if(spnQHVRESUL.getSelectedItemKey()!=null){
                    	if (spnQHVRESUL.getSelectedItemKey().toString().equals("2")
		                 || spnQHVRESUL.getSelectedItemKey().toString().equals("3")
		                 || spnQHVRESUL.getSelectedItemKey().toString().equals("4")
		                 || spnQHVRESUL.getSelectedItemKey().toString().equals("5")
                ) {
                        if (bean.qhvhorap != null && bean.qhvhorap!=null) {
                                int anio = Util.getInt(Util.getFechaFormateada(Calendar.getInstance().getTime(), "yyyy"));
                                Date fechaProx = Util.getFechaHora(anio+"", bean.qhvmesp,bean.qhvdiap , bean.qhvhorap , bean.qhvminup, 0+"");
                                txtFECHA_PROXIMA.setValue(fechaProx);
                                txtHORA_PROXIMA.setValue(fechaProx);    
                        }
                }
           }
                
                    Date fechaIni = Util.getFechaHora(App.ANIOPORDEFECTOSUPERIOR+"", bean.qhvmes_ini,bean.qhvdia_ini, bean.qhhora_ini,bean.qhmin_ini, 0+"");
                    Date fechaFin = Util.getFechaHora(App.ANIOPORDEFECTOSUPERIOR+"", bean.qhvmes_ini, bean.qhvdia_ini, bean.qhhora_ini, bean.qhmin_ini, 0+"");

                    txtFECHA_INICIO.setValue(fechaIni);
                    txtHORA_INICIO.setValue(fechaIni);
                    txtHORA_FIN.setValue(fechaFin);
            }
            
            inicio();
    }

    private void inicio() {
            if (action == ACTION.INICIAR) {
                    btnAceptar.requestFocus();
                    q3.setVisibility(View.GONE);
                    q2.setVisibility(View.GONE);
            } else if (action == ACTION.FINALIZAR) {
                    q3.setVisibility(View.VISIBLE);
                    q2.setVisibility(View.VISIBLE);
                    spnQHVRESUL.requestFocus();
            }
            onC1_RVISITAChangeValue(spnQHVRESUL);
    }

    
    public void onC1_RVISITAChangeValue(FieldComponent component) {
            String resultadoStr = (String) component.getValue();
            if (resultadoStr.substring(0, 1).equals("5")) {
            	Util.lockView(getActivity(), false, spnQHVRESUL_R);
            	spnQHVRESUL_R.setVisibility(View.VISIBLE);
            	spnQHVRESUL_R.requestFocus();
            } else {
            	Util.cleanAndLockView(getActivity(), spnQHVRESUL_R);
            	spnQHVRESUL_R.setVisibility(View.GONE);
            }
            
            if (resultadoStr.substring(0, 1).equals("2")|| resultadoStr.substring(0, 1).equals("3")|| resultadoStr.substring(0, 1).equals("4")|| resultadoStr.substring(0, 1).equals("5")) {
            	Util.lockView(getActivity(), false, txtFECHA_PROXIMA,txtHORA_PROXIMA);
            	txtFECHA_PROXIMA.requestFocus();
            } else {
            	Util.cleanAndLockView(getActivity(), txtFECHA_PROXIMA,txtHORA_PROXIMA);
            }
    }

    private void cargarSpinner() {
            if (esResultado3) {
                    List<Object> keys = new ArrayList<Object>();
                    keys.add(null);
                    keys.add(1);
                    keys.add(6);
                    String[] items1 = new String[] { " --- Seleccione --- ","1. COMPLETO", "6. INCOMPLETO" };
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                    spnQHVRESUL.setAdapterWithKey(adapter, keys);
            } else {
                    List<Object> keys = new ArrayList<Object>();
                    keys.add(null);
                    keys.add(2);
                    keys.add(3);
                    keys.add(4);
                    keys.add(5);
                    String[] items1 = new String[] { " --- Seleccione --- ",
                    				"2. HOGAR PRESENTE PERO ENTREVISTADO COMPETENTE AUSENTE", 
                                    "3. HOGAR AUSENTE",
                                    "4. APLAZADA",
                                    "5. RECHAZADA", 
                    
                    };
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                    spnQHVRESUL.setAdapterWithKey(adapter, keys);
            }
    }
    
    
    private void cargarSpinnerRRechazo() { 
                List<Object> keys = new ArrayList<Object>();
                keys.add(null);
                keys.add(1);
                keys.add(2);
                keys.add(3);
                String[] items1 = new String[] { " --- Seleccione --- ",
                                "1. NO DESEAN LA ENTREVISTA", 
                                "2. YA FUE ENTREVISTADA POR LA ENDES",
                                "3. YA FUE ENTREVISTADA POR OTRA ENCUESTA DEL INEI",                
                };
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
                spnQHVRESUL_R.setAdapterWithKey(adapter, keys);        
    }
	
    public CuestionarioService getService() {
        if (service == null) {
            service = CuestionarioService.getInstance(getActivity());
        }
        return service;
}
	
    private VisitaService getVisitaService() {
        if (visitaService == null) {
                visitaService = VisitaService.getInstance(getActivity());
        }
        return visitaService;
}

	private CuestionarioService getCuestionarioService() {
		// TODO Auto-generated method stub
		return null;
	}

}
