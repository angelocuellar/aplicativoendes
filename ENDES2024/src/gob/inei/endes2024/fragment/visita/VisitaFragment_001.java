package gob.inei.endes2024.fragment.visita;

import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.visita.Dialog.VisitaDialog;
import gob.inei.endes2024.fragment.visita.Dialog.VisitaDialog.ACTION;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class VisitaFragment_001 extends FragmentForm implements Respondible{
	private TableComponent tcVisita;
	@FieldAnnotation(orderIndex = 1)
	public ButtonComponent btnIniciar;
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnFinalizar;
	public List<Seccion01> posiblesinformantessalud;
	public TextField txtOrden;
	private List<Visita> visitas;
	private Visita ultimaVisita,menorVisita;
	Hogar hogar;
	Seccion01 informantedesalud;
	private GridComponent grid;
	private GridComponent2  gridresultado;
	private LabelComponent dummy;
	private RadioGroupOtherField rgOther;
	private LabelComponent lblResultado,lblfecha, lblFechaFinal, lblResultadoFinal,
			lblSubTituloresultado,lblGridcabecera,lblGridcargo,lblGriddni,lblGridNombres,lblGridencuestador,lblGridSupervision,lblGridCoordinador,lblGridSupervisor;
	Visita visita;
	public LabelComponent lblnombredomestica,lblviolenciadomestica,lblnombreInformante,lblInformanteSalud,lblDatosdeControl,lblTotalPersonas,lblTotalPersonasvalor ,lblmefnro,lblmefnrovalor,lbldocenro,lbldocenrovalor,lblniniosdocenro,lblniniosdocenrovalor,lblniniosseisnro,lblniniosseisnrovalor,lblninioscinconro,lblninioscinconrovalor,lblniniostresnro,lblniniostresnrovalor,lblinformantehogarnro,lblinformantehogarnrovalor,lblinformantesaludnro,lblinformantesaludnrovalor,lblControl,lblmefnro12_14,lblmefnrovalor12_14;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo;
	private GridComponent2 gridResultados;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	DialogComponent dialog;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] SeccionesGrabadoHogar;
	SeccionCapitulo[] seccionesCargadoInformanteSalud;
	VisitaService visitaService;
	private Seccion01Service personaService;
	public List<Seccion01> ListadoConteo;
	private Seccion01Service seccion01;
	private Visita bean;
	public VisitaFragment_001() {
	}

	public VisitaFragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);

		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QHVDIA_INI", "QHVMES_INI", "QHVANIO_INI", "QHHORA_INI","QHMIN_INI", "QHVRESUL", "QHVRESUL_R", "QHVHORA_FIN", "QHVMIN_FIN","QHVDIAP", "QHVMESP", "QHVHORAP", "QHVMINUP", "NRO_VISITA","ID", "HOGAR_ID","QHVRESUL_O") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QHVDIA_INI", "QHVMES_INI", "QHVANIO_INI", "QHHORA_INI","QHMIN_INI", "QHVRESUL", "QHVRESUL_R", "QHVHORA_FIN", "QHVMIN_FIN","QHVDIAP", "QHVMESP", "QHVHORAP", "QHVMINUP","QHVRESUL_O") };
		SeccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "NRO_INFO_S","QHTOT_VIS")};
//		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoInformanteSalud=new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.visita).textSize(21).centrar().negrita();
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcVisita = new TableComponent(getActivity(), this,App.ESTILO).size(250, 770).dataColumHeight(60).headerHeight(60).headerTextSize(17);
//		}
//		else{
			tcVisita = new TableComponent(getActivity(), this,App.ESTILO).size(400, 770).dataColumHeight(40).headerHeight(45).headerTextSize(15);
//		}		
		tcVisita.addHeader(R.string.v_nro, 0.4f);
		tcVisita.addHeader(R.string.v_fecha, 0.6f);
		tcVisita.addHeader(R.string.v_hora_ini, 0.8f);
		tcVisita.addHeader(R.string.v_hora_fin, 0.8f);
		tcVisita.addHeader(R.string.v_fecha_prox, 0.8f);
		tcVisita.addHeader(R.string.v_hora_prox, 0.8f);
		tcVisita.addHeader(R.string.v_resultado, 1.0f);
		
		btnIniciar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(150, 55);
		btnIniciar.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				if (visitas.size() > 0) {					
					
					if (!Util.esDiferente(visitas.get(visitas.size() - 1).qhvresul, 1)) {
						ToastMessage.msgBox(getActivity(),"Cuestionarios finalizados",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					if (visitas.get(visitas.size() - 1).qhvresul == null) {
						ToastMessage.msgBox(getActivity(),"Finalice la visita anterior",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}			

				}

				Calendar fechaactual = new GregorianCalendar();
				bean = new Visita();
				bean.id = App.getInstance().getHogar().id;
				bean.hogar_id = App.getInstance().getHogar().hogar_id;
				bean.nro_visita = visitas.size() + 1;
				bean.qhhora_ini = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
				bean.qhmin_ini =  fechaactual.get(Calendar.MINUTE)+"";
				bean.qhvdia_ini = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
				bean.qhvmes_ini = (fechaactual.get(Calendar.MONTH)+1)+"";
				bean.qhvanio_ini= fechaactual.get(Calendar.YEAR);
				
				bean.qhhora_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhhora_ini);
				bean.qhmin_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhmin_ini);
				bean.qhvdia_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhvdia_ini);
				bean.qhvmes_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(bean.qhvmes_ini);
				
//				Log.e("nro_visita: ",""+bean.nro_visita);
//				Log.e("ulim: ",""+getCuestionarioService().getUltima_visitaVivienda(visita.id).nro_visita);
//				
				if( bean.nro_visita >getCuestionarioService().getUltima_visitaVivienda(visita.id).nro_visita){
					ToastMessage.msgBox(getActivity(),"Debe iniciar visita en la vivienda",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return;
				}
				
				boolean flag=grabar();
				if(flag){
					recargarLista();
				}
			}
		});
		btnFinalizar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_finalizar).size(150, 55);
		btnFinalizar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Visita bean = (Visita) tcVisita.getSelectedItem();
				if (bean == null) {
					ToastMessage.msgBox(getActivity(),"Seleccione una visita primero.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					return;
				}
				if (bean.qhvresul != null) {
					ToastMessage.msgBox(getActivity(),"Visita ya fue finalizada.",ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
					return;
				}
				abrirDialogo(bean, VisitaDialog.ACTION.FINALIZAR);
			}
		});
		
		dummy = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		grid = new GridComponent(getActivity(), Gravity.CENTER, 3, 0);
		grid.addComponent(btnIniciar);
		grid.addComponent(dummy);
		grid.addComponent(btnFinalizar);
		lblSubTituloresultado = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vresultcabecera).textSize(21).centrar().negrita();
	
		lblfecha = new LabelComponent(this.getActivity()).size(altoComponente, 170) .text(R.string.v_fecha).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblResultado = new LabelComponent(this.getActivity()).text(R.string.v_resultado).size(altoComponente+10, 170).textSize(18).centrar().negrita().colorFondo(color.griscabece);
		lblFechaFinal = new LabelComponent(this.getActivity()).size(altoComponente, 500).textSize(18).centrar().negrita();
		lblResultadoFinal = new LabelComponent(this.getActivity()).size(altoComponente+10, 500).textSize(18).centrar().negrita();
		
		gridresultado = new GridComponent2(this.getActivity(), 2);
		gridresultado.addComponent(lblfecha);
		gridresultado.addComponent(lblFechaFinal);
		gridresultado.addComponent(lblResultado);
		gridresultado.addComponent(lblResultadoFinal);
		
		lblControl = new LabelComponent(this.getActivity()).size(altoComponente,650).textSize(19).text(R.string.datosdecontrol).negrita().centrar().colorFondo(color.griscabece);
		lblTotalPersonas = new LabelComponent(this.getActivity()).text(R.string.totaldepersonas).size(altoComponente-4,650).textSize(16);
		lblmefnro = new LabelComponent(this.getActivity()).text(R.string.mujeres15a49).size(altoComponente-4, 650).textSize(16);
		lblmefnro12_14 = new LabelComponent(this.getActivity()).text(R.string.mujeres12a14).size(altoComponente-4, 650).textSize(16);
		lblniniosdocenro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresadoce).size(altoComponente-4, 650).textSize(16);
		lblniniosseisnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresaseis).size(altoComponente-4, 650).textSize(16);
		lblninioscinconro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresacinco).size(altoComponente-4, 650).textSize(16);
		lblniniostresnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresatres).size(altoComponente-4, 650).textSize(16);
		lblinformantehogarnro = new LabelComponent(this.getActivity()).text(R.string.informantehogar).size(altoComponente-4, 650).textSize(16);
		lblinformantesaludnro = new LabelComponent(this.getActivity()).text(R.string.informantesaludnro).size(altoComponente-4, 650).textSize(16);
		lblTotalPersonasvalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblmefnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblmefnrovalor12_14= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblniniosdocenrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblniniosseisnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblninioscinconrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblniniostresnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 50).textSize(16).centrar();
		lblinformantehogarnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 100).textSize(16).centrar();
		lblinformantesaludnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente-4, 100).textSize(16).centrar();  	
		
		gridResultados = new GridComponent2(this.getActivity(),2);
		gridResultados.addComponent(lblControl,2);
		gridResultados.addComponent(lblTotalPersonas);
		gridResultados.addComponent(lblTotalPersonasvalor) ;
		gridResultados.addComponent(lblmefnro);
		gridResultados.addComponent(lblmefnrovalor);
		gridResultados.addComponent(lblmefnro12_14);
		gridResultados.addComponent(lblmefnrovalor12_14);
		gridResultados.addComponent(lblniniosdocenro);
		gridResultados.addComponent(lblniniosdocenrovalor);
		gridResultados.addComponent(lblniniosseisnro);
		gridResultados.addComponent(lblniniosseisnrovalor);
		gridResultados.addComponent(lblninioscinconro);
		gridResultados.addComponent(lblninioscinconrovalor);
		gridResultados.addComponent(lblniniostresnro);
		gridResultados.addComponent(lblniniostresnrovalor);
		gridResultados.addComponent(lblinformantehogarnro);
		gridResultados.addComponent(lblinformantehogarnrovalor);
		gridResultados.addComponent(lblinformantesaludnro);
		gridResultados.addComponent(lblinformantesaludnrovalor);
		
		
	}

	private void abrirDialogo(Visita bean, ACTION action) {
		FragmentManager fm = this.getFragmentManager();
		VisitaDialog aperturaDialog = VisitaDialog.newInstance(this, bean,action);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
		recargarLista();
	}

	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(tcVisita.getTableView(), grid.component());
		tcVisita.setSelectorData(R.drawable.selector_sombra_lista);
		q2 = createQuestionSection(lblSubTituloresultado,gridresultado.component());
		q3 = createQuestionSection(gridResultados.component());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		if(bean!=null){
		try {
			if(!getVisitaService().saveOrUpdate(bean,"QHVDIA_INI","QHVMES_INI","QHVANIO_INI","QHHORA_INI","NRO_VISITA","QHMIN_INI")){
				 ToastMessage.msgBox(getActivity(), "Los datos no fueron guardados.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
         			}
		} catch (Exception e) {
			
		}
		}
		return true;
	}

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
		return true;
	}
	
    public Visita registrarVisitaCompletada(){		
    	return	MyUtil.RegistrarVisitaCompletada(getCuestionarioService(), getPersonaService(), App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getVisita().nro_visita, ultimaVisita.qhvresul);
    }
	

	@Override
	public void cargarDatos() {
		
		if(App.getInstance().getHogar()==null){
			parent.nextFragment(CuestionarioFragmentActivity.CARATULA);
			return;
		}	
		
		if (visita == null) {
			visita = new Visita();
			visita.id = App.getInstance().getHogar().id;
			visita.hogar_id = App.getInstance().getHogar().hogar_id;
			visitas = new ArrayList<Visita>();
		}	
		
		entityToUI(visita);
		inicio();	
		
		recargarLista();
		try {	
			if (Util.esDiferente(ultimaVisita.qhvresul,2,3,4,5,6,7,8,9)) {			
				getVisitaService().saveOrUpdate(registrarVisitaCompletada(),"QHVRESUL");	
			}			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		recargarLista();		
		RecargarDatosDeControl();
		try {
			Hogar hogar = new Hogar();
			hogar.id=App.getInstance().getHogar().id;
			hogar.hogar_id=App.getInstance().getHogar().hogar_id;
			hogar.nro_info_s=App.getInstance().getPersonaSeccion01().persona_id;
			hogar.qhtot_vis =ultimaVisita.nro_visita;
			getCuestionarioService().saveOrUpdate(hogar, SeccionesGrabadoHogar);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void LimpiarEtiquetas(){
		lblTotalPersonasvalor.setText("");
		lblmefnrovalor.setText("");
		lblmefnrovalor12_14.setText("");
		lblniniosdocenrovalor.setText("");
		lblniniosseisnrovalor.setText("");
		lblninioscinconrovalor.setText("");
		lblniniostresnrovalor.setText("");
		lblinformantehogarnrovalor.setText("");
		lblinformantesaludnrovalor.setText("");
		
	}
	public void LimpiarResultado(){
		lblFechaFinal.setText("");
		lblResultadoFinal.setText("");
	}
	
	public void RecargarDatosDeControl(){	
//		App.getInstance().SetPersonaSeccion01(null);
		App.getInstance().setEditarPersonaSeccion01(null);
		LimpiarEtiquetas();
	
		String  cadena;
		Integer cantidad = getServiceSeccion01().TotalDePersonas(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		lblTotalPersonasvalor.setText(cantidad+"");
//		ListadoConteo = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		ListadoConteo = getServiceSeccion01().getMujerSeleccionable(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,App.HogEdadMefMin,App.HogEdadMefMax);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblmefnrovalor.setText(cadena);
		/*****/
		ListadoConteo = getServiceSeccion01().getMujerSeleccionable(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,12,14);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblmefnrovalor12_14.setText(cadena);
		/*****/
		ListadoConteo=getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 11);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblniniosdocenrovalor.setText(cadena);
		ListadoConteo=getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 5);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblniniosseisnrovalor.setText(cadena);
		ListadoConteo=getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 4);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblninioscinconrovalor.setText(cadena);
		ListadoConteo=getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 2);
		cadena=ListadoConteo.size()==0?"0":ListadoConteo.size()+"";
		lblniniostresnrovalor.setText(cadena);
		Seccion01 informantehogar = getServiceSeccion01().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
		cadena=informantehogar==null?"":informantehogar.persona_id+"";
		lblinformantehogarnrovalor.setText(cadena);
		if(cantidad>0 && visitas.size()>0){
		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,getVisitaService(),getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersonanformanteSalud(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id,persona_id,seccionesCargadoInformanteSalud);
		App.getInstance().SetPersonaSeccion01(informantedesalud);
		cadena=App.getInstance().getPersonaSeccion01()==null?"":App.getInstance().getPersonaSeccion01().persona_id+"";
		lblinformantesaludnrovalor.setText(cadena);
		}
	}
	
	private void inicio() {
		lblFechaFinal.setText("...");
		lblResultadoFinal.setText("...");
		ValidarsiesSupervisora();
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			Util.cleanAndLockView(getActivity(), btnIniciar,btnFinalizar);
		}
		else{
			Util.lockView(getActivity(),false ,btnIniciar,btnFinalizar);
		}
	}
	public void recargarLista() {		
	
		ultimaVisita=null;
		menorVisita=null;
		visitas = getCuestionarioService().getVisitas(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		tcVisita.setData(visitas, "nro_visita", "getFechainicio","getHoraInicio", "getHoraFin", "getFechaProxima","getHoraProxima", "getResultado");
		registerForContextMenu(tcVisita.getListView());
		Integer indice=0;
		if(visitas.size()>0){
			if(visitas.size()>1){
				indice = visitas.size()-1;
			}
			else{
				indice=0;
			}
				
		ultimaVisita = getCuestionarioService().getVisita(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,visitas.get(indice).nro_visita, seccionesCargado);
		App.getInstance().setVisita(ultimaVisita);
		menorVisita = getCuestionarioService().getVisita(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,visitas.get(0).nro_visita, seccionesCargado);
		}
		if (menorVisita != null && ultimaVisita != null) {
			if(ultimaVisita.qhvdia_ini!=null){
				lblFechaFinal.setText(Util.completarCadena(Util.getText(ultimaVisita.qhvdia_ini), "0", 2, COMPLETAR.IZQUIERDA) + "/"
                            + Util.completarCadena(Util.getText(ultimaVisita.qhvmes_ini), "0", 2, COMPLETAR.IZQUIERDA)+"/"+Util.getText(ultimaVisita.qhvanio_ini));
			}
			else{
				lblFechaFinal.setText("....");
			}
			
            if (!Util.esVacio(menorVisita.qhvresul)) {
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 1)) {
                    	lblResultadoFinal.setText("1. COMPLETA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 2)) {
                    	lblResultadoFinal.setText("2. HOGAR PRESENTE PERO ENTREVISTADO COMPETENTE AUSENTE");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 3)) {
                    	lblResultadoFinal.setText("3. HOGAR AUSENTE");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 4)) {
                    	lblResultadoFinal.setText("4. APLAZADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 5)) {
                    	lblResultadoFinal.setText("5. RECHAZADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 6)) {
                    	lblResultadoFinal.setText("6. VIVIENDA DESOCUPADA O NO ES VIVIENDA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 7)) {
                    	lblResultadoFinal.setText("7.VIVIENDA DESTRUIDA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 8)) {
                    	lblResultadoFinal.setText("8. VIVIENDA NO ENCONTRADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qhvresul, 9)) {
                    	lblResultadoFinal.setText("9. OTRO");
                    }
            } else {
            	lblResultadoFinal.setText("....");
            }
		}		
		
	}
	  @Override
      public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		  super.onCreateContextMenu(menu, v, menuInfo);
		  if (v.equals(tcVisita.getListView())) {
			  menu.setHeaderTitle("Opciones de las Visitas");
			  menu.add(0, 0, 1, "Editar");
			  menu.add(0, 1, 1, "Eliminar");
			  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			  menu.getItem(1).setVisible(false);
			  if (info.position != visitas.size() - 1) {
				  menu.getItem(0).setVisible(false);
				  menu.getItem(1).setVisible(false);
			  }
			  if (info.position == visitas.size() - 1) {
				  if (visitas.get(info.position).qhvresul == null || !Util.esDiferente(visitas.get(info.position).qhvresul, 1)) {
					  menu.getItem(0).setVisible(false);
				  }
			  }
		  }
	  }
      
      @Override
      public boolean onContextItemSelected(MenuItem item) {
    	  if (!getUserVisibleHint())
    		  return false;
    	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    	  if (item.getGroupId() == 0) {
    		  switch (item.getItemId()) {
    		  case 0:
    			  editarVisita(info.position);
    			  break;
    		  case 1:
    			  eliminarVisita(info.position);
    			  break;
    		  }
    	  }
    	  return super.onContextItemSelected(item);
      }

      private void editarVisita(int position) {
              abrirDialogo(visitas.get(position), ACTION.EDITAR);
      }
      private void eliminarVisita(int position) {
    	  dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Desea borrar la visita?");
    	  dialog.put("visita", visitas.get(position));
    	  dialog.showDialog();
      }

      public CuestionarioService getCuestionarioService() {
    	  if (cuestionarioService == null) {
    		  cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
      }
      private Seccion01Service getServiceSeccion01() {
    	  if (seccion01 == null) {
    		  seccion01 = Seccion01Service.getInstance(getActivity());
    	  }	
    	  return seccion01;	
      }	
      public Seccion01Service getPersonaService() {
    	  if (personaService == null) {
    		  personaService = Seccion01Service.getInstance(getActivity());
    	  }
    	  return personaService;
      }
	 
	public VisitaService getVisitaService()
	{
		if(visitaService==null)
		{
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	public List<Visita> getVisitas() {
		visitas = getVisitaService().getVisitas(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,"ID", "HOGAR_ID", "QHVDIA_INI","QHVMES_INI","QHVANIO_INI","QHHORA_INI","QHMIN_INI","QHVHORA_FIN","QHVMIN_FIN" ,"QHVRESUL","QHVRESUL_O","QHVDIAP","QHVMESP","QHVHORAP","QHVMINUP");             
		return visitas;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onAccept() {
		 if (dialog == null) {
             return;
     }
     Visita bean = (Visita) dialog.get("visita");
     if (bean == null) {
    	 ToastMessage.msgBox(getActivity(), "Error al eliminar visita.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
    	 return;
     }
     if (!getVisitaService().eliminar(bean.id,bean.hogar_id, bean.nro_visita)) {
    	 ToastMessage.msgBox(getActivity(), "Los datos no fueron eliminados.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
    	 return;
     }
     LimpiarResultado();
     recargarLista();
		
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}
