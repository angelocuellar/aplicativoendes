package gob.inei.endes2024.fragment;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.ReportDatesofBirth;
import gob.inei.endes2024.model.ReporteHMS;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.SegmentacionService;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;


public class ReporteHMSFragment extends FragmentForm implements Respondible {
	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERIODO;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnCONGLOMERADO;
	
	private LabelComponent lblTitulo, lblperiodo, lblconglomerado, lblReporte, lblResumen, lblResumen2,lbltituloFechas;
	
	private SegmentacionService segmentacionService;
	private CuestionarioService cuestionarioService;
	
	private GridComponent2 grid1,grid2;
	
	public static SegmentacionFiltro PERIODO;
	public static SegmentacionFiltro CONGLOMERADO;
	
	public TableComponent TablaResultados, TablaDetalle, TablaDetalleViv,TcResultadoFechas;
	public List<ReportDatesofBirth> reportefechas;
	public List<ReporteHMS> reporte;
	
	public int tot_hogar,tot_mef,tot_salud,comple_hogar,comple_mef,comple_salud;
	
	LinearLayout q2, q3;
	
	public ReporteHMSFragment parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		return rootView;
	}
	
	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(lblTitulo);
		LinearLayout q1_1 = createQuestionSection(0, Gravity.CENTER,LinearLayout.HORIZONTAL, grid1.component());
		LinearLayout q1 = createQuestionSection(q1_1);
		q2 = createQuestionSection(TablaResultados.getTableView());
		q3 = createQuestionSection(0,Gravity.RIGHT, LinearLayout.HORIZONTAL,grid2.component());
		LinearLayout q4= createQuestionSection(lbltituloFechas,TcResultadoFechas.getTableView());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);

		return contenedor;
	}
	
	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.reporte_titulo).size(MATCH_PARENT, MATCH_PARENT).centrar().textSize(20);
		lblReporte = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_vivienda).size(50, MATCH_PARENT).centrar().textSize(20);
		lblperiodo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_periodo).size(50, MATCH_PARENT).centrar().textSize(18);
		lblconglomerado = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_conglomerado).size(50, MATCH_PARENT).centrar().textSize(18);
		lbltituloFechas = new LabelComponent(getActivity(),App.ESTILO).text(R.string.reporte_fechas).size(50, MATCH_PARENT).centrar().textSize(18);
		lblResumen = new LabelComponent(getActivity(),App.ESTILO).text(R.string.reporte_tit1).size(50, MATCH_PARENT).centrar().textSize(18);//reporte_tit1
		lblResumen2 = new LabelComponent(getActivity(),App.ESTILO).text(R.string.reporte_tit2).size(50, MATCH_PARENT).centrar().textSize(18);//reporte_tit1
		
		spnPERIODO = new SpinnerField(getActivity()).size(altoComponente + 15,250).callback("onPeriodoChangeValue");
		spnCONGLOMERADO = new SpinnerField(getActivity()).size(altoComponente + 15, 250).callback("onConglomeradoChangeValue");
		MyUtil.llenarPeriodo(getActivity(), getSegmentacionService(),spnPERIODO);
		
		grid1 = new GridComponent2(getActivity(),App.ESTILO, 2);
		grid1.addComponent(lblperiodo);
		grid1.addComponent(lblconglomerado);
		grid1.addComponent(spnPERIODO);
		grid1.addComponent(spnCONGLOMERADO);
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			TablaResultados	= new TableComponent(getActivity(), this, App.ESTILO).size(400, 2600).headerHeight(60).dataColumHeight(60);	
//			TablaDetalle = new TableComponent(getActivity(), this, App.ESTILO).size(194, 550).headerHeight(60).dataColumHeight(40);
//			TablaDetalleViv = new TableComponent(getActivity(), this, App.ESTILO).size(194, 400).headerHeight(85).dataColumHeight(109);
//			TcResultadoFechas = new TableComponent(getActivity(), this,App.ESTILO).size(600, 2000).dataColumHeight(60).headerHeight(60);
//		}
//		else{
			TcResultadoFechas = new TableComponent(getActivity(), this,App.ESTILO).size(600, 1500).dataColumHeight(30).headerHeight(45);
			TablaResultados	= new TableComponent(getActivity(), this, App.ESTILO).size(800, 2600).dataColumHeight(35).headerHeight(45).headerTextSize(15);
			TablaDetalle = new TableComponent(getActivity(), this, App.ESTILO).size(280, 550).dataColumHeight(30).headerHeight(45).headerTextSize(15);
			TablaDetalleViv = new TableComponent(getActivity(), this, App.ESTILO).size(280, 400).dataColumHeight(90).headerHeight(60).headerTextSize(15);
//			}
		
		TablaResultados.addHeader(R.string.reporte_vivienda, 3.6f,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_linea, 2.6f,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_entrev, 6,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_seccion, 6,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_nombre, 10,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_gps, 2.6f,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_resultado, 5,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_visitas, 3.5f,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_pes_tall, 5,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_hemo, 5,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_pt6, 4,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_hem6, 4,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_sal, 4,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_viol, 4,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_pres, 5,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_salud, 4,
				TableComponent.ALIGN.CENTER);
		TablaResultados.addHeader(R.string.reporte_cepillo, 3.5f,
				TableComponent.ALIGN.CENTER);
		TablaResultados.setColorCondition("SECCION", Util.getHMObject("HOGAR",R.color.celesteclarito2,"MEF",R.color.rosado_claro,"SALUD",R.color.lila_claro2));
//		2131296300(morado), 2131296330(melon), 2131296340(amarillo), 
//		TablaDetalle
			
		TablaDetalle.addHeader(R.string.reporte_detalle1, 6,TableComponent.ALIGN.LEFT);
		TablaDetalle.addHeader(R.string.reporte_detalle2, 5,TableComponent.ALIGN.RIGHT);
		TablaDetalle.addHeader(R.string.reporte_detalle3, 5,TableComponent.ALIGN.RIGHT);
		TablaDetalle.addHeader(R.string.reporte_detalle4, 6,TableComponent.ALIGN.RIGHT);
		TablaDetalle.setColorCondition("SECCION", Util.getHMObject("HOGAR",R.color.celesteclarito2,"MEF",R.color.rosado_claro,"SALUD",R.color.lila_claro2));

		TablaDetalleViv.addHeader(R.string.reporte_detallev1, 4,
				TableComponent.ALIGN.CENTER);
		TablaDetalleViv.addHeader(R.string.reporte_detallev2, 4,
				TableComponent.ALIGN.CENTER);
		
		grid2 = new GridComponent2(getActivity(),App.ESTILO, 2);
		grid2.addComponent(lblResumen);
		grid2.addComponent(lblResumen2);
		grid2.addComponent(TablaDetalleViv.getTableView());
		grid2.addComponent(TablaDetalle.getTableView());
		
		TcResultadoFechas.addHeader(R.string.rfecha_vivienda, 0.9f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_orden, 0.9f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_hogar, 1.0f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_edad, 0.8f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_sexo, 1.0f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_nombre, 3.0f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechahogar, 1.5f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechaseccion04, 1.6f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechasindividual, 1.6f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechasseccion02, 1.6f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechasalud, 1.6f,TableComponent.ALIGN.CENTER);
		TcResultadoFechas.addHeader(R.string.rfecha_fechasocho, 1.5f,TableComponent.ALIGN.CENTER);
	}
	
	@Override
	public boolean grabar() {
		return true;
	}
	
	@Override
	public void cargarDatos() {
		
	}
	
	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
		
	}	
	
	public SegmentacionService getSegmentacionService() {
		if (segmentacionService == null) {
			segmentacionService = SegmentacionService.getInstance(getActivity());
		}
		return segmentacionService;
	}
	
	public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	
	public void onPeriodoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) component.getValue();
		MyUtil.llenarConglomerado(getActivity(), getSegmentacionService(),spnCONGLOMERADO, periodo);
	}
	
	public void onConglomeradoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) spnPERIODO.getValue();
		SegmentacionFiltro conglomerado = (SegmentacionFiltro) component.getValue();
		if (periodo == null || conglomerado == null) {
			return;
		}
//		Log.e("conglomerado: ",""+conglomerado);
		cargarMarco(periodo, conglomerado);
//		MyUtil.LiberarMemoria();
//		if(reporte.size()==0){			
//			q2.setVisibility(View.GONE);
//    		q3.setVisibility(View.GONE);
//		}else{
//			q2.setVisibility(View.VISIBLE);
//    		q3.setVisibility(View.VISIBLE);
//		}
	}
	
	public void cargarMarco(SegmentacionFiltro periodo,
			SegmentacionFiltro conglomerado) {
		PERIODO = periodo;
		CONGLOMERADO = conglomerado;
		if (conglomerado == null || periodo == null) {
			return;
		}
		Segmentacion seg = getSegmentacionService().getSegmentacion(periodo.id,	conglomerado.nombre);
		App.getInstance().setSegmentacion(seg);
		
		//Log.e("conglomerado.nombre:",""+conglomerado.nombre);
		//Log.e("CONGLOMERADO.nombre:",""+CONGLOMERADO.nombre);
		recargarLista();
	}
	
	
	private void recargarLista() {
		int tot_ap=0,tos_s_ap=0;Integer viv=-1;
		
		reporte = getCuestionarioService().getReporteHMS(CONGLOMERADO.nombre,getActivity());
		if(!CONGLOMERADO.nombre.equals("-- CONGLOMERADO --")){
			reportefechas = getCuestionarioService().getListadodeFechasporConglomerado(CONGLOMERADO.nombre);
		}
		tot_hogar = 0; tot_mef = 0; tot_salud = 0; comple_hogar = 0; comple_mef = 0; comple_salud = 0;
		
		for(int i=0; i<reporte.size();i++){
			reporte.get(i).Completado();
			if(reporte.get(i).seccion.equals("HOGAR") && reporte.get(i).hogar!=null){
				tot_hogar++;
				if(reporte.get(i).getResultado().equals("Completa")){
					comple_hogar++;
				}	
				if(!viv.equals(reporte.get(i).vivienda)){
					tot_ap++;
					viv = reporte.get(i).vivienda;
				}
			}else{
				if(reporte.get(i).seccion.equals("HOGAR") && reporte.get(i).hogar==null){
					tos_s_ap++;
				}
			}
			if(reporte.get(i).seccion.equals("MEF")){
				tot_mef++;
				if(reporte.get(i).getResultado().equals("Completa")){
					comple_mef++;
				}
			}
			if(reporte.get(i).seccion.equals("SALUD")){
				tot_salud++;
				if(reporte.get(i).getResultado().equals("Completa")){
					comple_salud++;
				}
			}
		}
		
		TablaResultados.setData(reporte, "getViv_hogar","getLinea","entrevistadora","seccion","nombre","getGps","getResultado","visitas","getPeso_talla","getHemog","getPeso_talla6","getHemo6","getSal","getViolencia","getPresion_arterial","getSalud12","getCepillo");
		
		TablaResultados.setBorder("completo");
		
//		Log.e("tot_hogar-comple_hogar: ",""+tot_hogar+"-"+comple_hogar);
//		Log.e("tod_mef-comple_mef: ",""+tot_mef+"-"+comple_mef);
//		Log.e("tot_salud-comple_salud: ",""+tot_salud+"-"+comple_salud);
//		Log.e("R.color.light_pink: ",""+R.color.light_pink);
//		Log.e("R.color.verde_claro: ",""+R.color.verde_claro);//2131296343
//		Log.e("R.color.light_pink: ",""+R.color.light_pink);
		
		List<ReporteHMS> detalle = new ArrayList<ReporteHMS>();
		ReporteHMS band;
		band= new ReporteHMS(); band.detalle("HOGAR", tot_hogar, comple_hogar);
		detalle.add(band);
		band= new ReporteHMS(); band.detalle("INDIVIDUAL", tot_mef, comple_mef);
		detalle.add(band);
		band= new ReporteHMS(); band.detalle("SALUD", tot_salud, comple_salud);
		detalle.add(band);
		
		TablaDetalle.setData(detalle, "seccion","totales","completas","incompletas");
		
		List<ReporteHMS> detallev = new ArrayList<ReporteHMS>();
		band= new ReporteHMS(); band.detallev(tot_ap, tos_s_ap);
		detallev.add(band);
		
		TablaDetalleViv.setData(detallev,"completas","incompletas");
		TcResultadoFechas.setData(reportefechas,"vivienda","orden","hogar","getEdad","getSexo","nombre","fecha_hogar","fecha_seccion04","fecha_individual","fecha_individualseccion02","fecha_salud","fechaseccion08");
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}

}
