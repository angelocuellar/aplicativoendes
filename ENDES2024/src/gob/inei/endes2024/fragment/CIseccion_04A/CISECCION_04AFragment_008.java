package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_008 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI427DA; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI427DB; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI427DC; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI427DD; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI427DE; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI427DF; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI427DG; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI427F; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI427G; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI428;
		
	public TextField txtCabecera;
	
	CISECCION_04A c2seccion_04a;
	CISECCION_01_03 individual;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta427d,lblpregunta427f,lblpregunta427g,lblpregunta428,lbl427da,lbl427db,lbl427dc,
	lbl427dd,lbl427de,lbl427df,lbl427dg,lbl427da2,lbl427db2,lbl427dc2,
	lbl427dd2,lbl427de2,lbl427df2,lbl427dg2,lblblanco,lblP427d_1,lblP427d_2; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI427DA","QI427DB","QI427DC","QI427DD","QI427DE","QI427DF","QI427DG","QI427F","QI427G","QI428")}; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS01; 
	
	public GridComponent2 grid1;
	String nombre_persona;

	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_008() {} 
	public CISECCION_04AFragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI427DA","QI427DB","QI427DC","QI427DD","QI427DE","QI427DF","QI427DG","QI427F","QI427G","QI428","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI236","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta427d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427d);
		lblpregunta427f = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427f);
		lblpregunta427g = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427g);
		lblpregunta428 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi428);
				
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP427d_1 = new LabelComponent(getActivity()).textSize(16).size(50, 150).text(R.string.c2seccion_04aqi427d_c1).centrar();
		lblP427d_2 = new LabelComponent(getActivity()).textSize(16).size(50, 149).text(R.string.c2seccion_04aqi427d_c2).centrar();
				
		lbl427da=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04aqi427da);		
		rgQI427DA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue"); 
				
		lbl427db=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04aqi427db);	
		rgQI427DB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");  
		
		lbl427dc=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04aqi427dc);
		rgQI427DC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");  
				
		lbl427dd=new LabelComponent(getActivity()).textSize(17).size(85, 450).text(R.string.c2seccion_04aqi427dd);
		rgQI427DD=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");  
		
		lbl427de=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04aqi427de);
		rgQI427DE=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");   
		
		lbl427df=new LabelComponent(getActivity()).textSize(17).size(85, 450).text(R.string.c2seccion_04aqi427df);
		rgQI427DF=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");    
				
		lbl427dg=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04aqi427dg);
		rgQI427DG=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427d_radio1,R.string.c2seccion_04aqi427d_radio2).size(altoComponente,250).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427dChangeValue");    
		
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lbl427da);
		grid1.addComponent(rgQI427DA);
		
		grid1.addComponent(lbl427db);
		grid1.addComponent(rgQI427DB);
		
		grid1.addComponent(lbl427dc);
		grid1.addComponent(rgQI427DC);
		
		grid1.addComponent(lbl427dd);
		grid1.addComponent(rgQI427DD);
		
		grid1.addComponent(lbl427de);
		grid1.addComponent(rgQI427DE);
		
		grid1.addComponent(lbl427df);
		grid1.addComponent(rgQI427DF);
		
		grid1.addComponent(lbl427dg);
		grid1.addComponent(rgQI427DG);
			
		
	
		rgQI427F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427f_1,R.string.c2seccion_04aqi427f_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI427G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427g_1,R.string.c2seccion_04aqi427g_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI428=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi428_1,R.string.c2seccion_04aqi428_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI428ChangeValue"); 
				
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta427d,grid1.component());
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta427f,rgQI427F); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta427g,rgQI427G); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta428,rgQI428); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		
		App.getInstance().getSeccion04A().qi428 = c2seccion_04a.qi428;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(esPreguntaInicial) {		
			if (Util.esVacio(c2seccion_04a.qi427da)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DA"); 
				view = rgQI427DA; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427db)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DB"); 
				view = rgQI427DB; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427dc)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DC"); 
				view = rgQI427DC; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427dd)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DD"); 
				view = rgQI427DD; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427de)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DE"); 
				view = rgQI427DE; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427df)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DF"); 
				view = rgQI427DF; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi427dg)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427DG"); 
				view = rgQI427DG; 
				error = true; 
				return false; 
			} 
			if(verifica427d()) {
				if (Util.esVacio(c2seccion_04a.qi427f)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI427F"); 
					view = rgQI427F; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04a.qi427g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427G"); 
				view = rgQI427G; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi428)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI428"); 
				view = rgQI428; 
				error = true; 
				return false; 
			} 
			if(Util.esDiferente(individual.qi236, 1,2,3) && !Util.esDiferente(c2seccion_04a.qi428, 1)){
    			Mensaje("Verifique pregunta QI236");
    		}
			if(!Util.esDiferente(individual.qi236, 1,2,3) && !Util.esDiferente(c2seccion_04a.qi428, 2)){
    			Mensaje("Verifique pregunta QI236.");
    		}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado);
		individual = getCuestionarioService().getCISECCION_01_03(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id, seccionesCargadoS01);
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212; 
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		inicio(); 
    } 
    private void inicio() { 
    	if(c2seccion_04a.qi427da!=null && c2seccion_04a.qi427db!=null && c2seccion_04a.qi427dc!=null && c2seccion_04a.qi427dd!=null &&
    	   c2seccion_04a.qi427de!=null && c2seccion_04a.qi427df!=null && c2seccion_04a.qi427dg!=null	)
    	onqrgQI427dChangeValue(); 
    	if(c2seccion_04a.qi428!=null)
    		onqrgQI428ChangeValue();
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		Util.cleanAndLockView(getActivity(),rgQI427DA,rgQI427DB,rgQI427DC,rgQI427DD,rgQI427DE,rgQI427DF,rgQI427DG,rgQI427F,rgQI427G,rgQI428);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);   
    		MyUtil.LiberarMemoria();
    	}
    }
        
    public void onqrgQI427dChangeValue() {  	
    	if (verifica427d()) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);	
    		Util.lockView(getActivity(),false,rgQI427F);  
    		q2.setVisibility(View.VISIBLE);
    	} 
    	else {
    		Util.cleanAndLockView(getActivity(),rgQI427F);
    		q2.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    		//Util.lockView(getActivity(),false,chbQI407_Y);  				
    	}	
    }
    
    public void onqrgQI428ChangeValue() {
    	if(rgQI428.getValue()!=null) { 	
	    	if (rgQI428.getValue().toString().equals("1")) {    		
	    		App.getInstance().getSeccion04A().qi428 = 1; 
	    		if(Util.esDiferente(individual.qi236, 1,2,3)){
	    			Mensaje("Verifique pregunta QI236");
	    		}
	    	} 
	    	else if(rgQI428.getValue().toString().equals("2")) {
	    		App.getInstance().getSeccion04A().qi428 = 2;
	    		if(!Util.esDiferente(individual.qi236, 1,2,3)){
	    			Mensaje("Verifique pregunta QI236.");
	    		}
	    	}	
    	}
    }
    public void Mensaje(String mensaje){
    	ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    public boolean verifica427d() {
    	boolean res=false;
	    if(rgQI427DA.getValue().toString().equals("1") || rgQI427DB.getValue().toString().equals("1") || rgQI427DC.getValue().toString().equals("1") || 
	       rgQI427DD.getValue().toString().equals("1") || rgQI427DE.getValue().toString().equals("1") || rgQI427DF.getValue().toString().equals("1") || 
	       rgQI427DG.getValue().toString().equals("1")) {
	    	res = true;
	    }   
	    return res;
    }
    

	public void renombrarEtiquetas()
    {	
    	lblpregunta427g.setText(lblpregunta427g.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta428.setText(lblpregunta428.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI427DA.readOnly();
			rgQI427DB.readOnly();
			rgQI427DC.readOnly();
			rgQI427DD.readOnly();
			rgQI427DE.readOnly();
			rgQI427DF.readOnly();
			rgQI427DG.readOnly();
			rgQI427F.readOnly();
			rgQI427G.readOnly();
			rgQI428.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
	uiToEntity(c2seccion_04a); 
		
		App.getInstance().getSeccion04A().qi428 = c2seccion_04a.qi428;
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
