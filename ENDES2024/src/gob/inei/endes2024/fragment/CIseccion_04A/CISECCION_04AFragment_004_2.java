package gob.inei.endes2024.fragment.CIseccion_04A; 
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

public class CISECCION_04AFragment_004_2 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI419M; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI419Y; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI420; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI421;
	@FieldAnnotation(orderIndex=5) 
	public TextAreaField txtQI421_O; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI422D; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQI422I; 
	@FieldAnnotation(orderIndex=8) 
	public TextAreaField txtQI422_O; 
//	@FieldAnnotation(orderIndex=9) 
//	public RadioGroupOtherField rgQI422A;
	
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI422A_A; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI422A_B; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI422A_C; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI422A_D; 
	
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI423; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI424; 
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQI425; 
	
	public CheckBoxField chb419_m,chb419_a,chb420,chb422_m,chb422_a;
	
	CISECCION_04A c2seccion_04a; 
	CISECCION_02 ciseccion_02;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblvacunames419,lblembarazo420,lblvacunaanio419,lblpregunta419,lblpregunta420,
	lblpregunta421,lblpregunta422,lblpregunta422A,lblpregunta423,lblpregunta424,lblpregunta425,lblembarazodias422,lblembarazoinyecciones422,
	lblpregunta422_o,lblpregunta421_o,lblpregunta422A_A,lblpregunta422A_B,lblpregunta422A_C,lblpregunta422A_D; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI419M","QI419Y","QI420","QI421","QI421_O","QI422D","QI422_O","QI422I","QI422A_A","QI422A_B","QI422A_C","QI422A_D","QI423","QI424","QI425")}; 
	SeccionCapitulo[] seccionesCargado,seccionesPersonas; 
	
	public ButtonComponent btnNoSabeVacunaMes,btnNoSabeVacunaAnio,btnNoSabe420,btnNoSabeDias422,btnNoSabeInyecciones422,btnNoSabeEmbarazo415,btnNoSabe418;
	public GridComponent2 gdVacuna,gdEmbarazo420,gdEmbarazo422,gdEmbarazo415,gdVacuna418,grid422A;
	public TextField txtCabecera;
	String nombre_persona;
	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_004_2() {} 
	public CISECCION_04AFragment_004_2 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQI419M, 1, 12);
		rango(getActivity(), txtQI419Y, App.ANIOPORDEFECTOSUPERIOR-49, App.ANIOPORDEFECTOSUPERIOR);  //edad  15-49
		rango(getActivity(), txtQI420, 1, 49);  //edad  15-49
		rango(getActivity(), txtQI422D, 0, 364);  //edad  15-49
		rango(getActivity(), txtQI422I, 0, 97);  //edad  15-49
		enlazarCajas();    
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI415","QI417","QI419M","QI419Y","QI420","QI421","QI421_O","QI422D","QI422_O","QI422I","QI422A_A","QI422A_B","QI422A_C","QI422A_D","QI423","QI424","QI425","QI414","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215D","QI215M","QI215Y","QI220A","ID","HOGAR_ID","PERSONA_ID") }; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar();

	    txtQI419M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI419Y=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(4); 
		txtQI419Y.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
				if(s.toString().length()>3)
					ontxtQI419YChangeValue();
			
			}
		});	
		lblpregunta419  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi419).textSize(19);
		lblpregunta420  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi420).textSize(19);
		lblpregunta421  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi421).textSize(19);
		lblpregunta422  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi422).textSize(19);
//		lblpregunta422A  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi422A).textSize(19);
		
		lblpregunta422A  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi422A).textSize(19);
		lblpregunta422A_A  = new LabelComponent(this.getActivity()).size(altoComponente+45, 430).text(R.string.c2seccion_04aqi422A_A).textSize(17);
		lblpregunta422A_B  = new LabelComponent(this.getActivity()).size(altoComponente+15, 430).text(R.string.c2seccion_04aqi422A_B).textSize(17);
		lblpregunta422A_C  = new LabelComponent(this.getActivity()).size(altoComponente+30, 430).textSize(17).text("");		
		lblpregunta422A_D  = new LabelComponent(this.getActivity()).size(altoComponente+15, 430).text(R.string.c2seccion_04aqi422A_D).textSize(17);
		
		
		lblpregunta423  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi423).textSize(19);
		lblpregunta424  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi424).textSize(19);
		lblpregunta425  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi425).textSize(19);	
		
		lblvacunames419  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi419m).textSize(16);
		lblvacunaanio419  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi419y).textSize(16);
		chb419_a=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi419y_ns, "1:0").size(altoComponente, 200);
		chb419_a.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb419_a.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI419Y);
				Util.lockView(getActivity(),false, txtQI420,chb420);
				q2.setVisibility(View.VISIBLE);
			}
			else {
				Util.lockView(getActivity(),false, txtQI419Y);	
			}
		}
		});
		
		chb419_m=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi419m_ns,"1:0").size(altoComponente, 200);
		chb419_m.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb419_m.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI419M);
			}
			else {
				Util.lockView(getActivity(),false, txtQI419M);	
			}
		}
		});
				
		gdVacuna = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna.addComponent(lblvacunames419);
		gdVacuna.addComponent(txtQI419M);
		gdVacuna.addComponent(chb419_m);
		gdVacuna.addComponent(lblvacunaanio419);
		gdVacuna.addComponent(txtQI419Y);
		gdVacuna.addComponent(chb419_a);
		txtQI420=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(4); 
		
		lblembarazo420  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi420a).textSize(16).centrar();
		chb420=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi420a_ns, "1:0").size(altoComponente, 200);
		chb420.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb420.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI420);
			}
			else {
				Util.lockView(getActivity(),false, txtQI420);	
			}
		}
		});
		
		gdEmbarazo420 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo420.addComponent(lblembarazo420);
		gdEmbarazo420.addComponent(txtQI420);
		gdEmbarazo420.addComponent(chb420);
		
		
		txtQI422D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(3).callback("onp422dChangeValue");
		txtQI422I=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
				
		lblembarazodias422  = new LabelComponent(this.getActivity()).size(altoComponente, 240).text(R.string.c2seccion_04aqi422d).textSize(16);
		lblembarazoinyecciones422  = new LabelComponent(this.getActivity()).size(altoComponente, 240).text(R.string.c2seccion_04aqi422i).textSize(16);
		chb422_a=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi422d_ns, "1:0").size(altoComponente, 200);
		chb422_a.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb422_a.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI422D);
			}
			else {
				Util.lockView(getActivity(),false, txtQI422D);	
			}
		}
		});
		
		chb422_m=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi422i_ns,"1:0").size(altoComponente, 200);
		chb422_m.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb422_m.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI422I);
			}
			else {
				Util.lockView(getActivity(),false, txtQI422I);	
			}
		}
		});
		lblpregunta422_o =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi422observacion);
		lblpregunta421_o =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi421observacion);
		txtQI421_O = new TextAreaField(getActivity()).size(90, 750).alfanumerico().maxLength(500);
		txtQI422_O = new TextAreaField(getActivity()).size(90, 750).alfanumerico().maxLength(500);
		gdEmbarazo422 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo422.addComponent(lblembarazodias422);
		gdEmbarazo422.addComponent(txtQI422D);
		gdEmbarazo422.addComponent(chb422_a);
		gdEmbarazo422.addComponent(lblembarazoinyecciones422);
		gdEmbarazo422.addComponent(txtQI422I);
		gdEmbarazo422.addComponent(chb422_m);
		
		rgQI421=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi421_1,R.string.c2seccion_04aqi421_2,R.string.c2seccion_04aqi421_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI421ChangeValue"); 
		rgQI422A_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi422A_1,R.string.c2seccion_04aqi422A_2,R.string.c2seccion_04aqi422A_8).size(altoComponente+15,320).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI422A_AChangeValue");
		rgQI422A_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi422A_1,R.string.c2seccion_04aqi422A_2,R.string.c2seccion_04aqi422A_8).size(altoComponente+15,320).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI422A_BChangeValue");
		rgQI422A_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi422A_1,R.string.c2seccion_04aqi422A_2,R.string.c2seccion_04aqi422A_8).size(altoComponente+15,320).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI422A_CChangeValue");
		rgQI422A_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi422A_1,R.string.c2seccion_04aqi422A_2,R.string.c2seccion_04aqi422A_8).size(altoComponente+15,320).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	
		rgQI423=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi423_1,R.string.c2seccion_04aqi423_2,R.string.c2seccion_04aqi423_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI424=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi424_1,R.string.c2seccion_04aqi424_2,R.string.c2seccion_04aqi424_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI425=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi425_1,R.string.c2seccion_04aqi425_2,R.string.c2seccion_04aqi425_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		grid422A=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid422A.addComponent(lblpregunta422A_A);
		grid422A.addComponent(rgQI422A_A);
		grid422A.addComponent(lblpregunta422A_B);
		grid422A.addComponent(rgQI422A_B);
		grid422A.addComponent(lblpregunta422A_C);
		grid422A.addComponent(rgQI422A_C);
		grid422A.addComponent(lblpregunta422A_D);
		grid422A.addComponent(rgQI422A_D);
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 	
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta419,gdVacuna.component());
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta420,gdEmbarazo420.component()); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta421,rgQI421,lblpregunta421_o,txtQI421_O);
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta422,gdEmbarazo422.component(),lblpregunta422_o,txtQI422_O);		
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta422A,grid422A.component());
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta423,rgQI423); 
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta424,rgQI424); 
		q8 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta425,rgQI425); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		if(c2seccion_04a!=null){
			if (c2seccion_04a.qi421!=null) {
				c2seccion_04a.qi421=c2seccion_04a.getConvertQi421(c2seccion_04a.qi421);
			}
			if (c2seccion_04a.qi422a_a!=null) {
				c2seccion_04a.qi422a_a=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_a);
			}
			if (c2seccion_04a.qi422a_b!=null) {
				c2seccion_04a.qi422a_b=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_b);
			}
			if (c2seccion_04a.qi422a_c!=null) {
				c2seccion_04a.qi422a_c=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_c);
			}
			if (c2seccion_04a.qi422a_d!=null) {
				c2seccion_04a.qi422a_d=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_d);
			}
			
			if (c2seccion_04a.qi423!=null) {
				c2seccion_04a.qi423=c2seccion_04a.getConvertQi423(c2seccion_04a.qi423);
			}
			if (c2seccion_04a.qi424!=null) {
				c2seccion_04a.qi424=c2seccion_04a.getConvertQi424(c2seccion_04a.qi424);
			}
			if (c2seccion_04a.qi425!=null) {
				c2seccion_04a.qi425=c2seccion_04a.getConvertQi425(c2seccion_04a.qi425);
			}
		}
		
		if(chb419_m.isChecked() ) {
			c2seccion_04a.qi419m="98";			
		}
		
		if(chb419_a.isChecked() ) {
			c2seccion_04a.qi419y=9998;			
		}
		
		if(chb420.isChecked() ) {
			c2seccion_04a.qi420=98;			
		}
		
		if(chb422_a.isChecked() ) {
			c2seccion_04a.qi422d=998;			
		}
		
		if(chb422_m.isChecked() ) {
			c2seccion_04a.qi422i=98;			
		}
		
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
   
    
	private void validarMensaje(String msj) {
	    ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	
    public Calendar fechadeNinio(){
    	Calendar fecha = new GregorianCalendar(ciseccion_02.qi215y,Integer.parseInt(ciseccion_02.qi215m.toString())-(ciseccion_02.qi220a+1),Integer.parseInt(ciseccion_02.qi215d.toString()));
    	return fecha;
    }
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212; 
	    } 		
		
		ciseccion_02 = getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesPersonas);
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		if (c2seccion_04a!=null && c2seccion_04a.qi421!=null) {
			c2seccion_04a.qi421=c2seccion_04a.setConvertQi421(c2seccion_04a.qi421);
		}

		if (c2seccion_04a!=null && c2seccion_04a.qi422a_a!=null) {
			c2seccion_04a.qi422a_a=c2seccion_04a.setConvertQi422a(c2seccion_04a.qi422a_a);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi422a_b!=null) {
			c2seccion_04a.qi422a_b=c2seccion_04a.setConvertQi422a(c2seccion_04a.qi422a_b);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi422a_c!=null) {
			c2seccion_04a.qi422a_c=c2seccion_04a.setConvertQi422a(c2seccion_04a.qi422a_c);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi422a_d!=null) {
			c2seccion_04a.qi422a_d=c2seccion_04a.setConvertQi422a(c2seccion_04a.qi422a_d);
		}
		
		if (c2seccion_04a!=null && c2seccion_04a.qi423!=null) {
			c2seccion_04a.qi423=c2seccion_04a.setConvertQi423(c2seccion_04a.qi423);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi424!=null) {
			c2seccion_04a.qi424=c2seccion_04a.setConvertQi424(c2seccion_04a.qi424);
		}	
		if (c2seccion_04a!=null && c2seccion_04a.qi425!=null) {
			c2seccion_04a.qi425=c2seccion_04a.setConvertQi425(c2seccion_04a.qi425);
		}
				
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 

		if(c2seccion_04a.qi419m!=null && c2seccion_04a.qi419m.equals("98")) {
			chb419_m.setChecked(true);
			txtQI419M.setText("");
		}
		
		if(c2seccion_04a.qi419y!=null && c2seccion_04a.qi419y==9998) {
			chb419_a.setChecked(true);
			txtQI419Y.setText("");
		}
		
		if(c2seccion_04a.qi420!=null && c2seccion_04a.qi420==98) {
			chb420.setChecked(true);
			txtQI420.setText("");
		}
		
		if(c2seccion_04a.qi422d!=null && c2seccion_04a.qi422d==998) {
			chb422_a.setChecked(true);
			txtQI422D.setText("");
		}
		
		if(c2seccion_04a.qi422i!=null && c2seccion_04a.qi422i==98) {
			chb422_m.setChecked(true);
			txtQI422I.setText("");
		}
			
		
		inicio(); 
    } 
    private void inicio() {   
    	if (c2seccion_04a!=null && c2seccion_04a.qi421!=null) {
    		Log.e("111111","5555");
    	onqrgQI421ChangeValue();
    	}
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){
    		Log.e("111111","1");
    		Util.cleanAndLockView(getActivity(),txtQI419M,chb419_m,txtQI419Y,chb419_a,txtQI420,chb420,
    		rgQI421,txtQI422D,chb422_a,txtQI422I,chb422_m,rgQI422A_A,rgQI422A_B,rgQI422A_C,rgQI422A_D,rgQI423,rgQI424,rgQI425);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    	}
    	
    	else {
    		if (   (!Util.esDiferente(c2seccion_04a.qi414, 1) && (Util.esMayor(c2seccion_04a.qi415, 1) && Util.esMenor(c2seccion_04a.qi415, 8))) 
    			|| (!Util.esDiferente(c2seccion_04a.qi414,1) && !Util.esDiferente(c2seccion_04a.qi415,1,8) && Util.esDiferente(c2seccion_04a.qi417, 1))
    			|| (Util.esDiferente(c2seccion_04a.qi414,1) && Util.esDiferente(c2seccion_04a.qi417,1)) ) {
    			Util.cleanAndLockView(getActivity(),txtQI419M,chb419_m,txtQI419Y,chb419_a,txtQI420,chb420);
    			Log.e("111111","ssssssssssssss");
    			q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
			}
    		else{
    			Log.e("111111","sdddddddddddd");
    			Util.lockView(getActivity(),false,txtQI419M,chb419_m,txtQI419Y,chb419_a,txtQI420,chb420);
    			q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
        		
        		if (c2seccion_04a!=null && c2seccion_04a.qi419y!=null) {
            		Log.e("111111","4444");
            	ontxtQI419YChangeValue();
            	}        		
    		}
    		
    		if (c2seccion_04a.qi419m!=null && c2seccion_04a.qi419m.equals("98")) { Util.cleanAndLockView(getActivity(),txtQI419M); } else{ Util.lockView(getActivity(),false,txtQI419M);}
    		if (c2seccion_04a.qi419y!=null && c2seccion_04a.qi419y==9998) { Util.cleanAndLockView(getActivity(),txtQI419Y); } else{ Util.lockView(getActivity(),false,txtQI419Y);}
    		if (c2seccion_04a.qi420!=null && c2seccion_04a.qi420==98) { Util.cleanAndLockView(getActivity(),txtQI420); } else{ Util.lockView(getActivity(),false,txtQI420);}
    		if (c2seccion_04a.qi422d!=null && c2seccion_04a.qi422d==998) { Util.cleanAndLockView(getActivity(),txtQI422D); } else{ Util.lockView(getActivity(),false,txtQI422D);}
    		if (c2seccion_04a.qi422i!=null && c2seccion_04a.qi422i==98) { Util.cleanAndLockView(getActivity(),txtQI422I); } else{ Util.lockView(getActivity(),false,txtQI422I);}
    		
    		onqrgQI422A_AChangeValue();
    	}
    }
    
    public void onqrgQI422A_AChangeValue() {
    	if (MyUtil.incluyeRango(2,8,rgQI422A_A.getTagSelected("").toString())) {
    		Log.e("","A28");
			Util.cleanAndLockView(getActivity(),rgQI422A_B,rgQI422A_C,rgQI422A_D);
			MyUtil.LiberarMemoria();
			rgQI422A_B.setVisibility(View.GONE);			
			rgQI422A_C.setVisibility(View.GONE);
			rgQI422A_D.setVisibility(View.GONE);
			lblpregunta422A_B.setVisibility(View.GONE);
			lblpregunta422A_C.setVisibility(View.GONE);			
			lblpregunta422A_D.setVisibility(View.GONE);
			rgQI423.requestFocus();
    	}
    	else{
    		Log.e("","A1");
    		Util.lockView(getActivity(), false,rgQI422A_B,rgQI422A_C,rgQI422A_D);			
			rgQI422A_B.setVisibility(View.VISIBLE);			
			rgQI422A_C.setVisibility(View.VISIBLE);
			rgQI422A_D.setVisibility(View.VISIBLE);
			lblpregunta422A_B.setVisibility(View.VISIBLE);
			lblpregunta422A_C.setVisibility(View.VISIBLE);			
			lblpregunta422A_D.setVisibility(View.VISIBLE);
			onqrgQI422A_BChangeValue();
			rgQI422A_B.requestFocus();
    	}
    }
    
    public void onqrgQI422A_BChangeValue() {
    	if (MyUtil.incluyeRango(2,8,rgQI422A_B.getTagSelected("").toString())) {
    		Log.e("","B28");
			Util.cleanAndLockView(getActivity(),rgQI422A_C,rgQI422A_D);
			MyUtil.LiberarMemoria();						
			rgQI422A_C.setVisibility(View.GONE);
			rgQI422A_D.setVisibility(View.GONE);			
			lblpregunta422A_C.setVisibility(View.GONE);			
			lblpregunta422A_D.setVisibility(View.GONE);
			rgQI423.requestFocus();
    	}
    	else{
    		Log.e("","B1");
    		Util.lockView(getActivity(), false,rgQI422A_C,rgQI422A_D);								
			rgQI422A_C.setVisibility(View.VISIBLE);
			rgQI422A_D.setVisibility(View.VISIBLE);			
			lblpregunta422A_C.setVisibility(View.VISIBLE);			
			lblpregunta422A_D.setVisibility(View.VISIBLE);			
			onqrgQI422A_CChangeValue();
			rgQI422A_C.requestFocus();
    	}
    }
    
    public void onqrgQI422A_CChangeValue() {
    	if (MyUtil.incluyeRango(2,8,rgQI422A_C.getTagSelected("").toString())) {
    		Log.e("","C28");
			Util.cleanAndLockView(getActivity(),rgQI422A_D);
			MyUtil.LiberarMemoria();
			rgQI422A_D.setVisibility(View.GONE);
			lblpregunta422A_D.setVisibility(View.GONE);
			rgQI423.requestFocus();
    	}
    	else{
    		Log.e("","C1");
    		Util.lockView(getActivity(), false,rgQI422A_D);
			rgQI422A_D.setVisibility(View.VISIBLE);
			lblpregunta422A_D.setVisibility(View.VISIBLE);
			rgQI422A_D.requestFocus();
    	}
    }
    
    
	
	public void ontxtQI419YChangeValue() {
		
		if(txtQI419Y.getValue()!=null) {
			if (txtQI419Y.getValue().toString().length()==4) {    	
				Log.e("","17");
				int oqi419y = Integer.parseInt(txtQI419Y.getValue().toString());
				if(oqi419y>=1968 && oqi419y<=App.ANIOPORDEFECTOSUPERIOR && oqi419y!=9998) {
					Log.e("","18");
					Util.cleanAndLockView(getActivity(),txtQI420,chb420);
		    		q2.setVisibility(View.GONE);
				}			
				else {	
					Log.e("111111","19");
					Util.lockView(getActivity(),false,txtQI420,chb420);  
					q2.setVisibility(View.VISIBLE);				
		    	}	
			} 
	    	else {	
	    		Log.e("","20");
	    		Util.cleanAndLockView(getActivity(),txtQI420,chb420);
	    		q2.setVisibility(View.GONE);
	    	}	
		}
		
		else if(txtQI419Y.getText()!=null) {
			if (txtQI419Y.getText().toString().length()==4) {    	
				Log.e("","21");
				int oqi419y = Integer.parseInt(txtQI419Y.getText().toString());
				if(oqi419y>=1968 && oqi419y<=App.ANIOPORDEFECTOSUPERIOR && oqi419y!=9998) {
					Log.e("","22");
					Util.cleanAndLockView(getActivity(),txtQI420,chb420);
		    		q2.setVisibility(View.GONE);
				}			
				else {	
					Log.e("111111","23");
					Util.lockView(getActivity(),false,txtQI420,chb420);  
					q2.setVisibility(View.VISIBLE);				
		    	}	
			} 
	    	else {	
	    		Log.e("111111","24");
	    		Util.cleanAndLockView(getActivity(),txtQI420,chb420);
	    		q2.setVisibility(View.GONE);
	    	}	
		}
	}
	
	public void onqrgQI421ChangeValue() {
		if(rgQI421.getValue()!=null) {
			Log.e("111111","25");
			if (rgQI421.getValue().toString().equals("1")) {
				Log.e("111111","26");
				Util.lockView(getActivity(),false,txtQI422D,chb422_a,txtQI422I,chb422_m);  
	    		q4.setVisibility(View.VISIBLE);
	    		txtQI422D.requestFocus();
	    	} 
	    	else {	
	    		Log.e("111111","27");
	    		Util.cleanAndLockView(getActivity(),txtQI422D,chb422_a,txtQI422I,chb422_m);
	    		q4.setVisibility(View.GONE);
	    		rgQI422A_A.requestFocus();
	    	}	
		}
	}
	
	public void onp422dChangeValue() {
		Integer dias=0;
		if(txtQI422D.getText()!=null && txtQI422D.getText().toString().length()>0) {
			String str ="Verifique la cantidad ingresada en n�mero de d�as";
			dias = Integer.parseInt(txtQI422D.getText().toString());
			if(((ciseccion_02.qi220a==9 && dias > 270) || (ciseccion_02.qi220a==8 && dias > 240) || (ciseccion_02.qi220a==7 && dias > 220)) || dias < 30) {
				ToastMessage.msgBox(this.getActivity(), str,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);	
			}	
			
		}
		txtQI422I.requestFocus();
	}
	
	public void renombrarEtiquetas() {	
    	lblpregunta419.setText(lblpregunta419.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    	lblpregunta420.setText(lblpregunta420.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta421.setText(lblpregunta421.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta422.setText(lblpregunta422.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta422A.setText(lblpregunta422A.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta423.setText(lblpregunta423.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta424.setText(lblpregunta424.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta425.setText(lblpregunta425.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta422A_C.setText("c. �Le indicaron tratamiento con hierro?");
    	Spanned texto422a_c =Html.fromHtml(lblpregunta422A_C.getText()+ "<br><b>SI DICE \"NO\" SONDEE</b> �Qu� indicaci�n le dieron?");
    	lblpregunta422A_C.setText(texto422a_c);
    	
    }
	
	public void notificacion(String msg) {
		ToastMessage.msgBox(this.getActivity(), msg, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	} 
	
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI421.readOnly();
			rgQI422A_A.readOnly();
			rgQI422A_B.readOnly();
			rgQI422A_C.readOnly();
			rgQI422A_D.readOnly();
			rgQI423.readOnly();
			rgQI424.readOnly();
			rgQI425.readOnly();
			txtQI419M.readOnly();
			txtQI419Y.readOnly();
			txtQI420.readOnly();
			txtQI422D.readOnly();
			txtQI422I.readOnly();
			txtQI422_O.setEnabled(false);
			chb419_a.readOnly();
			chb419_m.readOnly();
			chb420.readOnly();
			chb422_a.readOnly();
			chb422_m.readOnly();
		}
	}
	
	private boolean validar() { 
	if(!isInRange()) return false; 
	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
			
	if(esPreguntaInicial) {				
		if(c2seccion_04a.qi415!=null && (c2seccion_04a.qi415==1 || c2seccion_04a.qi415==8)){								
			if(c2seccion_04a.qi417!=null && c2seccion_04a.qi417==1) {						
				if (Util.esVacio(c2seccion_04a.qi419m) && !chb419_m.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI419M"); 
					view = txtQI419M; 
					error = true; 
					return false; 
				} 
				
				if (Util.esVacio(c2seccion_04a.qi419y) && !chb419_a.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI419Y"); 
					view = txtQI419Y; 
					error = true; 
					return false; 
				} 
				
				if(!c2seccion_04a.qi419m.equals("98") && c2seccion_04a.qi419y!=9998){
					Calendar actual = new GregorianCalendar(c2seccion_04a.qi419y,Integer.parseInt(c2seccion_04a.qi419m.toString())-1,1);
					if(actual.compareTo(fechadeNinio())>0){
						MyUtil.MensajeGeneral(this.getActivity(), "La fecha ingresa no puede ser mayor a la fecha de nacimiento");
					}
				}
					
				if(c2seccion_04a.qi419y!=null && c2seccion_04a.qi419y==9998) {
					if (Util.esVacio(c2seccion_04a.qi420) && !chb420.isChecked()) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI420"); 
						view = txtQI420; 
						error = true; 
						return false; 
					}
				}
			}
		}
			
		if(c2seccion_04a.qi415==null) {
			if(c2seccion_04a.qi417!=null && c2seccion_04a.qi417==1) {
				if (Util.esVacio(c2seccion_04a.qi419m)  && !chb419_m.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI419M"); 
					view = txtQI419M; 
					error = true; 
					return false; 
				} 
			
				if (Util.esVacio(c2seccion_04a.qi419y) && !chb419_a.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI419Y"); 
					view = txtQI419Y; 
					error = true; 
					return false; 
				}
				
				if(c2seccion_04a.qi419y!=null && c2seccion_04a.qi419y==9998) {
					if (Util.esVacio(c2seccion_04a.qi420) && !chb420.isChecked()) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI420"); 
						view = txtQI420; 
						error = true; 
						return false; 
					}
				}
			}
		}
		
		if (Util.esVacio(c2seccion_04a.qi421)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI421"); 
			view = rgQI421; 
			error = true; 
			return false; 
		} 
				
		if(c2seccion_04a.qi421!=null && c2seccion_04a.qi421==1) {
			if (Util.esVacio(c2seccion_04a.qi422d) && !chb422_a.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI422D"); 
				view = txtQI422D; 
				error = true; 
				return false; 
			} 
			
			if (c2seccion_04a.qi422d!=null && Util.esMenor(c2seccion_04a.qi422d, 30)) {					
				validarMensaje("Verifique la cantidad ingresada en n�mero de d�as");
				error=true;
			} 
					
			if (Util.esVacio(c2seccion_04a.qi422i) && !chb422_m.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI422I"); 
				view = txtQI422I; 
				error = true; 
				return false; 
			} 
					
			if (!Util.esDiferente(c2seccion_04a.qi422d,0) && !Util.esDiferente(c2seccion_04a.qi422i,0)) {
				mensaje = "Los dos valores no pueden ser igual a 0"; 
				view = txtQI422D; 
				error = true; 
				return false; 
			}
		}
		
		if (Util.esVacio(c2seccion_04a.qi422a_a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI422A_A"); 
			view = rgQI422A_A; 
			error = true; 
			return false; 
		} 
		
		if (c2seccion_04a.qi422a_a==1) { 
			if (Util.esVacio(c2seccion_04a.qi422a_b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI422A_B"); 
				view = rgQI422A_B; 
				error = true; 
				return false; 
			} 
			if (c2seccion_04a.qi422a_b==1) {
				if (Util.esVacio(c2seccion_04a.qi422a_c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI422A_C"); 
					view = rgQI422A_C; 
					error = true; 
					return false; 
				}			
				if (c2seccion_04a.qi422a_c==1) {
					if (Util.esVacio(c2seccion_04a.qi422a_d)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI422A_D"); 
						view = rgQI422A_D; 
						error = true; 
						return false; 
					}
				}
			}
		}
				
		if (Util.esVacio(c2seccion_04a.qi423)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI423"); 
			view = rgQI423; 
			error = true; 
			return false; 
		} 
		
		if (Util.esVacio(c2seccion_04a.qi424)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI424"); 
			view = rgQI424; 
			error = true; 
			return false; 
		} 
		
		if (Util.esVacio(c2seccion_04a.qi425)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI425"); 
			view = rgQI425; 
			error = true; 
			return false; 
		} 
	}
	return true; 
	} 
	 
	
	public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		if(c2seccion_04a!=null){
			if (c2seccion_04a.qi421!=null) {
				c2seccion_04a.qi421=c2seccion_04a.getConvertQi421(c2seccion_04a.qi421);
			}
			if (c2seccion_04a.qi422a_a!=null) {
				c2seccion_04a.qi422a_a=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_a);
			}
			if (c2seccion_04a.qi422a_b!=null) {
				c2seccion_04a.qi422a_b=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_b);
			}
			if (c2seccion_04a.qi422a_c!=null) {
				c2seccion_04a.qi422a_c=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_c);
			}
			if (c2seccion_04a.qi422a_d!=null) {
				c2seccion_04a.qi422a_d=c2seccion_04a.getConvertQi422a(c2seccion_04a.qi422a_d);
			}
			
			if (c2seccion_04a.qi423!=null) {
				c2seccion_04a.qi423=c2seccion_04a.getConvertQi423(c2seccion_04a.qi423);
			}
			if (c2seccion_04a.qi424!=null) {
				c2seccion_04a.qi424=c2seccion_04a.getConvertQi424(c2seccion_04a.qi424);
			}
			if (c2seccion_04a.qi425!=null) {
				c2seccion_04a.qi425=c2seccion_04a.getConvertQi425(c2seccion_04a.qi425);
			}
		}

		if(chb419_m.isChecked() ) {
			c2seccion_04a.qi419m="98";			
		}
		
		if(chb419_a.isChecked() ) {
			c2seccion_04a.qi419y=9998;			
		}
		
		if(chb420.isChecked() ) {
			c2seccion_04a.qi420=98;			
		}
		
		if(chb422_a.isChecked() ) {
			c2seccion_04a.qi422d=998;			
		}
		
		if(chb422_m.isChecked() ) {
			c2seccion_04a.qi422i=98;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
