package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// ffff
public class CISECCION_04AFragment_009 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI428A; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI428B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI429; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI429A; 
	@FieldAnnotation(orderIndex=5) 
	public TextAreaField txtQI429A_OBS;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI430; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI430A; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI430B; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI430B_C; 
	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta428a ,lblpregunta428b, lblpregunta429, lblpregunta429a, lblpregunta430,lblpregunta430b_ind,lblpregunta429a_ind, 
	lblpregunta430a ,lblpregunta430b,lblpregunta430b_c,lblregla428b,lblnacimiento429a,lblvacio430b,lblpregunta429a_observacion; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargado1_3;

	public boolean esPreguntaInicial;
	GridComponent2 gdregla428b,gdnacimiento429a,gridPregunta430b;
	public TextField txtCabecera;
	public IntegerField txtQI430BC1,txtQI430BC2;
	public CheckBoxField chb429A_ns,chb429A_nc;
	String nombre_persona;
	public ButtonComponent btnNoSaberegla428b,btnNoSabenacimiento429a;
	public CISECCION_01_03 seccion1_3;
	
	public CheckBoxField chb428b;
//	public RadioGroupOtherField rg429a;
	
	public CISECCION_04AFragment_009() {} 
	public CISECCION_04AFragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI428A","QI428B","QI429","QI429A","QI429A_OBS","QI430","QI430A","QI430B","QI430B_C","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI428A","QI428B","QI429","QI429A","QI429A_OBS","QI430","QI430A","QI430B","QI430B_C")}; 
		seccionesCargado1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI221A","QI222","QI224","QI226","QI227","QI228","ID","HOGAR_ID","PERSONA_ID")};
		
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta428a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi428a);
		lblpregunta428b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi428b);
		lblpregunta429 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi429);
		lblpregunta429a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi429a);
		lblpregunta429a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi429a_ind);
		lblpregunta429a_observacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi429a_observacion);
		lblpregunta430 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi430);
		lblpregunta430a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi430a);
		lblpregunta430b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi430b);
		lblpregunta430b_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi430b_ind);
		lblpregunta430b_c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi430b_c).textSize(16);
		
		
		rgQI428A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi428a_1,R.string.c2seccion_04aqi428a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI428aChangeValue"); 
		
		txtQI428B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		lblregla428b  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi428b_1).textSize(16).centrar();
		
		
		chb428b=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi428b_2, "1:0").size(altoComponente, 200);
		chb428b.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb428b.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI428B);
			}
			else {
				Util.lockView(getActivity(),false, txtQI428B);	
			}
		}
		});
		 
		gdregla428b = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdregla428b.addComponent(lblregla428b);
		gdregla428b.addComponent(txtQI428B);
		gdregla428b.addComponent(chb428b);		
		
		
		rgQI429=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi429_1,R.string.c2seccion_04aqi429_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI429ChangeValue"); 
		
		txtQI429A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		lblnacimiento429a  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi429a_1).textSize(16).centrar();
		
//		rg429a = new RadioGroupOtherField(getActivity(),R.string.c2seccion_04aqi429a_2,R.string.c2seccion_04aqi429a_3).size(WRAP_CONTENT, 450).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI429aChangeValue");
		


		rgQI430=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi430_1,R.string.c2seccion_04aqi430_2,R.string.c2seccion_04aqi430_3,R.string.c2seccion_04aqi430_4,R.string.c2seccion_04aqi430_5,R.string.c2seccion_04aqi430_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI430A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi430a_1,R.string.c2seccion_04aqi430a_2,R.string.c2seccion_04aqi430a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI430AChangeValue"); 
		rgQI430B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi430b_1,R.string.c2seccion_04aqi430b_2,R.string.c2seccion_04aqi430b_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI430BChangeValue"); 
		
		txtQI430BC1=new IntegerField(this.getActivity()).size(75, 100).maxLength(4); 
		txtQI430BC2=new IntegerField(this.getActivity()).size(75, 100).maxLength(4);
		lblvacio430b = new LabelComponent(this.getActivity()).size(75, 100);
		
		chb429A_ns=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi429a_2, "1:0").size(altoComponente, 200);
		chb429A_ns.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb429A_ns.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI429A,chb429A_nc);
			}
			else {
				Util.lockView(getActivity(),false, txtQI429A,chb429A_nc);	
			}
		}
		});
		
		chb429A_nc=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi429a_3, "1:0").size(altoComponente, 200);
		chb429A_nc.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb429A_nc.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI429A,chb429A_ns);
			}
			else {
				Util.lockView(getActivity(),false, txtQI429A,chb429A_ns);	
			}
		}
		});
		
		gdnacimiento429a = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		gdnacimiento429a.addComponent(lblnacimiento429a);
		gdnacimiento429a.addComponent(txtQI429A);		
		gdnacimiento429a.addComponent(chb429A_ns);
		gdnacimiento429a.addComponent(chb429A_nc);
		
		gridPregunta430b = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPregunta430b.addComponent(rgQI430B,1,3);		
		gridPregunta430b.addComponent(txtQI430BC1);
		gridPregunta430b.addComponent(txtQI430BC2);
		gridPregunta430b.addComponent(lblvacio430b);
		
		txtQI429A_OBS= new TextAreaField(getActivity()).size(100, 750).maxLength(1000).alfanumerico();
		
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta428a,rgQI428A); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta428b,gdregla428b.component());
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta429,rgQI429); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta429a,lblpregunta429a_ind,gdnacimiento429a.component(),lblpregunta429a_observacion,txtQI429A_OBS);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta430,rgQI430); 
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta430a,rgQI430A); 
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta430b,lblpregunta430b_ind,gridPregunta430b.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		return contenedor; 
    }
    
//    public void onqrgQI429aChangeValue(){
//    	Integer data= Integer.parseInt(rg429a.getTagSelected("0").toString());
//    	if(Util.esDiferente(data, 0)) {
//			Util.cleanAndLockView(getActivity(), txtQI429A);
//		}
//		else {
//			Util.lockView(getActivity(),false, txtQI429A);	
//		}
//    }
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a);
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430!=null) {
				c2seccion_04a.qi430=c2seccion_04a.getConvertQi430(c2seccion_04a.qi430);
			}
			
			if (c2seccion_04a.qi430a!=null) {
				c2seccion_04a.qi430a=c2seccion_04a.getConvertQi430a(c2seccion_04a.qi430a);
			}
			
			if (c2seccion_04a.qi430b!=null) {
				c2seccion_04a.qi430b=c2seccion_04a.getConvertQi430b(c2seccion_04a.qi430b);
			}
		}
		
		
		if (c2seccion_04a.qi430b!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi430b,1) && txtQI430BC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi430b_c=Integer.parseInt(txtQI430BC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi430b,2) && txtQI430BC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi430b_c=Integer.parseInt(txtQI430BC2.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi430b_c=null;
			}
		}
		
		if (c2seccion_04a.qi430b==null)
			c2seccion_04a.qi430b_c=null;
		
		if(chb428b.isChecked() ) {
			c2seccion_04a.qi428b=98;			
		}
		
//		Integer s=Integer.parseInt(rg429a.getTagSelected("0").toString());
//		if(rg429a.getValue()!=null &&  !Util.esDiferente(s,1)) {
//			c2seccion_04a.qi429a=98;			
//		}
//		if(rg429a.getValue()!=null &&  !Util.esDiferente(s,2)) {
//			c2seccion_04a.qi429a=99;			
//		}
		
		if(chb429A_ns.isChecked() ) {
			c2seccion_04a.qi429a=98;			
		}
		if(chb429A_nc.isChecked() ) {
			c2seccion_04a.qi429a=99;			
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(!esPreguntaInicial) {
			if (Util.esVacio(c2seccion_04a.qi428a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI428A"); 
				view = rgQI428A; 
				error = true; 
				return false; 
			} 
			
			if (c2seccion_04a.qi428a!=null && c2seccion_04a.qi428a==1) {
				if (Util.esVacio(c2seccion_04a.qi428b) && !chb428b.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI428B"); 
					view = txtQI428B; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi428b!=null &&  !(c2seccion_04a.qi428b>=0 && c2seccion_04a.qi428b<=48) && !chb428b.isChecked()) {
					mensaje = "Valor fuera de rango Pregunta QI428B"; 
					view = txtQI428B; 
					error = true; 
					return false; 
				}
				
			}
			
		}
		if(App.getInstance().getSeccion04A().qi428!=null && App.getInstance().getSeccion04A().qi428==1) {
			if (Util.esVacio(c2seccion_04a.qi428b) && !chb428b.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI428B"); 
				view = txtQI428B; 
				error = true; 
				return false; 
			} 
			if (c2seccion_04a.qi428b!=null && !(c2seccion_04a.qi428b>=0 && c2seccion_04a.qi428b<=48) && !chb428b.isChecked()) {
				mensaje = "Valor fuera de rango Pregunta QI428B"; 
				view = txtQI428B; 
				error = true; 
				return false; 
			}
		}
		
		
		if(esPreguntaInicial) {
			if(seccion1_3.qi226==2) {
				if (Util.esVacio(c2seccion_04a.qi429)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI429"); 
					view = rgQI429; 
					error = true; 
					return false; 
				} 
							
				if(!Util.esDiferente(c2seccion_04a.qi429,1)) {
					
					if (Util.esVacio(c2seccion_04a.qi429a) && !(chb429A_nc.isChecked() || chb429A_ns.isChecked())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI448"); 
						view = txtQI429A; 
						error = true; 
						return false; 
					} 
					
//					if (Util.esVacio(c2seccion_04a.qi429a) && rg429a.getValue()==null) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta QI429A"); 
//						view = txtQI429A; 
//						error = true; 
//						return false; 
//					} 
					if (c2seccion_04a.qi429a!=null &&  c2seccion_04a.qi429a!=98 && c2seccion_04a.qi429a!=99 && !(c2seccion_04a.qi429a>=0 && c2seccion_04a.qi429a<=48)) {
						mensaje = "Valor fuera de rango Pregunta QI429A"; 
						view = txtQI429A; 
						error = true; 
						return false; 
					}
				}
			}
			else {
				if (Util.esVacio(c2seccion_04a.qi429a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI429A"); 
					view = txtQI429A; 
					error = true; 
					return false; 
				}
				if (c2seccion_04a.qi429a!=null &&  c2seccion_04a.qi429a!=98 && c2seccion_04a.qi429a!=99 &&  !(c2seccion_04a.qi429a>=0 && c2seccion_04a.qi429a<=48)) {
					mensaje = "Valor fuera de rango Pregunta QI429A"; 
					view = txtQI429A; 
					error = true; 
					return false; 
				}
			}
		}
		else {
			
				if (Util.esVacio(c2seccion_04a.qi429a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI429A"); 
					view = txtQI429A; 
					error = true; 
					return false; 
				} 
			
		}
		if (Util.esVacio(c2seccion_04a.qi430)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI430"); 
			view = rgQI430; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04a.qi430a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI430A"); 
			view = rgQI430A; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(c2seccion_04a.qi430a,1)) {
			if (Util.esVacio(c2seccion_04a.qi430b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI430B"); 
				view = rgQI430B; 
				error = true; 
				return false; 
			} 
		}
		
		if (!Util.esDiferente(c2seccion_04a.qi430b,1)) {
			if (Util.esVacio(c2seccion_04a.qi430b_c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.430BC1"); 
				view = txtQI430BC1; 
				error = true; 
				return false; 
			}
			//500
			if (!(c2seccion_04a.qi430b_c>=350 && c2seccion_04a.qi430b_c<=6000)) { 
				mensaje = "Valor fuera de rando en P.430BC1"; 
				view = txtQI430BC1; 
				error = true; 
				return false; 
			}
		}	
		
		if (!Util.esDiferente(c2seccion_04a.qi430b,2)) {
			if (Util.esVacio(c2seccion_04a.qi430b_c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.430BC2"); 
				error = true; 
				return false; 
			}
			
			if (!(c2seccion_04a.qi430b_c>=350 && c2seccion_04a.qi430b_c<=6000)) { 
				mensaje = "Valor fuera de rando en P.430BC2"; 
				view = txtQI430BC2; 
				error = true; 
				return false; 
			}
		}
		
//		if (Util.esVacio(c2seccion_04a.qi430b_c)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta QI430B_C"); 
//			view = txtQI430B_C; 
//			error = true; 
//			return false; 
//		} 
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430!=null) {
				c2seccion_04a.qi430=c2seccion_04a.setConvertQi430(c2seccion_04a.qi430);
			}
			
			if (c2seccion_04a.qi430a!=null) {
				c2seccion_04a.qi430a=c2seccion_04a.setConvertQi430a(c2seccion_04a.qi430a);
			}
			
			if (c2seccion_04a.qi430b!=null) {
				c2seccion_04a.qi430b=c2seccion_04a.setConvertQi430b(c2seccion_04a.qi430b);
			}
		}
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
				
		
		entityToUI(c2seccion_04a); 
		
		if (c2seccion_04a.qi430b_c!=null) {	
    		if (!Util.esDiferente(c2seccion_04a.qi430b,1)) {
    			txtQI430BC1.setText(c2seccion_04a.qi430b_c.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi430b,2)) {
    			txtQI430BC2.setText(c2seccion_04a.qi430b_c.toString());
    		}    		
    	}
		
		if(c2seccion_04a.qi428b!=null && c2seccion_04a.qi428b==98) {
			chb428b.setChecked(true);
			txtQI428B.setText("");
		}
//		if(c2seccion_04a.qi429a!=null && c2seccion_04a.qi429a==98) {
//			rg429a.setTagSelected(1);
//			txtQI429A.setText("");
//		}
//		if(c2seccion_04a.qi429a!=null && c2seccion_04a.qi429a==99) {
//			rg429a.setTagSelected(2);
//			txtQI429A.setText("");
//		}
		
		if(c2seccion_04a.qi429a!=null && c2seccion_04a.qi429a==98) {
			chb429A_ns.setChecked(true);
			txtQI429A.setText("");
		}
		
		if(c2seccion_04a.qi429a!=null && c2seccion_04a.qi429a==99) {
			chb429A_nc.setChecked(true);
			txtQI429A.setText("");
		}
		
		seccion1_3=  getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,
				seccionesCargado1_3);
		inicio(); 
    } 
    private void inicio() {
    	
    	onqrgQI430AChangeValue();
    	onqrgQI430BChangeValue();
    	evaluaPregunta();
    	ValidarsiesSupervisora();
//    	onqrgQI429aChangeValue();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
   
    	if(!esPreguntaInicial){    	
    	
    		Util.cleanAndLockView(getActivity(),rgQI429);
    		q3.setVisibility(View.GONE);
    		Util.lockView(getActivity(),false,rgQI428A);
    		q1.setVisibility(View.VISIBLE); 
    		onqrgQI428aChangeValue();
    		
    	}
    	else {
    		
    		if(App.getInstance().getSeccion04A().qi428==2) {
    			Util.cleanAndLockView(getActivity(),txtQI428B,chb428b);
        		q2.setVisibility(View.GONE);	
    		}
    		else if(App.getInstance().getSeccion04A().qi428==1) {
    			Util.lockView(getActivity(),false,txtQI428B,chb428b);
        		q2.setVisibility(View.VISIBLE);	
        		onqrgQI428aChangeValue();
    		}
    		
    		if(seccion1_3.qi226==2) {
    			Util.lockView(getActivity(),false,rgQI429);
        		q3.setVisibility(View.VISIBLE);
        		onqrgQI429ChangeValue();
    		}
    		else {
    			Util.cleanAndLockView(getActivity(),rgQI429);
        		q3.setVisibility(View.GONE);	
    		}
    		
    		Util.cleanAndLockView(getActivity(),rgQI428A);
    		q1.setVisibility(View.GONE);    		
    		
//    		chb429A_nc.readOnly();
    		chb429A_nc.setVisibility(View.GONE);
//    		Util.cleanAndLockView(getActivity(), chb429A_nc);
    	}
    	
    	if (c2seccion_04a.qi428b!=null && c2seccion_04a.qi428b==98) { 
    		Util.cleanAndLockView(getActivity(),txtQI428B); 
    	} else 	{
    		Util.lockView(getActivity(),false,txtQI428B);
    	}
    	
    	if (c2seccion_04a!=null && c2seccion_04a.qi429a!=null && (c2seccion_04a.qi429a==99 || c2seccion_04a.qi429a==98)) {
    		Util.cleanAndLockView(getActivity(),txtQI429A); 
    	}else{         			
    		Util.lockView(getActivity(),false,txtQI429A);
    	}
    	
    }
    

	
	public void onqrgQI428aChangeValue() {
		
		if(rgQI428A.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI428A.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,txtQI428B,chb428b);
				q2.setVisibility(View.VISIBLE);			
				txtQI428B.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),txtQI428B,chb428b);
				q2.setVisibility(View.GONE);
				txtQI429A.requestFocus();
				MyUtil.LiberarMemoria();
			}
		}
	}	
	
	
	public void onqrgQI429ChangeValue() {
		if(rgQI429.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI429.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,txtQI429A,chb429A_nc,chb429A_ns);
				q4.setVisibility(View.VISIBLE);			
				txtQI429A.requestFocus();
				if(esPreguntaInicial){
//					rg429a.lockButtons(true, 1);
					chb429A_nc.setVisibility(View.GONE);
//					chb429A_nc.readOnly();
//					Util.cleanAndLockView(getActivity(), chb429A_nc);
					}
			} 
			else {
				Util.cleanAndLockView(getActivity(),txtQI429A,chb429A_nc,chb429A_ns);
				q4.setVisibility(View.GONE);
				rgQI430.requestFocus();
				MyUtil.LiberarMemoria();
			}
		}
	}	
	
	public void onqrgQI430AChangeValue() {
		if(rgQI430A.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI430A.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,rgQI430B,txtQI430BC1,txtQI430BC2);
				q7.setVisibility(View.VISIBLE);
				rgQI430B.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),rgQI430B,txtQI430BC1,txtQI430BC2);
				q7.setVisibility(View.GONE);	
				MyUtil.LiberarMemoria();
			}
		}
	}
	
	public void onqrgQI430BChangeValue() {
		if(rgQI430B.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI430B.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI430BC2);
				c2seccion_04a.qi430b_c=null;
				Util.lockView(getActivity(),false,txtQI430BC1);				
				txtQI430BC1.requestFocus();				
			} 
			else if (MyUtil.incluyeRango(2,2,rgQI430B.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI430BC1);	
				c2seccion_04a.qi430b_c=null;
				Util.lockView(getActivity(),false,txtQI430BC2);			
				txtQI430BC2.requestFocus();				
			}
			else if (MyUtil.incluyeRango(3,3,rgQI430B.getTagSelected("").toString())) {
				c2seccion_04a.qi430b_c=null;		
				Util.cleanAndLockView(getActivity(),txtQI430BC1,txtQI430BC2);			
			    			
			}
		}
		
	}
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta428a.setText(lblpregunta428a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta428b.setText(lblpregunta428b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta429.setText(lblpregunta429.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta429a.setText(lblpregunta429a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta430.setText(lblpregunta430.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta430a.setText(lblpregunta430a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta430b.setText(lblpregunta430b.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI428A.readOnly();
			rgQI429.readOnly();
			rgQI430.readOnly();
			rgQI430A.readOnly();
			rgQI430B.readOnly();
			txtQI428B.readOnly();
			txtQI429A.readOnly();
			txtQI429A_OBS.setEnabled(false);
			txtQI430BC1.readOnly();
			txtQI430BC2.readOnly();
			chb428b.readOnly();
			chb429A_nc.readOnly();
			chb429A_ns.readOnly();
		}
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a);
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430!=null) {
				c2seccion_04a.qi430=c2seccion_04a.getConvertQi430(c2seccion_04a.qi430);
			}
			
			if (c2seccion_04a.qi430a!=null) {
				c2seccion_04a.qi430a=c2seccion_04a.getConvertQi430a(c2seccion_04a.qi430a);
			}
			
			if (c2seccion_04a.qi430b!=null) {
				c2seccion_04a.qi430b=c2seccion_04a.getConvertQi430b(c2seccion_04a.qi430b);
			}
		}
		
		
		if (c2seccion_04a.qi430b!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi430b,1) && txtQI430BC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi430b_c=Integer.parseInt(txtQI430BC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi430b,2) && txtQI430BC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi430b_c=Integer.parseInt(txtQI430BC2.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi430b_c=null;
			}
		}
		
		if (c2seccion_04a.qi430b==null)
			c2seccion_04a.qi430b_c=null;
		
		if(chb428b.isChecked() ) {
			c2seccion_04a.qi428b=98;			
		}
		
//		if(rg429a.getValue()!=null &&  !Util.esDiferente(1,Integer.parseInt(rg429a.getTagSelected("0").toString()),1)) {
//			c2seccion_04a.qi429a=98;			
//		}
//		if(rg429a.getValue()!=null &&  !Util.esDiferente(2,Integer.parseInt(rg429a.getTagSelected("0").toString()),2)) {
//			c2seccion_04a.qi429a=99;			
//		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
