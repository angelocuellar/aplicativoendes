package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_012 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI434; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI435; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQI435X; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI436U; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI436N;
	@FieldAnnotation(orderIndex=6) 
	public TextAreaField txtQI436_O;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI436A; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI436B_A ; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI436B_B ; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI436B_C ; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI436B_D ; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI436B_E ; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI436B_F ; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI436B_G ; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI436B_H ; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI436B_X ; 
	@FieldAnnotation(orderIndex=17) 
	public TextField txtQI436B_XI ; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI436C; 
	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta434,lblpregunta435,lblpregunta436,lblpregunta436a,lblpregunta436b,lblpregunta436_observacion,lblpregunta436_ind,lblpregunta436c,lblpregunta436n,lblvacio436,lblpregunta436b_ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public TextField txtCabecera;
	String nombre_persona;
	
	GridComponent2 gridPregunta436;
	public IntegerField txtQI436C1,txtQI436C2;
	
	public CISECCION_04AFragment_012() {} 
	public CISECCION_04AFragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI434","QI435","QI435X","QI436U","QI436N","QI436A","QI436_O","QI436B_A","QI436B_B","QI436B_C","QI436B_D","QI436B_E","QI436B_F","QI436B_G","QI436B_H","QI436B_X","QI436B_XI","QI436C","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI434","QI435","QI435X","QI436U","QI436N","QI436A","QI436_O","QI436B_A","QI436B_B","QI436B_C","QI436B_D","QI436B_E","QI436B_F","QI436B_G","QI436B_H","QI436B_X","QI436B_XI","QI436C")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta434 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi434);
		lblpregunta435 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi435);
		lblpregunta436 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi436u);
		lblpregunta436_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi436u_ind);
		lblpregunta436a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi436a);
		lblpregunta436b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi436b);
		lblpregunta436b_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi436b_ind);
		lblpregunta436c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi436c);
		
		lblpregunta436_observacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi436_o);
		
		rgQI434=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi434_1,R.string.c2seccion_04aqi434_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI434ChangeValue"); 
		rgQI435=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi435_1,R.string.c2seccion_04aqi435_2,R.string.c2seccion_04aqi435_3,R.string.c2seccion_04aqi435_4,R.string.c2seccion_04aqi435_5,R.string.c2seccion_04aqi435_6,R.string.c2seccion_04aqi435_7,R.string.c2seccion_04aqi435_8,R.string.c2seccion_04aqi435_9).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI435ChangeValue"); 
		txtQI435X=new TextField(this.getActivity()).size(altoComponente, 500);
		rgQI435.agregarEspecifique(8, txtQI435X);
		
		lblpregunta436n= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi436n).textSize(16);
		
		rgQI436U=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi436u_1,R.string.c2seccion_04aqi436u_2,R.string.c2seccion_04aqi436u_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI436ChangeValue"); 
		txtQI436C1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI436C2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		lblvacio436 = new LabelComponent(this.getActivity()).size(75, 100);
				
		gridPregunta436 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPregunta436.addComponent(rgQI436U,1,3);		
		gridPregunta436.addComponent(lblvacio436);
		gridPregunta436.addComponent(txtQI436C1);
		gridPregunta436.addComponent(txtQI436C2);
		
		txtQI436_O= new TextAreaField(getActivity()).size(100, 700).maxLength(500).alfanumerico();
		
		rgQI436A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi436a_1,R.string.c2seccion_04aqi436a_2,R.string.c2seccion_04aqi436a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI436aChangeValue"); 
		
		chbQI436B_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); //.callback("onqrgQS807aChangeValue");
		chbQI436B_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI436B_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi436b_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI436BxChangeValue");
		txtQI436B_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		
		rgQI436C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi436c_1,R.string.c2seccion_04aqi436c_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta434,rgQI434); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta435,rgQI435); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta436,lblpregunta436_ind,gridPregunta436.component(),lblpregunta436_observacion,txtQI436_O); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta436a,rgQI436A); 
		
		LinearLayout ly436bx = new LinearLayout(getActivity());
		ly436bx.addView(chbQI436B_X);
		ly436bx.addView(txtQI436B_XI);
		
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta436b,lblpregunta436b_ind,chbQI436B_A,chbQI436B_B,chbQI436B_C,chbQI436B_D,chbQI436B_E,chbQI436B_F,chbQI436B_G,chbQI436B_H,ly436bx);
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta436c,rgQI436C); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi436u!=null) {
				c2seccion_04a.qi436u=c2seccion_04a.getConvertQi436(c2seccion_04a.qi436u);
			}
			
			if (c2seccion_04a.qi436a!=null) {
				c2seccion_04a.qi436a=c2seccion_04a.getConvertQi436a(c2seccion_04a.qi436a);
			}						
		}
		
		if (c2seccion_04a.qi436u!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi436u,1) && txtQI436C1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi436n=Integer.parseInt(txtQI436C1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi436u,2) && txtQI436C2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi436n=Integer.parseInt(txtQI436C2.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi436n=null;
			}
		}		
		if (c2seccion_04a.qi436u==null)
			c2seccion_04a.qi436n=null;
		App.getInstance().getSeccion04A().qi435 = c2seccion_04a.qi435;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {				
				if(App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi435!=null) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_013.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(c2seccion_04a.qi434)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI434"); 
			view = rgQI434; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(c2seccion_04a.qi434,2)) {	
			if (Util.esVacio(c2seccion_04a.qi435)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI435"); 
				view = rgQI435; 
				error = true; 
				return false; 
			} 		
		
			if (!Util.esDiferente(c2seccion_04a.qi435,9)) {		
				if (Util.esVacio(c2seccion_04a.qi435x)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI435X"); 
					view = txtQI435X; 
					error = true; 
					return false; 
				} 
			}
		
		}
		
		if (c2seccion_04a.qi435==null) {					
			if (Util.esVacio(c2seccion_04a.qi436u)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI436U"); 
				view = rgQI436U; 
				error = true; 
				return false; 
			} 
			
			if (!Util.esDiferente(c2seccion_04a.qi436u,1) ) {
				if (Util.esVacio(c2seccion_04a.qi436n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.436C1"); 
					view = txtQI436C1; 
					error = true; 
					return false; 
				}
				if (Util.esMenor(c2seccion_04a.qi436n,1) || Util.esMayor(c2seccion_04a.qi436n,23) && Util.esDiferente(c2seccion_04a.qi436n,98)) {
					mensaje = "Valores validos en Horas de 1 a 23"; 
					view = txtQI436C1; 
					error = true; 
					return false; 
				}
			}	
			
			if (!Util.esDiferente(c2seccion_04a.qi436u,2)) {
				if (Util.esVacio(c2seccion_04a.qi436n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.436C2"); 
					view = txtQI436C2; 
					error = true; 
					return false; 
				}
				if (Util.esMenor(c2seccion_04a.qi436n,1) || Util.esMayor(c2seccion_04a.qi436n,90) && Util.esDiferente(c2seccion_04a.qi436n,98)) {
					mensaje = "Valores validos en D�as de 1 a 90"; 
					view = txtQI436C2; 
					error = true; 
					return false; 
				}
			}
			
			if (Util.esVacio(c2seccion_04a.qi436a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI436A"); 
				view = rgQI436A; 
				error = true; 
				return false; 
			} 
			
			if (!Util.esDiferente(c2seccion_04a.qi436a,1)) {
				if(!verificarCheck436B()) {
					mensaje = "Seleccione una opci�n en P.436B"; 
					view = chbQI436B_A; 
					error = true; 
					return false; 
				}
				
				if (chbQI436B_X.isChecked()) {
					if (Util.esVacio(c2seccion_04a.qi436b_xi)) { 
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI436B_XI; 
						error = true; 
						return false; 
					}
				}	
			}
			
			
			if (Util.esVacio(c2seccion_04a.qi436c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI436C"); 
				view = rgQI436C; 
				error = true; 
				return false; 
			} 
		
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		if(c2seccion_04a!=null) {
			if (c2seccion_04a.qi436u!=null) {
				c2seccion_04a.qi436u=c2seccion_04a.setConvertQi436(c2seccion_04a.qi436u);
			}
			
			if (c2seccion_04a.qi436a!=null) {
				c2seccion_04a.qi436a=c2seccion_04a.setConvertQi436a(c2seccion_04a.qi436a);
			}						
		}
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		
		
		
		if(c2seccion_04a.qi436n!=null) {	
    		if (!Util.esDiferente(c2seccion_04a.qi436u,2)) {
    			txtQI436C1.setText(c2seccion_04a.qi436n.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi436u,3)) {
    			txtQI436C2.setText(c2seccion_04a.qi436n.toString());
    		}  
    	}  	
		inicio(); 
    } 
    private void inicio() {
    	onqrgQI434ChangeValue();
    	onqrgQI435ChangeValue();
    	onqrgQI436aChangeValue();
    	onqrgQI436BxChangeValue();
    	onqrgQI436ChangeValue();
    	if (!chbQI436B_X.isChecked()) {    	
    		Util.cleanAndLockView(getActivity(),txtQI436B_XI);	
		}
    	else {
    		Util.lockView(getActivity(),false,txtQI436B_XI);  	
    	}
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 

	
	public boolean verificarCheck436B() {
  		if (chbQI436B_A.isChecked() || chbQI436B_B.isChecked() || chbQI436B_C.isChecked() || chbQI436B_D.isChecked() || chbQI436B_E.isChecked() || chbQI436B_F.isChecked() || chbQI436B_G.isChecked()
  				||chbQI436B_H.isChecked() || chbQI436B_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}	
	
	public void onqrgQI434ChangeValue() {
		if(rgQI434.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(2,2,rgQI434.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,rgQI435);
				q2.setVisibility(View.VISIBLE);
				rgQI435.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),rgQI435);
				q2.setVisibility(View.GONE);
				onqrgQI435ChangeValue();
				rgQI436U.requestFocus();	
				MyUtil.LiberarMemoria();
			}
		}
	}
	
	public void onqrgQI435ChangeValue() {
		if(rgQI435.getTagSelected("").toString()!=null) {
			if (!rgQI435.getTagSelected("").toString().isEmpty()) {			
				ocultarCampos();
				App.getInstance().getSeccion04A().qi435 = Integer.parseInt(rgQI435.getValue().toString());
			} 
			else {
				mostrarCampos();	
				App.getInstance().getSeccion04A().qi435 = null;
			}
		} else {
			App.getInstance().getSeccion04A().qi435 = null;
			mostrarCampos();	
		}
	}
	
	public void ocultarCampos() {
		Util.cleanAndLockView(getActivity(),rgQI436U,txtQI436C1,txtQI436C2,txtQI436_O,rgQI436A,chbQI436B_A,chbQI436B_B,
		chbQI436B_C,chbQI436B_D,chbQI436B_E,chbQI436B_F,chbQI436B_G,chbQI436B_H,chbQI436B_X,txtQI436B_XI,rgQI436C);
		q3.setVisibility(View.GONE);
		q4.setVisibility(View.GONE);
		q5.setVisibility(View.GONE);
		q6.setVisibility(View.GONE);
		MyUtil.LiberarMemoria();
	}
	
	public void mostrarCampos() {
		Util.lockView(getActivity(),false,rgQI436U,txtQI436C1,txtQI436C2,txtQI436_O,rgQI436A,chbQI436B_A,chbQI436B_B,
		chbQI436B_C,chbQI436B_D,chbQI436B_E,chbQI436B_F,chbQI436B_G,chbQI436B_H,chbQI436B_X,txtQI436B_XI,rgQI436C);
		q3.setVisibility(View.VISIBLE);
		q4.setVisibility(View.VISIBLE);
		q5.setVisibility(View.VISIBLE);
		q6.setVisibility(View.VISIBLE);
		rgQI436A.requestFocus();
	}
	
	public void onqrgQI436aChangeValue() {
		if(rgQI436A.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI436A.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,chbQI436B_A,chbQI436B_B,chbQI436B_C,chbQI436B_D,chbQI436B_E,chbQI436B_F,chbQI436B_G,chbQI436B_H,chbQI436B_X,txtQI436B_XI);
				q5.setVisibility(View.VISIBLE);
				onqrgQI436BxChangeValue();
				chbQI436B_A.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),chbQI436B_A,chbQI436B_B,chbQI436B_C,chbQI436B_D,chbQI436B_E,chbQI436B_F,chbQI436B_G,chbQI436B_H,chbQI436B_X,txtQI436B_XI);
				q5.setVisibility(View.GONE);
				rgQI436C.requestFocus();		
				MyUtil.LiberarMemoria();
			}
		}
	}
	
	public void onqrgQI436BxChangeValue() {  	
    	if (chbQI436B_X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQI436B_XI);
  			txtQI436B_XI.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI436B_XI);  
  		}	
    }
	
	public void onqrgQI436ChangeValue() {		
		if (MyUtil.incluyeRango(2,2,rgQI436U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI436C2);
			c2seccion_04a.qi436n=null;
			Util.lockView(getActivity(),false,txtQI436C1);				
			txtQI436C1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(3,3,rgQI436U.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI436C1);	
			c2seccion_04a.qi436n=null;
			Util.lockView(getActivity(),false,txtQI436C2);			
			txtQI436C2.requestFocus();				
		}
		
		else if (MyUtil.incluyeRango(1,1,rgQI436U.getTagSelected("").toString())) {
			c2seccion_04a.qi436n=null;		
			Util.cleanAndLockView(getActivity(),txtQI436C1,txtQI436C2);		
		}		
	}
	
	public void renombrarEtiquetas(){	
    	lblpregunta434.setText(lblpregunta434.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta435.setText(lblpregunta435.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta436.setText(lblpregunta436.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta436a.setText(lblpregunta436a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta436b.setText(lblpregunta436b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta436c.setText(lblpregunta436c.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI434.readOnly();
			rgQI435.readOnly();
			rgQI436A.readOnly();
			rgQI436C.readOnly();
			rgQI436U.readOnly();
			chbQI436B_A.readOnly();
			chbQI436B_B.readOnly();
			chbQI436B_C.readOnly();
			chbQI436B_D.readOnly();
			chbQI436B_E.readOnly();
			chbQI436B_F.readOnly();
			chbQI436B_G.readOnly();
			chbQI436B_H.readOnly();
			chbQI436B_X.readOnly();
			txtQI435X.readOnly();
			txtQI436_O.setEnabled(false);
			txtQI436B_XI.readOnly();
			txtQI436C1.readOnly();
			txtQI436C2.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi436u!=null) {
				c2seccion_04a.qi436u=c2seccion_04a.getConvertQi436(c2seccion_04a.qi436u);
			}
			
			if (c2seccion_04a.qi436a!=null) {
				c2seccion_04a.qi436a=c2seccion_04a.getConvertQi436a(c2seccion_04a.qi436a);
			}						
		}
		
		if (c2seccion_04a.qi436u!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi436u,1) && txtQI436C1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi436n=Integer.parseInt(txtQI436C1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi436u,2) && txtQI436C2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi436n=Integer.parseInt(txtQI436C2.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi436n=null;
			}
		}		
		if (c2seccion_04a.qi436u==null)
			c2seccion_04a.qi436n=null;
		App.getInstance().getSeccion04A().qi435 = c2seccion_04a.qi435;
		
	 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {				
					if(App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi435!=null) {
						List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
						seccs.add(CISECCION_04AFragment_013.seccionesGrabado);
						SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
						getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
					}
				
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL;
	} 
} 
