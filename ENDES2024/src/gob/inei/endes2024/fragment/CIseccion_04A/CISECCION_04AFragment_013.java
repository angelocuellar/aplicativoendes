package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DateTimeField.SHOW_HIDE;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_013 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI438; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI439; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI440; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQI440X; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI440B; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI440B_O; 
	 
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta438,lblpregunta439,lblpregunta439_ind,lblpregunta440,lbllechemes439,lblpregunta440B,lblpregunta440B_ind1,lblpregunta440B_ind2; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI438","QI439","QI440","QI440X","QI440B","QI440B_O")};
	SeccionCapitulo[] seccionesCargado,seccionesCargado1_3; 
	SeccionCapitulo[] seccionesPersonas;

	public ButtonComponent btnNoSabe439;
	public GridComponent2 gdPregunta439;
	public TextField txtCabecera;
	String nombre_persona;
	public boolean esPrimerRegistro;
	public CISECCION_02 nacimiento;
	public CISECCION_01_03 cs2seccion1_3;
	public CheckBoxField chb439;
	
	public CISECCION_04AFragment_013() {} 
	public CISECCION_04AFragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 			
		
		rango(getActivity(), txtQI439, 0, 48); 		
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI436A","QI438","QI439","QI440","QI440X","QI440B","QI440B_O","QI436A","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
		seccionesCargado1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta438 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi438);
		lblpregunta439 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi439);
		lblpregunta439_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi439_ind);
		lblpregunta440 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi440);
		lblpregunta440B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi440b);
		lblpregunta440B_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi440b_ind1).negrita();
		lblpregunta440B_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi440b_ind2);
		
		rgQI438=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi438_1,R.string.c2seccion_04aqi438_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI438ChangeValue"); 
		
		lbllechemes439  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi439m).textSize(16).centrar();
		txtQI439=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqrgQI439mChangeValue");	
		
		chb439=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi419m_ns, "1:0").size(altoComponente, 200);
		chb439.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb439.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI439);
			}
			else {
				Util.lockView(getActivity(),false, txtQI439);	
			}
		}
		});
				
		gdPregunta439 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdPregunta439.addComponent(lbllechemes439);
		gdPregunta439.addComponent(txtQI439);
		gdPregunta439.addComponent(chb439);
		
		rgQI440=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi440_1,R.string.c2seccion_04aqi440_2,R.string.c2seccion_04aqi440_3,R.string.c2seccion_04aqi440_4,R.string.c2seccion_04aqi440_5,R.string.c2seccion_04aqi440_6,R.string.c2seccion_04aqi440_7,R.string.c2seccion_04aqi440_8,R.string.c2seccion_04aqi440_9,R.string.c2seccion_04aqi440_10,R.string.c2seccion_04aqi440_11,R.string.c2seccion_04aqi440_12,R.string.c2seccion_04aqi440_13).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI440X = new TextField(this.getActivity()).size(altoComponente, 500);
		rgQI440.agregarEspecifique(12, txtQI440X);
		
		rgQI440B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi440b_1,R.string.c2seccion_04aqi440b_2,R.string.c2seccion_04aqi440b_8,R.string.c2seccion_04aqi440b_96).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI440B_O = new TextField(this.getActivity()).size(altoComponente, 500);
		rgQI440B.agregarEspecifique(3, txtQI440B_O);
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta438, rgQI438); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta439,lblpregunta439_ind,gdPregunta439.component()); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta440,rgQI440);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta440B,lblpregunta440B_ind1, lblpregunta440B_ind2,rgQI440B);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q4);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null){
			 if(c2seccion_04a.qi440b!=null) {
				 c2seccion_04a.qi440b=c2seccion_04a.getConvertQi440b(c2seccion_04a.qi440b);
			 }
		}
		
		
		App.getInstance().getInstance().getSeccion04A().qi438 = c2seccion_04a.qi438;
		
		if(chb439.isChecked() ) {
			c2seccion_04a.qi439=98;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));		
		Calendar fecharef = new GregorianCalendar();
		Integer meses_ninio = MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento, fecharef);
		
		if(nacimiento.qi216==1) {
			if (Util.esVacio(c2seccion_04a.qi438)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI438"); 
				view = rgQI438; 
				error = true; 
				return false; 
			} 
			
			if (Util.esDiferente(c2seccion_04a.qi438,1)) {	
				if (Util.esVacio(c2seccion_04a.qi439) && !chb439.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI439"); 
					view = txtQI439; 
					error = true; 
					return false; 
				} 
				
				
				
				if(nacimiento.qi217cons!=null){
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date2 = null;
					try {
						date2 = df.parse(nacimiento.qi217cons);
					} catch (ParseException e) {
						e.printStackTrace();
					}		
					Calendar cal = Calendar.getInstance();
					cal.setTime(date2);
					fecharef=cal;
				}	
				
				
				
				if (c2seccion_04a.qi439!=98 && c2seccion_04a.qi439 > meses_ninio ) { 
					mensaje = "Cantidad de meses mayor que la edad del ni�o"; 
					view = txtQI439; 
					error = true; 
					return false; 
				} 
				
				
				if (Util.esVacio(c2seccion_04a.qi440)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI440"); 
					view = rgQI440; 
					error = true; 
					return false; 
				} 
				
				if (!Util.esDiferente(c2seccion_04a.qi440,13)) {	
					if (Util.esVacio(c2seccion_04a.qi440x)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI440X"); 
						view = txtQI440X; 
						error = true; 
						return false; 
					} 
				}
			}
		}
		else {
			
			if (Util.esVacio(c2seccion_04a.qi439) && !chb439.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI439"); 
				view = txtQI439; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(c2seccion_04a.qi440)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI440"); 
				view = rgQI440; 
				error = true; 
				return false; 
			} 
			
			if (!Util.esDiferente(c2seccion_04a.qi440,13)) {	
				if (Util.esVacio(c2seccion_04a.qi440x)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI440X"); 
					view = txtQI440X; 
					error = true; 
					return false; 
				} 
			}
		
		}
		
//		if ((MyUtil.incluyeRango(2,2,rgQI438.getTagSelected("").toString())  && meses_ninio >=6 && MyUtil.incluyeRango(6,97,txtQI439.getText().toString()) ) 
//				|| (meses_ninio >=6 && MyUtil.incluyeRango(1,1,rgQI438.getTagSelected("").toString()) ) ) {
//		
		Integer in436a=c2seccion_04a.qi436a==null?0:c2seccion_04a.qi436a;
		
		if ( (!(in436a==1 || meses_ninio <6))&& ((MyUtil.incluyeRango(2,2,rgQI438.getTagSelected("").toString()) && MyUtil.incluyeRango(6,97,txtQI439.getText().toString()) ) 
				|| (MyUtil.incluyeRango(1,1,rgQI438.getTagSelected("").toString()) ) ) ) {
			if (Util.esVacio(c2seccion_04a.qi440b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI440B"); 
				view = rgQI440B; 
				error = true; 
				return false; 
			} 
			
			if (!Util.esDiferente(c2seccion_04a.qi440b,4)) {	
				if (Util.esVacio(c2seccion_04a.qi440b_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI440B_O"); 
					view = txtQI440B_O; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	
    	Log.e("hhhhhhhhhhhhhhhhhhhhhhhhhh","gggggggggggggg");
    	
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		esPrimerRegistro = App.getInstance().getNacimiento().esPrimerRegistro;
		nacimiento =  getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimiento().id,App.getInstance().getNacimiento().hogar_id,App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesPersonas);
				
		cs2seccion1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado1_3);
		App.getInstance().setNacimiento(nacimiento);
		App.getInstance().getNacimiento().esPrimerRegistro = esPrimerRegistro;
		renombrarEtiquetas();
		
		if (c2seccion_04a!=null){
			 if(c2seccion_04a.qi440b!=null) {
				 c2seccion_04a.qi440b=c2seccion_04a.setConvertQi440b(c2seccion_04a.qi440b);
			 }
		}
		
		entityToUI(c2seccion_04a); 
		
		
//		if(c2seccion_04a!=null && c2seccion_04a.qi440b!=null) {
//    		c2seccion_04a.qi440b=c2seccion_04a.setConvertQi440b(c2seccion_04a.qi440b);
//		}
		
		Log.e("c2seccion_04a.qi440b "," "+ c2seccion_04a.qi440b);
		Log.e("c2seccion_04a.qi440b_o "," "+ c2seccion_04a.qi440b_o);
		
		if(c2seccion_04a.qi439!=null && c2seccion_04a.qi439==98) {
			chb439.setChecked(true);
			txtQI439.setText("");
		}
		
		
		
		inicio(); 
    } 
    
    
    private void inicio() { 

//    	if(c2seccion_04a.qi440b != null){
//        	ToastMessage.msgBox(this.getActivity(), c2seccion_04a.qi440b_o.toString()== null?"nulo":c2seccion_04a.qi440b_o.toString()+ " "+ c2seccion_04a.qi440b.toString()== null?"nulo obs":c2seccion_04a.qi440b.toString()
//        	        , ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//    	}

		
    	Log.e("dentro de inicio"," "+ c2seccion_04a.qi440b);
    	
    	if(nacimiento.qi216==2) {
    		Util.cleanAndLockView(getActivity(),rgQI438);
    		q1.setVisibility(View.GONE);
    		onqrgQI438ChangeValue();
    	}
    	else {
    		Util.lockView(getActivity(),false,rgQI438);
    		q1.setVisibility(View.VISIBLE); 
    		onqrgQI438ChangeValue();
    	}
    	
//    	Integer in436a=c2seccion_04a.qi436a==null?0:c2seccion_04a.qi436a;
//		
//		if(in436a == 1){
//    		Util.cleanAndLockView(getActivity(),rgQI440B);
//    		q4.setVisibility(View.GONE);
//    		ToastMessage.msgBox(this.getActivity(), c2seccion_04a.qi436a.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//    	}else{
//    		Util.lockView(getActivity(),false,rgQI440B);
//    		q4.setVisibility(View.VISIBLE);
//    	}
		
    	ValidarsiesSupervisora();	
    	txtCabecera.requestFocus();
    } 
    

	
	public void onqrgQI438ChangeValue() {
		Log.e("dentro de 438 change"," "+ c2seccion_04a.qi440b);
		if(rgQI438.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI438.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI439,chb439,rgQI440);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				App.getInstance().getInstance().getSeccion04A().qi438 = 1;
				MyUtil.LiberarMemoria();
				onqrgQI439mChangeValue() ;
				
				if(cs2seccion1_3.qi226==1) {
					ToastMessage.msgBox(this.getActivity(), "Verificar respuesta, la mef esta embarazada", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				}
			} 
			else {				
				Util.lockView(getActivity(),false,txtQI439,chb439,rgQI440);
				q2.setVisibility(View.VISIBLE);
				q3.setVisibility(View.VISIBLE);
				onqrgQI439mChangeValue() ;
				App.getInstance().getInstance().getSeccion04A().qi438 = 2;
				txtQI439.requestFocus();	
			}
		}
		
		
		if (c2seccion_04a.qi439!=null && c2seccion_04a.qi439==98) { 
			Util.cleanAndLockView(getActivity(),txtQI439); 
		} 
		else{ 
			Util.lockView(getActivity(),false,txtQI439);
		}
	}
    
//	public void onqrgQI439mChangeValue() {
//		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
//		Calendar fecharef = new GregorianCalendar();
//		Integer meses_ninio = MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento, fecharef);	
//		
//		Integer in436a=c2seccion_04a.qi436a==null?0:c2seccion_04a.qi436a;
//		Integer in438=c2seccion_04a.qi438==null?0:c2seccion_04a.qi438;
////		Integer in439=c2seccion_04a.qi439==null?0:c2seccion_04a.qi439;
//		
//		Integer valor=txtQI439.getText().toString().trim().length()>0?Integer.parseInt(txtQI439.getText().toString().trim()):0;
//		
//		Log.e("meses_ninio "," "+ meses_ninio);
//		Log.e("qi436a"," "+ in436a);
//		Log.e("rgQI438 "," "+ in438);
//		Log.e("rgQI439 "," "+ valor);
//
////		MyUtil.LiberarMemoria();
////		ToastMessage.msgBox(this.getActivity(), valor.toString(), ToastMessage.MESSAGE_INFO , ToastMessage.DURATION_SHORT);
//		
//	
//		
//		if (in436a==1 || meses_ninio <6 || valor<6) {
////			MyUtil.LiberarMemoria();
//			ToastMessage.msgBox(this.getActivity(), "entro onchange", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//			Log.e("ddduuu","entro qi436a=1 o ninio<6 meses ");			
//			Util.cleanAndLockView(getActivity(),rgQI440B);
//			q4.setVisibility(View.GONE);
//		}
//		else{
//			Log.e("ddd"," caso contrario");
//			
//			if ( ((MyUtil.incluyeRango(2,2,rgQI438.getTagSelected("").toString()) && MyUtil.incluyeRango(6,97,txtQI439.getText().toString()) ) 
//					|| (MyUtil.incluyeRango(1,1,rgQI438.getTagSelected("").toString()) ) ) ) {
//				Log.e("ddduu2","entro 1");
//				Util.lockView(getActivity(),false,rgQI440B);
//				q4.setVisibility(View.VISIBLE);
//			}
//			else{
//				Log.e("ddduu3","entro 2");
//				Util.cleanAndLockView(getActivity(),rgQI440B);
//				q4.setVisibility(View.GONE);
//			}
//		}
//		
////		if(valor<6){
////			Log.e("valor:"," "+ valor);
////			Util.cleanAndLockView(getActivity(),rgQI440B);
////			q4.setVisibility(View.GONE);
////		}else{
////			Util.lockView(getActivity(),false,rgQI440B);
////			Log.e("valor_entro:"," "+ valor);
////			q4.setVisibility(View.VISIBLE);
////		}
//	}

	
	public void onqrgQI439mChangeValue() {
		
		Log.e("dentro de 439 change"," "+ c2seccion_04a.qi440b);
		
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
		Calendar fecharef = new GregorianCalendar();
		
		Integer in436a=c2seccion_04a.qi436a==null?0:c2seccion_04a.qi436a;
		Integer in439=c2seccion_04a.qi439==null?0:c2seccion_04a.qi439;
		Integer in216=nacimiento.qi216==null?0:nacimiento.qi216;
		Integer in220u=nacimiento.qi220u==null?0:nacimiento.qi220u;
		Integer in220=nacimiento.qi220n==null?0:nacimiento.qi220n;
		
		Integer meses_ninio = MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento, fecharef);		
		
		if (in436a!=1 && ((MyUtil.incluyeRango(2,2,rgQI438.getTagSelected("").toString())  && meses_ninio >=6 && MyUtil.incluyeRango(6,97,txtQI439.getText().toString()) ) 
				|| (meses_ninio >=6 && MyUtil.incluyeRango(1,1,rgQI438.getTagSelected("").toString()) ) ) ) {
			Log.e("ddd","entro 1");
			Util.lockView(getActivity(),false,rgQI440B);
			q4.setVisibility(View.VISIBLE);
		}
		else{
			Log.e("in220","entro 2"+in220);
			Log.e("in439","entro 2"+in439);
			Log.e("hola gato", "hhh"+MyUtil.incluyeRango(6,97,txtQI439.getText().toString()));
			if(in216 == 2){
				if(in436a == 1){
					Util.cleanAndLockView(getActivity(),rgQI440B);
					//ToastMessage.msgBox(this.getActivity(), "UNO", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					q4.setVisibility(View.GONE);
				}else if( (in220u == 2 ? in220 : in220u == 3 ? in220*12 : in220*0 ) >= 6  && MyUtil.incluyeRango(6,97,txtQI439.getText().toString()) ){
					Log.e("ddd","entro 1 muerto");
					//ToastMessage.msgBox(this.getActivity(), "DOS", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					Util.lockView(getActivity(),false,rgQI440B);
					q4.setVisibility(View.VISIBLE);
				}else{
					Log.e("ddd","entro 2 muerto");
					//ToastMessage.msgBox(this.getActivity(), in436a.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					Util.cleanAndLockView(getActivity(),rgQI440B);
					q4.setVisibility(View.GONE);
				}
			}else{
				Util.cleanAndLockView(getActivity(),rgQI440B);
				//ToastMessage.msgBox(this.getActivity(), "CUATRO", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				q4.setVisibility(View.GONE);
			}
			
		}
		

		Log.e("final de 439 change"," "+ c2seccion_04a.qi440b);

	}
	
	public void renombrarEtiquetas(){	
    	lblpregunta438.setText(lblpregunta438.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta439.setText(lblpregunta439.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta440.setText(lblpregunta440.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta440B.setText(lblpregunta440B.getText().toString().replace("(NOMBRE)", nombre_persona));
    }

	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI438.readOnly();
			rgQI440.readOnly();
			rgQI440B.readOnly();
			txtQI440B_O.readOnly();
			txtQI439.readOnly();
			txtQI440X.readOnly();
			chb439.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
//		if (c2seccion_04a!=null && c2seccion_04a.qi440b!=null) {
//			c2seccion_04a.qi440b=c2seccion_04a.getConvertQi440b(c2seccion_04a.qi440b);
//		}
		
		if (c2seccion_04a!=null){
			 if(c2seccion_04a.qi440b!=null) {
				 c2seccion_04a.qi440b=c2seccion_04a.getConvertQi440b(c2seccion_04a.qi440b);
			 }
		}
		
		App.getInstance().getInstance().getSeccion04A().qi438 = c2seccion_04a.qi438;
		
		if(chb439.isChecked() ) {
			c2seccion_04a.qi439=98;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
