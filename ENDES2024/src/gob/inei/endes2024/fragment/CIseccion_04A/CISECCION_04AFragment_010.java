package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// HOLAAA
// 
public class CISECCION_04AFragment_010 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI430C; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI430D; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI430DN;
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI431A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI432;	
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI433; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI433AU; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI433AN;
	@FieldAnnotation(orderIndex=9)
	public TextAreaField txtQI433A_O;
	
	CISECCION_04A c2seccion_04a; 
	Seccion01 persona=null;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta430c,lblpregunta430d,lblpregunta431a,lblpregunta432,lblpregunta433,lblvacio433a,lblpregunta430c_ind,lblpregunta433a_ind,
	lblpregunta433a,lblpregunta430d_nom,lblpregunta433a_c,lblpregunta433A_observacion;
//	public IntegerField txtQI430DNN,txtQI430DNNN;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8;
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoHS01; 
// 
	GridComponent2 gridPregunta433a;
	public IntegerField txtQI433AC1,txtQI433AC2,txtQI433AC3;
	public TextField txtCabecera;
	String nombre_persona;
	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_010() {} 
	public CISECCION_04AFragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
//		rango(getActivity(), txtQI430DNNN, 00000001, 99999998); 				
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI430C","QI430D","QI430DN","QI431A","QI432","QI433","QI433AU","QI433AN","QI433A_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI430C","QI430D","QI430DN","QI431A","QI432","QI433","QI433AU","QI433AN","QI433A_O")};
		seccionesCargadoHS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta430c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi430c);
		lblpregunta430c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi430c_ind);
		lblpregunta430d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04aqi430d).negrita();
		lblpregunta430d_nom = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi430dn).textSize(16);
		lblpregunta431a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi431a);
		lblpregunta432 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi432);
		lblpregunta433 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi433);
		lblpregunta433a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");;
		lblpregunta433a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi433au_ind);
		lblpregunta433a_c= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi433an).textSize(16);
		lblpregunta433A_observacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi433au_observacion);
		
		rgQI430C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi430c_1,R.string.c2seccion_04aqi430c_2,R.string.c2seccion_04aqi430c_3,R.string.c2seccion_04aqi430c_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI430CChangeValue"); 
		rgQI430D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi430d_0,R.string.c2seccion_04aqi430d_1,R.string.c2seccion_04aqi430d_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI430DNCChangeValue"); 
		txtQI430DN=new IntegerField(this.getActivity()).size(altoComponente, 250).maxLength(8).alinearIzquierda();
				 
		rgQI431A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi431a_1,R.string.c2seccion_04aqi431a_2,R.string.c2seccion_04aqi431a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI432=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi432_1,R.string.c2seccion_04aqi432_2,R.string.c2seccion_04aqi432_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI432ChangeValue"); 
		rgQI433=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi433_1,R.string.c2seccion_04aqi433_2,R.string.c2seccion_04aqi433_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI433ChangeValue"); 
		rgQI433AU=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi433au_1,R.string.c2seccion_04aqi433au_2,R.string.c2seccion_04aqi433au_3,R.string.c2seccion_04aqi433au_4).size(225,230).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI433AChangeValue"); 
		txtQI433A_O= new TextAreaField(getActivity()).size(100, 700).maxLength(1000).alfanumerico();
		
		txtQI433AC1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI433AC2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		txtQI433AC3=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		lblvacio433a = new LabelComponent(this.getActivity()).size(75, 100);
		
		
		gridPregunta433a = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPregunta433a.addComponent(rgQI433AU,1,4);		
		gridPregunta433a.addComponent(txtQI433AC1);
		gridPregunta433a.addComponent(txtQI433AC2);
		gridPregunta433a.addComponent(txtQI433AC3);
		gridPregunta433a.addComponent(lblvacio433a);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta430c,lblpregunta430c_ind,rgQI430C); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta430d,rgQI430D,txtQI430DN); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta431a,rgQI431A); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta432,rgQI432); 
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta433,rgQI433); 
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta433a,lblpregunta433a_ind,gridPregunta433a.component(),lblpregunta433A_observacion,txtQI433A_O);
			
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		return contenedor; 
    } 
   
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430c!=null) {
				c2seccion_04a.qi430c=c2seccion_04a.getConvertQi430c(c2seccion_04a.qi430c);
			}
			
//			if (c2seccion_04a.qi430d!=null) {
//				c2seccion_04a.qi430d=c2seccion_04a.getConvertQi430d(c2seccion_04a.qi430d);
//			}
			
			if (c2seccion_04a.qi431a!=null) {
				c2seccion_04a.qi431a=c2seccion_04a.getConvertQi431a(c2seccion_04a.qi431a);
			}
			
			if (c2seccion_04a.qi432!=null) {
				c2seccion_04a.qi432=c2seccion_04a.getConvertQi432(c2seccion_04a.qi432);
			}
			
			if (c2seccion_04a.qi433!=null) {
				c2seccion_04a.qi433=c2seccion_04a.getConvertQi433(c2seccion_04a.qi433);
			}
			
			if (c2seccion_04a.qi433au!=null) {
				c2seccion_04a.qi433au=c2seccion_04a.getConvertQi433a(c2seccion_04a.qi433au);
			}
		}
		
		if (c2seccion_04a.qi433au!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi433au,1) && txtQI433AC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi433au,2) && txtQI433AC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi433au,3) && txtQI433AC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi433an=null;
			}
		}	
		
		if (c2seccion_04a.qi433au==null)
			c2seccion_04a.qi433an=null;
		
		App.getInstance().getSeccion04A().qi433 = c2seccion_04a.qi433;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi433!=1) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_011.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_011.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(c2seccion_04a.qi430c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI430C"); 
			view = rgQI430C; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(c2seccion_04a.qi430c,1)) {
			if (Util.esVacio(c2seccion_04a.qi430d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI430D"); 
				view = rgQI430D; 
				error = true; 
				return false; 
			} 
		}
//		Log.e("qi430d",""+c2seccion_04a.qi430d);
//		Log.e("qi430dn",""+c2seccion_04a.qi430dn);
		
		if (!Util.esDiferente(c2seccion_04a.qi430d,1,2)) {
			if (Util.esVacio(c2seccion_04a.qi430dn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI430DN"); 
				view = txtQI430DN; 
				error = true; 
				return false;
			} 
			if(c2seccion_04a.qi430dn.length()<8) {
				mensaje = "El valor ingresado es incorrecto"; 
				view = txtQI430DN; 
				error = true; 
				return false;
			}
		}
//		if (!Util.esDiferente(c2seccion_04a.qi430d,2)) {
//			if (Util.esVacio(c2seccion_04a.qi430dn)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QI430DNNN"); 
//				MyUtil.MensajeGeneral(this.getActivity(), mensaje); 
//			} 
//			
//			if(c2seccion_04a.qi430dn.length()<8) {
//				view = txtQI430DNNN; 
//				error = true; 
//				return false;
//			}
//		}
//		Log.e("qi430d d ",""+c2seccion_04a.qi430d);
//		Log.e("qi430dn d",""+c2seccion_04a.qi430dn);

		
		if(esPreguntaInicial) {
			if(!(App.getInstance().getSeccion04A().qi426a==1 || App.getInstance().getSeccion04A().qi426a==11 || App.getInstance().getSeccion04A().qi426a==14)) {
				if (Util.esVacio(c2seccion_04a.qi431a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI431A"); 
					view = rgQI431A; 
					error = true; 
					return false; 
				}
			}
		}
		if (Util.esVacio(c2seccion_04a.qi432)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI432"); 
			view = rgQI432; 
			error = true; 
			return false; 
		} 		
		
		if(!Util.esDiferente(c2seccion_04a.qi432, 1) && (NosabeSitieneSeguro()|| NotieneSeguro()) && persona!=null){
			MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren. ");
    	}
    	if(!Util.esDiferente(c2seccion_04a.qi432, 2)&& TieneSeguroSis() && persona!=null){
    		MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren.. ");   		
    	}
    	if(!Util.esDiferente(c2seccion_04a.qi432, 8)&& TieneSeguroSis() && persona!=null){
    		MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren... ");   		
    	}
		if(esPreguntaInicial) {
			if (Util.esVacio(c2seccion_04a.qi433)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI433"); 
				view = rgQI433; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(c2seccion_04a.qi433,1)) {
				if (Util.esVacio(c2seccion_04a.qi433au)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI433AU"); 
					view = rgQI433AU; 
					error = true; 
					return false; 
				} 
			}
			
			if (!Util.esDiferente(c2seccion_04a.qi433au,1)) {
				if (Util.esVacio(c2seccion_04a.qi433an)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.433AC1"); 
					view = txtQI433AC1; 
					error = true; 
					return false; 
				}		
				if ( Util.esMenor(c2seccion_04a.qi433an,0) || Util.esMayor(c2seccion_04a.qi433an,40)) {
					mensaje = "Valores validos en Horas de 0 a 40"; 
					view = txtQI433AC1; 
					error = true; 
					return false; 
				}
			}	
			
			if (!Util.esDiferente(c2seccion_04a.qi433au,2)) {
				if (Util.esVacio(c2seccion_04a.qi433an)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.430BC2"); 
					view = txtQI433AC2; 
					error = true; 
					return false; 
				}	
				if ( Util.esMenor(c2seccion_04a.qi433an,1) || Util.esMayor(c2seccion_04a.qi433an,50)) {
					mensaje = "Valores validos en D�as de 1 a 50"; 
					view = txtQI433AC2; 
					error = true; 
					return false; 
				}
			}
			
			if (!Util.esDiferente(c2seccion_04a.qi433au,3)) {
				if (Util.esVacio(c2seccion_04a.qi433an)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.430BC3"); 
					view = txtQI433AC3; 
					error = true; 
					return false; 
				}	
				if ( Util.esMenor(c2seccion_04a.qi433an,1) || Util.esMayor(c2seccion_04a.qi433an,40)) {
					mensaje = "Valores validos en Semanas de 1 a 40"; 
					view = txtQI433AC3; 
					error = true; 
					return false; 
				}
			}
		
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado);
		if(App.getInstance().getNacimiento().qi219!=null && App.getInstance().getNacimiento().qi216!=null && App.getInstance().getNacimiento().qi216==1){
			persona = getCuestionarioService().getSeccion01(App.getInstance().getNacimiento().id,App.getInstance().getNacimiento().hogar_id,App.getInstance().getNacimiento().qi219,seccionesCargadoHS01);
		}
		
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430c!=null) {
				c2seccion_04a.qi430c=c2seccion_04a.setConvertQi430c(c2seccion_04a.qi430c);
			}
			
//			if (c2seccion_04a.qi430d!=null) {
//				c2seccion_04a.qi430d=c2seccion_04a.setConvertQi430d(c2seccion_04a.qi430d);
//			}
			
			if (c2seccion_04a.qi431a!=null) {
				c2seccion_04a.qi431a=c2seccion_04a.setConvertQi431a(c2seccion_04a.qi431a);
			}
			
			if (c2seccion_04a.qi432!=null) {
				c2seccion_04a.qi432=c2seccion_04a.setConvertQi432(c2seccion_04a.qi432);
			}
			
			if (c2seccion_04a.qi433!=null) {
				c2seccion_04a.qi433=c2seccion_04a.setConvertQi433(c2seccion_04a.qi433);
			}
			
			if (c2seccion_04a.qi433au!=null) {
				c2seccion_04a.qi433au=c2seccion_04a.setConvertQi433a(c2seccion_04a.qi433au);
			}
		}
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		
		if (c2seccion_04a.qi433an!=null) {	
    		if (!Util.esDiferente(c2seccion_04a.qi433au,1)) {
    			txtQI433AC1.setText(c2seccion_04a.qi433an.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi433au,2)) {
    			txtQI433AC2.setText(c2seccion_04a.qi433an.toString());
    		}  
    		if (!Util.esDiferente(c2seccion_04a.qi433au,3)) {
    			txtQI433AC3.setText(c2seccion_04a.qi433an.toString());
    		} 
    	}
		inicio(); 
    } 
    private void inicio() { 
    	onqrgQI430CChangeValue();
    	onQI432ChangeValue();
    	onqrgQI433ChangeValue();
    	onqrgQI433AChangeValue();
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public boolean NosabeSitieneSeguro(){
    	if(persona!=null && !Util.esDiferente(persona.qh11_y, 1))
    		return true;
    	else
    		return false;
    }
    public boolean NotieneSeguro(){
    	if(persona!=null && !Util.esDiferente(persona.qh11_z, 1))
    		return true;
    	else
    		return false;
    }
    public boolean TieneSeguroSis(){
    		if(persona!=null && 
    	   !Util.esDiferente(persona.qh11_c, 1))
    		return true;
    	else
    		return false;
    }
    public void onQI432ChangeValue(){
    	Integer valor=Integer.parseInt(rgQI432.getTagSelected("0").toString());
//    	Log.e("VALOR: ","SSS: "+valor);
//    	Log.e("TieneSeguroSis: ","TieneSeguroSis: "+TieneSeguroSis());
    	if(!Util.esDiferente(valor, 1) && (NosabeSitieneSeguro()|| NotieneSeguro()) && persona!=null){
    	MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren.\nC.H. respondi� que no tiene seguro o no sabe ");
    	}
    	if(!Util.esDiferente(valor, 2) && TieneSeguroSis() && persona!=null){
    		MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren.\nC.H. respondi� que est� afiliado");   		
    	}
    	if(!Util.esDiferente(valor, 3) && TieneSeguroSis() && persona!=null){
    		MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren.\nC.H. respondi� que est� afiliado");   		
    	}
    	if(!Util.esDiferente(valor, 1) && !TieneSeguroSis() && persona!=null){
    		MyUtil.MensajeGeneral(this.getActivity(),"Verifique C.H. las respuestas  difieren.\nC.H. respondi� que no est� afiliado");   		
    	}
    }

    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		Util.cleanAndLockView(getActivity(),rgQI431A,rgQI433,rgQI433AU,txtQI433AC1,txtQI433AC2,txtQI433AC3,txtQI433A_O);
    		q3.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    	else {
    		if(App.getInstance().getSeccion04A().qi426a!=null && App.getInstance().getSeccion04A().qi426a==1 || App.getInstance().getSeccion04A().qi426a==11 || App.getInstance().getSeccion04A().qi426a==14) {
    			Util.cleanAndLockView(getActivity(),rgQI431A);
        		q3.setVisibility(View.GONE);
    		}
    		else {
    			Util.lockView(getActivity(),false,rgQI431A);
        		q3.setVisibility(View.VISIBLE);	
    		}
    	}
    }
    

	
	public void onqrgQI430CChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI430C.getTagSelected("").toString())) {			
			Util.lockView(getActivity(),false,rgQI430D);
			q2.setVisibility(View.VISIBLE);
			rgQI430D.requestFocus();
			onqrgQI430DNCChangeValue();
		} 
		else {
			Util.cleanAndLockView(getActivity(),rgQI430D);
			q2.setVisibility(View.GONE);	
			MyUtil.LiberarMemoria();
		}
	}
	public void onqrgQI430DNCChangeValue() {
		if (MyUtil.incluyeRango(1,2,rgQI430D.getTagSelected("").toString())) {			
			Util.lockView(getActivity(),false,txtQI430DN);
			txtQI430DN.requestFocus();
		} 
		else {
			Util.cleanAndLockView(getActivity(),txtQI430DN);
		}
	}
	
	public void onqrgQI433ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI433.getTagSelected("").toString())) {			
			Util.lockView(getActivity(),false,rgQI433AU,txtQI433AC1,txtQI433AC2,txtQI433AC3,txtQI433A_O);
			q6.setVisibility(View.VISIBLE);
			App.getInstance().getSeccion04A().qi433 = 1;
			rgQI433AU.requestFocus();
		} 
		else {
			Util.cleanAndLockView(getActivity(),rgQI433AU,txtQI433AC1,txtQI433AC2,txtQI433AC3,txtQI433A_O);
			q6.setVisibility(View.GONE);
			if(rgQI433.getValue()!=null) 
				App.getInstance().getSeccion04A().qi433 = Integer.parseInt(rgQI433.getValue().toString());
			MyUtil.LiberarMemoria();
		}
	}
	
	public void onqrgQI433AChangeValue() {		
		if (MyUtil.incluyeRango(1,1,rgQI433AU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI433AC2,txtQI433AC3);
			c2seccion_04a.qi433an=null;
			Util.lockView(getActivity(),false,txtQI433AC1);				
			txtQI433AC1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI433AU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI433AC1,txtQI433AC3);	
			c2seccion_04a.qi433an=null;
			Util.lockView(getActivity(),false,txtQI433AC2);			
			txtQI433AC2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI433AU.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI433AC1,txtQI433AC2);	
			c2seccion_04a.qi433an=null;
			Util.lockView(getActivity(),false,txtQI433AC3);			
			txtQI433AC3.requestFocus();				
		}
		else if (MyUtil.incluyeRango(4,4,rgQI433AU.getTagSelected("").toString())) {
			c2seccion_04a.qi433an=null;		
			Util.cleanAndLockView(getActivity(),txtQI433AC1,txtQI433AC2,txtQI433AC3);		
		}		
	}

	
	public void renombrarEtiquetas() {
		lblpregunta433a.setText("(NOMBRE)");
    	lblpregunta430c.setText(lblpregunta430c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta431a.setText(lblpregunta431a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta432.setText(lblpregunta432.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta433.setText(lblpregunta433.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta433a.setText(lblpregunta433a.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    	Spanned texto433a =Html.fromHtml("433A. �A las cu�ntas <b>horas</b>, d�as o semanas despu�s de que "+lblpregunta433a.getText() +" naci� tuvo su primer chequeo o revisi�n m�dica?");
    	lblpregunta433a.setText(texto433a);
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI430C.readOnly();
			rgQI430D.readOnly();
			rgQI431A.readOnly();
			rgQI432.readOnly();
			rgQI433.readOnly();
			rgQI433AU.readOnly();
			txtQI430DN.readOnly();
			txtQI433AC1.readOnly();
			txtQI433AC2.readOnly();
			txtQI433AC3.readOnly();
			txtQI433A_O.setEnabled(false);
		}
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi430c!=null) {
				c2seccion_04a.qi430c=c2seccion_04a.getConvertQi430c(c2seccion_04a.qi430c);
			}
			
//			if (c2seccion_04a.qi430d!=null) {
//				c2seccion_04a.qi430d=c2seccion_04a.getConvertQi430d(c2seccion_04a.qi430d);
//			}
			
			if (c2seccion_04a.qi431a!=null) {
				c2seccion_04a.qi431a=c2seccion_04a.getConvertQi431a(c2seccion_04a.qi431a);
			}
			
			if (c2seccion_04a.qi432!=null) {
				c2seccion_04a.qi432=c2seccion_04a.getConvertQi432(c2seccion_04a.qi432);
			}
			
			if (c2seccion_04a.qi433!=null) {
				c2seccion_04a.qi433=c2seccion_04a.getConvertQi433(c2seccion_04a.qi433);
			}
			
			if (c2seccion_04a.qi433au!=null) {
				c2seccion_04a.qi433au=c2seccion_04a.getConvertQi433a(c2seccion_04a.qi433au);
			}
		}
		
		if (c2seccion_04a.qi433au!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi433au,1) && txtQI433AC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi433au,2) && txtQI433AC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi433au,3) && txtQI433AC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi433an=Integer.parseInt(txtQI433AC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi433an=null;
			}
		}	
		
		if (c2seccion_04a.qi433au==null)
			c2seccion_04a.qi433an=null;
		
		App.getInstance().getSeccion04A().qi433 = c2seccion_04a.qi433;
		
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi433!=1) {
					//SeccionCapitulo[] seccion =  CISECCION_04AFragment_003.seccionesGrabado ;
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_011.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_011.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
