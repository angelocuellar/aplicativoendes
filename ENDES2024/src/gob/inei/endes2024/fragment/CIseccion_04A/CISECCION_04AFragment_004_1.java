package gob.inei.endes2024.fragment.CIseccion_04A; 
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

public class CISECCION_04AFragment_004_1 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI412; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI412A; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI413; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI414; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI415; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI417; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQI418; 
		
	public CheckBoxField chb415,chb418;
	
	CISECCION_04A c2seccion_04a; 
	CISECCION_02 ciseccion_02;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta417,lblpregunta418,lblpregunta412,lblpregunta412a,lblpregunta413,lblpregunta414,lblpregunta414_ind,lblpregunta415,lblembarazocontroles415,lblvacuna418; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI412","QI412A","QI413","QI414","QI415","QI417","QI418")}; 
	SeccionCapitulo[] seccionesCargado,seccionesPersonas; 
	
	public ButtonComponent btnNoSabeVacunaMes,btnNoSabeVacunaAnio,btnNoSabeEmbarazo415,btnNoSabe418;
	public GridComponent2 gdVacuna,gdEmbarazo415,gdVacuna418;
	public TextField txtCabecera;
	String nombre_persona;
	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_004_1() {} 
	public CISECCION_04AFragment_004_1 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQI415, 1, 7); 
		rango(getActivity(), txtQI418, 1, 7);
		enlazarCajas();    
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI412","QI412A","QI413","QI414","QI415","QI417","QI418","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215D","QI215M","QI215Y","QI220A","ID","HOGAR_ID","PERSONA_ID") }; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar();
		  
	    rgQI412=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi412_1,R.string.c2seccion_04aqi412_2,R.string.c2seccion_04aqi412_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI412ChangeValue");    
		rgQI412A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi412a_1,R.string.c2seccion_04aqi412a_2,R.string.c2seccion_04aqi412a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI413=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi413_1,R.string.c2seccion_04aqi413_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI414=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi414_1,R.string.c2seccion_04aqi414_2,R.string.c2seccion_04aqi414_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI414ChangeValue"); 
		txtQI415=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1).callback("ontxtQI415ChangeValue"); 
		
//		txtQI415.addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				//dd TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//								
//				if(s.toString().length()>0)
//				{    	
//					ontxtQI415ChangeValue();
//				}
//			}
//		});
		
		rgQI417=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi417_1,R.string.c2seccion_04aqi417_2,R.string.c2seccion_04aqi417_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI417ChangeValue"); 
		lblpregunta412 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi412);
		lblpregunta412a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi412a);
		lblpregunta413 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi413);
		lblpregunta414 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi414);
		lblpregunta414_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi414_ind);
		lblpregunta415 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi415);
		
		lblpregunta417  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi417).textSize(19);
		lblpregunta418  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04aqi418).textSize(19);
		lblembarazocontroles415  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi415_1).textSize(16).centrar();
		chb415=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi415_2, "1:0").size(altoComponente, 200);
		chb415.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb415.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI415);
				Util.lockView(getActivity(),false, rgQI417,txtQI418,chb418);
				q6.setVisibility(View.VISIBLE);
				q7.setVisibility(View.VISIBLE);
				onqrgQI417ChangeValue();
				
			}
			else {
				if (Util.esMayor(c2seccion_04a.qi415,2)) {
					Util.lockView(getActivity(),false, txtQI415);
					Util.cleanAndLockView(getActivity(),rgQI417,txtQI418,chb418);
					q6.setVisibility(View.GONE);
					q7.setVisibility(View.GONE);
				}
				else{
					Util.lockView(getActivity(),false, txtQI415);
					Util.lockView(getActivity(),false, rgQI417,txtQI418,chb418);
					q6.setVisibility(View.VISIBLE);
					q7.setVisibility(View.VISIBLE);
					onqrgQI417ChangeValue();
				}
			}
		}
		});
		
		gdEmbarazo415 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo415.addComponent(lblembarazocontroles415);
		gdEmbarazo415.addComponent(txtQI415);
		gdEmbarazo415.addComponent(chb415);
				
		txtQI418=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1);
		
		lblvacuna418  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi418_1).textSize(16).centrar();
		chb418=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi418_2, "1:0").size(altoComponente, 200);
		chb418.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb418.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI418);
			}
			else {
				Util.lockView(getActivity(),false, txtQI418);	
			}
		}
		});
				
		gdVacuna418 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna418.addComponent(lblvacuna418);
		gdVacuna418.addComponent(txtQI418);
		gdVacuna418.addComponent(chb418);
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 		
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta412,rgQI412); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta412a,rgQI412A); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta413,rgQI413); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta414,lblpregunta414_ind,rgQI414); 
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta415,gdEmbarazo415.component()); 
    	q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta417,rgQI417); 
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta418,gdVacuna418.component());

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		if(c2seccion_04a!=null){
			if (c2seccion_04a.qi412!=null) {
				c2seccion_04a.qi412=c2seccion_04a.getConvertQi412(c2seccion_04a.qi412);
			}	
			if (c2seccion_04a.qi412a!=null) {
				c2seccion_04a.qi412a=c2seccion_04a.getConvertQi412a(c2seccion_04a.qi412a);
			}
			if (c2seccion_04a.qi414!=null) {
				c2seccion_04a.qi414=c2seccion_04a.getConvertQi414(c2seccion_04a.qi414);
			}
			if (c2seccion_04a.qi417!=null) {
				c2seccion_04a.qi417=c2seccion_04a.getConvertQi417(c2seccion_04a.qi417);
			}
		}
		
		if(chb418.isChecked() ) {
			c2seccion_04a.qi418=8;			
		}
		
		if(chb415.isChecked() ) {
			c2seccion_04a.qi415=8;			
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(esPreguntaInicial) {
			
			if(!(App.getInstance().getSeccion04A().qi407_y!=null && App.getInstance().getSeccion04A().qi407_y==1)) {
			
				if (Util.esVacio(c2seccion_04a.qi412)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI412"); 
					view = rgQI412; 
					error = true; 
					return false; 
				} 
				
				if(c2seccion_04a.qi412!=null && c2seccion_04a.qi412==1) {
					if (Util.esVacio(c2seccion_04a.qi412a)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI412A"); 
						view = rgQI412A; 
						error = true; 
						return false; 
					} 			
				}
			}
			
			if (Util.esVacio(c2seccion_04a.qi413)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI413"); 
				view = rgQI413; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi414)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI414"); 
				view = rgQI414; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04a.qi414!=null && c2seccion_04a.qi414==1) {
				if (Util.esVacio(c2seccion_04a.qi415) && !chb415.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI415"); 
					view = txtQI415; 
					error = true; 
					return false; 
				} 
			}
			
			if(c2seccion_04a.qi415!=null && (c2seccion_04a.qi415==1 || c2seccion_04a.qi415==8)){
				if (Util.esVacio(c2seccion_04a.qi417)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI417"); 
					view = rgQI417; 
					error = true; 
					return false; 
				} 
			
				if(c2seccion_04a.qi417!=null && c2seccion_04a.qi417==1) {
					if (Util.esVacio(c2seccion_04a.qi418) && !chb418.isChecked()) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI418"); 
						view = txtQI418; 
						error = true; 
						return false; 
					} 
				}
			}
			if(c2seccion_04a.qi415==null) {
				if (Util.esVacio(c2seccion_04a.qi417)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI417"); 
					view = rgQI417; 
					error = true; 
					return false; 
				} 
			
				if(c2seccion_04a.qi417!=null && c2seccion_04a.qi417==1) {
					if (Util.esVacio(c2seccion_04a.qi418) && !chb418.isChecked()) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI418"); 
						view = txtQI418; 
						error = true; 
						return false; 
					} 
				}
			}
		}
		return true; 
    } 
    public Calendar fechadeNinio(){
    	Calendar fecha = new GregorianCalendar(ciseccion_02.qi215y,Integer.parseInt(ciseccion_02.qi215m.toString())-(ciseccion_02.qi220a+1),Integer.parseInt(ciseccion_02.qi215d.toString()));
    	return fecha;
    }
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212; 
	    } 		
		
		ciseccion_02 = getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesPersonas);
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		if (c2seccion_04a!=null && c2seccion_04a.qi412!=null) {
			c2seccion_04a.qi412=c2seccion_04a.setConvertQi412(c2seccion_04a.qi412);
		}			
		if (c2seccion_04a!=null && c2seccion_04a.qi412a!=null) {
			c2seccion_04a.qi412a=c2seccion_04a.setConvertQi412a(c2seccion_04a.qi412a);
		}			
		if (c2seccion_04a!=null && c2seccion_04a.qi414!=null) {
			c2seccion_04a.qi414=c2seccion_04a.setConvertQi414(c2seccion_04a.qi414);
		}			
		if (c2seccion_04a!=null && c2seccion_04a.qi417!=null) {
			c2seccion_04a.qi417=c2seccion_04a.setConvertQi417(c2seccion_04a.qi417);
		}			
				
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		
		if(c2seccion_04a.qi418!=null && c2seccion_04a.qi418==8) {
			chb418.setChecked(true);
			txtQI418.setText("");
		}
				
		if(c2seccion_04a.qi415!=null && c2seccion_04a.qi415==8) {
			chb415.setChecked(true);
			txtQI415.setText("");
		}
		inicio(); 
    } 
    
    private void inicio() { 
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){
    		Log.e("111111","1");
    		Util.cleanAndLockView(getActivity(),rgQI412,rgQI412A,rgQI413,rgQI414,txtQI415,chb415,rgQI417,txtQI418,chb418);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    	}
    	
    	else {
    		if(App.getInstance().getSeccion04A().qi407_y!=null && App.getInstance().getSeccion04A().qi407_y==1) {
    			Util.cleanAndLockView(getActivity(), rgQI412,rgQI412A);
    			q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			Log.e("111111","3");
    		}
    		else {
    			Log.e("111111","4");
    			Util.lockView(getActivity(), false,rgQI412,rgQI412A);
    			q1.setVisibility(View.VISIBLE);
    			q2.setVisibility(View.VISIBLE);
    			if (c2seccion_04a!=null && c2seccion_04a.qi412!=null) {
    				Log.e("111111","5");
    		    	onqrgQI412ChangeValue();
    			}
    		}
    		onqrgQI414ChangeValue();
    		if (c2seccion_04a.qi415!=null && c2seccion_04a.qi415==8) { Util.cleanAndLockView(getActivity(),txtQI415); } else{ Util.lockView(getActivity(),false,txtQI415);}
    		if (c2seccion_04a.qi418!=null && c2seccion_04a.qi418==8) { Util.cleanAndLockView(getActivity(),txtQI418); } else{ Util.lockView(getActivity(),false,txtQI418);}
    	}
    }
	
    public void onqrgQI412ChangeValue() {
		if (MyUtil.incluyeRango(2,8,rgQI412.getTagSelected("").toString())) {
			Log.e("111111","12");
			Util.cleanAndLockView(getActivity(),rgQI412A);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			rgQI413.requestFocus();
		} else {
			Log.e("111111","13");
			Util.lockView(getActivity(), false,rgQI412A);
			q2.setVisibility(View.VISIBLE);
		}
    }
    
    public void onqrgQI414ChangeValue() {
  		if (MyUtil.incluyeRango(2,8,rgQI414.getTagSelected("").toString())) {
//  			Log.e("111111","14");
  			Util.cleanAndLockView(getActivity(),txtQI415,chb415);
  			Util.lockView(getActivity(), false,rgQI417,txtQI418,chb418);
  			MyUtil.LiberarMemoria();
  			q5.setVisibility(View.GONE);
  			q6.setVisibility(View.VISIBLE);
  			onqrgQI417ChangeValue();
  		} else {
//  			Log.e("111111","15");
  			Util.lockView(getActivity(), false,txtQI415,chb415);
  			q5.setVisibility(View.VISIBLE);
  			ontxtQI415ChangeValue();
  		}
    }
    
    public void ontxtQI415ChangeValue() {
    	Integer valor = Integer.parseInt(rgQI417.getTagSelected("0").toString());
//    	Log.e("", "VALOR: "+valor);
  		if (MyUtil.incluyeRango(2,10,txtQI415.getText().toString()) && !chb415.isChecked()) {
//  			Log.e("111111","16");
  			//se puso debido a que no limpia las variables al grabar
  			Util.cleanAndLockView(getActivity(),rgQI417,txtQI418,chb418);
  			MyUtil.LiberarMemoria();
  			q6.setVisibility(View.GONE);
  			q7.setVisibility(View.GONE);
  			Integer qi415 =txtQI415.getValue()!=null?Integer.parseInt(txtQI415.getValue().toString()):0;
    		if(qi415>2 && qi415<7 && !chb415.isChecked()) {
    			ToastMessage.msgBox(this.getActivity(), "Verifique la cantidad de dosis", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
    		}
  		} 
  		else {
//  			Log.e("111111","17");
  			MyUtil.LiberarMemoria();
  			Util.lockView(getActivity(), false,rgQI417,txtQI418,chb418);
  			q6.setVisibility(View.VISIBLE);
  			q7.setVisibility(View.VISIBLE);
  			onqrgQI417ChangeValue();
  		}
    }
    
    public void onqrgQI417ChangeValue() {
    	Integer valor = Integer.parseInt(rgQI417.getTagSelected("0").toString());
  		if (MyUtil.incluyeRango(2,8,rgQI417.getTagSelected("").toString())) {
//  		Log.e("111111","18");
  			Util.cleanAndLockView(getActivity(),txtQI418,chb418);
  			MyUtil.LiberarMemoria();
  			q7.setVisibility(View.GONE);
  		}
  		if (MyUtil.incluyeRango(1,1,rgQI417.getTagSelected("").toString())) {
//  			Log.e("111111","19");
  			Util.lockView(getActivity(), false,txtQI418,chb418);
  			q7.setVisibility(View.VISIBLE);
  		}
    }
	
	public void renombrarEtiquetas() {	
    	lblpregunta413.setText(lblpregunta413.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta414.setText(lblpregunta414.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta415.setText(lblpregunta415.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta417.setText(lblpregunta417.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta418.setText(lblpregunta418.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void notificacion(String msg) {
		ToastMessage.msgBox(this.getActivity(), msg, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	} 
	
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI412.readOnly();
			rgQI412A.readOnly();
			rgQI413.readOnly();
			rgQI414.readOnly();
			rgQI417.readOnly();
			txtQI415.readOnly();
			txtQI418.readOnly();
			chb415.readOnly();
			chb418.readOnly();
		}
	}
	
	
	public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		if(c2seccion_04a!=null){
			if (c2seccion_04a.qi412!=null) {
				c2seccion_04a.qi412=c2seccion_04a.getConvertQi412(c2seccion_04a.qi412);
			}	
			if (c2seccion_04a.qi412a!=null) {
				c2seccion_04a.qi412a=c2seccion_04a.getConvertQi412a(c2seccion_04a.qi412a);
			}
			if (c2seccion_04a.qi414!=null) {
				c2seccion_04a.qi414=c2seccion_04a.getConvertQi414(c2seccion_04a.qi414);
			}
			if (c2seccion_04a.qi417!=null) {
				c2seccion_04a.qi417=c2seccion_04a.getConvertQi417(c2seccion_04a.qi417);
			}
		}
		
		if(chb418.isChecked() ) {
			c2seccion_04a.qi418=8;			
		}
		
		if(chb415.isChecked() ) {
			c2seccion_04a.qi415=8;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
