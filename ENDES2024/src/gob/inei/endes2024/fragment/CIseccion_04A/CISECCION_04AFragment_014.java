package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_014 extends FragmentForm { 
	
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI441;	
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI442_NOM; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI442; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQI442X; 
	@FieldAnnotation(orderIndex=5) 
	public TextAreaField txtQIOBS_4A;
	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta441,lblpregunta442,lblpregunta442_Pu_Ind,lblpregunta442_Pri_Ind,lblpregunta442_Org_Ind,lbllugar,lblpregunta442_ind,lblobservacion; 
	GridComponent2 gdLugarSalud;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String nombre_persona;
	String observacion;
	public TextField txtCabecera;
	
	public CISECCION_04AFragment_014() {} 
	public CISECCION_04AFragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
							
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI441","QI442_NOM","QI442","QI442X","QIOBS_4A","QI411_K","QI411_L","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI441","QI442_NOM","QI442","QI442X","QIOBS_4A")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta441 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi441);
		lblpregunta442 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi442);
		lblpregunta442_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi442_ind);
		lblobservacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqiobservacion);
		
		rgQI441=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi441_1,R.string.c2seccion_04aqi441_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI441ChangeValue"); 
				
		Spanned texto442Pu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta442_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta442_Pu_Ind.setText(texto442Pu);
		
		Spanned texto442Pri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta442_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta442_Pri_Ind.setText(texto442Pri);
		Spanned texto442Org =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta442_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta442_Org_Ind.setText(texto442Org);
			
		txtQI442_NOM=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico();				
		rgQI442=new RadioGroupOtherField(this.getActivity()).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI442.addOption(1,getResources().getText(R.string.c2seccion_04aqi442_1).toString());
	
		rgQI442.addOption(2,getResources().getText(R.string.c2seccion_04aqi442_2).toString());
		rgQI442.addOption(3,getResources().getText(R.string.c2seccion_04aqi442_3).toString());
		rgQI442.addOption(4,getResources().getText(R.string.c2seccion_04aqi442_4).toString());
		rgQI442.addOption(5,getResources().getText(R.string.c2seccion_04aqi442_5).toString());
		rgQI442.addOption(6,getResources().getText(R.string.c2seccion_04aqi442_6).toString());
		rgQI442.addOption(7,getResources().getText(R.string.c2seccion_04aqi442_7).toString());
		rgQI442.addOption(8,getResources().getText(R.string.c2seccion_04aqi442_8).toString());
		
		rgQI442.addOption(9,getResources().getText(R.string.c2seccion_04aqi442_9).toString());
		rgQI442.addOption(10,getResources().getText(R.string.c2seccion_04aqi442_10).toString());
		rgQI442.addOption(11,getResources().getText(R.string.c2seccion_04aqi442_11).toString());
		
		rgQI442.addOption(12,getResources().getText(R.string.c2seccion_04aqi442_12).toString());
		rgQI442.addOption(13,getResources().getText(R.string.c2seccion_04aqi442_13).toString());
		txtQI442X=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI442.agregarEspecifique(12,txtQI442X); 
			
		
		rgQI442.addView(lblpregunta442_Pu_Ind,0);
		rgQI442.addView(lblpregunta442_Pri_Ind,8);
		rgQI442.addView(lblpregunta442_Org_Ind,12);
		txtQIOBS_4A= new TextAreaField(getActivity()).size(150, 700).maxLength(1000).alfanumerico();
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04aqi442_nom).textSize(16);
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI442_NOM);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
	    q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta441,rgQI441); 				
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta442,lblpregunta442_ind,gdLugarSalud.component(),rgQI442);
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblobservacion,txtQIOBS_4A);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
    	uiToEntity(c2seccion_04a); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try {
			if(App.getInstance().getNacimiento().qi216==1){
				c2seccion_04a.qiobs_4a=observacion;
			}
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
					if( App.getInstance().getNacimiento().qi216==2) {
						List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
						seccs.add(CISECCION_04AFragment_015.seccionesGrabado);
						SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
						getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
					}
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
			
	
		if (Util.esVacio(c2seccion_04a.qi441)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI441"); 
			view = rgQI441; 
			error = true; 
			return false; 
		} 
// 	DE COMENTO A SOLICITUDAD DE CORDINADORAS CURSO DE CAPACITACION 2017
//		if (c2seccion_04a!=null && c2seccion_04a.qi411_l!=null && c2seccion_04a.qi411_k!=null && c2seccion_04a.qi441!=null) {
//			if (c2seccion_04a.qi411_l==1 && c2seccion_04a.qi411_k==1 && c2seccion_04a.qi441==2){
//				Mensaje("Verifique pregunta 441 debe tener relaci�n con las preguntas 411_K, QI411_L");
//			}
//			if (c2seccion_04a.qi411_l==2 && c2seccion_04a.qi411_k==2 && c2seccion_04a.qi441==1){
//				Mensaje("Verifique pregunta 441 debe tener relaci�n con las preguntas 411_K, QI411_L");
//			}
//		}
		
		
		
		
		if (!Util.esDiferente(c2seccion_04a.qi441,1)) {		
			if (Util.esVacio(c2seccion_04a.qi442)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI442"); 
				view = rgQI442; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(c2seccion_04a.qi442,13)) {	
				if (Util.esVacio(c2seccion_04a.qi442x)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI442X"); 
					view = txtQI442X; 
					error = true; 
					return false; 
				} 
			}	
		}
	
		return true; 
    } 
    
    public void Mensaje(String mensaje){
    	ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		observacion = c2seccion_04a.qiobs_4a;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		inicio(); 
    } 
    private void inicio() { 
    	onqrgQI441ChangeValue();
    	mostrarobservacionsiesqueestamuerto();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
	public void mostrarobservacionsiesqueestamuerto(){
		if(!Util.esDiferente(App.getInstance().getNacimiento().qi216,1)){
			Util.cleanAndLockView(getActivity(), txtQIOBS_4A);
			q3.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,txtQIOBS_4A);
			q3.setVisibility(View.VISIBLE);
		}
	}
	
	public void onqrgQI441ChangeValue() {
		if(rgQI441.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI441.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,txtQI442_NOM,rgQI442);
				q2.setVisibility(View.VISIBLE);
				txtQI442_NOM.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),txtQI442_NOM,rgQI442);
				q2.setVisibility(View.GONE);
				MyUtil.LiberarMemoria();
			}
		}
	}
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta441.setText(lblpregunta441.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI441.readOnly();
			rgQI442.readOnly();
			txtQI442_NOM.readOnly();
			txtQI442X.readOnly();
			txtQIOBS_4A.setEnabled(false);
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
					if( App.getInstance().getNacimiento().qi216==2) {
						//SeccionCapitulo[] seccion =  CISECCION_04AFragment_003.seccionesGrabado ;
						List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
						seccs.add(CISECCION_04AFragment_015.seccionesGrabado);
						SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
						getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
					}
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
	
	
} 
