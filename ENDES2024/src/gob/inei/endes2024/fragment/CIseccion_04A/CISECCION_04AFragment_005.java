package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI426_A; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI426_B; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI426_C; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI426_D; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI426_E; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI426_F; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI426_G; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI426_X; 
	@FieldAnnotation(orderIndex=9) 
	public TextField txtQI426_X_O; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI426_Y; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI426A_NOM; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI426A; 
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQI426A_O; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI426B; 
	@FieldAnnotation(orderIndex=15) 
	public TextField txtQI426BX; 
	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta426,lblpregunta426b,lblpregunta426a,lblpregunta426a_ind,lblpregunta426a_Pu_Ind,lblpregunta426_ind,
	lblpregunta426a_Pri_Ind,lblpregunta426a_Org_Ind,lbllugar; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 

	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	GridComponent2 gdLugarSalud;
	public TextField txtCabecera;
	public boolean esPrimerRegistro;	
	String nombre_persona;
	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_005() {} 
	public CISECCION_04AFragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI426_A","QI426_B","QI426_C","QI426_D","QI426_E","QI426_F","QI426_G","QI426_X","QI426_X_O","QI426_Y","QI426A_NOM","QI426A","QI426A_O","QI426B","QI426BX","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI426_A","QI426_B","QI426_C","QI426_D","QI426_E","QI426_F","QI426_G","QI426_X","QI426_X_O","QI426_Y","QI426A_NOM","QI426A","QI426A_O","QI426B","QI426BX")}; 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		txtQI426A_NOM=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).alfanumerico();
		
		chbQI426_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426_aChangeValue"); //.callback("onqrgQS807aChangeValue");
		chbQI426_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426bChangeValue");
		chbQI426_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426cChangeValue");
		chbQI426_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426dChangeValue");
		chbQI426_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426eChangeValue");
		chbQI426_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426fChangeValue");
		chbQI426_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426gChangeValue");
		chbQI426_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426xChangeValue");
		chbQI426_Y=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi426_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI426yChangeValue");
		txtQI426_X_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		
		lblpregunta426 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426);
		lblpregunta426_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi426_ind);
		lblpregunta426a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426a);
		lblpregunta426a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi426a_ind);
		lblpregunta426b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426b);
		
				
		Spanned texto426aPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta426a_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta426a_Pu_Ind.setText(texto426aPu);
		
		Spanned texto426aPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta426a_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta426a_Pri_Ind.setText(texto426aPri);
		Spanned texto426aOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta426a_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta426a_Org_Ind.setText(texto426aOrg);
			
		txtQI426A_NOM=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100);				
		rgQI426A=new RadioGroupOtherField(this.getActivity()).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI426aChangeValue");
		rgQI426A.addOption(1,getResources().getText(R.string.c2seccion_04aqi426a_1).toString());
	
		rgQI426A.addOption(2,getResources().getText(R.string.c2seccion_04aqi426a_2).toString());
		rgQI426A.addOption(3,getResources().getText(R.string.c2seccion_04aqi426a_3).toString());
		rgQI426A.addOption(4,getResources().getText(R.string.c2seccion_04aqi426a_4).toString());
		rgQI426A.addOption(5,getResources().getText(R.string.c2seccion_04aqi426a_5).toString());
		rgQI426A.addOption(6,getResources().getText(R.string.c2seccion_04aqi426a_6).toString());
		rgQI426A.addOption(7,getResources().getText(R.string.c2seccion_04aqi426a_7).toString());
		rgQI426A.addOption(8,getResources().getText(R.string.c2seccion_04aqi426a_8).toString());
		
		rgQI426A.addOption(9,getResources().getText(R.string.c2seccion_04aqi426a_9).toString());
		rgQI426A.addOption(10,getResources().getText(R.string.c2seccion_04aqi426a_10).toString());
		rgQI426A.addOption(11,getResources().getText(R.string.c2seccion_04aqi426a_11).toString());
		
		rgQI426A.addOption(12,getResources().getText(R.string.c2seccion_04aqi426a_12).toString());
		rgQI426A.addOption(13,getResources().getText(R.string.c2seccion_04aqi426a_13).toString());
		rgQI426A.addOption(14,getResources().getText(R.string.c2seccion_04aqi426a_14).toString());
		txtQI426A_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI426A.agregarEspecifique(13,txtQI426A_O); 
			
		
		rgQI426A.addView(lblpregunta426a_Pu_Ind,1);
		rgQI426A.addView(lblpregunta426a_Pri_Ind,9);
		rgQI426A.addView(lblpregunta426a_Org_Ind,13);
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04aqi426a_nom).textSize(16);
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI426A_NOM);
		
		rgQI426B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426b_1,R.string.c2seccion_04aqi426b_2,R.string.c2seccion_04aqi426b_3,R.string.c2seccion_04aqi426b_4,R.string.c2seccion_04aqi426b_5,R.string.c2seccion_04aqi426b_6,R.string.c2seccion_04aqi426b_7,R.string.c2seccion_04aqi426b_8,R.string.c2seccion_04aqi426b_9,R.string.c2seccion_04aqi426b_10,R.string.c2seccion_04aqi426b_11,R.string.c2seccion_04aqi426b_12,R.string.c2seccion_04aqi426b_13,R.string.c2seccion_04aqi426b_14,R.string.c2seccion_04aqi426b_15,R.string.c2seccion_04aqi426b_16).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI426BX = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQI426B.agregarEspecifique(15, txtQI426BX);
		
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		
		LinearLayout ly426X = new LinearLayout(getActivity());
		ly426X.addView(chbQI426_X);
		ly426X.addView(txtQI426_X_O);
		
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta426,lblpregunta426_ind,chbQI426_A,chbQI426_B,chbQI426_C,chbQI426_D,chbQI426_E,chbQI426_F,chbQI426_G,ly426X,chbQI426_Y);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta426a,lblpregunta426a_ind,gdLugarSalud.component(),rgQI426A);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta426b,rgQI426B); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		
		App.getInstance().getSeccion04A().qi426a = c2seccion_04a.qi426a;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		
		if(!verificarCheck426() && !chbQI426_Y.isChecked()) {
			mensaje = "Seleccione una opci�n en P.426"; 
			view = chbQI426_A; 
			error = true; 
			return false; 
		}
		
		if (chbQI426_X.isChecked()) {
			if (Util.esVacio(c2seccion_04a.qi426_x_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQI426_X_O; 
				error = true; 
				return false; 
			}
		}		
		
		/*
		if (Util.esVacio(c2seccion_04a.qi426a_nom)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI426A_NOM"); 
			view = txtQI426A_NOM; 
			error = true; 
			return false; 
		} */
		if (Util.esVacio(c2seccion_04a.qi426a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI426A"); 
			view = rgQI426A; 
			error = true; 
			return false; 
		} 
		
		if(!Util.esDiferente(c2seccion_04a.qi426a,14)){ 
			if (Util.esVacio(c2seccion_04a.qi426a_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQI426A_O; 
				error = true; 
				return false; 
			} 
		}
		
		if(!(c2seccion_04a.qi426a==2 || c2seccion_04a.qi426a==5 || c2seccion_04a.qi426a==6)){		
			if (Util.esVacio(c2seccion_04a.qi426b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426B"); 
				view = rgQI426B; 
				error = true; 
				return false; 
			} 
	
			if(!Util.esDiferente(c2seccion_04a.qi426b,16)){ 
				if (Util.esVacio(c2seccion_04a.qi426bx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI426BX; 
					error = true; 
					return false; 
				} 
			} 
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212 ,seccionesCargado);
		esPrimerRegistro = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		App.getInstance().getNacimiento().esPrimerRegistro = esPrimerRegistro;
		
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212; 
	    } 
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		inicio();
    } 
    private void inicio() {
    	
    	onqrgQI426xChangeValue();
    	onqrgQI426_aChangeValue();
    	onqrgQI426yChangeValue();
    	
    	if(c2seccion_04a.qi426a!=null)
    	onqrgQI426aChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
 
	
	
	public boolean verificarCheck426() {
  		if (chbQI426_A.isChecked() || chbQI426_B.isChecked() || chbQI426_C.isChecked() || chbQI426_D.isChecked() || chbQI426_E.isChecked() || chbQI426_F.isChecked() || chbQI426_G.isChecked() ||chbQI426_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	
	 public void onqrgQI426_aChangeValue() {  	
	    	if (verificarCheck426()) {
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    public void onqrgQI426bChangeValue() {  	
	    	if (verificarCheck426()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    public void onqrgQI426cChangeValue() {  	
	    	if (verificarCheck426()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    public void onqrgQI426dChangeValue() {  	
	    	if (verificarCheck426()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    public void onqrgQI426eChangeValue() {  	
	    	if (verificarCheck426()) {    		
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    public void onqrgQI426fChangeValue() {  	
	    	if (verificarCheck426()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    
	    public void onqrgQI426gChangeValue() {  	
	    	if (verificarCheck426()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI426_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI426_Y);  				
	    	}	
	    }
	    
	    
	    public void onqrgQI426xChangeValue() {  	
	    	if (chbQI426_X.isChecked()){		
	  			Util.lockView(getActivity(),false,txtQI426_X_O);
	  			if (verificarCheck426()) {  			
	  				Util.cleanAndLockView(getActivity(),chbQI426_Y);
	  			}
	  			txtQI426_X_O.requestFocus();
	  		} 
	  		else {
	  			Util.cleanAndLockView(getActivity(),txtQI426_X_O);  
	  			if (!verificarCheck426()) {  			
	  				Util.lockView(getActivity(),false,chbQI426_Y);  				
				}
	  		}	
	    }
	    public void onqrgQI426yChangeValue() {    	
	  		if (chbQI426_Y.isChecked()){  		
	  			Util.cleanAndLockView(getActivity(),chbQI426_A,chbQI426_B,chbQI426_C,chbQI426_D,chbQI426_E,chbQI426_F,chbQI426_G,chbQI426_X);  	
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,chbQI426_A,chbQI426_B,chbQI426_C,chbQI426_D,chbQI426_E,chbQI426_F,chbQI426_G,chbQI426_X);  					
	  		}	
	  	}
	    
	    public void onqrgQI426aChangeValue() {
			if (rgQI426A.getValue().toString().equals("2") || rgQI426A.getValue().toString().equals("5") || rgQI426A.getValue().toString().equals("6")) {    		
	    		Util.cleanAndLockView(getActivity(),rgQI426B);
	     		MyUtil.LiberarMemoria();
	    		q3.setVisibility(View.GONE);
	    		c2seccion_04a.qi426a = Integer.parseInt(rgQI426A.getValue().toString());
	    		App.getInstance().getSeccion04A().qi426a =c2seccion_04a.qi426a; 
	   
	    	} 
	    	else {	    		
	    		Util.lockView(getActivity(),false,rgQI426B);  
	    		q3.setVisibility(View.VISIBLE);
	    		rgQI426B.requestFocus();
	    		if(rgQI426A.getValue()!=null) {
	    			c2seccion_04a.qi426a = Integer.parseInt(rgQI426A.getValue().toString());
	    			App.getInstance().getSeccion04A().qi426a =c2seccion_04a.qi426a;
	    		}
	    	}	
		}
	
	
	public void renombrarEtiquetas(){	
    	lblpregunta426.setText(lblpregunta426.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta426a.setText(lblpregunta426a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta426b.setText(lblpregunta426b.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI426A.readOnly();
			rgQI426B.readOnly();
			txtQI426_X_O.readOnly();
			txtQI426A_NOM.readOnly();
			txtQI426A_O.readOnly();
			txtQI426BX.readOnly();
			chbQI426_A.readOnly();
			chbQI426_B.readOnly();
			chbQI426_C.readOnly();
			chbQI426_D.readOnly();
			chbQI426_E.readOnly();
			chbQI426_F.readOnly();
			chbQI426_G.readOnly();
			chbQI426_X.readOnly();
			chbQI426_Y.readOnly();
		}
	}
	
	   
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		App.getInstance().getSeccion04A().qi426a = c2seccion_04a.qi426a;
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
