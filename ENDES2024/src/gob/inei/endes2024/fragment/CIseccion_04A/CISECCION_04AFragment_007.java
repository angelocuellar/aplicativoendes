package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_007 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI427B; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI427B_O; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQI427C_NOM; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI427C; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQI427C_O; 
	 	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta427b,lblpregunta427b_ind,lblpregunta427c,lblpregunta427c_ind,lblpregunta427c_nom,lblpregunta427c_Pu_Ind,lblpregunta427c_Pri_Ind,lblpregunta427c_Org_Ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 

	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI427B","QI427B_O","QI427C_NOM","QI427C","QI427C_O")};
	SeccionCapitulo[] seccionesCargado; 

	public boolean esPreguntaInicial;
	public TextField txtCabecera;
	public GridComponent2 gdLugarSalud;
	public CISECCION_04AFragment_007() {} 
	public CISECCION_04AFragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI427B","QI427B_O","QI427C_NOM","QI427C","QI427C_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta427b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427b);
		lblpregunta427b_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi427b_ind);
		lblpregunta427c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427c);
		lblpregunta427c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi427c_ind);
		lblpregunta427c_nom = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04aqi427c_nom).textSize(16);
		
		rgQI427B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427b_1,R.string.c2seccion_04aqi427b_2,R.string.c2seccion_04aqi427b_3,R.string.c2seccion_04aqi427b_4,R.string.c2seccion_04aqi427b_5,R.string.c2seccion_04aqi427b_6,R.string.c2seccion_04aqi427b_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI427B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI427B.agregarEspecifique(6,txtQI427B_O); 
		rgQI427C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427c_1,R.string.c2seccion_04aqi427c_2,R.string.c2seccion_04aqi427c_3,R.string.c2seccion_04aqi427c_4,R.string.c2seccion_04aqi427c_5,R.string.c2seccion_04aqi427c_6,R.string.c2seccion_04aqi427c_7,R.string.c2seccion_04aqi427c_8,R.string.c2seccion_04aqi427c_9,R.string.c2seccion_04aqi427c_10,R.string.c2seccion_04aqi427c_11,R.string.c2seccion_04aqi427c_12,R.string.c2seccion_04aqi427c_13,R.string.c2seccion_04aqi427c_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
				
		Spanned texto427cPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta427c_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta427c_Pu_Ind.setText(texto427cPu);
		
		Spanned texto427cPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta427c_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta427c_Pri_Ind.setText(texto427cPri);
		
		Spanned texto427cOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta427c_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta427c_Org_Ind.setText(texto427cOrg);
			
		txtQI427C_NOM=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico();				
		rgQI427C=new RadioGroupOtherField(this.getActivity()).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI427C.addOption(1,getResources().getText(R.string.c2seccion_04aqi427c_1).toString());
	
		rgQI427C.addOption(2,getResources().getText(R.string.c2seccion_04aqi427c_2).toString());
		rgQI427C.addOption(3,getResources().getText(R.string.c2seccion_04aqi427c_3).toString());
		rgQI427C.addOption(4,getResources().getText(R.string.c2seccion_04aqi427c_4).toString());
		rgQI427C.addOption(5,getResources().getText(R.string.c2seccion_04aqi427c_5).toString());
		rgQI427C.addOption(6,getResources().getText(R.string.c2seccion_04aqi427c_6).toString());
		rgQI427C.addOption(7,getResources().getText(R.string.c2seccion_04aqi427c_7).toString());
		rgQI427C.addOption(8,getResources().getText(R.string.c2seccion_04aqi427c_8).toString());
		
		rgQI427C.addOption(9,getResources().getText(R.string.c2seccion_04aqi427c_9).toString());
		rgQI427C.addOption(10,getResources().getText(R.string.c2seccion_04aqi427c_10).toString());
		rgQI427C.addOption(11,getResources().getText(R.string.c2seccion_04aqi427c_11).toString());
		
		rgQI427C.addOption(12,getResources().getText(R.string.c2seccion_04aqi427c_12).toString());
		rgQI427C.addOption(13,getResources().getText(R.string.c2seccion_04aqi427c_13).toString());
		
		rgQI427C.addOption(14,getResources().getText(R.string.c2seccion_04aqi427c_14).toString());
		txtQI427C_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI427C.agregarEspecifique(13,txtQI427C_O); 
		
		rgQI427C.addView(lblpregunta427c_Pu_Ind,1);
		rgQI427C.addView(lblpregunta427c_Pri_Ind,9);
		rgQI427C.addView(lblpregunta427c_Org_Ind,13);
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lblpregunta427c_nom);
		gdLugarSalud.addComponent(txtQI427C_NOM);
				
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta427b,lblpregunta427b_ind,rgQI427B); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta427c,lblpregunta427c_ind,gdLugarSalud.component(),rgQI427C); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(esPreguntaInicial) {
		
			if (Util.esVacio(c2seccion_04a.qi427b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427B"); 
				view = rgQI427B; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04a.qi427b,7)){ 
				if (Util.esVacio(c2seccion_04a.qi427b_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI427B_O; 
					error = true; 
					return false; 
				} 
			} 
			/*if (Util.esVacio(c2seccion_04a.qi427c_nom)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427C_NOM"); 
				view = txtQI427C_NOM; 
				error = true; 
				return false; 
			} */
			if (Util.esVacio(c2seccion_04a.qi427c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI427C"); 
				view = rgQI427C; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04a.qi427c,14)){ 
				if (Util.esVacio(c2seccion_04a.qi427c_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI427C_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		entityToUI(c2seccion_04a); 
		inicio(); 
    } 
    private void inicio() { 
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		Util.cleanAndLockView(getActivity(),rgQI427B,txtQI427B_O,rgQI427C,txtQI427C_NOM,txtQI427C_O);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI427B.readOnly();
    		rgQI427C.readOnly();
    		txtQI427B_O.readOnly();
    		txtQI427C_NOM.readOnly();
    		txtQI427C_O.readOnly();
    	}
    }
    
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
