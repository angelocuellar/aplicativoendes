package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_015 extends FragmentForm { 
		
	
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI444; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI445; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI446; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI447; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI448; 
	@FieldAnnotation(orderIndex=6) 
	public TextAreaField txtQIOBS_4A;
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta444,lblobservacion,lblpregunta445,lblpregunta446,lblpregunta447,lblpregunta448,lblcomidas448,lblpregunta444_ind,lblpregunta445_ind,lblpregunta444_nro,lblpregunta445_nro; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI444","QI445","QI446","QI447","QI448")}; 
	SeccionCapitulo[] seccionesCargado; 
	public TextField txtCabecera;
	String nombre_persona;
	public ButtonComponent btnNoCome448,btnNoSabe448;
	public GridComponent2 gdPregunta448,gdnacimiento444,gdnacimiento445;
	public boolean esPreguntaInicial;
	public CheckBoxField chb448_ns,chb448_nc;
	public CISECCION_04AFragment_015() {} 
	public CISECCION_04AFragment_015 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
					
		rango(getActivity(), txtQI444, 0, 40); 
		rango(getActivity(), txtQI445, 0, 99); 
		rango(getActivity(), txtQI448, 1, 10); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI444","QI445","QI446","QI447","QI448","QIOBS_4A","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI444","QI445","QI446","QI447","QI448","QIOBS_4A")}; 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta444 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi444);
		lblpregunta444_ind =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi444_ind);
		lblpregunta445 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi445);
		lblpregunta445_ind =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi445_ind);
		lblpregunta446 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi446);
		lblpregunta447 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi447);
		lblpregunta448 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi448);
		lblobservacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqiobservacion);
		lblcomidas448  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_04aqi448v).textSize(16).centrar();
		lblpregunta444_nro  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_04aqi444_nro).textSize(16).centrar();
		lblpregunta445_nro  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_04aqi444_nro).textSize(16).centrar();
		
		txtQI444=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI445=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		rgQI446=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi446_1,R.string.c2seccion_04aqi446_2,R.string.c2seccion_04aqi446_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI447=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi447_1,R.string.c2seccion_04aqi447_2,R.string.c2seccion_04aqi447_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI448=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
    
		gdnacimiento444 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdnacimiento444.addComponent(lblpregunta444_nro);
		gdnacimiento444.addComponent(txtQI444);
		
		gdnacimiento445 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdnacimiento445.addComponent(lblpregunta445_nro);
		gdnacimiento445.addComponent(txtQI445);
		
		chb448_ns=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi448_ns, "1:0").size(MATCH_PARENT, MATCH_PARENT);
		chb448_ns.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb448_ns.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI448,chb448_nc);
			}
			else {
				Util.lockView(getActivity(),false, txtQI448,chb448_nc);	
			}
		}
		});
		
		chb448_nc=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi448_nc, "1:0").size(MATCH_PARENT, MATCH_PARENT);
		chb448_nc.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb448_nc.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI448,chb448_ns);
			}
			else {
				Util.lockView(getActivity(),false, txtQI448,chb448_ns);	
			}
		}
		});
		
		gdPregunta448 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdPregunta448.addComponent(lblcomidas448);
		gdPregunta448.addComponent(txtQI448);
		gdPregunta448.addComponent(chb448_nc,2);
		gdPregunta448.addComponent(chb448_ns,2);
		
		txtQIOBS_4A= new TextAreaField(getActivity()).size(200, 700).maxLength(1000).alfanumerico();
		
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta444,lblpregunta444_ind,gdnacimiento444.component()); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta445,lblpregunta445_ind,gdnacimiento445.component()); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta446,rgQI446); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta447,rgQI447); 
		q5	= createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta448,gdPregunta448.component());
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblobservacion,txtQIOBS_4A);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi446!=null) {
				c2seccion_04a.qi446=c2seccion_04a.getConvertQi436(c2seccion_04a.qi446);
			}
			
			if (c2seccion_04a.qi447!=null) {
				c2seccion_04a.qi447=c2seccion_04a.getConvertQi447(c2seccion_04a.qi447);
			}						
		}
		
		if(chb448_ns.isChecked() ) {
			c2seccion_04a.qi448=98;			
		}
		if(chb448_nc.isChecked() ) {
			c2seccion_04a.qi448=0;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 

		if(esPreguntaInicial) {
			
			if(App.getInstance().getSeccion04A().qi438==1) {
				if (Util.esVacio(c2seccion_04a.qi444)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI444"); 
					view = txtQI444; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(c2seccion_04a.qi445)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI445"); 
					view = txtQI445; 
					error = true; 
					return false; 
				} 
			}
		}
		if (Util.esVacio(c2seccion_04a.qi446)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI446"); 
			view = rgQI446; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04a.qi447)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI447"); 
			view = rgQI447; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04a.qi448) && !(chb448_ns.isChecked() || chb448_nc.isChecked())) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI448"); 
			view = txtQI448; 
			error = true; 
			return false; 
		} 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);		
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi446!=null) {
				c2seccion_04a.qi446=c2seccion_04a.setConvertQi436(c2seccion_04a.qi446);
			}
			
			if (c2seccion_04a.qi447!=null) {
				c2seccion_04a.qi447=c2seccion_04a.setConvertQi447(c2seccion_04a.qi447);
			}						
		}
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a);
		
		if(c2seccion_04a.qi448!=null && c2seccion_04a.qi448==98) {
			chb448_ns.setChecked(true);
			txtQI448.setText("");
		}
		
		if(c2seccion_04a.qi448!=null && c2seccion_04a.qi448==0) {
			chb448_nc.setChecked(true);
			txtQI448.setText("");
		}
		
		inicio(); 
    } 
    private void inicio() {
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		Util.cleanAndLockView(getActivity(), txtQI444,txtQI445);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    	else {
//    		Log.d("Mensaje","test seccion4a"+App.getInstance().getSeccion04A());
//    		Log.d("Mensaje","test qi438"+App.getInstance().getSeccion04A().qi438);
    		if(App.getInstance().getSeccion04A().qi438!=null && App.getInstance().getSeccion04A().qi438==1) {
    			Util.lockView(getActivity(),false,txtQI444,txtQI445);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
    		}
    		else {
        		Util.cleanAndLockView(getActivity(), txtQI444,txtQI445);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
    		}
    		
        	if (c2seccion_04a!=null && c2seccion_04a.qi448!=null && (c2seccion_04a.qi448==0 || c2seccion_04a.qi448==98)) { Util.cleanAndLockView(getActivity(),txtQI448); } else{ Util.lockView(getActivity(),false,txtQI448);}
    		
    	}
    }
    

	
	public void renombrarEtiquetas()
    {	
    	lblpregunta444.setText(lblpregunta444.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta445.setText(lblpregunta445.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta446.setText(lblpregunta446.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta447.setText(lblpregunta447.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta448.setText(lblpregunta448.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI446.readOnly();
			rgQI447.readOnly();
			chb448_nc.readOnly();
			chb448_ns.readOnly();
			txtQI444.readOnly();
			txtQI445.readOnly();
			txtQI448.readOnly();
			txtQIOBS_4A.setEnabled(false);
		}
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi446!=null) {
				c2seccion_04a.qi446=c2seccion_04a.getConvertQi436(c2seccion_04a.qi446);
			}
			
			if (c2seccion_04a.qi447!=null) {
				c2seccion_04a.qi447=c2seccion_04a.getConvertQi447(c2seccion_04a.qi447);
			}						
		}
		
		if(chb448_ns.isChecked() ) {
			c2seccion_04a.qi448=98;			
		}
		if(chb448_nc.isChecked() ) {
			c2seccion_04a.qi448=0;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
