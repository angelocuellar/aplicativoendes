package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_011 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI433B; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI433BX; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQI433C_N; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI433C; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQI433CX; 
	
	CISECCION_04A c2seccion_04a; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta433b,lblpregunta433c,lblpregunta433c_Pu_Ind,lblpregunta433c_Pri_Ind,lblpregunta433b_ind,lblpregunta433c_Org_Ind,lbllugar,lblpregunta433c_ind; 
	
	GridComponent2 gdLugarSalud;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 

	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI433B","QI433BX","QI433C_N","QI433C","QI433CX")}; 
	SeccionCapitulo[] seccionesCargado; 
	
	public TextField txtCabecera;
	String nombre_persona;
	public boolean esPreguntaInicial;
	public CISECCION_04AFragment_011() {} 
	public CISECCION_04AFragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
					
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI433B","QI433BX","QI433C_N","QI433C","QI433CX","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta433b= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi433b);
		lblpregunta433b_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi433b_ind);
		lblpregunta433c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi433c);
		lblpregunta433c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi433c_ind);

		rgQI433B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi433b_1,R.string.c2seccion_04aqi433b_2,R.string.c2seccion_04aqi433b_3,R.string.c2seccion_04aqi433b_4,R.string.c2seccion_04aqi433b_5,R.string.c2seccion_04aqi433b_6,R.string.c2seccion_04aqi433b_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI433BX=new TextField(this.getActivity()).size(altoComponente, 500);
		rgQI433B.agregarEspecifique(6,txtQI433BX); 
		
		Spanned texto433cPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta433c_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta433c_Pu_Ind.setText(texto433cPu);
		
		Spanned texto433cPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta433c_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta433c_Pri_Ind.setText(texto433cPri);
		
		Spanned texto433cOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta433c_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta433c_Org_Ind.setText(texto433cOrg);
			
		txtQI433C_N=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico();				
		rgQI433C=new RadioGroupOtherField(this.getActivity()).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI433C.addOption(1,getResources().getText(R.string.c2seccion_04aqi433c_1).toString());
	
		rgQI433C.addOption(2,getResources().getText(R.string.c2seccion_04aqi433c_2).toString());
		rgQI433C.addOption(3,getResources().getText(R.string.c2seccion_04aqi433c_3).toString());
		rgQI433C.addOption(4,getResources().getText(R.string.c2seccion_04aqi433c_4).toString());
		rgQI433C.addOption(5,getResources().getText(R.string.c2seccion_04aqi433c_5).toString());
		rgQI433C.addOption(6,getResources().getText(R.string.c2seccion_04aqi433c_6).toString());
		rgQI433C.addOption(7,getResources().getText(R.string.c2seccion_04aqi433c_7).toString());
		rgQI433C.addOption(8,getResources().getText(R.string.c2seccion_04aqi433c_8).toString());
		
		rgQI433C.addOption(9,getResources().getText(R.string.c2seccion_04aqi433c_9).toString());
		rgQI433C.addOption(10,getResources().getText(R.string.c2seccion_04aqi433c_10).toString());
		rgQI433C.addOption(11,getResources().getText(R.string.c2seccion_04aqi433c_11).toString());
		
		rgQI433C.addOption(12,getResources().getText(R.string.c2seccion_04aqi433c_12).toString());
		rgQI433C.addOption(13,getResources().getText(R.string.c2seccion_04aqi433c_13).toString());
		rgQI433C.addOption(14,getResources().getText(R.string.c2seccion_04aqi433c_14).toString());

		txtQI433CX=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI433C.agregarEspecifique(13,txtQI433CX); 
			
		
		rgQI433C.addView(lblpregunta433c_Pu_Ind,1);
		rgQI433C.addView(lblpregunta433c_Pri_Ind,9);
		rgQI433C.addView(lblpregunta433c_Org_Ind,13);
		
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04aqi433c_n).textSize(16);
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI433C_N);
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta433b,lblpregunta433b_ind,rgQI433B); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta433c,lblpregunta433c_ind,gdLugarSalud.component(),rgQI433C);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_04a); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(esPreguntaInicial) {
			if (Util.esVacio(c2seccion_04a.qi433b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI433B"); 
				view = rgQI433B; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04a.qi433b==7) {
				if (Util.esVacio(c2seccion_04a.qi433bx)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI433BX"); 
					view = txtQI433BX; 
					error = true; 
					return false; 
				} 
			}
			/*if (Util.esVacio(c2seccion_04a.qi433c_n)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI433_N"); 
				view = txtQI433C_N; 
				error = true; 
				return false; 
			} */
			if (Util.esVacio(c2seccion_04a.qi433c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI433C"); 
				view = rgQI433C; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04a.qi433c==14) {
				if (Util.esVacio(c2seccion_04a.qi433cx)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI433CX"); 
					view = txtQI433CX; 
					error = true; 
					return false; 
				} 
			}
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		inicio(); 
    } 
    private void inicio() { 
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		Util.cleanAndLockView(getActivity(), rgQI433B,txtQI433BX,txtQI433C_N,rgQI433C,txtQI433CX);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    }
 
	
	public void renombrarEtiquetas(){	
    	lblpregunta433c.setText(lblpregunta433c.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	   
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI433B.readOnly();
    		rgQI433C.readOnly();
    		txtQI433BX.readOnly();
    		txtQI433C_N.readOnly();
    		txtQI433CX.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
