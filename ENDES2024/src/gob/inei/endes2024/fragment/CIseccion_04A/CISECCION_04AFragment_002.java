package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI405; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI406_A; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI406_B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI407_A; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI407_B; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI407_C; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI407_D; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI407_E; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI407_F; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI407_X;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI407_Y; 
	@FieldAnnotation(orderIndex=12) 
	public TextField txtQI407_XI;	
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQI408; 	
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQI408_A;
	@FieldAnnotation(orderIndex=15)
	public CheckBoxField chbQI408_B;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQI408_C;
	@FieldAnnotation(orderIndex=17)
	public CheckBoxField chbQI408_D;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQI408_E;
	@FieldAnnotation(orderIndex=19)
	public CheckBoxField chbQI408_F;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQI408_G;
	@FieldAnnotation(orderIndex=21)
	public CheckBoxField chbQI408_H;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQI408_I;
	@FieldAnnotation(orderIndex=23)
	public CheckBoxField chbQI408_J;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQI408_K;
	@FieldAnnotation(orderIndex=25)
	public CheckBoxField chbQI408_L;
	@FieldAnnotation(orderIndex=26)
	public CheckBoxField chbQI408_X;
	@FieldAnnotation(orderIndex=27)
	public TextField txtQI408_O;
	LinearLayout form;
	
	String nombre_persona;
	CISECCION_01_03 seccion1_3;
	CISECCION_04A c2seccion_04a;
	CICALENDARIO_TRAMO_COL01_03 tramo;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lbllugar,lblpregunta405,lblpregunta406,lblpregunta407,lblpregunta407_ind,lblpregunta408,lblpregunta408lug,lblpregunta408_Pu_Ind,lblpregunta408_Pri_Ind,lblpregunta408_Org_Ind,lblprenatalmeses,lblvacio,lblpregunta408_ind,lblpregunta408lug_otro; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	public ButtonComponent btnNoSabePrenatal;
	public TextField txtCabecera;
	public IntegerField txtQI406C1,txtQI406C2;
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoTramos;
	SeccionCapitulo[] seccionesCargado2;
	
	public boolean esPrimerRegistro;	
	public GridComponent2 gdLugarSalud,gridPreguntas406;

	public CISECCION_04AFragment_002() {} 
	public CISECCION_04AFragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"NINIO_ID","QI405","QI406_A","QI406_B","QI407_A","QI407_B","QI407_C","QI407_D","QI407_E","QI407_F","QI407_X","QI407_XI","QI407_Y","QI408","QI408_A","QI408_B","QI408_C","QI408_D","QI408_E","QI408_F","QI408_G","QI408_H","QI408_I","QI408_J","QI408_K","QI408_L","QI408_X","QI408_O","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"NINIO_ID","QI405","QI406_A","QI406_B","QI407_A","QI407_B","QI407_C","QI407_D","QI407_E","QI407_F","QI407_X","QI407_XI","QI407_Y","QI408","QI408_A","QI408_B","QI408_C","QI408_D","QI408_E","QI408_F","QI408_G","QI408_H","QI408_I","QI408_J","QI408_K","QI408_L","QI408_X","QI408_O")}; 
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI229C_B","QI229C_C","ID","HOGAR_ID","PERSONA_ID")};//"QI101H","QI101M",
		seccionesCargadoTramos = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QITRAMO_ID","QIMES_INI","QIANIO_INI","QIR_INICIAL","QIMES_FIN","QIANIO_FIN","QIR_FINAL","NINIO_ID","TERMINACION_ID","EMBARAZO_ID","METODO_ID","NINGUNO_ID","ID","QICOL2","QICOL3","HOGAR_ID","PERSONA_ID","QICANTIDAD")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar();
		rgQI405=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi405_1,R.string.c2seccion_04aqi405_2,R.string.c2seccion_04aqi405_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI405ChangeValue"); 
		rgQI406_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi406_a_1,R.string.c2seccion_04aqi406_a_2,R.string.c2seccion_04aqi406_a_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI406ChangeValue"); 
		
		txtQI406C1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI406C2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
		
		gridPreguntas406 = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas406.addComponent(rgQI406_A,1,3);		
		gridPreguntas406.addComponent(txtQI406C1);
		gridPreguntas406.addComponent(txtQI406C2);
		gridPreguntas406.addComponent(lblvacio);
		
		chbQI407_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407aChangeValue");
		chbQI407_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407bChangeValue");
		chbQI407_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407cChangeValue");
		chbQI407_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407dChangeValue");
		chbQI407_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407eChangeValue");
		chbQI407_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407fChangeValue"); 
		chbQI407_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407xChangeValue");
		chbQI407_Y=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi407_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI407yChangeValue");
		txtQI407_XI=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);		
		
		txtQI408=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico();
		chbQI408_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI408_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi408_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI408xChangeValue");
		txtQI408_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500).alfanumerico(); 
		
		lblpregunta405 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi405);
		lblpregunta406 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi406);
		lblpregunta407 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi407);
		lblpregunta407_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi407_ind);
		lblpregunta408 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi408);
		lblpregunta408_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi408_ind);
		
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04aqi408nom).textSize(16);
		lblpregunta408lug_otro = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi408lug_otro);
		lblpregunta408lug = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi408lug);
		
		Spanned texto408Pu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta408_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta408_Pu_Ind.setText(texto408Pu);
		
		Spanned texto408Pri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta408_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta408_Pri_Ind.setText(texto408Pri);
		
		Spanned texto408Org =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta408_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta408_Org_Ind.setText(texto408Org);
		
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI408);
				
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta405,rgQI405); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta406,gridPreguntas406.component()); 
	
		LinearLayout ly407x = new LinearLayout(getActivity());
		ly407x.addView(chbQI407_X);
		ly407x.addView(txtQI407_XI);
		
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta407,lblpregunta407_ind,chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,chbQI407_E,chbQI407_F,ly407x,chbQI407_Y);
		
		LinearLayout ly408 = new LinearLayout(getActivity());
		ly408.addView(chbQI408_X);
		ly408.addView(txtQI408_O);
		
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta408,lblpregunta408_ind,gdLugarSalud.component(),lblpregunta408lug_otro,lblpregunta408lug,lblpregunta408_Pu_Ind,chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,chbQI408_E,chbQI408_F,chbQI408_G,lblpregunta408_Pri_Ind,chbQI408_H,chbQI408_I,chbQI408_J,lblpregunta408_Org_Ind,chbQI408_K,chbQI408_L,ly408);//,,,chbQI229_Y
		
		ScrollView contenedor = createForm(); 
		form = (LinearLayout) contenedor.getChildAt(0);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		if (c2seccion_04a!=null) {
							
			if (c2seccion_04a.qi406_a!=null) {
				c2seccion_04a.qi406_a=c2seccion_04a.getConvertQi406(c2seccion_04a.qi406_a);
				}
			
			if (c2seccion_04a.qi406_a!=null) {
				if (!Util.esDiferente(c2seccion_04a.qi406_a,1) && txtQI406C1.getText().toString().trim().length()!=0 ) {
					c2seccion_04a.qi406_b=Integer.parseInt(txtQI406C1.getText().toString().trim());
				}
				else if (!Util.esDiferente(c2seccion_04a.qi406_a,2) && txtQI406C2.getText().toString().trim().length()!=0) {
					c2seccion_04a.qi406_b=Integer.parseInt(txtQI406C2.getText().toString().trim());
				}
				else {
					c2seccion_04a.qi406_b=null;
				}
			}
			
			if(c2seccion_04a.qi406_a==null)
				c2seccion_04a.qi406_b=null;
		}
		
		if(App.getInstance().getSeccion04A()==null) {
			App.getInstance().setSeccion4A(c2seccion_04a);
		}		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			App.getInstance().setSeccion4A(c2seccion_04a);
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi407_y==1) {
					//SeccionCapitulo[] seccion =  CISECCION_04AFragment_003.seccionesGrabado ;
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_003.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI412","QI412A");					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_003.seccionesGrabado);
					seccs.add(CISECCION_04AFragment_004_1.seccionesGrabado);
					seccs.add(CISECCION_04AFragment_004_2.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			if(!Util.esDiferente(c2seccion_04a.qi405, 1))
			App.getInstance().getPersonaCuestionarioIndividual().qi405="QUER�A QUEDAR EMBARAZADA ENTONCES";
			if(!Util.esDiferente(c2seccion_04a.qi405, 2))
				App.getInstance().getPersonaCuestionarioIndividual().qi405="QUER�A ESPERAR M�S";
			if(!Util.esDiferente(c2seccion_04a.qi405, 3))
				App.getInstance().getPersonaCuestionarioIndividual().qi405="NO QUER�A M�S";
			
			App.getInstance().getPersonaCuestionarioIndividual().qi405=c2seccion_04a.qi405!=null?c2seccion_04a.qi405.toString():"";
		}
		return true; 
    } 
    
    
  
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (c2seccion_04a!=null) {		
			if (Util.esVacio(c2seccion_04a.qi405)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI405"); 
				view = rgQI405; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04a.qi405==2) {
			
				if (Util.esVacio(c2seccion_04a.qi406_a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI406_A"); 
					view = rgQI406_A; 
					error = true; 
					return false; 
				} 
				
				if (!Util.esDiferente(c2seccion_04a.qi406_a,1)) {
					if (Util.esVacio(c2seccion_04a.qi406_b)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.406C1"); 
						view = txtQI406C1; 
						error = true; 
						return false; 
					}			
				}	
				
				if (!Util.esDiferente(c2seccion_04a.qi406_a,2)) {
					if (Util.esVacio(c2seccion_04a.qi406_b)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.406C2"); 
						view = txtQI406C2; 
						error = true; 
						return false; 
					}				
				}
				
				
				if(c2seccion_04a.qi406_a==1) {
					if(!(c2seccion_04a.qi406_b>=1 && c2seccion_04a.qi406_b<=11)) {
						mensaje = "Rango no corresponde a selecci�n"; 
						view = txtQI406_B; 
						error = true; 
						return false; 
					}
				}
				if(c2seccion_04a.qi406_a==2) {
					if(!(c2seccion_04a.qi406_b>=1 && c2seccion_04a.qi406_b<=40)) {
						mensaje = "Rango no corresponde a selecci�n"; 
						view = txtQI406_B; 
						error = true; 
						return false; 
					}
				} 
			
			}
			
			if(esPrimerRegistro){   
			
				if(!verificarCheck407() && !chbQI407_Y.isChecked()) {
					mensaje = "Seleccione una opci�n en P.407"; 
					view = chbQI407_A; 
					error = true; 
					return false; 
				}
				
				if (chbQI407_X.isChecked()) {
					if (Util.esVacio(c2seccion_04a.qi407_xi)) { 
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI407_XI; 
						error = true; 
						return false; 
					}
				}		
						
				
				if(c2seccion_04a.qi407_y!=1) {
					/*		
					if (Util.esVacio(c2seccion_04a.qi408)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI408"); 
						view = txtQI408; 
						error = true; 
						return false; 
					} */
					
					if(seccion1_3.qi229c_b!=null) {
						if(c2seccion_04a.qi408_a==1 && seccion1_3.qi229c_b==1) {
								validarMensaje("VERIFICAR  �Tenencia de seguro si corresponde a lugar donde se controla�");
								error=true;
							}
					}
					
					if(seccion1_3.qi229c_c!=null) {
						if(c2seccion_04a.qi408_a==1 && seccion1_3.qi229c_c==1) {
								validarMensaje("VERIFICAR  �Tenencia de seguro si corresponde a lugar donde se controla�");
								error=true;
							}
					}		
							
					if(seccion1_3.qi229c_a!=null) {
						if(c2seccion_04a.qi408_b==1 && seccion1_3.qi229c_a==1) {
							validarMensaje("VERIFICAR  �Tenencia de seguro si corresponde a lugar donde se controla�");
							error=true;
							}
						if(c2seccion_04a.qi408_c==1 && seccion1_3.qi229c_a==1) {
							validarMensaje("VERIFICAR  �Tenencia de seguro si corresponde a lugar donde se controla�");
							error=true;
							}
					}
					
					if(!verificarCheck408() && !chbQI408_X.isChecked()) {
						mensaje = "Seleccione una opci�n en P.407"; 
						view = chbQI408_A; 
						error = true; 
						return false; 
					}
					
					if (chbQI408_X.isChecked()) {
						if (Util.esVacio(c2seccion_04a.qi408_o)) { 
							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
							view = txtQI408_O; 
							error = true; 
							return false; 
						}
					}		
					
				}
			}
		}
		if(ExisteMetodoConCodigoUnoEnLaColumnaDosDelCalendario() && Util.esDiferente(c2seccion_04a.qi405, 1)){
			mensaje = "Verificar Discontinuidad en Calendario"; 
			view = rgQI405; 
			error = true; 
			return false;
		}
		if(!ExisteMetodoConCodigoUnoEnLaColumnaDosDelCalendario() && !Util.esDiferente(c2seccion_04a.qi405, 1) && tramo!=null  && tramo.qir_inicial!=null && !tramo.qir_inicial.equals("0") && !tramo.qir_inicial.equals("E") && !tramo.qir_inicial.equals("T") && !tramo.qir_inicial.equals("N")){
			mensaje = "Verificar Discontinuidad en Calendario Es diferente de codigo 2. Deseaba quedar embarazada"; 
			view = rgQI405; 
			error = true; 
			return false;
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	
    	if(parent.isEsAvanceDual()) {
    		form.removeAllViews();
    		form.addView(q0); 
    		form.addView(q1); 
    		form.addView(q2); 
    		form.addView(q3); 
    		form.addView(q4);
    		}    	
    	else {
    		ocultarCampos(true);
    	}
    	
    	if(App.getInstance().getNacimiento()!=null) {    	
    		
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
	    } 
		
		esPrimerRegistro = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		App.getInstance().getNacimiento().esPrimerRegistro = esPrimerRegistro;
		if (c2seccion_04a!=null) {
			if (c2seccion_04a.qi406_a!=null) {
				c2seccion_04a.qi406_a=c2seccion_04a.setConvertQi406(c2seccion_04a.qi406_a);
				}
		}
		
		seccion1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id ,seccionesCargado2);
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 		
		
		if (c2seccion_04a.qi406_b!=null) {	
				
    		if (!Util.esDiferente(c2seccion_04a.qi406_a,1)) {
    			txtQI406C1.setText(c2seccion_04a.qi406_b.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi406_a,2)) {
    			txtQI406C2.setText(c2seccion_04a.qi406_b.toString());
    		}
    	}      
		
		inicio(); 
    	}
    } 
    private void inicio() {
    	saltoInicio();    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
        
    } 
    
    private void ocultarCampos(boolean estado) {
    	int ocultar = View.GONE;
    	if(!estado)
    		ocultar = View.VISIBLE;
    	q0.setVisibility(ocultar);
		q1.setVisibility(ocultar);
		q2.setVisibility(ocultar);
		q3.setVisibility(ocultar);
		q4.setVisibility(ocultar);
    }
    
    private void saltoInicio() {
    	if(!parent.isEsAvanceDual() ) {
    		Util.cleanAndLockView(getActivity(),rgQI405,rgQI406_A,txtQI406_B,chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,
    		chbQI407_E,chbQI407_F,chbQI407_X,chbQI407_Y,txtQI407_XI,txtQI408,chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,
    		chbQI408_E,chbQI408_F,chbQI408_G,chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O);
    		ocultarCampos(true);
    		
    	}
    	else {
    		Util.lockView(getActivity(),false,rgQI405,rgQI406_A,txtQI406_B,chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,
    	    chbQI407_E,chbQI407_F,chbQI407_X,chbQI407_Y,txtQI407_XI,txtQI408,chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,
    	    chbQI408_E,chbQI408_F,chbQI408_G,chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O);
    		ocultarCampos(false);
    	    
    	    evaluaPregunta();
    	    
    	    if(c2seccion_04a.qi405!=null)
        		onqrgQI405ChangeValue();
        	
        	onqrgQI406ChangeValue();
    	}
    }
    
    public void evaluaPregunta() {
    	if(!esPrimerRegistro){    
    		
    		Util.cleanAndLockView(getActivity(),chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,chbQI407_E,chbQI407_F,chbQI407_X,chbQI407_Y,
    				txtQI407_XI,chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,chbQI408_E,chbQI408_F,chbQI408_G,
    	    	    chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O,txtQI408);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    	else {
    		Util.lockView(getActivity(),false,chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,chbQI407_E,chbQI407_F,chbQI407_X,chbQI407_Y,
    				txtQI407_XI,chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,chbQI408_E,chbQI408_F,chbQI408_G,
    	    	    chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O,txtQI408);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    		
    		if (!chbQI407_X.isChecked()) {    	
        		Util.cleanAndLockView(getActivity(),txtQI407_XI);	
    		}
        	else {
        		Util.lockView(getActivity(),false,txtQI407_XI);  	
        	}    		
    		
    		onqrgQI407aChangeValue();
        	onqrgQI407yChangeValue();
        	if (!chbQI408_X.isChecked()) {
        		Util.cleanAndLockView(getActivity(),txtQI408_O);	
    		}
        	else {
        		Util.lockView(getActivity(),false,txtQI408_O);  	
        	}
        	
    	}
    }
    

	
	public boolean verificarCheck407() {
  		if (chbQI407_A.isChecked() || chbQI407_B.isChecked() || chbQI407_C.isChecked() || chbQI407_D.isChecked() || chbQI407_E.isChecked() || chbQI407_F.isChecked() ||chbQI407_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}		
	
	public boolean verificarCheck408() {
  		if (chbQI408_A.isChecked() || chbQI408_B.isChecked() || chbQI408_C.isChecked() || chbQI408_D.isChecked() || chbQI408_E.isChecked() || chbQI408_F.isChecked() ||chbQI408_G.isChecked()
  				||chbQI408_H.isChecked() || chbQI408_I.isChecked() || chbQI408_J.isChecked() ||chbQI408_K.isChecked() ||chbQI408_L.isChecked() ||chbQI408_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}	
	
	public void onqrgQI406ChangeValue() {
					
			if (MyUtil.incluyeRango(1,1,rgQI406_A.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI406C2);
				c2seccion_04a.qi406_b=null;
				Util.lockView(getActivity(),false,txtQI406C1);				
				txtQI406C1.requestFocus();				
			} 
			else if (MyUtil.incluyeRango(2,2,rgQI406_A.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI406C1);	
				c2seccion_04a.qi406_b=null;
				Util.lockView(getActivity(),false,txtQI406C2);			
				txtQI406C2.requestFocus();				
			}	
			else if (MyUtil.incluyeRango(3,3,rgQI406_A.getTagSelected("").toString())) {
				c2seccion_04a.qi406_b=null;		
				Util.cleanAndLockView(getActivity(),txtQI406C1,txtQI406C2);			
				chbQI407_A.requestFocus();				
			}
		
	}
	
	 public void onqrgQI407aChangeValue() {  	
	    	if (verificarCheck407()) {
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    public void onqrgQI407bChangeValue() {  	
	    	if (verificarCheck407()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    public void onqrgQI407cChangeValue() {  	
	    	if (verificarCheck407()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    public void onqrgQI407dChangeValue() {  	
	    	if (verificarCheck407()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    public void onqrgQI407eChangeValue() {  	
	    	if (verificarCheck407()) {    		
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    public void onqrgQI407fChangeValue() {  	
	    	if (verificarCheck407()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI407_Y);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
	    }
	    
	    
	    public void onqrgQI407xChangeValue() {  	
	    	if (chbQI407_X.isChecked()){		
	  			Util.lockView(getActivity(),false,txtQI407_XI);
	  			if (verificarCheck407()) {  			
	  				Util.cleanAndLockView(getActivity(),chbQI407_Y);
	  			}
	  			txtQI407_XI.requestFocus();
	  		} 
	  		else {
	  			Util.cleanAndLockView(getActivity(),txtQI407_XI);  
	  			if (!verificarCheck407()) {  			
	  				Util.lockView(getActivity(),false,chbQI407_Y);  				
				}
	  		}	
	    }
	    public void onqrgQI407yChangeValue() {    	
	  		if (chbQI407_Y.isChecked()){  		
	  			Util.cleanAndLockView(getActivity(),chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,chbQI407_E,chbQI407_F,chbQI407_X); 
	  			Util.cleanAndLockView(getActivity(),txtQI408, chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,chbQI408_E,chbQI408_F,
	  			chbQI408_G,chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O);
				q4.setVisibility(View.GONE);
				MyUtil.LiberarMemoria();
				c2seccion_04a.qi407_y = 1;
				App.getInstance().setSeccion4A(c2seccion_04a);
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,chbQI407_A,chbQI407_B,chbQI407_C,chbQI407_D,chbQI407_E,chbQI407_F,chbQI407_X); 
	  			Util.lockView(getActivity(),false,txtQI408, chbQI408_A,chbQI408_B,chbQI408_C,chbQI408_D,chbQI408_E,chbQI408_F,
	  			chbQI408_G,chbQI408_H,chbQI408_I,chbQI408_J,chbQI408_K,chbQI408_L,chbQI408_X,txtQI408_O); 
	  			q4.setVisibility(View.VISIBLE);
	  			onqrgQI408xChangeValue();
	  			c2seccion_04a.qi407_y = 0;
				App.getInstance().setSeccion4A(c2seccion_04a);
	  		}	
	  	}
	
	    
	    
	    public void onqrgQI408xChangeValue() {  	
	    	if (chbQI408_X.isChecked()){		
	  			Util.lockView(getActivity(),false,txtQI408_O);
	  			/*if (verificarCheck407()) {  			
	  				//Util.cleanAndLockView(getActivity(),chbQI408_X);
	  			}*/
	  			txtQI408_O.requestFocus();
	  		} 
	  		else {
	  			Util.cleanAndLockView(getActivity(),txtQI408_O);  
	  			/*if (!verificarCheck407()) {  			
	  				Util.lockView(getActivity(),false,chbQI407_Y);  				
				}*/
	  		}	
	    }
	 
	    public void onqrgQI405ChangeValue() {  	
			if(rgQI405.getValue()!=null) {
//				if (rgQI405.getValue().toString().equals("1") || rgQI405.getValue().toString().equals("3")) {    ///se modific� a peticion de usuario fecha 05/01/2017 
				if (rgQI405.getValue().toString().equals("1") || rgQI405.getValue().toString().equals("3")) { 
					if (rgQI405.getValue().toString().equals("3")) {
						if(ExisteMetodoConCodigoUnoEnLaColumnaDosDelCalendario())validarMensaje("Verificar Discontinuidad en Calendario");					
					}		    		
					Util.cleanAndLockView(getActivity(),rgQI406_A,txtQI406C1,txtQI406C2);
					q2.setVisibility(View.GONE);
					MyUtil.LiberarMemoria();
		    	}
				else {
					if (rgQI405.getValue().toString().equals("2")) {
						if(ExisteMetodoConCodigoUnoEnLaColumnaDosDelCalendario())validarMensaje("Verificar Discontinuidad en Calendario");					
					}
					Util.lockView(getActivity(),false,rgQI406_A,txtQI406C1,txtQI406C2);
					q2.setVisibility(View.VISIBLE);
				}
			}
	    }   
	    
	public boolean ExisteMetodoConCodigoUnoEnLaColumnaDosDelCalendario(){
		boolean retorna=false;
		tramo = getCuestionarioService().getTramoparaNacimientos(c2seccion_04a.id, c2seccion_04a.hogar_id, c2seccion_04a.persona_id,c2seccion_04a.ninio_id, seccionesCargadoTramos);
		if(tramo!=null && tramo.qimes_ini!=null) {
		Integer mes=0;
		Integer anio=0;
		mes=tramo.qimes_ini-1;
		anio=tramo.qianio_ini;
		if(mes==0){
			mes=12;
			anio=tramo.qianio_ini-1;	
		}
		tramo=getCuestionarioService().getTramoporFechadeInicio(c2seccion_04a.id,c2seccion_04a.hogar_id,c2seccion_04a.persona_id,mes,anio,seccionesCargadoTramos);
		if(tramo!=null && tramo.qicol2!=null && tramo.qicol2.equals("2")){
			retorna= true;
		}
		}
		return retorna;
	}
	    

	private void validarMensaje(String msj) {
	    ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	public void renombrarEtiquetas()
    {	
		lblpregunta405.text(R.string.c2seccion_04aqi405);
		lblpregunta407.text(R.string.c2seccion_04aqi407);
		
    	lblpregunta405.setText(lblpregunta405.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta407.setText(lblpregunta407.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI405.readOnly();
    		rgQI406_A.readOnly();
    		txtQI406C2.readOnly();
    		txtQI407_XI.readOnly();
    		txtQI408.readOnly();
    		txtQI408_O.readOnly();
    		chbQI407_A.readOnly();
    		chbQI407_B.readOnly();
    		chbQI407_C.readOnly();
    		chbQI407_D.readOnly();
    		chbQI407_E.readOnly();
    		chbQI407_F.readOnly();
    		chbQI407_X.readOnly();
    		chbQI407_X.readOnly();
    		chbQI408_A.readOnly();
    		chbQI408_B.readOnly();
    		chbQI408_C.readOnly();
    		chbQI408_D.readOnly();
    		chbQI408_E.readOnly();
    		chbQI408_F.readOnly();
    		chbQI408_G.readOnly();
    		chbQI408_H.readOnly();
    		chbQI408_I.readOnly();
    		chbQI408_J.readOnly();
    		chbQI408_K.readOnly();
    		chbQI408_L.readOnly();
    		chbQI408_X.readOnly();
    	}
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		if (c2seccion_04a!=null) {
							
			if (c2seccion_04a.qi406_a!=null) {
				c2seccion_04a.qi406_a=c2seccion_04a.getConvertQi406(c2seccion_04a.qi406_a);
				}
			
			if (c2seccion_04a.qi406_a!=null) {
				if (!Util.esDiferente(c2seccion_04a.qi406_a,1) && txtQI406C1.getText().toString().trim().length()!=0 ) {
					c2seccion_04a.qi406_b=Integer.parseInt(txtQI406C1.getText().toString().trim());
				}
				else if (!Util.esDiferente(c2seccion_04a.qi406_a,2) && txtQI406C2.getText().toString().trim().length()!=0) {
					c2seccion_04a.qi406_b=Integer.parseInt(txtQI406C2.getText().toString().trim());
				}
				else {
					c2seccion_04a.qi406_b=null;
				}
			}
			
			if(c2seccion_04a.qi406_a==null)
				c2seccion_04a.qi406_b=null;
		}
		
		if(App.getInstance().getSeccion04A()==null) {
			App.getInstance().setSeccion4A(c2seccion_04a);
		}		
		
		try { 
			App.getInstance().setSeccion4A(c2seccion_04a);
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi407_y==1) {
					//SeccionCapitulo[] seccion =  CISECCION_04AFragment_003.seccionesGrabado ;
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_003.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI412","QI412A");					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_003.seccionesGrabado);
					seccs.add(CISECCION_04AFragment_004_1.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
