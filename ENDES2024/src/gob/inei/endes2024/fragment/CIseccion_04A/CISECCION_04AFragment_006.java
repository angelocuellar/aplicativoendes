package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_006 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI426D; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI426E; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI426FB;	
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI426FB_C; 
	@FieldAnnotation(orderIndex=5) 
	public TextAreaField txtQI426FB_O;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI426GA; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI426GB; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI426GC; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI426GD; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI426GE; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI426GE_O; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI427; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI427AA; 
	@FieldAnnotation(orderIndex=14) 
	public IntegerField txtQI427AB;
	@FieldAnnotation(orderIndex=15) 
	public TextAreaField txtQI427A_O;
	
	CISECCION_04A c2seccion_04a;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta426d,lblpregunta426e,lblpregunta426fb,lblobservacion,lblpregunta426fb_ind,lblpregunta427_ind,lblpregunta426g,lblpregunta427,lblpregunta427a,lbl426fb_c,lbl426ga,lbl426gb,
	lbl426gc,lbl426gd,lbl426ge,lbl427AB,lblvacio1,lblvacio2,lblvacio427a1,lblblanco ,lblP426g_1 ,lblP426g_2 ,lblQI426gA,lblQI426gA2,lblpregunta427_observacion,
	lblQI426gB,lblQI426gB2,lblQI426gC,lblQI426gC2,lblQI426gD,lblQI426gD2,lblQI426gE,lblQI426gE2; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	public boolean esPreguntaInicial;
	public TextField txtCabecera;
	String nombre_persona;
	public IntegerField txtQI426FBC1,txtQI426FBC2,txtQI426FBC3,txtQI427AC1,txtQI427AC2,txtQI427AC3;
	public GridComponent2 gridPreguntas426fb,gridPreguntas427a,grid1;
	public CISECCION_04AFragment_006() {} 
	public CISECCION_04AFragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI426D","QI426E","QI426FB","QI426FB_C","QI426FB_O","QI426GA","QI426GB","QI426GC","QI426GD","QI426GE","QI426GE_O","QI427","QI427AA","QI427AB","QI427A_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI426D","QI426E","QI426FB","QI426FB_C","QI426FB_O","QI426GA","QI426GB","QI426GC","QI426GD","QI426GE","QI426GE_O","QI427","QI427AA","QI427AB","QI427A_O")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar(); 
		
		lblpregunta426d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426d);
		lblpregunta426e = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426e);
		lblpregunta426fb = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426fb);
		lblpregunta426fb_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi426fb_ind);
		lblobservacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426fb_o);
		lblpregunta426g = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi426g);
		lblpregunta427 =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427);
		lblpregunta427_ind =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi427_ind);		
		lblpregunta427a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");		
		lblpregunta427_observacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi427a_observacion);
		
		rgQI426D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426d_1,R.string.c2seccion_04aqi426d_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI426dChangeValue"); 
		rgQI426E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426e_1,R.string.c2seccion_04aqi426e_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI426eChangeValue"); 
		rgQI426FB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426fb_1,R.string.c2seccion_04aqi426fb_2,R.string.c2seccion_04aqi426fb_3,R.string.c2seccion_04aqi426fb_4,R.string.c2seccion_04aqi426fb_5).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI426FBChangeValue"); 
		txtQI426FB_O= new TextAreaField(getActivity()).size(100, 750).maxLength(1000).alfanumerico();		
		
		txtQI426FBC1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI426FBC2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		txtQI426FBC3=new IntegerField(this.getActivity()).size(75, 100).maxLength(1);
		lblvacio1 = new LabelComponent(this.getActivity()).size(75, 100);
		lblvacio2 = new LabelComponent(this.getActivity()).size(75, 100);
		
		gridPreguntas426fb = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas426fb.addComponent(rgQI426FB,1,5);		
		gridPreguntas426fb.addComponent(txtQI426FBC1);
		gridPreguntas426fb.addComponent(txtQI426FBC2);
		gridPreguntas426fb.addComponent(txtQI426FBC3);
		gridPreguntas426fb.addComponent(lblvacio1);
		gridPreguntas426fb.addComponent(lblvacio2);
	
		
		lblQI426gA=new LabelComponent(getActivity()).textSize(17).size(altoComponente+35, 550).text(R.string.c2seccion_04aqi426ga);		
		rgQI426GA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426g_o1,R.string.c2seccion_04aqi426g_o2).size(altoComponente+35,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI426gaChangeValue"); 
				
		lblQI426gB=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 550).text(R.string.c2seccion_04aqi426gb);	
		rgQI426GB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426g_o1,R.string.c2seccion_04aqi426g_o2).size(altoComponente,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI426gbChangeValue"); 
		
		lblQI426gC=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 550).text(R.string.c2seccion_04aqi426gc);
		rgQI426GC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426g_o1,R.string.c2seccion_04aqi426g_o2).size(altoComponente,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI426gbChangeValue");
				
		lblQI426gD=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 550).text(R.string.c2seccion_04aqi426gd);
		rgQI426GD=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426g_o1,R.string.c2seccion_04aqi426g_o2).size(altoComponente,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI426gdChangeValue");  
		
		lblQI426gE=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 550).text(R.string.c2seccion_04aqi426ge);
		rgQI426GE=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi426g_o1,R.string.c2seccion_04aqi426g_o2).size(altoComponente,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI426geChangeValue");  
		 
		txtQI426GE_O= new TextField(getActivity()).size(altoComponente, 750);
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI426gA);
		grid1.addComponent(rgQI426GA);
		grid1.addComponent(lblQI426gB);
		grid1.addComponent(rgQI426GB);
		grid1.addComponent(lblQI426gC);
		grid1.addComponent(rgQI426GC);
		grid1.addComponent(lblQI426gD);
		grid1.addComponent(rgQI426GD);
		grid1.addComponent(lblQI426gE);
		grid1.addComponent(rgQI426GE);
		grid1.addComponent(txtQI426GE_O,2);
		
		lbl426fb_c = new LabelComponent(getActivity()).size(altoComponente, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi426fb_c);
		
		rgQI427=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427_1,R.string.c2seccion_04aqi427_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI427ChangeValue"); 
		rgQI427AA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi427aa_1,R.string.c2seccion_04aqi427aa_2,R.string.c2seccion_04aqi427aa_3,R.string.c2seccion_04aqi427aa_4).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI427AChangeValue"); 
		
		txtQI427AC1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI427AC2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		txtQI427AC3=new IntegerField(this.getActivity()).size(75, 100).maxLength(1);
		lblvacio427a1 = new LabelComponent(this.getActivity()).size(75, 100);
				
		gridPreguntas427a = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas427a.addComponent(rgQI427AA,1,4);		
		gridPreguntas427a.addComponent(txtQI427AC1);
		gridPreguntas427a.addComponent(txtQI427AC2);
		gridPreguntas427a.addComponent(txtQI427AC3);
		gridPreguntas427a.addComponent(lblvacio427a1);
		
		lbl427AB = new LabelComponent(getActivity()).size(altoComponente, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04aqi427ab);
		txtQI427AB=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI427A_O= new TextAreaField(getActivity()).size(100, 750).maxLength(1000).alfanumerico();
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera, lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta426d,rgQI426D); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta426e,rgQI426E); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta426fb,lblpregunta426fb_ind,gridPreguntas426fb.component(),lblobservacion,txtQI426FB_O); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta426g,grid1.component());
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta427,rgQI427); 
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta427a,lblpregunta427_ind,gridPreguntas427a.component(),lblpregunta427_observacion,txtQI427A_O);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {			
			if (c2seccion_04a.qi426fb!=null) {
				c2seccion_04a.qi426fb=c2seccion_04a.getConvertQi426fb(c2seccion_04a.qi426fb);
				}
			
			if (c2seccion_04a.qi427aa!=null) {
				c2seccion_04a.qi427aa=c2seccion_04a.getConvertQi427a(c2seccion_04a.qi427aa);
				}
		}
		
		if (c2seccion_04a.qi426fb!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi426fb,1) && txtQI426FBC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi426fb,2) && txtQI426FBC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi426fb,3) && txtQI426FBC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi426fb_c=null;
			}
		}
		if (c2seccion_04a.qi426fb==null) 
			c2seccion_04a.qi426fb_c=null;
		
		if (c2seccion_04a.qi427aa!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi427aa,1) && txtQI427AC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi427aa,2) && txtQI427AC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi427aa,3) && txtQI427AC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi427ab=null;
			}
		}
		if (c2seccion_04a.qi427aa==null) 
			c2seccion_04a.qi427ab=null;
		
		App.getInstance().getSeccion04A().qi427 = c2seccion_04a.qi427;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi427==2) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_007.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_007.seccionesGrabado);
					seccs.add(CISECCION_04AFragment_008.seccionesGrabado);
					App.getInstance().getSeccion04A().qi428 = null;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(!(App.getInstance().getSeccion04A().qi426a!=null &&  (App.getInstance().getSeccion04A().qi426a==1 || App.getInstance().getSeccion04A().qi426a==11 || App.getInstance().getSeccion04A().qi426a==14))) {
		
			if (Util.esVacio(c2seccion_04a.qi426d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426D"); 
				view = rgQI426D; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04a.qi426d==1) {
				if (Util.esVacio(c2seccion_04a.qi426e)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI426E"); 
					view = rgQI426E; 
					error = true; 
					return false; 
				} 
			}
		

			if((c2seccion_04a.qi426d==2 && c2seccion_04a.qi426e==null) || (c2seccion_04a.qi426d==1 && c2seccion_04a.qi426e==2)) {
				if (Util.esVacio(c2seccion_04a.qi426fb)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI429FB"); 
					view = rgQI426FB; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(c2seccion_04a.qi426fb,1)) {
					if (Util.esVacio(c2seccion_04a.qi426fb_c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.426FBC1"); 
						view = txtQI426FBC1; 
						error = true; 
						return false; 
					}
					if ( Util.esMenor(c2seccion_04a.qi426fb_c,0) || Util.esMayor(c2seccion_04a.qi426fb_c,23)) {
						mensaje = "Valores validos en Horas de 0 a 23"; 
						view = txtQI426FBC1; 
						error = true; 
						return false; 
					}
				}	
				
				if (!Util.esDiferente(c2seccion_04a.qi426fb,2)) {
					if (Util.esVacio(c2seccion_04a.qi426fb_c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.426FBC2"); 
						view = txtQI426FBC2; 
						error = true; 
						return false; 
					}		
					if (Util.esMenor(c2seccion_04a.qi426fb_c,1) || Util.esMayor(c2seccion_04a.qi426fb_c,29)) {
						mensaje = "Valores validos en D�as de 1 a 29"; 
						view = txtQI426FBC2; 
						error = true; 
						return false; 
					}
				}
				
				if (!Util.esDiferente(c2seccion_04a.qi426fb,3)) {
					if (Util.esVacio(c2seccion_04a.qi426fb_c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.426FBC3"); 
						view = txtQI426FBC3; 
						error = true; 
						return false; 
					}	
					if (Util.esMenor(c2seccion_04a.qi426fb_c,1) || Util.esMayor(c2seccion_04a.qi426fb_c,9)) {
						mensaje = "Valores validos en Semanas de 1 a 9"; 
						view = txtQI426FBC3; 
						error = true; 
						return false; 
					}
				}
			}
		}
		if(esPreguntaInicial) {
			if (Util.esVacio(c2seccion_04a.qi426ga)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426GA"); 
				view = rgQI426GA; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi426gb)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426GB"); 
				view = rgQI426GB; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi426gc)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426GC"); 
				view = rgQI426GC; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi426gd)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426GD"); 
				view = rgQI426GD; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi426ge)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI426GE"); 
				view = rgQI426GE; 
				error = true; 
				return false; 
			} 
			
			if(!Util.esDiferente(c2seccion_04a.qi426ge,1)) {
				if (Util.esVacio(c2seccion_04a.qi426ge_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI426GE_O; 
					error = true; 
					return false; 
				}
			}
		}
		
		if (Util.esVacio(c2seccion_04a.qi427)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI427"); 
			view = rgQI427; 
			error = true; 
			return false; 
		} 
		
		if(esPreguntaInicial) {		
			if(c2seccion_04a.qi427==1) {
				if (Util.esVacio(c2seccion_04a.qi427aa)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI427AA"); 
					view = rgQI427AA; 
					error = true; 
					return false; 
				} 
				
				if (!Util.esDiferente(c2seccion_04a.qi427aa,1)) {
					if (Util.esVacio(c2seccion_04a.qi427ab)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.427AC1"); 
						view = txtQI427AC1; 
						error = true; 
						return false; 
					}	
					if (Util.esMenor(c2seccion_04a.qi427ab,0) || Util.esMayor(c2seccion_04a.qi427ab,23)) {
						mensaje = "Valores validos en Horas de 0 a 23"; 
						view = txtQI427AC1; 
						error = true; 
						return false; 
					}
				}	
				
				if (!Util.esDiferente(c2seccion_04a.qi427aa,2)) {
					if (Util.esVacio(c2seccion_04a.qi427ab)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.427AC2"); 
						view = txtQI427AC2; 
						error = true; 
						return false; 
					}	
					if (Util.esMenor(c2seccion_04a.qi427ab,1) || Util.esMayor(c2seccion_04a.qi427ab,29)) {
						mensaje = "Valores validos en D�as de 1 a 29"; 
						view = txtQI427AC2; 
						error = true; 
						return false; 
					}
				}
				
				if (!Util.esDiferente(c2seccion_04a.qi427aa,3)) {
					if (Util.esVacio(c2seccion_04a.qi427ab)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.427AC3"); 
						view = txtQI427AC3; 
						error = true; 
						return false; 
					}
					if (Util.esMenor(c2seccion_04a.qi427ab,1) || Util.esMayor(c2seccion_04a.qi427ab,9)) {
						mensaje = "Valores validos en Semanas de 1 a 9"; 
						view = txtQI427AC3; 
						error = true; 
						return false; 
					}
				}
			}
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado);
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212; 
	    } 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		if (c2seccion_04a!=null) {			
			if (c2seccion_04a.qi426fb!=null) {
				c2seccion_04a.qi426fb=c2seccion_04a.setConvertQi426fb(c2seccion_04a.qi426fb);
				}
			if (c2seccion_04a.qi427aa!=null) {
				c2seccion_04a.qi427aa=c2seccion_04a.setConvertQi427a(c2seccion_04a.qi427aa);
				}
		}
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a);
		
		
		
		if (c2seccion_04a.qi426fb_c!=null) {	
    		if (!Util.esDiferente(c2seccion_04a.qi426fb,1)) {
    			txtQI426FBC1.setText(c2seccion_04a.qi426fb_c.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi426fb,2)) {
    			txtQI426FBC2.setText(c2seccion_04a.qi426fb_c.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi426fb,3)) {
    			txtQI426FBC3.setText(c2seccion_04a.qi426fb_c.toString());
    		}
    	}  
		
		if (c2seccion_04a.qi427ab!=null) {	
    		if (!Util.esDiferente(c2seccion_04a.qi427aa,1)) {
    			txtQI427AC1.setText(c2seccion_04a.qi427ab.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi427aa,2)) {
    			txtQI427AC2.setText(c2seccion_04a.qi427ab.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04a.qi427aa,3)) {
    			txtQI427AC3.setText(c2seccion_04a.qi427ab.toString());
    		}
    	}  
		
		inicio(); 
    } 
    private void inicio() { 
    	
    	/*onqrgQI426geChangeValue();
    	if(c2seccion_04a.qi427!=null)
    		onqrgQI427ChangeValue();
    	onqrgQI427AChangeValue();*/
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		
    		Util.cleanAndLockView(getActivity(),rgQI426GA,rgQI426GB,rgQI426GC,rgQI426GD,rgQI426GE,txtQI426GE_O,rgQI427AA,txtQI427AC1,txtQI427AC2,txtQI427AC3,txtQI427A_O);
    		q4.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		
    		if(App.getInstance().getSeccion04A().qi426a!=null && (App.getInstance().getSeccion04A().qi426a==1 || App.getInstance().getSeccion04A().qi426a==11 || App.getInstance().getSeccion04A().qi426a==14) )
    		{
    			Util.cleanAndLockView(getActivity(),rgQI426D,rgQI426E,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
        		q3.setVisibility(View.GONE);
        		MyUtil.LiberarMemoria();
    		}
    		else {
    			Util.lockView(getActivity(),false,rgQI426D,rgQI426E,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
        		q3.setVisibility(View.VISIBLE);
        		if(c2seccion_04a.qi426d!=null)
            		onqrgQI426dChangeValue();
            	if(c2seccion_04a.qi426e!=null)
            		onqrgQI426eChangeValue();  
            	onqrgQI426FBChangeValue();
    			
    		}
    		    		
    		MyUtil.LiberarMemoria();
    	}
    	else {
    		if(App.getInstance().getSeccion04A().qi426a!=null && (App.getInstance().getSeccion04A().qi426a==1 || App.getInstance().getSeccion04A().qi426a==11 || App.getInstance().getSeccion04A().qi426a==14) )
    		{
    			Util.cleanAndLockView(getActivity(),rgQI426D,rgQI426E,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
        		q1.setVisibility(View.GONE);
        		q2.setVisibility(View.GONE);
        		q3.setVisibility(View.GONE);
        		MyUtil.LiberarMemoria();
    		}
    		else {
    			Util.lockView(getActivity(),false,rgQI426D,rgQI426E,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
        		q1.setVisibility(View.VISIBLE);
        		q2.setVisibility(View.VISIBLE);
        		q3.setVisibility(View.VISIBLE);
        		
        		if(c2seccion_04a.qi426d!=null)
            		onqrgQI426dChangeValue();
            	if(c2seccion_04a.qi426e!=null)
            		onqrgQI426eChangeValue();
            		onqrgQI426FBChangeValue();
//        		MyUtil.LiberarMemoria();
    		}
    		onqrgQI426geChangeValue();
    		if(c2seccion_04a.qi427!=null)
        		onqrgQI427ChangeValue();
    		onqrgQI427AChangeValue();
    	
    	
    	}
    }
    
	
	public void onqrgQI426dChangeValue() {
		if(rgQI426D.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI426D.getTagSelected("").toString())) {			
				Util.lockView(getActivity(),false,rgQI426E);
				q2.setVisibility(View.VISIBLE);
				rgQI426E.requestFocus();				
			} 
			else {
				Util.cleanAndLockView(getActivity(),rgQI426E);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.VISIBLE);
				Util.lockView(getActivity(),false,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
				
				rgQI426FB.requestFocus();
				MyUtil.LiberarMemoria();
			}
		}
	}
	
	public void onqrgQI426eChangeValue() {
		if(rgQI426E.getTagSelected("").toString()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI426E.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
				q3.setVisibility(View.GONE);
				MyUtil.LiberarMemoria();				
			} 
			else {
				Util.lockView(getActivity(),false,rgQI426FB,txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);
				q3.setVisibility(View.VISIBLE);
				rgQI426FB.requestFocus();
			}
		}
	}
	
	public void onqrgQI426FBChangeValue() {
				
		
		if (MyUtil.incluyeRango(1,1,rgQI426FB.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI426FBC2,txtQI426FBC3);
			c2seccion_04a.qi426fb_c=null;
			Util.lockView(getActivity(),false,txtQI426FBC1);				
			txtQI426FBC1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI426FB.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI426FBC1,txtQI426FBC3);	
			c2seccion_04a.qi426fb_c=null;
			Util.lockView(getActivity(),false,txtQI426FBC2);			
			txtQI426FBC2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI426FB.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI426FBC1,txtQI426FBC2);			
			c2seccion_04a.qi426fb_c=null;
			Util.lockView(getActivity(),false,txtQI426FBC3);			
			txtQI426FBC3.requestFocus();								
		}	
		else if (MyUtil.incluyeRango(4,4,rgQI426FB.getTagSelected("").toString())) {
			c2seccion_04a.qi426fb_c=null;		
			Util.cleanAndLockView(getActivity(),txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);			
		    rgQI426GA.requestFocus();				
		}
		else if (MyUtil.incluyeRango(5,5,rgQI426FB.getTagSelected("").toString())) {
			c2seccion_04a.qi426fb_c=null;		
			Util.cleanAndLockView(getActivity(),txtQI426FBC1,txtQI426FBC2,txtQI426FBC3);			
		    rgQI426GA.requestFocus();				
		}
	}
	
	public void onqrgQI427ChangeValue() {
		if(rgQI427.getValue()!=null) {
			if(rgQI427.getTagSelected("").toString()!=null) {
				if (MyUtil.incluyeRango(1,1,rgQI427.getTagSelected("").toString()) && esPreguntaInicial) {			
					Util.lockView(getActivity(),false,rgQI427AA,txtQI427AC1,txtQI427AC2,txtQI427AC3,txtQI427A_O);
					q6.setVisibility(View.VISIBLE);				
					rgQI427AA.requestFocus();				
				} 
				else {			
					
					Util.cleanAndLockView(getActivity(),rgQI427AA,txtQI427AC1,txtQI427AC2,txtQI427AC3,txtQI427A_O);
					q6.setVisibility(View.GONE);
					MyUtil.LiberarMemoria();
				}
				App.getInstance().getSeccion04A().qi427 = Integer.parseInt(rgQI427.getValue().toString()); 
			}
		}
	}
	
	public void onqrgQI427AChangeValue() {
		
		if (MyUtil.incluyeRango(1,1,rgQI427AA.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI427AC2,txtQI427AC3);
			c2seccion_04a.qi427ab=null;
			Util.lockView(getActivity(),false,txtQI427AC1);				
			txtQI427AC1.requestFocus();				
		} 
		else if (MyUtil.incluyeRango(2,2,rgQI427AA.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),txtQI427AC1,txtQI427AC3);	
			c2seccion_04a.qi427ab=null;
			Util.lockView(getActivity(),false,txtQI427AC2);			
			txtQI427AC2.requestFocus();				
		}
		else if (MyUtil.incluyeRango(3,3,rgQI427AA.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQI427AC1,txtQI427AC2);			
			c2seccion_04a.qi427ab=null;
			Util.lockView(getActivity(),false,txtQI427AC3);			
			txtQI427AC3.requestFocus();								
		}	
		else if (MyUtil.incluyeRango(4,4,rgQI427AA.getTagSelected("").toString())) {
			c2seccion_04a.qi427ab=null;		
			Util.cleanAndLockView(getActivity(),txtQI427AC1,txtQI427AC2,txtQI427AC3);			
		    			
		}
		
	}
	
	
	public void onqrgQI426geChangeValue() {  	
		if (rgQI426GE.getValue()!=null && rgQI426GE.getValue().toString().equals("1")) {    		
			Util.lockView(getActivity(),false,txtQI426GE_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI426GE_O);    		
    	}	
    }
	
	public void renombrarEtiquetas(){	
		lblpregunta427a.setText("(NOMBRE)");
    	lblpregunta426d.setText(lblpregunta426d.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta426e.setText(lblpregunta426e.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta426g.setText(lblpregunta426g.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta427.setText(lblpregunta427.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta427a.setText(lblpregunta427a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	Spanned texto427a =Html.fromHtml("427A. �A las cu�ntas <b>horas</b>, d�as o semanas despu�s del nacimiento de "+lblpregunta427a.getText() +" tuvo Ud. su primer chequeo o revisi�n m�dica?");
    	lblpregunta427a.setText(texto427a);
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI426D.readOnly();
			rgQI426E.readOnly();
			rgQI426FB.readOnly();
			rgQI426GA.readOnly();
			rgQI426GB.readOnly();
			rgQI426GC.readOnly();
			rgQI426GD.readOnly();
			rgQI426GE.readOnly();
			rgQI427.readOnly();
			rgQI427AA.readOnly();
			txtQI426FB_O.setEnabled(false);
			txtQI426FBC1.readOnly();
			txtQI426FBC2.readOnly();
			txtQI426FBC3.readOnly();
			txtQI426GE_O.readOnly();
			txtQI427A_O.setEnabled(false);
			txtQI427AC1.readOnly();
			txtQI427AC2.readOnly();
			txtQI427AC3.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		if (c2seccion_04a!=null) {			
			if (c2seccion_04a.qi426fb!=null) {
				c2seccion_04a.qi426fb=c2seccion_04a.getConvertQi426fb(c2seccion_04a.qi426fb);
				}
			
			if (c2seccion_04a.qi427aa!=null) {
				c2seccion_04a.qi427aa=c2seccion_04a.getConvertQi427a(c2seccion_04a.qi427aa);
				}
		}
		
		if (c2seccion_04a.qi426fb!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi426fb,1) && txtQI426FBC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi426fb,2) && txtQI426FBC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi426fb,3) && txtQI426FBC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi426fb_c=Integer.parseInt(txtQI426FBC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi426fb_c=null;
			}
		}
		if (c2seccion_04a.qi426fb==null) 
			c2seccion_04a.qi426fb_c=null;
		
		if (c2seccion_04a.qi427aa!=null) {
			if (!Util.esDiferente(c2seccion_04a.qi427aa,1) && txtQI427AC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi427aa,2) && txtQI427AC2.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC2.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04a.qi427aa,3) && txtQI427AC3.getText().toString().trim().length()!=0) {
				c2seccion_04a.qi427ab=Integer.parseInt(txtQI427AC3.getText().toString().trim());
			}
			else {
				c2seccion_04a.qi427ab=null;
			}
		}
		if (c2seccion_04a.qi427aa==null) 
			c2seccion_04a.qi427ab=null;
		
		App.getInstance().getSeccion04A().qi427 = c2seccion_04a.qi427;
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				
				if(App.getInstance().getNacimiento().esPrimerRegistro == true && App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi427==2) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_007.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
				else if(App.getInstance().getNacimiento().esPrimerRegistro == false) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(CISECCION_04AFragment_007.seccionesGrabado);
					seccs.add(CISECCION_04AFragment_008.seccionesGrabado);
					App.getInstance().getSeccion04A().qi428 = null;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04a,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
