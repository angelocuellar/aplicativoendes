package gob.inei.endes2024.fragment.CIseccion_04A; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04AFragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI409; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI410; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI410_B; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI411_A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI411_B; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI411_C; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI411_D; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI411_E; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI411_F; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI411_G; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI411_H;
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI411_I; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI411_J; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI411_K;
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQI411_L;
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQI411_M; 
	@FieldAnnotation(orderIndex=17) 
	public IntegerField txtQI411B; 
	@FieldAnnotation(orderIndex=18) 
	public IntegerField txtQI411C; 
	@FieldAnnotation(orderIndex=19) 
	public IntegerField txtQI411D; 
	@FieldAnnotation(orderIndex=20) 
	public IntegerField txtQI411E; 
	@FieldAnnotation(orderIndex=21) 
	public IntegerField txtQI411F; 
	
	public CheckBoxField chb409,chb410,chb410b,chb411b,chb411c,chb411d,chb411e,chb411f;
	
	String nombre_persona;
	
	CISECCION_04A c2seccion_04a; 
	CISECCION_02 c2seccion_2;
	CISECCION_01_03 ciseccion_1_3;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblprenatalcontroles,lblpregunta409,lblpregunta410,lblpregunta410b,lblpregunta411,
	lblpregunta411b,lblpregunta411c,lblpregunta411d,lblpregunta411e,lblpregunta411f,lblembarazocontroles,lblembarazocontroles411b,lblembarazocontroles411c,
	lblembarazocontroles411d,lblembarazocontroles411e,lblembarazocontroles411f,lblprenatalmeses,
	lblQI411A,lblQI411B,lblQI411C,lblQI411D,lblQI411E,lblQI411F,lblQI411G,lblQI411H,lblQI411I,lblQI411J,lblQI411K,lblQI411L,lblQI411M; 
	public ButtonComponent btnNoSabePrenatal,btnNoSabeEmbarazo,btnNoSabeEmbarazo411b,btnNoSabeEmbarazo411c,btnNoSabeEmbarazo411d,btnNoSabeEmbarazo411e,btnNoSabePrenatal409;
	public GridComponent2 gdPrenatales,gdEmbarazo,gdEmbarazo411b,gdEmbarazo411c,gdEmbarazo411d,gdEmbarazo411e,gdEmbarazo411f,
	gdPrenatales409,grid1,grid2,grid3,grid4,grid5,grid6,grid7,grid8,grid9;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI409","QI410","QI410_B","QI411_A","QI411_B","QI411_C","QI411_D","QI411_E","QI411_F","QI411_G","QI411_H","QI411_I","QI411_J","QI411_K","QI411_L","QI411_M","QI411B","QI411C","QI411D","QI411E","QI411F")};
	SeccionCapitulo[] seccionesCargado,seccionesCargado2,seccionesCargado3; 
	public boolean esPreguntaInicial;
	public TextField txtCabecera;

	public CISECCION_04AFragment_003() {} 
	public CISECCION_04AFragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI409","QI410","QI410_B","QI411_A","QI411_B","QI411_C","QI411_D","QI411_E","QI411_F","QI411_G","QI411_H","QI411_I","QI411_J","QI411_K","QI411_L","QI411_M","QI411B","QI411C","QI411D","QI411E","QI411F","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI220A","ID","HOGAR_ID","PERSONA_ID","QI212")};
		seccionesCargado3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI227","ID","HOGAR_ID","PERSONA_ID")};
		 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04a).textSize(21).centrar();
	    
		lblprenatalmeses  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi409_1).textSize(16).centrar();
		txtQI409=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		chb409=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi409_2, "1:0").size(altoComponente, 200);		
		chb409.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb409.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI409);
				txtQI410.requestFocus();
			}
			else {
				Util.lockView(getActivity(),false, txtQI409);	
			}
		}
		});
		
		gdPrenatales409 = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdPrenatales409.addComponent(lblprenatalmeses);
		gdPrenatales409.addComponent(txtQI409);
		gdPrenatales409.addComponent(chb409);
		
		lblpregunta409 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi409);
		lblpregunta410 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi410);
		lblpregunta410b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi410b);
		lblpregunta411 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04aqi411);
		
		
		lblQI411A=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 460).text(R.string.c2seccion_04aqi411_a);		
		rgQI411_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
				
		lblQI411B=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 460).text(R.string.c2seccion_04aqi411_b);	
		rgQI411_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
		
		lblQI411C=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 460).text(R.string.c2seccion_04aqi411_c);
		rgQI411_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
				
		lblQI411D=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 460).text(R.string.c2seccion_04aqi411_d);
		rgQI411_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI411dChangeValue");  
		
		lblQI411E=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 460).text(R.string.c2seccion_04aqi411_e);
		rgQI411_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI411eChangeValue");   
		
		lblQI411F=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_f);
		rgQI411_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
				
		lblQI411G=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_g);
		rgQI411_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI411gChangeValue");  
		
		
		lblQI411H=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_h);
		rgQI411_H=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI411hChangeValue");
		
		lblQI411I=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_i);
		rgQI411_I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI411hChangeValue");
		
		lblQI411J=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_j);
		rgQI411_J=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI411hChangeValue");
		
		lblQI411K=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_k);
		rgQI411_K=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI411hChangeValue");
		
		lblQI411L=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_l);
		rgQI411_L=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI411hChangeValue");
		
		lblQI411M=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 460).text(R.string.c2seccion_04aqi411_m);
		rgQI411_M=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04aqi411_radio1,R.string.c2seccion_04aqi411_radio2,R.string.c2seccion_04aqi411_radio3).size(altoComponente+10,280).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI411mChangeValue");
		
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI411A);
		grid1.addComponent(rgQI411_A);
		grid1.addComponent(lblQI411B);
		grid1.addComponent(rgQI411_B);
		grid1.addComponent(lblQI411C);
		grid1.addComponent(rgQI411_C);
		grid1.addComponent(lblQI411D);
		grid1.addComponent(rgQI411_D);
		grid1.addComponent(lblQI411E);
		grid1.addComponent(rgQI411_E);
		grid1.addComponent(lblQI411F);
		grid1.addComponent(rgQI411_F);
		grid1.addComponent(lblQI411G);
		grid1.addComponent(rgQI411_G);
		grid1.addComponent(lblQI411H);
		grid1.addComponent(rgQI411_H);
		
		grid1.addComponent(lblQI411I);
		grid1.addComponent(rgQI411_I);
		
		grid1.addComponent(lblQI411J);
		grid1.addComponent(rgQI411_J);
		
		grid1.addComponent(lblQI411K);
		grid1.addComponent(rgQI411_K);
		
		grid1.addComponent(lblQI411L);
		grid1.addComponent(rgQI411_L);
		
		grid1.addComponent(lblQI411M);
		grid1.addComponent(rgQI411_M);
	
		Spanned texto411b =Html.fromHtml("411B. �Cu�ntos meses de embarazo ten�a usted cuando le realizaron <b>el primer Examen de Orina?</b>");
		lblpregunta411b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta411b.setText(texto411b);
		
		Spanned texto411c =Html.fromHtml("411C. �Cu�ntos meses de embarazo ten�a usted cuando le realizaron <b>el primer Examen de Sangre?</b>");
		lblpregunta411c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta411c.setText(texto411c);
		
		Spanned texto411d =Html.fromHtml("411D. �Cu�ntos meses de embarazo ten�a usted cuando le realizaron <b>la primera prueba para descartar S�filis?</b>");
		lblpregunta411d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta411d.setText(texto411d);

		Spanned texto411e =Html.fromHtml("411E. �Cu�ntos meses de embarazo ten�a usted cuando le realizaron <b>la primera prueba para descartar VIH/SIDA?</b>");
		lblpregunta411e = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta411e.setText(texto411e);
		
		
		Spanned texto411f =Html.fromHtml("411F. �Cu�ntos meses de embarazo ten�a usted cuando le realizaron <b>la primera prueba para descartar Hepatitis B?</b>");
		lblpregunta411f = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta411f.setText(texto411f);
	
		
		txtQI410=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("ontxtQI410ChangeValue"); 
		txtQI410_B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);		
		txtQI411B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI411C=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI411D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQI411E=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		txtQI411F=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		
		
		lblprenatalcontroles  = new LabelComponent(this.getActivity()).size(altoComponente, 200).text(R.string.c2seccion_04aqi410_1).textSize(16).centrar();
		chb410=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi410_2, "1:0").size(altoComponente, 200);
		chb410.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb410.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI410);
				Util.lockView(getActivity(),false,txtQI410_B,chb410b);  
	    		q3.setVisibility(View.VISIBLE);
	    		txtQI410_B.requestFocus();
			}
			else {
				Util.lockView(getActivity(),false, txtQI410);	
			}
		}
		});
		
		gdPrenatales = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdPrenatales.addComponent(lblprenatalcontroles);
		gdPrenatales.addComponent(txtQI410);
		gdPrenatales.addComponent(chb410);
		
		lblembarazocontroles  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi410b_1).textSize(16).centrar();
		chb410b=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi410b_2, "1:0").size(altoComponente, 200);
		chb410b.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb410b.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI410_B);
			}
			else {
				Util.lockView(getActivity(),false, txtQI410_B);	
			}
		}
		});
		
		gdEmbarazo = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo.addComponent(lblembarazocontroles);
		gdEmbarazo.addComponent(txtQI410_B);
		gdEmbarazo.addComponent(chb410b);
		
		
		lblembarazocontroles411b  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi411b_1).textSize(16).centrar();
		chb411b=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi411b_2, "1:0").size(altoComponente, 200);
		chb411b.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb411b.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI411B);
			}
			else {
				Util.lockView(getActivity(),false, txtQI411B);	
			}
		}
		});
		
		gdEmbarazo411b = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo411b.addComponent(lblembarazocontroles411b);
		gdEmbarazo411b.addComponent(txtQI411B);
		gdEmbarazo411b.addComponent(chb411b);
		
		lblembarazocontroles411c  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi411c_1).textSize(16).centrar();
		chb411c=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi411c_2, "1:0").size(altoComponente, 200);
		chb411c.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb411c.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI411C);
			}
			else {
				Util.lockView(getActivity(),false, txtQI411C);	
			}
		}
		});
		
		gdEmbarazo411c = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo411c.addComponent(lblembarazocontroles411c);
		gdEmbarazo411c.addComponent(txtQI411C);
		gdEmbarazo411c.addComponent(chb411c);
		
		lblembarazocontroles411d  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi411d_1).textSize(16).centrar();
		chb411d=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi411d_2, "1:0").size(altoComponente, 200);
		chb411d.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb411d.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI411D);
			}
			else {
				Util.lockView(getActivity(),false, txtQI411D);	
			}
		}
		});
		
		gdEmbarazo411d = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo411d.addComponent(lblembarazocontroles411d);
		gdEmbarazo411d.addComponent(txtQI411D);
		gdEmbarazo411d.addComponent(chb411d);
		
		
		lblembarazocontroles411e  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi411e_1).textSize(16).centrar();
		chb411e=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi411e_2, "1:0").size(altoComponente, 200);
		chb411e.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb411e.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI411E);
			}
			else {
				Util.lockView(getActivity(),false, txtQI411E);	
			}
		}
		});
		
		gdEmbarazo411e = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo411e.addComponent(lblembarazocontroles411e);
		gdEmbarazo411e.addComponent(txtQI411E);
		gdEmbarazo411e.addComponent(chb411e);
		
		
		lblembarazocontroles411f  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04aqi411e_1).textSize(16).centrar();
		chb411f=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi411e_2, "1:0").size(altoComponente, 200);
		chb411f.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb411f.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI411F);
			}
			else {
				Util.lockView(getActivity(),false, txtQI411F);	
			}
		}
		});
		
		gdEmbarazo411f = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdEmbarazo411f.addComponent(lblembarazocontroles411f);
		gdEmbarazo411f.addComponent(txtQI411F);
		gdEmbarazo411f.addComponent(chb411f);
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera, lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta409,gdPrenatales409.component()); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta410,gdPrenatales.component()); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta410b,gdEmbarazo.component()); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411,grid1.component()); 
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411b,gdEmbarazo411b.component()); 
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411c,gdEmbarazo411c.component()); 
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411d,gdEmbarazo411d.component()); 
		q8 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411e,gdEmbarazo411e.component());
		q9 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta411f,gdEmbarazo411f.component());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8);
		form.addView(q9);
		
		q5.setVisibility(View.GONE);
		q6.setVisibility(View.GONE);
		q7.setVisibility(View.GONE);
		q8.setVisibility(View.GONE);
		q9.setVisibility(View.GONE);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04a); 

		if(c2seccion_04a!=null){
			
			if (c2seccion_04a.qi411_a!=null) {
				c2seccion_04a.qi411_a=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_a);
			}
			if (c2seccion_04a.qi411_b!=null) {
				c2seccion_04a.qi411_b=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_b);
			}
			if (c2seccion_04a.qi411_c!=null) {
				c2seccion_04a.qi411_c=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_c);
			}			
			if (c2seccion_04a.qi411_d!=null) {
				c2seccion_04a.qi411_d=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_d);
			}
			if (c2seccion_04a.qi411_e!=null) {
				c2seccion_04a.qi411_e=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_e);
			}
			if (c2seccion_04a.qi411_f!=null) {
				c2seccion_04a.qi411_f=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_f);
			}
			if (c2seccion_04a.qi411_g!=null) {
				c2seccion_04a.qi411_g=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_g);
			}
			if (c2seccion_04a.qi411_h!=null) {
				c2seccion_04a.qi411_h=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_h);
			}
			if (c2seccion_04a.qi411_i!=null) {
				c2seccion_04a.qi411_i=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_i);
			}
			if (c2seccion_04a.qi411_j!=null) {
				c2seccion_04a.qi411_j=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_j);
			}
			if (c2seccion_04a.qi411_k!=null) {
				c2seccion_04a.qi411_k=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_k);
			}
			if (c2seccion_04a.qi411_l!=null) {
				c2seccion_04a.qi411_l=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_l);
			}
			if (c2seccion_04a.qi411_m!=null) {
				c2seccion_04a.qi411_m=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_m);
			}
			
			
		}
		
		
		if(chb409.isChecked() ) {
			c2seccion_04a.qi409=98;			
		}
		
		if(chb410.isChecked() ) {
			c2seccion_04a.qi410=98;			
		}
		
		if(chb410b.isChecked() ) {
			c2seccion_04a.qi410_b=98;			
		}
		
		if(chb411b.isChecked() ) {
			c2seccion_04a.qi411b=98;			
		}
		
		if(chb411c.isChecked() ) {
			c2seccion_04a.qi411c=98;			
		}
		
		if(chb411d.isChecked() ) {
			c2seccion_04a.qi411d=98;			
		}
		
		if(chb411e.isChecked() ) {
			c2seccion_04a.qi411e=98;			
		}
		
		if(chb411f.isChecked() ) {
			c2seccion_04a.qi411f=98;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			if(!Util.esDiferente(c2seccion_04a.qi409, 98)){
				App.getInstance().getPersonaCuestionarioIndividual().qi409="NO SABE";
			}
			else{
				App.getInstance().getPersonaCuestionarioIndividual().qi409=c2seccion_04a.qi409.toString();				
			}
			if(!Util.esDiferente(c2seccion_04a.qi411_h, 1))
				App.getInstance().getPersonaCuestionarioIndividual().qi411_h="SI";
			if(!Util.esDiferente(c2seccion_04a.qi411_h, 2))
				App.getInstance().getPersonaCuestionarioIndividual().qi411_h="NO";
			if(!Util.esDiferente(c2seccion_04a.qi411_h, 8))
				App.getInstance().getPersonaCuestionarioIndividual().qi411_h="NO SABE";
		}
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		
//		rango(getActivity(), txtQI409, 0, 9); 
//		rango(getActivity(), txtQI410, 0, 20); 
//		rango(getActivity(), txtQI410_B, 0, 9);
//		rango(getActivity(), txtQI411B, 0, 9);
//		rango(getActivity(), txtQI411C, 0, 9);
//		rango(getActivity(), txtQI411D, 0, 9);
//		rango(getActivity(), txtQI411E, 0, 9);
		
		
		if(esPreguntaInicial) {
			if (Util.esVacio(c2seccion_04a.qi409) && !chb409.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI409"); 
				view = txtQI409; 
				error = true; 
				return false; 
			} 
			if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi409) && Util.esDiferente(c2seccion_04a.qi409,98)) {
				mensaje = "El valor ingresado est� fuera de rango"; 
				view = txtQI409; 
				error = true; 
				return false; 
			}
			
			if (c2seccion_2.qi220a!=null && c2seccion_04a.qi409!=null && (c2seccion_04a.qi409 > c2seccion_2.qi220a ) && c2seccion_04a.qi409 !=98 ) { 
				mensaje = "El valor ingresado debe ser menor o igual que meses de embarazo"; 
				view = txtQI409; 
				error = true; 
				return false; 
			} 
			
			
			if (Util.esVacio(c2seccion_04a.qi410) && !chb410.isChecked() ) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI410"); 
				view = txtQI410; 
				error = true; 
				return false; 
			} 
			if (!MyUtil.incluyeRango(0, 20, c2seccion_04a.qi410) && Util.esDiferente(c2seccion_04a.qi410,98) ) {
				mensaje = "El valor ingresado est� fuera de rango"; 
				view = txtQI410; 
				error = true; 
				return false; 
			}
			
			if(c2seccion_04a.qi410!=1) {
				if (Util.esVacio(c2seccion_04a.qi410_b) && !chb410b.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI410_B"); 
					view = txtQI410_B; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi410_b) && Util.esDiferente(c2seccion_04a.qi410_b,98) ) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI410_B; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_2.qi220a!=null && c2seccion_04a.qi410_b!=98 && (c2seccion_04a.qi410_b > c2seccion_2.qi220a ) && c2seccion_04a.qi410_b !=98 && c2seccion_2.qi220a !=98) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 220a"; 
					view = txtQI410_B; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi410_b!=98 && (c2seccion_04a.qi410_b < c2seccion_04a.qi409) && c2seccion_04a.qi410_b !=98 && c2seccion_04a.qi409 !=98) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI410_B; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04a.qi411_a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_A"); 
				view = rgQI411_A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_B"); 
				view = rgQI411_B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_c) ) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_C"); 
				view = rgQI411_C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_d) ) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_D"); 
				view = rgQI411_D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_e) ) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_E"); 
				view = rgQI411_E; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_F"); 
				view = rgQI411_F; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_G"); 
				view = rgQI411_G; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_h)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_H"); 
				view = rgQI411_H; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_i)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_I"); 
				view = rgQI411_I; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_j)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_J"); 
				view = rgQI411_J; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_k)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_K"); 
				view = rgQI411_K; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04a.qi411_l)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_L"); 
				view = rgQI411_L; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(c2seccion_04a.qi411_m)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI411_M"); 
				view = rgQI411_M; 
				error = true; 
				return false; 
			}
			
			if(c2seccion_04a.qi411_d!=null && c2seccion_04a.qi411_d==1) {
				if (Util.esVacio(c2seccion_04a.qi411b)  && !chb411b.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI411B"); 
					view = txtQI411B; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi411b) && Util.esDiferente(c2seccion_04a.qi411b,98)) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI411B; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi411b!=null && (c2seccion_04a.qi411b > c2seccion_04a.qi410_b ) && c2seccion_04a.qi411b !=98 ) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 410B"; 
					view = txtQI411B; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi411b!=null && (c2seccion_04a.qi411b < c2seccion_04a.qi409) && c2seccion_04a.qi411b !=98 && c2seccion_04a.qi409 !=98 ) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI411B; 
					error = true; 
					return false; 
				} 
			}
			
			if(c2seccion_04a.qi411_e!=null && c2seccion_04a.qi411_e==1) {
				if (Util.esVacio(c2seccion_04a.qi411c) && !chb411c.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI411C"); 
					view = txtQI411C; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi411c) && Util.esDiferente(c2seccion_04a.qi411c,98)) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI411C; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi411c!=null && (c2seccion_04a.qi411c > c2seccion_04a.qi410_b ) && c2seccion_04a.qi411c !=98  && c2seccion_04a.qi410_b !=98) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 410B"; 
					view = txtQI411C; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi411c!=null && (c2seccion_04a.qi411c < c2seccion_04a.qi409) && c2seccion_04a.qi411c !=98 && c2seccion_04a.qi409 !=98) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI411C; 
					error = true; 
					return false; 
				} 
			}
			
			if(c2seccion_04a.qi411_g!=null && c2seccion_04a.qi411_g==1) {
				if (Util.esVacio(c2seccion_04a.qi411d) && !chb411d.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI411D"); 
					view = txtQI411D; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi411d) && Util.esDiferente(c2seccion_04a.qi411d,98)) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI411D; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi411d!=null && (c2seccion_04a.qi411d > c2seccion_04a.qi410_b ) && c2seccion_04a.qi411d !=98 && c2seccion_04a.qi410_b !=98) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 410B"; 
					view = txtQI411D; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi411d!=null && (c2seccion_04a.qi411d < c2seccion_04a.qi409) && c2seccion_04a.qi411d !=98 && c2seccion_04a.qi409 !=98) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI411D; 
					error = true; 
					return false; 
				} 
			}
			
			if(c2seccion_04a.qi411_h!=null && c2seccion_04a.qi411_h==1) {
				if (Util.esVacio(c2seccion_04a.qi411e) && !chb411e.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI411E"); 
					view = txtQI411E; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi411e) && Util.esDiferente(c2seccion_04a.qi411e,98) ) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI411E; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi411e!=null && (c2seccion_04a.qi411e > c2seccion_04a.qi410_b ) && c2seccion_04a.qi411e !=98 && c2seccion_04a.qi410_b !=98 ) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 410B"; 
					view = txtQI411E; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi411e!=null && (c2seccion_04a.qi411e < c2seccion_04a.qi409) && c2seccion_04a.qi411e !=98 && c2seccion_04a.qi409 !=98) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI411E; 
					error = true; 
					return false; 
				} 
			}
			
			
			if(c2seccion_04a.qi411_m!=null && c2seccion_04a.qi411_m==1) {
				if (Util.esVacio(c2seccion_04a.qi411f) && !chb411f.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI411F"); 
					view = txtQI411F; 
					error = true; 
					return false; 
				} 
				if (!MyUtil.incluyeRango(0, 9, c2seccion_04a.qi411f) && Util.esDiferente(c2seccion_04a.qi411f,98) ) {
					mensaje = "El valor ingresado est� fuera de rango"; 
					view = txtQI411F; 
					error = true; 
					return false; 
				}
				
				if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi411f!=null && (c2seccion_04a.qi411f > c2seccion_04a.qi410_b ) && c2seccion_04a.qi411f !=98 && c2seccion_04a.qi410_b !=98 ) { 
					mensaje = "El valor ingresado debe ser menor o igual que el campo 410B"; 
					view = txtQI411F; 
					error = true; 
					return false; 
				} 
				
				if (c2seccion_04a.qi409!=null && c2seccion_04a.qi411f!=null && (c2seccion_04a.qi411f < c2seccion_04a.qi409) && c2seccion_04a.qi411f !=98 && c2seccion_04a.qi409 !=98) { 
					mensaje = "El valor ingresado debe ser mayor o igual que el campo 409"; 
					view = txtQI411F; 
					error = true; 
					return false; 
				} 
			}
			
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		c2seccion_04a = getCuestionarioService().getCISECCION_04A(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado); 
		
		c2seccion_2 = getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,App.getInstance().getNacimiento().qi212,seccionesCargado2);
		
		ciseccion_1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,seccionesCargado3);
		if(c2seccion_04a==null){ 
		  c2seccion_04a=new CISECCION_04A(); 
		  c2seccion_04a.id=App.getInstance().getNacimiento().persona_id; 
		  c2seccion_04a.hogar_id=App.getInstance().getNacimiento().hogar_id; 
		  c2seccion_04a.persona_id=App.getInstance().getNacimiento().persona_id;
		  c2seccion_04a.ninio_id=App.getInstance().getNacimiento().qi212;
		} 
		
		esPreguntaInicial = getCuestionarioService().esPrimerRegistroCap4a(App.getInstance().getNacimiento().id, App.getInstance().getNacimiento().hogar_id, App.getInstance().getNacimiento().persona_id,App.getInstance().getNacimiento().qi212);
		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_a!=null) {
			c2seccion_04a.qi411_a=c2seccion_04a.setConvertQi411a(c2seccion_04a.qi411_a);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_b!=null) {
			c2seccion_04a.qi411_b=c2seccion_04a.setConvertQi411b(c2seccion_04a.qi411_b);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_c!=null) {
			c2seccion_04a.qi411_c=c2seccion_04a.setConvertQi411c(c2seccion_04a.qi411_c);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_d!=null) {
			c2seccion_04a.qi411_d=c2seccion_04a.setConvertQi411d(c2seccion_04a.qi411_d);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_e!=null) {
			c2seccion_04a.qi411_e=c2seccion_04a.setConvertQi411e(c2seccion_04a.qi411_e);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_f!=null) {
			c2seccion_04a.qi411_f=c2seccion_04a.setConvertQi411f(c2seccion_04a.qi411_f);
		}		
		if (c2seccion_04a!=null && c2seccion_04a.qi411_g!=null) {
			c2seccion_04a.qi411_g=c2seccion_04a.setConvertQi411g(c2seccion_04a.qi411_g);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_h!=null) {
			c2seccion_04a.qi411_h=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_h);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_i!=null) {
			c2seccion_04a.qi411_i=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_i);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_j!=null) {
			c2seccion_04a.qi411_j=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_j);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_k!=null) {
			c2seccion_04a.qi411_k=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_k);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_l!=null) {
			c2seccion_04a.qi411_l=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_l);
		}
		if (c2seccion_04a!=null && c2seccion_04a.qi411_m!=null) {
			c2seccion_04a.qi411_m=c2seccion_04a.setConvertQi411h(c2seccion_04a.qi411_m);
		}
		
		nombre_persona = App.getInstance().getNacimiento().qi212_nom;
		renombrarEtiquetas();
		entityToUI(c2seccion_04a); 
		
		if(c2seccion_04a.qi409!=null && c2seccion_04a.qi409==98) {
			chb409.setChecked(true);
			txtQI409.setText("");
		}
		
		if(c2seccion_04a.qi410!=null && c2seccion_04a.qi410==98) {
			chb410.setChecked(true);
			txtQI410.setText("");
		}
		
		if(c2seccion_04a.qi410_b!=null && c2seccion_04a.qi410_b==98) {
			chb410b.setChecked(true);
			txtQI410_B.setText("");
		}
		
		if(c2seccion_04a.qi411b!=null && c2seccion_04a.qi411b==98) {
			chb411b.setChecked(true);
			txtQI411B.setText("");
		}
		
		if(c2seccion_04a.qi411c!=null && c2seccion_04a.qi411c==98) {
			chb411c.setChecked(true);
			txtQI411C.setText("");
		}
		
		if(c2seccion_04a.qi411d!=null && c2seccion_04a.qi411d==98) {
			chb411d.setChecked(true);
			txtQI411D.setText("");
		}
		
		if(c2seccion_04a.qi411e!=null && c2seccion_04a.qi411e==98) {
			chb411e.setChecked(true);
			txtQI411E.setText("");
		}
		if(c2seccion_04a.qi411f!=null && c2seccion_04a.qi411f==98) {
			chb411f.setChecked(true);
			txtQI411F.setText("");
		}
		inicio(); 
    } 
    private void inicio() {
    	
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(!esPreguntaInicial){    	
    		ocultarCampos();
    	}
    	else {
    		
    		if(App.getInstance().getSeccion04A()!=null && App.getInstance().getSeccion04A().qi407_y!=null && App.getInstance().getSeccion04A().qi407_y==1) {
    			ocultarCampos();
    		}
    		else {
    			mostrarCampos();
    			ontxtQI410ChangeValue();
    			if (c2seccion_04a!=null && c2seccion_04a.qi411_d!=null) {
    		    	onqrgQI411dChangeValue();
    		    }
    		    if (c2seccion_04a!=null && c2seccion_04a.qi411_e!=null) {
    		    	onqrgQI411eChangeValue();
    		    }
    		    if (c2seccion_04a!=null && c2seccion_04a.qi411_g!=null) {
    		    	onqrgQI411gChangeValue();
    		    }
    		    if (c2seccion_04a!=null && c2seccion_04a.qi411_h!=null) {
    		    	onqrgQI411hChangeValue();
    		    }
    		    if (c2seccion_04a!=null && c2seccion_04a.qi411_m!=null) {
    		    	onqrgQI411mChangeValue();
    		    }
    		}
    	
        	if (c2seccion_04a.qi409!=null && c2seccion_04a.qi409==98) { Util.cleanAndLockView(getActivity(),txtQI409); } else{ Util.lockView(getActivity(),false,txtQI409);}
        	if (c2seccion_04a.qi410!=null && c2seccion_04a.qi410==98) { Util.cleanAndLockView(getActivity(),txtQI410); } else{ Util.lockView(getActivity(),false,txtQI410); }
    		if (c2seccion_04a.qi410_b!=null && c2seccion_04a.qi410_b==98) { Util.cleanAndLockView(getActivity(),txtQI410_B); } else{ Util.lockView(getActivity(),false,txtQI410_B);}
        	
        	if (c2seccion_04a.qi411b!=null && c2seccion_04a.qi411b==98) { Util.cleanAndLockView(getActivity(),txtQI411B); } else{ Util.lockView(getActivity(),false,txtQI411B); }
        	if (c2seccion_04a.qi411c!=null && c2seccion_04a.qi411c==98) { Util.cleanAndLockView(getActivity(),txtQI411C); } else{ Util.lockView(getActivity(),false,txtQI411C); }
        	if (c2seccion_04a.qi411d!=null && c2seccion_04a.qi411d==98) { Util.cleanAndLockView(getActivity(),txtQI411D); } else{ Util.lockView(getActivity(),false,txtQI411D); }
        	if (c2seccion_04a.qi411e!=null && c2seccion_04a.qi411e==98) { Util.cleanAndLockView(getActivity(),txtQI411E); } else{ Util.lockView(getActivity(),false,txtQI411E); }
        	if (c2seccion_04a.qi411f!=null && c2seccion_04a.qi411f==98) { Util.cleanAndLockView(getActivity(),txtQI411F); } else{ Util.lockView(getActivity(),false,txtQI411F); }
    	}
    }
    
    public void ocultarCampos() {
    	Util.cleanAndLockView(getActivity(),txtQI409,chb409,txtQI410,chb410, txtQI410_B,chb410b,rgQI411_A,rgQI411_B,rgQI411_C,rgQI411_D,rgQI411_E,rgQI411_F,
    	rgQI411_G,rgQI411_H,txtQI411B,chb411b,txtQI411C,chb411c,txtQI411D,chb411d,txtQI411E,chb411e,txtQI411F,chb411f);
    	q1.setVisibility(View.GONE);
    	q2.setVisibility(View.GONE);
    	q3.setVisibility(View.GONE);
    	q4.setVisibility(View.GONE);
    	q5.setVisibility(View.GONE);
    	q6.setVisibility(View.GONE);
    	q7.setVisibility(View.GONE);
    	q8.setVisibility(View.GONE);
    	q9.setVisibility(View.GONE);
    	MyUtil.LiberarMemoria();    	
    }
    
    public void mostrarCampos() {
    	Util.lockView(getActivity(),false,txtQI409,chb409,txtQI410,chb410, txtQI410_B,chb410b,rgQI411_A,rgQI411_B,rgQI411_C,rgQI411_D,rgQI411_E,rgQI411_F,
    	rgQI411_G,rgQI411_H,txtQI411B,chb411b,txtQI411C,chb411c,txtQI411D,chb411d,txtQI411E,chb411e,txtQI411F,chb411f);
    	q1.setVisibility(View.VISIBLE);
    	q2.setVisibility(View.VISIBLE);
    	q3.setVisibility(View.VISIBLE);
    	q4.setVisibility(View.VISIBLE);
    	q5.setVisibility(View.VISIBLE);
    	q6.setVisibility(View.VISIBLE);
    	q7.setVisibility(View.VISIBLE);
    	q8.setVisibility(View.VISIBLE);
    	q9.setVisibility(View.VISIBLE);
    }
    

	public void ontxtQI410ChangeValue() {  	
    	if (txtQI410.getText()!=null && txtQI410.getText().toString().equals("1")) {    		
    		Util.cleanAndLockView(getActivity(),txtQI410_B,chb410b);
    		q3.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    		rgQI411_A.requestFocus();
    	} 
    	else {
    		Util.lockView(getActivity(),false,txtQI410_B,chb410b);  
    		q3.setVisibility(View.VISIBLE);
    		txtQI410_B.requestFocus();
    		
    	}	
    }
	
	public void onqrgQI411dChangeValue() {  
		
    	if(rgQI411_D.getValue()!=null) {
			if (rgQI411_D.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI411B,chb411b);  
				q5.setVisibility(View.VISIBLE);
				rgQI411_E.requestFocus();
			} 
			else {
				Util.cleanAndLockView(getActivity(),txtQI411B,chb411b);
				q5.setVisibility(View.GONE);
				rgQI411_E.requestFocus();
				MyUtil.LiberarMemoria();
			}			
    	}
    }
	
	public void onqrgQI411eChangeValue() {  	
		if(rgQI411_E.getValue()!=null) {
			if (rgQI411_E.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI411C,chb411c);  
	    		q6.setVisibility(View.VISIBLE);
	    		rgQI411_F.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI411C,chb411c);
	    		q6.setVisibility(View.GONE);
	    		rgQI411_F.requestFocus();
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI411gChangeValue() {  	
		if(rgQI411_G.getValue()!=null) {
			if (rgQI411_G.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI411D,chb411d);  
	    		q7.setVisibility(View.VISIBLE);
	    		rgQI411_H.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI411D,chb411d);
	    		q7.setVisibility(View.GONE);
	    		rgQI411_H.requestFocus();
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI411hChangeValue() {  	
		if(rgQI411_H.getValue()!=null) {
			if (rgQI411_H.getValue().toString().equals("1")) {    		
	    		Util.lockView(getActivity(),false,txtQI411E,chb411e);  
	    		q8.setVisibility(View.VISIBLE);
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI411E,chb411e);
	    		q8.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI411mChangeValue() {  	
		if(rgQI411_M.getValue()!=null) {
			if (rgQI411_M.getValue().toString().equals("1")) {    		
	    		Util.lockView(getActivity(),false,txtQI411F,chb411f);  
	    		q9.setVisibility(View.VISIBLE);
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI411F,chb411f);
	    		q9.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	public void renombrarEtiquetas(){	
		lblpregunta410.text(R.string.c2seccion_04aqi410);
    	lblpregunta410.setText(lblpregunta410.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI411_A.readOnly();
			rgQI411_B.readOnly();
			rgQI411_C.readOnly();
			rgQI411_D.readOnly();
			rgQI411_E.readOnly();
			rgQI411_F.readOnly();
			rgQI411_G.readOnly();
			rgQI411_H.readOnly();
			rgQI411_I.readOnly();
			rgQI411_J.readOnly();
			rgQI411_F.readOnly();
			rgQI411_L.readOnly();
			rgQI411_M.readOnly();
			txtQI409.readOnly();
			txtQI410.readOnly();
			txtQI410_B.readOnly();
			txtQI411B.readOnly();
			txtQI411C.readOnly();
			txtQI411D.readOnly();
			txtQI411E.readOnly();
			txtQI411F.readOnly();
			chb409.readOnly();
			chb410.readOnly();
			chb410b.readOnly();
			chb411b.readOnly();
			chb411c.readOnly();
			chb411d.readOnly();
			chb411e.readOnly();
			chb411f.readOnly();
			
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04a); 
		
		if(c2seccion_04a!=null){
			
			if (c2seccion_04a.qi411_a!=null) {
				c2seccion_04a.qi411_a=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_a);
			}
			if (c2seccion_04a.qi411_b!=null) {
				c2seccion_04a.qi411_b=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_b);
			}
			if (c2seccion_04a.qi411_c!=null) {
				c2seccion_04a.qi411_c=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_c);
			}			
			if (c2seccion_04a.qi411_d!=null) {
				c2seccion_04a.qi411_d=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_d);
			}
			if (c2seccion_04a.qi411_e!=null) {
				c2seccion_04a.qi411_e=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_e);
			}
			if (c2seccion_04a.qi411_f!=null) {
				c2seccion_04a.qi411_f=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_f);
			}
			if (c2seccion_04a.qi411_g!=null) {
				c2seccion_04a.qi411_g=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_g);
			}
			if (c2seccion_04a.qi411_h!=null) {
				c2seccion_04a.qi411_h=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_h);
			}
			if (c2seccion_04a.qi411_i!=null) {
				c2seccion_04a.qi411_i=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_i);
			}
			if (c2seccion_04a.qi411_j!=null) {
				c2seccion_04a.qi411_j=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_j);
			}
			if (c2seccion_04a.qi411_k!=null) {
				c2seccion_04a.qi411_k=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_k);
			}
			if (c2seccion_04a.qi411_l!=null) {
				c2seccion_04a.qi411_l=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_l);
			}
			if (c2seccion_04a.qi411_m!=null) {
				c2seccion_04a.qi411_m=c2seccion_04a.getConvertQi411d(c2seccion_04a.qi411_m);
			}
			
			
		}
		
		
		if(chb409.isChecked() ) {
			c2seccion_04a.qi409=98;			
		}
		
		if(chb410.isChecked() ) {
			c2seccion_04a.qi410=98;			
		}
		
		if(chb410b.isChecked() ) {
			c2seccion_04a.qi410_b=98;			
		}
		
		if(chb411b.isChecked() ) {
			c2seccion_04a.qi411b=98;			
		}
		
		if(chb411c.isChecked() ) {
			c2seccion_04a.qi411c=98;			
		}
		
		if(chb411d.isChecked() ) {
			c2seccion_04a.qi411d=98;			
		}
		
		if(chb411e.isChecked() ) {
			c2seccion_04a.qi411e=98;			
		}
		if(chb411f.isChecked() ) {
			c2seccion_04a.qi411f=98;			
		}
		

		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04a,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL;
	} 
	
} 
