package gob.inei.endes2024.fragment;

import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.TableComponent.ALIGN;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.CapturadorGPS;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.asyncTask.ExportData;
import gob.inei.endes2024.asyncTask.UploadFile;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.controller.Exportacion;
import gob.inei.endes2024.model.CISECCION_04;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.SegmentacionService;
import gob.inei.endes2024.service.UbigeoService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import paul.arian.fileselector.FileSelectionActivity;
import paul.arian.fileselector.FolderSelectionActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import gob.inei.dnce.components.IntegerField;

public class ExportacionFragment extends FragmentForm implements Respondible{

	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERIODO;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnCONGLOMERADO;
	@FieldAnnotation(orderIndex = 3)
	public SpinnerField spnPERIODO2;
	
	public TextField vivienda;
	
	private String TAG = ExportacionFragment.this.getClass().getSimpleName();
	private static enum PROCESS{EXPORTAR,VALIDAR,SERVER};
	private DialogComponent dialog;
	public static SegmentacionFiltro PERIODO;
	public static SegmentacionFiltro ANIO;
	public static SegmentacionFiltro MES;
	public static SegmentacionFiltro CONGLOMERADO;
	private LabelComponent lblViviendas, lblHogares , lblTitulo2;
	private List<SegmentacionFiltro> exportables;
	private TableComponent tcMarco;
	private LabelComponent lblTitulo, lblperiodo, lblconglomerado;
	private UbigeoService ubigeoService;
	private GridComponent2 grid1;
	private MarcoService service;
	public Integer tselv;
	public List<SegmentacionFiltro> viviendasexportables,viviendasexportadasavalidar;
	private List<Marco> marcos;
	private List<Hogar> hogares;
	private SegmentacionService segmentacionService;
	private HogarService hogarService;
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	private Seccion01Service personaService;
	 private CapturadorGPS tracker;
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesGrabado;
	private List<ViviendaSeleccionada> viviendaseleccionada;
	private ButtonComponent btnExportar, btnUpLoadFile;
	private String ruta;
	private PROCESS accionp;
	List<SegmentacionFiltro> lista;
	int serverResponseCode = 0;
	private ArrayList<File> uploadFiles = new ArrayList<File>();
	private boolean validarSupervisora = false;
	public ExportacionFragment parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID") };
		return rootView;
	}


	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(lblTitulo);
		LinearLayout q1 = createQuestionSection(0, Gravity.CENTER,LinearLayout.HORIZONTAL, grid1.component());
		LinearLayout q5 = createQuestionSection(q1);
		LinearLayout q2 = createQuestionSection(lblViviendas,tcMarco.getTableView());
		LinearLayout q3 = createButtonSection(btnExportar,btnUpLoadFile);		
		LinearLayout q4 = createQuestionSection(lblTitulo2, vivienda);
		//LinearLayout q6 = createQuestionSection(spnPERIODO2);
		
		tcMarco.setSelectorData(R.drawable.selector_sombra_lista);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q5);
		form.addView(q3);
		form.addView(q2);
		form.addView(q4);
		//form.addView(q6);

		return contenedor;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_exportacion).size(altoComponente, MATCH_PARENT).centrar().textSize(22);
		lblViviendas = new LabelComponent(getActivity(),App.ESTILO).text(R.string.conglomerados).size(50, MATCH_PARENT).centrar().textSize(20);
		lblHogares = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_hogares).size(50, MATCH_PARENT).centrar().textSize(20);
		
		lblperiodo = new LabelComponent(getActivity()).text(R.string.titulo_periodo).size(altoComponente+15, 180).centrar().textSize(17).colorFondo(color.azulacero).negrita().colorTexto(color.WhiteSmoke);
		spnPERIODO = new SpinnerField(getActivity()).size(altoComponente + 15,250).callback("onPeriodoChangeValue");
		//spnPERIODO2 = new SpinnerField(getActivity()).size(altoComponente + 15,250).callback("onPeriodoChangeValue2");
		lblTitulo2=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.titulo_export_vivienda).textSize(22).centrar().negrita().size(altoComponente+20, MATCH_PARENT);
		
		vivienda=new TextField(this.getActivity()).size(altoComponente, 450).maxLength(300).centrar().negrita();
		
		lblconglomerado = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_conglomerado).size(50, MATCH_PARENT).centrar().textSize(16);
		btnExportar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnTablet).size(200, 55);
		btnExportar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				exportar();
			}
		});
		btnUpLoadFile = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnUploadFile).size(200, 55);
		String codigo = App.getInstance().getUsuario().cargo_id.toString();

//		if(!codigo.equals("22"))
//			btnUpLoadFile.setVisibility(0x00000004);
//		else{
			btnUpLoadFile.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					server();
				}
			});
//		}
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(400, 770).headerHeight(70).headerTextSize(18).dataColumHeight(60);	
//		   }
//		else{
			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(700, 770).dataColumHeight(40).headerHeight(45).headerTextSize(15);
//			}
		tcMarco.addHeader(R.string.m_resultado_viv , 0.15f, CheckBoxField.class,ALIGN.CENTER);
		tcMarco.addHeader(R.string.conglomerados, 0.15f, ALIGN.CENTER);
		tcMarco.addCallback(0, "tcMarcoChangeValue");
		
		
		
		spnCONGLOMERADO = new SpinnerField(getActivity()).size(altoComponente + 15, 250).callback("onConglomeradoChangeValue");
		MyUtil.llenarPeriodo(getActivity(), getSegmentacionService(),spnPERIODO);
		
		grid1 = new GridComponent2(getActivity(),0,2);
		grid1.addComponent(lblperiodo);
		grid1.addComponent(spnPERIODO);

	}

	private void recargarLista() {
		tcMarco.setData(viviendaseleccionada,"seleccionada","getNombre");
		tcMarco.reloadData();
		registerForContextMenu(tcMarco.getListView());
	}

	public void onPeriodoChangeValue(FieldComponent component) {
		
		Log.e("prueba clcik", "ggg");
		
		SegmentacionFiltro periodo = (SegmentacionFiltro) component.getValue();
		lista = getSegmentacionService().getConglomerados(periodo.id);
		viviendaseleccionada = new ArrayList<ExportacionFragment.ViviendaSeleccionada>();
		Integer contador=0;
		for(SegmentacionFiltro m:lista){
			contador++;
			if(Util.esDiferente(contador, 1) && getSegmentacionService().getConglomeradoTieneAsignado(m.nombre)){
				viviendaseleccionada.add(new ViviendaSeleccionada(0, m));
			}
		}
		recargarLista();
	}

	@Override
	public void onCancel() {
		
	}

	@Override
	public void onAccept() {
		if(accionp == null) return;
		switch (accionp) {
		
		case EXPORTAR:
			Log.e("num vivienda", vivienda.getText().toString().length()+"ff");
			if(vivienda.getText().toString().length() == 0){
				//ToastMessage.msgBox(this.getActivity(), "Se exportar�n todas las viviendas", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}else{
				ToastMessage.msgBox(this.getActivity(), "S�lo se exportar� la vivienda : "+vivienda.getText().toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}
			
			//escribirEnTablet(vivienda.toString());
			escribirEnTablet(vivienda.getText().toString());
			
			//escribirEnTablet();
			
			break;
		
		case VALIDAR:
			if(viviendasexportables.size()>0){
				//Log.e("viviendasexpor", viviendasexportables.get(1).nombre);
				accionp = PROCESS.EXPORTAR;
				seleccionarRuta();
			}
			break;
			
		case SERVER:			
			String codigo = App.getInstance().getUsuario().cargo_id.toString();
			String usuario = App.getInstance().getUsuario().usuario;
            
			UploadFile uploadFile = new UploadFile(this,"Subiendo archivos al servidor.", usuario);
			ArrayList<File> files =  (ArrayList<File>) cargarArchivos(viviendasexportables); // (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
			uploadFile.setFiles(files);
			uploadFile.execute();
			break;
			
		default:		
			break;
		}
	}	
	
	private void escribirEnTablet(String n) {
		//Log.e("dd", "dddggg"+viviendasexportables.get(2).nombre);
		Exportacion r = new Exportacion(this, ruta, "Exportando informaci�n. ", n);
		r.setRegistros(viviendasexportables);
		r.execute();
	}
	@Override
	public boolean grabar() {
		return true;
	}
	public void tcMarcoChangeValue(Object entity,Integer row,Integer opcion){
		if (entity != null) {
			ViviendaSeleccionada tmp = (ViviendaSeleccionada) entity;
			tmp.seleccionada = opcion;
		}
	}

	private CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	@Override
	public void cargarDatos() {
		App.getInstance().setMarco(null);
		App.getInstance().setHogar(null);
		if (PERIODO != null) {
			spnPERIODO.setSelectionKey(PERIODO.id);
			onPeriodoChangeValue(spnPERIODO);
		}
		if (CONGLOMERADO != null) {
			spnCONGLOMERADO.setSelectionKey(CONGLOMERADO.id);
		}

		((CuestionarioFragmentActivity) parent).setTitulo(null);
		((CuestionarioFragmentActivity) parent).setSubTitulo(null);
	}

	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(getActivity());
		}
		return service;
	}

	public SegmentacionService getSegmentacionService() {
		if (segmentacionService == null) {
			segmentacionService = SegmentacionService.getInstance(getActivity());
		}
		return segmentacionService;
	}

	public UbigeoService getUbigeoService() {
		if (ubigeoService == null) {
			ubigeoService = UbigeoService.getInstance(getActivity());
		}
		return ubigeoService;
	}

	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	public HogarService getHogarService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}
	private void exportar() {

		DialogComponent dlg = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.NEUTRAL, 
				getResources().getString(R.string.app_name), "Seleccione alg�n item.");
		 viviendasexportables = new ArrayList<SegmentacionFiltro>();
		 
		 viviendasexportadasavalidar = new ArrayList<SegmentacionFiltro>();
		 if(viviendaseleccionada==null){
			 return;
		}
		for (ViviendaSeleccionada mco: viviendaseleccionada) {
			if(!Util.esDiferente(mco.seleccionada, 1)){
				viviendasexportadasavalidar.add(mco.marco);
			}
		}
		if(viviendasexportadasavalidar.size()==0){
			dlg.showDialog();
			return;
		}
		else{
			String cadena="";
			String mensajegeneral="";
						
			for(SegmentacionFiltro detalle:viviendasexportadasavalidar){
				cadena=ValidaciondeViviendasaExportar(detalle);
				if(cadena.equals("completo")){
					viviendasexportables.add(detalle);
					
				}
				else{
					mensajegeneral="Conglomerado: "+detalle.nombre+" "+cadena+"\n"+mensajegeneral;
					viviendasexportables.add(detalle);
				}
				cadena="";
			}
			if(mensajegeneral.toString().length()>0){
				DialogComponent dialogo = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, 
						getResources().getString(R.string.app_name),mensajegeneral);
				dialogo.showDialog();
				
				accionp=PROCESS.VALIDAR;
			}
			else{
				accionp = PROCESS.EXPORTAR;
				seleccionarRuta();
			}
		}
	}
	
	private void server(){
		
		DialogComponent dlg = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.NEUTRAL, getResources().getString(R.string.app_name), "Seleccione alg�n item.");
		viviendasexportables = new ArrayList<SegmentacionFiltro>();
		viviendasexportadasavalidar = new ArrayList<SegmentacionFiltro>();
		if(viviendaseleccionada==null) return;
		for (ViviendaSeleccionada mco: viviendaseleccionada) {
			if(!Util.esDiferente(mco.seleccionada, 1))
				viviendasexportadasavalidar.add(mco.marco);	
		}
		
		if(viviendasexportadasavalidar.size()==0) {
				dlg.showDialog();
				return;
		}
		
		else{
			
			String cadena="";
			String mensajegeneral="";
			for(SegmentacionFiltro detalle:viviendasexportadasavalidar){
				cadena=ValidaciondeViviendasaExportar(detalle);
				if(cadena.equals("completo")){
					viviendasexportables.add(detalle);
				}
				else{
					mensajegeneral="Conglomerado: "+detalle.nombre+" "+cadena+"\n"+mensajegeneral;
					viviendasexportables.add(detalle);
				}
				cadena="";
			}
			if(mensajegeneral.toString().length() > 0){
				DialogComponent dialogo = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, 
						getResources().getString(R.string.app_name),mensajegeneral +"\n" + "Desea subir archivos al servidor ?");
				dialogo.showDialog();
				
				accionp = PROCESS.SERVER;
				
			}
			else{
				DialogComponent dialogo = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, 
						getResources().getString(R.string.app_name),"Desea subir archivos al servidor ?");
				dialogo.showDialog();
				accionp = PROCESS.SERVER;
			}
		}
	
	
        /*String ruta = App.RUTA_BASE + "/backups/";
        File directorio = new File(ruta);
        if (!directorio.exists()) {
               directorio.mkdirs();
        }
        Intent intent = new Intent(this.getActivity(), FileSelectionActivity.class);
        intent.putExtra(FileSelectionActivity.START_FOLDER, ruta);
        intent.putExtra("FILTER_EXTENSION", new String[] { "zip" });*/
        //startActivityForResult(new Intent(this.getActivity(), FileSelectionActivity.class), 1);
	}
	
	public ArrayList<File> cargarArchivos(List<SegmentacionFiltro> viviendasexportables){		
		ArrayList<File> files = new ArrayList<File>();		
		for (SegmentacionFiltro r : viviendasexportables) {
			 //Calendar fecha = new GregorianCalendar();
			 File file = new File(App.RUTA_ENVIO_SERVIDOR + r.nombre + "/" + App.ANIOPORDEFECTOSUPERIOR + r.nombre +App.getInstance().getUsuario().usuario + ".zip");
			 files.add(file);
		}
		return files;
	}
	public String ValidaciondeViviendasaExportar(SegmentacionFiltro conglomerado){
		boolean flag=false;
		String mensaje="completo";
			marcos= getService().getMarco(Integer.parseInt(spnPERIODO.getSelectedItemKey().toString()),conglomerado.nombre);
			//Log.e("prueba", "ddddini"+marcos.size()); 
			//marcos.remove(7);
			//Log.e("prueba", "ddddfin"+marcos.size());
			for(Marco m:marcos){
				if(!mensaje.equals("completo")){
					break;
				}
				if(m.id!=null){
					flag = getCuestionarioService().getExisteCaratulaDelaVivienda(m.id);
					if(!flag){
						mensaje="La Vivienda: "+m.nselv+" Falta aperturar";
						break;
					}
				}
				if(m.resviv!=null){
					flag=getCuestionarioService().getViviendaTieneGPS(m.id);
					if(!flag){
						mensaje="La Vivienda: "+m.nselv+" no tiene punto GPS";
						break;
					}
					hogares = getHogarService().getHogares(m.id, seccionesCargado);
					for(Hogar h: hogares){
						if(getCuestionarioService().getResultadodelaUltimaVisitaHogar(h.id, h.hogar_id)){
							flag=getCuestionarioService().getExisteResultadodelasVisitas(h.id, h.hogar_id);
							if(!flag){
								mensaje=" Falta resultado a la visita de la Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getHogarCompletado(h.id,h.hogar_id);
							if(!flag){
								mensaje="Falta Completar Hogar Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag =getPersonaService().TodoLosMiembrosDelhogarCompletados(h.id,h.hogar_id);
							if(!flag){
								mensaje="Completar Cuestionario para todo los miembros del hogar la Vivienda: "+m.nselv+" Hogar:"+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getSeccion04_05Completado(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta completa Secci�n 04 o Secci�n 05 Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getVisitaSaludCompletada(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta aperturar visita de salud existe un seleccionado en Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getExisteCuestionarioDeSalud(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta Completar Cuestionario de Salud Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getSeccion08Completado(h.id,h.hogar_id);
							if(!flag){
								mensaje="Falta completar secci�n 08 salud Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag = getPersonaService().TodasLasMefCompletadas(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta poner resultado a alguna mef en la vivienda "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag= getCuestionarioService().getExisteAlgunHogarquefalteResponderPregunta224(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta poner resultado en la pregunta 224 secci�n 6 del cuetionario del hogar\nVivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							List<CISECCION_04B> ninios = getCuestionarioService().getNiniosVivosYquetienenLatarjetadevacunacion(h.id, h.hogar_id);
							String concatenarcontrol ="";
							String concatenarvacuna ="";
							if(ninios.size()>0){
								for (CISECCION_04B c4b : ninios) {
									if(!getCuestionarioService().getTieneAlMenosUnControl(c4b.id, c4b.hogar_id, c4b.persona_id, c4b.ninio_id)){
										concatenarcontrol= concatenarcontrol+" MEF: "+c4b.persona_id+" ninio orden: "+c4b.ninio_id+"\n"; 
									}
									if(!getCuestionarioService().getTieneAlMenosUnaVacuna(c4b.id, c4b.hogar_id, c4b.persona_id, c4b.ninio_id)){
										concatenarvacuna= concatenarvacuna+" MEF: "+c4b.persona_id+ " ninio orden: "+c4b.ninio_id+"\n";
									}
								}
							}
							if(!concatenarcontrol.trim().isEmpty()){
								mensaje="Falta ingresar controles en la vivienda "+m.nselv+" Hogar: "+h.hogar_id+" para las siguientes:\n "+concatenarcontrol;
								break;
							}
							if(!concatenarvacuna.trim().isEmpty()){
								mensaje="Falta ingresar vacunas en la vivienda "+m.nselv+" Hogar: "+h.hogar_id+" para las siguientes:\n "+concatenarvacuna;
								break;
							}
							if(flag)
								mensaje="completo";
						}
						else{
							flag=getCuestionarioService().getExisteResultadodelasVisitas(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta resultado a la visita de la Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							if(flag){
								mensaje="completo";
							}
						}
							
					}
				}
				else{
					mensaje="completo";
				}
			}
	 return mensaje;
	}
	public static class ViviendaSeleccionada implements IDetailEntityComponent {
		public Integer seleccionada=null;
		private SegmentacionFiltro marco=null;
		public ViviendaSeleccionada(Integer seleccionada,SegmentacionFiltro marco){
			super();
			this.seleccionada=seleccionada;
			this.marco=marco;
		}
		@Override
		public void cleanEntity() {
		}

		@Override
		public boolean isTitle() {
			// TODO Auto-generated method stub
			return false;
		}
		public String getNombre(){
			return marco.nombre;
		}
			
	}
	private static int REQUEST_CODE_PICK_FOLDER = 1;
	private void seleccionarRuta() {
		ruta = App.RUTA_BASE + "/backups/";
		File directorio = new File(ruta);
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		Intent intent = new Intent(this.getActivity(), FolderSelectionActivity.class);
		intent.putExtra(FolderSelectionActivity.START_FOLDER, ruta);
		startActivityForResult(intent, REQUEST_CODE_PICK_FOLDER);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode != Activity.RESULT_OK ) return;
		
		if(accionp == PROCESS.EXPORTAR) {
			File carpeta = (File) data.getExtras().getSerializable(FolderSelectionActivity.FILES_TO_UPLOAD);
			ruta = carpeta.getAbsolutePath();
			DialogComponent dlg = new DialogComponent(getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"Esta seguro de guardar los archivos en: "+ ruta+"?");
			dlg.showDialog();
		}		
		else {
			if(accionp == PROCESS.SERVER){				
				String dni = App.getInstance().getUsuario().dni;
				UploadFile uploadFile = new UploadFile(this,"Subiendo archivos al servidor.", dni);
				ArrayList<File> files = uploadFiles; // (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
				uploadFile.setFiles(files);
				uploadFile.execute("");
		    }
		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}