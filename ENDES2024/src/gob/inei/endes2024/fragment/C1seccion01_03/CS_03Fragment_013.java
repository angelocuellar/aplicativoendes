package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_03Fragment_013 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS301;	
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS302; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS302A1;	
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS302A2;	
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS303; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQS303_O; 
	Salud salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblsectorprivado,lblhospital,lblong,lblcampa,lblsectorpublico,lblP301,lblP301_extra,lblP302,lblP302_extra,lblP303,lblvacio; 
	public LinearLayout q0; 
	public LinearLayout q1; 
	public LinearLayout q2; 
	public LinearLayout q3;
	public LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	private boolean s03_301=false;
	
	public GridComponent2 gridPreguntas302;
	private Seccion01Service seccion01service; 
	public Seccion01 seleccionado;
	public boolean mayor_50=true;
	public CS_03Fragment_013() {} 
	public CS_03Fragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() {
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo_1).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblP301 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs301).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP301_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs301_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		rgQS301=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs301_1,R.string.cap01_03qs301_2,R.string.cap01_03qs301_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS301ChangeValue"); //onqrgQS301ChangeValue - salto_rgQS301
		
		lblP302 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs302).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();		
		lblP302_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs302_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		rgQS302=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs302_1,R.string.cap01_03qs302_2,R.string.cap01_03qs302_3).size(altoComponente*3+45,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS302ChangeValue"); //salto_rgQS302
		txtQS302A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS302A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas302=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas302.addComponent(rgQS302,1,3);		
		gridPreguntas302.addComponent(txtQS302A1);
		gridPreguntas302.addComponent(txtQS302A2);
		gridPreguntas302.addComponent(lblvacio);
		
		lblP303 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS303=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs303_1,R.string.cap01_03qs303_2,R.string.cap01_03qs303_3,R.string.cap01_03qs303_4,R.string.cap01_03qs303_5,R.string.cap01_03qs303_6,R.string.cap01_03qs303_7,R.string.cap01_03qs303_8,R.string.cap01_03qs303_9,R.string.cap01_03qs303_10,R.string.cap01_03qs303_11,R.string.cap01_03qs303_12,R.string.cap01_03qs303_13,R.string.cap01_03qs303_14,R.string.cap01_03qs303_15).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS303ChangeValue");//salto_rgQS303
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_a).size(altoComponente+20, 250).textSize(18).negrita();
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_b).textSize(18).negrita();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_c).textSize(18).negrita();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_d).textSize(18).negrita();
		
		rgQS303.agregarTitle(0,lblsectorpublico);
		rgQS303.agregarTitle(1,lblhospital);
		rgQS303.agregarTitle(8,lblsectorprivado);
		rgQS303.agregarTitle(11,lblong);
		rgQS303.agregarTitle(14,lblcampa);	
		
		txtQS303_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblTitulo2);
		q2 = createQuestionSection(lblP301,lblP301_extra,rgQS301);
		q3 = createQuestionSection(lblP302,lblP302_extra,gridPreguntas302.component());	
		q4 = createQuestionSection(lblP303,rgQS303,txtQS303_O);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		
		if (salud.qs301!=null) {
			salud.qs301=salud.getConvertqs100(salud.qs301);
		}
		
		if (salud.qs302!=null) {
			salud.qs302=salud.getConvertqs100(salud.qs302);
		}
		
		if (!Util.esDiferente(salud.qs302,1) && txtQS302A1.getText().toString().trim().length()!=0 ) {
			salud.qs302a=Integer.parseInt(txtQS302A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs302,2) && txtQS302A2.getText().toString().trim().length()!=0) {
			salud.qs302a=Integer.parseInt(txtQS302A2.getText().toString().trim());
		}
		
		if (salud.qs303!=null) {
			salud.qs303=salud.getConvertqs303(salud.qs303);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS1_3().qs301=salud.qs301;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(mayor_50){
			if (Util.esVacio(salud.qs301)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.301"); 
				view = rgQS301; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(salud.qs302) && !Util.esDiferente(salud.qs301,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.302"); 
				view = rgQS302; 
				error = true; 
				return false; 
			} 
			
			if(Util.esDiferente(salud.qs302,8) && !Util.esDiferente(salud.qs301,1)){ 
				if (Util.esVacio(salud.qs302a) && !Util.esDiferente(salud.qs302,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.302a");
					view = txtQS302A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs302a) && !Util.esDiferente(salud.qs302,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.302a");
					view = txtQS302A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 23, salud.qs302a) && !Util.esDiferente(salud.qs302,1)){
					mensaje = ""+salud.qs302a+" Esta fuera del rango: 0-23";
					view = txtQS302A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(2, salud.qs23, salud.qs302a) && !Util.esDiferente(salud.qs302,2)){
					mensaje = ""+salud.qs302a+" Esta fuera del rango: 2-"+salud.qs23;
					view = txtQS302A2; 
					error = true; 
					return false;
				}
			}
			
			if (Util.esVacio(salud.qs303) && !Util.esDiferente(salud.qs301,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.303"); 
				view = rgQS303; 
				error = true; 
				return false; 
			}
					
			if(!Util.esDiferente(salud.qs303,96,42) && !Util.esDiferente(salud.qs301,1)){ 
				if (Util.esVacio(salud.qs303_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.303/OTRO");
					view = txtQS303_O; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() {
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
		if(salud==null){ 
		  salud=new Salud(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id;
	    }
			
		if (salud.qs301!=null) {
    		salud.qs301=salud.setConvertqs100(salud.qs301);    
    	}
    	if (salud.qs302!=null) {
    		salud.qs302=salud.setConvertqs100(salud.qs302);    
    	}
    	
    	if (salud.qs303!=null) {
    		salud.qs303=salud.setConvertqs303(salud.qs303);
    	}
		

    	
		seleccionado = getSeccion01Service().getPersonaEdad(salud.id,salud.hogar_id,salud.persona_id);
		
		entityToUI(salud); 
		
		if (salud.qs302a!=null) {	
	  		  if (!Util.esDiferente(salud.qs302,1)) {
	  	  		  txtQS302A1.setText(salud.qs302a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs302,2)) {
	  			  txtQS302A2.setText(salud.qs302a.toString());
	  		  }
	  	  }
		
		inicio(); 
		App.getInstance().setPersonabySalud(salud);
    } 
    private void inicio() { 
    	entra_seccion3();
    	
    	if(mayor_50){
			onqrgQS301ChangeValue();
	    	onqrgQS302ChangeValue();
	    	onqrgQS303ChangeValue();
	    	ValidarsiesSupervisora();	    	
	    	if (salud.qs302==null) {
	    		Util.cleanAndLockView(getActivity(),txtQS302A1,txtQS302A2);
	    	}
    	}else{
    		grabar();
    	}
    	
    	txtCabecera.requestFocus();
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
    
    public void mostrarPreguntas(){
    	
    	int val301 = Integer.parseInt(rgQS301.getTagSelected("0").toString());
    	
		if (Util.esDiferente(val301, 1,1)) {
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			salud.qs302 = null;
			salud.qs302a = null;
			salud.qs303 = null;
			salud.qs303_o = null; 
		}else{
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			int val302 = Integer.parseInt(rgQS302.getTagSelected("0").toString());
			
			if (!Util.esDiferente(val302, 3,3)) {
//				MyUtil.LiberarMemoria();
				salud.qs302a = null;
				txtQS302A1.setVisibility(View.GONE);
			}else{
				txtQS302A1.setVisibility(View.VISIBLE);
			}
			
			int val303 = Integer.parseInt(rgQS303.getTagSelected("0").toString());
			
			if (!Util.esDiferente(val303, 14,14)) {
				txtQS303_O.setVisibility(View.VISIBLE);
			}else{
//				MyUtil.LiberarMemoria();
				txtQS303_O.setVisibility(View.GONE);
				salud.qs303_o = null;
			}
		}
    }
    
    public void onqrgQS301ChangeValue() {
//		mostrarPreguntas();    	
		if (MyUtil.incluyeRango(2,3,rgQS301.getTagSelected("").toString())) {
	    	MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS302,txtQS302A1,txtQS302A2,rgQS303,txtQS303_O);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS302,rgQS303,txtQS303_O);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			rgQS302.requestFocus();
		}
	}
    public void onqrgQS302ChangeValue() {
//		mostrarPreguntas();
		if (!MyUtil.incluyeRango(3,3,rgQS302.getTagSelected("").toString())) {	
			if (MyUtil.incluyeRango(1,1,rgQS301.getTagSelected("").toString())) {
				if(MyUtil.incluyeRango(1,1,rgQS302.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS302A2);
					salud.qs302a=null;
					Util.lockView(getActivity(), false, txtQS302A1);
					txtQS302A1.setVisibility(View.VISIBLE);
					txtQS302A1.requestFocus();
				}
				if(MyUtil.incluyeRango(2,2,rgQS302.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS302A1);
					salud.qs302a=null;
					Util.lockView(getActivity(), false, txtQS302A2);
					txtQS302A2.setVisibility(View.VISIBLE);
					txtQS302A2.requestFocus();
				}
			}
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS302A1,txtQS302A2);
			rgQS303.requestFocus();	
		}
	}
    public void onqrgQS303ChangeValue() {
		if (!MyUtil.incluyeRango(13,14,rgQS303.getTagSelected("").toString())) {	
//			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(),txtQS303_O);
			txtQS303_O.setVisibility(View.GONE);
		} 
		else {
			Util.lockView(getActivity(), false, txtQS303_O);
			txtQS303_O.setVisibility(View.VISIBLE);
			txtQS303_O.requestFocus();	
		}
    }    
    
    public void entra_seccion3(){
    	if(salud.qs23<50){
//    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(this.getActivity(), rgQS301,rgQS302,txtQS302A1,txtQS302A2,rgQS303,txtQS303_O);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		mayor_50 = false;
    	}else{
     		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE); 		
    		mayor_50 = true;
    		Util.lockView(getActivity(), false, rgQS301,rgQS302,txtQS302A1,txtQS302A2,rgQS303,txtQS303_O);
    	}    	
    }    
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS301.readOnly();
			rgQS302.readOnly();
			txtQS302A1.readOnly();
			txtQS302A2.readOnly();
			rgQS303.readOnly();
			txtQS303_O.readOnly();
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs301!=null) {
			salud.qs301=salud.getConvertqs100(salud.qs301);
		}
		
		if (salud.qs302!=null) {
			salud.qs302=salud.getConvertqs100(salud.qs302);
		}
		if(salud.qs302!=null){
			if (!Util.esDiferente(salud.qs302,1) && txtQS302A1.getText().toString().trim().length()!=0 ) {
				salud.qs302a=Integer.parseInt(txtQS302A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs302,2) && txtQS302A2.getText().toString().trim().length()!=0) {
				salud.qs302a=Integer.parseInt(txtQS302A2.getText().toString().trim());
			}
		}		
		
		if (salud.qs303!=null) {
			salud.qs303=salud.getConvertqs303(salud.qs303);
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		} 
		return App.SALUD;
	}
} 
