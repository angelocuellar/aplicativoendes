package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_005 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS102; 
//	@FieldAnnotation(orderIndex=2) 
//	public RadioGroupOtherField rgQS103U; 
//	@FieldAnnotation(orderIndex=3) 
//	public IntegerField txtQS103C; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS104; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS105; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS106; 
	Salud salud; 

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblpregunta102,lblpregunta103,lblpregunta106,	lblpregunta105,lblpregunta102_ind,lblpregunta103_ind; 
	public GridComponent2 gridPreguntas103;
	public LabelComponent lblpregunta104,lblvacio;
	public IntegerField txtQS103C1,txtQS103C2;
	
	LinearLayout q0; 
	LinearLayout q1; 
//	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6; 


	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String qs104fechref;
	
	public CS_01Fragment_005() {} 
	public CS_01Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 	
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS102","QS103U","QS103C","QS104","QS104FECH_REF","QS105","QS106","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS102","QS103U","QS103C","QS104","QS104FECH_REF","QS105","QS106")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		rgQS102=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs102_1,R.string.cap01_03qs102_2,R.string.cap01_03qs102_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS102ChangeValue"); 
		
//		rgQS103U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs103u_1,R.string.cap01_03qs103u_2,R.string.cap01_03qs103u_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS103ChangeValue"); 
//		txtQS103C1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
//		txtQS103C2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
//		lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
		
		rgQS104=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs104_1,R.string.cap01_03qs104_2,R.string.cap01_03qs104_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS104ChangeValue"); 
		rgQS105=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs105_1,R.string.cap01_03qs105_2,R.string.cap01_03qs105_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQS106=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs106_1,R.string.cap01_03qs106_2,R.string.cap01_03qs106_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta102_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs102_ind).textSize(17).negrita();
//		lblpregunta103_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs103_ind).textSize(17).negrita();
		
		Spanned texto102 = Html.fromHtml("102. �Alguna vez en su vida un m�dico le ha diagnosticado <b>\"hipertensi�n arterial\"</b> o <b>\"presi�n alta\"</b>?");
		lblpregunta102 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
		lblpregunta102.setText(texto102);
				
//		Spanned texto103 = Html.fromHtml("103. �Hace cu�nto tiempo le dijeron a usted que tiene <b>hipertensi�n arterial</b> o <b>\"presi�n alta\"</b>?");
//		lblpregunta103 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
//		lblpregunta103.setText(texto103);
		
		Spanned texto104 = Html.fromHtml("104. �En los �ltimos 12 meses, es decir, desde F1 del a�o pasado hasta F2 F3, usted ha recibido y/o comprado medicamentos para controlar su <b>presi�n arterial</b>?");
		lblpregunta104 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
		lblpregunta104.setText(texto104);
		
		Spanned texto106 = Html.fromHtml("106. �En los �ltimos 12 meses usted tom� sus medicamentos  <b>tal como</b> le indic� el m�dico?");
		lblpregunta106 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
		lblpregunta105=new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs105).alinearIzquierda();
		lblpregunta106.setText(texto106);
		
//		gridPreguntas103=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
//		gridPreguntas103.addComponent(rgQS103U,1,3);		
//		gridPreguntas103.addComponent(txtQS103C1);
//		gridPreguntas103.addComponent(txtQS103C2);
//		gridPreguntas103.addComponent(lblvacio);
		
		
		
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q6 = createQuestionSection(lblSubtitulo);
		q1 = createQuestionSection(lblpregunta102,lblpregunta102_ind,rgQS102); 
//		q2 = createQuestionSection(lblpregunta103,lblpregunta103_ind,gridPreguntas103.component());
		q3 = createQuestionSection(lblpregunta104,rgQS104); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta105,rgQS105); 
		q5 = createQuestionSection(lblpregunta106,rgQS106); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
 
		form.addView(q0);
		form.addView(q6); 
		form.addView(q1); 
//		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 


    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
	 
		uiToEntity(salud); 
		
		if (salud.qs102!=null) {
			salud.qs102=salud.getConvertqs100(salud.qs102);
		}
//		if (salud.qs103u!=null) {
//			salud.qs103u=salud.getConvertqs100(salud.qs103u);
//		}
		if (salud.qs104!=null) {
			salud.qs104=salud.getConvertqs100(salud.qs104);
			 
			if(qs104fechref == null) {
			 	salud.qs104fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs104fech_ref = qs104fechref;
				 }				
		}
		else {
			salud.qs104fech_ref = qs104fechref;			
		}
		if (salud.qs105!=null) {
			salud.qs105=salud.getConvertqs100(salud.qs105);
		}
		if (salud.qs106!=null) {
			salud.qs106=salud.getConvertqs100(salud.qs106);
		}
		
//		if (!Util.esDiferente(salud.qs103u,1) && txtQS103C1.getText().toString().trim().length()!=0 ) {
//			salud.qs103c=Integer.parseInt(txtQS103C1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs103u,2) && txtQS103C2.getText().toString().trim().length()!=0) {
//			salud.qs103c=Integer.parseInt(txtQS103C2.getText().toString().trim());
//		}
				
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
	
		if (Util.esVacio(salud.qs102)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.102"); 
			view = rgQS102; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs102,1)) {
//			if (Util.esVacio(salud.qs103u)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta P.103U"); 
//				view = rgQS103U; 
//				error = true; 
//				return false; 
//			} 
//			if (!Util.esDiferente(salud.qs103u,1)) {
//				if (Util.esVacio(salud.qs103c)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.103C1"); 
//					view = txtQS103C1; 
//					error = true; 
//					return false; 
//				}
//				if(Util.esMayor(salud.qs103c, 23)){
//					mensaje = "Opci�n meses no debe ser mayor a 23"; 
//					view = txtQS103C1; 
//					error = true; 
//					return false; 
//				}
//			}	
//			if (!Util.esDiferente(salud.qs103u,2)) {
//				if (Util.esVacio(salud.qs103c)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.103C2"); 
//					view = txtQS103C2; 
//					error = true; 
//					return false; 
//				}
//				if(Util.esMenor(salud.qs103c,2) || Util.esMayor(salud.qs103c,salud.qs23)){
//					mensaje = "A�os del diagn�stico no corresponde"; 
//					view = txtQS103C2; 
//					error = true; 
//					return false; 
//				}
//			}
			if (Util.esVacio(salud.qs104)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.104"); 
				view = rgQS104; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs104,1)) {
				if (Util.esVacio(salud.qs105)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.105"); 
					view = rgQS105; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs106)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.106"); 
					view = rgQS106; 
					error = true; 
					return false; 
				}
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() {   
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	  	  
  	  if (salud.qs102!=null) {
  		  salud.qs102=salud.setConvertqs100(salud.qs102);
  	  }
//  	  if (salud.qs103u!=null) {
//  		  salud.qs103u=salud.setConvertqs100(salud.qs103u);
//  	  }
  	  if (salud.qs104!=null) {
  		  salud.qs104=salud.setConvertqs100(salud.qs104);
  	  }
  	  if (salud.qs105!=null) {
  		  salud.qs105=salud.setConvertqs100(salud.qs105);
  	  }
  	  if (salud.qs106!=null) {
  		  salud.qs106=salud.setConvertqs100(salud.qs106);
  	  }  	  
 
  	  if(salud==null){ 
  		  salud=new Salud(); 
  		  salud.id=App.getInstance().getPersonaSeccion01().id; 
  		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
  		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
  	  } 
  	  
  	  entityToUI(salud);
  	  
  	  qs104fechref = salud.qs104fech_ref;  		
  	  
//  	  if (salud.qs103c!=null) {	
//  		  if (!Util.esDiferente(salud.qs103u,1)) {
//  	  		  txtQS103C1.setText(salud.qs103c.toString());
//  		  }
//  		  if (!Util.esDiferente(salud.qs103u,2)) {
//  			  txtQS103C2.setText(salud.qs103c.toString());
//  		  }
//  	  }   	
  
   	  inicio(); 
	}    
 
    private void inicio() {    	
    	RenombrarEtiquetas();
       	onqrgQS102ChangeValue();
       	ValidarsiesSupervisora();
//    	rgQS102.requestFocus();
       	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS102.readOnly();
//			rgQS103U.readOnly();
//			txtQS103C.readOnly();
//			txtQS103C1.readOnly();
//			txtQS103C2.readOnly();
			rgQS104.readOnly();
			rgQS105.readOnly();
			rgQS106.readOnly();
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(qs104fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs104fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta104.setText(lblpregunta104.getText().toString().replace("F1", F2)); 
    	lblpregunta104.setText(lblpregunta104.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta104.setText(lblpregunta104.getText().toString().replace("F3", F3));
    	
    	
    	Spanned texto1041= Html.fromHtml(lblpregunta104.getText().toString().replace("presi�n arterial", "<b>presi�n arterial</b>"));
    	lblpregunta104.setText(texto1041);
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	public void onqrgQS102ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQS102.getTagSelected("").toString())) {			
			Util.cleanAndLockView(getActivity(),/*rgQS103U,*/txtQS103C1,txtQS103C2,rgQS104,rgQS105,rgQS106);
			MyUtil.LiberarMemoria();
//			salud.qs103c=null;
//			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);		
		} 
		else {
			
			Util.lockView(getActivity(),false,/*rgQS103U,*/rgQS104,rgQS105,rgQS106);		
//			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
//			onqrgQS103ChangeValue();    
			onqrgQS104ChangeValue();
//			rgQS103U.requestFocus();		    				
		}	
	}
	
//	public void onqrgQS103ChangeValue() {
//		if (!MyUtil.incluyeRango(2,3,rgQS102.getTagSelected("").toString())) {			
//			if (MyUtil.incluyeRango(1,1,rgQS103U.getTagSelected("").toString())) {			
//				Util.cleanAndLockView(getActivity(),txtQS103C2);
//				salud.qs103c=null;
//				Util.lockView(getActivity(),false,txtQS103C1);				
//				txtQS103C1.requestFocus();				
//			} 
//			if (MyUtil.incluyeRango(2,2,rgQS103U.getTagSelected("").toString())) {		
//				Util.cleanAndLockView(getActivity(),txtQS103C1);	
//				salud.qs103c=null;
//				Util.lockView(getActivity(),false,txtQS103C2);			
//				txtQS103C2.requestFocus();				
//			}	
//			if (MyUtil.incluyeRango(3,3,rgQS103U.getTagSelected("").toString())) {
//				salud.qs103c=null;		
//				Util.cleanAndLockView(getActivity(),txtQS103C1,txtQS103C2);			
//				rgQS104.requestFocus();				
//			}
//		}	
//	}
	
	public void onqrgQS104ChangeValue() {
		if (!MyUtil.incluyeRango(2,3,rgQS102.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(2,3,rgQS104.getTagSelected("").toString())) {				
				Util.cleanAndLockView(getActivity(),rgQS105,rgQS106);	
				MyUtil.LiberarMemoria();
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);				
			} 
			else {
				Util.lockView(getActivity(),false,rgQS105,rgQS106);
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
//				rgQS105.requestFocus();
			}	
		}	
	}
	
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs102!=null) {
			salud.qs102=salud.getConvertqs100(salud.qs102);
		}
//		if (salud.qs103u!=null) {
//			salud.qs103u=salud.getConvertqs100(salud.qs103u);
//		}
		if (salud.qs104!=null) {
			salud.qs104=salud.getConvertqs100(salud.qs104);
			
			if(qs104fechref == null) {
			 	salud.qs104fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs104fech_ref = qs104fechref;
				 }			
		}
		else {
			salud.qs104fech_ref = null;
		}
		if (salud.qs105!=null) {
			salud.qs105=salud.getConvertqs100(salud.qs105);
		}
		if (salud.qs106!=null) {
			salud.qs106=salud.getConvertqs100(salud.qs106);
		}
		
//		if (!Util.esDiferente(salud.qs103u,1) && txtQS103C1.getText().toString().trim().length()!=0 ) {
//			salud.qs103c=Integer.parseInt(txtQS103C1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs103u,2) && txtQS103C2.getText().toString().trim().length()!=0) {
//			salud.qs103c=Integer.parseInt(txtQS103C2.getText().toString().trim());
//		}
				

		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		} 
		return App.SALUD;
	}
} 
