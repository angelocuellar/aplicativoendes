package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_03Fragment_015 extends FragmentForm { 
//	@FieldAnnotation(orderIndex=1) 
//	public RadioGroupOtherField rgQS309; 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS311; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS312; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS312A1;
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS312A2;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS313; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQS313_O;
	@FieldAnnotation(orderIndex=7)
	public TextAreaField txtQSOBS_SECCION3;
	Salud salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblsectorpublico,lblsectorprivado,lblhospital,lblong,lblcampa,lblObservaciones,lblObs_titulo; 
	private LabelComponent lblP311,lblP311_extra,lblP312,lblP312_extra,lblP313,lblvacio;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	
	public GridComponent2 gridPreguntas312;
	
	public boolean mayor_60=true;
	
// 
	public CS_03Fragment_015() {} 
	public CS_03Fragment_015 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
//		rango(getActivity(), txtQS312A1, 0, 23, null, null); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS311","QS312","QS312A","QS313","QS313_O","ID","HOGAR_ID","PERSONA_ID","QS23","QSOBS_SECCION3")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS311","QS312","QS312A","QS313","QS313_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo_2).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblP311 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs311).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP311_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs311_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS311=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs311_1,R.string.cap01_03qs311_2,R.string.cap01_03qs311_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS311ChangeValue"); 
		
		lblP312 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs312).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP312_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs302_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		rgQS312=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs312_1,R.string.cap01_03qs312_2,R.string.cap01_03qs312_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS312ChangeValue"); 
		txtQS312A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS312A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
//		txtQS312A1=new IntegerField(this.getActivity()).size(altoComponente+70, 80).maxLength(2); 
				
		gridPreguntas312=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas312.addComponent(rgQS312,1,3);		
		gridPreguntas312.addComponent(txtQS312A1);
		gridPreguntas312.addComponent(txtQS312A2);
		gridPreguntas312.addComponent(lblvacio);
		
		lblP313 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs313).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS313=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs313_1,R.string.cap01_03qs313_2,R.string.cap01_03qs313_3,R.string.cap01_03qs313_4,R.string.cap01_03qs313_5,R.string.cap01_03qs313_6,R.string.cap01_03qs313_7,R.string.cap01_03qs313_8,R.string.cap01_03qs313_9,R.string.cap01_03qs313_10,R.string.cap01_03qs313_11,R.string.cap01_03qs313_12,R.string.cap01_03qs313_13,R.string.cap01_03qs313_14).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS313ChangeValue");
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs313_a).textSize(18).negrita();
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs313_b).textSize(18).negrita();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs313_c).textSize(18).negrita();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs313_d).textSize(18).negrita();
		
		rgQS313.agregarTitle(0,lblsectorpublico);
		rgQS313.agregarTitle(1,lblhospital);
		rgQS313.agregarTitle(8,lblsectorprivado);
		rgQS313.agregarTitle(10,lblong);
		rgQS313.agregarTitle(13,lblcampa);
		
		txtQS313_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15); 
//		lblObs_titulo=new LabelComponent(this.getActivity()).size(60, 600).text(R.string.secccion04_obs_titulo).textSize(15).alinearIzquierda().negrita(); 
		txtQSOBS_SECCION3= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2);
		q2 = createQuestionSection(lblP311,lblP311_extra,rgQS311);		
		q3 = createQuestionSection(lblP312,lblP312_extra,gridPreguntas312.component()); 		 
		q4 = createQuestionSection(lblP313,rgQS313,txtQS313_O); 
		
		q5 = createQuestionSection(R.string.cap01_03qsObservaSeccion3,txtQSOBS_SECCION3); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);

    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		
		if (salud.qs311!=null) {
			salud.qs311=salud.getConvertqs100(salud.qs311);
		}
		
		if (salud.qs312!=null) {
			salud.qs312=salud.getConvertqs100(salud.qs312);
		}
		
		if (!Util.esDiferente(salud.qs312,1) && txtQS312A1.getText().toString().trim().length()!=0 ) {
			salud.qs312a=Integer.parseInt(txtQS312A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs312,2) && txtQS312A2.getText().toString().trim().length()!=0) {
			salud.qs312a=Integer.parseInt(txtQS312A2.getText().toString().trim());
		}
		
		if (salud.qs313!=null) {
			salud.qs313=salud.getConvertqs313(salud.qs313);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(mayor_60){
			if (Util.esVacio(salud.qs311)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS311"); 
				view = rgQS311; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(salud.qs312) && !Util.esDiferente(salud.qs311,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.312"); 
				view = rgQS312; 
				error = true; 
				return false; 
			} 
			
			if(Util.esDiferente(salud.qs312,8) && !Util.esDiferente(salud.qs311,1)){
				if (Util.esVacio(salud.qs312a) && !Util.esDiferente(salud.qs312,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.312a");
					view = txtQS312A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs312a) && !Util.esDiferente(salud.qs312,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.312a");
					view = txtQS312A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 23, salud.qs312a) && !Util.esDiferente(salud.qs312,1)){
					mensaje = ""+salud.qs312a+" Esta fuera del rango: 0-23";
					view = txtQS312A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(2, salud.qs23, salud.qs312a) && !Util.esDiferente(salud.qs312,2)){
					mensaje = ""+salud.qs312a+" Esta fuera del rango: 2-"+salud.qs23;
					view = txtQS312A2; 
					error = true; 
					return false;
				}
			}
			
			if (Util.esVacio(salud.qs313) && !Util.esDiferente(salud.qs311,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.313"); 
				view = rgQS313; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(salud.qs313,96,42) && !Util.esDiferente(salud.qs311,1)){ 	
				if (Util.esVacio(salud.qs313_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.313/OTRO");
					view = txtQS313_O; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		
  
    	if(salud==null){ 
		  salud=new Salud(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
    	
    	if (salud.qs311!=null) {
    		salud.qs311=salud.setConvertqs100(salud.qs311);    
    	}
    	if (salud.qs312!=null) {
    		salud.qs312=salud.setConvertqs100(salud.qs312);    
    	}
    	if (salud.qs313!=null) {
    		salud.qs313=salud.setConvertqs313(salud.qs313);
    	}
    	

    	
		entityToUI(salud);
		
		if (salud.qs312a!=null) {	
	  		  if (!Util.esDiferente(salud.qs312,1)) {
	  	  		  txtQS312A1.setText(salud.qs312a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs312,2)) {
	  			  txtQS312A2.setText(salud.qs312a.toString());
	  		  }
	  	}
		
//		Log.e("Cargar datos :",""+salud.qsobs_seccion3);
		
		inicio(); 
		App.getInstance().setPersonabySalud(salud);
    } 
    private void inicio() { 
    	entra_seccion3();
    	if(mayor_60){
			onqrgQS311ChangeValue();
	    	onqrgQS312ChangeValue();
	    	onqrgQS313ChangeValue();
	    	ValidarsiesSupervisora();
	    	if (salud.qs312==null) {
	    		Util.cleanAndLockView(getActivity(),txtQS312A1,txtQS312A2);
	    	}
//	    	rgQS311.requestFocus();
    	}else{
    		grabar();
//    		parent.nextFragment(29);
    	}
    	
    	txtCabecera.requestFocus();
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS311ChangeValue(){
    	if (MyUtil.incluyeRango(2,3,rgQS311.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS312,txtQS312A1,txtQS312A2,rgQS313,txtQS313_O);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS312,rgQS313,txtQS313_O);
			q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
			rgQS312.requestFocus();
		}
    }
    
    public void onqrgQS312ChangeValue(){
    	if (!MyUtil.incluyeRango(3,3,rgQS312.getTagSelected("").toString())) {	
    		if (MyUtil.incluyeRango(1,1,rgQS311.getTagSelected("").toString())) {
//				Util.lockView(getActivity(), false, txtQS312A1);
				
				if(MyUtil.incluyeRango(1,1,rgQS312.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS312A2);
					salud.qs312a=null;
					Util.lockView(getActivity(), false, txtQS312A1);
//					rango(getActivity(), txtQS312A1, 0, 23, null, null);
					txtQS312A1.setVisibility(View.VISIBLE);
					txtQS312A1.requestFocus();
				}
				if(MyUtil.incluyeRango(2,2,rgQS312.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS312A1);
					salud.qs312a=null;
					Util.lockView(getActivity(), false, txtQS312A2);
//					rango(getActivity(), txtQS312A2, 2, salud.qs23, null, null);
					txtQS312A2.setVisibility(View.VISIBLE);
					txtQS312A2.requestFocus();
				}
//				txtQS312A1.setVisibility(View.VISIBLE);
//				txtQS312A1.requestFocus();
    		}
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS312A1,txtQS312A2);
//			txtQS312A1.setVisibility(View.GONE);
			rgQS313.requestFocus();	
		}
    }
    
    public void onqrgQS313ChangeValue(){
    	if (!MyUtil.incluyeRango(12,13,rgQS313.getTagSelected("").toString())) {
//    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(),txtQS313_O);
			txtQS313_O.setVisibility(View.GONE);
		} 
		else {
			Util.lockView(getActivity(), false, txtQS313_O);
			txtQS313_O.setVisibility(View.VISIBLE);
			txtQS313_O.requestFocus();	
		}
    }
    
    public void entra_seccion3(){
    	if(salud.qs23<60){
//    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(this.getActivity(), rgQS311,rgQS312,txtQS312A1,txtQS312A2,rgQS313,txtQS313_O,txtQSOBS_SECCION3);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		mayor_60 = false;
    	}else{
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		mayor_60 = true;
    		Util.lockView(getActivity(), false, rgQS311,rgQS312,txtQS312A1,txtQS312A2,rgQS313,txtQS313_O);
    		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS311","QS312","QS312A","QS313","QS313_O","QSOBS_SECCION3")}; 
    	}
    } 

    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS311.readOnly();
			rgQS312.readOnly();
			txtQS312A1.readOnly();
			txtQS312A2.readOnly();
			rgQS313.readOnly();
			txtQS313_O.readOnly();
			txtQSOBS_SECCION3.setEnabled(false);
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs311!=null) {
			salud.qs311=salud.getConvertqs100(salud.qs311);
		}
		
		if (salud.qs312!=null) {
			salud.qs312=salud.getConvertqs100(salud.qs312);
		}
		if (salud.qs312!=null) {
			if (!Util.esDiferente(salud.qs312,1) && txtQS312A1.getText().toString().trim().length()!=0 ) {
				salud.qs312a=Integer.parseInt(txtQS312A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs312,2) && txtQS312A2.getText().toString().trim().length()!=0) {
				salud.qs312a=Integer.parseInt(txtQS312A2.getText().toString().trim());
			}
		}		
		
		if (salud.qs313!=null) {
			salud.qs313=salud.getConvertqs313(salud.qs313);
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
