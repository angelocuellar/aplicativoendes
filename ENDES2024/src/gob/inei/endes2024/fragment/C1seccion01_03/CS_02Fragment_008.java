package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_02Fragment_008 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS200; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS201; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS202; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS203; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQS203C; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS204U; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS204C; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS205U; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQS205C; 
	public LabelComponent lblpregunta200,lblpregunta201;
	Salud salud; 

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblSubtitulo1,lblpregunta200_ind,lblpregunta202,lblpregunta203,lblpregunta204,lblpregunta205; 
	public GridComponent2 gridPreguntas203, gridPreguntas204, gridPreguntas205;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7;
	LinearLayout q8; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
 
	String qs200fechref;
	String qs201fechref;
	
	public CS_02Fragment_008() {} 
	public CS_02Fragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);	
		rango(getActivity(), txtQS205C, 1, 50); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS200","QS200FECH_REF","QS201","QS201FECH_REF","QS202","QS203","QS203C","QS204U","QS204C","QS205U","QS205C","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS200","QS200FECH_REF","QS201","QS201FECH_REF","QS202","QS203","QS203C","QS204U","QS204C","QS205U","QS205C")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC02).textSize(21).centrar(); 
		lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01_2).negrita();
		rgQS200=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs200_1,R.string.cap01_03qs200_2,R.string.cap01_03qs200_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS200ChangeValue"); 
		rgQS201=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs201_1,R.string.cap01_03qs201_2,R.string.cap01_03qs201_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS201ChangeValue"); 
		rgQS202=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS202ChangeValue"); 
		
		lblpregunta200 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs200).textSize(19).size(MATCH_PARENT, MATCH_PARENT); 
		lblpregunta200_ind = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs200_ind).textSize(17).size(MATCH_PARENT, MATCH_PARENT).negrita();
		lblpregunta201 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs201).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta202 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs202).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta203 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs203).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta204 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs204u).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta205 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs205u).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		
		rgQS203=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs203_1,R.string.cap01_03qs203_2).size(altoComponente+30,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS203ChangeValue"); 
		txtQS203C=new IntegerField(this.getActivity()).size(altoComponente+30, 80).maxLength(2); 
		
		rgQS204U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs204u_1,R.string.cap01_03qs204u_2).size(altoComponente+30,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS204ChangeValue"); 
		txtQS204C=new IntegerField(this.getActivity()).size(altoComponente+30, 80).maxLength(2); 
		
		rgQS205U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs205u_1,R.string.cap01_03qs205u_2).size(altoComponente+30,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS205ChangeValue"); 
		txtQS205C=new IntegerField(this.getActivity()).size(altoComponente+30, 80).maxLength(2); 
		
		gridPreguntas203=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas203.addComponent(rgQS203);		
		gridPreguntas203.addComponent(txtQS203C);
		
		gridPreguntas204=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas204.addComponent(rgQS204U);		
		gridPreguntas204.addComponent(txtQS204C);
		
		gridPreguntas205=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas205.addComponent(rgQS205U);		
		gridPreguntas205.addComponent(txtQS205C);		 
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
	
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q8 = createQuestionSection(lblSubtitulo1);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblpregunta200,lblpregunta200_ind,rgQS200); 
		q3 = createQuestionSection(lblpregunta201,rgQS201); 
		q4 = createQuestionSection(lblpregunta202,rgQS202); 
		q5 = createQuestionSection(lblpregunta203,gridPreguntas203.component()); 
		q6 = createQuestionSection(lblpregunta204,gridPreguntas204.component()); 
		q7 = createQuestionSection(lblpregunta205,gridPreguntas205.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0);
		form.addView(q8);
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
	
	

    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 

		uiToEntity(salud);
		
		if (salud.qs200!=null) {
			salud.qs200=salud.getConvertqs100(salud.qs200);
			
			if(qs200fechref == null) {
			 	salud.qs200fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs200fech_ref = qs200fechref;
				 }
		}
		else {
			salud.qs200fech_ref = null;
		}
		if (salud.qs201!=null) {
			salud.qs201=salud.getConvertqs100(salud.qs201);
			
			if(qs201fechref == null) {
			 	salud.qs201fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs201fech_ref = qs201fechref;
				 }
		}
		else {
			salud.qs201fech_ref = null;
		}
		if (salud.qs203!=null) {
			salud.qs203=salud.getConvertqs203(salud.qs203);
		}
		if (salud.qs204u!=null) {
			salud.qs204u=salud.getConvertqs203(salud.qs204u);
		}
		if (salud.qs205u!=null) {
			salud.qs205u=salud.getConvertqs203(salud.qs205u);
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS1_3().qs200= salud.qs200;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(salud.qs200)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.200"); 
			view = rgQS200; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs200,1)) {
			if (Util.esVacio(salud.qs201)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.201"); 
				view = rgQS201; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs201,1)) {
				if (Util.esVacio(salud.qs202)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.202"); 
					view = rgQS202; 
					error = true; 
					return false; 
				}
				if (!Util.esDiferente(salud.qs202,1)) {
					if (Util.esVacio(salud.qs203)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.203"); 
						view = rgQS203; 
						error = true; 
						return false; 
					} 
					if (!Util.esDiferente(salud.qs203, 1)) {
						if (Util.esVacio(salud.qs203c)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.203C"); 
							view = txtQS203C; 
							error = true; 
							return false; 
						} 
					}
					if (!Util.esDiferente(salud.qs203,8)) {
						if (Util.esVacio(salud.qs204u)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.204U"); 
							view = rgQS204U; 
							error = true; 
							return false; 
						} 
						if (!Util.esDiferente(salud.qs204u, 1)) {
							if (Util.esVacio(salud.qs204c)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta P.204C"); 
								view = txtQS204C; 
								error = true; 
								return false; 
							} 
						}				
						if (Util.esVacio(salud.qs205u)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.205U"); 
							view = rgQS205U; 
							error = true; 
							return false; 
						} 
						if (!Util.esDiferente(salud.qs205u, 1)) {
							if (Util.esVacio(salud.qs205c)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta P.205C"); 
								view = txtQS205C; 
								error = true; 
								return false; 
							} 
						}
					}
					else{
						if (Util.esVacio(salud.qs205u)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.205U"); 
							view = rgQS205U; 
							error = true; 
							return false; 
						} 
						if (!Util.esDiferente(salud.qs205u, 1)) {
							if (Util.esVacio(salud.qs205c)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta P.205C"); 
								view = txtQS205C; 
								error = true; 
								return false; 
							} 
						}
					}
				
				}				
			}	
		}
		if(!Util.esDiferente(salud.qs202,1)){
			if((!Util.esDiferente(salud.qs203, 1) && Util.esMenor(salud.qs203c,10)) ||(!Util.esDiferente(salud.qs203, 1) && Util.esMenor(salud.qs23,salud.qs203c)) ){
				mensaje ="Edad en la que empez� a fumar no puede ser menor de 10 a�os";
				view = txtQS204C; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(salud.qs204u, 1) && Util.esMenor(salud.qs23-salud.qs204c , 10) ){
				mensaje ="A�os en la que empez� a fumar no corresponde con la edad";
				view = txtQS205C; 
				error = true; 
				return false; 
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() {
    	
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	if (salud.qs200!=null) {
			salud.qs200=salud.setConvertqs100(salud.qs200);
		}
		if (salud.qs201!=null) {
			salud.qs201=salud.setConvertqs100(salud.qs201);
		}
		if (salud.qs203!=null) {
			salud.qs203=salud.setConvertqs203(salud.qs203);
		}
		if (salud.qs204u!=null) {
			salud.qs204u=salud.setConvertqs203(salud.qs204u);
		}
		if (salud.qs205u!=null) {
			salud.qs205u=salud.setConvertqs203(salud.qs205u);
		}
		
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	qs200fechref = salud.qs200fech_ref;
    	qs201fechref = salud.qs201fech_ref;
    	inicio(); 
	}

    private void inicio() { 
		RenombrarEtiquetas();
    	onqrgQS200ChangeValue(); 
    	ValidarsiesSupervisora();
//    	rgQS200.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS200.readOnly();
			rgQS201.readOnly();
			rgQS202.readOnly();
			rgQS203.readOnly();
			txtQS203C.readOnly();
			rgQS204U.readOnly();
			txtQS204C.readOnly();
			rgQS205U.readOnly();
			txtQS205C.readOnly();
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(qs200fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs200fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta200.setText(lblpregunta200.getText().toString().replace("F1", F2)); 
    	lblpregunta200.setText(lblpregunta200.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta200.setText(lblpregunta200.getText().toString().replace("F3", F3));
    	
    	
    	Calendar fechahacetreintadias; 
    	fechaactual = new GregorianCalendar();
    	if(qs201fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs201fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	
   		int anio=  fechaactual.get(Calendar.YEAR); // Calendar.getInstance().get(Calendar.YEAR);   		
//   		if(((GregorianCalendar) fechaactual).isLeapYear(anio)){
//   			fechahacetreintadias= MyUtil.PrimeraFecha(fechaactual, 29);
//   		}
//   		else{
   			fechahacetreintadias = MyUtil.PrimeraFecha(fechaactual, 30);
//   		}
   		
   		lblpregunta201.setText(lblpregunta201.getText().toString().replace("F1", fechahacetreintadias.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias.get(Calendar.MONTH))));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS200ChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),rgQS201,rgQS202,rgQS203,txtQS203C,rgQS204U,txtQS204C,rgQS205U,txtQS205C);
  			q3.setVisibility(View.GONE);
  			q4.setVisibility(View.GONE);
  			q5.setVisibility(View.GONE);
  			q6.setVisibility(View.GONE);
  			q7.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,rgQS201,rgQS202,rgQS203,rgQS204U,rgQS205U);	
  			q3.setVisibility(View.VISIBLE);
  			q4.setVisibility(View.VISIBLE);
  			q5.setVisibility(View.VISIBLE);
  			q6.setVisibility(View.VISIBLE);
  			q7.setVisibility(View.VISIBLE);
  			onqrgQS201ChangeValue();  			
  			rgQS201.requestFocus();
  		}	
  	}
    
    public void onqrgQS201ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,3,rgQS201.getTagSelected("").toString())) {		
     			MyUtil.LiberarMemoria();
     			Util.cleanAndLockView(getActivity(),rgQS202,rgQS203,txtQS203C,rgQS204U,txtQS204C,rgQS205U,txtQS205C);
      			q4.setVisibility(View.GONE);
      			q5.setVisibility(View.GONE);
      			q6.setVisibility(View.GONE);
      			q7.setVisibility(View.GONE);
      		} 
      		else {
      			Util.lockView(getActivity(),false,rgQS202,rgQS203,rgQS204U,rgQS205U);		
      			onqrgQS202ChangeValue();
      			q4.setVisibility(View.VISIBLE);
      			q5.setVisibility(View.VISIBLE);
      			q6.setVisibility(View.VISIBLE);
      			q7.setVisibility(View.VISIBLE);
      			onqrgQS202ChangeValue();
      			rgQS202.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS202ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,3,rgQS201.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,2,rgQS202.getTagSelected("").toString())) {		
     			MyUtil.LiberarMemoria();
     			Util.cleanAndLockView(getActivity(),rgQS203,txtQS203C,rgQS204U,txtQS204C,rgQS205U,txtQS205C);
      			q5.setVisibility(View.GONE);
      			q6.setVisibility(View.GONE);
      			q7.setVisibility(View.GONE);
      		} 
      		else {
      			Util.lockView(getActivity(),false,rgQS203,rgQS204U,rgQS205U);
      			q5.setVisibility(View.VISIBLE);
      			q6.setVisibility(View.VISIBLE);
      			q7.setVisibility(View.VISIBLE);
      			onqrgQS203ChangeValue();      			
      			onqrgQS205ChangeValue();
      			rgQS203.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS203ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,3,rgQS201.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS202.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(1,1,rgQS203.getTagSelected("").toString())) {
    			MyUtil.LiberarMemoria();
    			Util.cleanAndLockView(getActivity(),rgQS204U,txtQS204C);
    			q6.setVisibility(View.GONE);
    			Util.lockView(getActivity(),false,txtQS203C);      		
    			txtQS203C.requestFocus();
    		}
    		else{
    			Util.lockView(getActivity(),false,rgQS204U,txtQS204C);
    			q6.setVisibility(View.VISIBLE);    	
    			Util.cleanAndLockView(getActivity(),txtQS203C);    			
    			onqrgQS204ChangeValue();
    			rgQS204U.requestFocus();
    		}    
    	} 	
  	}
    public void onqrgQS204ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,3,rgQS201.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS202.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,2,rgQS204U.getTagSelected("").toString())) {		
     			Util.cleanAndLockView(getActivity(),txtQS204C);
      			rgQS205U.requestFocus();
      		} 
      		else {
      			Util.lockView(getActivity(),false,txtQS204C);		
      			txtQS204C.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS205ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS200.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,3,rgQS201.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS202.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,2,rgQS205U.getTagSelected("").toString())) {		
     			Util.cleanAndLockView(getActivity(),txtQS205C);      			
      		} 
      		else {
      			Util.lockView(getActivity(),false,txtQS205C);		
      			txtQS205C.requestFocus();
      		}
		} 	
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);
		
		if (salud.qs200!=null) {
			salud.qs200=salud.getConvertqs100(salud.qs200);
			
			if(qs200fechref == null) {
			 	salud.qs200fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs200fech_ref = qs200fechref;
				 }
		}
		else {
			salud.qs200fech_ref = null;
		}
		if (salud.qs201!=null) {
			salud.qs201=salud.getConvertqs100(salud.qs201);
			
			if(qs201fechref == null) {
			 	salud.qs201fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs201fech_ref = qs201fechref;
				 }
		}
		else {
			salud.qs201fech_ref = null;
		}
		if (salud.qs203!=null) {
			salud.qs203=salud.getConvertqs203(salud.qs203);
		}
		if (salud.qs204u!=null) {
			salud.qs204u=salud.getConvertqs203(salud.qs204u);
		}
		if (salud.qs205u!=null) {
			salud.qs205u=salud.getConvertqs203(salud.qs205u);
		}

		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		return App.SALUD;
	}
} 
