package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_03Fragment_014 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS304; 
//	@FieldAnnotation(orderIndex=2) 
//	public RadioGroupOtherField rgQS305; 
//	@FieldAnnotation(orderIndex=3) 
//	public IntegerField txtQS305A1;
//	@FieldAnnotation(orderIndex=4) 
//	public IntegerField txtQS305A2;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS306; 
//	@FieldAnnotation(orderIndex=6) 
//	public RadioGroupOtherField rgQS307; 
//	@FieldAnnotation(orderIndex=7) 
//	public RadioGroupOtherField rgQS308; 
//	@FieldAnnotation(orderIndex=8) 
//	public RadioGroupOtherField rgQS309;
	@FieldAnnotation(orderIndex=3) 
	public TextAreaField txtQSOBS_SECCION3;//QSOBS_SECCION3
	
	Salud salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblP304,lblP304_extra,lblP305,lblpregunta306,lblP306_extra,lblpregunta307,lblP307_extra,lblpregunta308,lblP308_extra,lblpregunta309,lblP309_extra,lblObservaciones,lblObs_titulo,lblvacio,lblP302_extra; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
//	LinearLayout q3; 
	LinearLayout q4; 
//	LinearLayout q5;
//	LinearLayout q6;
//	LinearLayout q7;
	LinearLayout q8; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	public GridComponent2 gridPreguntas305;
	
	public boolean mayor_50=true;
	String fechareferencia306;
	String fechareferencia307;
	String fechareferencia308;
	String fechareferencia309;
	public CS_03Fragment_014() {} 
	public CS_03Fragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  	@Override 
  	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS304","QS305","QS305A","QS306","QS307","QS308","QS309","QS219FECH_REF","ID","HOGAR_ID","PERSONA_ID","QS23","QSOBS_SECCION3")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS304","QS305","QS305A","QS306","QS307","QS308","QS309","QSOBS_SECCION3")}; 
		return rootView; 
	} 
  	@Override 
  	protected void buildFields() { 
  		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
  		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cs03_titulo_1).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblP304 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs304).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP304_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs304_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS304=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs304_1,R.string.cap01_03qs304_2,R.string.cap01_03qs304_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS304ChangeValue");
		
//	  	lblP305 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs305).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
	  	lblP302_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs302_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
//	  	rgQS305=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs305_1,R.string.cap01_03qs305_2,R.string.cap01_03qs305_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS305ChangeValue"); 
//	  	txtQS305A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
//		txtQS305A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
//		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
//		gridPreguntas305=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
//		gridPreguntas305.addComponent(rgQS305,1,3);		
//		gridPreguntas305.addComponent(txtQS305A1);
//		gridPreguntas305.addComponent(txtQS305A2);
//		gridPreguntas305.addComponent(lblvacio);
		
		
		lblpregunta306 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs306);
		lblP306_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs306_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS306=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs306_1,R.string.cap01_03qs306_2,R.string.cap01_03qs306_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS306ChangeValue");
		
//		lblpregunta307 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs307).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
//		lblP307_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs307_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
//		rgQS307=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs307_1,R.string.cap01_03qs307_2,R.string.cap01_03qs307_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS307ChangeValue");	  
//		
//		lblpregunta308 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs308);
//		lblP308_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs308_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
//		rgQS308=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs308_1,R.string.cap01_03qs308_2,R.string.cap01_03qs308_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS308ChangeValue");
//		
//		lblpregunta309 = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs309).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
//		lblP309_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs309_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
//		rgQS309=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs309_1,R.string.cap01_03qs309_2,R.string.cap01_03qs309_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS309ChangeValue");
		
		lblObs_titulo=new LabelComponent(this.getActivity()).size(60, 600).text(R.string.secccion04_obs_titulo).textSize(15).alinearIzquierda().negrita(); 
		txtQSOBS_SECCION3= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
    } 
  	
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblP304,lblP304_extra,rgQS304);		
//		q3 = createQuestionSection(lblP305,lblP302_extra,gridPreguntas305.component());
		q4 = createQuestionSection(lblpregunta306,lblP306_extra,rgQS306); 
//		q5 = createQuestionSection(lblpregunta307,lblP307_extra,rgQS307); 
//		q6 = createQuestionSection(lblpregunta308,lblP308_extra,rgQS308);
//		q7 = createQuestionSection(lblpregunta309,lblP309_extra,rgQS309);
		q8 = createQuestionSection(R.string.cap01_03qsObservaSeccion3,txtQSOBS_SECCION3); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
//		form.addView(q3); 
		form.addView(q4); 
//		form.addView(q5); 
//		form.addView(q6);
//		form.addView(q7);
		form.addView(q8);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		
		if (salud.qs304!=null) {
			salud.qs304=salud.getConvertqs100(salud.qs304);
		}
		
//		if (salud.qs305!=null) {
//			salud.qs305=salud.getConvertqs100(salud.qs305);
//		}
		
//		if (!Util.esDiferente(salud.qs305,1) && txtQS305A1.getText().toString().trim().length()!=0 ) {
//			salud.qs305a=Integer.parseInt(txtQS305A1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs305,2) && txtQS305A2.getText().toString().trim().length()!=0) {
//			salud.qs305a=Integer.parseInt(txtQS305A2.getText().toString().trim());
//		}
		
		if (salud.qs306!=null) {
			salud.qs306=salud.getConvertqs100(salud.qs306);
		}
		
//		if (salud.qs307!=null) {
//			salud.qs307=salud.getConvertqs100(salud.qs307);
//		}
//		
//		if (salud.qs308!=null) {
//			salud.qs308=salud.getConvertqs100(salud.qs308);
//		}
//		
//		if (salud.qs309!=null) {
//			salud.qs309=salud.getConvertqs100(salud.qs309);
//		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs306 == null) {
				salud.qs219fech_ref = null;
			} 
			else {
				if(fechareferencia306 == null) {
					salud.qs219fech_ref =  Util.getFechaActualToString();
				}
				else {
					salud.qs219fech_ref = fechareferencia306;
				}			
			}	
			
//			if(salud.qs307 == null) {
//				salud.qs219fech_ref = null;
//			} 
//			else {
//				if(fechareferencia307 == null) {
//					salud.qs219fech_ref =  Util.getFechaActualToString();
//				}
//				else {
//					salud.qs219fech_ref = fechareferencia307;
//				}			
//			}
			
//			if(salud.qs308 == null) {
//				salud.qs219fech_ref = null;
//			} 
//			else {
//				if(fechareferencia308 == null) {
//					salud.qs219fech_ref =  Util.getFechaActualToString();
//				}
//				else {
//					salud.qs219fech_ref = fechareferencia308;
//				}			
//			}
			
//			if(salud.qs309 == null) {
//				salud.qs219fech_ref = null;
//			} 
//			else {
//				if(fechareferencia309 == null) {
//					salud.qs219fech_ref =  Util.getFechaActualToString();
//				}
//				else {
//					salud.qs219fech_ref = fechareferencia309;
//				}			
//			}
	
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(mayor_50){
			if (Util.esVacio(salud.qs304)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS304"); 
				view = rgQS304; 
				error = true; 
				return false; 
			}
//			if (Util.esVacio(salud.qs305) && !Util.esDiferente(salud.qs304,1)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QS305"); 
//				view = rgQS305; 
//				error = true; 
//				return false; 
//			} 
			
//			if(Util.esDiferente(salud.qs305,8) && !Util.esDiferente(salud.qs304,1)){
//				if (Util.esVacio(salud.qs305a) && !Util.esDiferente(salud.qs305,1)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.305a");
//					view = txtQS305A1; 
//					error = true; 
//					return false; 
//				}
//				if (Util.esVacio(salud.qs305a) && !Util.esDiferente(salud.qs305,2)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.305a");
//					view = txtQS305A2; 
//					error = true; 
//					return false; 
//				}
//				if(!MyUtil.incluyeRango(0, 23, salud.qs305a) && !Util.esDiferente(salud.qs305,1)){
//					mensaje = ""+salud.qs305a+" Esta fuera del rango: 0-23";
//					view = txtQS305A1; 
//					error = true; 
//					return false;
//				}
//				if(!MyUtil.incluyeRango(2, salud.qs23, salud.qs305a) && !Util.esDiferente(salud.qs305,2)){
//					mensaje = ""+salud.qs305a+" Esta fuera del rango: 2-"+salud.qs23;
//					view = txtQS305A2; 
//					error = true; 
//					return false;
//				}
//			}
			
			if (Util.esVacio(salud.qs306)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS306"); 
				view = rgQS306; 
				error = true; 
				return false; 
			} 
//			if (Util.esVacio(salud.qs307) && !Util.esDiferente(salud.qs306,1)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QS307"); 
//				view = rgQS307; 
//				error = true; 
//				return false; 
//			} 
//			if (Util.esVacio(salud.qs308)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QS308"); 
//				view = rgQS308; 
//				error = true; 
//				return false; 
//			} 
//			if (Util.esVacio(salud.qs309) && !Util.esDiferente(salud.qs308,1)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QS309"); 
//				view = rgQS309; 
//				error = true; 
//				return false; 
//			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
		if(salud==null){ 
		  salud=new Salud(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id;
	    } 
		
		if (salud.qs304!=null) {
    		salud.qs304=salud.setConvertqs100(salud.qs304);    
    	}
//    	if (salud.qs305!=null) {
//    		salud.qs305=salud.setConvertqs100(salud.qs305);    
//    	}
    	if (salud.qs306!=null) {
    		salud.qs306=salud.setConvertqs100(salud.qs306);    
    	}
//    	if (salud.qs307!=null) {
//    		salud.qs307=salud.setConvertqs100(salud.qs307);    
//    	}
//    	if (salud.qs308!=null) {
//    		salud.qs308=salud.setConvertqs100(salud.qs308);    
//    	}
//    	if (salud.qs309!=null) {
//    		salud.qs309=salud.setConvertqs100(salud.qs309);    
//    	}
    	
    
    	
		entityToUI(salud); 
		
		fechareferencia306 = salud.qs219fech_ref;
//		fechareferencia307 = salud.qs219fech_ref;
//		fechareferencia308 = salud.qs219fech_ref;
//		fechareferencia309 = salud.qs219fech_ref;
		
//		if (salud.qs305a!=null) {	
//	  		  if (!Util.esDiferente(salud.qs305,1)) {
//	  	  		  txtQS305A1.setText(salud.qs305a.toString());
//	  		  }
//	  		  if (!Util.esDiferente(salud.qs305,2)) {
//	  			  txtQS305A2.setText(salud.qs305a.toString());
//	  		  }
//	  	}
		
		inicio(); 
		App.getInstance().setPersonabySalud(salud);
    } 
    private void inicio() { 
    	entra_seccion3();
    	if(mayor_50){
//			onqrgQS304ChangeValue();
//	    	onqrgQS305ChangeValue();
//	    	onqrgQS306ChangeValue();
//	    	onqrgQS307ChangeValue();
//	    	onqrgQS308ChangeValue();
	    	RenombrarEtiquetas();
	    	ValidarsiesSupervisora();
//	    	if (salud.qs305==null) {
//	    		Util.cleanAndLockView(getActivity(),txtQS305A1,txtQS305A2);
//	    	}
    	}else{
    		grabar();
    	}
    	txtCabecera.requestFocus();
    } 
    
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual306 = new GregorianCalendar();
//    	Calendar fechaactual307 = new GregorianCalendar();
//    	Calendar fechaactual308 = new GregorianCalendar();
//    	Calendar fechaactual309 = new GregorianCalendar();
    	if(fechareferencia306!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia306);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual306=cal;
		}  
    	
//    	if(fechareferencia307!=null){
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fechareferencia307);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual307=cal;
//		} 
    	
//    	if(fechareferencia308!=null){
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fechareferencia308);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual308=cal;
//		} 
    	
//    	if(fechareferencia309!=null){
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fechareferencia309);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual309=cal;
//		} 
    	
    	Calendar fechainicio306 = MyUtil.RestarMeses(fechaactual306,1);
    	String F1 = MyUtil.Mes(fechainicio306.get(Calendar.MONTH));
    	Calendar fechados306 =	MyUtil.RestarMeses(fechaactual306,11);
    	String F2 = MyUtil.Mes(fechados306.get(Calendar.MONTH));
    	lblpregunta306.text(R.string.cap01_03qs306);
    	lblpregunta306.setText(lblpregunta306.getText().toString().replace("F1", F2));
    	lblpregunta306.setText(lblpregunta306.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta306.setText(lblpregunta306.getText().toString().replace("F3", F3));
    	
    	Spanned texto306 = Html.fromHtml(lblpregunta306.getText()+"<b> a una distancia aproximada de 6 metros?</b>");    	
    	lblpregunta306.setText(texto306);
    	
//    	Calendar fechahacetreintadias307,fechahacetreintadias309;   
//    	Calendar c1 = Calendar.getInstance();
//    	String data307 =Integer.toString(c1.get(Calendar.YEAR));
//   		int anio=Integer.parseInt(data307);   	  		
//   		if(((GregorianCalendar) c1).isLeapYear(anio)){
//   			fechahacetreintadias307= MyUtil.PrimeraFecha(fechaactual307, 29);
//   		}
//   		else{
//   			fechahacetreintadias307 = MyUtil.PrimeraFecha(fechaactual307, 30);
//   		}
   		
//   	lblpregunta307.setText(lblpregunta307.getText().toString().replace("F1", fechahacetreintadias307.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias307.get(Calendar.MONTH))));
//   	Calendar fechainicio308 = MyUtil.RestarMeses(fechaactual308,1);
//    	String F1a = MyUtil.Mes(fechainicio308.get(Calendar.MONTH));
//    	Calendar fechados308 =	MyUtil.RestarMeses(fechaactual308,11);
//    	String F2a = MyUtil.Mes(fechados308.get(Calendar.MONTH));
//   	lblpregunta308.text(R.string.cap01_03qs308);
//    	lblpregunta308.setText(lblpregunta308.getText().toString().replace("F1", F2a)); 
//    	lblpregunta308.setText(lblpregunta308.getText().toString().replace("F2", F1a));
    
//    	Spanned texto308 = Html.fromHtml(lblpregunta308.getText()+"<b> a la distancia de su brazo extendido?</b>");    	
//    	lblpregunta308.setText(texto308);
//    	String data =Integer.toString(c1.get(Calendar.YEAR));
//   		anio=Integer.parseInt(data);   		
//   		if(((GregorianCalendar) c1).isLeapYear(anio)){
//   			fechahacetreintadias309= MyUtil.PrimeraFecha(fechaactual309, 29);
//   		}
//   		else{
//   			fechahacetreintadias309 = MyUtil.PrimeraFecha(fechaactual309, 30);
//   		}
//    	lblpregunta309.setText(lblpregunta309.getText().toString().replace("F1", fechahacetreintadias309.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias309.get(Calendar.MONTH))));
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
//    public void onqrgQS304ChangeValue(){
//    	if (MyUtil.incluyeRango(2,3,rgQS304.getTagSelected("").toString())) {
//    		MyUtil.LiberarMemoria();
//			Util.cleanAndLockView(this.getActivity(), rgQS305,txtQS305A1,txtQS305A2);
//			q3.setVisibility(View.GONE);
//			rgQS306.requestFocus();
//		} 
//		else {				
//			Util.lockView(getActivity(), false,  rgQS305);
//			q3.setVisibility(View.VISIBLE);
//			rgQS305.requestFocus();
//		}
//    }
    
//    public void onqrgQS305ChangeValue(){
//    	if (!MyUtil.incluyeRango(3,3,rgQS305.getTagSelected("").toString())) {	
//			
//			if(MyUtil.incluyeRango(1,1,rgQS305.getTagSelected("").toString())){
//				Util.cleanAndLockView(getActivity(),txtQS305A2);
//				salud.qs305a=null;
//				Util.lockView(getActivity(), false, txtQS305A1);
//				txtQS305A1.setVisibility(View.VISIBLE);
//				txtQS305A1.requestFocus();
//			}
//			if(MyUtil.incluyeRango(2,2,rgQS305.getTagSelected("").toString())){
//				Util.cleanAndLockView(getActivity(),txtQS305A1);
//				salud.qs302a=null;
//				Util.lockView(getActivity(), false, txtQS305A2);
//				txtQS305A2.setVisibility(View.VISIBLE);
//				txtQS305A2.requestFocus();
//			}
//		} 
//		else {
//			Util.cleanAndLockView(this.getActivity(),txtQS305A1,txtQS305A2);
//			rgQS306.requestFocus();	
//		}
//    }
    
//    public void onqrgQS306ChangeValue(){
//    	if (MyUtil.incluyeRango(2,3,rgQS306.getTagSelected("").toString())) {
//    		MyUtil.LiberarMemoria();
//			Util.cleanAndLockView(this.getActivity(), rgQS307);
//			q5.setVisibility(View.GONE);
//			rgQS308.requestFocus();
//		} 
//		else {				
//			Util.lockView(getActivity(), false,  rgQS307);
//			q5.setVisibility(View.VISIBLE);
//			rgQS307.requestFocus();
//		}
//    }
    
//    public void onqrgQS307ChangeValue(){
//		rgQS308.requestFocus();
//    }
//    
//    public void onqrgQS308ChangeValue(){
//    	if (MyUtil.incluyeRango(2,3,rgQS308.getTagSelected("").toString())) {
//    		MyUtil.LiberarMemoria();
//    		q7.setVisibility(View.GONE);
//			Util.cleanAndLockView(this.getActivity(), rgQS309);
//		}
//    	else {				
//    		q7.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(), false,  rgQS309);
//		}
//    }
    
    public void onqrgQS309ChangeValue(){
    	
    }
    
    public void entra_seccion3(){
    	if(salud.qs23<50){
//    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(this.getActivity(), rgQS304,/*rgQS305,txtQS305A1,txtQS305A2,*/rgQS306,/*rgQS307,rgQS308,rgQS309,*/txtQSOBS_SECCION3);
    		mayor_50 = false;
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
//    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
//    		q5.setVisibility(View.GONE);
//    		q6.setVisibility(View.GONE);
//    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    	}else{
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);   		
    		
    		mayor_50 = true;
    		Util.lockView(getActivity(), false, rgQS304,/*rgQS305,txtQS305A1,txtQS305A2,*/rgQS306/*,rgQS307,rgQS308,rgQS309*/);    		
    		if(salud.qs23>59){
//    			MyUtil.LiberarMemoria();
    			seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS304","QS305","QS305A","QS306","QS307","QS308","QS309")};
    			q8.setVisibility(View.GONE);
    		}else{
    			seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS304","QS305","QS305A","QS306","QS307","QS308","QS309","QSOBS_SECCION3")};
    		}
    		
    	}
    } 

    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS304.readOnly();
//			rgQS305.readOnly();
//			txtQS305A1.readOnly();
//			txtQS305A2.readOnly();
			rgQS306.readOnly();
//			rgQS307.readOnly();
//			rgQS308.readOnly();
//			rgQS309.readOnly();
			txtQSOBS_SECCION3.setEnabled(false);
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs304!=null) {
			salud.qs304=salud.getConvertqs100(salud.qs304);
		}
		
//		if (salud.qs305!=null) {
//			salud.qs305=salud.getConvertqs100(salud.qs305);
//		}
//		if (salud.qs305!=null) {
//			if (!Util.esDiferente(salud.qs305,1) && txtQS305A1.getText().toString().trim().length()!=0 ) {
//				salud.qs305a=Integer.parseInt(txtQS305A1.getText().toString().trim());
//			}
//			if (!Util.esDiferente(salud.qs305,2) && txtQS305A2.getText().toString().trim().length()!=0) {
//				salud.qs305a=Integer.parseInt(txtQS305A2.getText().toString().trim());
//			}
//		}		
		
//		if (salud.qs306!=null) {
//			salud.qs306=salud.getConvertqs100(salud.qs306);
//		}
//		
//		if (salud.qs307!=null) {
//			salud.qs307=salud.getConvertqs100(salud.qs307);
//		}
//		
//		if (salud.qs308!=null) {
//			salud.qs308=salud.getConvertqs100(salud.qs308);
//		}
//		
//		if (salud.qs309!=null) {
//			salud.qs309=salud.getConvertqs100(salud.qs309);
//		}
		
		if(salud.qs306 == null) {
			salud.qs219fech_ref = null;
		} 
		else {
			if(fechareferencia306 == null) {
				salud.qs219fech_ref =  Util.getFechaActualToString();
			}
			else {
				salud.qs219fech_ref = fechareferencia306;
			}			
		}
		
//		if(salud.qs307 == null) {
//			salud.qs219fech_ref = null;
//		} 
//		else {
//			if(fechareferencia307 == null) {
//				salud.qs219fech_ref =  Util.getFechaActualToString();
//			}
//			else {
//				salud.qs219fech_ref = fechareferencia307;
//			}			
//		}
		
//		if(salud.qs308 == null) {
//			salud.qs219fech_ref = null;
//		} 
//		else {
//			if(fechareferencia308 == null) {
//				salud.qs219fech_ref =  Util.getFechaActualToString();
//			}
//			else {
//				salud.qs219fech_ref = fechareferencia308;
//			}			
//		}
		
//		if(salud.qs309 == null) {
//			salud.qs219fech_ref = null;
//		} 
//		else {
//			if(fechareferencia309 == null) {
//				salud.qs219fech_ref =  Util.getFechaActualToString();
//			}
//			else {
//				salud.qs219fech_ref = fechareferencia309;
//			}			
//		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
