package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_02Fragment_010 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQS212A; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQS212A1; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS212A2;
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS_212ASN;
	
	
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS212B; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS212B1; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS212B2; 
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQS212BSN;
		
	@FieldAnnotation(orderIndex=9) 	
	public CheckBoxField chbQS212C; 
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQS212C1; 	
	@FieldAnnotation(orderIndex=11) 	
	public IntegerField txtQS212C2;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQS212CSN;
	
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQS212D; 
	@FieldAnnotation(orderIndex=14) 
	public IntegerField txtQS212D1; 
	@FieldAnnotation(orderIndex=15) 
	public IntegerField txtQS212D2;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQS212DSN;
	
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQS212E; 
	@FieldAnnotation(orderIndex=18) 
	public IntegerField txtQS212E1; 
	@FieldAnnotation(orderIndex=19) 
	public IntegerField txtQS212E2;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQS212ESN;
	
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQS212F; 
	@FieldAnnotation(orderIndex=22) 
	public IntegerField txtQS212F1; 
	@FieldAnnotation(orderIndex=23) 
	public IntegerField txtQS212F2;
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQS212FSN;
	
	@FieldAnnotation(orderIndex=25) 
	public CheckBoxField chbQS212G; 
	@FieldAnnotation(orderIndex=26) 
	public IntegerField txtQS212G1; 
	@FieldAnnotation(orderIndex=27) 
	public IntegerField txtQS212G2;
	@FieldAnnotation(orderIndex=28)
	public CheckBoxField chbQS212GSN;
	
	@FieldAnnotation(orderIndex=29) 
	public CheckBoxField chbQS212X; 
	@FieldAnnotation(orderIndex=30) 
	public TextField txtQS212_O; 
	@FieldAnnotation(orderIndex=31) 
	public IntegerField txtQS212X1;
	@FieldAnnotation(orderIndex=32)
	public CheckBoxField chbQS212XSN;
	@FieldAnnotation(orderIndex=33) 
	public IntegerField txtQS212X2; 
	
	@FieldAnnotation(orderIndex=34) 
	public CheckBoxField chbQS212Z; 

	

	Salud salud; 

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta,lblblanco,lblvaso,lblbotella,lblSubtitulo,lblpregunta212,lblpregunta212_ind1,lblpregunta212_ind2,lblpregunta212_ind3,lblpregunta212_ind4,lblpregunta212_ind5,lblsn; 
	public GridComponent2 gridPreguntas212;
	
	
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CS_02Fragment_010() {} 
	public CS_02Fragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQS212A1, 0, 50, null, 98);
		rango(getActivity(), txtQS212A2, 0, 30, null, 98);
		rango(getActivity(), txtQS212B1, 0, 50, null, 98);
		rango(getActivity(), txtQS212B2, 0, 30, null, 98);
		rango(getActivity(), txtQS212C1, 0, 50, null, 98);
		rango(getActivity(), txtQS212C2, 0, 30, null, 98);
		rango(getActivity(), txtQS212D1, 0, 50, null, 98);
		rango(getActivity(), txtQS212D2, 0, 30, null, 98);
		rango(getActivity(), txtQS212E1, 0, 50, null, 98);
		rango(getActivity(), txtQS212E2, 0, 30, null, 98);
		rango(getActivity(), txtQS212F1, 0, 50, null, 98);
		rango(getActivity(), txtQS212F2, 0, 30, null, 98);
		rango(getActivity(), txtQS212G1, 0, 50, null, 98);
		rango(getActivity(), txtQS212G2, 0, 30, null, 98);
		rango(getActivity(), txtQS212X1, 0, 50, null, 98);
		rango(getActivity(), txtQS212X2, 0, 30, null, 98);
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS212A","QS212A1","QS212A2","QS212B","QS212B1","QS212B2","QS212C","QS212C1","QS212C2","QS212D","QS212D1","QS212D2","QS212E","QS212E1","QS212E2","QS212F","QS212F1","QS212F2","QS212G","QS212G1","QS212G2","QS212X","QS212_O","QS212X1","QS212X2","QS212Z","ID","HOGAR_ID","PERSONA_ID","QS206","QS208","QS210","QS211U","QS211C")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS212A","QS212A1","QS212A2","QS212B","QS212B1","QS212B2","QS212C","QS212C1","QS212C2","QS212D","QS212D1","QS212D2","QS212E","QS212E1","QS212E2","QS212F","QS212F1","QS212F2","QS212G","QS212G1","QS212G2","QS212X","QS212_O","QS212X1","QS212X2","QS212Z")}; 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC02).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		
		lblpregunta212 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212).textSize(17).negrita();
		lblpregunta212_ind1 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212_ind1).textSize(19);
		lblpregunta212_ind2 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212_ind2).textSize(17).negrita();
		lblpregunta212_ind3 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212_ind3).textSize(19);
		lblpregunta212_ind4 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212_ind4).textSize(19).negrita();
		lblpregunta212_ind5 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs212_ind5).textSize(16).negrita();
		
		lblpregunta = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 730).text(R.string.cap01_03qs212_0).negrita().centrar();
		lblblanco = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 400).negrita().centrar();
		lblvaso = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 120).text(R.string.cap01_03qs212_1).negrita().centrar();
		lblbotella = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 130).text(R.string.cap01_03qs212_2).negrita().centrar();
		lblsn = new LabelComponent(getActivity()).textSize(16).size(altoComponente+10, 80).text(R.string.cap01_03qs212_3).negrita().centrar();
		
		chbQS212A=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212a, "1:0").size(altoComponente, 400).callback("onqrgQS212aChangeValue");		
		txtQS212A1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212A2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS_212ASN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212aSNChangeValue");	
		
		chbQS212B=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212b, "1:0").size(altoComponente, 400).callback("onqrgQS212bChangeValue");		
		txtQS212B1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212B2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212BSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212bSNChangeValue");
		
		
		chbQS212C=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212c, "1:0").size(altoComponente, 400).callback("onqrgQS212cChangeValue");		
		txtQS212C1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212C2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212CSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212cSNChangeValue");
		
		chbQS212D=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212d, "1:0").size(altoComponente, 400).callback("onqrgQS212dChangeValue");		
		txtQS212D1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212D2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212DSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212dSNChangeValue");
		
		chbQS212E=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212e, "1:0").size(altoComponente, 400).callback("onqrgQS212eChangeValue");		
		txtQS212E1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212E2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212ESN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212eSNChangeValue");
		
		chbQS212F=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212f, "1:0").size(altoComponente, 400).callback("onqrgQS212fChangeValue");		
		txtQS212F1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212F2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212FSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212fSNChangeValue");
		
		chbQS212G=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212g, "1:0").size(altoComponente, 400).callback("onqrgQS212gChangeValue");		
		txtQS212G1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212G2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212GSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212gSNChangeValue");
		
		chbQS212X=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212x, "1:0").size(altoComponente, 400).callback("onqrgQS212xChangeValue");		
		txtQS212X1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQS212X2=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(2);
		chbQS212XSN=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212zz, "98:0").size(altoComponente, 80).callback("onqrgQS212xSNChangeValue");
		
		txtQS212_O=new TextField(this.getActivity()).maxLength(150).size(altoComponente, 730); 		
		chbQS212Z=new CheckBoxField(this.getActivity(), R.string.cap01_03qs212z, "1:0").size(altoComponente, 730).callback("onqrgQS212NoSabeChangeValue");	
		
		gridPreguntas212=new GridComponent2(this.getActivity(),App.ESTILO,4);
		gridPreguntas212.addComponent(lblpregunta,4);
		gridPreguntas212.addComponent(lblblanco);
		gridPreguntas212.addComponent(lblvaso);
		gridPreguntas212.addComponent(lblbotella);
		gridPreguntas212.addComponent(lblsn);
		
		gridPreguntas212.addComponent(chbQS212A);		
		gridPreguntas212.addComponent(txtQS212A1);
		gridPreguntas212.addComponent(txtQS212A2);
		gridPreguntas212.addComponent(chbQS_212ASN);
		gridPreguntas212.addComponent(chbQS212B);		
		gridPreguntas212.addComponent(txtQS212B1);
		gridPreguntas212.addComponent(txtQS212B2);
		gridPreguntas212.addComponent(chbQS212BSN);
		gridPreguntas212.addComponent(chbQS212C);		
		gridPreguntas212.addComponent(txtQS212C1);
		gridPreguntas212.addComponent(txtQS212C2);
		gridPreguntas212.addComponent(chbQS212CSN);
		gridPreguntas212.addComponent(chbQS212D);		
		gridPreguntas212.addComponent(txtQS212D1);
		gridPreguntas212.addComponent(txtQS212D2);
		gridPreguntas212.addComponent(chbQS212DSN);
		gridPreguntas212.addComponent(chbQS212E);		
		gridPreguntas212.addComponent(txtQS212E1);
		gridPreguntas212.addComponent(txtQS212E2);
		gridPreguntas212.addComponent(chbQS212ESN);
		gridPreguntas212.addComponent(chbQS212F);		
		gridPreguntas212.addComponent(txtQS212F1);
		gridPreguntas212.addComponent(txtQS212F2);
		gridPreguntas212.addComponent(chbQS212FSN);
		gridPreguntas212.addComponent(chbQS212G);		
		gridPreguntas212.addComponent(txtQS212G1);
		gridPreguntas212.addComponent(txtQS212G2);
		gridPreguntas212.addComponent(chbQS212GSN);
		gridPreguntas212.addComponent(chbQS212X);		
		gridPreguntas212.addComponent(txtQS212X1);
		gridPreguntas212.addComponent(txtQS212X2);
		gridPreguntas212.addComponent(chbQS212XSN);
		gridPreguntas212.addComponent(txtQS212_O,4);
		gridPreguntas212.addComponent(chbQS212Z,4);  
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q2 = createQuestionSection(lblSubtitulo); 
		q1 = createQuestionSection(lblpregunta212,lblpregunta212_ind1,lblpregunta212_ind2,lblpregunta212_ind3,lblpregunta212_ind4,lblpregunta212_ind5,gridPreguntas212.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 

		form.addView(q0);
		form.addView(q2); 
		form.addView(q1); 

    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 

    	uiToEntity(salud); 		
    	llenarlapregunta212();		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (!Util.esDiferente(salud.qs206,1)) {
			if (!Util.esDiferente(salud.qs208,1)) {
				if (!Util.esDiferente(salud.qs210,1)) {
					if (!Util.esDiferente(salud.qs211u,1)) {
						if (!Util.alMenosUnoEsDiferenteA(0,salud.qs212a,salud.qs212b,salud.qs212c,salud.qs212d,salud.qs212e,salud.qs212f,salud.qs212g,salud.qs212x,salud.qs212z)) { 
							mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
							view = chbQS212Z; 
							error = true; 
							return false; 
						} 
						if (chbQS212A.isChecked()) {
							if (Util.esVacio(salud.qs212a1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212A1"); 
								view = txtQS212A1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212a2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212A2"); 
								view = txtQS212A2; 
								error = true; 
								return false; 
							} 
						}	
						if (chbQS212B.isChecked()) {
							if (Util.esVacio(salud.qs212b1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212B1"); 
								view = txtQS212B1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212b2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212B2"); 
								view = txtQS212B2; 
								error = true; 
								return false; 
							} 
						}		
						if (chbQS212C.isChecked()) {
							if (Util.esVacio(salud.qs212c1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212C1"); 
								view = txtQS212C1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212c2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212C2"); 
								view = txtQS212C2; 
								error = true; 
								return false; 
							} 
						}
						if (chbQS212D.isChecked()) {
							if (Util.esVacio(salud.qs212d1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212D1"); 
								view = txtQS212D1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212d2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212D2"); 
								view = txtQS212D2; 
								error = true; 
								return false; 
							}
						}	
						if (chbQS212E.isChecked()) {
							if (Util.esVacio(salud.qs212e1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212E1"); 
								view = txtQS212E1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212e2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212E2"); 
								view = txtQS212E2; 
								error = true; 
								return false; 
							}
						} 
						if (chbQS212F.isChecked()) {
							if (Util.esVacio(salud.qs212f1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212F1"); 
								view = txtQS212F1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212f2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212F2"); 
								view = txtQS212F2; 
								error = true; 
								return false; 
							} 
						}	
						if (chbQS212G.isChecked()) {
							if (Util.esVacio(salud.qs212g1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212G1"); 
								view = txtQS212G1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212g2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212G2"); 
								view = txtQS212G2; 
								error = true; 
								return false; 
							} 
						}	
						if (chbQS212X.isChecked()) {					
							if (Util.esVacio(salud.qs212x1)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212X1"); 
								view = txtQS212X1; 
								error = true; 
								return false; 
							} 
							if (Util.esVacio(salud.qs212x2)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta QS212X2"); 
								view = txtQS212X2; 
								error = true; 
								return false; 
							} 	
							if(!Util.esDiferente(salud.qs212x,1)){ 
								if (Util.esVacio(salud.qs212_o)) { 
									mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
									view = txtQS212_O; 
									error = true; 
									return false; 
								} 
							}
						}	 
						if (Util.esVacio(salud.qs212z)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QS212Z"); 
							view = chbQS212Z; 
							error = true; 
							return false; 
						} 
					}
				}
			}
				
		}
		
		if (chbQS212A.isChecked() && !chbQS_212ASN.isChecked()) {
			if (!Util.esDiferente(salud.qs212a1,0) && !Util.esDiferente(salud.qs212a2,0)) {
				mensaje = "Vasos/copas � Botellas de Cerveza no pueden ser igual a 0"; 
				view = txtQS212A1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212a1,0) && Util.esDiferente(salud.qs212a2,0)) || (Util.esDiferente(salud.qs212a1,0) && !Util.esDiferente(salud.qs212a2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212A1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212B.isChecked() && !chbQS212BSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212b1,0) && !Util.esDiferente(salud.qs212b2,0)) {
				mensaje = "Vasos/copas � Botellas de Vino no pueden ser igual a 0"; 
				view = txtQS212B1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212b1,0) && Util.esDiferente(salud.qs212b2,0)) || (Util.esDiferente(salud.qs212b1,0) && !Util.esDiferente(salud.qs212b2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212B1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212C.isChecked() && !chbQS212CSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212c1,0) && !Util.esDiferente(salud.qs212c2,0)) {
				mensaje = "Vasos/copas � Botellas de Chicha no pueden ser igual a 0"; 
				view = txtQS212C1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212c1,0) && Util.esDiferente(salud.qs212c2,0)) || (Util.esDiferente(salud.qs212c1,0) && !Util.esDiferente(salud.qs212c2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212C1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212D.isChecked() && !chbQS212DSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212d1,0) && !Util.esDiferente(salud.qs212d2,0)) {
				mensaje = "Vasos/copas � Botellas de Masato no pueden ser igual a 0"; 
				view = txtQS212D1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212d1,0) && Util.esDiferente(salud.qs212d2,0)) || (Util.esDiferente(salud.qs212d1,0) && !Util.esDiferente(salud.qs212d2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212D1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212E.isChecked() && !chbQS212ESN.isChecked()) {
			if (!Util.esDiferente(salud.qs212e1,0) && !Util.esDiferente(salud.qs212e2,0)) {
				mensaje = "Vasos/copas � Botellas de Yonque no pueden ser igual a 0"; 
				view = txtQS212E1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212e1,0) && Util.esDiferente(salud.qs212e2,0)) || (Util.esDiferente(salud.qs212e1,0) && !Util.esDiferente(salud.qs212e2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212E1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212F.isChecked() && !chbQS212FSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212f1,0) && !Util.esDiferente(salud.qs212f2,0)) {
				mensaje = "Vasos/copas � Botellas de Anizado no pueden ser igual a 0"; 
				view = txtQS212F1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212f1,0) && Util.esDiferente(salud.qs212f2,0)) || (Util.esDiferente(salud.qs212f1,0) && !Util.esDiferente(salud.qs212f2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212F1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212G.isChecked() && !chbQS212GSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212g1,0) && !Util.esDiferente(salud.qs212g2,0)) {
				mensaje = "Vasos/copas � Botellas de Whisky no pueden ser igual a 0"; 
				view = txtQS212G1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212g1,0) && Util.esDiferente(salud.qs212g2,0)) || (Util.esDiferente(salud.qs212g1,0) && !Util.esDiferente(salud.qs212g2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212G1; 
//				error = true; 
//				return false; 
//			}
		}
		if (chbQS212X.isChecked() && !chbQS212XSN.isChecked()) {
			if (!Util.esDiferente(salud.qs212x1,0) && !Util.esDiferente(salud.qs212x2,0)) {
				mensaje = "Vasos/copas � Botellas de Otra bebida no pueden ser igual a 0"; 
				view = txtQS212X1; 
				error = true; 
				return false; 
			}
//			if (!((!Util.esDiferente(salud.qs212x1,0) && Util.esDiferente(salud.qs212x2,0)) || (Util.esDiferente(salud.qs212x1,0) && !Util.esDiferente(salud.qs212x2,0)))) {
//				mensaje = "Vasos/copas � Botellas de Cerveza debe ser igual a 0"; 
//				view = txtQS212X1; 
//				error = true; 
//				return false; 
//			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    
    	if (salud.qs220!=null) {
			salud.qs220=salud.setConvertqs100(salud.qs220);
		}
    	
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud);
    	MostrarPregunta212();
    	inicio(); 
	}
    
    public void ValidadQ12() {
		if (!Util.esDiferente(salud.qs206,2,8) || !Util.esDiferente(salud.qs208,2,8) || !Util.esDiferente(salud.qs210,2,8) || !Util.esDiferente(salud.qs211u,8)) {
			Util.cleanAndLockView(getActivity(),chbQS212A,chbQS212B,chbQS212C,chbQS212D,chbQS212E,chbQS212E,chbQS212F,chbQS212G,chbQS212G,chbQS212X,chbQS212Z,txtQS212_O);
			q0.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(),false,chbQS212A,chbQS212B,chbQS212C,chbQS212D,chbQS212E,chbQS212E,chbQS212F,chbQS212G,chbQS212G,chbQS212X,chbQS212Z);
			q0.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
		}
		
	}

    private void inicio() { 
    	ValidadQ12();
    	
    	if (!chbQS212A.isChecked()) {
    		Util.cleanAndLockView(getActivity(),txtQS212A1,txtQS212A2);	
		}  
    	if (!chbQS212B.isChecked()) {
    		Util.cleanAndLockView(getActivity(),txtQS212B1,txtQS212B2);	
		} 
    	if (!chbQS212C.isChecked()) {   
    		Util.cleanAndLockView(getActivity(),txtQS212C1,txtQS212C2);	
		}
    	if (!chbQS212D.isChecked()) {   
    		Util.cleanAndLockView(getActivity(),txtQS212D1,txtQS212D2);	
		}
    	if (!chbQS212E.isChecked()) {  
    		Util.cleanAndLockView(getActivity(),txtQS212E1,txtQS212E2);	
		}
    	if (!chbQS212F.isChecked()) {    
    		Util.cleanAndLockView(getActivity(),txtQS212F1,txtQS212F2);	
		}
    	if (!chbQS212G.isChecked()) {    
    		Util.cleanAndLockView(getActivity(),txtQS212G1,txtQS212G2);	
		}
    	if (!chbQS212X.isChecked()) {
    		Util.cleanAndLockView(getActivity(),txtQS212X1,txtQS212X2,txtQS212_O);	
		}
    	onqrgQS212aChangeValue();
    	onqrgQS212bChangeValue();
    	onqrgQS212cChangeValue();
    	onqrgQS212dChangeValue();
    	onqrgQS212eChangeValue();
    	onqrgQS212fChangeValue();
    	onqrgQS212gChangeValue();
    	onqrgQS212xChangeValue();
    	onqrgQS212NoSabeChangeValue();

    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	
//    	chbQS212A.requestFocus(); 
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQS212A.readOnly();
			chbQS212B.readOnly();
			chbQS212C.readOnly();
			chbQS212D.readOnly();
			chbQS212E.readOnly();
			chbQS212F.readOnly();
			chbQS212G.readOnly();
			chbQS212X.readOnly();
			chbQS212Z.readOnly();
			
			txtQS212A1.readOnly();
			txtQS212A2.readOnly();
			txtQS212B1.readOnly();
			txtQS212B2.readOnly();
			txtQS212C1.readOnly();
			txtQS212C2.readOnly();
			txtQS212D1.readOnly();
			txtQS212D2.readOnly();
			txtQS212E1.readOnly();
			txtQS212E2.readOnly();
			txtQS212F1.readOnly();
			txtQS212F2.readOnly();
			txtQS212G1.readOnly();
			txtQS212G2.readOnly();
			txtQS212X1.readOnly();
			txtQS212X2.readOnly();
			
			chbQS_212ASN.readOnly();
			chbQS212BSN.readOnly();
			chbQS212CSN.readOnly();
			chbQS212DSN.readOnly();
			chbQS212ESN.readOnly();
			chbQS212FSN.readOnly();
			chbQS212GSN.readOnly();
			chbQS212XSN.readOnly();
			
			txtQS212_O.readOnly();
			
			
		}
	}
	
    public void RenombrarEtiquetas(){
    	lblpregunta212_ind1.text(R.string.cap01_03qs212_ind1);
    	if(salud.qs211c!=null &&  Util.esMayor(salud.qs211c,1)){
    		lblpregunta212_ind1.setVisibility(View.VISIBLE);
    		lblpregunta212_ind1.setText(lblpregunta212_ind1.getText().toString().replace("F1", salud.qs211c.toString()));
    		lblpregunta212_ind2.setVisibility(View.GONE);
    		lblpregunta212_ind3.setVisibility(View.GONE);
    	}
    	else{
    		lblpregunta212_ind1.setVisibility(View.GONE);
    		lblpregunta212_ind2.setVisibility(View.VISIBLE);
    		lblpregunta212_ind3.setVisibility(View.VISIBLE);
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public boolean verificarCheck() {
  		if (chbQS212A.isChecked() || chbQS212B.isChecked() || chbQS212C.isChecked() || chbQS212D.isChecked() || chbQS212E.isChecked() || chbQS212F.isChecked() || chbQS212G.isChecked() || chbQS212X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public void onqrgQS212aChangeValue() {
  		if (chbQS212A.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212A1,txtQS212A2,chbQS_212ASN); 
  			onqrgQS212aSNChangeValue();
  			if (verificarCheck()) {
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
			}  		
  			txtQS212A1.requestFocus();
  		} 
  		else {  	
  			Util.cleanAndLockView(getActivity(),txtQS212A1,txtQS212A2,chbQS_212ASN); 
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}  		 			
  			chbQS212B.requestFocus();
  		}	
  	}
    
    
    public void onqrgQS212aSNChangeValue() {
    
  		if (chbQS_212ASN.isChecked()){	  		
  			Util.cleanAndLockView(getActivity(),txtQS212A1,txtQS212A2,chbQS212Z);  		
  		} 
  		else {
  		
  			Util.lockView(getActivity(),false,txtQS212A1,txtQS212A2);  		
  		}	
  	}
   public void onqrgQS212bSNChangeValue(){
	   if (chbQS212BSN.isChecked()){		
 			Util.cleanAndLockView(getActivity(),txtQS212B1,txtQS212B2,chbQS212Z);  		
 		} 
 		else {
 	
 			Util.lockView(getActivity(),false,txtQS212B1,txtQS212B2);  		
 		}
   }
public void onqrgQS212cSNChangeValue(){
	  if (chbQS212CSN.isChecked()){		
		  Util.cleanAndLockView(getActivity(),txtQS212C1,txtQS212C2,chbQS212Z);  		
		} 
		else {
			Util.lockView(getActivity(),false,txtQS212C1,txtQS212C2);  		
		}
}
public void onqrgQS212dSNChangeValue(){
	  if (chbQS212DSN.isChecked()){
		  Util.cleanAndLockView(getActivity(),txtQS212D1,txtQS212D2,chbQS212Z);  		
		} 
		else {
			Util.lockView(getActivity(),false,txtQS212D1,txtQS212D2);  		
		}
}
   
public void onqrgQS212eSNChangeValue(){
	 if (chbQS212ESN.isChecked()){	
		 Util.cleanAndLockView(getActivity(),txtQS212E1,txtQS212E2,chbQS212Z);  		
		} 
		else {
			Util.lockView(getActivity(),false,txtQS212E1,txtQS212E2);  		
		}
}
public void onqrgQS212fSNChangeValue(){
	if (chbQS212FSN.isChecked()){
		Util.cleanAndLockView(getActivity(),txtQS212F1,txtQS212F2,chbQS212Z);  		
	} 
	else {
		Util.lockView(getActivity(),false,txtQS212F1,txtQS212F2);  		
	}
}
public void onqrgQS212gSNChangeValue(){
	if (chbQS212GSN.isChecked()){		
		Util.cleanAndLockView(getActivity(),txtQS212G1,txtQS212G2,chbQS212Z);  		
	} 
	else {
		Util.lockView(getActivity(),false,txtQS212G1,txtQS212G2);  		
	}
}
public void onqrgQS212xSNChangeValue(){
	if (chbQS212XSN.isChecked()){	
		Util.cleanAndLockView(getActivity(),txtQS212X1,txtQS212X2,chbQS212Z);  		
	} 
	else {
		Util.lockView(getActivity(),false,txtQS212X1,txtQS212X2);  		
	}
}
    public void onqrgQS212bChangeValue() {
  		if (chbQS212B.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212B1,txtQS212B2,chbQS212BSN);
  			onqrgQS212bSNChangeValue();
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212B1.requestFocus();
  		} 
  		else {  	
  			Util.cleanAndLockView(getActivity(),txtQS212B1,txtQS212B2,chbQS212BSN);  
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}  		
  			chbQS212C.requestFocus();
  		}	
  	}
    public void onqrgQS212cChangeValue() {
  		if (chbQS212C.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212C1,txtQS212C2,chbQS212CSN);
  			onqrgQS212cSNChangeValue();
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS212Z);  				
  			}
  			txtQS212C1.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQS212C1,txtQS212C2,chbQS212CSN); 
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  			chbQS212D.requestFocus();
  		}	
  	}
    public void onqrgQS212dChangeValue() {
  		if (chbQS212D.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212D1,txtQS212D2,chbQS212DSN);
  			onqrgQS212dSNChangeValue();
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212D1.requestFocus();
  		} 
  		else {  	
  			Util.cleanAndLockView(getActivity(),txtQS212D1,txtQS212D2,chbQS212DSN); 
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  			chbQS212E.requestFocus();
  		}	
  	}
    public void onqrgQS212eChangeValue() {
  		if (chbQS212E.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212E1,txtQS212E2,chbQS212ESN);
  			onqrgQS212eSNChangeValue();
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212E1.requestFocus();
  		} 
  		else {  		
  			Util.cleanAndLockView(getActivity(),txtQS212E1,txtQS212E2,chbQS212ESN); 
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  			chbQS212F.requestFocus();
  		}	
  	}
    public void onqrgQS212fChangeValue() {
  		if (chbQS212F.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212F1,txtQS212F2,chbQS212FSN);
  			onqrgQS212fSNChangeValue();
  			if (verificarCheck()) {  			
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212F1.requestFocus();
  		} 
  		else {  	
  			Util.cleanAndLockView(getActivity(),txtQS212F1,txtQS212F2,chbQS212FSN); 
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  			chbQS212G.requestFocus();
  		}	
  	}
    public void onqrgQS212gChangeValue() {
  		if (chbQS212G.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212G1,txtQS212G2,chbQS212GSN);
  			onqrgQS212gSNChangeValue();
  			if (verificarCheck()) {  		
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212G1.requestFocus();
  		} 
  		else {  		
  			Util.cleanAndLockView(getActivity(),txtQS212G1,txtQS212G2,chbQS212GSN);  	
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  			chbQS212X.requestFocus();
  		}	
  	}
    public void onqrgQS212xChangeValue() {
  		if (chbQS212X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQS212X1,txtQS212X2,txtQS212_O,chbQS212XSN);
  			onqrgQS212xSNChangeValue();
  			if (verificarCheck()) {  		
  				Util.cleanAndLockView(getActivity(),chbQS212Z);
  			}
  			txtQS212X1.requestFocus();
  		} 
  		else { 
  			Util.cleanAndLockView(getActivity(),txtQS212X1,txtQS212X2,txtQS212_O,chbQS212XSN);  
  			if (!verificarCheck()) {
  				Util.lockView(getActivity(),false,chbQS212Z);  				
			}
  		}	
  	}
    public void onqrgQS212NoSabeChangeValue() {    	
  		if (chbQS212Z.isChecked()){
  			Util.cleanAndLockView(getActivity(),chbQS212A,txtQS212A1,txtQS212A2,chbQS212B,txtQS212B1,txtQS212B2,chbQS212C,txtQS212C1,txtQS212C2,chbQS212D,txtQS212D1,txtQS212D2,chbQS212E,txtQS212E1,txtQS212E2,chbQS212F,txtQS212F1,txtQS212F2,chbQS212G,txtQS212G1,txtQS212G2,chbQS212X,txtQS212X1,txtQS212X2,txtQS212_O);
  			
  		} 
  		else {
  			Util.lockView(getActivity(),false,chbQS212A,chbQS212B,chbQS212C,chbQS212D,chbQS212E,chbQS212F,chbQS212G,chbQS212X);
  			
  		}	
  	}
	public void llenarlapregunta212(){
		if(!Util.esDiferente(salud.qs212a,1)){
			if(chbQS_212ASN.isChecked()){
				salud.qs212a1=salud.qs212a2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212b,1)){
			if(chbQS212BSN.isChecked()){
				salud.qs212b1=salud.qs212b2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212c,1)){
			if(chbQS212CSN.isChecked()){
				salud.qs212c1=salud.qs212c2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212d,1)){
			if(chbQS212DSN.isChecked()){
				salud.qs212d1=salud.qs212d2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212e,1)){
			if(chbQS212ESN.isChecked()){
				salud.qs212e1=salud.qs212e2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212f,1)){
			if(chbQS212FSN.isChecked()){
				salud.qs212f1=salud.qs212f2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212g,1)){
			if(chbQS212GSN.isChecked()){
				salud.qs212g1=salud.qs212g2=98;
			}
		}
		if(!Util.esDiferente(salud.qs212x,1)){
			if(chbQS212XSN.isChecked()){
				salud.qs212x1=salud.qs212x2=98;
			}
		}
	}
	public void MostrarPregunta212(){
		if(!Util.esDiferente(salud.qs212a1,98) && !Util.esDiferente(salud.qs212a2, 98)){
    		chbQS_212ASN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212b1,98) && !Util.esDiferente(salud.qs212b2, 98)){
    		chbQS212BSN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212c1,98) && !Util.esDiferente(salud.qs212c2, 98)){
    		chbQS212CSN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212d1,98) && !Util.esDiferente(salud.qs212d2, 98)){
    		chbQS212DSN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212e1,98) && !Util.esDiferente(salud.qs212e2, 98)){
    		chbQS212ESN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212f1,98) && !Util.esDiferente(salud.qs212f2, 98)){
    		chbQS212FSN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212g1,98) && !Util.esDiferente(salud.qs212g2, 98)){
    		chbQS212GSN.setChecked(true);
    	}
		if(!Util.esDiferente(salud.qs212x1,98) && !Util.esDiferente(salud.qs212x2, 98)){
    		chbQS212XSN.setChecked(true);
    	}
	}
	@Override
	public Integer grabadoParcial() {
    	uiToEntity(salud); 		
    	llenarlapregunta212();	
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
