package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.R.color;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesEncuestaUtil;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public TextField txtQH02_1; 
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtPERSONA_ID;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH06; 
	@FieldAnnotation(orderIndex=4)  
	public IntegerField txtQS22D;
	@FieldAnnotation(orderIndex=5)  
	public IntegerField txtQS22M;
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS22A; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS23; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS24; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS25N; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQS25A;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQS25G; 
	
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQS25_A;
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQS25A_O_1;
	@FieldAnnotation(orderIndex=14) 
	public TextField txtQS25A_O_2;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQS25_B;
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQS25B_O_1;
	@FieldAnnotation(orderIndex=17) 
	public TextField txtQS25B_O_2;
	@FieldAnnotation(orderIndex=18) 
	public TextField txtQS25B_O_3;
	
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQS23A; 
	@FieldAnnotation(orderIndex=20) 
	public IntegerField txtQS23A_N; 
	@FieldAnnotation(orderIndex=21) 
	public TextField txtQS23A_O; 
//	public TextField txtCabecera;

	public TextField txtCabecera;
	public GridComponent2 gdFechaNacimiento;
	public ButtonComponent btnOmision;
	public List<Seccion01> ListadoMef;
	public Seccion01Service seccion01;
	Salud salud; 
	CAP04_07 saludS4; 
	Seccion01 informantesalud;
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	private Seccion01Service seccion01service;
	private LabelComponent lbledadCS,lbledadCH,lbldiaCH,lbldiaCH2,lbledadCH2,lblTitulo,lblnivel,lblanio,lblgrado,lblnombre,lblsexo,lbldia,lblmes,lblanioedad,lblSubtitulo,lblSubtitulo1,lblpregunta20,lblpregunta22,lblpregunta23,lblpregunta23a,lblpregunta24,lblpregunta25,lblpregunta25_ind,lblpregunta23_ind,lbledadCI,lbledadCI2,lblEspacio,lblEspacio2,lblEspacio1,lblmesCH,lblmesCH2,lblpregunta25a,lblpregunta25b; 
	public GridComponent2 gridPreguntas25,gridPreguntas20,gridPreguntas22,gridmesHogar;
	public GridComponent gridTextoedad,gridTextoedadCS,gridTextoedadI;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	SeccionCapitulo[] SeccionesGrabado,SeccionesGrabadoHogar,seccionesGrabadoMEF,SeccionesGrabadoEdadMef,
	SeccionesGrabadoSeccion01,SeccionesGrabadoSeccion02,
	seccionesGrabadoS4_1,seccionesGrabadoS4_2,seccionesGrabadoS4_3a,seccionesGrabadoS4_3b,seccionesGrabadoS4_4a,seccionesGrabadoS4_4b,
	seccionesGrabadoS3_1,seccionesGrabadoS3_2,seccionesGrabadoS5_1,
	seccionesGrabadoS6_1,seccionesGrabadoS6_2	
	;
//	seccionesGrabado15A29_S3,seccionesGrabado50A59_S3,seccionesGrabado30A39_S3,seccionesGrabado40A49_S3,
//	seccionesGrabado15A24_S4,
//	seccionesGrabado15A24_S4,seccionesGrabado40A49_S4,seccionesGrabado40A49_S4_VARONES,	
//	seccionesGrabado50A59_S4,seccionesGrabado60aMAS_S4,SeccionesGrabadoSeccionDifMef,
//	seccionesGrabado40A59,seccionesGrabado15A49,seccionesGrabado50A59,seccionesGrabado15A29,seccionesGrabado15A39S4,seccionesGrabado30A39_S4,
//	seccionesGrabado30A39_S4_VARONES,seccionesGrabado50A59_S4_VARONES,
	
	
	SeccionCapitulo[] seccionesCargado,seccionesCargadoInformanteSalud; 

	String fechareferencia;	
	
	public CS_01Fragment_002() {} 
	public CS_01Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening();
		
		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID")};
		
		SeccionesGrabadoEdadMef = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07")};
		
		SeccionesGrabadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH7DD","QH7MM","QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A")};
		SeccionesGrabadoSeccion02 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH07","QH7DD","QH7MM","QH14","QH15N","QH15Y","QH15G")};
	
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QS22D","QS22M","QS22A","QS22CONS","QS23","QS23A","QS23A_N", "QS23A_O","QS24","QS25N","QS25A","QS25G","QS25_A","QS25_AO","QS25_B","QS25_BO","ID","HOGAR_ID","PERSONA_ID","QH02_1","QH06","QH07","QH7DD","QH7MM","QS28","QS29A","QS29B","QH14","QH15N","QH15Y","QH15G")}; 
		
		seccionesGrabadoMEF = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS22D","QS22M","QS22A","QS23","QS23A","QS23A_N", "QS23A_O","QS24","QS25N","QS25A","QS25G","QS25_A","QS25_AO","QS25_B","QS25_BO")};
		SeccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QS22D","QS22M","QS22A","QS22CONS","QS23","QS23A","QS23A_N", "QS23A_O","QS24","QS25N","QS25A","QS25G","QS28","QS29A","QS29B","QS25_A","QS25_AO","QS25_B","QS25_BO")};
		
		seccionesGrabadoS3_1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O","QS304","QS305","QS305A","QS306")};
		seccionesGrabadoS3_2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS311","QS312","QS312A","QS313","QS313_O")};
		
		seccionesGrabadoS4_1  = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS401","QS402","QS403","QS404","QS404C")};	//"QS404A","QS404B"			
		seccionesGrabadoS4_2  = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406")};//,"QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y"		
		seccionesGrabadoS4_3a = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A")};
		seccionesGrabadoS4_3b = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS411","QS412","QS412_A","QS413","QS413A","QS413B","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F")};		
		seccionesGrabadoS4_4a = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS415","QS416","QS416_A")};
		seccionesGrabadoS4_4b = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS418A","QS419A","QS419A_A")};//"QS418","QS419","QS419_A";
//		seccionesGrabadoS5_1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS505A_A","QS505A_A_O")};//"QS418","QS419","QS419_A";
		
		seccionesGrabadoS6_1 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604")};
		seccionesGrabadoS6_2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS606","QS607","QS608","QS609","QS610","QS611")};
		
//		seccionesGrabado15A29_S3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O","QS304","QS305","QS305A","QS306","QS307","QS308","QS309",
//																					 "QS311","QS312","QS312A","QS313","QS313_O")};
//		seccionesGrabado30A39_S3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O","QS304","QS305","QS305A","QS306","QS307","QS308","QS309",
//				 																	 "QS311","QS312","QS312A","QS313","QS313_O")};
//		seccionesGrabado40A49_S3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS301","QS302","QS302A","QS303","QS303_O","QS304","QS305","QS305A","QS306","QS307","QS308","QS309",
//				 																	 "QS311","QS312","QS312A","QS313","QS313_O")};
//		seccionesGrabado50A59_S3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS311","QS312","QS312A","QS313","QS313_O")};
		
//		seccionesGrabado15A29_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",
//																					 "QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413",
//																					 "QS415","QS416","QS416_A")};

//		seccionesGrabado15A24_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",
//																					 "QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F",
//																					 "QS415","QS416","QS416_A")};
		
		
//		seccionesGrabado30A39_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",																					
//																					 "QS415","QS416","QS416_A",
//																					 "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		
//		seccionesGrabado30A39_S4_VARONES = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",
//																					 "QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F",																					 
//																					 "QS415","QS416","QS416_A",
//																					 "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		
//		
//		
//
//		seccionesGrabado40A49_S4_VARONES = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413",
//																							 "QS415","QS416","QS416_A",
//																					         "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		
//		
//		seccionesGrabado50A59_S4_VARONES = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F",
//																							 "QS415","QS416","QS416_A",
//																							 "QS601A","QS601B","QS603","QS604",
//																					         "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		
//		seccionesGrabado60aMAS_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS401","QS402","QS403","QS404",
//																					  "QS406","QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",
//																					  "QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F",
//																					  "QS415","QS416","QS416_A",
//																					  "QS601A","QS601B","QS603","QS604",
//																					  "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		
//		
//		seccionesGrabado40A49_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
//		seccionesGrabado50A59_S4 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604",
//																					 "QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};

		SeccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "NRO_INFO_S")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03).textSize(21).centrar().negrita(); 
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);		
		lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_ind).negrita().centrar().colorFondo(R.color.griscabece);
					
		lbledadCH = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.cap01_03qstexto2).textSize(16).colorTexto(color.red);
		lbledadCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		
		lbledadCI = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.cap01_03qstexto3).textSize(16).colorTexto(color.red);
		lbledadCI2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		
		lbledadCS = new LabelComponent(this.getActivity()).size(altoComponente, 300).text(R.string.cap01_03qstexto1).textSize(16);
		txtQS23=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqrgQS23ChangeValue").centrar();
		
		lblnombre = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.cap01_03qs20nombre).textSize(16).centrar();
		txtQH02_1 = new TextField(this.getActivity()).size(altoComponente, 300).maxLength(24).alinearIzquierda().soloTexto().readOnly();
		txtPERSONA_ID = new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2).alinearIzquierda().readOnly().centrar();
		lblsexo = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.cap01_03qs20sexo).textSize(16).centrar();
		rgQH06 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh06_1,R.string.seccion01qh06_2).size(altoComponente, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().readOnly();
		
		lbldiaCH = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.cap01_03qstexto4).textSize(16).colorTexto(color.red).centrar();
		lbldiaCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		lblmesCH = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.cap01_03qstexto5).textSize(16).colorTexto(color.red).centrar();
		lblmesCH2 = new LabelComponent(this.getActivity()).size(altoComponente, 50).textSize(17).colorTexto(color.red).centrar();
		
		lbldia = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.cap01_03qs22d).textSize(16).centrar();
		txtQS22D=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		
		lblmes = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.cap01_03qs22m).textSize(16).centrar();
		txtQS22M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		lblEspacio = new LabelComponent(this.getActivity()).size(altoComponente, 140);
		lblEspacio2 = new LabelComponent(this.getActivity()).size(altoComponente, 140);
		lblanioedad = new LabelComponent(this.getActivity()).size(altoComponente, 90).text(R.string.cap01_03qs22a).textSize(16).centrar();
		txtQS22A=new  IntegerField(this.getActivity()).size(altoComponente,100).maxLength(4) ;
		lblEspacio1 = new LabelComponent(this.getActivity()).size(altoComponente, 10);
		 
		rgQS24=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS24ChangeValue"); 
		
		lblnivel = new LabelComponent(this.getActivity()).size(40, 380).text(R.string.seccion01qh15).textSize(16).centrar().negrita();		
		rgQS25N=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs25n_1,R.string.cap01_03qs25n_2,R.string.cap01_03qs25n_3,R.string.cap01_03qs25n_4,R.string.cap01_03qs25n_5,R.string.cap01_03qs25n_6).size(300,380).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQs25nChangeValue"); 
		lblanio = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15y).textSize(16).centrar().negrita();
		rgQS25A=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15y_0,R.string.seccion01qh15y_1,R.string.seccion01qh15y_2,R.string.seccion01qh15y_3,R.string.seccion01qh15y_4,R.string.seccion01qh15y_5,R.string.seccion01qh15y_6).size(300,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH25AChangeValue");
		lblgrado = new LabelComponent(this.getActivity()).size(40, 180).text(R.string.seccion01qh15g).textSize(16).centrar().negrita();
		rgQS25G=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh15g_1,R.string.seccion01qh15g_2,R.string.seccion01qh15g_3,R.string.seccion01qh15g_4,R.string.seccion01qh15g_5,R.string.seccion01qh15g_6).size(300,180).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH25GChangeValue");
		
		lblpregunta20 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03_1).negrita();
		lblpregunta22 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs22);
		lblpregunta23 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs23);
		lblpregunta23_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs23_ind).negrita();
		
		lblpregunta23a = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs23a);
		rgQS23A=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs23a_1,R.string.cap01_03qs23a_2,R.string.cap01_03qs23a_3,R.string.cap01_03qs23a_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS23AChangeValue");
		txtQS23A_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 350).hint(R.string.especifique).centrar().alfanumerico().callback("onqrgQS23AOBSChangeValue");
		txtQS23A_N=new IntegerField(this.getActivity()).maxLength(100).size(altoComponente, 350).hint(R.string.especifique).centrar().maxLength(8).callback("onqrgQS23AOBSChangeValue");
		rgQS23A.agregarEspecifique(3,txtQS23A_O);
		rgQS23A.agregarEspecifique(0,txtQS23A_N);
		//rgQS23A.agregarEspecifique(2,txtQS23A_N);
		
		
		lblpregunta24 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs24);
		lblpregunta25 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs25n);
		lblpregunta25_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs25n_ind);
		
		
		lblpregunta25a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cspregunta25a).textSize(19);
		lblpregunta25b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cspregunta25b).textSize(19);
		rgQS25_A = new RadioGroupOtherField(getActivity(),R.string.cspregunta25a_1,R.string.cspregunta25a_2,R.string.cspregunta25a_3,R.string.cspregunta25a_4,R.string.cspregunta25a_5,R.string.cspregunta25a_6,R.string.cspregunta25a_7,R.string.cspregunta25a_8,R.string.cspregunta25a_9,R.string.cspregunta25a_10,R.string.cspregunta25a_11,R.string.cspregunta25a_12).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQS25_B = new RadioGroupOtherField(getActivity(),R.string.cspregunta25b_1,R.string.cspregunta25b_2,R.string.cspregunta25b_3,R.string.cspregunta25b_4,R.string.cspregunta25b_5,R.string.cspregunta25b_6,R.string.cspregunta25b_7,R.string.cspregunta25b_8,R.string.cspregunta25b_9).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS25BChangeValue");
		txtQS25A_O_1 = new TextField(getActivity()).size(altoComponente, 350).alfanumerico();
		txtQS25A_O_2 = new TextField(getActivity()).size(altoComponente, 350).alfanumerico();
		txtQS25B_O_1 = new TextField(getActivity()).size(altoComponente, 350).alfanumerico();
		txtQS25B_O_2 = new TextField(getActivity()).size(altoComponente+22, 350).alfanumerico();
		txtQS25B_O_3 = new TextField(getActivity()).size(altoComponente, 350).alfanumerico();
		rgQS25_A.agregarEspecifique(8, txtQS25A_O_1);
		rgQS25_A.agregarEspecifique(11, txtQS25A_O_2);
		rgQS25_B.agregarEspecifique(2, txtQS25B_O_1);
		rgQS25_B.agregarEspecifique(3, txtQS25B_O_2);
		rgQS25_B.agregarEspecifique(7, txtQS25B_O_3);
		
		gridTextoedad = new GridComponent(getActivity(),Gravity.LEFT, 2, 0);
		gridTextoedad.addComponent(lbledadCH);		
		gridTextoedad.addComponent(lbledadCH2);	
		
		gridTextoedadI = new GridComponent(getActivity(),Gravity.LEFT, 2, 0);
		gridTextoedadI.addComponent(lbledadCI);		
		gridTextoedadI.addComponent(lbledadCI2);	
	
		gridTextoedadCS = new GridComponent(getActivity(),0, 2, 0);
		gridTextoedadCS.addComponent(lbledadCS);		
		gridTextoedadCS.addComponent(txtQS23);	
		
		gridPreguntas20=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas20.addComponent(lblnombre);		
		gridPreguntas20.addComponent(txtQH02_1);	
		gridPreguntas20.addComponent(txtPERSONA_ID);
		gridPreguntas20.addComponent(lblsexo);
		gridPreguntas20.addComponent(rgQH06,2);
		
		gridPreguntas25=new GridComponent2(this.getActivity(),App.ESTILO,3);
		gridPreguntas25.addComponent(lblnivel);		
		gridPreguntas25.addComponent(lblanio);
		gridPreguntas25.addComponent(lblgrado);
		gridPreguntas25.addComponent(rgQS25N);
		gridPreguntas25.addComponent(rgQS25A);	
		gridPreguntas25.addComponent(rgQS25G);
		
		btnOmision = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnnosabe).size(130, 55);
		
		gridmesHogar = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridmesHogar.addComponent(lbldiaCH);
		gridmesHogar.addComponent(lbldiaCH2);
		gridmesHogar.addComponent(lblmesCH);
		gridmesHogar.addComponent(lblmesCH2);
		
		gdFechaNacimiento = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		
		gdFechaNacimiento.addComponent(lbldia);
		gdFechaNacimiento.addComponent(txtQS22D);
		gdFechaNacimiento.addComponent(lblEspacio2,2);
		gdFechaNacimiento.addComponent(lblmes);
		gdFechaNacimiento.addComponent(txtQS22M);
		gdFechaNacimiento.addComponent(lblEspacio,2);
		gdFechaNacimiento.addComponent(lblanioedad);
		gdFechaNacimiento.addComponent(txtQS22A);
		gdFechaNacimiento.addComponent(lblEspacio1);
		gdFechaNacimiento.addComponent(btnOmision);
		
		btnOmision.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                     txtQS22A.setText("9998");
             }
     });
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q6 = createQuestionSection(lblSubtitulo);
		q1 = createQuestionSection(gridPreguntas20.component());
		q7 = createQuestionSection(lblSubtitulo1);
		q2 = createQuestionSection(lblpregunta22,gridmesHogar.component(),gdFechaNacimiento.component()); 	 
		q3 = createQuestionSection(lblpregunta23,lblpregunta23_ind,gridTextoedad.component(),gridTextoedadI.component(),gridTextoedadCS.component()); 
		q10 = createQuestionSection(lblpregunta23a,rgQS23A);
		q4 = createQuestionSection(lblpregunta24,rgQS24); 
		q5 = createQuestionSection(lblpregunta25,lblpregunta25_ind,gridPreguntas25.component());  
		q8 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta25a,rgQS25_A);
		q9 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta25b,rgQS25_B);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q6);
		form.addView(q1);
		form.addView(q7); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q10);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q8); 
		form.addView(q9);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 

		uiToEntity(salud);
		informantesalud = App.getInstance().getPersonaSeccion01();
		informantesalud.qh06= Integer.parseInt(rgQH06.getTagSelected().toString()); 
		informantesalud.qh07 = salud.qs23;
		informantesalud.qh14 = salud.qs24;

		ModificarDatosParaBD();
		if (salud!=null) {			
			if (salud.qs25_b!=null) {
				salud.qs25_b=salud.setConvertqs25_b(salud.qs25_b);		
			}
		}
		
		if (salud.qs25n!=null) {
			salud.qs25n=salud.getConvertqs25n(salud.qs25n);
		}			
		if (salud.qs25a!=null) {
			salud.qs25a=salud.getConvertqs25a(salud.qs25a);
		}
		if (salud.qs25g!=null) {
			salud.qs25g=salud.getConvertqs25g(salud.qs25g);
		}
		
		informantesalud.qh15n = salud.qs25n;
		informantesalud.qh15y = salud.qs25a;
		informantesalud.qh15g = salud.qs25g;	
		
		salud.qs22d = !txtQS22D.getText().toString().equals("") ? Integer.parseInt(txtQS22D.getText().toString()) : null;
		salud.qs22m = !txtQS22M.getText().toString().equals("") ? Integer.parseInt(txtQS22M.getText().toString()) : null;
        salud.qs22a = !txtQS22A.getText().toString().equals("") ? Integer.parseInt(txtQS22A.getText().toString()) : null;

		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try {
			
			if(!Util.esDiferente(salud.qh06, 1) || (!Util.esDiferente(salud.qh06, 2) && Util.esMayor(salud.qs23, 49))){
				informantesalud.qh7dd = salud.qs22d.toString().length()>1?salud.qs22d.toString():"0"+salud.qs22d.toString();
				informantesalud.qh7mm = salud.qs22m.toString().length()>1?salud.qs22m.toString():"0"+salud.qs22m.toString();	
			}
			
			if(salud.qs22a== null && salud.qs22m == null) {
				salud.qs22cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs22cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs22cons = fechareferencia;
				}			
			}		
			
			
			
			saludS4=new CAP04_07();
			saludS4.id=salud.id;
			saludS4.hogar_id=salud.hogar_id;
			saludS4.persona_id=salud.persona_id;
			
			if (salud.qh06==2) {
				Log.e("111","mujer" +salud.qh06);
				if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoEdadMef) || !getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoMEF)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 1.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qh06==1) {
				Log.e("111","<30 varon"+salud.qh06);
				salud.qs28=null;
				salud.qs29a=null;
				salud.qs29b=null;
				if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 2.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
			//Limpiar seccion 3
			
			if (salud.qs23<50) {
				if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoS3_1)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 3.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qs23<60) {
				if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoS3_2)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 4.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
			//Limpiar seccion 4
			if (salud.qs23>75) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_1)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 5.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qs23<18 || salud.qs23>75) { //25 70
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_2)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 6.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
			if (salud.qh06==1 || (salud.qh06==2 && (salud.qs23<30 || salud.qs23>70 ))) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_3a)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 7.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qh06==1 || (salud.qh06==2 && (salud.qs23<25 || salud.qs23>64 ))) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_3b)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 8.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
			if (salud.qh06==1 || (salud.qh06==2 && (salud.qs23<40 || salud.qs23>70 ))) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_4a)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 9.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qh06==2 || (salud.qh06==1 && (salud.qs23<50 || salud.qs23>75 ))) {//70
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS4_4b)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 10.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
			
			//Limpiar seccion 6
			if (salud.qs23>50) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS6_1)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 11.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			if (salud.qs23>29) {
				if(!getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabadoS6_2)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 12.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}
			
/*		
			
			if (salud.qs23<25) {							
				if (salud.qs23<25 && salud.qh06==2) {
//					Log.e("111","<30 mujer" +salud.qs23);
					if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoEdadMef) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoMEF) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado15A29_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado15A24_S4)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 1.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}			
				}
				else{
//					Log.e("111","<30 varon"+salud.qs23);
					salud.qs28=null;
					salud.qs29a=null;
					salud.qs29b=null;
					if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado15A29_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado15A24_S4)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 2.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}				
			}	
			
			else if (salud.qs23>29 && salud.qs23<40) {				
				if (salud.qs23>29 && salud.qs23<40 && salud.qh06==2) {
//					Log.e("111","30 a 39 mujer"+salud.qs23);
					if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoEdadMef) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoMEF) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado30A39_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado30A39_S4)){ 
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 3.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
							return false; 
						}
					}
					else{
//						Log.e("111","30 a 39 varon"+salud.qs23);
						salud.qs28=null;
						salud.qs29a=null;
						salud.qs29b=null;
						if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
								|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado30A39_S3) 
								|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado30A39_S4_VARONES)){ 
							ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 4.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
							return false; 
						}
					}
			}
			else if (salud.qs23>39 && salud.qs23<50) {
				if (salud.qs23>39 && salud.qs23<50 && salud.qh06==2) {
//					Log.e("111","40 a 49 mujer"+salud.qs23);
					if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoEdadMef) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoMEF) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado40A49_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado40A49_S4)){
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 5.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}
				else{
//					Log.e("111","40 a 49 varon"+salud.qs23);
					salud.qs28=null;
					salud.qs29a=null;
					salud.qs29b=null;
					if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado40A49_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado40A49_S4_VARONES)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 6.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}				
			}
		
			else if (salud.qs23>49 && salud.qs23<60) {
				if (salud.qs23>49 && salud.qs23<60 && salud.qh06==2) {
//					Log.e("111","50 a 59 mujer"+salud.qs23);
					salud.qs28=null;
					salud.qs29a=null;
					salud.qs29b=null;
					if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado50A59_S4)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 7.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}
				else{
//					Log.e("111","50 a 59 varon"+salud.qs23);
					salud.qs28=null;
					salud.qs29a=null;
					salud.qs29b=null;
					if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
							|| !getCuestionarioService().saveOrUpdate(salud,seccionesGrabado50A59_S3) 
							|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado50A59_S4_VARONES)){ 
						ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 8.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
						return false; 
					}
				}			
			}
			else if (salud.qs23>59) {
//				Log.e("111","60 a mas"+salud.qs23);
				salud.qs28=null;
				salud.qs29a=null;
				salud.qs29b=null;
				if(!getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado) 
						|| !getCuestionarioService().saveOrUpdate(saludS4,seccionesGrabado60aMAS_S4)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 9.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false; 
				}
			}			
*/
			
			if (!Util.esDiferente(salud.qs24,1) && !Util.esDiferente(salud.qh06,1) && Util.esMenor(salud.qs23,25)){		
				Log.e("1","1");
				if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoSeccion02)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 13.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false;
				}
			}
			if ((!Util.esDiferente(salud.qs24,2) && !Util.esDiferente(salud.qh06,1) && Util.esMenor(salud.qs23,25))
				 || (!Util.esDiferente(salud.qh06,1) && Util.esMayor(salud.qs23,24))
				 || (!Util.esDiferente(salud.qh06,2) && Util.esMayor(salud.qs23,49))){
				Log.e("2","2");
				if(!getSeccion01Service().saveOrUpdate(informantesalud, SeccionesGrabadoSeccion01)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 14.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return false;
				}
			}
				
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().setPersonabySalud(salud);
	
		
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS1_3().qs23=salud.qs23;		
		try {
			getCuestionarioService().saveOrUpdate(llenerInformantedelHogar(), SeccionesGrabadoHogar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InformatedeSaludCorrecto();
		return true; 
    } 
    
    public void InformatedeSaludCorrecto() {
		Integer personaId=SeleccionarInformantedeSalud();
		if (personaId!=App.getInstance().getPersonaSeccion01().persona_id) {
			ActulializaPersona_idInformantedeSalud();
			validarMensaje("Existe otro seleccionado del Informate");
			parent.nextFragment(CuestionarioFragmentActivity.CARATULA);
			return;
		}
	}
    
    public void ActulializaPersona_idInformantedeSalud(){
    	if(salud!=null && salud.id!=null && salud.hogar_id!=null)
			getSeccion01Service().ModificarEnlaceInformanteDeSalud(salud.id,salud.hogar_id, SeleccionarInformantedeSalud());
	}
    
       
    public Hogar llenerInformantedelHogar(){
    	Hogar hogar = new Hogar();
		hogar.id= App.getInstance().getHogar().id;
		hogar.hogar_id=App.getInstance().getHogar().hogar_id;
		hogar.nro_info_s=App.getInstance().getPersonaSeccion01().persona_id;
		return hogar;
    }
    public Integer SeleccionarInformantedeSalud(){
  		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getSeccion01Service());
  		return persona_id;
    }
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		 if(!Util.esDiferente(salud.qh06, 1) || (!Util.esDiferente(salud.qh06, 2) && (Util.esMenor(salud.qs23,15) || Util.esMayor(salud.qs23, 49)))){
			 
			 if (Util.esVacio(salud.qs22d)) {
				 mensaje = preguntaVacia.replace("$", "La pregunta P.22D");
				 view = txtQS22D; 
				 error = true; 
				 return false; 
			 }
			 
			 if (Util.esVacio(salud.qs22m)) { 
				 mensaje = preguntaVacia.replace("$", "La pregunta P.22M"); 
				 view = txtQS22M; 
				 error = true; 
				 return false; 
			 } 
			 if (Util.esVacio(salud.qs22a)) { 
				 mensaje = preguntaVacia.replace("$", "La pregunta P.22A"); 
				 view = txtQS22A; 
				 error = true; 
				 return false; 
			 } 
			 
			 if(Util.esVacio(salud.qs23a)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.23A");
				 view = rgQS23A; 
				 error = true; 
				 return false; 
			}
			if(!Util.esDiferente(salud.qs23a, 4)){
				if (txtQS23A_O.getText().toString().isEmpty()) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS23A_O; 
				 error = true; 
				 return false; 
				}
			}
			
			if(!Util.esDiferente(salud.qs23a, 1) || !Util.esDiferente(salud.qs23a, 2) ){
				if (txtQS23A_N.getText().toString().isEmpty()) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS23A_N; 
				 error = true; 
				 return false; 
				}
				
				 String phonePattern = "^[0-9]{8}$";
	   			 if(!salud.qs23a_n.matches(phonePattern)){
	   				 mensaje = "N�mero de DNI no v�lido"; 
	   				 view = txtQS23A_N; 
	   				 error = true; 
	   				 return false; 
	   			 }
				
			}
				
			if (Util.esVacio(salud.qs24)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.24"); 
				view = rgQS24; 
				error = true; 
				return false; 
			} 
			
			if (!Util.esDiferente(salud.qs24,1)) {
				if (Util.esVacio(salud.qs25n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.25N"); 
					view = rgQS25N; 
					error = true; 
					return false; 
				} 
			
					if (Util.esVacio(salud.qs25a) && Util.esVacio(salud.qs25g)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.25A"); 
						view = rgQS25A; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(salud.qs25g) && Util.esVacio(salud.qs25a)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.25G"); 
						view = rgQS25G; 
						error = true; 
						return false; 
					}				
			}
			
			if(Util.esVacio(salud.qs25_a)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.25_A"); 
				 view = rgQS25_A; 
				 error = true; 
				 return false; 
			}
			if(!Util.esDiferente(salud.qs25_a, 9)){
				if (txtQS25A_O_1.getText().toString().isEmpty()) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS25A_O_1; 
				 error = true; 
				 return false; 
				}
			}
			if(!Util.esDiferente(salud.qs25_a, 12)){
				if (txtQS25A_O_2.getText().toString().isEmpty()) { 
				 mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS25A_O_2; 
				 error = true; 
				 return false; 
				}
			}
			if(Util.esVacio(salud.qs25_b)){
				 mensaje = preguntaVacia.replace("$", "La pregunta P.25_B"); 
				 view = rgQS25_B; 
				 error = true; 
				 return false; 
			}
			if(!Util.esDiferente(salud.qs25_b, 3)){
				if (txtQS25B_O_1.getText().toString().isEmpty()) { 
				 mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS25B_O_1; 
				 error = true; 
				 return false; 
				}
			}
			if(!Util.esDiferente(salud.qs25_b, 4)){
				if (txtQS25B_O_2.getText().toString().isEmpty()) { 
				 mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS25B_O_2; 
				 error = true; 
				 return false; 
				}
			}
			if(!Util.esDiferente(salud.qs25_b, 8)){
				if (txtQS25B_O_3.getText().toString().isEmpty()) { 
				 mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				 view = txtQS25B_O_3; 
				 error = true; 
				 return false; 
				}
			}
			
			
		
			if (Util.esMenor(salud.qs22d,1) || Util.esMayor(salud.qs22d,31) ) {
				mensaje = "D�a fuera de Rango"; 
				view = txtQS22D; 
				error = true; 
				return false; 
			}
			
			if (Util.esMenor(salud.qs22m,1) || Util.esMayor(salud.qs22m,12) ) {
				mensaje = "Mes fuera de Rango"; 
				view = txtQS22M; 
				error = true; 
				return false; 
			}
			if((Util.esMenor(salud.qs22a, App.ANIOPORDEFECTOSUPERIOR-100) || Util.esMayor(salud.qs22a, App.ANIOPORDEFECTOSUPERIOR-15)) && Util.esDiferente(salud.qs22a, 9998) ){
				mensaje = "A�o fuera de Rango"; 
				view = txtQS22A; 
				error = true; 
				return false; 
			}

			if (Util.esDiferente(Integer.parseInt(salud.qh7dd.toString()),salud.qs22d, salud.qs22d)) {
				validarMensaje("Se corrigi� el D�a de Nacimiento en el Cuestionario del Hogar");
			}
			
			if (Util.esDiferente(Integer.parseInt(salud.qh7mm.toString()),salud.qs22m, salud.qs22m)) {
				validarMensaje("Se corrigi� el Mes de Nacimiento en el Cuestionario del Hogar");
			}	
			
			if (Util.esDiferente(salud.qh14, salud.qs24)) {
				validarMensaje("Se corrigi� la P.14 el Cuestionario del Hogar");
			}
			if (Util.esDiferente(salud.qh15n, salud.qs25n)) {
				validarMensaje("Se corrigi� la P.15 Nivel el Cuestionario del Hogar");
			}
			if (Util.esDiferente(salud.qh15y, salud.qs25a)) {
				validarMensaje("Se corrigi� la P.15 A�o el Cuestionario del Hogar");
			}
			if (Util.esDiferente(salud.qh15g, salud.qs25g)) {
				validarMensaje("Se corrigi� la P.15 Grado el Cuestionario del Hogar");
			}
		 }
		 
		if (Util.esVacio(salud.qs23)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.23"); 
			view = txtQS23; 
			error = true; 
			return false; 
		} 
			
		if(Util.esMenor(salud.qs23,15)){
			mensaje = "La edad no puede ser menor a 15"; 
			view = txtQS23; 
			error = true; 
			return false; 
		}
		 
		if (Util.esDiferente(salud.qh07,salud.qs23)) {
			validarMensaje("Se corrigi� Edad en Cuestionario del Hogar");
		}			
		
		
		if(Util.esVacio(salud.qs23a)){
			 mensaje = preguntaVacia.replace("$", "La pregunta P.23A");
			 view = rgQS23A; 
			 error = true; 
			 return false; 
		}
		if(!Util.esDiferente(salud.qs23a, 4)){
			if (txtQS23A_O.getText().toString().isEmpty()) { 
			mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
			 view = txtQS23A_O; 
			 error = true; 
			 return false; 
			}
		}
		
		if(!Util.esDiferente(salud.qs23a, 1) || !Util.esDiferente(salud.qs23a, 2) ){
			if (txtQS23A_N.getText().toString().isEmpty()) { 
			mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
			 view = txtQS23A_N; 
			 error = true; 
			 return false; 
			}
			
			 String phonePattern = "^[0-9]{8}$";
  			 if(!salud.qs23a_n.matches(phonePattern)){
  				 mensaje = "N�mero de DNI no v�lido"; 
  				 view = txtQS23A_N; 
  				 error = true; 
  				 return false; 
  			 }
		}
		

		SeccionCapitulo[] seccionescargadovisita= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSVRESUL")};
		CSVISITA visitacompletada = getCuestionarioService().getVisitaSaludCompletada(salud.id,salud.hogar_id, seccionescargadovisita);
		if(!validarEsMef())	{
			if(visitacompletada==null || visitacompletada.qsvresul !=1){
				Calendar fechadenacimiento = new GregorianCalendar(salud.qs22a, salud.qs22m-1, salud.qs22d);
				Calendar fecharef = new GregorianCalendar();
				
				if(fechareferencia!=null){
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date2 = null;
					try {
						date2 = df.parse(fechareferencia);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Calendar cal = Calendar.getInstance();
					cal.setTime(date2);
					fecharef=cal;
				}
	
			
				if(salud.qs22a != 9998 ) {
//					Log.e("",""+MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef));
//					Log.e("","INGRESADO: "+salud.qs23);
//					Log.e("","INGRESADO: "+salud.qs22cons);
					if(Util.esDiferente(MyUtil.CalcularEdadByFechaNacimiento(fechadenacimiento,fecharef), salud.qs23) && !salud.qs22m.equals("98") && salud.qs22a!=9998){
						mensaje = "Edad no corresponde con P.23"; 
						view = txtQS23; 
						error = true; 
						return false;
				    }
				}
			}
		}
		return true; 
    } 
    
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    @Override 
    public void cargarDatos() { 
    	
    	MyUtil.LiberarMemoria();
      	if(App.getInstance().getPersonaSeccion01()!=null){
		   salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
     		
		   if(salud.qs23a_o != null){
			  // ToastMessage.msgBox(getActivity(), salud.qs23a_o.toString(), ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
		   }else{
			   //ToastMessage.msgBox(getActivity(), "es un nulo", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
		   }
		   
			  if(salud==null){ 
				  salud=new Salud(); 
				  salud.id=App.getInstance().getPersonaSeccion01().id; 
				  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
				  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 		  
			    } 
			  if (salud.qs25n!=null) {
					salud.qs25n=salud.setConvertqs25n(salud.qs25n);
				}
				if (salud.qs25a!=null) {
					salud.qs25a=salud.setConvertqs25a(salud.qs25a);
				}
				if (salud.qs25g!=null) {
					salud.qs25g=salud.setConvertqs25g(salud.qs25g);
				}
		    
		    if (salud.qs22a  != null) {
		        int dia=99; 
		        int mes=99;
		        txtQS22A.setValue(Util.getFecha(Integer.valueOf(salud.qs22a),mes,dia));
		    }
		    if(salud.qs22m != null)
		    {
		  	  int dia=99;
		  	  int anio=9999;
		  	  txtQS22M.setValue(Util.getFecha(anio, Integer.valueOf(salud.qs22m), dia));
		    }
		    
		    salud.qs25_b=salud.qs25_b!=null?salud.getConvertqs25_b(salud.qs25_b):null;
		    
				entityToUI(salud); 
				ModificarDatosparaUsuario();
				
				fechareferencia = salud.qs22cons;
				
				txtQH02_1.setText("["+salud.persona_id+"]"+txtQH02_1.getText().toString());
				inicio();
	   }
      
    } 
    public void VisitaResultado(){
    	if(MyUtil.incluyeRango(2, 9, App.getInstance().getC1Visita().qsresult) && App.getInstance().getC1Visita().qsresult!=5){
    		q0.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
    	}
    }
    private void inicio() {     	
    	esMef();       	
    	ValidarsiesSupervisora(); 
    	VisitaResultado();
    	onqrgQS23AChangeValue();
    	txtCabecera.requestFocus();
    } 
 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQS22M.readOnly();
			txtQS22A.readOnly();
			txtQS23.readOnly();
			rgQS24.readOnly();
			rgQS25A.readOnly();
			rgQS25G.readOnly();
			rgQS25N.readOnly();		
			rgQS25_A.readOnly();
			rgQS25_B.readOnly();
			txtQS25A_O_1.readOnly();
			txtQS25A_O_2.readOnly();
			txtQS25B_O_1.readOnly();
			txtQS25B_O_2.readOnly();
			txtQS25B_O_3.readOnly();
			Util.cleanAndLockView(getActivity(),btnOmision);
		}
	}
    
    public boolean ConsistenciarEdad(Integer edad,Integer anio,Integer mes){	
    	boolean flag=false;
    	if(anio.equals(9998)){
    		flag=true;
    	}
    	else{    	
    		Calendar date = new GregorianCalendar();
    		Integer anios= date.get(Calendar.YEAR)-anio;
	    	if(!Util.esDiferente(date.get(Calendar.MONTH),mes,mes)){
	    		if(!Util.esDiferente(edad,(anios-1)) || !Util.esDiferente(edad,anios)){
	    			flag=true;
	    		}
	    	}
	    	else{
	    		if(Util.esMayor(mes, date.get(Calendar.MONTH))){    		
	    			if(edad==(anios-1)){
	    				flag=true;
	    			}	
	    		}
	    		else{
	    			if(!Util.esDiferente(anios, edad)){    				
	    				flag=true;
	    			}
	    		}    			
	    	}
    	}
    	return flag;
    }
    	
   
    public boolean validarEsMef() {
    	salud.qs23=App.getInstance().getPersonaSeccion01().qh07;
   		return !Util.esDiferente(salud.qh06, 2) && Util.esMenor(salud.qs23, 50);
    }
    
	public void esMef() {
		salud.qs23=App.getInstance().getPersonaSeccion01().qh07;
   		if (!Util.esDiferente(salud.qh06, 2) && Util.esMenor(salud.qs23, 50)){ 
   			Util.cleanAndLockView(getActivity(),txtQS22A,txtQS22M,rgQS24,rgQS25N,rgQS25A,rgQS25G,rgQS25_A,rgQS25_B,txtQS25A_O_1,txtQS25A_O_2,txtQS25B_O_1,txtQS25B_O_2,txtQS25B_O_3);
   			q2.setVisibility(View.GONE);
   			q4.setVisibility(View.GONE);
   			q5.setVisibility(View.GONE);
   			q8.setVisibility(View.GONE);
   			q9.setVisibility(View.GONE);
   			
   			gridTextoedad.setVisibility(View.GONE);
   			gridTextoedadI.setVisibility(View.VISIBLE);
   			txtQS23.readOnly();
   			txtQS23.setText(salud.qh07.toString());  
   			lbledadCI2.setText(salud.qh07.toString());
	   		
	   	 }
   		if (!Util.esDiferente(salud.qh06, 1) ||( !Util.esDiferente(salud.qh06, 2) && Util.esMayor(salud.qs23, 49))){
	   		Util.lockView(getActivity(), false,txtQS22A,txtQS22M,rgQS24,rgQS25N,rgQS25A,rgQS25G,rgQS25_A,rgQS25_B,txtQS25A_O_1,txtQS25A_O_2,txtQS25B_O_1,txtQS25B_O_2,txtQS25B_O_3);
	   		q2.setVisibility(View.VISIBLE);
	   		q4.setVisibility(View.VISIBLE);
	   		q5.setVisibility(View.VISIBLE);
	   		q8.setVisibility(View.VISIBLE);
   			q9.setVisibility(View.VISIBLE);
	   		gridTextoedad.setVisibility(View.VISIBLE);
	   		gridTextoedadI.setVisibility(View.GONE);
	   		lbledadCH2.setText(salud.qh07==null?"":salud.qh07.toString());
	   		lbldiaCH2.setText(salud.qh7dd==null?"":salud.qh7dd.toString());
	   		lblmesCH2.setText(salud.qh7mm==null?"":salud.qh7mm.toString());
	   		onqrgQS24MEFChangeValue();
	   	 }
	}
	
    public void onqrgQS23ChangeValue() {
   		if (!Util.esDiferente(salud.qh06, 2) && MyUtil.incluyeRango(15,49,txtQS23.getText().toString())){	
	   		Util.cleanAndLockView(getActivity(),txtQS22A,txtQS22M,rgQS24,rgQS25N,rgQS25A,rgQS25G,rgQS25_A,rgQS25_B,txtQS25A_O_1,txtQS25A_O_2,txtQS25B_O_1,txtQS25B_O_2,txtQS25B_O_3);
	   		q2.setVisibility(View.GONE);
	   		q4.setVisibility(View.GONE);
	   		q5.setVisibility(View.GONE);	
	   		q8.setVisibility(View.GONE);
   			q9.setVisibility(View.GONE);
	   		gridTextoedad.setVisibility(View.GONE);
   			gridTextoedadI.setVisibility(View.VISIBLE);
   			lbledadCI2.setText(salud.qh07.toString());
	   		
	   	 }
   		if (!Util.esDiferente(salud.qh06, 1) || (!Util.esDiferente(salud.qh06, 2) && MyUtil.incluyeRango(50,97,txtQS23.getText().toString()))){
	   		Util.lockView(getActivity(), false,txtQS22A,txtQS22M,rgQS24,rgQS25N,rgQS25A,rgQS25G,rgQS25_A,rgQS25_B,txtQS25A_O_1,txtQS25A_O_2,txtQS25B_O_1,txtQS25B_O_2,txtQS25B_O_3);
	   		q2.setVisibility(View.VISIBLE);
	   		q4.setVisibility(View.VISIBLE);
	   		q5.setVisibility(View.VISIBLE);
	   		q8.setVisibility(View.VISIBLE);
   			q9.setVisibility(View.VISIBLE);
	   		gridTextoedad.setVisibility(View.VISIBLE);
	   		gridTextoedadI.setVisibility(View.GONE);
	   		lbledadCH2.setText(salud.qh07.toString());
	   		lbldiaCH2.setText(salud.qh7dd.toString());
	   		lblmesCH2.setText(salud.qh7mm.toString());
	   		//onqrgQS24ChangeValue();
	   		rgQS23A.requestFocus();
	   	 }
	}
    
    public void onqrgQS23AChangeValue() {
    	if (rgQS23A.getTagSelected("").toString().contains("2") || rgQS23A.getTagSelected("").toString().contains("1") ){ //!Util.esDiferente(salud.qs23a, 2) || !Util.esDiferente(salud.qs23a, 1)
    		Util.lockView(getActivity(), false,txtQS23A_N);
    		txtQS23A_N.requestFocus();
    	}else if(rgQS23A.getTagSelected("").toString().contains("3")){
    		rgQS24.requestFocus();
    	}else{
    		Util.cleanAndLockView(getActivity(),txtQS23A_N);
    	}
    	
    	
    }
    
    public void onqrgQS23AOBSChangeValue() {
    	rgQS24.requestFocus();
    }
    
    
    
    public void onqrgQS24MEFChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQS24.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
      		Util.cleanAndLockView(getActivity(),rgQS25N,rgQS25A,rgQS25G);
    		q5.setVisibility(View.GONE);
    	} 
		else {
			Util.lockView(getActivity(), false,rgQS25N,rgQS25A,rgQS25G);
			q5.setVisibility(View.VISIBLE);
			onrgQs25nChangeValue();
		}	
	}
	
    public void onqrgQS24ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQS24.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
      		Util.cleanAndLockView(getActivity(),rgQS25N,rgQS25A,rgQS25G);
    		q5.setVisibility(View.GONE);
    		rgQS25_A.requestFocus();
    	} 
		else {
			Util.lockView(getActivity(), false,rgQS25N,rgQS25A,rgQS25G);
			q5.setVisibility(View.VISIBLE);
			onrgQs25nChangeValue();
			rgQS25N.requestFocus();
		}	
	}

	
	public void onrgQs25nChangeValue() {
		if (!MyUtil.incluyeRango(2,2,rgQS24.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(1,1,rgQS25N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQS25G);			
				Util.lockView(getActivity(), false,rgQS25A);
				rgQS25A.lockButtons(true,1,2,3,4,5,6);
				rgQS25A.requestFocus();
			}	
			if (MyUtil.incluyeRango(3,4,rgQS25N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQS25G);			
				Util.lockView(getActivity(), false,rgQS25A);	
				rgQS25A.lockButtons(true,0,6);
				rgQS25G.requestFocus();
			}
			if (MyUtil.incluyeRango(5,5,rgQS25N.getTagSelected("").toString())) {		
				Util.cleanAndLockView(getActivity(),rgQS25G);			
				Util.lockView(getActivity(), false,rgQS25A);	
				rgQS25A.lockButtons(true,0);
				rgQS25G.requestFocus();
			}
			if (MyUtil.incluyeRango(6,6,rgQS25N.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),rgQS25G);			
				Util.lockView(getActivity(), false,rgQS25A);	
				rgQS25A.lockButtons(true,0,3,4,5,6);
				rgQS25G.requestFocus();
			}
			
			
			if (MyUtil.incluyeRango(15,47,txtQS23.getText().toString())) {
				if (MyUtil.incluyeRango(2,2,rgQS25N.getTagSelected("").toString())) {				
					Util.cleanAndLockView(getActivity(),rgQS25A);	
					Util.lockView(getActivity(), false,rgQS25G);				
					rgQS25G.requestFocus();
				}	
			}else{
				if (MyUtil.incluyeRango(2,2,rgQS25N.getTagSelected("").toString())) {		
					Util.lockView(getActivity(), false,rgQS25A,rgQS25G);
					rgQS25A.lockButtons(true,6);
					onrgQH25AChangeValue();
					onrgQH25GChangeValue();
					rgQS25G.requestFocus();
				}
			}			
		}
	}
	
	public void onrgQH25AChangeValue() {
		if (MyUtil.incluyeRango(47,97,txtQS23.getText().toString()) && MyUtil.incluyeRango(1,5,rgQS25A.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQS25N.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQS25G);
			Util.lockView(getActivity(), false,rgQS25G);
		}	
		rgQS25_A.requestFocus();
	}
	public void onrgQH25GChangeValue() {
		if (MyUtil.incluyeRango(47,97,txtQS23.getText().toString()) && MyUtil.incluyeRango(1,6,rgQS25G.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQS25N.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),rgQS25A);
			Util.lockView(getActivity(), false,rgQS25A);
			rgQS25A.lockButtons(true,0,6);
		}
		rgQS25_A.requestFocus();
	}
	
	public void ModificarDatosParaBD(){
		if(!Util.esDiferente(salud.qs25_a, 9)){
			salud.qs25_ao=txtQS25A_O_1.getText().toString().trim();
		}
		if(!Util.esDiferente(salud.qs25_a, 12)){
			salud.qs25_ao=txtQS25A_O_2.getText().toString().trim();
		}
		if(!Util.esDiferente(salud.qs25_b, 3)){
			salud.qs25_bo=txtQS25B_O_1.getText().toString().trim();
		}
		if(!Util.esDiferente(salud.qs25_b, 4)){
			salud.qs25_bo=txtQS25B_O_2.getText().toString().trim();
		}
		if(!Util.esDiferente(salud.qs25_b, 8)){
			salud.qs25_bo=txtQS25B_O_3.getText().toString().trim();
		}
		
		
	}
    
	public void ModificarDatosparaUsuario(){
		if(!Util.esDiferente(salud.qs25_a, 9)){
			txtQS25A_O_1.setText(salud.qs25_ao);
		}
		if(!Util.esDiferente(salud.qs25_a, 12)){
			txtQS25A_O_2.setText(salud.qs25_ao);
		}
		if(!Util.esDiferente(salud.qs25_b, 3)){
			txtQS25B_O_1.setText(salud.qs25_bo);
		}
		if(!Util.esDiferente(salud.qs25_b, 4)){
			txtQS25B_O_2.setText(salud.qs25_bo);
		}
		if(!Util.esDiferente(salud.qs25_b, 8)){
			txtQS25B_O_3.setText(salud.qs25_bo);
		}
	}
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);
		informantesalud = App.getInstance().getPersonaSeccion01();
        
		try {
					
			if (salud.qs25n!=null) {
				salud.qs25n=salud.getConvertqs25n(salud.qs25n);
			}			
			if (salud.qs25a!=null) {
				salud.qs25a=salud.getConvertqs25a(salud.qs25a);
			}
			if (salud.qs25g!=null) {
				salud.qs25g=salud.getConvertqs25g(salud.qs25g);
			}
			
			if (salud.qs22m!=null) {
				salud.qs22m = !txtQS22M.getText().toString().equals("") ? Integer.parseInt(txtQS22M.getText().toString()) : null;	
			}
			if (salud.qs212a!=null) {
				salud.qs22a = !txtQS22A.getText().toString().equals("") ? Integer.parseInt(txtQS22A.getText().toString()) : null;	
			}
			
			if(salud.qs22a== null && salud.qs22m == null) {
				salud.qs22cons = null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs22cons =  Util.getFechaActualToString();
				}
				else {
					salud.qs22cons = fechareferencia;
				}			
			}		
	        
	        
			boolean flag=false;
			if(!Util.esDiferente(salud.qh06, 1) || (!Util.esDiferente(salud.qh06, 2) && Util.esMayor(salud.qs23, 49))){
				flag=getCuestionarioService().saveOrUpdate(salud,SeccionesGrabado);
			}
			if(!Util.esDiferente(salud.qh06, 2) && Util.esMayor(salud.qs23, 49)){
				flag=getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoMEF);
			}
											
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public Seccion01Service getSeccion01Service(){
    	if(seccion01service==null){
    		seccion01service =  Seccion01Service.getInstance(getActivity());
    	}
    return seccion01service;
    }
    
	public VisitaService getVisitaService(){
		if(visitaService==null)
		{
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}

	public boolean CalcularEdadbyMesAnhio(Integer anio,Integer mes,Integer edad,Calendar fechareferencia) {
//    	Calendar fechaactual=fechareferencia;
//    	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
//    	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
//    	 if(mes<0){
//    		anio--;
//    	}
//    	 Log.e("","EDAD:  "+anio);
//    	return anio;
		boolean flag =false;
		if(anio.equals(9998)){
    		flag=true;
    	}
    	else{    	
    		Integer anios= fechareferencia.get(Calendar.YEAR)-anio;
	    	if(!Util.esDiferente(fechareferencia.get(Calendar.MONTH),mes,mes)){
	    		if(!Util.esDiferente(edad,(anios-1)) || !Util.esDiferente(edad,anios)){
	    			flag=true;
	    		}
	    	}
	    	else{
	    		if(Util.esMayor(mes, fechareferencia.get(Calendar.MONTH))){    		
	    			if(edad==(anios-1)){
	    				flag=true;
	    			}	
	    		}
	    		else{
	    			if(!Util.esDiferente(anios, edad)){    				
	    				flag=true;
	    			}
	    		}    			
	    	}
    	}
    	return flag;
    }
}
