package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_02Fragment_012 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS217; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQS217A; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS218; 
	@FieldAnnotation(orderIndex=4) 
	public DecimalField txtQS218A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS219; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS219A; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS220; 
	@FieldAnnotation(orderIndex=8) 
	public DecimalField txtQS220A;  
	
	Salud salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	public GridComponent2 gridPreguntas217,gridPreguntas218,gridPreguntas219,gridPreguntas220;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public LabelComponent lblpregunta217,lblpregunta218,lblpregunta218_ind,lblpregunta219,lblpregunta220,lblpregunta220_ind;
	
	String qs217fechref;
	String qs219fechref;
	
	public CS_02Fragment_012() {} 
	public CS_02Fragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQS217A, 1, 7, null, null); 
		rango(getActivity(), txtQS219A, 1, 7, null, null);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS217","QS217A","QS217FECH_REF","QS218","QS218A","QS219","QS219A","QS219FECH_REF","QS220","QS220A","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS217","QS217A","QS217FECH_REF","QS218","QS218A","QS219","QS219A","QS219FECH_REF","QS220","QS220A")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC02).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		
		lblpregunta217 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs217).textSize(19);
		lblpregunta218 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs218).textSize(19);
		lblpregunta218_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs218_ind).textSize(17).negrita();
		lblpregunta219 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs219).textSize(19);
		lblpregunta220 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs220).textSize(19);
		lblpregunta220_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs220_ind).textSize(17).negrita();
		
		rgQS217=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs217_1,R.string.cap01_03qs217_2,R.string.cap01_03qs217_3).size(altoComponente+60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS217ChangeValue"); 
		txtQS217A=new IntegerField(this.getActivity()).size(altoComponente+60, 80).maxLength(1); 
		
		rgQS218=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs218_1,R.string.cap01_03qs218_2).size(altoComponente+20,480).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS218ChangeValue"); 
		txtQS218A=new DecimalField(this.getActivity()).size(altoComponente+20, 80).maxLength(2).decimales(1); 
		
		rgQS219=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs219_1,R.string.cap01_03qs219_2,R.string.cap01_03qs219_3).size(altoComponente+60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS219ChangeValue"); 
		txtQS219A=new IntegerField(this.getActivity()).size(altoComponente+60, 80).maxLength(1); 
		
		rgQS220=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs220_1,R.string.cap01_03qs220_2,R.string.cap01_03qs220_3).size(altoComponente+60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS220ChangeValue"); 
		txtQS220A=new DecimalField(this.getActivity()).size(altoComponente+60, 80).maxLength(2).decimales(1); 
	
		gridPreguntas217=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas217.addComponent(rgQS217);		
		gridPreguntas217.addComponent(txtQS217A);
		
		gridPreguntas218=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas218.addComponent(rgQS218);		
		gridPreguntas218.addComponent(txtQS218A);
		
		gridPreguntas219=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas219.addComponent(rgQS219);		
		gridPreguntas219.addComponent(txtQS219A);
		
		gridPreguntas220=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas220.addComponent(rgQS220);		
		gridPreguntas220.addComponent(txtQS220A);

    } 
    @Override 
    protected View createUI() { 
		buildFields();  
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q5 = createQuestionSection(lblSubtitulo); 
		q1 = createQuestionSection(lblpregunta217,gridPreguntas217.component()); 
		q2 = createQuestionSection(lblpregunta218,lblpregunta218_ind,gridPreguntas218.component()); 
		q3 = createQuestionSection(lblpregunta219,gridPreguntas219.component()); 
		q4 = createQuestionSection(lblpregunta220,lblpregunta220_ind,gridPreguntas220.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);  
		form.addView(q0);
		form.addView(q5); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 	
		uiToEntity(salud);

		if (salud.qs217!=null) {
			salud.qs217=salud.getConvertqs215(salud.qs217);
			
			if(qs217fechref == null) {
			 	salud.qs217fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs217fech_ref = qs217fechref;
				 }
		}
		else {
			salud.qs217fech_ref = null;
		}
		if (salud.qs218!=null) {
			salud.qs218=salud.getConvertqs203(salud.qs218);
		}
		if (salud.qs219!=null) {
			salud.qs219=salud.getConvertqs215(salud.qs219);
			
			if(qs219fechref == null) {
			 	salud.qs219fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs219fech_ref = qs219fechref;
				 }
		}
		else {
			salud.qs219fech_ref = null;
		}  
		if (salud.qs220!=null) {
			salud.qs220=salud.getConvertqs100(salud.qs220);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(salud.qs217)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.217"); 
			view = rgQS217; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs217, 1)) {
			if (Util.esVacio(salud.qs217a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.217A"); 
				view = txtQS217A; 
				error = true; 
				return false; 
			}
			if (Util.esMenor(salud.qs217a,1) || Util.esMayor(salud.qs217a,7)) { 
				mensaje = "El rango es de 1 a 7"; 
				view = txtQS217A; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(salud.qs218)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.218"); 
				view = rgQS218; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs218, 1)) {
				if (Util.esVacio(salud.qs218a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.218A"); 
					view = txtQS218A; 
					error = true; 
					return false; 
				}
				if(salud.qs218a.doubleValue()==0 || salud.qs218a.doubleValue()%(0.5)!=0 || salud.qs218a.doubleValue()>9.5){
					mensaje ="Porci�n invalida "; 
					view = txtQS218A ; 
					error = true; 
					return false; 
				}
			}		 
		}
		
		 
		if (Util.esVacio(salud.qs219)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.219"); 
			view = rgQS219; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs219, 1)) {
			if (Util.esVacio(salud.qs219a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.219A"); 
				view = txtQS219A; 
				error = true; 
				return false; 
			} 
			if (Util.esMenor(salud.qs219a,1) || Util.esMayor(salud.qs219a,7)) { 
				mensaje = "El rango es de 1 a 7"; 
				view = txtQS219A; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(salud.qs220)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.220"); 
				view = rgQS220; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs220, 1)) {
				if (Util.esVacio(salud.qs220a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.220A"); 
					view = txtQS220A; 
					error = true; 
					return false; 
				} 
				if(salud.qs220a.doubleValue()==0 || salud.qs220a.doubleValue()%(0.5)!=0 || salud.qs220a.doubleValue()>9.5){
					mensaje ="Porci�n invalida "; 
					view = txtQS220A; 
					error = true; 
					return false; 
				}
			}	
			if (!Util.esDiferente(salud.qs220, 2)) {	
				if (Util.esVacio(salud.qs220a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.220A"); 
					view = txtQS220A; 
					error = true; 
					return false; 
				}
				if(salud.qs220a.doubleValue()%(4)==0){
					mensaje ="Convertir a Porci�n"; 
					view = txtQS220A; 
					error = true; 
					return false; 
				}
				if(salud.qs220a.doubleValue()%(1)!=0 || salud.qs220a.doubleValue()>9 ){
					mensaje ="La respueta debe ser un entero valido hasta 9"; 
					view = txtQS220A; 
					error = true; 
					return false; 
				}
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() {   
    	MyUtil.LiberarMemoria();
//    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	
    	if (salud.qs217!=null) {
			salud.qs217=salud.setConvertqs215(salud.qs217);
		}	
		if (salud.qs218!=null) {
			salud.qs218=salud.setConvertqs203(salud.qs218);
		}		
		if (salud.qs219!=null) {
			salud.qs219=salud.setConvertqs215(salud.qs219);
		}		
    	if (salud.qs220!=null) {
			salud.qs220=salud.setConvertqs100(salud.qs220);
		}
    	
    	entityToUI(salud); 
    	
    	qs217fechref = salud.qs217fech_ref;
    	qs219fechref = salud.qs219fech_ref;
    	inicio(); 
    
    	App.getInstance().setPersonabySalud(salud);
	}
    private void inicio() { 
    	RenombrarEtiqueta();
    	onqrgQS217ChangeValue();
    	onqrgQS219ChangeValue();
    	ValidarsiesSupervisora();
//    	rgQS217.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS217.readOnly();
			txtQS217A.readOnly();
			rgQS218.readOnly();
			txtQS218A.readOnly();
			rgQS219.readOnly();
			txtQS219A.readOnly();
			rgQS220.readOnly();
			txtQS220A.readOnly();
		}
	}
	
    public void RenombrarEtiqueta()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(qs217fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs217fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		} 
    	
    	
    	Calendar fechaayer= MyUtil.PrimeraFecha(fechaactual, 1);
    	Calendar fechaSemana= MyUtil.PrimeraFecha(fechaayer, 6);
    	lblpregunta217.setText(lblpregunta217.getText().toString().replace("F1",MyUtil.DiaDelaSemana(fechaSemana.get(Calendar.DAY_OF_WEEK))));
    	
    	fechaactual = new GregorianCalendar();
    	if(qs217fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs217fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		} 
    	
    	fechaayer= MyUtil.PrimeraFecha(fechaactual, 1);
    	fechaSemana= MyUtil.PrimeraFecha(fechaayer, 6);
    	
    	lblpregunta219.setText(lblpregunta219.getText().toString().replace("F1",MyUtil.DiaDelaSemana(fechaSemana.get(Calendar.DAY_OF_WEEK))));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS217ChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQS217.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),txtQS217A,rgQS218,txtQS218A); 
  			q2.setVisibility(View.GONE);
  			rgQS219.requestFocus();
  		} 
  		else {
  			Util.lockView(getActivity(),false,txtQS217A,rgQS218,txtQS218A);
  			q2.setVisibility(View.VISIBLE);
  			onqrgQS218ChangeValue();  		
  			txtQS217A.requestFocus();
  		}	
  	}
    public void onqrgQS218ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS217.getTagSelected("").toString())) {
	  		if (MyUtil.incluyeRango(2,2,rgQS218.getTagSelected("").toString())) {	  		
	  			Util.cleanAndLockView(getActivity(),txtQS218A);	
	  			rgQS219.requestFocus();
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,txtQS218A); 
	  			txtQS218A.requestFocus();
	  		}	
    	}
  	}
    public void onqrgQS219ChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQS219.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),txtQS219A,rgQS220,txtQS220A);
  			q4.setVisibility(View.GONE);
  			rgQS220.requestFocus();
  		} 
  		else {
  			Util.lockView(getActivity(),false,txtQS219A,rgQS220,txtQS220A);
  			q4.setVisibility(View.VISIBLE);
  			onqrgQS220ChangeValue();  	
  			txtQS219A.requestFocus();
  		}	
  	}
    public void onqrgQS220ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS219.getTagSelected("").toString())) {
	  		if (MyUtil.incluyeRango(1,1,rgQS220.getTagSelected("").toString())) {	  
	  			Util.lockView(getActivity(),false,txtQS220A);   
	  			txtQS220A.requestFocus();  			
	  		} 
	  		if (MyUtil.incluyeRango(2,2,rgQS220.getTagSelected("").toString())) {		  		
	  			Util.lockView(getActivity(),false,txtQS220A);   
	  			txtQS220A.requestFocus();  			
	  		}
	  		if (MyUtil.incluyeRango(3,3,rgQS220.getTagSelected("").toString())) {		  		
	  			Util.cleanAndLockView(getActivity(),txtQS220A);	  			
	  		}	  		
    	}
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);

		if (salud.qs217!=null) {
			salud.qs217=salud.getConvertqs215(salud.qs217);
			if(qs217fechref == null) {
			 	salud.qs217fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs217fech_ref = qs217fechref;
				 }
		}
		else {
			salud.qs217fech_ref = null;
		}
		if (salud.qs218!=null) {
			salud.qs218=salud.getConvertqs203(salud.qs218);
		}
		if (salud.qs219!=null) {
			salud.qs219=salud.getConvertqs215(salud.qs219);
			if(qs219fechref == null) {
			 	salud.qs219fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs219fech_ref = qs219fechref;
				 }
		}
		else {
			salud.qs219fech_ref = null;
		}		
		if (salud.qs220!=null) {
			salud.qs220=salud.getConvertqs100(salud.qs220);
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
