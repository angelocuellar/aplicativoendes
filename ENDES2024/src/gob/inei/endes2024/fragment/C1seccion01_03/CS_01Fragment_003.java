package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_003 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQS25C1;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQS25C2;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQS25C3;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQS25C4;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQS25C5;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQS25C6;
	
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS26; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS27_A; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQS27_B; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQS27_C; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQS27_D; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQS27_E; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQS27_X; 
	@FieldAnnotation(orderIndex=14) 
	public TextField txtQS27_O; 
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQH12;
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQS28;
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQS29A;
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQS29B;
	
	public TextField txtCabecera;
	Salud salud; 
	Seccion01 persona=null;
	private CuestionarioService cuestionarioService; 
	private VisitaService visitaService;
	private Seccion01Service seccion01service;
	private Seccion01Service personaservice;
	private LabelComponent lblTitulo,lblsida,lblvih,lblSubtitulo,lblSubtitulo1,lblPregunta12,LabelComponent,
	lblintro, lblpregunta25c, lbl25c1, lbl25c2, lbl25c3, lbl25c4, lbl25c5, lbl25c6,
	lblpregunta26,lblpregunta27,lblpregunta28,lblpregunta29; 
	public GridComponent2 gridPreguntas29, gridPreguntas25c;
	CISECCION_04B2 individual4b2; 
	CISECCION_08 individual8; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	LinearLayout q5; 
	LinearLayout q6; 
	SeccionCapitulo[] seccionesGrabado,seccionesGrabado0A2; 
	SeccionCapitulo[] seccionesCargado,seccionesCargado0A2;
	SeccionCapitulo[] SeccionesGrabadoSeccion01;
	SeccionCapitulo[] SeccionesCargadoSeccion04;
	SeccionCapitulo[] SeccionesCargadoSeccion08;
	SeccionCapitulo[] SeccionesCargadoSeccion01,SeccionesCargardoSeccion02;
	Seccion01 personas;
	public CS_01Fragment_003() {} 
	public CS_01Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QS25C1", "QS25C2", "QS25C3", "QS25C4", "QS25C5", "QS25C6" ,"QS26", "QS27_A", "QS27_B", "QS27_C", "QS27_D", "QS27_E","QS27_X", "QS27_O","QS28","QS29A","QS29B", "ID", "HOGAR_ID","PERSONA_ID","QS23","QS26","QH06") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QS25C1", "QS25C2", "QS25C3", "QS25C4", "QS25C5", "QS25C6" ,"QS26", "QS27_A", "QS27_B", "QS27_C", "QS27_D", "QS27_E","QS27_X", "QS27_O","QS28","QS29A","QS29B") };
		
		seccionesGrabado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12") };
		seccionesCargado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","ID","HOGAR_ID","PERSONA_ID") };
		
		
//		SeccionesGrabadoSeccion01  = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12")};
//		SeccionesCargardoSeccion02 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12")};
		
		SeccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI489","ID","HOGAR_ID","PERSONA_ID")};
		SeccionesCargadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI801A","QI801B","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03).textSize(21).centrar().negrita(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_Mef).negrita().centrar().colorFondo(R.color.griscabece);
		
		
		lblpregunta25c = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh25c);
		lbl25c1 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c1).textSize(18).alinearIzquierda();		
		lbl25c2 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c2).textSize(18).alinearIzquierda();
		lbl25c3 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c3).textSize(18).alinearIzquierda();
		lbl25c4 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c4).textSize(18).alinearIzquierda();
		lbl25c5 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c5).textSize(18).alinearIzquierda();
		lbl25c6 = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.seccion01qh25c6).textSize(18).alinearIzquierda();		
		rgQS25C1 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQS25C2 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
	    rgQS25C3 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQS25C4 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQS25C5 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQS25C6 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25c_1, R.string.seccion01qh25c_2).size(90, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		
		Spanned textointro = Html.fromHtml("25C. A continuaci&oacute;n le har&eacute algunas preguntas para saber si Usted presenta alguna dificultad o limitaci&oacute;n <b>PERMANENTE</b>, que le impida desarrollarse normalmente en sus actividades diarias,");
		lblintro = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblintro.setText(textointro);
		
		rgQS26=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS26ChangeValue"); 
		rgQH12=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh12_1,R.string.seccion01qh12_2,R.string.seccion01qh12_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		chbQS27_A=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS27_B=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS27_C=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS27_D=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS27_E=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS27_X=new CheckBoxField(this.getActivity(), R.string.cap01_03qs27_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS26XChangeValue");
		txtQS27_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 550);

		lblpregunta26 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs26);
		lblpregunta27 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs27);
		lblpregunta28 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs28);
		lblpregunta29 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs29a);
		lblPregunta12 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.seccion01qh12_ind);
		
		rgQS28=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs28_1, R.string.cap01_03qs28_2).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		rgQS29A=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs29a_1,R.string.cap01_03qs29a_1).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS29B=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs29a_1,R.string.cap01_03qs29a_1).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblvih = new LabelComponent(this.getActivity()).size(altoComponente, 380).text(R.string.cap01_03qs29a_11).textSize(19).alinearIzquierda();
	  	lblsida = new LabelComponent(this.getActivity()).size(altoComponente,380).text(R.string.cap01_03qs29a_22).textSize(19).alinearIzquierda();
		
	  	gridPreguntas25c = new GridComponent2(this.getActivity(), App.ESTILO, 2);
	  	gridPreguntas25c.addComponent(lbl25c1);
	  	gridPreguntas25c.addComponent(rgQS25C1);
	  	gridPreguntas25c.addComponent(lbl25c2);
	  	gridPreguntas25c.addComponent(rgQS25C2);
	  	gridPreguntas25c.addComponent(lbl25c3);
	  	gridPreguntas25c.addComponent(rgQS25C3);
	  	gridPreguntas25c.addComponent(lbl25c4);
	  	gridPreguntas25c.addComponent(rgQS25C4);
	  	gridPreguntas25c.addComponent(lbl25c5);
	  	gridPreguntas25c.addComponent(rgQS25C5);
	  	gridPreguntas25c.addComponent(lbl25c6);
	  	gridPreguntas25c.addComponent(rgQS25C6);
		
	  	gridPreguntas29 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas29.addComponent(lblvih);		
		gridPreguntas29.addComponent(rgQS29A);
		gridPreguntas29.addComponent(lblsida);
		gridPreguntas29.addComponent(rgQS29B);
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblintro, lblpregunta25c,gridPreguntas25c.component());
		q3 = createQuestionSection(lblpregunta26,rgQS26); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta27,chbQS27_A,chbQS27_B,chbQS27_C,chbQS27_D,chbQS27_E,chbQS27_X,txtQS27_O);
		q5 = createQuestionSection(lblPregunta12,rgQH12);
		
		ScrollView contenedor = createForm();		
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		 
		uiToEntity(salud);
		
		if (persona.qh12!=null) {
			persona.qh12=persona.getConvertqh12(persona.qh12);
		}		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(VerificarsiCambiodeRespuestasSalud(persona,salud)){
				if(salud.qs26!=null && salud.qs26==1){
					persona.id=salud.id;
					persona.hogar_id=salud.hogar_id;
					persona.persona_id=salud.persona_id;
					persona.qh11_a=salud.qs27_b;
					persona.qh11_b=salud.qs27_c;
					persona.qh11_c=salud.qs27_a;
					persona.qh11_d=salud.qs27_d;
					persona.qh11_e=salud.qs27_e;
					persona.qh11_y=0;
					persona.qh11_z=0;
					persona.qh12=Integer.parseInt(rgQH12.getTagSelected().toString());
				}
				if(salud.qs26!=null && salud.qs26==2){
					persona.id=salud.id;
					persona.hogar_id=salud.hogar_id;
					persona.persona_id=salud.persona_id;
					persona.qh11_a=0;
					persona.qh11_b=0;
					persona.qh11_c=0;
					persona.qh11_d=0;
					persona.qh11_e=0;
					persona.qh11_y=0;
					persona.qh11_z=1;
					persona.qh12=null;
				}
				if(!getCuestionarioService().saveOrUpdate(persona, null,seccionesGrabado0A2)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados. S01", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return false;
				}
			}
			 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    

    public boolean VerificarsiCambiodeRespuestasSalud(Seccion01 bean, Salud salud) {
    	if(bean.qh11_a!=salud.qs27_b  || bean.qh11_b!=salud.qs27_c  || bean.qh11_c!=salud.qs27_a  || bean.qh11_d!=salud.qs27_d  || bean.qh11_e!=salud.qs27_e){
    		if (Util.esDiferente(salud.qh06,2)) {		
    			MyUtil.MensajeGeneral(getActivity(), "Se actualizo datos de seguro de salud en el cuestionario de Hogar");	
    		}
    		
    		return true;
    	}    	
    	else{
    		return false;
    	}
    		
    }
    
    private boolean validar() { 
    	if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(!Util.esDiferente(salud.qh06, 1) || (!Util.esDiferente(salud.qh06, 2) && (Util.esMenor(salud.qs23,15) || Util.esMayor(salud.qs23,49)))){
		
			if (Util.esVacio(salud.qs25c1)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C1");
				view = rgQS25C1;
				error = true;
				return false;
			}
			if (Util.esVacio(salud.qs25c2)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C2");
				view = rgQS25C2;
				error = true;
				return false;
			}
			if (Util.esVacio(salud.qs25c3)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C3");
				view = rgQS25C3;
				error = true;
				return false;
			}
			if (Util.esVacio(salud.qs25c4)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C4");
				view = rgQS25C4;
				error = true;
				return false;
			}
			if (Util.esVacio(salud.qs25c5)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C5");
				view = rgQS25C5;
				error = true;
				return false;
			}
			if (Util.esVacio(salud.qs25c6)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.25C6");
				view = rgQS25C6;
				error = true;
				return false;
			}	
			
			
		if (Util.esVacio(salud.qs26)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QS26"); 
			view = rgQS26; 
			error = true; 
			return false; 
		}
		Integer qh12 =Integer.parseInt(rgQH12.getTagSelected("0").toString());
		if (!Util.esDiferente(qh12,0) && !Util.esDiferente(salud.qs26,1)) {
			mensaje = preguntaVacia.replace("$", "La pregunta QH12");
			view = rgQH12;
			error = true;
			return false;
		}
		if (!Util.esDiferente(salud.qs26,1)) {		
			if (!Util.alMenosUnoEsDiferenteA(0,salud.qs27_a,salud.qs27_b,salud.qs27_c,salud.qs27_d,salud.qs27_e,salud.qs27_x)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQS27_A; 
				error = true; 
				return false; 
			} 
			
			if(!Util.esDiferente(salud.qs27_x,1)){ 
				if (Util.esVacio(salud.qs27_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQS27_O; 
					error = true; 
					return false; 
				} 
			}
		} 
	}

		
		if(!Util.esDiferente(salud.qs26, 1)){
			
	    	if(!Util.esDiferente(salud.qs27_b, 1) && !Util.esDiferente(salud.qs27_a, 1)){
	    		mensaje ="Entidades no compatibles 1";
				view = chbQS27_A;
				error=true;
				return false;
	    	}
	     	
	    	if(!Util.esDiferente(salud.qs27_b, 0) && !Util.esDiferente(salud.qs27_c, 1) && (!Util.esDiferente(salud.qs27_a, 1) || !Util.esDiferente(salud.qs27_d, 1))){
	    		mensaje ="Entidades no compatibles";
	    		view = chbQS27_C;
	    		error=true;
	    		return false;
	    	}
	     	
	    	
	    	if(!Util.esDiferente(salud.qs27_a, 1) && (!Util.esDiferente(salud.qs27_b, 1) || !Util.esDiferente(salud.qs27_c, 1) || !Util.esDiferente(salud.qs27_d, 1) || !Util.esDiferente(salud.qs27_e,1))){
	    		mensaje ="Entidades no compatibles";
				view = chbQS27_B;
				error=true;
				return false;
	    	}	
	    	
	    	if(!Util.esDiferente(salud.qs27_a, 1) && !Util.esDiferente(salud.qs27_d, 1) && !Util.esDiferente(salud.qs27_a, 1)){
	    		mensaje ="Entidades no compatibles";
				view = chbQS27_D;
				error=true;
				return false;
	    	}
	    	
	    	if(!Util.esDiferente(salud.qs27_b, 0) && !Util.esDiferente(salud.qs27_d, 1) && (!Util.esDiferente(salud.qs27_c, 1) || !Util.esDiferente(salud.qs27_a, 1))){
	    		mensaje ="Entidades no compatibles";
				view = chbQS27_D;
				error=true;
				return false;
	    	}
	    	if(!Util.esDiferente(salud.qs27_e, 1) && !Util.esDiferente(salud.qs27_a, 1)){
	    		mensaje ="Entidades no compatibles";
				view = chbQS27_A;
				error=true;
				return false;
	    	}
		}
		return true; 
    } 
       
    
    @Override 
	public void cargarDatos() {
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	persona=null;
    	persona = getCuestionarioService().getSeccion01(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id, seccionesCargado0A2);
		
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	
    	individual4b2 = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,SeccionesCargadoSeccion04);
    	individual8 = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,SeccionesCargadoSeccion08);
    	
    	if(persona.qh12!=null)	{
    		persona.qh12=persona.setConvertqh12(persona.qh12);
		}
    	
    	if(individual4b2!=null) { 
    		if(individual4b2.qi489!=null) 
    			salud.qs28 = individual4b2.qi489; 
    		else
    			salud.qs28 = null;
    	}
    	else 
    		salud.qs28 = null;
    	
    	if(individual8!=null) {
    		if(individual8.qi801a!=null) {
    			salud.qs29a = individual8.qi801a; 
    		}
    		else 
    			salud.qs29a = null;
    		if(individual8.qi801b!=null) 
    			salud.qs29b = individual8.qi801b; 
    		else 
    			salud.qs29b = null;
    	}
    	else 
    		salud.qs29b = null;
    	
    	entityToUI(salud); 
		rgQH12.setTagSelected(persona.qh12);

    	inicio(); 
	}
    
	public void saltoqhMef(Integer p06, Integer p07) {	
		if (!Util.esDiferente(p06,2)) {		
			if (MyUtil.incluyeRango(15, 49,p07) ) {					
				q0.setVisibility(View.GONE);
				q1.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
			}
			else{		
				q0.setVisibility(View.VISIBLE);
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
				
				onqrgQS26ChangeValue(); 
			}		
		}			
		else{			
			q0.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onqrgQS26ChangeValue(); 
		}		
	}
	
	  public void ValidarPreguntaQH12() {
	    	Integer p11y=persona.qh11_y==null?0:persona.qh11_y;
	    	Integer p11x =persona.qh11_z==null?0:persona.qh11_z;
	    	
	    	if (p11y==1 || p11x==1) {
//	    		Log.e("d","11y->"+p11y + "p11x->"+p11x);
	    		Util.lockView(getActivity(), false,rgQH12);
				q5.setVisibility(View.VISIBLE);
			}
	    	else{
//	    		Log.e("sss","casp1");
	     		q5.setVisibility(View.GONE);
	    	}
	    	
	    }

    private void inicio() { 
    	
    	InformatedeSaludCorrecto();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	rgQS26.requestFocus();
    	
    } 
    
    
    public void InformatedeSaludCorrecto() {
		Integer personaId=SeleccionarInformantedeSalud();
		if (personaId!=App.getInstance().getPersonaSeccion01().persona_id) {
			q0.setVisibility(View.GONE);			
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		}
		else{
			saltoqhMef(salud.qh06, salud.qs23);
			
		}	
	}    
    public Integer SeleccionarInformantedeSalud(){
  		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getSeccion01Service());
  		return persona_id;
    }
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS25C1.readOnly();
			rgQS25C2.readOnly();
			rgQS25C3.readOnly();
			rgQS25C4.readOnly();
			rgQS25C5.readOnly();
			rgQS25C6.readOnly();	
			
			rgQS26.readOnly();
			rgQH12.readOnly();
			chbQS27_A.readOnly();
			chbQS27_B.readOnly();
			chbQS27_C.readOnly();
			chbQS27_D.readOnly();
			chbQS27_E.readOnly();
			chbQS27_X.readOnly();		
			rgQH12.readOnly();
		}		
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public Seccion01Service getPersonaService() { 
		if(personaservice==null){ 
			personaservice = Seccion01Service.getInstance(getActivity()); 
		} 
		return personaservice; 
    } 
	public VisitaService getVisitaService()
	{
		if(visitaService==null)
		{
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
    public Seccion01Service getSeccion01Service()
    {
    	if(seccion01service==null){
    		seccion01service =  Seccion01Service.getInstance(getActivity());
    	}
    return seccion01service;
    }
    
    
	public void onqrgQS26ChangeValue() {
		if (MyUtil.incluyeRango(2,2,rgQS26.getTagSelected("").toString())) {		
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),chbQS27_A,chbQS27_B,chbQS27_C,chbQS27_D,chbQS27_E,chbQS27_X,txtQS27_O);
			q4.setVisibility(View.GONE);	
			q5.setVisibility(View.GONE);	
			rgQS26.requestFocus();
			
		} 
		else {
			Util.lockView(getActivity(), false,chbQS27_A,chbQS27_B,chbQS27_C,chbQS27_D,chbQS27_E,chbQS27_X,rgQH12);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			onqrgQS26XChangeValue();
			ValidarPreguntaQH12();
			chbQS27_A.requestFocus();			
		}	
	}
	
	public void onqrgQS26XChangeValue() {
		if (chbQS27_X.isChecked()) {
			Util.lockView(getActivity(), false,txtQS27_O);			
			txtQS27_O.requestFocus();	
						
		} 
		else {	
			Util.cleanAndLockView(getActivity(),txtQS27_O);			
		}	
	}
	
	@Override
	public Integer grabadoParcial() {
		
		uiToEntity(salud);
		
		if (persona.qh12!=null) {
			persona.qh12=persona.getConvertqh12(persona.qh12);
		}		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return App.NODEFINIDO; 
		} 
		try { 
			if(VerificarsiCambiodeRespuestasSalud(persona,salud)){
				if(salud.qs26!=null && salud.qs26==1){
					persona.id=salud.id;
					persona.hogar_id=salud.hogar_id;
					persona.persona_id=salud.persona_id;
					persona.qh11_a=salud.qs27_b;
					persona.qh11_b=salud.qs27_c;
					persona.qh11_c=salud.qs27_a;
					persona.qh11_d=salud.qs27_d;
					persona.qh11_e=salud.qs27_e;
					persona.qh11_y=0;
					persona.qh11_z=0;
					persona.qh12=Integer.parseInt(rgQH12.getTagSelected().toString());
				}
				if(salud.qs26!=null && salud.qs26==2){
					persona.id=salud.id;
					persona.hogar_id=salud.hogar_id;
					persona.persona_id=salud.persona_id;
					persona.qh11_a=0;
					persona.qh11_b=0;
					persona.qh11_c=0;
					persona.qh11_d=0;
					persona.qh11_e=0;
					persona.qh11_y=0;
					persona.qh11_z=1;
					persona.qh12=null;
				}
				if(!getCuestionarioService().saveOrUpdate(persona, null,seccionesGrabado0A2)){
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados. S01", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
					return App.NODEFINIDO;
				}
			}
			 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD; 
		}
	} 
