package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS100; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS101; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQS101_O; 
	public LabelComponent lblpregunta100,lblpregunta101;
	Salud salud;

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblsectorpublico,lblsectorprivado,lblong,lblcampa,lblSubtitulo1,lblhospital; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;
	LinearLayout q4; 
 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	String qs100fechref;

	public CS_01Fragment_004() {} 
	public CS_01Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS100","QS100FECH_REF","QS101","QS101_O","ID","HOGAR_ID","PERSONA_ID","QS23","QH06")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS100","QS100FECH_REF","QS101","QS101_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01).textSize(21).centrar().negrita(); 
		lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		lblSubtitulo = new LabelComponent(getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01_1).negrita();
		Spanned texto = Html.fromHtml("100. �En los �ltimos 12 meses,es decir, desde F1 del a�o pasado hasta F2 F3, alg�n m�dico u otro personal de salud le ha medido la <b>presi�n arterial</b>?");
		lblpregunta100 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);//.text(R.string.cap01_03qs100);
		lblpregunta100.setText(texto);
		rgQS100=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs100_1,R.string.cap01_03qs100_2,R.string.cap01_03qs100_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS100ChangeValue"); 
		rgQS101=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs101_1,R.string.cap01_03qs101_2,R.string.cap01_03qs101_3,R.string.cap01_03qs101_4,R.string.cap01_03qs101_5,R.string.cap01_03qs101_6,R.string.cap01_03qs101_7,R.string.cap01_03qs101_8,R.string.cap01_03qs101_9,R.string.cap01_03qs101_10,R.string.cap01_03qs101_11,R.string.cap01_03qs101_12,R.string.cap01_03qs101_13,R.string.cap01_03qs101_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS101ChangeValue"); 
		txtQS101_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 

		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_a).textSize(18).negrita();
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_b).textSize(18).negrita();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_c).textSize(18).negrita();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_d).textSize(18).negrita();
		rgQS101.agregarTitle(0,lblsectorpublico);
		rgQS101.agregarTitle(1,lblhospital);
		rgQS101.agregarTitle(8,lblsectorprivado);
		rgQS101.agregarTitle(10,lblong);
		rgQS101.agregarTitle(13,lblcampa);
		Spanned texto101 =Html.fromHtml("101. �D�nde le midieron a usted la <b>presi�n arterial</b> la �ltima vez?");
		lblpregunta101 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);//.text(R.string.cap01_03qs101);
		lblpregunta101.setText(texto101);
	
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
	
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q4 = createQuestionSection(lblSubtitulo1);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblpregunta100,rgQS100); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta101,rgQS101,txtQS101_O); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);  
		form.addView(q0);
		form.addView(q4);
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 

    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		
		uiToEntity(salud); 
		
		if (salud.qs100!=null) {
			salud.qs100=salud.getConvertqs100(salud.qs100);
			
			 if(qs100fechref == null) {
				 	salud.qs100fech_ref =  Util.getFechaActualToString();
					}
				 else {
					 salud.qs100fech_ref = qs100fechref;
					}	
		}
		else {
			salud.qs100fech_ref = null;
		}
		if (salud.qs101!=null) {
			salud.qs101=salud.getConvertqs101(salud.qs101);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS1_3().qs100=salud.qs100;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(salud.qs100)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.100"); 
			view = rgQS100; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs100,1)) {
			if (Util.esVacio(salud.qs101)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.101"); 
				view = rgQS101; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(salud.qs101,42,96)){ 
				if (Util.esVacio(salud.qs101_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQS101_O; 
					error = true; 
					return false; 
				} 
			} 
		} 
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
 
    	if (salud.qs100!=null) {
    		salud.qs100=salud.setConvertqs100(salud.qs100);    
    	}		
    	if (salud.qs101!=null) {
    		salud.qs101=salud.setConvertqs101(salud.qs101);
    	}
  	
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
  		entityToUI(salud);
  		qs100fechref = salud.qs100fech_ref;  		
  		App.getInstance().setPersonabySalud(salud);
  		inicio(); 
    }
    private void inicio() { 
    	onqrgQS100ChangeValue();    	
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS100.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS100.readOnly();
			rgQS101.readOnly();
			txtQS101_O.readOnly();
		}
	}
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	
    	
    	if(qs100fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs100fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta100.setText(lblpregunta100.getText().toString().replace("F1", F2)); 
    	lblpregunta100.setText(lblpregunta100.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta100.setText(lblpregunta100.getText().toString().replace("F3", F3));
    	
    	Spanned texto = Html.fromHtml(lblpregunta100.getText().toString().replace("presi�n arterial?","<b>presi�n arterial</b>?"));
    	lblpregunta100.setText(texto);
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	public void onqrgQS100ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQS100.getTagSelected("").toString())) {		
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQS101,txtQS101_O);
			q3.setVisibility(View.GONE);			
		} 
		else {
			Util.lockView(getActivity(), false,rgQS101,txtQS101_O);			
			q3.setVisibility(View.VISIBLE);
			rgQS101.requestFocus();
			onqrgQS101ChangeValue();
		}	
	}
	
	public void onqrgQS101ChangeValue() {
		if (!MyUtil.incluyeRango(2,3,rgQS100.getTagSelected("").toString())) {
			if (!MyUtil.incluyeRango(12,13,rgQS101.getTagSelected("").toString())) {	
				Util.cleanAndLockView(getActivity(),txtQS101_O);				
			} 
			else {
				Util.lockView(getActivity(), false,txtQS101_O);		
				txtQS101_O.requestFocus();
			}
		}		
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs100!=null) {
			salud.qs100=salud.getConvertqs100(salud.qs100);
			
			 if(qs100fechref == null) {
				 	salud.qs100fech_ref =  Util.getFechaActualToString();
					}
				 else {
					 salud.qs100fech_ref = qs100fechref;
					}	
		}
		else {
			salud.qs100fech_ref = null;
		}
		if (salud.qs101!=null) {
			salud.qs101=salud.getConvertqs101(salud.qs101);
		}

		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
