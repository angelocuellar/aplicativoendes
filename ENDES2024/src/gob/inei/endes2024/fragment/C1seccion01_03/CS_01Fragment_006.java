package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_006 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS107; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS108; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQS108_O; 
	public LabelComponent lblpregunta107;
	Salud salud; 

	public TextField txtCabecera;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblsectorpublico,lblsectorprivado,lblong,lblcampa,lblSubtitulo,lblhospital,lblpregunta108,lblpregunta108_ind;  
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2;
	LinearLayout q3; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String qs107fechref;
	public Spanned texto107 = null;
	public CS_01Fragment_006() {} 
	public CS_01Fragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS107","QS107FECH_REF","QS108","QS108_O","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS107","QS107FECH_REF","QS108","QS108_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		rgQS107=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs107_1,R.string.cap01_03qs107_2,R.string.cap01_03qs107_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS107ChangeValue"); 
		rgQS108=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs108_1,R.string.cap01_03qs108_2,R.string.cap01_03qs108_3,R.string.cap01_03qs108_4,R.string.cap01_03qs108_5,R.string.cap01_03qs108_6,R.string.cap01_03qs108_7,R.string.cap01_03qs108_8,R.string.cap01_03qs108_9,R.string.cap01_03qs108_10,R.string.cap01_03qs108_11,R.string.cap01_03qs108_12,R.string.cap01_03qs108_13,R.string.cap01_03qs108_14).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS108ChangeValue"); 
		txtQS108_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		
		lblpregunta108_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs108_ind).textSize(17).negrita();
		
		texto107 = Html.fromHtml("107. �En los �ltimos 12 meses, es decir, desde F1 del a�o pasado hasta F2 F3, alg�n m�dico u otro personal de salud le ha medido la");
		lblpregunta107 =new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta107.setText(texto107);
	
		Spanned texto108=Html.fromHtml("108. �D�nde le midieron la <b>glucosa</b> o el <b>\"az�car\" en la sangre</b> la �ltima vez?");
		lblpregunta108 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta108.setText(texto108);
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_a).textSize(18).negrita();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_b).textSize(18).negrita();
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_c).textSize(18).negrita();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_d).textSize(18).negrita();
			
		rgQS108.agregarTitle(0,lblsectorpublico);
		rgQS108.agregarTitle(1,lblhospital);
		rgQS108.agregarTitle(8,lblsectorprivado);
		rgQS108.agregarTitle(10,lblong);
		rgQS108.agregarTitle(13,lblcampa);
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q3 = createQuestionSection(lblSubtitulo); 
		q1 = createQuestionSection(lblpregunta107,rgQS107); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta108,lblpregunta108_ind,rgQS108,txtQS108_O); 
 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 

		form.addView(q0);
		form.addView(q3); 
		form.addView(q1); 
		form.addView(q2); 
		
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 

		uiToEntity(salud); 
		
		if (salud.qs107!=null) {
			salud.qs107=salud.getConvertqs100(salud.qs107);
			
			if(qs107fechref == null) {
			 	salud.qs107fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs107fech_ref = qs107fechref;
				 }
		}
		else {
			salud.qs107fech_ref = qs107fechref;
		}
		if (salud.qs108!=null) {
			salud.qs108=salud.getConvertqs101(salud.qs108);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(salud.qs107)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.107"); 
			view = rgQS107; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs107,1)) {
			if (Util.esVacio(salud.qs108)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.108"); 
				view = rgQS108; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(salud.qs108,42,96)){ 
				if (Util.esVacio(salud.qs108_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQS108_O; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
   
    	if (salud.qs107!=null) {
			salud.qs107=salud.setConvertqs100(salud.qs107);
		}		
		if (salud.qs108!=null) {
			salud.qs108=salud.setConvertqs101(salud.qs108);
		}
    	
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	
    	qs107fechref = salud.qs107fech_ref;  
    	inicio(); 
	}

    private void inicio() { 
    	onqrgQS107ChangeValue();    
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
//    	rgQS107.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS107.readOnly();
			rgQS108.readOnly();
			txtQS108_O.readOnly();
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	if(qs107fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs107fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta107.setText(texto107);
    	lblpregunta107.setText(lblpregunta107.getText().toString().replace("F1", F2)); 
    	lblpregunta107.setText(lblpregunta107.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta107.setText(lblpregunta107.getText().toString().replace("F3", F3));
    	
    	
    	Spanned texto107 = Html.fromHtml(lblpregunta107.getText().toString()+ " <b>glucosa</b> o el <b>\"az�car\" en la sangre</b>?");
    	lblpregunta107.setText(texto107);
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	public void onqrgQS107ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQS107.getTagSelected("").toString())) {		
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQS108,txtQS108_O);
			q2.setVisibility(View.GONE);
		} 
		else {
			Util.lockView(getActivity(), false,rgQS108,txtQS108_O);	
			q2.setVisibility(View.VISIBLE);
			onqrgQS108ChangeValue();
			rgQS108.requestFocus();
		}	
	}
	
	public void onqrgQS108ChangeValue() {
		if (!MyUtil.incluyeRango(2,3,rgQS107.getTagSelected("").toString())) {
			if (!MyUtil.incluyeRango(12,13,rgQS108.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQS108_O);				
			} 
			else {
				Util.lockView(getActivity(), false,txtQS108_O);
				txtQS108_O.requestFocus();
			}
		}		
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs107!=null) {
			salud.qs107=salud.getConvertqs100(salud.qs107);
			
			if(qs107fechref == null) {
			 	salud.qs107fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs107fech_ref = qs107fechref;
				 }
		}
		else {
			salud.qs107fech_ref = null;
		}
		if (salud.qs108!=null) {
			salud.qs108=salud.getConvertqs101(salud.qs108);
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
