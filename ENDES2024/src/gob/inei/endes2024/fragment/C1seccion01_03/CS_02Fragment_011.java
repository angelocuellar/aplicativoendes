package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
 
public class CS_02Fragment_011 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS213; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQS213A;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS214; 
	@FieldAnnotation(orderIndex=4) 
	public DecimalField txtQS214A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS215; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQS215A; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS216; 
	@FieldAnnotation(orderIndex=8) 
	public DecimalField txtQS216A;  
	public LabelComponent lblpregunta213,lblpregunta214,lblpregunta214_ind,lblpregunta215,lblpregunta215_ind,lblpregunta216,lblpregunta216_ind;
	Salud salud; 

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblSubtitulo1; 
	public GridComponent2 gridPreguntas213,gridPreguntas214,gridPreguntas215,gridPreguntas216;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String qs213fechref;
	String qs215fechref;
	
	public CS_02Fragment_011() {} 
	public CS_02Fragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS213","QS213A","QS213FECH_REF","QS214","QS214A","QS215","QS215A","QS215FECH_REF","QS216","QS216A","ID","HOGAR_ID","PERSONA_ID","QS206","QS208","QS210","QS211U")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS213","QS213A","QS213FECH_REF","QS214","QS214A","QS215","QS215A","QS215FECH_REF","QS216","QS216A")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC02).textSize(21).centrar(); 
		lblSubtitulo1 = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01_3).negrita();
		
		lblpregunta213 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs213).textSize(19);
		lblpregunta214 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs214).textSize(19);
		lblpregunta214_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs214_ind).textSize(17).negrita();
		lblpregunta215 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs215).textSize(19);
		lblpregunta215_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs215_ind).textSize(17).negrita();
		lblpregunta216 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs216).textSize(19);
		lblpregunta216_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03qs216_ind).textSize(17).negrita();
		
		rgQS213=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs213_1,R.string.cap01_03qs213_2,R.string.cap01_03qs213_3).size(altoComponente+60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS213ChangeValue"); 
		txtQS213A=new IntegerField(this.getActivity()).size(altoComponente+60, 80).maxLength(1); 
		
		rgQS214=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs214_1,R.string.cap01_03qs214_2).size(altoComponente+20,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS214ChangeValue"); 
		txtQS214A=new DecimalField(this.getActivity()).size(altoComponente+20, 80).maxLength(2).decimales(1); 
		
		rgQS215=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs215_1,R.string.cap01_03qs215_2,R.string.cap01_03qs215_3).size(altoComponente+60,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS215ChangeValue"); 
		txtQS215A=new IntegerField(this.getActivity()).size(altoComponente+60, 80).maxLength(1); 
		
		rgQS216=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs216_1,R.string.cap01_03qs216_2).size(altoComponente+20,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS216ChangeValue"); 
		txtQS216A=new DecimalField(this.getActivity()).size(altoComponente+20, 80).maxLength(2).decimales(1); 

		gridPreguntas213=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas213.addComponent(rgQS213);		
		gridPreguntas213.addComponent(txtQS213A);
		
		gridPreguntas214=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas214.addComponent(rgQS214);		
		gridPreguntas214.addComponent(txtQS214A);
		
		gridPreguntas215=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas215.addComponent(rgQS215);		
		gridPreguntas215.addComponent(txtQS215A);
		
		gridPreguntas216=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas216.addComponent(rgQS216);		
		gridPreguntas216.addComponent(txtQS216A);

    } 
    @Override 
    protected View createUI() { 
		buildFields();  
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q6 = createQuestionSection(lblSubtitulo1);
		q1 = createQuestionSection(lblSubtitulo);
		q2 = createQuestionSection(lblpregunta213,gridPreguntas213.component()); 
		q3 = createQuestionSection(lblpregunta214,lblpregunta214_ind,gridPreguntas214.component()); 
		q4 = createQuestionSection(lblpregunta215,lblpregunta215_ind,gridPreguntas215.component()); 
		q5 = createQuestionSection(lblpregunta216,lblpregunta216_ind,gridPreguntas216.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);  
		form.addView(q0);
		form.addView(q6); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
    return contenedor;    
    } 
    
    @Override 
    public boolean grabar() { 
	
		uiToEntity(salud);
		
		if (salud.qs213!=null) {
			salud.qs213=salud.getConvertqs215(salud.qs213);
			
			if(qs213fechref == null) {
			 	salud.qs213fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs213fech_ref = qs213fechref;
				 }
		}
		else {
			salud.qs213fech_ref = null;
		}
		if (salud.qs214!=null) {
			salud.qs214=salud.getConvertqs203(salud.qs214);
		}	
		if (salud.qs215!=null) {
			salud.qs215=salud.getConvertqs215(salud.qs215);
			if(qs215fechref == null) {
			 	salud.qs215fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs215fech_ref = qs215fechref;
				 }
		}
		else {
			salud.qs215fech_ref = null;
		}
		if (salud.qs216!=null) {
			salud.qs216=salud.getConvertqs203(salud.qs216);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		App.getInstance().setPersonabySalud(salud);
		return true;
		
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(salud.qs213)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
			view = rgQS213; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(salud.qs213, 1)) {
			if (Util.esVacio(salud.qs213a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.213A"); 
				view = txtQS213A; 
				error = true; 
				return false; 
			}
			if (Util.esMenor(salud.qs213a,1) || Util.esMayor(salud.qs213a,7)) { 
				mensaje = "El rango es de 1 a 7"; 
				view = txtQS213A; 
				error = true; 
				return false; 
			}
			
			if (Util.esVacio(salud.qs214)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.214"); 
				view = rgQS214; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs214,1)) {
				if (Util.esVacio(salud.qs214a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.214A"); 
					view = txtQS214A; 
					error = true; 
					return false; 
				}	
				if(salud.qs214a.doubleValue()==0 || salud.qs214a.doubleValue()%(0.5)!=0 || salud.qs214a.doubleValue()>9.5){
					mensaje ="Porci�n invalida "; 
					view = txtQS214A ; 
					error = true; 
					return false; 
				}
				
			}		 
		}			
		
		if (Util.esVacio(salud.qs215)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.215"); 
			view = rgQS215; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs215,1)) {
			if (Util.esVacio(salud.qs215a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.215A"); 
				view = txtQS215A; 
				error = true; 
				return false; 
			} 
			if (Util.esMenor(salud.qs215a,1) || Util.esMayor(salud.qs215a,7)) { 
				mensaje = "El rango es de 1 a 7"; 
				view = txtQS215A; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(salud.qs216)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.216"); 
				view = rgQS216; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs216,1)) {
				if (Util.esVacio(salud.qs216a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.216A"); 
					view = txtQS216A; 
					error = true; 
					return false; 
				}
				if(salud.qs216a.doubleValue()==0 || salud.qs216a.doubleValue()%(0.5)!=0 || salud.qs216a.doubleValue()>9.5){
					mensaje ="Porci�n invalida "; 
					view = txtQS216A ; 
					error = true; 
					return false; 
				}
			}			 	
		}
			
		return true; 
    } 
    @Override 
    public void cargarDatos() {     	
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    
    	if (salud.qs213!=null) {
			salud.qs213=salud.setConvertqs215(salud.qs213);
		}
    	if (salud.qs214!=null) {
			salud.qs214=salud.setConvertqs203(salud.qs214);
		}
    	if (salud.qs215!=null) {
			salud.qs215=salud.setConvertqs215(salud.qs215);
		}
		if (salud.qs216!=null) {
			salud.qs216=salud.setConvertqs203(salud.qs216);
		}		

    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud);     	
    	qs213fechref = salud.qs213fech_ref;
    	qs215fechref = salud.qs215fech_ref;
    	App.getInstance().setPersonabySalud(salud);
    	inicio(); 
    	
	}
    
    private void inicio() { 
    	RenombrarEtiqueta();
    	onqrgQS213ChangeValue();
    	onqrgQS215ChangeValue();
    	ValidarsiesSupervisora();
//    	rgQS213.requestFocus();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS213.readOnly();
			txtQS213A.readOnly();
			rgQS214.readOnly();
			txtQS214A.readOnly();
			rgQS215.readOnly();
			txtQS215A.readOnly();
			rgQS216.readOnly();
			txtQS216A.readOnly();
		}
	}
    
    public void RenombrarEtiqueta(){
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(qs213fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs213fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}    	
    	
    	Calendar fechaayer= MyUtil.PrimeraFecha(fechaactual, 1);
    	Calendar fechaSemana= MyUtil.PrimeraFecha(fechaayer, 6);
    	lblpregunta213.setText(lblpregunta213.getText().toString().replace("F1",MyUtil.DiaDelaSemana(fechaSemana.get(Calendar.DAY_OF_WEEK))));
    	
    	fechaactual = new GregorianCalendar();
    	if(qs215fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs215fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}    	
    	
    	fechaayer= MyUtil.PrimeraFecha(fechaactual, 1);
    	fechaSemana= MyUtil.PrimeraFecha(fechaayer, 6);
    	lblpregunta215.setText(lblpregunta215.getText().toString().replace("F1",MyUtil.DiaDelaSemana(fechaSemana.get(Calendar.DAY_OF_WEEK))));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS213ChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQS213.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),txtQS213A,rgQS214,txtQS214A);
  			q3.setVisibility(View.GONE);
  			rgQS215.requestFocus();
  		} 
  		else {
  			Util.lockView(getActivity(),false,txtQS213A,rgQS214,txtQS214A);  
  			q3.setVisibility(View.VISIBLE);
  			onqrgQS214ChangeValue();  
  			txtQS213A.requestFocus();  			
  		}	
  	}
    public void onqrgQS214ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS213.getTagSelected("").toString())) {
	  		if (MyUtil.incluyeRango(2,2,rgQS214.getTagSelected("").toString())) {	  		
	  			Util.cleanAndLockView(getActivity(),txtQS214A);		
	  			rgQS215.requestFocus();
	  			
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,txtQS214A);
	  			txtQS214A.requestFocus();
	  		}	
    	}
  	}
    public void onqrgQS215ChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQS215.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),txtQS215A,rgQS216,txtQS216A);	
  			q5.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,txtQS215A,rgQS216,txtQS216A);
  			q5.setVisibility(View.VISIBLE);
  			onqrgQS216ChangeValue();  	
  			txtQS215A.requestFocus();
  		}	
  	}
    public void onqrgQS216ChangeValue() {
    	if (!MyUtil.incluyeRango(2,3,rgQS215.getTagSelected("").toString())) {
	  		if (MyUtil.incluyeRango(2,2,rgQS216.getTagSelected("").toString())) {		  		
	  			Util.cleanAndLockView(getActivity(),txtQS216A);	  			
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,txtQS216A); 
	  			txtQS216A.requestFocus();
	  		}	
    	}
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);
		
		if (salud.qs213!=null) {
			salud.qs213=salud.getConvertqs215(salud.qs213);
			
			if(qs213fechref == null) {
			 	salud.qs213fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs213fech_ref = qs213fechref;
				 }
		}
		else {
			 salud.qs213fech_ref = null;
		}
		if (salud.qs214!=null) {
			salud.qs214=salud.getConvertqs203(salud.qs214);
		}	
		if (salud.qs215!=null) {
			salud.qs215=salud.getConvertqs215(salud.qs215);
			
			if(qs215fechref == null) {
			 	salud.qs215fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs215fech_ref = qs215fechref;
				 }
		}
		else {
			 salud.qs215fech_ref = null;			
		}
		if (salud.qs216!=null) {
			salud.qs216=salud.getConvertqs203(salud.qs216);
		}

		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		return App.SALUD;
	}
} 
