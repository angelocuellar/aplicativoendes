package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_01Fragment_007 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS109; 
//	@FieldAnnotation(orderIndex=2) 
//	public RadioGroupOtherField rgQS110U; 
//	@FieldAnnotation(orderIndex=3) 
//	public IntegerField txtQS110C; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS111; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS112; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS113; 
	public LabelComponent lblpregunta111,lblpregunta109,/*lblpregunta110,*/lblpregunta112,lblpregunta113,lblpregunta109_ind,lblpregunta110_ind,lblpregunta113_ind;
	Salud salud; 

	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo,lblvacio; 
//	public GridComponent2 gridPreguntas110;
//	public IntegerField txtQS110C1,txtQS110C2;
	
	LinearLayout q0; 
	LinearLayout q1; 
//	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5;
	LinearLayout q6; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	
	String qs111fechref;
	public Spanned texto111=null;
	public CS_01Fragment_007() {} 
	public CS_01Fragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS109","QS110U","QS110C","QS111","QS111FECH_REF","QS112","QS113","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS109","QS110U","QS110C","QS111","QS111FECH_REF","QS112","QS113")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC01).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		rgQS109=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs109_1,R.string.cap01_03qs109_2,R.string.cap01_03qs109_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS109ChangeValue"); 
		
//		rgQS110U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs110u_1,R.string.cap01_03qs110u_2,R.string.cap01_03qs110u_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS110ChangeValue"); 
//		txtQS110C1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
//		txtQS110C2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
//		lblvacio = new LabelComponent(this.getActivity()).size(75, 100);
		
		rgQS111=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs111_1,R.string.cap01_03qs111_2,R.string.cap01_03qs111_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS111ChangeValue"); 
		rgQS112=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs112_1,R.string.cap01_03qs112_2,R.string.cap01_03qs112_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQS113=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs113_1,R.string.cap01_03qs113_2,R.string.cap01_03qs113_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		texto111=Html.fromHtml("111. �En los �ltimos 12 meses, es decir, desde F1 del a�o pasado hasta F2 F3, usted ha recibido y/o comprado medicamentos para controlar su ");
		lblpregunta111 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta111.setText(texto111);
		
		Spanned texto109=Html.fromHtml("109. �Alguna vez en su vida un m�dico le ha diagnosticado <b>diabetes</b> o <b>\"az�car alta\"</b> en la sangre?");
		lblpregunta109 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta109.setText(texto109);
		
//		Spanned texto110=Html.fromHtml("110. �Hace cu�nto tiempo le dijeron a usted que tiene  <b>diabetes</b> o <b>\"az�car alta\"</b> en la sangre?");
//		lblpregunta110 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
//		lblpregunta110.setText(texto110);
		
		Spanned texto113 = Html.fromHtml("113. �En los �ltimos 12 meses usted tom� sus medicamentos  <b>tal como</b> le indic� el m�dico?");
		lblpregunta113 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta113.setText(texto113);
		
		lblpregunta112 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.cap01_03qs112);
		lblpregunta109_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs109_ind).textSize(17).negrita();
		lblpregunta110_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs110_ind).textSize(17).negrita();
		lblpregunta113_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.cap01_03qs113_ind).textSize(17).negrita();
		
//		gridPreguntas110=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
//		gridPreguntas110.addComponent(rgQS110U,1,3);		
//		gridPreguntas110.addComponent(txtQS110C1);
//		gridPreguntas110.addComponent(txtQS110C2);
//		gridPreguntas110.addComponent(lblvacio);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q6 = createQuestionSection(lblSubtitulo); 
		q1 = createQuestionSection(lblpregunta109,lblpregunta109_ind,rgQS109); 
//		q2 = createQuestionSection(lblpregunta110,lblpregunta110_ind,gridPreguntas110.component()); 
		q3 = createQuestionSection(lblpregunta111,rgQS111); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta112,rgQS112); 
		q5 = createQuestionSection(lblpregunta113,lblpregunta113_ind,rgQS113); 
	
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
	 
		form.addView(q0);
		form.addView(q6); 
		form.addView(q1); 
//		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		    	 	
		uiToEntity(salud); 
		
		if (salud.qs109!=null) {
			salud.qs109=salud.getConvertqs100(salud.qs109);
		}
//		if (salud.qs110u!=null) {
//			salud.qs110u=salud.getConvertqs100(salud.qs110u);
//		}
		if (salud.qs111!=null) {
			salud.qs111=salud.getConvertqs100(salud.qs111);
			
			if(qs111fechref == null) {
			 	salud.qs111fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs111fech_ref = qs111fechref;
				 }
		}
		else {
			salud.qs111fech_ref = null;
		}
		if (salud.qs112!=null) {
			salud.qs112=salud.getConvertqs100(salud.qs112);
		}
		if (salud.qs113!=null) {
			salud.qs113=salud.getConvertqs100(salud.qs113);
		}	
		
//		if (!Util.esDiferente(salud.qs110u,1) && txtQS110C1.getText().toString().trim().length()!=0) {
//			salud.qs110c=Integer.parseInt(txtQS110C1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs110u,2) && txtQS110C2.getText().toString().trim().length()!=0) {
//			salud.qs110c=Integer.parseInt(txtQS110C2.getText().toString().trim());
//		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 

		if (Util.esVacio(salud.qs109)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.109"); 
			view = rgQS109; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(salud.qs109,1)) {
//			if (Util.esVacio(salud.qs110u)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta P.110U"); 
//				view = rgQS110U; 
//				error = true; 
//				return false; 
//			} 
//			if (!Util.esDiferente(salud.qs110u,1)) {
//				if (Util.esVacio(salud.qs110c)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.110C1"); 
//					view = txtQS110C1; 
//					error = true; 
//					return false; 
//				} 
//				if(Util.esMayor(salud.qs110c, 23)){
//					mensaje = "Opci�n meses no debe ser mayor a 23"; 
//					view = txtQS110C1; 
//					error = true; 
//					return false; 
//				}
//			}	
//			if (!Util.esDiferente(salud.qs110u,2)) {
//				if (Util.esVacio(salud.qs110c)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.110C2"); 
//					view = txtQS110C2; 
//					error = true; 
//					return false; 
//				} 
//				if(Util.esMenor(salud.qs110c,2) || Util.esMenor(salud.qs23, salud.qs110c)){
//					mensaje = "A�os del diagn�stico no corresponde"; 
//					view = txtQS110C2; 
//					error = true; 
//					return false;
//				}
//			}
			if (Util.esVacio(salud.qs111)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.111"); 
				view = rgQS111; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs111,1)) {
				if (Util.esVacio(salud.qs112)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.112"); 
					view = rgQS112; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs113)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.113"); 
					view = rgQS113; 
					error = true; 
					return false; 
				} 
			}	
		}	
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    
    	if (salud.qs109!=null) {
			salud.qs109=salud.setConvertqs100(salud.qs109);
		}
//		if (salud.qs110u!=null) {
//			salud.qs110u=salud.setConvertqs100(salud.qs110u);
//		}
		if (salud.qs111!=null) {
			salud.qs111=salud.setConvertqs100(salud.qs111);
		}
		if (salud.qs112!=null) {
			salud.qs112=salud.setConvertqs100(salud.qs112);
		}
		if (salud.qs113!=null) {
			salud.qs113=salud.setConvertqs100(salud.qs113);
		}
    	
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	
    	entityToUI(salud);
    	
    	qs111fechref = salud.qs111fech_ref;  
    	
//    	if (salud.qs110c!=null) {	
//	    	if (!Util.esDiferente(salud.qs110u,1)) {
//	    		txtQS110C1.setText(salud.qs110c.toString());
//	    	}
//	   	  	if (!Util.esDiferente(salud.qs110u,2)) {
//	   	  		txtQS110C2.setText(salud.qs110c.toString());
//	   	  	}
//    	}
    	
   	  	inicio(); 
	}

    private void inicio() { 
    	RenombrarEtiquetas();
    	onqrgQS109ChangeValue(); 
    	ValidarsiesSupervisora();
//    	rgQS109.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS109.readOnly();
//			rgQS110U.readOnly();
//			txtQS110C.readOnly();
//			txtQS110C1.readOnly();
//			txtQS110C2.readOnly();
			rgQS111.readOnly();
			rgQS112.readOnly();
			rgQS113.readOnly();			
		}
	}

    public void RenombrarEtiquetas(){
    	Calendar fechaactual = new GregorianCalendar();
    	if(qs111fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs111fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta111.setText(texto111);
    	lblpregunta111.setText(lblpregunta111.getText().toString().replace("F1", F2)); 
    	lblpregunta111.setText(lblpregunta111.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta111.setText(lblpregunta111.getText().toString().replace("F3", F3));
    	
    	Spanned texto111 = Html.fromHtml(lblpregunta111.getText()+"<b>\"diabetes\"</b> o <b>\"az�car alta\"</b> en la sangre?");
    	lblpregunta111.setText(texto111);
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS109ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQS109.getTagSelected("").toString())) {		
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),/*rgQS110U,txtQS110C1,txtQS110C2,*/rgQS111,rgQS112,rgQS113);
//			salud.qs110c=null;
//			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
		} 
		else {
			Util.lockView(getActivity(),false,/*rgQS110U,*/rgQS111,rgQS112,rgQS113);	
//			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
//	    	onqrgQS110ChangeValue();  
			onqrgQS111ChangeValue();
//			rgQS110U.requestFocus();
		}	
	}
	
//	public void onqrgQS110ChangeValue() {
//		if (MyUtil.incluyeRango(1,1,rgQS109.getTagSelected("").toString())) {
//			if (MyUtil.incluyeRango(1,1,rgQS110U.getTagSelected("").toString())) {				
//				Util.cleanAndLockView(getActivity(),txtQS110C2);
//				salud.qs110c=null;
//				Util.lockView(getActivity(),false,txtQS110C1);				
//				txtQS110C1.requestFocus();	
//			} 
//			if (MyUtil.incluyeRango(2,2,rgQS110U.getTagSelected("").toString())) {			
//				Util.cleanAndLockView(getActivity(),txtQS110C1);	
//				salud.qs110c=null;
//				Util.lockView(getActivity(),false,txtQS110C2);			
//				txtQS110C2.requestFocus();	
//			} 
//			if (MyUtil.incluyeRango(3,3,rgQS110U.getTagSelected("").toString())) {		
//				salud.qs110c=null;		
//				Util.cleanAndLockView(getActivity(),txtQS110C1,txtQS110C2);			
//				rgQS111.requestFocus();	
//			}		
//		}	
//	}
	
	public void onqrgQS111ChangeValue() {
		if (!MyUtil.incluyeRango(2,8,rgQS109.getTagSelected("").toString())) {
			if (MyUtil.incluyeRango(2,3,rgQS111.getTagSelected("").toString())) {		
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),rgQS112,rgQS113);
				q4.setVisibility(View.GONE);
				q5.setVisibility(View.GONE);
			} 
			else {
				Util.lockView(getActivity(),false,rgQS112,rgQS113);	
				q4.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
				rgQS112.requestFocus();
			}	
		}	
	}
	@Override
	public Integer grabadoParcial() {
		
		uiToEntity(salud); 
		
		if (salud.qs109!=null) {
			salud.qs109=salud.getConvertqs100(salud.qs109);
		}
//		if (salud.qs110u!=null) {
//			salud.qs110u=salud.getConvertqs100(salud.qs110u);
//		}
		if (salud.qs111!=null) {
			salud.qs111=salud.getConvertqs100(salud.qs111);
			
			if(qs111fechref == null) {
			 	salud.qs111fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs111fech_ref = qs111fechref;
				 }
		}
		else {
			salud.qs111fech_ref = null;
		}
		if (salud.qs112!=null) {
			salud.qs112=salud.getConvertqs100(salud.qs112);
		}
		if (salud.qs113!=null) {
			salud.qs113=salud.getConvertqs100(salud.qs113);
		}	
		
//		if (!Util.esDiferente(salud.qs110u,1) && txtQS110C1.getText().toString().trim().length()!=0) {
//			salud.qs110c=Integer.parseInt(txtQS110C1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs110u,2) && txtQS110C2.getText().toString().trim().length()!=0) {
//			salud.qs110c=Integer.parseInt(txtQS110C2.getText().toString().trim());
//		}
	
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			} catch (SQLException e) { 
				ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		
		return App.SALUD;
	}
} 
