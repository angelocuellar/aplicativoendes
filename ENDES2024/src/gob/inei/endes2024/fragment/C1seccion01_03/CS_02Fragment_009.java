package gob.inei.endes2024.fragment.C1seccion01_03; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_02Fragment_009 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS206; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS207U; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS207C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS208; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS209; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS210; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS211U; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQS211C; 
	
	public LabelComponent lblpregunta206,lblpregunta206_ind,lblpregunta207,lblpregunta208,lblpregunta209,lblpregunta209_ind,lblpregunta210,lblpregunta211;
	Salud salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSubtitulo; 
	public GridComponent2 gridPreguntas207,gridPreguntas211;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6;
	LinearLayout q7; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String qs208fechref;
	String qs210fechref;
	
	public CS_02Fragment_009() {} 
	public CS_02Fragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS206","QS207U","QS207C","QS208","QS208FECH_REF","QS209","QS210","QS210FECH_REF","QS211U","QS211C","ID","HOGAR_ID","PERSONA_ID","QS23")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS206","QS207U","QS207C","QS208","QS208FECH_REF","QS209","QS210","QS210FECH_REF","QS211U","QS211C")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_SEC02).textSize(21).centrar(); 
		lblSubtitulo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_03_sub).negrita().centrar().colorFondo(R.color.griscabece);
		rgQS206=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs206_1,R.string.cap01_03qs206_2,R.string.cap01_03qs206_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS206ChangeValue").callback("onqrgQS206ChangeValue");
		rgQS207U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs207u_1,R.string.cap01_03qs207u_2).size(altoComponente+30,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS207ChangeValue"); 
		txtQS207C=new IntegerField(this.getActivity()).size(altoComponente+30, 80).maxLength(2); 
		rgQS208=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs208_1,R.string.cap01_03qs208_2,R.string.cap01_03qs208_3).size(altoComponente,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS208ChangeValue"); 
		rgQS209=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs209_1,R.string.cap01_03qs209_2,R.string.cap01_03qs209_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS210=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs210_1,R.string.cap01_03qs210_2,R.string.cap01_03qs210_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS210ChangeValue"); 
		rgQS211U=new RadioGroupOtherField(this.getActivity(),R.string.cap01_03qs211u_1,R.string.cap01_03qs211u_2).size(altoComponente+30,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS211ChangeValue"); 
		txtQS211C=new IntegerField(this.getActivity()).size(altoComponente+30, 80).maxLength(2); 
	
		lblpregunta206 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs206);
		lblpregunta206_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs206_ind).negrita();
		lblpregunta207 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs207u);
		lblpregunta208 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs208);
		lblpregunta209 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs209);
		lblpregunta209_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.cap01_03qs209_ind).negrita();
		lblpregunta210 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs210);
		lblpregunta211 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.cap01_03qs211u);
		
		
		gridPreguntas207=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas207.addComponent(rgQS207U);		
		gridPreguntas207.addComponent(txtQS207C);
		
		gridPreguntas211=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas211.addComponent(rgQS211U);		
		gridPreguntas211.addComponent(txtQS211C);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 

		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q7 = createQuestionSection(lblSubtitulo);
		q1 = createQuestionSection(lblpregunta206,lblpregunta206_ind,rgQS206); 
		q2 = createQuestionSection(lblpregunta207,gridPreguntas207.component()); 
		q3 = createQuestionSection(lblpregunta208,rgQS208); 
		q4 = createQuestionSection(lblpregunta209,lblpregunta209_ind,rgQS209); 
		q5 = createQuestionSection(lblpregunta210,rgQS210); 
		q6 = createQuestionSection(lblpregunta211,gridPreguntas211.component()); 

	 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0); 
		form.addView(q7);
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 

		
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		
		uiToEntity(salud); 
		
		if (salud.qs206!=null) {
			salud.qs206=salud.getConvertqs100(salud.qs206);
		}
		if (salud.qs207u!=null) {
			salud.qs207u=salud.getConvertqs203(salud.qs207u);
		}
		if (salud.qs208!=null) {
			salud.qs208=salud.getConvertqs100(salud.qs208);
			
			if(qs208fechref == null) {
			 	salud.qs208fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs208fech_ref = qs208fechref;
				 }
		}	
		else {
			salud.qs208fech_ref = null;
		}
		if (salud.qs209!=null) {
			salud.qs209=salud.getConvertqs100(salud.qs209);
		}
		if (salud.qs210!=null) {
			salud.qs210=salud.getConvertqs100(salud.qs210);
			
			if(qs210fechref == null) {
			 	salud.qs210fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs210fech_ref = qs210fechref;
				 }
		}
		else {
			salud.qs210fech_ref = null;
		}
		if (salud.qs211u!=null) {
			salud.qs211u=salud.getConvertqs203(salud.qs211u);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		App.getInstance().setPersonabySalud(salud);
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
				
		if (Util.esVacio(salud.qs206)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.206"); 
			view = rgQS206; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(salud.qs206,1)) {
			if (Util.esVacio(salud.qs207u)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.207U"); 
				view = rgQS207U; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs207u, 1)) {
				if (Util.esVacio(salud.qs207c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.207C"); 
					view = txtQS207C; 
					error = true; 
					return false; 
				} 
			}		
			if (Util.esVacio(salud.qs208)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.208"); 
				view = rgQS208; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs208,1)) {
				if (Util.esVacio(salud.qs209)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.209"); 
					view = rgQS209; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs210)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.210"); 
					view = rgQS210; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(salud.qs210,1)) {
					if (Util.esVacio(salud.qs211u)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.211U"); 
						view = rgQS211U; 
						error = true; 
						return false; 
					}				 
					if (!Util.esDiferente(salud.qs211u,1)) {
						if (Util.esVacio(salud.qs211c)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.211C"); 
							view = txtQS211C; 
							error = true; 
							return false; 
						}
						if(!Util.esDiferente(salud.qs211c,0)){
							mensaje = "Opci�n meses no debe ser mayor a 23"; 
							view = txtQS211C; 
							error = true; 
							return false; 
						}
					}					
				}			 
			}	
		}
	if(!Util.esDiferente(salud.qs206, 1)){
//		if((!Util.esDiferente(salud.qs207u, 1) && Util.esMenor(salud.qs207c,10)) ||(!Util.esDiferente(salud.qs207u, 1) && Util.esMenor(salud.qs23,salud.qs207c)) ){
		if((!Util.esDiferente(salud.qs207u, 1) && Util.esMenor(salud.qs207c,5)) ||(!Util.esDiferente(salud.qs207u, 1) && Util.esMenor(salud.qs23,salud.qs207c)) ){
			mensaje = "Edad en la que empez� a tomar alcohol no puede ser menor de 5 a�os"; 
			view = txtQS207C; 
			error = true; 
			return false; 
		}
		if(!Util.esDiferente(salud.qs209, 2) && !Util.esDiferente(salud.qs211u, 1) && Util.esMayor(salud.qs211c,12)){
			mensaje = "Numero de veces que tomo en el mes es mayor al n�mero de veces que tomo en el a�o"; 
			view = txtQS211C; 
			error = true; 
			return false; 
		}
		
	}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    
    	if (salud.qs206!=null) {
			salud.qs206=salud.setConvertqs100(salud.qs206);
		}
    	if (salud.qs207u!=null) {
			salud.qs207u=salud.setConvertqs203(salud.qs207u);
		}
		if (salud.qs208!=null) {
			salud.qs208=salud.setConvertqs100(salud.qs208);
		}
		if (salud.qs209!=null) {
			salud.qs209=salud.setConvertqs100(salud.qs209);
		}
		if (salud.qs210!=null) {
			salud.qs210=salud.setConvertqs100(salud.qs210);
		}
		if (salud.qs211u!=null) {
			salud.qs211u=salud.setConvertqs203(salud.qs211u);
		}
		
    	if(salud==null){ 
    		salud=new Salud(); 
    		salud.id=App.getInstance().getPersonaSeccion01().id; 
    		salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
    		salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
    	} 
    	entityToUI(salud); 
    	
    	qs208fechref = salud.qs208fech_ref;
    	qs210fechref = salud.qs210fech_ref;
    	
    	App.getInstance().setPersonabySalud(salud);
    	inicio(); 
	}

    private void inicio() {
    	RenombrarEtiquetas();
    	onqrgQS206ChangeValue();
    	ValidarsiesSupervisora();
//    	rgQS206.requestFocus();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS206.readOnly();
			rgQS207U.readOnly();
			txtQS207C.readOnly();
			rgQS208.readOnly();
			rgQS209.readOnly();
			rgQS210.readOnly();
			rgQS211U.readOnly();
			txtQS211C.readOnly();
		}
	}
	
    public void RenombrarEtiquetas()
    {
    	Calendar fechaactual = new GregorianCalendar();
    	
    	if(qs208fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs208fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta208.setText(lblpregunta208.getText().toString().replace("F1", F2)); 
    	lblpregunta208.setText(lblpregunta208.getText().toString().replace("F2", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta208.setText(lblpregunta208.getText().toString().replace("F3", F3));

    	Calendar fechahacetreintadias;   	
    	fechaactual = new GregorianCalendar();
    	if(qs210fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qs210fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;			
		}
    	
    	int anio=  fechaactual.get(Calendar.YEAR);  		
//   		if(((GregorianCalendar) fechaactual).isLeapYear(anio)){
//   			fechahacetreintadias= MyUtil.PrimeraFecha(fechaactual, 29);
//   		}
//   		else{
   			fechahacetreintadias = MyUtil.PrimeraFecha(fechaactual, 30);
//   		}
   		
   		lblpregunta210.setText(lblpregunta210.getText().toString().replace("F1", fechahacetreintadias.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechahacetreintadias.get(Calendar.MONTH))));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS206ChangeValue() {
  		if (MyUtil.incluyeRango(2,8,rgQS206.getTagSelected("").toString())) {		
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(getActivity(),rgQS207U,txtQS207C,rgQS208,rgQS209,rgQS210,rgQS211U,txtQS211C);	
  			q2.setVisibility(View.GONE);
  			q3.setVisibility(View.GONE);
  			q4.setVisibility(View.GONE);
  			q5.setVisibility(View.GONE);
  			q6.setVisibility(View.GONE);
  		} 
  		else {
  			Util.lockView(getActivity(),false,rgQS207U,txtQS207C,rgQS208,rgQS209,rgQS210,rgQS211U,txtQS211C);
  			q2.setVisibility(View.VISIBLE);
  			q3.setVisibility(View.VISIBLE);
  			q4.setVisibility(View.VISIBLE);
  			q5.setVisibility(View.VISIBLE);
  			q6.setVisibility(View.VISIBLE);
  			onqrgQS207ChangeValue();
  			onqrgQS208ChangeValue();  		
  			rgQS207U.requestFocus();
  		}	
  	}
    public void onqrgQS207ChangeValue() {
    	if (!MyUtil.incluyeRango(2,8,rgQS206.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,2,rgQS207U.getTagSelected("").toString())) {	     		
     			Util.cleanAndLockView(getActivity(),txtQS207C);      			      			
      			rgQS208.requestFocus();
      		} 
      		else {
      			Util.lockView(getActivity(),false,txtQS207C);   
      			txtQS207C.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS208ChangeValue() {
    	if (!MyUtil.incluyeRango(2,8,rgQS206.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,3,rgQS208.getTagSelected("").toString())) {		
     			MyUtil.LiberarMemoria();
     			Util.cleanAndLockView(getActivity(),rgQS209,rgQS210,rgQS211U,txtQS211C);		
      			q4.setVisibility(View.GONE);
      			q5.setVisibility(View.GONE);
      			q6.setVisibility(View.GONE);
      		} 
      		else {
      			Util.lockView(getActivity(),false,rgQS209,rgQS210,rgQS211U,txtQS211C);	
      			q4.setVisibility(View.VISIBLE);
      			q5.setVisibility(View.VISIBLE);
      			q6.setVisibility(View.VISIBLE);
      			onqrgQS210ChangeValue();
      			rgQS209.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS210ChangeValue() {
    	if (!MyUtil.incluyeRango(2,8,rgQS206.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS208.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,3,rgQS210.getTagSelected("").toString())) {		
     			MyUtil.LiberarMemoria();
     			Util.cleanAndLockView(getActivity(),rgQS211U,txtQS211C);
      			q6.setVisibility(View.GONE);
      		} 
      		else {
      			Util.lockView(getActivity(),false,rgQS211U,txtQS211C);	
      			q6.setVisibility(View.VISIBLE);
      			onqrgQS211ChangeValue();
      			rgQS211U.requestFocus();
      		}
		} 	
  	}
    public void onqrgQS211ChangeValue() {
    	if (!MyUtil.incluyeRango(2,8,rgQS206.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS208.getTagSelected("").toString()) && !MyUtil.incluyeRango(2,8,rgQS210.getTagSelected("").toString())) {
     		if (MyUtil.incluyeRango(2,2,rgQS211U.getTagSelected("").toString())) {		
     			Util.cleanAndLockView(getActivity(),txtQS211C);     			
      		} 
      		else {
      			Util.lockView(getActivity(),false,txtQS211C);   
      			txtQS211C.requestFocus();
      		}
		} 	
  	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs206!=null) {
			salud.qs206=salud.getConvertqs100(salud.qs206);
		}
		if (salud.qs207u!=null) {
			salud.qs207u=salud.getConvertqs203(salud.qs207u);
		}
		if (salud.qs208!=null) {
			salud.qs208=salud.getConvertqs100(salud.qs208);
			if(qs208fechref == null) {
			 	salud.qs208fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs208fech_ref = qs208fechref;
				 }
		}	
		else {
			salud.qs208fech_ref = null;
		}
		if (salud.qs209!=null) {
			salud.qs209=salud.getConvertqs100(salud.qs209);
		}
		if (salud.qs210!=null) {
			salud.qs210=salud.getConvertqs100(salud.qs210);
			
			if(qs210fechref == null) {
			 	salud.qs210fech_ref =  Util.getFechaActualToString();
				}
			else {
			    salud.qs210fech_ref = qs210fechref;
				 }
		}
		else {
			salud.qs210fech_ref = null;
		}
		if (salud.qs211u!=null) {
			salud.qs211u=salud.getConvertqs203(salud.qs211u);
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.SALUD;
	}
} 
