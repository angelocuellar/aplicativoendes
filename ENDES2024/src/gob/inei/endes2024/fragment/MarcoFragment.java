package gob.inei.endes2024.fragment;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogComponent.TIPO_ICONO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TableComponent.ALIGN;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.Dialog.MarcoModificarViviendaDialog;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.Endes_Resultado_vivienda;
import gob.inei.endes2024.model.Endes_omisiones;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.SegmentacionService;
import gob.inei.endes2024.service.UbigeoService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mapswithme.maps.api.MapsWithMeApi;

public class MarcoFragment extends FragmentForm implements Respondible {

	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERIODO;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnCONGLOMERADO;
	HogarClickListener adapter;
	private String TAG = MarcoFragment.this.getClass().getSimpleName();
	public Seccion01 informantedesalud;
	private Seccion01Service seccion01;
	private Seccion01Service Personaservice;
	SeccionCapitulo[] seccionesCargadoInformanteSalud,seccionesCargadoSaludS1_3,seccionesCargadoSaludS4_7;

	private enum ACTION {
		APERTURARC1, BORRAR, ELIMINAR_FILA, APERTURARC2, UPDATE_MARCOADI,RECUPERACION
	};

	private ACTION action;
	private DialogComponent dialog;
	public static SegmentacionFiltro PERIODO;
	public static SegmentacionFiltro ANIO;
	public static SegmentacionFiltro MES;
	public static SegmentacionFiltro CONGLOMERADO;
	private LabelComponent lblViviendas, lblHogares;

	private TableComponent tcMarco, tcHogar,TcOmisiones;
	private LabelComponent lblTitulo, lblperiodo, lblconglomerado;
	private UbigeoService ubigeoService;
	private GridComponent2 grid1;
	private MarcoService service;
	public Integer tselv;
	public List<Marco> marcos;
	private List<Hogar> hogares;
	private SegmentacionService segmentacionService;
	private HogarService hogarService;
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	 private Seccion04_05Service Persona04_05Service;
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesGrabado;
	private List<Endes_omisiones> omisiones;
	public MarcoFragment parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new HogarClickListener();
		tcHogar.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","QH02_1","QH02_2","QH02_3", "HOGAR_ID","estado","QHBEBE","QH40","QH91","QH93","QH224","QH101","QH103","NRO_INFO_S") };
		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
		seccionesCargadoSaludS1_3 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS23","QS100","QS200","QS301")};
		seccionesCargadoSaludS4_7 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS401","QS406","QS409","QS415","QS500","QS601A","QS603","QS700A","QS906")};		
		return rootView;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info;
		info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		if (v.equals(tcMarco.getListView())) {
			
			menu.setHeaderTitle("Opciones de la Vivienda");			
			menu.add(0, 0, 1, "Aperturar Vivienda");
			menu.add(0, 1, 1, "Modificar Vivienda");
			menu.add(0, 2, 1, "Revisar Vivienda");
			menu.add(0, 3, 1, "Ver Mapa");
			menu.add(0, 4, 1, "Revertir Asignacion");
			menu.add(0, 5, 1, "Cambiar N� Vivienda");
			menu.add(0, 6, 1, "Recuperacion de la vivienda");
			try {
				Marco seleccion = (Marco) info.targetView.getTag();
				App.getInstance().setMarco(seleccion);
				Caratula carat = getCuestionarioService().getCaratula(seleccion.id);
				List<Hogar> hogares = getCuestionarioService().getHogares(seleccion.id);
				Visita_Viv resulta_vivienda =getCuestionarioService().getUltima_visitaVivienda(seleccion.id);
				if (carat != null || hogares.size() > 0) {
					menu.removeItem(0);
					menu.removeItem(4);
					
					if(!Util.esDiferente(seleccion.asignado,App.VIVIENDAASIGNADASUPERVISORA)){
						menu.removeItem(0);
						menu.removeItem(1);
						menu.removeItem(5);
					}
					else{
						menu.removeItem(2);
					}
						
				} else {
					menu.removeItem(1);
					menu.removeItem(2);
					menu.removeItem(5);
					if(!Util.esDiferente(seleccion.asignado,App.VIVIENDAASIGNADASUPERVISORA)){
					menu.removeItem(4);
					}
				}
				if(resulta_vivienda!=null && resulta_vivienda.qvvresul!=null && resulta_vivienda.qvvresul==1 || seleccion.asignado==4){
					menu.removeItem(6);
				}
			} catch (ClassCastException e) {
			}
		} else if (v.equals(tcHogar.getListView())) {
			menu.setHeaderTitle("Opciones del Hogar");
			menu.add(1, 0, 1, "Ir a Visitas Hogar");
			menu.add(1, 1, 1, "Ir a Visitas Salud");
			menu.add(1, 2, 1, "Ir a Visitas Individual");

			//			menu.add(1, 3, 1, "Eliminar Hogar");

			Hogar seleccion = (Hogar) info.targetView.getTag();
			if (seleccion == null) {
				return;
			}
			Marco marco = getService().getMarco(seleccion.id);
			Caratula carat = getCuestionarioService().getCaratula(seleccion.id);
			Visita visita = getVisitaService().getUltimaVisita(seleccion.id,seleccion.hogar_id, "ID", "HOGAR_ID", "QHVRESUL");
			if(visita!=null){
				SeleccionarInformantedeSalud(seleccion);
			}
			boolean HabilitarCuandoesMef=false;
			if(App.getInstance().getPersonaSeccion01()!=null && seleccion!=null && seleccion.nro_info_s!=null && App.getInstance().getPersonaSeccion01().qh06!=null && App.getInstance().getPersonaSeccion01().qh07!=null){
				if(!Util.esDiferente(App.getInstance().getPersonaSeccion01().persona_id, seleccion.nro_info_s) 
						&& App.getInstance().getPersonaSeccion01().qh06==2
						&& App.getInstance().getPersonaSeccion01().qh07>=App.HogEdadMefMin 
						&& App.getInstance().getPersonaSeccion01().qh07<=App.HogEdadMefMax){
					HabilitarCuandoesMef= getCuestionarioService().getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id);
//					Log.e("sss","1111"+HabilitarCuandoesMef);
				}else{
					if(App.getInstance().getPersonaSeccion01()!=null && App.getInstance().getPersonaSeccion01().qh06!=null && App.getInstance().getPersonaSeccion01().qh06==2 && App.getInstance().getPersonaSeccion01().qh07>=50){
						HabilitarCuandoesMef=true;
//						Log.e("sss","22222"+HabilitarCuandoesMef);
						}
					else{
						HabilitarCuandoesMef=true;
//						Log.e("sss","33333"+HabilitarCuandoesMef);
						}
				}
			}
//			boolean flagMef= false;
//			flagMef=getPersonaService().getCantidadMef(seleccion.id,seleccion.hogar_id,15,49);L
//			Log.e("","MEF: "+HabilitarCuandoesMef);
			if(visita!=null && visita.qhvresul!=null){
					if(Util.esDiferente(visita.qhvresul,1)){
						Log.e("AQUII","");
						menu.getItem(1).setVisible(false);
					}
					if(!Util.esDiferente(visita.qhvresul,1)){
						Log.e("AQUII","COMPLETO HOGAR: ");
						if(!HabilitarCuandoesMef){
							Log.e("AQUII","11111");
							menu.getItem(1).setVisible(false);	
						}
						else{
							Log.e("AQUII","222222");
							menu.getItem(1).setVisible(true);
						}
					}
			}
			else{
				Log.e("AQUII","ELSE GENERAL");
				menu.getItem(1).setVisible(false);
				menu.getItem(2).setVisible(false);
			}
			
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		if (item.getGroupId() == 0) {
			Marco seleccion = (Marco) info.targetView.getTag();
			App.getInstance().setMarco(seleccion);

			((CuestionarioFragmentActivity) parent).setTitulo(Util.getText(seleccion.conglomerado + " - "+ Util.getText(seleccion.nselv)));

			switch (item.getItemId()) {
			case 0:
				aperturarVivienda(seleccion);
				break;
			case 1:
				irCaratula();
				break;
			case 2:
				irCaratula();
				break;
			case 3:
				verMapa(seleccion);
				break;
			case 4:
				revertirAsignacion(seleccion);
				break;
			case 5:
				cargarDialogoModificar(seleccion,(SegmentacionFiltro) spnPERIODO.getValue(),(SegmentacionFiltro) spnCONGLOMERADO.getValue());
				break;
			case 6:
				RecuperarVivienda(seleccion);
				break;
			}
		} else if (item.getGroupId() == 1) {
			Hogar seleccion = (Hogar) info.targetView.getTag();
			App.getInstance().setHogar(seleccion);
			Marco marco = getService().getMarco(seleccion.id);
			App.getInstance().setMarco(marco);
			((CuestionarioFragmentActivity) parent).setTitulo(Util.getText(marco.conglomerado)+ " - "+ Util.getText(marco.nselv)
					+ " - "	+ Util.completarCadena(Util.getText(seleccion.hogar_id),"0", 2, COMPLETAR.IZQUIERDA));
			Visita visita = getVisitaService().getPrimeraVisitaAceptada(seleccion.id, seleccion.hogar_id);
			App.getInstance().setVisita(visita);
			boolean entraaseccion04=false; 
			entraaseccion04 = getSeccion04_05Service().getEntraaSeccion04_05(seleccion.id, seleccion.hogar_id);
			App.getInstance().getHogar().existeseccion04=entraaseccion04;
			boolean flag= getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id,seleccion.hogar_id,0,11);
			boolean flagMef= false;
			flagMef=getPersonaService().getCantidadMef(seleccion.id,seleccion.hogar_id,App.HogEdadMefMin,App.HogEdadMefMax);
			if(visita!=null){
			SeleccionarInformantedeSalud(seleccion);
			}
			if(App.getInstance().getPersonaSeccion01()!=null){
				Salud salud = getCuestionarioService().getSaludNavegacionS1_3(App.getInstance().getMarco().id,seleccion.hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS1_3);
				if (salud!=null) {
					App.getInstance().setSaludNavegacionS1_3(salud);						
					App.getInstance().getSaludNavegacionS1_3().qh06=App.getInstance().getPersonaSeccion01().qh06;						
				}		
				
				CAP04_07 cap04_07 = getCuestionarioService().getSaludNavegacionS4_7(App.getInstance().getMarco().id,seleccion.hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS4_7);
				if (cap04_07!=null) {
					App.getInstance().setSaludNavegacionS4_7(cap04_07);	
					App.getInstance().getSaludNavegacionS4_7().existes8=flag;
					App.getInstance().getSaludNavegacionS4_7().existemef=flagMef;
				}
			}
			CSVISITA visitaSalud= getVisitaService().getUltimaVisitaSalud(App.getInstance().getMarco().id,seleccion.hogar_id, "ID", "HOGAR_ID", "QSVRESUL");
			if(visitaSalud!=null){
				App.getInstance().setC1Visita(visitaSalud);
			}
			switch (item.getItemId()) {
			case 0:
				irVisitas();
				break;
			case 1:
				try {
					SeleccionarInformantedeSalud(seleccion);
					goTo(CuestionarioFragmentActivity.CSVISITA);
				} catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case 2:
				goTo(CuestionarioFragmentActivity.CISECCION_f_carat);
				return true;
//			case 3:
//				eliminar((Hogar) hogares.get(info.position));
//				break;
			}
		}

		return super.onContextItemSelected(item);
	}
	
	public void SeleccionarInformantedeSalud(Hogar hogar){
		App.getInstance().setHogar(hogar);
		if(hogar!=null){
		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(hogar.id, hogar.hogar_id,  getVisitaService(), getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersonanformanteSalud(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id,persona_id,seccionesCargadoInformanteSalud);
		App.getInstance().SetPersonaSeccion01(informantedesalud);
		}
		else{
//			Log.e("","NULL");
			App.getInstance().SetPersonaSeccion01(null);
		}
	}
	private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
}
	public Seccion01Service getPersonaService()
    {
    	if(Personaservice == null)
    	{
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }

	private void updateMarcoAdi(Marco marco) {
		action = ACTION.UPDATE_MARCOADI;
		dialog = new DialogComponent(getActivity(),	this,TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Esta seguro que desea revertir la viv: "
						+ marco.nselv==null?"":marco.nselv
						+ " al Marco Adicional. Considerar que "
						+ "si registr\u00f3 informaci\u00f3n en esta Vivienda; estas ser\u00e1n elimanadas. Esta seguro que desa continuar?");
		dialog.put("bean", marco);
		dialog.showDialog();
	}

	private void irCapitulo100(Hogar bean) {
		parent.nextFragment(CuestionarioFragmentActivity.SECCION01);
//		if (bean.hogar_id == 1) {
//			parent.nextFragment(CuestionarioFragmentActivity.HOGAR);
//		} else {
//			parent.nextFragment(CuestionarioFragmentActivity.HOGAR + 2);
//		}
	}

	private void goTo(int fragment) {
		parent.nextFragment(fragment);
	}

	public void eliminar(Hogar bean) {
		action = ACTION.ELIMINAR_FILA;
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Esta seguro que desea borrar el hogar?");
		dialog.put("bean", bean);
		dialog.showDialog();
	}

	public HogarService getHogarService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(lblTitulo);
		LinearLayout q1 = createQuestionSection(0, Gravity.CENTER,LinearLayout.HORIZONTAL, grid1.component());
		LinearLayout q5 = createQuestionSection(q1);
		LinearLayout q2 = createQuestionSection(lblViviendas,tcMarco.getTableView());
		LinearLayout q3 = createQuestionSection(lblHogares, tcHogar.getTableView());
		LinearLayout q4 = createQuestionSection(0, TcOmisiones.getTableView());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q5);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);

		return contenedor;
	}

	@Override
	protected void buildFields() {
		
		lblTitulo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_marco1).size(altoComponente, MATCH_PARENT).centrar().textSize(20);
		lblViviendas = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_vivienda).size(50, MATCH_PARENT).centrar().textSize(20);
		lblHogares = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_hogares).size(50, MATCH_PARENT).centrar().textSize(20);
		lblperiodo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_periodo).size(50, MATCH_PARENT).centrar().textSize(18);
		lblconglomerado = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_conglomerado).size(50, MATCH_PARENT).centrar().textSize(18);
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(400, 770).headerHeight(70).headerTextSize(18).dataColumHeight(60);	
//			tcHogar = new TableComponent(getActivity(), this,App.ESTILO).size(300, 700).dataColumHeight(60).headerHeight(60).headerTextSize(18);
//		}
//		else{
			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(600, 770).headerHeight(45).headerTextSize(15).dataColumHeight(40);
			tcHogar = new TableComponent(getActivity(), this,App.ESTILO).size(500, 700).dataColumHeight(40).headerHeight(45).headerTextSize(15);
//		}
		tcMarco.addHeader(R.string.m_orden_vivienda, 0.16f);
		tcMarco.addHeader(R.string.m_nselv_vivienda, 0.2f, ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_conglomerado, 0.22f, ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_tself_vivienda, 0.16f, ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_usuario_asignado, 0.3f, ALIGN.CENTER);
		tcMarco.setColorCondition("ASIGNADO", Util.getHMObject("3",R.color.dark_pink_agua,"1",R.color.azulmarino_claro,"4",R.color.dark_purpura));
		
		
		
		tcHogar.addHeader(R.string.m_nro_hogar, 0.4f);
		tcHogar.addHeader(R.string.m_jefe_hogar, 1f, ALIGN.LEFT);
		
		TcOmisiones = new TableComponent(getActivity(), this,App.ESTILO).size(600, 875).headerHeight(45).headerTextSize(16).dataColumHeight(40);
		TcOmisiones.addHeader(R.string.oms_vivienda,0.5f);
		TcOmisiones.addHeader(R.string.oms_cuestionario,0.4f);
		TcOmisiones.addHeader(R.string.oms_seccion,1.0f);
		TcOmisiones.addHeader(R.string.oms_hogar,0.5f);
//		TcOmisiones.addHeader(R.string.oms_entrevistadora,1.7f);
		TcOmisiones.addHeader(R.string.oms_resultado,0.7f);
		TcOmisiones.addHeader(R.string.oms_persona,1.0f);
		TcOmisiones.addHeader(R.string.oms_r01,0.5f);
		TcOmisiones.addHeader(R.string.oms_r02,0.4f);
		spnPERIODO = new SpinnerField(getActivity()).size(altoComponente + 15,250).callback("onPeriodoChangeValue");
		spnCONGLOMERADO = new SpinnerField(getActivity()).size(altoComponente + 15, 250).callback("onConglomeradoChangeValue");
		MyUtil.llenarPeriodo(getActivity(), getSegmentacionService(),spnPERIODO);
		
		grid1 = new GridComponent2(getActivity(),App.ESTILO, 2);
		grid1.addComponent(lblperiodo);
		grid1.addComponent(lblconglomerado);
		grid1.addComponent(spnPERIODO);
		grid1.addComponent(spnCONGLOMERADO);
	}

	public void recargarLista() {
		MyUtil.DelDatosGeolocalizacion();
		tcMarco.setData(marcos, "nroorden", "nselv", "conglomerado", "getTipoVivienda", "usuario_asig");

		for (int row = 0; row < marcos.size(); row++) {
			if (obtenerEstado(marcos.get(row)) == 1) {
				// borde de color azul
				tcMarco.setBorderRow(row, true);
			} else if (obtenerEstado(marcos.get(row)) == 2) {
				// borde de color rojo
				tcMarco.setBorderRow(row, true, R.color.red);
			} else {
				tcMarco.setBorderRow(row, false);
			}
		}

		tcMarco.reloadData();

		registerForContextMenu(tcMarco.getListView());
		MarcoAdapter adapter = new MarcoAdapter(marcos);
		tcMarco.getListView().setOnItemClickListener(adapter);
	}
	public void recargarOmisiones(){
		TcOmisiones.setData(omisiones, "vivienda", "cuestionario", "seccion", "hogar","getResultado","persona","resultado1","resultado2");
	}
	private int obtenerEstado(Marco detalle) {
		
		Visita_Viv resulta_vivienda =getCuestionarioService().getUltima_visitaVivienda(detalle.id);
		
		
		if(resulta_vivienda!=null && resulta_vivienda.qvvresul!=null && resulta_vivienda.qvvresul==1){
//			Log.e("resulta_vivienda 111",""+resulta_vivienda.qvvresul);
			return 1;
		}
		else if (resulta_vivienda!=null && resulta_vivienda.qvvresul!=null && resulta_vivienda.qvvresul!=1) {
//			Log.e("resulta_vivienda <>",""+resulta_vivienda.qvvresul);
			return 2;
		}
		return 0;
		
//		if (!Util.esDiferente(detalle.resviv, 0)) {
//			return 1;
//		} else if (!Util.esDiferente(detalle.resviv,2,3,4,5,6,7,8,9)) {
//			return 2;
//		}
//		return 0;
	}

	public void onPeriodoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) component.getValue();
		MyUtil.llenarConglomerado(getActivity(), getSegmentacionService(),spnCONGLOMERADO, periodo);
		//Log.d("onPeriodoChangeValue", "secuencia1: "+"onPeriodoChangeValue "+PERIODO+" - "+CONGLOMERADO);
	}

	public void onConglomeradoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) spnPERIODO.getValue();
		SegmentacionFiltro conglomerado = (SegmentacionFiltro) component.getValue();
		if (periodo == null || conglomerado == null) {
			return;
		}
		if (periodo != null && conglomerado != null && !conglomerado.nombre.equals("-- CONGLOMERADO --")) { //
			cargarMarco(periodo, conglomerado);
		}
	}

	public void cargarMarco(SegmentacionFiltro periodo,
			SegmentacionFiltro conglomerado) {
		PERIODO = periodo;
		CONGLOMERADO = conglomerado;
		if (conglomerado == null || periodo == null) {
			return;
		}
		marcos = getService().getMarco(periodo.id, conglomerado.nombre);
		omisiones=getService().getOmisiones(conglomerado.nombre);
		Segmentacion seg = getSegmentacionService().getSegmentacion(periodo.id,	conglomerado.nombre);
		App.getInstance().setSegmentacion(seg);
		recargarLista();
		recargarOmisiones();
		if (hogares != null) {
			hogares.clear();
			cargarHogares();
		}
	}

	@Override
	public boolean grabar() {
		return true;
	}

	private CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	@Override
	public void cargarDatos() {
		App.getInstance().setMarco(null);
		App.getInstance().setHogar(null);
		if (PERIODO != null) {
			spnPERIODO.setSelectionKey(PERIODO.id);
			onPeriodoChangeValue(spnPERIODO);
		}
		if (CONGLOMERADO != null) {
			spnCONGLOMERADO.setSelectionKey(CONGLOMERADO.id);
			onConglomeradoChangeValue(spnCONGLOMERADO);
		}

		((CuestionarioFragmentActivity) parent).setTitulo(null);
		((CuestionarioFragmentActivity) parent).setSubTitulo(null);

	}

	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(getActivity());
		}
		return service;
	}

	public SegmentacionService getSegmentacionService() {
		if (segmentacionService == null) {
			segmentacionService = SegmentacionService.getInstance(getActivity());
		}
		return segmentacionService;
	}

	public UbigeoService getUbigeoService() {
		if (ubigeoService == null) {
			ubigeoService = UbigeoService.getInstance(getActivity());
		}
		return ubigeoService;
	}

	private void aperturarVivienda(Marco vivienda) {
		action = ACTION.APERTURARC1;
		
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Desea aperturar la vivienda N� " + (vivienda.nselv==null?"":vivienda.nselv),TIPO_ICONO.INFO);
		dialog.put("marco", vivienda);
		dialog.showDialog();
	}
	
	private void verMapa(Marco vivienda) {			
		
		cargarMapa(Double.valueOf(vivienda.p18_1), Double.valueOf(vivienda.p18_2), "Ubicaci�n actual.");
	}
	
	private void cargarMapa(Double lati, Double longi, String msj) {
		final double lat = lati;
		final double lon = longi;
		final String name = msj;
		MapsWithMeApi.showPointOnMap(getActivity(), lat, lon, name);
	}
	
	private void irCaratula() {
		parent.nextFragment(CuestionarioFragmentActivity.CARATULA);
	}
	private void revertirAsignacion(Marco vivienda){
		vivienda.asignado=App.VIVIENDADESAGNADA;
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ASIGNADO") };
		boolean flag = false;
		try {
			flag=getCuestionarioService().saveOrUpdate(vivienda, seccionesGrabado);	
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(!flag){
			return;
		}
		marcos = getService().getMarco(PERIODO.id, CONGLOMERADO.nombre);
		recargarLista();
	}
	private void RecuperarVivienda(Marco vivienda){
		action = ACTION.RECUPERACION;
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Desea aperturar la vivienda N� " + vivienda.nselv,TIPO_ICONO.INFO);
		dialog.put("marco", vivienda);
		dialog.showDialog();
	}
	private void GrabadodeRecuperacion(){
		Marco marco = (Marco) dialog.get("marco");
		marco.asignado=App.VIVIENDARECUPERADA;
		marco.f_recuperacion=Util.getFechaActualToString();
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ASIGNADO","F_RECUPERACION") };
		boolean flag = false;
		try {
			flag=getCuestionarioService().saveOrUpdate(marco, seccionesGrabado);	
		} catch (Exception e) {
		}
		if(!flag){
			return;
		}
		marcos = getService().getMarco(PERIODO.id, CONGLOMERADO.nombre);
		recargarLista();
	}
	private void cargarDialogoModificar(Marco marco,SegmentacionFiltro periodo,SegmentacionFiltro conglomerado){
		FragmentManager fm = MarcoFragment.this.getFragmentManager();
		MarcoModificarViviendaDialog aperturaDialog = MarcoModificarViviendaDialog.newInstance(MarcoFragment.this, marco);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	public void refrescarHogares(Hogar bean) {
		if (hogares.contains(bean)) {
			return;
		}
		hogares.add(bean);
		cargarHogares();
		grabar();
	}

	private void cargarHogares() {
		tcHogar.setData(hogares, "hogar_id", "getJefeHogar");
		registerForContextMenu(tcHogar.getListView());
		HogarAdapter adapter = new HogarAdapter(hogares);
		tcHogar.setBorder("estado");
		registerForContextMenu(tcHogar.getListView());
//		tcHogar.getListView().setOnItemClickListener(adapter);
	}

	private void irVisitas() {
		parent.nextFragment(CuestionarioFragmentActivity.VISITA);
	}

	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}

	private class MarcoAdapter implements OnItemClickListener {

		private List<Marco> localesAdapter;

		public MarcoAdapter(List<Marco> objetos) {
			localesAdapter = new ArrayList<Marco>();
			localesAdapter.addAll(objetos);
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
			Marco marco = localesAdapter.get(position);
			App.getInstance().setMarco(marco);
			tselv = marco.tselv;
			hogares = getHogarService().getHogares(marco.id,seccionesCargado);
			cargarHogares();
		}
	}

	private class HogarAdapter implements OnItemClickListener {
		private List<Hogar> hogarAdapter;

		public HogarAdapter(List<Hogar> objetos) {
			hogarAdapter = new ArrayList<Hogar>();
			hogarAdapter.addAll(objetos);
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
			Hogar hogar = hogarAdapter.get(position);
			hogares = getHogarService().getHogares(hogar.id,seccionesCargado);
			cargarHogares();
		}
	}
	public Seccion04_05Service getSeccion04_05Service()
    {
    	if(Persona04_05Service == null)
    	{
    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
    	}
    	return Persona04_05Service;
    }

	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
		switch (action) {
		case APERTURARC1:
			if (dialog == null) {
				return;
			}
			Marco m = (Marco) dialog.get("marco");
			App.getInstance().setMarco(m);
			parent.nextFragment(CuestionarioFragmentActivity.CARATULA);
			break;
		case BORRAR:
			break;
		case ELIMINAR_FILA:
			Hogar bean = (Hogar) dialog.get("bean");
			hogares.remove(bean);
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id = App.getInstance().getHogar().hogar_id;
			getHogarService().borrarHogar(bean);
			cargarHogares();
			break;
		case UPDATE_MARCOADI:
			Marco marco = (Marco) dialog.get("bean");
			getService().borrarHogares(marco);
			try {
				int totViv = getService().getTotalViendasAdi(MarcoFragment.PERIODO.id,MarcoFragment.CONGLOMERADO.nombre);
				marco.nroorden = Util.completarCadena(String.valueOf(totViv + 1), "0", 3,COMPLETAR.IZQUIERDA);
				if (!getService().saveOrUpdate(marco)) {
					throw new SQLException(	"Los datos no pudieron ser guardados.");
				}
				cargarMarco(MarcoFragment.PERIODO, MarcoFragment.CONGLOMERADO);
			} catch (SQLException e) {
				ToastMessage.msgBox(getActivity(), "Error: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
			break;
		case RECUPERACION:
			GrabadodeRecuperacion();
			break;
		default:
			ToastMessage.msgBox(getActivity(), "Operaci�n no definida.",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			break;
		}
	}
	private class HogarClickListener implements OnItemClickListener {
		public HogarClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			Hogar c = (Hogar) hogares.get(arg2);
			App.getInstance().setHogar(c);
			parent.nextFragment(CuestionarioFragmentActivity.CARATULA);		
		}
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}

}