package gob.inei.endes2024.fragment.caratula.dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.caratula.CARATULAFragment_002;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarDialog extends DialogFragmentComponent {

	public interface HogarDialogListener {
		void onFinishEditDialog(String inputText);
	}

	@FieldAnnotation(orderIndex = 1)
	public IntegerField txtHOGAR_ID;
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex = 3)
	public ButtonComponent btnCancelar;
	
	private static CARATULAFragment_002 caller;
	private CuestionarioService service;
	LinearLayout q0, q1, q2, q3, q4;
	Hogar bean;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas;
	private CuestionarioService hogarService;
	private GridComponent2 gridJefe;
	private LabelComponent lblHogar;
	private List<Hogar> detalles;
	public static HogarDialog newInstance(FragmentForm pagina, Hogar detalle) {
		caller = (CARATULAFragment_002) pagina;
		HogarDialog f = new HogarDialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public HogarDialog() {
		super();
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "HOGAR_ID") };
//		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,
//				-1, "ID", "HOGAR_ID","QH02_3", "QH03", "QH04") };

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Hogar) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE); // decir q no
																	// muestre
																	// seccion
																	// de titulo
																	// de
																	// dialogo
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	private void cargarDatos() {
		entityToUI(bean);
		caretaker.addMemento("antes", bean.saveToMemento(Hogar.class));
		inicio();
	}

	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(gridJefe.component());

		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(botones);

		return contenedor;
	}

	@Override
	protected void buildFields() {
		lblHogar = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente, 220).text(R.string.t_caratulahogar2).textSize(18).negrita().alinearIzquierda();
		txtHOGAR_ID = new IntegerField(this.getActivity()).maxLength(1).size(altoComponente, 50);
		gridJefe = new GridComponent2(this.getActivity(), 2);
		gridJefe.addComponent(lblHogar);
		gridJefe.addComponent(txtHOGAR_ID);
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),	App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(	R.string.btnCancelar).size(200, 60);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				HogarDialog.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarHogares(bean);
				caller.MostrarBotonIncentivos();
				HogarDialog.this.dismiss();
			}
		});

	}

	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}

	public boolean grabar() {
		uiToEntity(bean);
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		Integer cantidad=bean.hogar_id;
		try {
			for (int i = 1; i <=cantidad ; i++) {
				
				Hogar bean= new Hogar();
				bean.id=App.getInstance().getMarco().id;
				bean.hogar_id=i;
				flag = getCuestionarioService().saveOrUpdate(bean, dbTX,
						seccionesGrabado);	
//				detalles.add(bean);
			}
			
		
			if (!flag) {
				throw new Exception(
						"Ocurri� un problema al grabar los datos de la caratula.");
			}
			getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}

	private boolean validar() {
		if(Util.esVacio(bean.hogar_id))
		{ 
		mensaje = "cantidad de hogares no puede estar vacio";
        view = txtHOGAR_ID;
        error = true;
        return false;
		}
		if(Util.esMayor(bean.hogar_id, 5))
		{
			mensaje ="cantidad de hogares no puedeser mayor a 5";
	        view = txtHOGAR_ID;
	        error = true;
	        return false;	
		}
		return true;
	}

	private void inicio() {

	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}

}