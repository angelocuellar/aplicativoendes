package gob.inei.endes2024.fragment.caratula; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.CapturadorGPS;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.activity.CuestionarioSaludFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.caratula.dialog.HogarDialog;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_001;
import gob.inei.endes2024.fragment.seccion01.Dialog.Seccion01_001Dialog;
import gob.inei.endes2024.fragment.visita.Dialog.Viv_VisitaDialog;
import gob.inei.endes2024.fragment.visita.Dialog.Viv_VisitaDialog.ACTION;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.mapswithme.maps.api.MapsWithMeApi;

import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
public class CARATULAFragment_002 extends FragmentForm  implements Respondible{

	@FieldAnnotation(orderIndex = 1)
	public TextField txtCONGLOMERADO;
	@FieldAnnotation(orderIndex = 2)
	public IntegerField txtNSELV;
	@FieldAnnotation(orderIndex = 3)
	public SpinnerField spnTIPVIA;
	@FieldAnnotation(orderIndex = 4)
	public TextField txtTIPVIA_O;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtNOMVIA;
	@FieldAnnotation(orderIndex = 6)
	public IntegerField txtN_HOGAR;
	@FieldAnnotation(orderIndex = 7)	
	public TextField txtGPS_LAT;
	@FieldAnnotation(orderIndex = 8)
	public TextField txtGPS_LONG;
	@FieldAnnotation(orderIndex = 9)
	public TextField txtGPS_ALT;
	@FieldAnnotation(orderIndex = 10)
	public SpinnerField spnRESVIV;
	@FieldAnnotation(orderIndex = 11)
	public TextField txtQHVRESUL_O;

	Marco marco;
	Caratula bean; 
//	Visita visita;
	Visita_Viv visita_viv;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblFechaFinal,lblResultadoFinal,dummy,dummy2,lblTitulo,lblTitulo2,lblTitulo3,lblTitulo4, lblMsg, lblTOT_HOGAR,lblpregunta1,lblCordenadas1, lblCordenadas2, lblCordenadas3,lblConglomerado, lblSelVivienda, lbltipovia,lblnomvia;
	public TableComponent tcHOGARES;
	public List<Hogar> detalles;
	private MarcoService service;
	public ButtonComponent btnIniciar;	
	private TableComponent tcVisita;
	private DialogComponent dialog;
	private VisitaService visitaService;
	private Seccion01Service seccion01;
	private HogarService hogarService;
	public List<Seccion01> posiblesinformantessalud;
	public Seccion01 informantedesalud;
	public GridComponent grid;
	 public GridComponent2 tblGps, tblResultadovivienda,thogares,tblConglomerado,tblDireccion;
	public List<Seccion04_05> detalles04_05;
	private Seccion04_05Service Persona04_05Service;
	private Seccion01Service Personaservice;
	public Seccion01 informantehogar;
	private CapturadorGPS tracker;
	private List<Visita_Viv> visitas;
	private Visita_Viv ultimaVisita,menorVisita;
	Integer viv_reserva;
	
	public boolean grabarcantidad=false;
	private SeccionCapitulo[] seccionesCargado,seccionesCargadoInformanteSalud,seccionesCargadovisita,seccionesCargadoS,seccionesCargadoSaludS1_3,seccionesCargadoSaludS4_7,seccionesCargadoSaludS8,seccionesCargadoInd;
	private SeccionCapitulo[] seccionesGrabado,seccionesGrabadohogar,seccionesCargado_viv,seccionesGrabado_viv,seccionesGrabadoMarco;

	private enum PROCESS {
		ELIMINAR_FILA,ELIMINAR_VISITA_VIV
	}

	private PROCESS action;

	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;

	private ButtonComponent btnAgregar,btnResultado,btnGPS,btnVerenMapa,btnFinalizar, btnIncentivos;
    public boolean bandera=false;

    public CARATULAFragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","N_HOGAR","GPS_LAT", "GPS_LONG", "GPS_ALT","RESVIV","QHVRESUL_O","CONGLOMERADO","NSELV","TIPVIA", "TIPVIA_O", "NOMVIA","TIPSEL","NOM_DPTO","PROVINCIA","DISTRITO","CCCPP","NOM_CCPP","ZON_NUM","MZA","AER_INI", "AER_FIN","VIVIENDA","NOMVIA", "PTANUM", "BLOCK", "INTERIOR","PISO", "MZ", "LOTE", "KM")};
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "N_HOGAR","GPS_LAT", "GPS_LONG", "GPS_ALT","RESVIV","QHVRESUL_O","CONGLOMERADO","NSELV","TIPVIA", "TIPVIA_O", "NOMVIA","TIPSEL","NOM_DPTO","PROVINCIA","DISTRITO","CCCPP","NOM_CCPP","ZON_NUM","MZA","AER_INI", "AER_FIN","VIVIENDA","NOMVIA", "PTANUM", "BLOCK", "INTERIOR","PISO", "MZ", "LOTE", "KM") };
		
		seccionesCargado_viv = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QVVDIA_INI", "QVVMES_INI", "QVVANIO_INI", "QVHORA_INI","QVMIN_INI", "QVVRESUL", "QVVRESUL_R", "QVVHORA_FIN", "QVVMIN_FIN","QVVDIAP", "QVVMESP", "QVVHORAP", "QVVMINUP", "QVVRESUL_O","QVGPS_LONG","QVGPS_LAT","QVGPS_ALT", "QVCONDICION", "QVCONDICION_F", "QVCONDICION_FN", "QVINFORMANTE", "QVINFORMANTE_O", "NRO_VISITA","ID") };
		seccionesGrabado_viv = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QVVDIA_INI", "QVVMES_INI", "QVVANIO_INI", "QVHORA_INI","QVMIN_INI", "QVVHORA_FIN", "QVVMIN_FIN", "QVVRESUL", "QVVRESUL_R","QVVDIAP", "QVVMESP", "QVVHORAP", "QVVMINUP", "QVVRESUL_O","QVGPS_LONG","QVGPS_LAT","QVGPS_ALT", "QVCONDICION", "QVCONDICION_F", "QVCONDICION_FN", "QVINFORMANTE", "QVINFORMANTE_O") };
		
		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
		seccionesGrabadohogar = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"HOGAR_ID")};
		seccionesCargadoS = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS28", "QS29A", "QS29B")};
		seccionesCargadoSaludS1_3 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS23","QS100","QS200","QS301")};
		seccionesCargadoSaludS4_7 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS401","QS406","QS409","QS415","QS500","QS601A","QS603","QS700A","QS906")};
		seccionesCargadoSaludS8 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS802D")};
		
		seccionesCargadoInd= new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QIVRESUL")};
		seccionesGrabadoMarco= new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"NSELV")};
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c1_hogar).textSize(21).centrar().negrita();
		lblTitulo2 = new LabelComponent(this.getActivity(),	App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c1_hogar2).textSize(21).centrar().negrita();
		
		lblFechaFinal = new LabelComponent(this.getActivity()).size(altoComponente, 500).textSize(18).centrar().negrita();
		lblResultadoFinal = new LabelComponent(this.getActivity()).size(altoComponente+10, 500).textSize(18).centrar().negrita();
		
		lblConglomerado = new LabelComponent(this.getActivity()).size(altoComponente, 210).text(R.string.c_caratulaconglomerado).textSize(16).negrita().colorFondo(R.color.griscabece);
		txtCONGLOMERADO = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 210).alfanumerico().readOnly().centrar();
		lblSelVivienda = new LabelComponent(this.getActivity()).size(altoComponente, 200).text(R.string.c_vivi_seleccionada).textSize(16).centrar().negrita().colorFondo(R.color.griscabece);
		txtNSELV = new IntegerField(this.getActivity()).maxLength(3).size(altoComponente, 146).centrar();
        
               
        lbltipovia = new LabelComponent(this.getActivity()).text(R.string.c_tipodevia).size(altoComponente+13, 210).negrita().alinearIzquierda().colorFondo(R.color.griscabece);
        spnTIPVIA = new SpinnerField(this.getActivity()).size(altoComponente+13, 250).callback("salto_tipo_via");
        llenarTipovia();
        txtTIPVIA_O = new TextField(this.getActivity()).maxLength(60).size(altoComponente+13, 310).hint(R.string.especifique).soloTextoNumero();
        
        lblnomvia = new LabelComponent(this.getActivity()).text(R.string.c_nombrevia).size(altoComponente, 210).negrita().alinearIzquierda().colorFondo(R.color.griscabece);
        txtNOMVIA = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 560).alfanumerico();    
		
		lblpregunta1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.carat_p10);
		lblTOT_HOGAR = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.carat_p10_1).textSize(16).centrar();
		txtN_HOGAR = new IntegerField(this.getActivity()).size(altoComponente+5, 80).maxLength(1).readOnly().centrar();
		Spanned textomsj= Html.fromHtml("<b>Sr(a): Si hogar</b> es la persona o grupo de personas que se alimentan de una misma olla y atienden en com�n otras necesidades b�sicas.");
		lblMsg = new LabelComponent(this.getActivity())	.size(WRAP_CONTENT, WRAP_CONTENT).textSize(18).centrar().colorFondo(R.color.amarillo);
		lblMsg.setText(textomsj);
		
		
		tcHOGARES = new TableComponent(getActivity(), this,	App.ESTILO).size(350, 600).headerHeight(45).dataColumHeight(45);				
		tcHOGARES.addHeader(R.string.m_nro_hogar, 0.8f,	TableComponent.ALIGN.RIGHT);
		tcHOGARES.addHeader(R.string.m_jefe_hogar, 2f, TableComponent.ALIGN.LEFT);
		
		txtGPS_LAT = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 170).alfanumerico().hint(R.string.c_latitud).readOnly();
		txtGPS_LONG = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 170).alfanumerico().hint(R.string.c_longitud).readOnly();
		txtGPS_ALT = new TextField(this.getActivity()).maxLength(25).size(altoComponente+5, 118).alfanumerico().hint(R.string.c_altitud).readOnly();
				
		lblCordenadas1 = new LabelComponent(this.getActivity()).size(altoComponente+5, 99).text(R.string.c_latitud).textSize(16).negrita().colorFondo(R.color.griscabece).alinearIzquierda();
		lblCordenadas2 = new LabelComponent(this.getActivity()).size(altoComponente+5, 117).text(R.string.c_longitud).textSize(16).negrita().colorFondo(R.color.griscabece).alinearIzquierda();
		lblCordenadas3 = new LabelComponent(this.getActivity()).size(altoComponente+5, 98).text(R.string.c_altitud).textSize(16).negrita().colorFondo(R.color.griscabece).alinearIzquierda();
		
		
		btnResultado = new ButtonComponent(getActivity(), App.ESTILO_BOTON).size(200,40).text(R.string.c_resultado);
		btnGPS = new ButtonComponent(getActivity(), App.ESTILO_BOTON).size(200, altoComponente).text(R.string.c_gps);
		btnVerenMapa = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200, 55).text(R.string.btnMapa);
		btnAgregar = new ButtonComponent(getActivity(),	App.ESTILO_BOTON).text(R.string.btnAgregar).size(190, altoComponente);
		btnIncentivos = new ButtonComponent(getActivity(),	App.ESTILO_BOTON).text(R.string.btnIncentivos).size(190, altoComponente);
		
		tblConglomerado = new GridComponent2(this.getActivity(),App.ESTILO, 4);
		tblConglomerado.addComponent(lblConglomerado);
		tblConglomerado.addComponent(txtCONGLOMERADO);
		tblConglomerado.addComponent(lblSelVivienda);
		tblConglomerado.addComponent(txtNSELV);  
     
		 tblDireccion = new GridComponent2(this.getActivity(), 3);
         tblDireccion.addComponent(lbltipovia);
         tblDireccion.addComponent(spnTIPVIA);
         tblDireccion.addComponent(txtTIPVIA_O);
         tblDireccion.addComponent(lblnomvia);
         tblDireccion.addComponent(txtNOMVIA,2);

         thogares = new GridComponent2(this.getActivity(),Gravity.CENTER,3,0);
         thogares.addComponent(lblTOT_HOGAR);
         thogares.addComponent(txtN_HOGAR);
         thogares.addComponent(btnAgregar);

         tcVisita = new TableComponent(getActivity(), this,App.ESTILO).size(400, 1200).dataColumHeight(45).headerHeight(45).headerTextSize(15);
         tcVisita.addHeader(R.string.v_nro, 0.4f);
         tcVisita.addHeader(R.string.v_fecha, 0.7f);
         tcVisita.addHeader(R.string.v_hora_ini, 0.7f);
         tcVisita.addHeader(R.string.v_fecha_prox, 0.8f);
         tcVisita.addHeader(R.string.v_hora_prox, 0.8f);
         tcVisita.addHeader(R.string.v_resultadoinf, 1.0f);
         tcVisita.addHeader(R.string.v_GPS_Longitud, 1.2f);
         tcVisita.addHeader(R.string.v_GPS_Latitud, 1.2f);
         tcVisita.addHeader(R.string.v_GPS_Altitud, 1.2f);
		
         btnIniciar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_iniciar).size(150, 55);
         btnIniciar.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				
					if (visitas.size() > 0) {
						if (visitas.get(visitas.size() - 1).qvvresul == null) {
							ToastMessage.msgBox(getActivity(),"Finalice la visita anterior",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
							return;
						}
					}
					Calendar fechaactual = new GregorianCalendar();
					visita_viv = new Visita_Viv();
					visita_viv.id = App.getInstance().getMarco().id;
					visita_viv.nro_visita = visitas.size() + 1;
					visita_viv.qvhora_ini = fechaactual.get(Calendar.HOUR_OF_DAY)+"";
					visita_viv.qvmin_ini = fechaactual.get(Calendar.MINUTE)+"";
					visita_viv.qvvdia_ini = fechaactual.get(Calendar.DAY_OF_MONTH)+"";
					visita_viv.qvvmes_ini = (fechaactual.get(Calendar.MONTH)+1)+"";
					visita_viv.qvvanio_ini= fechaactual.get(Calendar.YEAR);
					
					visita_viv.qvhora_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita_viv.qvhora_ini);
					visita_viv.qvmin_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita_viv.qvmin_ini);
					visita_viv.qvvdia_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita_viv.qvvdia_ini);
					visita_viv.qvvmes_ini=MyUtil.CompletarCerosalaIzquierdaParadosDigitos(visita_viv.qvvmes_ini);
					
					boolean flag=grabar();
					if(flag){
						recargarLista();
						abrirDialogo(visita_viv, Viv_VisitaDialog.ACTION.INICIAR);
					}
					
			}
		});
		
		btnFinalizar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.v_l_finalizar).size(150, 55);
		btnFinalizar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					Visita_Viv vivista_vivenda = (Visita_Viv) tcVisita.getSelectedItem();
					if (detalles.size()>0) {
						ToastMessage.msgBox(getActivity(),"Finalice la Visita en el Cuestionario de Hogar.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					if (vivista_vivenda == null) {
						ToastMessage.msgBox(getActivity(),"Seleccione una visita primero.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					if (vivista_vivenda.qvvresul != null) {
						ToastMessage.msgBox(getActivity(),"Visita ya fue finalizada.",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
						return;
					}
					boolean flag=grabar();
					abrirDialogo(vivista_vivenda, Viv_VisitaDialog.ACTION.FINALIZAR);
			}
		});
		
		dummy = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		dummy2 = new LabelComponent(getActivity()).text("").size(MATCH_PARENT, 30).textSize(24).colorFondo(R.color.WhiteSmoke);
		grid = new GridComponent(getActivity(), Gravity.CENTER, 5, 0);
		grid.addComponent(btnIniciar);
		grid.addComponent(dummy);
		grid.addComponent(btnFinalizar);
		grid.addComponent(dummy2);
		grid.addComponent(btnIncentivos);
		
		
		btnAgregar.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				agregarHogar();	}
		});		
		
		btnIncentivos.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				agregarIncentivos();	}
		});		
	}
	
	
	@Override
	protected View createUI() { 
		buildFields(); 
		q1 = createQuestionSection(lblTitulo,tblConglomerado.component(),tblDireccion.component());
		q0 = createButtonSection(btnGPS,btnVerenMapa);
		q2 = createQuestionSection(tcVisita.getTableView(), grid.component());
		q4 = createQuestionSection(lblTitulo2,lblMsg,lblpregunta1,thogares.component(),tcHOGARES.getTableView());
				
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q4);
		return contenedor;
	}
	
	private void abrirDialogo(Visita_Viv visita_viv, ACTION action) {
		FragmentManager fm = this.getFragmentManager();
		Viv_VisitaDialog aperturaDialog = Viv_VisitaDialog.newInstance(this, visita_viv, action);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
		recargarLista();
	}
	
	public void recargarLista() {				
		ultimaVisita=null;
		menorVisita=null;
		visitas = getCuestionarioService().getVisitas_Viv(App.getInstance().getMarco().id, seccionesCargado_viv);		
		tcVisita.setData(visitas, "nro_visita", "getFechainicio","getHoraInicio", "getFechaProxima","getHoraProxima", "getResultado","qvgps_long", "qvgps_lat", "qvgps_alt");
		registerForContextMenu(tcVisita.getListView());
		
		Integer indice=0;
		if(visitas.size()>0){
			if(visitas.size()>1){
				indice = visitas.size()-1;
			}
			else{
				indice=0;
			}
			ultimaVisita = getCuestionarioService().getVisita_Viv(App.getInstance().getMarco().id,visitas.get(indice).nro_visita, seccionesCargado_viv);
			App.getInstance().setVisita_Viv(ultimaVisita);
			menorVisita = getCuestionarioService().getVisita_Viv(App.getInstance().getMarco().id,visitas.get(0).nro_visita, seccionesCargado_viv);
		}
		
		if (menorVisita != null && ultimaVisita != null) {
			if(ultimaVisita.qvvdia_ini!=null){
				lblFechaFinal.setText(Util.completarCadena(Util.getText(ultimaVisita.qvvdia_ini), "0", 2, COMPLETAR.IZQUIERDA) + "/"
                            + Util.completarCadena(Util.getText(ultimaVisita.qvvmes_ini), "0", 2, COMPLETAR.IZQUIERDA)+"/"+Util.getText(ultimaVisita.qvvanio_ini));
			}
			else{
				lblFechaFinal.setText("....");
			}
			
            if (!Util.esVacio(menorVisita.qvvresul)) {
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 1)) {
                    	lblResultadoFinal.setText("1. COMPLETA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 2)) {
                    	lblResultadoFinal.setText("2. HOGAR PRESENTE PERO ENTREVISTADO COMPETENTE AUSENTE");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 3)) {
                    	lblResultadoFinal.setText("3. HOGAR AUSENTE");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 4)) {
                    	lblResultadoFinal.setText("4. APLAZADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 5)) {
                    	lblResultadoFinal.setText("5. RECHAZADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 6)) {
                    	lblResultadoFinal.setText("6. VIVIENDA DESOCUPADA O NO ES VIVIENDA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 7)) {
                    	lblResultadoFinal.setText("7.VIVIENDA DESTRUIDA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 8)) {
                    	lblResultadoFinal.setText("8. VIVIENDA NO ENCONTRADA");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 9)) {
                    	lblResultadoFinal.setText("9. OTRO");
                    }
                    if (!Util.esDiferente(ultimaVisita.qvvresul, 9)) {
                    	lblResultadoFinal.setText("10. 	incompleta");
                    }
            } else {
            	lblResultadoFinal.setText("....");
            }
		}		
		
	}
	
    private void llenarTipovia() {
        List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        keys.add(1);
        keys.add(2);
        keys.add(3);
        keys.add(4);
        keys.add(5);
        keys.add(6);
        String[] items1 = new String[] { " --- Seleccione --- ", "1. AVENIDA","2. CALLE", "3. JIRON", "4. PASAJE", "5. CARRETERA", "6. OTRO" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item, R.id.textview, items1);
        spnTIPVIA.setAdapterWithKey(adapter, keys);
    }
    

	
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info;
		info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		
		
		 if (v.equals(tcVisita.getListView())) {
			  menu.setHeaderTitle("Opciones de las Visitas");
			  menu.add(0, 0, 1, "Editar");
			  menu.add(0, 1, 1, "Eliminar");
			  menu.getItem(1).setVisible(false);
			  menu.getItem(0).setVisible(true);
			  
//			  menu.getItem(1).setVisible(true);
//			  if (info.position != visitas.size() - 1 || (App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && ) {
			 
			  
			  /*
			  Integer codigo=App.getInstance().getUsuario().cargo_id;
			  if (info.position != visitas.size() - 1 || (!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)) ) {	  
				  
				  Log.e("entro aki","111");				  
				  menu.getItem(0).setVisible(false);
			  }
			  else{
				  Log.e("entro aki","222");
				  menu.getItem(0).setVisible(true);
			  }
			  */
			  
			  
//			  if (info.position == visitas.size() - 1) { 
//				  if (visitas.get(info.position).qvvresul == null || !Util.esDiferente(visitas.get(info.position).qvvresul, 1)) {
//				  if (visitas.get(info.position).qvvresul == null ) {
//					  menu.getItem(0).setVisible(false);
//				  }
//			  }
		  }

		
		if (v.equals(tcHOGARES.getListView())) {			
			menu.setHeaderTitle("Opciones del Hogar");
			menu.add(1, 0, 1, "Revisar Hogar");
			menu.add(1, 1, 1, "Iniciar Entrevista");
			menu.add(1, 2, 1, "Ir a Seccion 4 y 5");
			menu.add(1, 3, 1, "Ir a Seccion 6");
			menu.add(1, 4, 1, "Ir a Cuestionario de Salud");
			menu.add(1, 5, 1, "Ir a Seccion 8 Salud");
			menu.add(1, 6, 1, "Ir a Seccion 9 Salud");
			menu.add(1, 7, 1, "Ir a Cuestionario Individual");
			menu.add(1, 8, 1, "Eliminar Hogar");
//			menu.add(1, 9, 1, "Ir a Seccion 7 Covid");
			menu.add(1, 10, 1, "Ir a Seccion 8 Incentivos");
			
			Hogar seleccion = (Hogar) info.targetView.getTag();
			App.getInstance().setHogar(seleccion);
				
			if (seleccion == null) {
				return;
			}
			detalles04_05= new ArrayList<Seccion04_05>();
			boolean entraaseccion04=false;
			informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,seleccion.hogar_id, 1);
			entraaseccion04 = getSeccion04_05Service().getEntraaSeccion04_05(App.getInstance().getMarco().id, seleccion.hogar_id);

			Visita visita = getVisitaService().getUltimaVisita(seleccion.id,seleccion.hogar_id, "ID", "HOGAR_ID", "QHVRESUL");
			App.getInstance().setVisita_Viv(visita_viv);
			CSVISITA visitaSalud= getVisitaService().getUltimaVisitaSalud(App.getInstance().getMarco().id,seleccion.hogar_id, "ID", "HOGAR_ID", "QSVRESUL");
			
			if(visitaSalud!=null){
				App.getInstance().setC1Visita(visitaSalud);
			}
			
			boolean flag= getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id,seleccion.hogar_id,0,11);
			App.getInstance().getHogar().existeseccion04=entraaseccion04;
			boolean flagMef= getPersonaService().getCantidadMef(App.getInstance().getMarco().id,seleccion.hogar_id,App.HogEdadMefMin,App.HogEdadMefMax);
			boolean puedeeliminar=false;
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				puedeeliminar=true;	
			}
			Seccion01 band = null;
			band = InformantedeSalud(seleccion);
			boolean HabilitarCuandoesMef=false;
			if(band!=null && !Util.esDiferente(band.persona_id, seleccion.nro_info_s) && band.qh06==2 && band.qh07>=App.HogEdadMefMin && band.qh07<=App.HogEdadMefMax){
				HabilitarCuandoesMef= getCuestionarioService().getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(band.id,band.hogar_id, band.persona_id);
//				Log.e("sss","1111"+HabilitarCuandoesMef);
			}else{
				if(band!=null && band.qh06!=null && band.qh06==2 && band.qh07>=50){
					HabilitarCuandoesMef=true;
//					Log.e("sss","22222"+HabilitarCuandoesMef);
					}
				else{
					HabilitarCuandoesMef=true;
//					Log.e("sss","33333"+HabilitarCuandoesMef);
					}
			}
			boolean s9=false;		
			if(visita!=null){
				SeleccionarInformantedeSalud();
				if(App.getInstance().getPersonaSeccion01()!=null){
					s9= getCuestionarioService().getCuestionarioDelSalud01_07Completado(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,App.getInstance().getPersonaSeccion01().persona_id);
					
					Salud salud = getCuestionarioService().getSaludNavegacionS1_3(App.getInstance().getMarco().id,seleccion.hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS1_3);
					if (salud!=null) {
						App.getInstance().setSaludNavegacionS1_3(salud);						
						App.getInstance().getSaludNavegacionS1_3().qh06=App.getInstance().getPersonaSeccion01().qh06;						
					}		
					
					CAP04_07 cap04_07 = getCuestionarioService().getSaludNavegacionS4_7(App.getInstance().getMarco().id,seleccion.hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS4_7);
					if (cap04_07!=null) {
						App.getInstance().setSaludNavegacionS4_7(cap04_07);	
						App.getInstance().getSaludNavegacionS4_7().existes8=flag;
						App.getInstance().getSaludNavegacionS4_7().existemef=getPersonaService().getCantidadMef(App.getInstance().getMarco().id, seleccion.hogar_id, App.HogEdadMefMin, App.HogEdadMefMax);
					}
				}				
			}
			if (visita == null) {				
				menu.removeItem(0);
				menu.getItem(1).setVisible(false);
				menu.getItem(2).setVisible(false);
				menu.getItem(3).setVisible(false);
				menu.getItem(4).setVisible(false);
				menu.getItem(5).setVisible(false);
				menu.getItem(6).setVisible(false);
				menu.getItem(8).setVisible(false);
				if(puedeeliminar){
					menu.getItem(7).setVisible(false);
				}
				
//				if (detalles.get(detalles.size() - 1).hogar_id != seleccion.hogar_id) {
//					menu.getItem(7).setVisible(false);
//				} 
//				else {
//					menu.getItem(7).setVisible(true);
//				}
				return;
			}
			if(Util.esDiferente(visita.qhvresul,1)){
				menu.removeItem(0);
				menu.getItem(1).setVisible(false);
				menu.getItem(2).setVisible(false);
				menu.getItem(3).setVisible(false);
				menu.getItem(4).setVisible(false);
				menu.getItem(5).setVisible(false);
				menu.getItem(6).setVisible(false);
//				menu.getItem(8).setVisible(false);
				menu.getItem(8).setVisible(false);
				if(puedeeliminar){
					menu.getItem(7).setVisible(false);
				}
			}  
			else{
				menu.removeItem(1);
//				menu.getItem(8).setVisible(true);
				if(!entraaseccion04){
					menu.getItem(1).setVisible(false);
				}
				if(!flag){
					menu.getItem(4).setVisible(false);
				}
				if(!s9){
					menu.getItem(5).setVisible(false);
				}
				if(!HabilitarCuandoesMef){					
					menu.getItem(3).setVisible(false);
				}
				if(!flagMef){
					menu.getItem(6).setVisible(false);
				}
				if(puedeeliminar){
					menu.getItem(7).setVisible(false);
				}
			}
		}
	}

	 private void editarVisita(int position) {
         abrirDialogo(visitas.get(position), ACTION.EDITAR);
	 }
	 private void eliminarVisita(int position) {
		  dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Desea borrar la visita?");
		  dialog.put("visita", visitas.get(position));
		  dialog.showDialog();
		  action= PROCESS.ELIMINAR_VISITA_VIV;
	 }
		
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		
		
		
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		
		if (item.getGroupId() == 0) {
  		  switch (item.getItemId()) {
  		  case 0:
  			  editarVisita(info.position);
  			  break;
  		  case 1:
  			  eliminarVisita(info.position);
  			  break;
  		  }
  	  }
//  	  return super.onContextItemSelected(item);
  	  
  	  
		if (item.getGroupId() == 1) {
			Hogar seleccion = (Hogar) info.targetView.getTag();
			App.getInstance().setHogar(seleccion);
			
			Marco marco = getService().getMarco(seleccion.id);
			App.getInstance().setMarco(marco);
			
			((CuestionarioFragmentActivity) parent).setTitulo(Util.getText(marco.conglomerado)	+ " - "	+ Util.getText(marco.nselv) + " - " + Util.completarCadena(Util.getText(seleccion.hogar_id), "0", 2, COMPLETAR.IZQUIERDA));
			switch (item.getItemId()) {
			
			case 0:
				irVisitas();
				break;
			case 1:irVisitas();
			        break;
	        case 2: 
//	        	App.FRAGMENTO_QHSECCION04=true; 
	        	goTo(CuestionarioFragmentActivity.SECCION04);
			        break;
			case 3:	
				goTo(CuestionarioFragmentActivity.SECCION06);
				break;
			case 4:
				try {
					SeleccionarInformantedeSalud();
					goTo(CuestionarioFragmentActivity.CSVISITA);	
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			break;
		
			case 5:
				try {
					SeleccionarInformantedeSalud();
					goTo(CuestionarioFragmentActivity.CSSECCION8f_1);	
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				break;	
			case 6:try {
						SeleccionarInformantedeSalud();
						goTo(CuestionarioFragmentActivity.CSSECCION9f_1);	
					} catch (Exception e) {
						// TODO: handle exception
					}
					break;		
			case 7:	goTo(CuestionarioFragmentActivity.CISECCION_f_carat);
					break;
			case 8:eliminar((Hogar) detalles.get(info.position));break;
//			case 9:goTo(CuestionarioFragmentActivity.SECCIONCOVID_1);break;
			case 10:irIncentivos(); break;
			}
		}
		return super.onContextItemSelected(item);
	}
	
	

	
	public void eliminar(Hogar bean) {
		action = PROCESS.ELIMINAR_FILA;
		dialog = new DialogComponent(getActivity(), this, TIPO_DIALOGO.YES_NO,getResources().getString(R.string.app_name),"Esta seguro que desea borrar el hogar se borrara toda la informaci�n respecto a este hogar?");
		dialog.put("bean", bean);
		dialog.showDialog();
	}



	protected void agregarHogar() {
		if (detalles.size() == 5) {
			ToastMessage.msgBox(this.getActivity(), "No mas de 5 Hogares. ",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return;
		}
		if (ultimaVisita!=null && ultimaVisita.qvvresul!=null && (ultimaVisita.qvvresul==6 ||ultimaVisita.qvvresul==7 || ultimaVisita.qvvresul== 8 || ultimaVisita.qvvresul==9) ) {
		ToastMessage.msgBox(this.getActivity(), "No puede crear hogares",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		
		return;
	}
		
		Hogar bean = new Hogar();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = detalles.size() + 1;
		abrirDetalle(bean);
	}
	public void abrirDetalle(Hogar tmp) {
		FragmentManager fm = CARATULAFragment_002.this.getFragmentManager();
		HogarDialog aperturaDialog = HogarDialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	
	public void agregarIncentivos() {
		Incentivos inc = new Incentivos();
		inc.id = App.getInstance().getMarco().id;
		inc.hogar_id = 1;	
		App.getInstance().setIncentivos(inc);
		abrirIncentivos(inc);
	}

	public void abrirIncentivos(Incentivos tmp) {
		App.getInstance().setIncentivos(null);
		App.getInstance().setIncentivos(tmp);
		parent.nextFragment(CuestionarioFragmentActivity.SECCION8INCENTIVOS);
	}



	
	@Override
	public boolean grabar() {
	uiToEntity(bean); 
	
//	if(grabarcantidad){
//		bean.n_hogar=1;
//		grabarcantidad=false;
//	}
//	if(bean.resviv!=0){
////		grabarResultado();
//	}
	if (!validar()) {
		if (error) {
			if (!mensaje.equals(""))
				ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			if (view != null)
				view.requestFocus();
		}
		
		return false;
	}
	try {
		marco= new Marco();
		marco.id=bean.id;
		
		marco.nselv = txtNSELV.getText().toString();
		App.getInstance().getMarco().nselv=marco.nselv;
		if(!getCuestionarioService().ModificarNumeroDeVivienda(bean.id, marco.nselv)){
			ToastMessage.msgBox(this.getActivity(),"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return false;
		}
		if (!getCuestionarioService().saveOrUpdate(bean, seccionesGrabado)) {
			ToastMessage.msgBox(this.getActivity(),"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			//Log.e("","NSELV"+App.getInstance().getMarco().nselv);
			return false;
		}
	} catch (SQLException e) {
		ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		return false;
	}		
	return true;
	}

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);		
		
		if (Util.esVacio(bean.nselv)) {
			mensaje = preguntaVacia.replace("$", "La pregunta Numero de Vivienda");
			view = txtNSELV;
			error = true;
			return false;
		}
		
		if (!Util.esVacio(bean.nselv)) {
			if (bean.nselv.toString().trim().length()!=3) {
				error = true;
				mensaje = "CAMPO NUMERO DE VIVIENDA DEBE TENER 3 DIGITOS";
				view = txtNSELV;
				return false;
			}
		}
		if (Util.esVacio(bean.tipvia)) {
			mensaje = preguntaVacia.replace("$", "La pregunta Tipo de Via");
			view = spnTIPVIA;
			error = true;
			return false;
		}
		if (!Util.esDiferente(bean.tipvia,6)) {
			if (Util.esVacio(bean.tipvia_o)) {
				mensaje = preguntaVacia.replace("$", "La pregunta Tipo de Via Especifique");
				view = txtTIPVIA_O;
				error = true;
				return false;
			}
		}
		if (Util.esVacio(bean.nomvia)) {
			mensaje = preguntaVacia.replace("$", "La pregunta Nombre de Via");
			view = txtNOMVIA;
			error = true;
			return false;
		}
		
		return true;
	}
	
	public void LimpiarDatosParaNavegacion(){
		App.getInstance().setHogar(null);
		App.getInstance().SetPersonaSeccion01(null);
		App.getInstance().setSaludNavegacionS1_3(null);
		App.getInstance().setSaludNavegacionS4_7(null);
		App.getInstance().setVisita_Viv(null);
		App.getInstance().setC1Visita(null);
	}
	
	@Override
	public void cargarDatos() {
		
		LimpiarDatosParaNavegacion();
		if (App.getInstance().getMarco()==null) {
			goTo(CuestionarioFragmentActivity.MARCO);
			return;
		}
		
		if (visita_viv == null) {
			visita_viv = new Visita_Viv();
			visita_viv.id = App.getInstance().getMarco().id;
			visitas = new ArrayList<Visita_Viv>();
		}	
		

		
		bean = getCuestionarioService().getCaratula(App.getInstance().getMarco().id, seccionesCargado);
				
		
		if(bean!=null){
			bean.conglomerado=App.getInstance().getMarco().conglomerado;
//			bean.nselv=App.getInstance().getMarco().nselv;
			bean.n_hogar=1;
		}
		
		if(bean!=null && (bean.conglomerado==null)){
//		if(bean!=null && (bean.conglomerado==null || bean.nselv==null)){
			goTo(CuestionarioFragmentActivity.MARCO);
			return;
		}
		viv_reserva=App.getInstance().getMarco().viv_reserva==null?0:App.getInstance().getMarco().viv_reserva;
//		Log.e("viv_reserva:: "," "+viv_reserva);
//		Log.e("viv_reserva01:: "," "+App.getInstance().getMarco().nselv);
		
		if (bean == null) {
			bean = new Caratula();
			bean.id = App.getInstance().getMarco().id;
			detalles = new ArrayList<Hogar>();
			bean.conglomerado = App.getInstance().getMarco().conglomerado;
			bean.nselv = App.getInstance().getMarco().nselv!=null?App.getInstance().getMarco().nselv:bean.nselv;
			bean.tipvia = App.getInstance().getMarco().catvia;
			bean.nomvia = App.getInstance().getMarco().via;
			bean.nom_dpto=App.getInstance().getMarco().departamento;
			bean.distrito=App.getInstance().getMarco().distrito;
			bean.provincia=App.getInstance().getMarco().provincia;
			bean.nom_ccpp = App.getInstance().getMarco().nomccpp;
			bean.zon_num = App.getInstance().getMarco().zona;
			bean.mza = App.getInstance().getMarco().manzana;
			bean.aer_ini = App.getInstance().getMarco().aer_ini;
			bean.aer_fin = App.getInstance().getMarco().aer_fin;
			bean.vivienda = App.getInstance().getMarco().nroorden;
			bean.ptanum = App.getInstance().getMarco().puerta;
			bean.mz = App.getInstance().getMarco().mz;
			bean.lote = App.getInstance().getMarco().lote;
			bean.piso = App.getInstance().getMarco().piso;
			bean.block = App.getInstance().getMarco().block;
			bean.interior = App.getInstance().getMarco().interior;
			bean.tipsel = App.getInstance().getMarco().tselv;
			bean.p19 = App.getInstance().getMarco().p19;
			bean.km = App.getInstance().getMarco().km;
		} 

		else {
			detalles = getCuestionarioService().getHogares(bean.id);
			
		}
		
		
		if(viv_reserva==1){
			txtNSELV.setReadOnly(false);
		}
		else{
			txtNSELV.readOnly();
		}
	
		cargarTabla();
		tracker = new CapturadorGPS(getActivity());
		entityToUI(bean);
//		Log.e("bean.n_hogar: ",""+bean.n_hogar);
//		Log.e("detalles.size: ",""+detalles.size());
//		if(detalles.size()>0 && bean.n_hogar!=null && bean.n_hogar!=detalles.size()){
		if(detalles.size()>0 && bean.n_hogar!=null ){
//			Log.e("1111: ","1111");
			String cantidad = detalles.size()+"";
			txtN_HOGAR.setText(cantidad);	
		}
		else{
//			Log.e("2222 ","222");
			String cantidad = "";
			txtN_HOGAR.setText(cantidad);
		}
		
		
		recargarLista();
		inicio();
		
		
	}
	  
	private void inicio() {
		salto_tipo_via(spnTIPVIA);
		validarSiesSupervisora();
		
		if(visitas!=null && visitas.size()>0){
			EndesCalendario.ModificaVisitaVivienda_final(bean.id);
			recargarLista();
		}
		MostrarBotonIncentivos();
	}

	public void MostrarBotonIncentivos() {
//		Log.e("detalles: ",""+detalles.size());
		if(detalles.size()>0){
			Util.cleanAndLockView(getActivity(), btnIncentivos);
			btnIncentivos.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(),false, btnIncentivos);
			btnIncentivos.setVisibility(View.VISIBLE);
		}
	}
	
	public void validarSiesSupervisora(){
//		Log.e("cargo_id",""+App.getInstance().getUsuario().cargo_id);
//		Log.e("asignado",""+App.getInstance().getMarco().asignado);
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			Util.cleanAndLockView(getActivity(), btnIniciar,btnFinalizar,btnAgregar);
			txtNOMVIA.readOnly();
			spnTIPVIA.readOnly();
		}
		else{
			Util.lockView(getActivity(),false, btnIniciar,btnFinalizar,btnAgregar);
		}
	}
	
    public void salto_tipo_via(FieldComponent component) {
        String resultadoStr = (String) component.getValue();
        if (resultadoStr.substring(0, 1).equals("6")) {
                Util.lockView(getActivity(), false, txtTIPVIA_O);
        } else {
                Util.cleanAndLockView(getActivity(), txtTIPVIA_O);
        }
}
    

	public void refrescarHogares(Hogar bean) {
		if (detalles.contains(bean)) {
			cargarTabla();
			return;
		}
		detalles.add(bean);
		cargarTabla();
		grabar();	
	}

	private void cargarTabla() {		
		verificarsiHogarestaCompletado();
		detalles = getCuestionarioService().getHogares(bean.id);
		tcHOGARES.setData(detalles, "hogar_id", "getJefeHogar");
		tcHOGARES.setBorder("estado");
		txtN_HOGAR.setText(Util.getText(detalles.size()==0?"":detalles.size()));
		registerForContextMenu(tcHOGARES.getListView());
	}
	private void verificarsiHogarestaCompletado(){
		Visita visitagrabar=null;
		List<Visita> visitas;
		seccionesCargadovisita = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","HOGAR_ID","QHVRESUL","NRO_VISITA")};
		detalles = getCuestionarioService().getHogares(bean.id);
		if(detalles.size()>0){
			for(Hogar h: detalles){
				visitas = getCuestionarioService().getVisitas(bean.id,h.hogar_id, seccionesCargadovisita);
//				ultimaVisita = getCuestionarioService().getVisita(bean.id,h.hogar_id,visitas.get(visitas.size()-1).nro_visita, seccionesCargadovisita);
				if(visitas.size()>0){
				visitagrabar =MyUtil.RegistrarVisitaCompletada(getCuestionarioService(),getServiceSeccion01(),bean.id,h.hogar_id,visitas.get(visitas.size()-1).nro_visita, visitas.get(visitas.size()-1).qhvresul);
				 try {	
						if (Util.esDiferente(visitas.get(visitas.size()-1).qhvresul,2,3,4,5,6,7,8,9)) {			
							getVisitaService().saveOrUpdate(visitagrabar,"QHVRESUL");	
						}			
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
	}


	
	public void SeleccionarInformantedeSalud(){
		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersonanformanteSalud(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id,persona_id,seccionesCargadoInformanteSalud);
		App.getInstance().SetPersonaSeccion01(informantedesalud);
	}
	
	public Seccion01 InformantedeSalud(Hogar seleccion){
		App.getInstance().setHogar(seleccion);
		Marco marco = getService().getMarco(seleccion.id);
		App.getInstance().setMarco(marco);
		String vivienda=txtNSELV.getText().toString().trim()!=null?txtNSELV.getText().toString():"";
		((CuestionarioFragmentActivity) parent).setTitulo(Util.getText(marco.conglomerado)	+ " - "	+ Util.getText(vivienda) + " - " + Util.completarCadena(Util.getText(seleccion.hogar_id), "0", 2, COMPLETAR.IZQUIERDA));
//		Visita visita = getVisitaService().getPrimeraVisitaAceptada(seleccion.id, seleccion.hogar_id);
		Visita visita = getVisitaService().getMaximaVisitaAceptada(seleccion.id,seleccion.hogar_id);
		App.getInstance().setVisita(visita);
//		Log.e("6 seleccion.id: ",""+seleccion.id);
//		Log.e("6 seleccion.hogar_id: ",""+seleccion.hogar_id);
		if(visita==null){
			return null;
		}else{
			if(Util.esDiferente(visita.qhvresul,1)){
				return null;
			}else{
//				Log.e("entro seleccion.id: ",""+seleccion.id);
//				Log.e("entro seleccion.hogar_id: ",""+seleccion.hogar_id);
				Integer persona_id= MyUtil.SeleccionarInformantedeSalud(seleccion.id, seleccion.hogar_id,  getVisitaService(), getServiceSeccion01());
				return getServiceSeccion01().getPersonanformanteSalud(seleccion.id ,seleccion.hogar_id,persona_id,seccionesCargadoInformanteSalud);
			}
		}
	}
	



	
	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub

	}
	@Override
	public void onAccept() {	
		if (dialog == null) {
			ToastMessage.msgBox(getActivity(),"Ocurri� un problema al ejecutar la operaci�n",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return;
		}
		if (action == PROCESS.ELIMINAR_FILA) {
			Hogar bean = (Hogar) dialog.get("bean");
			detalles.remove(bean);
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id = App.getInstance().getHogar().hogar_id;
			getHogarService().borrarHogar(bean);			
			if (detalles.size()==0 && visitas!=null && visitas.size()==0) {
				try {
					getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CARATULA, bean.id, null);	
				} catch (Exception e) {
					// TODO: handle exception
				}
				goTo(CuestionarioFragmentActivity.MARCO);
			}
			else {
				cargarTabla();
			}
			MostrarBotonIncentivos();
			action=null;
		}
		if(action==PROCESS.ELIMINAR_VISITA_VIV){
			Visita_Viv visita = (Visita_Viv) dialog.get("visita");
			try {
				getCuestionarioService().EliminarUltimaVisitaVivienda(visita.id,visita.nro_visita);
				recargarLista();
			} catch (Exception e) {
				// TODO: handle exception
			}
			action=null;
		}
	}
	
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(getActivity());
		}
		return service;
	}
	
	public void abrirDetalle(int index) {
		this.abrirDetalle((Hogar) detalles.get(index));
	}
	
	private void irVisitas() {
//		if (ultimaVisita.qvvresul!=null && ultimaVisita.qvvresul!=1 && ultimaVisita.qvvresul!=10  ) {
//			ToastMessage.msgBox(this.getActivity(), "Debe iniciar visita",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//			return;
//		}
		parent.nextFragment(CuestionarioFragmentActivity.VISITA);
	}

	private void irIncentivos() {
		Incentivos inc = new Incentivos();
		inc.id = App.getInstance().getMarco().id;
		inc.hogar_id = App.getInstance().getHogar().hogar_id;	
		App.getInstance().setIncentivos(inc);
		parent.nextFragment(CuestionarioFragmentActivity.SECCION8INCENTIVOS);
	}
	
	private void goTo(int fragment) {
		parent.nextFragment(fragment);
	}

	public List<Visita_Viv> getVisitas_Viv() {
		visitas = getCuestionarioService().getVisitas_Viv(App.getInstance().getMarco().id, seccionesCargado_viv);
		return visitas;
	}
	
	public HogarService getHogarService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	private Seccion01Service getServiceSeccion01() {
	        if (seccion01 == null) {
	        	seccion01 = Seccion01Service.getInstance(getActivity());
	        }
	        return seccion01;
	}
	public Seccion04_05Service getSeccion04_05Service()
    {
    	if(Persona04_05Service == null)
    	{
    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
    	}
    	return Persona04_05Service;
    }
	public Seccion01Service getPersonaService()
    {
    	if(Personaservice == null)
    	{
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }
	
//	@Override
//	public FragmentForm getForm() {
//		return this;
//	}
} 
