package gob.inei.endes2024.fragment.caratula;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil.INFORMANTE;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
//import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CaratulaFragmentCI extends FragmentForm implements Respondible {
	Hogar hogar;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo;
	public GridComponent2 gridPreguntas802B,gridPreguntas802C,gridPreguntas802D;
	public TableComponent tcMEF;
	public List<Seccion01> detalles,ListadoMef;
	public ButtonComponent btnSeccion9;

	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	public List<Seccion01> detallesindividual;
	private DialogComponent dialog;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoSeccion08,seccionesGrabadoHogar;
	SeccionCapitulo[] seccionesCargado,seccionesCargadoSeccion01,SeccionesCargadovisita;
	Seccion01ClickListener adapter;
	SeccionCapitulo[] seccionesCargadoHogar;
	private enum ACTION {
		ELIMINAR, RECUPERACION,MENSAJE
	}

	private ACTION action;
	public boolean validar;
	public Seccion01 mef;
	public CISECCION_01_03 mef2;
	public Hogar hogarIndividual;
	public Seccion01Service seccion01;
	public CaratulaFragmentCI() {}
	public CaratulaFragmentCI parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	public VisitaService visitaService;
  @Override 
  public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcMEF.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","QH01","QH02_1","QH02_2","QH02_3","QH06","QH07","QH7DD","QH7MM")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","QH02_1","QH02_2","QH02_3","QH07","QH06","QH07","QH7DD","QH7MM","ID","HOGAR_ID","ESTADO_REC_CI")};
		seccionesGrabadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO" )};
		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "QHVIOLEN") };
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHVIOLEN") };
		SeccionesCargadovisita = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "PERSONA_ID","NRO_VISITA","QIVRESUL") };
		
		return rootView;
	}
  @Override 
  protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.caratulaCI).textSize(21).centrar();
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMEF = new TableComponent(getActivity(), this,App.ESTILO).size(600, 770).headerHeight(altoComponente + 10).dataColumHeight(60);
//		}
//		else{
			tcMEF = new TableComponent(getActivity(), this,App.ESTILO).size(700, 770).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcMEF.addHeader(R.string.individual_nro_orden, 0.5f,TableComponent.ALIGN.CENTER);
		tcMEF.addHeader(R.string.individual_nombres, 1.3f,TableComponent.ALIGN.LEFT);
		tcMEF.addHeader(R.string.individual_violencia,0.8f, TableComponent.ALIGN.CENTER);
		tcMEF.setColorCondition("ESTADO_REC_CI", Util.getHMObject(String.valueOf(App.INFORMANTE), R.color.azulacero,String.valueOf(0), R.color.WhiteSmoke));
    }
    @Override 
    protected View createUI() {
		buildFields();
		
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(tcMEF.getTableView());

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);

    return contenedor;
    }
    
    @Override 
    public boolean grabar() {
    	if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null) view.requestFocus();
			}
			return false;
		}
    	return true;
    }
    
    private boolean validar() {
    	if(!isInRange()) return false;
    	if (!validar) {
    		mensaje = "Seleccione una Persona";
    		error = true;
    		return false;
		}
    	return true;
    }
    
    @Override 
    public void cargarDatos() {
    	validar=false;
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoHogar);
		try { 
			hogar.qhviolen=SeleccionarMefViolenciaDomestica()==-1?0:SeleccionarMefViolenciaDomestica();
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
		} 
    	if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null){
    		detalles = getCuestionarioService().getListadoMEF(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion01);
	    	cargarTabla();
    	}
    	inicio();

    }
    
    public void cargarTabla() {
    	detallesindividual=new ArrayList<Seccion01>();
    	CARATULA_INDIVIDUAL visita=null;
    	if(App.getInstance().getMarco()!=null && App.getInstance().getHogar()!=null ){
        	detallesindividual= getCuestionarioService().getListadoMEF(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion01);
        	App.getInstance().setQhviolen(SeleccionarMefViolenciaDomestica());

        	for (int i = 0;i < detallesindividual.size();i++) {
        		visita =getVisitaService().getVisitaIndividualCompletada(detallesindividual.get(i).id, detallesindividual.get(i).hogar_id, detallesindividual.get(i).persona_id,SeccionesCargadovisita);
        		if(visita!=null && visita.qivresul!=null && visita.qivresul==App.INDIVIDUAL_RESULTADO_COMPLETADO){
        			detallesindividual.get(i).estadomef=0;
        		}else{
        			detallesindividual.get(i).estadomef=1;
        		}
        	}
        	for (int row = 0; row < detallesindividual.size(); row++) {
        		if (obtenerEstado(detallesindividual.get(row)) == 1) {
    				// borde de color azul
    				tcMEF.setBorderRow(row, true);
    			} else if (obtenerEstado(detallesindividual.get(row)) == 2) {
    				// borde de color rojo
    				tcMEF.setBorderRow(row, true, R.color.red);
    			} else {
    				tcMEF.setBorderRow(row, false);
    			}
    		}
        	tcMEF.reloadData();
    		tcMEF.setData(detallesindividual ,"persona_id", "getNombre","getViolencia");
            registerForContextMenu(tcMEF.getListView());
    	}
    	    	
    }
    
    private int obtenerEstado(Seccion01 detalle) {
    	if (!Util.esDiferente(detalle.estadomef, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalle.estadomef,1)) {
			return 2;
		}
		return 0;
	}
    private void inicio() {
    }
    public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
    }
    
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                    ContextMenuInfo menuInfo) {
            super.onCreateContextMenu(menu, v, menuInfo);
          
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            if (v.equals(tcMEF.getListView())) {
                    menu.setHeaderTitle("Opciones de la MEF");
                    menu.add(0, 1, 1, "Eliminar Mef: "+detalles.get(info.position).getNombre());
                    menu.add(0, 2, 2, "Recuperaci�n de Mef: "+detalles.get(info.position).getNombre());
                    if(detallesindividual.get(info.position).estadomef==0){
                    	menu.getItem(0).setVisible(false);
                    	menu.getItem(1).setVisible(false);
                    }
            }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	if (!getUserVisibleHint())
    		return false;
    		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            if (item.getGroupId() == 0) {
            	switch (item.getItemId()) {
            	case 1:
            		action = ACTION.ELIMINAR;
    				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea eliminar?");
    				dialog.put("seleccion", (Seccion01) detallesindividual.get(info.position));
    				dialog.showDialog();
            		break;
            	case 2:
            		action = ACTION.RECUPERACION;
    				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Est� seguro que desea Recuperar informaci�n?");
    				dialog.put("seleccion", (Seccion01) detallesindividual.get(info.position));
    				dialog.showDialog();
            		break;
            	}
            }
                
            return super.onContextItemSelected(item);
    }
    
    public void EditarIndividual(CISECCION_01_03 tmp) {
    	App.getInstance().setPersonaCuestionarioIndividual(null);
    	
//    	Log.d("Mensaje","test qi230 "+tmp.qi230+"-"+tmp.qi231_y);
    	App.getInstance().setPersonaCuestionarioIndividual(tmp);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto1y3 =  getCuestionarioService().getCompletadoSeccion0103CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto4a =  getCuestionarioService().getSeccion4ACompletado(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto4b =  getCuestionarioService().getSeccion4BCompletado(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto4dit =  getCuestionarioService().getSeccion4DITCompletado(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 = getCuestionarioService().getCompletadoSeccion0507CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8 = getCuestionarioService().getCompletadoSeccion008CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto10 = getCuestionarioService().getCompletadoSeccion10CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
    	
    	hogarIndividual = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
    	App.getInstance().setQhviolen(0);
    	App.getInstance().setQhviolen(hogarIndividual.qhviolen==null?0:hogarIndividual.qhviolen);
    	App.getInstance().setNumMef(0);
    	App.getInstance().setNumMef(detallesindividual.size());
    	
    	
    	validar=true;
    	parent.nextFragment(CuestionarioFragmentActivity.CISECCION_f_carat+1);
	}
    
    public Integer SeleccionarMefViolenciaDomestica(){
		Integer[][] MatrizViolencia = LlenarMatrizViolenciaFamiliar();
		Integer fila =Integer.parseInt(App.getInstance().getMarco().nselv.substring(2,3));
		Integer columna = getServiceSeccion01().cantidaddeMef(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id);
		Integer mefPosition =0;
		if(columna!=0){
			mefPosition=MatrizViolencia[fila][columna-1];
		}
		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		Integer persona_id=-1;
		if(ListadoMef.size()>0){
			persona_id=ListadoMef.get(mefPosition-1).persona_id;
		}
		return persona_id;
	}
	
	public Integer[][] LlenarMatrizViolenciaFamiliar(){
		Integer[][] matriz= new Integer[10][10];
		matriz[0][0]=1;matriz[0][1]=2;matriz[0][2]=2;matriz[0][3]=4;matriz[0][4]=3;matriz[0][5]=6;matriz[0][6]=5;matriz[0][7]=4;
		matriz[1][0]=1;matriz[1][1]=1;matriz[1][2]=3;matriz[1][3]=1;matriz[1][4]=4;matriz[1][5]=1;matriz[1][6]=6;matriz[1][7]=5;
		matriz[2][0]=1;matriz[2][1]=2;matriz[2][2]=1;matriz[2][3]=2;matriz[2][4]=5;matriz[2][5]=2;matriz[2][6]=7;matriz[2][7]=6;
		matriz[3][0]=1;matriz[3][1]=1;matriz[3][2]=2;matriz[3][3]=3;matriz[3][4]=1;matriz[3][5]=3;matriz[3][6]=1;matriz[3][7]=7;
		matriz[4][0]=1;matriz[4][1]=2;matriz[4][2]=3;matriz[4][3]=4;matriz[4][4]=2;matriz[4][5]=4;matriz[4][6]=2;matriz[4][7]=8;
		matriz[5][0]=1;matriz[5][1]=1;matriz[5][2]=1;matriz[5][3]=1;matriz[5][4]=3;matriz[5][5]=5;matriz[5][6]=3;matriz[5][7]=1;
		matriz[6][0]=1;matriz[6][1]=2;matriz[6][2]=2;matriz[6][3]=2;matriz[6][4]=4;matriz[6][5]=6;matriz[6][6]=4;matriz[6][7]=2;
		matriz[7][0]=1;matriz[7][1]=1;matriz[7][2]=3;matriz[7][3]=3;matriz[7][4]=5;matriz[7][5]=1;matriz[7][6]=5;matriz[7][7]=3;
		matriz[8][0]=1;matriz[8][1]=2;matriz[8][2]=1;matriz[8][3]=4;matriz[8][4]=1;matriz[8][5]=2;matriz[8][6]=6;matriz[8][7]=4;
		matriz[9][0]=1;matriz[9][1]=1;matriz[9][2]=2;matriz[9][3]=1;matriz[9][4]=2;matriz[9][5]=3;matriz[9][6]=7;matriz[9][7]=5;
		return matriz;
	}
	
	public class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			mef = new Seccion01();
        	mef = (Seccion01) detallesindividual.get(arg2);
        	App.getInstance().setPersonaCuestionarioI(mef);
        	
             //campos iniciales para saltos 
            SeccionCapitulo[] secciones01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI102","QI105D","QI105M","QI105Y","QI105CONS","QI106","QI107","QI108N","QI108Y","QI108G","QI201","QI208","QI226","QI230","QI231_Y","QI233","QI230","QI234","QI231_Y","QI231_M","QI235A","QI235B_Y","QI235B_M","QI302_01","QI302_02","QI302_03","QI302_04","QI302_05A","QI302_05B","QI302_06","QI302_07","QI302_08","QI302_09","QI302_10","QI302_11","QI302_12","QI302_13","QI302_14","QI302O","QI304","QI307","QI310","QI315Y","QI316Y","QI320","QI325A","ID","HOGAR_ID","PERSONA_ID")};
            SeccionCapitulo[] secciones04b2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI479A","ID","HOGAR_ID","PERSONA_ID")};
            SeccionCapitulo[] secciones05_07 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI501","QI502","QI509M","QI509Y","QI512","QI512AB","QI602","QI614","QI603U","QI603N","QI707","ID","HOGAR_ID","PERSONA_ID")}; 
            SeccionCapitulo[] secciones08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI801A","QI801B","QI815","QI817A","QI817B","QI901","ID","HOGAR_ID","PERSONA_ID")};
            SeccionCapitulo[] secciones10 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI1043", "QI1000A","ID", "HOGAR_ID", "PERSONA_ID")};
            
            
            CISECCION_01_03 individual01_03 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,secciones01_03);
 			CISECCION_04B2 individual04b2 = getCuestionarioService().getCISECCION_04B2(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,secciones04b2); 
			CISECCION_05_07 individual05_07 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,secciones05_07);
			CISECCION_08 individual08 = getCuestionarioService().getCISECCION_08(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,secciones08);
			
			CISECCION_10_01 individual10 = getCuestionarioService().getCISECCION_10_01(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,secciones10);
			
 			/////
 			SeccionCapitulo[] seccionesCarga = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID")};
 			List<CISECCION_02> detalles=null;
 			detalles = getCuestionarioService().getNacimientosListbyPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id, seccionesCarga);
 			
 			List<CISECCION_02> detalles2=null;
 			detalles2 = getCuestionarioService().getNacimientosVivoListbyPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id, seccionesCarga);
 			
 			List<CISECCION_02> detalles3=null;
 			detalles3 = getCuestionarioService().getNacimientosMayor2011ListbyPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id, seccionesCarga);
 			
 			/*
 			SeccionCapitulo[] seccionesCargadoDIT = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","QI212_NOM","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO","QI478") };
 			List<CISECCION_04DIT> detalles4=null;
 			detalles4 = getCuestionarioService().getListadoDit(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,seccionesCargadoDIT);
 			 */		
 			
 			SeccionCapitulo[] seccionesCargadoDIT_02 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","QI212_NOM","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO","QI478") };
 			List<CISECCION_04DIT_02> detalles4=null;
 			detalles4 = getCuestionarioService().getListadoDit_02(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,seccionesCargadoDIT_02);
 			 

 			
        	if(individual01_03==null) {
	        	individual01_03 = new CISECCION_01_03();
	        	individual01_03.id = detallesindividual.get(arg2).id;
	        	individual01_03.hogar_id = detallesindividual.get(arg2).hogar_id;
	        	individual01_03.persona_id = detallesindividual.get(arg2).persona_id;
	        	individual01_03.qh02_1=  detallesindividual.get(arg2).qh02_1;
	        	individual01_03.qh02_2=  detallesindividual.get(arg2).qh02_2;
        	}	        	
	        	individual01_03.qh01 = detallesindividual.get(arg2).persona_id.toString();
	        	individual01_03.qh02_1 = detallesindividual.get(arg2).qh02_1;
	        	individual01_03.qh02_2 = detallesindividual.get(arg2).qh02_2;
	        	individual01_03.qh7dd = Integer.valueOf(detallesindividual.get(arg2).qh7dd==null?"0":detallesindividual.get(arg2).qh7dd);
	        	individual01_03.qh7mm = Integer.valueOf(detallesindividual.get(arg2).qh7mm==null?"0":detallesindividual.get(arg2).qh7mm);
	        	individual01_03.qh07 = detallesindividual.get(arg2).qh07;
	        	individual01_03.total_ninios = detalles.size();
	        	individual01_03.total_ninios_vivos = detalles2.size();
	        	individual01_03.total_ninios_4a = detalles3.size();
	        	individual01_03.total_ninios_4dit = detalles4.size();
	        	
	        	CISECCION_02 seccion2 = getCuestionarioService().primerNacimientoCap2(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
	        	CISECCION_02T seccion2t = getCuestionarioService().primeraTerminacionCap1_3(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
	        	if(seccion2!=null){
	        	individual01_03.qi215 = seccion2.getQi215();
	        	}
	        	//"QI230","QI234","QI231_Y","QI231_M","QI235A","QI235B_Y","QI235B_M"
	        	/* SE MODIFICA LAS CONDICIONES INICIALES DE MUESTRA 26/01/2017  (FCP)*/
	        	
	        	 if(individual01_03.qi230!=null && individual01_03.qi234!=null) {          
	   	          if(individual01_03.qi230 == 1 && individual01_03.qi234==2) {
	   	        	individual01_03.fechaPrimerAborto = individual01_03.qi231_m+"/"+ individual01_03.qi231_y;
	   	            }  
	   	          else  if(individual01_03.qi235a!=null) {
	   		          if(individual01_03.qi230 == 1 && individual01_03.qi234==1 &&  individual01_03.qi235a==2) {
	   		        	  if(seccion2t!=null) {
	   		        		individual01_03.fechaPrimerAborto = seccion2t.qi235_m+"/"+seccion2t.qi235_y;
	   		        	  	}
	   		          } 		
	   		          else if(individual01_03.qi230 == 1 && individual01_03.qi234==1 &&  individual01_03.qi235a==1) {
	   		        	individual01_03.fechaPrimerAborto = individual01_03.qi235b_m+"/"+individual01_03.qi235b_y;
	   		          }           
	   	          }
	             }
	        	 else{
	        		 if(individual01_03.qi230!=null && individual01_03.qi230 == 1 && individual01_03.qi234==null) {
	 	   	        	individual01_03.fechaPrimerAborto = individual01_03.qi231_m+"/"+ individual01_03.qi231_y;
	 	   	            }
	        	 }
//	        	if(individual01_03.qi230!=null && individual01_03.qi231_m!=null && individual01_03.qi231_y!=null) {          
//	  	          if(individual01_03.qi230 == 1) {
//	  	        	  individual01_03.fechaPrimerAborto = individual01_03.qi231_m+"/"+ individual01_03.qi231_y;
//	  	            }
//	        	}
	        	CISECCION_04A ci4a = getCuestionarioService().primerRegistroCap4a(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
	        	
	        	if(ci4a!=null && ci4a.qi405!=null)
	        		individual01_03.qi405 = ci4a.qi405.toString();
	        	if(ci4a!=null && ci4a.qi409!=null)
	        		individual01_03.qi409 = ci4a.qi409.toString();
	        	if(ci4a!=null && ci4a.qi411_h!=null)
	        		individual01_03.qi411_h = ci4a.qi411_h.toString();
	        	
	        	Calendar fecha = Calendar.getInstance();	        	
	        	
	        	if(individual04b2==null) {
	        		individual04b2 = new CISECCION_04B2();
	        		individual04b2.id = detallesindividual.get(arg2).id;
	        		individual04b2.hogar_id = detallesindividual.get(arg2).hogar_id;
	        		individual04b2.persona_id = detallesindividual.get(arg2).persona_id;
	        	}	        	
	        	
	        	individual04b2.filtro480=getCuestionarioService().VerificarFiltro480(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
//	        	 Log.e("","filtro 480"+individual04b2.filtro480);
	        	individual04b2.filtro479=getCuestionarioService().ExisteninioMenoraTresAniosyVivo(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id);
	        	individual04b2.filtro481 =getCuestionarioService().VerificarFiltro481(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,fecha.get(Calendar.YEAR)-5); 
	        	individual04b2.filtro491 = getCuestionarioService().VerificarFiltro481(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id, fecha.get(Calendar.YEAR)-3);
	        	
	        	App.getInstance().setSeccion04B2(null);
	        	App.getInstance().setSeccion04B2(individual04b2);
	        	
	        	if(individual05_07==null) {
	        		individual05_07 = new CISECCION_05_07();
	        		individual05_07.id=individual01_03.id;
	        		individual05_07.hogar_id=individual01_03.hogar_id;
	        		individual05_07.persona_id=individual01_03.persona_id;
	        	}	
	        	
	        	individual05_07.filtro720= getCuestionarioService().VerificarFiltro720(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, mef.persona_id,6);
	        	
	        	App.getInstance().setCiseccion05_07(null);
	        	App.getInstance().setCiseccion05_07(individual05_07);
	        	
	        	
	        	if(individual08==null) {
	        		individual08 = new CISECCION_08();
	        		individual08.id=individual01_03.id;
	        		individual08.hogar_id=individual01_03.hogar_id;
	        		individual08.persona_id=individual01_03.persona_id;
	        	}	
	        	
	        	App.getInstance().setCiseccion_08(null);
	        	App.getInstance().setCiseccion_08(individual08);
	        	
	        	if(individual10==null) {
	        		individual10 = new CISECCION_10_01();
	        		individual10.id=individual01_03.id;
	        		individual10.hogar_id=individual01_03.hogar_id;
	        		individual10.persona_id=individual01_03.persona_id;
	        	}	
	        	
	        	App.getInstance().setCiseccion_10_01(null);
	        	App.getInstance().setCiseccion_10_01(individual10);
	        	App.getInstance().getCiseccion_10_01().filtroSiNinioS10=getCuestionarioService().getNiniosConCarnetVistabyPersonaId(individual10.id, individual10.hogar_id, individual10.persona_id);	        	
	        	
    		EditarIndividual(individual01_03);
		}
	}

	@Override
	public Integer grabadoParcial() {
		return App.NODEFINIDO;
	}
	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		Seccion01 bean = (Seccion01) dialog.get("seleccion");
		if (action == ACTION.RECUPERACION) {
			try {
			boolean test=false;
			test=getCuestionarioService().ModificarRecuperacionDeLaMef(bean);
			if (!test) {
				throw new SQLException("No se pudo grabar recuperaci�n");
			}
			} catch (SQLException e) {
				e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
		if(action==ACTION.ELIMINAR){
			SQLiteDatabase tran= getCuestionarioService().startTX();
			try {
				boolean borrado = false;
				borrado = getCuestionarioService().BorrarCuestionarioIndividualTransaccion(bean,tran);
				if (!borrado) {
					throw new SQLException("Persona no pudo ser borrada.");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
			finally{
				getCuestionarioService().endTX(tran);
			}
		}
		cargarTabla();
	}
	
}