package gob.inei.endes2024.fragment.CIseccion_04B_2;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04B2_Fragment_006 extends FragmentForm{
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI493_A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI493_B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI493_C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI493_D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI493_E; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI493_F; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI493_G; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI493_H; 
//	@FieldAnnotation(orderIndex=9) 
//	public RadioGroupOtherField rgQI493_I; 
	
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI493_I1; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI493_I2; 
	
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI493_J; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI493_K; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI493_L; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI493_M; 
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQI493_N; 
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQI493_O; 
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQI493_P; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI495A; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQI496; 
	@FieldAnnotation(orderIndex=20) 
	public TextField txtQI496X; 
	
	CISECCION_04B2 Individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta493,lblpregunta493a,lblpregunta493b,lblpregunta493c,lblpregunta493d,lblpregunta493e,lblpregunta493f,lblpregunta493g,lblpregunta493h,lblpregunta493i1,lblpregunta493i2,lblpregunta493j,
	lblpregunta493k,lblpregunta493l,lblpregunta493m,lblpregunta493n,lblpregunta493o,lblpregunta493p,lblpregunta495a,lblpregunta496; 
	private GridComponent2 gridPregunta493;
	public TextField txtCabecera;
	
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,SeccionesCargadoNacimiento;
	
	private boolean flagfiltro491=false;
	private boolean flagfiltro495=false;
	public CISECCION_04B2_Fragment_006() {} 
	public CISECCION_04B2_Fragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI493_A","QI493_B","QI493_C","QI493_D","QI493_E","QI493_F","QI493_G","QI493_H","QI493_I1","QI493_I2","QI493_J","QI493_K","QI493_L","QI493_M","QI493_N","QI493_O","QI493_P","QI495A","QI496","QI496X","QI492_A","QI492_B","QI492_C","QI492_D","QI492_E","QI492_F","QI492_G","QI490","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI493_A","QI493_B","QI493_C","QI493_D","QI493_E","QI493_F","QI493_G","QI493_H","QI493_I1","QI493_I2","QI493_J","QI493_K","QI493_L","QI493_M","QI493_N","QI493_O","QI493_P","QI495A","QI496","QI496X")};
//		SeccionesCargadoNacimiento = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"MIN(QI217)","QI212_NOM")};
		SeccionesCargadoNacimiento = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  
	  lblpregunta493 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi493);
	  lblpregunta493a = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_a);
	  lblpregunta493b = new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_b);
	  lblpregunta493c = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_c);
	  lblpregunta493d = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_d);
	  lblpregunta493e = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_e);
	  lblpregunta493f = new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_f);
	  lblpregunta493g = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_g);
	  lblpregunta493h = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_h);
	  //lblpregunta493i = new LabelComponent(getActivity()).size(altoComponente+38, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_i);
	  lblpregunta493i1 = new LabelComponent(getActivity()).size(altoComponente+38, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_i1);
	  lblpregunta493i2 = new LabelComponent(getActivity()).size(altoComponente+38, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_i2);
	  lblpregunta493j = new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_j);
	  lblpregunta493k = new LabelComponent(getActivity()).size(altoComponente+38, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_k);
	  lblpregunta493l = new LabelComponent(getActivity()).size(altoComponente+38, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_l);
	  lblpregunta493m = new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_m);
	  lblpregunta493n = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_n);
	  lblpregunta493o = new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_o);
	  lblpregunta493p = new LabelComponent(getActivity()).size(altoComponente+10, 450).textSize(17).text(R.string.c2seccion_04b_2qi493_p);
	  lblpregunta495a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi495a);
	  lblpregunta496 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi496);
		
	  rgQI493_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_a_1,R.string.c2seccion_04b_2qi493_a_2,R.string.c2seccion_04b_2qi493_a_3).size(altoComponente+10,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493AChangeValue"); 
		rgQI493_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_b_1,R.string.c2seccion_04b_2qi493_b_2,R.string.c2seccion_04b_2qi493_b_3).size(altoComponente,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493BChangeValue"); 
		rgQI493_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_c_1,R.string.c2seccion_04b_2qi493_c_2,R.string.c2seccion_04b_2qi493_c_3).size(altoComponente+10,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493CChangeValue"); 
		rgQI493_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_d_1,R.string.c2seccion_04b_2qi493_d_2,R.string.c2seccion_04b_2qi493_d_3).size(altoComponente+10,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493DChangeValue"); 
		rgQI493_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_e_1,R.string.c2seccion_04b_2qi493_e_2,R.string.c2seccion_04b_2qi493_e_3).size(altoComponente+10,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493EChangeValue"); 
		rgQI493_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_f_1,R.string.c2seccion_04b_2qi493_f_2,R.string.c2seccion_04b_2qi493_f_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493FChangeValue"); 
		rgQI493_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_g_1,R.string.c2seccion_04b_2qi493_g_2,R.string.c2seccion_04b_2qi493_g_3).size(altoComponente+10,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493GChangeValue"); 
		rgQI493_H=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_h_1,R.string.c2seccion_04b_2qi493_h_2,R.string.c2seccion_04b_2qi493_h_3).size(altoComponente+10,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493HChangeValue"); 
		rgQI493_I1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_i1_1,R.string.c2seccion_04b_2qi493_i1_2,R.string.c2seccion_04b_2qi493_i1_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493I1ChangeValue");
		rgQI493_I2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_i2_1,R.string.c2seccion_04b_2qi493_i2_2,R.string.c2seccion_04b_2qi493_i2_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493I2ChangeValue");
		rgQI493_J=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_j_1,R.string.c2seccion_04b_2qi493_j_2,R.string.c2seccion_04b_2qi493_j_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493JChangeValue"); 
		rgQI493_K=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_k_1,R.string.c2seccion_04b_2qi493_k_2,R.string.c2seccion_04b_2qi493_k_3).size(altoComponente+38,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493KChangeValue"); 
		rgQI493_L=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_l_1,R.string.c2seccion_04b_2qi493_l_2,R.string.c2seccion_04b_2qi493_l_3).size(altoComponente+38,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493LChangeValue"); 
		rgQI493_M=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_m_1,R.string.c2seccion_04b_2qi493_m_2,R.string.c2seccion_04b_2qi493_m_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493MChangeValue"); 
		rgQI493_N=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_n_1,R.string.c2seccion_04b_2qi493_n_2,R.string.c2seccion_04b_2qi493_n_3).size(altoComponente+10,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493NChangeValue"); 
		rgQI493_O=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_o_1,R.string.c2seccion_04b_2qi493_o_2,R.string.c2seccion_04b_2qi493_o_3).size(altoComponente,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493OChangeValue"); 
		rgQI493_P=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi493_p_1,R.string.c2seccion_04b_2qi493_p_2,R.string.c2seccion_04b_2qi493_p_3).size(altoComponente+10,220).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI493PChangeValue"); 
		
		gridPregunta493 = new GridComponent2(getActivity(), App.ESTILO,2,1);
		gridPregunta493.addComponent(lblpregunta493a);
		gridPregunta493.addComponent(rgQI493_A);
		gridPregunta493.addComponent(lblpregunta493b);
		gridPregunta493.addComponent(rgQI493_B);
		gridPregunta493.addComponent(lblpregunta493c);
		gridPregunta493.addComponent(rgQI493_C);
		gridPregunta493.addComponent(lblpregunta493d);
		gridPregunta493.addComponent(rgQI493_D);
		gridPregunta493.addComponent(lblpregunta493e);
		gridPregunta493.addComponent(rgQI493_E);
		gridPregunta493.addComponent(lblpregunta493f);
		gridPregunta493.addComponent(rgQI493_F);
		gridPregunta493.addComponent(lblpregunta493g);
		gridPregunta493.addComponent(rgQI493_G);
		gridPregunta493.addComponent(lblpregunta493h);
		gridPregunta493.addComponent(rgQI493_H);
		gridPregunta493.addComponent(lblpregunta493i1);
		gridPregunta493.addComponent(rgQI493_I1);
		gridPregunta493.addComponent(lblpregunta493i2);
		gridPregunta493.addComponent(rgQI493_I2);
		gridPregunta493.addComponent(lblpregunta493j);
		gridPregunta493.addComponent(rgQI493_J);
		gridPregunta493.addComponent(lblpregunta493k);
		gridPregunta493.addComponent(rgQI493_K);
		gridPregunta493.addComponent(lblpregunta493l);
		gridPregunta493.addComponent(rgQI493_L);
		gridPregunta493.addComponent(lblpregunta493m);
		gridPregunta493.addComponent(rgQI493_M);
		gridPregunta493.addComponent(lblpregunta493n);
		gridPregunta493.addComponent(rgQI493_N);
		gridPregunta493.addComponent(lblpregunta493o);
		gridPregunta493.addComponent(rgQI493_O);
		gridPregunta493.addComponent(lblpregunta493p);
		gridPregunta493.addComponent(rgQI493_P);
		
		rgQI495A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi495a_1,R.string.c2seccion_04b_2qi495a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI495AChangeValue"); 
		rgQI496=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi496_1,R.string.c2seccion_04b_2qi496_2,R.string.c2seccion_04b_2qi496_3,R.string.c2seccion_04b_2qi496_4,R.string.c2seccion_04b_2qi496_5,R.string.c2seccion_04b_2qi496_6,R.string.c2seccion_04b_2qi496_7,R.string.c2seccion_04b_2qi496_8,R.string.c2seccion_04b_2qi496_9,R.string.c2seccion_04b_2qi496_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI496X= new TextField(getActivity()).size(altoComponente, 500).maxLength(100);
		rgQI496.agregarEspecifique(9, txtQI496X);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q1 = createQuestionSection(txtCabecera,lblpregunta493,gridPregunta493.component()); 
		q2 = createQuestionSection(lblpregunta495a ,rgQI495A); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta496,rgQI496); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    public void cambiarDatosparalaBD(){
    	Individual.qi493_a=Individual.qi493_a!=null?Individual.setConvertcontresvariables(Individual.qi493_a):null;
        Individual.qi493_b=Individual.qi493_b!=null?Individual.setConvertcontresvariables(Individual.qi493_b):null;
        Individual.qi493_c=Individual.qi493_c!=null?Individual.setConvertcontresvariables(Individual.qi493_c):null;
        Individual.qi493_d=Individual.qi493_d!=null?Individual.setConvertcontresvariables(Individual.qi493_d):null;
        Individual.qi493_e=Individual.qi493_e!=null?Individual.setConvertcontresvariables(Individual.qi493_e):null;
        Individual.qi493_f=Individual.qi493_f!=null?Individual.setConvertcontresvariables(Individual.qi493_f):null;
        Individual.qi493_g=Individual.qi493_g!=null?Individual.setConvertcontresvariables(Individual.qi493_g):null;
        Individual.qi493_h=Individual.qi493_h!=null?Individual.setConvertcontresvariables(Individual.qi493_h):null;
        //Individual.qi493_i=Individual.qi493_i!=null?Individual.setConvertcontresvariables(Individual.qi493_i):null;
        Individual.qi493_i1=Individual.qi493_i1!=null?Individual.setConvertcontresvariables(Individual.qi493_i1):null;
        Individual.qi493_i2=Individual.qi493_i2!=null?Individual.setConvertcontresvariables(Individual.qi493_i2):null;
        Individual.qi493_j=Individual.qi493_j!=null?Individual.setConvertcontresvariables(Individual.qi493_j):null;
        Individual.qi493_k=Individual.qi493_k!=null?Individual.setConvertcontresvariables(Individual.qi493_k):null;
        Individual.qi493_l=Individual.qi493_l!=null?Individual.setConvertcontresvariables(Individual.qi493_l):null;
        Individual.qi493_m=Individual.qi493_m!=null?Individual.setConvertcontresvariables(Individual.qi493_m):null;
        Individual.qi493_n=Individual.qi493_n!=null?Individual.setConvertcontresvariables(Individual.qi493_n):null;
        Individual.qi493_o=Individual.qi493_o!=null?Individual.setConvertcontresvariables(Individual.qi493_o):null;
        Individual.qi493_p=Individual.qi493_p!=null?Individual.setConvertcontresvariables(Individual.qi493_p):null;
        Individual.qi496=Individual.qi496!=null?Individual.setConvertcondiezvariables(Individual.qi496):null;
    }
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(flagfiltro491){
			if (Util.esVacio(Individual.qi493_a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_A"); 
				view = rgQI493_A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_B"); 
				view = rgQI493_B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_C"); 
				view = rgQI493_C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_D"); 
				view = rgQI493_D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_E"); 
				view = rgQI493_E; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_F"); 
				view = rgQI493_F; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_G"); 
				view = rgQI493_G; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_h)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_H"); 
				view = rgQI493_H; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_i1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_I1"); 
				view = rgQI493_I1; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_i2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_I2"); 
				view = rgQI493_I2; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_j)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_J"); 
				view = rgQI493_J; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_k)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_K"); 
				view = rgQI493_K; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_l)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_L"); 
				view = rgQI493_L; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_m)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_M"); 
				view = rgQI493_M; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_n)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_N"); 
				view = rgQI493_N; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_o)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_O"); 
				view = rgQI493_O; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi493_p)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI493_P"); 
				view = rgQI493_P; 
				error = true; 
				return false; 
			} 
			if(ObtenervalorP493()){
			if (Util.esVacio(Individual.qi495a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI495A"); 
				view = rgQI495A; 
				error = true; 
				return false; 
			} 
			if (Util.esDiferente(Individual.qi495a,1)){
				if (Util.esVacio(Individual.qi496)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI496"); 
					view = rgQI496; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(Individual.qi496,10)) {
					if (Util.esVacio(Individual.qi496x)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI496X"); 
						view = txtQI496X; 
						error = true; 
						return false; 
					}
				}
			}
		 }
			else{
				if (Util.esVacio(Individual.qi496)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI496"); 
					view = rgQI496; 
					error = true; 
					return false; 
				} 
				if (!Util.esDiferente(Individual.qi496,10)) {
					if (Util.esVacio(Individual.qi496x)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI496X"); 
						view = txtQI496X; 
						error = true; 
						return false; 
					}
				}
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado); 
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
		} 
		VerificarFiltro491();
    	VerificarFiltro495();
		App.getInstance().getSeccion04B2().qi490=Individual.qi490;
		recuperarDatosParaMostrar();
		entityToUI(Individual); 
		inicio(); 
    } 
    public void recuperarDatosParaMostrar(){
        Individual.qi493_a=Individual.qi493_a!=null?Individual.getConvertcontresvariables(Individual.qi493_a):null;
        Individual.qi493_b=Individual.qi493_b!=null?Individual.getConvertcontresvariables(Individual.qi493_b):null;
        Individual.qi493_c=Individual.qi493_c!=null?Individual.getConvertcontresvariables(Individual.qi493_c):null;
        Individual.qi493_d=Individual.qi493_d!=null?Individual.getConvertcontresvariables(Individual.qi493_d):null;
        Individual.qi493_e=Individual.qi493_e!=null?Individual.getConvertcontresvariables(Individual.qi493_e):null;
        Individual.qi493_f=Individual.qi493_f!=null?Individual.getConvertcontresvariables(Individual.qi493_f):null;
        Individual.qi493_g=Individual.qi493_g!=null?Individual.getConvertcontresvariables(Individual.qi493_g):null;
        Individual.qi493_h=Individual.qi493_h!=null?Individual.getConvertcontresvariables(Individual.qi493_h):null;
        //Individual.qi493_i=Individual.qi493_i!=null?Individual.getConvertcontresvariables(Individual.qi493_i):null;
        Individual.qi493_i1=Individual.qi493_i1!=null?Individual.getConvertcontresvariables(Individual.qi493_i1):null;
        Individual.qi493_i2=Individual.qi493_i2!=null?Individual.getConvertcontresvariables(Individual.qi493_i2):null;
        Individual.qi493_j=Individual.qi493_j!=null?Individual.getConvertcontresvariables(Individual.qi493_j):null;
        Individual.qi493_k=Individual.qi493_k!=null?Individual.getConvertcontresvariables(Individual.qi493_k):null;
        Individual.qi493_l=Individual.qi493_l!=null?Individual.getConvertcontresvariables(Individual.qi493_l):null;
        Individual.qi493_m=Individual.qi493_m!=null?Individual.getConvertcontresvariables(Individual.qi493_m):null;
        Individual.qi493_n=Individual.qi493_n!=null?Individual.getConvertcontresvariables(Individual.qi493_n):null;
        Individual.qi493_o=Individual.qi493_o!=null?Individual.getConvertcontresvariables(Individual.qi493_o):null;
        Individual.qi493_p=Individual.qi493_p!=null?Individual.getConvertcontresvariables(Individual.qi493_p):null;
        Individual.qi496=Individual.qi496!=null?Individual.getConvertcondiezvariables(Individual.qi496):null;
    }
    
    private void ValidarPase() { 
    	if (Util.esDiferente(Individual.qi490,5) && App.getInstance().getSeccion04B2().filtro491==false) {
    		Util.cleanAndLockView(getActivity(),rgQI495A,rgQI493_A,rgQI493_B,rgQI493_C,rgQI493_D,rgQI493_E,rgQI493_F,rgQI493_F,rgQI493_G,rgQI493_H,rgQI493_I1,rgQI493_I2,rgQI493_J,rgQI493_K,rgQI493_L,rgQI493_M,rgQI493_N,rgQI493_O,rgQI493_P,rgQI496);
//    		Log.e("sss","222");
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
		}
    	else{
//    		Log.e("sss","2222");
//        	VerificarFiltro491();
//        	VerificarFiltro495();
        	onQI493AChangeValue();
        	onQI493BChangeValue();
        	onQI493CChangeValue();
        	onQI493DChangeValue();
        	onQI493EChangeValue();
        	onQI493FChangeValue();
        	onQI493GChangeValue();
        	onQI493HChangeValue();
        	//onQI493IChangeValue();
        	onQI493I1ChangeValue();
        	onQI493I2ChangeValue();
        	onQI493JChangeValue();
        	onQI493KChangeValue();
        	onQI493LChangeValue();
        	onQI493MChangeValue();
        	onQI493NChangeValue();
        	onQI493OChangeValue();
        	onQI493PChangeValue();
        	onQI495AChangeValue();
        	RenombrarEtiquetas();
    	}
    }
    
    
    private void inicio() { 
    	ValidarPase();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void RenombrarEtiquetas() {
    	if(flagfiltro491){
    		CISECCION_02 ninio=getCuestionarioService().getUltimoNacimientoidMayorVivo(Individual.id,Individual.hogar_id,Individual.persona_id,SeccionesCargadoNacimiento);
    		if (ninio!=null && ninio.qi212_nom!=null) {
    			lblpregunta493.setText(lblpregunta493.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
    			lblpregunta496.setText(lblpregunta496.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
    		}
    	}
    }
    public void VerificarFiltro491(){
    	Calendar fecha = Calendar.getInstance();
    	flagfiltro491= getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha.get(Calendar.YEAR)-3);
    	
    	if(!flagfiltro491){
    		Util.cleanAndLockView(getActivity(), rgQI493_A,rgQI493_B,rgQI493_C,rgQI493_D,rgQI493_E,rgQI493_F,rgQI493_F,rgQI493_G,rgQI493_H,rgQI493_I1,rgQI493_I2,rgQI493_J,rgQI493_K,rgQI493_L,rgQI493_M,rgQI493_N,rgQI493_O,rgQI493_P,rgQI496);
    		q1.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI493_A,rgQI493_B,rgQI493_C,rgQI493_D,rgQI493_E,rgQI493_F,rgQI493_F,rgQI493_G,rgQI493_H,rgQI493_I1,rgQI493_I2,rgQI493_J,rgQI493_K,rgQI493_L,rgQI493_M,rgQI493_N,rgQI493_O,rgQI493_P,rgQI496);
    		q1.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    	}
    }
    public void VerificarFiltro495(){
    	flagfiltro495 = obtienevaloresp492()||obtienevaloresp493();
    	if(flagfiltro495){
    		Util.cleanAndLockView(getActivity(), rgQI495A);
    		q2.setVisibility(View.GONE);
    		rgQI496.requestFocus();
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI495A);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    public boolean obtienevaloresp492(){
    	if(flagfiltro491 && ((Individual.qi492_a==1 || Individual.qi492_a==8) || 
    						 (Individual.qi492_b==1 || Individual.qi492_b==8) || 
    						 (Individual.qi492_c==1 || Individual.qi492_c==8) || 
    						 (Individual.qi492_d==1 || Individual.qi492_d==8) || 
    						 (Individual.qi492_e==1 || Individual.qi492_e==8) || 
    						 (Individual.qi492_f==1 || Individual.qi492_f==8) || 
    						 (Individual.qi492_g==1 || Individual.qi492_g==8)))
    		return true;
    	else
    		return false;
    }
    public boolean obtienevaloresp493(){
    	boolean flagdata= false;
    	if(Individual.qi493_a!=null){
    	if(flagfiltro491 && ((Individual.qi493_a==1 || Individual.qi493_a==8) || 
    						 (Individual.qi493_b==1 || Individual.qi493_b==8) || 
    						 (Individual.qi493_c==1 || Individual.qi493_c==8) || 
    						 (Individual.qi493_d==1 || Individual.qi493_d==8) || 
    						 (Individual.qi493_e==1 || Individual.qi493_e==8) || 
    						 (Individual.qi493_f==1 || Individual.qi493_f==8) || 
    						 (Individual.qi493_g==1 || Individual.qi493_g==8) || 
    						 (Individual.qi493_h==1 || Individual.qi493_h==8) ||
//    						 (Individual.qi493_i==1 || Individual.qi493_i==8) ||
    						 (Individual.qi493_i1==1 || Individual.qi493_i1==8) ||
    						 (Individual.qi493_i2==1 || Individual.qi493_i2==8) ||
    						 (Individual.qi493_j==1 || Individual.qi493_j==8) || 
    						 (Individual.qi493_k==1 || Individual.qi493_k==8) || 
    						 (Individual.qi493_l==1 || Individual.qi493_l==8) || 
    						 (Individual.qi493_m==1 || Individual.qi493_m==8) || 
    						 (Individual.qi493_n==1 || Individual.qi493_n==8) || 
    						 (Individual.qi493_o==1 || Individual.qi493_o==8) || 
    						 (Individual.qi493_p==1 || Individual.qi493_p==8)))
    		flagdata=true;
    	else
    		flagdata=false;
    	}
    	return flagdata;
    }
    public boolean ObtenervalorP493(){
    	Integer valor_a = Integer.parseInt(rgQI493_A.getTagSelected("0").toString());
    	Integer valor_b = Integer.parseInt(rgQI493_B.getTagSelected("0").toString());
    	Integer valor_c = Integer.parseInt(rgQI493_C.getTagSelected("0").toString());
    	Integer valor_d = Integer.parseInt(rgQI493_D.getTagSelected("0").toString());
    	Integer valor_e = Integer.parseInt(rgQI493_E.getTagSelected("0").toString());
    	Integer valor_f = Integer.parseInt(rgQI493_F.getTagSelected("0").toString());
    	Integer valor_g = Integer.parseInt(rgQI493_G.getTagSelected("0").toString());
    	Integer valor_h = Integer.parseInt(rgQI493_H.getTagSelected("0").toString());
    	//Integer valor_i=  Integer.parseInt(rgQI493_I.getTagSelected("0").toString());
    	Integer valor_i1=  Integer.parseInt(rgQI493_I1.getTagSelected("0").toString());
    	Integer valor_i2=  Integer.parseInt(rgQI493_I2.getTagSelected("0").toString());
    	Integer valor_j = Integer.parseInt(rgQI493_J.getTagSelected("0").toString());
    	Integer valor_k = Integer.parseInt(rgQI493_K.getTagSelected("0").toString());
    	Integer valor_l = Integer.parseInt(rgQI493_L.getTagSelected("0").toString());
    	Integer valor_m = Integer.parseInt(rgQI493_M.getTagSelected("0").toString());
    	Integer valor_n = Integer.parseInt(rgQI493_N.getTagSelected("0").toString());
    	Integer valor_o = Integer.parseInt(rgQI493_O.getTagSelected("0").toString());
    	Integer valor_p = Integer.parseInt(rgQI493_P.getTagSelected("0").toString());
    	if(!Util.esDiferente(valor_a, 1,3)||
    			!Util.esDiferente(valor_a, 1,3)||
    			!Util.esDiferente(valor_b, 1,3)||
    			!Util.esDiferente(valor_c, 1,3)||
    			!Util.esDiferente(valor_d, 1,3)||
    			!Util.esDiferente(valor_e, 1,3)||
    			!Util.esDiferente(valor_f, 1,3)||
    			!Util.esDiferente(valor_g, 1,3)||
    			!Util.esDiferente(valor_h, 1,3)||
    			//!Util.esDiferente(valor_i, 1,3)||
    			!Util.esDiferente(valor_i1, 1,3)||
    			!Util.esDiferente(valor_i2, 1,3)||
    			!Util.esDiferente(valor_j, 1,3)||
    			!Util.esDiferente(valor_k, 1,3)||
    			!Util.esDiferente(valor_l, 1,3)||
    			!Util.esDiferente(valor_m, 1,3)||
    			!Util.esDiferente(valor_n, 1,3)||
    			!Util.esDiferente(valor_o, 1,3)||
    			!Util.esDiferente(valor_p, 1,3)||
    			!Util.esDiferente(Individual.qi492_a,1,8) || 
    			!Util.esDiferente(Individual.qi492_b,1,8) || 
    			!Util.esDiferente(Individual.qi492_c,1,8) || 
    			!Util.esDiferente(Individual.qi492_d,1,8) || 
    			!Util.esDiferente(Individual.qi492_e,1,8) || 
    			!Util.esDiferente(Individual.qi492_f,1,8) || 
    			!Util.esDiferente(Individual.qi492_g,1,8)
    			)
    		return false;
    		else
    			return true;
    }
    public void onQI493AChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493BChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493CChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493DChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493EChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493FChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493GChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493HChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
//    public void onQI493IChangeValue(){
//    	MostraroEsconderp495a(ObtenervalorP493());
//    }
    
    public void onQI493I1ChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493I2ChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    
    public void onQI493JChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493KChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493LChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493MChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493NChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493OChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    public void onQI493PChangeValue(){
    	MostraroEsconderp495a(ObtenervalorP493());
    }
    
    public void MostraroEsconderp495a(boolean qi493){
    	if(!qi493){
    		Util.cleanAndLockView(getActivity(), rgQI495A);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI495A);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    
    public void onQI495AChangeValue(){
    	Integer valor = Integer.parseInt(rgQI495A.getTagSelected("0").toString());
    	if(!Util.esDiferente(valor, 1)){
    		Util.cleanAndLockView(getActivity(), rgQI496);
    		q3.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI496);
    		q3.setVisibility(View.VISIBLE);
    		rgQI496.requestFocus();
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI493_A.readOnly();
    		rgQI493_B.readOnly();
    		rgQI493_C.readOnly();
    		rgQI493_D.readOnly();
    		rgQI493_E.readOnly();
    		rgQI493_F.readOnly();
    		rgQI493_G.readOnly();
    		rgQI493_H.readOnly();
    		rgQI493_I1.readOnly();
    		rgQI493_I2.readOnly();
    		rgQI493_J.readOnly();
    		rgQI493_K.readOnly();
    		rgQI493_L.readOnly();
    		rgQI493_M.readOnly();
    		rgQI493_N.readOnly();
    		rgQI493_O.readOnly();
    		rgQI493_P.readOnly();
    		rgQI495A.readOnly();
    		rgQI496.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
}
