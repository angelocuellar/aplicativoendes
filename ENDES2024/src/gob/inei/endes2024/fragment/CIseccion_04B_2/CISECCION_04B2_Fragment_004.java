package gob.inei.endes2024.fragment.CIseccion_04B_2; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04B2_Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI489A_A; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI489A_B; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI489A_C; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI489A_D; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI489A_E; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI489A_F; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI489A_X; 
	@FieldAnnotation(orderIndex=8) 
	public TextField txtQI489A_XI; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI489A_Z; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI489B; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI489C; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI489D; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI490; 
	public LabelComponent lblpregunta489a,lblpregunta489a1,lblpregunta489b,lblpregunta489c,lblpregunta489d,lblpregunta490;
	public GridComponent2 gridPregunta490A;
	public TextField txtCabecera;
	
	CISECCION_04B2 Individual; 
	private CuestionarioService cuestionarioService; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;  
	public CISECCION_04B2_Fragment_004() {} 
	public CISECCION_04B2_Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI489A_A","QI489A_B","QI489A_C","QI489A_D","QI489A_E","QI489A_F","QI489A_X","QI489A_XI","QI489A_Z","QI489B","QI489C","QI489D","QI490","QI489","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI489A_A","QI489A_B","QI489A_C","QI489A_D","QI489A_E","QI489A_F","QI489A_X","QI489A_XI","QI489A_Z","QI489B","QI489C","QI489D","QI490")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();	
	  	
	  	lblpregunta489a= new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi489a);
	  	lblpregunta489a1= new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi489a1);
	  	lblpregunta489b = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi489b);
	  	lblpregunta489c = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi489c);
	  	lblpregunta489d = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi489d);
	  	lblpregunta490 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi490);
		
	  	rgQI490=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi490_1,R.string.c2seccion_04b_2qi490_2,R.string.c2seccion_04b_2qi490_3,R.string.c2seccion_04b_2qi490_4,R.string.c2seccion_04b_2qi490_5,R.string.c2seccion_04b_2qi490_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		chbQI489A_A=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_a,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_B=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_b,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_C=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_c,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_D=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_d,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_E=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_e,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_F=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_f,"1:0").size(WRAP_CONTENT, 200); 
		chbQI489A_X=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI489AChangeValue"); 
		chbQI489A_Z=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi489a_z,"1:0").size(WRAP_CONTENT, 200);
		txtQI489A_XI = new TextField(getActivity()).size(altoComponente, 550).maxLength(100);
		rgQI489B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi489b_1,R.string.c2seccion_04b_2qi489b_2,R.string.c2seccion_04b_2qi489b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI489C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi489c_1,R.string.c2seccion_04b_2qi489c_2,R.string.c2seccion_04b_2qi489c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI489D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi489d_1,R.string.c2seccion_04b_2qi489d_2,R.string.c2seccion_04b_2qi489d_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		
		gridPregunta490A= new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridPregunta490A.addComponent(chbQI489A_A,2);
		gridPregunta490A.addComponent(chbQI489A_B,2);
		gridPregunta490A.addComponent(chbQI489A_C,2);
		gridPregunta490A.addComponent(chbQI489A_D,2);
		gridPregunta490A.addComponent(chbQI489A_E,2);
		gridPregunta490A.addComponent(chbQI489A_F,2);
		gridPregunta490A.addComponent(chbQI489A_X);
		gridPregunta490A.addComponent(txtQI489A_XI);
		gridPregunta490A.addComponent(chbQI489A_Z,2);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q1= createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,txtCabecera,lblpregunta489a,lblpregunta489a1,gridPregunta490A.component());
		q2= createQuestionSection(lblpregunta489b,rgQI489B);
		q3= createQuestionSection(lblpregunta489c,rgQI489C);
		q4= createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta489d,rgQI489D);
		q5= createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta490,rgQI490);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getSeccion04B2()==null) {
    		CISECCION_04B2 seccion4b2 = new CISECCION_04B2();
    		App.getInstance().setSeccion04B2(seccion4b2);
    	}
		
    	Calendar fecha = Calendar.getInstance();
    	App.getInstance().getSeccion04B2().filtro491 = getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha.get(Calendar.YEAR)-3);
    	App.getInstance().getSeccion04B2().qi490=Individual.qi490;
		
    	return true; 
    } 
    public void cambiarDatosparalaBD(){
    	Individual.qi489b =Individual.qi489b!=null?Individual.setConvertcontresvariables(Individual.qi489b):null;
    	Individual.qi489c = Individual.qi489c!=null?Individual.setConvertconnuevevariables(Individual.qi489c):null;
    	Individual.qi489d = Individual.qi489d!=null?Individual.setConvertconnuevevariables(Individual.qi489d):null;
    	Individual.qi490 = Individual.qi490!=null?Individual.setConvertconseisvariables(Individual.qi490):null;
    }
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(Individual.qi489,2)){
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi489a_a,Individual.qi489a_b,Individual.qi489a_c,Individual.qi489a_d,Individual.qi489a_e,Individual.qi489a_f,Individual.qi489a_x,Individual.qi489a_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI489A_A; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(Individual.qi489a_x, 1)) {
				if (Util.esVacio(Individual.qi489a_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI489XI"); 
					view = txtQI489A_XI; 
					error = true; 
					return false; 
				}
			}
			if (Util.esVacio(Individual.qi489b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI489B"); 
				view = rgQI489B; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(Individual.qi489c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI489C"); 
				view = rgQI489C; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(Individual.qi489d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI489D"); 
				view = rgQI489D; 
				error = true; 
				return false; 
			}
		}
		if (Util.esVacio(Individual.qi490)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI490"); 
			view = rgQI490; 
			error = true; 
			return false; 
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado); 
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
		} 
		App.getInstance().getSeccion04B2().qi490=Individual.qi490;
		recuperarDatosParaMostrar();
		entityToUI(Individual); 
		inicio(); 
    } 
    private void inicio() { 
    	onQI489AChangeValue();
    	if(Individual!=null && Individual.qi489!=null){
    		VerificarPregunta489();
    	}
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void VerificarFiltro491(){
    	
    }
    public void VerificarPregunta489(){
    	if(!Util.esDiferente(Individual.qi489,2)){
    		Util.cleanAndLockView(getActivity(), chbQI489A_A,chbQI489A_B,chbQI489A_C,chbQI489A_D,chbQI489A_E,chbQI489A_F,chbQI489A_X,chbQI489A_Z,rgQI489B,rgQI489C,rgQI489D);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,chbQI489A_A,chbQI489A_B,chbQI489A_C,chbQI489A_D,chbQI489A_E,chbQI489A_F,chbQI489A_X,chbQI489A_Z,rgQI489B,rgQI489C,rgQI489D);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    	}
    }
  

    public void recuperarDatosParaMostrar(){
  
//    HABILITAR EN AL A�O 2017    	
//    	Individual.qi489b =Individual.qi489b!=null?Individual.getConvertcontresvariables(Individual.qi489b):null;
//    	Individual.qi489c = Individual.qi489c!=null?Individual.getConvertcontresvariables(Individual.qi489c):null;
//    	Individual.qi489d = Individual.qi489d!=null?Individual.getConvertcontresvariables(Individual.qi489d):null;
//    	Individual.qi490 = Individual.qi490!=null?Individual.getConvertconseisvariables(Individual.qi490):null;
    	
    	Individual.qi489b =Individual.qi489b!=null?Individual.getConvertcontresvariables(Individual.qi489b):null;
    	Individual.qi489c = Individual.qi489c!=null?Individual.getConvertconnuevevariables(Individual.qi489c):null;
    	Individual.qi489d = Individual.qi489d!=null?Individual.getConvertconnuevevariables(Individual.qi489d):null;
    	Individual.qi490 = Individual.qi490!=null?Individual.getConvertconseisvariables(Individual.qi490):null;
    }
    
    public void onQI489AChangeValue(){
    	if(chbQI489A_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI489A_XI);
    	}else{
    		Util.cleanAndLockView(getActivity(), txtQI489A_XI);
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI489B.readOnly();
    		rgQI489C.readOnly();
    		rgQI489D.readOnly();
    		rgQI490.readOnly();
    		txtQI489A_XI.readOnly();
    		chbQI489A_A.readOnly();
    		chbQI489A_B.readOnly();
    		chbQI489A_C.readOnly();
    		chbQI489A_D.readOnly();
    		chbQI489A_E.readOnly();
    		chbQI489A_F.readOnly();
    		chbQI489A_X.readOnly();
    		chbQI489A_Z.readOnly();
    		txtQI489A_XI.readOnly();
    		
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
  		if(cuestionarioService==null){ 
  			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
  		} 
  		return cuestionarioService; 
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		if(App.getInstance().getSeccion04B2()==null) {
    		CISECCION_04B2 seccion4b2 = new CISECCION_04B2();
    		App.getInstance().setSeccion04B2(seccion4b2);
    	}
		
    	Calendar fecha = Calendar.getInstance();
    	App.getInstance().getSeccion04B2().filtro491 = getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha.get(Calendar.YEAR)-3);
    	App.getInstance().getSeccion04B2().qi490=Individual.qi490;
		
    	return App.INDIVIDUAL;
	} 
} 
