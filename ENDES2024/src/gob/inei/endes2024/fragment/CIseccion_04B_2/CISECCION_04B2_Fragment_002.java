package gob.inei.endes2024.fragment.CIseccion_04B_2; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
public class CISECCION_04B2_Fragment_002 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI480A;
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI481A_A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI481A_B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI481A_C;
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI481A_D; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI481A_E; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI481A_F; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI481A_G; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI481A_X; 
	@FieldAnnotation(orderIndex=10) 
	public TextField txtQI481A_XI; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI481A_Z; 
	@FieldAnnotation(orderIndex=12) //8 
	public RadioGroupOtherField rgQI482; 
	@FieldAnnotation(orderIndex=13) //9 
	public RadioGroupOtherField rgQI483; 
	@FieldAnnotation(orderIndex=14) //10
	
	public TextField txtQI483_X; 
	public LabelComponent lblpregunta480a,lblpregunta481a,lblpregunta481a1,lblpregunta481a2,lblpregunta482,lblpregunta483;
	public GridComponent2 gridpregunta481A;
	CISECCION_04B2 Individual; 
	public TextField txtCabecera;
	
	private CuestionarioService cuestionarioService; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	private boolean flagp480a=false;
	private boolean flag481=false;
	public CISECCION_04B2_Fragment_002() {} 
	public CISECCION_04B2_Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI480A","QI481A_A","QI481A_B","QI481A_C","QI481A_D","QI481A_E","QI481A_F","QI481A_G","QI481A_X","QI481A_XI","QI481A_Z","QI482","QI483","QI483_X","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI480A","QI481A_A","QI481A_B","QI481A_C","QI481A_D","QI481A_E","QI481A_F","QI481A_G","QI481A_X","QI481A_XI","QI481A_Z","QI482","QI483","QI483_X")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta482 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi482).textSize(19);
		lblpregunta480a = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi480a).textSize(19);
		lblpregunta481a= new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi481a).textSize(19);
		lblpregunta481a1 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi481a1).textSize(19);
		lblpregunta481a2 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi481a2).textSize(16);
		lblpregunta482 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi482).textSize(19);
		lblpregunta483 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi483).textSize(19);
		
		rgQI480A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi480a_1,R.string.c2seccion_04b_2qi480a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI482=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi482_1,R.string.c2seccion_04b_2qi482_2,R.string.c2seccion_04b_2qi482_3,R.string.c2seccion_04b_2qi482_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		chbQI481A_A=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_a,"1:0").size(WRAP_CONTENT, 200); 
		chbQI481A_B=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_b,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_C=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_c,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_D=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_d,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_E=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_e,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_F=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_f,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_G=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_g,"1:0").size(WRAP_CONTENT, 200);
		chbQI481A_X=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI481AXChangeValue");
		txtQI481A_XI = new TextField(getActivity()).size(altoComponente, 550).maxLength(100);
		chbQI481A_Z=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi481a_z,"1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		
		gridpregunta481A = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta481A.addComponent(chbQI481A_A,2);
		gridpregunta481A.addComponent(chbQI481A_B,2);
		gridpregunta481A.addComponent(chbQI481A_C,2);
		gridpregunta481A.addComponent(chbQI481A_D,2);
		gridpregunta481A.addComponent(chbQI481A_E,2);
		gridpregunta481A.addComponent(chbQI481A_F,2);
		gridpregunta481A.addComponent(chbQI481A_G,2);
		gridpregunta481A.addComponent(chbQI481A_X);
		gridpregunta481A.addComponent(txtQI481A_XI);
		gridpregunta481A.addComponent(chbQI481A_Z,2);
		
		rgQI482=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi482_1,R.string.c2seccion_04b_2qi482_2,R.string.c2seccion_04b_2qi482_3,R.string.c2seccion_04b_2qi482_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI483=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi483_1,R.string.c2seccion_04b_2qi483_2,R.string.c2seccion_04b_2qi483_3,R.string.c2seccion_04b_2qi483_4,R.string.c2seccion_04b_2qi483_5,R.string.c2seccion_04b_2qi483_6,R.string.c2seccion_04b_2qi483_7,R.string.c2seccion_04b_2qi483_8,R.string.c2seccion_04b_2qi483_9).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI483_X = new TextField(getActivity()).size(altoComponente, 280).maxLength(100);
		rgQI483.agregarEspecifique(8, txtQI483_X);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q1 =  createQuestionSection(0,txtCabecera,lblpregunta480a,rgQI480A);
		q2 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta481a,lblpregunta481a1,lblpregunta481a2,gridpregunta481A.component());
		q3 =  createQuestionSection(lblpregunta482,rgQI482);
		q4 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta483,rgQI483);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1); 
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    public void cambiarDatosparalaBD(){
    	Individual.qi482=Individual.qi482!=null?Individual.setConvertconcuatrovariables(Individual.qi482):null;
    	Individual.qi483 =Individual.qi483!=null?Individual.setConvertconnuevevariables(Individual.qi483):null;
    }
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(!flagp480a){
			if (Util.esVacio(Individual.qi480a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI480A"); 
				view = rgQI480A; 
				error = true; 
				return false; 
			} 
		}
		if(flag481){
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi481a_a,Individual.qi481a_b,Individual.qi481a_c,Individual.qi481a_d,Individual.qi481a_e,Individual.qi481a_f,Individual.qi481a_g,Individual.qi481a_x,Individual.qi481a_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI481A_A; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(Individual.qi481a_x)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI481A_X"); 
				view = chbQI481A_X; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(Individual.qi481a_x,1)) {
				if (Util.esVacio(Individual.qi481a_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI481A_XI"); 
					view = txtQI481A_XI; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(Individual.qi482)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI482"); 
				view = rgQI482; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(Individual.qi483)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI483"); 
				view = rgQI483; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(Individual.qi483,9)) {
				if (Util.esVacio(Individual.qi483_x)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI483_X"); 
					view = txtQI483_X; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado); 
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
	    } 
		recuperarDatosParaMostrar();
		entityToUI(Individual); 
		inicio(); 
    } 
    private void inicio() { 
    	verificarFiltro480();
    	verificarFiltro481();
    	onQI481AXChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void recuperarDatosParaMostrar(){
    	Individual.qi482=Individual.qi482!=null?Individual.getConvertconcuatrovariables(Individual.qi482):null;
    	Individual.qi483 = Individual.qi483!=null?Individual.getConvertconnuevevariables(Individual.qi483):null;
    }
    public void verificarFiltro480(){
    	flagp480a = getCuestionarioService().VerificarFiltro480(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	if(flagp480a){
    		Util.cleanAndLockView(getActivity(), rgQI480A);
    		q1.setVisibility(View.GONE);
    		chbQI481A_A.requestFocus();
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI480A);
    		q1.setVisibility(View.VISIBLE);
    	}
    }
    public void verificarFiltro481(){
    	Calendar fechaactual = Calendar.getInstance();
    	flag481 = getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fechaactual.get(Calendar.YEAR)-5);
    	if(!flag481){
    		Util.cleanAndLockView(getActivity(), chbQI481A_A,chbQI481A_B,chbQI481A_C,chbQI481A_D,chbQI481A_E,chbQI481A_F,chbQI481A_G,chbQI481A_X,chbQI481A_Z,rgQI482,rgQI483,txtQI481A_XI);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,chbQI481A_A,chbQI481A_B,chbQI481A_C,chbQI481A_D,chbQI481A_E,chbQI481A_F,chbQI481A_G,chbQI481A_X,chbQI481A_Z,rgQI482,rgQI483,txtQI481A_XI);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    	}
    }

    public void onQI481AXChangeValue(){
    	if(chbQI481A_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI481A_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI481A_XI);    		
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI480A.readOnly();
    		rgQI482.readOnly();
    		rgQI483.readOnly();
    		chbQI481A_A.readOnly();
    		chbQI481A_B.readOnly();
    		chbQI481A_C.readOnly();
    		chbQI481A_D.readOnly();
    		chbQI481A_E.readOnly();
    		chbQI481A_F.readOnly();
    		chbQI481A_G.readOnly();
    		chbQI481A_X.readOnly();
    		chbQI481A_Z.readOnly();
    		txtQI481A_XI.readOnly();
    		txtQI483_X.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
