package gob.inei.endes2024.fragment.CIseccion_04B_2;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04B2_Fragment_005 extends FragmentForm {
	
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI490A_A; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI490A_B; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI490A_C; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI490A_D; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI490A_E; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI490A_F; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI490A_G; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI490A_X; 
	@FieldAnnotation(orderIndex=9) 
	public TextField txtQI490A_XI; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI490B_A; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI490B_B; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI490B_C; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI490B_D; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI490B_X; 
	@FieldAnnotation(orderIndex=15) 
	public TextField txtQI490B_XI; 
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQI492_A; 
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQI492_B; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI492_C; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQI492_D; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQI492_E; 
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQI492_F; 
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQI492_G; 
	
	CISECCION_04B2 Individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblpregunta490a,lblpregunta490b,lblpregunta492,lblpregunta492a,lblpregunta492b,lblpregunta492c,lblpregunta492d,lblpregunta492e,lblpregunta492f,lblpregunta492g; 
	private GridComponent2 gridPregunta490A,gridPregunta490B,gridPregunta492;
	public TextField txtCabecera;
	
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,SeccionesCargadoNacimiento; 
	private boolean flagfiltro491=false;
	public CISECCION_04B2_Fragment_005() {} 
	public CISECCION_04B2_Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI490A_A","QI490A_B","QI490A_C","QI490A_D","QI490A_E","QI490A_F","QI490A_G","QI490A_X","QI490A_XI","QI490B_A","QI490B_B","QI490B_C","QI490B_D","QI490B_X","QI490B_XI","QI492_A","QI492_B","QI492_C","QI492_D","QI492_E","QI492_F","QI492_G","QI490","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI490A_A","QI490A_B","QI490A_C","QI490A_D","QI490A_E","QI490A_F","QI490A_G","QI490A_X","QI490A_XI","QI490B_A","QI490B_B","QI490B_C","QI490B_D","QI490B_X","QI490B_XI","QI492_A","QI492_B","QI492_C","QI492_D","QI492_E","QI492_F","QI492_G")};
//		SeccionesCargadoNacimiento = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"MIN(QI217)","QI212_NOM")}; 
		SeccionesCargadoNacimiento = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218")};
		
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  
		lblpregunta490a= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi490a);
		lblpregunta490b= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi490b);
		lblpregunta492= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi492);
		lblpregunta492a= new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_a);
		lblpregunta492b= new LabelComponent(getActivity()).size(altoComponente+20, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_b);
		lblpregunta492c= new LabelComponent(getActivity()).size(altoComponente+20, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_c);
		lblpregunta492d= new LabelComponent(getActivity()).size(altoComponente+20, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_d);
		lblpregunta492e= new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_e);
		lblpregunta492f= new LabelComponent(getActivity()).size(altoComponente, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_f);
		lblpregunta492g= new LabelComponent(getActivity()).size(altoComponente+20, 450).textSize(17).text(R.string.c2seccion_04b_2qi492_g);
		
		chbQI490A_A = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_a,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_B = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_b,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_C = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_c,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_D = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_d,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_E = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_e,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_F = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_f,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_G = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_g,"1:0").size(WRAP_CONTENT, 200);
		chbQI490A_X = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490a_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI490AChangeValue");
		txtQI490A_XI = new TextField(getActivity()).size(altoComponente, 500).maxLength(100);
		
		gridPregunta490A = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridPregunta490A.addComponent(chbQI490A_A,2);
		gridPregunta490A.addComponent(chbQI490A_B,2);
		gridPregunta490A.addComponent(chbQI490A_C,2);
		gridPregunta490A.addComponent(chbQI490A_D,2);
		gridPregunta490A.addComponent(chbQI490A_E,2);
		gridPregunta490A.addComponent(chbQI490A_F,2);
		gridPregunta490A.addComponent(chbQI490A_G,2);
		gridPregunta490A.addComponent(chbQI490A_X);
		gridPregunta490A.addComponent(txtQI490A_XI);
		
		chbQI490B_A = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490b_a,"1:0").size(WRAP_CONTENT, 200);
		chbQI490B_B = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490b_b,"1:0").size(WRAP_CONTENT, 200);
		chbQI490B_C = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490b_c,"1:0").size(WRAP_CONTENT, 200);
		chbQI490B_D = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490b_d,"1:0").size(WRAP_CONTENT, 200);
		chbQI490B_X = new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi490b_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI490BChangeValue");
		txtQI490B_XI = new TextField(getActivity()).size(altoComponente, 500).maxLength(100);
		
		gridPregunta490B = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridPregunta490B.addComponent(chbQI490B_A,2);
		gridPregunta490B.addComponent(chbQI490B_B,2);
		gridPregunta490B.addComponent(chbQI490B_C,2);
		gridPregunta490B.addComponent(chbQI490B_D,2);
		gridPregunta490B.addComponent(chbQI490B_X);
		gridPregunta490B.addComponent(txtQI490B_XI);
		
		rgQI492_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_a_1,R.string.c2seccion_04b_2qi492_a_2,R.string.c2seccion_04b_2qi492_a_3).size(altoComponente,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_b_1,R.string.c2seccion_04b_2qi492_b_2,R.string.c2seccion_04b_2qi492_b_3).size(altoComponente+20,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_c_1,R.string.c2seccion_04b_2qi492_c_2,R.string.c2seccion_04b_2qi492_c_3).size(altoComponente+20,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_d_1,R.string.c2seccion_04b_2qi492_d_2,R.string.c2seccion_04b_2qi492_d_3).size(altoComponente+20,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_e_1,R.string.c2seccion_04b_2qi492_e_2,R.string.c2seccion_04b_2qi492_e_3).size(altoComponente,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_f_1,R.string.c2seccion_04b_2qi492_f_2,R.string.c2seccion_04b_2qi492_f_3).size(altoComponente,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI492_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi492_g_1,R.string.c2seccion_04b_2qi492_g_2,R.string.c2seccion_04b_2qi492_g_3).size(altoComponente+20,280).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		gridPregunta492 = new GridComponent2(getActivity(), App.ESTILO,2,1);
		gridPregunta492.addComponent(lblpregunta492a);
		gridPregunta492.addComponent(rgQI492_A);
		gridPregunta492.addComponent(lblpregunta492b);
		gridPregunta492.addComponent(rgQI492_B);
		gridPregunta492.addComponent(lblpregunta492c);
		gridPregunta492.addComponent(rgQI492_C);
		gridPregunta492.addComponent(lblpregunta492d);
		gridPregunta492.addComponent(rgQI492_D);
		gridPregunta492.addComponent(lblpregunta492e);
		gridPregunta492.addComponent(rgQI492_E);
		gridPregunta492.addComponent(lblpregunta492f);
		gridPregunta492.addComponent(rgQI492_F);
		gridPregunta492.addComponent(lblpregunta492g);
		gridPregunta492.addComponent(rgQI492_G);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q1=createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,txtCabecera,lblpregunta490a,gridPregunta490A.component());
		q2=createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta490b,gridPregunta490B.component());
		q3=createQuestionSection(lblpregunta492,gridPregunta492.component());
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		Calendar cal=Calendar.getInstance();
		App.getInstance().getSeccion04B2().filtro491=getCuestionarioService().VerificarFiltro481(Individual.id, Individual.hogar_id, Individual.persona_id, cal.get(Calendar.YEAR)-3);
		return true; 
    } 
    
    public void cambiarDatosparalaBD(){
    	Individual.qi492_a=Individual.qi492_a!=null?Individual.setConvertcontresvariables(Individual.qi492_a):null;
    	Individual.qi492_b=Individual.qi492_b!=null?Individual.setConvertcontresvariables(Individual.qi492_b):null;
    	Individual.qi492_c=Individual.qi492_c!=null?Individual.setConvertcontresvariables(Individual.qi492_c):null;
    	Individual.qi492_d=Individual.qi492_d!=null?Individual.setConvertcontresvariables(Individual.qi492_d):null;
    	Individual.qi492_e=Individual.qi492_e!=null?Individual.setConvertcontresvariables(Individual.qi492_e):null;
    	Individual.qi492_f=Individual.qi492_f!=null?Individual.setConvertcontresvariables(Individual.qi492_f):null;
    	Individual.qi492_g=Individual.qi492_g!=null?Individual.setConvertcontresvariables(Individual.qi492_g):null;
    }
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if(Util.esDiferente(Individual.qi490, 5)){ 
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi490a_a,Individual.qi490a_b,Individual.qi490a_c,Individual.qi490a_d,Individual.qi490a_e,Individual.qi490a_f,Individual.qi490a_g,Individual.qi490a_x) && Util.esDiferente(Individual.qi490, 5)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI490A_A; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(Individual.qi490a_x, 1)){
				if (Util.esVacio(Individual.qi490a_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI490A_XI"); 
					view = txtQI490A_XI; 
					error = true; 
					return false; 
				} 
			}
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi490b_a,Individual.qi490b_b,Individual.qi490b_c,Individual.qi490b_d,Individual.qi490b_x) && Util.esDiferente(Individual.qi490, 5)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI490B_A; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(Individual.qi490b_x, 1)){
				if (Util.esVacio(Individual.qi490b_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI490B_XI"); 
					view = txtQI490B_XI; 
					error = true; 
					return false; 
				} 
			}
		}
			
			if(flagfiltro491){
				if (Util.esVacio(Individual.qi492_a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_A"); 
					view = rgQI492_A; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(Individual.qi492_b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_B"); 
					view = rgQI492_B; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(Individual.qi492_c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_C"); 
					view = rgQI492_C; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(Individual.qi492_d)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_D"); 
					view = rgQI492_D; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(Individual.qi492_e)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_E"); 
					view = rgQI492_E; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(Individual.qi492_f)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_F"); 
					view = rgQI492_F; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(Individual.qi492_g)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI492_G"); 
					view = rgQI492_G; 
					error = true; 
					return false; 
				}
		  }
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado); 
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
		} 
		App.getInstance().getSeccion04B2().qi490=Individual.qi490;
		recuperarDatosParaMostrar();
		entityToUI(Individual); 
		inicio(); 
    } 
    
    public void recuperarDatosParaMostrar(){
		Individual.qi492_a=Individual.qi492_a!=null?Individual.getConvertcontresvariables(Individual.qi492_a):null;
    	Individual.qi492_b=Individual.qi492_b!=null?Individual.getConvertcontresvariables(Individual.qi492_b):null;
    	Individual.qi492_c=Individual.qi492_c!=null?Individual.getConvertcontresvariables(Individual.qi492_c):null;
    	Individual.qi492_d=Individual.qi492_d!=null?Individual.getConvertcontresvariables(Individual.qi492_d):null;
    	Individual.qi492_e=Individual.qi492_e!=null?Individual.getConvertcontresvariables(Individual.qi492_e):null;
    	Individual.qi492_f=Individual.qi492_f!=null?Individual.getConvertcontresvariables(Individual.qi492_f):null;
    	Individual.qi492_g=Individual.qi492_g!=null?Individual.getConvertcontresvariables(Individual.qi492_g):null;
    }
    
    private void inicio() { 
    	VerificarsaltoQI490();
    	VerificarFiltro491();
    	onQI490AChangeValue();
    	onQI490BChangeValue();
    	RenombrarEtiquetas();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	
    } 
    
    public void RenombrarEtiquetas(){
    	
    	lblpregunta492.text(R.string.c2seccion_04b_2qi492);
    	
    	if(flagfiltro491){
    		CISECCION_02 ninio=getCuestionarioService().getUltimoNacimientoidMayorVivo(Individual.id,Individual.hogar_id,Individual.persona_id,SeccionesCargadoNacimiento);
    		if (ninio!=null && ninio.qi212_nom!=null) {
    			lblpregunta492.setText(lblpregunta492.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));	
    		} 
    	}
    }
    
    public void VerificarsaltoQI490(){
    	if(!Util.esDiferente(Individual.qi490, 5)){
    		Util.cleanAndLockView(getActivity(), chbQI490A_A,chbQI490A_B,chbQI490A_C,chbQI490A_D,chbQI490A_D,chbQI490A_E,chbQI490A_F,chbQI490A_G,chbQI490A_X,txtQI490A_XI,chbQI490B_A,chbQI490B_B,chbQI490B_C,chbQI490B_D,chbQI490B_X,txtQI490B_XI);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,chbQI490A_A,chbQI490A_B,chbQI490A_C,chbQI490A_D,chbQI490A_D,chbQI490A_E,chbQI490A_F,chbQI490A_G,chbQI490A_X,txtQI490A_XI,chbQI490B_A,chbQI490B_B,chbQI490B_C,chbQI490B_D,chbQI490B_X,txtQI490B_XI);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    	}
    }
    
    public void VerificarFiltro491(){
    	Calendar fecha = Calendar.getInstance();
    	flagfiltro491= getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fecha.get(Calendar.YEAR)-3);
    	if(!flagfiltro491){
    		Util.cleanAndLockView(getActivity(), rgQI492_A,rgQI492_B,rgQI492_C,rgQI492_D,rgQI492_E,rgQI492_F,rgQI492_G);
    		q3.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI492_A,rgQI492_B,rgQI492_C,rgQI492_D,rgQI492_E,rgQI492_F,rgQI492_G);
    		q3.setVisibility(View.VISIBLE);
    	}
    }
    
    public void onQI490AChangeValue(){
    	if(chbQI490A_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI490A_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI490A_XI);
    	}
    }
    
    public void onQI490BChangeValue(){
    	if(chbQI490B_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI490B_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI490B_XI);
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI492_A.readOnly();
    		rgQI492_B.readOnly();
    		rgQI492_C.readOnly();
    		rgQI492_D.readOnly();
    		rgQI492_E.readOnly();
    		rgQI492_F.readOnly();
    		rgQI492_G.readOnly();
    		chbQI490A_A.readOnly();
    		chbQI490A_B.readOnly();
    		chbQI490A_C.readOnly();
    		chbQI490A_D.readOnly();
    		chbQI490A_E.readOnly();
    		chbQI490A_F.readOnly();
    		chbQI490A_G.readOnly();
    		chbQI490A_X.readOnly();
    		chbQI490B_A.readOnly();
    		chbQI490B_B.readOnly();
    		chbQI490B_C.readOnly();
    		chbQI490B_D.readOnly();
    		chbQI490B_X.readOnly();
    		txtQI490A_XI.readOnly();
    		txtQI490A_XI.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		Calendar cal=Calendar.getInstance();
		App.getInstance().getSeccion04B2().filtro491=getCuestionarioService().VerificarFiltro481(Individual.id, Individual.hogar_id, Individual.persona_id, cal.get(Calendar.YEAR)-3);
		return App.INDIVIDUAL;
	}
}
