package gob.inei.endes2024.fragment.CIseccion_04B_2; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04B2_Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI487_A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI487_B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI487_C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI487_D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI487_E; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI487_F; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI487_G; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI487_H; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI487_I; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI488; 
	@FieldAnnotation(orderIndex=11) 
	public IntegerField txtQI488A; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI489;
	
	public LabelComponent lblpregunta487,lblpregunta487a,lblpregunta487b,lblpregunta487c,lblpregunta487d,lblpregunta487e,lblpregunta487f,lblpregunta487g,lblpregunta487h
	,lblpregunta487i,lblpregunta488,lblpregunta488a,lblpregunta489;
	public GridComponent2 gridPregunta487;
	public TextField txtCabecera;
	
	CISECCION_04B2 Individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	public CISECCION_04B2_Fragment_003() {} 
	public CISECCION_04B2_Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI487_A","QI487_B","QI487_C","QI487_D","QI487_E","QI487_F","QI487_G","QI487_H","QI487_I","QI488","QI488A","QI489","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI487_A","QI487_B","QI487_C","QI487_D","QI487_E","QI487_F","QI487_G","QI487_H","QI487_I","QI488","QI488A","QI489")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta487 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi487).textSize(19);
		lblpregunta487a = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_a).textSize(17);
		lblpregunta487b = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_b).textSize(17);
		lblpregunta487c = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_c).textSize(17);
		lblpregunta487d = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_d).textSize(17);
		lblpregunta487e = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_e).textSize(17);
		lblpregunta487f = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_f).textSize(17);
		lblpregunta487g = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_g).textSize(17);
		lblpregunta487h = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_h).textSize(17);
		lblpregunta487i = new LabelComponent(getActivity()).size(altoComponente,550).text(R.string.c2seccion_04b_2qi487_i).textSize(17);
		lblpregunta488 =  new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi488).textSize(19);
		lblpregunta488a =  new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi488a).textSize(19);
		lblpregunta489 =  new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi489).textSize(19);
		
		rgQI487_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_a_1,R.string.c2seccion_04b_2qi487_a_2).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_b_1,R.string.c2seccion_04b_2qi487_b_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_c_1,R.string.c2seccion_04b_2qi487_c_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_d_1,R.string.c2seccion_04b_2qi487_d_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_e_1,R.string.c2seccion_04b_2qi487_e_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_f_1,R.string.c2seccion_04b_2qi487_f_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_g_1,R.string.c2seccion_04b_2qi487_g_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_H=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_h_1,R.string.c2seccion_04b_2qi487_h_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI487_I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi487_i_1,R.string.c2seccion_04b_2qi487_i_2).size(WRAP_CONTENT,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQI488=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi488_1,R.string.c2seccion_04b_2qi488_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("qi488ChangeValue"); 
		rgQI489=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi489_1,R.string.c2seccion_04b_2qi489_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		txtQI488A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		
		gridPregunta487 = new GridComponent2(getActivity(), App.ESTILO,2,1);
		gridPregunta487.addComponent(lblpregunta487a);
		gridPregunta487.addComponent(rgQI487_A);
		gridPregunta487.addComponent(lblpregunta487b);
		gridPregunta487.addComponent(rgQI487_B);
		gridPregunta487.addComponent(lblpregunta487c);
		gridPregunta487.addComponent(rgQI487_C);
		gridPregunta487.addComponent(lblpregunta487d);
		gridPregunta487.addComponent(rgQI487_D);
		gridPregunta487.addComponent(lblpregunta487e);
		gridPregunta487.addComponent(rgQI487_E);
		gridPregunta487.addComponent(lblpregunta487f);
		gridPregunta487.addComponent(rgQI487_F);
		gridPregunta487.addComponent(lblpregunta487g);
		gridPregunta487.addComponent(rgQI487_G);
		gridPregunta487.addComponent(lblpregunta487h);
		gridPregunta487.addComponent(rgQI487_H);
		gridPregunta487.addComponent(lblpregunta487i);
		gridPregunta487.addComponent(rgQI487_I);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q1 = createQuestionSection(txtCabecera,lblpregunta487,gridPregunta487.component()); 
		q2 = createQuestionSection(lblpregunta488 ,rgQI488); 
		q3 = createQuestionSection(lblpregunta488a ,txtQI488A); 
		q4 = createQuestionSection(lblpregunta489 ,rgQI489);
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getPersonaSeccion01()!=null && !Util.esDiferente(App.getInstance().getPersonaSeccion01().persona_id,Individual.persona_id)){
					SeccionCapitulo[] seccionesGrabadoSalud = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QS28") };
					Salud individualSalud = new Salud();
					individualSalud.id= Individual.id;
					individualSalud.hogar_id= Individual.hogar_id;
					individualSalud.persona_id= Individual.persona_id;
					individualSalud.qs28= Individual.qi489;
					if(!getCuestionarioService().saveOrUpdate(individualSalud,seccionesGrabadoSalud)){
						ToastMessage.msgBox(this.getActivity(), "ERROR: al grabar", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					}
										
					CISECCION_08 individual8; 
					SeccionCapitulo[] SeccionesCargadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI801A","QI801B","ID","HOGAR_ID","PERSONA_ID")};
					individual8 = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,SeccionesCargadoSeccion08);
					if(individual8==null)
						individual8 = new CISECCION_08();
					if( !((Individual.qi489!=null && Individual.qi489==1) || (individual8.qi801a!=null && individual8.qi801a==1) || (individual8.qi801b!=null && individual8.qi801b==1))) {
						CAP04_07 cap04_07 = new CAP04_07(); 
						cap04_07.id= Individual.id;
						cap04_07.hogar_id= Individual.hogar_id;
						cap04_07.persona_id= Individual.persona_id;
						SeccionCapitulo[] seccionesGrabado4_7 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604","QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
						getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado4_7);
					}
				
			}
		  }	
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(Individual.qi487_a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_A"); 
			view = rgQI487_A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_B"); 
			view = rgQI487_B; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_C"); 
			view = rgQI487_C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_D"); 
			view = rgQI487_D; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_E"); 
			view = rgQI487_E; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_f)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_F"); 
			view = rgQI487_F; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_g)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_G"); 
			view = rgQI487_G; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_h)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_H"); 
			view = rgQI487_H; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi487_i)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI487_I"); 
			view = rgQI487_I; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(Individual.qi488)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI488"); 
			view = rgQI488; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(Individual.qi488,1)) {
			if (Util.esVacio(Individual.qi488a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI488A"); 
				view = txtQI488A; 
				error = true; 
				return false; 
			}
		}
		if (Util.esVacio(Individual.qi489)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI489"); 
			view = rgQI489; 
			error = true; 
			return false; 
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado); 
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
		} 
		entityToUI(Individual); 
		inicio(); 
    } 
    private void inicio() { 
    	RenombrarEtiquetas();
    	qi488ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void RenombrarEtiquetas(){
    	lblpregunta487.text(R.string.c2seccion_04b_2qi487);
    	lblpregunta487.setText(Html.fromHtml(lblpregunta487.getText()+"<b> �Es para Ud. un gran problema:</b>"));
    }

    
    public void qi488ChangeValue(){
    	Integer valor = Integer.parseInt(rgQI488.getTagSelected("0").toString());
    	if(Util.esDiferente(valor, 0,1)){
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), txtQI488A);
    		q3.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(),false, txtQI488A);
    		q3.setVisibility(View.VISIBLE);
    	}
    }
    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI487_A.readOnly();
    		rgQI487_B.readOnly();
    		rgQI487_C.readOnly();
    		rgQI487_D.readOnly();
    		rgQI487_E.readOnly();
    		rgQI487_F.readOnly();
    		rgQI487_G.readOnly();
    		rgQI487_H.readOnly();
    		rgQI487_I.readOnly();
    		rgQI488.readOnly();
    		rgQI489.readOnly();
    		txtQI488A.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
