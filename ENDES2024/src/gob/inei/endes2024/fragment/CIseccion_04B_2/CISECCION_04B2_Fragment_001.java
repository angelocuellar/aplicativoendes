package gob.inei.endes2024.fragment.CIseccion_04B_2; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.io.Console;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.DownloadManager.Request;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_04B2_Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI479A; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI479B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI479C; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI479D;
	
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI479E_A; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI479E_B; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI479E_C;
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI479E_D; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI479E_E; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI479E_F; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI479E_G; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI479E_H;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI479E_I;
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI479E_J;
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI479E_X;
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQI479E_XI; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI479E_Y; 
	
	
	
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI479F_A; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI479F_B; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI479F_C;
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI479F_D; 
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI479F_E; 
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI479F_F; 
	@FieldAnnotation(orderIndex=24) 
	public CheckBoxField chbQI479F_G; 
	@FieldAnnotation(orderIndex=25) 
	public CheckBoxField chbQI479F_H;
	@FieldAnnotation(orderIndex=26) 
	public CheckBoxField chbQI479F_I;
	@FieldAnnotation(orderIndex=27) 
	public CheckBoxField chbQI479F_J;
	@FieldAnnotation(orderIndex=28) 
	public CheckBoxField chbQI479F_K;
	@FieldAnnotation(orderIndex=29) 
	public CheckBoxField chbQI479F_L;
	@FieldAnnotation(orderIndex=30) 
	public CheckBoxField chbQI479F_M;	
	@FieldAnnotation(orderIndex=31) 
	public CheckBoxField chbQI479F_X;
	@FieldAnnotation(orderIndex=32) 
	public TextField txtQI479F_XI; 
	@FieldAnnotation(orderIndex=33) 
	public CheckBoxField chbQI479F_Y; 
	
	
	@FieldAnnotation(orderIndex=34) 
	public RadioGroupOtherField rgQI479G;
	@FieldAnnotation(orderIndex=35) 
	public CheckBoxField chbQI479G_A; 
	@FieldAnnotation(orderIndex=36) 
	public CheckBoxField chbQI479G_B; 
	@FieldAnnotation(orderIndex=37) 
	public CheckBoxField chbQI479G_C;
	@FieldAnnotation(orderIndex=38) 
	public CheckBoxField chbQI479G_D; 
	@FieldAnnotation(orderIndex=39) 
	public CheckBoxField chbQI479G_E; 
	@FieldAnnotation(orderIndex=40) 
	public CheckBoxField chbQI479G_F; 
	@FieldAnnotation(orderIndex=41) 
	public CheckBoxField chbQI479G_G; 
	@FieldAnnotation(orderIndex=42) 
	public CheckBoxField chbQI479G_H;
	@FieldAnnotation(orderIndex=43) 
	public CheckBoxField chbQI479G_I;
	@FieldAnnotation(orderIndex=44) 
	public CheckBoxField chbQI479G_J;
	@FieldAnnotation(orderIndex=45) 
	public CheckBoxField chbQI479G_X;
	@FieldAnnotation(orderIndex=46) 
	public TextField txtQI479G_XI; 
	@FieldAnnotation(orderIndex=47) 
	public CheckBoxField chbQI479G_Y;
	
	@FieldAnnotation(orderIndex=48) 
	public IntegerField txtQI479GB_A;
	@FieldAnnotation(orderIndex=49) 
	public IntegerField txtQI479GB_B;
	@FieldAnnotation(orderIndex=50) 
	public IntegerField txtQI479GB_C;
	@FieldAnnotation(orderIndex=51) 
	public IntegerField txtQI479GB_D;
	@FieldAnnotation(orderIndex=52) 
	public IntegerField txtQI479GB_E;
	@FieldAnnotation(orderIndex=53) 
	public IntegerField txtQI479GB_F;
	@FieldAnnotation(orderIndex=54) 
	public IntegerField txtQI479GB_G;
	@FieldAnnotation(orderIndex=55)
	public IntegerField txtQI479GB_H;
	@FieldAnnotation(orderIndex=56) 
	public IntegerField txtQI479GB_I;
	@FieldAnnotation(orderIndex=57) 
	public IntegerField txtQI479GB_J;
	@FieldAnnotation(orderIndex=58) 
	public IntegerField txtQI479GB_X;	
	@FieldAnnotation(orderIndex=59) 
	public IntegerField txtQI479GB_Y;
	
	@FieldAnnotation(orderIndex=60) 
	public RadioGroupOtherField rgQI479GC_A;
	@FieldAnnotation(orderIndex=61) 
	public RadioGroupOtherField rgQI479GC_B;
	@FieldAnnotation(orderIndex=62) 
	public RadioGroupOtherField rgQI479GC_C;
	@FieldAnnotation(orderIndex=63) 
	public RadioGroupOtherField rgQI479GC_D;
	@FieldAnnotation(orderIndex=64) 
	public RadioGroupOtherField rgQI479GC_E;
	@FieldAnnotation(orderIndex=65) 
	public RadioGroupOtherField rgQI479GC_F;
	@FieldAnnotation(orderIndex=66) 
	public RadioGroupOtherField rgQI479GC_G;
	@FieldAnnotation(orderIndex=67)
	public RadioGroupOtherField rgQI479GC_H;
	@FieldAnnotation(orderIndex=68) 
	public RadioGroupOtherField rgQI479GC_I;
	@FieldAnnotation(orderIndex=69) 
	public RadioGroupOtherField rgQI479GC_J;
	@FieldAnnotation(orderIndex=70) 
	public RadioGroupOtherField rgQI479GC_X;	
	@FieldAnnotation(orderIndex=71) 
	public RadioGroupOtherField rgQI479GC_Y;
	
	public TextField txtCabecera;
	public CheckBoxField chb479gb_a, chb479gb_b, chb479gb_c, chb479gb_d, chb479gb_e, chb479gb_f, chb479gb_g, chb479gb_h, chb479gb_i, chb479gb_j, chb479gb_x;
	
	CISECCION_04B2 Individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta479a,lblpregunta479a1,lblpregunta479a2,lblpregunta479b,lblpregunta479c,lblpregunta479d,lblpregunta479e,lblpregunta479f,lblpregunta479e_ind,lblpregunta479f_ind,lblpregunta479G,lblpregunta479Gaa,lblpregunta479Gbb,lblpregunta479Gcc;
	private LabelComponent lblpregunta479gba,lblpregunta479gbb,lblpregunta479gbc,lblpregunta479gbd,lblpregunta479gbe,lblpregunta479gbf,lblpregunta479gbg,lblpregunta479gbh,lblpregunta479gbi,lblpregunta479gbj,lblpregunta479gbx;
	private LabelComponent lblpregunta479gca,lblpregunta479gcb,lblpregunta479gcc,lblpregunta479gcd,lblpregunta479gce,lblpregunta479gcf,lblpregunta479gcg,lblpregunta479gch,lblpregunta479gci,lblpregunta479gcj,lblpregunta479gcx;
	public GridComponent2 gridpregunta479E,gridpregunta479F,gridpregunta479G,gridpregunta479Gsubc,gridpregunta479GD;
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7,q8,q9; 
	 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	private boolean flagpase = false;
	private String fecharef_479g=null;
	public CISECCION_04B2_Fragment_001() {} 
	public CISECCION_04B2_Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI479A","QI479B","QI479C","QI479D","QI479E_A","QI479E_B","QI479E_C","QI479E_D","QI479E_E","QI479E_F","QI479E_G","QI479E_H","QI479E_I","QI479E_J","QI479E_X","QI479E_XI","QI479E_Y","QI479F_A","QI479F_B","QI479F_C","QI479F_D","QI479F_E","QI479F_F","QI479F_G","QI479F_H","QI479F_I","QI479F_J","QI479F_K","QI479F_L","QI479F_M","QI479F_X","QI479F_XI","QI479F_Y","QI479G","QI479G_A","QI479G_B","QI479G_C","QI479G_D","QI479G_E","QI479G_F","QI479G_G","QI479G_H","QI479G_I","QI479G_J","QI479G_X","QI479G_XI","QI479G_Y","QI479G_CONS","QI479GB_A", "QI479GB_B", "QI479GB_C", "QI479GB_D", "QI479GB_E", "QI479GB_F", "QI479GB_G", "QI479GB_H", "QI479GB_I", "QI479GB_J", "QI479GB_X", "QI479GC_A", "QI479GC_B", "QI479GC_C", "QI479GC_D", "QI479GC_E", "QI479GC_F", "QI479GC_G", "QI479GC_H", "QI479GC_I", "QI479GC_J", "QI479GC_X", "ID", "HOGAR_ID", "PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI479A","QI479B","QI479C","QI479D","QI479E_A","QI479E_B","QI479E_C","QI479E_D","QI479E_E","QI479E_F","QI479E_G","QI479E_H","QI479E_I","QI479E_J","QI479E_X","QI479E_XI","QI479E_Y","QI479F_A","QI479F_B","QI479F_C","QI479F_D","QI479F_E","QI479F_F","QI479F_G","QI479F_H","QI479F_I","QI479F_J","QI479F_K","QI479F_L","QI479F_M","QI479F_X","QI479F_XI","QI479F_Y","QI479G","QI479G_A","QI479G_B","QI479G_C","QI479G_D","QI479G_E","QI479G_F","QI479G_G","QI479G_H","QI479G_I","QI479G_J","QI479G_X","QI479G_XI","QI479G_Y","QI479G_CONS","QI479GB_A", "QI479GB_B", "QI479GB_C", "QI479GB_D", "QI479GB_E", "QI479GB_F", "QI479GB_G", "QI479GB_H", "QI479GB_I", "QI479GB_J", "QI479GB_X", "QI479GC_A", "QI479GC_B", "QI479GC_C", "QI479GC_D", "QI479GC_E", "QI479GC_F", "QI479GC_G", "QI479GC_H", "QI479GC_I", "QI479GC_J", "QI479GC_X")}; 
		
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.ciseccion_04b2titulo).textSize(21).centrar().negrita();
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		lblpregunta479a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479a).textSize(19);
		lblpregunta479a1 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479a1).negrita().textSize(16);
		lblpregunta479a2 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta479b = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479b).textSize(19);
		lblpregunta479c = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479c).textSize(19);
		lblpregunta479d = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479d).textSize(19);
		
		lblpregunta479e = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi479e).textSize(19);
	  	lblpregunta479e_ind = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi479e_ind).textSize(17);
	  	lblpregunta479f = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi479f).textSize(19);
	  	lblpregunta479f_ind = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.c2seccion_04b_2qi479f_ind).textSize(17);
	  
	  	lblpregunta479G = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479g).textSize(19);
    	lblpregunta479Gaa = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text("").textSize(19);
    	//Spanned texto479gaa =Html.fromHtml("<b>SI DICE 'SI' PREGUNTE</b><br>a. �La persona que le visit� a que instituci�n pertenece?" );
    	Spanned texto479gaa =Html.fromHtml("a. �La persona que le visit� a que instituci�n pertenece?" );
    	lblpregunta479Gaa.setText(texto479gaa);   
    	lblpregunta479Gbb= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479g_bb).textSize(19);
    	lblpregunta479Gcc= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b_2qi479g_cc).textSize(19);
    	
		rgQI479A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479a_1,R.string.c2seccion_04b_2qi479a_2,R.string.c2seccion_04b_2qi479a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi479AChangeValue"); 
		txtQI479B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		rgQI479C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479c_1,R.string.c2seccion_04b_2qi479c_2,R.string.c2seccion_04b_2qi479c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi479CChangeValue"); 
		txtQI479D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
		
		
		chbQI479E_A=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_a,"1:0").size(WRAP_CONTENT, 200); 
		chbQI479E_B=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_b,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_C=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_c,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_D=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_d,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_E=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_e,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_F=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_f,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_G=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_g,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_H=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_h,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_I=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_i,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_J=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_j,"1:0").size(WRAP_CONTENT, 200);
		chbQI479E_X=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI479E_XIChangeValue");
		txtQI479E_XI = new TextField(getActivity()).size(altoComponente, 550).maxLength(100);
		chbQI479E_Y=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479e_y,"1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		
		chbQI479F_A=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_a,"1:0").size(WRAP_CONTENT, 200); 
		chbQI479F_B=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_b,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_C=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_c,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_D=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_d,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_E=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_e,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_F=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_f,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_G=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_g,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_H=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_h,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_I=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_i,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_J=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_j,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_K=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_k,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_L=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_l,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_M=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_m,"1:0").size(WRAP_CONTENT, 200);
		chbQI479F_X=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI479F_XIChangeValue");
		txtQI479F_XI = new TextField(getActivity()).size(altoComponente, 550).maxLength(100);
		chbQI479F_Y=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479f_y,"1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		
		rgQI479G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi480a_1,R.string.c2seccion_04b_2qi480a_2,R.string.c2seccion_04b_2qi480a_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQI479GChangeValue").centrar();
		chbQI479G_A=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_a,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_AChangeValue"); 
		chbQI479G_B=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_b,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_BChangeValue");
		chbQI479G_C=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_c,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_CChangeValue");
		chbQI479G_D=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_d,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_DChangeValue");
		chbQI479G_E=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_e,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_EChangeValue");
		chbQI479G_F=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_f,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_FChangeValue");
		chbQI479G_G=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_g,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_GChangeValue");
		chbQI479G_H=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_h,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_HChangeValue");
		chbQI479G_I=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_i,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_IChangeValue");
		chbQI479G_J=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_j,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_JChangeValue");
		chbQI479G_X=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_x,"1:0").size(WRAP_CONTENT, 200).callback("onQI479G_XChangeValue");
		txtQI479G_XI = new TextField(getActivity()).size(altoComponente, 550).maxLength(100);
		chbQI479G_Y=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479g_y,"1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQI479G_YChangeValue");
		
		txtQI479GB_A= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_B= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_C= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_D= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_E= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_F= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_G= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_H= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_I= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_J= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		txtQI479GB_X= new IntegerField(this.getActivity()).maxLength(2).size(altoComponente, 80);
		
		chb479gb_a=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_b=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_c=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_d=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_e=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_f=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_g=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_h=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_i=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_j=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		chb479gb_x=new CheckBoxField(this.getActivity(),R.string.c2seccion_04b_2qi479gc,"1:0").size(altoComponente, 100);
		
		rgQI479GC_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_H=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_J=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI479GC_X=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi479gd_a1,R.string.c2seccion_04b_2qi479gd_a2,R.string.c2seccion_04b_2qi479gd_a3).size(altoComponente,300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		
		lblpregunta479gba= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_a).textSize(16);
		lblpregunta479gbb= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_b).textSize(16);
		lblpregunta479gbc= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_c).textSize(16);
		lblpregunta479gbd= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_d).textSize(16);
		lblpregunta479gbe= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_e).textSize(16);
		lblpregunta479gbf= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_f).textSize(16);
		lblpregunta479gbg= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_g).textSize(16);
		lblpregunta479gbh= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_h).textSize(16);
		lblpregunta479gbi= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_i).textSize(16);
		lblpregunta479gbj= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_j).textSize(16);
		lblpregunta479gbx= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_x).textSize(16);
		
		lblpregunta479gca= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_a).textSize(16);
		lblpregunta479gcb= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_b).textSize(16);
		lblpregunta479gcc= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_c).textSize(16);
		lblpregunta479gcd= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_d).textSize(16);
		lblpregunta479gce= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_e).textSize(16);
		lblpregunta479gcf= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_f).textSize(16);
		lblpregunta479gcg= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_g).textSize(16);
		lblpregunta479gch= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_h).textSize(16);
		lblpregunta479gci= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_i).textSize(16);
		lblpregunta479gcj= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_j).textSize(16);
		lblpregunta479gcx= new LabelComponent(getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04b_2qi479gc_x).textSize(16);
		
		gridpregunta479E = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta479E.addComponent(chbQI479E_A,2);
		gridpregunta479E.addComponent(chbQI479E_B,2);
		gridpregunta479E.addComponent(chbQI479E_C,2);
		gridpregunta479E.addComponent(chbQI479E_D,2);
		gridpregunta479E.addComponent(chbQI479E_E,2);
		gridpregunta479E.addComponent(chbQI479E_F,2);
		gridpregunta479E.addComponent(chbQI479E_G,2);
		gridpregunta479E.addComponent(chbQI479E_H,2);
		gridpregunta479E.addComponent(chbQI479E_I,2);
		gridpregunta479E.addComponent(chbQI479E_J,2);
		gridpregunta479E.addComponent(chbQI479E_X);
		gridpregunta479E.addComponent(txtQI479E_XI);
		gridpregunta479E.addComponent(chbQI479E_Y,2);
		
		gridpregunta479F = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta479F.addComponent(chbQI479F_A,2);
		gridpregunta479F.addComponent(chbQI479F_B,2);
		gridpregunta479F.addComponent(chbQI479F_C,2);
		gridpregunta479F.addComponent(chbQI479F_D,2);
		gridpregunta479F.addComponent(chbQI479F_E,2);
		gridpregunta479F.addComponent(chbQI479F_F,2);
		gridpregunta479F.addComponent(chbQI479F_G,2);
		gridpregunta479F.addComponent(chbQI479F_H,2);
		gridpregunta479F.addComponent(chbQI479F_I,2);
		gridpregunta479F.addComponent(chbQI479F_J,2);
		gridpregunta479F.addComponent(chbQI479F_K,2);
		gridpregunta479F.addComponent(chbQI479F_L,2);
		gridpregunta479F.addComponent(chbQI479F_M,2);
		gridpregunta479F.addComponent(chbQI479F_X);
		gridpregunta479F.addComponent(txtQI479F_XI);
		gridpregunta479F.addComponent(chbQI479F_Y,2);
		
		gridpregunta479G = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta479G.addComponent(chbQI479G_A,2);
		gridpregunta479G.addComponent(chbQI479G_B,2);
		gridpregunta479G.addComponent(chbQI479G_C,2);
		gridpregunta479G.addComponent(chbQI479G_D,2);
		gridpregunta479G.addComponent(chbQI479G_E,2);
		gridpregunta479G.addComponent(chbQI479G_F,2);
		gridpregunta479G.addComponent(chbQI479G_G,2);
		gridpregunta479G.addComponent(chbQI479G_H,2);
		gridpregunta479G.addComponent(chbQI479G_I,2);
		gridpregunta479G.addComponent(chbQI479G_J,2);
		gridpregunta479G.addComponent(chbQI479G_X);
		gridpregunta479G.addComponent(txtQI479G_XI);
		gridpregunta479G.addComponent(chbQI479G_Y,2);
		
		gridpregunta479Gsubc=new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridpregunta479Gsubc.addComponent(lblpregunta479gba);
		gridpregunta479Gsubc.addComponent(txtQI479GB_A);
		gridpregunta479Gsubc.addComponent(chb479gb_a);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbb);
		gridpregunta479Gsubc.addComponent(txtQI479GB_B);
		gridpregunta479Gsubc.addComponent(chb479gb_b);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbc);
		gridpregunta479Gsubc.addComponent(txtQI479GB_C);
		gridpregunta479Gsubc.addComponent(chb479gb_c);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbd);
		gridpregunta479Gsubc.addComponent(txtQI479GB_D);
		gridpregunta479Gsubc.addComponent(chb479gb_d);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbe);
		gridpregunta479Gsubc.addComponent(txtQI479GB_E);
		gridpregunta479Gsubc.addComponent(chb479gb_e);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbf);
		gridpregunta479Gsubc.addComponent(txtQI479GB_F);
		gridpregunta479Gsubc.addComponent(chb479gb_f);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbg);
		gridpregunta479Gsubc.addComponent(txtQI479GB_G);
		gridpregunta479Gsubc.addComponent(chb479gb_g);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbh);
		gridpregunta479Gsubc.addComponent(txtQI479GB_H);
		gridpregunta479Gsubc.addComponent(chb479gb_h);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbi);
		gridpregunta479Gsubc.addComponent(txtQI479GB_I);
		gridpregunta479Gsubc.addComponent(chb479gb_i);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbj);
		gridpregunta479Gsubc.addComponent(txtQI479GB_J);
		gridpregunta479Gsubc.addComponent(chb479gb_j);
		gridpregunta479Gsubc.addComponent(lblpregunta479gbx);
		gridpregunta479Gsubc.addComponent(txtQI479GB_X);
		gridpregunta479Gsubc.addComponent(chb479gb_x);
		
		gridpregunta479GD=new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridpregunta479GD.addComponent(lblpregunta479gca);
		gridpregunta479GD.addComponent(rgQI479GC_A);
		gridpregunta479GD.addComponent(lblpregunta479gcb);
		gridpregunta479GD.addComponent(rgQI479GC_B);
		gridpregunta479GD.addComponent(lblpregunta479gcc);
		gridpregunta479GD.addComponent(rgQI479GC_C);
		gridpregunta479GD.addComponent(lblpregunta479gcd);
		gridpregunta479GD.addComponent(rgQI479GC_D);
		gridpregunta479GD.addComponent(lblpregunta479gce);
		gridpregunta479GD.addComponent(rgQI479GC_E);
		gridpregunta479GD.addComponent(lblpregunta479gcf);
		gridpregunta479GD.addComponent(rgQI479GC_F);
		gridpregunta479GD.addComponent(lblpregunta479gcg);
		gridpregunta479GD.addComponent(rgQI479GC_G);
		gridpregunta479GD.addComponent(lblpregunta479gch);
		gridpregunta479GD.addComponent(rgQI479GC_H);
		gridpregunta479GD.addComponent(lblpregunta479gci);
		gridpregunta479GD.addComponent(rgQI479GC_I);
		gridpregunta479GD.addComponent(lblpregunta479gcj);
		gridpregunta479GD.addComponent(rgQI479GC_J);
		gridpregunta479GD.addComponent(lblpregunta479gcx);
		gridpregunta479GD.addComponent(rgQI479GC_X);
		

	    chb479gb_a.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_a.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_A);}
				else {Util.lockView(getActivity(),false, txtQI479GB_A);}
			}
		});	    
	    chb479gb_b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_b.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_B);}
				else {Util.lockView(getActivity(),false, txtQI479GB_B);}
			}
		});
	    chb479gb_c.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_c.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_C);}
				else {Util.lockView(getActivity(),false, txtQI479GB_C);}
			}
		});
	    chb479gb_d.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_d.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_D);}
				else {Util.lockView(getActivity(),false, txtQI479GB_D);}
			}
		});
	    chb479gb_e.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_e.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_E);}
				else {Util.lockView(getActivity(),false, txtQI479GB_E);}
			}
		});
	    chb479gb_f.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_f.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_F);}
				else {Util.lockView(getActivity(),false, txtQI479GB_F);}
			}
		});
	    chb479gb_g.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_g.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_G);}
				else {Util.lockView(getActivity(),false, txtQI479GB_G);}
			}
		});
	    chb479gb_h.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_h.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_H);}
				else {Util.lockView(getActivity(),false, txtQI479GB_H);}
			}
		});
	    chb479gb_i.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_i.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_I);}
				else {Util.lockView(getActivity(),false, txtQI479GB_I);}
			}
		});
	    chb479gb_j.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_j.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_J);}
				else {Util.lockView(getActivity(),false, txtQI479GB_J);}
			}
		});
	    chb479gb_x.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb479gb_x.isChecked()) {Util.cleanAndLockView(getActivity(), txtQI479GB_X);}
				else {Util.lockView(getActivity(),false, txtQI479GB_X);}
			}
		});
	    
	    txtQI479G_XI.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
					ontxtQI212_NOMChangeValue();
				}
				else{ 
					resetearLabels();
				}
			}
		});
		
    } 
  
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,lblpregunta479a,lblpregunta479a1,lblpregunta479a2,rgQI479A); 
		q2 = createQuestionSection(0,lblpregunta479b,txtQI479B); 
		q3 = createQuestionSection(0,lblpregunta479c,rgQI479C); 
		q4 = createQuestionSection(0,lblpregunta479d,txtQI479D); 
		
		q5 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta479e,lblpregunta479e_ind,gridpregunta479E.component());
		q6 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta479f,lblpregunta479f_ind,gridpregunta479F.component());
		q7 =  createQuestionSection(lblpregunta479G,rgQI479G);
		q8 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta479Gaa,gridpregunta479G.component());
		q9 =  createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta479Gbb,gridpregunta479Gsubc.component(),lblpregunta479Gcc,gridpregunta479GD.component());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
    return contenedor; 
    } 
    
    
    @Override 
    public boolean grabar() { 
		uiToEntity(Individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			cambiarDatosparalaBD();
			Log.e("grabar ",""+Individual.qi479gb_a);
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(App.getInstance().getSeccion04B2()!=null) 
			App.getInstance().getSeccion04B2().qi479a = Individual.qi479a;
		else 
			App.getInstance().setSeccion04B2(Individual);
		
		
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(flagpase){
			if (Util.esVacio(Individual.qi479a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI479A"); 
				view = rgQI479A; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(Individual.qi479a,1)) {
				if (Util.esVacio(Individual.qi479b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI479B"); 
					view = txtQI479B; 
					error = true; 
					return false; 
				}
				if (Util.esMenor(Individual.qi479b,1) || Util.esMayor(Individual.qi479b, 9)) { 
					mensaje = "Valor fuera de rango pregunta 479B"; 
					view = txtQI479B; 
					error = true; 
					return false; 
				}
				
			}
			if (Util.esVacio(Individual.qi479c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI479C"); 
				view = rgQI479C; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(Individual.qi479c,1)) {
				if (Util.esVacio(Individual.qi479d)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI479D"); 
					view = txtQI479D; 
					error = true; 
					return false; 
				} 
				if (Util.esMenor(Individual.qi479d,1) || Util.esMayor(Individual.qi479d, 9)) { 
					mensaje = "Valor fuera de rango pregunta 479D"; 
					view = txtQI479D; 
					error = true; 
					return false; 
				}
			}
			
			
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi479e_a,Individual.qi479e_b,Individual.qi479e_c,Individual.qi479e_d,Individual.qi479e_e,Individual.qi479e_f,Individual.qi479e_g,Individual.qi479e_h,Individual.qi479e_i,Individual.qi479e_j,Individual.qi479e_x,Individual.qi479e_y)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI479E_A; 
				error = true; 
				return false; 
			}		
			if (!Util.esDiferente(Individual.qi479e_x,1)) {
				if (Util.esVacio(Individual.qi479e_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI479E_XI"); 
					view = txtQI479E_XI; 
					error = true; 
					return false; 
				} 
			}
			
			if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi479f_a,Individual.qi479f_b,Individual.qi479f_c,Individual.qi479f_d,Individual.qi479f_e,Individual.qi479f_f,Individual.qi479f_g,Individual.qi479f_h,Individual.qi479f_i,Individual.qi479f_j,Individual.qi479f_k,Individual.qi479f_l,Individual.qi479f_m,Individual.qi479f_x,Individual.qi479f_y)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI479F_A; 
				error = true; 
				return false; 
			}		
			if (!Util.esDiferente(Individual.qi479f_x,1)) {
				if (Util.esVacio(Individual.qi479f_xi)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI479F_XI"); 
					view = txtQI479F_XI; 
					error = true; 
					return false; 
				} 
			}
			
			if (Util.esVacio(Individual.qi479g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI478G"); 
				view = rgQI479G; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(Individual.qi479g,1)) {
				if (!Util.alMenosUnoEsDiferenteA(0,Individual.qi479g_a,Individual.qi479g_b,Individual.qi479g_c,Individual.qi479g_d,Individual.qi479g_e,Individual.qi479g_f,Individual.qi479g_g,Individual.qi479g_h,Individual.qi479g_i,Individual.qi479g_j,Individual.qi479g_x,Individual.qi479g_y)) { 
					mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
					view = chbQI479G_A; 
					error = true; 
					return false; 
				}		
				if (!Util.esDiferente(Individual.qi479g_x,1)) {
					if (Util.esVacio(Individual.qi479g_xi)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI479G_XI"); 
						view = txtQI479G_XI; 
						error = true; 
						return false; 
					} 
				}
				
				if(!Util.esDiferente(Individual.qi479g_a, 1)){
					if(Util.esVacio(Individual.qi479gb_a) && !chb479gb_a.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB_A"); 
						view = txtQI479GB_A; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_a,1) || Util.esMayor(Individual.qi479gb_a,7)) && !chb479gb_a.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_A; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_a)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC_A"); 
						view = rgQI479GC_A; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_b, 1)){
					if(Util.esVacio(Individual.qi479gb_b) && !chb479gb_b.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB B"); 
						view = txtQI479GB_B; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479g_b,1) || Util.esMayor(Individual.qi479g_b,7)) && !chb479gb_b.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_B; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_b)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC B"); 
						view = rgQI479GC_B; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_c, 1)){
					if(Util.esVacio(Individual.qi479gb_c) && !chb479gb_c.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB C"); 
						view = txtQI479GB_C; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_c,1) || Util.esMayor(Individual.qi479gb_c,7)) && !chb479gb_c.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_C; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_c)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC C"); 
						view = rgQI479GC_C; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_d, 1)){
					if(Util.esVacio(Individual.qi479gb_d) && !chb479gb_d.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB D"); 
						view = txtQI479GB_D; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_d,1) || Util.esMayor(Individual.qi479gb_d,7)) && !chb479gb_d.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_D; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_d)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC D"); 
						view = rgQI479GC_D; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_e, 1)){
					if(Util.esVacio(Individual.qi479gb_e) && !chb479gb_e.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB E"); 
						view = txtQI479GB_E; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_e,1) || Util.esMayor(Individual.qi479gb_e,7)) && !chb479gb_e.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_E; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_e)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC E"); 
						view = rgQI479GC_E; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_f, 1)){
					if(Util.esVacio(Individual.qi479gb_f) && !chb479gb_f.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB F"); 
						view = txtQI479GB_F; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_f,1) || Util.esMayor(Individual.qi479gb_f,7)) && !chb479gb_f.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_F; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_f)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC F"); 
						view = rgQI479GC_F; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_g, 1)){
					if(Util.esVacio(Individual.qi479gb_g) && !chb479gb_g.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB G"); 
						view = txtQI479GB_G; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_g,1) || Util.esMayor(Individual.qi479gb_g,7)) && !chb479gb_g.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_G; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_g)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC G"); 
						view = rgQI479GC_G; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_h, 1)){
					if(Util.esVacio(Individual.qi479gb_h) && !chb479gb_h.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB H"); 
						view = txtQI479GB_H; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_h,1) || Util.esMayor(Individual.qi479gb_h,7)) && !chb479gb_h.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_H; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_h)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC H"); 
						view = rgQI479GC_H; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_i, 1)){
					if(Util.esVacio(Individual.qi479gb_i) && !chb479gb_i.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB I"); 
						view = txtQI479GB_I; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_i,1) || Util.esMayor(Individual.qi479gb_i,7)) && !chb479gb_i.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_I; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_i)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC I"); 
						view = rgQI479GC_I; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_j, 1)){
					if(Util.esVacio(Individual.qi479gb_j) && !chb479gb_j.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB J"); 
						view = txtQI479GB_J; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_j,1) || Util.esMayor(Individual.qi479gb_j,7)) && !chb479gb_j.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_J; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_j)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC J"); 
						view = rgQI479GC_J; 
						error = true; 
						return false; 
					}
				}
				if(!Util.esDiferente(Individual.qi479g_x, 1)){
					if(Util.esVacio(Individual.qi479gb_x) && !chb479gb_x.isChecked()){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GB X"); 
						view = txtQI479GB_X; 
						error = true; 
						return false; 
					}
					if ((Util.esMenor(Individual.qi479gb_x,1) || Util.esMayor(Individual.qi479gb_x,7)) && !chb479gb_x.isChecked() ) { 
						mensaje = "Valor fuera de rango pregunta QI479GB_A"; 
						view = txtQI479GB_X; 
						error = true; 
						return false; 
					}
					if(Util.esVacio(Individual.qi479gc_x)){
						mensaje = preguntaVacia.replace("$", "La pregunta QI479GC X"); 
						view = rgQI479GC_X; 
						error = true; 
						return false; 
					}
				}
			}
		}
		return true; 
    }
       
    
    @Override 
    public void cargarDatos() { 
    	Individual = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	App.getInstance().getSeccion04B2().filtro479=getCuestionarioService().ExisteninioMenoraTresAniosyVivo(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	parent.setEsAvanceDual(false);
		if(Individual==null){ 
			Individual=new CISECCION_04B2(); 
			Individual.id=App.getInstance().getPersonaCuestionarioIndividual().id; 
			Individual.hogar_id=App.getInstance().getPersonaCuestionarioIndividual().hogar_id; 
			Individual.persona_id=App.getInstance().getPersonaCuestionarioIndividual().persona_id; 
	    } 
		recuperarDatosParaMostrar();    	
    	fecharef_479g=Individual.qi479g_cons;    	
		entityToUI(Individual);  
     	if(Individual.qi479gb_a!=null && Individual.qi479gb_a==8) {	chb479gb_a.setChecked(true); txtQI479GB_A.setText(""); }
    	if(Individual.qi479gb_b!=null && Individual.qi479gb_b==8) {	chb479gb_b.setChecked(true); txtQI479GB_B.setText(""); }
    	if(Individual.qi479gb_c!=null && Individual.qi479gb_c==8) {	chb479gb_c.setChecked(true); txtQI479GB_C.setText(""); }
    	if(Individual.qi479gb_d!=null && Individual.qi479gb_d==8) {	chb479gb_d.setChecked(true); txtQI479GB_D.setText(""); }
    	if(Individual.qi479gb_e!=null && Individual.qi479gb_e==8) {	chb479gb_e.setChecked(true); txtQI479GB_E.setText(""); }
    	if(Individual.qi479gb_f!=null && Individual.qi479gb_f==8) {	chb479gb_f.setChecked(true); txtQI479GB_G.setText(""); }
    	if(Individual.qi479gb_g!=null && Individual.qi479gb_g==8) {	chb479gb_g.setChecked(true); txtQI479GB_H.setText(""); }
    	if(Individual.qi479gb_h!=null && Individual.qi479gb_h==8) {	chb479gb_h.setChecked(true); txtQI479GB_H.setText(""); }
    	if(Individual.qi479gb_i!=null && Individual.qi479gb_i==8) {	chb479gb_i.setChecked(true); txtQI479GB_I.setText(""); }
    	if(Individual.qi479gb_j!=null && Individual.qi479gb_j==8) {	chb479gb_j.setChecked(true); txtQI479GB_J.setText(""); }
    	if(Individual.qi479gb_x!=null && Individual.qi479gb_x==8) {	chb479gb_x.setChecked(true); txtQI479GB_X.setText(""); }
		inicio(); 
    } 
    
    
    private void inicio() {
    	verificarfiltro479();
    	onqi479AChangeValue();
    	lblpregunta479a2.setText(Html.fromHtml("<b>Sesi&oacute;n demostrativa</b> es una reuni&oacute;n donde el personal del <b>Ministerio de Salud</b>, ense�a y prepara con las mam&aacute;s, papilla y comidas nutritivas para sus ni�as y ni�os menores de 3 a�os"));
    	onqi479CChangeValue();
    	ValidarsiesSupervisora();
    	
    	onQI479E_XIChangeValue();
    	onQI479F_XIChangeValue();
    	onQI479GChangeValue();
    	RenombrarEtiquetas();
    	txtCabecera.requestFocus();
    	
    } 
    public void RenombrarEtiquetas(){
    	lblpregunta479Gbb.text(R.string.c2seccion_04b_2qi479g_bb);
    	Calendar fecharef = new GregorianCalendar();
    	Calendar fechainicio2 = new GregorianCalendar();
    	if(fecharef_479g!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fecharef_479g);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal.setTime(date2);
			cal2.setTime(date2);
			fecharef=cal;
			fechainicio2=cal2;
		}
    
    	Calendar fechainicio = MyUtil.restarDias(fecharef,30);
    	Integer f1 =fechainicio.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio.get(Calendar.MONTH)) ;
    	lblpregunta479G.setText(lblpregunta479G.getText().toString().replace("#", f1.toString()+" de "+mes1));
    	lblpregunta479Gbb.setText(lblpregunta479Gbb.getText().toString().replace("#", f1.toString()+" de "+mes1));
    	
    	Calendar fechainicio3 = MyUtil.restarDias(fechainicio2,7);
    	lblpregunta479Gcc.text(R.string.c2seccion_04b_2qi479g_cc);
    	lblpregunta479Gcc.setText(lblpregunta479Gcc.getText().toString().replace("#",MyUtil.DiaDelaSemana(fechainicio3.get(Calendar.DAY_OF_WEEK))));
    }
    
    public void cambiarDatosparalaBD(){
    	Individual.qi479a=Individual.qi479a!=null?Individual.setConvertcontresvariables(Individual.qi479a):null;
    	Individual.qi479c=Individual.qi479c!=null?Individual.setConvertcontresvariables(Individual.qi479c):null;
    	Individual.qi479g=Individual.qi479g!=null?Individual.setConvertcontresvariables(Individual.qi479g):null;
    	
    	if(Individual.qi479g_cons==null){
			Individual.qi479g_cons=Util.getFechaActualToString();
		}		
		if(chb479gb_a.isChecked() ){ Individual.qi479gb_a=8; }
		if(chb479gb_b.isChecked() ){ Individual.qi479gb_b=8; }
		if(chb479gb_c.isChecked() ){ Individual.qi479gb_c=8; }
		if(chb479gb_d.isChecked() ){ Individual.qi479gb_d=8; }
		if(chb479gb_e.isChecked() ){ Individual.qi479gb_e=8; }
		if(chb479gb_f.isChecked() ){ Individual.qi479gb_f=8; }
		if(chb479gb_g.isChecked() ){ Individual.qi479gb_g=8; }
		if(chb479gb_h.isChecked() ){ Individual.qi479gb_h=8; }
		if(chb479gb_i.isChecked() ){ Individual.qi479gb_i=8; }
		if(chb479gb_j.isChecked() ){ Individual.qi479gb_j=8; }
		if(chb479gb_x.isChecked() ){ Individual.qi479gb_x=8; }
    }
    
    public void recuperarDatosParaMostrar(){
    	Individual.qi479a= Individual.qi479a!=null?Individual.getConvertcontresvariables(Individual.qi479a):null;
    	Individual.qi479c= Individual.qi479c!=null?Individual.getConvertcontresvariables(Individual.qi479c):null;
    	Individual.qi479g= Individual.qi479g!=null?Individual.getConvertcontresvariables(Individual.qi479g):null;
    }
        
    public void verificarfiltro479(){
    	flagpase = getCuestionarioService().ExisteninioMenoraTresAniosyVivo(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	App.getInstance().getSeccion04B2().filtro479 =flagpase; 
    	if(!flagpase){
    		Util.cleanAndLockView(getActivity(), rgQI479A,rgQI479C);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(),false, rgQI479A,rgQI479C);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    	}
    }
    
    
    public void onQI479GB_ANChangeValue(){
    	if(chb479gb_a.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_A);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_A);
    	}
    }
    public void onQI479GB_BNChangeValue(){
    	if(chb479gb_b.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_B);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_B);
    	}
    }
    public void onQI479GB_CNChangeValue(){
    	if(chb479gb_c.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_C);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_C);
    	}
    }
    public void onQI479GB_DNChangeValue(){
    	if(chb479gb_d.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_D);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_D);
    	}
    }
    public void onQI479GB_ENChangeValue(){
    	if(chb479gb_e.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_E);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_E);
    	}
    }
    public void onQI479GB_FNChangeValue(){
    	if(chb479gb_f.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_F);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_F);
    	}
    }
    public void onQI479GB_GNChangeValue(){
    	if(chb479gb_g.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_G);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_G);
    	}
    }
    public void onQI479GB_HNChangeValue(){
    	if(chb479gb_h.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_H);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_H);
    	}
    }
    public void onQI479GB_INChangeValue(){
    	if(chb479gb_i.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_I);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_I);
    	}
    }
    public void onQI479GB_JNChangeValue(){
    	if(chb479gb_j.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_J);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_J);
    	}
    }
    public void onQI479GB_XNChangeValue(){
    	if(chb479gb_x.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI479GB_X);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI479GB_X);
    	}
    }
    
	public void onQI479G_AChangeValue(){
		boolean value=chbQI479G_A.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_A, rgQI479GC_A);			
			lblpregunta479gba.setVisibility(View.VISIBLE);
			txtQI479GB_A.setVisibility(View.VISIBLE);
			chb479gb_a.setVisibility(View.VISIBLE);
			onQI479GB_ANChangeValue();
			lblpregunta479gca.setVisibility(View.VISIBLE);
			rgQI479GC_A.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_A, rgQI479GC_A);
			chb479gb_a.setChecked(false);
			lblpregunta479gba.setVisibility(View.GONE);
			txtQI479GB_A.setVisibility(View.GONE);
			chb479gb_a.setVisibility(View.GONE);

			lblpregunta479gca.setVisibility(View.GONE);
			rgQI479GC_A.setVisibility(View.GONE);
		}
	 }
	 public void onQI479G_BChangeValue(){
		boolean value=chbQI479G_B.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_B, rgQI479GC_B);
			lblpregunta479gbb.setVisibility(View.VISIBLE);
			txtQI479GB_B.setVisibility(View.VISIBLE);
			chb479gb_b.setVisibility(View.VISIBLE);
			onQI479GB_BNChangeValue();
			lblpregunta479gcb.setVisibility(View.VISIBLE);
			rgQI479GC_B.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_B, rgQI479GC_B);
			chb479gb_b.setChecked(false);
			lblpregunta479gbb.setVisibility(View.GONE);
			txtQI479GB_B.setVisibility(View.GONE);
			chb479gb_b.setVisibility(View.GONE);

			lblpregunta479gcb.setVisibility(View.GONE);
			rgQI479GC_B.setVisibility(View.GONE);
		}
	 }
	 public void onQI479G_CChangeValue(){
			boolean value=chbQI479G_C.isChecked();
			if(value==true){
				Util.lockView(getActivity(), false, txtQI479GB_C, rgQI479GC_C);
				lblpregunta479gbc.setVisibility(View.VISIBLE);
				txtQI479GB_C.setVisibility(View.VISIBLE);
				chb479gb_c.setVisibility(View.VISIBLE);
				onQI479GB_CNChangeValue();
				lblpregunta479gcc.setVisibility(View.VISIBLE);
				rgQI479GC_C.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(), txtQI479GB_C, rgQI479GC_C);
				chb479gb_c.setChecked(false);
				lblpregunta479gbc.setVisibility(View.GONE);
				txtQI479GB_C.setVisibility(View.GONE);
				chb479gb_c.setVisibility(View.GONE);

				lblpregunta479gcc.setVisibility(View.GONE);
				rgQI479GC_C.setVisibility(View.GONE);
			}
	}
	 public void onQI479G_DChangeValue(){
			boolean value=chbQI479G_D.isChecked();
			if(value==true){
				Util.lockView(getActivity(), false, txtQI479GB_D, rgQI479GC_D);
				lblpregunta479gbd.setVisibility(View.VISIBLE);
				txtQI479GB_D.setVisibility(View.VISIBLE);
				chb479gb_d.setVisibility(View.VISIBLE);
				onQI479GB_DNChangeValue();
				lblpregunta479gcd.setVisibility(View.VISIBLE);
				rgQI479GC_D.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(), txtQI479GB_D, rgQI479GC_D);
				chb479gb_d.setChecked(false);
				lblpregunta479gbd.setVisibility(View.GONE);
				txtQI479GB_D.setVisibility(View.GONE);
				chb479gb_d.setVisibility(View.GONE);

				lblpregunta479gcd.setVisibility(View.GONE);
				rgQI479GC_D.setVisibility(View.GONE);
			}
	}
	 public void onQI479G_EChangeValue(){
			boolean value=chbQI479G_E.isChecked();
			if(value==true){
				Util.lockView(getActivity(), false, txtQI479GB_E, rgQI479GC_E);
				lblpregunta479gbe.setVisibility(View.VISIBLE);
				txtQI479GB_E.setVisibility(View.VISIBLE);
				chb479gb_e.setVisibility(View.VISIBLE);
				onQI479GB_ENChangeValue();
				lblpregunta479gce.setVisibility(View.VISIBLE);
				rgQI479GC_E.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(), txtQI479GB_E, rgQI479GC_E);
				chb479gb_e.setChecked(false);
				lblpregunta479gbe.setVisibility(View.GONE);
				txtQI479GB_E.setVisibility(View.GONE);
				chb479gb_e.setVisibility(View.GONE);

				lblpregunta479gce.setVisibility(View.GONE);
				rgQI479GC_E.setVisibility(View.GONE);
			}
	}
	public void onQI479G_FChangeValue(){
			boolean value=chbQI479G_F.isChecked();
			if(value==true){
				Util.lockView(getActivity(), false, txtQI479GB_F, rgQI479GC_F);
				lblpregunta479gbf.setVisibility(View.VISIBLE);
				txtQI479GB_F.setVisibility(View.VISIBLE);
				chb479gb_f.setVisibility(View.VISIBLE);
				onQI479GB_FNChangeValue();
				lblpregunta479gcf.setVisibility(View.VISIBLE);
				rgQI479GC_F.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(), txtQI479GB_F, rgQI479GC_F);
				chb479gb_f.setChecked(false);
				lblpregunta479gbf.setVisibility(View.GONE);
				txtQI479GB_F.setVisibility(View.GONE);
				chb479gb_f.setVisibility(View.GONE);

				lblpregunta479gcf.setVisibility(View.GONE);
				rgQI479GC_F.setVisibility(View.GONE);
			}
	}
	public void onQI479G_GChangeValue(){
		boolean value=chbQI479G_G.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_G, rgQI479GC_G);
			lblpregunta479gbg.setVisibility(View.VISIBLE);
			txtQI479GB_G.setVisibility(View.VISIBLE);
			chb479gb_g.setVisibility(View.VISIBLE);
			onQI479GB_GNChangeValue();
			lblpregunta479gcg.setVisibility(View.VISIBLE);
			rgQI479GC_G.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_G, rgQI479GC_G);
			chb479gb_g.setChecked(false);
			lblpregunta479gbg.setVisibility(View.GONE);
			txtQI479GB_G.setVisibility(View.GONE);
			chb479gb_g.setVisibility(View.GONE);

			lblpregunta479gcg.setVisibility(View.GONE);
			rgQI479GC_G.setVisibility(View.GONE);
		}
	}
	public void onQI479G_HChangeValue(){
		boolean value=chbQI479G_H.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_H, rgQI479GC_H);
			lblpregunta479gbh.setVisibility(View.VISIBLE);
			txtQI479GB_H.setVisibility(View.VISIBLE);
			chb479gb_h.setVisibility(View.VISIBLE);
			onQI479GB_HNChangeValue();
			lblpregunta479gch.setVisibility(View.VISIBLE);
			rgQI479GC_H.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_H, rgQI479GC_H);
			chb479gb_h.setChecked(false);
			lblpregunta479gbh.setVisibility(View.GONE);
			txtQI479GB_H.setVisibility(View.GONE);
			chb479gb_h.setVisibility(View.GONE);

			lblpregunta479gch.setVisibility(View.GONE);
			rgQI479GC_H.setVisibility(View.GONE);
		}
	}
	public void onQI479G_IChangeValue(){
		boolean value=chbQI479G_I.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_I, rgQI479GC_I);
			lblpregunta479gbi.setVisibility(View.VISIBLE);
			txtQI479GB_I.setVisibility(View.VISIBLE);
			chb479gb_i.setVisibility(View.VISIBLE);
			onQI479GB_INChangeValue();
			lblpregunta479gci.setVisibility(View.VISIBLE);
			rgQI479GC_I.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_I, rgQI479GC_I);
			chb479gb_i.setChecked(false);
			lblpregunta479gbi.setVisibility(View.GONE);
			txtQI479GB_I.setVisibility(View.GONE);
			chb479gb_i.setVisibility(View.GONE);

			lblpregunta479gci.setVisibility(View.GONE);
			rgQI479GC_I.setVisibility(View.GONE);
		}
	}
	public void onQI479G_JChangeValue(){
		boolean value=chbQI479G_J.isChecked();
		if(value==true){
			Util.lockView(getActivity(), false, txtQI479GB_J, rgQI479GC_J);
			lblpregunta479gbj.setVisibility(View.VISIBLE);
			txtQI479GB_J.setVisibility(View.VISIBLE);
			chb479gb_j.setVisibility(View.VISIBLE);
			onQI479GB_JNChangeValue();
			lblpregunta479gcj.setVisibility(View.VISIBLE);
			rgQI479GC_J.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI479GB_J, rgQI479GC_J);
			chb479gb_j.setChecked(false);
			lblpregunta479gbj.setVisibility(View.GONE);
			txtQI479GB_J.setVisibility(View.GONE);
			chb479gb_j.setVisibility(View.GONE);

			lblpregunta479gcj.setVisibility(View.GONE);
			rgQI479GC_J.setVisibility(View.GONE);
		}
	}
	
    public void onQI479G_XChangeValue(){
    	if(chbQI479G_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI479G_XI);
    		lblpregunta479gbx.setVisibility(View.VISIBLE);
			txtQI479GB_X.setVisibility(View.VISIBLE);
			chb479gb_x.setVisibility(View.VISIBLE);
			onQI479GB_XNChangeValue();
			lblpregunta479gcx.setVisibility(View.VISIBLE);
			rgQI479GC_X.setVisibility(View.VISIBLE);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI479G_XI);  
    		lblpregunta479gbx.setVisibility(View.GONE);
			txtQI479GB_X.setVisibility(View.GONE);
			chb479gb_x.setVisibility(View.GONE);
			lblpregunta479gcx.setVisibility(View.GONE);
			rgQI479GC_X.setVisibility(View.GONE);
    	}
    }
    
	public void onQI479G_YChangeValue(){
		if(chbQI479G_Y.isChecked()){
			Util.cleanAndLockView(getActivity(), 
					txtQI479GB_A, txtQI479GB_B, txtQI479GB_C, txtQI479GB_D, txtQI479GB_E, txtQI479GB_F, txtQI479GB_G, txtQI479GB_H, txtQI479GB_I, txtQI479GB_J, txtQI479GB_X,
					chb479gb_a, chb479gb_b, chb479gb_c, chb479gb_d, chb479gb_e, chb479gb_f, chb479gb_g, chb479gb_h, chb479gb_i, chb479gb_j, chb479gb_x, 
					rgQI479GC_A, rgQI479GC_B, rgQI479GC_C, rgQI479GC_D,rgQI479GC_E,rgQI479GC_F,rgQI479GC_G,rgQI479GC_H,rgQI479GC_I,rgQI479GC_J,rgQI479GC_X,
					chbQI479G_A, chbQI479G_B, chbQI479G_C, chbQI479G_D,chbQI479G_E,chbQI479G_F,chbQI479G_G,chbQI479G_H,chbQI479G_I,chbQI479G_J,chbQI479G_X,txtQI479G_XI);
			q9.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(),false,
					txtQI479GB_A, txtQI479GB_B, txtQI479GB_C, txtQI479GB_D, txtQI479GB_E, txtQI479GB_F, txtQI479GB_G, txtQI479GB_H, txtQI479GB_I, txtQI479GB_J, txtQI479GB_X,
					chb479gb_a, chb479gb_b, chb479gb_c, chb479gb_d, chb479gb_e, chb479gb_f, chb479gb_g, chb479gb_h, chb479gb_i, chb479gb_j, chb479gb_x, 
					rgQI479GC_A, rgQI479GC_B, rgQI479GC_C, rgQI479GC_D,rgQI479GC_E,rgQI479GC_F,rgQI479GC_G,rgQI479GC_H,rgQI479GC_I,rgQI479GC_J,rgQI479GC_X,
					chbQI479G_A, chbQI479G_B, chbQI479G_C, chbQI479G_D,chbQI479G_E,chbQI479G_F,chbQI479G_G,chbQI479G_H,chbQI479G_I,chbQI479G_J,chbQI479G_X);
			q9.setVisibility(View.VISIBLE);
    		onQI479G_AChangeValue();
    		onQI479G_BChangeValue();
    		onQI479G_CChangeValue();
    		onQI479G_DChangeValue();
    		onQI479G_EChangeValue();
    		onQI479G_FChangeValue();
    		onQI479G_GChangeValue();
    		onQI479G_HChangeValue();
    		onQI479G_IChangeValue();
    		onQI479G_JChangeValue();
    		onQI479G_XChangeValue();   		    	
    		chbQI479G_A.requestFocus();
		}
	}
	
	
	
	
    public void onqi479AChangeValue(){
    	Integer valor = Integer.parseInt(rgQI479A.getTagSelected("0").toString());
    	if(Util.esDiferente(valor, 0,1) || !flagpase){
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), txtQI479B);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		
    		Util.lockView(getActivity(),false, txtQI479B);
    		q2.setVisibility(View.VISIBLE);
    		txtQI479B.requestFocus();
    		
    	}
    }
    public void onqi479CChangeValue(){
    	Integer valor = Integer.parseInt(rgQI479C.getTagSelected("0").toString());
    	if(Util.esDiferente(valor, 0,1) || !flagpase){
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), txtQI479D);
    		q4.setVisibility(View.GONE);
    	}
    	else{
    		if(flagpase){
    		Util.lockView(getActivity(),false, txtQI479D);
    		q4.setVisibility(View.VISIBLE);
    		txtQI479D.requestFocus();
    		}
    	} 
    }
    
    public void onQI479E_XIChangeValue(){
    	if(chbQI479E_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI479E_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI479E_XI);    		
    	}
    }
    
    public void onQI479F_XIChangeValue(){
    	if(chbQI479F_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI479F_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI479F_XI);    		
    	}
    }
    
    public void onQI479GChangeValue(){
    	if (MyUtil.incluyeRango(2,8,rgQI479G.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),chbQI479G_A,chbQI479G_B,chbQI479G_C,chbQI479G_D,chbQI479G_E,chbQI479G_F,chbQI479G_G,chbQI479G_H,chbQI479G_I,chbQI479G_J,chbQI479G_X,txtQI479G_XI,chbQI479G_Y); 
    		MyUtil.LiberarMemoria();
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,chbQI479G_A,chbQI479G_B,chbQI479G_C,chbQI479G_D,chbQI479G_E,chbQI479G_F,chbQI479G_G,chbQI479G_H,chbQI479G_I,chbQI479G_J,chbQI479G_X,txtQI479G_XI,chbQI479G_Y);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		onQI479G_YChangeValue();
    	}
    }
    

    
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI479A.readOnly();
    		rgQI479C.readOnly();
    		txtQI479B.readOnly();
    		txtQI479D.readOnly();
    		
    		chbQI479E_A.readOnly();
    		chbQI479E_B.readOnly();
    		chbQI479E_C.readOnly();
    		chbQI479E_D.readOnly();
    		chbQI479E_E.readOnly();
    		chbQI479E_F.readOnly();
    		chbQI479E_G.readOnly();
    		chbQI479E_H.readOnly();
    		chbQI479E_I.readOnly();
    		chbQI479E_J.readOnly();
    		chbQI479E_X.readOnly();
    		txtQI479E_XI.readOnly();
    		chbQI479E_Y.readOnly();
    		
    		chbQI479F_A.readOnly();
    		chbQI479F_B.readOnly();
    		chbQI479F_C.readOnly();
    		chbQI479F_D.readOnly();
    		chbQI479F_E.readOnly();
    		chbQI479F_F.readOnly();
    		chbQI479F_G.readOnly();
    		chbQI479F_H.readOnly();
    		chbQI479F_I.readOnly();
    		chbQI479F_J.readOnly();
    		chbQI479F_K.readOnly();
    		chbQI479F_L.readOnly();
    		chbQI479F_M.readOnly();
    		chbQI479F_X.readOnly();
    		txtQI479F_XI.readOnly();
    		chbQI479F_Y.readOnly();
    		
    		rgQI479G.readOnly();
    		chbQI479G_A.readOnly();
    		chbQI479G_B.readOnly();
    		chbQI479G_C.readOnly();
    		chbQI479G_D.readOnly();
    		chbQI479G_E.readOnly();
    		chbQI479G_F.readOnly();
    		chbQI479G_G.readOnly();
    		chbQI479G_H.readOnly();
    		chbQI479G_I.readOnly();
    		chbQI479G_J.readOnly();
    		chbQI479G_X.readOnly();
    		txtQI479G_XI.readOnly();
    		chbQI479G_Y.readOnly();
    		
    		txtQI479GB_A.readOnly();
    		txtQI479GB_B.readOnly();
    		txtQI479GB_C.readOnly();
    		txtQI479GB_D.readOnly();
    		txtQI479GB_E.readOnly();
    		txtQI479GB_F.readOnly();
    		txtQI479GB_G.readOnly();
    		txtQI479GB_H.readOnly();
    		txtQI479GB_I.readOnly();
    		txtQI479GB_J.readOnly();
    		txtQI479GB_X.readOnly();

    		chb479gb_a.readOnly();
    		chb479gb_b.readOnly();
    		chb479gb_c.readOnly();
    		chb479gb_d.readOnly();
    		chb479gb_e.readOnly();
    		chb479gb_f.readOnly();
    		chb479gb_g.readOnly();
    		chb479gb_h.readOnly();
    		chb479gb_i.readOnly();
    		chb479gb_j.readOnly();
    		chb479gb_x.readOnly();
    		
    		rgQI479GC_A.readOnly();
    		rgQI479GC_B.readOnly();
    		rgQI479GC_C.readOnly();
    		rgQI479GC_D.readOnly();
    		rgQI479GC_E.readOnly();
    		rgQI479GC_F.readOnly();
    		rgQI479GC_G.readOnly();
    		rgQI479GC_H.readOnly();
    		rgQI479GC_I.readOnly();
    		rgQI479GC_J.readOnly();
    		rgQI479GC_X.readOnly();
    		
    	}
    }
 
	public void ontxtQI212_NOMChangeValue(){
		if(txtQI479G_XI.getText().toString().length()>0){
			resetearLabels();
			lblpregunta479gbx.setText(lblpregunta479gbx.getText().toString().replace("OTRO",txtQI479G_XI.getText().toString()));
			lblpregunta479gcx.setText(lblpregunta479gcx.getText().toString().replace("OTRO",txtQI479G_XI.getText().toString()));
		}
	}
	public void resetearLabels() {
		lblpregunta479gbx.setText(getResources().getString(R.string.c2seccion_04b_2qi479gc_x));
		lblpregunta479gcx.setText(getResources().getString(R.string.c2seccion_04b_2qi479gc_x));
	}
	
    
    public CuestionarioService getCuestionarioService() { 
 		if(cuestionarioService==null){ 
 			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
 		} 
 		return cuestionarioService; 
     }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(Individual); 
		try { 
			cambiarDatosparalaBD();
			if(!getCuestionarioService().saveOrUpdate(Individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		if(App.getInstance().getSeccion04B2()!=null) 
			App.getInstance().getSeccion04B2().qi479a = Individual.qi479a;
		else 
			App.getInstance().setSeccion04B2(Individual);
		
		
		return App.INDIVIDUAL; 
	} 
} 
