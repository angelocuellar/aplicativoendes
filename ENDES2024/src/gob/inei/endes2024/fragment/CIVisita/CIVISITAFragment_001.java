package gob.inei.endes2024.fragment.CIVisita; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CIVISITAFragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public TextField txtCONGLOME; 
	CARATULA_INDIVIDUAL c2visita; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	LinearLayout q0; 
	LinearLayout q1; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
// 
	public CIVISITAFragment_001() {} 
	public CIVISITAFragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"CONGLOME","ID","HOGAR_ID","PERSONA_ID","NRO_VISITA")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"CONGLOME")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2visita).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(lblTitulo); 
		q1 = createQuestionSection(R.string.c2visitaconglome,txtCONGLOME); 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2visita); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2visita,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		if (Util.esVacio(c2visita.conglome)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta CONGLOME"); 
//			view = txtCONGLOME; 
//			error = true; 
//			return false; 
//		} 
		return true; 
    } 
    @Override 
	public void cargarDatos() {
//		c2visita = getCuestionarioService().getC2VISITA(
//				App.getInstance().getPersona().id,
//				App.getInstance().getPersona().hogar_id,
//				App.getInstance().getPersona().persona_id, seccionesCargado);
//		if (c2visita == null) {
//			c2visita = new C2VISITA();
//			c2visita.id = App.getInstance().getPersona().id;
//			c2visita.hogar_id = App.getInstance().getPersona().hogar_id;
//			c2visita.persona_id = App.getInstance().getPersona().persona_id;
//			c2visita.nro_visita = App.getInstance().getPersona().nro_visita;
//		}
//		entityToUI(C2VISITA);
		inicio();
	} 
    private void inicio() { 
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	} 
} 
