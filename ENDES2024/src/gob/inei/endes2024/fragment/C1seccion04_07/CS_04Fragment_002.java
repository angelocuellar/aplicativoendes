package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_04Fragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS406; 
//	@FieldAnnotation(orderIndex=2) 
//	public CheckBoxField chbQS407_A; 
//	@FieldAnnotation(orderIndex=3) 
//	public CheckBoxField chbQS407_B; 
//	@FieldAnnotation(orderIndex=4) 
//	public CheckBoxField chbQS407_C; 
//	@FieldAnnotation(orderIndex=5) 
//	public CheckBoxField chbQS407_D; 
//	@FieldAnnotation(orderIndex=6) 
//	public CheckBoxField chbQS407_E; 
//	@FieldAnnotation(orderIndex=7) 
//	public CheckBoxField chbQS407_F; 
//	@FieldAnnotation(orderIndex=8) 
//	public CheckBoxField chbQS407_G; 
//	@FieldAnnotation(orderIndex=9) 
//	public CheckBoxField chbQS407_H; 
//	@FieldAnnotation(orderIndex=10) 
//	public CheckBoxField chbQS407_I; 
//	@FieldAnnotation(orderIndex=11) 
//	public CheckBoxField chbQS407_J; 
//	@FieldAnnotation(orderIndex=12) 
//	public CheckBoxField chbQS407_K; 
//	@FieldAnnotation(orderIndex=13) 
//	public CheckBoxField chbQS407_L; 
//	@FieldAnnotation(orderIndex=14) 
//	public CheckBoxField chbQS407_M; 
//	@FieldAnnotation(orderIndex=15) 
//	public CheckBoxField chbQS407_N; 
//	@FieldAnnotation(orderIndex=16) 
//	public TextField txtQS407_N_O; 
//	@FieldAnnotation(orderIndex=17) 
//	public CheckBoxField chbQS407_X; 
//	@FieldAnnotation(orderIndex=18) 
//	public TextField txtQS407_X_O; 
//	@FieldAnnotation(orderIndex=19) 
//	public CheckBoxField chbQS407_Y;
	@FieldAnnotation(orderIndex=2)
	public TextAreaField txtQSOBS_SECCION4;
	
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo, lblTitulo2, lblsectorpublico,lblhospital,lblsectorprivado,lblong,lblcampa,lblAlguienMas,lblpregunta406,lblObservaciones;//lblP407
//	private GridComponent2 grid_QS407; 
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	
	Salud seccion1; 
	
	//public boolean rango_40_59=true;//hasta 2019
	public boolean rango_18_75=true; //rango_25_70
	

	String fechareferencia;
	public CS_04Fragment_002() {} 
	public CS_04Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 															      
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS700FECH_REF","ID","HOGAR_ID","PERSONA_ID","QSOBS_SECCION4")};//,"QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y" 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QS700FECH_REF")};//,"QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y" 
		
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QH06","QS311")};
		
		return rootView; 
	} 
  
	  @Override 
	  protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_2).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblAlguienMas=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_07qs407_mas).textSize(21).centrar().negrita().alinearIzquierda();
		
		lblpregunta406 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs406);
		rgQS406=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs406_1,R.string.cap04_07qs406_2,R.string.cap04_07qs406_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS406ChangeValue"); 
		
//		lblP407 = new LabelComponent(this.getActivity()).text(R.string.cap01_07qs407).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
//		chbQS407_A=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_a, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_AChangeValue"); 
//		chbQS407_B=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_b, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_BChangeValue"); 
//		chbQS407_C=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_c, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_CChangeValue"); 
//		chbQS407_D=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_d, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_DChangeValue"); 
//		chbQS407_E=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_e, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_EChangeValue"); 
//		chbQS407_F=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_f, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_FChangeValue"); 
//		chbQS407_G=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_g, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_GChangeValue"); 
//		chbQS407_H=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_h, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_HChangeValue"); 
//		chbQS407_I=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_i, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_IChangeValue"); 
//		chbQS407_J=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_j, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_JChangeValue"); 
//		chbQS407_K=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_k, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_KChangeValue"); 
//		chbQS407_L=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_l, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_LChangeValue"); 
//		chbQS407_M=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_m, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_MChangeValue"); 
//		chbQS407_N=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_n, "0:1").size(altoComponente+10,290).callback("onqrgQS407_NChangeValue"); 
//		
//		txtQS407_N_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400);
//		chbQS407_X=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_x, "0:1").size(altoComponente+10,290).callback("onqrgQS407_XChangeValue"); 
//		txtQS407_X_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400);
//		chbQS407_Y=new CheckBoxField(this.getActivity(), R.string.cap04_07qs407_y, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS407_YChangeValue");
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_a).textSize(18).negrita().alinearIzquierda();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_b).textSize(18).negrita().alinearIzquierda();
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_c).textSize(18).negrita().alinearIzquierda();
		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_d).textSize(18).negrita().alinearIzquierda();
		
//	    grid_QS407 = new GridComponent2(this.getActivity(),App.ESTILO,2);
//	    
//	    grid_QS407.addComponent(lblsectorpublico,2,1);
//	    grid_QS407.addComponent(lblhospital,2,1);
//	    grid_QS407.addComponent(chbQS407_A,2,1);
//	    grid_QS407.addComponent(chbQS407_B,2,1);
//	    grid_QS407.addComponent(chbQS407_C,2,1);
//	    grid_QS407.addComponent(chbQS407_D,2,1);
//	    grid_QS407.addComponent(chbQS407_E,2,1);
//	    grid_QS407.addComponent(chbQS407_F,2,1);
//	    grid_QS407.addComponent(lblsectorprivado,2,1);
//	    grid_QS407.addComponent(chbQS407_G,2,1);
//	    grid_QS407.addComponent(chbQS407_H,2,1);
//	    grid_QS407.addComponent(lblong,2,1);
//	    grid_QS407.addComponent(chbQS407_I,2,1);
//	    grid_QS407.addComponent(chbQS407_J,2,1);
//	    grid_QS407.addComponent(chbQS407_K,2,1);
//	    grid_QS407.addComponent(lblcampa,2,1);
//	    grid_QS407.addComponent(chbQS407_L,2,1);
//	    grid_QS407.addComponent(chbQS407_M,2,1);
//	    grid_QS407.addComponent(chbQS407_N);
//	    grid_QS407.addComponent(txtQS407_N_O);
//	    grid_QS407.addComponent(chbQS407_X);
//	    grid_QS407.addComponent(txtQS407_X_O);
//	    grid_QS407.addComponent(chbQS407_Y,2,1);
	    
	    lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);  
		txtQSOBS_SECCION4= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblpregunta406,rgQS406); 
//		LinearLayout q3_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,grid_QS407.component());
//		q3 = createQuestionSection(lblP407,q3_2);
		
		q3 = createQuestionSection(R.string.cap04_07qsObservaSeccion4,txtQSOBS_SECCION4); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2);
		form.addView(q3);
//		form.addView(q4);
		
		return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(salud); 
		
		if (salud.qs406!=null) {
			salud.qs406=salud.getConvertqs401(salud.qs406);
		}		
//		if (salud.qs407_a!=null) {
//			salud.qs407_a=salud.getConvertqs407(salud.qs407_a);
//		}
//		if (salud.qs407_b!=null) {
//			salud.qs407_b=salud.getConvertqs407(salud.qs407_b);
//		}
//		if (salud.qs407_c!=null) {
//			salud.qs407_c=salud.getConvertqs407(salud.qs407_c);
//		}
//		if (salud.qs407_d!=null) {
//			salud.qs407_d=salud.getConvertqs407(salud.qs407_d);
//		}
//		if (salud.qs407_e!=null) {
//			salud.qs407_e=salud.getConvertqs407(salud.qs407_e);
//		}
//		if (salud.qs407_f!=null) {
//			salud.qs407_f=salud.getConvertqs407(salud.qs407_f);
//		}
//		if (salud.qs407_g!=null) {
//			salud.qs407_g=salud.getConvertqs407(salud.qs407_g);
//		}
//		if (salud.qs407_h!=null) {
//			salud.qs407_h=salud.getConvertqs407(salud.qs407_h);
//		}
//		if (salud.qs407_i!=null) {
//			salud.qs407_i=salud.getConvertqs407(salud.qs407_i);
//		}
//		if (salud.qs407_j!=null) {
//			salud.qs407_j=salud.getConvertqs407(salud.qs407_j);
//		}
//		if (salud.qs407_k!=null) {
//			salud.qs407_k=salud.getConvertqs407(salud.qs407_k);
//		}
//		if (salud.qs407_l!=null) {
//			salud.qs407_l=salud.getConvertqs407(salud.qs407_l);
//		}
//		if (salud.qs407_m!=null) {
//			salud.qs407_m=salud.getConvertqs407(salud.qs407_m);
//		}
//		if (salud.qs407_n!=null) {
//			salud.qs407_n=salud.getConvertqs407(salud.qs407_n);
//		}
//		if (salud.qs407_x!=null) {
//			salud.qs407_x=salud.getConvertqs407(salud.qs407_x);
//		}
//		if (salud.qs407_y!=null) {
//			salud.qs407_y=salud.getConvertqs407(salud.qs407_y);
//		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(salud.qs406 == null) {
				salud.qs700fech_ref=null;
			} 
			else {
				if(fechareferencia == null) {
					salud.qs700fech_ref =  Util.getFechaActualToString();
				}
				else {
					salud.qs700fech_ref = fechareferencia;
				}			
			}		
			
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(rango_18_75){
			if (Util.esVacio(salud.qs406)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.406"); 
				view = rgQS406; 
				error = true; 
				return false; 
			}
			
//			if (MyUtil.incluyeRango(1,1,salud.qs406.toString())) {
//				if (!Util.alMenosUnoEsDiferenteA(0,salud.qs407_a,salud.qs407_b,salud.qs407_c,salud.qs407_d,salud.qs407_e,salud.qs407_f,salud.qs407_g,salud.qs407_h,salud.qs407_i,salud.qs407_j,salud.qs407_k,salud.qs407_l,salud.qs407_m,salud.qs407_n,salud.qs407_x,salud.qs407_y)) { 
//					mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
//					view = chbQS407_A; 
//					error = true; 
//					return false; 
//				} 
//			}
//			
//			if (MyUtil.incluyeRango(1,1,salud.qs407_n.toString())) {
//				if (Util.esVacio(salud.qs407_n_o)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.407_N_O"); 
//					view = txtQS407_N_O; 
//					error = true; 
//					return false; 
//				} 
//			}
//			
//			if (MyUtil.incluyeRango(1,1,salud.qs407_x.toString())) {
//				if (Util.esVacio(salud.qs407_x_o)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.407_X_O"); 
//					view = txtQS407_X_O; 
//					error = true; 
//					return false; 
//				} 
//			}
		}
		return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	
		if(salud==null){
			salud=new CAP04_07();
			salud.id=App.getInstance().getPersonaSeccion01().id;
			salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id;
			salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id;
	    }		
		if (salud.qs406!=null) {
    		salud.qs406=salud.setConvertqs401(salud.qs406);    
    	}		
//		if (salud.qs407_a!=null) {
//    		salud.qs407_a=salud.setConvertqs407(salud.qs407_a);    
//    	}		
//		if (salud.qs407_b!=null) {
//    		salud.qs407_b=salud.setConvertqs407(salud.qs407_b);    
//    	}		
//		if (salud.qs407_c!=null) {
//    		salud.qs407_c=salud.setConvertqs407(salud.qs407_c);    
//    	}
//		if (salud.qs407_d!=null) {
//    		salud.qs407_d=salud.setConvertqs407(salud.qs407_d);    
//    	}
//		if (salud.qs407_e!=null) {
//    		salud.qs407_e=salud.setConvertqs407(salud.qs407_e);    
//    	}
//		if (salud.qs407_f!=null) {
//    		salud.qs407_f=salud.setConvertqs407(salud.qs407_f);    
//    	}
//		if (salud.qs407_g!=null) {
//    		salud.qs407_g=salud.setConvertqs407(salud.qs407_g);    
//    	}
//		if (salud.qs407_h!=null) {
//    		salud.qs407_h=salud.setConvertqs407(salud.qs407_h);    
//    	}
//		if (salud.qs407_i!=null) {
//    		salud.qs407_i=salud.setConvertqs407(salud.qs407_i);    
//    	}
//		if (salud.qs407_j!=null) {
//    		salud.qs407_j=salud.setConvertqs407(salud.qs407_j);    
//    	}
//		if (salud.qs407_k!=null) {
//    		salud.qs407_k=salud.setConvertqs407(salud.qs407_k);    
//    	}
//		if (salud.qs407_l!=null) {
//    		salud.qs407_l=salud.setConvertqs407(salud.qs407_l);    
//    	}
//		if (salud.qs407_m!=null) {
//    		salud.qs407_m=salud.setConvertqs407(salud.qs407_m);    
//    	}
//		if (salud.qs407_n!=null) {
//    		salud.qs407_n=salud.setConvertqs407(salud.qs407_n);    
//    	}
//		if (salud.qs407_x!=null) {
//    		salud.qs407_x=salud.setConvertqs407(salud.qs407_x);    
//    	}
//		if (salud.qs407_y!=null) {
//    		salud.qs407_y=salud.setConvertqs407(salud.qs407_y);    
//    	}
		
		entityToUI(salud); 
		fechareferencia = salud.qs700fech_ref;
		inicio(); 
		Log.e("","PPPP: "+seccion1.persona_id);
		Log.e("","SEXO: "+seccion1.qh06);
		App.getInstance().setPersonabySalud(seccion1);
    } 
    
    private void inicio() { 
    	entra_seccion4();    	
    	if(rango_18_75){
	    	onqrgQS406ChangeValue();
//	    	onqrgQS407_AChangeValue();
//	    	onqrgQS407_BChangeValue();
//	    	onqrgQS407_CChangeValue();
//	    	onqrgQS407_DChangeValue();
//	    	onqrgQS407_EChangeValue();
//	    	onqrgQS407_FChangeValue();
//	    	onqrgQS407_GChangeValue();
//	    	onqrgQS407_HChangeValue();
//	    	onqrgQS407_IChangeValue();
//	    	onqrgQS407_JChangeValue();
//	    	onqrgQS407_KChangeValue();
//	    	onqrgQS407_LChangeValue();
//	    	onqrgQS407_MChangeValue();
//	    	onqrgQS407_NChangeValue();
//	    	onqrgQS407_XChangeValue();
//	    	onqrgQS407_YChangeValue();	    
	    	
    	}
    	else{
    		grabar();
    	}
    	ValidarsiesSupervisora();    	
    	txtCabecera.requestFocus();
    } 
    
    public void entra_seccion4(){
    	if(!MyUtil.incluyeRango(18, 75, seccion1.qs23)){ //25, 70, seccion1.qs23   		
    		Util.cleanAndLockView(this.getActivity(),rgQS406,txtQSOBS_SECCION4);//,chbQS407_A,chbQS407_B,chbQS407_C,chbQS407_D,chbQS407_E,chbQS407_F,chbQS407_G,chbQS407_H,chbQS407_I,chbQS407_J,chbQS407_K,chbQS407_L,chbQS407_M,chbQS407_N,chbQS407_X,chbQS407_Y,txtQS407_N_O,txtQS407_X_O
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
//    		q4.setVisibility(View.GONE);
    		rango_18_75 = false;
    	}
    	else{
    		Util.lockView(getActivity(), false, rgQS406);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		rango_18_75 = true;
    		
    		if(MyUtil.incluyeRango(2, 2, seccion1.qh06) || (MyUtil.incluyeRango(1,1, seccion1.qh06) && MyUtil.incluyeRango(50, 75, seccion1.qs23)  )){    			
    			q3.setVisibility(View.GONE);
    		}
    		else{
    			seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS406","QSOBS_SECCION4")};//"QS407_A","QS407_B","QS407_C","QS407_D","QS407_E","QS407_F","QS407_G","QS407_H","QS407_I","QS407_J","QS407_K","QS407_L","QS407_M","QS407_N","QS407_N_O","QS407_X","QS407_X_O","QS407_Y",
    		}
    		RenombrarEtiquetas();
    	}
    }   
    
    
    public void onqrgQS406ChangeValue(){

//    	if (MyUtil.incluyeRango(2,3,rgQS406.getTagSelected("").toString())) {
//    		MyUtil.LiberarMemoria();
////			Util.cleanAndLockView(this.getActivity(),chbQS407_A,chbQS407_B,chbQS407_C,chbQS407_D,chbQS407_E,chbQS407_F,chbQS407_G,
////								chbQS407_H,chbQS407_I,chbQS407_J,chbQS407_K,chbQS407_L,chbQS407_M,chbQS407_N,chbQS407_X,chbQS407_Y,
////								txtQS407_N_O,txtQS407_X_O);
////			q3.setVisibility(View.GONE);
//		} 
//		else {				
////			Util.lockView(getActivity(),false,chbQS407_A,chbQS407_B,chbQS407_C,chbQS407_D,chbQS407_E,chbQS407_F,chbQS407_G,chbQS407_H,
////											  chbQS407_I,chbQS407_J,chbQS407_K,chbQS407_L,chbQS407_M,chbQS407_N,chbQS407_X,chbQS407_Y);
////			q3.setVisibility(View.VISIBLE);
////			chbQS407_A.requestFocus();
//		}
    }
    
//    public void onqrgQS407_AChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_B.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_BChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_C.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_CChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_D.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_DChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_E.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_EChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_F.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_FChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_G.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_GChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_H.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_HChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_I.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_IChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_J.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_JChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_K.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_KChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_L.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_LChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_M.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_MChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		chbQS407_N.requestFocus();			
//		}
//    }
//    
//    public void onqrgQS407_NChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		if (MyUtil.incluyeRango(0,0,chbQS407_N.getValue().toString())) {
//    			Util.lockView(getActivity(),false,txtQS407_N_O);
//    			txtQS407_N_O.setVisibility(View.VISIBLE);
//    			txtQS407_N_O.requestFocus();
//    		}else{
//    			MyUtil.LiberarMemoria();
//    			Util.cleanAndLockView(this.getActivity(),txtQS407_N_O);
//    			txtQS407_N_O.setVisibility(View.GONE);
//        		chbQS407_X.requestFocus();
//    		}		
//		}
//    }
//    
//    public void onqrgQS407_XChangeValue(){
//    	if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//    		if (MyUtil.incluyeRango(0,0,chbQS407_X.getValue().toString())) {
//    			Util.lockView(getActivity(),false,txtQS407_X_O);
//    			txtQS407_X_O.setVisibility(View.VISIBLE);
//    			txtQS407_X_O.requestFocus();
//    		}else{
//    			MyUtil.LiberarMemoria();
//    			Util.cleanAndLockView(this.getActivity(),txtQS407_X_O);
//    			txtQS407_X_O.setVisibility(View.GONE);
//        		chbQS407_Y.requestFocus();
//    		}			
//		}
//    }
    
//    public void onqrgQS407_YChangeValue(){  
//    	if (MyUtil.incluyeRango(0,0,chbQS407_Y.getValue().toString())) {
//    		MyUtil.LiberarMemoria();
//    		Util.cleanAndLockView(this.getActivity(),chbQS407_A,chbQS407_B,chbQS407_C,chbQS407_D,chbQS407_E,chbQS407_F,chbQS407_G,chbQS407_H,
//    												chbQS407_I,chbQS407_J,chbQS407_K,chbQS407_L,chbQS407_M,chbQS407_N,chbQS407_X,
//    												txtQS407_N_O,txtQS407_X_O);
//    		txtQS407_N_O.setVisibility(View.GONE);
//    		txtQS407_X_O.setVisibility(View.GONE);
//		}
//    	else{
//			if (MyUtil.incluyeRango(1,1,rgQS406.getTagSelected("").toString())) {
//				Util.lockView(getActivity(),false,chbQS407_A,chbQS407_B,chbQS407_C,chbQS407_D,chbQS407_E,chbQS407_F,chbQS407_G,chbQS407_H,
//												chbQS407_I,chbQS407_J,chbQS407_K,chbQS407_L,chbQS407_M,chbQS407_N,chbQS407_X);
//				chbQS407_A.requestFocus();
//			}
//		}
//    }
    
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS406.readOnly();
//			chbQS407_A.readOnly();
//			chbQS407_B.readOnly();
//			chbQS407_C.readOnly();
//			chbQS407_D.readOnly();
//			chbQS407_E.readOnly();
//			chbQS407_F.readOnly();			
//			chbQS407_G.readOnly();
//			chbQS407_H.readOnly();
//			chbQS407_I.readOnly();
//			chbQS407_J.readOnly();
//			chbQS407_K.readOnly();
//			chbQS407_L.readOnly();
//			chbQS407_M.readOnly();			
//			chbQS407_N.readOnly();
//			chbQS407_X.readOnly();
//			chbQS407_Y.readOnly();
//			txtQS407_N_O.readOnly();
//			txtQS407_X_O.readOnly();			
			txtQSOBS_SECCION4.setEnabled(false);
		}
	}
    
    public void RenombrarEtiquetas(){
    	Calendar fechaactual = new GregorianCalendar();
    	if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fechaactual=cal;
		}    	
    	String F2 = ""+(fechaactual.get(Calendar.YEAR)-2);
    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
    	String F3 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	
    	lblpregunta406.setText(lblpregunta406.getText().toString().replace("F1", F3));
    	lblpregunta406.setText(lblpregunta406.getText().toString().replace("F2", F2));
    	lblpregunta406.setText(lblpregunta406.getText().toString().replace("F3", F1));
    	
    	String F4="";
    	if(F1.equals("Diciembre")){
    		F4 = "del a�o pasado";
    	}else{
    		F4 = "de este a�o";
    	}
    	lblpregunta406.setText(lblpregunta406.getText().toString().replace("F4", F4));
    	
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs406!=null) {
			salud.qs406=salud.getConvertqs401(salud.qs406);
		}		
//		if (salud.qs407_a!=null) {
//			salud.qs407_a=salud.getConvertqs407(salud.qs407_a);
//		}
//		if (salud.qs407_b!=null) {
//			salud.qs407_b=salud.getConvertqs407(salud.qs407_b);
//		}
//		if (salud.qs407_c!=null) {
//			salud.qs407_c=salud.getConvertqs407(salud.qs407_c);
//		}
//		if (salud.qs407_d!=null) {
//			salud.qs407_d=salud.getConvertqs407(salud.qs407_d);
//		}
//		if (salud.qs407_e!=null) {
//			salud.qs407_e=salud.getConvertqs407(salud.qs407_e);
//		}
//		if (salud.qs407_f!=null) {
//			salud.qs407_f=salud.getConvertqs407(salud.qs407_f);
//		}
//		if (salud.qs407_g!=null) {
//			salud.qs407_g=salud.getConvertqs407(salud.qs407_g);
//		}
//		if (salud.qs407_h!=null) {
//			salud.qs407_h=salud.getConvertqs407(salud.qs407_h);
//		}
//		if (salud.qs407_i!=null) {
//			salud.qs407_i=salud.getConvertqs407(salud.qs407_i);
//		}
//		if (salud.qs407_j!=null) {
//			salud.qs407_j=salud.getConvertqs407(salud.qs407_j);
//		}
//		if (salud.qs407_k!=null) {
//			salud.qs407_k=salud.getConvertqs407(salud.qs407_k);
//		}
//		if (salud.qs407_l!=null) {
//			salud.qs407_l=salud.getConvertqs407(salud.qs407_l);
//		}
//		if (salud.qs407_m!=null) {
//			salud.qs407_m=salud.getConvertqs407(salud.qs407_m);
//		}
//		if (salud.qs407_n!=null) {
//			salud.qs407_n=salud.getConvertqs407(salud.qs407_n);
//		}
//		if (salud.qs407_x!=null) {
//			salud.qs407_x=salud.getConvertqs407(salud.qs407_x);
//		}
//		if (salud.qs407_y!=null) {
//			salud.qs407_y=salud.getConvertqs407(salud.qs407_y);
//		}
		
		if(salud.qs406 == null) {
			salud.qs700fech_ref = null;
		} 
		else {
			if(fechareferencia == null) {
				salud.qs700fech_ref =  Util.getFechaActualToString();
			}
			else {
				salud.qs700fech_ref = fechareferencia;
			}			
		}		
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
