package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_06Fragment_007 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS606; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS607; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS608; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS609; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS610; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS611; 
	@FieldAnnotation(orderIndex=7) 
	public TextAreaField txtQSOBS_S6;
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo, lblDescripcion,lblPreg606,lblPreg607,lblPreg608,lblPreg609,lblPreg610,lblPreg611; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6,q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	public boolean rango_15_29=true;
	public int qh06;
	public boolean mef=false;
// 
	public CS_06Fragment_007() {} 
	public CS_06Fragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604","QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS29A","QS29B")};
        
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s6).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s6_2).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		lblPreg606 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs606);
		lblPreg607 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs607);
		lblPreg608 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs608);
		lblPreg609 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs609);
		lblPreg610 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs610);
		lblPreg611 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs611);
		
		rgQS606=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs606_1,R.string.cap04_07qs606_2,R.string.cap04_07qs606_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS607=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs607_1,R.string.cap04_07qs607_2,R.string.cap04_07qs607_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS608=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs608_1,R.string.cap04_07qs608_2,R.string.cap04_07qs608_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS609=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs609_1,R.string.cap04_07qs609_2,R.string.cap04_07qs609_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS610=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs610_1,R.string.cap04_07qs610_2,R.string.cap04_07qs610_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS611=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs611_1,R.string.cap04_07qs611_2,R.string.cap04_07qs611_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		txtQSOBS_S6= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg606,rgQS606); 
		q2 = createQuestionSection(lblPreg607,rgQS607); 
		q3 = createQuestionSection(lblPreg608,rgQS608); 
		q4 = createQuestionSection(lblPreg609,rgQS609); 
		q5 = createQuestionSection(lblPreg610,rgQS610); 
		q6 = createQuestionSection(lblPreg611,rgQS611); 
		q7 = createQuestionSection(R.string.cap04_07qsObservaciones_s6,txtQSOBS_S6); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(cap04_07); 
		if (cap04_07.qs606 != null) {
			cap04_07.qs606 = cap04_07.getConvert603(cap04_07.qs606);
		}
		if (cap04_07.qs607 != null) {
			cap04_07.qs607 = cap04_07.getConvert603(cap04_07.qs607);
		}
		if (cap04_07.qs608 != null) {
			cap04_07.qs608 = cap04_07.getConvert603(cap04_07.qs608);
		}
		if (cap04_07.qs609 != null) {
			cap04_07.qs609 = cap04_07.getConvert603(cap04_07.qs609);
		}
		if (cap04_07.qs610 != null) {
			cap04_07.qs610 = cap04_07.getConvert603(cap04_07.qs610);
		}
		if (cap04_07.qs611 != null) {
			cap04_07.qs611 = cap04_07.getConvert603(cap04_07.qs611);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
    	
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		if(rango_15_29){
//            if(!mef){
		if (cap04_07.qs601a!=null && cap04_07.qs601b!=null && (cap04_07.qs601a==1 || cap04_07.qs601b==1)){
			if(MyUtil.incluyeRango(15, 29, seccion1.qs23) && MyUtil.incluyeRango(1, 1, qh06)  ){
				if (Util.esVacio(cap04_07.qs606)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.606"); 
					view = rgQS606; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs607)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.607"); 
					view = rgQS607; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs608)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.608"); 
					view = rgQS608; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs609)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.609"); 
					view = rgQS609; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs610)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.610"); 
					view = rgQS610; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs611)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.611"); 
					view = rgQS611; 
					error = true; 
					return false; 
				} 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	
    	mef=false;
    	cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);

    	qh06=App.getInstance().getPersonaSeccion01().qh06;

    	if(qh06==2 && MyUtil.incluyeRango(15, 49, seccion1.qs23)){
    		mef=true;
    	}
    	
    	if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    }
    	if (cap04_07.qs606 != null) {
			cap04_07.qs606 = cap04_07.setConvert603(cap04_07.qs606);
		}
    	if (cap04_07.qs607 != null) {
			cap04_07.qs607 = cap04_07.setConvert603(cap04_07.qs607);
		}
    	if (cap04_07.qs608 != null) {
			cap04_07.qs608 = cap04_07.setConvert603(cap04_07.qs608);
		}
    	if (cap04_07.qs609 != null) {
			cap04_07.qs609 = cap04_07.setConvert603(cap04_07.qs609);
		}
    	if (cap04_07.qs610 != null) {
			cap04_07.qs610 = cap04_07.setConvert603(cap04_07.qs610);
		}
    	if (cap04_07.qs611 != null) {
			cap04_07.qs611 = cap04_07.setConvert603(cap04_07.qs611);
		}
//    	Log.e("601a","" + cap04_07.qs601a);
//    	Log.e("601b","" + cap04_07.qs601b);
//    	Log.e("qh06","" + qh06);
//    	Log.e("qs23","" + seccion1.qs23);
    	
		entityToUI(cap04_07); 
		inicio(); 
    } 
    private void inicio() { 
    	
    	onP601ChangeValue();
//    	if(rango_15_29){
//    		onP601ChangeValue();
//    	}
    	txtCabecera.requestFocus();
    	ValidarsiesSupervisora();
    } 

    
    public void onP601ChangeValue(){  	  
    	if(cap04_07.qs601a!=null && cap04_07.qs601b!=null && cap04_07.qs601a==2 && cap04_07.qs601b==2){
    		Util.cleanAndLockView(this.getActivity(),rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
        	q0.setVisibility(View.GONE);
        	q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
        }
    	else{
        	entra_seccion6();
        	rgQS606.requestFocus();
    	}
    }
    
    public void entra_seccion6(){
    	if(MyUtil.incluyeRango(15, 29, seccion1.qs23) && MyUtil.incluyeRango(1, 1, qh06) ){
    		Log.e("ddd", "entro a 15 a 29 hombre");
    		Util.lockView(getActivity(), false, rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
    		q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
    	}
    	else{    	
    		Log.e("ddd", "entro a 15 a 29 mujer y 30 a 49 a�os  hombre y mujer");
    		Util.cleanAndLockView(this.getActivity(),rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
    	}
    }
    

    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS606.readOnly(); 
			rgQS607.readOnly();
			rgQS608.readOnly();
			rgQS609.readOnly();
			rgQS610.readOnly();
			rgQS611.readOnly();
			txtQSOBS_S6.setEnabled(false);
		}
	}
    
   
    
    /*
    public void entra_seccion6(){    	
    	if(!MyUtil.incluyeRango(15, 29, seccion1.qs23)){
    		Util.cleanAndLockView(this.getActivity(),rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
    		rango_15_29 = false;
    	}else{
    		if(mef){
    			Util.cleanAndLockView(this.getActivity(),rgQS606,rgQS607,rgQS608,rgQS609);
        		q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.GONE);
    			q4.setVisibility(View.GONE);
    			
    			if(seccion1.qs29a!=null && seccion1.qs29b!=null){
        	    	if(!Util.esDiferente(seccion1.qs29a, 1) || !Util.esDiferente(seccion1.qs29b, 1)){
        	    		q0.setVisibility(View.VISIBLE);
        	    		q5.setVisibility(View.VISIBLE);
            			q6.setVisibility(View.VISIBLE);
            			Util.lockView(getActivity(), false, rgQS610,rgQS611);
            			rango_15_29 = true;
                		rgQS610.requestFocus();
        	    	}
        	    	if(!Util.esDiferente(seccion1.qs29a, 2) && !Util.esDiferente(seccion1.qs29b, 2)){
        	    		Util.cleanAndLockView(this.getActivity(),rgQS610,rgQS611);
        	    		rango_15_29 = false;
        	    		q5.setVisibility(View.GONE);
        	    		q6.setVisibility(View.GONE);
        	    	}
    			}
    			
    			
    		}else{
    			q0.setVisibility(View.VISIBLE);
    			q1.setVisibility(View.VISIBLE);
    			q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			q5.setVisibility(View.VISIBLE);
    			q6.setVisibility(View.VISIBLE);
    			q7.setVisibility(View.VISIBLE);
        		rango_15_29 = true;
        		Util.lockView(getActivity(), false, rgQS606);
    		}
    		
    	}
    }
    */
    /*
    public void onP601ChangeValue(){
  
    	if(cap04_07.qs601a!=null && cap04_07.qs601b!=null){
    		if(cap04_07.qs601a==2 && cap04_07.qs601b==2){
        		Util.cleanAndLockView(this.getActivity(),rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
        		q0.setVisibility(View.GONE);
        		q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);
    			q3.setVisibility(View.GONE);
    			q4.setVisibility(View.GONE);
    			q5.setVisibility(View.GONE);
    			q6.setVisibility(View.GONE);
    			q7.setVisibility(View.GONE);
        		rango_15_29 = false;
        	}else{
        		q0.setVisibility(View.VISIBLE);
        		q1.setVisibility(View.VISIBLE);
    			q2.setVisibility(View.VISIBLE);
    			q3.setVisibility(View.VISIBLE);
    			q4.setVisibility(View.VISIBLE);
    			q5.setVisibility(View.VISIBLE);
    			q6.setVisibility(View.VISIBLE);
    			q7.setVisibility(View.VISIBLE);
        		rango_15_29 = true;
      
        		Util.lockView(getActivity(), false, rgQS606,rgQS607,rgQS608,rgQS609,rgQS610,rgQS611);
        		rgQS606.requestFocus();
        	}
    	}
    	
    }*/
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs606 != null) {
				cap04_07.qs606 = cap04_07.getConvert603(cap04_07.qs606);
			}
			if (cap04_07.qs607 != null) {
				cap04_07.qs607 = cap04_07.getConvert603(cap04_07.qs607);
			}
			if (cap04_07.qs608 != null) {
				cap04_07.qs608 = cap04_07.getConvert603(cap04_07.qs608);
			}
			if (cap04_07.qs609 != null) {
				cap04_07.qs609 = cap04_07.getConvert603(cap04_07.qs609);
			}
			if (cap04_07.qs610 != null) {
				cap04_07.qs610 = cap04_07.getConvert603(cap04_07.qs610);
			}
			if (cap04_07.qs611 != null) {
				cap04_07.qs611 = cap04_07.getConvert603(cap04_07.qs611);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
