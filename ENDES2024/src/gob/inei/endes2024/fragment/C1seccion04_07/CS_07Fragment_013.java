package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_013 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS724; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS725; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS726; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS727; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS728; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS729; 
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion; 
	private LabelComponent lblPreg724,lblPreg725,lblPreg726,lblPreg727,lblPreg728,lblPreg729;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	boolean entro=false,preg723=false;
	
	int valor723=0;
// 
	public CS_07Fragment_013() {} 
	public CS_07Fragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS713","QS714","QS715","QS716","QS717","QS723","QS724","QS725","QS726","QS727","QS728","QS729","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS723","QS724","QS725","QS726","QS727","QS728","QS729")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS206","QS209")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		Spanned texto = Html.fromHtml("724. �En los �ltimos 12 meses, <b>hubo momentos</b> en que bebi� m�s frecuentemente o durante <b>m�s d�as seguidos</b> de lo que se hab�a propuesto?");
		lblPreg724 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg724.setText(texto);
		
		Spanned texto2 = Html.fromHtml("725. �En los �ltimos 12 meses, <b>hubo momentos</b> en que empez� a beber y se emborrach� cuando <b>no quer�a hacerlo?</b>");
		lblPreg725 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg725.setText(texto2);
		
		Spanned texto3 = Html.fromHtml("726. �En los �ltimos 12 meses, <b>hubo momentos</b> en que intent� beber menos o dejar de beber y no pudo hacerlo?");
		lblPreg726 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg726.setText(texto3);
		
		Spanned texto4 = Html.fromHtml("727. �En los �ltimos 12 meses, <b>hubo periodos</b> de varios d�as o m�s en los que pas� tanto tiempo bebiendo o recuper�ndose de los efectos del alcohol que casi no le quedaba tiempo para nada m�s?");
		lblPreg727 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg727.setText(texto4);
		
		Spanned texto5 = Html.fromHtml("728. �En los �ltimos 12 meses, tuvo un <b>periodo de un mes o m�s</b> en que dej� de hacer actividades importantes (como trabajar, estudiar, ver a amigos, familiares o practicar deportes), o las redujo considerablemente a causa de la bebida?");
		lblPreg728 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg728.setText(texto5);
		
		Spanned texto6= Html.fromHtml("729. �En los �ltimos 12 meses, <b>hubo momentos</b> en que sigui� bebiendo aunque sab�a que ten�a un problema f�sico o emocional grave que podr�a haber sido provocado o empeorado por la bebida?");
		lblPreg729 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg729.setText(texto6);
		
		rgQS724=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs724_1,R.string.cap04_07qs724_2,R.string.cap04_07qs724_3,R.string.cap04_07qs724_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP724ChangeValue"); 
		rgQS725=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs725_1,R.string.cap04_07qs725_2,R.string.cap04_07qs725_3,R.string.cap04_07qs725_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS726=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs726_1,R.string.cap04_07qs726_2,R.string.cap04_07qs726_3,R.string.cap04_07qs726_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS727=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs727_1,R.string.cap04_07qs727_2,R.string.cap04_07qs727_3,R.string.cap04_07qs727_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS728=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs728_1,R.string.cap04_07qs728_2,R.string.cap04_07qs728_3,R.string.cap04_07qs728_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS729=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs729_1,R.string.cap04_07qs729_2,R.string.cap04_07qs729_3,R.string.cap04_07qs729_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg724,rgQS724); 
		q2 = createQuestionSection(lblPreg725,rgQS725); 
		q3 = createQuestionSection(lblPreg726,rgQS726); 
		q4 = createQuestionSection(lblPreg727,rgQS727); 
		q5 = createQuestionSection(lblPreg728,rgQS728); 
		q6 = createQuestionSection(lblPreg729,rgQS729); 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		
			form.addView(q0); 
			form.addView(q1); 
			form.addView(q2); 
			form.addView(q3); 
			form.addView(q4); 
			form.addView(q5); 
			form.addView(q6); 
		
		
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		if (cap04_07.qs724 != null) {
			cap04_07.qs724 = cap04_07.getConvert713(cap04_07.qs724);
		}
		if (cap04_07.qs725 != null) {
			cap04_07.qs725 = cap04_07.getConvert713(cap04_07.qs725);
		}
		if (cap04_07.qs726 != null) {
			cap04_07.qs726 = cap04_07.getConvert713(cap04_07.qs726);
		}
		if (cap04_07.qs727 != null) {
			cap04_07.qs727 = cap04_07.getConvert713(cap04_07.qs727);
		}
		if (cap04_07.qs728 != null) {
			cap04_07.qs728 = cap04_07.getConvert713(cap04_07.qs728);
		}
		if (cap04_07.qs729 != null) {
			cap04_07.qs729 = cap04_07.getConvert713(cap04_07.qs729);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(entro==false){
//			if (cap04_07.qs723!=1) {
			if(!preg723){
				if (Util.esVacio(cap04_07.qs724)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.724"); 
					view = rgQS724; 
					error = true; 
					return false; 
				} 
				if (MyUtil.incluyeRango(2,4, rgQS724.getTagSelected("").toString())) {
					if (Util.esVacio(cap04_07.qs725)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.725"); 
						view = rgQS725; 
						error = true; 
						return false; 
					} 	
				}
			}
			
			
			if (Util.esVacio(cap04_07.qs726)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.726"); 
				view = rgQS726; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs727)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.727"); 
				view = rgQS727; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs728)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.728"); 
				view = rgQS728; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs729)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.729"); 
				view = rgQS729; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersona().id, App.getInstance().getPersona().hogar_id, App.getInstance().getPersona().persona_id,seccionesCargado); 
		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	
    	entro=false;
    	
		if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
//		valor723=cap04_07.qs723;
		if(cap04_07.qs723==null){
			preg723=false;
		}else{
			 if(cap04_07.qs723==1){
				 preg723=true;
			 }else preg723=false;
		}
	
		
		if (cap04_07.qs724 != null) {
			cap04_07.qs724 = cap04_07.setConvert713(cap04_07.qs724);
		}
		if (cap04_07.qs725 != null) {
			cap04_07.qs725 = cap04_07.setConvert713(cap04_07.qs725);
		}
		if (cap04_07.qs726 != null) {
			cap04_07.qs726 = cap04_07.setConvert713(cap04_07.qs726);
		}
		if (cap04_07.qs727 != null) {
			cap04_07.qs727 = cap04_07.setConvert713(cap04_07.qs727);
		}
		if (cap04_07.qs728 != null) {
			cap04_07.qs728 = cap04_07.setConvert713(cap04_07.qs728);
		}
		if (cap04_07.qs729 != null) {
			cap04_07.qs729 = cap04_07.setConvert713(cap04_07.qs729);
		}
		entityToUI(cap04_07); 
		inicio(); 
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    	pregunta709();
    	txtCabecera.requestFocus();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS724.readOnly(); 
				rgQS725.readOnly();
				rgQS726.readOnly();
				rgQS727.readOnly(); 
				rgQS728.readOnly();
				rgQS729.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onP723ChangeValue() {

		

      if (preg723) {
			Util.cleanAndLockView(getActivity(), rgQS724,rgQS725);	
			q1.setVisibility(view.GONE);
			q2.setVisibility(view.GONE);
			rgQS726.requestFocus();
		}		
		else {	
			    q1.setVisibility(view.VISIBLE);
			    q2.setVisibility(view.VISIBLE);
			  
			    	Util.lockView(getActivity(), false,rgQS724,rgQS725);
					rgQS724.requestFocus();	
					onP724ChangeValue();
			    
				
		}
		
	}
    public void onP724ChangeValue() {
    	
//		if(cap04_07.qs723!=1){
		if(!preg723){
			if (MyUtil.incluyeRango(1,1, rgQS724.getTagSelected("").toString())) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(), rgQS725);	
				q2.setVisibility(view.GONE);
			}		
			else {	
				    q2.setVisibility(view.VISIBLE);
					Util.lockView(getActivity(), false,rgQS725);
					rgQS725.requestFocus();	
			}
		}
    	
		
	}
    public void pregunta709(){
    	if(seccion1.qs209==null || seccion1.qs209!=1 || preg717()){
    		entro=true;
    		Util.cleanAndLockView(this.getActivity(),rgQS724,rgQS725,rgQS726,rgQS727,rgQS728,rgQS729);
    		q1.setVisibility(view.GONE);
			q2.setVisibility(view.GONE);
			q3.setVisibility(view.GONE);
			q4.setVisibility(view.GONE);
			q5.setVisibility(view.GONE);
			q6.setVisibility(view.GONE);
    	}else{
    		q1.setVisibility(view.VISIBLE);
			q2.setVisibility(view.VISIBLE);
			q3.setVisibility(view.VISIBLE);
			q4.setVisibility(view.VISIBLE);
			q5.setVisibility(view.VISIBLE);
			q6.setVisibility(view.VISIBLE);
    		Util.lockView(getActivity(), false,rgQS724,rgQS725,rgQS726,rgQS727,rgQS728,rgQS729);
    		onP723ChangeValue();
    	}
    }
    public boolean preg717(){
    	
   	 boolean band=!MyUtil.incluyeRango(1,1,cap04_07.qs713) && !MyUtil.incluyeRango(1,1,cap04_07.qs714) && !MyUtil.incluyeRango(1,1,cap04_07.qs715) 
   			 && !MyUtil.incluyeRango(1,1,cap04_07.qs716) && !MyUtil.incluyeRango(1,1,cap04_07.qs717);
 
   	 
   	 return band;
   }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs724 != null) {
				cap04_07.qs724 = cap04_07.getConvert713(cap04_07.qs724);
			}
			if (cap04_07.qs725 != null) {
				cap04_07.qs725 = cap04_07.getConvert713(cap04_07.qs725);
			}
			if (cap04_07.qs726 != null) {
				cap04_07.qs726 = cap04_07.getConvert713(cap04_07.qs726);
			}
			if (cap04_07.qs727 != null) {
				cap04_07.qs727 = cap04_07.getConvert713(cap04_07.qs727);
			}
			if (cap04_07.qs728 != null) {
				cap04_07.qs728 = cap04_07.getConvert713(cap04_07.qs728);
			}
			if (cap04_07.qs729 != null) {
				cap04_07.qs729 = cap04_07.getConvert713(cap04_07.qs729);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
