package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_008 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS700A; 
	
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS700B; 

	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS700C;

	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS700D;

	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS700E; 

	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS700F;

	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS700G;

	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS700H;

	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS700I;
	
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQS702; 
	
	
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lblblanco,lblPreg700,lblEncabezado,lbl704_12,lbl704_13,lbl704_14,lblPreg702; 
	private LabelComponent lblP700_1,lblP700_2,lblP700_3,lblP700_4; 
	public LabelComponent lblQS700A,lblQS700B,lblQS700C,lblQS700D,lblQS700E,lblQS700F,lblQS700G,lblQS700H,lblQS700I;
	public LabelComponent lblQS700A2,lblQS700B2,lblQS700C2,lblQS700D2,lblQS700E2,lblQS700F2,lblQS700G2,lblQS700H2,lblQS700I2;
	public GridComponent2 grid1,grid2,grid3,grid4,grid5,grid6,grid7,grid8,grid9,grid10;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	LinearLayout q15; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	
	public boolean rango_15_99=true;
	
	String qs700fechref;
// 
	public CS_07Fragment_008() {} 
	public CS_07Fragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS700A","QS700B","QS700C","QS700D","QS700E","QS700F","QS700G","QS700H","QS700I","QS700FECH_REF","QS702","QS703","ID","HOGAR_ID","PERSONA_ID","QS601B","QS601A")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS700A","QS700B","QS700C","QS700D","QS700E","QS700F","QS700G","QS700H","QS700I","QS700FECH_REF","QS702","QS703")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS29A","QS29B","QS209")};
        
		return rootView; 
	} 
  public void RenombrarEtiquetas(){
  	Calendar fechaactual = new GregorianCalendar();
  	Calendar c1 = Calendar.getInstance();
  	if(qs700fechref!=null)
	{			
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		try {
			date2 = df.parse(qs700fechref);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		fechaactual=cal;			
	} 
  	
  	
	String data =Integer.toString(c1.get(Calendar.YEAR));
	int anio=Integer.parseInt(data);
	 boolean isLeapYear = ((GregorianCalendar) c1).isLeapYear(anio);
	
	 if(isLeapYear){
		if(App.getInstance().getC1Visita()!=null && App.getInstance().getC1Visita().qsvdia!=null && App.getInstance().getC1Visita().qsvdia.equals("31")&& fechaactual.getActualMaximum(Calendar.DAY_OF_MONTH)==31){
			fechaactual= MyUtil.restarDias(fechaactual, 13);
		}
		else {
			fechaactual = MyUtil.restarDias(fechaactual, 14);	
		}
	}
	else{
		fechaactual = MyUtil.restarDias(fechaactual, 14);	}
  	
	lblPreg700.setText(lblPreg700.getText().toString().replace("F1", fechaactual.get(Calendar.DAY_OF_MONTH)+ " de " + MyUtil.Mes(fechaactual.get(Calendar.MONTH))));
  	
  	}
  
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);  
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(180, 360).negrita().centrar();
		
		lblEncabezado = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs700_encabezado).negrita();
		
		lblPreg700 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs700).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
        lbl704_12 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsPreg_704_2).negrita();
		
		Spanned texto = Html.fromHtml("SI RESPONDE \"S&Iacute;\", PREGUNTE: <b> �Cu&aacute;ntos d&iacute;as? </b>"); 
		lbl704_13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lbl704_13.setText(texto);
		lbl704_14 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsPreg_704_3);
		
		
		lblP700_1 = new LabelComponent(getActivity()).textSize(16).size(180, 80).text(R.string.cap04_07qs700a_1).centrar();
		lblP700_2 = new LabelComponent(getActivity()).textSize(16).size(180, 90).text(R.string.cap04_07qs700a_2).centrar();
		lblP700_3 = new LabelComponent(getActivity()).textSize(16).size(180, 110).text(R.string.cap04_07qs700a_3).centrar();
		lblP700_4 = new LabelComponent(getActivity()).textSize(16).size(180, 110).text(R.string.cap04_07qs700a_4).centrar();
		
		
		Spanned textoA = Html.fromHtml("<b>A.</b> �Pocas ganas o inter�s en hacer las cosas? (DE SER NECESARIO LEA: Es decir, no disfruta sus actividades cotidianas)"); 
		Spanned textoA2 = Html.fromHtml("<b>A.</b> POCO INTERES��............................"); 
		
		lblQS700A=new LabelComponent(getActivity()).textSize(17).size(150, 360); lblQS700A.setText(textoA);	
		lblQS700A2=new LabelComponent(getActivity()).textSize(17).size(55, 200);  lblQS700A2.setText(textoA2);	
		rgQS700A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(95,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700AChangeValue");
		
		Spanned textoB = Html.fromHtml("<b>B.</b> �Sentirse desanimada(o), deprimida(o), triste o sin esperanza?"); 
		Spanned textoB2 = Html.fromHtml("<b>B.</b> DEPRIMIDA(O)����........................"); 
		
		lblQS700B=new LabelComponent(getActivity()).textSize(17).size(90, 360);	lblQS700B.setText(textoB);	
		lblQS700B2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS700B2.setText(textoB2);	
		rgQS700B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700BChangeValue"); 
		
		Spanned textoC = Html.fromHtml("<b>C.</b> �Problemas para dormir o mantenerse dormida(o), o en dormir demasiado?"); 
		Spanned textoC2 = Html.fromHtml("<b>C.</b> DORMIR�������.........................."); 
		
		lblQS700C=new LabelComponent(getActivity()).textSize(17).size(95, 360);lblQS700C.setText(textoC);
		lblQS700C2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS700C2.setText(textoC2);
		rgQS700C=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700CChangeValue");
		
		Spanned textoD = Html.fromHtml("<b>D.</b> �Sentirse cansada(o)  o  tener poca energ�a  sin motivo que lo justifique?"); 
		Spanned textoD2 = Html.fromHtml("<b>D.</b> CANSADA(O)������...................."); 
		
		lblQS700D=new LabelComponent(getActivity()).textSize(17).size(115, 360);lblQS700D.setText(textoD);
		lblQS700D2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS700D2.setText(textoD2);
		rgQS700D=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700DChangeValue"); 
		
		Spanned textoE = Html.fromHtml("<b>E.</b> �Poco apetito o comer en exceso?"); 
		Spanned textoE2 = Html.fromHtml("<b>E.</b> APETITO�������........................."); 
		
		lblQS700E=new LabelComponent(getActivity()).textSize(17).size(90, 360);lblQS700E.setText(textoE);
		lblQS700E2=new LabelComponent(getActivity()).textSize(17).size(40, 200);lblQS700E2.setText(textoE2);	
		rgQS700E=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(50,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700EChangeValue");
		
		Spanned textoF = Html.fromHtml("<b>F.</b> �Dificultad  para  poner  atenci�n o concentrarse en las cosas que hace?  (DE SER NECESARIO LEA:  Como leer el peri�dico,  ver televisi�n,  escuchar atentamente la radio o conversar con otras personas)"); 
		Spanned textoF2 = Html.fromHtml("<b>F.</b> PONER ATENCI�N����.................."); 
		
		lblQS700F=new LabelComponent(getActivity()).textSize(17).size(200, 360);lblQS700F.setText(textoF);
		lblQS700F2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS700F2.setText(textoF2);
		rgQS700F=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(145,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700FChangeValue");
		
		Spanned textoG = Html.fromHtml("<b>G.</b> �Moverse mas lento o hablar m�s lento de lo normal o sentirse m�s inquieta(o) o intranquila(o) de lo normal?"); 
		Spanned textoG2 = Html.fromHtml("<b>G.</b> MOVERSE�����..........................."); 
		
		lblQS700G=new LabelComponent(getActivity()).textSize(17).size(145, 360);lblQS700G.setText(textoG);
		lblQS700G2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS700G2.setText(textoG2);
		rgQS700G=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700GChangeValue"); 
		
		Spanned textoH = Html.fromHtml("<b>H.</b> �Pensamientos de que ser�a mejor estar muerta(o) o que quisiera hacerse da�o de alguna forma buscando morir?"); 
		Spanned textoH2 = Html.fromHtml("<b>H.</b> MORIR���������........................"); 
		
		lblQS700H=new LabelComponent(getActivity()).textSize(17).size(145, 360);lblQS700H.setText(textoH);
		lblQS700H2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS700H2.setText(textoH2);
		rgQS700H=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700HChangeValue");
		
		Spanned textoI = Html.fromHtml("<b>I.</b> �Sentirse mal acerca de si misma(o) o sentir que es una(un) fracasada(o) o que se ha fallado a s� misma(o) o a su familia?"); 
		Spanned textoI2 = Html.fromHtml("<b>I.</b> SENTIRSE MAL������.................."); 
		
		lblQS700I=new LabelComponent(getActivity()).textSize(17).size(135, 360);lblQS700I.setText(textoI);
		lblQS700I2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS700I2.setText(textoI2);	
		rgQS700I=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(80,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP700IChangeValue");  
		
		lblPreg702 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs702).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
        
		rgQS702=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs702_1,R.string.cap04_07qs702_2,R.string.cap04_07qs702_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,5);
		grid1.addComponent(lblblanco);
		grid1.addComponent(lblP700_1);
		grid1.addComponent(lblP700_2);
		grid1.addComponent(lblP700_3);
		grid1.addComponent(lblP700_4);
		
		grid2=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid2.addComponent(lblQS700A,1,2);
		grid2.addComponent(lblQS700A2);
		grid2.addComponent(rgQS700A);
		
		grid3=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid3.addComponent(lblQS700B,1,2);
		grid3.addComponent(lblQS700B2);
		grid3.addComponent(rgQS700B);
		
		grid4=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid4.addComponent(lblQS700C,1,2);
		grid4.addComponent(lblQS700C2);
		grid4.addComponent(rgQS700C);
		
		grid5=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid5.addComponent(lblQS700D,1,2);
		grid5.addComponent(lblQS700D2);
		grid5.addComponent(rgQS700D);
		
		grid6=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid6.addComponent(lblQS700E,1,2);
		grid6.addComponent(lblQS700E2);
		grid6.addComponent(rgQS700E);
		
		grid7=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid7.addComponent(lblQS700F,1,2);
		grid7.addComponent(lblQS700F2);
		grid7.addComponent(rgQS700F);
		
		grid8=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid8.addComponent(lblQS700G,1,2);
		grid8.addComponent(lblQS700G2);
		grid8.addComponent(rgQS700G);
		
		grid9=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid9.addComponent(lblQS700H,1,2);
		grid9.addComponent(lblQS700H2);
		grid9.addComponent(rgQS700H);
		
		grid10=new GridComponent2(this.getActivity(),App.ESTILO,2);
		grid10.addComponent(lblQS700I,1,2);
		grid10.addComponent(lblQS700I2);
		grid10.addComponent(rgQS700I);

		
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion,lblEncabezado); 
		q1 = createQuestionSection(lblPreg700,lbl704_12,lbl704_13,lbl704_14,grid1.component(),grid2.component(),grid3.component(),grid4.component(),grid5.component(),
				grid6.component(),grid7.component(),grid8.component(),grid9.component(),grid10.component()); 

		q2 = createQuestionSection(lblPreg702,rgQS702); 

		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 

		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		if (cap04_07.qs700a != null) {
			cap04_07.qs700a = cap04_07.getConvert700(cap04_07.qs700a);
			
			if(qs700fechref == null) {
				cap04_07.qs700fech_ref =  Util.getFechaActualToString();
				}
			else {
				cap04_07.qs700fech_ref = qs700fechref;
				 }
		}
		else {
			cap04_07.qs700fech_ref = null;
		}
		if (cap04_07.qs700b != null) {
			cap04_07.qs700b = cap04_07.getConvert700(cap04_07.qs700b);
		}
		if (cap04_07.qs700c != null) {
			cap04_07.qs700c = cap04_07.getConvert700(cap04_07.qs700c);
		}
		if (cap04_07.qs700d != null) {
			cap04_07.qs700d = cap04_07.getConvert700(cap04_07.qs700d);
		}
		if (cap04_07.qs700e != null) {
			cap04_07.qs700e = cap04_07.getConvert700(cap04_07.qs700e);
		}
		if (cap04_07.qs700f != null) {
			cap04_07.qs700f = cap04_07.getConvert700(cap04_07.qs700f);
		}
		if (cap04_07.qs700g != null) {
			cap04_07.qs700g = cap04_07.getConvert700(cap04_07.qs700g);
		}
		if (cap04_07.qs700h != null) {
			cap04_07.qs700h = cap04_07.getConvert700(cap04_07.qs700h);
		}
		if (cap04_07.qs700i != null) {
			cap04_07.qs700i = cap04_07.getConvert700(cap04_07.qs700i);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS4_7().qs700a=cap04_07.qs700a;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 

		if(rango_15_99){
			if (Util.esVacio(cap04_07.qs700a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700A"); 
				view = rgQS700A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700B"); 
				view = rgQS700B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700C"); 
				view = rgQS700C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700D"); 
				view = rgQS700D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700E"); 
				view = rgQS700E; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700F"); 
				view = rgQS700F; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700G"); 
				view = rgQS700G; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700h)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700H"); 
				view = rgQS700H; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs700i)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.700I"); 
				view = rgQS700I; 
				error = true; 
				return false; 
			} 
			if (!MyUtil.incluyeRango(1,1, rgQS700A.getTagSelected("").toString()) || 
		    		!MyUtil.incluyeRango(1,1, rgQS700B.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700C.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700D.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700E.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700F.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700G.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700H.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS700I.getTagSelected("").toString())
		    		) {
				if (Util.esVacio(cap04_07.qs702)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.702"); 
					view = rgQS702; 
					error = true; 
					return false; 
				} 
			}
			
			
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	
		if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		if (cap04_07.qs700a != null) {
			cap04_07.qs700a = cap04_07.setConvert700(cap04_07.qs700a);
		}
		if (cap04_07.qs700b != null) {
			cap04_07.qs700b = cap04_07.setConvert700(cap04_07.qs700b);
		}
		if (cap04_07.qs700c != null) {
			cap04_07.qs700c = cap04_07.setConvert700(cap04_07.qs700c);
		}
		if (cap04_07.qs700d != null) {
			cap04_07.qs700d = cap04_07.setConvert700(cap04_07.qs700d);
		}
		if (cap04_07.qs700e != null) {
			cap04_07.qs700e = cap04_07.setConvert700(cap04_07.qs700e);
		}
		if (cap04_07.qs700f != null) {
			cap04_07.qs700f = cap04_07.setConvert700(cap04_07.qs700f);
		}
		if (cap04_07.qs700g != null) {
			cap04_07.qs700g = cap04_07.setConvert700(cap04_07.qs700g);
		}
		if (cap04_07.qs700h != null) {
			cap04_07.qs700h = cap04_07.setConvert700(cap04_07.qs700h);
		}
		if (cap04_07.qs700i != null) {
			cap04_07.qs700i = cap04_07.setConvert700(cap04_07.qs700i);
		}
		entityToUI(cap04_07); 
		qs700fechref = cap04_07.qs700fech_ref;
		App.getInstance().setPersona_04_07(cap04_07);
		inicio(); 
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    	entra_seccion7();
    	if(rango_15_99){
    		todosCero700();
    		RenombrarEtiquetas();
//    		rgQS700A.requestFocus();
    	}
    	App.getInstance().setPersonabySalud(null);
    	App.getInstance().setPersonabySalud(seccion1);
    	txtCabecera.requestFocus();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS700A.readOnly(); 
				rgQS700B.readOnly();
				rgQS700C.readOnly(); 
				rgQS700D.readOnly();
				rgQS700E.readOnly();
				rgQS700F.readOnly();
				rgQS700G.readOnly();
				rgQS700H.readOnly();
				rgQS700I.readOnly();
				rgQS702.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void entra_seccion7(){
    	if(!MyUtil.incluyeRango(15, 99, seccion1.qs23)){
    		Util.cleanAndLockView(this.getActivity(),rgQS700A,rgQS700B,rgQS700C,rgQS700D,rgQS700E,rgQS700F,rgQS700G,rgQS700H,rgQS700I,rgQS702);
    		rango_15_99 = false;
    	}else{
    		rango_15_99 = true;
    		Util.lockView(getActivity(), false, rgQS700A);
    	}
    }
    public void todosCero700(){
    	if (MyUtil.incluyeRango(1,1, rgQS700A.getTagSelected("").toString()) && 
        		MyUtil.incluyeRango(1,1, rgQS700B.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700C.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700D.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700E.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700F.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700G.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700H.getTagSelected("").toString()) &&
        		MyUtil.incluyeRango(1,1, rgQS700I.getTagSelected("").toString())
        		) {
        		Util.cleanAndLockView(this.getActivity(),rgQS702);
        		q2.setVisibility(view.GONE);
        	}else{
        		q2.setVisibility(view.VISIBLE);
        		Util.lockView(getActivity(), false, rgQS702);
//        		rgQS702.requestFocus();
        	}
    }
    public void onP700AChangeValue(){
    	todosCero700();
    	rgQS700B.requestFocus();
    }
    public void onP700BChangeValue(){
    	todosCero700();
    	rgQS700C.requestFocus();
    }
    public void onP700CChangeValue(){
    	todosCero700();
    	rgQS700D.requestFocus();
    }
    public void onP700DChangeValue(){
    	todosCero700();
    	rgQS700E.requestFocus();
    }
    public void onP700EChangeValue(){
    	todosCero700();
    	rgQS700F.requestFocus();
    }
    public void onP700FChangeValue(){
    	todosCero700();
    	rgQS700G.requestFocus();
    }
    public void onP700GChangeValue(){
    	todosCero700();
    	rgQS700H.requestFocus();
    }
    public void onP700HChangeValue(){
    	todosCero700();
    	rgQS700I.requestFocus();
    }
    public void onP700IChangeValue(){
    	todosCero700();
    	rgQS702.requestFocus();
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs700a != null) {
				cap04_07.qs700a = cap04_07.getConvert700(cap04_07.qs700a);
				if(qs700fechref == null) {
					cap04_07.qs700fech_ref =  Util.getFechaActualToString();
					}
				else {
					cap04_07.qs700fech_ref = qs700fechref;
					 }
			}
			else {
				cap04_07.qs700fech_ref = null;
			}
			if (cap04_07.qs700b != null) {
				cap04_07.qs700b = cap04_07.getConvert700(cap04_07.qs700b);
			}
			if (cap04_07.qs700c != null) {
				cap04_07.qs700c = cap04_07.getConvert700(cap04_07.qs700c);
			}
			if (cap04_07.qs700d != null) {
				cap04_07.qs700d = cap04_07.getConvert700(cap04_07.qs700d);
			}
			if (cap04_07.qs700e != null) {
				cap04_07.qs700e = cap04_07.getConvert700(cap04_07.qs700e);
			}
			if (cap04_07.qs700f != null) {
				cap04_07.qs700f = cap04_07.getConvert700(cap04_07.qs700f);
			}
			if (cap04_07.qs700g != null) {
				cap04_07.qs700g = cap04_07.getConvert700(cap04_07.qs700g);
			}
			if (cap04_07.qs700h != null) {
				cap04_07.qs700h = cap04_07.getConvert700(cap04_07.qs700h);
			}
			if (cap04_07.qs700i != null) {
				cap04_07.qs700i = cap04_07.getConvert700(cap04_07.qs700i);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
