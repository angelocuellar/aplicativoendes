package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_014 extends FragmentForm { 
	 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQSP730; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQS731_A; 
	
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQS731_AA; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS731_AB; 
	
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS731_B; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQS731_C; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQS731_D; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS731_E; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQS731_EA;
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQS731_F; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQS731_G; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQS731_H; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQS731_I; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQS731_J; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQS731_X; 
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQS731_O; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQS731_Y; 
	@FieldAnnotation(orderIndex=18) 
	public IntegerField txtQS731AH; 
	@FieldAnnotation(orderIndex=19) 
	public IntegerField txtQS731AM;
	@FieldAnnotation(orderIndex=20) 
	public TextAreaField txtQSOBS_S7;
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private Seccion01Service Personaservice;

	private LabelComponent lblTitulo,lblDescripcion,lblsectorpublico,lblsectorprivado,lblhospital,lblong; 
//	private LabelComponent lblHora, lblMinutos, lblHor, lblMin; 
	private LabelComponent lblhora,lblhoras,lblPreg730,lblPreg731,lblPreg731A;
	private GridComponent2 grid_QS731,gridHora; 
	public Time hora_fin;
	public Hogar hogar; 
	public ButtonComponent btnHoraFinalizar;
	
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	boolean entro=false;
// 
	public CS_07Fragment_014() {} 
	public CS_07Fragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQS731AH, 0, 23, null, 99); 
		rango(getActivity(), txtQS731AM, 0, 59, null, 99); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSP730","QS731_A","QS731_AA","QS731_AB","QS731_B","QS731_C","QS731_D","QS731_E","QS731_EA","QS731_F","QS731_G","QS731_H","QS731_I","QS731_J","QS731_X","QS731_O","QS731_Y","QS731AH","QS731AM","QSOBS_S7","QS601A","QS601B","QS713","QS714","QS715","QS716","QS717","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QSP730","QS731_A","QS731_AA","QS731_AB","QS731_B","QS731_C","QS731_D","QS731_E","QS731_EA","QS731_F","QS731_G","QS731_H","QS731_I","QS731_J","QS731_X","QS731_O","QS731_Y","QS731AH","QS731AM","QSOBS_S7")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS206","QS209")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
  
		Spanned texto = Html.fromHtml("730. �En los �ltimos 12 meses, usted ha recibido tratamiento de alg�n profesional de salud por consumo de alcohol?");
        
		lblPreg730 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblPreg730.setText(texto);
        rgQSP730=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qsp730_1,R.string.cap04_07qsp730_2,R.string.cap04_07qsp730_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS730ChangeValue"); 
		
        lblPreg731 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsp731);
		chbQS731_A=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_a, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS731_AA=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_aa, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS731_AB=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_ab, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS731_B=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_b, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_C=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_c, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_D=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_d, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_E=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_e, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS731_EA=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_ea, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS731_F=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_f, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_G=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_g, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_H=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_h, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_I=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_i, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS731_J=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_j, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 

		chbQS731_X=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_x, "0:1").size(altoComponente+10,290).callback("onqrgQS731_XChangeValue"); 
		txtQS731_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400);
		chbQS731_Y=new CheckBoxField(this.getActivity(), R.string.cap04_07qs731_y, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS731_YChangeValue");
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_a).textSize(18).negrita().alinearIzquierda().size(80, WRAP_CONTENT);
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_b).textSize(18).negrita().alinearIzquierda();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_c).textSize(18).negrita().alinearIzquierda();
		
		lblPreg731A = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs731_hora);
		txtQS731AH=new IntegerField(this.getActivity()).size(altoComponente, 40).maxLength(2); 
		txtQS731AM=new IntegerField(this.getActivity()).size(altoComponente, 40).maxLength(2);
		
		grid_QS731 = new GridComponent2(this.getActivity(),App.ESTILO,2);
	    
	    grid_QS731.addComponent(lblsectorpublico,2,1);
	    grid_QS731.addComponent(lblhospital,2,1);
	    grid_QS731.addComponent(chbQS731_A,2,1);
	    grid_QS731.addComponent(chbQS731_AA,2,1);
	    grid_QS731.addComponent(chbQS731_AB,2,1);
	    grid_QS731.addComponent(chbQS731_B,2,1);
	    grid_QS731.addComponent(chbQS731_C,2,1);
	    grid_QS731.addComponent(chbQS731_D,2,1);
	    grid_QS731.addComponent(chbQS731_E,2,1);
	    grid_QS731.addComponent(chbQS731_EA,2,1);
	    grid_QS731.addComponent(chbQS731_F,2,1);
	    
	    grid_QS731.addComponent(lblsectorprivado,2,1);
	    grid_QS731.addComponent(chbQS731_G,2,1);
	    grid_QS731.addComponent(chbQS731_H,2,1);
	    
	    grid_QS731.addComponent(lblong,2,1);
	    grid_QS731.addComponent(chbQS731_I,2,1);
	    grid_QS731.addComponent(chbQS731_J,2,1);
	    grid_QS731.addComponent(chbQS731_X);
	    grid_QS731.addComponent(txtQS731_O);
	    grid_QS731.addComponent(chbQS731_Y,2,1);
	    
	    lblhora = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 210).text(R.string.cap08_09qs802bh).centrar();
		btnHoraFinalizar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.hogarqh110_terminar).size(150, 55);
		lblhoras = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 120).text("").centrar();

		gridHora = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridHora.addComponent(btnHoraFinalizar);
		gridHora.addComponent(lblhora);				
		gridHora.addComponent(lblhoras);
		
		txtQSOBS_S7= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		btnHoraFinalizar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
					if(entro 
						|| (!entro && MyUtil.incluyeRango(2,8,rgQSP730.getTagSelected("").toString()))
						|| (!entro && MyUtil.incluyeRango(1,1,rgQSP730.getTagSelected("").toString())
							&& (chbQS731_A.isChecked() || 
								chbQS731_AA.isChecked() ||
								chbQS731_AB.isChecked() ||
								chbQS731_B.isChecked() ||
								chbQS731_C.isChecked() ||
								chbQS731_D.isChecked() ||
								chbQS731_E.isChecked() ||
								chbQS731_EA.isChecked() ||
								chbQS731_F.isChecked() ||
								chbQS731_G.isChecked() ||
								chbQS731_H.isChecked() ||
								chbQS731_I.isChecked() ||
								chbQS731_J.isChecked() ||
								chbQS731_X.isChecked() ||
								chbQS731_Y.isChecked() 
								)	
								)
						){
						Calendar calendario = new GregorianCalendar();
						Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
						Integer minute= calendario.get(Calendar.MINUTE);
						String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
						String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
						lblhoras.setText(muestrahora+":"+muestraminuto);
					}
					else {
						ToastMessage.msgBox(getActivity(), "Debe registar todas las preguntas", ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					}
				}
			});
    } 
  
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg730,rgQSP730); 
		LinearLayout q3_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,grid_QS731.component());
		q2 = createQuestionSection(lblPreg731,q3_2);
		q3 = createQuestionSection(lblPreg731A,gridHora.component()); 
		q4 = createQuestionSection(R.string.cap04_07qsObservaciones_s7,txtQSOBS_S7); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(salud); 
		salud.qs731ah = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		salud.qs731am = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;

		
		if (salud.qsp730 != null) {
			salud.qsp730 = salud.getConvert603(salud.qsp730);
		}
		if (salud.qs731_a!=null) {
			salud.qs731_a=salud.getConvertqs731(salud.qs731_a);
		}
		if (salud.qs731_aa!=null) {
			salud.qs731_aa=salud.getConvertqs731(salud.qs731_aa);
		}
		if (salud.qs731_ab!=null) {
			salud.qs731_ab=salud.getConvertqs731(salud.qs731_ab);
		}
		if (salud.qs731_b!=null) {
			salud.qs731_b=salud.getConvertqs731(salud.qs731_b);
		}
		if (salud.qs731_c!=null) {
			salud.qs731_c=salud.getConvertqs731(salud.qs731_c);
		}
		if (salud.qs731_d!=null) {
			salud.qs731_d=salud.getConvertqs731(salud.qs731_d);
		}
		if (salud.qs731_e!=null) {
			salud.qs731_e=salud.getConvertqs731(salud.qs731_e);
		}
		if (salud.qs731_ea!=null) {
			salud.qs731_ea=salud.getConvertqs731(salud.qs731_ea);
		}
		if (salud.qs731_f!=null) {
			salud.qs731_f=salud.getConvertqs731(salud.qs731_f);
		}
		if (salud.qs731_g!=null) {
			salud.qs731_g=salud.getConvertqs731(salud.qs731_g);
		}
		if (salud.qs731_h!=null) {
			salud.qs731_h=salud.getConvertqs731(salud.qs731_h);
		}
		if (salud.qs731_i!=null) {
			salud.qs731_i=salud.getConvertqs731(salud.qs731_i);
		}
		if (salud.qs731_j!=null) {
			salud.qs731_j=salud.getConvertqs731(salud.qs731_j);
		}
		
		if (salud.qs731_x!=null) {
			salud.qs731_x=salud.getConvertqs731(salud.qs731_x);
		}
		if (salud.qs731_y!=null) {
			salud.qs731_y=salud.getConvertqs731(salud.qs731_y);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(salud!=null && salud.id!=null){
			MensajeSinoTieneGps(salud.id);
		}
		return true; 
    } 
    private void MensajeSinoTieneGps(Integer id){
    	if(EndesCalendario.VerificarSitientePuntoGPS(id)==true){
    		MyUtil.MensajeGeneral(getActivity(),App.MENSAJE_GPS);
    	}
    }
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(!entro){
			if (Util.esVacio(salud.qsp730)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.730"); 
				view = rgQSP730; 
				error = true; 
				return false; 
			} 

			if (MyUtil.incluyeRango(1,1,salud.qsp730.toString())) {
				if (!Util.alMenosUnoEsDiferenteA(0,0,salud.qs731_a,salud.qs731_aa,salud.qs731_ab,salud.qs731_b,salud.qs731_c,salud.qs731_d,salud.qs731_e,salud.qs731_ea,salud.qs731_f,salud.qs731_g,salud.qs731_h,salud.qs731_i,salud.qs731_j,salud.qs731_x,salud.qs731_y)) { 
					mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
					view = chbQS731_A; 
					error = true; 
					return false; 
				} 
			}
			if (MyUtil.incluyeRango(1,1,salud.qs731_x.toString())) {
				if (Util.esVacio(salud.qs731_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.731_O"); 
					view = txtQS731_O; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(salud.qs731ah)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.731AH"); 
				view = txtQS731AH; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs731am)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.731AM"); 
				view = txtQS731AM; 
				error = true; 
				return false; 
			} 
		}else{
			if (Util.esVacio(salud.qs731ah)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.731AH"); 
				view = txtQS731AH; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs731am)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.731AM"); 
				view = txtQS731AM; 
				error = true; 
				return false; 
			} 
		}
		
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersona().id, App.getInstance().getPersona().hogar_id, App.getInstance().getPersona().persona_id,seccionesCargado); 
        salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
        seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);

    	entro=false;
    	
    	boolean flag= false;
    	if(salud==null){
//    		Log.e("","salud NULL");
    	}else{
//    		Log.e("","salud != NULL");
    		flag=getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id,0,11);
//        	Log.e("","flag="+flag);
        	salud.entrars8=flag;
    	}
    	
    	
    	
		if(salud==null){ 
		  salud=new CAP04_07(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		if (salud.qsp730 != null) {
			salud.qsp730 = salud.setConvert603(salud.qsp730);
		}
	
		if (salud.qs731_a!=null) {
    		salud.qs731_a=salud.setConvertqs731(salud.qs731_a);    
    	}	
		if (salud.qs731_aa!=null) {
    		salud.qs731_aa=salud.setConvertqs731(salud.qs731_aa);    
    	}	
		if (salud.qs731_ab!=null) {
    		salud.qs731_ab=salud.setConvertqs731(salud.qs731_ab);    
    	}	
		if (salud.qs731_b!=null) {
    		salud.qs731_b=salud.setConvertqs731(salud.qs731_b);    
    	}		
		if (salud.qs731_c!=null) {
    		salud.qs731_c=salud.setConvertqs731(salud.qs731_c);    
    	}
		if (salud.qs731_d!=null) {
    		salud.qs731_d=salud.setConvertqs731(salud.qs731_d);    
    	}
		if (salud.qs731_e!=null) {
    		salud.qs731_e=salud.setConvertqs731(salud.qs731_e);    
    	}
		if (salud.qs731_ea!=null) {
    		salud.qs731_ea=salud.setConvertqs731(salud.qs731_ea);    
    	}
		if (salud.qs731_f!=null) {
    		salud.qs731_f=salud.setConvertqs731(salud.qs731_f);    
    	}
		if (salud.qs731_g!=null) {
    		salud.qs731_g=salud.setConvertqs731(salud.qs731_g);    
    	}
		if (salud.qs731_h!=null) {
    		salud.qs731_h=salud.setConvertqs731(salud.qs731_h);    
    	}
		if (salud.qs731_i!=null) {
    		salud.qs731_i=salud.setConvertqs731(salud.qs731_i);    
    	}
		if (salud.qs731_j!=null) {
    		salud.qs731_j=salud.setConvertqs731(salud.qs731_j);    
    	}
		
		if (salud.qs731_x!=null) {
    		salud.qs731_x=salud.setConvertqs731(salud.qs731_x);    
    	}
		if (salud.qs731_y!=null) {
    		salud.qs731_y=salud.setConvertqs731(salud.qs731_y);    
    	}
		if(salud.qs731ah!=null && salud.qs731am!=null)
    	{
    		lblhoras.setText(salud.qs731ah.toString()+":"+salud.qs731am.toString());
    	}
		entityToUI(salud); 
		inicio(); 
    } 
    private void inicio() { 
    	
//    	onqrgQS730ChangeValue();
//    	onqrgQS731_XChangeValue();
//        onqrgQS731_YChangeValue();
//        imprimir_hora();
    	ValidarsiesSupervisora();
    	App.getInstance().setPersona_04_07(null);
    	App.getInstance().setPersona_04_07(salud);
    	App.getInstance().setPersonabySalud(null);
    	App.getInstance().setPersonabySalud(seccion1);
    	pregunta709();
    	txtCabecera.requestFocus();
        
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQSP730.readOnly(); 
				chbQS731_A.readOnly();
				chbQS731_AA.readOnly();
				chbQS731_AB.readOnly();
				chbQS731_B.readOnly();
				chbQS731_C.readOnly();
				chbQS731_D.readOnly();
				chbQS731_E.readOnly();
				chbQS731_EA.readOnly();
				chbQS731_F.readOnly();
				chbQS731_G.readOnly();
				chbQS731_H.readOnly();
				chbQS731_I.readOnly();
				chbQS731_J.readOnly();
				chbQS731_X.readOnly();
				chbQS731_Y.readOnly();
				txtQS731_O.readOnly();
				txtQS731AH.readOnly();
				txtQS731AM.readOnly();
				btnHoraFinalizar.setEnabled(false);
				txtQSOBS_S7.setEnabled(false);
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onqrgQS730ChangeValue(){
    	   
    	if (MyUtil.incluyeRango(2,3,rgQSP730.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(),chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H);
			Util.cleanAndLockView(this.getActivity(),chbQS731_I,chbQS731_J,chbQS731_X,chbQS731_Y);
			Util.cleanAndLockView(this.getActivity(),txtQS731_O);
			q2.setVisibility(view.GONE);
		} 
		else {		
			q2.setVisibility(view.VISIBLE);
			Util.lockView(getActivity(),false,chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H);
			Util.lockView(getActivity(),false,chbQS731_I,chbQS731_J,chbQS731_X,chbQS731_Y);
			
			if(entro==false){
//				chbQS731_A.requestFocus();
				onqrgQS731_XChangeValue();
		        onqrgQS731_YChangeValue();	
			}
			
		}
    }
    
    public void onqrgQS731_XChangeValue(){
    	if (MyUtil.incluyeRango(1,1,rgQSP730.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(0,0,chbQS731_X.getValue().toString())) {
    			Util.lockView(getActivity(),false,txtQS731_O);
    			txtQS731_O.requestFocus();
    		}else{
    			Util.cleanAndLockView(this.getActivity(),txtQS731_O);
    			chbQS731_X.requestFocus();
    		}			
		}
    }
    public void onqrgQS731_YChangeValue(){

    	if (MyUtil.incluyeRango(0,0,chbQS731_Y.getValue().toString())) {
    		Util.cleanAndLockView(this.getActivity(),chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H,
    				chbQS731_I,chbQS731_J,chbQS731_X,txtQS731_O);
		}else{
			if (MyUtil.incluyeRango(1,1,rgQSP730.getTagSelected("").toString())) {
				Util.lockView(getActivity(),false,chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H,
						chbQS731_I,chbQS731_J,chbQS731_X,chbQS731_Y);
				chbQS731_A.requestFocus();
			}
		}
    }

    public void pregunta709(){

    	if(seccion1.qs206!=null){
    		if((((seccion1.qs209==null || seccion1.qs209!=1) && seccion1.qs206!=1)) || preg717() && seccion1.qs206!=1){
        		entro=true;
        		Util.cleanAndLockView(this.getActivity(),rgQSP730,chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H,chbQS731_I,chbQS731_J,chbQS731_X,txtQS731_O,chbQS731_Y);
        		q1.setVisibility(view.GONE);
        		q2.setVisibility(view.GONE);
        	}else{
        		q1.setVisibility(view.VISIBLE);
        		q2.setVisibility(view.VISIBLE);
        		Util.lockView(getActivity(), false,rgQSP730,chbQS731_A,chbQS731_AA,chbQS731_AB,chbQS731_B,chbQS731_C,chbQS731_D,chbQS731_E,chbQS731_EA,chbQS731_F,chbQS731_G,chbQS731_H,chbQS731_I,chbQS731_J,chbQS731_X,chbQS731_Y);
        		rgQSP730.requestFocus();
        		onqrgQS730ChangeValue();
        	}	
    	}
    	
    }
    public boolean preg717(){
    	
      	 boolean band=!MyUtil.incluyeRango(1,1,salud.qs713) && !MyUtil.incluyeRango(1,1,salud.qs714) && !MyUtil.incluyeRango(1,1,salud.qs715) 
      			 && !MyUtil.incluyeRango(1,1,salud.qs716) && !MyUtil.incluyeRango(1,1,salud.qs717);
     
      	 
      	 return band;
      }
    
    public Seccion01Service getPersonaService()
    {
    	if(Personaservice == null)
    	{
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(salud);
		salud.qs731ah = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		salud.qs731am = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
		try {
			if (salud.qsp730 != null) {
				salud.qsp730 = salud.getConvert603(salud.qsp730);
			}
			if (salud.qs731_a!=null) {
				salud.qs731_a=salud.getConvertqs731(salud.qs731_a);
			}
			if (salud.qs731_aa!=null) {
				salud.qs731_aa=salud.getConvertqs731(salud.qs731_aa);
			}
			if (salud.qs731_ab!=null) {
				salud.qs731_ab=salud.getConvertqs731(salud.qs731_ab);
			}
			if (salud.qs731_b!=null) {
				salud.qs731_b=salud.getConvertqs731(salud.qs731_b);
			}
			if (salud.qs731_c!=null) {
				salud.qs731_c=salud.getConvertqs731(salud.qs731_c);
			}
			if (salud.qs731_d!=null) {
				salud.qs731_d=salud.getConvertqs731(salud.qs731_d);
			}
			if (salud.qs731_e!=null) {
				salud.qs731_e=salud.getConvertqs731(salud.qs731_e);
			}
			if (salud.qs731_ea!=null) {
				salud.qs731_ea=salud.getConvertqs731(salud.qs731_ea);
			}
			if (salud.qs731_f!=null) {
				salud.qs731_f=salud.getConvertqs731(salud.qs731_f);
			}
			if (salud.qs731_g!=null) {
				salud.qs731_g=salud.getConvertqs731(salud.qs731_g);
			}
			if (salud.qs731_h!=null) {
				salud.qs731_h=salud.getConvertqs731(salud.qs731_h);
			}
			if (salud.qs731_i!=null) {
				salud.qs731_i=salud.getConvertqs731(salud.qs731_i);
			}
			if (salud.qs731_j!=null) {
				salud.qs731_j=salud.getConvertqs731(salud.qs731_j);
			}
			
			if (salud.qs731_x!=null) {
				salud.qs731_x=salud.getConvertqs731(salud.qs731_x);
			}
			if (salud.qs731_y!=null) {
				salud.qs731_y=salud.getConvertqs731(salud.qs731_y);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(salud,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
