
package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_010 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS707; 
	
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQS708_A; 
	
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQS708_AA; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQS708_AB; 	
	
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQS708_B; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQS708_C; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQS708_D; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQS708_E;
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQS708_EA;
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQS708_F; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQS708_G; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQS708_H; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQS708_I; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQS708_J; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQS708_X; 
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQS708_O; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQS708_Y; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQS709; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQS710; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQS711; 
	CAP04_07 salud; 
	public TextField txtCabecera;
	private GridComponent2 grid_QS708; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lblsectorpublico,lblsectorprivado,lblhospital,lblong;
	private LabelComponent lblPreg707,lblPreg708,lblPreg709,lblPreg710E,lblPreg710,lblPreg711; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	String qs709fechref;
// 
	public CS_07Fragment_010() {} 
	public CS_07Fragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 

		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS707","QS708_A","QS708_AA","QS708_AB","QS708_B","QS708_C","QS708_D","QS708_E","QS708_EA","QS708_F","QS708_G","QS708_H","QS708_I","QS708_J","QS708_X","QS708_O","QS708_Y","QS709","QS709FECH_REF","QS710","QS711","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS707","QS708_A","QS708_AA","QS708_AB","QS708_B","QS708_C","QS708_D","QS708_E","QS708_EA","QS708_F","QS708_G","QS708_H","QS708_I","QS708_J","QS708_X","QS708_O","QS708_Y","QS709","QS709FECH_REF","QS710","QS711")}; 
		return rootView; 
	} 
  public void RenombrarEtiquetas()
  {
  	Calendar fechaactual = new GregorianCalendar();
  	
  	if(qs709fechref!=null)
	{			
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		try {
			date2 = df.parse(qs709fechref);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		fechaactual=cal;			
	} 
  	
  	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
  	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
  	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
  	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
  	
  	lblPreg709.setText(lblPreg709.getText().toString().replace("F1", F2)); 
  	lblPreg709.setText(lblPreg709.getText().toString().replace("F2", F1));
  	
  	String F3="";
	if(F1.equals("Diciembre")){
		F3 = "del a�o pasado";
	}else{
		F3 = "de este a�o";
	}
	lblPreg709.setText(lblPreg709.getText().toString().replace("F3", F3));

  }
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
	  lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
			
//		lblAlguienMas=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap01_07qs407_mas).textSize(21).centrar().negrita().alinearIzquierda();
	  Spanned texto = Html.fromHtml("707. �En los �ltimos 12 meses usted <b>ha recibido tratamiento</b> de alg�n profesional de salud por depresi�n, tristeza, des�nimo, falta de inter�s o irritabilidad ?"); 
      lblPreg707 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
      lblPreg707.setText(texto);
      
	  rgQS707=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs707_1,R.string.cap04_07qs707_2,R.string.cap04_07qs707_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS707ChangeValue"); 
	    
	  lblPreg708=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs708).textSize(18); 
		
		chbQS708_A=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_a, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS708_AA=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_aa, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS708_AB=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_ab, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS708_B=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_b, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_C=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_c, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_D=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_d, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_E=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_e, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS708_EA=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_ea, "0:1").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQS708_F=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_f, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_G=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_g, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_H=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_h, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_I=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_i, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQS708_J=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_j, "0:1").size(WRAP_CONTENT, WRAP_CONTENT); 

		chbQS708_X=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_x, "0:1").size(altoComponente+10,290).callback("onqrgQS708_XChangeValue"); 
		txtQS708_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400);
		chbQS708_Y=new CheckBoxField(this.getActivity(), R.string.cap04_07qs708_y, "0:1").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQS708_YChangeValue");
		
		lblsectorpublico = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_a).textSize(18).negrita().alinearIzquierda().size(80, WRAP_CONTENT);
		lblsectorprivado = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_b).textSize(18).negrita().alinearIzquierda();
		lblhospital = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs101_h).textSize(18);
		lblong = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_c).textSize(18).negrita().alinearIzquierda();
//		lblcampa = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs303_d).textSize(18).negrita().alinearIzquierda();
		
		lblPreg709 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs709);
		
		rgQS709=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs709_1, R.string.cap04_07qs709_2).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP709ChangeValue"); 
		
		lblPreg710E = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs710_encabezado).negrita().alinearIzquierda();
	
		lblPreg710 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs710);
		
		rgQS710=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs710_1,R.string.cap04_07qs710_2,R.string.cap04_07qs710_3,R.string.cap04_07qs710_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		lblPreg711 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs711);
		
		rgQS711=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs711_1,R.string.cap04_07qs711_2,R.string.cap04_07qs711_3,R.string.cap04_07qs711_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
    
		 grid_QS708 = new GridComponent2(this.getActivity(),App.ESTILO,2);
		    
		    grid_QS708.addComponent(lblsectorpublico,2,1);
		    grid_QS708.addComponent(lblhospital,2,1);
		    grid_QS708.addComponent(chbQS708_A,2,1);
		    grid_QS708.addComponent(chbQS708_AA,2,1);
		    grid_QS708.addComponent(chbQS708_AB,2,1);
		    grid_QS708.addComponent(chbQS708_B,2,1);
		    grid_QS708.addComponent(chbQS708_C,2,1);
		    grid_QS708.addComponent(chbQS708_D,2,1);
		    grid_QS708.addComponent(chbQS708_E,2,1);
		    grid_QS708.addComponent(chbQS708_EA,2,1);
		    grid_QS708.addComponent(chbQS708_F,2,1);
		    
		    grid_QS708.addComponent(lblsectorprivado,2,1);
		    grid_QS708.addComponent(chbQS708_G,2,1);
		    grid_QS708.addComponent(chbQS708_H,2,1);
		    
		    grid_QS708.addComponent(lblong,2,1);
		    grid_QS708.addComponent(chbQS708_I,2,1);
		    grid_QS708.addComponent(chbQS708_J,2,1);
		    grid_QS708.addComponent(chbQS708_X);
		    grid_QS708.addComponent(txtQS708_O);
		    grid_QS708.addComponent(chbQS708_Y,2,1);
		

		
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg707,rgQS707);
//		q8 = createQuestionSection(R.string.cap04_07qs708_a,Gravity.LEFT|Gravity.CENTER_VERTICAL,chbQS708_A,chbQS708_B,chbQS708_C,chbQS708_D,chbQS708_E,chbQS708_F,chbQS708_G,chbQS708_H,chbQS708_I,chbQS708_J,chbQS708_X); 
//		q9 = createQuestionSection(R.string.cap04_07qs708_y,Gravity.LEFT|Gravity.CENTER_VERTICAL,chbQS708_Y); 
		LinearLayout q3_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,grid_QS708.component());
		q2 = createQuestionSection(lblPreg708,q3_2);
		q3 = createQuestionSection(lblPreg709,rgQS709); 
		q6 = createQuestionSection(lblPreg710E); 
		q4 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg710,rgQS710); 
		q5 = createQuestionSection(0, Gravity.LEFT, LinearLayout.VERTICAL,lblPreg711,rgQS711); 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q6);
		form.addView(q4); 
		form.addView(q5); 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(salud); 
		if (salud.qs707 != null) {
			salud.qs707 = salud.getConvert603(salud.qs707);
		}
		if (salud.qs708_a!=null) {
			salud.qs708_a=salud.getConvertqs708(salud.qs708_a);
		}
		if (salud.qs708_aa!=null) {
			salud.qs708_aa=salud.getConvertqs708(salud.qs708_aa);
		}
		if (salud.qs708_ab!=null) {
			salud.qs708_ab=salud.getConvertqs708(salud.qs708_ab);
		}
		if (salud.qs708_b!=null) {
			salud.qs708_b=salud.getConvertqs708(salud.qs708_b);
		}
		if (salud.qs708_c!=null) {
			salud.qs708_c=salud.getConvertqs708(salud.qs708_c);
		}
		if (salud.qs708_d!=null) {
			salud.qs708_d=salud.getConvertqs708(salud.qs708_d);
		}
		if (salud.qs708_e!=null) {
			salud.qs708_e=salud.getConvertqs708(salud.qs708_e);
		}
		if (salud.qs708_ea!=null) {
			salud.qs708_ea=salud.getConvertqs708(salud.qs708_ea);
		}
		if (salud.qs708_f!=null) {
			salud.qs708_f=salud.getConvertqs708(salud.qs708_f);
		}
		if (salud.qs708_g!=null) {
			salud.qs708_g=salud.getConvertqs708(salud.qs708_g);
		}
		if (salud.qs708_h!=null) {
			salud.qs708_h=salud.getConvertqs708(salud.qs708_h);
		}
		if (salud.qs708_i!=null) {
			salud.qs708_i=salud.getConvertqs708(salud.qs708_i);
		}
		if (salud.qs708_j!=null) {
			salud.qs708_j=salud.getConvertqs708(salud.qs708_j);
		}
		
		if (salud.qs708_x!=null) {
			salud.qs708_x=salud.getConvertqs708(salud.qs708_x);
		}
		if (salud.qs708_y!=null) {
			salud.qs708_y=salud.getConvertqs708(salud.qs708_y);
		}
		
		if (salud.qs709!=null) {
		if(qs709fechref == null) {
			salud.qs709fech_ref =  Util.getFechaActualToString();
			}
		else {
			salud.qs709fech_ref = qs709fechref;
			 }
		}
		else {
			salud.qs709fech_ref = null;
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(salud.qs707)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.707"); 
			view = rgQS707; 
			error = true; 
			return false; 
		} 
//		if (!Util.alMenosUnoEsDiferenteA(0,salud.qs708_a,salud.qs708_b,salud.qs708_c,salud.qs708_d,salud.qs708_e,salud.qs708_f,salud.qs708_g,salud.qs708_h,salud.qs708_i,salud.qs708_j,salud.qs708_x)) { 
//			mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
//			view = chbQS708_A; 
//			error = true; 
//			return false; 
//		} 
		if (MyUtil.incluyeRango(1,1,salud.qs707.toString())) {
			if (!Util.alMenosUnoEsDiferenteA(0,salud.qs708_a,salud.qs708_aa,salud.qs708_ab,salud.qs708_b,salud.qs708_c,salud.qs708_d,salud.qs708_e,salud.qs708_ea,salud.qs708_f,salud.qs708_g,salud.qs708_h,salud.qs708_i,salud.qs708_j,salud.qs708_x,salud.qs708_y)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQS708_A; 
				error = true; 
				return false; 
			} 
		}
		if (MyUtil.incluyeRango(1,1,salud.qs708_x.toString())) {
			if (Util.esVacio(salud.qs708_o)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.708_O"); 
				view = txtQS708_O; 
				error = true; 
				return false; 
			} 
		}
		
		
		if (Util.esVacio(salud.qs709)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.709"); 
			view = rgQS709; 
			error = true; 
			return false; 
		} 
		if (MyUtil.incluyeRango(1,1, rgQS709.getTagSelected("").toString())) {
			if (Util.esVacio(salud.qs710)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.710"); 
				view = rgQS710; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs711)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.711"); 
				view = rgQS711; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		
		if(salud==null){ 
		  salud=new CAP04_07(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		if (salud.qs707 != null) {
			salud.qs707 = salud.setConvert603(salud.qs707);
		}
		
		if (salud.qs708_a!=null) {
    		salud.qs708_a=salud.setConvertqs708(salud.qs708_a);    
    	}
		if (salud.qs708_aa!=null) {
    		salud.qs708_aa=salud.setConvertqs708(salud.qs708_aa);    
    	}	
		if (salud.qs708_ab!=null) {
    		salud.qs708_ab=salud.setConvertqs708(salud.qs708_ab);    
    	}	
		if (salud.qs708_b!=null) {
    		salud.qs708_b=salud.setConvertqs708(salud.qs708_b);    
    	}		
		if (salud.qs708_c!=null) {
    		salud.qs708_c=salud.setConvertqs708(salud.qs708_c);    
    	}
		if (salud.qs708_d!=null) {
    		salud.qs708_d=salud.setConvertqs708(salud.qs708_d);    
    	}
		if (salud.qs708_e!=null) {
    		salud.qs708_e=salud.setConvertqs708(salud.qs708_e);    
    	}
		if (salud.qs708_ea!=null) {
    		salud.qs708_ea=salud.setConvertqs708(salud.qs708_ea);    
    	}
		if (salud.qs708_f!=null) {
    		salud.qs708_f=salud.setConvertqs708(salud.qs708_f);    
    	}
		if (salud.qs708_g!=null) {
    		salud.qs708_g=salud.setConvertqs708(salud.qs708_g);    
    	}
		if (salud.qs708_h!=null) {
    		salud.qs708_h=salud.setConvertqs708(salud.qs708_h);    
    	}
		if (salud.qs708_i!=null) {
    		salud.qs708_i=salud.setConvertqs708(salud.qs708_i);    
    	}
		if (salud.qs708_j!=null) {
    		salud.qs708_j=salud.setConvertqs708(salud.qs708_j);    
    	}
		
		if (salud.qs708_x!=null) {
    		salud.qs708_x=salud.setConvertqs708(salud.qs708_x);    
    	}
		if (salud.qs708_y!=null) {
    		salud.qs708_y=salud.setConvertqs708(salud.qs708_y);    
    	}
		entityToUI(salud); 
		qs709fechref = salud.qs709fech_ref;		
		inicio(); 
    } 
    private void inicio() { 
         
    	ValidarsiesSupervisora();
         onqrgQS707ChangeValue();
         onqrgQS708_XChangeValue();
         onqrgQS708_YChangeValue();
         onP709ChangeValue();
         RenombrarEtiquetas();
//         rgQS707.requestFocus();
         txtCabecera.requestFocus();
         
    } 
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS707.readOnly(); 
				chbQS708_A.readOnly();
				chbQS708_AA.readOnly();
				chbQS708_AB.readOnly();
				chbQS708_B.readOnly();
				chbQS708_C.readOnly();
				chbQS708_D.readOnly();
				chbQS708_E.readOnly();
				chbQS708_EA.readOnly();
				chbQS708_F.readOnly();
				chbQS708_G.readOnly();
				chbQS708_H.readOnly();
				chbQS708_I.readOnly();
				chbQS708_J.readOnly();
				chbQS708_X.readOnly();
				chbQS708_Y.readOnly();
				txtQS708_O.readOnly();
				rgQS709.readOnly();
				rgQS710.readOnly();
				rgQS711.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onqrgQS707ChangeValue(){
    
    	if (MyUtil.incluyeRango(2,3,rgQS707.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(),chbQS708_A,chbQS708_AA,chbQS708_AB,chbQS708_B,chbQS708_C,chbQS708_D,chbQS708_E,chbQS708_EA,chbQS708_F,chbQS708_G,chbQS708_H);
			Util.cleanAndLockView(this.getActivity(),chbQS708_I,chbQS708_J,chbQS708_X,chbQS708_Y);
			Util.cleanAndLockView(this.getActivity(),txtQS708_O);
			q2.setVisibility(view.GONE);
		} 
		else {		
			q2.setVisibility(view.VISIBLE);
			Util.lockView(getActivity(),false,chbQS708_A,chbQS708_AA,chbQS708_AB,chbQS708_B,chbQS708_C,chbQS708_D,chbQS708_E,chbQS708_EA,chbQS708_F,chbQS708_G,chbQS708_H);
			Util.lockView(getActivity(),false,chbQS708_I,chbQS708_J,chbQS708_X,chbQS708_Y);
			chbQS708_A.requestFocus();
		}
    }
    
    public void onqrgQS708_XChangeValue(){
    	if (MyUtil.incluyeRango(1,1,rgQS707.getTagSelected("").toString())) {
    		if (MyUtil.incluyeRango(0,0,chbQS708_X.getValue().toString())) {
    			Util.lockView(getActivity(),false,txtQS708_O);
    			txtQS708_O.requestFocus();
    		}else{
    			Util.cleanAndLockView(this.getActivity(),txtQS708_O);
        		chbQS708_X.requestFocus();
    		}			
		}
    }
    public void onqrgQS708_YChangeValue(){
    	
    	if (MyUtil.incluyeRango(0,0,chbQS708_Y.getValue().toString())) {
    		Util.cleanAndLockView(this.getActivity(),chbQS708_A,chbQS708_AA,chbQS708_AB,chbQS708_B,chbQS708_C,chbQS708_D,chbQS708_E,chbQS708_EA,chbQS708_F,chbQS708_G,chbQS708_H,
    				                                  chbQS708_I,chbQS708_J,chbQS708_X,txtQS708_O);
		}else{
			if (MyUtil.incluyeRango(1,1,rgQS707.getTagSelected("").toString())) {
				Util.lockView(getActivity(),false,chbQS708_A,chbQS708_AA,chbQS708_AB,chbQS708_B,chbQS708_C,chbQS708_D,chbQS708_E,chbQS708_EA,chbQS708_F,chbQS708_G,chbQS708_H,
						 chbQS708_I,chbQS708_J,chbQS708_X,chbQS708_Y);
				chbQS708_A.requestFocus();
			}
		}
    }
    public void onP709ChangeValue() {
		if (MyUtil.incluyeRango(2,2, rgQS709.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQS710,rgQS711);	
			q6.setVisibility(view.GONE);
			q4.setVisibility(view.GONE);
			q5.setVisibility(view.GONE);
		}		
		else {	
			    q6.setVisibility(view.VISIBLE);
			    q4.setVisibility(view.VISIBLE);
			    q5.setVisibility(view.VISIBLE);
				Util.lockView(getActivity(), false,rgQS710,rgQS711);
				rgQS710.requestFocus();	
		}
		
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(salud);
		try {
			if (salud.qs707 != null) {
				salud.qs707 = salud.getConvert603(salud.qs707);
			}
			if (salud.qs708_a!=null) {
				salud.qs708_a=salud.getConvertqs708(salud.qs708_a);
			}
			if (salud.qs708_aa!=null) {
				salud.qs708_aa=salud.getConvertqs708(salud.qs708_aa);
			}
			if (salud.qs708_ab!=null) {
				salud.qs708_ab=salud.getConvertqs708(salud.qs708_ab);
			}
			if (salud.qs708_b!=null) {
				salud.qs708_b=salud.getConvertqs708(salud.qs708_b);
			}
			if (salud.qs708_c!=null) {
				salud.qs708_c=salud.getConvertqs708(salud.qs708_c);
			}
			if (salud.qs708_d!=null) {
				salud.qs708_d=salud.getConvertqs708(salud.qs708_d);
			}
			if (salud.qs708_e!=null) {
				salud.qs708_e=salud.getConvertqs708(salud.qs708_e);
			}
			if (salud.qs708_ea!=null) {
				salud.qs708_ea=salud.getConvertqs708(salud.qs708_ea);
			}
			if (salud.qs708_f!=null) {
				salud.qs708_f=salud.getConvertqs708(salud.qs708_f);
			}
			if (salud.qs708_g!=null) {
				salud.qs708_g=salud.getConvertqs708(salud.qs708_g);
			}
			if (salud.qs708_h!=null) {
				salud.qs708_h=salud.getConvertqs708(salud.qs708_h);
			}
			if (salud.qs708_i!=null) {
				salud.qs708_i=salud.getConvertqs708(salud.qs708_i);
			}
			if (salud.qs708_j!=null) {
				salud.qs708_j=salud.getConvertqs708(salud.qs708_j);
			}
			
			if (salud.qs708_x!=null) {
				salud.qs708_x=salud.getConvertqs708(salud.qs708_x);
			}
			if (salud.qs708_y!=null) {
				salud.qs708_y=salud.getConvertqs708(salud.qs708_y);
			}
			
			if (salud.qs709!=null) {
				if(qs709fechref == null) {
					salud.qs709fech_ref =  Util.getFechaActualToString();
					}
				else {
					salud.qs709fech_ref = qs709fechref;
					 }
				}
				else {
					salud.qs709fech_ref = null;
				}
			
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(salud,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
