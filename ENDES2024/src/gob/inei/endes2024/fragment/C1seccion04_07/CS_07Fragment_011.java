package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_011 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS713; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS714; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS715; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS716; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS717; 
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lbl713,lblPreg713,lblPreg713_1,lblEncabezadoPreg713;
	private LabelComponent lblPreg714,lblPreg715,lblPreg716,lblPreg717;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	boolean entro=false;
	
	String qs713fechref;
// 
	public CS_07Fragment_011() {} 
	public CS_07Fragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS709","QS713","QS713FECH_REF","QS714","QS715","QS716","QS717","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS713","QS713FECH_REF","QS714","QS715","QS716","QS717")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS206","QS209")};
        
		return rootView; 
	} 
  public void RenombrarEtiquetas()
  {
  	Calendar fechaactual = new GregorianCalendar();
  	
  	if(qs713fechref!=null)
	{			
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		try {
			date2 = df.parse(qs713fechref);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		fechaactual=cal;			
	} 
  	
  	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
  	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
  	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
  	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
  	lblPreg713_1.text(R.string.cap04_07qs713s);
  	lblPreg713_1.setText(lblPreg713_1.getText().toString().replace("F1", F2)); 
  	lblPreg713_1.setText(lblPreg713_1.getText().toString().replace("F2", F1));
  	
  	String F3="";
	if(F1.equals("Diciembre")){
		F3 = "del a�o pasado";
	}else{
		F3 = "de este a�o";
	}
	lblPreg713_1.setText(lblPreg713_1.getText().toString().replace("F3", F3));
  	
  	String texto2=""+lblPreg713_1.getText().toString();
  	Spanned texto = Html.fromHtml(texto2+" "+"<b> hubo momentos</b> en que la bebida o la resaca le dificultaron realizar sus actividades o sus responsabilidades en los estudios, en el trabajo o en la casa?");
  	
  	lblPreg713_1.setText(texto);

  }
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		lblEncabezadoPreg713 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs713_encabezado).negrita().alinearIzquierda();
		
		
		
		lblPreg713 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs713);
		lblPreg713_1 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs713s);
		lbl713=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs713desc).textSize(17); 
		
		Spanned texto = Html.fromHtml("714. �En los �ltimos 12 meses, <b>hubo momentos</b> en que el consumo de alcohol le <b>provoc�</b> discusiones u otros <b>problemas</b> con su <b>familia, amigos, vecinos o compa�eros de trabajo?</b>");
		lblPreg714 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg714.setText(texto);
		lblPreg715 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs715);; 
		Spanned texto2 = Html.fromHtml("716. �En los �ltimos 12 meses, <b>hubo momentos</b> en que estuvo bajo los efectos del alcohol en situaciones en las que pod�a hacerse da�o, por ejemplo andando en bicicleta, manejando auto, manejando una m�quina o en cualquier otra situaci�n?");
		Spanned texto3 = Html.fromHtml("717. �Le han sancionado <b>m�s de una vez</b> por alterar el orden o manejar bajo los efectos del alcohol?");
		
		lblPreg716 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg716.setText(texto2);
		lblPreg717 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg717.setText(texto3);
		
		rgQS713=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs713_1,R.string.cap04_07qs713_2,R.string.cap04_07qs713_3,R.string.cap04_07qs713_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS714=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs714_1,R.string.cap04_07qs714_2,R.string.cap04_07qs714_3,R.string.cap04_07qs714_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP714ChangeValue"); 
		rgQS715=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs715_1,R.string.cap04_07qs715_2,R.string.cap04_07qs715_3,R.string.cap04_07qs715_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS716=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs716_1,R.string.cap04_07qs716_2,R.string.cap04_07qs716_3,R.string.cap04_07qs716_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS717=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs717_1,R.string.cap04_07qs717_2,R.string.cap04_07qs717_3,R.string.cap04_07qs717_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		RenombrarEtiquetas();
  } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblEncabezadoPreg713); 
		q4 = createQuestionSection(lblPreg713,lblPreg713_1,lbl713,rgQS713); 
		q5 = createQuestionSection(lblPreg714,rgQS714); 
		q6 = createQuestionSection(lblPreg715,rgQS715); 
		q7 = createQuestionSection(lblPreg716,rgQS716); 
		q8 = createQuestionSection(lblPreg717,rgQS717); 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
//		form.addView(q2); 
//		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		if (cap04_07.qs713 != null) {
			cap04_07.qs713 = cap04_07.getConvert713(cap04_07.qs713);
			
			if(qs713fechref == null) {
				cap04_07.qs713fech_ref =  Util.getFechaActualToString();
				}
			else {
				cap04_07.qs713fech_ref = qs713fechref;
				 }			
		}
		else {
			cap04_07.qs713fech_ref = null;
		}
		if (cap04_07.qs714 != null) {
			cap04_07.qs714 = cap04_07.getConvert713(cap04_07.qs714);
		}
		if (cap04_07.qs715 != null) {
			cap04_07.qs715 = cap04_07.getConvert713(cap04_07.qs715);
		}
		if (cap04_07.qs716 != null) {
			cap04_07.qs716 = cap04_07.getConvert713(cap04_07.qs716);
		}
		if (cap04_07.qs717 != null) {
			cap04_07.qs717 = cap04_07.getConvert713(cap04_07.qs717);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(seccion1.qs209==null){
			
		}
//		Log.e("","entro s7="+entro);
		if(entro==false){
			if (Util.esVacio(cap04_07.qs713)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.713"); 
				view = rgQS713; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs714)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.714"); 
				view = rgQS714; 
				error = true; 
				return false; 
			} 
			if (MyUtil.incluyeRango(1,1, rgQS714.getTagSelected("").toString())) {
				if (Util.esVacio(cap04_07.qs715)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.715"); 
					view = rgQS715; 
					error = true; 
					return false; 
				} 
			}
			
			if (Util.esVacio(cap04_07.qs716)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.716"); 
				view = rgQS716; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs717)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.717"); 
				view = rgQS717; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersona().id, App.getInstance().getPersona().hogar_id, App.getInstance().getPersona().persona_id,seccionesCargado); 
    	
    	cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	
    	entro=false;
    	
    	if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
    	if (cap04_07.qs713 != null) {
			cap04_07.qs713 = cap04_07.setConvert713(cap04_07.qs713);
		}
    	if (cap04_07.qs714 != null) {
			cap04_07.qs714 = cap04_07.setConvert713(cap04_07.qs714);
		}
    	if (cap04_07.qs715 != null) {
			cap04_07.qs715 = cap04_07.setConvert713(cap04_07.qs715);
		}
    	if (cap04_07.qs716 != null) {
			cap04_07.qs716 = cap04_07.setConvert713(cap04_07.qs716);
		}
    	if (cap04_07.qs717 != null) {
			cap04_07.qs717 = cap04_07.setConvert713(cap04_07.qs717);
		}
		entityToUI(cap04_07); 
		qs713fechref = cap04_07.qs713fech_ref;	
		inicio(); 
    } 
    private void inicio() { 
    	 ValidarsiesSupervisora();
    	pregunta709();
//    	rgQS713.requestFocus();
//    	App.getInstance().setPersona_04_07(null);
//    	App.getInstance().setPersona_04_07(cap04_07);
//    	onP714ChangeValue();
    	txtCabecera.requestFocus();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS713.readOnly(); 
				rgQS714.readOnly();
				rgQS715.readOnly();
				rgQS716.readOnly(); 
				rgQS717.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onP714ChangeValue() {

		if (MyUtil.incluyeRango(2,4, rgQS714.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQS715);	
			q6.setVisibility(view.GONE);
		}		
		else {	
			    q6.setVisibility(view.VISIBLE);
				Util.lockView(getActivity(), false,rgQS715);
				rgQS715.requestFocus();	
		}
		
	}
    
    public void pregunta709(){
    	
//    	if(seccion1.qs209==null){
//    		entro=true;
//    	}else{
//    	    if(seccion1.qs209!=null){
    	       if(seccion1.qs209==null || seccion1.qs209!=1){
        			entro=true;
            		Util.cleanAndLockView(this.getActivity(),rgQS713,rgQS714,rgQS715,rgQS716,rgQS717);
            		q4.setVisibility(view.GONE);
            		q5.setVisibility(view.GONE);
            		q6.setVisibility(view.GONE);
            		q7.setVisibility(view.GONE);
            		q8.setVisibility(view.GONE);
            	}else{
            		entro=false;
            		q4.setVisibility(view.VISIBLE);
            		q5.setVisibility(view.VISIBLE);
            		q6.setVisibility(view.VISIBLE);
            		q7.setVisibility(view.VISIBLE);
            		q8.setVisibility(view.VISIBLE);
            		Util.lockView(getActivity(), false,rgQS713,rgQS714,rgQS715,rgQS716,rgQS717);
            		onP714ChangeValue();
            	}
//    	    }
    		
//    	}
    	
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs713 != null) {
				cap04_07.qs713 = cap04_07.getConvert713(cap04_07.qs713);
				if(qs713fechref == null) {
					cap04_07.qs713fech_ref =  Util.getFechaActualToString();
					}
				else {
					cap04_07.qs713fech_ref = qs713fechref;
					 }			
			}
			else {
				cap04_07.qs713fech_ref = null;
			}
			if (cap04_07.qs714 != null) {
				cap04_07.qs714 = cap04_07.getConvert713(cap04_07.qs714);
			}
			if (cap04_07.qs715 != null) {
				cap04_07.qs715 = cap04_07.getConvert713(cap04_07.qs715);
			}
			if (cap04_07.qs716 != null) {
				cap04_07.qs716 = cap04_07.getConvert713(cap04_07.qs716);
			}
			if (cap04_07.qs717 != null) {
				cap04_07.qs717 = cap04_07.getConvert713(cap04_07.qs717);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
