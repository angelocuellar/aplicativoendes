package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_009 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS703; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQS704PRV; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS704A; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS704B; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS704C; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS704D; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS704E; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS704F; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS704G; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQS704H; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQS704I; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQS706; 
	
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lblblanco,lbl704,lbl704_11,lbl704_12,lbl704_13,lbl704_14;
	private LabelComponent lblPreg703,lblPreg704,lblPreg706;
	private LabelComponent lblP704_1,lblP704_2,lblP704_3,lblP704_4; 
	public LabelComponent lblQS704A,lblQS704B,lblQS704C,lblQS704D,lblQS704E,lblQS704F,lblQS704G,lblQS704H,lblQS704I;
	public LabelComponent lblQS704A2,lblQS704B2,lblQS704C2,lblQS704D2,lblQS704E2,lblQS704F2,lblQS704G2,lblQS704H2,lblQS704I2;
	public GridComponent2 grid1,grid2,grid3,grid4,grid5,grid6,grid7,grid8,grid9,grid10,grid11;

	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	LinearLayout q12; 
	LinearLayout q13; 
	LinearLayout q14; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	String qs703fechref;
// 
	public CS_07Fragment_009() {} 
	public CS_07Fragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS703","QS703FECH_REF","QS704PRV","QS704A","QS704B","QS704C","QS704D","QS704E","QS704F","QS704G","QS704H","QS704I","QS706","QS707","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS703","QS703FECH_REF","QS704PRV","QS704A","QS704B","QS704C","QS704D","QS704E","QS704F","QS704G","QS704H","QS704I","QS706","QS707")}; 
		return rootView; 
	} 
  public void RenombrarEtiquetas()
  {
  	Calendar fechaactual = new GregorianCalendar();
  	
  	if(qs703fechref!=null)
	{			
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		try {
			date2 = df.parse(qs703fechref);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		fechaactual=cal;			
	} 
  	
  	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
  	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
  	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
  	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
  	
  	lblPreg703.setText(lblPreg703.getText().toString().replace("F1", F2)); 
  	lblPreg703.setText(lblPreg703.getText().toString().replace("F2", F1));
  	
  	String F3="";
	if(F1.equals("Diciembre")){
		F3 = "del a�o pasado";
	}else{
		F3 = "de este a�o";
	}
	lblPreg703.setText(lblPreg703.getText().toString().replace("F3", F3));
  	
  }
  
	  @Override 
	  protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		lblPreg703 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs703);
		
		rgQS703=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs703_1, R.string.cap04_07qs703_2).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP703ChangeValue"); 
		lblPreg704 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs704prv);
		lbl704 = new LabelComponent(getActivity()).textSize(18).size(60, 160).text(R.string.cap04_07qs704_1).alinearIzquierda();
		txtQS704PRV = new TextField(this.getActivity()).maxLength(250).size(altoComponente, 600).callback("onP704PRVChangeValue");
		
		lbl704_11 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsPreg_704_1);
		lbl704_12 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsPreg_704_2).negrita();
		
		Spanned texto = Html.fromHtml("SI RESPONDE \"S&Iacute;\", PREGUNTE: <b> �Cu&aacute;ntos d&iacute;as? </b>"); 
		lbl704_13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lbl704_13.setText(texto);
		lbl704_14 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qsPreg_704_3);
		
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(180, 360).negrita().centrar();
		lblP704_1 = new LabelComponent(getActivity()).textSize(16).size(180, 80).text(R.string.cap04_07qs700a_1).centrar();
		lblP704_2 = new LabelComponent(getActivity()).textSize(16).size(180, 90).text(R.string.cap04_07qs700a_2).centrar();
		lblP704_3 = new LabelComponent(getActivity()).textSize(16).size(180, 110).text(R.string.cap04_07qs700a_3).centrar();
		lblP704_4 = new LabelComponent(getActivity()).textSize(16).size(180, 110).text(R.string.cap04_07qs700a_4).centrar();
				
		Spanned textoA = Html.fromHtml("<b>A.</b> �Ha tenido pocas ganas o inter�s en hacer las cosas? (DE SER NECESARIO LEA: Es decir, no disfruta sus actividades cotidianas)"); 
		Spanned textoA2 = Html.fromHtml("<b>A.</b> POCO INTERES��............................"); 
		
		lblQS704A=new LabelComponent(getActivity()).textSize(17).size(150, 360);	lblQS704A.setText(textoA);	
		lblQS704A2=new LabelComponent(getActivity()).textSize(17).size(55, 200);	lblQS704A2.setText(textoA2);	
		rgQS704A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(95,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704AChangeValue"); 
		
		Spanned textoB = Html.fromHtml("<b>B.</b> �Se ha sentido desanimada(o), deprimida(o), triste o sin esperanza?"); 
		Spanned textoB2 = Html.fromHtml("<b>B.</b> DEPRIMIDA(O)����........................"); 
		
		lblQS704B=new LabelComponent(getActivity()).textSize(17).size(120, 360);lblQS704B.setText(textoB);		
		lblQS704B2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS704B2.setText(textoB2);		
		rgQS704B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704BChangeValue");  
		
		Spanned textoC = Html.fromHtml("<b>C.</b> �Ha tenido problemas para dormir o mantenerse dormida(o), o en dormir demasiado?"); 
		Spanned textoC2 = Html.fromHtml("<b>C.</b> DORMIR�������.........................."); 
		
		lblQS704C=new LabelComponent(getActivity()).textSize(17).size(130, 360);lblQS704C.setText(textoC);
		lblQS704C2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS704C2.setText(textoC2);	
		rgQS704C=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704CChangeValue");  
		
		Spanned textoD = Html.fromHtml("<b>D.</b> �Se ha sentido cansada(o) o ha tenido poca energ�a sin motivo que lo justifique?"); 
		Spanned textoD2 = Html.fromHtml("<b>D.</b> CANSADA(O)������...................."); 
		
		lblQS704D=new LabelComponent(getActivity()).textSize(17).size(130, 360);lblQS704D.setText(textoD);
		lblQS704D2=new LabelComponent(getActivity()).textSize(17).size(50, 200);lblQS704D2.setText(textoD2);	
		rgQS704D=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704DChangeValue");  
		
		Spanned textoE = Html.fromHtml("<b>E.</b> �Ha tenido poco apetito o ha comido en exceso?"); 
		Spanned textoE2 = Html.fromHtml("<b>E.</b> APETITO�������........................."); 
		
		lblQS704E=new LabelComponent(getActivity()).textSize(17).size(90, 360);lblQS704E.setText(textoE);
		lblQS704E2=new LabelComponent(getActivity()).textSize(17).size(45, 200);lblQS704E2.setText(textoE2);
		rgQS704E=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(altoComponente,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704EChangeValue");  
		
		Spanned textoF = Html.fromHtml("<b>F.</b> �Ha tenido dificultad para poner atenci�n o concentrarse en las cosas que hace?  (DE SER NECESARIO LEA: Como leer el peri�dico, ver televisi�n, escuchar atentamente la radio o conversar con otras personas)"); 
		Spanned textoF2 = Html.fromHtml("<b>F.</b> PONER ATENCI�N����.................."); 
		
		lblQS704F=new LabelComponent(getActivity()).textSize(17).size(200, 360);lblQS704F.setText(textoF);
		lblQS704F2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS704F2.setText(textoF2);	
		rgQS704F=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(145,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704FChangeValue"); 
		
		Spanned textoG = Html.fromHtml("<b>G.</b> �Se ha movido o hablado m�s lento de lo normal o se ha sentido m�s inquieta(o) o intranquila(o) de lo normal?"); 
		Spanned textoG2 = Html.fromHtml("<b>G.</b> MOVERSE�����..........................."); 
		
		lblQS704G=new LabelComponent(getActivity()).textSize(17).size(165, 360);lblQS704G.setText(textoG);
		lblQS704G2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS704G2.setText(textoG2);	
		rgQS704G=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(110,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704GChangeValue"); 
		
		Spanned textoH = Html.fromHtml("<b>H.</b> �Ha tenido pensamientos de que ser�a mejor estar muerta(o) o que quisiera hacerse da�o de alguna forma buscando morir?"); 
		Spanned textoH2 = Html.fromHtml("<b>H.</b> MORIR���������........................"); 
		
		lblQS704H=new LabelComponent(getActivity()).textSize(17).size(165, 360);lblQS704H.setText(textoH);
		lblQS704H2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS704H2.setText(textoH2);
		rgQS704H=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(110,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704HChangeValue"); 
		
		Spanned textoI = Html.fromHtml("<b>I.</b> �Se ha sentido mal acerca de si misma(o) o ha sentido que es una(un) fracasada(o) o que se ha fallado a s� misma(o) o a su familia?"); 
		Spanned textoI2 = Html.fromHtml("<b>I.</b> SENTIRSE MAL������.................."); 
		
		lblQS704I=new LabelComponent(getActivity()).textSize(17).size(135, 360);lblQS704I.setText(textoI);
		lblQS704I2=new LabelComponent(getActivity()).textSize(17).size(55, 200);lblQS704I2.setText(textoI2);	
		rgQS704I=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07radio_1,R.string.cap04_07radio_2,R.string.cap04_07radio_3,R.string.cap04_07radio_4).size(80,400).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP704IChangeValue"); 
		
		lblPreg706 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs706);
		
		rgQS706=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs706_1,R.string.cap04_07qs706_2,R.string.cap04_07qs706_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		grid1 = new GridComponent2(this.getActivity(),App.ESTILO,5,1);
		grid1.addComponent(lblblanco);
		grid1.addComponent(lblP704_1);
		grid1.addComponent(lblP704_2);
		grid1.addComponent(lblP704_3);
		grid1.addComponent(lblP704_4);
		
		grid2 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid2.addComponent(lblQS704A,1,2);
		grid2.addComponent(lblQS704A2);
		grid2.addComponent(rgQS704A);
		
		grid3 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid3.addComponent(lblQS704B,1,2);
		grid3.addComponent(lblQS704B2);
		grid3.addComponent(rgQS704B);
		
		grid4 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid4.addComponent(lblQS704C,1,2);
		grid4.addComponent(lblQS704C2);
		grid4.addComponent(rgQS704C);
		
		grid5 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid5.addComponent(lblQS704D,1,2);
		grid5.addComponent(lblQS704D2);
		grid5.addComponent(rgQS704D);
		
		grid6 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid6.addComponent(lblQS704E,1,2);
		grid6.addComponent(lblQS704E2);
		grid6.addComponent(rgQS704E);
		
		grid7 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid7.addComponent(lblQS704F,1,2);
		grid7.addComponent(lblQS704F2);
		grid7.addComponent(rgQS704F);
		
		grid8 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid8.addComponent(lblQS704G,1,2);
		grid8.addComponent(lblQS704G2);
		grid8.addComponent(rgQS704G);
		
		grid9 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid9.addComponent(lblQS704H,1,2);
		grid9.addComponent(lblQS704H2);
		grid9.addComponent(rgQS704H);
		
		grid10 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid10.addComponent(lblQS704I,1,2);
		grid10.addComponent(lblQS704I2);
		grid10.addComponent(rgQS704I);
		
		grid11 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		grid11.addComponent(lbl704);
		grid11.addComponent(txtQS704PRV);		
	  } 
	  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion);
		q1 = createQuestionSection(lblPreg703,rgQS703);
		q2 = createQuestionSection(0,lblPreg704,grid11.component(),lbl704_11,lbl704_12,lbl704_13,lbl704_14,grid1.component(),grid2.component(),grid3.component(),grid4.component(),grid5.component(),
				grid6.component(),grid7.component(),grid8.component(),grid9.component(),grid10.component()); 
		q3 = createQuestionSection(lblPreg706,rgQS706); 
		 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
    return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		
		if(cap04_07.qs703 != null) {
			if(qs703fechref == null) {
				cap04_07.qs703fech_ref =  Util.getFechaActualToString();
				}
			else {
				cap04_07.qs703fech_ref = qs703fechref;
				 }
		}
		else{ 
			cap04_07.qs703fech_ref = null;
		}
		
		if (cap04_07.qs704a != null) {
			cap04_07.qs704a = cap04_07.getConvert700(cap04_07.qs704a);
		}
		if (cap04_07.qs704b != null) {
			cap04_07.qs704b = cap04_07.getConvert700(cap04_07.qs704b);
		}
		if (cap04_07.qs704c != null) {
			cap04_07.qs704c = cap04_07.getConvert700(cap04_07.qs704c);
		}
		if (cap04_07.qs704d != null) {
			cap04_07.qs704d = cap04_07.getConvert700(cap04_07.qs704d);
		}
		if (cap04_07.qs704e != null) {
			cap04_07.qs704e = cap04_07.getConvert700(cap04_07.qs704e);
		}
		if (cap04_07.qs704f != null) {
			cap04_07.qs704f = cap04_07.getConvert700(cap04_07.qs704f);
		}
		if (cap04_07.qs704g != null) {
			cap04_07.qs704g = cap04_07.getConvert700(cap04_07.qs704g);
		}
		if (cap04_07.qs704h != null) {
			cap04_07.qs704h = cap04_07.getConvert700(cap04_07.qs704h);
		}
		if (cap04_07.qs704i != null) {
			cap04_07.qs704i = cap04_07.getConvert700(cap04_07.qs704i);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(cap04_07.qs703)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QS703"); 
			view = rgQS703; 
			error = true; 
			return false; 
		} 
		if (MyUtil.incluyeRango(1,1, rgQS703.getTagSelected("").toString())) {
			if (Util.esVacio(cap04_07.qs704prv)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704PRV"); 
				view = txtQS704PRV; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704A"); 
				view = rgQS704A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704B"); 
				view = rgQS704B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704C"); 
				view = rgQS704C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704D"); 
				view = rgQS704D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704E"); 
				view = rgQS704E; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704F"); 
				view = rgQS704F; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704G"); 
				view = rgQS704G; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704h)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704H"); 
				view = rgQS704H; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs704i)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.704I"); 
				view = rgQS704I; 
				error = true; 
				return false; 
			} 
			if (!MyUtil.incluyeRango(1,1, rgQS704A.getTagSelected("").toString()) || 
		    		!MyUtil.incluyeRango(1,1, rgQS704B.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704C.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704D.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704E.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704F.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704G.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704H.getTagSelected("").toString()) ||
		    		!MyUtil.incluyeRango(1,1, rgQS704I.getTagSelected("").toString())
		    		) {
				if (Util.esVacio(cap04_07.qs706)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.706"); 
					view = rgQS706; 
					error = true; 
					return false; 
				} 
			}
			
		}
		
		
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 

    	
    	cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    }
		if (cap04_07.qs704a != null) {
			cap04_07.qs704a = cap04_07.setConvert700(cap04_07.qs704a);
		}
		if (cap04_07.qs704b != null) {
			cap04_07.qs704b = cap04_07.setConvert700(cap04_07.qs704b);
		}
		if (cap04_07.qs704c != null) {
			cap04_07.qs704c = cap04_07.setConvert700(cap04_07.qs704c);
		}
		if (cap04_07.qs704d != null) {
			cap04_07.qs704d = cap04_07.setConvert700(cap04_07.qs704d);
		}
		if (cap04_07.qs704e != null) {
			cap04_07.qs704e = cap04_07.setConvert700(cap04_07.qs704e);
		}
		if (cap04_07.qs704f != null) {
			cap04_07.qs704f = cap04_07.setConvert700(cap04_07.qs704f);
		}
		if (cap04_07.qs704g != null) {
			cap04_07.qs704g = cap04_07.setConvert700(cap04_07.qs704g);
		}
		if (cap04_07.qs704h != null) {
			cap04_07.qs704h = cap04_07.setConvert700(cap04_07.qs704h);
		}
		if (cap04_07.qs704i != null) {
			cap04_07.qs704i = cap04_07.setConvert700(cap04_07.qs704i);
		}
		entityToUI(cap04_07); 
		qs703fechref = cap04_07.qs703fech_ref;
		inicio(); 
    } 
    private void inicio() { 
    	   
    	   
           onP703ChangeValue();
           RenombrarEtiquetas();
           onP704PRVChangeValue();
//           onP704ChangeValue();
           ValidarsiesSupervisora();
//           rgQS703.requestFocus();
           txtCabecera.requestFocus();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS703.readOnly(); 
				txtQS704PRV.readOnly();
				rgQS704A.readOnly();
				rgQS704B.readOnly(); 
				rgQS704C.readOnly();
				rgQS704D.readOnly();
				rgQS704E.readOnly();
				rgQS704F.readOnly();
				rgQS704G.readOnly();
				rgQS704H.readOnly();
				rgQS704I.readOnly();
				rgQS706.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onP703ChangeValue() {

		if (MyUtil.incluyeRango(2,2, rgQS703.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(), txtQS704PRV,rgQS704A,rgQS704B,rgQS704C,rgQS704D,rgQS704E,rgQS704F,rgQS704G,rgQS704H,rgQS704I,rgQS706);	
			MyUtil.LiberarMemoria();
			q2.setVisibility(view.GONE);
			q3.setVisibility(view.GONE);
		}		
		else {	
			 q2.setVisibility(view.VISIBLE);
			 q3.setVisibility(view.VISIBLE);
			  Util.lockView(getActivity(), false,txtQS704PRV,rgQS704A,rgQS704B,rgQS704C,rgQS704D,rgQS704E,rgQS704F,rgQS704G,rgQS704H,rgQS704I,rgQS706);
			  txtQS704PRV.requestFocus();	
			  todosCero704();
		}
		
	}
  
    public void onP704PRVChangeValue(){
    	String cadena1=this.getResources().getString(R.string.cap04_07qs704); 
    	String cadena2="";
//      	cadena2=cadena1+" "+txtQS704PRV.getText().toString()+""+this.getResources().getString(R.string.cap04_07qs704_2);
    	cadena2=cadena1+" "+txtQS704PRV.getText().toString();
      	lbl704_11.setText(cadena2);
    }
    public void todosCero704(){
    	if (MyUtil.incluyeRango(1,1, rgQS704A.getTagSelected("").toString()) && 
	    		MyUtil.incluyeRango(1,1, rgQS704B.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704C.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704D.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704E.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704F.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704G.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704H.getTagSelected("").toString()) &&
	    		MyUtil.incluyeRango(1,1, rgQS704I.getTagSelected("").toString())
	    		) {
    		Util.cleanAndLockView(this.getActivity(),rgQS706);
    		q3.setVisibility(view.GONE);
    	}else{
    		q3.setVisibility(view.VISIBLE);
    		Util.lockView(getActivity(), false, rgQS706);
    	}
    }
    public void onP704AChangeValue(){
    	todosCero704();
    	rgQS704B.requestFocus();
    }
    public void onP704BChangeValue(){
    	todosCero704();
    	rgQS704C.requestFocus();
    }
    public void onP704CChangeValue(){
    	todosCero704();
    	rgQS704D.requestFocus();
    }
    public void onP704DChangeValue(){
    	todosCero704();
    	rgQS704E.requestFocus();
    }
    public void onP704EChangeValue(){
    	todosCero704();
    	rgQS704F.requestFocus();
    }
    public void onP704FChangeValue(){
    	todosCero704();
    	rgQS704G.requestFocus();
    }
    public void onP704GChangeValue(){
    	todosCero704();
    	rgQS704H.requestFocus();
    }
    public void onP704HChangeValue(){
    	todosCero704();
    	rgQS704I.requestFocus();
    }
    public void onP704IChangeValue(){
    	todosCero704();
    	rgQS706.requestFocus();
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			
			if(cap04_07.qs703 != null) {
				if(qs703fechref == null) {
					cap04_07.qs703fech_ref =  Util.getFechaActualToString();
					}
				else {
					cap04_07.qs703fech_ref = qs703fechref;
					 }
			}
			else{ 
				cap04_07.qs703fech_ref = null;
			}
			
			
			if (cap04_07.qs704a != null) {
				cap04_07.qs704a = cap04_07.getConvert700(cap04_07.qs704a);
			}
			if (cap04_07.qs704b != null) {
				cap04_07.qs704b = cap04_07.getConvert700(cap04_07.qs704b);
			}
			if (cap04_07.qs704c != null) {
				cap04_07.qs704c = cap04_07.getConvert700(cap04_07.qs704c);
			}
			if (cap04_07.qs704d != null) {
				cap04_07.qs704d = cap04_07.getConvert700(cap04_07.qs704d);
			}
			if (cap04_07.qs704e != null) {
				cap04_07.qs704e = cap04_07.getConvert700(cap04_07.qs704e);
			}
			if (cap04_07.qs704f != null) {
				cap04_07.qs704f = cap04_07.getConvert700(cap04_07.qs704f);
			}
			if (cap04_07.qs704g != null) {
				cap04_07.qs704g = cap04_07.getConvert700(cap04_07.qs704g);
			}
			if (cap04_07.qs704h != null) {
				cap04_07.qs704h = cap04_07.getConvert700(cap04_07.qs704h);
			}
			if (cap04_07.qs704i != null) {
				cap04_07.qs704i = cap04_07.getConvert700(cap04_07.qs704i);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
