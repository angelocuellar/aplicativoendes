package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_04Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS409; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS410; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS410_A1;
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS410_A2;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS411; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS412; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS412_A1;
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQS412_A2;
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS413;
	
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQS413A;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQS413B;
	@FieldAnnotation(orderIndex=12) 
	public IntegerField txtQS413B_A1;
	@FieldAnnotation(orderIndex=13) 
	public IntegerField txtQS413B_A2;
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQS413C;
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQS413D;
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQS413E;
	@FieldAnnotation(orderIndex=17) 
	public IntegerField txtQS413E_A1;
	@FieldAnnotation(orderIndex=18) 
	public IntegerField txtQS413E_A2;
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQS413F;
	
	@FieldAnnotation(orderIndex=20)
	public TextAreaField txtQSOBS_SECCION4;
	
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblObservaciones,lblpregunta409,lblP409_extra,lblpregunta410,lblP410_extra,lblpregunta411,
			lblP411_extra,lblpregunta412,lblP412_extra,lblpregunta413,lblvacio,lblvacio2,lblpregunta413A,lblpregunta413B,lblpregunta413C,
			lblpregunta413D,lblpregunta413E,lblpregunta413F,lblpregunta413A_ind1,lblpregunta413A_ind2,lblpregunta413B_ind,lblpregunta413D_ind1,
			lblpregunta413D_ind2,lblpregunta413E_ind,lblvacio3,lblvacio4,lblTitulo2A;
	
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13; 
	 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01;
	
	public GridComponent2 gridPreguntas410,gridPreguntas412,gridPreguntas413B,gridPreguntas413E;	
	
	Salud seccion1;	
	//boolean mujer_30_59=true;	
	boolean mujer_30_70=true;
	boolean mujer_25_64=true;
	
	public CS_04Fragment_003() {} 
	public CS_04Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413A","QS413B","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F","QS708_A","QS708_B","QS708_C","QS708_D","QS708_E","QS708_F","QS708_G","QS708_H","QS708_I","QS708_J","QS708_X","QS708_O","QS708_Y","QS709","QS710","QS711","ID","HOGAR_ID","PERSONA_ID","QSOBS_SECCION4")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413A","QS413B","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F","QS708_A","QS708_B","QS708_C","QS708_D","QS708_E","QS708_F","QS708_G","QS708_H","QS708_I","QS708_J","QS708_X","QS708_O","QS708_Y","QS709","QS710","QS711","QSOBS_SECCION4")};
		
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QH06","QH02_1","QS311")};
		
		return rootView; 
	} 
	
	@Override 
	protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_3).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		lblTitulo2A=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_3A).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblpregunta409 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs409);
		lblP409_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs409_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS409=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs409_1,R.string.cap04_07qs409_2,R.string.cap04_07qs409_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS409ChangeValue"); 
		
		lblpregunta410 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs410);
		lblP410_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs410_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS410=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs410_1,R.string.cap04_07qs410_2,R.string.cap04_07qs410_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS410ChangeValue"); 
		txtQS410_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS410_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas410=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas410.addComponent(rgQS410,1,3);		
		gridPreguntas410.addComponent(txtQS410_A1);
		gridPreguntas410.addComponent(txtQS410_A2);
		gridPreguntas410.addComponent(lblvacio);
		
		lblpregunta411 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs411);
		lblP411_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs411_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS411=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs411_1,R.string.cap04_07qs411_2,R.string.cap04_07qs411_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS411ChangeValue"); 
		
		lblpregunta412 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs412);
		lblP412_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs410_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS412=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs412_1,R.string.cap04_07qs412_2,R.string.cap04_07qs412_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS412ChangeValue"); 
		txtQS412_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS412_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio2 = new LabelComponent(this.getActivity()).size(altoComponente+15, 90); 
		
		gridPreguntas412=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas412.addComponent(rgQS412,1,3);		
		gridPreguntas412.addComponent(txtQS412_A1);
		gridPreguntas412.addComponent(txtQS412_A2);
		gridPreguntas412.addComponent(lblvacio2);
		
		lblpregunta413 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413);
		rgQS413=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs413_1,R.string.cap04_07qs413_2,R.string.cap04_07qs413_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta413A = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413a);
		lblpregunta413A_ind1 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413a_ind1).negrita();
		lblpregunta413A_ind2 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413a_ind2);
		rgQS413A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs413_1,R.string.cap04_07qs413_2,R.string.cap04_07qs413_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS413AChangeValue");
		
		lblpregunta413B = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413b);		
		lblpregunta413B_ind = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs413b_ind).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS413B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs412_1,R.string.cap04_07qs412_2,R.string.cap04_07qs412_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS413BChangeValue"); 
		txtQS413B_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS413B_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio3 = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas413B=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas413B.addComponent(rgQS413B,1,3);		
		gridPreguntas413B.addComponent(txtQS413B_A1);
		gridPreguntas413B.addComponent(txtQS413B_A2);
		gridPreguntas413B.addComponent(lblvacio3);
		
		
		lblpregunta413C = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413c);
		rgQS413C=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs413_1,R.string.cap04_07qs413_2,R.string.cap04_07qs413_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta413D = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413d);
		lblpregunta413D_ind1 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413d_ind1).negrita();
		lblpregunta413D_ind2 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413d_ind2);
		rgQS413D=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs413_1,R.string.cap04_07qs413_2,R.string.cap04_07qs413_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS413DChangeValue");
		
		lblpregunta413E = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413e);		
		lblpregunta413E_ind = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs413e_ind).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda().negrita();
		rgQS413E=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs412_1,R.string.cap04_07qs412_2,R.string.cap04_07qs412_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS413EChangeValue"); 
		txtQS413E_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS413E_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio4 = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas413E=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas413E.addComponent(rgQS413E,1,3);		
		gridPreguntas413E.addComponent(txtQS413E_A1);
		gridPreguntas413E.addComponent(txtQS413E_A2);
		gridPreguntas413E.addComponent(lblvacio4);
		
		
		lblpregunta413F = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs413f);
		rgQS413F=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs413_1,R.string.cap04_07qs413_2,R.string.cap04_07qs413_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);  
		txtQSOBS_SECCION4= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		textoNegrita();
    } 
  
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblpregunta409,lblP409_extra,rgQS409);		
		q3 = createQuestionSection(lblpregunta410,lblP410_extra,gridPreguntas410.component());
		
		q4 = createQuestionSection(lblTitulo2A,lblpregunta411,lblP411_extra,rgQS411);		
		q5 = createQuestionSection(lblpregunta412,lblP412_extra,gridPreguntas412.component());
		q6 = createQuestionSection(lblpregunta413,rgQS413); 
		q7 = createQuestionSection(lblpregunta413A,lblpregunta413A_ind1,lblpregunta413A_ind2,rgQS413A);
		q8 = createQuestionSection(lblpregunta413B,lblpregunta413B_ind,gridPreguntas413B.component());
		q9 = createQuestionSection(lblpregunta413C,rgQS413C);
		q10 = createQuestionSection(lblpregunta413D,lblpregunta413D_ind1,lblpregunta413D_ind2,rgQS413D);
		q11 = createQuestionSection(lblpregunta413E,lblpregunta413E_ind,gridPreguntas413E.component());
		q12 = createQuestionSection(lblpregunta413F,rgQS413F);
		q13 = createQuestionSection(R.string.cap04_07qsObservaSeccion4,txtQSOBS_SECCION4); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		form.addView(q11);
		form.addView(q12);
		form.addView(q13); 
		return contenedor; 
    } 
    

    public boolean grabar() {
    	if(salud!=null){
		uiToEntity(salud);
//		Log.e("salud dddd",""+salud);
		
		if (salud.qs409!=null) {salud.qs409=salud.getConvertqs401(salud.qs409);}
		if (salud.qs410!=null) {salud.qs410=salud.getConvertqs401(salud.qs410);}
		if (salud.qs411!=null) {salud.qs411=salud.getConvertqs401(salud.qs411);}
		if (salud.qs412!=null) {salud.qs412=salud.getConvertqs401(salud.qs412);}		
		if (salud.qs413!=null) {salud.qs413=salud.getConvertqs401(salud.qs413);}
		
		if (salud.qs413b!=null) {salud.qs413b=salud.getConvertqs413b(salud.qs413b);}
		
		if (salud.qs413a!=null) {salud.qs413a=salud.getConvertqs401(salud.qs413a);}
		if (salud.qs413c!=null) {salud.qs413c=salud.getConvertqs401(salud.qs413c);}
		if (salud.qs413d!=null) {salud.qs413d=salud.getConvertqs401(salud.qs413d);}
		if (salud.qs413f!=null) {salud.qs413f=salud.getConvertqs401(salud.qs413f);}
		
		if (salud.qs413e!=null) {salud.qs413e=salud.getConvertqs413e(salud.qs413e);}
		
		if (!Util.esDiferente(salud.qs410,1) && txtQS410_A1.getText().toString().trim().length()!=0 ) {
			salud.qs410_a=Integer.parseInt(txtQS410_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs410,2) && txtQS410_A2.getText().toString().trim().length()!=0) {
			salud.qs410_a=Integer.parseInt(txtQS410_A2.getText().toString().trim());
		}
		
				
		if (!Util.esDiferente(salud.qs412,1) && txtQS412_A1.getText().toString().trim().length()!=0 ) {
			salud.qs412_a=Integer.parseInt(txtQS412_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs412,2) && txtQS412_A2.getText().toString().trim().length()!=0) {
			salud.qs412_a=Integer.parseInt(txtQS412_A2.getText().toString().trim());
		}
		
				
		if (!Util.esDiferente(salud.qs413b,1) && txtQS413B_A1.getText().toString().trim().length()!=0 ) {
			salud.qs413b_a=Integer.parseInt(txtQS413B_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs413b,2) && txtQS413B_A2.getText().toString().trim().length()!=0) {
			salud.qs413b_a=Integer.parseInt(txtQS413B_A2.getText().toString().trim());
		}
		
						
		if (!Util.esDiferente(salud.qs413e,1) && txtQS413E_A1.getText().toString().trim().length()!=0 ) {
			salud.qs413e_a=Integer.parseInt(txtQS413E_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs413e,2) && txtQS413E_A2.getText().toString().trim().length()!=0) {
			salud.qs413e_a=Integer.parseInt(txtQS413E_A2.getText().toString().trim());
		}
		
		
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
    }
		return true; 
    	
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		Log.e("","MUJER: "+mujer_30_70);
		Log.e("","SEXO: "+seccion1.qh06);
		if(mujer_30_70 && seccion1.qh06==2){
			if (Util.esVacio(salud.qs409)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS409"); 
				view = rgQS409; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs410) && !Util.esDiferente(salud.qs409,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS410"); 
				view = rgQS410; 
				error = true; 
				return false; 
			}			
			if(Util.esDiferente(salud.qs410,8) && !Util.esDiferente(salud.qs409,1)){
				if (Util.esVacio(salud.qs410_a) && !Util.esDiferente(salud.qs410,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.410_a");
					view = txtQS410_A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs410_a) && !Util.esDiferente(salud.qs410,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.410_a");
					view = txtQS410_A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 23, salud.qs410_a) && !Util.esDiferente(salud.qs410,1)){
					mensaje = ""+salud.qs410_a+" Esta fuera del rango: 0-23";
					view = txtQS410_A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs410_a) && !Util.esDiferente(salud.qs410,2)){
					mensaje = ""+salud.qs410_a+" Esta fuera del rango: 2-"+seccion1.qs23;
					view = txtQS410_A2; 
					error = true; 
					return false;
				}
			}
		}
		
		if(mujer_25_64 && seccion1.qh06==2){		
			if (Util.esVacio(salud.qs411)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS411"); 
				view = rgQS411; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs412) && !Util.esDiferente(salud.qs411,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS412"); 
				view = rgQS412; 
				error = true; 
				return false; 
			}
			
			if(Util.esDiferente(salud.qs412,8) && !Util.esDiferente(salud.qs411,1)){ 
				if (Util.esVacio(salud.qs412_a) && !Util.esDiferente(salud.qs412,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.412_a");
					view = txtQS412_A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs412_a) && !Util.esDiferente(salud.qs412,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.412_a");
					view = txtQS412_A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 23, salud.qs412_a) && !Util.esDiferente(salud.qs412,1)){
					mensaje = ""+salud.qs412_a+" Esta fuera del rango: 0-23";
					view = txtQS412_A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs412_a) && !Util.esDiferente(salud.qs412,2)){
					mensaje = ""+salud.qs412_a+" Esta fuera del rango: 2-"+seccion1.qs23;
					view = txtQS412_A2; 
					error = true; 
					return false;
				}
			} 
			
			if (Util.esVacio(salud.qs413) && !Util.esDiferente(salud.qs411,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.413"); 
				view = rgQS413; 
				error = true; 
				return false; 
			}
			
			if (Util.esVacio(salud.qs413a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.413A"); 
				view = rgQS413A; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(salud.qs413a,1)){
				if(Util.esDiferente(salud.qs413b,8) ){
					if (Util.esVacio(salud.qs413b_a) && !Util.esDiferente(salud.qs413b,1)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.413B_a");
						view = txtQS413B_A1; 
						error = true; 
						return false; 
					}
					if (Util.esVacio(salud.qs413b_a) && !Util.esDiferente(salud.qs413b,2)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.413B_a");
						view = txtQS413B_A2; 
						error = true; 
						return false; 
					}
					if(!MyUtil.incluyeRango(0, 23, salud.qs413b_a) && !Util.esDiferente(salud.qs413b,1)){
						mensaje = ""+salud.qs413b_a+" Esta fuera del rango: 0-23";
						view = txtQS413B_A1; 
						error = true; 
						return false;
					}
					if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs413b_a) && !Util.esDiferente(salud.qs413b,2)){
						mensaje = ""+salud.qs413b_a+" Esta fuera del rango: 2-"+seccion1.qs23;
						view = txtQS413B_A2; 
						error = true; 
						return false;
					}
				}
				
				if (Util.esVacio(salud.qs413c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.413C"); 
					view = rgQS413C; 
					error = true; 
					return false; 
				}
			}
			
			if (Util.esVacio(salud.qs413d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.413D"); 
				view = rgQS413D; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(salud.qs413d,1)){
				if(Util.esDiferente(salud.qs413e,8) ){
					if (Util.esVacio(salud.qs413e_a) && !Util.esDiferente(salud.qs413e,1)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.413E_a");
						view = txtQS413E_A1; 
						error = true; 
						return false; 
					}
					if (Util.esVacio(salud.qs413e_a) && !Util.esDiferente(salud.qs413e,2)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.413E_a");
						view = txtQS413E_A2; 
						error = true; 
						return false; 
					}
					if(!MyUtil.incluyeRango(0, 23, salud.qs413e_a) && !Util.esDiferente(salud.qs413e,1)){
						mensaje = ""+salud.qs413e_a+" Esta fuera del rango: 0-23";
						view = txtQS413E_A1; 
						error = true; 
						return false;
					}
					if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs413e_a) && !Util.esDiferente(salud.qs413e,2)){
						mensaje = ""+salud.qs413e_a+" Esta fuera del rango: 2-"+seccion1.qs23;
						view = txtQS413E_A2; 
						error = true; 
						return false;
					}
				}
				
				if (Util.esVacio(salud.qs413f)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.413F"); 
					view = rgQS413F; 
					error = true; 
					return false; 
				}
			}
		}
		 
		return true; 
    } 
    
    

    public void cargarDatos() {     	
    	salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
		if(salud==null){ 
			salud=new CAP04_07(); 
			salud.id=App.getInstance().getPersonaSeccion01().id; 
			salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
			salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		
//		Log.e("salud....",""+salud);
//		Log.e("mujer_30_70....",""+mujer_30_70);
//		
		if (salud.qs409!=null) {salud.qs409=salud.setConvertqs401(salud.qs409);}
		if (salud.qs410!=null) {salud.qs410=salud.setConvertqs401(salud.qs410);}
		if (salud.qs411!=null) {salud.qs411=salud.setConvertqs401(salud.qs411);}
		if (salud.qs412!=null) {salud.qs412=salud.setConvertqs401(salud.qs412);}
		if (salud.qs413!=null) {salud.qs413=salud.setConvertqs401(salud.qs413);}
		
		if (salud.qs413a!=null) {salud.qs413a=salud.setConvertqs401(salud.qs413a);}
		if (salud.qs413b!=null) {salud.qs413b=salud.setConvertqs413b(salud.qs413b);}
		if (salud.qs413c!=null) {salud.qs413c=salud.setConvertqs401(salud.qs413c);}
		if (salud.qs413d!=null) {salud.qs413d=salud.setConvertqs401(salud.qs413d);}
		if (salud.qs413e!=null) {salud.qs413e=salud.setConvertqs413e(salud.qs413e);}
		if (salud.qs413f!=null) {salud.qs413f=salud.setConvertqs401(salud.qs413f);}
		
		entityToUI(salud);		 
		
		if (salud.qs410_a!=null) {	
	  		  if (!Util.esDiferente(salud.qs410,1)) {
	  	  		  txtQS410_A1.setText(salud.qs410_a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs410,2)) {
	  			  txtQS410_A2.setText(salud.qs410_a.toString());
	  		  }
	  	}
		
		if (salud.qs412_a!=null) {	
	  		  if (!Util.esDiferente(salud.qs412,1)) {
	  	  		  txtQS412_A1.setText(salud.qs412_a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs412,2)) {
	  			  txtQS412_A2.setText(salud.qs412_a.toString());
	  		  }
	  	}
		
		if (salud.qs413b_a!=null) {	
	  		  if (!Util.esDiferente(salud.qs413b,1)) {
	  	  		  txtQS413B_A1.setText(salud.qs413b_a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs413b,2)) {
	  			  txtQS413B_A2.setText(salud.qs413b_a.toString());
	  		  }
	  	}
		if (salud.qs413e_a!=null) {	
	  		  if (!Util.esDiferente(salud.qs413e,1)) {
	  	  		  txtQS413E_A1.setText(salud.qs413e_a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs413e,2)) {
	  			  txtQS413E_A2.setText(salud.qs413e_a.toString());
	  		  }
	  	}		
		inicio(); 
		Log.e("","PERSONA: "+seccion1.persona_id);
		App.getInstance().setPersonabySalud(seccion1);
    } 
    
    
    private void inicio() { 
    	entra_seccion4();
		entra_seccion42();
//    	if(mujer_30_70 || mujer_25_64){    		
//    			
//    	}    	
//    	else{
//    		grabar();
//    	}
    	ValidarsiesSupervisora();    	
    	txtCabecera.requestFocus();
    	
    	/*if( (MyUtil.incluyeRango(40,70,seccion1.qs23)&& MyUtil.incluyeRango(2,2,seccion1.qh06))|| (MyUtil.incluyeRango(50,70,seccion1.qs23) && MyUtil.incluyeRango(1,1,seccion1.qh06))){
    		Log.e("","AQUI: ");
    		q7.setVisibility(View.GONE);    		
    	}
    	else{
    		q7.setVisibility(View.VISIBLE);
    	}*/
    } 
    
    
    public void entra_seccion4(){
//    	Log.e("seccion1.qs23","por aki paso"+seccion1.qs23);
//    	Log.e("seccion1.qs23","por aki paso"+seccion1.qh06);
 	   if((!MyUtil.incluyeRango(30, 70, seccion1.qs23)|| !MyUtil.incluyeRango(2, 2, seccion1.qh06))){ 
 		   Log.e("holaaaa","por aki paso");
 			Util.cleanAndLockView(this.getActivity(),rgQS409,rgQS410,txtQS410_A1,txtQS410_A2);
 			q0.setVisibility(View.GONE);
     		q1.setVisibility(View.GONE);
     		q2.setVisibility(View.GONE);
     		q3.setVisibility(View.GONE);
 			mujer_30_70 = false;
 	   }
 	   else{
 		  Util.lockView(getActivity(), false, rgQS409,rgQS410);
 		  q0.setVisibility(View.VISIBLE);
     	  q1.setVisibility(View.VISIBLE);
     	  q2.setVisibility(View.VISIBLE);
     	  q3.setVisibility(View.VISIBLE);
     	  onqrgQS409ChangeValue();
     	  //if (salud.qs412==null) {Util.cleanAndLockView(getActivity(),txtQS412_A1,txtQS412_A2);}
     		
 			mujer_30_70 = true;
 			
// 			if(seccion1.qs23>39){ 				
// 				q13.setVisibility(View.GONE);
// 			}
// 			else{
// 				seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413A","QS413B","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F","QSOBS_SECCION4")};
// 			}
 			
 		}
 	  onqrgQS411ChangeValue();
    }
    
    public void entra_seccion42(){
//     	Log.e("seccion1.qs23","por aki paso "+seccion1.qs23);
//    	Log.e("seccion1.qs23","por aki paso asssssa "+seccion1.qh06);
  	   if((!MyUtil.incluyeRango(25, 64, seccion1.qs23)|| !MyUtil.incluyeRango(2, 2, seccion1.qh06))){ 		
  		   Log.e("","AQUI");
  			Util.cleanAndLockView(this.getActivity(),rgQS411,rgQS412,txtQS412_A1,txtQS412_A2,rgQS413,rgQS413A,rgQS413B,rgQS413C,rgQS413D,rgQS413E,rgQS413F);  			      		
  			q4.setVisibility(View.GONE);
     		q5.setVisibility(View.GONE);
     		q6.setVisibility(View.GONE);
  			q7.setVisibility(View.GONE);
      		q8.setVisibility(View.GONE);
      		q9.setVisibility(View.GONE);
      		q10.setVisibility(View.GONE);
      		q11.setVisibility(View.GONE);
      		q12.setVisibility(View.GONE);
      		//q13.setVisibility(View.GONE);
  			mujer_25_64 = false;
  	   }
  	   else{ 
  		 Log.e("","AQUI ssss");
  		   Util.lockView(getActivity(), false, rgQS411,rgQS412,rgQS413,rgQS413A,rgQS413B,rgQS413C,rgQS413D,rgQS413E,rgQS413F);
//      		q1.setVisibility(View.VISIBLE);
      		q4.setVisibility(View.VISIBLE);
     		q5.setVisibility(View.VISIBLE);
     		q6.setVisibility(View.VISIBLE);
      		q7.setVisibility(View.VISIBLE);
      		q8.setVisibility(View.VISIBLE);
      		q9.setVisibility(View.VISIBLE);
      		q10.setVisibility(View.VISIBLE);
      		q11.setVisibility(View.VISIBLE);
      		q12.setVisibility(View.VISIBLE);
      		//q13.setVisibility(View.VISIBLE);
      		onqrgQS411ChangeValue();
      		mujer_25_64 = true;
      		onqrgQS413AChangeValue();
      		onqrgQS413DChangeValue();
//      		if (salud.qs413b==null) {Util.cleanAndLockView(getActivity(),txtQS413B_A1,txtQS413B_A2);}
//      		if (salud.qs413e==null) {Util.cleanAndLockView(getActivity(),txtQS413E_A1,txtQS413E_A2);}
      		
  			
  			
//  			if(seccion1.qs23>39){ 				
//  				q13.setVisibility(View.GONE);
//  			}
//  			else{
//  				q13.setVisibility(View.VISIBLE);
//  				//seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS409","QS410","QS410_A","QS411","QS412","QS412_A","QS413","QS413A","QS413B","QS413B_A","QS413C","QS413D","QS413E","QS413E_A","QS413F","QSOBS_SECCION4")};
//  			}
  		}
     }
    
    
    public void onqrgQS409ChangeValue(){
    	if (MyUtil.incluyeRango(2,3,rgQS409.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS410,txtQS410_A1,txtQS410_A2);
			q3.setVisibility(View.GONE);
			rgQS411.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS410);
			q3.setVisibility(View.VISIBLE);
			onqrgQS410ChangeValue();
			rgQS410.requestFocus();
		}
    }
    
    public void onqrgQS410ChangeValue(){
    	if (!MyUtil.incluyeRango(3,3,rgQS410.getTagSelected("").toString())) {	
    		if (MyUtil.incluyeRango(1,1,rgQS409.getTagSelected("").toString())) {
				if(MyUtil.incluyeRango(1,1,rgQS410.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS410_A2);
//					salud.qs410_a=null;
					Util.lockView(getActivity(), false, txtQS410_A1);
					txtQS410_A1.setVisibility(View.VISIBLE);
					txtQS410_A1.requestFocus();
				}
				if(MyUtil.incluyeRango(2,2,rgQS410.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS410_A1);
//					salud.qs410_a=null;
					Util.lockView(getActivity(), false, txtQS410_A2);
					txtQS410_A2.setVisibility(View.VISIBLE);
					txtQS410_A2.requestFocus();
				}
    		}
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS410_A1,txtQS410_A2);
			rgQS411.requestFocus();	
		}
   }
    
   public void onqrgQS411ChangeValue(){
	   if (MyUtil.incluyeRango(2,3,rgQS411.getTagSelected("").toString())) {
		   Log.e("","assa");
		   MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS412,txtQS412_A1,txtQS412_A2,rgQS413);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			rgQS413A.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS412,rgQS413);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			onqrgQS412ChangeValue();
			rgQS412.requestFocus();
		}
   }
   
   public void onqrgQS412ChangeValue(){
	   if (!MyUtil.incluyeRango(3,3,rgQS412.getTagSelected("").toString())) {	
		   if (MyUtil.incluyeRango(1,1,rgQS411.getTagSelected("").toString())) {
			   if(MyUtil.incluyeRango(1,1,rgQS412.getTagSelected("").toString())){
				    Util.cleanAndLockView(getActivity(),txtQS412_A2);
//					salud.qs412_a=null;
					Util.lockView(getActivity(), false, txtQS412_A1);
					txtQS412_A1.setVisibility(View.VISIBLE);
					txtQS412_A1.requestFocus();
			   }
			   if(MyUtil.incluyeRango(2,2,rgQS412.getTagSelected("").toString())){
				    Util.cleanAndLockView(getActivity(),txtQS412_A1);
//					salud.qs412_a=null;
					Util.lockView(getActivity(), false, txtQS412_A2);
					txtQS412_A2.setVisibility(View.VISIBLE);
					txtQS412_A2.requestFocus();
			   }
		   }
	   } 
	   else {
		   Util.cleanAndLockView(this.getActivity(),txtQS412_A1,txtQS412_A2);
		   rgQS413.requestFocus();
	   }
   }
   
   public void onqrgQS413AChangeValue(){
   	if (MyUtil.incluyeRango(2,3,rgQS413A.getTagSelected("0").toString())) {
   		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS413B,txtQS413B_A1,txtQS413B_A2,rgQS413C);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			rgQS413D.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS413B,rgQS413C);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			onqrgQS413BChangeValue();
			rgQS413B.requestFocus();
		}
   }
   
   public void onqrgQS413BChangeValue(){
   	if (!MyUtil.incluyeRango(3,3,rgQS413B.getTagSelected("").toString())) {	
   		if (MyUtil.incluyeRango(1,1,rgQS413A.getTagSelected("").toString())) {
				if(MyUtil.incluyeRango(1,1,rgQS413B.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS413B_A2);
//					salud.qs413b_a=null;
					Util.lockView(getActivity(), false, txtQS413B_A1);
					txtQS413B_A1.setVisibility(View.VISIBLE);
					txtQS413B_A1.requestFocus();
				}
				if(MyUtil.incluyeRango(2,2,rgQS413B.getTagSelected("").toString())){
					Util.cleanAndLockView(getActivity(),txtQS413B_A1);
//					salud.qs413b_a=null;
					Util.lockView(getActivity(), false, txtQS413B_A2);
					txtQS413B_A2.setVisibility(View.VISIBLE);
					txtQS413B_A2.requestFocus();
				}
   		}
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS413B_A1,txtQS413B_A2);
			rgQS413C.requestFocus();	
		}
  }
   
   public void onqrgQS413DChangeValue(){
	   	if (MyUtil.incluyeRango(2,3,rgQS413D.getTagSelected("").toString())) {
	   		MyUtil.LiberarMemoria();
				Util.cleanAndLockView(this.getActivity(), rgQS413E,txtQS413E_A1,txtQS413E_A2,rgQS413F);
				q11.setVisibility(View.GONE);
				q12.setVisibility(View.GONE);
				txtQSOBS_SECCION4.requestFocus();
			} 
			else {				
				Util.lockView(getActivity(), false,  rgQS413E,rgQS413F);
				q11.setVisibility(View.VISIBLE);
				q12.setVisibility(View.VISIBLE);
				onqrgQS413EChangeValue();
				rgQS413E.requestFocus();
			}
   }
   
   public void onqrgQS413EChangeValue(){
	   	if (!MyUtil.incluyeRango(3,3,rgQS413E.getTagSelected("").toString())) {	
	   		if (MyUtil.incluyeRango(1,1,rgQS413D.getTagSelected("").toString())) {
					if(MyUtil.incluyeRango(1,1,rgQS413E.getTagSelected("").toString())){
						Util.cleanAndLockView(getActivity(),txtQS413E_A2);
						salud.qs413e_a=null;
						Util.lockView(getActivity(), false, txtQS413E_A1);
						txtQS413E_A1.setVisibility(View.VISIBLE);
						txtQS413E_A1.requestFocus();
					}
					if(MyUtil.incluyeRango(2,2,rgQS413E.getTagSelected("").toString())){
						Util.cleanAndLockView(getActivity(),txtQS413E_A1);
						salud.qs413e_a=null;
						Util.lockView(getActivity(), false, txtQS413E_A2);
						txtQS413E_A2.setVisibility(View.VISIBLE);
						txtQS413E_A2.requestFocus();
					}
	   		}
			} 
			else {
				Util.cleanAndLockView(this.getActivity(),txtQS413E_A1,txtQS413E_A2);
				rgQS413F.requestFocus();	
			}
	  }
   
   
   
  
   
   public void textoNegrita(){
	   lblP409_extra.text(R.string.cap04_07qs409_ex);
	   Spanned texto409_ex = Html.fromHtml("<b>DE SER NECESARIO LEA: </b>"+lblP409_extra.getText());    	
	   lblP409_extra.setText(texto409_ex);
	   
	   lblP411_extra.text(R.string.cap04_07qs411_ex);
	   Spanned texto411_ex = Html.fromHtml("<b>DE SER NECESARIO LEA: </b>"+lblP411_extra.getText());    	
	   lblP411_extra.setText(texto411_ex);
   }
   
   public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS409.readOnly();
			rgQS410.readOnly();
			txtQS410_A1.readOnly();
			txtQS410_A2.readOnly();
			rgQS411.readOnly();
			rgQS412.readOnly();
			txtQS412_A1.readOnly();			
			txtQS412_A2.readOnly();
			rgQS413.readOnly();
			rgQS413A.readOnly();
			rgQS413B.readOnly();
			txtQS413B_A1.readOnly();
			txtQS413B_A2.readOnly();
			rgQS413C.readOnly();
			rgQS413D.readOnly();
			rgQS413E.readOnly();
			txtQS413E_A1.readOnly();
			txtQS413E_A2.readOnly();
			rgQS413F.readOnly();
			
			txtQSOBS_SECCION4.setEnabled(false);
		}
	}
   
   
   public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
   }
   
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs409!=null) {salud.qs409=salud.getConvertqs401(salud.qs409);}
		if (salud.qs410!=null) {salud.qs410=salud.getConvertqs401(salud.qs410);}
		
		if (salud.qs410!=null) {
			if (!Util.esDiferente(salud.qs410,1) && txtQS410_A1.getText().toString().trim().length()!=0 ) {
				salud.qs410_a=Integer.parseInt(txtQS410_A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs410,2) && txtQS410_A2.getText().toString().trim().length()!=0) {
				salud.qs410_a=Integer.parseInt(txtQS410_A2.getText().toString().trim());
			}
		}		
		
		if (salud.qs411!=null) {salud.qs411=salud.getConvertqs401(salud.qs411);}
		if (salud.qs412!=null) {salud.qs412=salud.getConvertqs401(salud.qs412);}
		
		if (salud.qs412!=null) {
			if (!Util.esDiferente(salud.qs412,1) && txtQS412_A1.getText().toString().trim().length()!=0 ) {
				salud.qs412_a=Integer.parseInt(txtQS412_A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs412,2) && txtQS412_A2.getText().toString().trim().length()!=0) {
				salud.qs412_a=Integer.parseInt(txtQS412_A2.getText().toString().trim());
			}
		}
		
		if (salud.qs413!=null) {salud.qs413=salud.getConvertqs401(salud.qs413);}
		
		if (salud.qs413a!=null) {salud.qs413a=salud.getConvertqs401(salud.qs413a);}
		if (salud.qs413b!=null) {salud.qs413b=salud.getConvertqs413b(salud.qs413b);}
		if (salud.qs413c!=null) {salud.qs413c=salud.getConvertqs401(salud.qs413c);}
		if (salud.qs413d!=null) {salud.qs413d=salud.getConvertqs401(salud.qs413d);}
		if (salud.qs413e!=null) {salud.qs413e=salud.getConvertqs413e(salud.qs413e);}
		if (salud.qs413f!=null) {salud.qs413f=salud.getConvertqs401(salud.qs413f);}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
