package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CS_04Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS401; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS402; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS403; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS404;  
//	@FieldAnnotation(orderIndex=5) 
//	public IntegerField txtQS404A; 
//	@FieldAnnotation(orderIndex=6) 
//	public RadioGroupOtherField rgQS404B; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS404C; 
	@FieldAnnotation(orderIndex=6)
	public TextAreaField txtQSOBS_SECCION4;

	
	
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblObservaciones,lblP401,lblP402,lblP403,lblP404,lblP404c;//lblP404a,lblP404b,lblEspacio,lblP404b_edad;
	public GridComponent2 gridPreguntas404a;
	public CheckBoxField chbQS404AA;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
//	LinearLayout q8;	
 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	
	//public boolean rango_15_59=true;
	public boolean rango_15_75=true;

	public CS_04Fragment_001() {} 
	public CS_04Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS401","QS402","QS403","QS404","QS404C","ID","HOGAR_ID","PERSONA_ID","QSOBS_SECCION4")}; //"QS404A","QS404B"
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS401","QS402","QS403","QS404","QS404C")}; //"QS404A","QS404B"
		
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QH06","QS311")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_1).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblP401 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs401).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS401=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs401_1,R.string.cap04_07qs401_2,R.string.cap04_07qs401_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS401ChangeValue");
		
		lblP402 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs402).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS402=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs402_1,R.string.cap04_07qs402_2,R.string.cap04_07qs402_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS402ChangeValue"); 
		
		lblP403 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs403).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS403=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs403_1,R.string.cap04_07qs403_2,R.string.cap04_07qs403_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS403ChangeValue");
		
		lblP404 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs404).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS404=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs404_1,R.string.cap04_07qs404_2,R.string.cap04_07qs404_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
//		lblP404a = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs404a).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
//		lblEspacio  = new LabelComponent(this.getActivity()).size(altoComponente, 15);
//		lblP404b_edad  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.cap04_07qs404a_edad).textSize(16).centrar();
//		txtQS404A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).alinearDerecha();
//		chbQS404AA=new CheckBoxField(this.getActivity(), R.string.cap04_07qs404a_98, "1:0").size(60, 450);		
//		chbQS404AA.setOnCheckedChangeListener(new OnCheckedChangeListener() {				
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				// TODO Auto-generated method stub
//				if (isChecked){
//					MyUtil.LiberarMemoria();
//		    		Util.cleanAndLockView(getActivity(),txtQS404A);
//		    		rgQS404B.requestFocus();
//		  		}else {		  			
//					Util.lockView(getActivity(), false,txtQS404A);
//					//txtQS404A.setVisibility(View.VISIBLE);
//					txtQS404A.requestFocus();
//					}
//			}
//		});
		  
//		gridPreguntas404a = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
//		gridPreguntas404a.addComponent(lblP404b_edad);
//		gridPreguntas404a.addComponent(txtQS404A);
//		gridPreguntas404a.addComponent(lblEspacio);
//		gridPreguntas404a.addComponent(chbQS404AA);
		
//		lblP404b = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs404b).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
//		rgQS404B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs404b_1,R.string.cap04_07qs404b_2,R.string.cap04_07qs404b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblP404c = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs404c).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS404C=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs404c_1,R.string.cap04_07qs404c_2,R.string.cap04_07qs404c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);  
		txtQSOBS_SECCION4= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
    }
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblP401,rgQS401); 
		q3 = createQuestionSection(lblP402,rgQS402); 
		q4 = createQuestionSection(lblP403,rgQS403); 
		q5 = createQuestionSection(lblP404,rgQS404);
//		q6 = createQuestionSection(lblP404a,gridPreguntas404a.component());
//		q7 = createQuestionSection(lblP404b,rgQS404B);
		q6 = createQuestionSection(lblP404c,rgQS404C);
		q7 = createQuestionSection(R.string.cap04_07qsObservaSeccion4,txtQSOBS_SECCION4); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
//		form.addView(q8);
 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(salud); 
		
		if (salud.qs401!=null) {
			salud.qs401=salud.getConvertqs401(salud.qs401);
		}
		if (salud.qs402!=null) {
			salud.qs402=salud.getConvertqs401(salud.qs402);
		}
		if (salud.qs403!=null) {
			salud.qs403=salud.getConvertqs401(salud.qs403);
		}
		if (salud.qs404!=null) {
			salud.qs404=salud.getConvertqs401(salud.qs404);
		}	
//		if (salud.qs404b!=null) {
//			salud.qs404b=salud.getConvertqs401(salud.qs404b);
//		}
		if (salud.qs404c!=null) {
		salud.qs404c=salud.getConvertqs401(salud.qs404c);
		}
//		if (chbQS404AA.isChecked()){
//			salud.qs404a=98;
//		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS4_7().qs401=salud.qs401;
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(rango_15_75){
			if (Util.esVacio(salud.qs401)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.401"); 
				view = rgQS401; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs402)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.402"); 
				view = rgQS402; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs402,1)) { 
				if(Util.esVacio(salud.qs403)){
					mensaje = preguntaVacia.replace("$", "La pregunta P.403"); 
					view = rgQS403; 
					error = true; 
					return false;
				}
				if(!Util.esDiferente(salud.qs403,1)){
					if(Util.esVacio(salud.qs404)){
						mensaje = preguntaVacia.replace("$", "La pregunta P.404"); 
						view = rgQS404; 
						error = true; 
						return false; 
					}					
				}				
			}
			
//			if(Util.esVacio(salud.qs404a)){
//				mensaje = preguntaVacia.replace("$", "La pregunta P.404A"); 
//				view = txtQS404A; 
//				error = true; 
//				return false; 
//			}
//			if(Util.esVacio(salud.qs404b)){
//				mensaje = preguntaVacia.replace("$", "La pregunta P.404B"); 
//				view = rgQS404B; 
//				error = true; 
//				return false; 
//			}
			
			if(Util.esVacio(salud.qs404c)){
			mensaje = preguntaVacia.replace("$", "La pregunta P.404C"); 
			view = rgQS404C; 
			error = true; 
			return false; 
			}
		}
		return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	    	
		if(salud==null){ 
			salud=new CAP04_07(); 
			salud.id=App.getInstance().getPersonaSeccion01().id; 
			salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
			salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		
		if (salud.qs401!=null) {salud.qs401=salud.setConvertqs401(salud.qs401);}
    	if (salud.qs402!=null) {salud.qs402=salud.setConvertqs401(salud.qs402);}
    	if (salud.qs403!=null) {salud.qs403=salud.setConvertqs401(salud.qs403);}
    	if (salud.qs404!=null) {salud.qs404=salud.setConvertqs401(salud.qs404);}    
//    	if (salud.qs404b!=null) {salud.qs404b=salud.setConvertqs401(salud.qs404b);}    	
//    	if (salud.qs404a!=null && salud.qs404a==98) {			
//    		chbQS404AA.setChecked(true);    		
//		}
    	if (salud.qs404c!=null) {salud.qs404c=salud.setConvertqs401(salud.qs404c);} 
    	
		entityToUI(salud); 
		inicio(); 
		App.getInstance().setPersonabySalud(seccion1);
    } 
    
    private void inicio() {
    	entra_seccion4();
    	if(rango_15_75){
			onqrgQS401ChangeValue();
	    	onqrgQS402ChangeValue();
	    	onqrgQS403ChangeValue();
	    	ValidarsiesSupervisora();
//	    	if (salud.qs404a!=null && salud.qs404a==98) {				
//	    		chbQS404AA.setChecked(true);
//	    		Util.cleanAndLockView(getActivity(),txtQS404A);	    		
//			}
	    	
    	}
    	else{
    		grabar();
    	}
    	txtCabecera.requestFocus();
    } 
    
    
    public void entra_seccion4(){
    	if(!MyUtil.incluyeRango(15, 75, seccion1.qs23)){  
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
//    		q8.setVisibility(View.GONE);
    		rango_15_75 = false;
    	}
    	else{
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		rango_15_75 = true;
    		Util.lockView(getActivity(), false, rgQS401,rgQS402,rgQS403,rgQS404,rgQS404C);//,txtQS404A,rgQS404B

//    		if (salud.qs404a!=null && salud.qs404a==98) {				
//	    		chbQS404AA.setChecked(true);
//	    		Util.cleanAndLockView(getActivity(),txtQS404A);	    		
//			}
    		
    			
    		if(seccion1.qs23<18 || seccion1.qs23>75){
    				q7.setVisibility(View.VISIBLE);
    				seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS401","QS402","QS403","QS404","QS404C","QSOBS_SECCION4")};	//"QS404A","QS404B"				
    			}
    			else{
    				q7.setVisibility(View.GONE);    				
    			}

    	}
    }
    
    

    public void onqrgQS401ChangeValue(){
    	rgQS402.requestFocus();
    }
    
    public void onqrgQS402ChangeValue(){
    	if (MyUtil.incluyeRango(2,3,rgQS402.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS403, rgQS404);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
//			txtQS404A.requestFocus();
		} 
		else {				
			Util.lockView(getActivity(), false, rgQS403, rgQS404);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);	
			rgQS403.requestFocus();
		}
    }
    
    public void onqrgQS403ChangeValue(){
    	if (MyUtil.incluyeRango(2,3,rgQS403.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS404);
			q5.setVisibility(View.GONE);		
//			txtQS404A.requestFocus();
		} 
		else {
			if (MyUtil.incluyeRango(1,1,rgQS402.getTagSelected("").toString())) {
				Util.lockView(getActivity(), false, rgQS404);
				q5.setVisibility(View.VISIBLE);			
				rgQS404.requestFocus();
			}
		}
    }
    
   
    
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS401.readOnly();
			rgQS402.readOnly();
			rgQS403.readOnly();
			rgQS404.readOnly();
//			txtQS404A.readOnly();
//			rgQS404B.readOnly();
			rgQS404C.readOnly();
			txtQSOBS_SECCION4.setEnabled(false);
		}
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs401!=null) {
			salud.qs401=salud.getConvertqs401(salud.qs401);
		}
		if (salud.qs402!=null) {
			salud.qs402=salud.getConvertqs401(salud.qs402);
		}
		if (salud.qs403!=null) {
			salud.qs403=salud.getConvertqs401(salud.qs403);
		}
		if (salud.qs404!=null) {
			salud.qs404=salud.getConvertqs401(salud.qs404);
		}
//		if (salud.qs404b!=null) {
//			salud.qs404b=salud.getConvertqs401(salud.qs404b);
//		}
//		if (chbQS404AA.isChecked()){
//			salud.qs404a=98;
//		}
		if (salud.qs404c!=null) {
			salud.qs404c=salud.getConvertqs401(salud.qs404c);
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
