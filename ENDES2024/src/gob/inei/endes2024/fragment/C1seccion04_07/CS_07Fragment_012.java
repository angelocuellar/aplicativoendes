package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_07Fragment_012 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS719; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS720; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS721; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS722; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS723; 
	
	CAP04_07 cap04_07; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lblPreg719,lblPreg719_1,lblPreg720,lblPreg721,lblPreg722,lblPreg723; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	Salud seccion1; 
	boolean entro=false;
// 
	public CS_07Fragment_012() {} 
	public CS_07Fragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 

		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS713","QS714","QS715","QS716","QS717","QS719","QS720","QS721","QS722","QS723","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS719","QS720","QS721","QS722","QS723")}; 
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS206","QS209")};
        
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s7).textSize(21).centrar().negrita(); 
		lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s7).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		lblPreg719 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs719);
		
	    Spanned texto = Html.fromHtml("�En los �ltimos 12 meses, <b>hubo momentos</b> en que tuvo tantas ganas de beber que no pudo resistirse a tomar una copa o le result� dif�cil pensar en otra cosa?");
		lblPreg719_1 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg719_1.setText(texto);
		Spanned texto2 = Html.fromHtml("720. �En los �ltimos 12 meses, <b>hubo momentos</b> que necesit� beber m�s cantidad de alcohol para sentir el mismo efecto, o not� que ya no consegu�a <b>\"ponerse alegre\" </b>con la misma cantidad que beb�a habitualmente?");
		lblPreg720 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg720.setText(texto2);
		
		lblPreg721 = new LabelComponent(this.getActivity()).textSize(17).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs721);
		Spanned texto3 = Html.fromHtml("722. �En los �ltimos 12 meses, <b>hubo momentos</b> en que tom� un vaso, copa o botella <b>para evitar</b> tener problemas como los mencionados anteriormente? <br/><br/>(DE SER NECESARIO LEA: Como cansancio, dolores de cabeza, diarrea, temblores o problemas emocionales)");
		lblPreg722 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg722.setText(texto3);
		
		Spanned texto4 = Html.fromHtml("723. �En los �ltimos 12 meses, <b>hubo momentos</b> en que continu� bebiendo a pesar de que se hab�a <b>prometido</b> que no lo har�a, o bebi� mucho m�s de lo que se hab�a propuesto?");
		lblPreg723 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT); lblPreg723.setText(texto4);
		
		rgQS719=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs719_1,R.string.cap04_07qs719_2,R.string.cap04_07qs719_3,R.string.cap04_07qs719_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS720=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs720_1,R.string.cap04_07qs720_2,R.string.cap04_07qs720_3,R.string.cap04_07qs720_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS721=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs721_1,R.string.cap04_07qs721_2,R.string.cap04_07qs721_3,R.string.cap04_07qs721_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP721ChangeValue");  
		rgQS722=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs722_1,R.string.cap04_07qs722_2,R.string.cap04_07qs722_3,R.string.cap04_07qs722_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQS723=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs723_1,R.string.cap04_07qs723_2,R.string.cap04_07qs723_3,R.string.cap04_07qs723_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg719,lblPreg719_1,rgQS719); 
		q2 = createQuestionSection(lblPreg720,rgQS720); 
		q3 = createQuestionSection(lblPreg721,rgQS721); 
		q4 = createQuestionSection(lblPreg722,rgQS722); 
		q5 = createQuestionSection(lblPreg723,rgQS723); 
		 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
	 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		if (cap04_07.qs719 != null) {
			cap04_07.qs719 = cap04_07.getConvert713(cap04_07.qs719);
		}
		if (cap04_07.qs720 != null) {
			cap04_07.qs720 = cap04_07.getConvert713(cap04_07.qs720);
		}
		if (cap04_07.qs721 != null) {
			cap04_07.qs721 = cap04_07.getConvert713(cap04_07.qs721);
		}
		if (cap04_07.qs722 != null) {
			cap04_07.qs722 = cap04_07.getConvert713(cap04_07.qs722);
		}
		if (cap04_07.qs723 != null) {
			cap04_07.qs723 = cap04_07.getConvert713(cap04_07.qs723);
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//	    Log.e("","717="+preg717());
//	    Log.e("","entro="+entro);
		if(entro==false){
			if (Util.esVacio(cap04_07.qs719)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.719"); 
				view = rgQS719; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs720)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.720"); 
				view = rgQS720; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(cap04_07.qs721)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.721"); 
				view = rgQS721; 
				error = true; 
				return false; 
			} 
			if (MyUtil.incluyeRango(2,4, rgQS721.getTagSelected("").toString())) {
				if (Util.esVacio(cap04_07.qs722)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.722"); 
					view = rgQS722; 
					error = true; 
					return false; 
				} 	
			}
			
			if (Util.esVacio(cap04_07.qs723)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.723"); 
				view = rgQS723; 
				error = true; 
				return false; 
			} 
		}
		
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
//		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersona().id, App.getInstance().getPersona().hogar_id, App.getInstance().getPersona().persona_id,seccionesCargado); 
		cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
		seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    
    	entro=false;
    	
		if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		if (cap04_07.qs719 != null) {
			cap04_07.qs719 = cap04_07.setConvert713(cap04_07.qs719);
		}
		if (cap04_07.qs720 != null) {
			cap04_07.qs720 = cap04_07.setConvert713(cap04_07.qs720);
		}
		if (cap04_07.qs721 != null) {
			cap04_07.qs721 = cap04_07.setConvert713(cap04_07.qs721);
		}
		if (cap04_07.qs722 != null) {
			cap04_07.qs722 = cap04_07.setConvert713(cap04_07.qs722);
		}
		if (cap04_07.qs723 != null) {
			cap04_07.qs723 = cap04_07.setConvert713(cap04_07.qs723);
		}
		entityToUI(cap04_07); 
		inicio(); 
		App.getInstance().setPersona_04_07(null);
    	App.getInstance().setPersona_04_07(cap04_07);
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    	pregunta709();
//    	rgQS719.requestFocus();
//    	onP721ChangeValue();
    	txtCabecera.requestFocus();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
				rgQS719.readOnly(); 
				rgQS720.readOnly();
				rgQS721.readOnly();
				rgQS722.readOnly(); 
				rgQS723.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    public void onP721ChangeValue() {

		if (MyUtil.incluyeRango(1,1, rgQS721.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQS722);	
			q4.setVisibility(view.GONE);
		}		
		else {	
			    q4.setVisibility(view.VISIBLE);
				Util.lockView(getActivity(), false,rgQS722);
				rgQS722.requestFocus();	
		}
		
	}
    public boolean preg717(){
    	
    	 boolean band=!MyUtil.incluyeRango(1,1,cap04_07.qs713) && !MyUtil.incluyeRango(1,1,cap04_07.qs714) && !MyUtil.incluyeRango(1,1,cap04_07.qs715) 
    			 && !MyUtil.incluyeRango(1,1,cap04_07.qs716) && !MyUtil.incluyeRango(1,1,cap04_07.qs717);
    	
    	 
    	 return band;
    }
    public void pregunta709(){
    	if(seccion1.qs209==null || seccion1.qs209!=1 || preg717()){
    		entro=true;
    		Util.cleanAndLockView(this.getActivity(),rgQS719,rgQS720,rgQS721,rgQS722,rgQS723);
    		q1.setVisibility(view.GONE);
			q2.setVisibility(view.GONE);
			q3.setVisibility(view.GONE);
			q4.setVisibility(view.GONE);
			q5.setVisibility(view.GONE);
    	}else{
    		q1.setVisibility(view.VISIBLE);
			q2.setVisibility(view.VISIBLE);
			q3.setVisibility(view.VISIBLE);
			q4.setVisibility(view.VISIBLE);
			q5.setVisibility(view.VISIBLE);
	
    		Util.lockView(getActivity(), false,rgQS719,rgQS720,rgQS721,rgQS722,rgQS723);
    		onP721ChangeValue();
    	}
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs719 != null) {
				cap04_07.qs719 = cap04_07.getConvert713(cap04_07.qs719);
			}
			if (cap04_07.qs720 != null) {
				cap04_07.qs720 = cap04_07.getConvert713(cap04_07.qs720);
			}
			if (cap04_07.qs721 != null) {
				cap04_07.qs721 = cap04_07.getConvert713(cap04_07.qs721);
			}
			if (cap04_07.qs722 != null) {
				cap04_07.qs722 = cap04_07.getConvert713(cap04_07.qs722);
			}
			if (cap04_07.qs723 != null) {
				cap04_07.qs723 = cap04_07.getConvert713(cap04_07.qs723);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
} 
