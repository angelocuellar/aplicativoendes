package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_04Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS415; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS416; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS416_A1; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS416_A2;
	
//	@FieldAnnotation(orderIndex=5) 
//	public RadioGroupOtherField rgQS418; 
//	@FieldAnnotation(orderIndex=6) 
//	public RadioGroupOtherField rgQS419; 
//	@FieldAnnotation(orderIndex=7) 
//	public IntegerField txtQS419_A1; 
//	@FieldAnnotation(orderIndex=8) 
//	public IntegerField txtQS419_A2;
	
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS418A; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS419A; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQS419A_A1; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQS419A_A2;
	
	@FieldAnnotation(orderIndex=9)
	public TextAreaField txtQSOBS_SECCION4;
	
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lblTitulo3,lblpregunta415,lblP415_extra,lblpregunta416,lblP416_extra,lblObservaciones,lblvacio,lblpregunta418a,lblpregunta418a_ind1,lblpregunta418a_ind2,lblpregunta419a,lblpregunta419a_ind,lblvacio1;//lblpregunta418, lblpregunta418_ind1,lblpregunta418_ind2,lblpregunta419,lblpregunta419_ind 
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7;
	 
 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01;
	
	public GridComponent2 gridPreguntas416,gridPreguntas419a;
	
	Salud seccion1;
	
	//public boolean mujer_40_59 = true;
	public boolean mujer_40_70 = true;
	public boolean hombre_50_75 = true;//hombre_50_70
	
	public CS_04Fragment_004() {} 
	public CS_04Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS415","QS416","QS416_A","QS418A","QS419A","QS419A_A","QSOBS_SECCION4","ID","HOGAR_ID","PERSONA_ID","QSOBS_SECCION4")};//"QS418","QS419","QS419_A" 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS415","QS416","QS416_A","QS418A","QS419A","QS419A_A","QSOBS_SECCION4")};//"QS418","QS419","QS419_A"
		
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QH06","QH02_1","QS311")};
		
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_4).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);		
		lblTitulo3=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07_4_H).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblpregunta415 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs415);
		lblP415_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs415_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQS415=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs415_1,R.string.cap04_07qs415_2,R.string.cap04_07qs415_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS415ChangeValue"); 
		
		lblpregunta416 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs416);
		lblP416_extra = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs410_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda().negrita();
		rgQS416=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs416_1,R.string.cap04_07qs416_2,R.string.cap04_07qs416_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS416ChangeValue"); 
		txtQS416_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS416_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas416=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas416.addComponent(rgQS416,1,3);		
		gridPreguntas416.addComponent(txtQS416_A1);
		gridPreguntas416.addComponent(txtQS416_A2);
		gridPreguntas416.addComponent(lblvacio);	
		
		
//		lblpregunta418 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs418);
//		lblpregunta418a_ind1 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs418_ind1).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda().negrita();
//		lblpregunta418a_ind2 = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs418_ind2).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
//		rgQS418=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs418_1,R.string.cap04_07qs418_2,R.string.cap04_07qs418_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS418ChangeValue"); 
		
//		lblpregunta419 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs419);
//		lblpregunta419_ind = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs419_ind).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda().negrita();
//		rgQS419=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs419_1,R.string.cap04_07qs419_2,R.string.cap04_07qs419_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS419ChangeValue"); 
//		txtQS419_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
//		txtQS419_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
//		lblvacio1 = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		lblpregunta418a = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs418a);
		rgQS418A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs418a_1,R.string.cap04_07qs418a_2,R.string.cap04_07qs418a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS418aChangeValue");		
		
		lblpregunta419a = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs419a);
		lblpregunta419a_ind = new LabelComponent(this.getActivity()).text(R.string.cap04_07qs419a_ind).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda().negrita();
		rgQS419A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs419a_1,R.string.cap04_07qs419a_2,R.string.cap04_07qs419a_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS419aChangeValue"); 
		txtQS419A_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS419A_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio1 = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
//		gridPreguntas419=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
//		gridPreguntas419.addComponent(rgQS419,1,3);		
//		gridPreguntas419.addComponent(txtQS419_A1);
//		gridPreguntas419.addComponent(txtQS419_A2);
//		gridPreguntas419.addComponent(lblvacio1);
		
		gridPreguntas419a=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas419a.addComponent(rgQS419A,1,3);		
		gridPreguntas419a.addComponent(txtQS419A_A1);
		gridPreguntas419a.addComponent(txtQS419A_A2);
		gridPreguntas419a.addComponent(lblvacio1);
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);  
		txtQSOBS_SECCION4= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		textoNegrita();
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblpregunta415,lblP415_extra,rgQS415); 
		q3 = createQuestionSection(lblpregunta416,lblP416_extra,gridPreguntas416.component());
		q4 = createQuestionSection(lblTitulo3);
//		q5 = createQuestionSection(lblpregunta418,lblpregunta418_ind1,lblpregunta418_ind2,rgQS418); 
//		q6 = createQuestionSection(lblpregunta419,lblpregunta419_ind,gridPreguntas419.component());
		q5 = createQuestionSection(lblpregunta418a,rgQS418A); 
		q6 = createQuestionSection(lblpregunta419a,lblpregunta419a_ind,gridPreguntas419a.component());
		q7 = createQuestionSection(R.string.cap04_07qsObservaSeccion4,txtQSOBS_SECCION4); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {
    	if(salud!=null){
		uiToEntity(salud); 
		
		if (salud.qs415!=null) {salud.qs415=salud.getConvertqs401(salud.qs415);}
		if (salud.qs416!=null) {salud.qs416=salud.getConvertqs401(salud.qs416);}
//		if (salud.qs418!=null) {salud.qs418=salud.getConvertqs401(salud.qs418);}
//		if (salud.qs419!=null) {salud.qs419=salud.getConvertqs401(salud.qs419);}
		if (salud.qs418a!=null) {salud.qs418a=salud.getConvertqs401(salud.qs418a);}
		if (salud.qs419a!=null) {salud.qs419a=salud.getConvertqs401(salud.qs419a);}
		
		if (!Util.esDiferente(salud.qs416,1) && txtQS416_A1.getText().toString().trim().length()!=0 ) {
			salud.qs416_a=Integer.parseInt(txtQS416_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs416,2) && txtQS416_A2.getText().toString().trim().length()!=0) {
			salud.qs416_a=Integer.parseInt(txtQS416_A2.getText().toString().trim());
		}
				
//		if (!Util.esDiferente(salud.qs419,1) && txtQS419_A1.getText().toString().trim().length()!=0 ) {
//			salud.qs419_a=Integer.parseInt(txtQS419_A1.getText().toString().trim());
//		}
//		if (!Util.esDiferente(salud.qs419,2) && txtQS419_A2.getText().toString().trim().length()!=0) {
//			salud.qs419_a=Integer.parseInt(txtQS419_A2.getText().toString().trim());
//		}
		
		if (!Util.esDiferente(salud.qs419a,1) && txtQS419A_A1.getText().toString().trim().length()!=0 ) {
			salud.qs419a_a=Integer.parseInt(txtQS419A_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs419a,2) && txtQS419A_A2.getText().toString().trim().length()!=0) {
			salud.qs419a_a=Integer.parseInt(txtQS419A_A2.getText().toString().trim());
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
    	}
		return true; 
    }

    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(mujer_40_70 && seccion1.qh06==2){
			if (Util.esVacio(salud.qs415)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.415"); 
				view = rgQS415; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(salud.qs416) && !Util.esDiferente(salud.qs415,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.416"); 
				view = rgQS416; 
				error = true; 
				return false; 
			} 
			if(Util.esDiferente(salud.qs416,8) && !Util.esDiferente(salud.qs415,1)){
				if (Util.esVacio(salud.qs416_a) && !Util.esDiferente(salud.qs416,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.416_a");
					view = txtQS416_A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs416_a) && !Util.esDiferente(salud.qs416,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.416_a");
					view = txtQS416_A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 23, salud.qs416_a) && !Util.esDiferente(salud.qs416,1)){
					mensaje = ""+salud.qs416_a+" Esta fuera del rango: 0-23";
					view = txtQS416_A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs416_a) && !Util.esDiferente(salud.qs416,2)){
					mensaje = ""+salud.qs416_a+" Esta fuera del rango: 2-"+seccion1.qs23;
					view = txtQS416_A2; 
					error = true; 
					return false;
				}
			}
		}
		
		
		if(hombre_50_75 && seccion1.qh06==1){
			if (Util.esVacio(salud.qs418a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.418a"); 
				view = rgQS418A; 
				error = true; 
				return false; 
			} 
			if (!Util.esDiferente(salud.qs418a,1)) { 
				if (Util.esVacio(salud.qs418a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.418a"); 
					view = rgQS418A; 
					error = true; 
					return false; 
				} 
				if(Util.esDiferente(salud.qs419a,8)){
					if (Util.esVacio(salud.qs419a_a) && !Util.esDiferente(salud.qs419a,1)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.419_a");
						view = txtQS419A_A1; 
						error = true; 
						return false; 
					}
					if (Util.esVacio(salud.qs419a_a) && !Util.esDiferente(salud.qs419a,2)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.416_a");
						view = txtQS419A_A2; 
						error = true; 
						return false; 
					}
					if(!MyUtil.incluyeRango(0, 23, salud.qs419a_a) && !Util.esDiferente(salud.qs419a,1)){
						mensaje = ""+salud.qs419a_a+" Esta fuera del rango: 0-23";
						view = txtQS419A_A1; 
						error = true; 
						return false;
					}
					if(!MyUtil.incluyeRango(2, seccion1.qs23, salud.qs419a_a) && !Util.esDiferente(salud.qs419a,2)){
						mensaje = ""+salud.qs419a_a+" Esta fuera del rango: 2-"+seccion1.qs23;
						view = txtQS419A_A2; 
						error = true; 
						return false;
					}
				}
			}
		}
		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() {
    	salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
    	
		if(salud==null){ 
			salud=new CAP04_07(); 
			salud.id=App.getInstance().getPersonaSeccion01().id; 
			salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
			salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		
		if (salud.qs415!=null) {salud.qs415=salud.setConvertqs401(salud.qs415);}
		if (salud.qs416!=null) {salud.qs416=salud.setConvertqs401(salud.qs416);}
//		if (salud.qs418!=null) {salud.qs418=salud.setConvertqs401(salud.qs418);}
//		if (salud.qs419!=null) {salud.qs419=salud.setConvertqs401(salud.qs419);}
		if (salud.qs418a!=null) {salud.qs418a=salud.setConvertqs401(salud.qs418a);}
		if (salud.qs419a!=null) {salud.qs419a=salud.setConvertqs401(salud.qs419a);}
		entityToUI(salud); 

		if (salud.qs416_a!=null) {	
			if (!Util.esDiferente(salud.qs416,1)) {txtQS416_A1.setText(salud.qs416_a.toString());}
	  		if (!Util.esDiferente(salud.qs416,2)) {txtQS416_A2.setText(salud.qs416_a.toString());}
	  	}
//		if (salud.qs419_a!=null) {	
//			if (!Util.esDiferente(salud.qs419,1)) {txtQS419_A1.setText(salud.qs419_a.toString());}
//	  		if (!Util.esDiferente(salud.qs419,2)) {txtQS419_A2.setText(salud.qs419_a.toString());}
//	  	}
		
		if (salud.qs419a_a!=null) {	
			if (!Util.esDiferente(salud.qs419a,1)) {txtQS419A_A1.setText(salud.qs419a_a.toString());}
	  		if (!Util.esDiferente(salud.qs419a,2)) {txtQS419A_A2.setText(salud.qs419a_a.toString());}
	  	}
		
		inicio(); 
		App.getInstance().setPersonabySalud(seccion1);
    } 
    
    private void inicio() {  
    	entra_seccion4_Mujer();   
		entra_seccion4_Hombre();
//    	if(mujer_40_70 || hombre_50_75){
//    		
//    	}
//    	else{
//    		grabar();
//    	}  	
    	txtCabecera.requestFocus();
    	ValidarsiesSupervisora();
    	
    	if( (MyUtil.incluyeRango(40,70,seccion1.qs23)&& MyUtil.incluyeRango(2,2,seccion1.qh06))|| (MyUtil.incluyeRango(50,75,seccion1.qs23) && MyUtil.incluyeRango(1,1,seccion1.qh06))){
    		q7.setVisibility(View.VISIBLE);
    	}
    	else{
    		q7.setVisibility(View.GONE);
    	}
    } 
    
    public void entra_seccion4_Mujer(){
    	Log.e("","sss: ");
   	  if((!MyUtil.incluyeRango(40, 70, seccion1.qs23)|| !MyUtil.incluyeRango(2, 2, seccion1.qh06))){
   		  	MyUtil.LiberarMemoria();
   		  	Util.cleanAndLockView(this.getActivity(),rgQS415,rgQS416,txtQS416_A1,txtQS416_A2);
   			q0.setVisibility(View.GONE);
      		q1.setVisibility(View.GONE);
      		q2.setVisibility(View.GONE);
      		q3.setVisibility(View.GONE);      		
   			mujer_40_70 = false;
   			Log.e("","1111: ");
   		}
   	  else{
   		  Log.e("","2222: ");
   		  Util.lockView(getActivity(), false, rgQS415,rgQS416);
   		  q0.setVisibility(View.VISIBLE);
   		  q1.setVisibility(View.VISIBLE);
   		  q2.setVisibility(View.VISIBLE);
   		  q3.setVisibility(View.VISIBLE);   		  
   		  onqrgQS415ChangeValue();      		
   		  mujer_40_70 = true;   			
   		  //seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS415","QS416","QS416_A","QS418","QS419","QS419_A","QSOBS_SECCION4")};
   		}
    }
    
    public void entra_seccion4_Hombre(){
     	  if((!MyUtil.incluyeRango(50, 75, seccion1.qs23)|| !MyUtil.incluyeRango(1, 1, seccion1.qh06))){//50, 70
     		 Log.e("","33333: ");
     		  MyUtil.LiberarMemoria();
     		  Util.cleanAndLockView(this.getActivity(),rgQS418A,rgQS419A,txtQS419A_A1,txtQS419A_A2);
     		  q4.setVisibility(View.GONE);
     		  q5.setVisibility(View.GONE);
     		  q6.setVisibility(View.GONE);     		  
     		  hombre_50_75 = false;
     	 }
     	 else{
     		Log.e("","444444: ");
     		Util.lockView(getActivity(), false,rgQS418A,rgQS419A);
     		q0.setVisibility(View.VISIBLE);
     		q4.setVisibility(View.VISIBLE);
     		q5.setVisibility(View.VISIBLE);
     		q6.setVisibility(View.VISIBLE);
     		onqrgQS418aChangeValue();
     		hombre_50_75 = true;     		
     		//seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS415","QS416","QS416_A","QS418","QS419","QS419_A","QSOBS_SECCION4")};
     	}
      }
    
    public void onqrgQS415ChangeValue(){
//    	Log.e("","aaaa");
    	if (MyUtil.incluyeRango(2,3,rgQS415.getTagSelected("").toString())) {
    		Log.e("","aaaa");
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS416,txtQS416_A1,txtQS416_A2);
			q3.setVisibility(View.GONE);
		} 
		else {
			Log.e("","bbbb");
			Util.lockView(getActivity(), false,rgQS416);
			q3.setVisibility(View.VISIBLE);
			onqrgQS416ChangeValue();
			if (salud.qs416==null) {Log.e("","fffff"); Util.cleanAndLockView(getActivity(),txtQS416_A1,txtQS416_A2);}
			rgQS416.requestFocus();
		}
    }
    
    public void onqrgQS416ChangeValue(){
    	if (!MyUtil.incluyeRango(3,3,rgQS416.getTagSelected("").toString())) {
    		Log.e("","cccccc");
		   if(MyUtil.incluyeRango(1,1,rgQS416.getTagSelected("").toString())){
			   Log.e("","dddddd");
			    Util.cleanAndLockView(getActivity(),txtQS416_A2);
				//salud.qs416_a=null;
				Util.lockView(getActivity(), false, txtQS416_A1);
				txtQS416_A1.setVisibility(View.VISIBLE);
				txtQS416_A1.requestFocus();
		   }
		   if(MyUtil.incluyeRango(2,2,rgQS416.getTagSelected("").toString())){
			   Log.e("","eeeeeee");
			    Util.cleanAndLockView(getActivity(),txtQS416_A1);
				//salud.qs416_a=null;
				Util.lockView(getActivity(), false, txtQS416_A2);
				txtQS416_A2.setVisibility(View.VISIBLE);
				txtQS416_A2.requestFocus();
		   }
		} 
		else {
			Log.e("","juujjj");
			Util.cleanAndLockView(this.getActivity(),txtQS416_A1,txtQS416_A2);	
		}
    }
    
    
    public void onqrgQS418aChangeValue(){
    	if (MyUtil.incluyeRango(2,3,rgQS418A.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS419A,txtQS419A_A1,txtQS419A_A2);
			q6.setVisibility(View.GONE);
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS419A);
			q6.setVisibility(View.VISIBLE);
			onqrgQS419aChangeValue();
			rgQS419A.requestFocus();
		}
    }
    
    public void onqrgQS419aChangeValue(){
    	if (!MyUtil.incluyeRango(3,3,rgQS419A.getTagSelected("").toString())) {
		   if(MyUtil.incluyeRango(1,1,rgQS419A.getTagSelected("").toString())){
			    Util.cleanAndLockView(getActivity(),txtQS419A_A2);
				//salud.qs419_a=null;
				Util.lockView(getActivity(), false, txtQS419A_A1);
				txtQS419A_A1.setVisibility(View.VISIBLE);
				txtQS419A_A1.requestFocus();
		   }
		   if(MyUtil.incluyeRango(2,2,rgQS419A.getTagSelected("").toString())){
			    Util.cleanAndLockView(getActivity(),txtQS419A_A1);
				//salud.qs419_a=null;
				Util.lockView(getActivity(), false, txtQS419A_A2);
				txtQS419A_A2.setVisibility(View.VISIBLE);
				txtQS419A_A2.requestFocus();
		   }
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS419A_A1,txtQS419A_A2);	
		}
    }
    
    
  
    
    public void textoNegrita(){
    	lblP415_extra.text(R.string.cap04_07qs415_ex);
 	    Spanned texto415_ex = Html.fromHtml("<b>DE SER NECESARIO LEA: </b>"+lblP415_extra.getText());    	
 	    lblP415_extra.setText(texto415_ex);
    }
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS415.readOnly();
			rgQS416.readOnly();
			txtQS416_A1.readOnly();
			txtQS416_A2.readOnly();	
			rgQS418A.readOnly();
			rgQS419A.readOnly();
			txtQS419A_A1.readOnly();
			txtQS419A_A2.readOnly();
			txtQSOBS_SECCION4.setEnabled(false);
		}
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud); 
		
		if (salud.qs415!=null) {salud.qs415=salud.getConvertqs401(salud.qs415);}
		if (salud.qs416!=null) {salud.qs416=salud.getConvertqs401(salud.qs416);}
		if (salud.qs418a!=null) {salud.qs418a=salud.getConvertqs401(salud.qs418a);}
		if (salud.qs419a!=null) {salud.qs419a=salud.getConvertqs401(salud.qs419a);}
		
		if (salud.qs416!=null) {
			if (!Util.esDiferente(salud.qs416,1) && txtQS416_A1.getText().toString().trim().length()!=0 ) {
				salud.qs416_a=Integer.parseInt(txtQS416_A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs416,2) && txtQS416_A2.getText().toString().trim().length()!=0) {
				salud.qs416_a=Integer.parseInt(txtQS416_A2.getText().toString().trim());
			}
		}
		
		if (!Util.esDiferente(salud.qs419a,1) && txtQS419A_A1.getText().toString().trim().length()!=0 ) {
			salud.qs419a_a=Integer.parseInt(txtQS419A_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs419a,2) && txtQS419A_A2.getText().toString().trim().length()!=0) {
			salud.qs419a_a=Integer.parseInt(txtQS419A_A2.getText().toString().trim());
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
