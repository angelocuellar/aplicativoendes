package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class CS_05Fragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS500; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS501; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQS501_A1;
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQS501_A2;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQS503;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQS505A; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQS505B; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQS505C; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQS505D; 
	
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQS505A1; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQS505A1_O; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQS505B1; 
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQS505B1_O; 
	
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQS506; 
	
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQS506A; 
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQS506B; 
	
	@FieldAnnotation(orderIndex=17)
	public TextAreaField txtQSOBS_SECCION5;
	CAP04_07 salud; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblTitulo2,lbl505A,lbl505B,lbl505C,lbl505D,lblObservaciones; 
	private LabelComponent lblpregunta500,lblpregunta501,lblP501_extra,lblpregunta503,lblpregunta505,lblpregunta505a1,lblpregunta505b1,lblpregunta506,lblvacio,lblpregunta506a,lblpregunta506b;//
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01;
	
	public GridComponent2 gridPreguntas501;
	public GridComponent2 gridPreguntas505;
	
	Salud seccion1;
	
	public boolean persona_mayor_15=true;
	public boolean hombre_mayor_15=true;
	public boolean mujer_mayor_50=true;
	public boolean mujer_15_49=true;
	
// 
	public CS_05Fragment_005() {} 
	public CS_05Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
//		rango(getActivity(), txtQS501_A1, 0, 90, null, null); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS500","QS501","QS501_A","QS503","ID","QS505A","QS505B","QS505C","QS505D","QS505A1","QS505A1_O","QS505B1","QS505B1_O","QS506","HOGAR_ID","PERSONA_ID","QSOBS_SECCION5","QS506A","QS506B")};// 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS500","QS501","QS501_A","QS503","QS505A","QS505B","QS505C","QS505D","QS505A1","QS505A1_O","QS505B1","QS505B1_O","QS506","QS506A","QS506B")};//
		
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QH06","QH02_1","QS28")};
		
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloSec05).textSize(21).centrar().negrita(); 
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloSec05_1).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		
		lblpregunta500 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs500);
		rgQS500=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS500ChangeValue"); 
		
		lblpregunta501 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs501);
		lblP501_extra = new LabelComponent(this.getActivity()).text(R.string.cap01_03qs501_ex).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().alinearIzquierda().negrita();
		rgQS501=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs501_1,R.string.cap04_07qs501_2,R.string.cap04_07qs501_3).size(altoComponente+70,550).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS501ChangeValue"); 
		txtQS501_A1=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2); 
		txtQS501_A2=new IntegerField(this.getActivity()).size(altoComponente+15, 90).maxLength(2);
		lblvacio = new LabelComponent(this.getActivity()).size(altoComponente+15, 90);
		
		gridPreguntas501=new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas501.addComponent(rgQS501,1,3);		
		gridPreguntas501.addComponent(txtQS501_A1);
		gridPreguntas501.addComponent(txtQS501_A2);
		gridPreguntas501.addComponent(lblvacio);
		
		lblpregunta503 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs503);
		rgQS503=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs503_1,R.string.cap04_07qs503_2,R.string.cap04_07qs503_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS503ChangeValue");
		
		lblpregunta505 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs505);
		lbl505A = new LabelComponent(this.getActivity()).size(altoComponente + 60, 290).text(R.string.cap04_07qs505a).textSize(16).alinearIzquierda();
		lbl505B = new LabelComponent(this.getActivity()).size(altoComponente + 60, 290).text(R.string.cap04_07qs505b).textSize(16).alinearIzquierda();
		lbl505C = new LabelComponent(this.getActivity()).size(altoComponente + 60, 290).text(R.string.cap04_07qs505c).textSize(16).alinearIzquierda();
		lbl505D = new LabelComponent(this.getActivity()).size(altoComponente + 60, 290).text(R.string.cap04_07qs505d).textSize(16).alinearIzquierda();
				
		rgQS505A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505a_1,R.string.cap04_07qs505a_2,R.string.cap04_07qs505a_3).size(altoComponente + 60, 485).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS505AChangeValue"); 
		//rgQS505A.agregarEspecifique(6, txtQS505A1_O);
		
		rgQS505B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505b_1,R.string.cap04_07qs505b_2,R.string.cap04_07qs505b_3).size(altoComponente + 60, 485).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS505BChangeValue"); 
		//rgQS505B.agregarEspecifique(6, txtQS505B1_O);
		
		rgQS505C=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505c_1,R.string.cap04_07qs505c_2,R.string.cap04_07qs505c_3).size(altoComponente + 60, 485).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS505CChangeValue"); 
		rgQS505D=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505d_1,R.string.cap04_07qs505d_2,R.string.cap04_07qs505d_3).size(altoComponente + 60, 485).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS505DChangeValue"); 
		
		gridPreguntas505 = new GridComponent2(this.getActivity(), 2);
		gridPreguntas505.addComponent(lbl505A);
		gridPreguntas505.addComponent(rgQS505A);
		gridPreguntas505.addComponent(lbl505B);
		gridPreguntas505.addComponent(rgQS505B);
		gridPreguntas505.addComponent(lbl505C);
		gridPreguntas505.addComponent(rgQS505C);
		gridPreguntas505.addComponent(lbl505D);
		gridPreguntas505.addComponent(rgQS505D);
		
		lblpregunta505a1 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs505a1);
		rgQS505A1=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505a1_1,R.string.cap04_07qs505a1_2,R.string.cap04_07qs505a1_3,R.string.cap04_07qs505a1_4,R.string.cap04_07qs505a1_5,R.string.cap04_07qs505a1_6,R.string.cap04_07qs505a1_7,R.string.cap04_07qs505a1_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS505A1ChangeValue").callback("onqrgQS505A1ChangeValue");
		txtQS505A1_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar();
		rgQS505A1.agregarEspecifique(6,txtQS505A1_O);
		
		lblpregunta505b1 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs505b1);
		rgQS505B1=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs505b1_1,R.string.cap04_07qs505b1_2,R.string.cap04_07qs505b1_3,R.string.cap04_07qs505b1_4,R.string.cap04_07qs505b1_5,R.string.cap04_07qs505b1_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQS505B1ChangeValue");
		txtQS505B1_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500).hint(R.string.especifique).centrar();
		rgQS505B1.agregarEspecifique(4,txtQS505B1_O);
		
		lblpregunta506 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs506);
		rgQS506=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs506_1,R.string.cap04_07qs506_2,R.string.cap04_07qs506_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS506ChangeValue");
		
		lblpregunta506a = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs506a);
		rgQS506A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs506a_1,R.string.cap04_07qs506a_2,R.string.cap04_07qs506a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS506aChangeValue");
		
		lblpregunta506b = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs506b);
		rgQS506B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs506b_1,R.string.cap04_07qs506b_2,R.string.cap04_07qs506b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS506bChangeValue");
		
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);  
		txtQSOBS_SECCION5= new TextAreaField(getActivity()).size(250, 650).maxLength(500).alfanumerico();
		
		textoNegrita();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblTitulo2); 
		q2 = createQuestionSection(lblpregunta500,rgQS500); 		
		q3 = createQuestionSection(lblpregunta501,lblP501_extra,gridPreguntas501.component());
		q4 = createQuestionSection(lblpregunta503,rgQS503); 
		q5 = createQuestionSection(lblpregunta505,gridPreguntas505.component());
		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta505a1,rgQS505A1);
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta505b1,rgQS505B1);
		
		q8 = createQuestionSection(lblpregunta506,rgQS506);
		q9 = createQuestionSection(lblpregunta506a,rgQS506A);
		q10 = createQuestionSection(lblpregunta506b,rgQS506B);
		
		q11 = createQuestionSection(R.string.cap04_07qsObservaSeccion5,txtQSOBS_SECCION5); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		form.addView(q11);

    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		uiToEntity(salud);		
		
		if (salud.qs501!=null) {
			salud.qs501=salud.getConvertqs401(salud.qs501);
		}
		
		if (!Util.esDiferente(salud.qs501,1) && txtQS501_A1.getText().toString().trim().length()!=0 ) {
			salud.qs501_a=Integer.parseInt(txtQS501_A1.getText().toString().trim());
		}
		if (!Util.esDiferente(salud.qs501,2) && txtQS501_A2.getText().toString().trim().length()!=0) {
			salud.qs501_a=Integer.parseInt(txtQS501_A2.getText().toString().trim());
		}
		
		if (salud.qs503!=null) {
			salud.qs503=salud.getConvertqs401(salud.qs503);
		}
		if (salud.qs505a!=null) {
			salud.qs505a=salud.getConvertqs401(salud.qs505a);
		}
		if (salud.qs505b!=null) {
			salud.qs505b=salud.getConvertqs401(salud.qs505b);
		}
		if (salud.qs505c!=null) {
			salud.qs505c=salud.getConvertqs401(salud.qs505c);
		}
		if (salud.qs505d!=null) {
			salud.qs505d=salud.getConvertqs401(salud.qs505d);
		}
		
		if (salud.qs505a1!=null) {
			salud.qs505a1=salud.getConvertqs505a1(salud.qs505a1);
		}
		if (salud.qs505b1!=null) {
			salud.qs505b1=salud.getConvertqs505b1(salud.qs505b1);
		}
		
		if (salud.qs506!=null) {
			salud.qs506=salud.getConvertqs401(salud.qs506);
		}
		
		if (salud.qs506a!=null) {
			salud.qs506a=salud.getConvertqs401(salud.qs506a);
		}
		if (salud.qs506b!=null) {
			salud.qs506b=salud.getConvertqs401(salud.qs506b);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS4_7().qs500=salud.qs500;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(persona_mayor_15){
			if (Util.esVacio(salud.qs500)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS500"); 
				view = rgQS500; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(salud.qs501) && !Util.esDiferente(salud.qs500,1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QS501"); 
				view = rgQS501; 
				error = true; 
				return false; 
			}
			if(Util.esDiferente(salud.qs501,8) && !Util.esDiferente(salud.qs500,1)){
				if (Util.esVacio(salud.qs501_a) && !Util.esDiferente(salud.qs501,1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.501_a");
					view = txtQS501_A1; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(salud.qs501_a) && !Util.esDiferente(salud.qs501,2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.501_a");
					view = txtQS501_A2; 
					error = true; 
					return false; 
				}
				if(!MyUtil.incluyeRango(0, 29, salud.qs501_a) && !Util.esDiferente(salud.qs501,1)){
					mensaje = ""+salud.qs501_a+" Esta fuera del rango: 0-29";
					view = txtQS501_A1; 
					error = true; 
					return false;
				}
				if(!MyUtil.incluyeRango(1, 90, salud.qs501_a) && !Util.esDiferente(salud.qs501,2)){
					mensaje = ""+salud.qs501_a+" Esta fuera del rango: 1-90";
					view = txtQS501_A2; 
					error = true; 
					return false;
				}
			}
			if (Util.esVacio(salud.qs503) && (hombre_mayor_15 || mujer_mayor_50)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.503"); 
				view = rgQS503; 
				error = true; 
				return false; 
			} 
			if(((hombre_mayor_15 || mujer_mayor_50)&&!Util.esDiferente(salud.qs503,1))||(mujer_15_49 && MyUtil.incluyeRango(1,1,seccion1.qs28.toString()))){
				if (Util.esVacio(salud.qs505a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.505A"); 
					view = rgQS505A; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs505b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.505B"); 
					view = rgQS505B; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs505c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.505C"); 
					view = rgQS505C; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(salud.qs505d)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.505D"); 
					view = rgQS505D; 
					error = true; 
					return false; 
				}
				
				if (Util.esVacio(salud.qs505a1)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QS505A1");
					view = rgQS505A1;
					error = true;
					return false;
				}
				if(!Util.esDiferente(salud.qs505a1,7)){
					if (Util.esVacio(salud.qs505a1_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique";
						view = txtQS505A1_O;
						error = true;
						return false;
					}
				}
				
				if (Util.esVacio(salud.qs505b1)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QS505B1");
					view = rgQS505B1;
					error = true;
					return false;
				}
				if(!Util.esDiferente(salud.qs505b1,5)){
					if (Util.esVacio(salud.qs505b1_o)) {
						mensaje = "Debe ingresar informaci\u00f3n en Especifique";
						view = txtQS505B1_O;
						error = true;
						return false;
					}
				}
			}
			 
			if (Util.esVacio(salud.qs506) && ((hombre_mayor_15 || mujer_mayor_50)&&!Util.esDiferente(salud.qs503,1))) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.506"); 
				view = rgQS506; 
				error = true; 
				return false; 
			}
			
			if (Util.esVacio(salud.qs506a) && ((hombre_mayor_15 || mujer_mayor_50)&&!Util.esDiferente(salud.qs503,1))) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.506A"); 
				view = rgQS506A; 
				error = true; 
				return false; 
			}
			
			if (Util.esVacio(salud.qs506b) && ((hombre_mayor_15 || mujer_mayor_50)&&!Util.esDiferente(salud.qs503,1))) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.506B"); 
				view = rgQS506B; 
				error = true; 
				return false; 
			}			

		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	salud = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado);
    	
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
		if(salud==null){ 
			salud=new CAP04_07(); 
		  salud.id=App.getInstance().getPersonaSeccion01().id; 
		  salud.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  salud.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    } 
		
		
		if (salud.qs501!=null) {
    		salud.qs501=salud.setConvertqs401(salud.qs501);    
    	}
		if (salud.qs503!=null) {
    		salud.qs503=salud.setConvertqs401(salud.qs503);    
    	}
		if (salud.qs505a!=null) {
    		salud.qs505a=salud.setConvertqs401(salud.qs505a);    
    	}
		if (salud.qs505b!=null) {
    		salud.qs505b=salud.setConvertqs401(salud.qs505b);    
    	}
		if (salud.qs505c!=null) {
    		salud.qs505c=salud.setConvertqs401(salud.qs505c);    
    	}
		if (salud.qs505d!=null) {
    		salud.qs505d=salud.setConvertqs401(salud.qs505d);    
    	}
		
		if (salud.qs505a1!=null) {
    		salud.qs505a1=salud.setConvertqs505a1(salud.qs505a1);    
    	}
		if (salud.qs505b1!=null) {
    		salud.qs505b1=salud.setConvertqs505b1(salud.qs505b1);    
    	}
		
		if (salud.qs506!=null) {
    		salud.qs506=salud.setConvertqs401(salud.qs506);    
    	}
		
		if (salud.qs506a!=null) {
    		salud.qs506a=salud.setConvertqs401(salud.qs506a);    
    	}
		if (salud.qs506b!=null) {
    		salud.qs506b=salud.setConvertqs401(salud.qs506b);    
    	}
	
		
		entityToUI(salud); 
		
		if (salud.qs501_a!=null) {	
	  		  if (!Util.esDiferente(salud.qs501,1)) {
	  	  		  txtQS501_A1.setText(salud.qs501_a.toString());
	  		  }
	  		  if (!Util.esDiferente(salud.qs501,2)) {
	  			  txtQS501_A2.setText(salud.qs501_a.toString());
	  		  }
	  	}
		
		inicio(); 
		App.getInstance().setPersonabySalud(seccion1);
    } 
    private void inicio() { 
    	entra_seccion4();
    	if(persona_mayor_15){
    		onqrgQS500ChangeValue();
    		onqrgQS501ChangeValue();
    		onqrgQS503ChangeValue();
    		onqrgQS505AChangeValue();
    		onqrgQS505BChangeValue();
    		onqrgQS505CChangeValue();
    		onqrgQS505DChangeValue();
    		onqrgQS505A1ChangeValue();
    		onqrgQS505B1ChangeValue();
    		onqrgQS506ChangeValue();
    		onqrgQS506AChangeValue();
    		onqrgQS506BChangeValue();
    		ValidarsiesSupervisora();
    		if (salud.qs501==null) {
	    		Util.cleanAndLockView(getActivity(),txtQS501_A1,txtQS501_A2);
	    	}
//    		rgQS500.requestFocus();
    	}else{
    		grabar();
//    		parent.nextFragment(34);
    	}
    	txtCabecera.requestFocus();
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onqrgQS500ChangeValue(){
    	if (MyUtil.incluyeRango(2,2,rgQS500.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
			Util.cleanAndLockView(this.getActivity(), rgQS501,txtQS501_A1,txtQS501_A2);
			q3.setVisibility(View.GONE);
			if((hombre_mayor_15 || mujer_mayor_50) && !mujer_15_49){
				Util.lockView(getActivity(), false,  rgQS503);
				q4.setVisibility(View.VISIBLE);
				rgQS503.requestFocus();
			}
			if(mujer_15_49){
		
				if (MyUtil.incluyeRango(1,1,seccion1.qs28.toString())) {
					Util.lockView(getActivity(), false,rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS505A1,txtQS505A1_O,rgQS505B1,txtQS505B1_O);
					q5.setVisibility(View.VISIBLE);
					q6.setVisibility(View.VISIBLE);
					q7.setVisibility(View.VISIBLE);
					Util.cleanAndLockView(this.getActivity(),rgQS503,rgQS506);
					q4.setVisibility(View.GONE);
					rgQS505A.requestFocus();
				}
			}
		} 
		else {				
			Util.lockView(getActivity(), false,  rgQS501);
			q3.setVisibility(View.VISIBLE);
			rgQS501.requestFocus();
		}
    }
    
    public void onqrgQS501ChangeValue(){
    	if (!MyUtil.incluyeRango(3,3,rgQS501.getTagSelected("").toString())) {	
    		if (MyUtil.incluyeRango(1,1,rgQS500.getTagSelected("").toString())) {								
			    if(MyUtil.incluyeRango(1,1,rgQS501.getTagSelected("").toString())){
			    	Util.cleanAndLockView(getActivity(),txtQS501_A2);
					salud.qs501_a=null;
					Util.lockView(getActivity(), false, txtQS501_A1);
//					rango(getActivity(), txtQS501_A1, 0, 29, null, null);
					
					txtQS501_A1.setVisibility(View.VISIBLE);
					txtQS501_A1.requestFocus();
			    }
			    if(MyUtil.incluyeRango(2,2,rgQS501.getTagSelected("").toString())){
			    	Util.cleanAndLockView(getActivity(),txtQS501_A1);
					salud.qs501_a=null;
					Util.lockView(getActivity(), false, txtQS501_A2);
//					rango(getActivity(), txtQS501_A2, 0, 90, null, null);
					txtQS501_A2.setVisibility(View.VISIBLE);
					txtQS501_A2.requestFocus();
			    }
    		}
		} 
		else {
			Util.cleanAndLockView(this.getActivity(),txtQS501_A1,txtQS501_A2);
			if((hombre_mayor_15 || mujer_mayor_50) && !mujer_15_49){
				rgQS503.requestFocus();
			}
			if(mujer_15_49){
				if(MyUtil.incluyeRango(2, 2, seccion1.qh06)){
					rgQS505A.requestFocus();
				}
			}
		}
    }
    
    public void onqrgQS503ChangeValue(){
    	if(hombre_mayor_15 || mujer_mayor_50){
	    	if (MyUtil.incluyeRango(2,3,rgQS503.getTagSelected("").toString())) {
	    		MyUtil.LiberarMemoria();
	    		Util.cleanAndLockView(this.getActivity(), rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS506,rgQS505A1,rgQS505B1,rgQS506A,rgQS506B);//
	    		q5.setVisibility(View.GONE);
	    		q6.setVisibility(View.GONE);
	    		q7.setVisibility(View.GONE);
	    		q8.setVisibility(View.GONE);
	    		q9.setVisibility(View.GONE);
	    		q10.setVisibility(View.GONE);
	    	}else{
	    		Util.lockView(getActivity(), false,rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS506,rgQS505A1,rgQS505B1,rgQS506A,rgQS506B);//
	    		q5.setVisibility(View.VISIBLE);
	    		q6.setVisibility(View.VISIBLE);
	    		q7.setVisibility(View.VISIBLE);
	    		q8.setVisibility(View.VISIBLE);
	    		q9.setVisibility(View.VISIBLE);
	    		q10.setVisibility(View.VISIBLE);
	    		rgQS505A.requestFocus();
	    	}
    	}else{
    		if(!mujer_15_49){
    			MyUtil.LiberarMemoria();
    			Util.cleanAndLockView(this.getActivity(), rgQS503,rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS506,rgQS505A1,rgQS505B1,rgQS506A,rgQS506B);//
    			q4.setVisibility(View.GONE);
    			q5.setVisibility(View.GONE);
    			q6.setVisibility(View.GONE);
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.GONE);
    			q9.setVisibility(View.GONE);
    			q10.setVisibility(View.GONE);
    		}
    	}
    }
    
    public void onqrgQS505AChangeValue(){
    	if(persona_mayor_15){
    		rgQS505B.requestFocus();
    	}
    }
    
    public void onqrgQS505BChangeValue(){
    	if(persona_mayor_15){
    		rgQS505C.requestFocus();
    	}
    }
    
    public void onqrgQS505CChangeValue(){
    	if(persona_mayor_15){
    		rgQS505D.requestFocus();
    	}
    }    
   
    public void onqrgQS505DChangeValue(){
    	if(persona_mayor_15){
    		if(hombre_mayor_15 || mujer_mayor_50){
    			rgQS505A1.requestFocus();    			
    		}
    	}
    }
    
    public void onqrgQS505A1ChangeValue(){
    	if(persona_mayor_15){
    		rgQS505B1.requestFocus();
    	}
    }
    
    public void onqrgQS505B1ChangeValue(){
    	if(persona_mayor_15){    		
    		rgQS506.requestFocus();
    	}
    }    
    public void onqrgQS506ChangeValue(){
    	if(persona_mayor_15){    		
    		rgQS506A.requestFocus();
    	}
    }
    public void onqrgQS506AChangeValue(){
    	if(persona_mayor_15){    		
    		rgQS506B.requestFocus();
    	}
    }
    public void onqrgQS506BChangeValue(){}
    
    public void entra_seccion4(){
		if(seccion1.qs28==null)
			seccion1.qs28=2;

  		if(!MyUtil.incluyeRango(15, 99, seccion1.qs23)){
  			MyUtil.LiberarMemoria();
  			Util.cleanAndLockView(this.getActivity(),rgQS500,rgQS501,txtQS501_A1,txtQS501_A2,rgQS503,rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS506,txtQSOBS_SECCION5,rgQS505A1,txtQS505A1_O,rgQS505B1,txtQS505B1_O,rgQS506A,rgQS506B);//
  			q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
    		q10.setVisibility(View.GONE);
    		q11.setVisibility(View.GONE);
  			persona_mayor_15 = false;
  			hombre_mayor_15 = false;
  			mujer_15_49 = false;
  			mujer_mayor_50 = false;
  		}else{
  			q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
  			persona_mayor_15 = true;
  			hombre_mayor_15 = false;
  			mujer_15_49 = false;
  			mujer_mayor_50 = false;
  			Util.lockView(getActivity(), false, rgQS500);
  			if(MyUtil.incluyeRango(15, 99, seccion1.qs23) && MyUtil.incluyeRango(1, 1, seccion1.qh06)){
  				hombre_mayor_15 = true;
  				Util.lockView(getActivity(), false, rgQS503);
  			}
//  			else{
//  				Util.cleanAndLockView(this.getActivity(),rgQS503);
//  			}
  			if(MyUtil.incluyeRango(50, 99, seccion1.qs23) && MyUtil.incluyeRango(2, 2, seccion1.qh06)){
  				mujer_mayor_50 = true;
  				Util.lockView(getActivity(), false, rgQS503);
  			}
  			if(MyUtil.incluyeRango(15, 49, seccion1.qs23) && MyUtil.incluyeRango(2, 2, seccion1.qh06)){
  				MyUtil.LiberarMemoria();
  				mujer_15_49 = true;  				
  				Util.cleanAndLockView(this.getActivity(),rgQS503,rgQS506,rgQS506A,rgQS506B);//  
  				q4.setVisibility(View.GONE);
  				q8.setVisibility(View.GONE);
  				q9.setVisibility(View.GONE);
  				q10.setVisibility(View.GONE);
  				if(MyUtil.incluyeRango(2,2,seccion1.qs28.toString())){
  					Util.cleanAndLockView(this.getActivity(),rgQS505A,rgQS505B,rgQS505C,rgQS505D,rgQS505A1,txtQS505A1_O,rgQS505B1,txtQS505B1_O);
  					q5.setVisibility(View.GONE);
  					q6.setVisibility(View.GONE);
  					q7.setVisibility(View.GONE);
  				}
  			}
//  			else{
//  				if(!hombre_mayor_15 && !mujer_mayor_50){
//  					Util.cleanAndLockView(this.getActivity(),rgQS503);
//  				}
//  			}
  			seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS500","QS501","QS501_A","QS503","QS505A","QS505B","QS505C","QS505D","QS506","QSOBS_SECCION5","QS505A1","QS505A1_O","QS505B1","QS505B1_O","QS506A","QS506B")};//
  		}
     }
    
    public void textoNegrita(){
    	lbl505A.text(R.string.cap04_07qs505a);
  	    Spanned texto505 = Html.fromHtml("<b>A. </b>"+lbl505A.getText());    	
  	    lbl505A.setText(texto505);
  	    
  	    lbl505B.text(R.string.cap04_07qs505b);
  	    texto505 = Html.fromHtml("<b>B. </b>"+lbl505B.getText());    	
	    lbl505B.setText(texto505);
  	    
	    lbl505C.text(R.string.cap04_07qs505c);
  	    texto505 = Html.fromHtml("<b>C. </b>"+lbl505C.getText());    	
	    lbl505C.setText(texto505);
  	   
	    lbl505D.text(R.string.cap04_07qs505d);
  	    texto505 = Html.fromHtml("<b>D. </b>"+lbl505D.getText());    	
	    lbl505D.setText(texto505);
    }
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS500.readOnly();
			rgQS501.readOnly();
			txtQS501_A1.readOnly();
			txtQS501_A2.readOnly();	
			rgQS503.readOnly();
			rgQS505A.readOnly();
			rgQS505B.readOnly();
			rgQS505C.readOnly();
			rgQS505D.readOnly();
			rgQS505A1.readOnly();
			txtQS505A1_O.readOnly();
			rgQS505B1.readOnly();
			txtQS505B1_O.readOnly();
			rgQS506.readOnly();
			rgQS506A.readOnly();
			rgQS506B.readOnly();
			txtQSOBS_SECCION5.setEnabled(false);
		}
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(salud);		
		
		if (salud.qs501!=null) {
			salud.qs501=salud.getConvertqs401(salud.qs501);
		}
		
		if (salud.qs501!=null) {
			if (!Util.esDiferente(salud.qs501,1) && txtQS501_A1.getText().toString().trim().length()!=0 ) {
				salud.qs501_a=Integer.parseInt(txtQS501_A1.getText().toString().trim());
			}
			if (!Util.esDiferente(salud.qs501,2) && txtQS501_A2.getText().toString().trim().length()!=0) {
				salud.qs501_a=Integer.parseInt(txtQS501_A2.getText().toString().trim());
			}
		}		
		
		if (salud.qs503!=null) {
			salud.qs503=salud.getConvertqs401(salud.qs503);
		}
		if (salud.qs505a!=null) {
			salud.qs505a=salud.getConvertqs401(salud.qs505a);
		}
		if (salud.qs505b!=null) {
			salud.qs505b=salud.getConvertqs401(salud.qs505b);
		}
		if (salud.qs505c!=null) {
			salud.qs505c=salud.getConvertqs401(salud.qs505c);
		}
		if (salud.qs505d!=null) {
			salud.qs505d=salud.getConvertqs401(salud.qs505d);
		}
		if (salud.qs505a1!=null) {
			salud.qs505a1=salud.getConvertqs505a1(salud.qs505a1);
		}
		if (salud.qs505b1!=null) {
			salud.qs505b1=salud.getConvertqs401(salud.qs505b1);
		}		
		if (salud.qs506!=null) {
			salud.qs506=salud.getConvertqs401(salud.qs506);
		}
		if (salud.qs506a!=null) {
			salud.qs506a=salud.getConvertqs401(salud.qs506a);
		}
		if (salud.qs506b!=null) {
			salud.qs506b=salud.getConvertqs401(salud.qs506b);
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO;
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		}
		
		return App.SALUD;
	}
} 
