package gob.inei.endes2024.fragment.C1seccion04_07; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CS_06Fragment_006 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQS601A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQS601B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQS603; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQS604; 

	CAP04_07 cap04_07; 
	Salud seccion1; 
	public TextField txtCabecera;
	public Seccion01Service seccion01;
	public List<Seccion01> ListadoMef;
	public Seccion01 informantedesalud,mefdeviolenciafamiliar;
	public boolean rango_15_49=true;
	public boolean preg601=false;
	public boolean mef=false;
	public int qh06;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblDescripcion,lblPreg603,lblPreg601,lblPreg601A,lblPreg601B,lblPreg604; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoS01; 
	public GridComponent2 gridPreguntas600;

	String fechareferencia;
	public CS_06Fragment_006() {} 
	public CS_06Fragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS700FECH_REF","QS604","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604")};
		seccionesCargadoS01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS23","QS29A","QS29B","QS209")};
        return rootView; 
	} 
  public void RenombrarEtiquetas()
  {
  	Calendar fechaactual = new GregorianCalendar();
  	if(fechareferencia!=null){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		try {
			date2 = df.parse(fechareferencia);
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		fechaactual=cal;
	}    	
  	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
  	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
  	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
  	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
  	
  	lblPreg603.setText(lblPreg603.getText().toString().replace("F1", F2)); 
  	lblPreg603.setText(lblPreg603.getText().toString().replace("F2", F1));
  	
  	String F3="";
	if(F1.equals("Diciembre")){
		F3 = "del a�o pasado";
	}else{
		F3 = "de este a�o";
	}
	lblPreg603.setText(lblPreg603.getText().toString().replace("F3", F3));

  }
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07titulo_s6).textSize(21).centrar().negrita(); 
	    lblDescripcion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07tituloDesc_s6).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
	    lblPreg601 = new LabelComponent(getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs601);
		
	    Spanned texto = Html.fromHtml("<b>A</b>. �La infecci�n por VIH?");
	    lblPreg601A = new LabelComponent(this.getActivity()).size(50,400).textSize(18); lblPreg601A.setText(texto);
	    rgQS601A=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs601a_1, R.string.cap04_07qs601a_2).size(50,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP601AChangeValue"); 
	   
	    Spanned texto2 = Html.fromHtml("<b>B</b>. �Una enfermedad llamada SIDA?");
	    lblPreg601B = new LabelComponent(this.getActivity()).size(50, 400).textSize(18); lblPreg601B.setText(texto2);
	    rgQS601B=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs601b_1, R.string.cap04_07qs601b_2).size(50,250).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP601BChangeValue"); 
		
		lblPreg603 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs603);
		rgQS603=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs603_1,R.string.cap04_07qs603_2,R.string.cap04_07qs603_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP603ChangeValue"); 
		lblPreg604 = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cap04_07qs604);
		rgQS604=new RadioGroupOtherField(this.getActivity(),R.string.cap04_07qs604_1,R.string.cap04_07qs604_2,R.string.cap04_07qs604_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
  
		gridPreguntas600=new GridComponent2(this.getActivity(),App.ESTILO,2);
		gridPreguntas600.addComponent(lblPreg601A);		
		gridPreguntas600.addComponent(rgQS601A);
		gridPreguntas600.addComponent(lblPreg601B);
		gridPreguntas600.addComponent(rgQS601B);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo,lblDescripcion); 
		q1 = createQuestionSection(lblPreg601,gridPreguntas600.component()); 
		q3 = createQuestionSection(lblPreg603,rgQS603); 
		q4 = createQuestionSection(lblPreg604,rgQS604); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q3); 
		form.addView(q4); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(cap04_07); 
		if (cap04_07.qs603 != null) {
			cap04_07.qs603 = cap04_07.getConvert603(cap04_07.qs603);
		}
		if (cap04_07.qs604 != null) {
			cap04_07.qs604 = cap04_07.getConvert603(cap04_07.qs604);
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(cap04_07.qs603 == null) {
				cap04_07.qs700fech_ref = null;
			} 
			else {
				if(fechareferencia == null) {
					cap04_07.qs700fech_ref =  Util.getFechaActualToString();
				}
				else {
					cap04_07.qs700fech_ref = fechareferencia;
				}			
			}		
			if(!getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getSaludNavegacionS4_7().qs601a=cap04_07.qs601a;
		App.getInstance().getSaludNavegacionS4_7().qs603=cap04_07.qs603;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(rango_15_49){
			if(!mef){
				if (Util.esVacio(cap04_07.qs601a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.601A"); 
					view = rgQS601A; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(cap04_07.qs601b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.601B"); 
					view = rgQS601B; 
					error = true; 
					return false; 
				} 
				
				if(MyUtil.incluyeRango(1,1, rgQS601A.getTagSelected("").toString())){
					if(MyUtil.incluyeRango(1,2, rgQS601B.getTagSelected("").toString())){
						if (Util.esVacio(cap04_07.qs603)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta P.603"); 
							view = rgQS603; 
							error = true; 
							return false; 
						} 
						
						if (MyUtil.incluyeRango(1,1, rgQS603.getTagSelected("").toString())) {
							if (Util.esVacio(cap04_07.qs604)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta P.604"); 
								view = rgQS604; 
								error = true; 
								return false; 
							} 
						}
					}
				}else{
					 if(MyUtil.incluyeRango(1,1, rgQS601B.getTagSelected("").toString())){
						 if (Util.esVacio(cap04_07.qs603)) { 
								mensaje = preguntaVacia.replace("$", "La pregunta P.603"); 
								view = rgQS603; 
								error = true; 
								return false; 
							} 
							
							if (MyUtil.incluyeRango(1,1, rgQS603.getTagSelected("").toString())) {
								if (Util.esVacio(cap04_07.qs604)) { 
									mensaje = preguntaVacia.replace("$", "La pregunta P.604"); 
									view = rgQS604; 
									error = true; 
									return false; 
								} 
							}
					}
				}
				
			}else{
				if (Util.esVacio(cap04_07.qs603)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.603"); 
					view = rgQS603; 
					error = true; 
					return false; 
				} 
				if (MyUtil.incluyeRango(1,1, rgQS603.getTagSelected("").toString())) {
					if (Util.esVacio(cap04_07.qs604)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.604"); 
						view = rgQS604; 
						error = true; 
						return false; 
					} 
				}
			}
			
		}
		
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
   
    	mef=false;
    	cap04_07 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
    	
    	seccion1 = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS01);
   

    	qh06=App.getInstance().getPersonaSeccion01().qh06;
    
    	
    	if(qh06==2 && MyUtil.incluyeRango(15, 49, seccion1.qs23)){
    		mef=true;
    	}
    	if(cap04_07==null){ 
		  cap04_07=new CAP04_07(); 
		  cap04_07.id=App.getInstance().getPersonaSeccion01().id; 
		  cap04_07.hogar_id=App.getInstance().getPersonaSeccion01().hogar_id; 
		  cap04_07.persona_id=App.getInstance().getPersonaSeccion01().persona_id; 
	    }
    	
    	if (!Util.esVacio(cap04_07.qs603)) {
			cap04_07.qs603 = cap04_07.setConvert603(cap04_07.qs603);
		}
    	if (cap04_07.qs604 != null) {
			cap04_07.qs604 = cap04_07.setConvert603(cap04_07.qs604);
		}
    	
//    	Log.e("qs28","" + seccion1.qs28);
//    	Log.e("qs29a","" + seccion1.qs29a);
//    	Log.e("qs28b","" + seccion1.qs29b);
//    	
		entityToUI(cap04_07);
		fechareferencia = cap04_07.qs700fech_ref;
		inicio(); 
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    	entra_seccion6();
    	if(rango_15_49){
    		onP601AChangeValue();
    		onP601BChangeValue();
    		RenombrarEtiquetas();
        	rgQS601A.requestFocus();
    	}
    	App.getInstance().setPersona_04_07(null);
    	App.getInstance().setPersona_04_07(cap04_07);
    	App.getInstance().setPersonabySalud(null);
    	App.getInstance().setPersonabySalud(seccion1);
    	txtCabecera.requestFocus();
    	
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQS601A.readOnly(); 
			rgQS601B.readOnly();
			rgQS603.readOnly();
			rgQS604.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 

    public void onP601AChangeValue() {
    	if (MyUtil.incluyeRango(2,2, rgQS601B.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2, rgQS601A.getTagSelected("").toString())) {
    		Log.e("1","1");
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(), rgQS603,rgQS604);
			q3.setVisibility(view.GONE);
			q4.setVisibility(view.GONE);
			preg601=true;
	    }		
    	else {	
    		Log.e("1","2");
    		q3.setVisibility(view.VISIBLE);
    		q4.setVisibility(view.VISIBLE);
    		Util.lockView(getActivity(), false,rgQS603,rgQS604);
    		rgQS603.requestFocus();	
			onP603ChangeValue();
	   }
    	rgQS601B.requestFocus();	
    	
    }
    public void onP601BChangeValue() {
   
		if (MyUtil.incluyeRango(2,2, rgQS601B.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2, rgQS601A.getTagSelected("").toString())) {
			Log.e("1","3"); 
			MyUtil.LiberarMemoria();
			    Util.cleanAndLockView(getActivity(), rgQS603,rgQS604);	
				q3.setVisibility(view.GONE);
				q4.setVisibility(view.GONE);
				onP603ChangeValue();
				preg601=true;
		}		
		else {	
			Log.e("1","4");
			    q3.setVisibility(view.VISIBLE);
			    q4.setVisibility(view.VISIBLE);
				Util.lockView(getActivity(), false,rgQS603,rgQS604);
				rgQS603.requestFocus();	
				onP603ChangeValue();
		}
		rgQS603.requestFocus();	
		
	}
    
    public void onP603ChangeValue() {
    	Log.e("1","5");
		if (MyUtil.incluyeRango(2,3, rgQS603.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQS604);	
			q4.setVisibility(view.GONE);
		}		
		if (MyUtil.incluyeRango(1,1, rgQS603.getTagSelected("").toString())) {
			Log.e("1","6");
		    q4.setVisibility(view.VISIBLE);
			Util.lockView(getActivity(), false,rgQS604);
			rgQS604.requestFocus();	
		}	
		
	}
    public void mef(){
    	 
    }
    public void entra_seccion6(){
    	
    	if(!MyUtil.incluyeRango(15, 49, seccion1.qs23)){
    		Log.e("1","7");
    		Util.cleanAndLockView(this.getActivity(),rgQS601A,rgQS601B,rgQS603,rgQS604);
    		rango_15_49 = false;
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(view.GONE);
    		q3.setVisibility(view.GONE);
    		q4.setVisibility(view.GONE);
    	}else{
    		q0.setVisibility(View.VISIBLE);
    		if(mef){
    			Util.cleanAndLockView(this.getActivity(),rgQS601A,rgQS601B);
    			q1.setVisibility(view.GONE);
    			if(seccion1.qs29a!=null && seccion1.qs29b!=null){
        	    	if(!Util.esDiferente(seccion1.qs29a, 1) || !Util.esDiferente(seccion1.qs29b, 1)){
        	    		q3.setVisibility(view.VISIBLE);
                		q4.setVisibility(view.VISIBLE);
                		rango_15_49 = true;
                		Util.lockView(getActivity(), false, rgQS603);
                		rgQS603.requestFocus();
        	    	}
        	    	if(!Util.esDiferente(seccion1.qs29a, 2) && !Util.esDiferente(seccion1.qs29b, 2)){
        	    		Log.e("1","8");
        	    		Util.cleanAndLockView(this.getActivity(),rgQS603,rgQS604);
        	    		rango_15_49 = false;
        	    		q3.setVisibility(view.GONE);
        	    		q4.setVisibility(view.GONE);
        	    	}
    			}
    			
    			
    		}else{
    			q1.setVisibility(view.VISIBLE);
        		q3.setVisibility(view.VISIBLE);
        		q4.setVisibility(view.VISIBLE);
        		rango_15_49 = true;
        		Util.lockView(getActivity(), false, rgQS601A);
    		}
    			
    	}
    }
   

    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(cap04_07);
		try {
			if (cap04_07.qs603 != null) {
				cap04_07.qs603 = cap04_07.getConvert603(cap04_07.qs603);
			}
			if (cap04_07.qs604 != null) {
				cap04_07.qs604 = cap04_07.getConvert603(cap04_07.qs604);
			}
			
			if(cap04_07.qs603 == null) {
				cap04_07.qs700fech_ref = null;
			} 
			else {
				if(fechareferencia == null) {
					cap04_07.qs700fech_ref =  Util.getFechaActualToString();
				}
				else {
					cap04_07.qs700fech_ref = fechareferencia;
				}			
			}
			
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.SALUD;
	}
    
} 
