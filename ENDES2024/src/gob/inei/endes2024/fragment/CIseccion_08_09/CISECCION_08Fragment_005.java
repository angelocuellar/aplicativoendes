package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI815; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI815A_A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI815A_B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI815A_C; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI815A_D; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI815A_E; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI815A_W; 
	@FieldAnnotation(orderIndex=8) 
	public TextField txtQI815A_WX; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI815A_X; 
	@FieldAnnotation(orderIndex=10) 
	public TextField txtQI815A_XI; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI815A_Z; 
	
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI816_A; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI816_B; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI816_C; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI816_D; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI816_E; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI816_F; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI816_G; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI816_H; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI816_I; 
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI816_J; 
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI816_K; 
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI816_L; 
	@FieldAnnotation(orderIndex=24) 
	public CheckBoxField chbQI816_M; 
	@FieldAnnotation(orderIndex=25) 
	public CheckBoxField chbQI816_W; 
	@FieldAnnotation(orderIndex=26) 
	public TextField txtQI816_WX; 	
	@FieldAnnotation(orderIndex=27) 
	public CheckBoxField chbQI816_Z; 
		
	CISECCION_08 individual; 
	CISECCION_05_07 individuals05;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta815,lblpregunta815A,lblpregunta815A_ind,lblpregunta816,lblpregunta816_ind;
	public TextField txtCabecera;
	private GridComponent2 gridPreguntas815A,gridPreguntas816;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5; 

	public CISECCION_08Fragment_005() {} 
	public CISECCION_08Fragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI815","QI815A_A","QI815A_B","QI815A_C","QI815A_D","QI815A_E","QI815A_W","QI815A_WX","QI815A_X","QI815A_XI","QI815A_Z","QI816_A","QI816_B","QI816_C","QI816_D","QI816_E","QI816_F","QI816_G","QI816_H","QI816_I","QI816_J","QI816_K","QI816_L","QI816_M","QI816_W","QI816_WX","QI816_Z","QI801A","QI801B","QI817B","QI817C","QI815","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI815","QI815A_A","QI815A_B","QI815A_C","QI815A_D","QI815A_E","QI815A_W","QI815A_WX","QI815A_X","QI815A_XI","QI815A_Z","QI816_A","QI816_B","QI816_C","QI816_D","QI816_E","QI816_F","QI816_G","QI816_H","QI816_I","QI816_J","QI816_K","QI816_L","QI816_M","QI816_W","QI816_WX","QI816_Z")}; 
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512","ID","HOGAR_ID","PERSONA_ID")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI815=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi815_1,R.string.c2seccion_08_09qi815_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS815ChangeValue"); 
		
		chbQI815A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_a, "1:0").size(WRAP_CONTENT, 200);		
		chbQI815A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_b, "1:0").size(WRAP_CONTENT, 200);
		chbQI815A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_c, "1:0").size(WRAP_CONTENT, 200);
		chbQI815A_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_d, "1:0").size(WRAP_CONTENT, 200);
		chbQI815A_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_e, "1:0").size(WRAP_CONTENT, 200);
		chbQI815A_W=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_w, "1:0").size(WRAP_CONTENT, 200).callback("onqi815AWChangeValue");
		chbQI815A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_x, "1:0").size(WRAP_CONTENT, 200).callback("onqi815AXChangeValue");
		chbQI815A_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi815a_z, "1:0").size(WRAP_CONTENT, 200);
		txtQI815A_WX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
		txtQI815A_XI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
		
		chbQI816_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_a, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_b, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_c, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_d, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_e, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_f, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_g, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_h, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_i, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_j, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_k, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_l, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_m, "1:0").size(WRAP_CONTENT, 200);
		chbQI816_W=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_w, "1:0").size(WRAP_CONTENT, 200).callback("onqi816WChangeValue");
		chbQI816_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816_z, "1:0").size(WRAP_CONTENT, 200);
		txtQI816_WX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
		
		lblpregunta815 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi815);
		lblpregunta815A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi815a);
		lblpregunta815A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi815a_ind);
//		lblpregunta816 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi816);
		Spanned texto816 = Html.fromHtml("816 �Qu� s�ntomas y signos le pueden hacer pensar a Ud. <u><b>que un hombre</b></u> tiene una enfermedad de transmisi�n sexual? <br>�Alg�n otro s�ntoma?");
		lblpregunta816 = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
		lblpregunta816.setText(texto816);
		lblpregunta816_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi816_ind);
				
		gridPreguntas815A = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas815A.addComponent(chbQI815A_A,2);
		gridPreguntas815A.addComponent(chbQI815A_B,2);
		gridPreguntas815A.addComponent(chbQI815A_C,2);
		gridPreguntas815A.addComponent(chbQI815A_D,2);
		gridPreguntas815A.addComponent(chbQI815A_E,2);
		gridPreguntas815A.addComponent(chbQI815A_W);
		gridPreguntas815A.addComponent(txtQI815A_WX);
		gridPreguntas815A.addComponent(chbQI815A_X);
		gridPreguntas815A.addComponent(txtQI815A_XI);
		gridPreguntas815A.addComponent(chbQI815A_Z,2);
		
		gridPreguntas816 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas816.addComponent(chbQI816_A,2);
		gridPreguntas816.addComponent(chbQI816_B,2);
		gridPreguntas816.addComponent(chbQI816_C,2);
		gridPreguntas816.addComponent(chbQI816_D,2);
		gridPreguntas816.addComponent(chbQI816_E,2);
		gridPreguntas816.addComponent(chbQI816_F,2);
		gridPreguntas816.addComponent(chbQI816_G,2);
		gridPreguntas816.addComponent(chbQI816_H,2);
		gridPreguntas816.addComponent(chbQI816_I,2);
		gridPreguntas816.addComponent(chbQI816_J,2);
		gridPreguntas816.addComponent(chbQI816_K,2);
		gridPreguntas816.addComponent(chbQI816_L,2);
		gridPreguntas816.addComponent(chbQI816_M,2);
		gridPreguntas816.addComponent(chbQI816_W);
		gridPreguntas816.addComponent(txtQI816_WX);
		gridPreguntas816.addComponent(chbQI816_Z,2);

    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta815,rgQI815); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta815A,lblpregunta815A_ind,gridPreguntas815A.component()); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta816,lblpregunta816_ind,gridPreguntas816.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi815 = individual.qi815;
			App.getInstance().getCiseccion_08().qi817a = individual.qi817a;
			App.getInstance().getCiseccion_08().qi817b = individual.qi817b;
		}
		else 
			App.getInstance().setCiseccion_08(individual);
				
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		App.getInstance().getCiseccion_08().qi815=individual.qi815;
		
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(individual.qi815)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI815"); 
			view = rgQI815; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(individual.qi815,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi815a_a,individual.qi815a_b,individual.qi815a_c,individual.qi815a_d,individual.qi815a_e,individual.qi815a_w,individual.qi815a_x,individual.qi815a_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI815A_A; 
				error = true; 
				return false; 
			}
			if (chbQI815A_W.isChecked()) {
				if (Util.esVacio(individual.qi815a_wx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI815A_WX; 
					error = true; 
					return false; 
				}
			}
			if (chbQI815A_X.isChecked()) {
				if (Util.esVacio(individual.qi815a_xi)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI815A_XI; 
					error = true; 
					return false; 
				}
			}
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi816_a,individual.qi816_b,individual.qi816_c,individual.qi816_d,individual.qi816_e,individual.qi816_f,individual.qi816_g,individual.qi816_h,individual.qi816_i,individual.qi816_j,individual.qi816_k,individual.qi816_l,individual.qi816_m,individual.qi816_w,individual.qi816_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI816_A; 
				error = true; 
				return false; 
			}
			if (chbQI816_W.isChecked()) {
				if (Util.esVacio(individual.qi816_wx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI816_WX; 
					error = true; 
					return false; 
				}
			}
		}

		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individuals05 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
     	App.getInstance().getCiseccion_08().qi815=individual.qi815;
    	App.getInstance().getCiseccion_08().qi817a=individual.qi817a;
    	App.getInstance().getCiseccion_08().qi817b=individual.qi817b;
    	App.getInstance().getCiseccion05_07().qi512=individuals05.qi512;
    	
    	if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi801a = individual.qi801a;
			App.getInstance().getCiseccion_08().qi801b = individual.qi801b;
		}
		else{ 
			App.getInstance().setCiseccion_08(individual);
		}
    	
    	
    	entityToUI(individual); 
    	inicio();
    } 
    
    private void inicio() { 
    	onqrgQS815ChangeValue();
    	onqi815AWChangeValue();
    	onqi815AXChangeValue();
    	onqi816WChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void onqrgQS815ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI815.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),chbQI815A_A,chbQI815A_B,chbQI815A_C,chbQI815A_D,chbQI815A_E,chbQI815A_W,chbQI815A_X,chbQI815A_Z,txtQI815A_WX,txtQI815A_XI,chbQI816_A,chbQI816_B,chbQI816_C,chbQI816_D,chbQI816_E,chbQI816_F,chbQI816_G,chbQI816_H,chbQI816_I,chbQI816_J,chbQI816_K,chbQI816_L,chbQI816_M,chbQI816_W,chbQI816_Z,txtQI816_WX);
    		MyUtil.LiberarMemoria();
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,chbQI815A_A,chbQI815A_B,chbQI815A_C,chbQI815A_D,chbQI815A_E,chbQI815A_W,chbQI815A_X,chbQI815A_Z,txtQI815A_WX,txtQI815A_XI,chbQI816_A,chbQI816_B,chbQI816_C,chbQI816_D,chbQI816_E,chbQI816_F,chbQI816_G,chbQI816_H,chbQI816_I,chbQI816_J,chbQI816_K,chbQI816_L,chbQI816_M,chbQI816_W,chbQI816_Z,txtQI816_WX); 
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		chbQI815A_A.requestFocus(); 
    	}
    }
    public void onqi816WChangeValue(){
    	if(!chbQI816_W.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI816_WX);
    	}else{
    		Util.lockView(getActivity(), false,txtQI816_WX);
    	}
    }
    public void onqi815AXChangeValue(){
    	if(!chbQI815A_X.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI815A_XI);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI815A_XI);
    	}
    }
    public void onqi815AWChangeValue(){
    	if(!chbQI815A_W.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI815A_WX);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI815A_WX);
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI815.readOnly();
    		txtQI815A_WX.readOnly();
    		txtQI815A_XI.readOnly();
    		txtQI816_WX.readOnly();
    		chbQI815A_A.readOnly();
    		chbQI815A_B.readOnly();
    		chbQI815A_C.readOnly();
    		chbQI815A_D.readOnly();
    		chbQI815A_E.readOnly();
    		chbQI815A_W.readOnly();
    		chbQI815A_X.readOnly();
    		chbQI815A_Z.readOnly();
    		chbQI816_A.readOnly();
    		chbQI816_B.readOnly();
    		chbQI816_C.readOnly();
    		chbQI816_D.readOnly();
    		chbQI816_E.readOnly();
    		chbQI816_F.readOnly();
    		chbQI816_G.readOnly();
    		chbQI816_H.readOnly();
    		chbQI816_I.readOnly();
    		chbQI816_J.readOnly();
    		chbQI816_K.readOnly();
    		chbQI816_L.readOnly();
    		chbQI816_M.readOnly();
    		chbQI816_W.readOnly();
    		chbQI816_Z.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		
		if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi815 = individual.qi815;
			App.getInstance().getCiseccion_08().qi817a = individual.qi817a;
			App.getInstance().getCiseccion_08().qi817b = individual.qi817b;
		}
		else 
			App.getInstance().setCiseccion_08(individual);
				
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		App.getInstance().getCiseccion_08().qi815=individual.qi815;
		
		return App.INDIVIDUAL;
	} 
} 
