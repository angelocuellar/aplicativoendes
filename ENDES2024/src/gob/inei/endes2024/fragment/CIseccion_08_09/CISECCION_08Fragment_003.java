package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_003 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI804; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI804A; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI804B; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI804C; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI805; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI806; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI806A_A; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI806A_B; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI806A_C; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI806A_X; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI806A_Z; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI806A_XI; 

	
	CISECCION_08 individual;
//	CISECCION_04A individualS4A; 
	private boolean sehizoexamen=false;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta804,lblpregunta804A,lblpregunta804B,lblpregunta804C,lblpregunta805,lblpregunta806,lblpregunta806A,lblpregunta806A_ind;
	public TextField txtCabecera;
	private GridComponent2 gridPreguntas806A;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6;
	LinearLayout q7; 
	LinearLayout q8; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS4A; 

	public CISECCION_08Fragment_003() {} 
	public CISECCION_08Fragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI804","QI804A","QI804B","QI804C","QI805","QI806","QI806A_A","QI806A_B","QI806A_C","QI806A_X","QI806A_XI","QI806A_Z","QI803","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI804","QI804A","QI804B","QI804C","QI805","QI806","QI806A_A","QI806A_B","QI806A_C","QI806A_X","QI806A_XI","QI806A_Z")};
		seccionesCargadoS4A = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI411_H")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI804=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi804_1,R.string.c2seccion_08_09qi804_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS804ChangeValue"); 
		rgQI804A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi804a_1,R.string.c2seccion_08_09qi804a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI804B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi804b_1,R.string.c2seccion_08_09qi804b_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS804BChangeValue"); 
		rgQI804C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi804c_1,R.string.c2seccion_08_09qi804c_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI805=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi805_1,R.string.c2seccion_08_09qi805_2,R.string.c2seccion_08_09qi805_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
	  	rgQI806=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi806_1,R.string.c2seccion_08_09qi806_2,R.string.c2seccion_08_09qi806_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS806ChangeValue"); 
	  	chbQI806A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi806a_a, "1:0").size(WRAP_CONTENT, 200);
	  	chbQI806A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi806a_b, "1:0").size(WRAP_CONTENT, 200);
	  	chbQI806A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi806a_c, "1:0").size(WRAP_CONTENT, 200);
	  	chbQI806A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi806a_x, "1:0").size(WRAP_CONTENT, 200).callback("onQI806AXChangeValue");
	  	chbQI806A_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi806a_z, "1:0").size(WRAP_CONTENT, 300);
	  	txtQI806A_XI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
		
		lblpregunta804 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi804);
		lblpregunta804A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi804a);
		lblpregunta804B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi804b);
		lblpregunta804C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi804c);
		lblpregunta805 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi805);
		lblpregunta806 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi806);
		lblpregunta806A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi806a);
		lblpregunta806A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_08_09qi806a_ind);
		
		gridPreguntas806A = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas806A.addComponent(chbQI806A_A,2);
		gridPreguntas806A.addComponent(chbQI806A_B,2);
		gridPreguntas806A.addComponent(chbQI806A_C,2);
		gridPreguntas806A.addComponent(chbQI806A_X);
		gridPreguntas806A.addComponent(txtQI806A_XI);
		gridPreguntas806A.addComponent(chbQI806A_Z,2);
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta804,rgQI804); 
		q2 = createQuestionSection(lblpregunta804A,rgQI804A); 
		q3 = createQuestionSection(lblpregunta804B,rgQI804B); 
		q4 = createQuestionSection(lblpregunta804C,rgQI804C); 
		q5 = createQuestionSection(lblpregunta805,rgQI805); 
		q6 = createQuestionSection(lblpregunta806,rgQI806); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta806A,lblpregunta806A_ind,gridPreguntas806A.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6);
		form.addView(q7);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		Integer in411h=0;
//		if(individualS4A!=null)
//			in411h= individualS4A.qi411_h==null?0:individualS4A.qi411_h;
		
		if (!Util.esDiferente(individual.qi803,1)) {
			if (Util.esVacio(individual.qi804)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI804"); 
				view = rgQI804; 
				error = true; 
				return false; 
			} 
			if (sehizoexamen && !Util.esDiferente(individual.qi804,2)) {
				mensaje = "Verficar Pregunta QI411_H"; 
				view = rgQI804; 
				error = true; 
				return false; 
			}
			
			if (!Util.esDiferente(individual.qi804,1)) {
				if (Util.esVacio(individual.qi804a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI804A"); 
					view = rgQI804A; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(individual.qi804b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI804B"); 
					view = rgQI804B; 
					error = true; 
					return false; 
				}
				if (!Util.esDiferente(individual.qi804b,1)) {
					if (Util.esVacio(individual.qi804c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI804C"); 
						view = rgQI804C; 
						error = true; 
						return false; 
					} 
				}
			}
		}
		
		if (Util.esVacio(individual.qi805)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI805"); 
			view = rgQI805; 
			error = true; 
			return false; 
		}	
		if (Util.esVacio(individual.qi806)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI806"); 
			view = rgQI806; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi806,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi806a_a,individual.qi806a_b,individual.qi806a_c,individual.qi806a_x,individual.qi806a_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI806A_A; 
				error = true; 
				return false; 
			}	
			if (chbQI806A_X.isChecked()) {
				if (Util.esVacio(individual.qi806a_xi)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI806A_XI; 
					error = true; 
					return false; 
				}
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
//    	individualS4A = getCuestionarioService().getCISECCION_04A411H(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS4A);
    	sehizoexamen =getCuestionarioService().getvalidarSiesqueLeHicieronExamenDeVIH(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
    	entityToUI(individual); 
    	inicio();
    } 
    
    public void ValidaPregunta803(){
    	if (!Util.esDiferente(individual.qi803,2)) {
    		Util.cleanAndLockView(getActivity(),rgQI804,rgQI804A,rgQI804B,rgQI804C);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);

		}
    	else{
    		Util.lockView(getActivity(),false,rgQI804,rgQI804A,rgQI804B,rgQI804C);
    		q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			onqrgQS804ChangeValue();
    	}
    }
    
    private void inicio() {
    	ValidaPregunta803();
    	onqrgQS806ChangeValue();
    	onQI806AXChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void onqrgQS804ChangeValue() { 
//       Integer in411h = 0;
//       if(individualS4A!=null)
//       {
//    	in411h= individualS4A.qi411_h==null?0:individualS4A.qi411_h;
//       }
    	if (MyUtil.incluyeRango(2,2,rgQI804.getTagSelected("").toString())) {
    		if (sehizoexamen) {
    			ToastMessage.msgBox(this.getActivity(), "No puede seleccionar esta opci�n", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			}
    		
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI804A,rgQI804B,rgQI804C);  		
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		rgQI805.requestFocus();
    	} 
    	else {
    		
    		Util.lockView(getActivity(),false,rgQI804A,rgQI804B,rgQI804C); 
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		onqrgQS804BChangeValue();
    		rgQI804A.requestFocus(); 
    	}
    }
    
    public void onqrgQS804BChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI804B.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI804C);  		
    		q4.setVisibility(View.GONE);
    		rgQI805.requestFocus();
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI804C); 
    		q4.setVisibility(View.VISIBLE);
    		rgQI804C.requestFocus(); 
    	}
    }
    
    public void onqrgQS806ChangeValue() {
    	if (MyUtil.incluyeRango(2,8,rgQI806.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),chbQI806A_A,chbQI806A_B,chbQI806A_C,chbQI806A_X,chbQI806A_Z,txtQI806A_XI);  		
    		q7.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,chbQI806A_A,chbQI806A_B,chbQI806A_C,chbQI806A_X,chbQI806A_Z,txtQI806A_XI); 
    		q7.setVisibility(View.VISIBLE);
    		chbQI806A_A.requestFocus(); 
    	}
    }
    public void onQI806AXChangeValue(){
    	if(chbQI806A_X.isChecked()){
    		Util.lockView(getActivity(), false,txtQI806A_XI);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI806A_XI);
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI804.readOnly();
    		rgQI804A.readOnly();
    		rgQI804B.readOnly();
    		rgQI804C.readOnly();
    		rgQI805.readOnly();
    		rgQI806.readOnly();
    		txtQI806A_XI.readOnly();
    		chbQI806A_A.readOnly();
    		chbQI806A_B.readOnly();
    		chbQI806A_C.readOnly();
    		chbQI806A_X.readOnly();
    		chbQI806A_Z.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
