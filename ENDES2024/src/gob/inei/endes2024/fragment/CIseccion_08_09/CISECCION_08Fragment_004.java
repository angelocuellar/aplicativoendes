package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_004 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI807; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI808; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI809; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI810; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI811; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI812; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI813; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI814; 
	
	CISECCION_08 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta807,lblpregunta808,lblpregunta809,lblpregunta810,lblpregunta811,lblpregunta812,lblpregunta813,lblpregunta814;
	public TextField txtCabecera;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CISECCION_08Fragment_004() {} 
	public CISECCION_08Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI807","QI808","QI809","QI810","QI811","QI812","QI813","QI814","QI801A","QI801B","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI807","QI808","QI809","QI810","QI811","QI812","QI813","QI814")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI807=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi807_1,R.string.c2seccion_08_09qi807_2,R.string.c2seccion_08_09qi807_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI808=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi808_1,R.string.c2seccion_08_09qi808_2,R.string.c2seccion_08_09qi808_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI809=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi809_1,R.string.c2seccion_08_09qi809_2,R.string.c2seccion_08_09qi809_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI810=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi810_1,R.string.c2seccion_08_09qi810_2,R.string.c2seccion_08_09qi810_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	  	rgQI811=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi811_1,R.string.c2seccion_08_09qi811_2,R.string.c2seccion_08_09qi811_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI812=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi812_1,R.string.c2seccion_08_09qi812_2,R.string.c2seccion_08_09qi812_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI813=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi813_1,R.string.c2seccion_08_09qi813_2,R.string.c2seccion_08_09qi813_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI814=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi814_1,R.string.c2seccion_08_09qi814_2,R.string.c2seccion_08_09qi814_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblpregunta807 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi807);
		lblpregunta808 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi808);
		lblpregunta809 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi809);
		lblpregunta810 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi810);
		lblpregunta811 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi811);
		lblpregunta812 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi812);
		lblpregunta813 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi813);
		lblpregunta814 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi814);
  }
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta807,rgQI807);
		q2 = createQuestionSection(lblpregunta808,rgQI808); 
		q3 = createQuestionSection(lblpregunta809,rgQI809); 
		q4 = createQuestionSection(lblpregunta810,rgQI810);
		q5 = createQuestionSection(lblpregunta811,rgQI811); 
		q6 = createQuestionSection(lblpregunta812,rgQI812); 
		q7 = createQuestionSection(lblpregunta813,rgQI813); 
		q8 = createQuestionSection(lblpregunta814,rgQI814); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(individual.qi807)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI807"); 
			view = rgQI807; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi808)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI808"); 
			view = rgQI808; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi809)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI809"); 
			view = rgQI809; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi810)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI810"); 
			view = rgQI810; 
			error = true; 
			return false; 
		}
		if (Util.esVacio(individual.qi811)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI811"); 
			view = rgQI811; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi812)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI812"); 
			view = rgQI812; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi813)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI813"); 
			view = rgQI813; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi814)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI814"); 
			view = rgQI814; 
			error = true; 
			return false; 
		} 
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
     	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
    	App.getInstance().getCiseccion_08().qi801a=individual.qi801a;
    	App.getInstance().getCiseccion_08().qi801b=individual.qi801b;
    	entityToUI(individual); 
    	inicio(); 
    } 
    
    public void validarPregunta801() {
    	 Integer in801a= individual.qi801a==null?0:individual.qi801a;
    	 Integer in801b= individual.qi801b==null?0:individual.qi801b;
    	
    	if(in801a==2 && in801b==2){
    		Util.cleanAndLockView(getActivity(), rgQI807,rgQI808,rgQI809,rgQI810,rgQI811,rgQI812,rgQI813,rgQI814);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(),false,rgQI807,rgQI808,rgQI809,rgQI810,rgQI811,rgQI812,rgQI813,rgQI814);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		q0.setVisibility(View.VISIBLE);
    	}
	}
    
    private void inicio() { 
    	validarPregunta801();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI807.readOnly();
    		rgQI808.readOnly();
    		rgQI809.readOnly();
    		rgQI810.readOnly();
    		rgQI811.readOnly();
    		rgQI812.readOnly();
    		rgQI813.readOnly();
    		rgQI814.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
