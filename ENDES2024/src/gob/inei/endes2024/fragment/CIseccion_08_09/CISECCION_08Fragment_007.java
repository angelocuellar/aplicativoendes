package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_007 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI817EA; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI817EB; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI817EC; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI817ED; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI817EE; 
	
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI817G; 	
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI817G_A; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI817G_B; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI817G_C; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI817G_D; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI817G_E; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI817G_F; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI817G_G; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI817G_H; 
	@FieldAnnotation(orderIndex=15) 
	public TextField txtQI817G_HI; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI817G_I; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI817G_J; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI817G_K; 
	@FieldAnnotation(orderIndex=19) 
	public TextField txtQI817G_KI; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI817G_L; 
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI817G_M; 
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI817G_X; 
	@FieldAnnotation(orderIndex=23) 
	public TextField txtQI817G_XI; 
	
	CISECCION_08 individual; 
	CISECCION_05_07 individualS5;
	CISECCION_01_03 individualS1_3;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSP,lblSPR,lblONG,lblpregunta717E_a,lblpregunta717E_b,lblpregunta717E_c,lblpregunta717E_d,lblpregunta717E_e,lblpregunta817E,lblpregunta817G,lblpregunta817G_ind,lblpregunta817G_nom,lblpregunta817G_rep,lblpregunta817G_ind2; 
	public TextField txtCabecera;
	private GridComponent2 gridPreguntas817G,gridPreguntas817E;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5,seccionesCargadoS1_3; 

	public CISECCION_08Fragment_007() {} 
	public CISECCION_08Fragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI817EA","QI817EB","QI817EC","QI817ED","QI817EE","QI817G","QI817G_A","QI817G_B","QI817G_C","QI817G_D","QI817G_E","QI817G_F","QI817G_G","QI817G_H","QI817G_HI","QI817G_I","QI817G_J","QI817G_K","QI817G_KI","QI817G_L","QI817G_M","QI817G_X","QI817G_XI","QI801A","QI801B","QI817A","QI817B","QI817C","QI815","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI817EA","QI817EB","QI817EC","QI817ED","QI817EE","QI817G","QI817G_A","QI817G_B","QI817G_C","QI817G_D","QI817G_E","QI817G_F","QI817G_G","QI817G_H","QI817G_HI","QI817G_I","QI817G_J","QI817G_K","QI817G_KI","QI817G_L","QI817G_M","QI817G_X","QI817G_XI")};
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512")};
		seccionesCargadoS1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI817EA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817e_1,R.string.c2seccion_08_09qi817e_2).size(altoComponente+20,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS817EAChangeValue");
		rgQI817EB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817e_1,R.string.c2seccion_08_09qi817e_2).size(altoComponente+20,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI817EC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817e_1,R.string.c2seccion_08_09qi817e_2).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI817ED=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817e_1,R.string.c2seccion_08_09qi817e_2).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI817EE=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817e_1,R.string.c2seccion_08_09qi817e_2).size(altoComponente+20,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		lblSP  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi817g_t1).textSize(16).negrita();
		lblSPR  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi817g_t2).textSize(16).negrita();
		lblONG  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi817g_t3).textSize(16).negrita();
		chbQI817G_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_a, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_b, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_c, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_d, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_e, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_f, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_g, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_h, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426IChangeValue");				
		chbQI817G_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_i, "1:0").size(WRAP_CONTENT, 300);		
		chbQI817G_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_j, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_k, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426MChangeValue");			
		chbQI817G_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_l, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_m, "1:0").size(WRAP_CONTENT, 300);
		chbQI817G_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi817g_x, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426XChangeValue");
		txtQI817G_KI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);	
		txtQI817G_HI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		txtQI817G_XI=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		txtQI817G=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 500).alfanumerico();
		
		lblpregunta817E = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817e);
		lblpregunta817G = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817g);
		lblpregunta817G_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi817g_ind);
		lblpregunta817G_nom = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi817g_nom).centrar();
		lblpregunta817G_rep = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817g_rep);
		lblpregunta817G_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi817g_ind2);
		
		lblpregunta717E_a = new LabelComponent(this.getActivity()).size(altoComponente+20, 550).textSize(19).text(R.string.c2seccion_08_09qi817e_a);
		lblpregunta717E_b = new LabelComponent(this.getActivity()).size(altoComponente+20, 550).textSize(19).text(R.string.c2seccion_08_09qi817e_b);
		lblpregunta717E_c = new LabelComponent(this.getActivity()).size(altoComponente, 550).textSize(19).text(R.string.c2seccion_08_09qi817e_c);
		lblpregunta717E_d = new LabelComponent(this.getActivity()).size(altoComponente, 550).textSize(19).text(R.string.c2seccion_08_09qi817e_d);
		lblpregunta717E_e = new LabelComponent(this.getActivity()).size(altoComponente+20, 550).textSize(19).text(R.string.c2seccion_08_09qi817e_e);
		
		gridPreguntas817E = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas817E.addComponent(lblpregunta717E_a);
		gridPreguntas817E.addComponent(rgQI817EA);
		gridPreguntas817E.addComponent(lblpregunta717E_b);
		gridPreguntas817E.addComponent(rgQI817EB);
		gridPreguntas817E.addComponent(lblpregunta717E_c);
		gridPreguntas817E.addComponent(rgQI817EC);
		gridPreguntas817E.addComponent(lblpregunta717E_d);
		gridPreguntas817E.addComponent(rgQI817ED);
		gridPreguntas817E.addComponent(lblpregunta717E_e);
		gridPreguntas817E.addComponent(rgQI817EE);
		
		gridPreguntas817G = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas817G.addComponent(lblSP,2);
		gridPreguntas817G.addComponent(chbQI817G_A,2);
		gridPreguntas817G.addComponent(chbQI817G_B,2);
		gridPreguntas817G.addComponent(chbQI817G_C,2);
		gridPreguntas817G.addComponent(chbQI817G_D,2);
		gridPreguntas817G.addComponent(chbQI817G_E,2);
		gridPreguntas817G.addComponent(chbQI817G_F,2);
		gridPreguntas817G.addComponent(chbQI817G_G,2);
		gridPreguntas817G.addComponent(chbQI817G_H);
		gridPreguntas817G.addComponent(txtQI817G_HI);
		gridPreguntas817G.addComponent(lblSPR,2);
		gridPreguntas817G.addComponent(chbQI817G_I,2);
		gridPreguntas817G.addComponent(chbQI817G_J,2);
		gridPreguntas817G.addComponent(chbQI817G_K);
		gridPreguntas817G.addComponent(txtQI817G_KI);
		gridPreguntas817G.addComponent(lblONG,2);
		gridPreguntas817G.addComponent(chbQI817G_L,2);
		gridPreguntas817G.addComponent(chbQI817G_M,2);
		gridPreguntas817G.addComponent(chbQI817G_X);
		gridPreguntas817G.addComponent(txtQI817G_XI);
    } 	
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta817E,gridPreguntas817E.component());  
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta817G,lblpregunta817G_ind,txtQI817G,lblpregunta817G_nom,lblpregunta817G_rep,lblpregunta817G_ind2,gridPreguntas817G.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);		
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (!Util.esDiferente(individual.qi817b,1) || !Util.esDiferente(individual.qi817c,1) || !Util.esDiferente(individual.qi817a,1)) {
			if (Util.esVacio(individual.qi817ea)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817EA"); 
				view = rgQI817EA; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi817eb)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817EB"); 
				view = rgQI817EB; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi817ec)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817EC"); 
				view = rgQI817EC; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi817ed)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817ED"); 
				view = rgQI817ED; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi817ee)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817EE"); 
				view = rgQI817EE; 
				error = true; 
				return false; 
			} 
		}
		
		if (!Util.esDiferente(individual.qi817ea,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi817g_a,individual.qi817g_b,individual.qi817g_c,individual.qi817g_d,individual.qi817g_e,individual.qi817g_f,individual.qi817g_g,individual.qi817g_h,individual.qi817g_i,individual.qi817g_j,individual.qi817g_k,individual.qi817g_l,individual.qi817g_m,individual.qi817g_x)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI817G_A; 
				error = true; 
				return false; 
			}
			if (chbQI817G_H.isChecked()) {
				if (Util.esVacio(individual.qi817g_hi)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI817G_HI; 
					error = true; 
					return false; 
				}
			}
			if (chbQI817G_K.isChecked()) {
				if (Util.esVacio(individual.qi817g_ki)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI817G_KI; 
					error = true; 
					return false; 
				}
			}
			if (chbQI817G_X.isChecked()) {
				if (Util.esVacio(individual.qi817g_xi)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI817G_XI; 
					error = true; 
					return false; 
				}
			}
		}

		return true; 
    } 
    @Override 
    public void cargarDatos() { 
     	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
     	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
     	individualS1_3 = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS1_3);
     	
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
    	if (App.getInstance().getCiseccion05_07()!=null && individualS5!=null) {
    		App.getInstance().getCiseccion05_07().qi512=individualS5.qi512;
		}
    	
    	
    	if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi801a = individual.qi801a;
			App.getInstance().getCiseccion_08().qi801b = individual.qi801b;
		}
		else{ 
			App.getInstance().setCiseccion_08(individual);
		}
    	
    	entityToUI(individual); 
    	inicio();
    } 
    
    public void ValidarPregunta817BC() {
    	Integer in815 = individual.qi815==null?0:individual.qi815;
    	Integer in512=0;
    	if (individualS5!=null) {
    		in512 = individualS5.qi512==null?0:individualS5.qi512;	
		}
    	
    	
    	if ((!Util.esDiferente(individual.qi817b,2,3) && !Util.esDiferente(individual.qi817c,2,3)) || (in815==1 && in512==0)) {
    		Util.cleanAndLockView(getActivity(),rgQI817EA,rgQI817EB,rgQI817EC,rgQI817ED,rgQI817EE,chbQI817G_A,chbQI817G_B,chbQI817G_C,chbQI817G_D,chbQI817G_E,chbQI817G_F,chbQI817G_G,chbQI817G_H,chbQI817G_I,chbQI817G_J,chbQI817G_K,chbQI817G_L,chbQI817G_M,chbQI817G_X,txtQI817G_HI,txtQI817G_KI,txtQI817G_XI); 
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE); 
    	}
    	else{
     		Util.lockView(getActivity(),false,rgQI817EA,rgQI817EB,rgQI817EC,rgQI817ED,rgQI817EE,chbQI817G_A,chbQI817G_B,chbQI817G_C,chbQI817G_D,chbQI817G_E,chbQI817G_F,chbQI817G_G,chbQI817G_H,chbQI817G_I,chbQI817G_J,chbQI817G_K,chbQI817G_L,chbQI817G_M,chbQI817G_X); 
     		q0.setVisibility(View.VISIBLE);
     		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		onqrgQS817EAChangeValue();
    	}
    }
    private void inicio() { 
    	ValidarPregunta817BC();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    public void onqrgQS817EAChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI817EA.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),chbQI817G_A,chbQI817G_B,chbQI817G_C,chbQI817G_D,chbQI817G_E,chbQI817G_F,chbQI817G_G,chbQI817G_H,chbQI817G_I,chbQI817G_J,chbQI817G_K,chbQI817G_L,chbQI817G_M,chbQI817G_X,txtQI817G_HI,txtQI817G_KI,txtQI817G_XI);
    		MyUtil.LiberarMemoria();
    		q2.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,chbQI817G_A,chbQI817G_B,chbQI817G_C,chbQI817G_D,chbQI817G_E,chbQI817G_F,chbQI817G_G,chbQI817G_H,chbQI817G_I,chbQI817G_J,chbQI817G_K,chbQI817G_L,chbQI817G_M,chbQI817G_X,txtQI817G_HI); 
    		q2.setVisibility(View.VISIBLE);
    		chbQI817G_A.requestFocus();
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI817EA.readOnly();
    		rgQI817EB.readOnly();
    		rgQI817EC.readOnly();
    		rgQI817ED.readOnly();
    		rgQI817EE.readOnly();
    		txtQI817G.readOnly();
    		txtQI817G_HI.readOnly();
    		txtQI817G_KI.readOnly();
    		txtQI817G_XI.readOnly();
    		chbQI817G_A.readOnly();
    		chbQI817G_B.readOnly();
    		chbQI817G_C.readOnly();
    		chbQI817G_D.readOnly();
    		chbQI817G_E.readOnly();
    		chbQI817G_F.readOnly();
    		chbQI817G_G.readOnly();
    		chbQI817G_H.readOnly();
    		chbQI817G_I.readOnly();
    		chbQI817G_J.readOnly();
    		chbQI817G_K.readOnly();
    		chbQI817G_L.readOnly();
    		chbQI817G_M.readOnly();
    		chbQI817G_X.readOnly();
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
