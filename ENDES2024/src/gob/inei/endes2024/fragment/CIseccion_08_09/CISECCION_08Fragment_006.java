package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_006 extends FragmentForm { 	
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI816A_A; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI816A_B; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI816A_C; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI816A_D; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI816A_E; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI816A_F; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI816A_G; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI816A_H; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI816A_I; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI816A_J; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI816A_K; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI816A_L; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI816A_W; 
	@FieldAnnotation(orderIndex=14) 
	public TextField txtQI816A_WX; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI816A_Z;
	
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQI817A; 
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQI817B; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI817C; 
	
	CISECCION_08 individual; 
	CISECCION_05_07 individualS5;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta816A,lblpregunta816A_ind,lblpregunta817A,lblpregunta817B,lblpregunta817C;
	public TextField txtCabecera;
	private GridComponent2 gridPreguntas816A;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS5; 
	private Calendar fecha_actual =null; 
	public CISECCION_08Fragment_006() {} 
	public CISECCION_08Fragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI816A_A","QI816A_B","QI816A_C","QI816A_D","QI816A_E","QI816A_F","QI816A_G","QI816A_H","QI816A_I","QI816A_J","QI816A_K","QI816A_L","QI816A_W","QI816A_WX","QI816A_Z","QI817A","QI817B","QI817C","QI815","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI816A_A","QI816A_B","QI816A_C","QI816A_D","QI816A_E","QI816A_F","QI816A_G","QI816A_H","QI816A_I","QI816A_J","QI816A_K","QI816A_L","QI816A_W","QI816A_WX","QI816A_Z","QI817A","QI817B","QI817C")}; 
		seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512","QI707_CONS")};
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		chbQI816A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_a, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_b, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_c, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_d, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_e, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_f, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_g, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_h, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_i, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_j, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_k, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_l, "1:0").size(WRAP_CONTENT, 200);
		chbQI816A_W=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_w, "1:0").size(WRAP_CONTENT, 200).callback("onqi816AWChangeValue");
		chbQI816A_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi816a_z, "1:0").size(WRAP_CONTENT, 200);
		txtQI816A_WX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 550);
		
		rgQI817A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817a_1,R.string.c2seccion_08_09qi817a_2,R.string.c2seccion_08_09qi817a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS817AChangeValue"); 
		rgQI817B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817b_1,R.string.c2seccion_08_09qi817b_2,R.string.c2seccion_08_09qi817b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI817C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi817c_1,R.string.c2seccion_08_09qi817c_2,R.string.c2seccion_08_09qi817c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
//		lblpregunta816A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi816a);
		Spanned texto816A = Html.fromHtml("816A �Qu� s�ntomas y signos le pueden hacer pensar a Ud. <u><b>que una mujer</b></u> tiene una enfermedad de transmisi�n sexual? <br>�Alg�n otro s�ntoma? ");
		lblpregunta816A = new LabelComponent(this.getActivity()).textSize(19).size(MATCH_PARENT, MATCH_PARENT);
		lblpregunta816A.setText(texto816A);
		
		lblpregunta816A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi816a_ind);
		lblpregunta817A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817a);
		lblpregunta817B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817b);
		lblpregunta817C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi817c);
		
		gridPreguntas816A = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas816A.addComponent(chbQI816A_A,2);
		gridPreguntas816A.addComponent(chbQI816A_B,2);
		gridPreguntas816A.addComponent(chbQI816A_C,2);
		gridPreguntas816A.addComponent(chbQI816A_D,2);
		gridPreguntas816A.addComponent(chbQI816A_E,2);
		gridPreguntas816A.addComponent(chbQI816A_F,2);
		gridPreguntas816A.addComponent(chbQI816A_G,2);
		gridPreguntas816A.addComponent(chbQI816A_H,2);
		gridPreguntas816A.addComponent(chbQI816A_I,2);
		gridPreguntas816A.addComponent(chbQI816A_J,2);
		gridPreguntas816A.addComponent(chbQI816A_K,2);
		gridPreguntas816A.addComponent(chbQI816A_L,2);
		gridPreguntas816A.addComponent(chbQI816A_W);
		gridPreguntas816A.addComponent(txtQI816A_WX);
		gridPreguntas816A.addComponent(chbQI816A_Z,2);

    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta816A,lblpregunta816A_ind,gridPreguntas816A.component()); 
		q2 = createQuestionSection(lblpregunta817A,rgQI817A); 
		q3 = createQuestionSection(lblpregunta817B,rgQI817B); 
		q4 = createQuestionSection(lblpregunta817C,rgQI817C); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		App.getInstance().setCiseccion_08(individual);
		App.getInstance().getCiseccion_08().qi817b= individual.qi817b;
		App.getInstance().getCiseccion_08().qi817c = individual.qi817c;
		
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
 
		if (!Util.esDiferente(individual.qi815,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi816a_a,individual.qi816a_b,individual.qi816a_c,individual.qi816a_d,individual.qi816a_e,individual.qi816a_f,individual.qi816a_g,individual.qi816a_h,individual.qi816a_i,individual.qi816a_j,individual.qi816a_k,individual.qi816a_l,individual.qi816a_w,individual.qi816a_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI816A_A; 
				error = true; 
				return false; 
			}
			if (chbQI816A_W.isChecked()) {
				if (Util.esVacio(individual.qi816a_wx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI816A_WX; 
					error = true; 
					return false; 
				}
			}
			
			if (Util.esDiferente(individualS5.qi512,0)) {
				if (Util.esVacio(individual.qi817a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI817A"); 
					view = rgQI817A; 
					error = true; 
					return false; 
				}
			}
		}	
		if (Util.esDiferente(individual.qi817a,1) && Util.esDiferente(individualS5.qi512,0)) {
			if (Util.esVacio(individual.qi817b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817B"); 
				view = rgQI817B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi817c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI817C"); 
				view = rgQI817C; 
				error = true; 
				return false; 
			}
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individualS5 = getCuestionarioService().getCISECCION_05_07(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS5);
    	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
    	if(individualS5!=null && individualS5.qi707_cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(individualS5.qi707_cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
    	App.getInstance().getCiseccion05_07().qi512=individualS5.qi512;
    	App.getInstance().getCiseccion_08().qi817b=individual.qi817b;
    	App.getInstance().getCiseccion_08().qi817c=individual.qi817c;
    	
    	entityToUI(individual);
    	inicio();
    }
    
    public void ValidarPregunta815() {
    	if (!Util.esDiferente(individual.qi815,1) && Util.esDiferente(individualS5.qi512,0)) {
    		Log.e("1","1");
    		Util.lockView(getActivity(),false,chbQI816A_A,chbQI816A_B,chbQI816A_C,chbQI816A_D,chbQI816A_E,chbQI816A_F,chbQI816A_G,chbQI816A_H,chbQI816A_I,chbQI816A_J,chbQI816A_K,chbQI816A_L,chbQI816A_W,chbQI816A_Z,txtQI816A_WX,rgQI817A,rgQI817B,rgQI817C); 
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		onqrgQS817AChangeValue();
    	}
    	if (!Util.esDiferente(individual.qi815,2) && Util.esDiferente(individualS5.qi512,0)) {
    		Log.e("1","2");
    		Util.cleanAndLockView(getActivity(),chbQI816A_A,chbQI816A_B,chbQI816A_C,chbQI816A_D,chbQI816A_E,chbQI816A_F,chbQI816A_G,chbQI816A_H,chbQI816A_I,chbQI816A_J,chbQI816A_K,chbQI816A_L,chbQI816A_W,chbQI816A_Z,txtQI816A_WX,rgQI817A); 
    		Util.lockView(getActivity(),false,rgQI817B,rgQI817C);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    	}
    	if (!Util.esDiferente(individual.qi815,1) && !Util.esDiferente(individualS5.qi512,0)) {
    		Log.e("1","3");
    		Util.cleanAndLockView(getActivity(),rgQI817A,rgQI817B,rgQI817C); 
    		Util.lockView(getActivity(),false,chbQI816A_A,chbQI816A_B,chbQI816A_C,chbQI816A_D,chbQI816A_E,chbQI816A_F,chbQI816A_G,chbQI816A_H,chbQI816A_I,chbQI816A_J,chbQI816A_K,chbQI816A_L,chbQI816A_W,chbQI816A_Z,txtQI816A_WX);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	}
    	if (!Util.esDiferente(individual.qi815,2) && !Util.esDiferente(individualS5.qi512,0)) {
    		Log.e("1","4");
    		Util.cleanAndLockView(getActivity(),chbQI816A_A,chbQI816A_B,chbQI816A_C,chbQI816A_D,chbQI816A_E,chbQI816A_F,chbQI816A_G,chbQI816A_H,chbQI816A_I,chbQI816A_J,chbQI816A_K,chbQI816A_L,chbQI816A_W,chbQI816A_Z,txtQI816A_WX,rgQI817A,rgQI817B,rgQI817C);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	}
	}
    
    private void inicio() { 
    	ValidarPregunta815();
    	RenombrarEtiquetas();
    	onqi816AWChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void onqi816AWChangeValue(){
    	if(!chbQI816A_W.isChecked()){
    		Util.cleanAndLockView(getActivity(), txtQI816A_WX);
    	}
    	else{
    		Util.lockView(getActivity(), false,txtQI816A_WX);
    	}
    }
    public void onqrgQS817AChangeValue() {
    	if (MyUtil.incluyeRango(1,1,rgQI817A.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),rgQI817B,rgQI817C);  		
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI817B,rgQI817C); 
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		rgQI817B.requestFocus();
    		
    	}
    }
    
    public void RenombrarEtiquetas() {
//    	Calendar fechaactual = new GregorianCalendar();
    	Calendar fechainicio = MyUtil.RestarMeses(fecha_actual,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecha_actual,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
    	lblpregunta817A.text(R.string.c2seccion_08_09qi817a);
    	lblpregunta817A.setText(lblpregunta817A.getText().toString().replace("#", F2));
    	lblpregunta817A.setText(lblpregunta817A.getText().toString().replace("$", F1));
    	
    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o";
    	}
    	lblpregunta817A.setText(lblpregunta817A.getText().toString().replace("%", F3));
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI817A.readOnly();
    		rgQI817B.readOnly();
    		rgQI817C.readOnly();
    		txtQI816A_WX.readOnly();
    		chbQI816A_A.readOnly();
    		chbQI816A_B.readOnly();
    		chbQI816A_C.readOnly();
    		chbQI816A_D.readOnly();
    		chbQI816A_E.readOnly();
    		chbQI816A_F.readOnly();
    		chbQI816A_G.readOnly();
    		chbQI816A_H.readOnly();
    		chbQI816A_I.readOnly();
    		chbQI816A_J.readOnly();
    		chbQI816A_K.readOnly();
    		chbQI816A_L.readOnly();
    		chbQI816A_W.readOnly();
    		chbQI816A_Z.readOnly();
    		
    	}
    }
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		App.getInstance().setCiseccion_08(individual);
		App.getInstance().getCiseccion_08().qi817b= individual.qi817b;
		App.getInstance().getCiseccion_08().qi817c = individual.qi817c;
		
		return App.INDIVIDUAL; 
	} 
} 
