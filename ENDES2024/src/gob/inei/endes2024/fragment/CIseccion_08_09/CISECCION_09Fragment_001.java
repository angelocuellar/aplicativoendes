package gob.inei.endes2024.fragment.CIseccion_08_09;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_08_09.Dialog.CISECCION_09Fragment_001_1Dialog;
import gob.inei.endes2024.fragment.CIseccion_08_09.Dialog.CISECCION_09Fragment_001_2Dialog;
import gob.inei.endes2024.model.CIHERMANOS;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.IExportacion;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.net.NetworkInfo.DetailedState;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_09Fragment_001 extends FragmentForm implements Respondible {
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI901; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI903;
//	@FieldAnnotation(orderIndex=3)
	public TextAreaField txtQIOBS_S9;

	
	public ButtonComponent btnAgregar; 
	public TableComponent tcHermanos;
	public TextField txtCabecera;
	public List<CISECCION_09> personas;
	public List<CIHERMANOS> cihermanos;
	
	public Integer numHerMayores;
	
	
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoseccion08; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoseccion08,seccionesCargadoS5, seccionesCargadoseccion01; 
	
	private CuestionarioService cuestionarioService;
	HermanosClickListener adapter;
	public LabelComponent lbltitulo,lblprimeraindicacion,lblpregunta901,lblpregunta903,lblobs_s9;
	private DialogComponent dialog;
	
	public GridComponent2 gridpregunta901;
	
	public LinearLayout q0;
	public LinearLayout q1;
	public LinearLayout q2;
	public LinearLayout q3;
	
	CISECCION_08 individual;
	CISECCION_01_03 mef;
	
	private enum ACTION {
		ELIMINAR
	}

	private ACTION action;
	
	public CISECCION_09Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	@Override
	protected View createUI() {
		// TODO Auto-generated method stub
		buildFields();
		q0 = createQuestionSection(lbltitulo,txtCabecera);
		q1 = createQuestionSection(lblprimeraindicacion,gridpregunta901.component());
		q2 = createQuestionSection(0,btnAgregar,tcHermanos.getTableView());
		q3 = createQuestionSection(lblobs_s9,txtQIOBS_S9);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		return contenedor;
	}
	  @Override 
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
			rootView = createUI(); 
			initObjectsWithoutXML(this, rootView); 
			adapter = new HermanosClickListener();
			tcHermanos.getListView().setOnItemClickListener(adapter);
			enlazarCajas(); 
			listening();
			seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI917","QI916","QI915","QI914","QI913","QI912","QI911","QI910","QI909","QI908","QI907","QI906","QI905","QI904","QINRO_ORDEN","ID","HOGAR_ID","PERSONA_ID","ESTADO") };
			seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"QI917","QI916","QI915","QI914","QI913","QI912","QI911","QI910","QI909","QI908","QI907","QI906","QI905","QI904","QINRO_ORDEN")};
			seccionesCargadoseccion08 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI901","QI903","QIOBS_S9","QI817B","QI817C","QI815","ID","HOGAR_ID","PERSONA_ID") };
			seccionesGrabadoseccion08 = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI901","QI903","QIOBS_S9") };
			seccionesCargadoS5 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI512","ID","HOGAR_ID","PERSONA_ID")};
			seccionesCargadoseccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","ID","HOGAR_ID","PERSONA_ID")};
			
			return rootView; 
		} 
	@Override
	protected void buildFields() {
		lbltitulo = new LabelComponent(getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_09titulo).textSize(20).centrar();
		lblprimeraindicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_indicacion).textSize(19);
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblpregunta901 = new LabelComponent(getActivity()).size(altoComponente+10, 650).text(R.string.c2seccion_dit_qi901).textSize(17);
		lblpregunta903 = new LabelComponent(getActivity()).size(altoComponente+15, 650).text(R.string.c2seccion_dit_qi903).textSize(18);
		txtQI901 = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2).callback("onQI901ChangeValue");
		txtQI903 = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2);
		
		gridpregunta901=new GridComponent2(getActivity(),App.ESTILO, 2,0);
		gridpregunta901.addComponent(lblpregunta901);
		gridpregunta901.addComponent(txtQI901);
		gridpregunta901.addComponent(lblpregunta903);
		gridpregunta901.addComponent(txtQI903);
		
		lblobs_s9 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2obss9);
		txtQIOBS_S9 = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
		
		
		btnAgregar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200,55).text(R.string.btnAgregar);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag=false;
				flag=grabaralgunos();
				if(!flag){
					return;
				}
				agregarHermano();
				Util.cleanAndLockView(getActivity(), btnAgregar);
			}
		});
//		  Integer d=getResources().getDisplayMetrics().densityDpi;
//		  if(!Util.esDiferente(d,App.YOGA8)){
//			  tcHermanos = new TableComponent(getActivity(), this,App.ESTILO).size(500,750).headerHeight(altoComponente + 10).dataColumHeight(50);
//			}
//			else{
			  tcHermanos = new TableComponent(getActivity(), this,App.ESTILO).size(600,750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//			} 
		
		tcHermanos.addHeader(R.string.c2seccion_08qi900nroorden,0.3f);
		tcHermanos.addHeader(R.string.c2seccion_08qi904nombre,1f);
		tcHermanos.addHeader(R.string.c2seccion_08qi90sexo,1f);
		tcHermanos.addHeader(R.string.c2seccion_08qi90edad,1f);
		tcHermanos.addHeader(R.string.c2seccion_08qi90vive,1f);
	}

	public boolean grabaralgunos(){
		uiToEntity(individual); 
		if (!validaralgunos()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabadoseccion08)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8 = getCuestionarioService().getCompletadoSeccion008CI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		return true;
	}
	
	@Override
	public boolean grabar() {
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabadoseccion08)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8 = getCuestionarioService().getCompletadoSeccion008CI(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion_08()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null){ 
			App.getInstance().getCiseccion_08().qi901 = individual.qi901;
			App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto8 = getCuestionarioService().getCompletadoSeccion008CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		}
		else 
			App.getInstance().setCiseccion_08(individual);
		
		
		lista_hermanos_s9();
		return true;
	}
	private boolean validaralgunos(){
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
//		Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//		if (!(int106>=12 && int106<=14)) {
			if (Util.esVacio(individual.qi901)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI901"); 
				view = txtQI901; 
				error = true; 
				return false; 
			} 
			if (Util.esMayor(individual.qi901,20)) { 
				mensaje = "Pregunta QI901 no puede ser mayor a 20"; 
				view = txtQI901; 
				error = true; 
				return false; 
			} 
			if(Util.esMayor(individual.qi901, 1)){
				if (Util.esVacio(individual.qi903)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI903"); 
					view = txtQI903; 
					error = true; 
					return false; 
				}
				if (Util.esMenor(individual.qi901,1)) { 
					mensaje = "La pregunta QI901 no puede ser menor a 1"; 
					view = txtQI901; 
					error = true; 
					return false; 
				}
				if (Util.esMayor(individual.qi903,individual.qi901)) { 
					mensaje = "La Pregunta QI903 no puede ser mayor que QI901"; 
					view = txtQI903; 
					error = true; 
					return false; 
				} 
//			}
		}
		
		return true;
	}
	
	  private boolean validar() {
			if(!isInRange()) return false; 
			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
			Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//			if (!(int106>=12 && int106<=14)) {
				
			
				if (Util.esVacio(individual.qi901)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QI901"); 
					view = txtQI901; 
					error = true; 
					return false; 
				} 
				if (Util.esMayor(individual.qi901,20)) { 
					mensaje = "Pregunta QI901 no puede ser mayor a 20"; 
					view = txtQI901; 
					error = true; 
					return false; 
				} 
				if(Util.esMayor(individual.qi901, 1)){
					if (Util.esVacio(individual.qi903)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI903"); 
						view = txtQI903; 
						error = true; 
						return false; 
					}
					if (Util.esMenor(individual.qi901,1)) { 
						mensaje = "La pregunta QI901 no puede ser menor a 1"; 
						view = txtQI901; 
						error = true; 
						return false; 
					}
					if (Util.esMayor(individual.qi903,individual.qi901)) { 
						mensaje = "La Pregunta QI903 no puede ser mayor que QI901"; 
						view = txtQI903; 
						error = true; 
						return false; 
					} 
				}
				
			if (individual.qi901!=null) {
				Integer totalpersonas=personas.size();
				Integer contadorns=0;
				Integer todosson98=0;
				boolean edaddetodosnosabe=false;
				if(totalpersonas>0){
					for(CISECCION_09 hermano: personas){
						if(hermano.qi906!= null && hermano.qi906==8){
							contadorns++;
						}
						if(hermano.qi907!=null && hermano.qi907==98){
							todosson98++;
						}
					}
				}
				edaddetodosnosabe=todosson98==totalpersonas;
				if(contadorns!=totalpersonas && !edaddetodosnosabe){
				Integer edad=-1;
				boolean noexistehermanosgemelos=false;
				boolean meftienegemela=false;
				Integer contador=0;
				if(totalpersonas>0){
					for(CISECCION_09 h: personas){
						if(h.edad!=null && !Util.esDiferente(h.edad, edad)){
							edad=h.edad;
							noexistehermanosgemelos=true;
						}
						edad=h.edad;
						if(h.edad!=null && !Util.esDiferente(h.edad, mef.qi106)){
							meftienegemela=true;
							contador++;
						}
					}
				}
				if (Util.esMayor(individual.qi901,1) && noexistehermanosgemelos && !meftienegemela) {
					if (individual.qi903!=numHerMayores) {
						validarMensaje ("Verifique Pregunta 903 deber�a ser . : "+numHerMayores);
//						mensaje = "Pregunta 903 deber�a ser .: "+numHerMayores;
//						view = txtQI903; 
//						error = true; 
//						return false;
					}
				}
				if (Util.esMayor(individual.qi901,1) && !noexistehermanosgemelos && meftienegemela) {
					Integer restado=numHerMayores+1;
					if (individual.qi903!=numHerMayores && individual.qi903!=restado) {
						validarMensaje ("Verifique Pregunta 903 deber�a ser .. : "+numHerMayores+" � "+restado);
//						mensaje = "Pregunta 903 deber�a ser ..: "+numHerMayores+" � "+restado;
//						view = txtQI903; 
//						error = true; 
//						return false;
					}
				}
				if (Util.esMayor(individual.qi901,1) && !noexistehermanosgemelos && !meftienegemela) {
					if (individual.qi903!=numHerMayores) {
						validarMensaje ("Verifique Pregunta 903 deber�a ser ... : "+numHerMayores);
//						mensaje = "Pregunta 903 deber�a ser ...: "+numHerMayores;
//						view = txtQI903; 
//						error = true; 
//						return false;
					}
				}
				if(meftienegemela)
					contador++;
				
				if (Util.esMayor(individual.qi901,1) && noexistehermanosgemelos && meftienegemela) {
					Integer restado=numHerMayores+1;
					Integer restado1=restado+1;
					if (individual.qi903!=numHerMayores && individual.qi903!=restado && contador!=3) {
						validarMensaje ("Verifique Pregunta 903 deber�a ser .... : "+numHerMayores+" � "+restado);
//						mensaje = "Pregunta 903 deber�a ser ....: "+numHerMayores+" � "+restado;	
//						view = txtQI903; 
//						error = true; 
//						return false;
					}
					if (individual.qi903!=numHerMayores && individual.qi903!=restado && contador==3) {
						validarMensaje ("Verifique Pregunta 903 deber�a ser ..... : "+numHerMayores+","+restado+" � "+restado1);
//						mensaje = "Pregunta 903 deber�a ser .....: "+numHerMayores+","+restado+" � "+restado1;
//						view = txtQI903; 
//						error = true; 
//						return false;
					}
				}
			}
			}
			Integer cantidad=personas.size();
			if(individual.qi901!=null && Util.esDiferente(cantidad,individual.qi901-1)){
				mensaje = "La cantidad de hermanos debe ser igual a la cantidad del listado"; 
				view = txtQI901; 
				error = true; 
				return false; 
			}
			if(!getCuestionarioService().getCompletadoTodoLosRegistrosHermanos(individual.id, individual.hogar_id, individual.persona_id)){
				validarMensaje("FALTA COMPLETAR REGISTROS DE HERMANOS!!");
			}

			Integer total=personas.size();
			if(total>0){
				Integer edadfiferida =personas.get(total-1).edad!=null?personas.get(total-1).edad -(personas.get(0).edad!=null?personas.get(0).edad:0):0;
				if(Util.esMayor(edadfiferida, 30)){
					validarMensaje("Verificar de edad del hemano menor y mayor no pude ser mayor a 30");
				}
			}
//			Log.e("","DIF: "+diferencia8anios);
//			if (diferencia8anios) {
//				validarMensaje("Diferencia entre hermanos es mayor a 8 a�os");
//			}
		
//		}
		return true; 
	  }
	  
	 private void validarMensaje(String msj) {
	        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	   }
	 
	  
	@Override
	public void cargarDatos() {
		individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoseccion08);
		
		if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
		entityToUI(individual);
		recargardatos();
		
		if (App.getInstance().getCiseccion_08()!=null && individual!=null &&individual.qi817b!=null && individual.qi817c!=null) {
			Log.e("dd","da");
			Log.e("dd","ds"+individual.qi817b+" "+ individual.qi817c);
			App.getInstance().getCiseccion_08().qi817b=individual.qi817b;
			App.getInstance().getCiseccion_08().qi817c=individual.qi817c;
		}
		else{
			Log.e("dd","df");
			App.getInstance().setCiseccion_08(individual);
		}
		
		inicio();
	}

	public void lista_hermanos_s9(){
		cihermanos= getCuestionarioService().getHermanosdeMefdiferencia(individual.id, individual.hogar_id,individual.persona_id);
		Integer edadanterior=0;
		Integer nro_orden=-1;
		boolean existe=false;
		for (CIHERMANOS items : cihermanos) {
			if(items.edad!=null){
				edadanterior=items.edad;
				nro_orden=items.persona;
			break;
			}
		}
		for (CIHERMANOS item : cihermanos) {
			if(item.edad!=null &&  (edadanterior-item.edad)>=8){
				existe=true;
				break;
			}
			edadanterior=item.edad==null?edadanterior:item.edad;
		}
		if(existe){
			MyUtil.MensajeGeneral(getActivity(), "Existe Diferencia de 8 a�os entre hermanos");
		}
	}
	
	public void agregarHermano() {
		CISECCION_09 bean = new CISECCION_09();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		bean.qinro_orden = personas.size() + 1;
		abrirDetalle(bean);
	}
	
	public void recargardatos(){
		personas= getCuestionarioService().getHermanosdeMef(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
		mef = getCuestionarioService().getCISECCION_01_03(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoseccion01);
		Integer edadMef;
		edadMef=mef.qi106==null?0:mef.qi106;
    	int year = Calendar.getInstance().get(Calendar.YEAR);
		Integer ordenMayorPersona=0;
    	if (personas.size()>0) {
    		for (CISECCION_09 persona : personas) {
    			if (persona.qi906!=null && persona.qi906==1 && persona.qi907!=null)
    				persona.edad=persona.qi907;
    			if (persona.qi906!=null && persona.qi906==2 && persona.qi912!=null && persona.qi910!=null && persona.qi910!=9998)
    				persona.edad=persona.qi912+year-persona.qi910;
    			if (persona.qi906!=null && persona.qi906==2 && persona.qi912!=null && persona.qi910!=null && persona.qi910==9998 && persona.qi911!=null)
    				persona.edad=persona.qi911+persona.qi912;
    			if (persona.edad!=null && (persona.edad>edadMef || persona.edad<edadMef))
    				ordenMayorPersona=persona.qinro_orden;
    		}
		}
		tcHermanos.setData(personas, "qinro_orden","qi904","getSexo","edad","getEstaVivo");//qi907
		DesabilitarelBotonAgregar();
		numHerMayores=0;
		Integer edadHermano=0;
//		Integer edadAnteriorHermano=0;
//		diferencia8anios=false;
//		boolean primero = true;
//		Integer contado=0;
//		contado=personas.size();
//		Integer resultado=0;
//		boolean valida=true;
		
		
		for (int row = 0; row < personas.size(); row++) {
			if (obtenerEstado(personas.get(row)) == 1) {
				// borde de color azul
				tcHermanos.setBorderRow(row, true);
			} else if (obtenerEstado(personas.get(row)) == 2) {
				// borde de color rojo
				tcHermanos.setBorderRow(row, true, R.color.red);
			} else {
				tcHermanos.setBorderRow(row, false);
			}
			edadHermano=personas.get(row).edad==null?0:personas.get(row).edad;
			
			/*if (edadHermano>0 && edadAnteriorHermano!=98) {
				Log.e("IF","");
				if (primero){
					edadAnteriorHermano=edadHermano;
				    primero = false;
				    resultado=edadAnteriorHermano- edadMef;
				    resultado=resultado<0?resultado*-1:resultado;
				    if (resultado>=8 && contado==1)
                		diferencia8anios=true;
				}else {
					Log.e("","ELSE");
					Log.e("","ORDEN PESONA 1 "+ordenMayorPersona+ " NRO: "+personas.get(row).qinro_orden);
	                if (ordenMayorPersona>=personas.get(row).qinro_orden) {
	                	if(valida){
	                	resultado= edadMef-(personas.get(0).edad!=null?personas.get(0).edad:0);
	                	valida=false;
	                	resultado=resultado<0?resultado*-1:resultado;
	                	if(resultado>=8)
	                		diferencia8anios=true;
	                	}
	                	Log.e("","2: " +(edadAnteriorHermano));
	                	if (edadAnteriorHermano-edadHermano>=8)
	                		diferencia8anios=true;
					}
	                else if (ordenMayorPersona<personas.get(row).qinro_orden) {
	                	if (ordenMayorPersona+1==personas.get(row).qinro_orden) {
	                		Log.e("","3: "+(edadAnteriorHermano- edadMef));
		                	if (edadAnteriorHermano- edadMef>=8)
		                		diferencia8anios=true;
		                	if (edadMef-edadHermano>=8)
		                		diferencia8anios=true;
						}else {
							if (edadAnteriorHermano-edadHermano>=8)
		                		diferencia8anios=true;
						}
					}
	                edadAnteriorHermano=edadHermano;

				}
			}
			*/
			/////////////
			if (edadMef < edadHermano){
				numHerMayores = personas.get(row).qinro_orden==null?0:personas.get(row).qinro_orden;
			}
		}
//		VerificarDiferenciaDeEdades(personas);
		tcHermanos.reloadData();
		registerForContextMenu(tcHermanos.getListView());
		
	}
	public void VerificarDiferenciaDeEdades(List<CISECCION_09> listado){
		List<CISECCION_09> copiahermanos=new ArrayList<CISECCION_09>();
		boolean entro=false;
		Integer orden=0;
		copiahermanos.addAll(listado);
		for(CISECCION_09 persona: listado){
			if(entro)
				break;
			copiahermanos.remove(persona);
			for(CISECCION_09 hermano: copiahermanos){
				orden=persona.qinro_orden+1;
				if(!Util.esDiferente(orden,hermano.qinro_orden) && hermano.edad!=null && persona.edad!=null && hermano.edad-persona.edad>=8){
					entro=true;
					break;
				}
			}
		}
		if(entro){
			validarMensaje("Verificar existen Hermanos que difieren en 8 a�os a mas");
		}
	}
		
	 private int obtenerEstado(CISECCION_09 detalle) {
			if (!Util.esDiferente(detalle.estado, 0)) {
				Log.e("","UNO");
				return 1 ;
			} else if (!Util.esDiferente(detalle.estado,1)) {
				Log.e("","DOS");
				return 2;
			}
			Log.e("","NINGUNO");
			return 0;
		}
	    
	public void DesabilitarelBotonAgregar(){
		Integer cantidad=personas.size();
		Integer caja=0;
		if(txtQI901.getValue()!=null){
			caja=Integer.parseInt(txtQI901.getText().toString());
			if(cantidad>=caja-1){
				btnAgregar.setEnabled(false);
			}
			else{
				btnAgregar.setEnabled(true);
			}
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    private class HermanosClickListener implements OnItemClickListener {
		public HermanosClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			CISECCION_09 c = (CISECCION_09) personas.get(arg2);
			abrirDetalleEditar(c, arg2, (List<CISECCION_09>) personas);
		}
	}
    public void refrescarPersonas(CISECCION_09 datos) {
		if (personas.contains(datos)) {
			recargardatos();
			return;
		} else {
			personas.add(datos);
			recargardatos();
		}
	}
//    public void abrirDetalleEditar(CISECCION_09 tmp){
//    	FragmentManager fm = CISECCION_09Fragment_001.this.getFragmentManager();
//    	CISeccion09_001_2Dialog aperturaDialog = CISeccion09_001_2Dialog.newInstance(this, tmp);
//		aperturaDialog.setAncho(MATCH_PARENT);
//		aperturaDialog.show(fm, "aperturaDialog");
//    }
   public void abrirDetalleEditar(CISECCION_09 tmp, int index, List<CISECCION_09> detalles){
    	FragmentManager fm = CISECCION_09Fragment_001.this.getFragmentManager();
    	CISECCION_09Fragment_001_2Dialog aperturaDialog = CISECCION_09Fragment_001_2Dialog.newInstance(CISECCION_09Fragment_001.this, tmp, index, detalles);
    	aperturaDialog.setAncho(MATCH_PARENT);
    	aperturaDialog.show(fm, "aperturaDialog");
    }
    
    public void abrirDetalle(CISECCION_09 tmp) {
    	FragmentManager fm = CISECCION_09Fragment_001.this.getFragmentManager();
    	CISECCION_09Fragment_001_1Dialog aperturaDialog = CISECCION_09Fragment_001_1Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
    }
    
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcHermanos.getListView())) {
			menu.setHeaderTitle("Opciones del Residente");
			menu.add(1, 0, 1, "Editar Hermano(a)");
			menu.add(1, 1, 1, "Eliminar Hermano(a)");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = personas.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			CISECCION_09 seleccion = (CISECCION_09) info.targetView.getTag();
			Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		menu.getItem(1).setVisible(false);
	    	}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		CISECCION_09 seleccion = (CISECCION_09) info.targetView.getTag();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalleEditar((CISECCION_09) personas.get(info.position), info.position,  (List<CISECCION_09>) personas);  //CISECCION_09 tmp, int index, List<CISECCION_09> detalles
				break;
			case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
				dialog.put("seleccion", (CISECCION_09) personas.get(info.position));
				dialog.showDialog();
				break;
				}
			}
		return super.onContextItemSelected(item);
	}
	
	
	public void onQI901ChangeValue(){
		Integer int106 = App.getInstance().getPersonaCuestionarioIndividual().qi106==null?0:App.getInstance().getPersonaCuestionarioIndividual().qi106;
//		if ((int106>=12 && int106<=14)) {
//			Util.cleanAndLockView(getActivity(),txtQI901, txtQI903, txtQIOBS_S9);  		
//    		q0.setVisibility(View.GONE);
//    		q1.setVisibility(View.GONE);
//    		q2.setVisibility(View.GONE);
//    		q3.setVisibility(View.GONE);
//			
//		}
//		else{
//			Util.lockView(getActivity(),false,txtQI901, txtQI903, txtQIOBS_S9);  		
//			q0.setVisibility(View.VISIBLE);
//    		q1.setVisibility(View.VISIBLE);
//    		q2.setVisibility(View.VISIBLE);
//    		q3.setVisibility(View.VISIBLE);
		
			Integer valor=txtQI901.getText().toString().trim().length()>0?Integer.parseInt(txtQI901.getText().toString().trim()):0;
			if (valor==0) {
				mensaje = "La Pregunta QI901 no puede ser 0"; 
				view = txtQI901; 
				error = true; 
			}
			List<CISECCION_09> todo=new ArrayList<CISECCION_09>();
			if(!Util.esDiferente(valor, 1) && personas.size()>0){
				todo.addAll(personas);
				for(CISECCION_09 h: todo){
					personas.remove(h);
					getCuestionarioService().DeleteMortalidadMaterna(h);
				}
			}
			if(Util.esMenor(valor, 2)){
				Util.cleanAndLockView(getActivity(), txtQI903,gridpregunta901);
				MyUtil.LiberarMemoria();
				lblpregunta903.setVisibility(View.GONE);
				txtQI903.setVisibility(View.GONE);
				q2.setVisibility(View.GONE);
				txtQI901.requestFocus();
			}
			else{
				Util.lockView(getActivity(), false,txtQI903,gridpregunta901);
				lblpregunta903.setVisibility(View.VISIBLE);
				txtQI903.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
				txtQI903.requestFocus();
			}
			DesabilitarelBotonAgregar();
//		}
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		if(action==ACTION.ELIMINAR){
			CISECCION_09 hermanoaeliminar = (CISECCION_09) dialog.get("seleccion");
			try {
				personas.remove(hermanoaeliminar);
				boolean borrado = false;
				borrado = getCuestionarioService().DeleteMortalidadMaterna(hermanoaeliminar);
				DesabilitarelBotonAgregar();
				if (!borrado) {
					throw new SQLException("Persona no pudo ser borrada.");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
		recargardatos();

		
	}
	
	public void inicio(){
		onQI901ChangeValue();
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQI901.readOnly();
			txtQI903.readOnly();
			txtQIOBS_S9.setEnabled(false);
			Util.cleanAndLockView(getActivity(), btnAgregar);
			btnAgregar.requestFocus();
		}
	}
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabadoseccion08)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}	
		return App.INDIVIDUAL;
	}
}