package gob.inei.endes2024.fragment.CIseccion_08_09.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_09Fragment_001;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_09Fragment_001_2Dialog extends DialogFragmentComponent  {

	@FieldAnnotation(orderIndex=1) 
	public TextField txtQI904; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI905; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI906; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI907; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI908; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI909; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQI910; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI911; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI912; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI913; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI914; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI915; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI916; 
	@FieldAnnotation(orderIndex=14) 
	public IntegerField txtQI917; 
	@FieldAnnotation(orderIndex=15) 
	public TextAreaField txtQI900_OBS;
	@FieldAnnotation(orderIndex=16) 
	public ButtonComponent btnAceptar;
	
	
	
	public static CISECCION_09Fragment_001 caller;
	CISECCION_09 hermano;
	CISECCION_08 individual; 
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	private static String nombre=null;
	private static List<CISECCION_09> hermanos;
	public GridComponent2 grid910;
	LabelComponent lblpregunta904,lblpregunta905,lblpregunta906,lblpregunta907,lblpregunta908,lblpregunta909,lblpregunta910,lblpregunta911,
	lblpregunta912,lblpregunta913,lblpregunta914,lblpregunta915,lblpregunta916,lblpregunta917,lblobs_s9;
	public ButtonComponent  btnCancelar;
	public CheckBoxField chbP910;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	LinearLayout q14,q15;
	
	public static CISECCION_09Fragment_001_2Dialog newInstance(FragmentForm pagina,CISECCION_09 detalle, int position, List<CISECCION_09> detalles) {
		caller = (CISECCION_09Fragment_001) pagina;
		hermanos=detalles;
		CISECCION_09Fragment_001_2Dialog f = new CISECCION_09Fragment_001_2Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", (CISECCION_09) detalles.get(position));
		f.setArguments(args);
		return f;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hermano = (CISECCION_09) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		getDialog().setTitle("N� "+hermano.qinro_orden);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	public CISECCION_09Fragment_001_2Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI917","QI916","QI915","QI914","QI913","QI912","QI911","QI910","QI909","QI908","QI907","QI906","QI905","QI904","QI900_OBS","QINRO_ORDEN","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI917","QI916","QI915","QI914","QI913","QI912","QI911","QI910","QI909","QI908","QI907","QI906","QI905","QI904","QI900_OBS","QINRO_ORDEN")}; 
	}            

	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		lblpregunta904= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_09qi904);
		lblpregunta905= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi905);
		lblpregunta906 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi906);
		lblpregunta907 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi907);
		lblpregunta908 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi908);
		lblpregunta909 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi909);
		lblpregunta910 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi910);
		lblpregunta911 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi911);
		lblpregunta912 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi912);
		lblpregunta913 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi913);
		lblpregunta914 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi914);
		lblpregunta915 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi915);
		lblpregunta916 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi916);
		lblpregunta917 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).text(R.string.c2seccion_08_09qi917);
		
		txtQI904 = new TextField(getActivity()).size(altoComponente, 730).maxLength(20);
		txtQI904.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().length()>0)
					RenombrarEtiquetassiCambioNombre();
				else
					RenombrarEtiquetas();
			}
		});
		rgQI905 = new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi905_1,R.string.c2seccion_08_09qi905_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("OcultarTecla");
		rgQI906=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi906_1,R.string.c2seccion_08_09qi906_2,R.string.c2seccion_08_09qi906_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi906ChangeValue"); 
		txtQI907=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqi907ChangeValue");
		
		rgQI908=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi908_1,R.string.c2seccion_08_09qi908_2,R.string.c2seccion_08_09qi908_3,R.string.c2seccion_08_09qi908_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("OcultarTecla"); 
		rgQI909=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi909_1,R.string.c2seccion_08_09qi909_2,R.string.c2seccion_08_09qi909_3,R.string.c2seccion_08_09qi909_4,R.string.c2seccion_08_09qi909_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("OcultarTecla"); 
		txtQI910=new IntegerField(this.getActivity()).size(altoComponente, 80).size(altoComponente, 100).maxLength(4).callback("onqi910ChangeValue");
		txtQI910.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length()>3)
					onqi910ChangeValue();					
			}
		});
		chbP910=new CheckBoxField(this.getActivity(), R.string.btnnosabe, "1:0").size(60, 200);
		chbP910.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
					MyUtil.LiberarMemoria();
		    		Util.cleanAndLockView(getActivity(),txtQI910);
		    		Util.lockView(getActivity(), false,txtQI911);
		    		q8.setVisibility(View.VISIBLE);
		  		}else {
		  			MyUtil.LiberarMemoria();
					Util.lockView(getActivity(), false,txtQI910);
					Util.cleanAndLockView(getActivity(),txtQI911);
					txtQI910.setVisibility(View.VISIBLE);
					q8.setVisibility(View.GONE);
					}
				}
		});
		txtQI911=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqi912ChangeValue"); 
		txtQI912=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqi912ChangeValue");
		txtQI912.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length()>1)
					onqi912ChangeValue();
			}
		});
		rgQI913=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi913_1,R.string.c2seccion_08_09qi913_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi913ChangeValue"); 
		rgQI914=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi914_1,R.string.c2seccion_08_09qi914_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi914ChangeValue"); 
		rgQI915=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi915_1,R.string.c2seccion_08_09qi915_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi915ChangeValue"); 
		rgQI916=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi916_1,R.string.c2seccion_08_09qi916_2,R.string.c2seccion_08_09qi916_3,R.string.c2seccion_08_09qi916_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqi916ChangeValue"); 
		txtQI917=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqi917ChangeValue"); 
		
		lblobs_s9    = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2obsxx);
		txtQI900_OBS = new TextAreaField(getActivity()).maxLength(1000).size(200, 600).alfanumerico();
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		grid910=new GridComponent2(getActivity(), 2);
		grid910.addComponent(chbP910);
		grid910.addComponent(txtQI910);
		btnCancelar.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
			CISECCION_09Fragment_001_2Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				CISECCION_09Fragment_001_2Dialog.this.dismiss();
				caller.recargardatos();
				/*if (caller.personas.size() > hermano.qinro_orden) {
					caller.abrirDetalleEditar(caller.personas.get(hermano.qinro_orden),hermano.qinro_orden, caller.personas,0,0);
				}*/
			}
		});   
	} 
	
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(lblpregunta904,txtQI904); 
		q2 = createQuestionSection(lblpregunta905,rgQI905); 
		q3 = createQuestionSection(lblpregunta906,rgQI906); 
		q4 = createQuestionSection(lblpregunta907,txtQI907); 
		q5 = createQuestionSection(lblpregunta908,rgQI908); 
		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta909,rgQI909); 
		q7 = createQuestionSection(lblpregunta910,grid910.component()); 
		q8 = createQuestionSection(lblpregunta911,txtQI911); 
		q9 = createQuestionSection(lblpregunta912,txtQI912); 
		q10 = createQuestionSection(lblpregunta913,rgQI913);
		q11 = createQuestionSection(lblpregunta914,rgQI914);
		q12 = createQuestionSection(lblpregunta915,rgQI915);
		q13 = createQuestionSection(lblpregunta916,rgQI916);
		q14 = createQuestionSection(lblpregunta917,txtQI917);
		q15 = createQuestionSection(lblobs_s9, txtQI900_OBS);
		
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10); 
		form.addView(q11);
		form.addView(q12);
		form.addView(q13);
		form.addView(q14);
		form.addView(q15);
		form.addView(botones);
		return contenedor;          
	}
	
	public boolean grabar(){
		uiToEntity(hermano);

		if (chbP910.isChecked())
			hermano.qi910=9998;
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesparalabasededatos();
			flag = getCuestionarioService().saveOrUpdate(hermano, seccionesGrabado);
			
			int year = Calendar.getInstance().get(Calendar.YEAR);
			if (hermano.qi906!=null && hermano.qi906==1 && hermano.qi907!=null)
				hermano.edad=hermano.qi907;
			if (hermano.qi906!=null && hermano.qi906==2 && hermano.qi912!=null && hermano.qi910!=null && hermano.qi910!=9998)
				hermano.edad=hermano.qi912+year-hermano.qi910;
			if (hermano.qi906!=null && hermano.qi906==2 && hermano.qi912!=null && hermano.qi910!=null && hermano.qi910==9998 && hermano.qi911!=null)
				hermano.edad=hermano.qi911+hermano.qi912;
           if(hermano.edad!=null){
				Integer edadMayorPersona=hermano.edad!=null?hermano.edad:-1;
				Integer ordenMayorPersona=0;
				boolean primero = true;
		    	if (hermanos.size()>0) {
		    		for (CISECCION_09 persona : hermanos) {
		    			if (persona.edad!=null && persona.edad<edadMayorPersona && primero){
		    				ordenMayorPersona=persona.qinro_orden;
		    				primero = false;
		    			}
		    				
		    		}
				}
		    	Integer edadAnterior =0;
		    	/*if (hermano.qinro_orden>1)
		    		edadAnterior = hermanos.get(hermano.qinro_orden-2).edad==null?0:hermanos.get(hermano.qinro_orden-2).edad;*/
//		    	Log.d("edad", "edad: "+ordenMayorPersona +" - "+ hermano.qinro_orden + " ant: "+edadAnterior);
		    	
				if (ordenMayorPersona==0)
					flag = getCuestionarioService().actualizarOrdenCISEc09(hermano, hermano.qinro_orden ,hermanos.size());
				else {
					//if (edadAnterior>0 ) {
						if (ordenMayorPersona < hermano.qinro_orden)
						     flag = getCuestionarioService().actualizarOrdenCISEc09(hermano, hermano.qinro_orden ,ordenMayorPersona);//+1
						if (ordenMayorPersona > hermano.qinro_orden)
						     flag = getCuestionarioService().actualizarOrdenCISEc09(hermano, hermano.qinro_orden ,ordenMayorPersona-1);
					//}
				}
		    }
			
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	public void cambiarVariablesparalabasededatos(){
		hermano.qi906=hermano.qi906!=null?hermano.setConvertcontresvariables(hermano.qi906):null;
		hermano.qi908=hermano.qi908!=null?hermano.setConvertconcuatrovariables(hermano.qi908):null;
		hermano.qi909=hermano.qi909!=null?hermano.setConvertconcuatrovariables(hermano.qi909):null;
//		hermano.qi909=hermano.qi909!=null?hermano.setConvertconcincovariables(hermano.qi909):null; correcto
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (hermano.qi904  == null) {
			error = true;
			view = txtQI904;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI904");
			return false;
		}
		if (hermano.qi905  == null) {
			error = true;
			view = rgQI905;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI905");
			return false;
		}
		if (hermano.qi906  == null) {
			error = true;
			view = rgQI906;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI906");
			return false;
		}
		if (!Util.esDiferente(hermano.qi906,1)) {
			if (hermano.qi907  == null) {
			error = true;
			view = txtQI907;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI907");
			return false;
			}
		}
		if (!Util.esDiferente(hermano.qi906,2)) {
			if (hermano.qi908  == null) {
				error = true;
				view = rgQI908;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI908");
				return false;
			}
			if (hermano.qi909  == null) {
				error = true;
				view = rgQI909;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI909");
				return false;
			}
			if (hermano.qi910  == null) {
				error = true;
				view = txtQI910;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI910");
				return false;
			}
			if (hermano.qi910!=null && !chbP910.isChecked() && !(hermano.qi910>=1800 && hermano.qi910<=App.ANIOPORDEFECTOSUPERIOR)) {
				error = true;
				view = txtQI910;
				mensaje = "Valor fuera de Rango";
				return false;
			}
			if (!Util.esDiferente(hermano.qi910,9998)) {
				if (hermano.qi911  == null) {
					error = true;
					view = txtQI911;
					mensaje = preguntaVacia.replace("$", "La pregunta P.QI911");
					return false;
				}
			}
			
			if (hermano.qi912  == null) {
				error = true;
				view = txtQI912;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QI912");
				return false;
			}
			if(Util.esMayor(hermano.qi912,11) && !Util.esDiferente(hermano.qi905, 2)){
				if (hermano.qi913  == null) {
					error = true;
					view = rgQI913;
					mensaje = preguntaVacia.replace("$", "La pregunta P.QI913");
					return false;
				}
				if(!Util.esDiferente(hermano.qi913, 2)){
					if (hermano.qi914  == null) {
						error = true;
						view = rgQI914;
						mensaje = preguntaVacia.replace("$", "La pregunta P.QI914");
						return false;
					}
					if(!Util.esDiferente(hermano.qi914, 2)){
						if (hermano.qi915  == null) {
							error = true;
							view = rgQI915;
							mensaje = preguntaVacia.replace("$", "La pregunta P.QI915");
							return false;
						}
					}
				}
				if(!Util.esDiferente(hermano.qi914, 2)	&& !Util.esDiferente(hermano.qi915, 2)){
					if (hermano.qi916  == null) {
						error = true;
						view = rgQI916;
						mensaje = preguntaVacia.replace("$", "La pregunta P.QI916");
						return false;
					}
				}
				if (hermano.qi917  == null) {
					error = true;
					view = txtQI917;
					mensaje = preguntaVacia.replace("$", "La pregunta P.QI917");
					return false;
				}
			}
		}
	    return true;
	}
	private void cargarDatos() {      
		hermano = getCuestionarioService().getMortalidadMaterna(hermano.id,hermano.hogar_id,hermano.persona_id,hermano.qinro_orden,seccionesCargado);
		if (hermano == null) {
			hermano = new CISECCION_09();
			hermano.id = App.getInstance().getMarco().id;
			hermano.hogar_id =App.getInstance().getHogar().hogar_id;
			hermano.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			hermano.qinro_orden = hermano.qinro_orden;
		}
		cambiarvariablesparamostrarausuario();
		nombre=hermano.qi904;
		entityToUI(hermano);
		if(hermano.qi910!=null && hermano.qi910==9998){
			chbP910.setChecked(true);
		}
		else{
		chbP910.setChecked(false);	
		}
		inicio();
       	}
	private void cambiarvariablesparamostrarausuario(){
		hermano.qi906=hermano.qi906!=null?hermano.getConvertcontresvariables(hermano.qi906):null;
		hermano.qi908=hermano.qi908!=null?hermano.getConvertconcuatrovariables(hermano.qi908):null;
		hermano.qi909=hermano.qi909!=null?hermano.getConvertconcuatrovariables(hermano.qi909):null;
//		hermano.qi909=hermano.qi909!=null?hermano.getConvertconcincovariables(hermano.qi909):null; CORRECTO
	}
	
	public void onqi911ChangeValue(){
		OcultarTecla();
	}
	public void onqi916ChangeValue(){
		OcultarTecla();
		txtQI917.requestFocus();
	}
	public void onqi917ChangeValue(){
		MyUtil.LiberarMemoria();
		OcultarTecla();
		btnAceptar.requestFocus();
	}
	
	
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQI905.getWindowToken(), 0);
	}
	
	public void inicio(){
		MyUtil.LiberarMemoria();
		RenombrarEtiquetas();
		onqi906ChangeValue();
		ValidarsiesSupervisora();
		rgQI905.requestFocus();
	}
	public void EstadoOriginalDeEqtiquetas(){
		lblpregunta904.setText(getResources().getString(R.string.c2seccion_09qi904));
		lblpregunta905.setText(getResources().getString(R.string.c2seccion_08_09qi905));
		lblpregunta906.setText(getResources().getString(R.string.c2seccion_08_09qi906));
		lblpregunta907.setText(getResources().getString(R.string.c2seccion_08_09qi907));
		lblpregunta908.setText(getResources().getString(R.string.c2seccion_08_09qi908));
		lblpregunta909.setText(getResources().getString(R.string.c2seccion_08_09qi909));
		lblpregunta910.setText(getResources().getString(R.string.c2seccion_08_09qi910));
		lblpregunta911.setText(getResources().getString(R.string.c2seccion_08_09qi911));
		lblpregunta912.setText(getResources().getString(R.string.c2seccion_08_09qi912));
		lblpregunta913.setText(getResources().getString(R.string.c2seccion_08_09qi913));
		lblpregunta914.setText(getResources().getString(R.string.c2seccion_08_09qi914));
		lblpregunta915.setText(getResources().getString(R.string.c2seccion_08_09qi915));
		lblpregunta916.setText(getResources().getString(R.string.c2seccion_08_09qi916));
		lblpregunta917.setText(getResources().getString(R.string.c2seccion_08_09qi917));
	}
	public void RenombrarEtiquetas(){
		String replace ="(NOMBRE)";
		EstadoOriginalDeEqtiquetas();
		lblpregunta905.setText(lblpregunta905.getText().toString().replace(replace, nombre));
		lblpregunta906.setText(lblpregunta906.getText().toString().replace(replace, nombre));
		lblpregunta907.setText(lblpregunta907.getText().toString().replace(replace, nombre));
		lblpregunta908.setText(lblpregunta908.getText().toString().replace(replace, nombre));
		lblpregunta909.setText(lblpregunta909.getText().toString().replace(replace, nombre));
		lblpregunta910.setText(lblpregunta910.getText().toString().replace(replace, nombre));
		lblpregunta911.setText(lblpregunta911.getText().toString().replace(replace, nombre));
		lblpregunta912.setText(lblpregunta912.getText().toString().replace(replace, nombre));
		lblpregunta913.setText(lblpregunta913.getText().toString().replace(replace, nombre));
		lblpregunta914.setText(lblpregunta914.getText().toString().replace(replace, nombre));
		lblpregunta915.setText(lblpregunta915.getText().toString().replace(replace, nombre));
		lblpregunta916.setText(lblpregunta916.getText().toString().replace(replace, nombre));
		lblpregunta917.setText(lblpregunta917.getText().toString().replace(replace, nombre));
	}
	public void RenombrarEtiquetassiCambioNombre(){
		if(txtQI904.getText().toString().length()>0){
		String replace =nombre;
		String nombre = txtQI904.getText().toString();
		EstadoOriginalDeEqtiquetas();
		RenombrarEtiquetas();
		lblpregunta905.setText(lblpregunta905.getText().toString().replace(replace, nombre));
		lblpregunta906.setText(lblpregunta906.getText().toString().replace(replace, nombre));
		lblpregunta907.setText(lblpregunta907.getText().toString().replace(replace, nombre));
		lblpregunta908.setText(lblpregunta908.getText().toString().replace(replace, nombre));
		lblpregunta909.setText(lblpregunta909.getText().toString().replace(replace, nombre));
		lblpregunta910.setText(lblpregunta910.getText().toString().replace(replace, nombre));
		lblpregunta911.setText(lblpregunta911.getText().toString().replace(replace, nombre));
		lblpregunta912.setText(lblpregunta912.getText().toString().replace(replace, nombre));
		lblpregunta913.setText(lblpregunta913.getText().toString().replace(replace, nombre));
		lblpregunta914.setText(lblpregunta914.getText().toString().replace(replace, nombre));
		lblpregunta915.setText(lblpregunta915.getText().toString().replace(replace, nombre));
		lblpregunta916.setText(lblpregunta916.getText().toString().replace(replace, nombre));
		lblpregunta917.setText(lblpregunta917.getText().toString().replace(replace, nombre));
		}
	}
	public void onqi906ChangeValue(){
		Integer valor = Integer.parseInt(rgQI906.getTagSelected("0").toString());
		if(!Util.esDiferente(valor, 2)){
			Util.cleanAndLockView(getActivity(),txtQI907);
			q4.setVisibility(View.GONE);
			Util.lockView(getActivity(), false,chbP910, rgQI908,rgQI909,txtQI910,txtQI912);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			onqi910ChangeValue();
			onqi912ChangeValue();
			rgQI908.requestFocus();
		}
		if(!Util.esDiferente(valor, 1)){
			Util.lockView(getActivity(), false,chbP910, txtQI907,rgQI908,rgQI909,txtQI910,txtQI911,txtQI912,rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			q14.setVisibility(View.VISIBLE);
			onqi907ChangeValue();
			txtQI907.requestFocus();
		}
		if(!Util.esDiferente(valor, 3)){
			Util.cleanAndLockView(getActivity(),chbP910, txtQI907,rgQI908,rgQI909,txtQI910,txtQI911,txtQI912,rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
			q14.setVisibility(View.GONE);
		}
		onch910ChangeValue();
		OcultarTecla();
	}
	public void onqi907ChangeValue(){
		Integer edad = txtQI907.getText().toString().length()>0?Integer.parseInt(txtQI907.getText().toString()):-1;
		if(Util.esDiferente(edad, -1)){
			Util.cleanAndLockView(getActivity(),chbP910, rgQI908,rgQI909,txtQI910,txtQI911,txtQI912,rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
			q14.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false, chbP910,rgQI908,rgQI909,txtQI910,txtQI911,txtQI912,rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			q14.setVisibility(View.VISIBLE);
			onqi910ChangeValue();
			onqi912ChangeValue();
		}
		OcultarTecla();
	}
	public void onqi910ChangeValue(){
		Integer anio = txtQI910.getText().toString().length()>0?Integer.parseInt(txtQI910.getText().toString()):-1;
		if(Util.esDiferente(anio, -1) && Util.esDiferente(anio, 9998)){
			Util.cleanAndLockView(getActivity(), txtQI911);
			q8.setVisibility(View.GONE);
			txtQI912.requestFocus();
		}
		else{
			Util.lockView(getActivity(), false,txtQI911);
			q8.setVisibility(View.VISIBLE);
		}
		OcultarTecla();
	}
	public void onqi912ChangeValue(){
		Integer edadtenia = txtQI912.getText().toString().trim().length()>0?Integer.parseInt(txtQI912.getText().toString()):-1;
		Integer sexo=Integer.parseInt(rgQI905.getTagSelected("0").toString());
				
		if(edadtenia<12 || !Util.esDiferente(sexo, 1)){
			Util.cleanAndLockView(getActivity(), rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
			q14.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,rgQI913,rgQI914,rgQI915,rgQI916,txtQI917);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			q14.setVisibility(View.VISIBLE);
			onqi913ChangeValue();
		}
		OcultarTecla();
	}
	public void onqi913ChangeValue(){
		Integer dato= Integer.parseInt(rgQI913.getTagSelected("0").toString());
		if(!Util.esDiferente(dato,1)){
//			Log.e("11","00");
			Util.cleanAndLockView(getActivity(), rgQI914,rgQI915);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
		}
		else{
//			Log.e("11","000");
			Util.lockView(getActivity(), false,rgQI914,rgQI915);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			onqi914ChangeValue();
		}
		OcultarTecla();
	}
	public void onqi914ChangeValue(){
		Integer dato= Integer.parseInt(rgQI914.getTagSelected("0").toString());
		Integer dato13= Integer.parseInt(rgQI913.getTagSelected("0").toString());
		if(!Util.esDiferente(dato,1) && !Util.esDiferente(dato13, 2)){
//			Log.e("11","11");
			Util.cleanAndLockView(getActivity(),rgQI915);
			q12.setVisibility(View.GONE);
			
			Util.cleanAndLockView(getActivity(),rgQI916);
			q13.setVisibility(View.GONE);
			
		}
		else{
//			Log.e("11","22");
			Util.lockView(getActivity(), false,rgQI915);
			q12.setVisibility(View.VISIBLE);
			Util.lockView(getActivity(), false,rgQI916);
			q13.setVisibility(View.VISIBLE);
			onqi915ChangeValue();
		}
		OcultarTecla();
	}
	public void onqi915ChangeValue(){
		Integer dato= Integer.parseInt(rgQI915.getTagSelected("0").toString());
		Integer dato14= Integer.parseInt(rgQI914.getTagSelected("0").toString());
		if(!Util.esDiferente(dato,1) && !Util.esDiferente(dato14, 2)){
			Util.cleanAndLockView(getActivity(),rgQI916);
//			Log.e("11","33");
			q13.setVisibility(View.GONE);
		}
		else{
//			Log.e("11","44");
			Util.lockView(getActivity(), false,rgQI916);
			q13.setVisibility(View.VISIBLE);
		}
		onch910ChangeValue();
		OcultarTecla();
	}
	public void onch910ChangeValue(){
		if(chbP910.isChecked()){
//			Log.e("11","55");
			Util.cleanAndLockView(getActivity(),txtQI910);
			txtQI910.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,txtQI910);
			txtQI910.setVisibility(View.VISIBLE);
		}
//		if (hermano.qi910!=null && hermano.qi910==9998) {
//			chbP910.setChecked(true);
//			Util.cleanAndLockView(getActivity(),txtQI910);
//			txtQI910.setVisibility(View.GONE);
//		}
//		else{
//			chbP910.setChecked(false);
//		}
	}
	 public void ValidarsiesSupervisora(){
	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		txtQI904.readOnly(); 
	    		rgQI905.readOnly(); 
	    		rgQI906.readOnly(); 
	    		txtQI907.readOnly(); 
	    		rgQI908.readOnly(); 
	    		rgQI909.readOnly(); 
	    		txtQI910.readOnly();
	    		txtQI911.readOnly(); 
	    		txtQI912.readOnly(); 
	    		rgQI913.readOnly(); 
	    		rgQI914.readOnly(); 
	    		rgQI915.readOnly(); 
	    		rgQI916.readOnly(); 
	    		txtQI917.readOnly();
	    		btnAceptar.setEnabled(false);
	    	}
	    }
	    
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
}