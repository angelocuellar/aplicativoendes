package gob.inei.endes2024.fragment.CIseccion_08_09.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_08_09.CISECCION_09Fragment_001;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_09Fragment_001_1Dialog  extends DialogFragmentComponent {
	@FieldAnnotation(orderIndex=1) 
	public TextField txtQI904; 
	@FieldAnnotation(orderIndex=2) 
	public ButtonComponent btnAceptar;

	public static CISECCION_09Fragment_001 caller;
	CISECCION_09 hermano;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	
	public GridComponent2 gridpregunta4789;
	public ButtonComponent  btnCancelar;
	public LabelComponent lblpregunta01,lblpregunta02;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	
	public static CISECCION_09Fragment_001_1Dialog newInstance(FragmentForm pagina, CISECCION_09 detalle) {
		caller = (CISECCION_09Fragment_001) pagina;
		CISECCION_09Fragment_001_1Dialog f = new CISECCION_09Fragment_001_1Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hermano = (CISECCION_09) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		getDialog().setTitle("N� "+hermano.qinro_orden);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	public CISECCION_09Fragment_001_1Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI904","QINRO_ORDEN","ID","HOGAR_ID","PERSONA_ID") };
		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"QI904","QINRO_ORDEN")}; 
	}            
	
	@Override
	protected View createUI() {
		buildFields();
		q2 = createQuestionSection(lblpregunta01,lblpregunta02,txtQI904); 
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q2);
		form.addView(botones);
		return contenedor;          
	}
	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		txtQI904 = new TextField(getActivity()).size(altoComponente, 290).maxLength(20);
		lblpregunta01= new LabelComponent(getActivity()).size(altoComponente+10,WRAP_CONTENT).text(R.string.c2seccion_08_09qi904).textSize(19).negrita();
		lblpregunta02= new LabelComponent(getActivity()).size(altoComponente,WRAP_CONTENT).text(R.string.c2seccion_08_09qi904_1).textSize(19).negrita();
		btnCancelar.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
			CISECCION_09Fragment_001_1Dialog.this.dismiss();
			Util.lockView(getActivity(), false,caller.btnAgregar);
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = false;
				flag=grabar();
				if(!flag){
					return;
				}
				caller.refrescarPersonas(hermano);
				CISECCION_09Fragment_001_1Dialog.this.dismiss();
				if(Integer.valueOf(caller.txtQI901.getValue().toString())-1>caller.personas.size()){
				caller.agregarHermano();
				}
			}
		});   
	} 
	public boolean grabar(){
		uiToEntity(hermano);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			flag = getCuestionarioService().saveOrUpdate(hermano, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (hermano.qi904  == null) {
			error = true;
			view = txtQI904;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QI904");
			return false;
		}		
	    return true;
	}
	private void cargarDatos() {        
		hermano = getCuestionarioService().getMortalidadMaterna(hermano.id,hermano.hogar_id,hermano.persona_id,hermano.qinro_orden,seccionesCargado);
		if (hermano == null) {
			hermano = new CISECCION_09();
			hermano.id = App.getInstance().getMarco().id;
			hermano.hogar_id =App.getInstance().getHogar().hogar_id;
			hermano.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			hermano.qinro_orden= caller.personas.size()+1;
		}
		entityToUI(hermano);
		inicio();
       	}
	public void inicio(){
		EsconderTextoSegunCorresponda();
	}
	public void EsconderTextoSegunCorresponda(){
		if(!Util.esDiferente(hermano.qinro_orden, 1)){
			lblpregunta02.setVisibility(View.GONE);
			lblpregunta01.setVisibility(View.VISIBLE);
		}
		else{
			lblpregunta02.setVisibility(View.VISIBLE);
			lblpregunta01.setVisibility(View.GONE);
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

}
