package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI802F; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI802G; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI802I;	
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI803; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtNUEVO; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI803A_A; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI803A_B; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI803A_C; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI803A_D; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI803A_E; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI803A_F; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI803A_G; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI803A_H; 
	@FieldAnnotation(orderIndex=14) 
	public TextField txtQI803A_HX; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI803A_I; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI803A_J; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI803A_K; 
	@FieldAnnotation(orderIndex=18) 
	public TextField txtQI803A_KX; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI803A_L; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI803A_M; 
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI803A_X; 
	@FieldAnnotation(orderIndex=22) 
	public TextField txtQI803AXX; 

	
	CISECCION_08 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblSP,lblSPR,lblONG,lblpregunta802F,lblpregunta802G,lblpregunta802I,lblpregunta803,lblpregunta803A,lblpregunta803A_ind,lblpregunta803A_rep,lblpregunta803A_nom,lblpregunta803A_ind2;
	public TextField txtCabecera;
	GridComponent2 gridPreguntas803A;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public CISECCION_08Fragment_002() {} 
	public CISECCION_08Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI802F","QI802G","QI802I","QI803","NUEVO","QI803A_A","QI803A_B","QI803A_C","QI803A_D","QI803A_E","QI803A_F","QI803A_G","QI803A_H","QI803A_HX","QI803A_I","QI803A_J","QI803A_K","QI803A_KX","QI803A_L","QI803A_M","QI803A_X","QI803AXX","QI801A","QI801B","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI802F","QI802G","QI802I","QI803","NUEVO","QI803A_A","QI803A_B","QI803A_C","QI803A_D","QI803A_E","QI803A_F","QI803A_G","QI803A_H","QI803A_HX","QI803A_I","QI803A_J","QI803A_K","QI803A_KX","QI803A_L","QI803A_M","QI803A_X","QI803AXX")}; 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita();
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQI802F=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802f_1,R.string.c2seccion_08_09qi802f_2,R.string.c2seccion_08_09qi802f_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802g_1,R.string.c2seccion_08_09qi802g_2,R.string.c2seccion_08_09qi802g_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802i_1,R.string.c2seccion_08_09qi802i_2,R.string.c2seccion_08_09qi802i_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
  	  	rgQI803=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi803_1,R.string.c2seccion_08_09qi803_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS803ChangeValue");
  	  	txtNUEVO=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 500).alfanumerico();
		
		lblSP  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi803a_t1).textSize(16).negrita();
		chbQI803A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_a, "1:0").size(WRAP_CONTENT, 300); 
		chbQI803A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_b, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_c, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_d, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_e, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_f, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_g, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_h, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426HChangeValue");
		txtQI803A_HX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		lblSPR  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi803a_t2).textSize(16).negrita();
		chbQI803A_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_i, "1:0").size(WRAP_CONTENT, 300);		
		chbQI803A_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_j, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_k, "1:0").size(WRAP_CONTENT, 300).callback("onQI803AKChangeValue");
		txtQI803A_KX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		lblONG  = new LabelComponent(this.getActivity()).size(WRAP_CONTENT, 300).text(R.string.c2seccion_08_09qi803a_t3).textSize(16).negrita();		
		chbQI803A_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_l, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_m, "1:0").size(WRAP_CONTENT, 300);
		chbQI803A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_08_09qi803a_x, "1:0").size(WRAP_CONTENT, 300).callback("onqrgQI426XChangeValue");
		txtQI803AXX=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		
		lblpregunta802F = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802f);
		lblpregunta802G = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802g);
		lblpregunta802I = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802i);
		lblpregunta803 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi803);
		lblpregunta803A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi803a);
		lblpregunta803A_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi803a_ind);
		lblpregunta803A_nom = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi803a_nom).centrar();
		lblpregunta803A_rep = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi803a_rep);
		lblpregunta803A_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_08_09qi803a_ind2);
		
		
		gridPreguntas803A = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridPreguntas803A.addComponent(lblSP,2);
		gridPreguntas803A.addComponent(chbQI803A_A,2);
		gridPreguntas803A.addComponent(chbQI803A_B,2);
		gridPreguntas803A.addComponent(chbQI803A_C,2);
		gridPreguntas803A.addComponent(chbQI803A_D,2);
		gridPreguntas803A.addComponent(chbQI803A_E,2);
		gridPreguntas803A.addComponent(chbQI803A_F,2);
		gridPreguntas803A.addComponent(chbQI803A_G,2);
		gridPreguntas803A.addComponent(chbQI803A_H);
		gridPreguntas803A.addComponent(txtQI803A_HX);
		gridPreguntas803A.addComponent(lblSPR,2);
		gridPreguntas803A.addComponent(chbQI803A_I,2);
		gridPreguntas803A.addComponent(chbQI803A_J,2);
		gridPreguntas803A.addComponent(chbQI803A_K);
		gridPreguntas803A.addComponent(txtQI803A_KX);
		gridPreguntas803A.addComponent(lblONG,2);
		gridPreguntas803A.addComponent(chbQI803A_L,2);
		gridPreguntas803A.addComponent(chbQI803A_M,2);
		gridPreguntas803A.addComponent(chbQI803A_X);
		gridPreguntas803A.addComponent(txtQI803AXX);
  
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta802F,rgQI802F); 
		q2 = createQuestionSection(lblpregunta802G,rgQI802G); 
		q3 = createQuestionSection(lblpregunta802I,rgQI802I); 
		q4 = createQuestionSection(lblpregunta803,rgQI803); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta803A,lblpregunta803A_ind,txtNUEVO,lblpregunta803A_nom,lblpregunta803A_rep,lblpregunta803A_ind2,gridPreguntas803A.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(Util.esDiferente(individual.qi801a,2) ||  Util.esDiferente(individual.qi801b, 2)){
		if (Util.esVacio(individual.qi802f)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI802F"); 
			view = rgQI802F; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi802g)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI802G"); 
			view = rgQI802G; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi802i)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI802H"); 
			view = rgQI802I; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi803)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI803"); 
			view = rgQI803; 
			error = true; 
			return false; 
		} 
		if (!Util.esDiferente(individual.qi803,1)) {
			if (!Util.alMenosUnoEsDiferenteA(0,individual.qi803a_a,individual.qi803a_b,individual.qi803a_c,individual.qi803a_d,individual.qi803a_e,individual.qi803a_f,individual.qi803a_g,individual.qi803a_h,individual.qi803a_i,individual.qi803a_j,individual.qi803a_k,individual.qi803a_l,individual.qi803a_m,individual.qi803a_x)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQI803A_A; 
				error = true; 
				return false; 
			}
			if (chbQI803A_H.isChecked()) {
				if (Util.esVacio(individual.qi803a_hx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI803A_HX; 
					error = true; 
					return false; 
				}
			}
			if (chbQI803A_K.isChecked()) {
				if (Util.esVacio(individual.qi803a_kx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI803A_KX; 
					error = true; 
					return false; 
				}
			}
			if (chbQI803A_X.isChecked()) {
				if (Util.esVacio(individual.qi803axx)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI803AXX; 
					error = true; 
					return false; 
				}
			}
		} 
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	if (individual == null) {
    		individual = new CISECCION_08();
    		individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
    		individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
    		individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
    	}
    	entityToUI(individual); 
    	inicio();
    } 
    
    public void validarPregunta801ab(){
    	 Integer in801a= individual.qi801a==null?0:individual.qi801a;
    	 Integer in801b= individual.qi801b==null?0:individual.qi801b;
    	
    	if(in801a==2 && in801b==2){
    		Util.cleanAndLockView(getActivity(), rgQI802F,rgQI802G,rgQI802I,rgQI803,txtNUEVO,chbQI803A_A,chbQI803A_B,chbQI803A_C,chbQI803A_D,chbQI803A_E,chbQI803A_F,chbQI803A_G,chbQI803A_H,chbQI803A_I,chbQI803A_J,chbQI803A_K,chbQI803A_L,chbQI803A_M,chbQI803A_X);
    		q0.setVisibility(View.GONE);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI802F,rgQI802G,rgQI802I,rgQI803,txtNUEVO,chbQI803A_A,chbQI803A_B,chbQI803A_C,chbQI803A_D,chbQI803A_E,chbQI803A_F,chbQI803A_G,chbQI803A_H,chbQI803A_I,chbQI803A_J,chbQI803A_K,chbQI803A_L,chbQI803A_M,chbQI803A_X);
    		q0.setVisibility(View.VISIBLE);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
        	onqrgQS803ChangeValue();
        	onqrgQI426HChangeValue();
        	onQI803AKChangeValue();
        	onqrgQI426XChangeValue();
    	}
    }
    
    private void inicio() { 
    	validarPregunta801ab();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }

    public void onqrgQS803ChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI803.getTagSelected("").toString())) {
    		Util.cleanAndLockView(getActivity(),txtNUEVO,chbQI803A_A,chbQI803A_B,chbQI803A_C,chbQI803A_D,chbQI803A_E,chbQI803A_F,chbQI803A_G,chbQI803A_H,chbQI803A_I,chbQI803A_J,chbQI803A_K,chbQI803A_L,chbQI803A_M,chbQI803A_X);  		
    		MyUtil.LiberarMemoria();
    		q5.setVisibility(View.GONE);
    	} 
    	else {
    		MyUtil.LiberarMemoria();
    		Util.lockView(getActivity(),false,txtNUEVO,chbQI803A_A,chbQI803A_B,chbQI803A_C,chbQI803A_D,chbQI803A_E,chbQI803A_F,chbQI803A_G,chbQI803A_H,chbQI803A_I,chbQI803A_J,chbQI803A_K,chbQI803A_L,chbQI803A_M,chbQI803A_X); 
    		q5.setVisibility(View.VISIBLE);
    		chbQI803A_A.requestFocus(); 
    	}
    }
    public void onqrgQI426HChangeValue(){
    	if(chbQI803A_H.isChecked()){
    		Util.lockView(getActivity(), false,txtQI803A_HX);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI803A_HX);
    	}
    }
    public void onQI803AKChangeValue(){
    	if(chbQI803A_K.isChecked()){
    		Util.lockView(getActivity(), false,txtQI803A_KX);
    	}
    	else{
    		Util.cleanAndLockView(getActivity(), txtQI803A_KX);
    	}
    }
	public void onqrgQI426XChangeValue(){
		if(chbQI803A_X.isChecked()){
			Util.lockView(getActivity(), false,txtQI803AXX);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQI803AXX);
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI802F.readOnly();
			rgQI802G.readOnly();
			rgQI802I.readOnly();
			rgQI803.readOnly();
			txtNUEVO.readOnly();
			txtQI803A_HX.readOnly();
			txtQI803A_KX.readOnly();
			txtQI803AXX.readOnly();
			chbQI803A_A.readOnly();
			chbQI803A_B.readOnly();
			chbQI803A_C.readOnly();
			chbQI803A_D.readOnly();
			chbQI803A_E.readOnly();
			chbQI803A_F.readOnly();
			chbQI803A_G.readOnly();
			chbQI803A_H.readOnly();
			chbQI803A_I.readOnly();
			chbQI803A_J.readOnly();
			chbQI803A_K.readOnly();
			chbQI803A_L.readOnly();
			chbQI803A_M.readOnly();
			chbQI803A_X.readOnly();
		}
	}

    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
