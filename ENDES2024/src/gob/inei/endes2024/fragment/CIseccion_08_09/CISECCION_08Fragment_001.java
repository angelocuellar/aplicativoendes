package gob.inei.endes2024.fragment.CIseccion_08_09; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CISECCION_08Fragment_001 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI801A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI801B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI802; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI802A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI802B; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI802C; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI802D; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI802E; 

	CISECCION_08 individual;
	CISECCION_04A individualS4A;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblvih,lblsida,lblpregunta801,lblpregunta802,lblpregunta802A,lblpregunta802B,lblpregunta802C,lblpregunta802D,lblpregunta802E; 
	public TextField txtCabecera;
	private GridComponent2 gridPreguntas801;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoS4A; 

	public CISECCION_08Fragment_001() {} 
	public CISECCION_08Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI801A","QI801B","QI802","QI802A","QI802B","QI802C","QI802D","QI802E","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI801A","QI801B","QI802","QI802A","QI802B","QI802C","QI802D","QI802E")};
		seccionesCargadoS4A = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI411_H")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_08_09).textSize(21).centrar().negrita(); 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	  	lblvih = new LabelComponent(this.getActivity()).size(altoComponente, 380).text(R.string.cap8_qi801_vih).textSize(19).alinearIzquierda();
	  	lblsida = new LabelComponent(this.getActivity()).size(altoComponente,380).text(R.string.cap8_qi801_sida).textSize(19).alinearIzquierda();
	  	rgQI801A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi801a_1,R.string.c2seccion_08_09qi801a_2).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS801AChangeValue").centrar(); 
		rgQI801B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi801b_1,R.string.c2seccion_08_09qi801b_2).size(altoComponente,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQS801AChangeValue").centrar(); 
		rgQI802=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802_1,R.string.c2seccion_08_09qi802_2,R.string.c2seccion_08_09qi802_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802a_1,R.string.c2seccion_08_09qi802a_2,R.string.c2seccion_08_09qi802a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802b_1,R.string.c2seccion_08_09qi802b_2,R.string.c2seccion_08_09qi802b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802c_1,R.string.c2seccion_08_09qi802c_2,R.string.c2seccion_08_09qi802c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802d_1,R.string.c2seccion_08_09qi802d_2,R.string.c2seccion_08_09qi802d_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI802E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi802e_1,R.string.c2seccion_08_09qi802e_2,R.string.c2seccion_08_09qi802e_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		lblpregunta801 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi801);
		lblpregunta802 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802);
		lblpregunta802A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802a);
		lblpregunta802B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802b);
		lblpregunta802C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802c);
		lblpregunta802D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802d);
		lblpregunta802E = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_08_09qi802e);
		
		gridPreguntas801 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridPreguntas801.addComponent(lblvih);		
		gridPreguntas801.addComponent(rgQI801A);
		gridPreguntas801.addComponent(lblsida);
		gridPreguntas801.addComponent(rgQI801B);
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo,txtCabecera); 
		q1 = createQuestionSection(lblpregunta801,gridPreguntas801.component()); 
		q2 = createQuestionSection(lblpregunta802,rgQI802); 
		q3 = createQuestionSection(lblpregunta802A,rgQI802A); 
		q4 = createQuestionSection(lblpregunta802B,rgQI802B); 
		q5 = createQuestionSection(lblpregunta802C,rgQI802C); 
		q6 = createQuestionSection(lblpregunta802D,rgQI802D); 
		q7 = createQuestionSection(lblpregunta802E,rgQI802E); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(individual); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {	
//				Log.e("","INFORMANTE: "+App.getInstance().getPersonaSeccion01());
//				Log.e("","INFORMANTE: SALUD "+App.getInstance().getPersonaSeccion01().persona_id);
				if(App.getInstance().getPersonaSeccion01()!=null && !Util.esDiferente(App.getInstance().getPersonaSeccion01().persona_id,individual.persona_id)){
						SeccionCapitulo[] seccionesGrabadoSalud = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QS29A" , "QS29B") };
						Salud individualSalud = new Salud();
						individualSalud.id= individual.id;
						individualSalud.hogar_id= individual.hogar_id;
						individualSalud.persona_id= individual.persona_id;
						individualSalud.qs29a= individual.qi801a;
						individualSalud.qs29b= individual.qi801b;
						getCuestionarioService().saveOrUpdate(individualSalud,seccionesGrabadoSalud);
//						if(App.getInstance().getSaludNavegacionS1_3()!=null && individual)
						CISECCION_04B2 individual4b2; 
						SeccionCapitulo[] SeccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI489","ID","HOGAR_ID","PERSONA_ID")};
						individual4b2 = getCuestionarioService().getCISECCION_04B2(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,SeccionesCargadoSeccion04);
			
						if( !((individual4b2.qi489!=null && individual4b2.qi489==1) || 
								(individual.qi801a!=null && individual.qi801a==1) || 
								(individual.qi801b!=null && individual.qi801b==1))) {
							CAP04_07 cap04_07 = new CAP04_07(); 
							cap04_07.id= individual.id;
							cap04_07.hogar_id= individual.hogar_id;
							cap04_07.persona_id= individual.persona_id;
							SeccionCapitulo[] seccionesGrabado4_7 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS601A","QS601B","QS603","QS604","QS606","QS607","QS608","QS609","QS610","QS611","QSOBS_S6")};
							getCuestionarioService().saveOrUpdate(cap04_07,seccionesGrabado4_7);
						}
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi801a = individual.qi801a;
			App.getInstance().getCiseccion_08().qi801b = individual.qi801b;
			App.getInstance().getPersonaCuestionarioIndividual().estadoCompleto5y7 = getCuestionarioService().getCompletadoSeccion0507CI(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, individual.persona_id);
		}
		else 
			App.getInstance().setCiseccion_08(individual);
		
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(individual.qi801a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI801A"); 
			view = rgQI801A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(individual.qi801b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI801B"); 
			view = rgQI801B; 
			error = true; 
			return false; 
		} 
		
		Integer in411h=0;
		if(individualS4A!=null)
			in411h= individualS4A.qi411_h==null?0:individualS4A.qi411_h;
		
	       
		if (in411h==1 && !Util.esDiferente(individual.qi801a,2) && !Util.esDiferente(individual.qi801b,2) ) {
			validarMensaje("Verficar Pregunta QI411_H");
		}
		
		
		if (!Util.esDiferente(individual.qi801a,1) || !Util.esDiferente(individual.qi801b,1)) {
			if (Util.esVacio(individual.qi802)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802"); 
				view = rgQI802; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi802a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802A"); 
				view = rgQI802A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi802b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802B"); 
				view = rgQI802B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi802c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802C"); 
				view = rgQI802C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi802d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802D"); 
				view = rgQI802D; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(individual.qi802e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI802E"); 
				view = rgQI802E; 
				error = true; 
				return false; 
			} 
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	individual = getCuestionarioService().getCISECCION_08(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargado);
    	individualS4A = getCuestionarioService().getCISECCION_04A411H(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id,seccionesCargadoS4A);
    	
    	if(App.getInstance().getCiseccion_08()!=null && individual!=null){
    	App.getInstance().getCiseccion_08().qi801a=individual.qi801a;
    	App.getInstance().getCiseccion_08().qi801b=individual.qi801b;
    	}
    	
    	if (individual == null) {
			individual = new CISECCION_08();
			individual.id = App.getInstance().getPersonaCuestionarioIndividual().id; 
			individual.hogar_id = App.getInstance().getPersonaCuestionarioIndividual().hogar_id;
			individual.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
		}
   
    	entityToUI(individual); 
		inicio(); 
    } 
    
    private void inicio() { 
    	onqrgQS801AChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void onqrgQS801AChangeValue() {
    	if (MyUtil.incluyeRango(2,2,rgQI801A.getTagSelected("").toString()) && MyUtil.incluyeRango(2,2,rgQI801B.getTagSelected("").toString())) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI802,rgQI802A,rgQI802B,rgQI802C,rgQI802D,rgQI802D,rgQI802E);  		
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI802,rgQI802A,rgQI802B,rgQI802C,rgQI802D,rgQI802D,rgQI802E); 
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE); 
    		rgQI802.requestFocus(); 
    	}
    }
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI801A.readOnly();
    		rgQI801B.readOnly();
    		rgQI802.readOnly();
    		rgQI802A.readOnly();
    		rgQI802B.readOnly();
    		rgQI802C.readOnly();
    		rgQI802D.readOnly();
    		rgQI802E.readOnly();
    	}
    }
    
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(individual); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(individual,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		
		if(App.getInstance().getCiseccion_08()!=null) {
			App.getInstance().getCiseccion_08().qi801a = individual.qi801a;
			App.getInstance().getCiseccion_08().qi801b = individual.qi801b;
		}
		else 
			App.getInstance().setCiseccion_08(individual);
		
		return App.INDIVIDUAL;
	} 
} 
