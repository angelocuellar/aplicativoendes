package gob.inei.endes2024.fragment.hogar;
//package gob.inei.endes2024.fragment.hogar;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.components.DialogComponent;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.MasterActivity;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.TableComponent;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
//import gob.inei.dnce.interfaces.Respondible;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.fragment.hogar.dialog.HogarDialog_013_2;
//import gob.inei.endes2024.model.Hogar;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.model.Seccion03;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.HogarService;
//import gob.inei.endes2024.service.Seccion03Service;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import android.os.Bundle;
//import android.support.v4.app.FragmentManager;
//import android.util.Log;
//import android.view.ContextMenu;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ContextMenu.ContextMenuInfo;
//import android.widget.AdapterView;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//import android.widget.AdapterView.OnItemClickListener;
//
//public class HogarFragment_013_02 extends FragmentForm  implements Respondible {
//	public GridComponent2 gridPregunta236,gridHorainicio;
//	public ButtonComponent btnAgregar;
//	public ButtonComponent btnHora;
//	public LabelComponent lblpregunta236indicacion,lblpregunta237indicacion;
//	public TableComponent tcBeneficiarios;
//	public LabelComponent lblTitulo,lblhora_final;
//	SeccionCapitulo[] seccionesGrabadoHogar;
//	@FieldAnnotation(orderIndex=1)
//	public RadioGroupOtherField rgQH233;
//	@FieldAnnotation(orderIndex=2)
//	public RadioGroupOtherField rgQH234;
//	@FieldAnnotation(orderIndex=3)
//	public IntegerField txtQH234_N;
//	@FieldAnnotation(orderIndex=4)
//	public RadioGroupOtherField rgQH235;
//	@FieldAnnotation(orderIndex=5)
//	public IntegerField txtQH235_N;
//	
//	@FieldAnnotation(orderIndex=6)
//	public RadioGroupOtherField rgQH235A;
//	@FieldAnnotation(orderIndex=7)
//	public IntegerField txtQH235A_N;
//	
//	@FieldAnnotation(orderIndex=8)
//	public CheckBoxField chbQH236_A;
//	@FieldAnnotation(orderIndex=9)
//	public CheckBoxField chbQH236_B;
//	@FieldAnnotation(orderIndex=10)
//	public CheckBoxField chbQH236_C;
//	@FieldAnnotation(orderIndex=11)
//	public CheckBoxField chbQH236_D;
//	@FieldAnnotation(orderIndex=12)
//	public CheckBoxField chbQH236_E;
//	@FieldAnnotation(orderIndex=13)
//	public CheckBoxField chbQH236_F;
//	@FieldAnnotation(orderIndex=14)
//	public CheckBoxField chbQH236_G;
//	@FieldAnnotation(orderIndex=15)
//	public CheckBoxField chbQH236_H;
//	@FieldAnnotation(orderIndex=16)
//	public CheckBoxField chbQH236_I;
//	@FieldAnnotation(orderIndex=17)
//	public CheckBoxField chbQH236_J;
//	@FieldAnnotation(orderIndex=18)
//	public CheckBoxField chbQH236_K;
//	@FieldAnnotation(orderIndex=19)
//	public CheckBoxField chbQH236_L;
//	@FieldAnnotation(orderIndex=20)
//	public CheckBoxField chbQH236_M;
//	@FieldAnnotation(orderIndex=21)
//	public CheckBoxField chbQH236_X;
//	@FieldAnnotation(orderIndex=22)
//	public TextField txtQH236_X_O;
//	@FieldAnnotation(orderIndex=23)
//	public CheckBoxField chbQH236_Y;
//
//
//	
//	private HogarService hogarService;
//	public Hogar hogar;
//	public CuestionarioService  cuestionarioService;
//	public Seccion03Service seccion3service;
//	LinearLayout q0; 
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5;
//	LinearLayout q6;
//	LinearLayout q7;
//	public TextField txtCabecera;
//	SeccionCapitulo[] seccionesCargado,seccionesCargado03;
//	SeccionCapitulo[] seccionesGrabado;
//	public List<Seccion03> listado=null; 
//	private enum ACTION {
//		ELIMINAR, MENSAJE
//	}
//	public GridComponent2 gridpreg234,gridpreg235,gridpreg235a;
//	public LabelComponent lblpregunta233,lblpregunta234,lblpregunta236,lblpregunta237,lblpregunta238,lblempty,
//	lblpregun234ind,lblpregunta235,lblempty235,lblpregun235ind,
//	lblpregunta235a,lblempty235a,lblpregun235aind
//	;
//	private ACTION action;
//	private DialogComponent dialog;
//	public HogarFragment_013_02 parent(MasterActivity parent) { 
//		this.parent = parent; 
//		return this; 
//	}
//	public void onCreate(Bundle savedInstanceState) { 
//			super.onCreate(savedInstanceState); 
//	}
//	@Override 
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
//		rootView = createUI(); 
//		initObjectsWithoutXML(this, rootView); 
//		enlazarCajas(); 
//		listening();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH238_H","QH238_M","QH233","QH234","QH234_N","QH235","QH235_N","QH235A","QH235A_N","QH236_A","QH236_B","QH236_C","QH236_D","QH236_E","QH236_F","QH236_G","QH236_H","QH236_I", "QH236_J", "QH236_K", "QH236_L", "QH236_M", "QH236_X","QH236_X_O","QH236_Y","ID","HOGAR_ID","PERSONA_INFORMANTE_ID")};  
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH238_H","QH238_M","QH233","QH234","QH234_N","QH235","QH235_N","QH235A","QH235A_N","QH236_A","QH236_B","QH236_C","QH236_D","QH236_E","QH236_F","QH236_G","QH236_H","QH236_I", "QH236_J", "QH236_K", "QH236_L", "QH236_M", "QH236_X","QH236_X_O","QH236_Y")};
//		seccionesCargado03 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PREGUNTA_ID","PERSONA_ID_ORDEN","QHS3_1A","QH02_1","ESTADO3")};
//		cargarDatos();
//		return rootView; 
//    } 
//
//	@Override
//	protected void buildFields() {
//		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
//		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_titulo).textSize(21).centrar().negrita();
//		lblempty=new LabelComponent(this.getActivity()).size(altoComponente+10, 180).text("");
//		
//		lblpregunta234= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_234).textSize(19);
//		lblpregun234ind=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_234_indicacion).textSize(19);
//		
//		rgQH233 = new RadioGroupOtherField(this.getActivity(),R.string.covid19_233_1, R.string.covid19_233_2,R.string.covid19_233_8).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		rgQH234 = new RadioGroupOtherField(this.getActivity(),R.string.covid19_234_1, R.string.covid19_234_2,R.string.covid19_234_8).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP234ChangeValue");
//		txtQH234_N = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		
////		gridpreg234= new GridComponent2(this.getActivity(), 2);
////		gridpreg234.addComponent(lblempty);
////		gridpreg234.addComponent(lblpregun234ind);
////		gridpreg234.addComponent(rgQH234);
////		gridpreg234.addComponent(txtQH234_N);
//		
////		rgQH234.agregarEspecifique(0,txtQH234_N);
//		rgQH235 = new RadioGroupOtherField(this.getActivity(),R.string.covid19_235_1, R.string.covid19_235_2,R.string.covid19_235_8).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP235ChangeValue");
//		txtQH235_N = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		
//		rgQH235A = new RadioGroupOtherField(this.getActivity(),R.string.covid19_235_1, R.string.covid19_235_2,R.string.covid19_235_8).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP235AChangeValue");
//		txtQH235A_N = new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		
//		
//		//rgQH235.agregarEspecifique(0,txtQH235_N);
//		
//		lblpregunta235 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_235).textSize(19);
//		lblpregun235ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_235_indicacion).textSize(19);
//		
//		lblpregunta235a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_235a).textSize(19);
//		lblpregun235aind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_235a_indicacion).textSize(19);
//		
//		lblpregunta236 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_236).textSize(19);
//		lblpregunta237 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_237).textSize(19);
//		lblpregunta238 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_238).textSize(19);
//		lblpregunta233 =new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_233).textSize(19);
//
//		//		lblempty235=new LabelComponent(this.getActivity()).size(altoComponente+10, 180).text("");
//		
//		
////		gridpreg235=new GridComponent2(this.getActivity(), 2);
////		gridpreg235.addComponent(lblempty235);
////		gridpreg235.addComponent(lblpregun235ind);
////		gridpreg235.addComponent(rgQH235);
////		gridpreg235.addComponent(txtQH235_N);
//		
//		
////		lblempty235a=new LabelComponent(this.getActivity()).size(altoComponente+10, 180).text("");
//		
////		gridpreg235a=new GridComponent2(this.getActivity(), 2);
////		gridpreg235a.addComponent(lblempty235a);
////		gridpreg235a.addComponent(lblpregun235aind);
////		gridpreg235a.addComponent(rgQH235A);
////		gridpreg235a.addComponent(txtQH235A_N);
//	
//		//gridpreg235.addComponent(btnAgregar);
//		lblpregunta236indicacion = new LabelComponent(getActivity()).size(MATCH_PARENT , MATCH_PARENT).text(R.string.covid19_236_ind).textSize(16).alinearIzquierda();
//		lblpregunta237indicacion = new LabelComponent(getActivity()).size(MATCH_PARENT , MATCH_PARENT).text(R.string.covid19_237_ind).textSize(16).alinearIzquierda();
//		chbQH236_A = new CheckBoxField(this.getActivity(), R.string.covid19_236_a, "1:0").size(altoComponente, 290);
//		chbQH236_B = new CheckBoxField(this.getActivity(), R.string.covid19_236_b, "1:0").size(altoComponente, 290);
//		chbQH236_C = new CheckBoxField(this.getActivity(), R.string.covid19_236_c, "1:0").size(altoComponente, 290);
//		chbQH236_D = new CheckBoxField(this.getActivity(), R.string.covid19_236_d, "1:0").size(altoComponente, 290);
//		chbQH236_E = new CheckBoxField(this.getActivity(), R.string.covid19_236_e, "1:0").size(altoComponente, 290);
//		chbQH236_F = new CheckBoxField(this.getActivity(), R.string.covid19_236_f, "1:0").size(altoComponente, 290);
//		chbQH236_G = new CheckBoxField(this.getActivity(), R.string.covid19_236_g, "1:0").size(altoComponente, 290);
//		chbQH236_H = new CheckBoxField(this.getActivity(), R.string.covid19_236_h, "1:0").size(altoComponente, 290);
//		chbQH236_I = new CheckBoxField(this.getActivity(), R.string.covid19_236_i, "1:0").size(altoComponente, 290);
//		chbQH236_J = new CheckBoxField(this.getActivity(), R.string.covid19_236_j, "1:0").size(altoComponente, 290);
//		chbQH236_K = new CheckBoxField(this.getActivity(), R.string.covid19_236_k, "1:0").size(altoComponente, 290);
//		chbQH236_L = new CheckBoxField(this.getActivity(), R.string.covid19_236_l, "1:0").size(altoComponente, 290);
//		chbQH236_M = new CheckBoxField(this.getActivity(), R.string.covid19_236_m, "1:0").size(altoComponente, 290);
//		chbQH236_X = new CheckBoxField(this.getActivity(), R.string.covid19_236_x, "1:0").size(altoComponente, 200).callback("onQH236_XChangeValue");
//		txtQH236_X_O = new TextField(this.getActivity()).maxLength(800).size(altoComponente, 550).centrar();
//		chbQH236_Y = new CheckBoxField(this.getActivity(), R.string.covid19_236_y, "1:0").size(altoComponente, 290).callback("onQH236_YChangeValue");
//		
//		gridPregunta236 = new GridComponent2(getActivity(), 2);
//		gridPregunta236.addComponent(chbQH236_A,2);
//		gridPregunta236.addComponent(chbQH236_B,2);
//		gridPregunta236.addComponent(chbQH236_C,2);
//		gridPregunta236.addComponent(chbQH236_D,2);
//		gridPregunta236.addComponent(chbQH236_E,2);
//		gridPregunta236.addComponent(chbQH236_F,2);
//		gridPregunta236.addComponent(chbQH236_G,2);
//		gridPregunta236.addComponent(chbQH236_H,2);
//		gridPregunta236.addComponent(chbQH236_I,2);
//		gridPregunta236.addComponent(chbQH236_J,2);
//		gridPregunta236.addComponent(chbQH236_K,2);
//		gridPregunta236.addComponent(chbQH236_L,2);
//		gridPregunta236.addComponent(chbQH236_M,2);
//		gridPregunta236.addComponent(chbQH236_X);
//		gridPregunta236.addComponent(txtQH236_X_O);
//		gridPregunta236.addComponent(chbQH236_Y,2);
//		
//		btnAgregar=new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 55);
//		btnAgregar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				agregarBeneficiario();
//			}
//		});
//		
//		tcBeneficiarios = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//		tcBeneficiarios.addHeader(R.string.m_orden_b18_hogar, 2f , TableComponent.ALIGN.CENTER);
//		tcBeneficiarios.addHeader(R.string.m_nombre_b18_hogar, 3.5f, TableComponent.ALIGN.LEFT);
//		tcBeneficiarios.addHeader(R.string.r_cantidad, 1.5f, TableComponent.ALIGN.CENTER);
//		
//		btnHora = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.covid19_238_btn).size(200, 55);
//		lblhora_final= new LabelComponent(this.getActivity()).size(60,200).text("").centrar();
//		gridHorainicio = new GridComponent2(this.getActivity(), 2);
//		gridHorainicio.addComponent(btnHora);
//		gridHorainicio.addComponent(lblhora_final);
//		btnHora.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Calendar calendario= new GregorianCalendar();
//				Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
//				Integer minute= calendario.get(Calendar.MINUTE);
//				String muestrahora =hora.toString().length()>1?hora.toString():"0"+""+hora;
//				String muestraminuto=minute.toString().length()>1?minute.toString():"0"+""+minute;
//				lblhora_final.setText(muestrahora+":"+muestraminuto);
//			}
//		});
//		
//	}
//	
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		 q0 = createQuestionSection(txtCabecera,lblTitulo);
//		 q1 = createQuestionSection(lblpregunta233,rgQH233);
//		 q2 = createQuestionSection(lblpregunta234,rgQH234, lblpregun234ind, txtQH234_N);
//		 q3 = createQuestionSection(lblpregunta235,rgQH235, lblpregun235ind, txtQH235_N);
//		 q7 = createQuestionSection(lblpregunta235a,rgQH235A, lblpregun235aind, txtQH235A_N);
//		 q4 = createQuestionSection(lblpregunta236,lblpregunta236indicacion,gridPregunta236.component());
//		 q5 = createQuestionSection(lblpregunta237,lblpregunta237indicacion,btnAgregar,tcBeneficiarios.getTableView());
//		 q6 = createQuestionSection(lblpregunta238,gridHorainicio.component());
//		
//		ScrollView contenedor = createForm(); 
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q7);
//		form.addView(q4);
//		form.addView(q5);
//		form.addView(q6);
//		return contenedor;
//	}
//
//	@Override
//	public boolean grabar() {
//		uiToEntity(hogar);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		try {
//			boolean flag=false;
//			if(hogar.qh233!=null){
//				hogar.qh233=hogar.setValue3a8(hogar.qh233);
//			}
//			if(hogar.qh234!=null){
//				hogar.qh234=hogar.setValue3a8(hogar.qh234);
//			}
//			if(hogar.qh235a!=null){
//				hogar.qh235a=hogar.setValue3a8(hogar.qh235a);	
//			}
//			
//			hogar.qh238_h = lblhora_final.getText().toString().length()>0?lblhora_final.getText().toString().substring(0,2):null;
//			hogar.qh238_m = lblhora_final.getText().toString().length()>3?lblhora_final.getText().toString().substring(3,5):null;
//			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);								
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//			return false;
//		}
//		return true;
//	}
//	
//	@Override
//	public void cargarDatos() {
//		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
//		if (hogar == null) {
//			hogar = new Hogar();
//			hogar.id = App.getInstance().getHogar().id;
//			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
//		}
//		
//		if(hogar.qh233!=null)
//		hogar.qh233=hogar.getValue8a3(hogar.qh233);
//		if(hogar.qh234!=null)
//		hogar.qh234=hogar.getValue8a3(hogar.qh234);
//		if(hogar.qh235a!=null)
//		hogar.qh235a=hogar.getValue8a3(hogar.qh235a);
//		
//		entityToUI(hogar);
//		
//		if(hogar.qh238_h!=null){
//			String hora_=hogar.qh238_h.toString().length()==1?"0"+hogar.qh238_h.toString():hogar.qh238_h.toString();
//			String minuto_=hogar.qh238_m.toString().length()==1?"0"+hogar.qh238_m.toString():hogar.qh238_m.toString();
//			lblhora_final.setText(hora_+":"+minuto_);
//		}
//		
//		inicio();
//	}
//	
//	private boolean validar() {
//		if (!isInRange())
//			return false;
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if(Util.esVacio(hogar.qh233)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH233");
//			view = rgQH233;
//			error = true;
//			return false;
//		}
//		if(Util.esVacio(hogar.qh234)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH234");
//			view = rgQH234;
//			error = true;
//			return false;
//		}
//		if(!Util.esDiferente(hogar.qh234,1)){
//			if(Util.esVacio(hogar.qh234_n)){
//				mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH234N");
//				view = txtQH234_N;
//				error = true;
//				return false;
//			}
//			if(Util.esVacio(hogar.qh235)){
//				mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH235");
//				view = rgQH235;
//				error = true;
//				return false;
//			}
//			if(!Util.esDiferente(hogar.qh235,1)){
//				if(Util.esVacio(hogar.qh235_n)){
//					mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH235N");
//					view = txtQH235_N;
//					error = true;
//					return false;
//				}
//				
//				if(hogar.qh234_n!=null && hogar.qh235_n!=null &&Util.esMayor(hogar.qh235_n, hogar.qh234_n)){
//					mensaje = "La pregunta QH235N no debe ser mayor o igual a la pregunta QH234N ";
//					view = txtQH235_N;
//					error = true;
//					return false;
//				}
//			}
//			if(!Util.esDiferente(hogar.qh235,1)){
//				if(Util.esVacio(hogar.qh235a)){
//					mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH235A");
//					view = rgQH235A;
//					error = true;
//					return false;
//				}
//				if(!Util.esDiferente(hogar.qh235a,1)){
//					if(Util.esVacio(hogar.qh235a_n)){
//						mensaje = preguntaVacia.replace("$", "Debe registrar pregunta QH235AN");
//						view = txtQH235A_N;
//						error = true;
//						return false;
//					}
//					
//					if(hogar.qh235a_n!=null && hogar.qh235_n!=null &&Util.esMayor(hogar.qh235a_n, hogar.qh235_n)){
//						mensaje = "La pregunta QH235AN no debe ser mayor o igual a la pregunta QH235N";
//						view = txtQH235A_N;
//						error = true;
//						return false;
//					}
//				}
//			}
//		}
//		if(!EsValidoPreg236()){
//			mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
//			view = chbQH236_A;
//			error = true;
//			return false;
//		}
//		if(chbQH236_X.isChecked()){
//			if(Util.esVacio(hogar.qh236_x_o)){
//				mensaje = preguntaVacia.replace("$", "Debe ingresar especifique");
//				view = txtQH236_X_O;
//				error = true;
//				return false;	
//			}
//		}
//		if(!chbQH236_Y.isChecked()){
//			if (listado.size()<1) { 
//				mensaje = "Debe tener algun Beneficiario"; 
//				view = btnAgregar;
//				error = true; 
//				return false; 
//			}
//		}
//		if(lblhora_final.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la hora final");
//			view = btnHora;
//			error = true;
//			return false;
//		}
//		return true;
//	}
//	public void inicio(){
//		onP234ChangeValue();
//		
//		onQH236_YChangeValue();
//		ValidarsiesSupervisora();
//		cargartabla();
//	}
//	public boolean EsValidoPreg236(){
//		if(!chbQH236_A.isChecked() && !chbQH236_B.isChecked() && !chbQH236_C.isChecked() && !chbQH236_D.isChecked() && !chbQH236_E.isChecked() && !chbQH236_F.isChecked() && !chbQH236_G.isChecked() && !chbQH236_H.isChecked() && !chbQH236_I.isChecked() && !chbQH236_J.isChecked() && !chbQH236_K.isChecked() && !chbQH236_L.isChecked() && !chbQH236_M.isChecked() && !chbQH236_X.isChecked() && !chbQH236_Y.isChecked())
//		return false;
//		else
//			return true;
//	}
//	public void onP234ChangeValue(){
//		int valor= rgQH234.getTagSelected()==null?0:Integer.parseInt(rgQH234.getTagSelected().toString());
//		if(!Util.esDiferente(valor, 2,3)){
//			Util.cleanAndLockView(getActivity(), txtQH234_N,rgQH235, txtQH235_N, rgQH235A, txtQH235A_N);
//			q3.setVisibility(View.GONE);
//			q7.setVisibility(View.GONE);
//			lblpregun234ind.setVisibility(View.GONE);
//			txtQH234_N.setVisibility(View.GONE);
//		}
//		else{
//			Util.lockView(getActivity(),false, txtQH234_N, rgQH235, txtQH235_N, rgQH235A, txtQH235A_N);
//			q3.setVisibility(View.VISIBLE);
//			q7.setVisibility(View.VISIBLE);
//			lblpregun234ind.setVisibility(View.VISIBLE);
//			txtQH234_N.setVisibility(View.VISIBLE);
//			onP235ChangeValue();
//		}
//	}
//	public void onP235ChangeValue(){
//		int valor= rgQH235.getTagSelected()==null?0:Integer.parseInt(rgQH235.getTagSelected().toString());
//		if(!Util.esDiferente(valor, 2,3)){
//			Util.cleanAndLockView(getActivity(), txtQH235_N, rgQH235A, txtQH235A_N);
//			q7.setVisibility(View.GONE);
//			lblpregun235ind.setVisibility(View.GONE);
//			txtQH235_N.setVisibility(View.GONE);
//		}
//		else{
//			Util.lockView(getActivity(),false, txtQH235_N, rgQH235A, txtQH235A_N);
//			q7.setVisibility(View.VISIBLE);
//			lblpregun235ind.setVisibility(View.VISIBLE);
//			txtQH235_N.setVisibility(View.VISIBLE);
//			onP235AChangeValue();
//		}
//	}
//	
//	public void onP235AChangeValue(){
//		int valor= rgQH235A.getTagSelected()==null?0:Integer.parseInt(rgQH235A.getTagSelected().toString());
//		if(!Util.esDiferente(valor, 2,3)){
//			Util.cleanAndLockView(getActivity(), txtQH235A_N);
//			lblpregun235aind.setVisibility(View.GONE);
//			txtQH235A_N.setVisibility(View.GONE);
//		}
//		else{
//			Util.lockView(getActivity(),false, txtQH235A_N);
//			lblpregun235aind.setVisibility(View.VISIBLE);
//			txtQH235A_N.setVisibility(View.VISIBLE);
//		}
//	}
//	
//	public void onQH236_YChangeValue(){
//		if(chbQH236_Y.isChecked()){
//			Util.cleanAndLockView(getActivity(), chbQH236_A,chbQH236_B,chbQH236_C,chbQH236_D,chbQH236_E,chbQH236_F,chbQH236_G,chbQH236_H,chbQH236_I,chbQH236_J,chbQH236_K,chbQH236_L,chbQH236_M,chbQH236_X);
//			q5.setVisibility(View.GONE);
//		}
//		else{
//			Util.lockView(getActivity(),false,chbQH236_A,chbQH236_B,chbQH236_C,chbQH236_D,chbQH236_E,chbQH236_F,chbQH236_G,chbQH236_H,chbQH236_I,chbQH236_J,chbQH236_K,chbQH236_L,chbQH236_M,chbQH236_X);
//			q5.setVisibility(View.VISIBLE);
//			onQH236_XChangeValue();
//		}
//	}
//	public void onQH236_XChangeValue(){
//		if(!chbQH236_X.isChecked()){
//			Util.cleanAndLockView(getActivity(), txtQH236_X_O);
//		}
//		else {
//			Util.lockView(getActivity(),false,txtQH236_X_O);
//		}
//	}
//	protected void agregarBeneficiario(){
//		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
//		Seccion03 bean = new Seccion03();
//		bean.id = App.getInstance().getMarco().id;
//		bean.hogar_id = App.getInstance().getHogar().hogar_id;
//		bean.pregunta_id = App.PERSONAS_COVID;
//		bean.persona_id = hogar.persona_informante_id;
//		abrirDetalle(bean);
//    }
//	public void abrirDetalle(Seccion03 tmp) {    	
//		FragmentManager fm = HogarFragment_013_02.this.getFragmentManager();
//		HogarDialog_013_2 aperturaDialog = HogarDialog_013_2.newInstance(this, tmp);
//		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
//		aperturaDialog.show(fm, "aperturaDialog");
//	}
//	public void LimpiarEtiquetas(){
//	}
//	public void cargartabla(){
////		listado= new ArrayList<Seccion03>();
////		listado=getSeccion03Service().getListadoCovid(hogar.id,hogar.hogar_id,App.PERSONAS_COVID,seccionesCargado03);
////		tcBeneficiarios.setData(listado,"persona_id_orden","qh02_1","qhs3_1a");
////    	registerForContextMenu(tcBeneficiarios.getListView());
//    	
//    	listado= new ArrayList<Seccion03>();
//		listado=getSeccion03Service().getListadoCovid(hogar.id,hogar.hogar_id,App.PERSONAS_COVID,seccionesCargado03);
//		tcBeneficiarios.setData(listado,"persona_id_orden","qh02_1","qhs3_1a");
//		if(listado!=null&& listado.size()>0){
//	    	for (int row = 0; row < listado.size(); row++) {
//				if (obtenerEstado(listado.get(row)) == 1) {
//					// borde de color azul
//					tcBeneficiarios.setBorderRow(row, true);
//				} else if (obtenerEstado(listado.get(row)) == 2) {
//					// borde de color rojo
//					tcBeneficiarios.setBorderRow(row, true, R.color.red);
//				} else {
//					tcBeneficiarios.setBorderRow(row, false);
//				}
//			}
//    	}
//    	tcBeneficiarios.reloadData();
//	   	registerForContextMenu(tcBeneficiarios.getListView());
//	}
//	
//    private int obtenerEstado(Seccion03 detalle) {
// 		if (!Util.esDiferente(detalle.estado3, 0)) {
// 			return 1 ;
// 		} else if (!Util.esDiferente(detalle.estado3,1)) {
// 			return 2;
// 		}
// 		return 0;
// 	}
//	
//	 public CuestionarioService getCuestionarioService() {
//			if (cuestionarioService == null) {
//				cuestionarioService = CuestionarioService.getInstance(getActivity());
//			}
//			return cuestionarioService;
//
//	}
//	 public Seccion03Service getSeccion03Service() {
//			if (seccion3service == null) {
//				seccion3service = Seccion03Service.getInstance(getActivity());
//			}
//			return seccion3service;
//
//	}
//		private class Seccion03ClickListener implements OnItemClickListener {
//			public Seccion03ClickListener() {
//			}
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//				Seccion03 c = (Seccion03) listado.get(arg2);
//				
//			}
//		}
//		@Override
//		public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
//			super.onCreateContextMenu(menu, v, menuInfo);
//			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//			String nombre="";
//			Integer posicion = 0;
//				posicion = listado.get(info.position).persona_id!=null?listado.get(info.position).persona_id:0;
//				nombre=listado.get(info.position).qh02_1!=null?listado.get(info.position).qh02_1:"";
//				if (v.equals(tcBeneficiarios.getListView())) {
//					menu.setHeaderTitle("Menu");
//					menu.add(1, 0, 1, "Editar Beneficiario: ("+posicion+") "+nombre);
//					menu.add(1, 1, 1, "Eliminar Beneficiario: ("+posicion+") "+nombre);
//					if (posicion > 1) {
//						posicion = posicion - 1;
//					}
//					
//					Seccion03 seleccion = (Seccion03) info.targetView.getTag();
//					if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
//						menu.getItem(0).setVisible(false);
//						menu.getItem(1).setVisible(false);
//					}
//				}
//		}
//		@Override
//		public boolean onContextItemSelected(MenuItem item) {
//			if (!getUserVisibleHint())
//				return false;
//			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//			Seccion03 seleccion = (Seccion03) info.targetView.getTag();
//			if (item.getGroupId() == 1) {
//				switch (item.getItemId()) {
//				case 0: abrirDetalle(listado.get(info.position));
//						break; 
//				case 1:	action = ACTION.ELIMINAR;
//						String nombre="";
//						nombre=listado.get(info.position).qh02_1!=null?listado.get(info.position).qh02_1:"";
//						dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar a "+nombre+"?");
//						dialog.put("seleccion", (Seccion03) listado.get(info.position));
//						dialog.showDialog();
//				        break;
//				}
//			}		
//			return super.onContextItemSelected(item);
//		}
//	private HogarService getService() {
//		if (hogarService == null) {
//			hogarService = HogarService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	 
//	@Override
//	public Integer grabadoParcial() {
//		// TODO Auto-generated method stub
//		return App.NODEFINIDO;
//	}
//	@Override
//	public void onCancel() {
//		// TODO Auto-generated method stub
//		
//	}
//	@Override
//	public void onAccept() {
//		if (action == ACTION.MENSAJE) {
//			return;
//		}
//		if (dialog == null) {
//			return;
//		}
//		Seccion03 bean = (Seccion03) dialog.get("seleccion");
//		try {
//			listado.remove(bean);
//			boolean borrado = false;
//			borrado = getSeccion03Service().BorrarPersonaPregunta(bean);
//			if (!borrado) {
//				throw new SQLException("Persona no pudo ser borrada.");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
//					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//		}
//		cargartabla();
//	}
//	public void ValidarsiesSupervisora(){
//		Integer codigo=App.getInstance().getUsuario().cargo_id;
//		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//			rgQH233.readOnly();
//			rgQH234.readOnly();
//			txtQH234_N.readOnly();
//			rgQH235.readOnly();
//			txtQH235_N.readOnly();
//			rgQH235A.readOnly();
//			txtQH235A_N.readOnly();
//			chbQH236_A.readOnly();
//			chbQH236_B.readOnly();
//			chbQH236_C.readOnly();
//			chbQH236_D.readOnly();
//			chbQH236_E.readOnly();
//			chbQH236_F.readOnly();
//			chbQH236_G.readOnly();
//			chbQH236_H.readOnly();
//			chbQH236_I.readOnly();
//			chbQH236_J.readOnly();
//			chbQH236_K.readOnly();
//			chbQH236_L.readOnly();
//			chbQH236_M.readOnly();
//			chbQH236_X.readOnly();
//			txtQH236_X_O.readOnly();
//			chbQH236_Y.readOnly();
//			Util.cleanAndLockView(getActivity(), btnHora,btnAgregar);
//		}
//		else{
//			Util.lockView(getActivity(),false, btnHora,btnAgregar);
//		}
//	}
//}
