package gob.inei.endes2024.fragment.hogar; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
public class HogarFragment_009 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH77A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH77B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH77C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQH77D; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH77E; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH77F; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQH77_O; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQH78; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQH79; 
	@FieldAnnotation(orderIndex=10) 
	public TextField txtQH79_O; 
	@FieldAnnotation(orderIndex=11) 
	public DecimalField txtQH79_1; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH80A; 
	@FieldAnnotation(orderIndex=13) 
	public IntegerField txtQH80AN; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQH80B; 
	@FieldAnnotation(orderIndex=15) 
	public IntegerField txtQH80BN; 
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQH80C; 
	@FieldAnnotation(orderIndex=17) 
	public IntegerField txtQH80CN; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQH80D; 
	@FieldAnnotation(orderIndex=19) 
	public IntegerField txtQH80DN; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQH80E; 
	@FieldAnnotation(orderIndex=21) 
	public IntegerField txtQH80EN; 
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQH80F; 
	@FieldAnnotation(orderIndex=23) 
	public IntegerField txtQH80FN; 
	@FieldAnnotation(orderIndex=24) 
	public RadioGroupOtherField rgQH80G; 
	@FieldAnnotation(orderIndex=25) 
	public IntegerField txtQH80GN; 
	@FieldAnnotation(orderIndex=26) 
	public RadioGroupOtherField rgQH80H; 
	@FieldAnnotation(orderIndex=27) 
	public IntegerField txtQH80HN; 
	@FieldAnnotation(orderIndex=28) 
	public RadioGroupOtherField rgQH80I; 
	@FieldAnnotation(orderIndex=29) 
	public TextField txtQH80I_O; 
	@FieldAnnotation(orderIndex=30) 
	public IntegerField txtQH80IN; 
	@FieldAnnotation(orderIndex=31) 
	public TextAreaField txtQH_OBS_SECCION02;
	
	Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta77,lblpregunta78,lblpregunta79,lblpregunta80,lblpregunta_OBS; 
	
	public GridComponent2 gridPreguntas,gridPreguntas2,gridpreg79;	
	public LabelComponent lbltitulopregunta,lblbicicletas,lblmotocicleta,lblcarro,lblcarreta,lblbote,lblotro,lbldueniotierras,lblCantidadHectareas,lblreses,lblcaballos,lblcabras,lblovejas,lblavesgeral,lblconejos,lblcerdos,lblllamas,lblotrosanimales,lblmedida,lblindicacion;
	
	LinearLayout q0;
	LinearLayout q1;		
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 		
	LinearLayout q5;
	LinearLayout q6;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public HogarFragment_009() {} 
	public HogarFragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQH79_1, 0, 998, null, null); 
		rango(getActivity(), txtQH80AN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80BN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80CN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80DN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80EN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80FN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80GN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80HN, 1, 95, null, 98); 
		rango(getActivity(), txtQH80IN, 1, 95, null, 98); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH77A","QH77B","QH77C","QH77D","QH77E","QH77F","QH77_O","QH78","QH79","QH79_O","QH79_1","QH80A","QH80AN","QH80B","QH80BN","QH80C","QH80CN","QH80D","QH80DN","QH80E","QH80EN","QH80F","QH80FN","QH80G","QH80GN","QH80H","QH80HN","QH80I","QH80IN","QH80I_O","ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH_OBS_SECCION02")};
		seccionesGrabado= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH77A","QH77B","QH77C","QH77D","QH77E","QH77F","QH77_O","QH78","QH79","QH79_O","QH79_1","QH80A","QH80AN","QH80B","QH80BN","QH80C","QH80CN","QH80D","QH80DN","QH80E","QH80EN","QH80F","QH80FN","QH80G","QH80GN","QH80H","QH80HN","QH80I","QH80IN","QH80I_O","QH_OBS_SECCION02")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();		
		lbltitulopregunta = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh77).textSize(21).centrar().negrita();
		
		lblbicicletas = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77a).textSize(18).alinearIzquierda();
		rgQH77A=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		
		lblmotocicleta = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77b).textSize(18).alinearIzquierda();
		rgQH77B=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		lblcarro = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77c).textSize(18).alinearIzquierda();
		rgQH77C=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		lblcarreta = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77d).textSize(18).alinearIzquierda();
		rgQH77D=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		lblbote = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77e).textSize(18).alinearIzquierda();
		rgQH77E=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();		
		lblotro = new LabelComponent(this.getActivity()).size(altoComponente, 570).text(R.string.hogarqh77f).textSize(18).alinearIzquierda();
		rgQH77F=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(70,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP77fChangeValue");
		txtQH77_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 

		rgQH78=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP78ChangeValue"); 
		
		rgQH79=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh79_1,R.string.hogarqh79_2,R.string.hogarqh79_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP79ChangeValue");
		txtQH79_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH79.agregarEspecifique(1,txtQH79_O); 
		
		lblmedida = new LabelComponent(this.getActivity()).size(altoComponente,250).text(R.string.hogarqh79_1).textSize(17).centrar();
		txtQH79_1=new DecimalField(this.getActivity()).maxLength(3).size(altoComponente, 100).decimales(1);
		
		lbltitulopregunta= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh80).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		lblreses =  new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80a).textSize(18).alinearIzquierda();
		rgQH80A=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80a_1,R.string.hogarqh80a_2,R.string.hogarqh80a_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80aChangeValue"); 
		txtQH80AN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblcaballos = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80b).textSize(18).alinearIzquierda();
		rgQH80B=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80b_1,R.string.hogarqh80b_2,R.string.hogarqh80b_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80bChangeValue"); 
		txtQH80BN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblcabras =  new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80c).textSize(18).alinearIzquierda();
		rgQH80C=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80c_1,R.string.hogarqh80c_2,R.string.hogarqh80c_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80cChangeValue"); 
		txtQH80CN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblovejas = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80d).textSize(18).alinearIzquierda();
		rgQH80D=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80d_1,R.string.hogarqh80d_2,R.string.hogarqh80d_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80dChangeValue"); 
		txtQH80DN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblavesgeral = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80e).textSize(18).alinearIzquierda();
		rgQH80E=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80e_1,R.string.hogarqh80e_2,R.string.hogarqh80e_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80eChangeValue"); 
		txtQH80EN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblconejos = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80f).textSize(18).alinearIzquierda();
		rgQH80F=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80f_1,R.string.hogarqh80f_2,R.string.hogarqh80f_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80fChangeValue"); 
		txtQH80FN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblcerdos = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80g).textSize(18).alinearIzquierda();
		rgQH80G=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80g_1,R.string.hogarqh80g_2,R.string.hogarqh80g_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80gChangeValue"); 
		txtQH80GN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblllamas = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80h).textSize(18).alinearIzquierda();
		rgQH80H=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80h_1,R.string.hogarqh80h_2,R.string.hogarqh80h_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80hChangeValue"); 
		txtQH80HN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2); 
		lblotrosanimales =  new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.hogarqh80i).textSize(18).alinearIzquierda();
		rgQH80I=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh80i_1,R.string.hogarqh80i_2,R.string.hogarqh80i_3).size(70,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP80iChangeValue"); 
		txtQH80IN=new IntegerField(this.getActivity()).size(altoComponente, 60).maxLength(2);
		
		txtQH80I_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		Spanned texto = Html.fromHtml("PARA CADA RESPUESTA <b>\"SI\"</b> PREGUNTE: �Cu�ntos? \n SI EL DATO ES DE 95 A MAS REGISTRE 95");
		lblindicacion = new LabelComponent(this.getActivity()).size(altoComponente+15, 350).textSize(18);	
		lblindicacion.setText(texto);
		 
		
		txtQH_OBS_SECCION02 = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
		txtQH_OBS_SECCION02.setCallback("OcultarTecla");
		
		lblpregunta77 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh77).textSize(19);
		lblpregunta78 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh78).textSize(19);
		lblpregunta79 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh79).textSize(19);		
		lblpregunta80 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh80).textSize(19);
		lblpregunta_OBS = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqhObservaciones).textSize(19);
		
		
		gridPreguntas= new 	GridComponent2(this.getActivity(),2);		
		gridPreguntas.addComponent(lblbicicletas);
		gridPreguntas.addComponent(rgQH77A);
		gridPreguntas.addComponent(lblmotocicleta);
		gridPreguntas.addComponent(rgQH77B);
		gridPreguntas.addComponent(lblcarro);
		gridPreguntas.addComponent(rgQH77C);
		gridPreguntas.addComponent(lblcarreta);
		gridPreguntas.addComponent(rgQH77D);
		gridPreguntas.addComponent(lblbote);
		gridPreguntas.addComponent(rgQH77E);
		gridPreguntas.addComponent(lblotro);
		gridPreguntas.addComponent(rgQH77F);
		gridPreguntas.addComponent(txtQH77_O,2);
		
		gridpreg79 = new GridComponent2(getActivity(),Gravity.CENTER, 2,0);
		gridpreg79.addComponent(lblmedida);
		gridpreg79.addComponent(txtQH79_1);
		
		
		gridPreguntas2= new GridComponent2(this.getActivity(),3);
		gridPreguntas2.addComponent(lblreses);
		gridPreguntas2.addComponent(rgQH80A);
		gridPreguntas2.addComponent(txtQH80AN);
		gridPreguntas2.addComponent(lblcaballos);
		gridPreguntas2.addComponent(rgQH80B);
		gridPreguntas2.addComponent(txtQH80BN);
		gridPreguntas2.addComponent(lblcabras);
		gridPreguntas2.addComponent(rgQH80C);
		gridPreguntas2.addComponent(txtQH80CN);
		gridPreguntas2.addComponent(lblovejas);
		gridPreguntas2.addComponent(rgQH80D);
		gridPreguntas2.addComponent(txtQH80DN);
		gridPreguntas2.addComponent(lblavesgeral);
		gridPreguntas2.addComponent(rgQH80E);
		gridPreguntas2.addComponent(txtQH80EN);
		gridPreguntas2.addComponent(lblconejos);
		gridPreguntas2.addComponent(rgQH80F);
		gridPreguntas2.addComponent(txtQH80FN);
		gridPreguntas2.addComponent(lblcerdos);
		gridPreguntas2.addComponent(rgQH80G);
		gridPreguntas2.addComponent(txtQH80GN);
		gridPreguntas2.addComponent(lblllamas);
		gridPreguntas2.addComponent(rgQH80H);
		gridPreguntas2.addComponent(txtQH80HN);
		gridPreguntas2.addComponent(lblotrosanimales);
		gridPreguntas2.addComponent(rgQH80I);
		gridPreguntas2.addComponent(txtQH80IN);
		
		gridPreguntas2.addComponent(txtQH80I_O,3);
		gridPreguntas2.addComponent(lblindicacion,3);	

    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1= createQuestionSection(lblpregunta77,gridPreguntas.component());		
		q2 = createQuestionSection(lblpregunta78,rgQH78);
		q4 = createQuestionSection(gridpreg79.component());
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta79,rgQH79,q4); 
		q5= createQuestionSection(lblpregunta80,gridPreguntas2.component());
		q6= createQuestionSection(lblpregunta_OBS,txtQH_OBS_SECCION02);		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q5); 
		form.addView(q6);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar);
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh79!=null) {
				hogar.qh79=hogar.getConver79(hogar.qh79);				
			}
			if (hogar.qh80a!=null) {
				hogar.qh80a=hogar.getConver80a(hogar.qh80a);
			}
			if (hogar.qh80b!=null) {
				hogar.qh80b=hogar.getConver80b(hogar.qh80b);
			}
			if (hogar.qh80c!=null) {
				hogar.qh80c=hogar.getConver80c(hogar.qh80c);
			}
			if (hogar.qh80d!=null) {
				hogar.qh80d=hogar.getConver80d(hogar.qh80d);
			}
			if (hogar.qh80e!=null) {
				hogar.qh80e=hogar.getConver80e(hogar.qh80e);
			}
			if (hogar.qh80f!=null) {
				hogar.qh80f=hogar.getConver80f(hogar.qh80f);
			}
			if (hogar.qh80g!=null) {
				hogar.qh80g=hogar.getConver80g(hogar.qh80g);
			}
			if (hogar.qh80h!=null) {
				hogar.qh80h=hogar.getConver80h(hogar.qh80h);
			}
			if (hogar.qh80i!=null) {
				hogar.qh80i=hogar.getConver80i(hogar.qh80i);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		if(App.getInstance().getEditarPersonaSeccion01()==null){
			Seccion01 Datos= new Seccion01();
			Datos.todaslaspersonasmenoresa16=getCuestionarioService().getPersonasMenoresa16Seccion1(hogar.id, hogar.hogar_id);
			App.getInstance().setEditarPersonaSeccion01(Datos);
		}else{
			App.getInstance().getEditarPersonaSeccion01().todaslaspersonasmenoresa16=getCuestionarioService().getPersonasMenoresa16Seccion1(hogar.id, hogar.hogar_id);
		}
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh77a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77A"); 
			view = rgQH77A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh77b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77B"); 
			view = rgQH77B; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh77c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77C"); 
			view = rgQH77C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh77d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77D"); 
			view = rgQH77D; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh77e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77E"); 
			view = rgQH77E; 
			error = true; 
			return false; 
		} 
	
		if (Util.esVacio(hogar.qh77f)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.77F"); 
			view = rgQH77F; 
			error = true; 
			return false; 
		} 
		
		if (Util.esDiferente(hogar.qh77f,2)) {
			if(Util.esVacio(hogar.qh77_o)){
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQH77_O; 
				error = true; 
				return false;
			}
		}
	
		if (Util.esVacio(hogar.qh78)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.78"); 
			view = rgQH78; 
			error = true; 
			return false; 
		} 

		if (Util.esDiferente(hogar.qh78,2)) {
			if (Util.esVacio(hogar.qh79)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.79"); 
				view = rgQH79; 
				error = true; 
				return false; 
			}
		}
		
		if(!Util.esDiferente(hogar.qh79,2)){ 
			if (Util.esVacio(hogar.qh79_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQH79_O; 
				error = true; 
				return false; 
			} 
		} 
		
		if (!Util.esDiferente(hogar.qh78,1)) {
			if (Util.esDiferente(hogar.qh79,3) &&  Util.esVacio(hogar.qh79_1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.79_1"); 
				view = txtQH79_1; 
				error = true; 
				return false; 
			}	
		}	
		
		if (Util.esVacio(hogar.qh80a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80A"); 
			view = rgQH80A; 
			error = true; 
			return false; 
		} 
		
		if (Util.esDiferente(hogar.qh80a,2,3)) {
			if (Util.esVacio(hogar.qh80an)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80AN"); 
				view = txtQH80AN; 
				error = true; 
				return false; 
			} 	
		}
		
		if (Util.esVacio(hogar.qh80b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80B"); 
			view = rgQH80B; 
			error = true; 
			return false; 
		}
		if (Util.esDiferente(hogar.qh80b,2,3)) {		
			if (Util.esVacio(hogar.qh80bn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80BN"); 
				view = txtQH80BN; 
				error = true; 
				return false; 
			}	
		}
 
		if (Util.esVacio(hogar.qh80c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80C"); 
			view = rgQH80C; 
			error = true; 
			return false; 
		} 
		if (Util.esDiferente(hogar.qh80c,2,3)) {
				if (Util.esVacio(hogar.qh80cn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80CN"); 
				view = txtQH80CN; 
				error = true; 
				return false; 
			} 
		}
		if (Util.esVacio(hogar.qh80d)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80D"); 
			view = rgQH80D; 
			error = true; 
			return false; 
		}
		if (Util.esDiferente(hogar.qh80d,2,3)) {		
			if (Util.esVacio(hogar.qh80dn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80DN"); 
				view = txtQH80DN; 
				error = true; 
				return false; 
			} 
		}
		if (Util.esVacio(hogar.qh80e)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80E"); 
			view = rgQH80E; 
			error = true; 
			return false; 
		} 
		if (Util.esDiferente(hogar.qh80e,2,3)) {		
			if (Util.esVacio(hogar.qh80en)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80EN"); 
				view = txtQH80EN; 
				error = true; 
				return false; 
			} 
		}
		if (Util.esVacio(hogar.qh80f)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80F"); 
			view = rgQH80F; 
			error = true; 
			return false; 
		}
		if (Util.esDiferente(hogar.qh80f,2,3)) {		
			if (Util.esVacio(hogar.qh80fn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80FN"); 
				view = txtQH80FN; 
				error = true; 
				return false; 
			} 
		}
	
		if (Util.esVacio(hogar.qh80g)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80G"); 
			view = rgQH80G; 
			error = true; 
			return false; 
		} 
		if (Util.esDiferente(hogar.qh80g,2,3)) {		
			if (Util.esVacio(hogar.qh80gn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80GN"); 
				view = txtQH80GN; 
				error = true; 
				return false; 
			} 
		}
		if (Util.esVacio(hogar.qh80h)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80H"); 
			view = rgQH80H; 
			error = true; 
			return false; 
		}
		if (Util.esDiferente(hogar.qh80h,2,3)) {	
			if (Util.esVacio(hogar.qh80hn)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80HN"); 
				view = txtQH80HN; 
				error = true; 
				return false; 
			} 
		}
		if (Util.esVacio(hogar.qh80i)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.80I"); 
			view = rgQH80I; 
			error = true; 
			return false; 
		} 
		if (Util.esDiferente(hogar.qh80i,2,3)) {		
			if (Util.esVacio(hogar.qh80in)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.80IN"); 
				view = txtQH80IN; 
				error = true; 
				return false; 
			} 
		}
		if(!Util.esDiferente(hogar.qh80i,1)){ 
			if (Util.esVacio(hogar.qh80i_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQH80I_O; 
				error = true; 
				return false; 
			} 
		} 
	
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh79!=null) {
			hogar.qh79=hogar.setConver79(hogar.qh79);
		}
		if (hogar.qh80a!=null) {
			hogar.qh80a=hogar.setConver80a(hogar.qh80a);
		}
		if (hogar.qh80b!=null) {
			hogar.qh80b=hogar.setConver80b(hogar.qh80b);
		}
		if (hogar.qh80c!=null) {
			hogar.qh80c=hogar.setConver80c(hogar.qh80c);
		}
		if (hogar.qh80d!=null) {
			hogar.qh80d=hogar.setConver80d(hogar.qh80d);
		}
		if (hogar.qh80e!=null) {
			hogar.qh80e=hogar.setConver80e(hogar.qh80e);
		}
		if (hogar.qh80f!=null) {
			hogar.qh80f=hogar.setConver80f(hogar.qh80f);
		}
		if (hogar.qh80g!=null) {
			hogar.qh80g=hogar.setConver80g(hogar.qh80g);
		}
		if (hogar.qh80h!=null) {
			hogar.qh80h=hogar.setConver80h(hogar.qh80h);
		}
		if (hogar.qh80i!=null) {
			hogar.qh80i=hogar.setConver80i(hogar.qh80i);
		}
		entityToUI(hogar); 
		inicio(); 
    } 
    private void inicio() { 
    	onP77fChangeValue();    
    	onP78ChangeValue();
    	onP80aChangeValue();
	    onP80bChangeValue();
	    onP80cChangeValue();
	    onP80dChangeValue();
	    onP80eChangeValue();
	    onP80fChangeValue();
	    onP80gChangeValue();
	    onP80hChangeValue();
	    onP80iChangeValue();
	    ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH77A.readOnly();
			rgQH77B.readOnly();
			rgQH77C.readOnly();
			rgQH77D.readOnly();
			rgQH77E.readOnly();
			rgQH77F.readOnly();
			txtQH77_O.readOnly();
			rgQH78.readOnly();
			rgQH79.readOnly();
			txtQH79_1.readOnly();
			txtQH79_O.readOnly();
			rgQH80A.readOnly();
			rgQH80B.readOnly();
			rgQH80C.readOnly();
			rgQH80D.readOnly();
			rgQH80E.readOnly();
			rgQH80F.readOnly();
			rgQH80G.readOnly();
			rgQH80H.readOnly();
			rgQH80I.readOnly();
			txtQH80AN.readOnly();
			txtQH80BN.readOnly();
			txtQH80CN.readOnly();
			txtQH80DN.readOnly();		
			txtQH80EN.readOnly();
			txtQH80FN.readOnly();	
			txtQH80GN.readOnly();
			txtQH80HN.readOnly();
			txtQH80IN.readOnly();
			txtQH80I_O.readOnly();
			txtQH_OBS_SECCION02.setEnabled(false);
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    public void onP77fChangeValue() {
 		if (MyUtil.incluyeRango(2,2,rgQH77F.getTagSelected("").toString()) ) { 		
 			Util.cleanAndLockView(getActivity(),txtQH77_O);
 			rgQH78.requestFocus(); 			
 		} else {
 			Util.lockView(getActivity(), false,txtQH77_O);	
 			rgQH78.requestFocus();
 		} 		
 	}
    public void onP78ChangeValue() {
 		if (MyUtil.incluyeRango(2,2,rgQH78.getTagSelected("").toString()) ) {
 			MyUtil.LiberarMemoria();
 			Util.cleanAndLockView(getActivity(),rgQH79,txtQH79_1);
 			q3.setVisibility(View.GONE);
 			gridpreg79.setVisibility(View.GONE);
 			rgQH80A.requestFocus(); 			
 		} else {
 			Util.lockView(getActivity(), false,rgQH79,txtQH79_1);	
 			q3.setVisibility(View.VISIBLE);
 			gridpreg79.setVisibility(View.VISIBLE);
 			onP79ChangeValue();
 			rgQH79.requestFocus(); 		
 			} 		
 	}
    
    public void onP79ChangeValue() {
    	if(MyUtil.incluyeRango(1,1,rgQH79.getTagSelected("").toString())){
    		lblmedida.setText(R.string.hogarqh79_1);    		
    	}
    	if(MyUtil.incluyeRango(2,2,rgQH79.getTagSelected("").toString())){
    		lblmedida.setText(R.string.hogarqh79_2);
    	}
 		if (MyUtil.incluyeRango(3,3,rgQH79.getTagSelected("").toString()) ) {
// 			MyUtil.LiberarMemoria();
 			Util.cleanAndLockView(getActivity(),txtQH79_1);
 			gridpreg79.setVisibility(View.GONE);
 			rgQH80A.requestFocus(); 			
 		} 
 		else {
 			Util.lockView(getActivity(), false,txtQH79_1);
 			gridpreg79.setVisibility(View.VISIBLE);
 			txtQH79_1.requestFocus(); 		
 		} 		
 	}
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH80A.getWindowToken(), 0);
	}
    
    public void onP80aChangeValue() {
    	OcultarTecla();
  		if (MyUtil.incluyeRango(2,3,rgQH80A.getTagSelected("").toString()) ) {  		
  			Util.cleanAndLockView(getActivity(),txtQH80AN);
  			rgQH80B.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80AN);	
  			txtQH80AN.requestFocus();
  		}  		
  	}
    public void onP80bChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80B.getTagSelected("").toString()) ) {  		
  			Util.cleanAndLockView(getActivity(),txtQH80BN);
  			rgQH80C.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80BN);	
  			txtQH80BN.requestFocus();
  		}  		
  	}
    public void onP80cChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80C.getTagSelected("").toString()) ) {  
  			Util.cleanAndLockView(getActivity(),txtQH80CN);
  			rgQH80D.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80CN);	
  			txtQH80CN.requestFocus();
  		}  		
  	}
    public void onP80dChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80D.getTagSelected("").toString()) ) {  		
  			Util.cleanAndLockView(getActivity(),txtQH80DN);
  			rgQH80E.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80DN);	
  			txtQH80DN.requestFocus();
  		}  		
  	}
    public void onP80eChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80E.getTagSelected("").toString()) ) {  	
  			Util.cleanAndLockView(getActivity(),txtQH80EN);
  			rgQH80F.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80EN);	
  			txtQH80EN.requestFocus();
  		}  		
  	}

    public void onP80fChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80F.getTagSelected("").toString()) ) {
  			Util.cleanAndLockView(getActivity(),txtQH80FN);
  			rgQH80G.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80FN);	
  			txtQH80FN.requestFocus();
  		}  		
  	}
    public void onP80gChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80G.getTagSelected("").toString()) ) {  		
  			Util.cleanAndLockView(getActivity(),txtQH80GN);
  			rgQH80H.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80GN);	
  			txtQH80GN.requestFocus();
  		}  		
  	}
    public void onP80hChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80H.getTagSelected("").toString()) ) {  		
  			Util.cleanAndLockView(getActivity(),txtQH80HN);
  			rgQH80I.requestFocus();  			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80HN);	
  			txtQH80HN.requestFocus();
  		}  		
  	}
    public void onP80iChangeValue() {
  		if (MyUtil.incluyeRango(2,3,rgQH80I.getTagSelected("").toString()) ) { 
  			Util.cleanAndLockView(getActivity(),txtQH80IN,txtQH80I_O);  			 			
  		} else {
  			Util.lockView(getActivity(), false,txtQH80IN,txtQH80I_O);	
  			txtQH80IN.requestFocus();
  		}  		
  	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try { 
			if (hogar.qh79!=null) {
				hogar.qh79=hogar.getConver79(hogar.qh79);				
			}
			if (hogar.qh80a!=null) {
				hogar.qh80a=hogar.getConver80a(hogar.qh80a);
			}
			if (hogar.qh80b!=null) {
				hogar.qh80b=hogar.getConver80b(hogar.qh80b);
			}
			if (hogar.qh80c!=null) {
				hogar.qh80c=hogar.getConver80c(hogar.qh80c);
			}
			if (hogar.qh80d!=null) {
				hogar.qh80d=hogar.getConver80d(hogar.qh80d);
			}
			if (hogar.qh80e!=null) {
				hogar.qh80e=hogar.getConver80e(hogar.qh80e);
			}
			if (hogar.qh80f!=null) {
				hogar.qh80f=hogar.getConver80f(hogar.qh80f);
			}
			if (hogar.qh80g!=null) {
				hogar.qh80g=hogar.getConver80g(hogar.qh80g);
			}
			if (hogar.qh80h!=null) {
				hogar.qh80h=hogar.getConver80h(hogar.qh80h);
			}
			if (hogar.qh80i!=null) {
				hogar.qh80i=hogar.getConver80i(hogar.qh80i);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.HOGAR;
	}
} 
