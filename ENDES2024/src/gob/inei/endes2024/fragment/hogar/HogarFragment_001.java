package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
// 
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

// 
public class HogarFragment_001 extends FragmentForm {
	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQH40;
	@FieldAnnotation(orderIndex = 2)
	public TextField txtQH40_O;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQH41;
	@FieldAnnotation(orderIndex = 4)
	public TextField txtQH41_O;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQH42;
	public TextField txtCabecera;
	Hogar hogar;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblredpublica,lblaguapozo,lblaguasuperficie,lblotras,lblredpublica1,lblaguapozo1,lblaguasuperficie1,lblotras1,lblpregunta40,lblpregunta41,lblpregunta42;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoQH40,seccionesGrabadoQH40_91;
	SeccionCapitulo[] seccionesCargado;

	//
	public HogarFragment_001() {
	}

	public HogarFragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH40", "QH40_O", "QH41", "QH41_O", "QH42", "ID","HOGAR_ID","PERSONA_INFORMANTE_ID") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH40", "QH40_O", "QH41", "QH41_O", "QH42") };
		seccionesGrabadoQH40 = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH40", "QH40_O", "QH41", "QH41_O", "QH42","QH45","QH46") };
		return rootView;
	}

	@Override
	protected void buildFields() {
		
		rgQH40 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh40_1, R.string.hogarqh40_2,R.string.hogarqh40_3, R.string.hogarqh40_4,R.string.hogarqh40_5, R.string.hogarqh40_6,R.string.hogarqh40_7, R.string.hogarqh40_8,R.string.hogarqh40_9, R.string.hogarqh40_10,
				R.string.hogarqh40_11).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP40ChangeValue");
		
		txtQH40_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH40.agregarEspecifique(10, txtQH40_O);
		
		lblTitulo = new LabelComponent(this.getActivity(),	App.ESTILO)	.size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();
		lblredpublica = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t1).textSize(18).negrita();
		lblaguapozo = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t2).textSize(18).negrita();
		lblaguasuperficie = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t3).textSize(18).negrita();
		lblotras = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t4).textSize(18).negrita();
		rgQH40.agregarTitle(0, lblredpublica);
		rgQH40.agregarTitle(4, lblaguapozo);
		rgQH40.agregarTitle(7, lblaguasuperficie);
		rgQH40.agregarTitle(10, lblotras);
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		rgQH41 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh41_1, R.string.hogarqh41_2,R.string.hogarqh41_3, R.string.hogarqh41_4,R.string.hogarqh41_5, R.string.hogarqh41_6,R.string.hogarqh41_7, R.string.hogarqh41_8,R.string.hogarqh41_9, R.string.hogarqh41_10).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP41ChangeValue");
		txtQH41_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH41.agregarEspecifique(9, txtQH41_O);
		
		lblredpublica1 = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t1).textSize(18).negrita();
		lblaguapozo1 = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t2).textSize(18).negrita();
		lblaguasuperficie1 = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t3).textSize(18).negrita();
		lblotras1 = new LabelComponent(this.getActivity()).text(R.string.hogarqh40_t4).textSize(18).negrita();			
		
		lblpregunta40 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh40).textSize(19);
		lblpregunta41 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh41).textSize(19);
		lblpregunta42 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh42).textSize(19);
		
		rgQH41.agregarTitle(0, lblredpublica1);
		rgQH41.agregarTitle(4, lblaguapozo1);
		rgQH41.agregarTitle(7, lblaguasuperficie1);
		rgQH41.agregarTitle(10, lblotras1);
		rgQH42 = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
				
	}

	@Override
	protected View createUI() {
		buildFields();

		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta40, rgQH40);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta41, rgQH41);
		q3 = createQuestionSection(0,lblpregunta42, rgQH42);
	
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
	
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		
		return contenedor;
	}

	@Override
	public boolean grabar() {
		// uiToEntity(entity);
		uiToEntity(hogar);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		try {
			// hogarData=new hogar
			hogar.qh40 = hogar.getConvertqh40(hogar.qh40);
			if (hogar.qh41 != null) {
				hogar.qh41 = hogar.getConvertqh41(hogar.qh41);
			}
			boolean flag=false;
			if (!Util.esDiferente(hogar.qh40,11)  || !Util.esDiferente(hogar.qh40,21) || !Util.esDiferente(hogar.qh40,41) || !Util.esDiferente(hogar.qh41,11) || !Util.esDiferente(hogar.qh41,21) || !Util.esDiferente(hogar.qh41,41)) {
				flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabadoQH40);								
			}
			else{
				flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);							
			}
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getHogar().qh40=hogar.qh40;
		return true;
	}

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);

		if (Util.esVacio(hogar.qh40)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.40");
			view = rgQH40;
			error = true;
			return false;

		} else {
			if (!Util.esDiferente(Integer.parseInt(rgQH40.getTagSelected("").toString()), 11,11)) {
				if (Util.esVacio(txtQH40_O)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQH40_O;
					error = true;
					return false;
				}
			}
			if (!Util.esDiferente(Integer.parseInt(rgQH40.getTagSelected("").toString()), 10,11)) {
				if (Util.esVacio(rgQH41.getTagSelected("").toString())) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.41");
					view = rgQH41;
					error = true;
					return false;
				}
				if (!rgQH41.getTagSelected("").toString().equals("")) {
					if (!Util.esDiferente(Integer.parseInt(rgQH41.getTagSelected("").toString()), 10, 10)) {
						if (Util.esVacio(txtQH41_O)) {
							mensaje = "Debe ingresar informaci\u00f3n en Especifique";
							view = txtQH41_O;
							error = true;
							return false;
						}
					}
					if (!Util.esDiferente(Integer.parseInt(rgQH41.getTagSelected("").toString()), 1, 2, 3)) {
						if (Util.esVacio(rgQH42.getTagSelected("").toString())) {
							mensaje = preguntaVacia.replace("$","La pregunta P.42");
							view = rgQH42;
							error = true;
							return false;
						}
					}
				}
			} 
			
			else {
				if (!Util.esDiferente(Integer.parseInt(rgQH40.getTagSelected("").toString()),1, 2, 3)) {
					if (Util.esVacio(rgQH42.getTagSelected("").toString())) {
						mensaje = preguntaVacia.replace("$", "La pregunta P.42");
						view = rgQH42;
						error = true;
						return false;
					}
				}

				if (!rgQH41.getTagSelected("").toString().equals("")) {
					if (!Util.esDiferente(Integer.parseInt(rgQH41.getTagSelected("").toString()), 1, 2, 3)) {
						if (Util.esVacio(rgQH42.getTagSelected("").toString())) {
							mensaje = preguntaVacia.replace("$","La pregunta P.42");
							view = rgQH42;
							error = true;
							return false;
						}
					}
				}

			}
		}

		return true;
	}

	@Override
	public void cargarDatos() {
		
		if (App.getInstance().getHogar().id != null) {
			hogar = getCuestionarioService().getHogarByInformante(
					App.getInstance().getHogar().id,
					App.getInstance().getHogar().hogar_id, seccionesCargado);
			if (hogar == null) {
				hogar = new Hogar();
				hogar.id = App.getInstance().getHogar().id;
				hogar.hogar_id = App.getInstance().getHogar().hogar_id;
				// hogar.persona_id=App.getInstance().persona_id;
			}
			if (hogar.qh40 != null) {
				hogar.qh40 = hogar.setConvertqh40(hogar.qh40);
			}
			if (hogar.qh41 != null) {
				hogar.qh41 = hogar.setConvertqh41(hogar.qh41);
			}
			entityToUI(hogar);

			inicio();
		}
	}

	private void inicio() {
		onP41ChangeValue();	
		onP40ChangeValue();
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}

	public void onP40ChangeValue() {
		if (MyUtil.incluyeRango(4, 9, rgQH40.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH41,rgQH42);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
		}		
		else {			
			if (MyUtil.incluyeRango(1, 3, rgQH40.getTagSelected("").toString())) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(), rgQH41);		
				q2.setVisibility(View.GONE);
				rgQH42.requestFocus();								
			}
			else{
				onP41ChangeValue();	
				Util.lockView(getActivity(), false,rgQH41);		
				q2.setVisibility(View.VISIBLE);
				rgQH41.requestFocus();				
			}
		}
	}
	
	public void onP41ChangeValue() {	     
		if (MyUtil.incluyeRango(4, 10, rgQH41.getTagSelected("").toString()) && MyUtil.incluyeRango(10, 11, rgQH40.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH42);
			q3.setVisibility(View.GONE);
			
		} else {
			Util.lockView(getActivity(), false, rgQH42);
			q3.setVisibility(View.VISIBLE);
			rgQH42.requestFocus();
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH40.readOnly(); 
			rgQH41.readOnly();
			rgQH42.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if(hogar.qh40!=null){
			hogar.qh40 = hogar.getConvertqh40(hogar.qh40);
			}
			if (hogar.qh41 != null) {
				hogar.qh41 = hogar.getConvertqh41(hogar.qh41);
			}
			boolean flag=false;
				flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);								
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),"Los datos no pudieron ser guardados.",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
}
