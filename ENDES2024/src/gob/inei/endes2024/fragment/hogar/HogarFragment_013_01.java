package gob.inei.endes2024.fragment.hogar;
//package gob.inei.endes2024.fragment.hogar;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.MasterActivity;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.model.Hogar;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.HogarService;
//
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.GregorianCalendar;
//
//import android.app.NativeActivity;
//import android.database.sqlite.SQLiteDatabase;
//import android.os.Bundle;
////import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class HogarFragment_013_01 extends FragmentForm  {
//	public GridComponent2 gridHorainicio,gridPregunta229,gridPregunta230,gridPregunta231,gridPregunta232;
//	public ButtonComponent btnHora;
//	public LabelComponent lblTitulo,lblhora_inicial,lblpregunta229indicacion,lblpreg230,lblpreg231,lblpreg232;
//	SeccionCapitulo[] seccionesGrabadoHogar;
//	@FieldAnnotation(orderIndex=1)
//	public CheckBoxField chbQH229_A;
//	@FieldAnnotation(orderIndex=2)
//	public CheckBoxField chbQH229_B;
//	@FieldAnnotation(orderIndex=3)
//	public CheckBoxField chbQH229_C;
//	@FieldAnnotation(orderIndex=4)
//	public CheckBoxField chbQH229_D;
//	@FieldAnnotation(orderIndex=5)
//	public CheckBoxField chbQH229_X;
//	@FieldAnnotation(orderIndex=6)
//	public TextField txtQH229_X_O;
//	@FieldAnnotation(orderIndex=7)
//	public CheckBoxField chbQH229_Y;
//	@FieldAnnotation(orderIndex=8)
//	public CheckBoxField chbQH230_A;
//	@FieldAnnotation(orderIndex=9)
//	public CheckBoxField chbQH230_B;
//	@FieldAnnotation(orderIndex=10)
//	public CheckBoxField chbQH230_C;
//	@FieldAnnotation(orderIndex=11)
//	public CheckBoxField chbQH230_D;
//	@FieldAnnotation(orderIndex=12)
//	public CheckBoxField chbQH230_E;
//	@FieldAnnotation(orderIndex=13)
//	public CheckBoxField chbQH230_F;	
//	@FieldAnnotation(orderIndex=14)
//	public CheckBoxField chbQH230_X;
//	@FieldAnnotation(orderIndex=15)
//	public TextField txtQH230_X_O;
//	@FieldAnnotation(orderIndex=16)
//	public CheckBoxField chbQH230_Y;
//	@FieldAnnotation(orderIndex=17)
//	public CheckBoxField chbQH231_A;
//	@FieldAnnotation(orderIndex=18)
//	public CheckBoxField chbQH231_B;
//	@FieldAnnotation(orderIndex=19)
//	public CheckBoxField chbQH231_C;
//	@FieldAnnotation(orderIndex=20)
//	public CheckBoxField chbQH231_D;
//	@FieldAnnotation(orderIndex=21)
//	public CheckBoxField chbQH231_E;
//	@FieldAnnotation(orderIndex=22)
//	public CheckBoxField chbQH231_X;
//	@FieldAnnotation(orderIndex=23)
//	public TextField txtQH231_X_O;
//	@FieldAnnotation(orderIndex=24)
//	public CheckBoxField chbQH231_Y;
//	
//	@FieldAnnotation(orderIndex=25)
//	public CheckBoxField chbQH232_A;
//	@FieldAnnotation(orderIndex=26)
//	public CheckBoxField chbQH232_B;
//	@FieldAnnotation(orderIndex=27)
//	public CheckBoxField chbQH232_C;
//	@FieldAnnotation(orderIndex=28)
//	public CheckBoxField chbQH232_D;
//	@FieldAnnotation(orderIndex=29)
//	public CheckBoxField chbQH232_E;
//	@FieldAnnotation(orderIndex=30)
//	public CheckBoxField chbQH232_F;
//	@FieldAnnotation(orderIndex=31)
//	public CheckBoxField chbQH232_G;
//	@FieldAnnotation(orderIndex=32)
//	public CheckBoxField chbQH232_H;
//	@FieldAnnotation(orderIndex=33)
//	public CheckBoxField chbQH232_I;
//	@FieldAnnotation(orderIndex=34)
//	public CheckBoxField chbQH232_J;
//	@FieldAnnotation(orderIndex=35)
//	public CheckBoxField chbQH232_K;
//	@FieldAnnotation(orderIndex=36)
//	public CheckBoxField chbQH232_L;
//	@FieldAnnotation(orderIndex=37)
//	public CheckBoxField chbQH232_M;
//	@FieldAnnotation(orderIndex=38)
//	public CheckBoxField chbQH232_N;
//	@FieldAnnotation(orderIndex=39)
//	public CheckBoxField chbQH232_O;
//	@FieldAnnotation(orderIndex=40)
//	public CheckBoxField chbQH232_P;
//	@FieldAnnotation(orderIndex=41)
//	public CheckBoxField chbQH232_X;
//	@FieldAnnotation(orderIndex=42)
//	public TextField txtQH232_X_O;
//	@FieldAnnotation(orderIndex=43)
//	public CheckBoxField chbQH232_Y;
//	
//	
//	SeccionCapitulo[] seccionesCargado;
//	SeccionCapitulo[] seccionesGrabado; 
//	
//	public LabelComponent lblpregunta228,lblpregunta228_ind,lblpregunta229,lblpregunta230,lblpregunta231,lblpregunta232;
//	private HogarService hogarService;
//	public Hogar hogar;
//	public CuestionarioService  cuestionarioService;
//	LinearLayout q0; 
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5;
//	public TextField txtCabecera;
//	public HogarFragment_013_01 parent(MasterActivity parent) { 
//		this.parent = parent; 
//		return this; 
//	}
//	public void onCreate(Bundle savedInstanceState) { 
//			super.onCreate(savedInstanceState); 
//	}
//	@Override 
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
//		rootView = createUI(); 
//		initObjectsWithoutXML(this, rootView); 
//		enlazarCajas(); 
//		listening();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH228_H","QH228_M","QH229_A","QH229_B","QH229_C","QH229_D","QH229_X","QH229_X_O","QH229_Y","QH230_A","QH230_B","QH230_C","QH230_D", "QH230_E", "QH230_F", "QH230_X","QH230_X_O","QH230_Y","QH231_A","QH231_B","QH231_C","QH231_D","QH231_E","QH231_X","QH231_X_O","QH231_Y","QH232_A","QH232_B","QH232_C","QH232_D","QH232_E","QH232_F","QH232_G","QH232_H","QH232_I","QH232_J","QH232_K","QH232_L","QH232_M","QH232_N","QH232_O","QH232_P","QH232_X","QH232_X_O","QH232_Y","ID","HOGAR_ID")};  
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH228_H","QH228_M","QH229_A","QH229_B","QH229_C","QH229_D","QH229_X","QH229_X_O","QH229_Y","QH230_A","QH230_B","QH230_C","QH230_D", "QH230_E", "QH230_F", "QH230_X","QH230_X_O","QH230_Y","QH231_A","QH231_B","QH231_C","QH231_D","QH231_E","QH231_X","QH231_X_O","QH231_Y","QH232_A","QH232_B","QH232_C","QH232_D","QH232_E","QH232_F","QH232_G","QH232_H","QH232_I","QH232_J","QH232_K","QH232_L","QH232_M","QH232_N","QH232_O","QH232_P","QH232_X","QH232_X_O","QH232_Y")};
//		cargarDatos();
//		return rootView; 
//    } 
//
//	@Override
//	protected void buildFields() {
//		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
//		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_titulo).textSize(21).centrar();
//		lblpregunta229indicacion=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_229_indicacion).textSize(18).negrita();
//		lblpregunta228 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_228).textSize(19);
//		lblpregunta229 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_229).textSize(19);
//		lblpregunta230 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_230).textSize(19);
//		lblpregunta231 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_231).textSize(19);
//		lblpregunta232 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_232).textSize(19);
//		lblpreg230=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_230_indicacion).textSize(18);
//		lblpreg231=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_231_indicacion).textSize(18);
//		lblpreg232=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.covid19_232_indicacion).textSize(18);
//		btnHora = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.covid19_228_btn).size(200, 55);
//		lblhora_inicial= new LabelComponent(this.getActivity()).size(60,200).text("").centrar();
//		gridHorainicio = new GridComponent2(this.getActivity(), 2);
//		gridHorainicio.addComponent(btnHora);
//		gridHorainicio.addComponent(lblhora_inicial);
//		btnHora.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Calendar calendario= new GregorianCalendar();
//				Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
//				Integer minute= calendario.get(Calendar.MINUTE);
//				String muestrahora =hora.toString().length()>1?hora.toString():"0"+""+hora;
//				String muestraminuto=minute.toString().length()>1?minute.toString():"0"+""+minute;
//				lblhora_inicial.setText(muestrahora+":"+muestraminuto);
//			}
//		});
//		chbQH229_A = new CheckBoxField(this.getActivity(), R.string.covid19_229_a, "1:0").size(altoComponente, 430);
//		chbQH229_B = new CheckBoxField(this.getActivity(), R.string.covid19_229_b, "1:0").size(altoComponente, 430);
//		chbQH229_C = new CheckBoxField(this.getActivity(), R.string.covid19_229_c, "1:0").size(altoComponente, 430);
//		chbQH229_D = new CheckBoxField(this.getActivity(), R.string.covid19_229_d, "1:0").size(altoComponente, 430);
//		chbQH229_X = new CheckBoxField(this.getActivity(), R.string.covid19_229_x, "1:0").size(altoComponente, 200).callback("onQH229_XChangeValue");
//		chbQH229_Y = new CheckBoxField(this.getActivity(), R.string.covid19_229_y, "1:0").size(altoComponente, 430).callback("onQH229_YChangeValue");
//		txtQH229_X_O = new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580).centrar();
//		gridPregunta229 = new GridComponent2(this.getActivity(), 2);
//		gridPregunta229.addComponent(chbQH229_A,2);
//		gridPregunta229.addComponent(chbQH229_B,2);
//		gridPregunta229.addComponent(chbQH229_C,2);
//		gridPregunta229.addComponent(chbQH229_D,2);
//		gridPregunta229.addComponent(chbQH229_X);
//		gridPregunta229.addComponent(txtQH229_X_O);
//		gridPregunta229.addComponent(chbQH229_Y,2);
//		chbQH230_A = new CheckBoxField(this.getActivity(), R.string.covid19_230_a, "1:0").size(altoComponente, 310);
//		chbQH230_B = new CheckBoxField(this.getActivity(), R.string.covid19_230_b, "1:0").size(altoComponente, 310);
//		chbQH230_C = new CheckBoxField(this.getActivity(), R.string.covid19_230_c, "1:0").size(altoComponente, 310);
//		chbQH230_D = new CheckBoxField(this.getActivity(), R.string.covid19_230_d, "1:0").size(altoComponente, 310);
//		chbQH230_E = new CheckBoxField(this.getActivity(), R.string.covid19_230_e, "1:0").size(altoComponente, 310);
//		chbQH230_F = new CheckBoxField(this.getActivity(), R.string.covid19_230_f, "1:0").size(altoComponente, 310);
//		chbQH230_X = new CheckBoxField(this.getActivity(), R.string.covid19_230_x, "1:0").size(altoComponente, 280).callback("onQH230_XChangeValue");
//		txtQH230_X_O = new TextField(this.getActivity()).maxLength(800).size(altoComponente, 480).centrar();
//		chbQH230_Y = new CheckBoxField(this.getActivity(), R.string.covid19_230_y, "1:0").size(altoComponente, 310).callback("onQH230_YChangeValue");
//		gridPregunta230 = new GridComponent2(this.getActivity(), 2);
//		gridPregunta230.addComponent(chbQH230_A,2);
//		gridPregunta230.addComponent(chbQH230_B,2);
//		gridPregunta230.addComponent(chbQH230_C,2);
//		gridPregunta230.addComponent(chbQH230_D,2);
//		gridPregunta230.addComponent(chbQH230_E,2);
//		gridPregunta230.addComponent(chbQH230_F,2);
//		gridPregunta230.addComponent(chbQH230_X);
//		gridPregunta230.addComponent(txtQH230_X_O);
//		gridPregunta230.addComponent(chbQH230_Y,2);
//		
//		chbQH231_A = new CheckBoxField(this.getActivity(), R.string.covid19_231_a, "1:0").size(altoComponente, 300);
//		chbQH231_B = new CheckBoxField(this.getActivity(), R.string.covid19_231_b, "1:0").size(altoComponente+10, 300);
//		chbQH231_C = new CheckBoxField(this.getActivity(), R.string.covid19_231_c, "1:0").size(altoComponente, 300);
//		chbQH231_D = new CheckBoxField(this.getActivity(), R.string.covid19_231_d, "1:0").size(altoComponente, 300);
//		chbQH231_E = new CheckBoxField(this.getActivity(), R.string.covid19_231_e, "1:0").size(altoComponente, 300);
//		chbQH231_X = new CheckBoxField(this.getActivity(), R.string.covid19_231_x, "1:0").size(altoComponente, 200).callback("onQH231_XChangeValue");
//		txtQH231_X_O = new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580).centrar();
//		chbQH231_Y = new CheckBoxField(this.getActivity(), R.string.covid19_231_y, "1:0").size(altoComponente, 310).callback("onQH231_YChangeValue");
//		gridPregunta231 = new GridComponent2(this.getActivity(), 2);
//		gridPregunta231.addComponent(chbQH231_A,2);
//		gridPregunta231.addComponent(chbQH231_B,2);
//		gridPregunta231.addComponent(chbQH231_C,2);
//		gridPregunta231.addComponent(chbQH231_D,2);
//		gridPregunta231.addComponent(chbQH231_E,2);
//		gridPregunta231.addComponent(chbQH231_X);
//		gridPregunta231.addComponent(txtQH231_X_O);
//		gridPregunta231.addComponent(chbQH231_Y,2);
//		
//		chbQH232_A = new CheckBoxField(this.getActivity(), R.string.covid19_232_a, "1:0").size(altoComponente+10, 280);
//		chbQH232_B = new CheckBoxField(this.getActivity(), R.string.covid19_232_b, "1:0").size(altoComponente, 280);
//		chbQH232_C = new CheckBoxField(this.getActivity(), R.string.covid19_232_c, "1:0").size(altoComponente, 280);
//		chbQH232_D = new CheckBoxField(this.getActivity(), R.string.covid19_232_d, "1:0").size(altoComponente, 280);
//		chbQH232_E = new CheckBoxField(this.getActivity(), R.string.covid19_232_e, "1:0").size(altoComponente, 280);
//		chbQH232_F = new CheckBoxField(this.getActivity(), R.string.covid19_232_f, "1:0").size(altoComponente, 280);
//		chbQH232_G = new CheckBoxField(this.getActivity(), R.string.covid19_232_g, "1:0").size(altoComponente, 280);
//		chbQH232_H = new CheckBoxField(this.getActivity(), R.string.covid19_232_h, "1:0").size(altoComponente, 280);
//		chbQH232_I = new CheckBoxField(this.getActivity(), R.string.covid19_232_i, "1:0").size(altoComponente, 280);
//		chbQH232_J = new CheckBoxField(this.getActivity(), R.string.covid19_232_j, "1:0").size(altoComponente, 280);
//		chbQH232_K = new CheckBoxField(this.getActivity(), R.string.covid19_232_k, "1:0").size(altoComponente, 280);
//		chbQH232_L = new CheckBoxField(this.getActivity(), R.string.covid19_232_l, "1:0").size(altoComponente+10, 280);
//		chbQH232_M = new CheckBoxField(this.getActivity(), R.string.covid19_232_m, "1:0").size(altoComponente, 280);
//		chbQH232_N = new CheckBoxField(this.getActivity(), R.string.covid19_232_n, "1:0").size(altoComponente, 280);
//		chbQH232_O = new CheckBoxField(this.getActivity(), R.string.covid19_232_o, "1:0").size(altoComponente, 280);
//		chbQH232_P = new CheckBoxField(this.getActivity(), R.string.covid19_232_p, "1:0").size(altoComponente, 280);
//		chbQH232_X = new CheckBoxField(this.getActivity(), R.string.covid19_232_x, "1:0").size(altoComponente, 200).callback("onQH232_XChangeValue");
//		txtQH232_X_O = new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580).centrar();
//		chbQH232_Y = new CheckBoxField(this.getActivity(), R.string.covid19_232_y, "1:0").size(altoComponente, 280).callback("onQH232_YChangeValue");
//
//		gridPregunta232=new GridComponent2(getActivity(), 2);
//		gridPregunta232.addComponent(chbQH232_A,2);
//		gridPregunta232.addComponent(chbQH232_B,2);
//		gridPregunta232.addComponent(chbQH232_C,2);
//		gridPregunta232.addComponent(chbQH232_D,2);
//		gridPregunta232.addComponent(chbQH232_E,2);
//		gridPregunta232.addComponent(chbQH232_F,2);
//		gridPregunta232.addComponent(chbQH232_G,2);
//		gridPregunta232.addComponent(chbQH232_H,2);
//		gridPregunta232.addComponent(chbQH232_I,2);
//		gridPregunta232.addComponent(chbQH232_J,2);
//		gridPregunta232.addComponent(chbQH232_K,2);
//		gridPregunta232.addComponent(chbQH232_L,2);
//		gridPregunta232.addComponent(chbQH232_M,2);
//		gridPregunta232.addComponent(chbQH232_N,2);
//		gridPregunta232.addComponent(chbQH232_O,2);
//		gridPregunta232.addComponent(chbQH232_P,2);
//		gridPregunta232.addComponent(chbQH232_X);
//		gridPregunta232.addComponent(txtQH232_X_O);
//		gridPregunta232.addComponent(chbQH232_Y,2);
//	}
//		
//	@Override
//	protected View createUI() {
//		buildFields();
//		 q0 = createQuestionSection(txtCabecera,lblTitulo);
//		 q1 = createQuestionSection(lblpregunta228,gridHorainicio.component(),lblpregunta229indicacion);
//		 q2 = createQuestionSection(lblpregunta229,gridPregunta229.component());
//		 q3 = createQuestionSection(lblpregunta230,lblpreg230,gridPregunta230.component());
//		 q4 = createQuestionSection(lblpregunta231,lblpreg231,gridPregunta231.component());
//		 q5 = createQuestionSection(lblpregunta232,lblpreg232,gridPregunta232.component());
//	
//		ScrollView contenedor = createForm(); 
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q4);
//		form.addView(q5);
//		/*form.addView(q4);*/
//		return contenedor;
//	}
//
//	@Override
//	public boolean grabar() {
//		uiToEntity(hogar);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		try {
//			boolean flag=false;
//			hogar.qh228_h = lblhora_inicial.getText().toString().length()>0?lblhora_inicial.getText().toString().substring(0,2):null;
//			hogar.qh228_m = lblhora_inicial.getText().toString().length()>3?lblhora_inicial.getText().toString().substring(3,5):null;
//			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);								
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
//			return false;
//		}
//		return true;
//	}
//	
//	@Override
//	public void cargarDatos() {
//		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
//		if (hogar == null) {
//			hogar = new Hogar();
//			hogar.id = App.getInstance().getHogar().id;
//			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
//		}
//		entityToUI(hogar);
//		if(hogar.qh228_h!=null){
//			String hora_=hogar.qh228_h.toString().length()==1?"0"+hogar.qh228_h.toString():hogar.qh228_h.toString();
//			String minuto_=hogar.qh228_m.toString().length()==1?"0"+hogar.qh228_m.toString():hogar.qh228_m.toString();
//			lblhora_inicial.setText(hora_+":"+minuto_);
//		}
//		inicio();
//	}
//	public void inicio(){
//		onQH229_XChangeValue();
//		onQH230_XChangeValue();
//		onQH231_XChangeValue();
//		onQH232_XChangeValue();
//		ValidarsiesSupervisora();
//	}
//	private boolean validar() {
//		if (!isInRange())
//			return false;
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if(lblhora_inicial.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la hora inicial");
//			view = btnHora;
//			error = true;
//			return false;
//		}
//		if(!EsValidoPreg229()){
//			mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
//			view = chbQH229_A;
//			error = true;
//			return false;
//		}
//		if(chbQH229_X.isChecked() && Util.esVacio(txtQH229_X_O)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
//			view = txtQH229_X_O;
//			error = true;
//			return false;
//		}
//		if(!EsValidoPreg230()){
//			mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
//			view = chbQH230_A;
//			error = true;
//			return false;
//		}
//		if(chbQH230_X.isChecked() && Util.esVacio(txtQH230_X_O)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
//			view = txtQH230_X_O;
//			error = true;
//			return false;
//		}
//		if(!EsValidoPreg231()){
//			mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
//			view = chbQH231_A;
//			error = true;
//			return false;
//		}
//		if(chbQH231_X.isChecked() && Util.esVacio(txtQH231_X_O)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
//			view = txtQH231_X_O;
//			error = true;
//			return false;
//		}
////		Log.e("DATA","RESULTADO: "+!EsValidoPreg232());
//		if(!EsValidoPreg232()){
//			mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
//			view = chbQH232_A;
//			error = true;
//			return false;
//		}
//		if(chbQH232_X.isChecked() && Util.esVacio(txtQH232_X_O)){
//			mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
//			view = txtQH232_X_O;
//			error = true;
//			return false;
//		}
//		return true;
//	}
//	public boolean EsValidoPreg229(){
//		if(!chbQH229_A.isChecked() && !chbQH229_B.isChecked() && !chbQH229_C.isChecked() && !chbQH229_D.isChecked() && !chbQH229_X.isChecked() && !chbQH229_Y.isChecked())
//		return false;
//		else
//			return true;
//	}
//	public boolean EsValidoPreg230(){
//		if(!chbQH230_A.isChecked() && !chbQH230_B.isChecked() && !chbQH230_C.isChecked() && !chbQH230_D.isChecked() && !chbQH230_E.isChecked() && !chbQH230_F.isChecked() && !chbQH230_X.isChecked() && !chbQH230_Y.isChecked())
//			return false;
//		else 
//			return true;
//	}
//	public boolean EsValidoPreg231(){
//		if(!chbQH231_A.isChecked() && !chbQH231_B.isChecked() && !chbQH231_C.isChecked() && !chbQH231_D.isChecked() && !chbQH231_E.isChecked() && !chbQH231_X.isChecked() && !chbQH231_Y.isChecked())
//			return false;
//		else 
//			return true;
//	}
//	public boolean EsValidoPreg232(){
//		if(!chbQH232_A.isChecked() && !chbQH232_B.isChecked() && !chbQH232_C.isChecked() && !chbQH232_D.isChecked() && !chbQH232_E.isChecked() && !chbQH232_F.isChecked() && !chbQH232_G.isChecked() && !chbQH232_H.isChecked() && !chbQH232_I.isChecked() && !chbQH232_J.isChecked() && !chbQH232_K.isChecked() && !chbQH232_L.isChecked() && !chbQH232_M.isChecked() && !chbQH232_N.isChecked() && !chbQH232_O.isChecked() && !chbQH232_P.isChecked() && !chbQH232_X.isChecked() && !chbQH232_Y.isChecked())
//			return false;
//		else 
//			return true;
//	}
//	public void onQH229_YChangeValue(){
//		if(chbQH229_Y.isChecked()){
//			Util.cleanAndLockView(getActivity(), chbQH229_A,chbQH229_B,chbQH229_C,chbQH229_D,chbQH229_X);
//		}
//		else{
//			onQH229_XChangeValue();
//			Util.lockView(getActivity(),false,chbQH229_A,chbQH229_B,chbQH229_C,chbQH229_D,chbQH229_X);
//		}
//	}
//	public void onQH229_XChangeValue(){
//		if(!chbQH229_X.isChecked()){
//			Util.cleanAndLockView(getActivity(), txtQH229_X_O);
//		}
//		else{
//			Util.lockView(getActivity(), false,txtQH229_X_O);
//		}
//	}
//	 public CuestionarioService getCuestionarioService() {
//			if (cuestionarioService == null) {
//				cuestionarioService = CuestionarioService.getInstance(getActivity());
//			}
//			return cuestionarioService;
//
//	}
//	public void onQH230_YChangeValue(){
//		if(chbQH230_Y.isChecked()){
//			Util.cleanAndLockView(getActivity(), chbQH230_A,chbQH230_B,chbQH230_C,chbQH230_D,chbQH230_E,chbQH230_F,chbQH230_X);
//		}
//		else{
//			Util.lockView(getActivity(),false,chbQH230_A,chbQH230_B,chbQH230_C,chbQH230_D,chbQH230_E,chbQH230_F,chbQH230_X);
//		}
//	}
//	public void onQH230_XChangeValue(){
//		if(!chbQH230_X.isChecked()){
//			Util.cleanAndLockView(getActivity(), txtQH230_X_O);
//		}
//		else{
//			Util.lockView(getActivity(), false,txtQH230_X_O);
//		}
//	}
//	public void onQH231_YChangeValue(){
//		if(chbQH231_Y.isChecked()){
//			Util.cleanAndLockView(getActivity(), chbQH231_A,chbQH231_B,chbQH231_C,chbQH231_D,chbQH231_E,chbQH231_X);
//		}
//		else{
//			Util.lockView(getActivity(),false, chbQH231_A,chbQH231_B,chbQH231_C,chbQH231_D,chbQH231_E,chbQH231_X);
//		}
//	}
//	public void onQH231_XChangeValue(){
//		if(!chbQH231_X.isChecked()){
//			Util.cleanAndLockView(getActivity(), txtQH231_X_O);
//		}
//		else{
//			Util.lockView(getActivity(), false,txtQH231_X_O);
//		}
//	}
//	public void onQH232_YChangeValue(){
//		if(chbQH232_Y.isChecked()){
//			Util.cleanAndLockView(getActivity(), chbQH232_A,chbQH232_B,chbQH232_C,chbQH232_D,chbQH232_E,chbQH232_F,chbQH232_G,chbQH232_H,chbQH232_G,chbQH232_I,chbQH232_J,chbQH232_K,chbQH232_L,chbQH232_M,chbQH232_N,chbQH232_O,chbQH232_P,chbQH232_X);
//		}
//		else{
//			Util.lockView(getActivity(), false, chbQH232_A,chbQH232_B,chbQH232_C,chbQH232_D,chbQH232_E,chbQH232_F,chbQH232_G,chbQH232_H,chbQH232_G,chbQH232_I,chbQH232_J,chbQH232_K,chbQH232_L,chbQH232_M,chbQH232_N,chbQH232_O,chbQH232_P,chbQH232_X);
//		}
//	}
//	public void onQH232_XChangeValue(){
//		if(!chbQH232_X.isChecked()){
//			Util.cleanAndLockView(getActivity(), txtQH232_X_O);
//		}
//		else{
//			Util.lockView(getActivity(), false,txtQH232_X_O);
//		}
//	}
//	 public void ValidarsiesSupervisora(){
//			Integer codigo=App.getInstance().getUsuario().cargo_id;
//			if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//				chbQH229_A.readOnly();
//				chbQH229_B.readOnly();
//				chbQH229_C.readOnly();
//				chbQH229_D.readOnly();
//				chbQH229_X.readOnly();
//				txtQH229_X_O.readOnly();
//				chbQH229_Y.readOnly();
//				chbQH230_A.readOnly();
//				chbQH230_B.readOnly();
//				chbQH230_C.readOnly();
//				chbQH230_D.readOnly();
//				chbQH230_E.readOnly();
//				chbQH230_F.readOnly();
//				chbQH230_X.readOnly();
//				txtQH230_X_O.readOnly();
//				chbQH230_Y.readOnly();
//				chbQH231_A.readOnly();
//				chbQH231_B.readOnly();
//				chbQH231_C.readOnly();
//				chbQH231_D.readOnly();
//				chbQH231_E.readOnly();
//				chbQH231_X.readOnly();
//				txtQH231_X_O.readOnly();
//				chbQH231_Y.readOnly();
//				chbQH232_A.readOnly();
//				chbQH232_B.readOnly();
//				chbQH232_C.readOnly();
//				chbQH232_D.readOnly();
//				chbQH232_E.readOnly();
//				chbQH232_F.readOnly();
//				chbQH232_G.readOnly();
//				chbQH232_H.readOnly();
//				chbQH232_I.readOnly();
//				chbQH232_J.readOnly();
//				chbQH232_K.readOnly();
//				chbQH232_L.readOnly();
//				chbQH232_M.readOnly();
//				chbQH232_N.readOnly();
//				chbQH232_O.readOnly();
//				chbQH232_P.readOnly();
//				chbQH232_X.readOnly();
//				txtQH232_X_O.readOnly();
//				chbQH232_Y.readOnly();
//				Util.cleanAndLockView(getActivity(), btnHora);
//			}
//			else{
//				Util.lockView(getActivity(),false, btnHora);
//			}
//		}
//	private HogarService getService() {
//		if (hogarService == null) {
//			hogarService = HogarService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	 
//	@Override
//	public Integer grabadoParcial() {
//		// TODO Auto-generated method stub
//		return App.NODEFINIDO;
//	}
//}
