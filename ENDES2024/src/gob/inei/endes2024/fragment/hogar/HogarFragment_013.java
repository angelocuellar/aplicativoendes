package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TableComponent.ALIGN;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.util.Calendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_013 extends FragmentForm  {
	public TableComponent tcMef,tcMenoresaSeisanios,tcMenoresaDoceanios,tcReporte;
	public GridComponent2 gridResultado,gridResultados;
	public LabelComponent lblTitulo,lblnombredomestica,lblviolenciadomestica,lblnombreInformante,lblInformanteSalud,lblDatosdeControl,lblTotalPersonas,lblTotalPersonasvalor ,lblmefnro,lblmefnrovalor,lbldocenro,lbldocenrovalor,lblniniosdocenro,lblniniosdocenrovalor,lblniniosseisnro,lblniniosseisnrovalor,lblninioscinconro,lblninioscinconrovalor,lblniniostresnro,lblniniostresnrovalor,lblinformantehogarnro,lblinformantehogarnrovalor,lblinformantesaludnro,lblinformantesaludnrovalor;
	public List<Seccion01> ListadoMef,ListMenoresaSeis,ListMenoresaDoce;
	public Seccion01Service seccion01;
	SeccionCapitulo[] seccionesGrabadoHogar;
	private HogarService hogarService;
	public VisitaService visita;
	public Seccion01 informantedesalud,mefdeviolenciafamiliar;
	public List<Seccion01> posiblesinformantessalud;
	public CuestionarioService  cuestionarioService;
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	public List<Seccion01> detalles;
	public TextField txtCabecera;
	public HogarFragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
	public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
	}
	@Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 

		listening();
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHVIOLEN") };
		cargarDatos();
		return rootView; 
    } 

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01Reporte).textSize(21).centrar().negrita();
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMef= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcMenoresaSeisanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcReporte= new TableComponent(getActivity(),this,App.ESTILO).size(500, 1200).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcMenoresaDoceanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//		}
//		else{
			tcMef= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcMenoresaSeisanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcReporte= new TableComponent(getActivity(),this,App.ESTILO).size(500, 1200).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcMenoresaDoceanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcMef.addHeader(R.string.seccion01_nro_orden,1f);
		tcMef.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMef.addHeader(R.string.seccion01_edad,1f);
		tcMef.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		lblviolenciadomestica = new LabelComponent(this.getActivity()).text(R.string.mefviolenciaDomestica).size(altoComponente+10, 380) .negrita();
		lblInformanteSalud = new LabelComponent(this.getActivity()).text(R.string.informantesalud).size(altoComponente+10, 380) .negrita();
		lblnombredomestica = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 200) .negrita();
		lblnombreInformante = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 200) .negrita();
		
		gridResultado= new GridComponent2(this.getActivity(),2);
		gridResultado.addComponent(lblviolenciadomestica);
		gridResultado.addComponent(lblnombredomestica);
		gridResultado.addComponent(lblInformanteSalud);
		gridResultado.addComponent(lblnombreInformante);
		
		
		tcMenoresaSeisanios.addHeader(R.string.seccion01_nro_orden,1f);
		tcMenoresaSeisanios.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMenoresaSeisanios.addHeader(R.string.seccion01_edad,1f);
		tcMenoresaSeisanios.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		
		
		tcMenoresaDoceanios.addHeader(R.string.seccion01_nro_orden,1f);
		tcMenoresaDoceanios.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMenoresaDoceanios.addHeader(R.string.seccion01_edad,1f);
		
		
		tcReporte.addHeader(R.string.seccion01_nro_orden,1f);
		tcReporte.addHeader(R.string.seccion01nombres,3f,ALIGN.LEFT);
		tcReporte.addHeader(R.string.seccion01_edad,2f);
		tcReporte.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		tcReporte.addHeader(R.string.seccion01_educacion14,2f);
		tcReporte.addHeader(R.string.seccion01_educacion15,3f,ALIGN.LEFT);
		
//		lblDatosdeControl = new LabelComponent(this.getActivity()).text(R.string.datosdecontrol).size(altoComponente, 250).negrita().textSize(20);
		lblTotalPersonas = new LabelComponent(this.getActivity()).text(R.string.totaldepersonas).size(altoComponente,550).negrita().textSize(16);
		lblmefnro = new LabelComponent(this.getActivity()).text(R.string.mujeres15a49).size(altoComponente, 250).negrita().textSize(16);
		lblniniosdocenro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresadoce).size(altoComponente, 550).negrita().textSize(16);
		lblniniosseisnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresaseis).size(altoComponente, 550).negrita().textSize(16);
		lblninioscinconro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresacinco).size(altoComponente, 550).negrita().textSize(16);
		lblniniostresnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresatres).size(altoComponente, 550).negrita().textSize(16);
		lblinformantehogarnro = new LabelComponent(this.getActivity()).text(R.string.informantehogar).size(altoComponente, 550).negrita().textSize(16);
		lblinformantesaludnro = new LabelComponent(this.getActivity()).text(R.string.informantesaludnro).size(altoComponente, 550).negrita().textSize(16);
		lblTotalPersonasvalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblmefnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniosdocenrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniosseisnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblninioscinconrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniostresnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblinformantehogarnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 100).negrita().textSize(16);
		lblinformantesaludnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 100).negrita().textSize(16);
		gridResultados = new GridComponent2(this.getActivity(),2);
	}
	
	
	@Override
	protected View createUI() {
		buildFields();
		 q0 = createQuestionSection(txtCabecera,lblTitulo);
		 q5 = createQuestionSection(R.string.resumendelhogar,tcReporte.getTableView());
		 q1 = createQuestionSection(R.string.listadomef,tcMef.getTableView());
		 q2 = createQuestionSection(gridResultado.component());
		 q3 = createQuestionSection(R.string.menoresaseis,tcMenoresaSeisanios.getTableView());
		 q4 = createQuestionSection(R.string.menoresadoce,tcMenoresaDoceanios.getTableView());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q5);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		// TODO Auto-generated method stub
		
		mefdeviolenciafamiliar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, SeleccionarMefViolenciaDomestica());
		Hogar hogar = new Hogar();
		hogar.id=App.getInstance().getMarco().id;
		hogar.hogar_id=App.getInstance().getHogar().hogar_id;
		if(mefdeviolenciafamiliar!=null){
		hogar.qhviolen = mefdeviolenciafamiliar.persona_id;
		}
		else{
			hogar.qhviolen=0;	
		}
		
		boolean flag = false;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(hogar, dbTX, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		
		
		MensajeSinoTieneGps(hogar.id);
		
		return flag;
	}
	
	private void MensajeSinoTieneGps(Integer id){
		if(EndesCalendario.VerificarSitientePuntoGPS(id)==true){
			MyUtil.MensajeGeneral(getActivity(),App.MENSAJE_GPS);
		}
	}
	
	@Override
	public void cargarDatos() {
		LimpiarEtiquetas();
//		detalles = getCuestionarioService().GetSeccion01ListByMarcoyHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		detalles = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 99);
		tcReporte.setData(detalles, "persona_id","getNombre","qh07","getSegurodeSalud","getPregunta14","getPregunta15");
		Integer persona_id=MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, getServiceVisita(), getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersona(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id, persona_id);
		if(informantedesalud!=null){
			informantedesalud.qh02_2=informantedesalud.qh02_2==null?"":informantedesalud.qh02_2;
			lblnombreInformante.setText(informantedesalud.persona_id + " "+ informantedesalud.qh02_1+" "+informantedesalud.qh02_2);
//			lblinformantesaludnrovalor.setText(informantedesalud.persona_id+"");
		}
		mefdeviolenciafamiliar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, SeleccionarMefViolenciaDomestica());
		if(mefdeviolenciafamiliar!=null)
		{mefdeviolenciafamiliar.qh02_2=mefdeviolenciafamiliar.qh02_2==null?"": mefdeviolenciafamiliar.qh02_2;
			lblnombredomestica.setText(mefdeviolenciafamiliar.persona_id+" "+mefdeviolenciafamiliar.qh02_1+" "+mefdeviolenciafamiliar.qh02_2);}
//		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		ListadoMef = getServiceSeccion01().getMujerSeleccionable(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,App.HogEdadMefMin,App.HogEdadMefMax);
		if(ListadoMef.size()==0){
		q1.setVisibility(View.GONE);
		}else{
		tcMef.setData(ListadoMef, "persona_id","qh02_1","qh07","getSegurodeSalud");
//		lblmefnrovalor.setText(ListadoMef.size()+"");
		}
		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 5);
		if(ListMenoresaSeis.size()==0){
			q3.setVisibility(View.GONE);
		}
		else{tcMenoresaSeisanios.setData(ListMenoresaSeis, "persona_id","getNombre","qh07","getSegurodeSalud");
//		 lblniniosseisnrovalor.setText(ListMenoresaSeis.size()+"");
		 }
		ListMenoresaDoce = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 11);
		if(ListMenoresaDoce.size()==0){
			q4.setVisibility(View.GONE);
		}
		else{
		tcMenoresaDoceanios.setData(ListMenoresaDoce, "persona_id","getNombre","qh07");
//		lblniniosdocenrovalor.setText(ListMenoresaDoce.size()+"");
		}
		
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 97);
//		lblTotalPersonasvalor.setText(ListMenoresaSeis.size()+"");
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 4);
//		lblninioscinconrovalor.setText(ListMenoresaSeis.size()+"");
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 2);
//		lblniniostresnrovalor.setText(ListMenoresaSeis.size()+"");
//		Seccion01 informantehogar = getServiceSeccion01().getPersonaInformante(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, 1);
//		String datahogar= informantehogar.persona_id+"";
//		lblinformantehogarnrovalor.setText(datahogar);
	}
	public void LimpiarEtiquetas()
	{
		lblTotalPersonasvalor.setText("");
		lblmefnrovalor.setText("");
		lblniniosdocenrovalor.setText("");
		lblniniosseisnrovalor.setText("");
		lblninioscinconrovalor.setText("");
		lblniniostresnrovalor.setText("");
		lblinformantehogarnrovalor.setText("");
		lblinformantesaludnrovalor.setText("");
	}
	
	public Integer SeleccionarMefViolenciaDomestica()
	{
		Integer[][] MatrizViolencia = LlenarMatrizViolenciaFamiliar();
		Integer fila =Integer.parseInt(App.getInstance().getMarco().nselv.substring(2,3));
		
		Integer columna = getServiceSeccion01().cantidaddeMef(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id);
		
		
		Integer mefPosition =0; 
		if(columna!=0)
		{
			mefPosition=MatrizViolencia[fila][columna-1];
		}
		
		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		Integer persona_id=-1;
		if(ListadoMef.size()>0)
		{
			persona_id=ListadoMef.get(mefPosition-1).persona_id;
		}
		return persona_id;
	}
	
	public Integer[][] LlenarMatrizViolenciaFamiliar()
	{
		Integer[][] matriz= new Integer[10][10];
		matriz[0][0]=1; matriz[0][1]=2; matriz[0][2]=2; matriz[0][3]=4; matriz[0][4]=3; matriz[0][5]=6; matriz[0][6]=5; matriz[0][7]=4; 
		matriz[1][0]=1; matriz[1][1]=1; matriz[1][2]=3; matriz[1][3]=1; matriz[1][4]=4; matriz[1][5]=1; matriz[1][6]=6; matriz[1][7]=5; 
		matriz[2][0]=1; matriz[2][1]=2; matriz[2][2]=1; matriz[2][3]=2; matriz[2][4]=5; matriz[2][5]=2; matriz[2][6]=7; matriz[2][7]=6; 
		matriz[3][0]=1; matriz[3][1]=1; matriz[3][2]=2; matriz[3][3]=3; matriz[3][4]=1; matriz[3][5]=3; matriz[3][6]=1; matriz[3][7]=7; 
		matriz[4][0]=1; matriz[4][1]=2; matriz[4][2]=3; matriz[4][3]=4; matriz[4][4]=2; matriz[4][5]=4; matriz[4][6]=2;	matriz[4][7]=8;
		matriz[5][0]=1; matriz[5][1]=1; matriz[5][2]=1; matriz[5][3]=1; matriz[5][4]=3; matriz[5][5]=5; matriz[5][6]=3; matriz[5][7]=1; 
		matriz[6][0]=1; matriz[6][1]=2; matriz[6][2]=2; matriz[6][3]=2; matriz[6][4]=4; matriz[6][5]=6; matriz[6][6]=4; matriz[6][7]=2; 
		matriz[7][0]=1; matriz[7][1]=1; matriz[7][2]=3; matriz[7][3]=3; matriz[7][4]=5; matriz[7][5]=1; matriz[7][6]=5; matriz[7][7]=3; 
		matriz[8][0]=1; matriz[8][1]=2; matriz[8][2]=1; matriz[8][3]=4; matriz[8][4]=1; matriz[8][5]=2; matriz[8][6]=6; matriz[8][7]=4; 
		matriz[9][0]=1;	matriz[9][1]=1;	matriz[9][2]=2;	matriz[9][3]=1;	matriz[9][4]=2;	matriz[9][5]=3;	matriz[9][6]=7;	matriz[9][7]=5;
		return matriz;
	}
	
	public Long CantidaddeDias(Calendar fechaInicio,Calendar fechaFin)
	{
		long total=0;
		total=fechaInicio.getTimeInMillis() - fechaFin.getTimeInMillis();
		long difd=total / (1000 * 60 * 60 * 24);
		return difd;
	}

	 private Seccion01Service getServiceSeccion01() {
	        if (seccion01 == null) {
	        	seccion01 = Seccion01Service.getInstance(getActivity());
	        }
	        return seccion01;
	}
	 private VisitaService getServiceVisita() {
	        if (visita == null) {
	        	visita = VisitaService.getInstance(getActivity());
	        }
	        return visita;
	}
	 public CuestionarioService getCuestionarioService() {
			if (cuestionarioService == null) {
				cuestionarioService = CuestionarioService.getInstance(getActivity());
			}
			return cuestionarioService;

	}
	 
	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	 
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}
