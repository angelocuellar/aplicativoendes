package gob.inei.endes2024.fragment.hogar;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_007 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH73; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQH73_O;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH74;
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQH74_O;
	public TextField txtCabecera;
	public LabelComponent lblTitulo,lblpacabado,lblprustico,lblpnatural,lblelavorados,lblrusticos,lblnaturales,lblpregunta73,lblpregunta74;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	Hogar hogar;
	private CuestionarioService cuestionarioService; 
	
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	
	public HogarFragment_007() {} 
	public HogarFragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH73","QH73_O","QH74","QH74_O","ID","HOGAR_ID","PERSONA_INFORMANTE_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH73","QH73_O","QH74","QH74_O")}; 
		return rootView; 
	} 
	
	@Override
	protected void buildFields() {

		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();
		
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		
		rgQH73 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh73_11,R.string.hogarqh73_12,R.string.hogarqh73_13,R.string.hogarqh73_14,R.string.hogarqh73_21,R.string.hogarqh73_22,R.string.hogarqh73_31,R.string.hogarqh73_96).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH73_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH73.agregarEspecifique(7,txtQH73_O);
		lblpacabado = new LabelComponent(this.getActivity()).text(R.string.hogarqh73_t1).textSize(18).negrita();
		lblprustico = new LabelComponent(this.getActivity()).text(R.string.hogarqh73_t2).textSize(18).negrita();
		lblpnatural = new LabelComponent(this.getActivity()).text(R.string.hogarqh73_t3).textSize(18).negrita();
		rgQH73.agregarTitle(0,lblpacabado);
		rgQH73.agregarTitle(5, lblprustico);
		rgQH73.agregarTitle(8,lblpnatural);
		
		rgQH74 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh74_11,R.string.hogarqh74_12,R.string.hogarqh74_13,R.string.hogarqh74_21,R.string.hogarqh74_22,R.string.hogarqh74_23,R.string.hogarqh74_24,R.string.hogarqh74_31,R.string.hogarqh74_32,R.string.hogarqh74_33,R.string.hogarqh74_34,R.string.hogarqh74_35,R.string.hogarqh74_96).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH74_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH74.agregarEspecifique(12,txtQH74_O);
		lblelavorados = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t1).textSize(18).negrita();
		lblrusticos = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t2).textSize(18).negrita();
		lblnaturales = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t3).textSize(18).negrita();
		rgQH74.agregarTitle(0,lblelavorados);
		rgQH74.agregarTitle(4, lblrusticos);
		rgQH74.agregarTitle(9, lblnaturales);
		
		lblpregunta73 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh73).textSize(18);
		lblpregunta74 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh74).textSize(18);		
	}
	
	
	@Override
	protected View createUI() {	
		buildFields();
		 q0 = createQuestionSection(txtCabecera,lblTitulo);
		 q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta73,rgQH73); 
		 q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta74,rgQH74); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}

	@Override
	public boolean grabar() {
	
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh73!=null) {
				hogar.qh73=hogar.getConvert73(hogar.qh73);
			}
			if (hogar.qh74!=null) {
				hogar.qh74=hogar.getConvert74(hogar.qh74);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
	}
	
	private boolean validar()
	{
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (!Util.esDiferente(hogar.hogar_id,1)) {
			
			if(Util.esVacio(hogar.qh73)){
				mensaje = preguntaVacia.replace("$", "La pregunta P.73"); 
				view = rgQH73; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(hogar.qh73,8)) {
				if(Util.esVacio(hogar.qh73_o)){
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQH73_O; 
					error = true; 
					return false;
				}				
			}	
			if(Util.esVacio(hogar.qh74)){
				mensaje = preguntaVacia.replace("$", "La pregunta P.74"); 
				view = rgQH74; 
				error = true; 
				return false; 
			}			
			if(!Util.esDiferente(hogar.qh74,13)) {
				if(Util.esVacio(hogar.qh74_o)){
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";  
					view = txtQH74_O; 
					error = true; 
					return false;
				}
			}
		}		
		return true;
	}

	@Override
	public void cargarDatos() {
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh73!=null) {
			hogar.qh73=hogar.setConvert73(hogar.qh73);
		}
		if (hogar.qh74!=null) {
			hogar.qh74=hogar.setConvert74(hogar.qh74);
		}
		entityToUI(hogar); 
		inicio(); 
	}
	
	   private void validarMensaje(String msj) {
	        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	    }

	private void inicio() { 
		if (Util.esMayor(hogar.hogar_id,1)) {
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		}		
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH73.readOnly();
			txtQH73_O.readOnly();
			rgQH74.readOnly();
			txtQH74_O.readOnly();
			
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try { 
			if (hogar.qh73!=null) {
				hogar.qh73=hogar.getConvert73(hogar.qh73);
			}
			if (hogar.qh74!=null) {
				hogar.qh74=hogar.getConvert74(hogar.qh74);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		} 
		return App.HOGAR;
	}
}
