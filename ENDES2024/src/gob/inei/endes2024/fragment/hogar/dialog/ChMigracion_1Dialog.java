package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2024.fragment.hogar.dialog;
//
//import java.sql.SQLException;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0001;
//import gob.inei.endes2024.model.MIGRACION;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class ChMigracion_1Dialog extends DialogFragmentComponent {
//	@FieldAnnotation(orderIndex=1) 
//	public TextField txtQH34B_1; 
//	@FieldAnnotation(orderIndex=2) 
//	public TextField txtQH34B_11;
//	@FieldAnnotation(orderIndex=3) 
//	public TextField txtQH34B_2; 
//	@FieldAnnotation(orderIndex=4) 
//	public TextField txtQH34B_3; 
//	
//	@FieldAnnotation(orderIndex=5) 
//	public ButtonComponent btnAceptar;
//
//	public static HogarFragment_0001 caller;
//	MIGRACION persona;
//	private CuestionarioService cuestionarioService;
//	 private Seccion01Service Personaservice;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	
//	public GridComponent2 gridqh34b;
//	public ButtonComponent  btnCancelar;
//	public LabelComponent lblpreguntaqh34_t,lblpreguntaqh34b,lblpreguntaqh34b_1,lblpreguntaqh34b_11,lblpreguntaqh34b_2,lblpreguntaqh34b_3;
//	public Seccion01 informantehogar;
//	LinearLayout q0;
//	LinearLayout q1;
//	
//	public static ChMigracion_1Dialog newInstance(FragmentForm pagina, MIGRACION detalle) {
//		caller = (HogarFragment_0001) pagina;
//		ChMigracion_1Dialog f = new ChMigracion_1Dialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", detalle);
//		f.setArguments(args);
//		return f;
//	}
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (MIGRACION) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().setTitle("N� "+persona.qh34a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public ChMigracion_1Dialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "ID","HOGAR_ID") };
//		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "ID","HOGAR_ID")}; 
//	}            
//	
//
//	
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		lblpreguntaqh34_t = new LabelComponent(this.getActivity(),	App.ESTILO)	.size(MATCH_PARENT, 750).text(R.string.migracion34_t).textSize(20).centrar().negrita();
//		
//		lblpreguntaqh34b= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(19).text(R.string.migracion34b);
//		lblpreguntaqh34b_1 = new LabelComponent(this.getActivity()).text(R.string.migracion34b_1).size(altoComponente, 350).textSize(16);
//		lblpreguntaqh34b_11 = new LabelComponent(this.getActivity()).text(R.string.migracion34b_11).size(altoComponente, 350).textSize(16);
//		lblpreguntaqh34b_2 = new LabelComponent(this.getActivity()).text(R.string.migracion34b_2).size(altoComponente, 350).textSize(16);
//		lblpreguntaqh34b_3 = new LabelComponent(this.getActivity()).text(R.string.migracion34b_3).size(altoComponente, 350).textSize(16);
//		
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		
//		txtQH34B_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH34B_11 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH34B_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH34B_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		
//		gridqh34b = new GridComponent2(this.getActivity(),App.ESTILO, 2);
//		gridqh34b.addComponent(lblpreguntaqh34b_1);
//		gridqh34b.addComponent(txtQH34B_1);
//		gridqh34b.addComponent(lblpreguntaqh34b_11);
//		gridqh34b.addComponent(txtQH34B_11);
//		gridqh34b.addComponent(lblpreguntaqh34b_2);
//		gridqh34b.addComponent(txtQH34B_2);
//		gridqh34b.addComponent(lblpreguntaqh34b_3);
//		gridqh34b.addComponent(txtQH34B_3);
//		
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				ChMigracion_1Dialog.this.dismiss();
//			Util.lockView(getActivity(), false,caller.btnAgregar);
//			}
//		});
//		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = false;
//				flag=grabar();
//				if(!flag){
//					return;
//				}
//				caller.refrescarPersonas(persona);
//				ChMigracion_1Dialog.this.dismiss();
//				if(Integer.valueOf(caller.txtQH34_N.getText().toString())>caller.migracion.size()){
//					caller.agregarPersona();
//				}
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q1 = createQuestionSection(lblpreguntaqh34b,gridqh34b.component());
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q1);
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar() {
//		uiToEntity(persona);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag = true;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		  } catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		} 
//		return flag;
//	}
//	
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if (persona.qh34b_1  == null) {
//			error = true;
//			view = txtQH34B_1;
//			mensaje = preguntaVacia.replace("$", "La pregunta Nombre");
//			return false;
//		}		
//	    return true;
//	}
//	
//	private void cargarDatos() { 
//		persona = getCuestionarioService().getPersonaMigracion(persona.id,persona.hogar_id,persona.qh34a,seccionesCargado);
//		if (persona == null) {
//			persona = new MIGRACION();
//			persona.id = App.getInstance().getMarco().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.qh34a= caller.migracion.size()+1;
//		}
//		entityToUI(persona);
//		inicio();
//    }
//	
//	public void inicio(){
//	}
//	
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//    public Seccion01Service getPersonaService(){
//    	if(Personaservice == null){
//    		Personaservice = Seccion01Service.getInstance(getActivity());
//    	}
//    	return Personaservice;
//    }
//
//}
