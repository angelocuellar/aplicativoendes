package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2024.fragment.hogar.dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_02;
//import gob.inei.endes2024.model.Beneficiario;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.model.Seccion03;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import gob.inei.endes2024.service.Seccion03Service;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class HogarDialog_013_2  extends DialogFragmentComponent {
//	public interface BeneficiarioDialogListener {
//		void onFinishEditDialog(String inputText);
//	}
//	@FieldAnnotation(orderIndex = 1)
//	public SpinnerField spnPERSONA_ID_ORDEN;
//	@FieldAnnotation(orderIndex = 2)
//	public IntegerField txtQHS3_1A;
//	@FieldAnnotation(orderIndex = 3)
//	public ButtonComponent btnAceptar;
//	@FieldAnnotation(orderIndex = 4)
//	public ButtonComponent btnCancelar;
//	private static HogarFragment_013_02 caller;
//	LinearLayout q0, q1, q2, q3, q4;
//	private CuestionarioService cuestionarioService;
//	private SeccionCapitulo[] seccionesCargado,seccionesGrabadoB;
//	private CuestionarioService hogarService;
//	private LabelComponent lblNombre, lblTiempo, lblAnios, lblcuantotiempo, lblAlimentar;
//	private GridComponent2 gridJefe;
//	public ArrayList<String> nombresAll;
//	public ArrayList<Beneficiario> Beneficiaros;
//	public List<Seccion01> posiblesBeneficiarios;
//
//	private Seccion03Service beneficiario;
//	private Seccion01Service seccion01;
//	
//	public Seccion03 bean; 
//	private Seccion01Service seccion01service;
//	public static HogarDialog_013_2 newInstance(FragmentForm pagina, Seccion03 detalle) {
//		caller = (HogarFragment_013_02) pagina;
//		HogarDialog_013_2 f = new HogarDialog_013_2();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", detalle);
//		f.setArguments(args);
//		return f;
//	}
//
//	public HogarDialog_013_2(){
//		super();
//		seccionesGrabadoB = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QHS3_1A","PREGUNTA_ID","PERSONA_ID")};
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PREGUNTA_ID","PERSONA_ID_ORDEN","QHS3_1A")};
//	}
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		bean = (Seccion03) getArguments().getSerializable("detalle");
//		//persona_id=bean.persona_id;
//		caretaker = new Caretaker<Entity>();
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
//		tituloPreguntas();
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	private Seccion03Service getServiceBeneficiario() {
//        if (beneficiario == null) {
//        	beneficiario = Seccion03Service.getInstance(getActivity());
//        }
//        return beneficiario;
//	}
//	private Seccion01Service getServiceSeccion01() {
//        if (seccion01 == null) {
//        	seccion01 = Seccion01Service.getInstance(getActivity());
//        }
//        return seccion01;
//	}
//	
//	private void cargarDatos() {
//		llenarSpinnerBeneficiarios(bean);
//		entityToUI(bean);
//		inicio();
//	}
//	//modificar aqui
//	public void llenarSpinnerBeneficiarios(Seccion03 seccion3){
//		Seccion01 qali = new Seccion01();
//		switch (seccion3.pregunta_id) {
//		case App.PERSONAS_COVID	:if(seccion3.persona_id_orden!=null) 
//						{MyUtil.llenarBeneficiariosCovid(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,18,99,seccion3.pregunta_id);
//						Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
//						else{MyUtil.llenarBeneficiariosCovid(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,18,99,seccion3.pregunta_id);}
//						break;
//		}
//	}
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q1 = createQuestionSection(gridJefe.component());
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q1);
//		form.addView(botones);
//		return contenedor;
//	}
//	@Override
//	protected void buildFields() {
//		lblTiempo = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 10, 630).text(R.string.hogarqh102_1t).textSize(18).alinearIzquierda();
//		lblNombre = new LabelComponent(this.getActivity()).size(altoComponente + 20, 270).text(R.string.hogarqh102_1n).textSize(16).negrita().alinearIzquierda();
//		lblAnios = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh102_1a).textSize(16).negrita().alinearIzquierda();
//		lblcuantotiempo = new LabelComponent(this.getActivity()).size(altoComponente + 20, 270).text(R.string.hogarqh237_2).textSize(16).negrita().alinearIzquierda();
//		lblAlimentar = new LabelComponent(this.getActivity()).size(altoComponente + 50, 270).text(R.string.hogarqh109_1r).textSize(16).alinearIzquierda();
//		textoPreguntas();
//		spnPERSONA_ID_ORDEN = new SpinnerField(getActivity()).size(altoComponente + 10, 360).callback("onPERSONA_ID_ORDENChangeValue");
//		txtQHS3_1A=new IntegerField(getActivity()).size(altoComponente+10, 360).maxLength(1);
//		gridJefe = new GridComponent2(this.getActivity(), 2);
//		gridJefe.addComponent(lblNombre);
//		gridJefe.addComponent(spnPERSONA_ID_ORDEN);
//		gridJefe.addComponent(lblcuantotiempo);
//		gridJefe.addComponent(txtQHS3_1A);
//		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 55);
//		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 55);
//		btnCancelar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				HogarDialog_013_2.this.dismiss();
//			}
//		});
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				caller.cargartabla();
//				HogarDialog_013_2.this.dismiss();
//			}
//		});
//	}
//	private CuestionarioService getService() {
//		if (hogarService == null) {
//			hogarService = CuestionarioService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	
//	public Seccion01Service getSeccion01Service() { 
//		if(seccion01service==null){ 
//			seccion01service = Seccion01Service.getInstance(getActivity()); 
//		} 
//		return seccion01service; 
//    }
//	
//	public boolean grabar() {
//		uiToEntity(bean);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag = true;
//		bean.pregunta_id=App.PERSONAS_COVID;
//		try {			
//			flag = getServiceBeneficiario().saveOrUpdate(bean,seccionesGrabadoB);
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		}
//		return flag;
//	}
//
//	private boolean validar() {
//		String preguntaVacia = this.getResources().getString(
//				R.string.pregunta_no_vacia);
//		if(Util.esVacio(bean.persona_id_orden)){
//			error = true;
//			view = spnPERSONA_ID_ORDEN;
//			mensaje = "Debe seleccionar nombres";
//			return false;
//		}
//		if(Util.esVacio(bean.qhs3_1a)){
//			error = true;
//			view = txtQHS3_1A;
//			mensaje = "debe ingresar cuantas veces recibio apoyo";
//			return false;
//		}
//		return true;
//	}
//	private void inicio() {
//	}
//
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService
//					.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//	public void textoPreguntas(){
//		if(bean.pregunta_id==App.PERSONAS_COVID){
//			lblNombre.text(R.string.hogarqh107_1n);
//			lblTiempo.text(R.string.hogarqh107_1t);
//		}
//	}
//	public void tituloPreguntas(){
//		if(bean.pregunta_id==App.WAWAMAS){
//			getDialog().setTitle("BENEFICIARIOS WAWA WASI/CUNA M�S");
//		}else if(bean.pregunta_id==App.QALIWARMA){			
//			getDialog().setTitle("BENEFICIARIOS QALI WARMA");
//		}										           	
//	}
//	
//	public void onPERSONA_ID_ORDENChangeValue(){
//		boolean entro=false;
//		uiToEntity(bean);
//		if(bean.persona_id_orden!=null){
//			
//			Seccion01 band = new Seccion01();
//			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
//			
//			
//		}		
//	}
//}
