package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2016.fragment.hogar.dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2016.R;
//import gob.inei.endes2016.common.App;
//import gob.inei.endes2016.common.EndesCalendario;
//import gob.inei.endes2016.fragment.hogar.HogarFragment_017;
//import gob.inei.endes2016.model.Beneficiario;
//import gob.inei.endes2016.model.Seccion01;
//import gob.inei.endes2016.service.CuestionarioService;
//import gob.inei.endes2016.service.Seccion01Service;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class HogarDialog_017  extends DialogFragmentComponent {
//
//	
//	@FieldAnnotation(orderIndex = 1) 
//	public RadioGroupOtherField rgPC701;
//	@FieldAnnotation(orderIndex = 2)
//	public RadioGroupOtherField rgPC702;
//	@FieldAnnotation(orderIndex = 3)
//	public RadioGroupOtherField rgPC703;
//	@FieldAnnotation(orderIndex = 4) 
//	public TextField txtPC703_O; 
//	@FieldAnnotation(orderIndex =5)
//	public SpinnerField spnPC704;
//	
//	
//	public ButtonComponent btnAceptar,btnCancelar;
//	private static HogarFragment_017 caller;
//	LinearLayout q0, q1, q2, q3, q4;
//	private CuestionarioService cuestionarioService;
//	private SeccionCapitulo[] seccionesCargado,seccionesGrabado;
//	private LabelComponent  lblpc701, lblpc702, lblpc703,lblpc704,lblinf701;
//	public ArrayList<String> nombresAll;
//	public ArrayList<Beneficiario> Beneficiaros;
//	public List<Seccion01> posiblesBeneficiarios;
//
//	
//	public IntegerField txt1,txt2,txt3,txt4;
//	public Seccion01 bean; 
//	
//	public int anios=0;
//	
//	public int val_109r=-1;
//	
//	private Seccion01Service seccion01service;
//
//	public static HogarDialog_017 newInstance(FragmentForm pagina, Seccion01 detalle) {
//		caller = (HogarFragment_017) pagina;
//		HogarDialog_017 f = new HogarDialog_017();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", detalle);
//		f.setArguments(args);
//		return f;
//	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		bean = (Seccion01) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		getDialog().setTitle(R.string.precenso_titulo);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//
//	public HogarDialog_017(){
//		super();
//		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PC701","PC702","PC703","PC703_O","PC704","PERSONA_ID","HOGAR_ID","ID")};
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PC701","PC702","PC703","PC703_O","PC704","QH02_1","PERSONA_ID","HOGAR_ID","ID")};
//	}
//	
//	@Override
//	protected void buildFields() {
//		lblpc701=new LabelComponent(getActivity()).size(MATCH_PARENT, 750).text(R.string.precenso_pre701).negrita().textSize(18);
//		lblpc702=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.precenso_pre702).negrita().textSize(18);
//		lblpc703=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.precenso_pre703).negrita().textSize(18);
//		lblpc704=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.precenso_pre704).negrita().textSize(18);
//		
//		rgPC701=new RadioGroupOtherField(this.getActivity(), R.string.precenso_pre701_1, R.string.precenso_pre701_2, R.string.precenso_pre701_3, R.string.precenso_pre701_4, R.string.precenso_pre701_5, R.string.precenso_pre701_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onPC701ChangeValue");
//		rgPC702=new RadioGroupOtherField(this.getActivity(), R.string.precenso_pre702_1, R.string.precenso_pre702_2).size(WRAP_CONTENT,WRAP_CONTENT).callback("onPC702changevalue");
//		rgPC703=new RadioGroupOtherField(this.getActivity(), R.string.precenso_pre703_1, R.string.precenso_pre703_2,R.string.precenso_pre703_3,R.string.precenso_pre703_4,R.string.precenso_pre703_5,R.string.precenso_pre703_6,R.string.precenso_pre703_7,R.string.precenso_pre703_8,R.string.precenso_pre703_9,R.string.precenso_pre703_10,R.string.precenso_pre703_11,R.string.precenso_pre703_12,R.string.precenso_pre703_13).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtPC703_O = new TextField(getActivity()).size(altoComponente, 420);
//		rgPC703.agregarEspecifique(12, txtPC703_O);
//		
//		lblinf701= new LabelComponent(getActivity()).size(WRAP_CONTENT, MATCH_PARENT).text(R.string.precenso_pre701_2_inf);
//		rgPC701.agregarTitle(1,lblinf701);
//		
//		spnPC704 = new SpinnerField(getActivity()).size(altoComponente + 10, 360).callback("onPC704ChangeValue");
//		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		
//		btnCancelar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				bean.restoreFromMemento(caretaker.get("antes"));
//				HogarDialog_017.this.dismiss();
//			}
//		});
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				caller.cargarTabla();
//				HogarDialog_017.this.dismiss();
//			}
//		});
//	}
//
//
//	
////	@Override
////	protected View createUI() {
////		buildFields();
////		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpc701,rgPC701);
////		q2 = createQuestionSection(0,lblpc702,rgPC702);
////		q3 = createQuestionSection(0,lblpc703,rgPC703);
////		q4 = createQuestionSection(0,lblpc704,spnPC704);
////		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
////		ScrollView contenedor = createForm();
////		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
////		form.addView(q1);
////		form.addView(q2);
////		form.addView(q3);
////		form.addView(q4);
////		form.addView(botones);
////		return contenedor;
////	}
//	
//	private void cargarDatos() {
////		bean=getCuestionarioService().getSeccion01(bean.id, bean.hogar_id, bean.persona_id, seccionesCargado);
////		EndesCalendario.llenarResponsablesPreCensos(getActivity(), spnPC704, bean.id, bean.hogar_id);
////		entityToUI(bean);
////		caretaker.addMemento("antes", bean.saveToMemento(Seccion01.class));
////		inicio();
//	}
//	
//	
//	public boolean grabar() {
//		uiToEntity(bean);
////		if (!validar()) {
////			if (error) {
////				if (!mensaje.equals(""))
////					ToastMessage.msgBox(this.getActivity(), mensaje,
////							ToastMessage.MESSAGE_ERROR,
////							ToastMessage.DURATION_LONG);
////				if (view != null)
////					view.requestFocus();
////			}
////			return false;
////		}
////		try {
////			getCuestionarioService().saveOrUpdate(bean, seccionesGrabado);
////		} catch (SQLException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
//		
//		return true;
//	}
//
//	private boolean validar() {
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
////		if(Util.esVacio(bean.pc701)){
////			error = true;
////			view = rgPC701;
////			mensaje = preguntaVacia.replace("$", "P701. ");
////			return false;
////		}
////		if(!Util.esDiferente(bean.pc701,1,2,3,6)){
////			if(Util.esVacio(bean.pc704)){
////				error = true;
////				view = spnPC704;
////				mensaje = preguntaVacia.replace("$", "P704. ");
////				return false;
////			}
////		}
////		if(!Util.esDiferente(bean.pc701, 4)){
////			if(Util.esVacio(bean.pc702)){
////				error = true;
////				view = rgPC702;
////				mensaje = preguntaVacia.replace("$", "P702. ");
////				return false;
////			}
////			if(Util.esVacio(bean.pc704)){
////				error = true;
////				view = spnPC704;
////				mensaje = preguntaVacia.replace("$", "P704. ");
////				return false;
////			}
////		}
////		if(!Util.esDiferente(bean.pc701, 5)){
////			if(Util.esVacio(bean.pc703)){
////				error = true;
////				view = rgPC703;
////				mensaje = preguntaVacia.replace("$", "P703. ");
////				return false;
////			}
////			if(!Util.esDiferente(bean.pc703, 13)){
////				if(Util.esVacio(bean.pc703_o)){
////					error = true;
////					view = txtPC703_O;
////					mensaje = preguntaVacia.replace("$", "P703 Otro. ");
////					return false;
////				}
////			}
////			if(Util.esVacio(bean.pc704)){
////				error = true;
////				view = spnPC704;
////				mensaje = preguntaVacia.replace("$", "P704. ");
////				return false;
////			}
////		}
//		return true;
//	}
//	
//	private void inicio() {
////		onPC701ChangeValue();
////		ReemplazarNombres();
////		ValidarsiesSupervisora();
//	}
//	
////	public void onPC701ChangeValue(){
////		Integer valor=rgPC701.getValue()==null?0:(Integer)rgPC701.getTagSelected("0");
////		if(!Util.esDiferente(valor,1,2,3,6) && Util.esDiferente(valor, 0)){
////			Log.e("","111 ");
////			Util.cleanAndLockView(getActivity(), rgPC702,rgPC703);
////			q2.setVisibility(View.GONE);
////			q3.setVisibility(View.GONE);
////			q4.setVisibility(View.VISIBLE);
////		}
////		else{
////			if(!Util.esDiferente(valor, 5)){
////				Log.e("","222");
////				q2.setVisibility(View.GONE);
////				Util.cleanAndLockView(getActivity(), rgPC702);
////			}
////			else{
////				Log.e("","33333 ");
////				Util.lockView(getActivity(), false,rgPC701,rgPC702,rgPC703,spnPC704);
////				q2.setVisibility(View.VISIBLE);
////				q3.setVisibility(View.VISIBLE);
////				q4.setVisibility(View.VISIBLE);	
////				onPC702changevalue();
////			}
////		}
////	}
////	public void onPC702changevalue(){
////		Integer valor=rgPC702.getValue()==null?0:(Integer)rgPC702.getTagSelected("0");
////		if(!Util.esDiferente(valor, 1,2) && Util.esDiferente(valor, 0)){
////			Log.e("","44444");
////			Util.cleanAndLockView(getActivity(), rgPC703);
////			q3.setVisibility(View.GONE);
////			q4.setVisibility(View.VISIBLE);
////		}
////		else{
////			Log.e("","55555 ");
////			Util.lockView(getActivity(),false, rgPC703,spnPC704);
////			q3.setVisibility(View.VISIBLE);
////			q4.setVisibility(View.VISIBLE);
////		}
////	}
////	
////	private void ReemplazarNombres(){
////		String replace="(NOMBRE)";
////		String nombre=bean.qh02_1;
////		lblpc701.setText(lblpc701.getText().toString().replace(replace, nombre));
////	}
////
////	
////
////	public void ValidarsiesSupervisora(){
////		Integer codigo=App.getInstance().getUsuario().cargo_id;
////		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
////			Util.cleanAndLockView(getActivity(), btnAceptar);
////			rgPC701.readOnly();
////			rgPC702.readOnly();
////			rgPC703.readOnly();
////			txtPC703_O.readOnly();
////			spnPC704.readOnly();
////		}
////	}
////	
////	
////	public Seccion01Service getSeccion01Service() { 
////		if(seccion01service==null){ 
////			seccion01service = Seccion01Service.getInstance(getActivity()); 
////		} 
////		return seccion01service; 
////    }
////	
////	public CuestionarioService getCuestionarioService() {
////		if (cuestionarioService == null) {
////			cuestionarioService = CuestionarioService
////					.getInstance(getActivity());
////		}
////		return cuestionarioService;
////	}
//
//}
