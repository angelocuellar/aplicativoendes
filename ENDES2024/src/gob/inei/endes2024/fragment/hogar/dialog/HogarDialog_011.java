package gob.inei.endes2024.fragment.hogar.dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.HogarFragment_010;
import gob.inei.endes2024.fragment.hogar.HogarFragment_011;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarDialog_011 extends DialogFragmentComponent {
	public interface BeneficiarioDialogListener {
		void onFinishEditDialog(String inputText);
	}

	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERSONA_ID_ORDEN;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnQHS3_1A;
	@FieldAnnotation(orderIndex = 3) 
	public CheckBoxField chbQHS3_1A; 
	@FieldAnnotation(orderIndex =4)
	public SpinnerField spnQHS3_1M;
	@FieldAnnotation(orderIndex = 5)
	public CheckBoxField chbQHS3_1M; 
	@FieldAnnotation(orderIndex = 6)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnCancelar;
	@FieldAnnotation(orderIndex = 8) 
	public RadioGroupOtherField rgQH96AC;
	@FieldAnnotation(orderIndex=9)
	public IntegerField txtQH97DNI;
	@FieldAnnotation(orderIndex=10) 
	public DateTimeField txtQH97_FECHA;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQH13;
//	@FieldAnnotation(orderIndex=10) 
//	public SpinnerField spnQH97D;
//	@FieldAnnotation(orderIndex = 11) 
//	public CheckBoxField chbQH97D;
//	@FieldAnnotation(orderIndex=12) 
//	public SpinnerField spnQH97M;
//	@FieldAnnotation(orderIndex = 13) 
//	public CheckBoxField chbQH97M;
//	@FieldAnnotation(orderIndex=14) 
//	public SpinnerField spnQH97A;
//	@FieldAnnotation(orderIndex = 15) 
//	public CheckBoxField chbQH97A;	
	
	private static HogarFragment_011 caller;
	LinearLayout q0, q1, q2, q3, q4;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesGrabadoB,seccionesGrabadoS1,seccionesCargadoVisita;
	private CuestionarioService hogarService;
	private LabelComponent lblNombre, lblTiempo, lblAnios, lblMeses, lblCompromiso, lblAcuerdo,lblAcDni,lblAcFecha, lblACdias, lblACmes,lblACanio,lblpregunta13;
	private GridComponent2 gridJefe;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public List<Seccion01> posiblesBeneficiarios;

	private Seccion03Service beneficiario;
	private Seccion01Service seccion01;
	
	public IntegerField txt1,txt2,txt3,txt4;
	public Seccion03 bean; 
    	
	public int anios=6,meses=11;
	public int val_96AC=-1;
	
	private Seccion01Service seccion01service; 
	
	public VisitaService visita;
	public Visita visita_bean;
	
	public int fecha_inicio=0,fecha_afiliacion=0;
	public int qh13=0;
	
	public boolean P13=false, fecha_afil_anio_actual=true, fecha_afil_mes_actual=true;

	public static HogarDialog_011 newInstance(FragmentForm pagina, Seccion03 detalle) {
		caller = (HogarFragment_011) pagina;
		HogarDialog_011 f = new HogarDialog_011();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public HogarDialog_011(){
		super();
		seccionesGrabadoB = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","PREGUNTA_ID","PERSONA_ID","QH96AC","QH97DNI","QH97D","QH97M","QH97A")};
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PREGUNTA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M")};
		
		seccionesCargadoVisita = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"NRO_VISITA","QHVDIA_INI","QHVMES_INI","QHVANIO_INI")};
		
		seccionesGrabadoS1 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH13") };
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion03) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setTitle("BENEFICIARIOS JUNTOS");
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	private Seccion03Service getServiceBeneficiario() {
        if (beneficiario == null) {
        	beneficiario = Seccion03Service.getInstance(getActivity());
        }
        return beneficiario;
	}
	private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
	private VisitaService getServiceVisita() {
        if (visita == null) {
        	visita = VisitaService.getInstance(getActivity());
        }
        return visita;
	}
	
	private void cargarDatos() {
		
		Log.e("hola nicol", "que hace");
		
		MyUtil.LiberarMemoria();
		fecha_afil_anio_actual = true;
		String dia="",mes="",anio="";
		visita_bean = getServiceVisita().getVisitaCompletada(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id , seccionesCargadoVisita);
		
		if(visita_bean.qhvanio_ini!=null){
			
			anio = "" + visita_bean.qhvanio_ini; 
		}
		if(visita_bean.qhvmes_ini!=null){
			
			mes = "" + visita_bean.qhvmes_ini;
			if(visita_bean.qhvmes_ini.length()<2){
				mes = "0" + visita_bean.qhvmes_ini;
			}
		}
		if(visita_bean.qhvdia_ini!=null){
			
			dia = "" + visita_bean.qhvdia_ini;
			if(visita_bean.qhvdia_ini.length()<2){
				dia = "0" + visita_bean.qhvdia_ini;
			}
		}
		
		String fecha_ini= anio+mes+dia;
		fecha_inicio = Integer.parseInt(fecha_ini);
				
		
		llenarSpinnerBeneficiarios(bean);
		bean.IniciarQh97_fecha();
//		if(bean.IniciarQh97_fecha()){
//			bean.qh97d=bean.qh97d!=null?bean.qh97d+"/"+bean.qh97m:null;
//		}
		entityToUI(bean);
		if (bean.qhs3_1a!=null) {
			muestraPregunta_y(bean.qhs3_1a);
	    }
		if (bean.qhs3_1m!=null) {
			muestraPregunta_m(bean.qhs3_1m);
		}
		caretaker.addMemento("antes", bean.saveToMemento(Seccion03.class));
		
		if(bean.persona_id_orden!=null){
			if(bean.pregunta_id==App.JUNTOS){
	        	anios = 11;
	        }else if(bean.pregunta_id==App.PENSION65){
	        	anios = 5;
	        }
		}
		inicio();
	}
	
	public void llenarSpinnerBeneficiarios(Seccion03 seccion3)
	{
		switch (seccion3.pregunta_id) {
		case App.JUNTOS	:if(seccion3.persona_id_orden!=null) 
						{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMAJUNTOS,App.EDADMAXIMAJUNTOS,seccion3.pregunta_id);
						Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
						else{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMAJUNTOS,App.EDADMAXIMAJUNTOS,seccion3.pregunta_id);}
						break;
		case App.PENSION65:if(seccion3.persona_id_orden!=null)
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMAPENSION65,App.EDADMAXIMAPENSION65,seccion3.pregunta_id);
							Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
							else
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMAPENSION65,App.EDADMAXIMAPENSION65,seccion3.pregunta_id);}
							break;
		}
	}
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(gridJefe.component());
		q2 = createQuestionSection(lblpregunta13,rgQH13);
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(botones);
		return contenedor;
	}
	@Override
	protected void buildFields() {
		chbQHS3_1A=new CheckBoxField(this.getActivity(), R.string.hogarqh92_1NSNR, "1:0").size(altoComponente+10,220).callback("on96yChangeValue");
		chbQHS3_1M=new CheckBoxField(this.getActivity(), R.string.hogarqh92_1NSNR, "1:0").size(altoComponente+10,220).callback("on96mChangeValue");
		lblTiempo = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 10, 630).text(R.string.hogarqh92_1t).textSize(18).alinearIzquierda();
		lblNombre = new LabelComponent(this.getActivity()).size(altoComponente + 20, 270).text(R.string.hogarqh92_1n).textSize(16).negrita().alinearIzquierda();
		lblAnios = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh92_1a).textSize(16).negrita().alinearIzquierda();		
		lblMeses = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh92_1m).textSize(16).negrita().alinearIzquierda();
		lblAcDni = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh97dni_1).textSize(16).negrita().alinearIzquierda();
		lblAcFecha = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh97d_1).textSize(16).negrita().alinearIzquierda();
		lblCompromiso = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 10, 630).text(R.string.hogarqh96ac).textSize(16).alinearIzquierda();
		lblAcuerdo = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 50, 630).text(R.string.hogarqh97).textSize(17).alinearIzquierda();
		
		rgQH96AC=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh96ac_1,R.string.hogarqh96ac_2).
				size(altoComponente + 20, 270).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH97AChangeValue");
		
		txtQH97DNI=new IntegerField(this.getActivity()).size(altoComponente + 10, 270).maxLength(8);
		
		textoPreguntas();
		
		spnPERSONA_ID_ORDEN = new SpinnerField(getActivity()).size(altoComponente + 10, 360).callback("onPERSONA_ID_ORDENChangeValue");
		spnQHS3_1A=new SpinnerField(getActivity()).size(70, 140).callback("on96_yChangeValue");
		cargarSpinnerqh_y();
		spnQHS3_1M=new SpinnerField(getActivity()).size(70, 140).callback("on96_mChangeValue");
		cargarSpinnerqh_m();
		
		txtQH97_FECHA=new DateTimeField(this.getActivity(),TIPO_DIALOGO.FECHA ,"dd/MM/yyyy").size(altoComponente, 360).setRangoYear(2005, App.ANIOPORDEFECTOSUPERIOR).callback("on97_FECHAChangeValue");
		
		gridJefe = new GridComponent2(this.getActivity(), 3);
		gridJefe.addComponent(lblNombre);
		gridJefe.addComponent(spnPERSONA_ID_ORDEN,2,1);
		gridJefe.addComponent(lblTiempo,3,1);
		gridJefe.addComponent(lblAnios);
		gridJefe.addComponent(spnQHS3_1A);
		gridJefe.addComponent(chbQHS3_1A);
		gridJefe.addComponent(lblMeses);
		gridJefe.addComponent(spnQHS3_1M);
		gridJefe.addComponent(chbQHS3_1M);
		
		if(bean.pregunta_id == App.JUNTOS){
			gridJefe.addComponent(lblCompromiso,3,1);
			gridJefe.addComponent(rgQH96AC,3,1);
			gridJefe.addComponent(lblAcuerdo,3,1);
			gridJefe.addComponent(lblAcDni);
			gridJefe.addComponent(txtQH97DNI,2,1);
			
			gridJefe.addComponent(lblAcFecha);
			gridJefe.addComponent(txtQH97_FECHA,2,1);
		}
		
		lblpregunta13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh13);
		rgQH13=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh13_1,R.string.seccion01qh13_2,R.string.seccion01qh13_3,R.string.seccion01qh13_4,R.string.seccion01qh13_5,R.string.seccion01qh13_6,R.string.seccion01qh13_7,R.string.seccion01qh13_8,R.string.seccion01qh13_9,R.string.seccion01qh13_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH13ChangeValue");
				
		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnAceptar).size(200, 55);
		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnCancelar).size(200, 55);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				HogarDialog_011.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				if(bean.pregunta_id==App.JUNTOS){
					Util.cleanAndLockView(caller.getActivity(), caller.btnAgregarJuntos);
					caller.btnAgregarJuntos.setVisibility(View.GONE);
				}
				caller.refrescarBeca18(bean);
//				caller.cargarTabla();
				
				if(bean.pregunta_id==App.JUNTOS){
					caller.PosiblesPension65();
				}
				
				HogarDialog_011.this.dismiss();
			}
		});
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
	public boolean grabar() {
		uiToEntity(bean);
		bean.qhs3_1a=combinaPregunta_y();
		bean.qhs3_1m=combinaPregunta_m();
		
//		if(bean.qh97d!=null){
//			bean.qh97d=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "dd"));
//			bean.qh97m=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "MM"));
//			bean.qh97a=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "yyyy"));
//		}
		
		if(bean.qh97_fecha!=null){
			bean.qh97d=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "dd"));
			bean.qh97m=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "MM"));
			bean.qh97a=Integer.parseInt(Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "yyyy"));
			
			String fecha_afil = Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "yyyy")+Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "MM")+Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "dd");
			fecha_afiliacion = Integer.parseInt(fecha_afil);
			
		}
		
//		bean.qh97d=combinaPreguntaQH97D();
//		bean.qh97m=combinaPreguntaQH97M();
//		bean.qh97a=combinaPreguntaQH97A();
		
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getServiceBeneficiario().saveOrUpdate(bean, dbTX,seccionesGrabadoB);
			if (!flag) {
				throw new Exception(
						"Ocurri� un problema al grabar beneficiario JUNTOS");
			}
			getServiceBeneficiario().commitTX(dbTX);
			if(P13){
				Seccion01 band = new Seccion01();
				band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);					
				if(bean.pregunta_id==App.PENSION65){
					band.qh13=qh13;
					
					flag = getServiceSeccion01().saveOrUpdate(band, dbTX,seccionesGrabadoS1);
					if (!flag) {
						throw new Exception(
								"Ocurri� un problema al grabar beneficiario JUNTOS");
					}
					getServiceSeccion01().commitTX(dbTX);
				}
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}

	private boolean validar() {
		String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
		fecha_afil_anio_actual = true;
		if (Util.esVacio(bean.persona_id_orden)) {
			error = true;
			view = spnPERSONA_ID_ORDEN;
			mensaje = preguntaVacia.replace("$", "Seleccione La pregunta P.96");
			return false;
		}
//		if(bean.pregunta_id==App.PENSION65){
//			if(qh13==8){
//				mensaje = "La persona no debe tener el codigo 08 en pregunta 13, CAMBIAR EL CODIGO!!!";
//   	   	   		error = true; 
//   	   	   		return false;
//			}
//		}
		if(Util.esVacio(bean.qhs3_1a)){
				error = true;
				view = spnQHS3_1A;
				mensaje = preguntaVacia.replace("$", "Seleccione A�os de la pregunta P.96_3");
				return false;
		}
		if(Util.esVacio(bean.qhs3_1m)){
				error = true;
				view = spnQHS3_1M;
				mensaje = preguntaVacia.replace("$", "Seleccione Meses de la pregunta P.96_3");
				return false;
		}
		if(!Util.esDiferente(bean.qhs3_1a, 0) && !Util.esDiferente(bean.qhs3_1m, 0)){
			error = true;
			view = spnQHS3_1A;
			mensaje = "El Mes y A�o no pueden ser cero a la vez!!!";
			return false;
		}		
		if(bean.pregunta_id==App.JUNTOS){
			if(Util.esVacio(bean.qh96ac)){
				error = true;
				view = rgQH96AC;
				mensaje = preguntaVacia.replace("$", "Seleccione si Muestra el acuerdo de Compromiso,");
				return false;
			}
			if(bean.qh96ac==1){			
				if(Util.esVacio(bean.qh97dni)){
					error = true;
					view = txtQH97DNI;
					mensaje = preguntaVacia.replace("$", "Ingrese el numero del DNI");
					return false;
				}
				if(bean.qh97dni.length()<8){
					error = true;
					view = txtQH97DNI;
					mensaje = "El DNI debe ser de longitud 8!!!";
					return false;
				}
				Calendar calendario = new GregorianCalendar();
				Integer anio_actual= calendario.get(Calendar.YEAR);
				Integer mes_actual= calendario.get(Calendar.MONTH)+1;
				
//				Log.e("mes_actual: ",""+mes_actual);
				
				if (Util.esVacio(bean.qh97d)) { 
	   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.97"); 
	   	   	   		view = txtQH97_FECHA; 
	   	   	   		error = true; 
	   	   	   		return false; 
	   	   	   	}
				if(fecha_afiliacion>fecha_inicio){
					mensaje = "La fecha de Afiliacion no debe ser posterior al dia de la entrevista. P.97"; 
	   	   	   		view = txtQH97_FECHA; 
	   	   	   		error = true; 
	   	   	   		return false; 
				}
				if(mes_actual>=bean.qh97m){
					if((anio_actual - bean.qh97a)!=bean.qhs3_1a){
						anios = anio_actual - bean.qh97a;
						if(anios>App.PREGUNTA_JUNTOS_MAXIMO_ANIO){
							anios=App.PREGUNTA_JUNTOS_MAXIMO_ANIO;
						}
						fecha_afil_anio_actual = false;
						cargarSpinnerqh_y();
						error = true;
						view = spnQHS3_1A;
						mensaje = "Los A�os no corresponden con la fecha de afiliacion, cambiarlo!!!";
						//return false;
					}
					if((mes_actual-bean.qh97m)!=bean.qhs3_1m){
						meses = mes_actual-bean.qh97m;
						cargarSpinnerqh_m();
						error = true;
						view = spnQHS3_1M;
						mensaje = "Los Meses no corresponden con la fecha de afiliacion, cambiarlo!!!";
						//return false;
					}
				}else{
					if((anio_actual - bean.qh97a - 1)!=bean.qhs3_1a){
						anios = anio_actual - bean.qh97a - 1;
						if(anios>App.PREGUNTA_JUNTOS_MAXIMO_ANIO){
							anios=App.PREGUNTA_JUNTOS_MAXIMO_ANIO;
						}
						fecha_afil_anio_actual = false;
						cargarSpinnerqh_y();
						error = true;
						view = spnQHS3_1A;
						mensaje = "Los A�os no corresponden con la fecha de afiliacion, cambiarlo!!!";
						//return false;
					}
					if((bean.qh97m - mes_actual + bean.qhs3_1m)!=12){
						meses = 12 - (bean.qh97m - mes_actual);
						cargarSpinnerqh_m();
						error = true;
						view = spnQHS3_1M;
						mensaje = "Los Meses no corresponden con la fecha de afiliacion, cambiarlo!!!";
						//return false;
					}
				}
//				if((anio_actual - bean.qh97a)!= bean.qhs3_1a){
//					Log.e("anios: ",""+anios);
//					Log.e("anio_actual: ",""+anio_actual);
//					Log.e("bean.qh97a: ",""+bean.qh97a);
//					if(anios>(anio_actual - bean.qh97a)){
//						anios = anio_actual - bean.qh97a;
//						fecha_afil_anio_actual = false;
//						if(anios>10){
//							anios=10;
//						}
//						cargarSpinnerqh_y();
//					}
//					error = true;
//					view = spnQHS3_1A;
//					mensaje = "La pregunta P.96_3 no corresponde con la fecha de afiliacion, cambiarla!!!";
//					return false;
//				}
			}
		}
		if(P13){
			if(qh13==8){
				q2.setVisibility(View.VISIBLE);
				mensaje = "La persona no debe tener el codigo 08 en pregunta 13, CAMBIAR EL CODIGO!!!"; 
   	   	   		view = rgQH13; 
   	   	   		error = true; 
   	   	   		return false;
			}
		}
		return true;
	}
	private void inicio() {
		q2.setVisibility(View.GONE);
		
		on96_yChangeValue();
		on96yChangeValue();
		on96_mChangeValue();
		on96mChangeValue();
		if(bean.pregunta_id==App.JUNTOS){
			onQH97AChangeValue();
		}
		spnQHS3_1A.requestFocus();
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
    public Integer combinaPregunta_y() {
	    Integer p_y=null;
//	    if(bean.pregunta_id==App.JUNTOS){
//        	anios = 11;
//        }else if(bean.pregunta_id==App.PENSION65){
//        	anios = 5;
//        }	  
//	    Log.e("","ANIO: " +anios);
	    if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString() )) {
	   	 	p_y= Integer.parseInt(spnQHS3_1A.getValue().toString());
	    }
	    if (chbQHS3_1A.getValue().toString().equals("1")) {
	   	 	p_y= bean.getConvertqh92((Integer.parseInt(chbQHS3_1A.getValue().toString())));
	    }
	    return p_y;         
    }
    public void muestraPregunta_y(Integer p92y) {
//    	if(bean.pregunta_id==App.JUNTOS){
//        	anios = 11;
//        }else if(bean.pregunta_id==App.PENSION65){
//        	anios = 5;
//        }
        if (p92y>=0 && p92y<=anios) {        
              spnQHS3_1A.setSelection(p92y+1);
        }            
        else {
              if (p92y==98) {                
				 chbQHS3_1A.setChecked(true);
			}                        
        }
    }
	private void cargarSpinnerqh_y() {
		if(bean.persona_id_orden!=null){
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			if(bean.pregunta_id==App.JUNTOS && fecha_afil_anio_actual){
				anios = band.qh07 - 15;
				if(anios>App.PREGUNTA_JUNTOS_MAXIMO_ANIO){
					anios=App.PREGUNTA_JUNTOS_MAXIMO_ANIO;
				}
			}
			if(bean.pregunta_id==App.PENSION65){
				anios = band.qh07 - 65;
				if(anios>App.PREGUNTA_PENSION65_MAXIMO_ANIO){
					anios=App.PREGUNTA_PENSION65_MAXIMO_ANIO;
				}
			}
			if(bean.pregunta_id==App.PENSION65){
				qh13 = band.qh13;
				
			}
		}
		
		List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        
        String[] items1 = new String[anios+2];
        items1[0] = "Selec";
        
        for(int i=0;i<=anios;i++){
        	keys.add(i);
        	items1[i+1] = ""+i;
        }     
        
//        String[] items1 = new String[] {"Selec","1","2","3","4","5","6"};
//        
//        if(bean.pregunta_id==App.JUNTOS){
//        	items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11"};
//        }else if(bean.pregunta_id==App.PENSION65){
//        	items1 = new String[] {"Selec","0","1","2","3","4","5"};
//        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1A.setAdapterWithKey(adapter, keys);
    }
	
	
    public Integer combinaPregunta_m() {
	    Integer p_m=null;
	    if (MyUtil.incluyeRango(0,11,spnQHS3_1M.getValue().toString() )) {
	   	 	p_m= Integer.parseInt(spnQHS3_1M.getValue().toString());
	    }
	    if (chbQHS3_1M.getValue().toString().equals("1")) {
	   	 	p_m= bean.getConvertqh92((Integer.parseInt(chbQHS3_1M.getValue().toString())));
	    }
	    return p_m;         
    }
    public void muestraPregunta_m(Integer p_m) {
        if (p_m>=0 && p_m<=11) {        
              spnQHS3_1M.setSelection(p_m+1);
        }            
        else {
              if (p_m==98) {                
				 chbQHS3_1M.setChecked(true);
			}                        
        }
    }
	private void cargarSpinnerqh_m() {
		String[] items1 = null;
		List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        
        if(bean.pregunta_id==App.JUNTOS){
        	items1 = new String[meses+2];
            items1[0] = "Selec";
        	for(int i=0;i<=meses;i++){
            	keys.add(i);
            	items1[i+1] = ""+i;
            }
        }else{        
	        for(int i=0;i<=11;i++){
	        	keys.add(i);
	        }
	        items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11"};
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1M.setAdapterWithKey(adapter, keys);
    }
	public void on96_yChangeValue() {
		if(bean.pregunta_id==App.JUNTOS){
        	anios = App.PREGUNTA_JUNTOS_MAXIMO_ANIO;
        }else if(bean.pregunta_id==App.PENSION65){
        	anios = App.PREGUNTA_PENSION65_MAXIMO_ANIO;
        }
		if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),chbQHS3_1A);
			Util.lockView(getActivity(), false,chbQHS3_1A);
			spnQHS3_1M.requestFocus();
		} 
//		else {
////			Util.lockView(getActivity(), false,chbQHS3_1A);
//			chbQHS3_1A.requestFocus();			
//		}
	}
	public void on96yChangeValue() {
		if (MyUtil.incluyeRango(1,1,chbQHS3_1A.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),spnQHS3_1A);
			Util.lockView(getActivity(), false,spnQHS3_1A);
			spnQHS3_1M.requestFocus();				
		} else {
//			Util.lockView(getActivity(), false,spnQHS3_1A);
			spnQHS3_1A.requestFocus();			
		}
	}
	public void on96_mChangeValue() {
		if (MyUtil.incluyeRango(0,12,spnQHS3_1M.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),chbQHS3_1M);
			Util.lockView(getActivity(), false,chbQHS3_1M);
			btnAceptar.requestFocus();
			if(bean.pregunta_id==App.JUNTOS){
				rgQH96AC.requestFocus();
			}
		} else {
//			Util.lockView(getActivity(), false,chbQHS3_1M);
			chbQHS3_1M.requestFocus();			
		}
	}
	public void on96mChangeValue() {	
		if (MyUtil.incluyeRango(1,1,chbQHS3_1M.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),spnQHS3_1M);
			Util.lockView(getActivity(), false,spnQHS3_1M);
			btnAceptar.requestFocus();
			if(bean.pregunta_id==App.JUNTOS){
				rgQH96AC.requestFocus();
			}				
		} else {
//			Util.lockView(getActivity(), false,spnQHS3_1M);
			spnQHS3_1M.requestFocus();			
		}			
	}
	
//	public void onrgQH96AC(){
//		int valor = Integer.parseInt(rgQH96AC.getTagSelected("0").toString());
//		val_96AC = valor;

//		if (Util.esDiferente(valor, 1,1)) {
//			Util.cleanAndLockView(this.getActivity(),txtQH97_FECHANI,spnQH97D);
//			btnAceptar.requestFocus();
//		} else { 
//            Util.lockView(getActivity(), false,txtQH97_FECHANI,spnQH97D);
//            txtQH97_FECHANI.requestFocus();
//		}
//	}
	
	public void textoPreguntas(){
		if(bean.pregunta_id==App.JUNTOS){
			getDialog().setTitle("JUNTOS");
			lblNombre.text(R.string.hogarqh96_1n);
			lblTiempo.text(R.string.hogarqh96_1t);
		}else if(bean.pregunta_id==App.PENSION65){		
			getDialog().setTitle("PENSI�N 65");
			lblNombre.text(R.string.hogarqh100_1n);
			lblTiempo.text(R.string.hogarqh100_1t);
		}
	}
	
	public void onQH97DChangeValue(){}
	
	public void onQH97MChangeValue(){}
	
	public void onQH97AChangeValue(){
		if (MyUtil.incluyeRango(2,2,rgQH96AC.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),txtQH97DNI,txtQH97_FECHA);
			bean.qh97d = null;
			bean.qh97m = null;
			bean.qh97a = null;
			btnAceptar.requestFocus();				
		} else {
			Util.lockView(getActivity(), false,txtQH97DNI,txtQH97_FECHA);
			txtQH97DNI.requestFocus();			
		}		
	}
	
	public void on97D_ChangeValue(){}
	
	public void on97M_ChangeValue(){}
	
	public void on97A_ChangeValue(){}
	
	public void onPERSONA_ID_ORDENChangeValue(){
		boolean entro=false;
		P13 = false;
		q2.setVisibility(View.GONE);
		uiToEntity(bean);
		if(bean.persona_id_orden!=null){
			
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			if(bean.pregunta_id==App.JUNTOS && fecha_afil_anio_actual){
				entro = true;
				anios = band.qh07 - 15;
				if(anios>App.PREGUNTA_JUNTOS_MAXIMO_ANIO){
					anios=App.PREGUNTA_JUNTOS_MAXIMO_ANIO;
				}
			}
			if(bean.pregunta_id==App.PENSION65){
				entro = true;
				anios = band.qh07 - 65;
				if(anios>App.PREGUNTA_PENSION65_MAXIMO_ANIO){
					anios=App.PREGUNTA_PENSION65_MAXIMO_ANIO;
				}
			}
			if(entro){
				cargarSpinnerqh_y();				
			}
			if(bean.pregunta_id==App.PENSION65){
				qh13 = band.qh13;
			
				if(qh13==8){
					P13 = true;
			
			
			
					CambiarNombreLabels(band);
//					q2.setVisibility(View.VISIBLE);
					rgQH13.lockButtons(true, 7);
				}
			}
		}		
	}
	
	
	public void onQH13ChangeValue(){
		
		qh13 = Integer.parseInt(rgQH13.getValue().toString());
		
		qh13=getConvertqh13(qh13);
		
		btnAceptar.requestFocus();	
	}
	
	
	public void on97_FECHAChangeValue(){
		String fecha_afil = Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "yyyy")+Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "MM")+Util.getFechaFormateada((Date) txtQH97_FECHA.getValue(), "dd");
			
	}
	
	//on97_FECHAChangeValue
	
	public void CambiarNombreLabels(Seccion01 beanS1){
		String valoracambiar="(NOMBRE)";
		String Ud="Usted";
		Date date= new Date();
   		int dias=MyUtil.getDayOfTheWeek(date);
   		Calendar Fecha_Actual = new GregorianCalendar();
   		String primero="";
   		String segundo="";
   		Calendar segunda_fecha;
   		if(dias==Fecha_Actual.SATURDAY && Fecha_Actual.get(Calendar.AM_PM) ==1){
   			primero=Fecha_Actual.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(Fecha_Actual.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(Fecha_Actual, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		else{
   			Calendar primera_fecha = MyUtil.PrimeraFecha(Fecha_Actual, dias);
   			primero=primera_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(primera_fecha.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(primera_fecha, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace(valoracambiar,"("+beanS1.qh02_1+")"));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F1",segundo));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F2",primero));
	}
	
	public Integer getConvertqh13(Integer qh13) {
		Integer valorReturn=0;
		switch (qh13){
		case 9: valorReturn=96; break;
		case 10: valorReturn=98;break;
		default: valorReturn=qh13;
		}
		return valorReturn;
	}
}