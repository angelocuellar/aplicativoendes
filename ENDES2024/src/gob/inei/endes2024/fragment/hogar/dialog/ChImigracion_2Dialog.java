package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2024.fragment.hogar.dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextAutCompleteField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0002;
//import gob.inei.endes2024.model.CISECCION_08;
//import gob.inei.endes2024.model.IMIGRACION;
//import gob.inei.endes2024.model.Ubigeo;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import gob.inei.endes2024.service.UbigeoService;
//import gob.inei.endes2024.service.VisitaService;
//
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.Html;
//import android.text.Spanned;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class ChImigracion_2Dialog extends DialogFragmentComponent  {
//	@FieldAnnotation(orderIndex=1) 
//	public TextField txtQH35B_1; 
//	@FieldAnnotation(orderIndex=2) 
//	public TextField txtQH35B_11; 
//	@FieldAnnotation(orderIndex=3) 
//	public TextField txtQH35B_2; 
//	@FieldAnnotation(orderIndex=4) 
//	public TextField txtQH35B_3;
//
//	
//	@FieldAnnotation(orderIndex=5)
//	public IntegerField txtQH35C_A; 
//	@FieldAnnotation(orderIndex=6)
//	public IntegerField txtQH35C_M;
//	
//	@FieldAnnotation(orderIndex=7)
//	public TextAutCompleteField txtQH35D_CCDD;
//	@FieldAnnotation(orderIndex=8)
//	public CheckBoxField chbQH35D_CCDD_NS;
//	@FieldAnnotation(orderIndex=9)
//	public TextAutCompleteField txtQH35D_CCPP;
//	@FieldAnnotation(orderIndex=10)
//	public CheckBoxField chbQH35D_CCPP_NS;
//	@FieldAnnotation(orderIndex=11)
//	public TextAutCompleteField txtQH35D_CCDI;
//	@FieldAnnotation(orderIndex=12)
//	public CheckBoxField chbQH35D_CCDI_NS;	
//	@FieldAnnotation(orderIndex=13)
//	public TextAutCompleteField txtQH35D_PAIS;
//	@FieldAnnotation(orderIndex=14)
//	public CheckBoxField chbQH35D_PAIS_NS;
//	
//	@FieldAnnotation(orderIndex=15) 
//	public RadioGroupOtherField rgQH35E; 	
//	@FieldAnnotation(orderIndex=16) 
//	public TextField txtQH35EO; 
//	@FieldAnnotation(orderIndex=17) 
//	public TextAreaField txtQH35E_OBSERVACION;
//	
//	@FieldAnnotation(orderIndex=18) 
//	public RadioGroupOtherField rgQH35F; 	
//	@FieldAnnotation(orderIndex=19) 
//	public TextField txtQH35FO;
//	
//	@FieldAnnotation(orderIndex=20) 
//	public SpinnerField spnQH35G;
//	
//	@FieldAnnotation(orderIndex=21)
//	public TextAreaField txtQH35_OBSERVACION;
//	
//	@FieldAnnotation(orderIndex=22) 
//	public ButtonComponent btnAceptar;
//	
//	
//	public static HogarFragment_0002 caller;	
//	IMIGRACION persona;
//	CISECCION_08 individual; 
//	private CuestionarioService cuestionarioService;
//	private VisitaService visitaService;
//	private UbigeoService ubigeoService;
//	private Seccion01Service serviceSeccion01;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	private static String nombre=null;
//	public GridComponent2 gridqh35b,gridqh35c,gridqh35d;
//	LabelComponent lblpreguntaqh34_t,lblobs_35,
//	lblpreguntaqh35b,lblpreguntaqh35b_1,lblpreguntaqh35b_11,lblpreguntaqh35b_2,lblpreguntaqh35b_3,
//	lblpreguntaqh35c,lblpreguntaqh35c_a,lblpreguntaqh35c_m,
//	lblpreguntaqh35d, lblpreguntaqh35e_obs,lblpreguntaqh35d_n,lblpreguntaqh35d_ndd,lblpreguntaqh35d_npp,lblpreguntaqh35d_ndi,lblpreguntaqh35d_e,lblpreguntaqh35d_ep,
//	lblpreguntaqh35e,lblpreguntaqh35f,lblpreguntaqh35g,lblpreguntaqh35d_ndiS;
//	public ButtonComponent  btnCancelar;
//	public CheckBoxField chbP910;
//	String fecha_referencia;
//	private String operador="-";
//	
//	LinearLayout q0;
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5,q6,q7;
//	
//	public static ChImigracion_2Dialog newInstance(FragmentForm pagina,IMIGRACION detalle, int position, List<IMIGRACION> detalles) {
//		caller = (HogarFragment_0002) pagina;
////		personas=detalles;
//		ChImigracion_2Dialog f = new ChImigracion_2Dialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", (IMIGRACION) detalles.get(position));
//		f.setArguments(args);
//		return f;
//	}
//	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (IMIGRACION) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
////		getDialog().setTitle("N� "+persona.qh34a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public ChImigracion_2Dialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "QH35C_A", "QH35C_M", "QH35D_CCDD", "QH35D_CCDD_NS", "QH35D_CCPP", "QH35D_CCPP_NS", "QH35D_CCDI", "QH35D_CCDI_NS", "QH35D_PAIS", "QH35D_PAIS_NS", "QH35E", "QH35EO", "QH35E_OBSERVACION", "QH35F", "QH35FO", "QH35F_REF", "QH35G", "QH35_OBSERVACION", "ID", "HOGAR_ID")};
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "QH35C_A", "QH35C_M", "QH35D_CCDD", "QH35D_CCDD_NS", "QH35D_CCPP", "QH35D_CCPP_NS", "QH35D_CCDI", "QH35D_CCDI_NS", "QH35D_PAIS", "QH35D_PAIS_NS", "QH35E", "QH35EO", "QH35E_OBSERVACION", "QH35F", "QH35FO", "QH35F_REF", "QH35G", "QH35_OBSERVACION")};
//	}            
//	
//
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		lblpreguntaqh34_t = new LabelComponent(this.getActivity(),	App.ESTILO)	.size(MATCH_PARENT, 750).text(R.string.migracion34_t).textSize(20).centrar().negrita();
//		
//		lblpreguntaqh35b= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35b);
//		lblpreguntaqh35b_1= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_1);
//		lblpreguntaqh35b_11= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_11);
//		lblpreguntaqh35b_2= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_2);
//		lblpreguntaqh35b_3= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_3);
//		
//		
//		lblpreguntaqh35c= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35c);
//		lblpreguntaqh35c_a= new LabelComponent(getActivity()).size(MATCH_PARENT, 100).textSize(16).text(R.string.migracion35c_a).alinearDerecha();
//		lblpreguntaqh35c_m= new LabelComponent(getActivity()).size(MATCH_PARENT, 100).textSize(16).text(R.string.migracion35c_m).alinearDerecha();
//		
//		lblpreguntaqh35d= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35d);		
//		lblpreguntaqh35d_n= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.migracion35d_n).negrita();
//		lblpreguntaqh35d_ndd= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion35d_ndd);
//		lblpreguntaqh35d_npp= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion35d_npp);
//		lblpreguntaqh35d_ndi= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion35d_ndi);
//		lblpreguntaqh35d_ndiS= new LabelComponent(getActivity()).size(MATCH_PARENT, 400).textSize(16).text("");
//		lblpreguntaqh35d_e= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.migracion35d_e).negrita();
//		lblpreguntaqh35d_ep= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion35d_ep);
//		
//		lblpreguntaqh35e= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35e);
//		lblpreguntaqh35e_obs= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.migracion35e_obs).negrita();
//		lblpreguntaqh35f= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35f);
//		lblpreguntaqh35g= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35g);
//	
//		txtQH35B_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH35B_11 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH35B_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH35B_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		
//		txtQH35B_1.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//				if(s.toString().length()>0)
//					RenombrarEtiquetassiCambioNombre();
//				else
//					RenombrarEtiquetas();
//			}
//		});
//		
//		gridqh35b = new GridComponent2(this.getActivity(),App.ESTILO, 2);
//		gridqh35b.addComponent(lblpreguntaqh35b_1);
//		gridqh35b.addComponent(txtQH35B_1);
//		gridqh35b.addComponent(lblpreguntaqh35b_11);
//		gridqh35b.addComponent(txtQH35B_11);
//		gridqh35b.addComponent(lblpreguntaqh35b_2);
//		gridqh35b.addComponent(txtQH35B_2);
//		gridqh35b.addComponent(lblpreguntaqh35b_3);
//		gridqh35b.addComponent(txtQH35B_3);
//		
//				
//		txtQH35C_A=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
//		txtQH35C_M =new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
//		
//		txtQH35D_CCDD = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 400);
//		txtQH35D_CCPP = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 400);
//		txtQH35D_CCDI = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 750);
//		txtQH35D_PAIS = new TextAutCompleteField(getActivity()).size(altoComponente, 400);
//		
//		chbQH35D_CCDD_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onQH35D_CCDD_nsChangeValue");
//		chbQH35D_CCPP_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onQH35D_CCPP_nsChangeValue");
//		chbQH35D_CCDI_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onQH35D_CCDI_nsChangeValue");		
//		chbQH35D_PAIS_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onQH35D_PAIS_nsChangeValue");
//		
//		lblobs_35 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//		txtQH35E_OBSERVACION = new TextAreaField(getActivity()).maxLength(1000).size(150, 700).alfanumerico();
//		txtQH35_OBSERVACION = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
//		
//		CargarDepartamento();
//		txtQH35D_CCDD.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
//            	String ccdd=item.substring(0,2);
//            	CargarProvincia(ccdd);
//            	CargarDistritoProvincia(ccdd);
//            	txtQH35D_CCPP.requestFocus();
//            }
//        });
//		txtQH35D_CCDD.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				txtQH35D_CCPP.setText("");
//				txtQH35D_CCDI.setText("");
//				if(!chbQH35D_CCDD_NS.isChecked() && !chbQH35D_PAIS_NS.isChecked()){
//					if(s.toString().trim().length()>0){
//						Util.cleanAndLockView(getActivity(),txtQH35D_PAIS,chbQH35D_PAIS_NS);					
//					}
//					else{
//						Util.lockView(getActivity(),false, txtQH35D_PAIS,chbQH35D_PAIS_NS);
//					}	
//				}
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH35D_CCDD.setText(s);
//			         txtQH35D_CCDD.setSelection(txtQH35D_CCDD.getText().length());
//			      }
//			}
//		});
//		
//		
//		txtQH35D_CCPP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
//            	String ccpp=item.substring(0,2);
//            	String departamento=txtQH35D_CCDD.getText().toString();
//            	CargarDistrito(departamento.substring(0,2), ccpp);
//            	txtQH35D_CCDI.setText("");
//            	txtQH35D_CCDI.requestFocus();
//            }
//        });
//		txtQH35D_CCPP.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				//txtQH35D_CCDI.setText("");
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH35D_CCPP.setText(s);
//			         txtQH35D_CCPP.setSelection(txtQH35D_CCPP.getText().length());
//			      }
//			}
//		});
//		
//		txtQH35D_CCDI.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
////            	String ccpp=item.substring(0,2);
////            	String departamento=txtQH35D_CCDD.getText().toString();
////            	CargarDistrito(departamento.substring(0,2), ccpp);
////            	txtQH35D_CCDI.setText("");
//            	if(item.indexOf(operador)>0){
//	            	String provincia=item.substring(0,item.indexOf(operador)-1);
//	            	String distrito=item.substring(item.indexOf(operador)+2,item.length());
//	            	txtQH35D_CCDI.setText(distrito);
//	            	txtQH35D_CCPP.setText(provincia);
//            	}
//            	rgQH35E.requestFocus();
//            	OcultarTecla();
//            }
//        });
//
//		txtQH35D_CCDI.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH35D_CCDI.setText(s);
//			         txtQH35D_CCDI.setSelection(txtQH35D_CCDI.getText().length());
//			      }
//			}
//		});
//		
//		
//		txtQH35D_PAIS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	OcultarTecla();
//            }
//        });
//		CargarPaises();
//		txtQH35D_PAIS.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				if(!chbQH35D_CCDD_NS.isChecked() && !chbQH35D_PAIS_NS.isChecked()){
//					if(s.toString().trim().length()>0){
//						Util.cleanAndLockView(getActivity(),txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCDD_NS, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);					
//					}
//					else{
//						Util.lockView(getActivity(),false, txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCDD_NS, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//					}	
//				}
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH35D_PAIS.setText(s);
//			         txtQH35D_PAIS.setSelection(txtQH35D_PAIS.getText().length());
//			      }
//			}
//		});
//		
//		
//		
//		
//		
//		rgQH35E=new RadioGroupOtherField(this.getActivity(),R.string.migracion35e_1,R.string.migracion35e_2,R.string.migracion35e_3,R.string.migracion35e_4,R.string.migracion35e_5,R.string.migracion35e_6,R.string.migracion35e_7,R.string.migracion35e_8,R.string.migracion35e_9,R.string.migracion35e_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQH35EO = new TextField(getActivity()).size(altoComponente, 600).maxLength(100);	
//		rgQH35E.agregarEspecifique(8,txtQH35EO);
//		
//		rgQH35F=new RadioGroupOtherField(this.getActivity(),R.string.migracion35f_1,R.string.migracion35f_2,R.string.migracion35f_3,R.string.migracion35f_4,R.string.migracion35f_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQH35FO = new TextField(getActivity()).size(altoComponente, 600).maxLength(100);	
//		rgQH35F.agregarEspecifique(3,txtQH35FO);
//
//		spnQH35G=new SpinnerField(getActivity()).size(altoComponente+15, 500);
//	
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//
//		
//		gridqh35c = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
//		gridqh35c.addComponent(lblpreguntaqh35c_a);
//		gridqh35c.addComponent(txtQH35C_A);
//		gridqh35c.addComponent(lblpreguntaqh35c_m);		
//		gridqh35c.addComponent(txtQH35C_M);
//		
//		gridqh35d = new GridComponent2(this.getActivity(),3);
//		gridqh35d.addComponent(lblpreguntaqh35d_n,3);
//		gridqh35d.addComponent(lblpreguntaqh35d_ndd);
//		gridqh35d.addComponent(txtQH35D_CCDD);
//		gridqh35d.addComponent(chbQH35D_CCDD_NS);
//		gridqh35d.addComponent(lblpreguntaqh35d_npp);
//		gridqh35d.addComponent(txtQH35D_CCPP);
//		gridqh35d.addComponent(chbQH35D_CCPP_NS);
//		gridqh35d.addComponent(lblpreguntaqh35d_ndi);
//		gridqh35d.addComponent(lblpreguntaqh35d_ndiS);		
//		gridqh35d.addComponent(chbQH35D_CCDI_NS);
//		gridqh35d.addComponent(txtQH35D_CCDI,3);
//		gridqh35d.addComponent(lblpreguntaqh35d_e,3);
//		gridqh35d.addComponent(lblpreguntaqh35d_ep);
//		gridqh35d.addComponent(txtQH35D_PAIS);
//		gridqh35d.addComponent(chbQH35D_PAIS_NS);
//		
//		
//		
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				ChImigracion_2Dialog.this.dismiss();
//			}
//		});
//		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				ChImigracion_2Dialog.this.dismiss();
//				caller.recargardatos();
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q1 = createQuestionSection(0,lblpreguntaqh35b,gridqh35b.component());
//		q2 = createQuestionSection(0,lblpreguntaqh35c,gridqh35c.component());
//		q3 = createQuestionSection(0,lblpreguntaqh35d,gridqh35d.component());
//		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh35e,rgQH35E,lblpreguntaqh35e_obs,txtQH35E_OBSERVACION);
//		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh35f,rgQH35F);
//		q7 = createQuestionSection(0,lblpreguntaqh35g,spnQH35G);
//		q6 = createQuestionSection(lblobs_35,txtQH35_OBSERVACION);
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q4); 
//		form.addView(q5);	
//		form.addView(q7);	
//		form.addView(q6);
//		
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar(){
//		uiToEntity(persona);
//		
////		if (persona.qh35f_ref!=null) {						
//			if(fecha_referencia == null) {
//				persona.qh35f_ref = Util.getFechaActualToString();
//			}
//			else {
//				 persona.qh35f_ref = fecha_referencia;
//			}	
////		}
////		else {
////			persona.qh35f_ref = null;
////		}
//		
//		
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag=false;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		}
//		//Log.e("grabar",""+persona.qh35g);
//		return flag;
//	}
//
//
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if (Util.esVacio(persona.qh35b_1)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35b_1"); 
//			view = txtQH35B_1; 
//			error = true; 
//			return false; 
//		}
//		
//		
//		if (Util.esVacio(persona.qh35c_a)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35c_a"); 
//			view = txtQH35C_A; 
//			error = true; 
//			return false; 
//		}
//		if (Util.esVacio(persona.qh35c_m)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35c_m"); 
//			view = txtQH35C_M; 
//			error = true; 
//			return false; 
//		}
//		if (persona.qh35c_a<App.ANIOPORDEFECTO_MIGRACION || persona.qh35c_a>App.ANIOPORDEFECTOSUPERIOR) {
//			error = true;
//			view = txtQH35C_A;
//			mensaje = "A�o ingresado fuera de rango";
//			return false;
//		}
//		if (persona.qh35c_m<=0 || persona.qh35c_m>12) {
//			error = true;
//			view = txtQH35C_M;
//			mensaje = "Mes ingresado fuera de rango";
//			return false;
//		}
//		
//		Calendar fechaactual = new GregorianCalendar();
//    	if(fecha_referencia!=null){			
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fecha_referencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual=cal;			
//		}
//		Integer anio_act = fechaactual.get(Calendar.YEAR);
//		Integer mes_act = fechaactual.get(Calendar.MONTH)+1;
//		//Log.e("mes::",""+ mes_act);
//		
//		if (!Util.esDiferente(persona.qh35c_a,anio_act)) {
//			if (Util.esMayor(persona.qh35c_m,mes_act)) {
//				error = true;
//				view = txtQH35C_M;
//				mensaje = "Mes ingresado fuera de rango.";
//				return false;
//			}
//		}
//		
//		
//		Integer qh35DPaisFexiste = txtQH35D_PAIS.getText().toString().trim().length()>0?1:0;
//		Integer qh35D_ccddFexiste = txtQH35D_CCDD.getText().toString().trim().length()>0?1:0;
//		Integer qh35D_ccppFexiste = txtQH35D_CCPP.getText().toString().trim().length()>0?1:0;
//		Integer qh35D_ccdiFexiste = txtQH35D_CCDI.getText().toString().trim().length()>0?1:0;
//		
//		if(qh35DPaisFexiste==0 && qh35D_ccddFexiste==0 && !chbQH35D_CCDD_NS.isChecked() && !chbQH35D_PAIS_NS.isChecked()){
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35D"); 
//			view = txtQH35D_CCDD; 
//			error = true; 
//			return false; 
//		}
//		if(qh35D_ccddFexiste==1){
//			if(qh35D_ccppFexiste==0 && !chbQH35D_CCPP_NS.isChecked()){
//				mensaje = preguntaVacia.replace("$", "La pregunta Provincia"); 
//				view = txtQH35D_CCPP; 
//				error = true; 
//				return false; 
//			}
//			if(qh35D_ccppFexiste==1){
//				if(qh35D_ccdiFexiste==0 && !chbQH35D_CCDI_NS.isChecked()){
//					mensaje = preguntaVacia.replace("$", "La pregunta Distrito"); 
//					view = txtQH35D_CCDI; 
//					error = true; 
//					return false; 
//				}
//			}
//		}
//		
//		
//		if (Util.esVacio(persona.qh35e)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35e"); 
//			view = rgQH35E; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(persona.qh35e,9)){ 
//			if (Util.esVacio(persona.qh35eo)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//				view = txtQH35EO; 
//				error = true; 
//				return false; 
//			} 
//		}
//		if(!Util.esDiferente(persona.qh35e,7) || !Util.esDiferente(persona.qh35e,8) || !Util.esDiferente(persona.qh35e,9)){ 
//			if (Util.esVacio(persona.qh35e_observacion)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Observaciones"; 
//				view = txtQH35E_OBSERVACION; 
//				error = true; 
//				return false; 
//			} 
//		}
//		
//		
//		if (Util.esVacio(persona.qh35f)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35f"); 
//			view = rgQH35F; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(persona.qh35f,4)){ 
//			if (Util.esVacio(persona.qh35f)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//				view = txtQH35FO; 
//				error = true; 
//				return false; 
//			} 
//		}
//		
//		if (Util.esVacio(persona.qh35g)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.35g"); 
//			view = spnQH35G; 
//			error = true; 
//			return false; 
//		}
//		
//		
//	    return true;
//	}
//	public void OcultarTecla() {
//		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(rgQH35E.getWindowToken(), 0);
//	}
//	
//	private void cargarDatos() {      
//		persona = getCuestionarioService().getPersonaImigracion(persona.id,persona.hogar_id,persona.qh35a,seccionesCargado);
//		if (persona == null) {
//			persona = new IMIGRACION();
//			persona.id = App.getInstance().getMarco().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.qh35a = persona.qh35a;
//		}
//		nombre=persona.qh35b_1;
//		MyUtil.llenarPersonasInformanteMigracion(this.getActivity() , getServiceSeccion01(), spnQH35G, persona.id, persona.hogar_id);
//		entityToUI(persona);
//		fecha_referencia = persona.qh35f_ref;
//		if(persona!=null && persona.qh35d_ccdi!=null){
//			txtQH35D_CCDI.setText("");
//			txtQH35D_CCDI.setText(persona.qh35d_ccdi);	
//		}
//
//		inicio();
//    }
//
//	public void inicio(){
//		RenombrarEtiquetas();
//		ValidarsiesSupervisora();
//		Integer qh34ccddFexiste = txtQH35D_CCDD.getText().toString().trim().length()>0?1:0;
//		Integer qh34paisFexiste = txtQH35D_PAIS.getText().toString().trim().length()>0?1:0;
//		
//		if(qh34ccddFexiste==1 || chbQH35D_CCDD_NS.isChecked()){
//			onQH35D_CCDD_nsChangeValue();	
//		}
//		if(qh34paisFexiste==1 || chbQH35D_PAIS_NS.isChecked()){
//			onQH35D_PAIS_nsChangeValue();	
//		}
//		
//		if(persona.qh35d_ccdd!=null){
//			CargarProvincia(persona.qh35d_ccdd.substring(0,2));
//		}
//		if(persona.qh35d_ccdd!=null && persona.qh35d_ccpp!=null){
//			CargarDistrito(persona.qh35d_ccdd.substring(0,2),persona.qh35d_ccpp.substring(0,2));	
//		}
//		txtQH35C_A.requestFocus();
//	}
//	
//	public void onQH35D_CCDD_nsChangeValue(){
//		if(chbQH35D_CCDD_NS.isChecked()){
//			rgQH35E.requestFocus();
//			Util.cleanAndLockView(getActivity(),chbQH35D_CCPP_NS);
//			Util.cleanAndLockView(getActivity(),chbQH35D_PAIS_NS);
//			Util.cleanAndLockView(getActivity(),chbQH35D_CCDI_NS);
//			Util.cleanAndLockView(getActivity(),txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, txtQH35D_PAIS);
//		}
//		else{
//			Integer qh34ccddFexiste = txtQH35D_CCDD.getText().toString().trim().length()>0?1:0;
//			if(qh34ccddFexiste==1){
//				Util.cleanAndLockView(getActivity(),txtQH35D_PAIS,chbQH35D_PAIS_NS);
//				Util.lockView(getActivity(),false ,txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//			}
//			else{
//				Util.lockView(getActivity(),false, txtQH35D_PAIS,chbQH35D_PAIS_NS);
//				Util.lockView(getActivity(),false ,txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//			}
//			onQH35D_CCPP_nsChangeValue();
//		}	
//	}
//	
//	
//	public void onQH35D_PAIS_nsChangeValue(){
//		if(chbQH35D_PAIS_NS.isChecked()){
//			rgQH35E.requestFocus();
//			Util.cleanAndLockView(getActivity(),txtQH35D_CCDD,txtQH35D_CCPP,txtQH35D_CCDI, txtQH35D_PAIS, chbQH35D_CCDD_NS, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//		}
//		else{
//			Integer qh34paisFexiste = txtQH35D_PAIS.getText().toString().trim().length()>0?1:0;
//			if(qh34paisFexiste==1){
//				Util.cleanAndLockView(getActivity(),txtQH35D_CCDD,txtQH35D_CCPP, txtQH35D_CCDI, chbQH35D_CCDD_NS, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//				Util.lockView(getActivity(),false,txtQH35D_PAIS,chbQH35D_PAIS_NS);
//			}
//			else{
//				Util.lockView(getActivity(),false, txtQH35D_CCDD,txtQH35D_CCPP, txtQH35D_CCDI, chbQH35D_CCDD_NS, chbQH35D_CCPP_NS, chbQH35D_CCDI_NS);
//				Util.lockView(getActivity(),false, txtQH35D_PAIS,chbQH35D_PAIS_NS);
//				onQH35D_CCPP_nsChangeValue();
//			}
//			
//		}
//	}	
//	
//	
//	
//	public void onQH35D_CCPP_nsChangeValue(){
//		if(chbQH35D_CCPP_NS.isChecked()){
//			Util.cleanAndLockView(getActivity(),txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCDI_NS);
//		}
//		else{
//			Util.lockView(getActivity(),false ,txtQH35D_CCPP,txtQH35D_CCDI, chbQH35D_CCDI_NS);
//			onQH35D_CCDI_nsChangeValue();
//		}
//	}
//	public void onQH35D_CCDI_nsChangeValue(){
//		if(chbQH35D_CCDI_NS.isChecked()){
//			Util.cleanAndLockView(getActivity(),txtQH35D_CCDI);
//		}
//		else{
//			Util.lockView(getActivity(),false ,txtQH35D_CCDI);
//		}
//	}
//	
//	
//	
//	private void CargarDepartamento() {
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, App.DEPARTAMENTOS);
//	    txtQH35D_CCDD.setAdapter(adapter);
//	    txtQH35D_CCDD.setThreshold(1);
//	}
//	
//	private void CargarProvincia(String ccdd){
//		List<Ubigeo> datos=getUbigeoService().getProvincias(ccdd);
//		String[] provincias=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			provincias[columna]=u.ccpp+" "+u.provincia;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, provincias);
//		txtQH35D_CCPP.setAdapter(adapter);
//	    txtQH35D_CCPP.setThreshold(1);
//	}
//	private void CargarDistrito(String ccdd,String ccpp){
//		List<Ubigeo> datos=getUbigeoService().getDistritos(ccdd, ccpp);
//		String[] distritos=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			distritos[columna]=u.ccdi+" "+u.distrito;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, distritos);
//		txtQH35D_CCDI.setAdapter(adapter);
//	    txtQH35D_CCDI.setThreshold(1);
//	}
//	private void CargarDistritoProvincia(String ccdd){
//		List<Ubigeo> datos=getUbigeoService().getDistritoByccdd(ccdd);
//		String[] distritos=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			distritos[columna]=u.ccpp+" "+u.provincia+" "+operador+" "+u.ccdi+" "+u.distrito;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, distritos);
//		txtQH35D_CCDI.setAdapter(adapter);
//	    txtQH35D_CCDI.setThreshold(1);
//	}
//	
//	private void CargarPaises(){
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, App.PAISES);
//	    txtQH35D_PAIS.setAdapter(adapter);
//	    txtQH35D_PAIS.setThreshold(1);
//	}
//	
//	public void EstadoOriginalDeEqtiquetas(){
//		lblpreguntaqh35c.setText(getResources().getString(R.string.migracion35c));
//		lblpreguntaqh35d.setText(getResources().getString(R.string.migracion35d));
//		lblpreguntaqh35e.setText(getResources().getString(R.string.migracion35e));
//		lblpreguntaqh35f.setText(getResources().getString(R.string.migracion35f));
//	}
//	
//	public void RenombrarEtiquetas(){
//		String replace ="(NOMBRE)";
//		EstadoOriginalDeEqtiquetas();
//		lblpreguntaqh35c.setText(lblpreguntaqh35c.getText().toString().replace(replace, nombre));
//		lblpreguntaqh35d.setText(lblpreguntaqh35d.getText().toString().replace(replace, nombre));
//		lblpreguntaqh35e.setText(lblpreguntaqh35e.getText().toString().replace(replace, nombre));
//		lblpreguntaqh35f.setText(lblpreguntaqh35f.getText().toString().replace(replace, nombre));
//		RenombrarFechaPregunta35f();
//		
//		//lblpreguntaqh29mobs.setText(lblpreguntaqh29mobs.getText().toString().replace("#", App.ANIOPORDEFECTO+""));
//	}
//	
//	public void RenombrarEtiquetassiCambioNombre(){
//		if(txtQH35B_1.getText().toString().length()>0){
//			String replace =nombre;
//			String nombre = txtQH35B_1.getText().toString();
//			EstadoOriginalDeEqtiquetas();
//			RenombrarEtiquetas();
//			lblpreguntaqh35c.setText(lblpreguntaqh35c.getText().toString().replace(replace, nombre));
//			lblpreguntaqh35d.setText(lblpreguntaqh35d.getText().toString().replace(replace, nombre));
//			lblpreguntaqh35e.setText(lblpreguntaqh35e.getText().toString().replace(replace, nombre));			
//			lblpreguntaqh35f.setText(lblpreguntaqh35f.getText().toString().replace(replace, nombre));
//			RenombrarFechaPregunta35f();			
//		}
//	}
//	
//    public void RenombrarFechaPregunta35f(){
//    	Calendar fechaactual = new GregorianCalendar();
//    	if(fecha_referencia!=null){			
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fecha_referencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual=cal;			
//		}
//    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
//    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
//    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
//    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
//    	lblpreguntaqh35f.setText(lblpreguntaqh35f.getText().toString().replace("F1", F2)); 
//    	lblpreguntaqh35f.setText(lblpreguntaqh35f.getText().toString().replace("F2", F1));
//    	
//    	String F3="";
//    	if(F1.equals("Diciembre")){
//    		F3 = "de este a�o";
//    	}
//    	else{
//    		F3 = "del pr�ximo a�o";    		
//    	}
//
//    	lblpreguntaqh35f.setText(lblpreguntaqh35f.getText().toString().replace("F3", F3));
//    
//    }
//    
//
//	
//	
//
//	
//	public void ValidarsiesSupervisora(){
//	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		txtQH35B_1.readOnly();
//	    		txtQH35B_2.readOnly();
//	    		txtQH35B_3.readOnly();	    		
//	    		txtQH35C_A.readOnly();
//	    		txtQH35C_M.readOnly();
//	    		
//	    		txtQH35D_CCDD.readOnly();
//	    		txtQH35D_CCPP.readOnly();
//	    		txtQH35D_CCDI.readOnly();
//	    		txtQH35D_PAIS.readOnly();
//	    		chbQH35D_CCDD_NS.readOnly();
//	    		chbQH35D_CCPP_NS.readOnly();
//	    		chbQH35D_CCDI_NS.readOnly();
//	    		chbQH35D_PAIS_NS.readOnly();
//	    		
//	    		rgQH35E.readOnly(); 
//	    		txtQH35EO.readOnly();
//	    		rgQH35F.readOnly(); 
//	    		txtQH35FO.readOnly();
//	    		txtQH35E_OBSERVACION.setEnabled(false);
//	    		spnQH35G.readOnly();
//	    		btnAceptar.setEnabled(false);
//	    	}
//	}
//	    
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//	
//	public VisitaService getVisitaService()
//	{
//		if(visitaService==null)
//		{
//				visitaService = VisitaService.getInstance(getActivity());
//		}
//			return visitaService;
//	}
//	public UbigeoService getUbigeoService()
//	{
//		if(ubigeoService==null)		{
//			ubigeoService = UbigeoService.getInstance(getActivity());
//		}
//			return ubigeoService;
//	}
//	public Seccion01Service getServiceSeccion01() {
//		if (serviceSeccion01 == null) {
//			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
//		}
//		return serviceSeccion01;
//	}
//}
