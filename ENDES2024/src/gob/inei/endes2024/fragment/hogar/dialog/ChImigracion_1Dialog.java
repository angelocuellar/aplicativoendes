package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2024.fragment.hogar.dialog;
//
//import java.sql.SQLException;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0002;
//import gob.inei.endes2024.model.IMIGRACION;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class ChImigracion_1Dialog extends DialogFragmentComponent {
//	@FieldAnnotation(orderIndex=1) 
//	public TextField txtQH35B_1; 
//	@FieldAnnotation(orderIndex=2) 
//	public TextField txtQH35B_11; 
//	@FieldAnnotation(orderIndex=3) 
//	public TextField txtQH35B_2; 
//	@FieldAnnotation(orderIndex=4) 
//	public TextField txtQH35B_3; 
//	
//	@FieldAnnotation(orderIndex=5) 
//	public ButtonComponent btnAceptar;
//
//	public static HogarFragment_0002 caller;
//	IMIGRACION persona;
//	private CuestionarioService cuestionarioService;
//	 private Seccion01Service Personaservice;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	
//	public GridComponent2 gridqh35b;
//	public ButtonComponent  btnCancelar;
//	public LabelComponent lblpreguntaqh35b,lblpreguntaqh35b_1,lblpreguntaqh35b_11,lblpreguntaqh35b_2,lblpreguntaqh35b_3;
//	public Seccion01 informantehogar;
//	LinearLayout q0;
//	LinearLayout q1;
//	
//	public static ChImigracion_1Dialog newInstance(FragmentForm pagina, IMIGRACION detalle) {
//		caller = (HogarFragment_0002) pagina;
//		ChImigracion_1Dialog f = new ChImigracion_1Dialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", detalle);
//		f.setArguments(args);
//		return f;
//	}
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (IMIGRACION) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().setTitle("N� "+persona.qh35a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public ChImigracion_1Dialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "ID","HOGAR_ID") };
//		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "ID","HOGAR_ID")}; 
//	}            
//	
//
//	
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		
//		lblpreguntaqh35b= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion35b);
//		lblpreguntaqh35b_1= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_1);
//		lblpreguntaqh35b_11= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_11);
//		lblpreguntaqh35b_2= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_2);
//		lblpreguntaqh35b_3= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion35b_3);
//		
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		
//		txtQH35B_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH35B_11 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH35B_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		txtQH35B_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
//		
//		gridqh35b = new GridComponent2(this.getActivity(),App.ESTILO, 2);
//		gridqh35b.addComponent(lblpreguntaqh35b_1);
//		gridqh35b.addComponent(txtQH35B_1);
//		gridqh35b.addComponent(lblpreguntaqh35b_11);
//		gridqh35b.addComponent(txtQH35B_11);
//		gridqh35b.addComponent(lblpreguntaqh35b_2);
//		gridqh35b.addComponent(txtQH35B_2);
//		gridqh35b.addComponent(lblpreguntaqh35b_3);
//		gridqh35b.addComponent(txtQH35B_3);
//		
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				ChImigracion_1Dialog.this.dismiss();
//			Util.lockView(getActivity(), false,caller.btnAgregar);
//			}
//		});
//		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = false;
//				flag=grabar();
//				if(!flag){
//					return;
//				}
//				caller.refrescarPersonas(persona);
//				ChImigracion_1Dialog.this.dismiss();
//				if(Integer.valueOf(caller.txtQH35_N.getText().toString())>caller.imigracion.size()){
//					caller.agregarPersona();
//				}
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q1 = createQuestionSection(lblpreguntaqh35b,gridqh35b.component());
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q1);
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar() {
//		uiToEntity(persona);
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag = true;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		  } catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		} 
//		
//		return flag;
//	}
//	
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if (persona.qh35b_1  == null) {
//			error = true;
//			view = txtQH35B_1;
//			mensaje = preguntaVacia.replace("$", "La pregunta Nombre");
//			return false;
//		}		
//	    return true;
//	}
//	
//	private void cargarDatos() { 
//		persona = getCuestionarioService().getPersonaImigracion(persona.id,persona.hogar_id,persona.qh35a,seccionesCargado);
//		if (persona == null) {
//			persona = new IMIGRACION();
//			persona.id = App.getInstance().getMarco().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.qh35a= caller.imigracion.size()+1;
//		}
//		entityToUI(persona);
//		inicio();
//    }
//	
//	public void inicio(){
//	}
//	
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//    public Seccion01Service getPersonaService(){
//    	if(Personaservice == null){
//    		Personaservice = Seccion01Service.getInstance(getActivity());
//    	}
//    	return Personaservice;
//    }
//
//}
