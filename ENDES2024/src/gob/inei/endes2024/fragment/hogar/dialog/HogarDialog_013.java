package gob.inei.endes2024.fragment.hogar.dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.HogarFragment_010;
import gob.inei.endes2024.fragment.hogar.HogarFragment_011;
import gob.inei.endes2024.fragment.hogar.HogarFragment_012;
import gob.inei.endes2024.fragment.hogar.HogarFragment_014;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarDialog_013  extends DialogFragmentComponent {
	public interface BeneficiarioDialogListener {
		void onFinishEditDialog(String inputText);
	}
	@FieldAnnotation(orderIndex = 1) 
	public RadioGroupOtherField rgQH109_1R;;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnPERSONA_ID_ORDEN;
	@FieldAnnotation(orderIndex = 3)
	public SpinnerField spnQHS3_1A;
	@FieldAnnotation(orderIndex = 4) 
	public CheckBoxField chbQHS3_1A; 
	@FieldAnnotation(orderIndex =5)
	public SpinnerField spnQHS3_1M;
	@FieldAnnotation(orderIndex = 6)
	public CheckBoxField chbQHS3_1M; 
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex = 8)
	public ButtonComponent btnCancelar;
	private static HogarFragment_014 caller;
	LinearLayout q0, q1, q2, q3, q4;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesGrabadoB;
	private CuestionarioService hogarService;
	private LabelComponent lblNombre, lblTiempo, lblAnios, lblMeses, lblAlimentar;
	private GridComponent2 gridJefe;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public List<Seccion01> posiblesBeneficiarios;

	private Seccion03Service beneficiario;
	private Seccion01Service seccion01;
	
	public IntegerField txt1,txt2,txt3,txt4;
	public Seccion03 bean; 
	
	public int anios=0;
	
	public int val_109r=-1;
	
	private Seccion01Service seccion01service;

	public static HogarDialog_013 newInstance(FragmentForm pagina, Seccion03 detalle) {
		caller = (HogarFragment_014) pagina;
		HogarDialog_013 f = new HogarDialog_013();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public HogarDialog_013(){
		super();
		seccionesGrabadoB = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","PREGUNTA_ID","PERSONA_ID","QH109_1R")};
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PREGUNTA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M")};
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion03) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//getDialog().setTitle("BENEFICIARIOS JUNTOS");
		tituloPreguntas();
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	private Seccion03Service getServiceBeneficiario() {
        if (beneficiario == null) {
        	beneficiario = Seccion03Service.getInstance(getActivity());
        }
        return beneficiario;
	}
	private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
	
	private void cargarDatos() {
		MyUtil.LiberarMemoria();
		llenarSpinnerBeneficiarios(bean);
		if (bean.qh109_1r!=null) {
			bean.qh109_1r=bean.setConver109_r(bean.qh109_1r);
		}
		entityToUI(bean);
	
		if (bean.qhs3_1a!=null) {
			muestraPregunta_y(bean.qhs3_1a);
	    }
		if (bean.qhs3_1m!=null) {
			muestraPregunta_m(bean.qhs3_1m);
		}
		
		
		caretaker.addMemento("antes", bean.saveToMemento(Seccion03.class));
		inicio();
	}
	
	public void llenarSpinnerBeneficiarios(Seccion03 seccion3)
	{
		Seccion01 qali = new Seccion01();
		switch (seccion3.pregunta_id) {
		case App.WAWAMAS	:if(seccion3.persona_id_orden!=null) 
						{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMAWAWAMAS,App.EDADMAXIMAWAWAMAS,seccion3.pregunta_id);
						Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
						else{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMAWAWAMAS,App.EDADMAXIMAWAWAMAS,seccion3.pregunta_id);}
						break;
		case App.QALIWARMA:if(seccion3.persona_id_orden!=null)
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMAQALIWARMA,App.EDADMAXIMAQALIWARMA,seccion3.pregunta_id);
							Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
							else
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMAQALIWARMA,App.EDADMAXIMAQALIWARMA,seccion3.pregunta_id);}
							qali = getServiceSeccion01().getPersona(seccion3.id, seccion3.hogar_id, seccion3.persona_id_orden);
							lblAlimentar.setText(lblAlimentar.getText().toString().replace("(NOMBRE)", qali.getNombre2()));
							
							Spanned textoAlim = Html.fromHtml(lblAlimentar.getText()+"<b> Qali Warma?</b>");    	
							lblAlimentar.setText(textoAlim);
							
							break;
		}
	}
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(gridJefe.component());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(botones);
		return contenedor;
	}
	@Override
	protected void buildFields() {
		chbQHS3_1A=new CheckBoxField(this.getActivity(), R.string.hogarqh102_1NSNR, "1:0").size(altoComponente+10,220).callback("on96yChangeValue");
		chbQHS3_1M=new CheckBoxField(this.getActivity(), R.string.hogarqh102_1NSNR, "1:0").size(altoComponente+10,220).callback("on96mChangeValue");
		lblTiempo = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 10, 630).text(R.string.hogarqh102_1t).textSize(18).alinearIzquierda();
		lblNombre = new LabelComponent(this.getActivity()).size(altoComponente + 20, 270).text(R.string.hogarqh102_1n).textSize(16).negrita().alinearIzquierda();
		lblAnios = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh102_1a).textSize(16).negrita().alinearIzquierda();
		lblMeses = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh102_1m).textSize(16).negrita().alinearIzquierda();
		lblAlimentar = new LabelComponent(this.getActivity()).size(altoComponente + 50, 270).text(R.string.hogarqh109_1r).textSize(16).alinearIzquierda();
		textoPreguntas();
		spnPERSONA_ID_ORDEN = new SpinnerField(getActivity()).size(altoComponente + 10, 360).callback("onPERSONA_ID_ORDENChangeValue");
		spnQHS3_1A=new SpinnerField(getActivity()).size(70, 140).callback("on96_yChangeValue");
		cargarSpinnerqh_y();
		spnQHS3_1M=new SpinnerField(getActivity()).size(70, 140).callback("on96_mChangeValue");
		cargarSpinnerqh_m();
		rgQH109_1R=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh109_1r_1,R.string.hogarqh109_1r_2,R.string.hogarqh109_1r_3).
				size(altoComponente + 10, 270).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH109_1R");
		
		gridJefe = new GridComponent2(this.getActivity(), 3);
		
		if(bean.pregunta_id==App.QALIWARMA){
			gridJefe.addComponent(lblAlimentar,3,1);
			gridJefe.addComponent(rgQH109_1R,3,1);
		}
		
		if(bean.pregunta_id==App.WAWAMAS){
			gridJefe.addComponent(lblNombre);
			gridJefe.addComponent(spnPERSONA_ID_ORDEN,2,1);
		}
		
		gridJefe.addComponent(lblTiempo,3,1);
		gridJefe.addComponent(lblAnios);
		gridJefe.addComponent(spnQHS3_1A);
		gridJefe.addComponent(chbQHS3_1A);
		gridJefe.addComponent(lblMeses);
		gridJefe.addComponent(spnQHS3_1M);
		gridJefe.addComponent(chbQHS3_1M);
		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnAceptar).size(200, 55);
		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnCancelar).size(200, 55);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				HogarDialog_013.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarBeca18(bean);
				//caller.cargarTabla();				
				if(bean.pregunta_id==App.QALIWARMA){
					if(caller.completar){
						caller.posi_qali++;
						caller.completar();
					}
				}
				HogarDialog_013.this.dismiss();
			}
		});
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
	
	public boolean grabar() {
		uiToEntity(bean);
		bean.qhs3_1a=combinaPregunta_y();
		bean.qhs3_1m=combinaPregunta_m();
		
	
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			if (bean.qh109_1r!=null) {
				bean.qh109_1r=bean.getConver109_r(bean.qh109_1r);
			}			
			flag = getServiceBeneficiario().saveOrUpdate(bean, dbTX,seccionesGrabadoB);
			if (!flag) {
				throw new Exception(
						"Ocurri� un problema al grabar beneficiario 2");
			}
			getServiceBeneficiario().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}

	private boolean validar() {
		String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
		if(bean.pregunta_id==App.QALIWARMA){
			if (Util.esVacio(bean.qh109_1r)) {
				error = true;
				view = rgQH109_1R;
				mensaje = preguntaVacia.replace("$", "Seleccione QALI WARMA");
				return false;
			}
			if(!Util.esDiferente(bean.qh109_1r, 1,1)){
				if (Util.esVacio(bean.persona_id_orden)) {
					error = true;
					view = spnPERSONA_ID_ORDEN;
					mensaje = preguntaVacia.replace("$", "Seleccione Nombre");
					return false;
				}
				if(Util.esVacio(bean.qhs3_1a)){
						error = true;
						view = spnQHS3_1A;
						mensaje = preguntaVacia.replace("$", "Seleccione A�os");
						return false;
				}
				if(Util.esVacio(bean.qhs3_1m)){
						error = true;
						view = spnQHS3_1M;
						mensaje = preguntaVacia.replace("$", "Seleccione Meses");
						return false;
				}
			}
			
		}else if(bean.pregunta_id==App.WAWAMAS){			
			if (Util.esVacio(bean.persona_id_orden)) {
				error = true;
				view = spnPERSONA_ID_ORDEN;
				mensaje = preguntaVacia.replace("$", "Seleccione Nombre");
				return false;
			}
			if(Util.esVacio(bean.qhs3_1a)){
					error = true;
					view = spnQHS3_1A;
					mensaje = preguntaVacia.replace("$", "Seleccione A�os");
					return false;
			}
			if(Util.esVacio(bean.qhs3_1m)){
					error = true;
					view = spnQHS3_1M;
					mensaje = preguntaVacia.replace("$", "Seleccione Meses");
					return false;
			}
		}
		if(!Util.esDiferente(bean.qhs3_1a, 0) && !Util.esDiferente(bean.qhs3_1m, 0)){
			error = true;
			view = spnQHS3_1A;
			mensaje = "El Mes y A�o no pueden ser cero a la vez!!!";
			return false;
		}
		
		return true;
	}
	private void inicio() {
		if(bean.pregunta_id==App.QALIWARMA){
			onrgQH109_1R();
		}
		if(bean.pregunta_id==App.WAWAMAS){
			on96_yChangeValue();
			on96yChangeValue();
//			on96_mChangeValue();
//			on96mChangeValue();
		}	
		
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
    public Integer combinaPregunta_y() {
	    Integer p_y=null;
	    
	    if(bean.persona_id_orden==null){
			if(bean.pregunta_id==App.WAWAMAS){
	        	anios = 3;
	        }
			else if(bean.pregunta_id==App.QALIWARMA){
	        	//anios = 6;
				anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
				Log.e("anios 6: ","  "+anios);
	        }
		}
	    
	    if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString() )) {
	   	 	p_y= Integer.parseInt(spnQHS3_1A.getValue().toString());
	    }
	    if (chbQHS3_1A.getValue().toString().equals("1")) {
	   	 	p_y= bean.getConvertqh92((Integer.parseInt(chbQHS3_1A.getValue().toString())));
	    }
	    return p_y;         
    }
    public void muestraPregunta_y(Integer p92y) {
    	if(bean.persona_id_orden==null){
			if(bean.pregunta_id==App.WAWAMAS){
	        	anios = 3;
	        }
			else if(bean.pregunta_id==App.QALIWARMA){
//	        	anios = 6;
				anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
				Log.e("anios 5: ","  "+anios);
	        }
		}
        if (p92y>=0 && p92y<=anios) {        
              spnQHS3_1A.setSelection(p92y+1);
        }            
        else {
              if (p92y==98) {                
				 chbQHS3_1A.setChecked(true);
			}                        
        }
    }
	private void cargarSpinnerqh_y() {
		if(bean.persona_id_orden==null){
			if(bean.pregunta_id==App.WAWAMAS){
	        	anios = 3;
	        }
			else if(bean.pregunta_id==App.QALIWARMA){
//	        	anios = 6;
				anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
				Log.e("anios 4: ","  "+anios);
	        }
		}		
		
		if(bean.persona_id_orden!=null){
			
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			if(bean.pregunta_id==App.WAWAMAS){
				anios = band.qh07;
				if(anios>App.EDADMAXIMAWAWAMAS){
					anios=App.EDADMAXIMAWAWAMAS;
				}
			}
			else if(bean.pregunta_id==App.QALIWARMA){
				anios = band.qh07 - 3;
			
				//if(anios>6){
				if(anios>App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA){
//					anios=6;
					anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
					Log.e("anios 3: ","  "+anios);
				}
			}
		}
		List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        
        String[] items1 = new String[anios+2];
        items1[0] = "Selec";
        
        for(int i=0;i<=anios;i++){
        	keys.add(i);
        	items1[i+1] = ""+i;
        }
        
//        keys.add(1);
//        keys.add(2);
//        keys.add(3);
//        keys.add(4);
//        keys.add(5);
//        keys.add(6);      
        
//        String[] items1 = new String[] {"Selec","1","2","3","4","5","6"};
//        
//        if(bean.pregunta_id==App.WAWAMAS){
//        	items1 = new String[] {"Selec","0","1","2","3"};
//        }else if(bean.pregunta_id==App.QALIWARMA){
//        	items1 = new String[] {"Selec","0","1","2","3","4","5","6"};
//        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1A.setAdapterWithKey(adapter, keys);
    }
    public Integer combinaPregunta_m() {
	    Integer p_m=null;
	    if (MyUtil.incluyeRango(0,11,spnQHS3_1M.getValue().toString() )) {
	   	 	p_m= Integer.parseInt(spnQHS3_1M.getValue().toString());
	    }
	    if (chbQHS3_1M.getValue().toString().equals("1")) {
	   	 	p_m= bean.getConvertqh92((Integer.parseInt(chbQHS3_1M.getValue().toString())));
	    }
	    return p_m;         
    }
    public void muestraPregunta_m(Integer p_m) {
            if (p_m>=0 && p_m<=11) {        
                  spnQHS3_1M.setSelection(p_m+1);
            }            
            else {
                  if (p_m==98) {                
    				 chbQHS3_1M.setChecked(true);
    			}                        
            }
    }
    
	private void cargarSpinnerqh_m() {
        List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        keys.add(0);
        keys.add(1);
        keys.add(2);
        keys.add(3);
        keys.add(4);
        keys.add(5);
        keys.add(6);
        keys.add(7);
        keys.add(8);
        keys.add(9);
        keys.add(10);
        keys.add(11);
        String[] items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1M.setAdapterWithKey(adapter, keys);
    }	
	public void on96_yChangeValue() {
		if(bean.pregunta_id==App.WAWAMAS){
        	anios = 3;
        }else if(bean.pregunta_id==App.QALIWARMA){
//        	anios = 6;
        	anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
        	Log.e("anios 2: ","  "+anios);
        }
		if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),chbQHS3_1A);
			Util.lockView(getActivity(), false,chbQHS3_1A);
			spnQHS3_1M.requestFocus();
			on96_mChangeValue();
    		on96mChangeValue();
		} else {
			if(bean.pregunta_id==App.WAWAMAS){
//				Util.lockView(getActivity(), false,chbQHS3_1A);
				chbQHS3_1A.requestFocus();
			}else if(bean.pregunta_id==App.QALIWARMA){			
				if(!Util.esDiferente(val_109r, 1,1)){
//					Util.lockView(getActivity(), false,chbQHS3_1A);
					chbQHS3_1A.requestFocus();
				}
			}							
		}
	}
	public void on96yChangeValue() {
			if (MyUtil.incluyeRango(1,1,chbQHS3_1A.getValue().toString())) {
				Util.cleanAndLockView(getActivity(),spnQHS3_1A);
				Util.lockView(getActivity(), false,spnQHS3_1A);
				spnQHS3_1M.requestFocus();
				on96_mChangeValue();
	    		on96mChangeValue();
			} else {
				if(bean.pregunta_id==App.WAWAMAS){
//					Util.lockView(getActivity(), false,spnQHS3_1A);
					spnQHS3_1A.requestFocus();
				}else if(bean.pregunta_id==App.QALIWARMA){			
					if(!Util.esDiferente(val_109r, 1,1)){
//						Util.lockView(getActivity(), false,spnQHS3_1A);
						spnQHS3_1A.requestFocus();
					}
				}			
			}
	}
	
	public void onrgQH109_1R(){
		int valor = Integer.parseInt(rgQH109_1R.getTagSelected("0").toString());
		val_109r = valor;
		
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(),spnQHS3_1A,spnQHS3_1M,chbQHS3_1A,chbQHS3_1M);
			btnAceptar.requestFocus();
		} else { 
            Util.lockView(getActivity(), false,spnQHS3_1A,spnQHS3_1M,chbQHS3_1A,chbQHS3_1M);
            spnQHS3_1A.requestFocus();
            on96_yChangeValue();
    		on96yChangeValue();
		} 
	}
	
	public void on96_mChangeValue() {
		if (MyUtil.incluyeRango(0,12,spnQHS3_1M.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),chbQHS3_1M);
			Util.lockView(getActivity(), false,chbQHS3_1M);
			btnAceptar.requestFocus();
			
		} else {
			if(bean.pregunta_id==App.WAWAMAS){
//				Util.lockView(getActivity(), false,chbQHS3_1M);
				chbQHS3_1M.requestFocus();
			}else if(bean.pregunta_id==App.QALIWARMA){			
				if(!Util.esDiferente(val_109r, 1,1)){
//					Util.lockView(getActivity(), false,chbQHS3_1M);
					chbQHS3_1M.requestFocus();
				}
			}			
		}
	}
	
	public void on109_1rChangeValue(){}
	
	public void on96mChangeValue() {	
			if (MyUtil.incluyeRango(1,1,chbQHS3_1M.getValue().toString())) {
				Util.cleanAndLockView(getActivity(),spnQHS3_1M);
				Util.lockView(getActivity(), false,spnQHS3_1M);
				btnAceptar.requestFocus();				
			} else {
				if(bean.pregunta_id==App.WAWAMAS){
//					Util.lockView(getActivity(), false,spnQHS3_1M);
					spnQHS3_1M.requestFocus();
				}else if(bean.pregunta_id==App.QALIWARMA){			
					if(!Util.esDiferente(val_109r, 1,1)){
//						Util.lockView(getActivity(), false,spnQHS3_1M);
						spnQHS3_1M.requestFocus();
					}
				}			
			}			
	}
	
	public void on1091rChangeValue(){}
	
	public void textoPreguntas(){
		if(bean.pregunta_id==App.WAWAMAS){
			lblNombre.text(R.string.hogarqh107_1n);
			lblTiempo.text(R.string.hogarqh107_1t);
		}else if(bean.pregunta_id==App.QALIWARMA){			
			lblNombre.text(R.string.hogarqh109_1n);
			lblTiempo.text(R.string.hogarqh109_1t);
		}
	}
	public void tituloPreguntas(){
		if(bean.pregunta_id==App.WAWAMAS){
			getDialog().setTitle("BENEFICIARIOS WAWA WASI/CUNA M�S");
		}else if(bean.pregunta_id==App.QALIWARMA){			
			getDialog().setTitle("BENEFICIARIOS QALI WARMA");
		}										           	
	}
	
	public void onPERSONA_ID_ORDENChangeValue(){
		boolean entro=false;
		uiToEntity(bean);
		if(bean.persona_id_orden!=null){
			
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			
			if(bean.pregunta_id==App.WAWAMAS){
				entro = true;
				anios = band.qh07;
				
				if(anios>2){
					anios=2;
				}
			}
			else if(bean.pregunta_id==App.QALIWARMA){
				entro = true;
				anios = band.qh07 - 3;
				
//				if(anios>6){
				if(anios>App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA){
//					anios=6;
					anios = App.ANIOPORDEFECTOSUPERIOR-App.ANIOINICIOQALIWARMA;
					Log.e("anios 1: ","  "+anios);
				}
			}
			if(entro){
				cargarSpinnerqh_y();				
			}
		}		
	}
}
