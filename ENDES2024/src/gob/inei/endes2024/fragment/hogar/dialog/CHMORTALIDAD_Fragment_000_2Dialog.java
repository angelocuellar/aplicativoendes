package gob.inei.endes2024.fragment.hogar.dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.hogar.HogarFragment_000;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CHMORTALIDAD_Fragment_000_2Dialog extends DialogFragmentComponent  {
	@FieldAnnotation(orderIndex=1) 
	public TextField txtQH29M; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH30M;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQH31M; 
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQH32M_M;
	@FieldAnnotation(orderIndex=5)
	public IntegerField txtQH32M_Y;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH33M; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQH33M_O; 
	 
	public ButtonComponent btnAceptar;
	public static HogarFragment_000 caller;
	MORTALIDAD hermano;
	CISECCION_08 individual; 
	private CuestionarioService cuestionarioService;
	private VisitaService visitaService;
	private SeccionCapitulo[] seccionesCargado,seccionesCargadolistado;
	private SeccionCapitulo[] seccionesGrabado;
	private static String nombre=null;
	private static List<MORTALIDAD> hermanos;
	public GridComponent2 gridqh32m;
	LabelComponent lblpreguntaqh29m,lblpreguntaqh29mobs,lblpreguntaqh30m,lblpreguntaqh31m,lblpreguntaqh31mobs,lblpreguntaqh32m,lblpreguntaqh32mmes,lblpreguntaqh32manio,lblpregunta908,lblpreguntaqh33m;
	public ButtonComponent  btnCancelar;
	public CheckBoxField chbP910;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	LinearLayout q14;
	
	public static CHMORTALIDAD_Fragment_000_2Dialog newInstance(FragmentForm pagina,MORTALIDAD detalle, int position, List<MORTALIDAD> detalles) {
		caller = (HogarFragment_000) pagina;
		hermanos=detalles;
		CHMORTALIDAD_Fragment_000_2Dialog f = new CHMORTALIDAD_Fragment_000_2Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", (MORTALIDAD) detalles.get(position));
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hermano = (MORTALIDAD) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	
	public CHMORTALIDAD_Fragment_000_2Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M","QH33M_O","NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargadolistado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ESTADO","QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M", "NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M","QH33M_O")}; 
	}            
	

	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		lblpreguntaqh29m= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(19).text(R.string.mortalidad_qh209msolouno);
		lblpreguntaqh29mobs = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_qh209mobs);
		lblpreguntaqh30m= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.mortalidad_qh30m);
		lblpreguntaqh31m = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.mortalidad_qh31m);
		lblpreguntaqh32m = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.mortalidad_qh32m);
		lblpreguntaqh32mmes = new LabelComponent(getActivity()).size(altoComponente, 100).textSize(16).text(R.string.mortalidad_qh32mmes);
		lblpreguntaqh32manio = new LabelComponent(getActivity()).size(altoComponente, 100).textSize(16).text(R.string.mortalidad_qh32manio);
		lblpreguntaqh33m = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.mortalidad_qh33m);
		lblpreguntaqh31mobs= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_qh31mobs);
		
		txtQH29M = new TextField(getActivity()).size(altoComponente, 290).maxLength(20);
		txtQH29M.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().length()>0)
					RenombrarEtiquetassiCambioNombre();
				else
					RenombrarEtiquetas();
			}
		});
		rgQH30M = new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_08_09qi905_1,R.string.c2seccion_08_09qi905_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh30MChangeValue");
		txtQH31M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(3).callback("onqi912ChangeValue");
		txtQH31M.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length()>2)
					onqi912ChangeValue();
			}
		});
		
		txtQH32M_M =new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		txtQH32M_Y =new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
		txtQH33M_O = new TextField(getActivity()).size(altoComponente, 350).maxLength(100);	
		rgQH33M=new RadioGroupOtherField(this.getActivity(),R.string.mortalidad_qh33m_1,R.string.mortalidad_qh33m_2,R.string.mortalidad_qh33m_3,R.string.mortalidad_qh33m_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQH33M.agregarEspecifique(3,txtQH33M_O);
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
	

		gridqh32m = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gridqh32m.addComponent(lblpreguntaqh32mmes);
		gridqh32m.addComponent(txtQH32M_M);
		gridqh32m.addComponent(lblpreguntaqh32manio);		
		gridqh32m.addComponent(txtQH32M_Y);
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				CHMORTALIDAD_Fragment_000_2Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				CHMORTALIDAD_Fragment_000_2Dialog.this.dismiss();
				caller.recargardatos();
			}
		});   
	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(0,lblpreguntaqh29m,lblpreguntaqh29mobs,txtQH29M); 
		q2 = createQuestionSection(0,lblpreguntaqh30m,rgQH30M); 
		q3 = createQuestionSection(0,lblpreguntaqh31m,lblpreguntaqh31mobs,txtQH31M); 
		q4 = createQuestionSection(0,lblpreguntaqh32m,gridqh32m.component()); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh33m,rgQH33M); 
		
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		
		form.addView(botones);
		return contenedor;          
	}
	
	public boolean grabar(){
		uiToEntity(hermano);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag=false;
		try {
			flag = getCuestionarioService().saveOrUpdate(hermano, seccionesGrabado);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		}
		
		return flag;
	}

//	public Integer IdentificarPosition(MORTALIDAD hermano){
//		Calendar fecha_actual = new GregorianCalendar();
//		Calendar fecha = new GregorianCalendar(hermano.qh32m_y, hermano.qh32m_m-1, 1);
//		Calendar fechados = null;
//		Integer position=0;
//		hermanos= getCuestionarioService().getMortalidad(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargadolistado);
//		boolean validar=false;
//		final long tiempoenmili= (1000 * 60 * 60 * 24);
//		for(MORTALIDAD persona: hermanos){
////			Log.e("","ID: "+persona.nro_orden_id+" PERSONA_ID: "+persona.getFecha());
//			if(persona.qh32m_m!=null && persona.qh32m_y!=null && (Util.esDiferente(persona.qh32m_y, hermano.qh32m_y) && Util.esDiferente(persona.qh32m_m, hermano.qh32m_m))){
//				fechados= new GregorianCalendar(persona.qh32m_y, persona.qh32m_m-1, 1);
////				Log.e("","HERMANO: "+(fecha.getTimeInMillis()/tiempoenmili));
////				Log.e("","PERSONA: "+(fechados.getTimeInMillis()/tiempoenmili));
//				validar=((fecha.getTimeInMillis()/tiempoenmili) >(fechados.getTimeInMillis()/tiempoenmili));
////				Log.e("","VALIDAR: H>P : "+validar);
//				if(validar){
////					Log.e("","AQUI");
//					position=persona.nro_orden_id;
//					validar=false;
//				}
//			}
//		}
//		validar=false;
//		if(!Util.esDiferente(position, 0)){
//			for(MORTALIDAD persona: hermanos){
////				Log.e("","ID: "+persona.nro_orden_id+" PERSONA_ID: "+persona.getFecha());
//				if(persona.qh32m_m!=null && persona.qh32m_y!=null){
//					fechados= new GregorianCalendar(persona.qh32m_y, persona.qh32m_m-1, 1);
//					validar=(fecha.getTimeInMillis()/tiempoenmili)<(fechados.getTimeInMillis()/tiempoenmili);
//					if(validar){
//						Log.e("","AQUI NO");
//						position=persona.nro_orden_id;
//						validar=false;
//					}
//				}
//			}
//		}
//		return position;
//	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (hermano.qh29m  == null) {
			error = true;
			view = txtQH29M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH29");
			return false;
		}
		if (hermano.qh30m  == null) {
			error = true;
			view = rgQH30M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH30");
			return false;
		}
		if (hermano.qh31m  == null) {
			error = true;
			view = txtQH31M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH31");
			return false;
		}
		if (hermano.qh31m<0 || hermano.qh31m>115) {
			error = true;
			view = txtQH31M;
			mensaje = "Edad ingresado fuera de rango";
			return false;
		}
		if (hermano.qh32m_m  == null) {
			error = true;
			view = txtQH32M_M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH32M");
			return false;
		}
		if (hermano.qh32m_m<=0 || hermano.qh32m_m>12) {
			error = true;
			view = txtQH32M_M;
			mensaje = "Mes ingresado fuera de rango";
			return false;
		}
		if (hermano.qh32m_y  == null) {
			error = true;
			view = txtQH32M_Y;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH32Y");
			return false;
		}
		
		if(!Util.esDiferente(hermano.qh30m,2) && Util.esMayor(hermano.qh31m, 11) && Util.esMenor(hermano.qh31m, 50)){
			if (hermano.qh33m  == null) {
				error = true;
				view = rgQH33M;
				mensaje = preguntaVacia.replace("$", "La pregunta P.QH33");
				return false;
			}
			if(!Util.esDiferente(hermano.qh33m, 4)){
				if(Util.esVacio(hermano.qh33m_o)){
					error = true;
					view = txtQH33M_O;
					mensaje = "Debe ingresar Especifique";
					return false;
				}
				
			}
		}
		Visita visita=getVisitaService().getUltimaVisita(hermano.id, hermano.hogar_id, "ID","HOGAR_ID","NRO_VISITA","QHVMES_INI","QHVANIO_INI");
		if(visita!=null && visita.qhvmes_ini!=null && visita.qhvanio_ini!=null && hermano.qh32m_m!=null && hermano.qh32m_y!=null){
			if((hermano.qh32m_y>visita.qhvanio_ini) || (!Util.esDiferente(hermano.qh32m_y,visita.qhvanio_ini) && hermano.qh32m_m>Integer.parseInt(visita.qhvmes_ini))){
				error = true;
				view = txtQH32M_M;
				mensaje = "La fecha de Fallecimiento no puede ser superior al de la ultima visita";
				return false;
			}
			
		}
		if (hermano.qh32m_y<App.ANIOPORDEFECTO || hermano.qh32m_y>App.ANIOPORDEFECTOSUPERIOR) {
			error = true;
			view = txtQH32M_Y;
			mensaje = "A�o ingresado fuera de rango";
			return false;
		}
		
	    return true;
	}
	
	private void cargarDatos() {      
		hermano = getCuestionarioService().getPersonafallecida(hermano.id,hermano.hogar_id,hermano.persona_id,hermano.nro_orden_id,seccionesCargado);
		if (hermano == null) {
			hermano = new MORTALIDAD();
			hermano.id = App.getInstance().getMarco().id;
			hermano.hogar_id =App.getInstance().getHogar().hogar_id;
			hermano.persona_id = App.getInstance().getPersonaCuestionarioIndividual().persona_id;
			hermano.nro_orden_id = hermano.nro_orden_id;
		}
		nombre=hermano.qh29m;
		entityToUI(hermano);
		inicio();
    }

	public void inicio(){
		RenombrarEtiquetas();
		onqh30MChangeValue();
		ValidarsiesSupervisora();
		rgQH30M.requestFocus();
	}
	
	public void EstadoOriginalDeEqtiquetas(){
		lblpreguntaqh29m.setText(getResources().getString(R.string.mortalidad_qh209msolouno));
		lblpreguntaqh30m.setText(getResources().getString(R.string.mortalidad_qh30m));
		lblpreguntaqh31m.setText(getResources().getString(R.string.mortalidad_qh31m));
		lblpreguntaqh32m.setText(getResources().getString(R.string.mortalidad_qh32m));
		lblpreguntaqh33m.setText(getResources().getString(R.string.mortalidad_qh33m));
		lblpreguntaqh29mobs.setText(getResources().getString(R.string.mortalidad_qh209mobs));
	}
	
	public void RenombrarEtiquetas(){
		String replace ="(NOMBRE)";
		EstadoOriginalDeEqtiquetas();
		lblpreguntaqh30m.setText(lblpreguntaqh30m.getText().toString().replace(replace, nombre));
		lblpreguntaqh31m.setText(lblpreguntaqh31m.getText().toString().replace(replace, nombre));
		lblpreguntaqh32m.setText(lblpreguntaqh32m.getText().toString().replace(replace, nombre));
		lblpreguntaqh33m.setText(lblpreguntaqh33m.getText().toString().replace(replace, nombre));
		lblpreguntaqh29mobs.setText(lblpreguntaqh29mobs.getText().toString().replace("#", App.ANIOPORDEFECTO+""));
	}
	
	public void RenombrarEtiquetassiCambioNombre(){
		if(txtQH29M.getText().toString().length()>0){
		String replace =nombre;
		String nombre = txtQH29M.getText().toString();
		EstadoOriginalDeEqtiquetas();
		RenombrarEtiquetas();
		lblpreguntaqh30m.setText(lblpreguntaqh30m.getText().toString().replace(replace, nombre));
		lblpreguntaqh31m.setText(lblpreguntaqh31m.getText().toString().replace(replace, nombre));
		lblpreguntaqh32m.setText(lblpreguntaqh32m.getText().toString().replace(replace, nombre));
		lblpreguntaqh33m.setText(lblpreguntaqh33m.getText().toString().replace(replace, nombre));
		}
	}

	public void onqi912ChangeValue(){
		Integer edadtenia = txtQH31M.getText().toString().trim().length()>0?Integer.parseInt(txtQH31M.getText().toString()):-1;
		Integer sexo=Integer.parseInt(rgQH30M.getTagSelected("0").toString());
		if(!Util.esDiferente(sexo, 2) && Util.esMayor(edadtenia,11) && Util.esMenor(edadtenia, 50)){
			
			Util.lockView(getActivity(),false ,rgQH33M);
			q5.setVisibility(View.VISIBLE);
			txtQH32M_M.requestFocus();
		}
		else{
			Util.cleanAndLockView(getActivity(),rgQH33M);
			q5.setVisibility(View.GONE);
			txtQH32M_M.requestFocus();
		}

	}
	
	public void onqh30MChangeValue(){
		Integer dato=Integer.parseInt(rgQH30M.getTagSelected("0").toString());
		Integer edad=Integer.parseInt(txtQH31M.getValue()!=null?txtQH31M.getText().toString():"0");
		if(!Util.esDiferente(dato, 1)){
			Util.cleanAndLockView(getActivity(), rgQH33M);
			q5.setVisibility(View.GONE);
		}else{
			if(Util.esMayor(edad, 11) && Util.esMenor(edad, 50)){
				Util.lockView(getActivity(), false,rgQH33M);
				q5.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(), rgQH33M);
				q5.setVisibility(View.GONE);
			}
		}
	}
	
	public void ValidarsiesSupervisora(){
	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		txtQH29M.readOnly(); 
	    		rgQH30M.readOnly(); 
	    		txtQH31M.readOnly();
	    		txtQH32M_M.readOnly();
	    		txtQH32M_Y.readOnly();
	    		rgQH33M.readOnly(); 
	    		txtQH33M_O.readOnly();
	    		btnAceptar.setEnabled(false);
	    	}
	}
	    
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	public VisitaService getVisitaService()
	{
		if(visitaService==null)
		{
				visitaService = VisitaService.getInstance(getActivity());
		}
			return visitaService;
	}
}
