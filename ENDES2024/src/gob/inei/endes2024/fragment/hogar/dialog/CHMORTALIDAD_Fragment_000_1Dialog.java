package gob.inei.endes2024.fragment.hogar.dialog;

import java.sql.SQLException;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.hogar.HogarFragment_000;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class CHMORTALIDAD_Fragment_000_1Dialog extends DialogFragmentComponent {
	@FieldAnnotation(orderIndex=1) 
	public TextField txtQH29M; 
	@FieldAnnotation(orderIndex=2) 
	public ButtonComponent btnAceptar;

	public static HogarFragment_000 caller;
	MORTALIDAD hermano;
	private CuestionarioService cuestionarioService;
	 private Seccion01Service Personaservice;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	
	public GridComponent2 gridpregunta4789;
	public ButtonComponent  btnCancelar;
	public LabelComponent lblpregunta01,lblpregunta02, lblpregunta01_sub;
	public Seccion01 informantehogar;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	
	public static CHMORTALIDAD_Fragment_000_1Dialog newInstance(FragmentForm pagina, MORTALIDAD detalle) {
		caller = (HogarFragment_000) pagina;
		CHMORTALIDAD_Fragment_000_1Dialog f = new CHMORTALIDAD_Fragment_000_1Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hermano = (MORTALIDAD) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		getDialog().setTitle("N� "+hermano.nro_orden_id);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	public CHMORTALIDAD_Fragment_000_1Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QH29M","NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_ID") };
		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1,"QH29M","NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_ID")}; 
	}            
	
	@Override
	protected View createUI() {
		buildFields();
		q2 = createQuestionSection(lblpregunta01,lblpregunta01_sub,txtQH29M); 
		//q3 = createQuestionSection(lblpregunta01_sub); 
		
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q2);
		//form.addView(q3);
		form.addView(botones);
		return contenedor;          
	}
	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		txtQH29M = new TextField(getActivity()).size(altoComponente, 290).maxLength(20);
		lblpregunta01= new LabelComponent(getActivity()).size(altoComponente+15,WRAP_CONTENT).text(R.string.mortalidad_qh209msolouno).textSize(19).negrita();
		lblpregunta01_sub= new LabelComponent(getActivity()).size(altoComponente+105,WRAP_CONTENT).text(R.string.mortalidad_qh209mmasdeuno_sub).textSize(19);
		lblpregunta02= new LabelComponent(getActivity()).size(altoComponente,WRAP_CONTENT).text(R.string.c2seccion_08_09qimorta_1).textSize(19).negrita();
		btnCancelar.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				CHMORTALIDAD_Fragment_000_1Dialog.this.dismiss();
			Util.lockView(getActivity(), false,caller.btnAgregar);
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = false;
				flag=grabar();
				if(!flag){
					return;
				}
				caller.refrescarPersonas(hermano);
				CHMORTALIDAD_Fragment_000_1Dialog.this.dismiss();
				if(Integer.valueOf(caller.txtQH28.getText().toString())>caller.fallecidas.size()){
				caller.agregarHermano();
				}
			}
		});   
	} 
	public boolean grabar() {
		uiToEntity(hermano);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			flag = getCuestionarioService().saveOrUpdate(hermano, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (hermano.qh29m  == null) {
			error = true;
			view = txtQH29M;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH29M");
			return false;
		}		
	    return true;
	}
	private void cargarDatos() { 
		informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
		hermano = getCuestionarioService().getPersonafallecida(hermano.id,hermano.hogar_id,informantehogar.persona_id,hermano.nro_orden_id,seccionesCargado);
		if (hermano == null) {
			hermano = new MORTALIDAD();
			hermano.id = App.getInstance().getMarco().id;
			hermano.hogar_id =App.getInstance().getHogar().hogar_id;
			hermano.persona_id = informantehogar.persona_id;
			hermano.nro_orden_id= caller.fallecidas.size()+1;
		}
		entityToUI(hermano);
		inicio();
       	}
	public void inicio(){
		EsconderTextoSegunCorresponda();
	}
	public void EsconderTextoSegunCorresponda(){
		if(!Util.esDiferente(Integer.parseInt(caller.txtQH28.getText().toString()), 1,1)){
//			lblpregunta02.setVisibility(View.GONE);
//			lblpregunta01.setVisibility(View.VISIBLE);
		}
		else{
//			lblpregunta02.setVisibility(View.VISIBLE);
			lblpregunta01.setText(R.string.mortalidad_qh209mmasdeuno);
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
    public Seccion01Service getPersonaService(){
    	if(Personaservice == null){
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }

}
