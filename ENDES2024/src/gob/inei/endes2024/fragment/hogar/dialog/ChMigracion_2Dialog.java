package gob.inei.endes2024.fragment.hogar.dialog;
//package gob.inei.endes2024.fragment.hogar.dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.components.TextAutCompleteField;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_0001;
//import gob.inei.endes2024.model.CISECCION_08;
//import gob.inei.endes2024.model.MIGRACION;
//import gob.inei.endes2024.model.Ubigeo;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.UbigeoService;
//import gob.inei.endes2024.service.VisitaService;
//
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.Html;
//import android.text.Spanned;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class ChMigracion_2Dialog extends DialogFragmentComponent  {
//	@FieldAnnotation(orderIndex=1) 
//	public TextField txtQH34B_1;
//	@FieldAnnotation(orderIndex=2) 
//	public TextField txtQH34B_11; 
//	@FieldAnnotation(orderIndex=3) 
//	public TextField txtQH34B_2; 
//	@FieldAnnotation(orderIndex=4) 
//	public TextField txtQH34B_3;
//			
//	@FieldAnnotation(orderIndex=5) 
//	public RadioGroupOtherField rgQH34C;
//	
//	@FieldAnnotation(orderIndex=6)
//	public IntegerField txtQH34D_A; 
//	@FieldAnnotation(orderIndex=7)
//	public IntegerField txtQH34D_M;
//	
//	@FieldAnnotation(orderIndex=8)
//	public IntegerField txtQH34E_A; 
//	@FieldAnnotation(orderIndex=9)
//	public IntegerField txtQH34E_M;
//	
//	@FieldAnnotation(orderIndex=10)
//	public TextAutCompleteField txtQH34F_CCDD;
//	@FieldAnnotation(orderIndex=11)
//	public CheckBoxField chbQH34F_CCDD_NS;
//	@FieldAnnotation(orderIndex=12)
//	public TextAutCompleteField txtQH34F_CCPP;
//	@FieldAnnotation(orderIndex=13)
//	public CheckBoxField chbQH34F_CCPP_NS;
//	@FieldAnnotation(orderIndex=14)
//	public TextAutCompleteField txtQH34F_CCDI;
//	@FieldAnnotation(orderIndex=15)
//	public CheckBoxField chbQH34F_CCDI_NS;
//	
//	@FieldAnnotation(orderIndex=16)
//	public TextAutCompleteField txtQH34F_PAIS;
//	@FieldAnnotation(orderIndex=17)
//	public CheckBoxField chbQH34F_PAIS_NS;
//	@FieldAnnotation(orderIndex=18)
//	public TextAreaField txtQH34F_OBSERVACION;
//	
//	
//	@FieldAnnotation(orderIndex=19) 
//	public RadioGroupOtherField rgQH34G; 	
//	@FieldAnnotation(orderIndex=20) 
//	public TextField txtQH34GO; 
//	@FieldAnnotation(orderIndex=21)
//	public TextAreaField txtQH34G_OBSERVACION;
//	
//	@FieldAnnotation(orderIndex=22) 
//	public RadioGroupOtherField rgQH34H; 	
//	@FieldAnnotation(orderIndex=23) 
//	public TextField txtQH34HO;
//	
//	@FieldAnnotation(orderIndex=24) 
//	public RadioGroupOtherField rgQH34I; 
//	@FieldAnnotation(orderIndex=25) 
//	public RadioGroupOtherField rgQH34J; 
//	@FieldAnnotation(orderIndex=26) 
//	public RadioGroupOtherField rgQH34K; 	
//	@FieldAnnotation(orderIndex=27) 
//	public TextField txtQH34KO;
//	
////	@FieldAnnotation(orderIndex=26)
//	
//	
//	@FieldAnnotation(orderIndex=26) 
//	public ButtonComponent btnAceptar;
//	
//	public TextAreaField txtQH34_OBSERVACION;
//	
//	public static HogarFragment_0001 caller;	
//	MIGRACION persona;
//	CISECCION_08 individual; 
//	private CuestionarioService cuestionarioService;
//	private VisitaService visitaService;
//	private UbigeoService ubigeoService;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	private static String nombre=null;
//	private String operador="-";
//	
//	public GridComponent2 gridqh34b,gridqh34d,gridqh34e,gridqh34f;
//	LabelComponent lblpreguntaqh34_t,lblobs_34,
//	lblpreguntaqh34b,lblpreguntaqh34b_1,lblpreguntaqh34b_11,lblpreguntaqh34b_2,lblpreguntaqh34b_3,
//	lblpreguntaqh34c,
//	lblpreguntaqh34d,lblpreguntaqh34d_a,lblpreguntaqh34d_m,
//	lblpreguntaqh34e,lblpreguntaqh34e_a,lblpreguntaqh34e_m,
//	lblpreguntaqh34f,lblpreguntaqh34f_obs,lblpreguntaqh34f_n,lblpreguntaqh34f_ndd,lblpreguntaqh34f_npp,lblpreguntaqh34f_ndi,lblpreguntaqh34f_e,lblpreguntaqh34f_ep,
//	lblpreguntaqh34g,lblpreguntaqh34g_obs,lblpreguntaqh34h,lblpreguntaqh34i,lblpreguntaqh34j,lblpreguntaqh34k,lblpreguntaqh34f_ndiS;
//	public ButtonComponent  btnCancelar;
//	public CheckBoxField chbP910;
//	String fecha_referencia;
//	
//	LinearLayout q0;
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5;
//	LinearLayout q55;
//	LinearLayout q6;
//	LinearLayout q7;
//	LinearLayout q8;
//	LinearLayout q9;
//	LinearLayout q10;
//	LinearLayout q11;
//	LinearLayout q12;
//	LinearLayout q13;
//	LinearLayout q14;
//	
//	public static ChMigracion_2Dialog newInstance(FragmentForm pagina,MIGRACION detalle, int position, List<MIGRACION> detalles) {
//		caller = (HogarFragment_0001) pagina;
////		personas=detalles;
//		ChMigracion_2Dialog f = new ChMigracion_2Dialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", (MIGRACION) detalles.get(position));
//		f.setArguments(args);
//		return f;
//	}
//	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (MIGRACION) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
////		getDialog().setTitle("N� "+persona.qh34a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public ChMigracion_2Dialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "QH34C", "QH34D_A", "QH34D_M", "QH34E_A", "QH34E_M", "QH34F_CCDD", "QH34F_CCDD_NS", "QH34F_CCPP", "QH34F_CCPP_NS", "QH34F_CCDI", "QH34F_CCDI_NS", "QH34F_PAIS", "QH34F_PAIS_NS", "QH34F_OBSERVACION", "QH34G", "QH34GO", "QH34G_OBSERVACION", "QH34H", "QH34HO", "QH34I", "QH34J", "QH34J_REF", "QH34K", "QH34KO", "QH34_OBSERVACION", "ID", "HOGAR_ID")};
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "QH34C", "QH34D_A", "QH34D_M", "QH34E_A", "QH34E_M", "QH34F_CCDD", "QH34F_CCDD_NS", "QH34F_CCPP", "QH34F_CCPP_NS", "QH34F_CCDI", "QH34F_CCDI_NS", "QH34F_PAIS", "QH34F_PAIS_NS", "QH34F_OBSERVACION", "QH34G", "QH34GO", "QH34G_OBSERVACION", "QH34H", "QH34HO", "QH34I", "QH34J", "QH34J_REF", "QH34K", "QH34KO", "QH34_OBSERVACION")};
//		
////		seccionesCargadolistado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ESTADO","QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M", "NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID")};
//		 
//	}            
//	
//
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		lblpreguntaqh34_t = new LabelComponent(this.getActivity(),	App.ESTILO)	.size(MATCH_PARENT, 750).text(R.string.migracion34_t).textSize(20).centrar().negrita();
//		
//		lblpreguntaqh34b= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34b);
//		lblpreguntaqh34b_1= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion34b_1);
//		lblpreguntaqh34b_11= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion34b_11);
//		lblpreguntaqh34b_2= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion34b_2);
//		lblpreguntaqh34b_3= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.migracion34b_3);
//		
//		lblpreguntaqh34c= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34c);
//		lblpreguntaqh34d= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34d);
//		lblpreguntaqh34d_a= new LabelComponent(getActivity()).size(MATCH_PARENT, 250).textSize(16).text(R.string.migracion34d_a).alinearDerecha();
//		lblpreguntaqh34d_m= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion34d_m).alinearDerecha();
//		
//		lblpreguntaqh34e= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34e);
//		lblpreguntaqh34e_a= new LabelComponent(getActivity()).size(MATCH_PARENT, 100).textSize(16).text(R.string.migracion34e_a).alinearDerecha();
//		lblpreguntaqh34e_m= new LabelComponent(getActivity()).size(MATCH_PARENT, 100).textSize(16).text(R.string.migracion34e_m).alinearDerecha();
//		
//		lblpreguntaqh34f= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34f);
//		lblpreguntaqh34f_obs= new LabelComponent(getActivity()).size(altoComponente, 750).textSize(16).text(R.string.migracion34f_obs).negrita();
//		lblpreguntaqh34f_n= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.migracion34f_n).negrita();
//		lblpreguntaqh34f_ndd= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion34f_ndd);
//		lblpreguntaqh34f_npp= new LabelComponent(getActivity()).size(MATCH_PARENT, 200).textSize(16).text(R.string.migracion34f_npp);
//		lblpreguntaqh34f_ndi= new LabelComponent(getActivity()).size(altoComponente, 200).textSize(16).text(R.string.migracion34f_ndi);
//		lblpreguntaqh34f_ndiS= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text("");
//		lblpreguntaqh34f_e= new LabelComponent(getActivity()).size(altoComponente, 750).textSize(16).text(R.string.migracion34f_e).negrita();
//		lblpreguntaqh34f_ep= new LabelComponent(getActivity()).size(altoComponente, 200).textSize(16).text(R.string.migracion34f_ep);
//		
//		lblpreguntaqh34g= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34g);
//		lblpreguntaqh34g_obs= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.migracion34g_obs).negrita();
//		lblpreguntaqh34h= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34h);
//		lblpreguntaqh34i= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34i);
//		lblpreguntaqh34j= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34j);
//		lblpreguntaqh34k= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.migracion34k);
//		
//	
//		txtQH34B_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH34B_11 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH34B_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		txtQH34B_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(100).soloTexto();
//		
//		txtQH34B_1.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//				if(s.toString().length()>0)
//					RenombrarEtiquetassiCambioNombre();
//				else
//					RenombrarEtiquetas();
//			}
//		});
//		
//		gridqh34b = new GridComponent2(this.getActivity(),App.ESTILO, 2);
//		gridqh34b.addComponent(lblpreguntaqh34b_1);
//		gridqh34b.addComponent(txtQH34B_1);
//		gridqh34b.addComponent(lblpreguntaqh34b_11);
//		gridqh34b.addComponent(txtQH34B_11);
//		gridqh34b.addComponent(lblpreguntaqh34b_2);
//		gridqh34b.addComponent(txtQH34B_2);
//		gridqh34b.addComponent(lblpreguntaqh34b_3);
//		gridqh34b.addComponent(txtQH34B_3);
//		
//		rgQH34C = new RadioGroupOtherField(this.getActivity(),R.string.migracion34c_1,R.string.migracion34c_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		
//		txtQH34D_A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2).callback("onqh34D_AChangeValue");
//		txtQH34D_M =new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
//		txtQH34D_A.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//				if(s.toString().trim().length()>0){
//					Util.cleanAndLockView(getActivity(),txtQH34D_M);
////					if(s.toString().trim().equals("0") || s.toString().trim().equals("1") || s.toString().trim().equals("2")){
////						Util.cleanAndLockView(getActivity(),rgQH34I);
////						q8.setVisibility(View.GONE);	
////					}
////					else{
////						Util.lockView(getActivity(),false ,rgQH34I);
////						q8.setVisibility(View.VISIBLE);
////					}
//				}
//				else{
//					Util.lockView(getActivity(),false, txtQH34D_M);
//				}			
//			}
//		});
//		
//		txtQH34D_M.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//				if(s.toString().trim().length()>0){
//					Util.cleanAndLockView(getActivity(),txtQH34D_A);					
//				}
//				else{
//					Util.lockView(getActivity(),false, txtQH34D_A);
//				}
//			}
//		});
//		
//		txtQH34E_A=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4);
//		txtQH34E_M =new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
//		
//		
//		txtQH34F_CCDD = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 400);
//		txtQH34F_CCPP = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 400);
//		txtQH34F_CCDI = new TextAutCompleteField(getActivity()).size(altoComponente + 15, 750);
//		txtQH34F_PAIS = new TextAutCompleteField(getActivity()).size(altoComponente, 400);
//		
//		chbQH34F_CCDD_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onqh34F_CCDD_nsChangeValue");
//		chbQH34F_CCPP_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onqh34F_CCPP_nsChangeValue");
//		chbQH34F_CCDI_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onqh34F_CCDI_nsChangeValue");		
//		chbQH34F_PAIS_NS = new CheckBoxField(this.getActivity(), R.string.migracion35d_ns, "1:0").size(altoComponente, 150).callback("onqh34F_PAIS_nsChangeValue");
//		
//		
//		CargarDepartamento();
//		txtQH34F_CCDD.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
//            	String ccdd=item.substring(0,2);
//            	CargarProvincia(ccdd);
//            	CargarDistritoProvincia(ccdd);
//            	onqh34F_PaisChangeValue();
//            	txtQH34F_CCPP.requestFocus();
//            }
//        });
//		txtQH34F_CCDD.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				txtQH34F_CCPP.setText("");
//				txtQH34F_CCDI.setText("");
//				if(!chbQH34F_CCDD_NS.isChecked() && !chbQH34F_PAIS_NS.isChecked()){
//					if(s.toString().trim().length()>0){
//						Util.cleanAndLockView(getActivity(),txtQH34F_PAIS,chbQH34F_PAIS_NS);					
//					}
//					else{
//						Util.lockView(getActivity(),false, txtQH34F_PAIS,chbQH34F_PAIS_NS);
//					}	
//				}
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH34F_CCDD.setText(s);
//			         txtQH34F_CCDD.setSelection(txtQH34F_CCDD.getText().length());
//			      }
//			}
//		});
//		
//		
//		txtQH34F_CCPP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
//            	String ccpp=item.substring(0,2);
//            	String departamento=txtQH34F_CCDD.getText().toString();
//            	CargarDistrito(departamento.substring(0,2), ccpp);
//            	txtQH34F_CCDI.setText("");
//            	txtQH34F_CCDI.requestFocus();
//            }
//        });
//		txtQH34F_CCPP.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				//txtQH34F_CCDI.setText("");
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH34F_CCPP.setText(s);
//			         txtQH34F_CCPP.setSelection(txtQH34F_CCPP.getText().length());
//			      }
//			}
//		});
//		
//		txtQH34F_CCDI.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	String item = parent.getItemAtPosition(position).toString();
//            	if(item.indexOf(operador)>0){
//	            	String provincia=item.substring(0,item.indexOf(operador)-1);
//	            	String distrito=item.substring(item.indexOf(operador)+2,item.length());
//	            	txtQH34F_CCDI.setText(distrito);
//	            	txtQH34F_CCPP.setText(provincia);
//            	}
//            	rgQH34G.requestFocus();
//            	OcultarTecla();
//            }
//        });
//
//		txtQH34F_CCDI.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				 String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH34F_CCDI.setText(s);
//			         txtQH34F_CCDI.setSelection(txtQH34F_CCDI.getText().length());
//			      }
//			}
//		});
//		
//		
//		txtQH34F_PAIS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            	onqh34F_PaisChangeValue();
//            	OcultarTecla();
//            }
//        });
//		CargarPaises();
//		txtQH34F_PAIS.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				if(!chbQH34F_CCDD_NS.isChecked() && !chbQH34F_PAIS_NS.isChecked()){
//					if(s.toString().trim().length()>0){
//						Util.cleanAndLockView(getActivity(),txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCDD_NS, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);					
//					}
//					else{
//						Util.lockView(getActivity(),false, txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCDD_NS, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//					}	
//				}
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void afterTextChanged(Editable et) {
//				// TODO Auto-generated method stub
//				String s=et.toString();
//			      if(!s.equals(s.toUpperCase())){
//			         s=s.toUpperCase();
//			         txtQH34F_PAIS.setText(s);
//			         txtQH34F_PAIS.setSelection(txtQH34F_PAIS.getText().length());
//			      }
//			}
//		});
//		txtQH34F_OBSERVACION = new TextAreaField(getActivity()).maxLength(1000).size(150, 700).alfanumerico();
//	
//		
//		rgQH34G=new RadioGroupOtherField(this.getActivity(),R.string.migracion34g_1,R.string.migracion34g_2,R.string.migracion34g_3,R.string.migracion34g_4,R.string.migracion34g_5,R.string.migracion34g_6,R.string.migracion34g_7,R.string.migracion34g_8,R.string.migracion34g_9,R.string.migracion34g_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqh34G_ChangeValue");
//		txtQH34GO = new TextField(getActivity()).size(altoComponente, 600).maxLength(100);	
//		rgQH34G.agregarEspecifique(8,txtQH34GO);
//		txtQH34G_OBSERVACION = new TextAreaField(getActivity()).maxLength(1000).size(150, 700).alfanumerico();
//		
//		rgQH34H=new RadioGroupOtherField(this.getActivity(),R.string.migracion34h_1,R.string.migracion34h_2,R.string.migracion34h_3,R.string.migracion34h_4,R.string.migracion34h_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQH34HO = new TextField(getActivity()).size(altoComponente, 600).maxLength(100);	
//		rgQH34H.agregarEspecifique(3,txtQH34HO);
//		
//		rgQH34I=new RadioGroupOtherField(this.getActivity(),R.string.migracion34i_1,R.string.migracion34i_2,R.string.migracion34i_3,R.string.migracion34i_4,R.string.migracion34i_5,R.string.migracion34i_6,R.string.migracion34i_7,R.string.migracion34i_8,R.string.migracion34i_9,R.string.migracion34i_10,R.string.migracion34i_11).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH34J=new RadioGroupOtherField(this.getActivity(),R.string.migracion34j_1,R.string.migracion34j_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh34J_ChangeValue");
//		
//		rgQH34K=new RadioGroupOtherField(this.getActivity(),R.string.migracion34k_1,R.string.migracion34k_2,R.string.migracion34k_3,R.string.migracion34k_4,R.string.migracion34k_5,R.string.migracion34k_6,R.string.migracion34k_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQH34KO = new TextField(getActivity()).size(altoComponente, 600).maxLength(100);	
//		rgQH34K.agregarEspecifique(6,txtQH34KO);
//		
//		lblobs_34 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//		txtQH34_OBSERVACION = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
//		
//	
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//
//		gridqh34d = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
//		gridqh34d.addComponent(lblpreguntaqh34d_a);
//		gridqh34d.addComponent(txtQH34D_A);
//		gridqh34d.addComponent(lblpreguntaqh34d_m);		
//		gridqh34d.addComponent(txtQH34D_M);
//		
//		gridqh34e = new GridComponent2(this.getActivity(),App.ESTILO,4,0);
//		gridqh34e.addComponent(lblpreguntaqh34e_a);
//		gridqh34e.addComponent(txtQH34E_A);
//		gridqh34e.addComponent(lblpreguntaqh34e_m);		
//		gridqh34e.addComponent(txtQH34E_M);
//		
//		gridqh34f = new GridComponent2(this.getActivity(),3);
//		gridqh34f.addComponent(lblpreguntaqh34f_n,3);
//		gridqh34f.addComponent(lblpreguntaqh34f_ndd);
//		gridqh34f.addComponent(txtQH34F_CCDD);
//		gridqh34f.addComponent(chbQH34F_CCDD_NS);
//		gridqh34f.addComponent(lblpreguntaqh34f_npp);
//		gridqh34f.addComponent(txtQH34F_CCPP);
//		gridqh34f.addComponent(chbQH34F_CCPP_NS);
//		
//		gridqh34f.addComponent(lblpreguntaqh34f_ndi);
//		gridqh34f.addComponent(lblpreguntaqh34f_ndiS);
//		gridqh34f.addComponent(chbQH34F_CCDI_NS);
////		
//		gridqh34f.addComponent(txtQH34F_CCDI,3);
//		
//		gridqh34f.addComponent(lblpreguntaqh34f_e,3);
//		gridqh34f.addComponent(lblpreguntaqh34f_ep);
//		gridqh34f.addComponent(txtQH34F_PAIS);
//		gridqh34f.addComponent(chbQH34F_PAIS_NS);
//		
//		
//		
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				ChMigracion_2Dialog.this.dismiss();
//			}
//		});
//		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				ChMigracion_2Dialog.this.dismiss();
//				caller.recargardatos();
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
////		q0 = createQuestionSection(0,lblpreguntaqh34_t);
//		q1 = createQuestionSection(0,lblpreguntaqh34b,gridqh34b.component());
//		q2 = createQuestionSection(0,lblpreguntaqh34c,rgQH34C);
//		q3 = createQuestionSection(0,lblpreguntaqh34d,gridqh34d.component());
//		q4 = createQuestionSection(0,lblpreguntaqh34e,gridqh34e.component());
//		q5 = createQuestionSection(0,lblpreguntaqh34f,gridqh34f.component());
//		q55 = createQuestionSection(0,lblpreguntaqh34f_obs,txtQH34F_OBSERVACION);
//		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh34g,rgQH34G,lblpreguntaqh34g_obs,txtQH34G_OBSERVACION);
//		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh34h,rgQH34H);
//		q8 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh34i,rgQH34I);
//		q9 = createQuestionSection(0,lblpreguntaqh34j,rgQH34J);
//		q10= createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh34k,rgQH34K);
//		q11 = createQuestionSection(lblobs_34,txtQH34_OBSERVACION);
//		 
//		//q4 = createQuestionSection(0,lblpreguntaqh32m,gridqh32m.component()); 
//		//q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh33m,rgQH34G); 
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
////		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q4); 
//		form.addView(q5);
//		form.addView(q55);
//		form.addView(q6);
//		form.addView(q7);
//		form.addView(q8);
//		form.addView(q9);
//		form.addView(q10);
//		form.addView(q11);
//		
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar(){
//		uiToEntity(persona);
//		
////		if (persona.qh34j!=null) {						
//			if(fecha_referencia == null) {
//				persona.qh34j_ref = Util.getFechaActualToString();
//			}
//			else {
//				 persona.qh34j_ref = fecha_referencia;
//			}	
////		}
////		else {
////			persona.qh34j_ref = null;
////		}
//		
//		
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag=false;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		}
//		return flag;
//	}
//
//	
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if (Util.esVacio(persona.qh34b_1)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34b_1"); 
//			view = txtQH34B_1; 
//			error = true; 
//			return false; 
//		}
//		if (Util.esVacio(persona.qh34c)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34c"); 
//			view = rgQH34C; 
//			error = true; 
//			return false; 
//		}
//		
//		
//		if (Util.esVacio(persona.qh34d_a) && Util.esVacio(persona.qh34d_m)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34d"); 
//			view = txtQH34D_A; 
//			error = true; 
//			return false; 
//		}
//		if (!Util.esVacio(persona.qh34d_a) && (persona.qh34d_a<0 || persona.qh34d_a>115)) {
//			error = true;
//			view = txtQH34D_A;
//			mensaje = "Edad ingresado fuera de rango";
//			return false;
//		}
//
//		if (!Util.esVacio(persona.qh34d_m) && (persona.qh34d_m<0 || persona.qh34d_m>12)) {
//			error = true;
//			view = txtQH34D_M;
//			mensaje = "Edad en Meses ingresado fuera de rango";
//			return false;
//		}
//		
//		
//		if (Util.esVacio(persona.qh34e_a)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34e_a"); 
//			view = txtQH34E_A; 
//			error = true; 
//			return false; 
//		}
//		if (Util.esVacio(persona.qh34e_m)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34e_m"); 
//			view = txtQH34E_M; 
//			error = true; 
//			return false; 
//		}
//		if (persona.qh34e_a<App.ANIOPORDEFECTO_MIGRACION || persona.qh34e_a>App.ANIOPORDEFECTOSUPERIOR) {
//			error = true;
//			view = txtQH34E_A;
//			mensaje = "A�o ingresado fuera de rango";
//			return false;
//		}
//		if (persona.qh34e_m<=0 || persona.qh34e_m>12) {
//			error = true;
//			view = txtQH34E_M;
//			mensaje = "Mes ingresado fuera de rango";
//			return false;
//		}
//		
//		Calendar fechaactual = new GregorianCalendar();
//    	if(fecha_referencia!=null){			
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fecha_referencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual=cal;			
//		}
//		Integer anio_act = fechaactual.get(Calendar.YEAR);
//		Integer mes_act = fechaactual.get(Calendar.MONTH)+1;
////		Log.e("anio_act::",""+ anio_act);
////		Log.e("mes::",""+ mes_act);
////		Log.e("persona.qh34e_a::",""+ persona.qh34e_a);
////		Log.e("persona.qh34e_m::",""+ persona.qh34e_m);
//		
//		if (!Util.esDiferente(persona.qh34e_a,anio_act)) {
//			Log.e("mes::","111111");
//			if (Util.esMayor(persona.qh34e_m,mes_act)) {
//				Log.e("mes::","2222");
//				error = true;
//				view = txtQH34E_M;
//				mensaje = "Mes ingresado fuera de rango.";
//				return false;
//			}
//		}
//		
//				
//		if (Util.esVacio(persona.qh34g)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34g"); 
//			view = rgQH34G; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(persona.qh34g,9)){ 
//			if (Util.esVacio(persona.qh34go)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//				view = txtQH34GO; 
//				error = true; 
//				return false; 
//			} 
//		}
//		if(!Util.esDiferente(persona.qh34g,7) || !Util.esDiferente(persona.qh34g,8) || !Util.esDiferente(persona.qh34g,9)){ 
//			if (Util.esVacio(persona.qh34g_observacion)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Observaciones"; 
//				view = txtQH34G_OBSERVACION; 
//				error = true; 
//				return false; 
//			} 
//		}
//		
//		if (Util.esVacio(persona.qh34h)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.34h"); 
//			view = rgQH34H; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(persona.qh34h,4)){ 
//			if (Util.esVacio(persona.qh34ho)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//				view = txtQH34HO; 
//				error = true; 
//				return false; 
//			} 
//		}
//		Integer qh34PaisFexiste = txtQH34F_PAIS.getText().toString().trim().length()>0?1:0;
//		Integer qh34F_ccddFexiste = txtQH34F_CCDD.getText().toString().trim().length()>0?1:0;
//		Integer qh34F_ccppFexiste = txtQH34F_CCPP.getText().toString().trim().length()>0?1:0;
//		Integer qh34F_ccdiFexiste = txtQH34F_CCDI.getText().toString().trim().length()>0?1:0;
//		
//		if(qh34PaisFexiste==0 && qh34F_ccddFexiste==0 && !chbQH34F_CCDD_NS.isChecked() && !chbQH34F_PAIS_NS.isChecked()){
//			mensaje = preguntaVacia.replace("$", "La pregunta Departamento"); 
//			view = txtQH34F_CCDD; 
//			error = true; 
//			return false; 
//		}
//		if(qh34F_ccddFexiste==1){
//			if(qh34F_ccppFexiste==0 && !chbQH34F_CCPP_NS.isChecked()){
//				mensaje = preguntaVacia.replace("$", "La pregunta Provincia"); 
//				view = txtQH34F_CCPP; 
//				error = true; 
//				return false; 
//			}
//			if(qh34F_ccppFexiste==1){
//				if(qh34F_ccdiFexiste==0 && !chbQH34F_CCDI_NS.isChecked()){
//					mensaje = preguntaVacia.replace("$", "La pregunta Distrito"); 
//					view = txtQH34F_CCDI; 
//					error = true; 
//					return false; 
//				}
//			}
//		}
//		
//		
//		
//		if(qh34PaisFexiste==1 || chbQH34F_PAIS_NS.isChecked()){	
//			if(Util.esMayor(persona.qh34d_a,2) && !Util.esVacio(persona.qh34d_a)){ 
//				if (Util.esVacio(persona.qh34i)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.34i"); 
//					view = rgQH34I; 
//					error = true; 
//					return false; 
//				}
//			}
//			
//			if (Util.esVacio(persona.qh34j)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta P.34j"); 
//				view = rgQH34J; 
//				error = true; 
//				return false; 
//			}
//			
//			if(!Util.esDiferente(persona.qh34j,1)){
//				if (Util.esVacio(persona.qh34k)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.34k"); 
//					view = rgQH34K; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh34k,7)){ 
//					if (Util.esVacio(persona.qh34ko)) { 
//						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//						view = txtQH34KO; 
//						error = true; 
//						return false; 
//					} 
//				}
//			}
//			
//		}
//		
//
//		
//
//	
//		
//		
//	    return true;
//	}
//	
//	private void cargarDatos() {      
//		persona = getCuestionarioService().getPersonaMigracion(persona.id,persona.hogar_id,persona.qh34a,seccionesCargado);
//		if (persona == null) {
//			persona = new MIGRACION();
//			persona.id = App.getInstance().getMarco().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.qh34a = persona.qh34a;
//		}
//		nombre=persona.qh34b_1;
//		entityToUI(persona);
//		fecha_referencia = persona.qh34j_ref;
//		if(persona!=null && persona.qh34f_ccdi!=null){
//			txtQH34F_CCDI.setText("");
//			txtQH34F_CCDI.setText(persona.qh34f_ccdi);	
//		}
//		inicio();
//    }
//
//	public void inicio(){
//		RenombrarEtiquetas();
//		ValidarsiesSupervisora();
//		Integer qh34ccddFexiste = txtQH34F_CCDD.getText().toString().trim().length()>0?1:0;
//		Integer qh34paisFexiste = txtQH34F_PAIS.getText().toString().trim().length()>0?1:0;
//		
//		if(qh34ccddFexiste==1 || chbQH34F_CCDD_NS.isChecked()){
//			Log.e("aaaa: ","0");
//			onqh34F_CCDD_nsChangeValue();	
//		}
//		if(qh34paisFexiste==1 || chbQH34F_PAIS_NS.isChecked()){
//			Log.e("aaaa: ","00");
//			onqh34F_PAIS_nsChangeValue();	
//		}
//		
//		if(persona.qh34f_ccdd!=null){
//			CargarProvincia(persona.qh34f_ccdd.substring(0,2));
//		}
//		if(persona.qh34f_ccdd!=null && persona.qh34f_ccpp!=null){
//			CargarDistrito(persona.qh34f_ccdd.substring(0,2),persona.qh34f_ccpp.substring(0,2));	
//		}
//		rgQH34C.requestFocus();
//	}
//	
//	public void OcultarTecla() {
//		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(rgQH34G.getWindowToken(), 0);
//	}
//	
//
//	
//	public void onqh34F_CCDD_nsChangeValue(){
//		validarPreguntaPais();
//		if(chbQH34F_CCDD_NS.isChecked()){
//			Log.e("aaaa: ","1");
//			rgQH34G.requestFocus();
//			Util.cleanAndLockView(getActivity(),chbQH34F_CCPP_NS);
//			Util.cleanAndLockView(getActivity(),chbQH34F_PAIS_NS);
//			Util.cleanAndLockView(getActivity(),chbQH34F_CCDI_NS);
//			Util.cleanAndLockView(getActivity(),txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, txtQH34F_PAIS);
//		}
//		else{
//			Integer qh34ccddFexiste = txtQH34F_CCDD.getText().toString().trim().length()>0?1:0;
//			if(qh34ccddFexiste==1){
//				Log.e("aaaa: ","2");
//				Util.cleanAndLockView(getActivity(),txtQH34F_PAIS,chbQH34F_PAIS_NS);
//				Util.lockView(getActivity(),false ,txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//			}
//			else{
//				Log.e("aaaa: ","3");
//				Util.lockView(getActivity(),false, txtQH34F_PAIS,chbQH34F_PAIS_NS);
//				Util.lockView(getActivity(),false ,txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//			}
//			onqh34F_CCPP_nsChangeValue();
//			chbQH34F_CCDD_NS.requestFocus();
//		}
//	}
//	
//	
//	public void onqh34F_PAIS_nsChangeValue(){
//		validarPreguntaPais();
//		if(chbQH34F_PAIS_NS.isChecked()){
//			Log.e("aaaa: ","4");
//			rgQH34G.requestFocus();
//			Util.cleanAndLockView(getActivity(),txtQH34F_CCDD,txtQH34F_CCPP,txtQH34F_CCDI, txtQH34F_PAIS, chbQH34F_CCDD_NS, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//		}
//		else{
//			Integer qh34paisFexiste = txtQH34F_PAIS.getText().toString().trim().length()>0?1:0;
//			if(qh34paisFexiste==1){
//				Log.e("aaaa: ","5");
//				Util.cleanAndLockView(getActivity(),txtQH34F_CCDD,txtQH34F_CCPP, txtQH34F_CCDI, chbQH34F_CCDD_NS, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//				Util.lockView(getActivity(),false,txtQH34F_PAIS,chbQH34F_PAIS_NS);
//			}
//			else{
//				Log.e("aaaa: ","6");				
//				Util.lockView(getActivity(),false, txtQH34F_CCDD,txtQH34F_CCPP, txtQH34F_CCDI, chbQH34F_CCDD_NS, chbQH34F_CCPP_NS, chbQH34F_CCDI_NS);
//				Util.lockView(getActivity(),false, txtQH34F_PAIS,chbQH34F_PAIS_NS);
//				onqh34F_CCPP_nsChangeValue();
//			}
//			chbQH34F_PAIS_NS.requestFocus();
//		}
//	}
//	
//	
//	public void onqh34F_CCPP_nsChangeValue(){
//		if(chbQH34F_CCPP_NS.isChecked()){
//			Log.e("aaaa: ","7");
//			Util.cleanAndLockView(getActivity(),txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCDI_NS);
//		}
//		else{
//			Log.e("aaaa: ","8");
//			Util.lockView(getActivity(),false ,txtQH34F_CCPP,txtQH34F_CCDI, chbQH34F_CCDI_NS);
//			onqh34F_CCDI_nsChangeValue();
//		}
//	}
//	public void onqh34F_CCDI_nsChangeValue(){
//		if(chbQH34F_CCDI_NS.isChecked()){
//			Log.e("aaaa: ","9");
//			Util.cleanAndLockView(getActivity(),txtQH34F_CCDI);
//		}
//		else{
//			Log.e("aaaa: ","10");
//			Util.lockView(getActivity(),false ,txtQH34F_CCDI);
//		}
//	}
//	
//	
//	
//	
//
//    
//    public void validarPreguntaPais() {
//    	Integer qh34Fexiste = txtQH34F_PAIS.getText().toString().trim().length()>0?1:0;
//		if(qh34Fexiste==0 && !chbQH34F_PAIS_NS.isChecked()){	
//			Util.cleanAndLockView(getActivity(),rgQH34I,rgQH34J, rgQH34K, txtQH34KO);
//			q8.setVisibility(View.GONE);
//			q9.setVisibility(View.GONE);
//			q10.setVisibility(View.GONE);			
//		}
//		else{
//			Util.lockView(getActivity(),false ,rgQH34I,rgQH34J, rgQH34K);
//			q8.setVisibility(View.VISIBLE);
//			q9.setVisibility(View.VISIBLE);
//			q10.setVisibility(View.VISIBLE);
//			onqh34D_AChangeValue();
//			onqh34J_ChangeValue();			
//		}
//	}
//    
//    public void onqh34F_PaisChangeValue(){
//    	validarPreguntaPais();
//		rgQH34G.requestFocus();
//	}
//    
//    public void onqh34G_ChangeValue(){
//    	validarPreguntaPais();
// 		rgQH34H.requestFocus();
// 	}
//    
//	public void onqh34D_AChangeValue(){
//		Integer edad = txtQH34D_A.getText().toString().trim().length()>0?Integer.parseInt(txtQH34D_A.getText().toString()):-1;
//		Integer qh34Fexiste = txtQH34F_PAIS.getText().toString().trim().length()>0?1:0;
//		if(Util.esMayor(edad,2)){	
//			Util.lockView(getActivity(),false ,rgQH34I);
//			q8.setVisibility(View.VISIBLE);			
//		}
//		else{
//			Util.cleanAndLockView(getActivity(),rgQH34I);
//			q8.setVisibility(View.GONE);
//		}
//		txtQH34E_A.requestFocus();
//	}
//    
//	public void onqh34J_ChangeValue(){
//		if(Integer.parseInt(rgQH34J.getTagSelected("0").toString())==1){
//			Util.lockView(getActivity(),false ,rgQH34K);
//			q10.setVisibility(View.VISIBLE);
//			rgQH34K.requestFocus();
//		}
//		else{
//			Util.cleanAndLockView(getActivity(),rgQH34K, txtQH34KO);
//			q10.setVisibility(View.GONE);
//			btnAceptar.requestFocus();
//		}
//	}
//	
//	
//	private void CargarDepartamento() {
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, App.DEPARTAMENTOS);
//	    txtQH34F_CCDD.setAdapter(adapter);
//	    txtQH34F_CCDD.setThreshold(1);
//	}
//	
//	private void CargarProvincia(String ccdd){
//		List<Ubigeo> datos=getUbigeoService().getProvincias(ccdd);
//		String[] provincias=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			provincias[columna]=u.ccpp+" "+u.provincia;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, provincias);
//		txtQH34F_CCPP.setAdapter(adapter);
//	    txtQH34F_CCPP.setThreshold(1);
//	}
//	private void CargarDistrito(String ccdd,String ccpp){
//		List<Ubigeo> datos=getUbigeoService().getDistritos(ccdd, ccpp);
//		String[] distritos=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			distritos[columna]=u.ccdi+" "+u.distrito;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, distritos);
//		txtQH34F_CCDI.setAdapter(adapter);
//	    txtQH34F_CCDI.setThreshold(1);
//	}
//	
//	private void CargarDistritoProvincia(String ccdd){
//		List<Ubigeo> datos=getUbigeoService().getDistritoByccdd(ccdd);
//		String[] distritos=new String[datos.size()];
//		Integer columna=0;
//		for(Ubigeo u: datos){
//			distritos[columna]=u.ccpp+" "+u.provincia+" "+operador+" "+u.ccdi+" "+u.distrito;
//			columna++;
//		}
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, distritos);
//		txtQH34F_CCDI.setAdapter(adapter);
//	    txtQH34F_CCDI.setThreshold(1);
//	}
//	
//	private void CargarPaises(){
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, App.PAISES);
//	    txtQH34F_PAIS.setAdapter(adapter);
//	    txtQH34F_PAIS.setThreshold(1);
//	}
//	
//	public void EstadoOriginalDeEqtiquetas(){
//		lblpreguntaqh34c.setText(getResources().getString(R.string.migracion34c));
//		lblpreguntaqh34d.setText(getResources().getString(R.string.migracion34d));
//		lblpreguntaqh34e.setText(getResources().getString(R.string.migracion34e));
//		lblpreguntaqh34f.setText(getResources().getString(R.string.migracion34f));
//		lblpreguntaqh34g.setText(getResources().getString(R.string.migracion34g));
//		lblpreguntaqh34h.setText(getResources().getString(R.string.migracion34h));
//		lblpreguntaqh34i.setText(getResources().getString(R.string.migracion34i));
//		lblpreguntaqh34j.setText(getResources().getString(R.string.migracion34j));
//		lblpreguntaqh34k.setText(getResources().getString(R.string.migracion34k));
//		
//	}
//	
//	public void RenombrarEtiquetas(){
//		String replace ="(NOMBRE)";
//		EstadoOriginalDeEqtiquetas();
//		lblpreguntaqh34c.setText(lblpreguntaqh34c.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34d.setText(lblpreguntaqh34d.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34e.setText(lblpreguntaqh34e.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34f.setText(lblpreguntaqh34f.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34g.setText(lblpreguntaqh34g.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34h.setText(lblpreguntaqh34h.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34i.setText(lblpreguntaqh34i.getText().toString().replace(replace, nombre));
//		lblpreguntaqh34j.setText(lblpreguntaqh34j.getText().toString().replace(replace, nombre));
//		RenombrarFechaPregunta34J();
//		lblpreguntaqh34k.setText(lblpreguntaqh34k.getText().toString().replace(replace, nombre));
//		
//		//lblpreguntaqh29mobs.setText(lblpreguntaqh29mobs.getText().toString().replace("#", App.ANIOPORDEFECTO+""));
//	}
//	
//	public void RenombrarEtiquetassiCambioNombre(){
//		if(txtQH34B_1.getText().toString().length()>0){
//			String replace =nombre;
//			String nombre = txtQH34B_1.getText().toString();
//			EstadoOriginalDeEqtiquetas();
//			RenombrarEtiquetas();
//			lblpreguntaqh34c.setText(lblpreguntaqh34c.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34d.setText(lblpreguntaqh34d.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34e.setText(lblpreguntaqh34e.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34f.setText(lblpreguntaqh34f.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34g.setText(lblpreguntaqh34g.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34h.setText(lblpreguntaqh34h.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34i.setText(lblpreguntaqh34i.getText().toString().replace(replace, nombre));
//			lblpreguntaqh34j.setText(lblpreguntaqh34j.getText().toString().replace(replace, nombre));
//			RenombrarFechaPregunta34J();
//			lblpreguntaqh34k.setText(lblpreguntaqh34k.getText().toString().replace(replace, nombre));
//		}
//	}
//	
//    public void RenombrarFechaPregunta34J(){
//    	Calendar fechaactual = new GregorianCalendar();
//    	if(fecha_referencia!=null){			
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fecha_referencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual=cal;			
//		}
//    	Calendar fechainicio = MyUtil.RestarMeses(fechaactual,1);
//    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
//    	Calendar fechados =	MyUtil.RestarMeses(fechaactual,11);
//    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
//    	
//    	lblpreguntaqh34j.setText(lblpreguntaqh34j.getText().toString().replace("F1", F2)); 
//    	lblpreguntaqh34j.setText(lblpreguntaqh34j.getText().toString().replace("F2", F1));
//    	String F4="";
//    	if(F1.equals("Diciembre")){
//    		F4 = "del a�o pasado";
//    	}
//    	else{
//    		F4 = "de este a�o";  		
//    	}    	
//    	
//    	lblpreguntaqh34j.setText(lblpreguntaqh34j.getText().toString().replace("F4", F4));
//    	
//    	String F3="";
//    	if(F1.equals("Diciembre")){
//    		F3 = "de este a�o";
//    	}
//    	else{
//    		F3 = "del pr�ximo a�o";    		
//    	}
//    	//34h
//    	lblpreguntaqh34h.setText(lblpreguntaqh34h.getText().toString().replace("F1", F2)); 
//    	lblpreguntaqh34h.setText(lblpreguntaqh34h.getText().toString().replace("F2", F1));    	
//    	lblpreguntaqh34h.setText(lblpreguntaqh34h.getText().toString().replace("F3", F3));
//    }
//    
//    
//
//
//	
//	public void ValidarsiesSupervisora(){
//	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		txtQH34B_1.readOnly();
//	    		txtQH34B_2.readOnly();
//	    		txtQH34B_3.readOnly();
//	    		rgQH34C.readOnly(); 
//	    		txtQH34D_A.readOnly();
//	    		txtQH34D_M.readOnly();
//	    		txtQH34E_A.readOnly();
//	    		txtQH34E_M.readOnly();
//	    		
//	    		txtQH34F_CCDD.readOnly();
//	    		txtQH34F_CCPP.readOnly();
//	    		txtQH34F_CCDI.readOnly();
//	    		txtQH34F_PAIS.readOnly();
//	    		chbQH34F_CCDD_NS.readOnly();
//	    		chbQH34F_CCPP_NS.readOnly();
//	    		chbQH34F_CCDI_NS.readOnly();
//	    		chbQH34F_PAIS_NS.readOnly();
//	    		
//	    		rgQH34G.readOnly(); 
//	    		txtQH34GO.readOnly();
//	    		rgQH34H.readOnly(); 
//	    		txtQH34HO.readOnly();
//	    		rgQH34I.readOnly(); 
//	    		rgQH34J.readOnly(); 
//	    		rgQH34K.readOnly(); 
//	    		txtQH34KO.readOnly();
//	    		txtQH34G_OBSERVACION.setEnabled(false);
//	    		btnAceptar.setEnabled(false);
//	    	}
//	}
//	    
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//	
//	public VisitaService getVisitaService()
//	{
//		if(visitaService==null)
//		{
//				visitaService = VisitaService.getInstance(getActivity());
//		}
//			return visitaService;
//	}
//	public UbigeoService getUbigeoService()
//	{
//		if(ubigeoService==null)		{
//			ubigeoService = UbigeoService.getInstance(getActivity());
//		}
//			return ubigeoService;
//	}
//}
