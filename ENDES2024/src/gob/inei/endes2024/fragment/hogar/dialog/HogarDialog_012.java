package gob.inei.endes2024.fragment.hogar.dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.HogarFragment_010;
import gob.inei.endes2024.fragment.hogar.HogarFragment_011;
import gob.inei.endes2024.fragment.hogar.HogarFragment_012;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarDialog_012 extends DialogFragmentComponent {
	public interface BeneficiarioDialogListener {
		void onFinishEditDialog(String inputText);
	}

	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERSONA_ID_ORDEN;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnQHS3_1A;
	@FieldAnnotation(orderIndex = 3) 
	public CheckBoxField chbQHS3_1A; 
	@FieldAnnotation(orderIndex =4)
	public SpinnerField spnQHS3_1M;
	@FieldAnnotation(orderIndex = 5)
	public CheckBoxField chbQHS3_1M; 
	@FieldAnnotation(orderIndex = 6)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnCancelar;
	private static HogarFragment_012 caller;
	LinearLayout q0, q1, q2, q3, q4;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado,seccionesGrabadoB;
	private CuestionarioService hogarService;
	private LabelComponent lblNombre, lblTiempo, lblAnios, lblMeses;
	private GridComponent2 gridJefe;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public List<Seccion01> posiblesBeneficiarios;

	private Seccion03Service beneficiario;
	private Seccion01Service seccion01;
	
	public IntegerField txt1,txt2,txt3,txt4;
	public Seccion03 bean; 
	
	public int anios=26;
	
	private Seccion01Service seccion01service; 

	public static HogarDialog_012 newInstance(FragmentForm pagina, Seccion03 detalle) {
		caller = (HogarFragment_012) pagina;
		HogarDialog_012 f = new HogarDialog_012();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public HogarDialog_012(){
		super();
		seccionesGrabadoB = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","PREGUNTA_ID","PERSONA_ID")};
		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PREGUNTA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M")};
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion03) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//getDialog().setTitle("BENEFICIARIOS JUNTOS");
		tituloPreguntas();
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}
	private Seccion03Service getServiceBeneficiario() {
        if (beneficiario == null) {
        	beneficiario = Seccion03Service.getInstance(getActivity());
        }
        return beneficiario;
	}
	private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
	
	private void cargarDatos() {
		MyUtil.LiberarMemoria();
		llenarSpinnerBeneficiarios(bean);
		entityToUI(bean);
		if (bean.qhs3_1a!=null) {
			muestraPregunta_y(bean.qhs3_1a);
	    }
		if (bean.qhs3_1m!=null) {
			muestraPregunta_m(bean.qhs3_1m);
		}
		caretaker.addMemento("antes", bean.saveToMemento(Seccion03.class));
		inicio();
	}
	
	public void llenarSpinnerBeneficiarios(Seccion03 seccion3)
	{
		switch (seccion3.pregunta_id) {
		case App.VASOLECHE	:if(seccion3.persona_id_orden!=null) 
						{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMAVASOLECHE,App.EDADMAXIMAVASOLECHE,seccion3.pregunta_id);
						Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
						else{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMAVASOLECHE,App.EDADMAXIMAVASOLECHE,seccion3.pregunta_id);}
						break;
		case App.COMEDORPOP:if(seccion3.persona_id_orden!=null)
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity() ,getServiceSeccion01() , spnPERSONA_ID_ORDEN,  seccion3.id, seccion3.hogar_id,seccion3.persona_id_orden,App.EDADMINIMACOMEDORPOP,App.EDADMAXIMACOMEDORPOP,seccion3.pregunta_id);
							Util.cleanAndLockView(this.getActivity(), spnPERSONA_ID_ORDEN);}
							else
							{MyUtil.llenarBeneficiariosBeca18(this.getActivity(),getServiceSeccion01() , spnPERSONA_ID_ORDEN, seccion3.id, seccion3.hogar_id,-1,App.EDADMINIMACOMEDORPOP,App.EDADMAXIMACOMEDORPOP,seccion3.pregunta_id);}
							break;
		}
	}
	
	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(gridJefe.component());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(botones);
		return contenedor;
	}
	@Override
	protected void buildFields() {
		chbQHS3_1A=new CheckBoxField(this.getActivity(), R.string.hogarqh102_1NSNR, "1:0").size(altoComponente+10,220).callback("on96yChangeValue");
		chbQHS3_1M=new CheckBoxField(this.getActivity(), R.string.hogarqh102_1NSNR, "1:0").size(altoComponente+10,220).callback("on96mChangeValue");
		lblTiempo = new LabelComponent(this.getActivity()).alinearDerecha().size(altoComponente + 10, 630).text(R.string.hogarqh102_1t).textSize(18).alinearIzquierda();
		lblNombre = new LabelComponent(this.getActivity()).size(altoComponente + 20, 270).text(R.string.hogarqh102_1n).textSize(16).negrita().alinearIzquierda();
		lblAnios = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh102_1a).textSize(16).negrita().alinearIzquierda();
		lblMeses = new LabelComponent(this.getActivity()).size(altoComponente + 10, 270).text(R.string.hogarqh102_1m).textSize(16).negrita().alinearIzquierda();
		textoPreguntas();
		spnPERSONA_ID_ORDEN = new SpinnerField(getActivity()).size(altoComponente + 10, 360).callback("onPERSONA_ID_ORDENChangeValue");
		spnQHS3_1A=new SpinnerField(getActivity()).size(70, 140).callback("on96_yChangeValue");
		cargarSpinnerqh_y();
		spnQHS3_1M=new SpinnerField(getActivity()).size(70, 140).callback("on96_mChangeValue");
		cargarSpinnerqh_m();
		gridJefe = new GridComponent2(this.getActivity(), 3);
		gridJefe.addComponent(lblNombre);
		gridJefe.addComponent(spnPERSONA_ID_ORDEN,2,1);
		gridJefe.addComponent(lblTiempo,3,1);
		gridJefe.addComponent(lblAnios);
		gridJefe.addComponent(spnQHS3_1A);
		gridJefe.addComponent(chbQHS3_1A);
		gridJefe.addComponent(lblMeses);
		gridJefe.addComponent(spnQHS3_1M);
		gridJefe.addComponent(chbQHS3_1M);
		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnAceptar).size(200, 55);
		btnCancelar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(
				R.string.btnCancelar).size(200, 55);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				HogarDialog_012.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarBeca18(bean);
				//caller.cargarTabla();				
								
				HogarDialog_012.this.dismiss();
			}
		});
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
	public boolean grabar() {
		uiToEntity(bean);
		bean.qhs3_1a=combinaPregunta_y();
		bean.qhs3_1m=combinaPregunta_m();
		
		
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getServiceBeneficiario().saveOrUpdate(bean, dbTX,seccionesGrabadoB);
			if (!flag) {
				throw new Exception(
						"Ocurri� un problema al grabar beneficiario 1");
			}
			getServiceBeneficiario().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}

	private boolean validar() {
		String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
		if (Util.esVacio(bean.persona_id_orden)) {
			error = true;
			view = spnPERSONA_ID_ORDEN;
			mensaje = preguntaVacia.replace("$", "Seleccione Nombre");
			return false;
		}
		if(Util.esVacio(bean.qhs3_1a)){
				error = true;
				view = spnQHS3_1A;
				mensaje = preguntaVacia.replace("$", "Seleccione A�os");
				return false;
		}
		if(Util.esVacio(bean.qhs3_1m)){
				error = true;
				view = spnQHS3_1M;
				mensaje = preguntaVacia.replace("$", "Seleccione Meses");
				return false;
		}
		if(!Util.esDiferente(bean.qhs3_1a, 0) && !Util.esDiferente(bean.qhs3_1m, 0)){
			error = true;
			view = spnQHS3_1A;
			mensaje = "El Mes y A�o no pueden ser cero a la vez!!!";
			return false;
		}
		return true;
	}
	private void inicio() {
		on96_yChangeValue();
		on96yChangeValue();
		on96_mChangeValue();
		on96mChangeValue();
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
    public Integer combinaPregunta_y() {
	    Integer p_y=null;
	    if(bean.pregunta_id==App.VASOLECHE){
        	anios = 26;
        }else if(bean.pregunta_id==App.COMEDORPOP){
        	anios = 19;
        }
	    if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString() )) {
	   	 	p_y= Integer.parseInt(spnQHS3_1A.getValue().toString());
	    }
	    if (chbQHS3_1A.getValue().toString().equals("1")) {
	   	 	p_y= bean.getConvertqh92((Integer.parseInt(chbQHS3_1A.getValue().toString())));
	    }
	    return p_y;         
    }
    public void muestraPregunta_y(Integer p92y) {
    	if(bean.pregunta_id==App.VASOLECHE){
        	anios = 26;
        }else if(bean.pregunta_id==App.COMEDORPOP){
        	anios = 19;
        }
        if (p92y>=0 && p92y<=anios) {        
              spnQHS3_1A.setSelection(p92y+1);
        }            
        else {
              if (p92y==98) {                
				 chbQHS3_1A.setChecked(true);
			}                        
        }
    }
	private void cargarSpinnerqh_y() {
		if(bean.persona_id_orden==null){
	        if(bean.pregunta_id==App.VASOLECHE){
	        	anios = 26;
	        }else if(bean.pregunta_id==App.COMEDORPOP){
	        	anios = 19;
	        }
		}
		
        if(bean.persona_id_orden!=null){
		
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			if(bean.pregunta_id==App.VASOLECHE){
				if(band.qh07==0){
					anios = band.qh07;
				}else{
					anios = band.qh07;
				}
				
				if(anios>26){
					anios=26;
				}
			}else if(bean.pregunta_id==App.COMEDORPOP){
				anios = band.qh07;
				if(anios>19){
					anios=19;
				}
			}
		}        
        
		List<Object> keys = new ArrayList<Object>();
        keys.add(null);
        
        String[] items1 = new String[anios+2];
        items1[0] = "Selec";
        
        for(int i=0;i<=anios;i++){
        	keys.add(i);
        	items1[i+1] = ""+i;
        }
        
//        keys.add(1);
//        keys.add(2);
//        keys.add(3);
//        keys.add(4);
//        keys.add(5);
//        keys.add(6);      
        
//        String[] items1 = new String[] {"Selec","1","2","3","4","5","6"};
//        
//        if(bean.pregunta_id==App.VASOLECHE){
//        	items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26"};
//        }else if(bean.pregunta_id==App.COMEDORPOP){
//        	items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"};
//        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1A.setAdapterWithKey(adapter, keys);
    }
    public Integer combinaPregunta_m() {
	    Integer p_m=null;
	    if (MyUtil.incluyeRango(0,11,spnQHS3_1M.getValue().toString() )) {
	   	 	p_m= Integer.parseInt(spnQHS3_1M.getValue().toString());
	    }
	    if (chbQHS3_1M.getValue().toString().equals("1")) {
	   	 	p_m= bean.getConvertqh92((Integer.parseInt(chbQHS3_1M.getValue().toString())));
	    }
	    return p_m;         
    }
    public void muestraPregunta_m(Integer p_m) {
            if (p_m>=0 && p_m<=11) {        
                  spnQHS3_1M.setSelection(p_m+1);
            }            
            else {
                  if (p_m==98) {                
    				 chbQHS3_1M.setChecked(true);
    			}                        
            }
    }
	private void cargarSpinnerqh_m() {
        List<Object> keys = new ArrayList<Object>();
        keys.add(null);
//        keys.add(1);
//        keys.add(2);
//        keys.add(3);
//        keys.add(4);
//        keys.add(5);
//        keys.add(6);
//        keys.add(7);
//        keys.add(8);
//        keys.add(9);
//        keys.add(10);
//        keys.add(11);
//        keys.add(12);
        for(int i=0;i<=11;i++){
        	keys.add(i);
        }
        String[] items1 = new String[] {"Selec","0","1","2","3","4","5","6","7","8","9","10","11"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, R.id.textview, items1);
        spnQHS3_1M.setAdapterWithKey(adapter, keys);
    }
	public void on96_yChangeValue() {
			if (MyUtil.incluyeRango(0,anios,spnQHS3_1A.getValue().toString())) {
				Util.cleanAndLockView(getActivity(),chbQHS3_1A);
				Util.lockView(getActivity(), false,chbQHS3_1A);
				spnQHS3_1M.requestFocus();
			} else {
//				Util.lockView(getActivity(), false,chbQHS3_1A);
				chbQHS3_1A.requestFocus();			
			}
	}
	public void on96yChangeValue() {
			if (MyUtil.incluyeRango(1,1,chbQHS3_1A.getValue().toString())) {
				Util.cleanAndLockView(getActivity(),spnQHS3_1A);
				Util.lockView(getActivity(), false,spnQHS3_1A);
				spnQHS3_1M.requestFocus();				
			} else {
//				Util.lockView(getActivity(), false,spnQHS3_1A);
				spnQHS3_1A.requestFocus();			
			}
	}
	public void on96_mChangeValue() {
		if (MyUtil.incluyeRango(0,12,spnQHS3_1M.getValue().toString())) {
			Util.cleanAndLockView(getActivity(),chbQHS3_1M);
			Util.lockView(getActivity(), false,chbQHS3_1M);
			btnAceptar.requestFocus();
			
		} else {
//			Util.lockView(getActivity(), false,chbQHS3_1M);
			chbQHS3_1M.requestFocus();			
		}
	}
	public void on96mChangeValue() {	
			if (MyUtil.incluyeRango(1,1,chbQHS3_1M.getValue().toString())) {
				Util.cleanAndLockView(getActivity(),spnQHS3_1M);
				Util.lockView(getActivity(), false,spnQHS3_1M);
				btnAceptar.requestFocus();				
			} else {
//				Util.lockView(getActivity(), false,spnQHS3_1M);
				spnQHS3_1M.requestFocus();			
			}			
	}
	
	public void textoPreguntas(){
		if(bean.pregunta_id==App.VASOLECHE){
			lblNombre.text(R.string.hogarqh102_1n);
			lblTiempo.text(R.string.hogarqh102_1t);
		}else if(bean.pregunta_id==App.COMEDORPOP){			
			lblNombre.text(R.string.hogarqh104_1n);
			lblTiempo.text(R.string.hogarqh104_1t);
		}
	}
	public void tituloPreguntas(){
		if(bean.pregunta_id==App.VASOLECHE){
			getDialog().setTitle("BENEFICIARIOS VASO DE LECHE");
		}else if(bean.pregunta_id==App.COMEDORPOP){			
			getDialog().setTitle("BENEFICIARIOS COMEDOR POPULAR");
		}
	}
	public void onPERSONA_ID_ORDENChangeValue(){
		boolean entro=false;
		uiToEntity(bean);
		if(bean.persona_id_orden!=null){
		
			Seccion01 band = new Seccion01();
			band = getSeccion01Service().getPersonaEdad(bean.id, bean.hogar_id,bean.persona_id_orden);
			if(bean.pregunta_id==App.VASOLECHE){
				entro = true;
				if(band.qh07==0){
					anios = band.qh07;
				}else{
					anios = band.qh07;
				}
				if(anios>26){
					anios=26;
				}
			}else if(bean.pregunta_id==App.COMEDORPOP){
				entro = true;
				anios = band.qh07;
				if(anios>19){
					anios=19;
				}
			}
			if(entro){
				cargarSpinnerqh_y();				
			}
		}		
	}
}
