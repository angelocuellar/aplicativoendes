package gob.inei.endes2024.fragment.hogar; 
import java.sql.SQLException; 
import java.util.ArrayList;
import java.util.List;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.dialog.HogarDialog_012;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation; 
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.IntegerField; 
import gob.inei.dnce.components.FragmentForm; 
import gob.inei.dnce.components.LabelComponent; 
import gob.inei.dnce.components.MasterActivity; 
import gob.inei.dnce.components.RadioGroupOtherField; 
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField; 
import gob.inei.dnce.components.ToastMessage; 
import gob.inei.dnce.util.Util; 
import gob.inei.dnce.interfaces.Respondible;
import android.os.Bundle; 
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity; 
import android.view.LayoutInflater; 
import android.view.MenuItem;
import android.view.View; 
import android.view.ViewGroup; 
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout; 
import android.widget.ScrollView; 

public class HogarFragment_012 extends FragmentForm implements Respondible { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH101; 
//	@FieldAnnotation(orderIndex=2) 
//	public IntegerField txtQH96M; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH103; 
	public Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private Seccion01Service seccion01service; 
	private Seccion03Service seccion03service;
	private LabelComponent lblTitulo,lblVL,lblCP,lblP101,lblP102,lblP103,lblP104; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoSeccion03;
	public TableComponent tcVasoLeche;
	public ButtonComponent btnAgregarVasoL,btnAgregarComedorPop;
	private List<Hogar> detalles;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public Seccion01 entrevistado;
	public Seccion01 elegido;
	public Seccion03 elegidos03;
	public List<Seccion03> beneficiadosVasoLeche;
	public List<Seccion03> beneficiadosComedorPop;
	public TableComponent tcComedorPop;
	private Seccion01Service seccion01;
	private Seccion03Service seccion03;
	private enum ACTION{ELIMINAR,MENSAJE}
	private ACTION action;
	private DialogComponent dialog;
	
	private boolean vasoLe=false,comPop=false;
	
	public LinearLayout q0,q1,q2,q3,q4;
	
	public int accion=0;
	
	public HogarFragment_012() {}
	
	public HogarFragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
    @Override 
    public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH101","ID","HOGAR_ID","QH103","PERSONA_INFORMANTE_ID")};  
		seccionesCargadoSeccion03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","QH02_1","ID","HOGAR_ID","PREGUNTA_ID","ESTADO3")}; 
		//seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH101","QH96_1A","QH96_1M","QH103","QH100_1A","QH10_1M")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH101","QH103")};
		cargarDatos();
		return rootView; 
    } 
  
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcVasoLeche.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(0, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(0, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			//Seccion03 seleccionB18 = (Seccion03) info.targetView.getTag();
		}
		if (v.equals(tcComedorPop.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(1, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(1, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
	}
  
    @Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosVasoLeche.get(info.position));
						break;
				case 1:	 
						accion = 0;
						
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,
		                        TIPO_DIALOGO.YES_NO, getResources().getString(
		                                        R.string.app_name),
		                        "¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadosVasoLeche.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosComedorPop.get(info.position));
						break;
				case 1:	 
						accion = 0;
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,
		                        TIPO_DIALOGO.YES_NO, getResources().getString(
		                                        R.string.app_name),
		                        "¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadosComedorPop.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		return super.onContextItemSelected(item);
	}
    
    @Override 
    protected void buildFields() { 
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar_3_b).textSize(21).centrar().negrita();
		lblVL = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		lblCP = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		
		lblP101 = new LabelComponent(this.getActivity()).text(R.string.hogarqh101).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP102 = new LabelComponent(this.getActivity()).text(R.string.hogarqh102).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP103 = new LabelComponent(this.getActivity()).text(R.string.hogarqh103).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP104 = new LabelComponent(this.getActivity()).text(R.string.hogarqh104).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		
		rgQH101=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh101_1,R.string.hogarqh103_2,R.string.hogarqh103_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH101");
		rgQH103=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh103_1,R.string.hogarqh103_2,R.string.hogarqh103_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH103");
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcVasoLeche = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//			tcComedorPop = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//		}
//		else{
			tcVasoLeche = new TableComponent(getActivity(), this,App.ESTILO).size(330, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tcComedorPop = new TableComponent(getActivity(), this,App.ESTILO).size(330, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcVasoLeche.addHeader(R.string.m_orden_b18_hogar, 2f,
				TableComponent.ALIGN.CENTER);
		tcVasoLeche.addHeader(R.string.m_nombre_b18_hogar, 3.5f,
				TableComponent.ALIGN.LEFT);
		tcVasoLeche.addHeader(R.string.m_anio_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		tcVasoLeche.addHeader(R.string.m_mes_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		btnAgregarVasoL = new ButtonComponent(getActivity(),
				App.ESTILO_BOTON).text(
				R.string.btnAgregar).size(200, 60);
		btnAgregarVasoL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
					agregarVasoLeche();
			}
		});	
		
		
		tcComedorPop.addHeader(R.string.m_orden_b18_hogar, 2f,
				TableComponent.ALIGN.CENTER);
		tcComedorPop.addHeader(R.string.m_nombre_b18_hogar, 3.5f,
				TableComponent.ALIGN.LEFT);
		tcComedorPop.addHeader(R.string.m_anio_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		tcComedorPop.addHeader(R.string.m_mes_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		btnAgregarComedorPop = new ButtonComponent(getActivity(),
				App.ESTILO_BOTON).text(
				R.string.btnAgregar).size(200, 60);
		btnAgregarComedorPop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				agregarComedorPop();
			}
		});	
		textoNegrita();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblP101,rgQH101);
		LinearLayout q2_1 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblVL,btnAgregarVasoL);
		q2 = createQuestionSection(lblP102, q2_1,tcVasoLeche.getTableView());
		LinearLayout q2_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblCP,btnAgregarComedorPop);
		q3 = createQuestionSection(lblP104, q2_2,tcComedorPop.getTableView());
			
		q4 = createQuestionSection(lblP103,rgQH103);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q4);
		form.addView(q3);
		/*Aca agregamos las preguntas a la pantalla*/ 
		return contenedor; 
    } 
    
    
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar);
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh101!=null) {
				hogar.qh101=hogar.getConver80a(hogar.qh101);
			}
			if (hogar.qh103!=null) {
				hogar.qh103=hogar.getConver80a(hogar.qh103);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/********VARIABLES PARA LA NAVEGACION NO BORRAR*******/  
		App.getInstance().getHogar().qh101=hogar.qh101;
		App.getInstance().getHogar().qh103=hogar.qh103;
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh101) && vasoLe) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.101"); 
			view = rgQH101; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh101, 1,1) && vasoLe) {
			if (beneficiadosVasoLeche.size()<1) { 
				mensaje = "La pregunta P.92 debe tener algun Beneficiario"; 
				view = btnAgregarVasoL;
				error = true; 
				return false; 
			}
		}
		 
		if (Util.esVacio(hogar.qh103) && comPop) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.103"); 
			view = rgQH103; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh103, 1,1) && comPop) {
			if (beneficiadosComedorPop.size()<1) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.104"); 
				view = btnAgregarComedorPop; 
				error = true; 
				return false; 
			} 		
		}
		 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh101!=null) {
			hogar.qh101=hogar.setConver80a(hogar.qh101);
		}
		if (hogar.qh103!=null) {
			hogar.qh103=hogar.setConver80a(hogar.qh103);
		}
		entityToUI(hogar); 
		cargarTabla();
		eliminarBeneficiados();
		inicio(); 
    } 
    
    public void cargarTabla(){
//    	beneficiadosVasoLeche = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
//    			App.getInstance().getHogar().hogar_id,App.VASOLECHE,seccionesCargadoSeccion03);
//    	tcVasoLeche.setData(beneficiadosVasoLeche,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//    	registerForContextMenu(tcVasoLeche.getListView());
    	beneficiadosVasoLeche = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
    			App.getInstance().getHogar().hogar_id,App.VASOLECHE,seccionesCargadoSeccion03);
    	tcVasoLeche.setData(beneficiadosVasoLeche,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadosVasoLeche!=null&& beneficiadosVasoLeche.size()>0){
	    	for (int row = 0; row < beneficiadosVasoLeche.size(); row++) {
				if (obtenerEstado(beneficiadosVasoLeche.get(row)) == 1) {
					// borde de color azul
					tcVasoLeche.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosVasoLeche.get(row)) == 2) {
					// borde de color rojo
					tcVasoLeche.setBorderRow(row, true, R.color.red);
				} else {
					tcVasoLeche.setBorderRow(row, false);
				}
			}
    	}
    	tcVasoLeche.reloadData();
	   	registerForContextMenu(tcVasoLeche.getListView());
	   	
    	
//    	beneficiadosComedorPop = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
//    			App.getInstance().getHogar().hogar_id,App.COMEDORPOP,seccionesCargadoSeccion03);
//    	tcComedorPop.setData(beneficiadosComedorPop, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//	   	registerForContextMenu(tcComedorPop.getListView());
	   	beneficiadosComedorPop = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
    			App.getInstance().getHogar().hogar_id,App.COMEDORPOP,seccionesCargadoSeccion03);
    	tcComedorPop.setData(beneficiadosComedorPop, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadosComedorPop!=null&& beneficiadosComedorPop.size()>0){
	    	for (int row = 0; row < beneficiadosComedorPop.size(); row++) {
				if (obtenerEstado(beneficiadosComedorPop.get(row)) == 1) {
					// borde de color azul
					tcComedorPop.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosComedorPop.get(row)) == 2) {
					// borde de color rojo
					tcComedorPop.setBorderRow(row, true, R.color.red);
				} else {
					tcComedorPop.setBorderRow(row, false);
				}
			}
    	}
    	tcComedorPop.reloadData();
	   	registerForContextMenu(tcComedorPop.getListView());
    }
    
    public void refrescarBeca18(Seccion03 beneficiario) {
        if(!Util.esDiferente(beneficiario.pregunta_id,App.VASOLECHE)){
    	if (beneficiadosVasoLeche.contains(beneficiario)) {
                cargarTabla();
                return;
        } else {
        	beneficiadosVasoLeche.add(beneficiario);
                cargarTabla();
        }
        }
        if(!Util.esDiferente(beneficiario.pregunta_id, App.COMEDORPOP))
        {if (beneficiadosComedorPop.contains(beneficiario)) {
            cargarTabla();
            return;
        } else {
        	beneficiadosComedorPop.add(beneficiario);
            cargarTabla();
        }
        	
        }
    }

    private void inicio() { 
    	mostrarPreguntas();
    	salto_rgQH101();
    	salto_rgQH103();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	} 
    
    private int obtenerEstado(Seccion03 detalle) {
//    	Log.e("ddddddd",""+detalle.estado3);
 		if (!Util.esDiferente(detalle.estado3, 0)) {
 			return 1 ;
 		} else if (!Util.esDiferente(detalle.estado3,1)) {
 			return 2;
 		}
 		return 0;
 	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
    
    public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
    
    public void salto_rgQH101() {
		int valor = Integer.parseInt(rgQH101.getTagSelected("0").toString());
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarVasoL,tcVasoLeche);
			
			if(beneficiadosVasoLeche.size()>0 && valor!=0){
				accion = App.VASOLECHE;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Vaso de Leche!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			
			rgQH103.requestFocus();
		} else { 
            Util.lockView(getActivity(), false, btnAgregarVasoL,tcVasoLeche);
            tcVasoLeche.requestFocus();
		} 
	}
    public void salto_rgQH103() {
		int valor = Integer.parseInt(rgQH103.getTagSelected("0").toString());
		
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarComedorPop,tcComedorPop);

			if(beneficiadosComedorPop.size()>0 && valor!=0){
				accion = App.COMEDORPOP;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Comedor Popular!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}			
		} else {
			Util.lockView(getActivity(), false,  btnAgregarComedorPop,tcComedorPop);
			tcComedorPop.requestFocus();
		}
	}
    protected void agregarVasoLeche(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.VASOLECHE;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetalle(bean);
    }
    protected void agregarComedorPop(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.COMEDORPOP;
		bean.persona_id = hogar.persona_informante_id;;
		abrirDetalle(bean);
}
    
    public void abrirDetalle(Seccion03 tmp) {    	
		FragmentManager fm = HogarFragment_012.this.getFragmentManager();
		HogarDialog_012 aperturaDialog = HogarDialog_012.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public void EditarSeccion03(Seccion03 tmp) {	
		FragmentManager fm = HogarFragment_012.this.getFragmentManager();
		HogarDialog_012 aperturaDialog = HogarDialog_012.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
   
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
    
    private Seccion03Service getServiceSeccion03() {
        if (seccion03 == null) {
        	seccion03 = Seccion03Service.getInstance(getActivity());
        }
        return seccion03;
	}
   @Override
    public void onCancel() {
	   if(accion==App.VASOLECHE){
      	
      		rgQH101.setTagSelected(1);
      }
	   if(accion==App.COMEDORPOP){
     	
     		rgQH103.setTagSelected(1);
     }
    }

   @Override 
    public void onAccept() {
        if (action == ACTION.MENSAJE) {
                return;
        }
        if (dialog == null) {
                return;
        }
        
        if(accion==0){
	        Seccion03 bean = (Seccion03) dialog.get("seleccion");
	        
	        try {//bean.persona_id=1;
	        if(!Util.esDiferente(bean.pregunta_id, App.VASOLECHE))
	        {    beneficiadosVasoLeche.remove(bean);}
	        if(!Util.esDiferente(bean.pregunta_id, App.COMEDORPOP))
	        {  beneficiadosComedorPop.remove(bean);}
	        	boolean borrado=false;
	                borrado =getSeccion03Service().BorrarPersonaPregunta(bean);
	                if (!borrado) {
	                        throw new SQLException("Persona no pudo ser borrada.");
	                }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        if(accion==App.VASOLECHE){
        	
        	
        	try {
	            while(beneficiadosVasoLeche.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadosVasoLeche.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadosVasoLeche.remove(beneficiadosVasoLeche.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        if(accion==App.COMEDORPOP){
        
        	
        	try {
	            while(beneficiadosComedorPop.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadosComedorPop.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadosComedorPop.remove(beneficiadosComedorPop.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }

        cargarTabla();
		salto_rgQH101();
        
    }
   
   public void PosiblesBeneficiarios(){
		List<Seccion01> PosibleVasoLe = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAVASOLECHE,App.EDADMAXIMAVASOLECHE,App.VASOLECHE);
	   	if(PosibleVasoLe.size()>0){
	   		vasoLe = true;
		}else{vasoLe=false;}
	   	
	   List<Seccion01> PosibleComPop = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMACOMEDORPOP,App.EDADMAXIMACOMEDORPOP,App.COMEDORPOP);
	   	if(PosibleComPop.size()>0){
	   		comPop = true;
		}else{comPop=false;}
   }
   
   public void mostrarPreguntas(){
   	int val101=0,val103=0;
   	PosiblesBeneficiarios();
   	
   	if(vasoLe){
   		q1.setVisibility(View.VISIBLE);
   		val101 = Integer.parseInt(rgQH101.getTagSelected("0").toString());
   		
   		if (Util.esDiferente(val101, 1,1)) {
   			q2.setVisibility(View.GONE);
   		}else{
   			q2.setVisibility(View.VISIBLE);
   		}    		
   	}else{
   		q1.setVisibility(View.GONE);
   		q2.setVisibility(View.GONE);
   	}
   	
   	if(comPop){
   		q4.setVisibility(View.VISIBLE);
   		val103 = Integer.parseInt(rgQH103.getTagSelected("0").toString());
   		
   		if (Util.esDiferente(val103, 1,1)) {
   			q3.setVisibility(View.GONE);
   		}else{
   			q3.setVisibility(View.VISIBLE);
   		}    		
   	}else{
   		q4.setVisibility(View.GONE);
   		q3.setVisibility(View.GONE);
   	}
   }
   
   public void eliminarBeneficiados(){
	   boolean eliminoVL=false,eliminoCP=false;
	   for(int i=0;i<beneficiadosVasoLeche.size();i++){
		   Seccion01 band = new Seccion01();
		   band = getSeccion01Service().getPersonaEdad(beneficiadosVasoLeche.get(i).id, beneficiadosVasoLeche.get(i).hogar_id, beneficiadosVasoLeche.get(i).persona_id_orden);
		   		   
		   if(!MyUtil.incluyeRango(App.EDADMINIMAVASOLECHE, App.EDADMAXIMAVASOLECHE, band.qh07)){
			   eliminoVL = true;
			   try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosVasoLeche.get(i));
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
		                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        } 
		   }
	   }
	   
	   for(int i=0;i<beneficiadosComedorPop.size();i++){
		   Seccion01 band = new Seccion01();
		   band = getSeccion01Service().getPersonaEdad(beneficiadosComedorPop.get(i).id, beneficiadosComedorPop.get(i).hogar_id, beneficiadosComedorPop.get(i).persona_id_orden);
		   		   
		   if(!MyUtil.incluyeRango(App.EDADMINIMACOMEDORPOP, App.EDADMAXIMACOMEDORPOP, band.qh07)){
			   eliminoCP = true;
			   try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosComedorPop.get(i));
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
		                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        } 
		   }
	   }
	   
	   if(eliminoVL || eliminoCP){
		   cargarTabla();
	   }
   }
   
   public void textoNegrita(){
  	   Spanned texto101 = Html.fromHtml(lblP101.getText()+"<b> Vaso de Leche?</b>");    	
  	   lblP101.setText(texto101);
  	   
  	   Spanned texto103 = Html.fromHtml(lblP103.getText()+"<b> Comedor popular?</b>");    	
  	   lblP103.setText(texto103);
	}
	
   
   public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH101.readOnly();
			tcComedorPop.setEnabled(false);
			rgQH103.readOnly();
			tcVasoLeche.setEnabled(false);
			btnAgregarComedorPop.setEnabled(false);
			btnAgregarVasoL.setEnabled(false);
		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (hogar.qh101!=null) {
				hogar.qh101=hogar.getConver80a(hogar.qh101);
			}
			if (hogar.qh103!=null) {
				hogar.qh103=hogar.getConver80a(hogar.qh103);
			}	
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
