package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.dialog.CHMORTALIDAD_Fragment_000_1Dialog;
import gob.inei.endes2024.fragment.hogar.dialog.CHMORTALIDAD_Fragment_000_2Dialog;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.AdapterView.OnItemClickListener;

public class HogarFragment_000 extends FragmentForm implements Respondible {
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH27; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQH28;
	@FieldAnnotation(orderIndex=3)
	public ButtonComponent btnAgregar;
	@FieldAnnotation(orderIndex=4)
	public TextAreaField txtQHM_OBS;

		 
	public TableComponent tcMortalidad;
	public List<MORTALIDAD> fallecidas;
	public Integer numHerMayores;
	public boolean diferencia8anios;
	private Seccion01 informantehogar;
	
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargadoHogar, seccionesCargadoseccion01,seccionesCargadoVisita,seccionesGrabadomortalidad; 
	
	private CuestionarioService cuestionarioService;
	private Seccion01Service Personaservice;
	
	HermanosFallecidosClickListener adapter;
	public LabelComponent lbltitulo,lblprimeraindicacion,lblsegundaindicacion,lblpreguntaqh27,lblpreguntaqh28,lblobs_mortalidad;
	private DialogComponent dialog;
	
	public GridComponent2 gridpregunta901;
	
	public LinearLayout q0;
	public LinearLayout q1;
	public LinearLayout q2;
	public LinearLayout q3;
	public LinearLayout q4;
	
	Hogar hogar;
	
	private enum ACTION {
		ELIMINAR,ELIMINARTODO
	}

	private ACTION action;
	
	public HogarFragment_000 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}
	@Override
	protected View createUI() {
		// TODO Auto-generated method stub
		buildFields();
		q0 = createQuestionSection(lbltitulo);
		q1 = createQuestionSection(lblpreguntaqh27,rgQH27);
		q2 = createQuestionSection(lblpreguntaqh28,txtQH28);
		q3 = createQuestionSection(0,lblprimeraindicacion,lblsegundaindicacion,btnAgregar,tcMortalidad.getTableView());
		q4 = createQuestionSection(lblobs_mortalidad,txtQHM_OBS);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
	}
	  @Override 
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
			rootView = createUI(); 
			initObjectsWithoutXML(this, rootView); 
			adapter = new HermanosFallecidosClickListener();
			tcMortalidad.getListView().setOnItemClickListener(adapter);
			enlazarCajas(); 
			listening();
			seccionesCargadoVisita = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QHVDIA_INI", "QHVMES_INI", "QHVANIO_INI", "QHHORA_INI","QHMIN_INI", "QHVRESUL", "QHVHORA_FIN", "QHVMIN_FIN","QHVDIAP", "QHVMESP", "QHVHORAP", "QHVMINUP", "NRO_VISITA","ID", "HOGAR_ID","QHVRESUL_O") };
			seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ESTADO","QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M","QH33M_O", "NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID")};
			seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH27", "QH28", "QHM_OBS","ID","HOGAR_ID","PERSONA_INFORMANTE_ID") };
			seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH27", "QH28","QHM_OBS") };
			seccionesGrabadomortalidad = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH29M","QH30M","QH31M","QH32M_M","QH32M_Y","QH33M","QH33M_O","NRO_ORDEN_ID","ID","HOGAR_ID","PERSONA_ID")};
			return rootView; 
		} 
	@Override
	protected void buildFields() {
		lbltitulo = new LabelComponent(getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.titulo_mortalidad).textSize(20).centrar();
		lblprimeraindicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.mortalidad_indicacion1).textSize(19);
		lblsegundaindicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.mortalidad_indicacion2).textSize(16).negrita();
		lblpreguntaqh27 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.mortalidad_qh27).textSize(19);
		lblpreguntaqh28 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.mortalidad_qh28).textSize(19);
		rgQH27 = new RadioGroupOtherField(getActivity(),R.string.mortalidad_qh27_1,R.string.mortalidad_qh27_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onh27ChangeValue");
		txtQH28 = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2).callback("onqh28Changevalue");

		
		lblobs_mortalidad = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
		txtQHM_OBS = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
		
		
		btnAgregar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200,55).text(R.string.btnAgregar);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag=false;
				flag=grabaralgunos();
				if(!flag){
					return;
				}
				agregarHermano();
				Util.cleanAndLockView(getActivity(), btnAgregar);
			}
		});
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMortalidad = new TableComponent(getActivity(), this,App.ESTILO).size(350,750).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcMortalidad = new TableComponent(getActivity(), this,App.ESTILO).size(450,750).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
//		tcMortalidad.addHeader(R.string.c2seccion_08qi900nroorden,0.3f);
		tcMortalidad.addHeader(R.string.c2seccion_08qi904nombre,1f);
		tcMortalidad.addHeader(R.string.c2seccion_08qi90edad,1f);
		tcMortalidad.addHeader(R.string.mortalidad_sexo,1f);
		tcMortalidad.addHeader(R.string.mortalidad_fecha,1f);
	}

	public boolean grabaralgunos(){
		uiToEntity(hogar); 
		if (!validaralgunos()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		return true;
	}
	
	@Override
	public boolean grabar() {
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			if(fallecidas.size()>0){
				ReordenarDatos(hogar);
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		
		return true;
	}
	public void ReordenarDatos(Hogar h){
		boolean flag=false;
		fallecidas= getCuestionarioService().getMortalidad(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
		flag=getCuestionarioService().DeleteMortalidadHogarTodo(h.id,h.hogar_id,h.persona_informante_id);
		Integer contador=1;
		for(MORTALIDAD persona: fallecidas){
			try {
				persona.nro_orden_id=contador;
				getCuestionarioService().saveOrUpdate(persona, seccionesGrabadomortalidad);
				contador++;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
	}
	
	public void onh27ChangeValue(){
		Integer dato=Integer.parseInt(rgQH27.getTagSelected("0").toString());
		if(!Util.esDiferente(dato, 2)){
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), txtQH28,txtQHM_OBS,tcMortalidad);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			if(fallecidas.size()>0){
				action = ACTION.ELIMINARTODO;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Se Eliminar� registro de personas fallecidos?");
				dialog.put("seleccion", (MORTALIDAD) fallecidas.get(0));
				dialog.showDialog();
			}
		}
		else{
			Util.lockView(getActivity(), false,txtQH28,txtQHM_OBS,tcMortalidad);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
		}
	}
	

	public void onqh28Changevalue(){
		DesabilitarelBotonAgregar();
		tcMortalidad.requestFocus();
	}
	private boolean validaralgunos(){
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh27)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH27"); 
			view = rgQH27; 
			error = true; 
			return false; 
		}
		if (!Util.esDiferente(hogar.qh27,1)  && Util.esVacio(hogar.qh28)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH28"); 
			view = txtQH28; 
			error = true; 
			return false; 
		}
		return true;
	}
	
	  private boolean validar() {
			if(!isInRange()) return false; 
			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
			if (Util.esVacio(hogar.qh27)) {
					mensaje = preguntaVacia.replace("$", "La pregunta QH27"); 
					view = rgQH27; 
					error = true; 
					return false; 
				} 
			if(!Util.esDiferente(hogar.qh27, 1)){
				if (Util.esVacio(hogar.qh28)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QH28"); 
					view = txtQH28; 
					error = true; 
					return false; 
				}
				if (Util.esMenor(hogar.qh28,1)) { 
					mensaje = "No puede ser menor que 1."; 
					view = txtQH28; 
					error = true; 
					return false; 
				}
				if (Util.esDiferente(hogar.qh28, fallecidas.size())){
					mensaje = "El total del listado debe conincidir con QH28"; 
					view = btnAgregar; 
					error = true; 
					return false;
				}
				if (Util.esMayor(hogar.qh28,50)){
					mensaje = "Verifique total de personas"; 
					view = txtQH28; 
					error = true; 
					return false;
				}
				if(!getCuestionarioService().TodoLosMiembrosMortalidadhogarCompletados(hogar.id, hogar.hogar_id)){
					mensaje = "Debe completar las preguntas para todos del listado";
					view = tcMortalidad;
					error = true;
					return false;
				}
			}
			
			return true; 
	  }
	  private void validarMensaje(String msj) {
	        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	    }
	 
	  
	@Override
	public void cargarDatos() {
		informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
		hogar = getCuestionarioService().getHogarByInformante(
				App.getInstance().getHogar().id,
				App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
		if (hogar == null) {
			hogar = new Hogar();
			hogar.id = App.getInstance().getHogar().id;
			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
			hogar.persona_informante_id=informantehogar.persona_id;
		}
		entityToUI(hogar);
		recargardatos();
		inicio();
	}
	public void agregarHermano() {
		MORTALIDAD bean = new MORTALIDAD();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.persona_id =informantehogar.persona_id;
		bean.nro_orden_id = fallecidas.size() + 1;
		abrirDetalle(bean);
	}
	public void recargardatos(){
		fallecidas= getCuestionarioService().getMortalidad(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
		tcMortalidad.setData(fallecidas, "qh29m","getEdad","getSexo","getFecha");
		
		for (int row = 0; row < fallecidas.size(); row++) {
    		if (obtenerEstado(fallecidas.get(row)) == 1) {
				// borde de color azul
    			tcMortalidad.setBorderRow(row, true);
			} else if (obtenerEstado(fallecidas.get(row)) == 2) {
				// borde de color rojo
				tcMortalidad.setBorderRow(row, true, R.color.red);
			} else {
				tcMortalidad.setBorderRow(row, false);
			}
		}
		
		tcMortalidad.reloadData();
		registerForContextMenu(tcMortalidad.getListView());
	}
 private int obtenerEstado(MORTALIDAD detalle) {
			if (!Util.esDiferente(detalle.estado, 0)) {
				Log.e("","UNO");
				return 1 ;
			} else if (!Util.esDiferente(detalle.estado,1)) {
				Log.e("","DOS");
				return 2;
			}
			Log.e("","NINGUNO");
			return 0;
		}
	    
	public void DesabilitarelBotonAgregar(){
		Integer cantidad=fallecidas.size();
		Integer caja=0;
		if(txtQH28.getValue()!=null){
			caja=Integer.parseInt(txtQH28.getText().toString());
			if(cantidad>=caja){
				btnAgregar.setEnabled(false);
			}
			else{
				btnAgregar.setEnabled(true);
			}
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    private class HermanosFallecidosClickListener implements OnItemClickListener {
		public HermanosFallecidosClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			MORTALIDAD c = (MORTALIDAD) fallecidas.get(arg2);
			abrirDetalleEditar(c, arg2, (List<MORTALIDAD>) fallecidas);
		}
	}
    public void refrescarPersonas(MORTALIDAD datos) {
		if (fallecidas.contains(datos)) {
			recargardatos();
			return;
		} else {
			fallecidas.add(datos);
			recargardatos();
		}
	}
   public void abrirDetalleEditar(MORTALIDAD  tmp, int index, List<MORTALIDAD> detalles){
    	FragmentManager fm = HogarFragment_000.this.getFragmentManager();
    	CHMORTALIDAD_Fragment_000_2Dialog aperturaDialog = CHMORTALIDAD_Fragment_000_2Dialog.newInstance(HogarFragment_000.this, tmp, index, detalles);
    	aperturaDialog.setAncho(MATCH_PARENT);
    	aperturaDialog.show(fm, "aperturaDialog");
    }
    
    public void abrirDetalle(MORTALIDAD tmp) {
    	FragmentManager fm = HogarFragment_000.this.getFragmentManager();
    	CHMORTALIDAD_Fragment_000_1Dialog aperturaDialog = CHMORTALIDAD_Fragment_000_1Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
    }
    
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcMortalidad.getListView())) {
			menu.setHeaderTitle("Opciones del Residente");
			menu.add(1, 0, 1, "Editar");
			menu.add(1, 1, 1, "Eliminar");
			
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			Integer posicion = 0;
			posicion = fallecidas.get(info.position).persona_id;
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			MORTALIDAD seleccion = (MORTALIDAD) info.targetView.getTag();
			Integer codigo=App.getInstance().getUsuario().cargo_id;
	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
	    		menu.getItem(1).setVisible(false);
	    	}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		MORTALIDAD seleccion = (MORTALIDAD) info.targetView.getTag();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				abrirDetalleEditar((MORTALIDAD) fallecidas.get(info.position), info.position,  (List<MORTALIDAD>) fallecidas);  //CISECCION_09 tmp, int index, List<CISECCION_09> detalles
				break;
			case 1:
				action = ACTION.ELIMINAR;
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
				dialog.put("seleccion", (MORTALIDAD) fallecidas.get(info.position));
				dialog.showDialog();
				break;
				}
			}
		return super.onContextItemSelected(item);
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		if(action==ACTION.ELIMINARTODO){
			rgQH27.setTagSelected(1);
		}
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		if(action==ACTION.ELIMINAR){
			MORTALIDAD hermanoaeliminar = (MORTALIDAD) dialog.get("seleccion");
			try {
				fallecidas.remove(hermanoaeliminar);
				boolean borrado = false;
				borrado = getCuestionarioService().DeleteMortalidadHogar(hermanoaeliminar);
				DesabilitarelBotonAgregar();
				if (!borrado) {
					throw new SQLException("Persona no pudo ser borrada.");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
			}
		}
		if(action==ACTION.ELIMINARTODO){
			MORTALIDAD bean = (MORTALIDAD)dialog.get("seleccion");
			try {
				boolean borrado = false;
				fallecidas.clear();
				borrado=getCuestionarioService().DeleteMortalidadHogarTodo(bean.id,bean.hogar_id,bean.persona_id);
				if (!borrado) {
					throw new SQLException("Error al eliminar.");
				}
			} catch (SQLException ee) {
				ee.printStackTrace();
				ToastMessage.msgBox(getActivity(), "ERROR: " + ee.getMessage(),
						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
			}
		}
		action=null;
		recargardatos();

		
	}
    public Seccion01Service getPersonaService(){
    	if(Personaservice == null){
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }
  	
	public void inicio(){
		onh27ChangeValue();
		onqh28Changevalue();
		ValidarsiesSupervisora();
		RenombrarEtiquetas();
	}
	public void RenombrarEtiquetas(){
		lblpreguntaqh27.setText(getResources().getString(R.string.mortalidad_qh27));
		lblpreguntaqh27.setText(lblpreguntaqh27.getText().toString().replace("#", App.ANIOPORDEFECTO+""));
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH27.readOnly();
			txtQH28.readOnly();
			txtQHM_OBS.setEnabled(false);
			Util.cleanAndLockView(getActivity(), btnAgregar);
			btnAgregar.requestFocus();
		}
	}
	
	@Override
	public Integer grabadoParcial() {
		uiToEntity(hogar); 
		try { 
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}	
		return App.HOGAR;
	}
}
