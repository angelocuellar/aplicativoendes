package gob.inei.endes2024.fragment.hogar;
import java.sql.SQLException;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

// 
public class HogarFragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQH61A;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQH61B;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQH61C;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQH61D;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQH61E;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQH61F;
	@FieldAnnotation(orderIndex = 7)
	public RadioGroupOtherField rgQH61G;
	@FieldAnnotation(orderIndex = 8)
	public RadioGroupOtherField rgQH61H;
	@FieldAnnotation(orderIndex = 9)
	public RadioGroupOtherField rgQH61I;
	@FieldAnnotation(orderIndex = 10)
	public RadioGroupOtherField rgQH61J;
	@FieldAnnotation(orderIndex = 11)
	public RadioGroupOtherField rgQH61K;
	@FieldAnnotation(orderIndex = 12)
	public RadioGroupOtherField rgQH61L;
	@FieldAnnotation(orderIndex = 13)
	public RadioGroupOtherField rgQH61M;
	@FieldAnnotation(orderIndex = 14)
	public RadioGroupOtherField rgQH61N;
	@FieldAnnotation(orderIndex = 15)
	public RadioGroupOtherField rgQH61O;
	@FieldAnnotation(orderIndex = 16)
	public RadioGroupOtherField rgQH61P;
	
	@FieldAnnotation(orderIndex = 17)
	public RadioGroupOtherField rgQH61Q;
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQH61Q_A; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQH61Q_B;
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQH61Q_C;
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQH61Q_D;
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQH61Q_X;
	@FieldAnnotation(orderIndex=23)
	public TextField txtQH61Q_XO;
	
	@FieldAnnotation(orderIndex = 24)
	public RadioGroupOtherField rgQH61R;
	@FieldAnnotation(orderIndex=25) 
	public RadioGroupOtherField rgQH61R_A; 
	@FieldAnnotation(orderIndex=26) 
	public RadioGroupOtherField rgQH61R_B;
	@FieldAnnotation(orderIndex=27) 
	public RadioGroupOtherField rgQH61R_C;
	@FieldAnnotation(orderIndex=28) 
	public RadioGroupOtherField rgQH61R_D;
	@FieldAnnotation(orderIndex=29) 
	public RadioGroupOtherField rgQH61R_X;
	@FieldAnnotation(orderIndex=30)
	public TextField txtQH61R_XO;
	
	@FieldAnnotation(orderIndex = 31)
	public RadioGroupOtherField rgQH61S;
	@FieldAnnotation(orderIndex = 32)
	public RadioGroupOtherField rgQH61T;
	
	LinearLayout q1;
	
	Hogar hogar;
	public TextField txtCabecera;
	public LabelComponent lblpreguntatitulo, lblsofa, lblvitrina, lblrepostero,
			lblcomoda, lblreloj, lblradio, lbltelevision, lbllicuadora,
			lblcocinaagras, lblcocinakerosene, lblmicroondas, lblrefrigerador,
			lbllavadora, lblcomputadora, lblbombadeagua,
			lblgeneradorelectricidad, lbltelefonofijo, lblcelular, lblinternet,
			lbltelevisionporcable,lblmuebles,lblequipos,lblservicios,lblpregunta61,
			lbltelefonofijo_p,lbltelefonofijo_pa,lbltelefonofijo_pb,lbltelefonofijo_pc,lbltelefonofijo_pd,lbltelefonofijo_px,
			lblcelular_p,lblcelular_pa,lblcelular_pb,lblcelular_pc,lblcelular_pd,lblcelular_px
			;

	private CuestionarioService cuestionarioService;

	public GridComponent2 gridPreguntas;

	private LabelComponent lblTitulo;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	
	//
	public HogarFragment_004() {
	}

	public HogarFragment_004 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,
				-1, "QH61A", "QH61B", "QH61C", "QH61D", "QH61E", "QH61F",
				"QH61G", "QH61H", "QH61I", "QH61J", "QH61K", "QH61L", "QH61M",
				"QH61N", "QH61O", "QH61P", "QH61Q", "QH61R", "QH61S", "QH61T",
				"QH61Q_A", "QH61Q_B", "QH61Q_C", "QH61Q_D", "QH61Q_X", "QH61Q_XO", 
				"QH61R_A", "QH61R_B", "QH61R_C", "QH61R_D", "QH61R_X", "QH61R_XO", 
				"ID", "HOGAR_ID","PERSONA_INFORMANTE_ID") };
		// seccionesCargado = new SeccionCapitulo[]{new
		// SeccionCapitulo(0,-1,-1,"QH61A","QH61B","QH61C","QH61D","QH61E","QH61F","QH61G","QH61H","QH61I","QH61J","QH61K","QH61L","QH61M","QH61N","QH61O","QH61P","QH61Q","QH61R","QH61S","QH61T","ID","HOGAR_ID","PERSONA_ID")};
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,
				-1, "QH61A", "QH61B", "QH61C", "QH61D", "QH61E", "QH61F",
				"QH61G", "QH61H", "QH61I", "QH61J", "QH61K", "QH61L", "QH61M",
				"QH61N", "QH61O", "QH61P", "QH61Q", "QH61R", "QH61S", "QH61T",
				"QH61Q_A", "QH61Q_B", "QH61Q_C", "QH61Q_D", "QH61Q_X", "QH61Q_XO", 
				"QH61R_A", "QH61R_B", "QH61R_C", "QH61R_D", "QH61R_X", "QH61R_XO"
				) };
		return rootView;
	}

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO)
				.size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar)
				.textSize(21).centrar().negrita();
				
		lblpreguntatitulo = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh61).textSize(21).centrar().negrita().alinearIzquierda().colorFondo(R.color.griscabece);
		lblmuebles = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61_a).textSize(20).alinearIzquierda().negrita();
		lblsofa = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61a).textSize(18).alinearIzquierda();
		rgQH61A = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblvitrina = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61b).textSize(18).alinearIzquierda();
		rgQH61B = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblrepostero = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61c).textSize(18).alinearIzquierda();
		rgQH61C = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblcomoda = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61d).textSize(18).alinearIzquierda();
		rgQH61D = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblreloj = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61e).textSize(18).alinearIzquierda();
		rgQH61E = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblequipos = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61_b).textSize(20).alinearIzquierda().negrita();
		lblradio = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61f).textSize(18).alinearIzquierda();
		rgQH61F = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbltelevision = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61g).textSize(18).alinearIzquierda();
		rgQH61G = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbllicuadora = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61h).textSize(18).alinearIzquierda();
		rgQH61H = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblcocinaagras = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61i).textSize(18).alinearIzquierda();
		rgQH61I = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblcocinakerosene = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61j).textSize(18).alinearIzquierda();
		rgQH61J = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,	R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblmicroondas = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61k).textSize(18).alinearIzquierda();
		rgQH61K = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblrefrigerador = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61l).textSize(18).alinearIzquierda();
		rgQH61L = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbllavadora = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61m).textSize(18).alinearIzquierda();
		rgQH61M = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblcomputadora = new LabelComponent(this.getActivity())	.size(altoComponente, 550).text(R.string.hogarqh61n).textSize(18).alinearIzquierda();
		rgQH61N = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblbombadeagua = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61o).textSize(18).alinearIzquierda();
		rgQH61O = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblgeneradorelectricidad = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61p).textSize(18).alinearIzquierda();
		rgQH61P = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblservicios = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61_c).textSize(20).alinearIzquierda().negrita();
		
		lbltelefonofijo = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61q).textSize(18).alinearIzquierda();
		rgQH61Q = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH61Q_ChangeValue");
		lbltelefonofijo_p = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61q_p).textSize(18).alinearIzquierda();
		lbltelefonofijo_pa = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_a).textSize(16).alinearIzquierda();
		lbltelefonofijo_pb = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_b).textSize(16).alinearIzquierda();
		lbltelefonofijo_pc = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_c).textSize(16).alinearIzquierda();
		lbltelefonofijo_pd = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_d).textSize(16).alinearIzquierda();
		lbltelefonofijo_px = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_x).textSize(16).alinearIzquierda();
		rgQH61Q_A=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61Q_B=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61Q_C=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61Q_D=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61Q_X=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH61Q_X_ChangeValue");
		txtQH61Q_XO=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		//rgQH61Q_X.agregarEspecifique(0,txtQH61Q_XO);
		
		lblcelular = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61r).textSize(18).alinearIzquierda();
		rgQH61R = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH61R_ChangeValue");
		lblcelular_p = new LabelComponent(this.getActivity()).size(altoComponente+10, 550).text(R.string.hogarqh61r_p).textSize(18).alinearIzquierda();
		lblcelular_pa = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_a).textSize(16).alinearIzquierda();
		lblcelular_pb = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_b).textSize(16).alinearIzquierda();
		lblcelular_pc = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_c).textSize(16).alinearIzquierda();
		lblcelular_pd = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_d).textSize(16).alinearIzquierda();
		lblcelular_px = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61x_p_x).textSize(16).alinearIzquierda();		
		rgQH61R_A=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61R_B=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61R_C=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61R_D=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH61R_X=new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onrgQH61R_X_ChangeValue");;
		txtQH61R_XO=new TextField(this.getActivity()).maxLength(1000).size(altoComponente, 450);
		//rgQH61R_X.agregarEspecifique(0,txtQH61R_XO);
		
		lblinternet = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61s).textSize(18).alinearIzquierda();
		rgQH61S = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbltelevisionporcable = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh61t).textSize(18).alinearIzquierda();
		rgQH61T = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(60, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		gridPreguntas = new GridComponent2(this.getActivity(),App.ESTILO, 2);
		gridPreguntas.addComponent(lblmuebles,2);
		gridPreguntas.addComponent(lblsofa);
        gridPreguntas.addComponent(rgQH61A);
		gridPreguntas.addComponent(lblvitrina);
		gridPreguntas.addComponent(rgQH61B);
		gridPreguntas.addComponent(lblrepostero);
		gridPreguntas.addComponent(rgQH61C);
		gridPreguntas.addComponent(lblcomoda);
		gridPreguntas.addComponent(rgQH61D);
		gridPreguntas.addComponent(lblreloj);
		gridPreguntas.addComponent(rgQH61E);
		gridPreguntas.addComponent(lblequipos,2);
		gridPreguntas.addComponent(lblradio);
		gridPreguntas.addComponent(rgQH61F);
		gridPreguntas.addComponent(lbltelevision);
		gridPreguntas.addComponent(rgQH61G);
		gridPreguntas.addComponent(lbllicuadora);
		gridPreguntas.addComponent(rgQH61H);
		gridPreguntas.addComponent(lblcocinaagras);
		gridPreguntas.addComponent(rgQH61I);
		gridPreguntas.addComponent(lblcocinakerosene);
		gridPreguntas.addComponent(rgQH61J);
		gridPreguntas.addComponent(lblmicroondas);
		gridPreguntas.addComponent(rgQH61K);
		gridPreguntas.addComponent(lblrefrigerador);
		gridPreguntas.addComponent(rgQH61L);
		gridPreguntas.addComponent(lbllavadora);
		gridPreguntas.addComponent(rgQH61M);
		gridPreguntas.addComponent(lblcomputadora);
		gridPreguntas.addComponent(rgQH61N);
		gridPreguntas.addComponent(lblbombadeagua);
		gridPreguntas.addComponent(rgQH61O);
		gridPreguntas.addComponent(lblgeneradorelectricidad);
		gridPreguntas.addComponent(rgQH61P);
		gridPreguntas.addComponent(lblservicios,2);
		
		gridPreguntas.addComponent(lbltelefonofijo);		
		gridPreguntas.addComponent(rgQH61Q);
		gridPreguntas.addComponent(lbltelefonofijo_p,2);
		gridPreguntas.addComponent(lbltelefonofijo_pa);		
		gridPreguntas.addComponent(rgQH61Q_A);
		gridPreguntas.addComponent(lbltelefonofijo_pb);		
		gridPreguntas.addComponent(rgQH61Q_B);
		gridPreguntas.addComponent(lbltelefonofijo_pc);		
		gridPreguntas.addComponent(rgQH61Q_C);
		gridPreguntas.addComponent(lbltelefonofijo_pd);		
		gridPreguntas.addComponent(rgQH61Q_D);
		gridPreguntas.addComponent(lbltelefonofijo_px);		
		gridPreguntas.addComponent(rgQH61Q_X);
		gridPreguntas.addComponent(txtQH61Q_XO,2);
		
		
		gridPreguntas.addComponent(lblcelular);		
		gridPreguntas.addComponent(rgQH61R);
		gridPreguntas.addComponent(lblcelular_p,2);
		gridPreguntas.addComponent(lblcelular_pa);		
		gridPreguntas.addComponent(rgQH61R_A);
		gridPreguntas.addComponent(lblcelular_pb);		
		gridPreguntas.addComponent(rgQH61R_B);
		gridPreguntas.addComponent(lblcelular_pc);		
		gridPreguntas.addComponent(rgQH61R_C);
		gridPreguntas.addComponent(lblcelular_pd);		
		gridPreguntas.addComponent(rgQH61R_D);
		gridPreguntas.addComponent(lblcelular_px);		
		gridPreguntas.addComponent(rgQH61R_X);
		gridPreguntas.addComponent(txtQH61R_XO,2);
		
		gridPreguntas.addComponent(lblinternet);
		gridPreguntas.addComponent(rgQH61S);
		gridPreguntas.addComponent(lbltelevisionporcable);
		gridPreguntas.addComponent(rgQH61T);
		
		lblpregunta61 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh61).textSize(19);
	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta61,gridPreguntas.component());	
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(hogar);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		try {
			if (!getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado)) {
				ToastMessage.msgBox(this.getActivity(),
						"Los datos no pudieron ser guardados.",
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(
				R.string.pregunta_no_vacia);
		if (Util.esVacio(hogar.qh61a)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61A A.Muebles");
			view = rgQH61A;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61b)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61B A.Muebles");
			view = rgQH61B;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61c)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61C A.Muebles");
			view = rgQH61C;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61d)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61D A.Muebles");
			view = rgQH61D;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61e)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61E A.Muebles");
			view = rgQH61E;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61f)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61A B.Equipos");
			view = rgQH61F;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61g)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61B B.Equipos");
			view = rgQH61G;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61h)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61C B.Equipos");
			view = rgQH61H;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61i)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61D B.Equipos");
			view = rgQH61I;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61j)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61E B.Equipos");
			view = rgQH61J;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61k)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61F B.Equipos");
			view = rgQH61K;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61l)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61G B.Equipos");
			view = rgQH61L;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61m)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61H B.Equipos");
			view = rgQH61M;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61n)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61I B.Equipos");
			view = rgQH61N;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61o)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61J B.Equipos");
			view = rgQH61O;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61p)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61K B.Equipos");
			view = rgQH61P;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61q)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61A C.Servicios");
			view = rgQH61Q;
			error = true;
			return false;
		}
		if (!Util.esDiferente(hogar.qh61q,1)) {
			if (Util.esVacio(hogar.qh61q_a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_A");
				view = rgQH61Q_A;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61q_b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_B");
				view = rgQH61Q_B;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61q_c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_C");
				view = rgQH61Q_C;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61q_d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_D");
				view = rgQH61Q_D;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61q_x)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_X");
				view = rgQH61Q_X;
				error = true;
				return false;
			}
			if (!Util.esDiferente(hogar.qh61q_x,1)) {
				if (Util.esVacio(hogar.qh61q_xo)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.61Q_XO");
					view = txtQH61Q_XO;
					error = true;
					return false;
				}
			}
			
		}
		if (Util.esVacio(hogar.qh61r)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61B C.Servicios");
			view = rgQH61R;
			error = true;
			return false;
		}
		if (!Util.esDiferente(hogar.qh61r,1)) {
			if (Util.esVacio(hogar.qh61r_a)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61R_A");
				view = rgQH61R_A;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61r_b)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61R_B");
				view = rgQH61R_B;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61r_c)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61R_C");
				view = rgQH61R_C;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61r_d)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61R_D");
				view = rgQH61R_D;
				error = true;
				return false;
			}
			if (Util.esVacio(hogar.qh61r_x)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.61R_X");
				view = rgQH61R_X;
				error = true;
				return false;
			}
			if (!Util.esDiferente(hogar.qh61r_x,1)) {
				if (Util.esVacio(hogar.qh61r_xo)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.61R_XO");
					view = txtQH61R_XO;
					error = true;
					return false;
				}
			}
			
		}
		if (Util.esVacio(hogar.qh61s)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61C C.Servicios");
			view = rgQH61S;
			error = true;
			return false;
		}
		if (Util.esVacio(hogar.qh61t)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.61D C.Servicios");
			view = rgQH61T;
			error = true;
			return false;
		}
		return true;
	}

	@Override
	public void cargarDatos() {
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		if (hogar == null) {
			hogar = new Hogar();
			hogar.id = App.getInstance().getHogar().id;
			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
		}
		entityToUI(hogar);
		inicio();
	}

	private void inicio() {
		onrgQH61Q_ChangeValue();
		onrgQH61R_ChangeValue();
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}
	
	 public void onrgQH61Q_ChangeValue() {   
	  	if (MyUtil.incluyeRango(1,1,rgQH61Q.getTagSelected("").toString())) {
	  		Util.lockView(getActivity(), false,rgQH61Q_A,rgQH61Q_B,rgQH61Q_C,rgQH61Q_D,rgQH61Q_X);
	  		lbltelefonofijo_p.setVisibility(View.VISIBLE);
	  		lbltelefonofijo_pa.setVisibility(View.VISIBLE);
	  		lbltelefonofijo_pb.setVisibility(View.VISIBLE);
	  		lbltelefonofijo_pc.setVisibility(View.VISIBLE);
	  		lbltelefonofijo_pd.setVisibility(View.VISIBLE);
	  		lbltelefonofijo_px.setVisibility(View.VISIBLE);
	  		rgQH61Q_A.setVisibility(View.VISIBLE);
	  		rgQH61Q_B.setVisibility(View.VISIBLE);
	  		rgQH61Q_C.setVisibility(View.VISIBLE);
	  		rgQH61Q_D.setVisibility(View.VISIBLE);
	  		rgQH61Q_X.setVisibility(View.VISIBLE);
	  		txtQH61Q_XO.setVisibility(View.VISIBLE);
	  		onrgQH61Q_X_ChangeValue();
			rgQH61R.requestFocus();
		}
		else{
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQH61Q_A,rgQH61Q_B,rgQH61Q_C,rgQH61Q_D,rgQH61Q_X, txtQH61Q_XO);
			lbltelefonofijo_p.setVisibility(View.GONE);
	  		lbltelefonofijo_pa.setVisibility(View.GONE);
	  		lbltelefonofijo_pb.setVisibility(View.GONE);
	  		lbltelefonofijo_pc.setVisibility(View.GONE);
	  		lbltelefonofijo_pd.setVisibility(View.GONE);
	  		lbltelefonofijo_px.setVisibility(View.GONE);
	  		rgQH61Q_A.setVisibility(View.GONE);
	  		rgQH61Q_B.setVisibility(View.GONE);
	  		rgQH61Q_C.setVisibility(View.GONE);
	  		rgQH61Q_D.setVisibility(View.GONE);
	  		rgQH61Q_X.setVisibility(View.GONE);
	  		txtQH61Q_XO.setVisibility(View.GONE);
			rgQH61R.requestFocus();
		}
	 }
	 public void onrgQH61Q_X_ChangeValue(){
		 if (MyUtil.incluyeRango(1,1,rgQH61Q_X.getTagSelected("").toString())) {
		  	Util.lockView(getActivity(), false,txtQH61Q_XO);
		  	txtQH61Q_XO.setVisibility(View.VISIBLE);
		  	txtQH61Q_XO.requestFocus();
		 }
		 else{
			 Util.cleanAndLockView(getActivity(),txtQH61Q_XO);
			 txtQH61Q_XO.setVisibility(View.GONE);
			 rgQH61R.requestFocus();
		 }
	 }
	 
	 public void onrgQH61R_ChangeValue() {   
		  	if (MyUtil.incluyeRango(1,1,rgQH61R.getTagSelected("").toString())) {
		  		Util.lockView(getActivity(), false,rgQH61R_A,rgQH61R_B,rgQH61R_C,rgQH61R_D,rgQH61R_X);
		  		lblcelular_p.setVisibility(View.VISIBLE);
				lblcelular_pa.setVisibility(View.VISIBLE);
		  		lblcelular_pb.setVisibility(View.VISIBLE);
		  		lblcelular_pc.setVisibility(View.VISIBLE);
		  		lblcelular_pd.setVisibility(View.VISIBLE);
		  		lblcelular_px.setVisibility(View.VISIBLE);
		  		rgQH61R_A.setVisibility(View.VISIBLE);
		  		rgQH61R_B.setVisibility(View.VISIBLE);
		  		rgQH61R_C.setVisibility(View.VISIBLE);
		  		rgQH61R_D.setVisibility(View.VISIBLE);
		  		rgQH61R_X.setVisibility(View.VISIBLE);
		  		txtQH61R_XO.setVisibility(View.VISIBLE);
		  		onrgQH61R_X_ChangeValue();
				rgQH61S.requestFocus();
			}
			else{
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),rgQH61R_A,rgQH61R_B,rgQH61R_C,rgQH61R_D,rgQH61R_X, txtQH61R_XO);
				lblcelular_p.setVisibility(View.GONE);
				lblcelular_pa.setVisibility(View.GONE);
		  		lblcelular_pb.setVisibility(View.GONE);
		  		lblcelular_pc.setVisibility(View.GONE);
		  		lblcelular_pd.setVisibility(View.GONE);
		  		lblcelular_px.setVisibility(View.GONE);
		  		rgQH61R_A.setVisibility(View.GONE);
		  		rgQH61R_B.setVisibility(View.GONE);
		  		rgQH61R_C.setVisibility(View.GONE);
		  		rgQH61R_D.setVisibility(View.GONE);
		  		rgQH61R_X.setVisibility(View.GONE);
		  		txtQH61R_XO.setVisibility(View.GONE);
				rgQH61S.requestFocus();
			}
		 }
	 
		
	 public void onrgQH61R_X_ChangeValue(){
		 if (MyUtil.incluyeRango(1,1,rgQH61R_X.getTagSelected("").toString())) {
		  	Util.lockView(getActivity(), false,txtQH61R_XO);
		  	txtQH61R_XO.setVisibility(View.VISIBLE);
		  	txtQH61R_XO.requestFocus();
		 }
		 else{
			 Util.cleanAndLockView(getActivity(),txtQH61R_XO);
			 txtQH61R_XO.setVisibility(View.GONE);
			 rgQH61S.requestFocus();
		 }
	 }
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH61A.readOnly();
			rgQH61B.readOnly();
			rgQH61C.readOnly();
			rgQH61D.readOnly();
			rgQH61E.readOnly();		
			rgQH61F.readOnly();
			rgQH61G.readOnly();
			rgQH61H.readOnly();
			rgQH61I.readOnly();
			rgQH61J.readOnly();
			rgQH61K.readOnly();
			rgQH61L.readOnly();
			rgQH61M.readOnly();
			rgQH61N.readOnly();
			rgQH61O.readOnly();
			rgQH61P.readOnly();
			rgQH61Q.readOnly();
			rgQH61Q_A.readOnly();
			rgQH61Q_B.readOnly();
			rgQH61Q_C.readOnly();
			rgQH61Q_D.readOnly();
			rgQH61Q_X.readOnly();
			txtQH61Q_XO.readOnly();
			rgQH61R.readOnly();
			rgQH61R_A.readOnly();
			rgQH61R_B.readOnly();
			rgQH61R_C.readOnly();
			rgQH61R_D.readOnly();
			rgQH61R_X.readOnly();
			txtQH61R_XO.readOnly();
			rgQH61S.readOnly();
			rgQH61T.readOnly();			
		}
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (!getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado)) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
}
