package gob.inei.endes2024.fragment.hogar;
//package gob.inei.endes2024.fragment.hogar;
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogComponent;
//import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.MasterActivity;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.TableComponent;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.interfaces.Respondible;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.dialog.ChImigracion_1Dialog;
//import gob.inei.endes2024.fragment.hogar.dialog.ChImigracion_2Dialog;
//import gob.inei.endes2024.fragment.hogar.dialog.ChMigracion_1Dialog;
//import gob.inei.endes2024.fragment.hogar.dialog.ChMigracion_2Dialog;
//import gob.inei.endes2024.model.Hogar;
//import gob.inei.endes2024.model.IMIGRACION;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//
//import java.sql.SQLException;
//import java.util.List;
//
//import android.os.Bundle;
//import android.support.v4.app.FragmentManager;
//import android.text.Html;
//import android.text.Spanned;
//import android.util.Log;
//import android.view.ContextMenu;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ContextMenu.ContextMenuInfo;
//import android.widget.AdapterView;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//import android.widget.AdapterView.OnItemClickListener;
//
//public class HogarFragment_0002 extends FragmentForm implements Respondible {
//	
//	@FieldAnnotation(orderIndex=1) 
//	public RadioGroupOtherField rgQH35;
//	@FieldAnnotation(orderIndex=2) 
//	public IntegerField txtQH35_N;
//	
//	@FieldAnnotation(orderIndex=3)
//	public ButtonComponent btnAgregar;
//	@FieldAnnotation(orderIndex=4)
//	public TextAreaField txtQH35_OBS;
//
//	Hogar hogar;	 
//	public TableComponent tcImigracion;
//	public List<IMIGRACION> imigracion;
//	public Integer numHerMayores;
//	
//	
//	SeccionCapitulo[] seccionesGrabadoHogar; 
//	SeccionCapitulo[] seccionesCargado,seccionesCargadoHogar, seccionesCargadoseccion01,seccionesCargadoVisita,seccionesGrabado; 
//	
//	private CuestionarioService cuestionarioService;
//	
//	personasMigracionClickListener adapter;
//	public LabelComponent lbltitulo,lblpreguntaqh35b, lblpreguntaqh35,lblpreguntaqh28,lblobs_35,lblpreguntaqh35_n;
//	private DialogComponent dialog;
//	public TextField txtCabecera;
//	public GridComponent2 gridpregunta901;
//	
//	public LinearLayout q0;
//	public LinearLayout q1;
//	public LinearLayout q2;
//	public LinearLayout q3;
//	
//	
//	
//	private enum ACTION {
//		ELIMINAR,ELIMINARTODO
//	}
//
//	private ACTION action;
//	
//	public HogarFragment_0002 parent(MasterActivity parent) {
//		this.parent = parent;
//		return this;
//	}
//	
//	@Override 
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
//		rootView = createUI(); 
//		initObjectsWithoutXML(this, rootView); 
//		adapter = new personasMigracionClickListener();
//		tcImigracion.getListView().setOnItemClickListener(adapter);
//		enlazarCajas(); 
//		listening();
//		
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "QH35C_A", "QH35C_M", "QH35D_CCDD", "QH35D_CCDD_NS", "QH35D_CCPP", "QH35D_CCPP_NS", "QH35D_CCDI", "QH35D_CCDI_NS", "QH35D_PAIS", "QH35D_PAIS_NS", "QH35E", "QH35EO", "QH35E_OBSERVACION", "QH35F", "QH35FO", "QH35F_REF", "QH35_OBSERVACION", "QH35G", "ESTADO","ID","HOGAR_ID")};
//		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo( 0,-1,-1, "QH35A", "QH35B_1", "QH35B_11", "QH35B_2", "QH35B_3", "QH35C_A", "QH35C_M", "QH35D_CCDD", "QH35D_CCDD_NS", "QH35D_CCPP", "QH35D_CCPP_NS", "QH35D_CCDI", "QH35D_CCDI_NS", "QH35D_PAIS", "QH35D_PAIS_NS", "QH35E", "QH35EO", "QH35E_OBSERVACION", "QH35F", "QH35FO", "QH35F_REF", "QH35_OBSERVACION", "QH35G", "ID","HOGAR_ID")};
//		
//		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH35", "QH35_N", "QH35_OBS","ID","HOGAR_ID") };
//		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH35", "QH35_N", "QH35_OBS") };
//		
//		return rootView; 
//	} 
//	  
//	  
//
//
//	@Override
//	protected void buildFields() {
//		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
//		lbltitulo = new LabelComponent(getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.migracion35_t).textSize(20).centrar();
//		lblpreguntaqh35 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion35);
//		lblpreguntaqh35_n = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion35_n);
//		lblpreguntaqh35b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion35b_n);
//		
//		lblobs_35 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//				
//		rgQH35 = new RadioGroupOtherField(getActivity(),R.string.mortalidad_qh27_1,R.string.mortalidad_qh27_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQH34ChangeValue");
//		txtQH35_N = new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(2);
//		txtQH35_OBS = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
//		
//		
//		btnAgregar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200,55).text(R.string.btnAgregar);
//		btnAgregar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag=false;
//				flag=grabaralgunos();
//				if(!flag){
//					return;
//				}
//				agregarPersona();
//				//Util.cleanAndLockView(getActivity(), btnAgregar);
//			}
//		});
//		
//		tcImigracion = new TableComponent(getActivity(), this,App.ESTILO).size(450,750).headerHeight(45).dataColumHeight(40).headerTextSize(15);		
//		tcImigracion.addHeader(R.string.migracion34_orden,0.5f);
//		tcImigracion.addHeader(R.string.migracion34_nombres,3f,TableComponent.ALIGN.LEFT);
//	}
//	
//	@Override
//	protected View createUI() {
//		// TODO Auto-generated method stub
//		buildFields();
//		q0 = createQuestionSection(txtCabecera,lbltitulo);
//		q1 = createQuestionSection(lblpreguntaqh35,rgQH35,lblpreguntaqh35_n,txtQH35_N);
//		q2 = createQuestionSection(0,lblpreguntaqh35b,btnAgregar,tcImigracion.getTableView());
//		q3 = createQuestionSection(lblobs_35,txtQH35_OBS);
//		
//		ScrollView contenedor = createForm(); 
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
//		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		return contenedor;
//	}
//
//	public boolean grabaralgunos(){
//		uiToEntity(hogar); 
//		if (!validaralgunos()) { 
//			if (error) { 
//				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
//				if (view != null) view.requestFocus(); 
//			} 
//			return false; 
//		} 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar)){ 
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return false; 
//			} 
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return false; 
//		}
//		return true;
//	}
//	
//	@Override
//	public boolean grabar() {
//		uiToEntity(hogar); 
//		if (!validar()) { 
//			if (error) { 
//				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
//				if (view != null) view.requestFocus(); 
//			} 
//			return false; 
//		} 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){ 
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return false; 
//			} 
//			if(imigracion.size()>0){
//				ReordenarDatos(hogar);
//			}
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return false; 
//		} 
//		return true;
//	}
//	
//	
//	public void ReordenarDatos(Hogar h){
//		boolean flag=false;
//		imigracion= getCuestionarioService().getImigracion(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
//		flag=getCuestionarioService().DeleteImigracionHogarTodo(h.id,h.hogar_id);
//		Integer contador=1;
//		for(IMIGRACION persona: imigracion){
//			try {
//				persona.qh35a=contador;
//				getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//				contador++;
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}
//	}
//	
//
//	private boolean validaralgunos(){
//		if(!isInRange()) return false; 
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		if (Util.esVacio(hogar.qh35)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta QH35"); 
//			view = rgQH35; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(hogar.qh35, 1)){
//			if (Util.esVacio(hogar.qh35_n)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QH35_N"); 
//				view = txtQH35_N; 
//				error = true; 
//				return false; 
//			}
//		}
//		return true;
//	}
//	
//	private boolean validar() {
//		if(!isInRange()) return false; 
//			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//			if (Util.esVacio(hogar.qh35)) {
//					mensaje = preguntaVacia.replace("$", "La pregunta QH35"); 
//					view = rgQH35; 
//					error = true; 
//					return false; 
//			} 
//			
//			if(!Util.esDiferente(hogar.qh35, 1)){
//				if (Util.esVacio(hogar.qh35_n)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta QH35_N"); 
//					view = txtQH35_N; 
//					error = true; 
//					return false; 
//				}
//				
//				if (Util.esMenor(hogar.qh35_n,1)) { 
//					mensaje = "No puede ser menor que 1."; 
//					view = txtQH35_N; 
//					error = true; 
//					return false; 
//				}
//				if (Util.esDiferente(hogar.qh35_n, imigracion.size())){
//					mensaje = "El total del listado debe conincidir con QH35_N"; 
//					view = btnAgregar; 
//					error = true; 
//					return false;
//				}
//				if(!getCuestionarioService().TodoLosMiembrosImigracionhogarCompletados(hogar.id, hogar.hogar_id) || imigracion.size()==0){
//					mensaje = "Debe completar las preguntas para todos del listado";
//					view = tcImigracion;
//					error = true;
//					return false;
//				}
//				
//			}
//		return true; 
//	}
//	  
//	private void validarMensaje(String msj) {
//	   ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//	}
//	 
//	  
//	@Override
//	public void cargarDatos() {
//		hogar = getCuestionarioService().getHogarByInformante(
//				App.getInstance().getHogar().id,
//				App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
//		if (hogar == null) {
//			hogar = new Hogar();
//			hogar.id = App.getInstance().getHogar().id;
//			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
//		}
//		entityToUI(hogar);
//		recargardatos();
//		inicio();
//	}
//	
//	public void inicio(){
//		onQH34ChangeValue();
//		ValidarsiesSupervisora();
//		//RenombrarEtiquetas();
//		txtCabecera.requestFocus();
//	}
//	
//	public void onQH34ChangeValue(){
//		Integer dato=Integer.parseInt(rgQH35.getTagSelected("0").toString());
//		if(!Util.esDiferente(dato, 2)){
//			MyUtil.LiberarMemoria();
//			Util.cleanAndLockView(getActivity(), txtQH35_N, txtQH35_OBS,tcImigracion);
//			lblpreguntaqh35_n.setVisibility(View.GONE);
//			txtQH35_N.setVisibility(View.GONE);
//			q2.setVisibility(View.GONE);
//			q3.setVisibility(View.GONE);
//			if(imigracion.size()>0){
//				action = ACTION.ELIMINARTODO;
//				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Se Eliminar� registro de personas?");
//				dialog.put("seleccion", (IMIGRACION) imigracion.get(0));
//				dialog.showDialog();
//			}
//		}
//		else{
//			Util.lockView(getActivity(), false, txtQH35_N, txtQH35_OBS,tcImigracion);
//			lblpreguntaqh35_n.setVisibility(View.VISIBLE);
//			txtQH35_N.setVisibility(View.VISIBLE);
//			q2.setVisibility(View.VISIBLE);
//			q3.setVisibility(View.VISIBLE);
//			txtQH35_N.requestFocus();
//		}
//	}
//	
////	public void RenombrarEtiquetas(){
////		lblpreguntaqh35.setText(lblpreguntaqh35.getText().toString().replace("#", App.ANIOPORDEFECTO+1+""));			
////	}
//	
//	public void ValidarsiesSupervisora(){
//		Integer codigo=App.getInstance().getUsuario().cargo_id;
//		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//			rgQH35.readOnly();
//			txtQH35_N.readOnly();
//			txtQH35_OBS.setEnabled(false);
//			Util.cleanAndLockView(getActivity(), btnAgregar);
//			btnAgregar.requestFocus();
//		}
//	}
//		
//	public void agregarPersona() {
//		IMIGRACION bean = new IMIGRACION();
//		bean.id = App.getInstance().getMarco().id;
//		bean.hogar_id = App.getInstance().getHogar().hogar_id;
//		bean.qh35a = imigracion.size() + 1;
//		abrirDetalle(bean);
//	}
//	
//	public void recargardatos(){
//		imigracion= getCuestionarioService().getImigracion(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
//		tcImigracion.setData(imigracion, "QH35A","getNombres");
//		
//		for (int row = 0; row < imigracion.size(); row++) {
//    		if (obtenerEstado(imigracion.get(row)) == 1) {
//				// borde de color azul
//    			tcImigracion.setBorderRow(row, true);
//			} else if (obtenerEstado(imigracion.get(row)) == 2) {
//				// borde de color rojo
//				tcImigracion.setBorderRow(row, true, R.color.red);
//			} else {
//				tcImigracion.setBorderRow(row, false);
//			}
//		}
//		
//		tcImigracion.reloadData();
//		registerForContextMenu(tcImigracion.getListView());
//	}
//	
//	private int obtenerEstado(IMIGRACION detalle) {
//		if (!Util.esDiferente(detalle.estado, 0)) {
//			return 1 ;
//		} else if (!Util.esDiferente(detalle.estado,1)) {
//			return 2;
//		}
//		return 0;
//	}
//
//
//    private class personasMigracionClickListener implements OnItemClickListener {
//		public personasMigracionClickListener() {
//		}
//
//		@Override
//		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//			IMIGRACION c = (IMIGRACION) imigracion.get(arg2);
//			abrirDetalleEditar(c, arg2, (List<IMIGRACION>) imigracion);
//		}
//	}
//    
//    public void refrescarPersonas(IMIGRACION datos) {
//		if (imigracion.contains(datos)) {
//			recargardatos();
//			return;
//		} else {
//			imigracion.add(datos);
//			recargardatos();
//		}
//	}
//    
//   public void abrirDetalleEditar(IMIGRACION  tmp, int index, List<IMIGRACION> detalles){
//    	FragmentManager fm = HogarFragment_0002.this.getFragmentManager();
//    	ChImigracion_2Dialog aperturaDialog = ChImigracion_2Dialog.newInstance(HogarFragment_0002.this, tmp, index, detalles);
//    	aperturaDialog.setAncho(MATCH_PARENT);
//    	aperturaDialog.show(fm, "aperturaDialog");
//    }
//    
//    public void abrirDetalle(IMIGRACION tmp) {
//    	FragmentManager fm = HogarFragment_0002.this.getFragmentManager();
//    	ChImigracion_1Dialog aperturaDialog = ChImigracion_1Dialog.newInstance(this, tmp);
//		aperturaDialog.setAncho(MATCH_PARENT);
//		aperturaDialog.show(fm, "aperturaDialog");
//    }
//    
//    @Override
//	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
//		super.onCreateContextMenu(menu, v, menuInfo);
//		if (v.equals(tcImigracion.getListView())) {
//			menu.setHeaderTitle("Opciones del Residente");
//			menu.add(1, 0, 1, "Editar");
//			menu.add(1, 1, 1, "Eliminar");
//			
//			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//			Integer posicion = 0;
//			posicion = imigracion.get(info.position).qh35a;
//			if (posicion > 1) {
//				posicion = posicion - 1;
//			}
//			IMIGRACION seleccion = (IMIGRACION) info.targetView.getTag();
//			Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		menu.getItem(1).setVisible(false);
//	    	}
//		}
//	}
//
//	@Override
//	public boolean onContextItemSelected(MenuItem item) {
//		if (!getUserVisibleHint())
//			return false;
//		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//		IMIGRACION seleccion = (IMIGRACION) info.targetView.getTag();
//		if (item.getGroupId() == 1) {
//			switch (item.getItemId()) {
//			case 0:
//				abrirDetalleEditar((IMIGRACION) imigracion.get(info.position), info.position,  (List<IMIGRACION>) imigracion);  //CISECCION_09 tmp, int index, List<CISECCION_09> detalles
//				break;
//			case 1:
//				action = ACTION.ELIMINAR;
//				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
//				dialog.put("seleccion", (IMIGRACION) imigracion.get(info.position));
//				dialog.showDialog();
//				break;
//				}
//			}
//		return super.onContextItemSelected(item);
//	}
//
//	@Override
//	public void onCancel() {
//		// TODO Auto-generated method stub
//		if(action==ACTION.ELIMINARTODO){
//			rgQH35.setTagSelected(1);
//		}
//		
//	}
//	
//	public CuestionarioService getCuestionarioService() { 
//		if(cuestionarioService==null){ 
//			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
//		} 
//		return cuestionarioService; 
//    }
//	
//	@Override
//	public void onAccept() {
//		// TODO Auto-generated method stub
//		if(action==ACTION.ELIMINAR){
//			IMIGRACION hermanoaeliminar = (IMIGRACION) dialog.get("seleccion");
//			try {
//				imigracion.remove(hermanoaeliminar);
//				boolean borrado = false;
//				borrado = getCuestionarioService().DeleteImigracionHogar(hermanoaeliminar);
//				//DesabilitarelBotonAgregar();
//				if (!borrado) {
//					throw new SQLException("Persona no pudo ser borrada.");
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
//						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
//			}
//		}
//		if(action==ACTION.ELIMINARTODO){
//			IMIGRACION bean = (IMIGRACION)dialog.get("seleccion");
//			try {
//				boolean borrado = false;
//				imigracion.clear();
//				borrado=getCuestionarioService().DeleteImigracionHogarTodo(bean.id,bean.hogar_id);
//				if (!borrado) {
//					throw new SQLException("Error al eliminar.");
//				}
//			} catch (SQLException ee) {
//				ee.printStackTrace();
//				ToastMessage.msgBox(getActivity(), "ERROR: " + ee.getMessage(),
//						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
//			}
//		}
//		action=null;
//		recargardatos();
//
//		
//	}
//
//	
//	@Override
//	public Integer grabadoParcial() {
//		uiToEntity(hogar); 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return App.NODEFINIDO; 
//			} 
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return App.NODEFINIDO; 
//		}	
//		return App.HOGAR;
//	}
//}
