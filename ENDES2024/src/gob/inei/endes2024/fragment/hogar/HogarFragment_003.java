package gob.inei.endes2024.fragment.hogar;
import java.sql.SQLException;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

// 
public class HogarFragment_003 extends FragmentForm {
	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQH49;
	@FieldAnnotation(orderIndex = 2)
	public TextField txtQH49_O;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQH50;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQH53;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtQH53_O;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQH54;
	
	Hogar hogar;
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lblretpublica,lblletrina,lblotras,lblpregunta49,lblpregunta50,lblpregunta53,lblpregunta54;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;

	
	public HogarFragment_003() {
	}

	public HogarFragment_003 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH49", "QH49_O", "QH50", "QH53", "QH53_O", "QH54", "ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH40","QH41","QH47","QH48") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH49", "QH49_O", "QH50", "QH53", "QH53_O", "QH54") };
		return rootView;
	}

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO)
				.size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar)
				.textSize(21).centrar().negrita();
				
		// rgQH49=new
		// RadioGroupOtherField(this.getActivity(),R.string.hogarqh49_1,R.string.hogarqh49_2,R.string.hogarqh49_3,R.string.hogarqh49_4,R.string.hogarqh49_5,R.string.hogarqh49_6,R.string.hogarqh49_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQH49 = new RadioGroupOtherField(this.getActivity(),
				R.string.hogarqh49_1, R.string.hogarqh49_2,
				R.string.hogarqh49_3, R.string.hogarqh49_4,
				R.string.hogarqh49_5).size(WRAP_CONTENT, WRAP_CONTENT)
				.orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH49_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH49.agregarEspecifique(4, txtQH49_O);
		rgQH50 = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(MATCH_PARENT, MATCH_PARENT).orientation(
				RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH53 = new RadioGroupOtherField(this.getActivity(),
				R.string.hogarqh53_1, R.string.hogarqh53_2,
				R.string.hogarqh53_3, R.string.hogarqh53_4,
				R.string.hogarqh53_5, R.string.hogarqh53_6,
				R.string.hogarqh53_7, R.string.hogarqh53_8,
				R.string.hogarqh53_9, R.string.hogarqh53_10,
				R.string.hogarqh53_11).size(WRAP_CONTENT, WRAP_CONTENT)
				.orientation(RadioGroupOtherField.ORIENTATION.VERTICAL)
				.callback("onP53ChangeValue");
		txtQH53_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH53.agregarEspecifique(10, txtQH53_O);
		
		lblretpublica = new LabelComponent(this.getActivity()).text(R.string.hogarqh53_t1).textSize(18).negrita();
		lblletrina = new LabelComponent(this.getActivity()).text(R.string.hogarqh53_t2).textSize(18).negrita();
		lblotras = new LabelComponent(this.getActivity()).text(R.string.hogarqh53_t3).textSize(18).negrita();
		rgQH53.agregarTitle(0, lblretpublica);
		rgQH53.agregarTitle(4, lblletrina);
		rgQH53.agregarTitle(10, lblotras);
		
		rgQH54 = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		lblpregunta49 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh49).textSize(19);
		lblpregunta50 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh50).textSize(19);
		lblpregunta53 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh53).textSize(19);
		lblpregunta54 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh54).textSize(19);
	}

	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(0, Gravity.LEFT	| Gravity.CENTER_VERTICAL,lblpregunta49, rgQH49);
		q2 = createQuestionSection(lblpregunta50, rgQH50);
		q3 = createQuestionSection(0, Gravity.LEFT | Gravity.CENTER_VERTICAL,lblpregunta53, rgQH53);
		q4 = createQuestionSection(lblpregunta54, rgQH54);
	
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(hogar);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		try {
			if (hogar.qh49!=null) {
				hogar.qh49=hogar.getConvertqh49(hogar.qh49);
			}
			if (hogar.qh53!=null) {
				hogar.qh53=hogar.getConvertqh53(hogar.qh53);
			}
			
			if (!getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado)) {
				ToastMessage.msgBox(this.getActivity(),
						"Los datos no pudieron ser guardados.",
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}	


	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esDiferente(hogar.qh47,91) && Util.esDiferente(hogar.qh48,2)) {
			if (Util.esVacio(hogar.qh49)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.49");
				view = rgQH49;
				error = true;
				return false;
			}
			if (!Util.esDiferente(hogar.qh49, 5)) {
				if (Util.esVacio(hogar.qh49_o)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQH49_O;
					error = true;
					return false;
				}
			}
			if (Util.esVacio(hogar.qh50)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.50");
				view = rgQH50;
				error = true;
				return false;
			}			
		}

		if (Util.esVacio(hogar.qh53)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.53");
			view = rgQH53;
			error = true;
			return false;
		}
		if (!Util.esDiferente(hogar.qh53, 11)) {
			if (Util.esVacio(hogar.qh53_o)) {
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQH53_O;
				error = true;
				return false;
			}
		}
		if (Util.esDiferente(hogar.qh53,9,10,11)) {
			if (Util.esVacio(hogar.qh54)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.54");
				view = rgQH54;
				error = true;
				return false;	
			}
		}
		
		return true;
	}

	@Override
	public void cargarDatos() {
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
		if (hogar == null) {
			hogar = new Hogar();
			hogar.id = App.getInstance().getHogar().id;
			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
		}
		if (hogar.qh49!=null) {
			hogar.qh49=hogar.setConvertqh49(hogar.qh49);
		}
		if (hogar.qh53!=null) {
			hogar.qh53=hogar.setConvertqh53(hogar.qh53);
		}
		entityToUI(hogar);
		inicio();
	}
	
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

	public void saltoqh47(Integer p47, Integer p48) {	
		if (!Util.esDiferente(p47,91) || !Util.esDiferente(p48,2) ) {
//			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH49,rgQH50);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			rgQH53.requestFocus();
		}			
		else{
			Util.lockView(getActivity(), false, rgQH49,rgQH50);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			rgQH49.requestFocus();
		}		
	}

	private void inicio() {
		onP53ChangeValue();
		saltoqh47(hogar.qh47,hogar.qh48);
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH49.readOnly(); 
			txtQH49_O.readOnly();
			rgQH50.readOnly();
			rgQH53.readOnly();
			txtQH53_O.readOnly();
		}
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	public void onP53ChangeValue() {
		if (MyUtil.incluyeRango(9,11,rgQH53.getTagSelected("").toString()) ) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH54);
			q4.setVisibility(View.GONE);
			
		} else {
			Util.lockView(getActivity(), false, rgQH54);
			q4.setVisibility(View.VISIBLE);
			rgQH54.requestFocus();
		}
		
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (hogar.qh49!=null) {
				hogar.qh49=hogar.getConvertqh49(hogar.qh49);
			}
			if (hogar.qh53!=null) {
				hogar.qh53=hogar.getConvertqh53(hogar.qh53);
			}
			
			if (!getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado)) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
}
