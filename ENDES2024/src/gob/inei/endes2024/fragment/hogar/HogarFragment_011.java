package gob.inei.endes2024.fragment.hogar; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.dialog.HogarDialog_011;
import gob.inei.endes2024.fragment.hogar.dialog.HogarDialog_011_1;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_011 extends FragmentForm implements Respondible { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH95; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH99; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH100B; 
	public Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private Seccion01Service seccion01service; 
	private Seccion03Service seccion03service;
	private LabelComponent lblTitulo,lblP65,lblP95,lblP96,lblP99,lblP100,lblP100b,lblind100b_1,lblind100b_2,lblP100c,lblP100c_1; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoSeccion03;
	public TableComponent tcJuntos;
	public ButtonComponent btnAgregarJuntos,btnAgregarPension65,btnAgregarcunamas;
	private List<Hogar> detalles;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public Seccion01 entrevistado;
	public Seccion01 elegido;
	public Seccion03 elegidos03;
	public List<Seccion03> beneficiadosJuntos;
	public List<Seccion03> beneficiadosPension65;
	public List<Seccion03> beneficiadoscunamas;
	public TableComponent tcPension65,tcCunamas_100b;
	private Seccion01Service seccion01;
	private Seccion03Service seccion03;
	private enum ACTION{ELIMINAR,MENSAJE}
	private ACTION action;
	private DialogComponent dialog;
	
	private boolean juntos=false,pension65=false,cunamas100b=false;
	
	public LinearLayout q0,q1,q2,q3,q4,q5,q6;
	
	public int accion=0;
	
// 
	public HogarFragment_011() {}
	
	public HogarFragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
    @Override 
    public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH95","ID","HOGAR_ID","QH99","QH100B","PERSONA_INFORMANTE_ID")};  
		seccionesCargadoSeccion03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","QH02_1","ID","HOGAR_ID","PREGUNTA_ID","QH96AC","QH97DNI","QH97D","QH97M","QH97A","ESTADO3")}; 
		//seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH95","QH96_1A","QH96_1M","QH99","QH100_1A","QH10_1M")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH95","QH99","QH100B")};
		//cargarDatos();
		return rootView; 
    } 
  
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcJuntos.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(0, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(0, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			//Seccion03 seleccionB18 = (Seccion03) info.targetView.getTag();
		}
		if (v.equals(tcPension65.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(1, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(1, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
		if (v.equals(tcCunamas_100b.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(2, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(2, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
	}
  
    @Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosJuntos.get(info.position));
						break;
				case 1:	 
						accion = 0;
				
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,
		                        TIPO_DIALOGO.YES_NO, getResources().getString(
		                                        R.string.app_name),
		                        "¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadosJuntos.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosPension65.get(info.position));
						break;
				case 1:	 
						accion = 0;
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,
		                        TIPO_DIALOGO.YES_NO, getResources().getString(
		                                        R.string.app_name),
		                        "¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadosPension65.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		if (item.getGroupId() == 2) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03cunamas((Seccion03) beneficiadoscunamas.get(info.position));
						break;
				case 1:	 
						accion = 0;
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,
		                        TIPO_DIALOGO.YES_NO, getResources().getString(
		                                        R.string.app_name),
		                        "¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadoscunamas.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		return super.onContextItemSelected(item);
	}
    
    @Override 
    protected void buildFields() { 
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar_3).textSize(21).centrar().negrita();
		lblP65 = new LabelComponent(this.getActivity()).size(altoComponente + 10, 250).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		lblP100c_1 = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh100c_1).textSize(16).alinearDerecha();
		lblind100b_1 = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh100b_ind_1).textSize(16).alinearIzquierda().negrita();
		lblind100b_2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT , MATCH_PARENT).text(R.string.hogarqh100b_ind_2).textSize(16).alinearIzquierda();
		
		lblP95 = new LabelComponent(this.getActivity()).text(R.string.hogarqh95).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP96 = new LabelComponent(this.getActivity()).text(R.string.hogarqh96).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP99 = new LabelComponent(this.getActivity()).text(R.string.hogarqh99).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP100 = new LabelComponent(this.getActivity()).text(R.string.hogarqh100).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP100c = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh100c).textSize(18).alinearIzquierda();
		
		rgQH95=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh95_1,R.string.hogarqh95_2,R.string.hogarqh95_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH95");
		rgQH99=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh99_1,R.string.hogarqh99_2,R.string.hogarqh99_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH99");
		lblP100b = new LabelComponent(this.getActivity()).text(R.string.hogarqh100b).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQH100B=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh100b_1,R.string.hogarqh100b_2,R.string.hogarqh100b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH100B");
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcJuntos = new TableComponent(getActivity(), this,App.ESTILO).size(120, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//			tcPension65 = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//			tcCunamas_100b = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//			
//		}
//		else{
			tcJuntos = new TableComponent(getActivity(), this,App.ESTILO).size(320, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tcPension65 = new TableComponent(getActivity(), this,App.ESTILO).size(330, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tcCunamas_100b = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcJuntos.addHeader(R.string.m_orden_b18_hogar, 2f,
				TableComponent.ALIGN.CENTER);
		tcJuntos.addHeader(R.string.m_nombre_b18_hogar, 3.5f,
				TableComponent.ALIGN.LEFT);
		tcJuntos.addHeader(R.string.m_anio_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		tcJuntos.addHeader(R.string.m_mes_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		btnAgregarJuntos = new ButtonComponent(getActivity(),
				App.ESTILO_BOTON).text(
				R.string.btnAgregar).size(200, 60);
		btnAgregarJuntos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
					agregarBeneficiario();
			}
		});	
		
		
		tcPension65.addHeader(R.string.m_orden_b18_hogar, 2f,
				TableComponent.ALIGN.CENTER);
		tcPension65.addHeader(R.string.m_nombre_b18_hogar, 3.5f,
				TableComponent.ALIGN.LEFT);
		tcPension65.addHeader(R.string.m_anio_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		tcPension65.addHeader(R.string.m_mes_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		btnAgregarPension65 = new ButtonComponent(getActivity(),
				App.ESTILO_BOTON).text(
				R.string.btnAgregar).size(200, 60);
		btnAgregarPension65.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				agregarPension65();
			}
		});
		tcCunamas_100b.addHeader(R.string.m_orden_b18_hogar, 2f,
				TableComponent.ALIGN.CENTER);
		tcCunamas_100b.addHeader(R.string.m_nombre_b18_hogar, 3.5f,
				TableComponent.ALIGN.LEFT);
		tcCunamas_100b.addHeader(R.string.m_anio_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		tcCunamas_100b.addHeader(R.string.m_mes_b18_hogar, 1.5f,
				TableComponent.ALIGN.CENTER);
		btnAgregarcunamas = new ButtonComponent(getActivity(),
				App.ESTILO_BOTON).text(
				R.string.btnAgregar).size(200, 60);
		btnAgregarcunamas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				agregarcunamas();
			}
		});
		textoNegrita();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblP95,rgQH95);
		LinearLayout q2_1 = createQuestionSection(0, Gravity.CENTER, LinearLayout.HORIZONTAL,btnAgregarJuntos);
		q2 = createQuestionSection(lblP96, q2_1,tcJuntos.getTableView());
		LinearLayout q2_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblP65,btnAgregarPension65);
		q3 = createQuestionSection(lblP100, q2_2,tcPension65.getTableView());
			
		q4 = createQuestionSection(lblP99,rgQH99);
		
		LinearLayout q2_3 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblP100c_1,btnAgregarcunamas);
		LinearLayout q2_4 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblind100b_1);
		LinearLayout q2_5 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblind100b_2);
		q5 = createQuestionSection(lblP100b,q2_4,q2_5,rgQH100B);
		q6 = createQuestionSection(lblP100c, q2_3,tcCunamas_100b.getTableView());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q4);
		form.addView(q3);
		form.addView(q5);
		form.addView(q6);
		return contenedor; 
    } 
    
    
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar);	
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh95!=null) {
				hogar.qh95=hogar.getConver80a(hogar.qh95);
			}
			if (hogar.qh99!=null) {
				hogar.qh99=hogar.getConver80a(hogar.qh99);
			}
			if (hogar.qh100b!=null) {
				hogar.qh100b=hogar.getConver80a(hogar.qh100b);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh95)&&juntos) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.95"); 
			view = rgQH95; 
			error = true; 
			return false; 
		}

		if (!Util.esDiferente(hogar.qh95, 1,1) && juntos) {
			if (beneficiadosJuntos.size()<1) { 
				mensaje = "La pregunta P.92 debe tener algun Beneficiario"; 
				view = btnAgregarJuntos;
				error = true; 
				return false; 
			}
		}
		 
		if (Util.esVacio(hogar.qh99)&&pension65) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.99"); 
			view = rgQH99; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh99, 1,1) &&pension65) {
			if (beneficiadosPension65.size()<1) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.94_1A"); 
				view = btnAgregarPension65; 
				error = true; 
				return false; 
			} 
		
		}
		if (Util.esVacio(hogar.qh100b)&& cunamas100b) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.100B"); 
			view = rgQH100B; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh100b, 1,1) && cunamas100b) {
			if (beneficiadoscunamas.size()<1) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.100C"); 
				view = btnAgregarcunamas; 
				error = true; 
				return false; 
			} 
		
		}
		 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    }
		if (hogar.qh95!=null) {
			hogar.qh95=hogar.setConver80a(hogar.qh95);
		}
		if (hogar.qh99!=null) {
			hogar.qh99=hogar.setConver80a(hogar.qh99);
		}
		if (hogar.qh100b!=null) {
			hogar.qh100b=hogar.setConver80a(hogar.qh100b);
		}
		entityToUI(hogar); 
		cargarTabla();
		eliminarBeneficiados();
		inicio(); 
    } 
    
    public void cargarTabla(){
//    	beneficiadosJuntos = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
//    			App.getInstance().getHogar().hogar_id,App.JUNTOS,seccionesCargadoSeccion03);
//    	tcJuntos.setData(beneficiadosJuntos,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//    	registerForContextMenu(tcJuntos.getListView());
    	
    	beneficiadosJuntos = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
    			App.getInstance().getHogar().hogar_id,App.JUNTOS,seccionesCargadoSeccion03);
    	tcJuntos.setData(beneficiadosJuntos,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadosJuntos!=null&& beneficiadosJuntos.size()>0){
	    	for (int row = 0; row < beneficiadosJuntos.size(); row++) {
				if (obtenerEstado(beneficiadosJuntos.get(row)) == 1) {
					// borde de color azul
					tcJuntos.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosJuntos.get(row)) == 2) {
					// borde de color rojo
					tcJuntos.setBorderRow(row, true, R.color.red);
				} else {
					tcJuntos.setBorderRow(row, false);
				}
			}
    	}
    	tcJuntos.reloadData();
	   	registerForContextMenu(tcJuntos.getListView());
	   	
    	
//    	beneficiadosPension65 = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
//    			App.getInstance().getHogar().hogar_id,App.PENSION65,seccionesCargadoSeccion03);
//    	tcPension65.setData(beneficiadosPension65, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//	   	registerForContextMenu(tcPension65.getListView());
	   	
	   	beneficiadosPension65 = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
    			App.getInstance().getHogar().hogar_id,App.PENSION65,seccionesCargadoSeccion03);
    	tcPension65.setData(beneficiadosPension65, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadosPension65!=null&& beneficiadosPension65.size()>0){
	    	for (int row = 0; row < beneficiadosPension65.size(); row++) {
				if (obtenerEstado(beneficiadosPension65.get(row)) == 1) {
					// borde de color azul
					tcPension65.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosPension65.get(row)) == 2) {
					// borde de color rojo
					tcPension65.setBorderRow(row, true, R.color.red);
				} else {
					tcPension65.setBorderRow(row, false);
				}
			}
    	}
    	tcPension65.reloadData();
	   	registerForContextMenu(tcPension65.getListView());
	   	
//	   	beneficiadoscunamas = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,
//    			App.getInstance().getHogar().hogar_id,App.CUNAMAS_100B,seccionesCargadoSeccion03);
//    	tcCunamas_100b.setData(beneficiadoscunamas, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//	   	registerForContextMenu(tcCunamas_100b.getListView());
	   		   	
	   	beneficiadoscunamas = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.CUNAMAS_100B,seccionesCargadoSeccion03);
    	tcCunamas_100b.setData(beneficiadoscunamas, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadoscunamas!=null&& beneficiadoscunamas.size()>0){
	    	for (int row = 0; row < beneficiadoscunamas.size(); row++) {
				if (obtenerEstado(beneficiadoscunamas.get(row)) == 1) {
					// borde de color azul
					tcCunamas_100b.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadoscunamas.get(row)) == 2) {
					// borde de color rojo
					tcCunamas_100b.setBorderRow(row, true, R.color.red);
				} else {
					tcCunamas_100b.setBorderRow(row, false);
				}
			}
    	}
    	tcCunamas_100b.reloadData();
	   	registerForContextMenu(tcCunamas_100b.getListView());
    }
    
    
    public void refrescarBeca18(Seccion03 beneficiarios) {
        if(!Util.esDiferente(beneficiarios.pregunta_id,App.JUNTOS)){
	    	if (beneficiadosJuntos.contains(beneficiarios)) {
	    	    cargarTabla();
                return;
	        } else {
	        	beneficiadosJuntos.add(beneficiarios);
	                     cargarTabla();
	        }
        }
        if(!Util.esDiferente(beneficiarios.pregunta_id, App.PENSION65))
	        {if (beneficiadosPension65.contains(beneficiarios)) {
	            cargarTabla();
	            return;
	        } else {
	        	beneficiadosPension65.add(beneficiarios);
	            cargarTabla();
	        }        	
        }
        if(!Util.esDiferente(beneficiarios.pregunta_id, App.CUNAMAS_100B))
        {if (beneficiadoscunamas.contains(beneficiarios)) {
            cargarTabla();
            return;
        } else {
        	beneficiadoscunamas.add(beneficiarios);
            cargarTabla();
        }        	
    }
        
}

    private void inicio() { 
    	mostrarPreguntas();
    	salto_rgQH95();
    	salto_rgQH99();
    	salto_rgQH100B();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    private int obtenerEstado(Seccion03 detalle) {
 		if (!Util.esDiferente(detalle.estado3, 0)) {
 			return 1 ;
 		} else if (!Util.esDiferente(detalle.estado3,1)) {
 			return 2;
 		}
 		return 0;
 	}
    
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
    
    public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
    
    public void salto_rgQH95() {
		int valor = Integer.parseInt(rgQH95.getTagSelected("0").toString());
	
		AgregarP96();
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarJuntos,tcJuntos);
			
			if(beneficiadosJuntos.size()>0 && valor!=0){
				accion = App.JUNTOS;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Juntos!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			
			rgQH99.requestFocus();
		} else { 
            Util.lockView(getActivity(), false, btnAgregarJuntos,tcJuntos);
            tcJuntos.requestFocus();
            AgregarP96();
		} 
		PosiblesPension65();
	}
    public void salto_rgQH99() {
		int valor = Integer.parseInt(rgQH99.getTagSelected("0").toString());
		
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarPension65,tcPension65);
			
			if(beneficiadosPension65.size()>0 && valor!=0){
				accion = App.PENSION65;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Pensión 65!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			
		} else {
			Util.lockView(getActivity(), false,  btnAgregarPension65,tcPension65);
			tcPension65.requestFocus();
		}
	}
    public void salto_rgQH100B() {
		int valor = Integer.parseInt(rgQH100B.getTagSelected("0").toString());
		
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarcunamas,tcCunamas_100b);
			
			if(beneficiadoscunamas.size()>0 && valor!=0){
				accion = App.CUNAMAS_100B;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			
		} else {
			Util.lockView(getActivity(), false,  btnAgregarcunamas,tcCunamas_100b);
			tcCunamas_100b.requestFocus();
		}
		Posiblescunamas_100b();
	}
    protected void agregarBeneficiario(){
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.JUNTOS;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetalle(bean);
    }
    protected void agregarPension65(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.PENSION65;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetalle(bean);
    }
    protected void agregarcunamas(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.CUNAMAS_100B;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetallep_100b(bean);
    }
    public void abrirDetallep_100b(Seccion03 tmp) {    	
		FragmentManager fm = HogarFragment_011.this.getFragmentManager();
		HogarDialog_011_1 aperturaDialog = HogarDialog_011_1.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    public void abrirDetalle(Seccion03 tmp) {    	
		FragmentManager fm = HogarFragment_011.this.getFragmentManager();
		HogarDialog_011 aperturaDialog = HogarDialog_011.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public void EditarSeccion03(Seccion03 tmp) {	
//    	tmp.persona_id=1;
		FragmentManager fm = HogarFragment_011.this.getFragmentManager();
		HogarDialog_011 aperturaDialog = HogarDialog_011.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    public void EditarSeccion03cunamas(Seccion03 tmp) {	
//    	tmp.persona_id=1;
		FragmentManager fm = HogarFragment_011.this.getFragmentManager();
		HogarDialog_011_1 aperturaDialog = HogarDialog_011_1.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
    
    private Seccion03Service getServiceSeccion03() {
        if (seccion03 == null) {
        	seccion03 = Seccion03Service.getInstance(getActivity());
        }
        return seccion03;
	}
   @Override
    public void onCancel() {
	   if(accion==App.JUNTOS){

      		rgQH95.setTagSelected(1);
      }
	   if(accion==App.PENSION65){
     	
     		rgQH99.setTagSelected(1);
     }
	   if(accion==App.CUNAMAS_100B ){
    		rgQH100B.setTagSelected(1);
    }
    }

   @Override 
    public void onAccept() {
        if (action == ACTION.MENSAJE) {
                return;
        }
        if (dialog == null) {
                return;
        }
        
        if(accion==0){
	        Seccion03 bean = (Seccion03) dialog.get("seleccion");
	        
	        try {//bean.persona_id=1;
	        if(!Util.esDiferente(bean.pregunta_id, App.JUNTOS))
	        {    beneficiadosJuntos.remove(bean);}
	        if(!Util.esDiferente(bean.pregunta_id, App.PENSION65))
	        {  beneficiadosPension65.remove(bean);}
	        if(!Util.esDiferente(bean.pregunta_id, App.CUNAMAS_100B))
	        {  beneficiadoscunamas.remove(bean);}
	        	boolean borrado=false;
	                borrado =getSeccion03Service().BorrarPersonaPregunta(bean);
	                if (!borrado) {
	                        throw new SQLException("Persona no pudo ser borrada.");
	                }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        if(accion==App.JUNTOS){

        	
        	try {
	            while(beneficiadosJuntos.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadosJuntos.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadosJuntos.remove(beneficiadosJuntos.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        if(accion==App.PENSION65){

        	
        	try {
	            while(beneficiadosPension65.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadosPension65.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadosPension65.remove(beneficiadosPension65.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
		if(accion==App.CUNAMAS_100B){
		
		        	
		        	try {
			            while(beneficiadoscunamas.size()>0){
			            	boolean borrado=false;
			            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadoscunamas.get(0));
			            	if (!borrado) {
			                    throw new SQLException("Persona no pudo ser borrada.");
			            	}
			            	beneficiadoscunamas.remove(beneficiadoscunamas.get(0));
			            }
			        } catch (SQLException e) {                      
			                e.printStackTrace();
			                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
			                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			        }
		        }
        
        cargarTabla();
		salto_rgQH95();
    }
	
	public void AgregarP96(){
	
		if(beneficiadosJuntos.size()>0){
			Util.cleanAndLockView(this.getActivity(), btnAgregarJuntos);
			btnAgregarJuntos.setVisibility(View.GONE);
		}else{
			Util.lockView(getActivity(), false, btnAgregarJuntos);
			btnAgregarJuntos.setVisibility(View.VISIBLE);
		}
	}
	
	public void PosiblesPension65(){
		List<Seccion01> PosiblePension65 = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAPENSION65,App.EDADMAXIMAPENSION65,App.PENSION65);
	   	if(PosiblePension65.size()<=0){
	   		rgQH99.lockButtons(true,0);
		}else{
			Util.lockView(getActivity(), false, rgQH99);
		}
    }
	public void Posiblescunamas_100b(){
		List<Seccion01> Posiblecunamas = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMACUNAMASNINIO100B,App.EDADMAXIMACUNAMASNINIO100B,App.CUNAMAS_100B);
		Posiblecunamas.addAll(getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMACUNAMASMEF100B,App.EDADMAXIMACUNAMASMEF100B,App.CUNAMAS_100B));
	   	if(Posiblecunamas.size()<=0){
	   		rgQH100B.lockButtons(true,0);
		}else{
			Util.lockView(getActivity(), false, rgQH100B);
		}
    }
	
	public void PosiblesBeneficiarios(){
 		List<Seccion01> PosibleJuntos = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAJUNTOS,App.EDADMAXIMAJUNTOS,App.JUNTOS);
 	   	if(PosibleJuntos.size()>0){
 	   		juntos = true;
 		}else{juntos=false;}
 	   	
 	   List<Seccion01> PosiblePension65 = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAPENSION65,App.EDADMAXIMAPENSION65,App.PENSION65);
	   	if(PosiblePension65.size()>0){
	   		pension65 = true;
		}else{pension65=false;}
	   	List<Seccion01> Posiblecunamas = getSeccion01Service().getCunamasvalidacion(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.CUNAMAS_100B);
	   	//Log.e("Cantidad: ",""+Posiblecunamas.size());
		if(Posiblecunamas.size()>0){
	   		cunamas100b = true;
		}
		else{cunamas100b=false;}
    }
	
	public void mostrarPreguntas(){
    	int val95=0,val99=0, val100b=0;
    	PosiblesBeneficiarios();
    	
    	if(juntos){
    		q1.setVisibility(View.VISIBLE);
    		val95 = Integer.parseInt(rgQH95.getTagSelected("0").toString());
    		
    		if (Util.esDiferente(val95, 1,1)) {
    			q2.setVisibility(View.GONE);
    		}else{
    			q2.setVisibility(View.VISIBLE);
    		}    		
    	}else{
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    	
    	if(pension65){
    		q4.setVisibility(View.VISIBLE);
    		val99 = Integer.parseInt(rgQH99.getTagSelected("0").toString());
    		
    		if (Util.esDiferente(val99, 1,1)) {
    			q3.setVisibility(View.GONE);
    		}else{
    			q3.setVisibility(View.VISIBLE);
    		}  
    	}
    	else{
    		q4.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	}
    	if(cunamas100b){
    			q5.setVisibility(View.VISIBLE);
    			val100b=Integer.parseInt(rgQH100B.getTagSelected("0").toString());
//    			Log.e("","SS: "+val100b);
    			if (Util.esDiferente(val100b, 1,1)) {
        			q6.setVisibility(View.GONE);
        		}else{
        			q6.setVisibility(View.VISIBLE);
        		}  
    		}
    	else{
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    	}
    }
	
	public void eliminarBeneficiados(){
		   boolean eliminoJuntos=false,eliminoP65=false;
		   for(int i=0;i<beneficiadosJuntos.size();i++){
			   Seccion01 band = new Seccion01();
			   band = getSeccion01Service().getPersonaEdad(beneficiadosJuntos.get(i).id, beneficiadosJuntos.get(i).hogar_id, beneficiadosJuntos.get(i).persona_id_orden);
			   		   
			   if(!MyUtil.incluyeRango(App.EDADMINIMAJUNTOS, App.EDADMAXIMAJUNTOS, band.qh07)){
				   eliminoJuntos = true;
				   try {
			        	boolean borrado=false;
			            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosJuntos.get(i));
			            if (!borrado) {
			                    throw new SQLException("Persona no pudo ser borrada.");
			            }
			        } catch (SQLException e) {                      
			                e.printStackTrace();
			                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
			                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			        } 
			   }
		   }
		   
		   for(int i=0;i<beneficiadosPension65.size();i++){
			   Seccion01 band = new Seccion01();
			   band = getSeccion01Service().getPersonaEliminarSeccion3(beneficiadosPension65.get(i).id, beneficiadosPension65.get(i).hogar_id, beneficiadosPension65.get(i).persona_id_orden);
			   		   
			   if(!MyUtil.incluyeRango(App.EDADMINIMAPENSION65, App.EDADMAXIMAPENSION65, band.qh07) || MyUtil.incluyeRango(8, 8, band.qh13)){
				   eliminoP65 = true;
				   try {
			        	boolean borrado=false;
			            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosPension65.get(i));
			            if (!borrado) {
			                    throw new SQLException("Persona no pudo ser borrada.");
			            }
			        } catch (SQLException e) {                      
			                e.printStackTrace();
			                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
			                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			        } 
			   }
		   }
		   
		   if(eliminoJuntos || eliminoP65){
			   cargarTabla();
		   }
	   }
	
	public void textoNegrita(){
  	   Spanned texto95 = Html.fromHtml(lblP95.getText()+"<b> JUNTOS?</b>");    	
  	   lblP95.setText(texto95);
  	   
  	   Spanned texto99 = Html.fromHtml(lblP99.getText()+"<b> PENSI&Oacute;N 65?</b>");    	
  	   lblP99.setText(texto99);
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH95.readOnly();
			tcJuntos.setEnabled(false);
			rgQH99.readOnly();
			rgQH100B.readOnly();
			tcPension65.setEnabled(false);
			btnAgregarJuntos.setEnabled(false);
			btnAgregarPension65.setEnabled(false);
		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (hogar.qh95!=null) {
				hogar.qh95=hogar.getConver80a(hogar.qh95);
			}
			if (hogar.qh99!=null) {
				hogar.qh99=hogar.getConver80a(hogar.qh99);
			}	
			if (hogar.qh99!=null) {
				hogar.qh100b=hogar.getConver80a(hogar.qh100b);
			}	
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
