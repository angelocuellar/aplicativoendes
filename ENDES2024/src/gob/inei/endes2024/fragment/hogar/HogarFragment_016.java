package gob.inei.endes2024.fragment.hogar; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion04.Dialog.Seccion06_001Dialog;
import gob.inei.endes2024.model.Endes_ch_seis_validar;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.N_LITERAL;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_016 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH224; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH225U; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQH225; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQH225T;
	@FieldAnnotation(orderIndex = 5)
	public ButtonComponent btnBuscar;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH227; 
	@FieldAnnotation(orderIndex=7) 
	public DecimalField txtQH227R; 
	@FieldAnnotation(orderIndex=8) 
	public DecimalField txtQH227R2; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQH227A;
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQH227B;
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQH227B_O;

	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH228_1;
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQH228_2;
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQH228_3;
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQH228_4;
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQH228_5;
	@FieldAnnotation(orderIndex=17) 
	public RadioGroupOtherField rgQH228_6;
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQH228_7;
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQH228_8;
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQH228_9;
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQH228_10;
	
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQHREC_06;
	@FieldAnnotation(orderIndex=23) 
	public TextAreaField txtQH_OBS_SECCION06;
	
	
	public ButtonComponent btnqh200_y_ini,btnqh200_y_fin;
	public ButtonComponent btnqh200_c_ini,btnqh200_c_fin;
	
	Hogar hogar; 
	Endes_ch_seis_validar modelvalidar=null;
	public TextField txtCabecera, txtMarca;
	private CuestionarioService cuestionarioService; 
	
	private LabelComponent lblTitulo,lbltituloMarca,lblMarca, lblPreg223,lblPreg224,lblPreg225,lblPreg225_ind1,lblPreg225n,lblPreg226,lblPreg226n,lblPreg227,lblresultado,lblmg,lblresultado2,lblmg2,
	lblEspacio,lblPreg227a,lblPreg227b,lblPreg227b_ind,lblPreg228,lblPreg228_ind,lblPreg228_1,lblPreg228_2,lblPreg228_3,lblPreg228_4,lblPreg228_5,lblPreg228_6,
	lblPreg228_7,lblPreg228_8,lblPreg228_9,lblPreg228_10;
	
	private LabelComponent lblpre200_y_ini_f,lblpre200_y_fin_f,lblpre200_y_ini_t,lblpre200_y_fin_t;
	private LabelComponent lblpre200_c_ini_f,lblpre200_c_fin_f,lblpre200_c_ini_t,lblpre200_c_fin_t;
	
	private GridComponent2 grid_QH200_y,grid_QH200_c,grid_QH200_yf,grid_QH200_cf;
	
	public SpinnerField spnQH225;
	private GridComponent2 grid1,grid2,gridPreguntas227, grid1P228;
	public N_LITERAL seleccionado=null;
	
	LinearLayout q0,q2,q3,q4,q6,q8,q9,q10,q12,q13,q14,q1,q5,q7,q11;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public HogarFragment_016() {} 
	public HogarFragment_016 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQH225, 1, 37, null, 96); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH200_REF_S6","QH223A", "QH225A", "QH226A", "QH227C", "QH224","QH225U","QH225","QH225T","QH227","QH227R","QH227R2","QH227A","QH227B","QH227B_O","QH228_1","QH228_2","QH228_3","QH228_4","QH228_5","QH228_6","QH228_7","QH228_8","QH228_9","QH228_10","QHREC_06","QHFRC_FECHA06","QH_OBS_SECCION06","QH40","QH47","QH48","ID","HOGAR_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH200_REF_S6","QH223A", "QH225A", "QH226A", "QH227C", "QH224","QH225U","QH225","QH225T","QH227","QH227R","QH227R2","QH227A","QH227B","QH227B_O","QH228_1","QH228_2","QH228_3","QH228_4","QH228_5","QH228_6","QH228_7","QH228_8","QH228_9","QH228_10","QH_OBS_SECCION06","QHREC_06")}; 
		return rootView; 
	} 
	@Override 
	protected void buildFields() { 
		txtCabecera 	= new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo		= new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.lbltituloseccion6).textSize(21).centrar().negrita(); 
		
		
	    lblpre200_y_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh223a).textSize(17);
	    lblpre200_y_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh225a).textSize(17);
	    lblpre200_y_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    lblpre200_y_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
	    lblpre200_c_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh226a).textSize(17);
	    lblpre200_c_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh227c).textSize(17);
	    lblpre200_c_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    lblpre200_c_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
	    btnqh200_y_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
	    btnqh200_y_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
	    btnqh200_c_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
	    btnqh200_c_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
	    
	    btnqh200_y_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre200_y_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				btnqh200_y_fin.setEnabled(true);
			}
		});
	    btnqh200_y_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre200_y_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
	    btnqh200_c_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre200_c_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				btnqh200_c_fin.setEnabled(true);
			}
		});
	    btnqh200_c_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre200_c_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
	    
	    grid_QH200_y = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QH200_y.addComponent(btnqh200_y_ini);
	    grid_QH200_y.addComponent(lblpre200_y_ini_f);
	    
	    grid_QH200_yf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QH200_yf.addComponent(btnqh200_y_fin);
	    grid_QH200_yf.addComponent(lblpre200_y_fin_f);
	    
	    grid_QH200_c = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QH200_c.addComponent(btnqh200_c_ini);
	    grid_QH200_c.addComponent(lblpre200_c_ini_f);
	    
	    grid_QH200_cf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
	    grid_QH200_cf.addComponent(btnqh200_c_fin);
	    grid_QH200_cf.addComponent(lblpre200_c_fin_f);
		
		lblPreg223		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh223).textSize(16).alinearIzquierda(); 
		lblPreg224		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh224).textSize(16).alinearIzquierda(); 
		lblPreg225		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqhPreg225).textSize(19).alinearIzquierda();
		lblPreg225_ind1	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqhPreg225_ind1).textSize(16).alinearIzquierda();
		lblPreg227a		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqhPreg227a).textSize(16).alinearIzquierda();
		lblPreg227b		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqhPreg227b).textSize(16).alinearIzquierda();
		lblPreg227b_ind	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqhPreg227b_ind).textSize(16).alinearIzquierda().negrita();
		lblPreg228		= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh228).textSize(16).alinearIzquierda().negrita();
		lblPreg228_ind	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh228_ind).textSize(16).alinearIzquierda();
		
		lblPreg228_1=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_1).textSize(15).alinearIzquierda();
		lblPreg228_2=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_2).textSize(15).alinearIzquierda();
		lblPreg228_3=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_3).textSize(15).alinearIzquierda();
		lblPreg228_4=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_4).textSize(15).alinearIzquierda();
		lblPreg228_5=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_5).textSize(15).alinearIzquierda();
		lblPreg228_6=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_6).textSize(15).alinearIzquierda();
		lblPreg228_7=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_7).textSize(15).alinearIzquierda();
		lblPreg228_8=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_8).textSize(15).alinearIzquierda();
		lblPreg228_9=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_9).textSize(15).alinearIzquierda();
		lblPreg228_10=new LabelComponent(this.getActivity()).size(80, 400).text(R.string.hogarqh228_10).textSize(15).alinearIzquierda();
		
		
		Spanned texto = Html.fromHtml("BOLSA NO VISTA: <b>�Cu&aacute;l es la marca de la sal que est&aacute;n usando?</b>"); 
		lblPreg225n = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17);
		lblPreg225n.setText(texto);
		
		rgQH224=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh224_1,R.string.hogarqh224_2,R.string.hogarqh224_3,R.string.hogarqh224_4,R.string.hogarqh224_5,R.string.hogarqh224_6).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP224ChangeValue"); 
		rgQH225U=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh225u_1, R.string.hogarqh225u_2,R.string.hogarqh225u_3).size(150,190).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP225ChangeValue");
		txtQH225=new IntegerField(this.getActivity()).size(60, 50).maxLength(2);
		txtQH225T = new TextField(getActivity()).size(60,500).maxLength(40);
	
		txtMarca = new TextField(this.getActivity()).size(altoComponente,250).maxLength(40);
		lblMarca=new LabelComponent(this.getActivity(),App.ESTILO).size(60,350).textSize(21).centrar().negrita(); 
		lbltituloMarca=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh225_sal).textSize(21).centrar().negrita(); 
		spnQH225=new SpinnerField(getActivity()).size(altoComponente+15, 420);

		Spanned texto2 = Html.fromHtml("<b>226. VEA LA PREGUNTA \"40\".   LUEGO</b>, SOLICITE  PERMISO  PARA  TOMAR  UNA  MUESTRA  DEL  AGUA  QUE  SE CONSUME EN EL HOGAR,  INDAGANDO SOBRE LA FUENTE O RECIPIENTE DE DONDE LOS MIEMBROS DEL HOGAR ACCEDEN  AL AGUA QUE BEBEN  DIARIAMENTE, A  FIN DE EVALUAR LA CALIDAD BACTERIOL&Oacute;GICA DE LA MISMA.<br/><br/><b>TOMAR LA MUESTRA DEL GRIFO O CA&Ntilde;O</b>, SI EL HOGAR CUENTA CON SERVICIO DE RED P&Uacute;BLICA Y LA CONSUME DIRECTAMENTE DEL LUGAR DE SUMINISTRO.<br/><br/><b>TOMAR  LA MUESTRA  DEL  DEP&Oacute;SITO O RECIPIENTE</b>,  SI  EL HOGAR NO CUENTA  CON  SERVICIO DE RED P&Uacute;BLICA; &Oacute;, SI TENIENDO EL SERVICIO DE RED P&Uacute;BLICA EL AGUA NO LA CONSUME DIRECTAMENTE DE LA FUENTE DE SUMINISTRO SINO DEL RECIPIENTE DONDE  LA GUARDA"); 
		lblPreg226 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblPreg226.setText(texto2);

		lblPreg226n=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh226n).textSize(16).alinearIzquierda().negrita(); 
		lblPreg227=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh227).textSize(16); 
		
		rgQH227=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh227_1,R.string.hogarqh227_2,R.string.hogarqh227_3,R.string.hogarqh227_4,R.string.hogarqh227_5,R.string.hogarqh227_6_1,R.string.hogarqh227_6).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP227RChangeValue");
		
		rgQH227A=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh227a_1,R.string.hogarqh227a_2).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH227B=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh227b_1,R.string.hogarqh227b_2,R.string.hogarqh227b_3,R.string.hogarqh227b_4,R.string.hogarqh227b_5,R.string.hogarqh227b_7).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH227B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH227B.agregarEspecifique(5,txtQH227B_O);
		
		lblresultado=new LabelComponent(this.getActivity()).size(altoComponente,400).text(R.string.hogarqh227res).textSize(16).negrita();
		txtQH227R=new DecimalField(this.getActivity()).maxLength(1).size(altoComponente+15, 100).decimales(1);
		lblmg=new LabelComponent(this.getActivity()).size(altoComponente,80).text(R.string.hogarqh227mg).textSize(17);
		
		lblresultado2=new LabelComponent(this.getActivity()).size(altoComponente,400).text(R.string.hogarqh227res2).textSize(16).negrita();
		txtQH227R2=new DecimalField(this.getActivity()).maxLength(1).size(altoComponente+15, 100).decimales(2);
		lblmg2=new LabelComponent(this.getActivity()).size(altoComponente,80).text(R.string.hogarqh227mg2).textSize(17);
		
		txtQH227R.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length()>0){
					Util.cleanAndLockView(getActivity(),txtQH227R2);					
				}
				else{
					Util.lockView(getActivity(),false, txtQH227R2);
				}
			}
		});
		
		txtQH227R2.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.toString().trim().length()>0){
					Util.cleanAndLockView(getActivity(),txtQH227R);					
				}
				else{
					Util.lockView(getActivity(),false, txtQH227R);
				}
			}
		});
		
		
		rgQH228_1 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_2 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_3 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_4 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_5 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_6 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_7 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_8 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_9 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQH228_10 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh228_x1, R.string.hogarqh228_x2,R.string.hogarqh228_x3).size(50, 370).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
				
		rgQHREC_06 = new RadioGroupOtherField(this.getActivity(),R.string.seccion04_rec01, R.string.seccion04_rec02).size(50, 350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		txtQH_OBS_SECCION06= new TextAreaField(getActivity()).size(200, 650).maxLength(500).alfanumerico();
		txtQH_OBS_SECCION06.setCallback("OcultarTecla");
		btnBuscar = new ButtonComponent(getActivity()).size(200, 60).text(R.string.btnBuscar);		
		
		gridPreguntas227= new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridPreguntas227.addComponent(lblresultado);		
		gridPreguntas227.addComponent(txtQH227R);
		gridPreguntas227.addComponent(lblmg);
		gridPreguntas227.addComponent(lblresultado2);		
		gridPreguntas227.addComponent(txtQH227R2);
		gridPreguntas227.addComponent(lblmg2);
		
		grid1 = new GridComponent2(this.getActivity(), 2);
		grid1.addComponent(lblMarca);		
		grid1.addComponent(btnBuscar);
		
		grid2 = new GridComponent2(this.getActivity(), 2);
		grid2.addComponent(txtQH225);
		grid2.addComponent(txtQH225T);
		
		grid1P228 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1P228.addComponent(lblPreg228_1);
		grid1P228.addComponent(rgQH228_1);
		grid1P228.addComponent(lblPreg228_2);
		grid1P228.addComponent(rgQH228_2);
		grid1P228.addComponent(lblPreg228_3);
		grid1P228.addComponent(rgQH228_3);
		grid1P228.addComponent(lblPreg228_4);
		grid1P228.addComponent(rgQH228_4);
		grid1P228.addComponent(lblPreg228_5);
		grid1P228.addComponent(rgQH228_5);
		grid1P228.addComponent(lblPreg228_6);
		grid1P228.addComponent(rgQH228_6);
		grid1P228.addComponent(lblPreg228_7);
		grid1P228.addComponent(rgQH228_7);
		grid1P228.addComponent(lblPreg228_8);
		grid1P228.addComponent(rgQH228_8);
		grid1P228.addComponent(lblPreg228_9);
		grid1P228.addComponent(rgQH228_9);
		grid1P228.addComponent(lblPreg228_10);
		grid1P228.addComponent(rgQH228_10);
			
		btnBuscar.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
				abrirBuscar();
			}
		});
		
		
	} 
	
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q2 = createQuestionSection(lblPreg223);
		
		q1 = createQuestionSection(lblpre200_y_ini_t,grid_QH200_y.component());		
		q3 = createQuestionSection(lblPreg224,rgQH224); 
		q4 = createQuestionSection(lblPreg225,lblPreg225_ind1,lblPreg225n,rgQH225U,lbltituloMarca,grid1.component(),grid2.component());
		q5 = createQuestionSection(lblpre200_y_fin_t,grid_QH200_yf.component());
		
		q6 = createQuestionSection(lblPreg226,lblPreg226n); 
		
		q7 = createQuestionSection(lblpre200_c_ini_t,grid_QH200_c.component());
		q8 = createQuestionSection(lblPreg227,gridPreguntas227.component(),rgQH227);		
		q9 = createQuestionSection(lblPreg227a,rgQH227A);
		q10 = createQuestionSection(lblPreg227b,lblPreg227b_ind,rgQH227B);
		q11 = createQuestionSection(lblpre200_c_fin_t,grid_QH200_cf.component());
		
		q12 = createQuestionSection(lblPreg228,lblPreg228_ind,grid1P228.component());
		q13 = createQuestionSection(R.string.seccion04_rec,rgQHREC_06);		
		q14 = createQuestionSection(R.string.hogarqhObservacioones_s6,txtQH_OBS_SECCION06);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q2);
		form.addView(q1);		 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);
		form.addView(q9);
		form.addView(q10);
		form.addView(q11);
		form.addView(q12);
		if(App.getInstance().getMarco().asignado==4){
			form.addView(q13);
		}
		form.addView(q14);
		return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar); 
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, 
						ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		}
		
		try { 
			hogar.qh224 = hogar.getConvert224(hogar.qh224);
			if (hogar.qh227 != null) {
				hogar.qh227 = hogar.getConvert227(hogar.qh227);
			}
			if (hogar.qh225u != null) {
				hogar.qh225u = hogar.getConvert225u(hogar.qh225u);
			}
			if (seleccionado!=null) {
				hogar.qh225=Integer.parseInt(seleccionado.n1);
			    hogar.qh225t=seleccionado.n1_literal;
			    if(seleccionado.n1=="96"){
			    	hogar.qh225t=""+txtQH225T.getText(); 
			    }
			}
			else{				
				hogar.qh225=null;
			    hogar.qh225t=null;
			}
			if(hogar!=null && hogar.qhrec_06!=null && hogar.qhfrc_fecha06==null){
				hogar.qhfrc_fecha06=Util.getFechaActualToString();
			}
			hogar.qh223a =lblpre200_y_ini_f.getText().toString();
			hogar.qh225a =lblpre200_y_fin_f.getText().toString();
			hogar.qh226a =lblpre200_c_ini_f.getText().toString();
			hogar.qh227c =lblpre200_c_fin_f.getText().toString();
			
			if(hogar.qh200_ref_s6==null ){
				hogar.qh200_ref_s6=Util.getFechaActualToString();
			}
			
			if (hogar.qh228_1 != null) {
				hogar.qh228_1 = hogar.getConver228_1(hogar.qh228_1);
			}
			if (hogar.qh228_2 != null) {
				hogar.qh228_2 = hogar.getConver228_2(hogar.qh228_2);
			}
			if (hogar.qh228_3 != null) {
				hogar.qh228_3 = hogar.getConver228_3(hogar.qh228_3);
			}
			if (hogar.qh228_4 != null) {
				hogar.qh228_4 = hogar.getConver228_4(hogar.qh228_4);
			}
			if (hogar.qh228_5 != null) {
				hogar.qh228_5 = hogar.getConver228_5(hogar.qh228_5);
			}
			if (hogar.qh228_6 != null) {
				hogar.qh228_6 = hogar.getConver228_6(hogar.qh228_6);
			}
			if (hogar.qh228_7 != null) {
				hogar.qh228_7 = hogar.getConver228_7(hogar.qh228_7);
			}
			if (hogar.qh228_8 != null) {
				hogar.qh228_8 = hogar.getConver228_8(hogar.qh228_8);
			}
			if (hogar.qh228_9 != null) {
				hogar.qh228_9 = hogar.getConver228_9(hogar.qh228_9);
			}
			if (hogar.qh228_10 != null) {
				hogar.qh228_10 = hogar.getConver228_10(hogar.qh228_10);
			}
			
			
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", 
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} 
		catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO,
					ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getHogar().qh224=hogar.qh224;
		return true; 
    } 
    
    public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH227.getWindowToken(), 0);
	}
    
    private boolean validar() { 
    	if(!isInRange()) return false; 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		

    	if (Util.esVacio(hogar.qh224)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 224"); 
    		view = rgQH224; 
    		error = true; 
    		return false; 
		}
    	if (!Util.esDiferente(hogar.qh224,1,2,3,4)) {
			if (Util.esVacio(hogar.qh225u)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta 225A"); 
				view = rgQH225U; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(hogar.qh225u,1,2)) {
			    if(seleccionado==null){
			    	if (Util.esVacio(hogar.qh225)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta 225"); 
						view = txtQH225; 
						error = true; 
						return false; 
						
					} 
					if (Util.esVacio(hogar.qh225t)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta 225T"); 
							view = txtQH225T; 
							error = true; 
							return false; 
					}
			    }
				if(seleccionado!=null && !Util.esDiferente(seleccionado.n1, "96")){
					if (Util.esVacio(hogar.qh225t)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta 225T"); 
						view = txtQH225T; 
						error = true; 
						return false; 
				    }
		    	}
			}
    	}
    	
    	if (Util.esVacio(rgQH227.getTagSelected("").toString())) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 227"); 
    		view = rgQH227; 
    		error = true; 
    		return false; 
    	}
	    if (!Util.esDiferente(hogar.qh227,1)) {
	    	if (Util.esVacio(hogar.qh227r) && Util.esVacio(hogar.qh227r2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta 227r"); 
				view = txtQH227R; 
				error = true; 
				return false; 
			} 
	    	if (!Util.esVacio(hogar.qh227r) && (hogar.qh227r.doubleValue()<0.5 || hogar.qh227r.doubleValue()>3.4) ) {
		    	mensaje ="Fuera de rango";
		    	view = txtQH227R; 
		    	error = true; 
		    	return false; 				    	
	    	}
	    
	    	if (!Util.esVacio(hogar.qh227r2) && (hogar.qh227r2.doubleValue()<0.5 || hogar.qh227r2.doubleValue()>8.00) ) {
		    	mensaje ="Fuera de rango";
		    	view = txtQH227R2; 
		    	error = true; 
		    	return false; 				    	
	    	}
	    }
	    if (!Util.esDiferente(hogar.qh227,2)) {
	    	if (Util.esVacio(hogar.qh227r) && Util.esVacio(hogar.qh227r2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta 227r"); 
				view = txtQH227R; 
				error = true; 
				return false; 
			} 
	    	if (!Util.esVacio(hogar.qh227r) && (hogar.qh227r.doubleValue()<0.1 || hogar.qh227r.doubleValue()>0.4) ) {
	    		mensaje ="Fuera de rango";
	    		view = txtQH227R; 
	    		error = true; 
	    		return false; 				    	
	    	}
	    	if (!Util.esVacio(hogar.qh227r2) && (hogar.qh227r2.doubleValue()<0.01 || hogar.qh227r2.doubleValue()>0.49) ) {
	    		mensaje ="Fuera de rango";
	    		view = txtQH227R2; 
	    		error = true; 
	    		return false; 				    	
	    	}
	    }
	    
	    if (!Util.esDiferente(hogar.qh227,1,2,3,6)) {
	    	if (Util.esVacio(hogar.qh227a)) { 
	    		mensaje = preguntaVacia.replace("$", "La pregunta 227A"); 
	    		view = rgQH227A; 
	    		error = true; 
	    		return false; 
			}
	    	
	    	if (Util.esVacio(hogar.qh227b)) { 
	    		mensaje = preguntaVacia.replace("$", "La pregunta 227B"); 
	    		view = rgQH227B; 
	    		error = true; 
	    		return false; 
			}
	    	if (!Util.esDiferente(hogar.qh227b,6)) {    		
	        	if (Util.esVacio(hogar.qh227b_o)) { 
	        		mensaje = preguntaVacia.replace("$", "La pregunta 227B_O"); 
	        		view = txtQH227B_O; 
	        		error = true; 
	        		return false; 
	    		}
			}
	    }
	    
	    if (Util.esVacio(hogar.qh228_1)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_1"); 
    		view = rgQH228_1; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_2)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_2"); 
    		view = rgQH228_2; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_3)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_3"); 
    		view = rgQH228_3; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_4)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_4"); 
    		view = rgQH228_4; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_5)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_5"); 
    		view = rgQH228_5; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_6)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_6"); 
    		view = rgQH228_6; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_7)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_7"); 
    		view = rgQH228_7; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_8)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_8"); 
    		view = rgQH228_8; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_9)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_9"); 
    		view = rgQH228_9; 
    		error = true; 
    		return false; 
		}
	    if (Util.esVacio(hogar.qh228_10)) { 
    		mensaje = preguntaVacia.replace("$", "La pregunta 228_10"); 
    		view = rgQH228_10; 
    		error = true; 
    		return false; 
		}
	    
	    
	    
	    if(lblpre200_y_ini_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 223A");
			view = btnqh200_y_ini;
			error = true;
			return false;
		}
		if(lblpre200_y_fin_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 225A");
			view = btnqh200_y_fin	;
			error = true;
			return false;
		}
	
		
		if(lblpre200_y_ini_f.getText().toString().trim().length()!=0 && lblpre200_y_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_iniA = new GregorianCalendar();
    		if(lblpre200_y_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_y_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_iniA=cal;			
    		}
    		
    		Calendar fecha_finA = new GregorianCalendar();
        	if(lblpre200_y_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_y_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_finA=cal;			
    		}
        	   		
    		
        	if (Util.compare((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) < 0) {
        		mensaje = "Fecha final P225A no puede ser menor a la fecha inicial"; 
    			view = lblpre200_y_fin_f; 
    			error = true; 
    			return false;
        	}	
        	if ( (Util.compare((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) == 0)  && (Util.compareTime((Date) fecha_finA.getTime(),(Date) fecha_iniA.getTime()) < 0)) {
        		mensaje = "Hora final P225A no puede ser menor a la hora inicial"; 
    			view = lblpre200_y_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
		
		
		if(lblpre200_c_ini_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 226A");
			view = btnqh200_c_ini;
			error = true;
			return false;
		}
		if(lblpre200_c_fin_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 227C");
			view = btnqh200_c_fin	;
			error = true;
			return false;
		}
		
		if(lblpre200_c_ini_f.getText().toString().trim().length()!=0 && lblpre200_c_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_iniB = new GregorianCalendar();
    		if(lblpre200_c_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_c_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_iniB=cal;			
    		}
    		
    		Calendar fecha_finB = new GregorianCalendar();
        	if(lblpre200_c_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_c_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_finB=cal;			
    		}
        	if (Util.compare((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) < 0) {
        		mensaje = "Fecha final P227C no puede ser menor a la fecha inicial"; 
    			view = lblpre200_c_fin_f; 
    			error = true; 
    			return false;
        	}	
        	if ( (Util.compare((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) == 0)  && (Util.compareTime((Date) fecha_finB.getTime(),(Date) fecha_iniB.getTime()) < 0)) {
        		mensaje = "Hora final P227C no puede ser menor a la hora inicial"; 
    			view = lblpre200_c_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
		
		
		

    	

    	
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
		hogar = getCuestionarioService().getHogar(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado);
		
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id;  
	    } 
		if (hogar.qh224 != null) {
			hogar.qh224 = hogar.setConvert224(hogar.qh224);
		}
		if (hogar.qh225u != null) {
			hogar.qh225u = hogar.setConvert225u(hogar.qh225u);
		}
		if (hogar.qh227 != null) {
			hogar.qh227 = hogar.setConvert227(hogar.qh227);
		}
		if (hogar.qh228_1!=null) {
			hogar.qh228_1=hogar.setConver228_1(hogar.qh228_1);
		}
		if (hogar.qh228_2!=null) {
			hogar.qh228_2=hogar.setConver228_2(hogar.qh228_2);
		}
		if (hogar.qh228_3!=null) {
			hogar.qh228_3=hogar.setConver228_3(hogar.qh228_3);
		}
		if (hogar.qh228_4!=null) {
			hogar.qh228_4=hogar.setConver228_4(hogar.qh228_4);
		}
		if (hogar.qh228_5!=null) {
			hogar.qh228_5=hogar.setConver228_5(hogar.qh228_5);
		}
		if (hogar.qh228_6!=null) {
			hogar.qh228_6=hogar.setConver228_6(hogar.qh228_6);
		}
		if (hogar.qh228_7!=null) {
			hogar.qh228_7=hogar.setConver228_7(hogar.qh228_7);
		}
		if (hogar.qh228_8!=null) {
			hogar.qh228_8=hogar.setConver228_8(hogar.qh228_8);
		}
		if (hogar.qh228_9!=null) {
			hogar.qh228_9=hogar.setConver228_9(hogar.qh228_9);
		}
		if (hogar.qh228_10!=null) {
			hogar.qh228_10=hogar.setConver228_10(hogar.qh228_10);
		}
		
		 
		if (hogar.qh225!=null) {
			seleccionado = new N_LITERAL();
			seleccionado.n1 =""+hogar.qh225;
			seleccionado.n1_literal = hogar.qh225t;
			lblMarca.setText(hogar.qh225+" "+hogar.qh225t);
			if (hogar.qh225==96) {
				lblMarca.setText("OTROS");
				grid2.setVisibility(View.VISIBLE);
			}else{
				grid2.setVisibility(View.GONE);
			}
		}
		else{
			 lblMarca.setText("");
			 grid2.setVisibility(View.GONE);
		}
		if(hogar.qh223a!=null) {lblpre200_y_ini_f.setText(hogar.qh223a);}
		if(hogar.qh225a!=null) {lblpre200_y_fin_f.setText(hogar.qh225a);}
		if(hogar.qh226a!=null) {lblpre200_c_ini_f.setText(hogar.qh226a);}
		if(hogar.qh227c!=null) {lblpre200_c_fin_f.setText(hogar.qh227c);}
		
		if(lblpre200_y_ini_f.getText().toString().trim().length()!=0){btnqh200_y_fin.setEnabled(true);}else{btnqh200_y_fin.setEnabled(false);}
		if(lblpre200_c_ini_f.getText().toString().trim().length()!=0){btnqh200_c_fin.setEnabled(true);}else{btnqh200_c_fin.setEnabled(false);}
		
		entityToUI(hogar);
		
		inicio(); 
    } 

    public void abrirBuscar() {
		FragmentManager fm = HogarFragment_016.this.getFragmentManager();
		Seccion06_001Dialog aperturaDialog = Seccion06_001Dialog.newInstance(this);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    public void seleccionar(Entity escogido) {
    	seleccionado = null;
		seleccionado = (N_LITERAL) escogido;
		if (seleccionado == null) {
			return;
		}
		
		txtQH225.setText((seleccionado.n1));
		String marca="";
		marca=seleccionado.n1_literal.toString();
		
		
		if (seleccionado.n1=="96") {
			lblMarca.setText("OTROS");
			grid2.setVisibility(View.VISIBLE);
			txtQH225T.setText("");
		}else{
			lblMarca.setText(seleccionado.n1+" "+seleccionado.n1_literal);
			grid2.setVisibility(View.GONE);
		}
	
	}
    
    private void inicio() { 
    	ValidarsiesSupervisora();
    	onP224ChangeValue();
    	onP227ChangeValue();
    	txtCabecera.requestFocus();
    } 

    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQH227R.readOnly();
			txtQH227R2.readOnly();
			rgQH224.readOnly(); 
			rgQH225U.readOnly();
			rgQH227.readOnly();
			rgQH227A.readOnly();
			rgQH227B.readOnly();
			txtQH227B_O.readOnly();
			
			rgQH228_1.readOnly();
			rgQH228_2.readOnly();
			rgQH228_3.readOnly();
			rgQH228_4.readOnly();
			rgQH228_5.readOnly();
			rgQH228_6.readOnly();
			rgQH228_7.readOnly();
			rgQH228_8.readOnly();
			rgQH228_9.readOnly();
			rgQH228_10.readOnly();
			rgQHREC_06.readOnly();
			
			btnqh200_y_ini.setEnabled(false);
			btnqh200_c_fin.setEnabled(false);
			btnqh200_y_ini.setEnabled(false);
			btnqh200_c_fin.setEnabled(false);
			
			Util.cleanAndLockView(getActivity(), btnBuscar);
			txtMarca.readOnly();
			txtQH_OBS_SECCION06.setEnabled(false);
		}
	}
    
    
    public void onP224ChangeValue() {
    	grid2.setVisibility(View.GONE);  
		if (MyUtil.incluyeRango(5,6, rgQH224.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH225U, txtQH225,txtQH225T);	
			q4.setVisibility(View.GONE);    
			seleccionado=null;
			rgQH227.requestFocus();	
			
		}		
		else {	
			Util.lockView(getActivity(), false,rgQH225U,txtQH225,txtQH225T);			
			q4.setVisibility(View.VISIBLE);    
			onP225ChangeValue();
			rgQH225U.requestFocus();	
		}
		
	}

    public void onP225ChangeValue() {
    	grid2.setVisibility(View.GONE);  
		if (MyUtil.incluyeRango(3,3, rgQH225U.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),txtQH225,txtQH225T);	
			lblMarca.setText("");
			grid1.setVisibility(View.GONE);
			lbltituloMarca.setVisibility(View.GONE);
			seleccionado=null;
			rgQH227.requestFocus();	
		}		
		else {	
			Util.lockView(getActivity(), false,txtQH225,txtQH225T);
			grid1.setVisibility(View.VISIBLE);
			if(seleccionado!=null && seleccionado.n1.equals("96")){
			grid2.setVisibility(View.VISIBLE);
			}
			lbltituloMarca.setVisibility(View.VISIBLE);
		    txtQH225.requestFocus();	
		}
		
	}
    
    
    public void onP227ChangeValue() {
    	if(hogar.qh40!=null && hogar.qh47!=null){
    		if (MyUtil.incluyeRango(91,91,hogar.qh40) && MyUtil.incluyeRango(91,91,hogar.qh47)) {
        		rgQH227.lockButtons(false,4);
        		rgQH227.lockButtons(true,0,1,2,3,5, 6);
        		onP227RChangeValue();
        	}
        	if (!MyUtil.incluyeRango(91,91,hogar.qh40) && !MyUtil.incluyeRango(91,91,hogar.qh47)) {
        		rgQH227.lockButtons(false,0,1,2,3,5,6);
        		rgQH227.lockButtons(true,4);
        		onP227RChangeValue();
        	}
    	}
    }
    
    public void onP227RChangeValue() {
    	if (MyUtil.incluyeRango(1,2,rgQH227.getTagSelected("").toString()) ) {
 			Util.lockView(getActivity(), false,rgQH227A,rgQH227B); 
 			if (txtQH227R.getText().toString().trim().length()!=0 && txtQH227R2.getText().toString().trim().length()==0) {
 				Util.lockView(getActivity(), false,txtQH227R);
 				Util.cleanAndLockView(getActivity(),txtQH227R2);
 			}
 			if (txtQH227R2.getText().toString().trim().length()!=0 && txtQH227R.getText().toString().trim().length()==0) {
 				Util.lockView(getActivity(), false,txtQH227R2);
 				Util.cleanAndLockView(getActivity(),txtQH227R); 				
 			}
 			if (txtQH227R2.getText().toString().trim().length()==0 && txtQH227R.getText().toString().trim().length()==0) {
 				Util.lockView(getActivity(), false,txtQH227R);
 				Util.lockView(getActivity(), false,txtQH227R2);
 			}
 		
 			gridPreguntas227.setVisibility(View.VISIBLE);
 			q9.setVisibility(View.VISIBLE);
 			q10.setVisibility(View.VISIBLE);
 			txtQH227R.requestFocus(); 			
 		} 
    	if (MyUtil.incluyeRango(3,3,rgQH227.getTagSelected("").toString()) || MyUtil.incluyeRango(6,6,rgQH227.getTagSelected("").toString()) ) {
 			Util.cleanAndLockView(getActivity(),txtQH227R,txtQH227R2);
 			gridPreguntas227.setVisibility(View.GONE);
 			Util.lockView(getActivity(), false,rgQH227A,rgQH227B); 	
 			q9.setVisibility(View.VISIBLE);
 			q10.setVisibility(View.VISIBLE);
 			rgQH227A.requestFocus(); 			
 		}
    	if (MyUtil.incluyeRango(4,5,rgQH227.getTagSelected("").toString()) || MyUtil.incluyeRango(7,9,rgQH227.getTagSelected("").toString()) ) {
    		MyUtil.LiberarMemoria(); 	
			Util.cleanAndLockView(getActivity(),txtQH227R,txtQH227R2,rgQH227A,rgQH227B);
 			gridPreguntas227.setVisibility(View.GONE);
 			q9.setVisibility(View.GONE);
 			q10.setVisibility(View.GONE);
    	}	
 	}
    

    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(hogar);
		try {
			hogar.qh224 = hogar.getConvert224(hogar.qh224);
			if (hogar.qh227 != null) {
				hogar.qh227 = hogar.getConvert227(hogar.qh227);
			}
			if (hogar.qh225u != null) {
				hogar.qh225u = hogar.getConvert225u(hogar.qh225u);
			}
			if (seleccionado!=null) {
				hogar.qh225=Integer.parseInt(seleccionado.n1);
			    hogar.qh225t=seleccionado.n1_literal;
			    if(seleccionado.n1=="96"){
			    	hogar.qh225t=""+txtQH225T.getText(); 
			    }
			    
			}else{
				hogar.qh225u=998;
				hogar.qh225=null;
			    hogar.qh225t=null;
			}
			hogar.qh223a =lblpre200_y_ini_f.getText().toString();
			hogar.qh225a =lblpre200_y_fin_f.getText().toString();
			hogar.qh226a =lblpre200_c_ini_f.getText().toString();
			hogar.qh227c =lblpre200_c_fin_f.getText().toString();
			
			if(hogar.qh200_ref_s6==null ){
				hogar.qh200_ref_s6=Util.getFechaActualToString();
			}
			
			if (hogar.qh228_1!=null) {
				hogar.qh228_1=hogar.getConver228_1(hogar.qh228_1);
			}
			if (hogar.qh228_2!=null) {
				hogar.qh228_2=hogar.getConver228_2(hogar.qh228_2);
			}
			if (hogar.qh228_3!=null) {
				hogar.qh228_3=hogar.getConver228_3(hogar.qh228_3);
			}
			if (hogar.qh228_4!=null) {
				hogar.qh228_4=hogar.getConver228_4(hogar.qh228_4);
			}
			if (hogar.qh228_5!=null) {
				hogar.qh228_5=hogar.getConver228_5(hogar.qh228_5);
			}
			if (hogar.qh228_6!=null) {
				hogar.qh228_6=hogar.getConver228_6(hogar.qh228_6);
			}
			if (hogar.qh228_7!=null) {
				hogar.qh228_7=hogar.getConver228_7(hogar.qh228_7);
			}
			if (hogar.qh228_8!=null) {
				hogar.qh228_8=hogar.getConver228_8(hogar.qh228_8);
			}
			if (hogar.qh228_9!=null) {
				hogar.qh228_9=hogar.getConver228_9(hogar.qh228_9);
			}
			if (hogar.qh228_10!=null) {
				hogar.qh228_10=hogar.getConver228_10(hogar.qh228_10);
			}
			
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
} 
