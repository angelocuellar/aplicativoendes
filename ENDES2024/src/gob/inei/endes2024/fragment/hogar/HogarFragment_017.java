package gob.inei.endes2024.fragment.hogar;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.Seccion04_05Service;
//import gob.inei.endes2016.fragment.hogar.dialog.HogarDialog_017;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_017 extends FragmentForm  {

//	LinearLayout q0,q1,q2;
	
//	public List<Seccion01> detalles;
	Hogar bean;
//	private CuestionarioService cuestionarioService;
//	private HogarService hogarservice;
//	private Seccion04_05Service seccion04service;
//	private Seccion03Service seccion03service;
//	public TableComponent tcPersonas;
//	Seccion01ClickListener adapter;
	
//	public LabelComponent lblTitulo,lblpreguntaInd,lblpregunta1,lblpregunta2,lblpregunta3,lblpreguntapersonas,lblespacio,lblespacio1;	
//	public TextField txtCabecera; 
//	
//	SeccionCapitulo[] seccionesCargado, seccionesGrabado, seccionesCargadoHogar,seccionesGrabadoHogar,seccionesGrabadoHogarInformante;
//	SeccionCapitulo[] seccionesGrabadoHogarInicial,seccionesCargadoInformanteSalud,seccionesCargadoS,seccionesCargadoSaludS1_3,seccionesCargadoSaludS4_7;
	
//	public Seccion01 informantedesalud;
//	public GridComponent2 botones; 
//	private HogarService hogarService;
//	private Seccion01Service personaService;
	
//	public Seccion01 mefdeviolenciafamiliar;
//	public Seccion01Service seccion01;
//	public List<Seccion01> ListadoMef;

//	private enum ACTION {
//		ELIMINAR, MENSAJE
//	}

//	private ACTION action;

	public HogarFragment_017() {
	}

	public HogarFragment_017 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
//		rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		adapter = new Seccion01ClickListener();
//		tcPersonas.getListView().setOnItemClickListener(adapter);
//		enlazarCajas();
//		listening();
//		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH01", "QH02_1", "QH02_2","QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD","QH7MM", "QHINFO", "estado") };
//		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QH01", "QH02_1", "QH02_2", "QH02_3") };
//		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","PERSONA_INFORMANTE_ID") };
//		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","QHVIOLEN", "PERSONA_INFORMANTE_ID") };
//		seccionesGrabadoHogarInformante = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "PERSONA_INFORMANTE_ID") };
//		seccionesGrabadoHogarInicial =new  SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","PERSONA_INFORMANTE_ID")};
//		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
//		seccionesCargadoS = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS28", "QS29A", "QS29B")};
//		seccionesCargadoSaludS1_3 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS23","QS100","QS200","QS301")};
//		seccionesCargadoSaludS4_7 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS401","QS406","QS409","QS415","QS500","QS601A","QS603","QS700A","QS906")};
		return rootView;
	}

	@Override
	protected void buildFields() {
//		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.precenso_titulo).textSize(21).centrar().negrita();
//
//		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(1000, 1200).headerHeight(altoComponente + 10).dataColumHeight(60);
//		}
//		else{
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(1000, 1200).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
//		
//		tcPersonas.addHeader(R.string.seccion01_nro_orden, 0.25f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01nombres, 1f,TableComponent.ALIGN.LEFT);
//		tcPersonas.addHeader(R.string.seccion01parentesco,0.59f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01viveaqui, 0.29f,TableComponent.ALIGN.CENTER);
//		tcPersonas.addHeader(R.string.seccion01durmioaqui, 0.39f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01_sexo, 0.29f,TableComponent.ALIGN.CENTER);
//		tcPersonas.addHeader(R.string.seccion01_edad, 0.29f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01fecha_nacimiento, 0.39f,TableComponent.ALIGN.CENTER);
//		
//		lblespacio = new LabelComponent(this.getActivity()).size(altoComponente,30);
//		lblespacio1 = new LabelComponent(this.getActivity()).size(10,MATCH_PARENT);
//		lblpregunta1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhbebe).textSize(19);
//		lblpreguntaInd = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhInd).textSize(19).negrita();
//		lblpregunta1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhbebe).textSize(19);
//		lblpregunta2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhnofam).textSize(19);
//		lblpregunta3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhtempo).textSize(19);
//		Spanned textopreguntapersonas=Html.fromHtml("2. Dígame por favor los <b>nombres y apellidos</b> de las personas que <b>habitualmente</b> viven en su hogar y de los visitantes que pasaron la noche anterior aquí, empezando por el Jefe del Hogar<br>");
//		lblpreguntapersonas = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).alinearIzquierda().textSize(19);
//		lblpreguntapersonas.setText(textopreguntapersonas);
	}

	
	@Override
	protected View createUI() {
		buildFields();
//		q0 = createQuestionSection(txtCabecera,lblTitulo);
//		q1 = createQuestionSection(R.string.precenso_inf,Gravity.CENTER);
//		q2 = createQuestionSection(tcPersonas.getTableView());
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
		return contenedor;
	}
	
//	public boolean grabar2() {
//		
//		uiToEntity(bean);
//	
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag = false;
//		SQLiteDatabase dbTX = getService().startTX();
//		try {
//			flag = getService().saveOrUpdate(bean, dbTX, seccionesGrabadoHogar);
//			if (!flag) {
//				throw new Exception("Ocurrió un problema al grabar los datos del hogar.");
//			}
//			getService().commitTX(dbTX);
//		} catch (Exception e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//			return false;
//		} finally {
//			getService().endTX(dbTX);
//		}
//		
//		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
//		App.getInstance().getHogar().qhbebe=bean.qhbebe;
//		return flag;
//	}
	

	
	@Override
	public boolean grabar() {
		/*variable para blanquear Seccion 04 */
//		App.FRAGMENTO_QHSECCION04=false;
		return true;
	}

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		return true;
	}

	@Override
	public void cargarDatos() {
//		if(App.getInstance().getHogar()!=null){
//			bean = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
//			detalles = getCuestionarioService().GetSeccion01ListByMarcoyHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
//			if (bean == null) {
//				bean = new Hogar();
//				bean.id = App.getInstance().getMarco().id;
//				bean.hogar_id = App.getInstance().getHogar().hogar_id;
//				detalles = new ArrayList<Seccion01>();
//			}
//			entityToUI(bean);			
//			cargarTabla();
//			inicio();
//			App.getInstance().setEditarPersonaSeccion01(null);			
//		}

	
	}
	
	public void EditarDetallePersona(Seccion01 tmp) {
//		FragmentManager fm = HogarFragment_017.this.getFragmentManager();
//		HogarDialog_017 aperturaDialog = HogarDialog_017.newInstance(HogarFragment_017.this, tmp);
//		if(aperturaDialog.getShowsDialog()){
//			aperturaDialog.show(fm, "aperturaDialog");
//		}
	}

//	public void refrescarPersonas(Seccion01 personas) {
//		if (detalles.contains(personas)) {
//			cargarTabla();
//			return;
//		} else {
//			detalles.add(personas);
//			cargarTabla();
//		}
//	}

	public void cargarTabla() {
//		detalles = getCuestionarioService().getSeccion01ListbyMarcoyHogar_precenso(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
//		tcPersonas.setData(detalles, "qh01", "getNombre", "getParentesco","getViveAqui", "getDurmioAqui", "getSexo", "qh07", "getFecha");
//		tcPersonas.setBorder("estado");
//		registerForContextMenu(tcPersonas.getListView());
	}
	
	private void inicio() {
//		BlanquearPantallaCuandoNoDebeMostrar();
//		txtCabecera.requestFocus();		
	}
	
//	private void BlanquearPantallaCuandoNoDebeMostrar() {
//		if (App.FRAGMENTO_POSTCENSAL==false) {
//			q0.setVisibility(View.GONE);
//			q1.setVisibility(View.GONE);
//			q2.setVisibility(View.GONE);
//		}
//		else{
//			q0.setVisibility(View.VISIBLE);
//			q1.setVisibility(View.VISIBLE);
//			q2.setVisibility(View.VISIBLE);
//			App.FRAGMENTO_POSTCENSAL=false;
//		}
//	}

	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
//		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//		String nombre="";
//		Integer posicion = 0;
//		posicion = detalles.get(info.position).persona_id!=null?detalles.get(info.position).persona_id:0;
//		nombre=detalles.get(info.position).qh02_1!=null?detalles.get(info.position).qh02_1:"";
			
//		if (v.equals(tcPersonas.getListView())) {
//			menu.setHeaderTitle("Opciones del Residente");
//			menu.add(1, 0, 1, "Editar Residente: ("+posicion+") "+nombre);
//	
//			if (posicion > 1) {
//				posicion = posicion - 1;
//			}
//			
//			Seccion01 seleccion = (Seccion01) info.targetView.getTag();
//			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
//				menu.getItem(0).setVisible(false);
//			}
//			else{
//				if (!getPersonaService().RegistroDePersonaCompletada(detalles.get(info.position).id,detalles.get(info.position).hogar_id, posicion)&& detalles.get(info.position).persona_id != App.JEFE) {
//					menu.getItem(0).setVisible(false);
//				}
//			}
//		}
	}
	
//	private class Seccion01ClickListener implements OnItemClickListener {
//		public Seccion01ClickListener() {
//		}
//
//		@Override
//		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//			Seccion01 c = (Seccion01) detalles.get(arg2);
//			EditarDetallePersona(c);
//		}
//	}
	

//	@Override
//	public boolean onContextItemSelected(MenuItem item) {
//		if (!getUserVisibleHint())
//			return false;
//		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//		Seccion01 seleccion = (Seccion01) info.targetView.getTag();
//		if (item.getGroupId() == 1) {
//			switch (item.getItemId()) {
//			case 0:
//				EditarDetallePersona((Seccion01) detalles.get(info.position));
//				break;
//			}
//		}
//		return super.onContextItemSelected(item);
//	}
	
		
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(bean);
//		boolean flag=false;
//		try {
//			flag = getService().saveOrUpdate(bean, seccionesGrabadoHogar);
//			if (!flag) {
//				throw new Exception("Ocurrió un problema al grabar los datos del hogar.");
//			}
//		} catch (Exception e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
//			return App.NODEFINIDO;
//		} 
		return App.HOGAR;
	}
	
		
//	private HogarService getService() {
//		if (hogarService == null) {
//			hogarService = HogarService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//
//	public Seccion01Service getPersonaService() {
//		if (personaService == null) {
//			personaService = Seccion01Service.getInstance(getActivity());
//		}
//		return personaService;
//	}
//	
//	public HogarService getHogarService(){
//		if(hogarservice==null){
//			hogarservice=HogarService.getInstance(getActivity());
//		}
//		return hogarservice;
//	}
//	
//	public Seccion04_05Service getSeccion04_05Service(){
//		if(seccion04service==null){
//			seccion04service=Seccion04_05Service.getInstance(getActivity());
//		}
//		return seccion04service;
//	}
//	
//	public Seccion03Service getSeccion03Service(){
//		if(seccion03service==null){
//			seccion03service=Seccion03Service.getInstance(getActivity());
//		}
//		return seccion03service;
//	}
}
