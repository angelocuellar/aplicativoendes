package gob.inei.endes2024.fragment.hogar; 
import java.sql.SQLException; 

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation; 
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.IntegerField; 
import gob.inei.dnce.components.FragmentForm; 
import gob.inei.dnce.components.LabelComponent; 
import gob.inei.dnce.components.MasterActivity; 
import gob.inei.dnce.components.TextField; 
import gob.inei.dnce.components.ToastMessage; 
import gob.inei.dnce.util.Util; 
import android.os.Bundle; 
import android.view.LayoutInflater; 
import android.view.View; 
import android.view.ViewGroup; 
import android.widget.LinearLayout; 
import android.widget.ScrollView; 
 

public class HogarFragment_015 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public TextField txtQH215; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQH215_C; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQH216; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQH216_C; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtOBSERVACIONES; 
	Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
// 
	public HogarFragment_015() {} 
	public HogarFragment_015 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
//		rango(getActivity(), txtQH215_C, 00000001, 99999998, null, 99999999); 
//		rango(getActivity(), txtQH216_C, 00000001, 99999998, null, 99999999); 
		enlazarCajas(); 
		listening(); 
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH215","QH215_C","QH216","QH216_C","Observaciones","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH215","QH215_C","QH216","QH216_C","Observaciones","ID","HOGAR_ID")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH215","QH215_C","QH216","QH216_C","Observaciones")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();
		txtQH215 = new TextField(this.getActivity()).size(altoComponente, 50).maxLength(8);
		txtQH215_C=new IntegerField(this.getActivity()).size(altoComponente, 160).maxLength(8); 
		txtQH216 = new TextField(this.getActivity()).size(altoComponente,50).maxLength(8);
		txtQH216_C=new IntegerField(this.getActivity()).size(altoComponente, 160).maxLength(8); 
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(R.string.hogarqh215,txtQH215); 
		q2 = createQuestionSection(R.string.hogarqh215_c,txtQH215_C); 
		q3 = createQuestionSection(R.string.hogarqh216,txtQH216); 
		q4 = createQuestionSection(R.string.hogarqh216_c,txtQH216_C); 
//		q5 = createQuestionSection(R.string.hogarobservaciones,txtObservaciones); 
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		/*Aca agregamos las preguntas a la pantalla*/ 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		//form.addView(q5); 
		/*Aca agregamos las preguntas a la pantalla*/ 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh215)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH215"); 
			view = txtQH215; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh215_c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH215_C"); 
			view = txtQH215_C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh216)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH216"); 
			view = txtQH216; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh216_c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QH216_C"); 
			view = txtQH216_C; 
			error = true; 
			return false; 
		} 
//		if (Util.esVacio(hogar.observaciones)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta Observaciones"); 
//			view = txtObservaciones; 
//			error = true; 
//			return false; 
//		} 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
		hogar = getCuestionarioService().getHogar(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id;  
	    } 
		entityToUI(hogar); 
		inicio(); 
    } 
    private void inicio() { 
    	txtCabecera.requestFocus();
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.HOGAR;
	}
} 
