package gob.inei.endes2024.fragment.hogar; 
import gob.inei.dnce.annotations.FieldAnnotation;

import java.sql.SQLException; 
import java.util.ArrayList;
import java.util.List;

import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.dialog.HogarDialog_013;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.text.format.Time;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.AdapterView.OnItemClickListener;

public class HogarFragment_014 extends FragmentForm implements Respondible { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH106; 
	public Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private Seccion01Service seccion01service; 
	private Seccion03Service seccion03service;
	private VisitaService visitaservice;
	private LabelComponent lblTitulo,lblWM,lblcunamas,lblcunamas_ind,lblHora, lblMinutos, lblHor, lblMin, lblP106, lblP107, lblP109, lblP110, lblP111; 
	private LabelComponent lblhora,lblhoras;
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoSeccion03;
	SeccionCapitulo[] seccionesCargadoSeccion04;
	SeccionCapitulo[] secciongrabadoVisita;
	public TableComponent tcWawaMas;
	public ButtonComponent btnWawaMas,btnQaliwarma,btnAgregarTeVas;
	private List<Hogar> detalles;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public Seccion01 entrevistado;
	public Seccion01 elegido;
	public Seccion03 elegidos03;
	public List<Seccion03> beneficiadosWawaMas;
	public List<Seccion03> beneficiadosQALIWARMA;
	public TableComponent tcQaliWarma;
	private Seccion01Service seccion01;
	private Seccion03Service seccion03;
	private enum ACTION{ELIMINAR,MENSAJE}
	private ACTION action;
	private DialogComponent dialog;
	private Visita finalizavisita;
	public Time hora_fin;

	public LinearLayout q0,q1,q2,q3,q4,q5;
	
	private boolean wawaMas=false,QaliWarma=false;
	
	public int accion=0, posi_qali=0;
	public boolean completar=true;
	
	private GridComponent2 gridHora;
	
	public ButtonComponent btnHoraInicio;
	
	Seccion01ClickListener adapter;
	
	public HogarFragment_014() {}
	
	public HogarFragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
    @Override 
    public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		adapter = new Seccion01ClickListener();
		tcQaliWarma.getListView().setOnItemClickListener(adapter);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH106","ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH110H","QH110M")};
		seccionesCargadoSeccion03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","QH02_1","ID","HOGAR_ID","PREGUNTA_ID","ESTADO3")}; 
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID","PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","QH02_1","ID","HOGAR_ID","PREGUNTA_ID","QH109_1R","ESTADO3")};
		secciongrabadoVisita = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"NRO_VISITA","QHVHORA_FIN","QHVMIN_FIN","QHVRESUL")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH106","QH110H","QH110M")};
		cargarDatos();
		return rootView; 
    } 
  
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcWawaMas.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(0, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(0, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
		if (v.equals(tcQaliWarma.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(1, 0, 1, "Editar Beneficiario");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
	}
  
    @Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosWawaMas.get(info.position));
						break;
				case 1:	 
						accion = 0;						
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadosWawaMas.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadosQALIWARMA.get(info.position));						
						break;
			}
		}
		return super.onContextItemSelected(item);
	}
    
    @Override 
    protected void buildFields() { 
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar_3_b).textSize(21).centrar().negrita();
		lblWM = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		lblcunamas = new LabelComponent(this.getActivity()).size(altoComponente + 10, 250).text(R.string.hogarqh106_ind_1).textSize(16).alinearIzquierda().negrita();
		lblcunamas_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh106_ind_2).textSize(16).alinearIzquierda();
		
		lblP106 = new LabelComponent(this.getActivity()).text(R.string.hogarqh106).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP107 = new LabelComponent(this.getActivity()).text(R.string.hogarqh107).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP109 = new LabelComponent(this.getActivity()).text(R.string.hogarqh109).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP110 = new LabelComponent(this.getActivity()).text(R.string.hogarqh110).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP111 = new LabelComponent(this.getActivity()).text(R.string.hogarqh111).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		
		rgQH106=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh106_1,R.string.hogarqh106_2,R.string.hogarqh106_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH106");
		tcWawaMas = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
		tcWawaMas.addHeader(R.string.m_orden_b18_hogar, 2f,TableComponent.ALIGN.CENTER);
		tcWawaMas.addHeader(R.string.m_nombre_b18_hogar, 3.5f,TableComponent.ALIGN.LEFT);
		tcWawaMas.addHeader(R.string.m_anio_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tcWawaMas.addHeader(R.string.m_mes_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		btnWawaMas = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 55);
		btnWawaMas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
					agregarWawaMas();
			}
		});	
		
		tcQaliWarma = new TableComponent(getActivity(), this,App.ESTILO).size(230, 800).headerHeight(45).dataColumHeight(40).headerTextSize(15);
		tcQaliWarma.addHeader(R.string.m_orden_b18_hogar, 2f,TableComponent.ALIGN.CENTER);
		tcQaliWarma.addHeader(R.string.m_nombre_b18_hogar, 3.5f,TableComponent.ALIGN.LEFT);
		tcQaliWarma.addHeader(R.string.m_anio_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tcQaliWarma.addHeader(R.string.m_mes_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tcQaliWarma.addHeader(R.string.r_almuerzo, 1.5f,TableComponent.ALIGN.CENTER);
		
		btnQaliwarma = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnCompletar).size(200, 55);
		btnQaliwarma.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				completar();
			}
		});
		
		
		btnAgregarTeVas = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 55);
		btnAgregarTeVas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				agregarNada();
			}
		});	
		
		
		lblhora = new LabelComponent(getActivity()).textSize(17).size(altoComponente, 210).text(R.string.cap08_09qs802bh).centrar();
		btnHoraInicio = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.hogarqh110_terminar).size(200, 55);
		lblhoras = new LabelComponent(getActivity()).textSize(20).size(altoComponente, 120).text("").centrar();
		
		gridHora = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gridHora.addComponent(btnHoraInicio);
		gridHora.addComponent(lblhora);		
		gridHora.addComponent(lblhoras);
		
		btnHoraInicio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
					Calendar calendario = new GregorianCalendar();
					Integer hora= calendario.get(Calendar.HOUR_OF_DAY);
					Integer minute= calendario.get(Calendar.MINUTE);
					String muestrahora =hora.toString().length()>1?hora.toString():0+""+hora;
					String muestraminuto=minute.toString().length()>1?minute.toString():0+""+minute;
					lblhoras.setText(muestrahora+":"+muestraminuto);
				}
			});
//		textoNegrita();
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		LinearLayout q1_1 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblcunamas);
		LinearLayout q1_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblcunamas_ind);
		q1 = createQuestionSection(lblP106,q1_1,q1_2,rgQH106);
		LinearLayout q2_1 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblWM,btnWawaMas);
		q2 = createQuestionSection(lblP107, q2_1,tcWawaMas.getTableView());
		LinearLayout q3_1 = createQuestionSection(0, Gravity.CENTER, LinearLayout.HORIZONTAL,btnQaliwarma);
		q3 = createQuestionSection(lblP109,q3_1,tcQaliWarma.getTableView());
		LinearLayout q4 = createQuestionSection(lblP110,gridHora.component());
		LinearLayout q5 = createQuestionSection(lblP111);
		
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		return contenedor; 
    }   
    
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar);
		hogar.qh110h = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		hogar.qh110m = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;

		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh106!=null) {
				hogar.qh106=hogar.getConver80a(hogar.qh106);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			}
			if(getCuestionarioService().getCuestionarioDelHogarCompletado(hogar.id,hogar.hogar_id))
			{
				if(!getVisitaService().saveOrUpdate(RegistrarVisitaCompletada(), "NRO_VISITA","QHVHORA_FIN","QHVMIN_FIN","QHVRESUL"))
				{ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados visita.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
				}
			}
			
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh106) && wawaMas) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.106"); 
			view = rgQH106; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh106, 1,1) && wawaMas) {
			if (beneficiadosWawaMas.size()<1) { 
				mensaje = "La pregunta P.106 debe tener algun Beneficiario"; 
				view = btnWawaMas;
				error = true; 
				return false; 
			}
		}
		
		if(!editadosTodosQaliWarma() && QaliWarma){
			mensaje = "Falta terminar de completar la pregunta P.109";
			view = tcQaliWarma;
			error = true;
			return false;
		}
		
		if (Util.esVacio(hogar.qh110h)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.110H"); 
			view = btnHoraInicio; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh110m)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.110M"); 
			view = btnHoraInicio; 
			error = true; 
			return false; 
		}
		 
		return true; 
    } 
    public Visita RegistrarVisitaCompletada()
    {
    	Visita visita= new Visita();
    	visita.id=App.getInstance().getMarco().id;
    	visita.hogar_id=App.getInstance().getHogar().hogar_id;
    	visita.nro_visita=App.getInstance().getVisita().nro_visita;
    	Calendar calendario = new GregorianCalendar();
    	visita.qhvhora_fin = hogar.qh110h==null?null:hogar.qh110h;
    	visita.qhvmin_fin = hogar.qh110m==null?null:hogar.qh110m;
    	visita.qhvresul=App.HOGAR_RESULTADO_COMPLETADO;
    	return visita;
    }
    @Override 
    public void cargarDatos() {    	
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh106!=null) {
			hogar.qh106=hogar.setConver80a(hogar.qh106);
		}
		/*if(hogar.qh110h!=null && hogar.qh110m!=null)
    	{
    		lblhoras.setText(hogar.qh110h.toString()+":"+hogar.qh110m.toString());
    	}*/
		if(hogar.qh110h!=null && hogar.qh110m!=null){
            String hora_  = hogar.qh110h.toString().length()==1?"0"+hogar.qh110h.toString():hogar.qh110h.toString();
            String minuto_= hogar.qh110m.toString().length()==1?"0"+hogar.qh110m.toString():hogar.qh110m.toString();
            lblhoras.setText(hora_+":"+minuto_);
		}
		entityToUI(hogar); 
		PosiblesQaliwarma();
		cargarTabla();
		eliminarBeneficiados();
		inicio(); 
    } 
    
    public void cargarTabla(){
//    	beneficiadosWawaMas = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.WAWAMAS,seccionesCargadoSeccion03);
//    	tcWawaMas.setData(beneficiadosWawaMas,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//    	registerForContextMenu(tcWawaMas.getListView());
    	beneficiadosWawaMas = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.WAWAMAS,seccionesCargadoSeccion03);
    	tcWawaMas.setData(beneficiadosWawaMas,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
    	if(beneficiadosWawaMas!=null&& beneficiadosWawaMas.size()>0){
	    	for (int row = 0; row < beneficiadosWawaMas.size(); row++) {
				if (obtenerEstado(beneficiadosWawaMas.get(row)) == 1) {
					// borde de color azul
					tcWawaMas.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosWawaMas.get(row)) == 2) {
					// borde de color rojo
					tcWawaMas.setBorderRow(row, true, R.color.red);
				} else {
					tcWawaMas.setBorderRow(row, false);
				}
			}
    	}
    	tcWawaMas.reloadData();
	   	registerForContextMenu(tcWawaMas.getListView());
    	
    	
    	
//    	beneficiadosQALIWARMA = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.QALIWARMA,seccionesCargadoSeccion04);
//    	tcQaliWarma.setData(beneficiadosQALIWARMA, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m","getQh109_1r");
//	   	registerForContextMenu(tcQaliWarma.getListView());
	   	beneficiadosQALIWARMA = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.QALIWARMA,seccionesCargadoSeccion04);
    	tcQaliWarma.setData(beneficiadosQALIWARMA, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m","getQh109_1r");
    	if(beneficiadosQALIWARMA!=null&& beneficiadosQALIWARMA.size()>0){
	    	for (int row = 0; row < beneficiadosQALIWARMA.size(); row++) {
				if (obtenerEstado(beneficiadosQALIWARMA.get(row)) == 1) {
					// borde de color azul
					tcQaliWarma.setBorderRow(row, true);
				} else if (obtenerEstado(beneficiadosQALIWARMA.get(row)) == 2) {
					// borde de color rojo
					tcQaliWarma.setBorderRow(row, true, R.color.red);
				} else {
					tcQaliWarma.setBorderRow(row, false);
				}
			}
    	}
    	tcQaliWarma.reloadData();
	   	registerForContextMenu(tcQaliWarma.getListView());
	   	
    }
    
    public void refrescarBeca18(Seccion03 beneficiariosWawaWasi) {		
        if(!Util.esDiferente(beneficiariosWawaWasi.pregunta_id,App.WAWAMAS)){
    	if (beneficiadosWawaMas.contains(beneficiariosWawaWasi)) {
                cargarTabla();
                return;
        } else {
        	beneficiadosWawaMas.add(beneficiariosWawaWasi);
                cargarTabla();
        }
        }
        if(!Util.esDiferente(beneficiariosWawaWasi.pregunta_id, App.QALIWARMA))
        {if (beneficiadosQALIWARMA.contains(beneficiariosWawaWasi)) {
            cargarTabla();
            return;
        } else {
        	beneficiadosQALIWARMA.add(beneficiariosWawaWasi);
            cargarTabla();
        }
        	
        }        
}

    private void inicio() { 
    	mostrarPreguntas();
    	salto_rgQH106();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    	renombrartexto();
    } 
    
    private int obtenerEstado(Seccion03 detalle) {
 		if (!Util.esDiferente(detalle.estado3, 0)) {
 			return 1 ;
 		} else if (!Util.esDiferente(detalle.estado3,1)) {
 			return 2;
 		}
 		return 0;
 	}
    
    
    private void renombrartexto(){
//    	SELECCIONE LA FILA Y CONTINUE CON LAS PREGUNTAS PARA IDENTIFICAR A LOS BENEFICIARIOS DEL PROGRAMA NACIONAL DE ALIMENTACIÓN ESCOLAR QALI WARMA
//    	lblpregunta478e1.setText(Html.fromHtml("478E1. <b>MUESTRE LA CARTILLA 1.</b> <br>De estas figuras ¿<b>cuál o cuáles</b> son las que "+lblpregunta478e1.getText()+" <b>ya realiza</b>?"));
    	lblP109.setText("");
    	lblP109.setText(Html.fromHtml("<b>109. SELECCIONE LA FILA Y CONTINUE CON LAS PREGUNTAS PARA IDENTIFICAR A LOS BENEFICIARIOS DEL</b> PROGRAMA NACIONAL DE ALIMENTACIÓN ESCOLAR QALI WARMA "));
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
    
    public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
    
    public VisitaService getVisitaService()
    {
    	if(visitaservice==null)
    	{
    		visitaservice = VisitaService.getInstance(getActivity());
    	}
    	return visitaservice;
    }
    
    public void salto_rgQH106() {
		int valor = Integer.parseInt(rgQH106.getTagSelected("0").toString());
		
		
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnWawaMas,tcWawaMas);
			
			if(beneficiadosWawaMas.size()>0 && valor!=0){
				accion = App.WAWAMAS;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Wawa Wasi/Cuna Más!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			if(QaliWarma){
				tcQaliWarma.requestFocus();
			}
		} else { 
            Util.lockView(getActivity(), false, btnWawaMas,tcWawaMas);
            tcWawaMas.requestFocus();
		} 
		PosiblesWawaMas();
	}
    
    protected void agregarWawaMas(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
    	Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.WAWAMAS;
		bean.persona_id = hogar.persona_informante_id;
		
		abrirDetalle(bean);
    }
    protected void agregarNada(){
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.QALIWARMA;
		bean.persona_id = hogar.persona_informante_id;
		
		abrirDetalle(bean);
    }
    
    public void abrirDetalle(Seccion03 tmp) {    	
		FragmentManager fm = HogarFragment_014.this.getFragmentManager();
		HogarDialog_013 aperturaDialog = HogarDialog_013.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public void EditarSeccion03(Seccion03 tmp) {	
		FragmentManager fm = HogarFragment_014.this.getFragmentManager();
		HogarDialog_013 aperturaDialog = HogarDialog_013.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
   
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
    
    private Seccion03Service getServiceSeccion03() {
        if (seccion03 == null) {
        	seccion03 = Seccion03Service.getInstance(getActivity());
        }
        return seccion03;
	}    
  
   @Override
    public void onCancel() {
	   if(accion==App.WAWAMAS){
      		
      		rgQH106.setTagSelected(1);
      }
    }

   @Override 
    public void onAccept() {
        if (action == ACTION.MENSAJE) {
                return;
        }
        if (dialog == null) {
                return;
        }
        
        if(accion==0){
	        Seccion03 bean = (Seccion03) dialog.get("seleccion");
	        
	        try {//bean.persona_id=1;
	        if(!Util.esDiferente(bean.pregunta_id, App.WAWAMAS))
	        {    beneficiadosWawaMas.remove(bean);}
	        if(!Util.esDiferente(bean.pregunta_id, App.QALIWARMA))
	        {  beneficiadosQALIWARMA.remove(bean);}
	        	boolean borrado=false;
	                borrado =getSeccion03Service().BorrarPersonaPregunta(bean);
	                if (!borrado) {
	                        throw new SQLException("Persona no pudo ser borrada.");
	                }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        if(accion==App.WAWAMAS){
        	
        	
        	try {
	            while(beneficiadosWawaMas.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadosWawaMas.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadosWawaMas.remove(beneficiadosWawaMas.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        cargarTabla();
		salto_rgQH106();
    }
	
	public void PosiblesWawaMas(){
		List<Seccion01> PosibleWawaMas = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAWAWAMAS,App.EDADMAXIMAWAWAMAS,App.WAWAMAS);
	   	if(PosibleWawaMas.size()<=0){
	   		rgQH106.lockButtons(true, 0);
		}else{
			Util.lockView(getActivity(), false, rgQH106);
		}
    }
	
	public void PosiblesQaliwarma(){
		Seccion03 bean;
		boolean flag=true;
		SQLiteDatabase dbTX;
		List<Seccion01> PosibleQaliwarma = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAQALIWARMA,App.EDADMAXIMAQALIWARMA,App.QALIWARMA);
		SeccionCapitulo[] seccionesGrabadoC = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN","PREGUNTA_ID")};
		
		for(int i=0;i<PosibleQaliwarma.size();i++){
			bean = new Seccion03();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id = App.getInstance().getHogar().hogar_id;
			bean.persona_id =hogar.persona_informante_id; 
			bean.persona_id_orden = PosibleQaliwarma.get(i).persona_id;
			bean.pregunta_id = App.QALIWARMA;
			
			
			dbTX = getCuestionarioService().startTX();
			try {
				flag = getServiceSeccion03().saveOrUpdate(bean, dbTX,seccionesGrabadoC);
				if (!flag) {
//					throw new Exception("Ocurrió un problema al grabar beneficiario 3");
				}
				getServiceSeccion03().commitTX(dbTX);
			} catch (SQLException e) {
				ToastMessage.msgBox(this.getActivity(), e.getMessage(),
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			} catch (Exception e) {
				ToastMessage.msgBox(this.getActivity(), e.getMessage(),
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			} finally {
				getCuestionarioService().endTX(dbTX);
			}
		}
		
    }
	
	public boolean editadosTodosQaliWarma(){
		boolean todos=true;
		Seccion03 bean;
		for(int i=0;i<beneficiadosQALIWARMA.size();i++){
			bean = new Seccion03();
			bean.qhs3_1a = beneficiadosQALIWARMA.get(i).qhs3_1a;
			bean.qhs3_1m = beneficiadosQALIWARMA.get(i).qhs3_1m;
			bean.qh109_1r = beneficiadosQALIWARMA.get(i).qh109_1r;
			if(Util.esVacio(bean.qh109_1r)){
				todos = false;
				i = beneficiadosQALIWARMA.size();
			}
		}		
		return todos;
	}
	
	public void PosiblesBeneficiarios(){
 		List<Seccion01> PosibleWawaMas = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAWAWAMAS,App.EDADMAXIMAWAWAMAS,App.WAWAMAS);
 	   	if(PosibleWawaMas.size()>0){
 	   		wawaMas = true;
 		}else{wawaMas=false;}
 	   	
 	   List<Seccion01> PosibleQaliWarma = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAQALIWARMA,App.EDADMAXIMAQALIWARMA,App.QALIWARMA);
	   	if(PosibleQaliWarma.size()>0){
	   		QaliWarma = true;
		}else{QaliWarma=false;}
    }
	
	public void mostrarPreguntas(){
    	int val106=0,val93=0;
    	PosiblesBeneficiarios();
    	if(wawaMas){
    		q1.setVisibility(View.VISIBLE);
    		val106 = Integer.parseInt(rgQH106.getTagSelected("0").toString());
    		
    		if (Util.esDiferente(val106, 1,1)) {
    			q2.setVisibility(View.GONE);
    		}else{
    			q2.setVisibility(View.VISIBLE);
    		}    		
    	}else{
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    	
    	if(QaliWarma){
			q3.setVisibility(View.VISIBLE);
    	}else{ 
    		q3.setVisibility(View.GONE);
    	}
    }
	
	public void imprimir_hora(){
		hora_fin = new Time(Time.getCurrentTimezone());
		hora_fin.setToNow();
		hogar.qh110h= ""+hora_fin.hour;
		hogar.qh110m= ""+hora_fin.minute;
		
		if(hora_fin.hour<10){
			lblHor.setText(" 0"+hora_fin.hour);
		}else{
			lblHor.setText(" "+hora_fin.hour);
		}
		if(hora_fin.minute<10){
			lblMin.setText(" 0"+hora_fin.minute);
		}else{
			lblMin.setText(" "+hora_fin.minute);
		}
	}
	
	public void eliminarBeneficiados(){
		   boolean eliminoWM=false,eliminoQW=false;
		   
		   for(int i=0;i<beneficiadosWawaMas.size();i++){
			   Seccion01 band = new Seccion01();
			   band = getSeccion01Service().getPersonaEliminarSeccion3(beneficiadosWawaMas.get(i).id, beneficiadosWawaMas.get(i).hogar_id, beneficiadosWawaMas.get(i).persona_id_orden);
			   		   
			   //if(!MyUtil.incluyeRango(App.EDADMINIMAWAWAMAS, App.EDADMAXIMAWAWAMAS, band.qh07) || !MyUtil.incluyeRango(1, 1, band.qh04)){
			   if(!MyUtil.incluyeRango(App.EDADMINIMAWAWAMAS, App.EDADMAXIMAWAWAMAS, band.qh07)){
				   eliminoWM = true;
				   try {
			        	boolean borrado=false;
			            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosWawaMas.get(i));
			            if (!borrado) {
			                    throw new SQLException("Persona no pudo ser borrada.");
			            }
			        } catch (SQLException e) {                      
			                e.printStackTrace();
			                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
			                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			        } 
			   }
		   }
		   
		   for(int i=0;i<beneficiadosQALIWARMA.size();i++){
			   Seccion01 band = new Seccion01();
			   band = getSeccion01Service().getPersonaEliminarSeccion3(beneficiadosQALIWARMA.get(i).id, beneficiadosQALIWARMA.get(i).hogar_id, beneficiadosQALIWARMA.get(i).persona_id_orden);
			   		   
			   if(!MyUtil.incluyeRango(App.EDADMINIMAQALIWARMA, App.EDADMAXIMAQALIWARMA, band.qh07) || !MyUtil.incluyeRango(0, 1, band.qh18n) || !MyUtil.incluyeRango(1, 1, band.qh21a)){
				   eliminoQW = true;
				   try {
			        	boolean borrado=false;
			            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadosQALIWARMA.get(i));
			            if (!borrado) {
			                    throw new SQLException("Persona no pudo ser borrada.");
			            }
			        } catch (SQLException e) {                      
			                e.printStackTrace();
			                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
			                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			        } 
			   }
		   }
		   
		   if(eliminoWM || eliminoQW){
			   cargarTabla();
		   }
	   }
	
	public void completar(){
		completar = true;
		if(posi_qali<beneficiadosQALIWARMA.size()){
			EditarSeccion03((Seccion03) beneficiadosQALIWARMA.get(posi_qali));
		}else{
			btnQaliwarma.setVisibility(View.INVISIBLE);
		}
	}
	
//	public void textoNegrita(){
//  	   Spanned texto106 = Html.fromHtml(lblP106.getText()+"<b> Wawa Wasi / Cuna m&aacute;s?</b>");    	
//  	   lblP106.setText(texto106);
//	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH106.readOnly();
			tcWawaMas.setEnabled(false);
			tcQaliWarma.setEnabled(false);
			btnWawaMas.setEnabled(false);
			btnQaliwarma.setEnabled(false);
			btnHoraInicio.setEnabled(false);
		}
	}
	public class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			EditarSeccion03((Seccion03) beneficiadosQALIWARMA.get(arg2));
		}
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		hogar.qh110h = lblhoras.getText().toString().length()>0?lblhoras.getText().toString().substring(0,2):null;
		hogar.qh110m = lblhoras.getText().toString().length()>3?lblhoras.getText().toString().substring(3,5):null;
		try {
			if (hogar.qh106!=null) {
				hogar.qh106=hogar.getConver80a(hogar.qh106);
			}	
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
