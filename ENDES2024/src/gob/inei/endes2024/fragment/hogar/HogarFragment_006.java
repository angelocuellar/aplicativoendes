package gob.inei.endes2024.fragment.hogar; 
import java.sql.SQLException; 

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation; 
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField; 
import gob.inei.dnce.components.FragmentForm; 
import gob.inei.dnce.components.LabelComponent; 
import gob.inei.dnce.components.MasterActivity; 
import gob.inei.dnce.components.RadioGroupOtherField; 
import gob.inei.dnce.components.TextField; 
import gob.inei.dnce.components.ToastMessage; 
import gob.inei.dnce.util.Util; 
import android.content.Context;
import android.os.Bundle; 
import android.util.Log;
import android.view.Gravity; 
import android.view.LayoutInflater; 
import android.view.View; 
import android.view.ViewGroup; 
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout; 
import android.widget.ScrollView; 

public class HogarFragment_006 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH66; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQH68; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH70; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQH70_O; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQH71; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQH72; 
	Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta66,lblpregunta68,lblpregunta70,lblpregunta71,lblpregunta72,lblpregunta71_ind,lblpregunta72_ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 
	
	public GridComponent2 gridpreg71,gridpreg72;
//	private GridComponent gridpreg71,gridpreg72;

	public HogarFragment_006() {} 
	public HogarFragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQH71, 1, 30); 
		rango(getActivity(), txtQH72, 1, 30); 
		enlazarCajas(); 
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH66","QH68","QH70","QH70_O","QH71","QH72","ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH62","QH63","QH64","QH61Q","QH61S","QH61T")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH66","QH68","QH70","QH70_O","QH71","QH72")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita(); 
		rgQH66=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQH68=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQH70=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh70_1,R.string.hogarqh70_2,R.string.hogarqh70_3,R.string.hogarqh70_4,R.string.hogarqh70_5,R.string.hogarqh70_6,R.string.hogarqh70_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH70_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH70.agregarEspecifique(6,txtQH70_O); 
		
		lblpregunta71_ind = new LabelComponent(this.getActivity()).size(altoComponente,320).text(R.string.hogarqh71_ind).textSize(17);
		txtQH71=new IntegerField(this.getActivity()).size(altoComponente+5, 80).maxLength(2).centrar(); 
		lblpregunta72_ind = new LabelComponent(this.getActivity()).size(altoComponente,320).text(R.string.hogarqh72_ind).textSize(17);
		txtQH72=new IntegerField(this.getActivity()).size(altoComponente+5, 80).maxLength(2).centrar().callback("onQH72ChangeValue"); 
		
		lblpregunta66 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh66).textSize(19);
		lblpregunta68 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh68).textSize(19);
		lblpregunta70 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh70).textSize(19);
		lblpregunta71 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh71).textSize(19);
		lblpregunta72 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh72).textSize(19);
		
		gridpreg71 = new GridComponent2(getActivity(),Gravity.CENTER, 2,0);
		gridpreg71.addComponent(lblpregunta71_ind);
		gridpreg71.addComponent(txtQH71);
		
		gridpreg72 = new GridComponent2(getActivity(),Gravity.CENTER, 2,0);
		gridpreg72.addComponent(lblpregunta72_ind);
		gridpreg72.addComponent(txtQH72);
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblpregunta66,rgQH66); 
		q2 = createQuestionSection(lblpregunta68,rgQH68); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta70,rgQH70); 
		q4 = createQuestionSection(lblpregunta71,gridpreg71.component());
		q5 = createQuestionSection(lblpregunta72,gridpreg72.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 

		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh70!=null) {
				hogar.qh70=hogar.getConvert70(hogar.qh70);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		
		return true; 
    } 
   
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 		

		if (Util.esDiferente(hogar.qh62,95)) {			
				if (!Util.esDiferente(hogar.qh62,5,6,7,8,9,10,96) || !Util.esDiferente(hogar.qh64,5,6,7,8,9,10)) {				
					if (Util.esVacio(hogar.qh66)) {
						mensaje=preguntaVacia.replace("$","La pregunta P.66");
						view=rgQH66;
						error=true;
						return false;			
					}	
				}
				if (Util.esVacio(hogar.qh68)) {
					mensaje=preguntaVacia.replace("$","La pregunta P.68");
					view=rgQH68;
					error=true;
					return false;
				}
			
		}			

			
		if (Util.esVacio(hogar.qh70)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.70"); 
			view = rgQH70; 
			error = true; 
			return false; 
		} 
		if(!Util.esDiferente(hogar.qh70,7)){ 
			if (Util.esVacio(hogar.qh70)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQH70_O; 
				error = true; 
				return false; 
			} 
		} 
		if (Util.esVacio(hogar.qh71)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.71"); 
			view = txtQH71; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(hogar.qh72)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.72"); 
			view = txtQH72; 
			error = true; 
			return false; 
		} 
		if (Util.esMayor(hogar.qh72, hogar.qh71)) {
			mensaje = ("Total de habitaciones no puede ser menor que habitaciones que usa para dormir"); 
			view = txtQH71; 
			error = true; 
			return false; 
		}
		
		return true; 		
    } 
        
    @Override 
    public void cargarDatos() { 
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado);
		
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh70!=null) {
			hogar.qh70=hogar.setConvert70(hogar.qh70);
		}
		entityToUI(hogar);	
		inicio(); 
    } 
    
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

    public void saltoqh62qh63qh64(Integer p62, Integer p63, Integer p64) {
    	if (Util.esDiferente(p63,1)) {// es null o 2		
    		if (!Util.esDiferente(p63,2)){
    			if (!Util.esDiferente(p62,1,2,3,4)) {
    				Log.e("111","111");    	
    				Util.cleanAndLockView(getActivity(),rgQH66);
					q1.setVisibility(View.GONE);
					Util.lockView(getActivity(),false,rgQH68);	    			
					q2.setVisibility(View.VISIBLE);
					rgQH68.requestFocus();
    			}
    			else{
    				Log.e("111","222");
		    		Util.lockView(getActivity(),false,rgQH66,rgQH68);	    			
					q1.setVisibility(View.VISIBLE);
					q2.setVisibility(View.VISIBLE);
					rgQH66.requestFocus();
    			}
			}
    		else{
    			Log.e("111","333");    		
    			Util.cleanAndLockView(getActivity(),rgQH66,rgQH68);	    			
    			q1.setVisibility(View.GONE);
    			q2.setVisibility(View.GONE);    		
    		}   	
		}
    	else{// es 1
    		if (!Util.esDiferente(p62,1,2,3,4) && !Util.esDiferente(p64,1,2,3,4)) {
    			Log.e("111","444");    	
    			Util.cleanAndLockView(getActivity(),rgQH66);
				q1.setVisibility(View.GONE);
				Util.lockView(getActivity(),false,rgQH68);	    			
				q2.setVisibility(View.VISIBLE);
				rgQH68.requestFocus();
			}
			else{
				Log.e("111","55");
	    		Util.lockView(getActivity(),false,rgQH66,rgQH68);	    			
				q1.setVisibility(View.VISIBLE);
				q2.setVisibility(View.VISIBLE);
				rgQH66.requestFocus();
			}    		
    	}
    }
    public void onQH72ChangeValue(){
    	OcultarTecla();
    }
    
    public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(txtQH72.getWindowToken(), 0);
	}

    private void inicio() { 
    	saltoqh62qh63qh64(hogar.qh62,hogar.qh63,hogar.qh64);
    	ValidarsiesSupervisora(); 
    	txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH66.readOnly();
			rgQH68.readOnly();
			rgQH70.readOnly();
			txtQH70_O.readOnly();
			txtQH71.readOnly();
			txtQH72.readOnly();
		}
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar); 
		try { 
			if (hogar.qh70!=null) {
				hogar.qh70=hogar.getConvert70(hogar.qh70);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.HOGAR;
	}
} 
