package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
// 
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

// 
public class HogarFragment_002 extends FragmentForm {
	@FieldAnnotation(orderIndex = 1)
	public IntegerField txtQH45;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQH46;
	@FieldAnnotation(orderIndex = 3)
	public TextField txtQH46_O;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQH47;
	@FieldAnnotation(orderIndex = 5)
	public TextField txtQH47_O;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQH48;
	
	public ButtonComponent btnNosabe;
	Hogar hogar;
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo,lbltitulo1,lblpregunta45,lblpregunta46,lblpregunta47,lblpregunta48,lblpregunta46_ind,lblpregunta47_ind,lblpregunta45_ind;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoqh47qh48;
	SeccionCapitulo[] seccionesCargado;
	public GridComponent2 gridpreg45;
	//
	public HogarFragment_002() {
	}

	public HogarFragment_002 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQH45, 1, 900, null, 998);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH45", "QH46", "QH46_O", "QH47", "QH47_O", "QH48", "ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH40","QH41") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH45", "QH46", "QH46_O", "QH47", "QH47_O", "QH48") };
		seccionesGrabadoqh47qh48 = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH45", "QH46", "QH46_O", "QH47", "QH47_O", "QH48","QH49","QH50")};
		return rootView;
	}

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();
		txtQH45 = new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(3);
		rgQH46 = new RadioGroupOtherField(this.getActivity(),
				R.string.hogarqh46_1, R.string.hogarqh46_2,
				R.string.hogarqh46_3, R.string.hogarqh46_4,
				R.string.hogarqh46_5).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH46_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH46.agregarEspecifique(4, txtQH46_O);
		rgQH47 = new RadioGroupOtherField(this.getActivity(),
				R.string.hogarqh47_1, R.string.hogarqh47_2,
				R.string.hogarqh47_3, R.string.hogarqh47_4,
				R.string.hogarqh47_5, R.string.hogarqh47_6,
				R.string.hogarqh47_7, R.string.hogarqh47_8,
				R.string.hogarqh47_9).size(WRAP_CONTENT, WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP47ChangeValue");
		txtQH47_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH47.agregarEspecifique(8, txtQH47_O);
		rgQH48 = new RadioGroupOtherField(this.getActivity(), R.string.rb_si,R.string.rb_no).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		Spanned texto1=Html.fromHtml("<b>SONDEE:</b> �Es mayor o menor de 15 a�os?");
		lblpregunta46_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta46_ind.setText(texto1);
		
		Spanned texto2=Html.fromHtml(" <b>\"SI\" :</b> SELECCIONE 1 <b><br><br>\"NO\" : �Qu� le hacen al agua que habitualmente utilizan para tomar o beber?</b>");
		lblpregunta47_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta47_ind.setText(texto2);
		
		lblpregunta45 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh45).textSize(19);	
		lblpregunta45_ind = new LabelComponent(this.getActivity()).size(altoComponente,120).text(R.string.hogarqh45_ind).textSize(17);
		btnNosabe = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnnosabe).size(180, 55);
		
		lblpregunta46 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh46).textSize(19);
		lblpregunta47 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh47).textSize(19);
		lblpregunta48 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh48).textSize(19);
				
		gridpreg45 = new GridComponent2(getActivity(), 3);
		gridpreg45.addComponent(lblpregunta45_ind);
		gridpreg45.addComponent(txtQH45);
		gridpreg45.addComponent(btnNosabe);
		
		btnNosabe.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				txtQH45.setText("998");
			}
		});
	}

	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(txtCabecera,lblTitulo);
		q1 = createQuestionSection(lblpregunta45, gridpreg45.component());
		q2 = createQuestionSection(0, Gravity.LEFT| Gravity.CENTER_VERTICAL,lblpregunta46,lblpregunta46_ind,rgQH46);
		q3 = createQuestionSection(0, Gravity.LEFT| Gravity.CENTER_VERTICAL,lblpregunta47,lblpregunta47_ind, rgQH47);
		q4 = createQuestionSection(lblpregunta48, rgQH48);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		// uiToEntity(entity);
		uiToEntity(hogar);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		try {
			
			if (hogar.qh46!=null) {
				hogar.qh46=hogar.getConvertqh46(hogar.qh46);	
			}
			if (hogar.qh47!=null) {
				hogar.qh47=hogar.getConvertqh47(hogar.qh47);
			}	
			boolean flag=false;
			if (!Util.esDiferente(hogar.qh47,91) || !Util.esDiferente(hogar.qh48,2)) {
				flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabadoqh47qh48);
			}
			else{
				flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			}
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return false;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
		return true;
	}
	
	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esDiferente(hogar.qh40,11,21,41) && Util.esDiferente(hogar.qh41,11,21,41)) {
			if (Util.esVacio(hogar.qh45)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.45");
				view = txtQH45;
				error = true;
				return false;		
			}
			if (Util.esVacio(hogar.qh46)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.46");
				view = rgQH46;
				error = true;
				return false;
			}		
			if (!Util.esDiferente(hogar.qh46,5)) {
				if (Util.esVacio(hogar.qh46_o)) {
					
					mensaje = "Debe ingresar informaci\u00f3n en Especifique";
					view = txtQH46_O;
					error = true;
					return false;
				}
			}
		}
		if (Util.esVacio(hogar.qh47)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.47");
			view = rgQH47;
			error = true;
			return false;
		}
		if (!Util.esDiferente(hogar.qh47, 9)) {
			if (Util.esVacio(hogar.qh47_o)) {
				
				mensaje = "Debe ingresar informaci\u00f3n en Especifique";
				view = txtQH47_O;
				error = true;
				return false;
			}
		}
		if (Util.esDiferente(hogar.qh47, 8)) {
			if (Util.esVacio(hogar.qh48)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.48");
				view = rgQH48;
				error = true;
				return false;
			}
		}
		return true;
	}

	@Override
	public void cargarDatos() {
		hogar = getCuestionarioService().getHogarByInformante(
				App.getInstance().getHogar().id,
				App.getInstance().getHogar().hogar_id, seccionesCargado);
		if (hogar == null) {
			hogar = new Hogar();
			hogar.id = App.getInstance().getHogar().id;
			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
		}
		if (hogar.qh46!=null) {
			hogar.qh46=hogar.setConvertqh46(hogar.qh46);
		}
		if (hogar.qh47!=null) {
			hogar.qh47=hogar.setConvertqh47(hogar.qh47);
		}
		entityToUI(hogar);
		//saltoqh40(hogar.qh40);		
		inicio();
	}
	
	public void validarrgQH40(Integer p40) {
		if (!Util.esDiferente(p40,91)) {
			rgQH47.lockButtons(true,0,1,2,3,4,5,6,8); 	
			rgQH47.lockButtons(false,7);
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH48);
			q4.setVisibility(View.GONE);	
        }
		if (!Util.esDiferente(p40,11,12,13,21,22,31,32,41,51,96)) {		
			rgQH47.lockButtons(true,7);
			rgQH47.lockButtons(false,0,1,2,3,4,5,6,8); 
			Util.lockView(getActivity(), false, rgQH48);
			q4.setVisibility(View.VISIBLE);
        }		
	}
	
	public void saltoqh40qh41(Integer p40, Integer p41) {	
		if (!Util.esDiferente(p40,11,21,41) || !Util.esDiferente(p41,11,21,41)) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), txtQH45,rgQH46);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			rgQH47.requestFocus();
		}			
		else{
			Util.lockView(getActivity(), false, txtQH45,rgQH46);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			txtQH45.requestFocus();
		}		
	}
	
	private void inicio() {
		onP47ChangeValue();
		saltoqh40qh41(hogar.qh40,hogar.qh41);
		validarrgQH40(hogar.qh40);
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQH45.readOnly();
			Util.cleanAndLockView(getActivity(),btnNosabe);
			rgQH46.readOnly();
			txtQH46_O.readOnly();
			rgQH47.readOnly();
			txtQH47_O.readOnly();
			rgQH48.readOnly();
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;}


	public void onP47ChangeValue() {
		if (MyUtil.incluyeRango(8,8,rgQH47.getTagSelected("").toString()) ) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(), rgQH48);
			q4.setVisibility(View.GONE);			
			
		} else {
			Util.lockView(getActivity(), false, rgQH48);
			q4.setVisibility(View.VISIBLE);
			rgQH48.requestFocus();
			
		}
		
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (hogar.qh46!=null) {
				hogar.qh46=hogar.getConvertqh46(hogar.qh46);	
			}
			if (hogar.qh47!=null) {
				hogar.qh47=hogar.getConvertqh47(hogar.qh47);
			}	
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
}
