package gob.inei.endes2024.fragment.hogar;
//package gob.inei.endes2024.fragment.hogar;
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.DialogComponent;
//import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.MasterActivity;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.TableComponent;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.interfaces.Respondible;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.dialog.ChMigracion_1Dialog;
//import gob.inei.endes2024.fragment.hogar.dialog.ChMigracion_2Dialog;
//import gob.inei.endes2024.model.Hogar;
//import gob.inei.endes2024.model.MIGRACION;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//
//import java.sql.SQLException;
//import java.util.List;
//
//import android.os.Bundle;
//import android.support.v4.app.FragmentManager;
//import android.text.Html;
//import android.text.Spanned;
//import android.text.style.TtsSpan;
//import android.util.Log;
//import android.view.ContextMenu;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.ContextMenu.ContextMenuInfo;
//import android.widget.AdapterView;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//import android.widget.AdapterView.OnItemClickListener;
//
//public class HogarFragment_0001 extends FragmentForm implements Respondible {
//	
//	@FieldAnnotation(orderIndex=1) 
//	public RadioGroupOtherField rgQH34;
//	@FieldAnnotation(orderIndex=2) 
//	public IntegerField txtQH34_N;
//	
//	@FieldAnnotation(orderIndex=3)
//	public ButtonComponent btnAgregar;
//	@FieldAnnotation(orderIndex=4)
//	public TextAreaField txtQH34_OBS;
//
//	Hogar hogar;	 
//	public TableComponent tcMigracion;
//	public List<MIGRACION> migracion;
//	public Integer numHerMayores;
//	
//	
//	SeccionCapitulo[] seccionesGrabadoHogar; 
//	SeccionCapitulo[] seccionesCargado,seccionesCargadoHogar, seccionesCargadoseccion01,seccionesCargadoVisita,seccionesGrabado; 
//	
//	private CuestionarioService cuestionarioService;
//	private Seccion01Service Personaservice;
//	public TextField txtCabecera;
//	personasMigracionClickListener adapter;
//	public LabelComponent lbltitulo,lblpreguntaqh34_n,lblprimeraindicacion,lblsegundaindicacion,lblpreguntaqh34b, lblpreguntaqh34_1,lblpreguntaqh34_2,lblpreguntaqh28,lblobs_34;
//	private DialogComponent dialog;
//	public GridComponent2 gridpregunta901;
//	
//	public LinearLayout q0;
//	public LinearLayout q1;
//	public LinearLayout q2;
//	public LinearLayout q3;
//	
//	
//	
//	private enum ACTION {
//		ELIMINAR,ELIMINARTODO
//	}
//
//	private ACTION action;
//	
//	public HogarFragment_0001 parent(MasterActivity parent) {
//		this.parent = parent;
//		return this;
//	}
//	
//	@Override 
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
//		rootView = createUI(); 
//		initObjectsWithoutXML(this, rootView); 
//		adapter = new personasMigracionClickListener();
//		tcMigracion.getListView().setOnItemClickListener(adapter);
//		enlazarCajas(); 
//		listening();
//		
//		seccionesCargado = new SeccionCapitulo[] {new SeccionCapitulo(-1,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "QH34C", "QH34D_A", "QH34D_M", "QH34E_A", "QH34E_M", "QH34F_CCDD", "QH34F_CCDD_NS", "QH34F_CCPP", "QH34F_CCPP_NS", "QH34F_CCDI", "QH34F_CCDI_NS", "QH34F_PAIS", "QH34F_PAIS_NS", "QH34G", "QH34GO", "QH34G_OBSERVACION", "QH34H", "QH34HO", "QH34I", "QH34J", "QH34J_REF", "QH34K", "QH34KO", "QH34_OBSERVACION", "ESTADO","ID","HOGAR_ID")};
//		seccionesGrabado = new SeccionCapitulo[] {new SeccionCapitulo( 0,-1,-1, "QH34A", "QH34B_1", "QH34B_11", "QH34B_2", "QH34B_3", "QH34C", "QH34D_A", "QH34D_M", "QH34E_A", "QH34E_M", "QH34F_CCDD", "QH34F_CCDD_NS", "QH34F_CCPP", "QH34F_CCPP_NS", "QH34F_CCDI", "QH34F_CCDI_NS", "QH34F_PAIS", "QH34F_PAIS_NS", "QH34G", "QH34GO", "QH34G_OBSERVACION", "QH34H", "QH34HO", "QH34I", "QH34J", "QH34J_REF", "QH34K", "QH34KO", "QH34_OBSERVACION", "ID","HOGAR_ID")};
//		
//		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH34", "QH34_N", "QH34_OBS","ID","HOGAR_ID") };
//		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QH34", "QH34_N", "QH34_OBS") };
//		
//		return rootView; 
//	} 
//	  
//	  
//
//
//	@Override
//	protected void buildFields() {
//		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
//		lbltitulo = new LabelComponent(getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.migracion34_t).textSize(20).centrar();
//		
//		lblpreguntaqh34_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion34_1);
//		
//		Spanned texto34_1=Html.fromHtml("Desde enero del 2019 hasta la fecha, �algunas personas que fueron miembros de este hogar se fueron a vivir permanentemente a otro distrito del pa�s, o al extranjero y no han vuelto a vivir a este hogar?");
//		lblpreguntaqh34_2 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).alinearIzquierda().textSize(19);
//		lblpreguntaqh34_2.setText(texto34_1);
//		
//		lblpreguntaqh34b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion34b_n);
//		
//		lblobs_34 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//		
//		lblpreguntaqh34_n = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.migracion34_n);
//		
//		rgQH34 = new RadioGroupOtherField(getActivity(),R.string.mortalidad_qh27_1,R.string.mortalidad_qh27_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onQH34ChangeValue");
//		txtQH34_N = new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(2);
//		txtQH34_OBS = new TextAreaField(getActivity()).maxLength(1000).size(200, 700).alfanumerico();
//		
//		
//		btnAgregar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200,55).text(R.string.btnAgregar);
//		btnAgregar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag=false;
//				flag=grabaralgunos();
//				if(!flag){
//					return;
//				}
//				agregarPersona();
//				//Util.cleanAndLockView(getActivity(), btnAgregar);
//			}
//		});
//		
//		tcMigracion = new TableComponent(getActivity(), this,App.ESTILO).size(450,750).headerHeight(45).dataColumHeight(40).headerTextSize(15);		
//		tcMigracion.addHeader(R.string.migracion34_orden,0.5f);
//		tcMigracion.addHeader(R.string.migracion34_nombres,3f,TableComponent.ALIGN.LEFT);
//		tcMigracion.addHeader(R.string.migracion34_sexo,0.8f);
////		tcMigracion.addHeader(R.string.migracion34_edad_a,0.5f);
////		tcMigracion.addHeader(R.string.migracion34_edad_m,0.5f);
//	}
//	
//	@Override
//	protected View createUI() {
//		// TODO Auto-generated method stub
//		buildFields();
//		q0 = createQuestionSection(txtCabecera,lbltitulo);
//		q1 = createQuestionSection(lblpreguntaqh34_1,lblpreguntaqh34_2,rgQH34, lblpreguntaqh34_n,txtQH34_N);
//		q2 = createQuestionSection(0,lblpreguntaqh34b,btnAgregar,tcMigracion.getTableView());
//		q3 = createQuestionSection(lblobs_34,txtQH34_OBS);
//		
//		ScrollView contenedor = createForm(); 
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
//		form.addView(q0);
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		return contenedor;
//	}
//
//	public boolean grabaralgunos(){
//		uiToEntity(hogar); 
//		if (!validaralgunos()) { 
//			if (error) { 
//				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
//				if (view != null) view.requestFocus(); 
//			} 
//			return false; 
//		} 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar)){ 
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return false; 
//			} 
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return false; 
//		}
//		return true;
//	}
//	
//	@Override
//	public boolean grabar() {
//		uiToEntity(hogar); 
//		if (!validar()) { 
//			if (error) { 
//				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
//				if (view != null) view.requestFocus(); 
//			} 
//			return false; 
//		} 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){ 
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return false; 
//			} 
//			if(migracion.size()>0){
//				ReordenarDatos(hogar);
//			}
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return false; 
//		} 
//		return true;
//	}
//	
//	
//	public void ReordenarDatos(Hogar h){
//		boolean flag=false;
//		migracion= getCuestionarioService().getMigracion(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
//		flag=getCuestionarioService().DeleteMigracionHogarTodo(h.id,h.hogar_id);
//		Integer contador=1;
//		for(MIGRACION persona: migracion){
//			try {
//				persona.qh34a=contador;
//				getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//				contador++;
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}
//	}
//	
//
//	private boolean validaralgunos(){
//		if(!isInRange()) return false; 
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		if (Util.esVacio(hogar.qh34)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta QH34"); 
//			view = rgQH34; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(hogar.qh34, 1)){
//			if (Util.esVacio(hogar.qh34_n)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QH34_N"); 
//				view = txtQH34_N; 
//				error = true; 
//				return false; 
//			}
//		}
//		return true;
//	}
//	
//	private boolean validar() {
//		if(!isInRange()) return false; 
//			String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//			if (Util.esVacio(hogar.qh34)) {
//					mensaje = preguntaVacia.replace("$", "La pregunta QH34"); 
//					view = rgQH34; 
//					error = true; 
//					return false; 
//			} 
//			if(!Util.esDiferente(hogar.qh34, 1)){
//				if (Util.esVacio(hogar.qh34_n)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta QH34_N"); 
//					view = txtQH34_N; 
//					error = true; 
//					return false; 
//				}
//				if (Util.esMenor(hogar.qh34_n,1)) { 
//					mensaje = "No puede ser menor que 1."; 
//					view = txtQH34_N; 
//					error = true; 
//					return false; 
//				}
//				if (Util.esDiferente(hogar.qh34_n, migracion.size())){
//					mensaje = "El total del listado debe conincidir con QH34_N"; 
//					view = btnAgregar; 
//					error = true; 
//					return false;
//				}
//				if(!getCuestionarioService().TodoLosMiembrosMigracionhogarCompletados(hogar.id, hogar.hogar_id) || migracion.size()==0){
//					mensaje = "Debe completar las preguntas para todos del listado";
//					view = tcMigracion;
//					error = true;
//					return false;
//				}
//			}
//		return true; 
//	}
//	  
//	private void validarMensaje(String msj) {
//	   ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//	}
//	 
//	  
//	@Override
//	public void cargarDatos() {
//		hogar = getCuestionarioService().getHogarByInformante(
//				App.getInstance().getHogar().id,
//				App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
//		if (hogar == null) {
//			hogar = new Hogar();
//			hogar.id = App.getInstance().getHogar().id;
//			hogar.hogar_id = App.getInstance().getHogar().hogar_id;
//		}
//		entityToUI(hogar);
//		recargardatos();
//		inicio();
//	}
//	
//	public void inicio(){
//		onQH34ChangeValue();
//		ValidarsiesSupervisora();
//		RenombrarEtiquetas();
//		txtCabecera.requestFocus();
//	}
//	
//	public void onQH34ChangeValue(){
//		Integer dato=Integer.parseInt(rgQH34.getTagSelected("0").toString());
//		if(!Util.esDiferente(dato, 2)){
//			MyUtil.LiberarMemoria();
//			Util.cleanAndLockView(getActivity(),txtQH34_N, txtQH34_OBS,tcMigracion);
//			lblpreguntaqh34_n.setVisibility(View.GONE);
//			txtQH34_N.setVisibility(View.GONE);
//			q2.setVisibility(View.GONE);
//			q3.setVisibility(View.GONE);
//			if(migracion.size()>0){
//				action = ACTION.ELIMINARTODO;
//				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Se Eliminar� registro de personas?");
//				dialog.put("seleccion", (MIGRACION) migracion.get(0));
//				dialog.showDialog();
//			}			
//		}
//		else{
//			Util.lockView(getActivity(), false, txtQH34_N,txtQH34_OBS,tcMigracion);
//			lblpreguntaqh34_n.setVisibility(View.VISIBLE);
//			txtQH34_N.setVisibility(View.VISIBLE);
//			q2.setVisibility(View.VISIBLE);
//			q3.setVisibility(View.VISIBLE);
//			txtQH34_N.requestFocus();
//		}
//	}
//	
//	public void RenombrarEtiquetas(){
//		//lblpreguntaqh34_2.setText(lblpreguntaqh34_2.getText().toString().replace("#", App.ANIOPORDEFECTO+1+""));
//		//lblpreguntaqh34_2.setText(lblpreguntaqh34_2.getText().toString().replace("#", "2017"));
//		Spanned texto34_2= Html.fromHtml(lblpreguntaqh34_2.getText().toString().replace("permanentemente a otro distrito", "<b>permanentemente</b> a otro <b>distrito</b>"));
//		lblpreguntaqh34_2.setText(texto34_2);
//	}
//	
//	public void ValidarsiesSupervisora(){
//		Integer codigo=App.getInstance().getUsuario().cargo_id;
//		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//			rgQH34.readOnly();
//			txtQH34_N.readOnly();
//			txtQH34_OBS.setEnabled(false);
//			Util.cleanAndLockView(getActivity(), btnAgregar);
//			btnAgregar.requestFocus();
//		}
//	}
//		
//	public void agregarPersona() {
//		MIGRACION bean = new MIGRACION();
//		bean.id = App.getInstance().getMarco().id;
//		bean.hogar_id = App.getInstance().getHogar().hogar_id;
//		bean.qh34a = migracion.size() + 1;
//		abrirDetalle(bean);
//	}
//	
//	public void recargardatos(){
//		migracion= getCuestionarioService().getMigracion(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id, seccionesCargado);
//		tcMigracion.setData(migracion, "QH34A","getNombres","getSexo");
////		tcMigracion.setData(migracion, "QH34A","getNombres","getSexo","QH34D_A","QH34D_M");
//		
//		for (int row = 0; row < migracion.size(); row++) {
//    		if (obtenerEstado(migracion.get(row)) == 1) {
//				// borde de color azul
//    			tcMigracion.setBorderRow(row, true);
//			} else if (obtenerEstado(migracion.get(row)) == 2) {
//				// borde de color rojo
//				tcMigracion.setBorderRow(row, true, R.color.red);
//			} else {
//				tcMigracion.setBorderRow(row, false);
//			}
//		}
//		
//		tcMigracion.reloadData();
//		registerForContextMenu(tcMigracion.getListView());
//	}
//	
//	private int obtenerEstado(MIGRACION detalle) {
//		if (!Util.esDiferente(detalle.estado, 0)) {
//			return 1 ;
//		} else if (!Util.esDiferente(detalle.estado,1)) {
//			return 2;
//		}
//		return 0;
//	}
//
//
//    private class personasMigracionClickListener implements OnItemClickListener {
//		public personasMigracionClickListener() {
//		}
//
//		@Override
//		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//			MIGRACION c = (MIGRACION) migracion.get(arg2);
//			abrirDetalleEditar(c, arg2, (List<MIGRACION>) migracion);
//		}
//	}
//    
//    public void refrescarPersonas(MIGRACION datos) {
//		if (migracion.contains(datos)) {
//			recargardatos();
//			return;
//		} else {
//			migracion.add(datos);
//			recargardatos();
//		}
//	}
//    
//   public void abrirDetalleEditar(MIGRACION  tmp, int index, List<MIGRACION> detalles){
//    	FragmentManager fm = HogarFragment_0001.this.getFragmentManager();
//    	ChMigracion_2Dialog aperturaDialog = ChMigracion_2Dialog.newInstance(HogarFragment_0001.this, tmp, index, detalles);
//    	aperturaDialog.setAncho(MATCH_PARENT);
//    	aperturaDialog.show(fm, "aperturaDialog");
//    }
//    
//    public void abrirDetalle(MIGRACION tmp) {
//    	FragmentManager fm = HogarFragment_0001.this.getFragmentManager();
//    	ChMigracion_1Dialog aperturaDialog = ChMigracion_1Dialog.newInstance(this, tmp);
//		aperturaDialog.setAncho(MATCH_PARENT);
//		aperturaDialog.show(fm, "aperturaDialog");
//    }
//    
//    @Override
//	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
//		super.onCreateContextMenu(menu, v, menuInfo);
//		if (v.equals(tcMigracion.getListView())) {
//			menu.setHeaderTitle("Opciones del Residente");
//			menu.add(1, 0, 1, "Editar");
//			menu.add(1, 1, 1, "Eliminar");
//			
//			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//			Integer posicion = 0;
//			posicion = migracion.get(info.position).qh34a;
//			if (posicion > 1) {
//				posicion = posicion - 1;
//			}
//			MIGRACION seleccion = (MIGRACION) info.targetView.getTag();
//			Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		menu.getItem(1).setVisible(false);
//	    	}
//		}
//	}
//
//	@Override
//	public boolean onContextItemSelected(MenuItem item) {
//		if (!getUserVisibleHint())
//			return false;
//		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//		MIGRACION seleccion = (MIGRACION) info.targetView.getTag();
//		if (item.getGroupId() == 1) {
//			switch (item.getItemId()) {
//			case 0:
//				abrirDetalleEditar((MIGRACION) migracion.get(info.position), info.position,  (List<MIGRACION>) migracion);  //CISECCION_09 tmp, int index, List<CISECCION_09> detalles
//				break;
//			case 1:
//				action = ACTION.ELIMINAR;
//				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"�Esta seguro que desea eliminar?");
//				dialog.put("seleccion", (MIGRACION) migracion.get(info.position));
//				dialog.showDialog();
//				break;
//				}
//			}
//		return super.onContextItemSelected(item);
//	}
//
//	@Override
//	public void onCancel() {
//		// TODO Auto-generated method stub
//		if(action==ACTION.ELIMINARTODO){
//			rgQH34.setTagSelected(1);
//		}
//		
//	}
//	
//	public CuestionarioService getCuestionarioService() { 
//		if(cuestionarioService==null){ 
//			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
//		} 
//		return cuestionarioService; 
//    }
//	
//	@Override
//	public void onAccept() {
//		// TODO Auto-generated method stub
//		if(action==ACTION.ELIMINAR){
//			MIGRACION hermanoaeliminar = (MIGRACION) dialog.get("seleccion");
//			try {
//				migracion.remove(hermanoaeliminar);
//				boolean borrado = false;
//				borrado = getCuestionarioService().DeleteMigracionHogar(hermanoaeliminar);
//				//DesabilitarelBotonAgregar();
//				if (!borrado) {
//					throw new SQLException("Persona no pudo ser borrada.");
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//				ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
//						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
//			}
//		}
//		if(action==ACTION.ELIMINARTODO){
//			MIGRACION bean = (MIGRACION)dialog.get("seleccion");
//			try {
//				boolean borrado = false;
//				migracion.clear();
//				borrado=getCuestionarioService().DeleteMigracionHogarTodo(bean.id,bean.hogar_id);
//				if (!borrado) {
//					throw new SQLException("Error al eliminar.");
//				}
//			} catch (SQLException ee) {
//				ee.printStackTrace();
//				ToastMessage.msgBox(getActivity(), "ERROR: " + ee.getMessage(),
//						ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_SHORT);
//			}
//		}
//		action=null;
//		recargardatos();
//
//		
//	}
//
//	
//	@Override
//	public Integer grabadoParcial() {
//		uiToEntity(hogar); 
//		try { 
//			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesCargadoHogar)){
//				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//				return App.NODEFINIDO; 
//			} 
//		} catch (SQLException e) { 
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
//			return App.NODEFINIDO; 
//		}	
//		return App.HOGAR;
//	}
//}
