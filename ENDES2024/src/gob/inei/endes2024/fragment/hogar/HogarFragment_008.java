package gob.inei.endes2024.fragment.hogar;


import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_008 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH75; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQH75_O;
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH76A;
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQH76B;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH76C;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH76D;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQH76E;

	
	public LabelComponent lblTitulo,lbltituloPregunta,lblventanas,lblvidrio,lblmadera,lblmallas,lblcortinas,lblelavorados,lblrusticos,lblnaturales,lblpregunta75,lblpregunta76;
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado;
	Hogar hogar;
	public TextField txtCabecera;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	private CuestionarioService cuestionarioService; 
	
	public GridComponent2 gridPreguntas76A,gridPreguntas76B,gridPreguntas76C,gridPreguntas76D,gridPreguntas76E;
	
	public HogarFragment_008() {} 
	public HogarFragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH75","QH75_O","QH76A","QH76B","QH76C","QH76D","QH76E","ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH73","QH74")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH75","QH75_O","QH76A","QH76B","QH76C","QH76D","QH76E")}; 
		return rootView; 
	} 
	
	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		// TODO Auto-generated method stub
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita();
		
		rgQH75 = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh75_11,R.string.hogarqh75_12,R.string.hogarqh75_21,R.string.hogarqh75_22,R.string.hogarqh75_23,R.string.hogarqh75_31,R.string.hogarqh75_32,R.string.hogarqh75_33,R.string.hogarqh75_34,R.string.hogarqh75_96).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQH75_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH75.agregarEspecifique(9,txtQH75_O);
		lblelavorados = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t1).textSize(18).negrita();
		lblrusticos = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t2).textSize(18).negrita();
		lblnaturales = new LabelComponent(this.getActivity()).text(R.string.hogarqh74_t3).textSize(18).negrita();
		rgQH75.agregarTitle(0,lblelavorados);
		rgQH75.agregarTitle(3, lblrusticos);
		rgQH75.agregarTitle(7, lblnaturales);
		
		lbltituloPregunta = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh76).textSize(21).centrar().negrita().alinearIzquierda();
		lblventanas = new LabelComponent(this.getActivity()).size(altoComponente,550).text(R.string.hogarqh76a).textSize(18).alinearIzquierda();	
		rgQH76A = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh76a_1,R.string.hogarqh76a_2).size(70, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP76aChangeValue");
		lblvidrio =  new LabelComponent(this.getActivity()).size(altoComponente,550).text(R.string.hogarqh76b).textSize(18).alinearIzquierda();
		rgQH76B = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh76b_1,R.string.hogarqh76b_2).size(70, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblmadera= new LabelComponent(this.getActivity()).size(altoComponente,550).text(R.string.hogarqh76c).textSize(18).alinearIzquierda();
		rgQH76C = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh76c_1,R.string.hogarqh76c_2).size(70, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblmallas = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh76d).textSize(18).alinearIzquierda();
		rgQH76D = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh76d_1,R.string.hogarqh76d_2).size(70, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblcortinas = new LabelComponent(this.getActivity()).size(altoComponente, 550).text(R.string.hogarqh76e).textSize(18).alinearIzquierda();
		rgQH76E = new RadioGroupOtherField(this.getActivity(),R.string.hogarqh76e_1,R.string.hogarqh76e_2).size(70, 200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
		gridPreguntas76A= new 	GridComponent2(this.getActivity(),2);	
		gridPreguntas76A.addComponent(lblventanas);
		gridPreguntas76A.addComponent(rgQH76A);	
				
		gridPreguntas76B= new 	GridComponent2(this.getActivity(),2);
		gridPreguntas76B.addComponent(lblvidrio);
		gridPreguntas76B.addComponent(rgQH76B);
		
		gridPreguntas76C= new 	GridComponent2(this.getActivity(),2);
		gridPreguntas76C.addComponent(lblmadera);
		gridPreguntas76C.addComponent(rgQH76C);
		
		gridPreguntas76D= new 	GridComponent2(this.getActivity(),2);
		gridPreguntas76D.addComponent(lblmallas);
		gridPreguntas76D.addComponent(rgQH76D);
		
		gridPreguntas76E= new 	GridComponent2(this.getActivity(),2);
		gridPreguntas76E.addComponent(lblcortinas);
		gridPreguntas76E.addComponent(rgQH76E);
		
		lblpregunta75 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh75).textSize(18);	
		lblpregunta76 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh76).textSize(18);	
		
		
	}
	
	
	@Override
	protected View createUI() {
		// TODO Auto-generated method stub
		buildFields();
		 q0 = createQuestionSection(txtCabecera,lblTitulo);
		 q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta75,rgQH75); 
		 q2 = createQuestionSection(lblpregunta76,gridPreguntas76A.component(),gridPreguntas76B.component(),gridPreguntas76C.component(),gridPreguntas76D.component(),gridPreguntas76E.component());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		return contenedor;
	}	

	@Override
	public boolean grabar() {
		// TODO Auto-generated method stub
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh75!=null) {
				hogar.qh75=hogar.getConvert75(hogar.qh75);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
	}
	
	private boolean validar()
	{
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (!Util.esDiferente(App.getInstance().getHogar().hogar_id,1)) {			
			if(Util.esVacio(hogar.qh75)){
				mensaje = preguntaVacia.replace("$", "La pregunta P.75"); 
				view = rgQH75; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(hogar.qh75,10)) {
				if(Util.esVacio(hogar.qh75_o)){
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQH75_O; 
					error = true; 
					return false;
				}
			}
			
			if (Util.esDiferente(hogar.qh76a,2)) {
				if (Util.esVacio(hogar.qh76a)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.76A");
					view = rgQH76A;
					error = true;
					return false;	
				}	
				if (Util.esVacio(hogar.qh76b)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.76B"); 
					view = rgQH76B; 
					error = true; 
					return false;
				}
				if (Util.esVacio(hogar.qh76c)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.76C"); 
					view = rgQH76C; 
					error = true; 
					return false; 
				}
				if (Util.esVacio(hogar.qh76d)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.76D"); 
					view = rgQH76D; 
					error = true; 
					return false;
				}
				if (Util.esVacio(hogar.qh76e)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.76E"); 
					view = rgQH76E; 
					error = true; 
					return false; 
				}
			}			

//			if (!Util.esDiferente(hogar.qh73,11,12,13) && Util.esDiferente(hogar.qh75, 1,2,10)) {
//				validarMensaje("VERIFICAR �Verificar material predominante de los pisos y techo�");
//			}
		}
		
		return true;
	}

	@Override
	public void cargarDatos() {
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
		if (hogar.qh75!=null) {
			hogar.qh75=hogar.setConvert75(hogar.qh75);
		}
		entityToUI(hogar); 
		inicio(); 
	}
	
	private void validarMensaje(String msj) {
		ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	 
	public void validarrgQH75(Integer p74) {
		if (!Util.esDiferente(p74,35)) {
			rgQH76A.lockButtons(true,0);
		}
		else{
			rgQH76A.lockButtons(false,0);
		}
	}
	
	
	private void inicio() {
		if (Util.esMayor(hogar.hogar_id,1)) {
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
		}
		onP76aChangeValue();
		validarrgQH75(hogar.qh74);
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
    } 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
		rgQH75.readOnly();
		txtQH75_O.readOnly();
		rgQH76A.readOnly();
		rgQH76B.readOnly();
		rgQH76C.readOnly();
		rgQH76D.readOnly();
		rgQH76E.readOnly();
		}
	}
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onP76aChangeValue() {
 		if (MyUtil.incluyeRango(2,2,rgQH76A.getTagSelected("").toString()) ) {
 			MyUtil.LiberarMemoria();
 			Util.cleanAndLockView(getActivity(),rgQH76B,rgQH76C,rgQH76D,rgQH76E);
 			gridPreguntas76B.setVisibility(View.GONE);
 			gridPreguntas76C.setVisibility(View.GONE);
 			gridPreguntas76D.setVisibility(View.GONE);
 			gridPreguntas76E.setVisibility(View.GONE);
 			
 		} else {
 			Util.lockView(getActivity(), false,rgQH76B,rgQH76C,rgQH76D,rgQH76E);	 
 			gridPreguntas76B.setVisibility(View.VISIBLE);
 			gridPreguntas76C.setVisibility(View.VISIBLE);
 			gridPreguntas76D.setVisibility(View.VISIBLE);
 			gridPreguntas76E.setVisibility(View.VISIBLE);
 			rgQH76B.requestFocus();
 		}
 		
 	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar); 
		try { 
			if (hogar.qh75!=null) {
				hogar.qh75=hogar.getConvert75(hogar.qh75);
			}
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO;
		} 
		return App.HOGAR;
	}
}
