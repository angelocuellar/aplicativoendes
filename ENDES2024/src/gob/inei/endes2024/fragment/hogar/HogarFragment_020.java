package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.NativeActivity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_020 extends FragmentForm  {

	@FieldAnnotation(orderIndex=1)
	public RadioGroupOtherField rgQH8_01;
	
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQH8_02_A;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQH8_02_B;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQH8_02_C;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQH8_02_D;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQH8_02_E;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQH8_02_F;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQH8_02_X;
	@FieldAnnotation(orderIndex=9)
	public TextField txtQH8_02_XO;
	
	@FieldAnnotation(orderIndex=10)
	public CheckBoxField chbQH8_03_A;
	@FieldAnnotation(orderIndex=11)
	public IntegerField txtQH8_03C_A;
	@FieldAnnotation(orderIndex=12)
	public CheckBoxField chbQH8_03_B;
	@FieldAnnotation(orderIndex=13)
	public IntegerField txtQH8_03C_B;
	@FieldAnnotation(orderIndex=14)
	public CheckBoxField chbQH8_03_C;
	@FieldAnnotation(orderIndex=15)
	public IntegerField txtQH8_03C_C;
	@FieldAnnotation(orderIndex=16)
	public CheckBoxField chbQH8_03_D;
	@FieldAnnotation(orderIndex=17)
	public IntegerField txtQH8_03C_D;
	@FieldAnnotation(orderIndex=18)
	public CheckBoxField chbQH8_03_E;
	@FieldAnnotation(orderIndex=19)
	public IntegerField txtQH8_03C_E;
	@FieldAnnotation(orderIndex=20)
	public CheckBoxField chbQH8_03_F;
	@FieldAnnotation(orderIndex=21)
	public IntegerField txtQH8_03C_F;
	@FieldAnnotation(orderIndex=22)
	public CheckBoxField chbQH8_03_G;
	@FieldAnnotation(orderIndex=23)
	public IntegerField txtQH8_03C_G;
	
	@FieldAnnotation(orderIndex=24)
	public CheckBoxField chbQH8_03_H;
	@FieldAnnotation(orderIndex=25)
	public TextField txtQH8_03_HO;
	@FieldAnnotation(orderIndex=26)
	public IntegerField txtQH8_03C_H;
	
	@FieldAnnotation(orderIndex=27)
	public CheckBoxField chbQH8_03_I;
	@FieldAnnotation(orderIndex=28)
	public TextField txtQH8_03_IO;
	@FieldAnnotation(orderIndex=29)
	public IntegerField txtQH8_03C_I;	
	
	@FieldAnnotation(orderIndex=30)
	public CheckBoxField chbQH8_03_J;
	@FieldAnnotation(orderIndex=31)
	public IntegerField txtQH8_03C_J;
	@FieldAnnotation(orderIndex=32)
	public CheckBoxField chbQH8_03_K;
	@FieldAnnotation(orderIndex=33)
	public IntegerField txtQH8_03C_K;
	@FieldAnnotation(orderIndex=34)
	public CheckBoxField chbQH8_03_L;
	@FieldAnnotation(orderIndex=35)
	public IntegerField txtQH8_03C_L;
	@FieldAnnotation(orderIndex=36)
	public CheckBoxField chbQH8_03_M;
	@FieldAnnotation(orderIndex=37)
	public IntegerField txtQH8_03C_M;	
	@FieldAnnotation(orderIndex=38)
	public CheckBoxField chbQH8_03_N;
	@FieldAnnotation(orderIndex=49)
	public IntegerField txtQH8_03C_N;	
	@FieldAnnotation(orderIndex=40)
	public CheckBoxField chbQH8_03_O;
	@FieldAnnotation(orderIndex=41)
	public IntegerField txtQH8_03C_O;
	
	@FieldAnnotation(orderIndex=42)
	public CheckBoxField chbQH8_03_P;
	@FieldAnnotation(orderIndex=43)
	public TextField txtQH8_03_PO;
	@FieldAnnotation(orderIndex=44)
	public IntegerField txtQH8_03C_P;
	
	@FieldAnnotation(orderIndex=45)
	public CheckBoxField chbQH8_03_Q;
	@FieldAnnotation(orderIndex=46)
	public TextField txtQH8_03_QO;
	@FieldAnnotation(orderIndex=47)
	public IntegerField txtQH8_03C_Q;
	
	@FieldAnnotation(orderIndex=48)
	public CheckBoxField chbQH8_03_R;
	@FieldAnnotation(orderIndex=49)
	public IntegerField txtQH8_03C_R;
	@FieldAnnotation(orderIndex=50)
	public CheckBoxField chbQH8_03_S;
	@FieldAnnotation(orderIndex=51)
	public IntegerField txtQH8_03C_S;
	@FieldAnnotation(orderIndex=52)
	public CheckBoxField chbQH8_03_T;
	@FieldAnnotation(orderIndex=53)
	public IntegerField txtQH8_03C_T;
	@FieldAnnotation(orderIndex=54)
	public CheckBoxField chbQH8_03_U;
	@FieldAnnotation(orderIndex=55)
	public IntegerField txtQH8_03C_U;
	@FieldAnnotation(orderIndex=56)
	public CheckBoxField chbQH8_03_V;
	@FieldAnnotation(orderIndex=57)
	public IntegerField txtQH8_03C_V;
	
	@FieldAnnotation(orderIndex=58)
	public CheckBoxField chbQH8_03_W;
	@FieldAnnotation(orderIndex=59)
	public IntegerField txtQH8_03C_W;
	
	
	@FieldAnnotation(orderIndex=60)
	public CheckBoxField chbQH8_03_X;
	@FieldAnnotation(orderIndex=61)
	public TextField txtQH8_03_XO;
	@FieldAnnotation(orderIndex=62)
	public IntegerField txtQH8_03C_X;
	
	@FieldAnnotation(orderIndex=63)
	public CheckBoxField chbQH8_03_Y;
	@FieldAnnotation(orderIndex=64)
	public TextField txtQH8_03_YO;
	@FieldAnnotation(orderIndex=65)
	public IntegerField txtQH8_03C_Y;
	
	@FieldAnnotation(orderIndex=66)
	public CheckBoxField chbQH8_03_Z;
	@FieldAnnotation(orderIndex=67)
	public TextField txtQH8_03_ZO;
	@FieldAnnotation(orderIndex=68)
	public IntegerField txtQH8_03C_Z;

	
	
	@FieldAnnotation(orderIndex=69)
	public CheckBoxField chbQH8_04_A;
	@FieldAnnotation(orderIndex=70)
	public CheckBoxField chbQH8_04_B;
	@FieldAnnotation(orderIndex=71)
	public CheckBoxField chbQH8_04_C;
	@FieldAnnotation(orderIndex=72)
	public CheckBoxField chbQH8_04_D;
	@FieldAnnotation(orderIndex=73)
	public CheckBoxField chbQH8_04_E;
	@FieldAnnotation(orderIndex=74)
	public CheckBoxField chbQH8_04_F;
	@FieldAnnotation(orderIndex=75)
	public CheckBoxField chbQH8_04_G;
	@FieldAnnotation(orderIndex=76)
	public CheckBoxField chbQH8_04_H;
	@FieldAnnotation(orderIndex=77)
	public CheckBoxField chbQH8_04_I;
	@FieldAnnotation(orderIndex=78)
	public CheckBoxField chbQH8_04_J;
	@FieldAnnotation(orderIndex=79)
	public CheckBoxField chbQH8_04_K;
	@FieldAnnotation(orderIndex=80)
	public CheckBoxField chbQH8_04_L;
	@FieldAnnotation(orderIndex=81)
	public CheckBoxField chbQH8_04_M;
	@FieldAnnotation(orderIndex=82)
	public CheckBoxField chbQH8_04_X;
	@FieldAnnotation(orderIndex=83)
	public TextField txtQH8_04_XO;
	
	
	public GridComponent2 gridPregunta04;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesGrabado; 
	
	public LabelComponent lblblanco, lblTitulo1,lblTitulo2,lblpregunta01,lblpregunta02,lblpregunta03,lblpregunta03_ind,lblpregunta04;
	public LabelComponent lblhora_inicial,lblpreg03_1,lblpreg03_2,lblpreg03_3,lblpreg03_4,lblpregunta02_ind;
	
	public LabelComponent lblpre01a_h_ini_t,lblpre05_h_fin_t,lblpre01a_h_ini_f,lblpre05_h_fin_f;
	public ButtonComponent btnqh8_01a_h_ini,btnqh8_05_h_fin;
	private GridComponent2 grid_QH8_01a_h,grid_QH8_05_hf;
	
	
	private HogarService hogarService;
	public Incentivos inc;
	public CuestionarioService  cuestionarioService;
	
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4,q5,q6;
	
	public TextField txtCabecera;
	public HogarFragment_020 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
	public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
	}
	
	@Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH8_01A","QH8_01", "QH8_02_A", "QH8_02_B", "QH8_02_C", "QH8_02_D", "QH8_02_E", "QH8_02_F", "QH8_02_X", "QH8_02_XO", "QH8_03_A", "QH8_03_B", "QH8_03_C", "QH8_03_D", "QH8_03_E", "QH8_03_F", "QH8_03_G", "QH8_03_H", "QH8_03_HO", "QH8_03_I", "QH8_03_IO", "QH8_03_J", "QH8_03_K", "QH8_03_L", "QH8_03_M", "QH8_03_N", "QH8_03_O", "QH8_03_P", "QH8_03_PO", "QH8_03_Q", "QH8_03_QO", "QH8_03_R", "QH8_03_S", "QH8_03_T", "QH8_03_U", "QH8_03_V", "QH8_03_W", "QH8_03_X", "QH8_03_XO", "QH8_03_Y", "QH8_03_YO", "QH8_03_Z", "QH8_03_ZO", "QH8_03C_A", "QH8_03C_B", "QH8_03C_C", "QH8_03C_D", "QH8_03C_E", "QH8_03C_F", "QH8_03C_G", "QH8_03C_H", "QH8_03C_I", "QH8_03C_J", "QH8_03C_K", "QH8_03C_L", "QH8_03C_M", "QH8_03C_N", "QH8_03C_O", "QH8_03C_P", "QH8_03C_Q", "QH8_03C_R", "QH8_03C_S", "QH8_03C_T", "QH8_03C_U", "QH8_03C_V", "QH8_03C_W", "QH8_03C_X", "QH8_03C_Y", "QH8_03C_Z", "QH8_04_A", "QH8_04_B", "QH8_04_C", "QH8_04_D", "QH8_04_E", "QH8_04_F", "QH8_04_G", "QH8_04_H", "QH8_04_I", "QH8_04_J", "QH8_04_K", "QH8_04_L", "QH8_04_M", "QH8_04_X", "QH8_04_XO", "QH8_05", "ID","HOGAR_ID")};  
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH8_01A","QH8_01", "QH8_02_A", "QH8_02_B", "QH8_02_C", "QH8_02_D", "QH8_02_E", "QH8_02_F", "QH8_02_X", "QH8_02_XO", "QH8_03_A", "QH8_03_B", "QH8_03_C", "QH8_03_D", "QH8_03_E", "QH8_03_F", "QH8_03_G", "QH8_03_H", "QH8_03_HO", "QH8_03_I", "QH8_03_IO", "QH8_03_J", "QH8_03_K", "QH8_03_L", "QH8_03_M", "QH8_03_N", "QH8_03_O", "QH8_03_P", "QH8_03_PO", "QH8_03_Q", "QH8_03_QO", "QH8_03_R", "QH8_03_S", "QH8_03_T", "QH8_03_U", "QH8_03_V", "QH8_03_W", "QH8_03_X", "QH8_03_XO", "QH8_03_Y", "QH8_03_YO", "QH8_03_Z", "QH8_03_ZO", "QH8_03C_A", "QH8_03C_B", "QH8_03C_C", "QH8_03C_D", "QH8_03C_E", "QH8_03C_F", "QH8_03C_G", "QH8_03C_H", "QH8_03C_I", "QH8_03C_J", "QH8_03C_K", "QH8_03C_L", "QH8_03C_M", "QH8_03C_N", "QH8_03C_O", "QH8_03C_P", "QH8_03C_Q", "QH8_03C_R", "QH8_03C_S", "QH8_03C_T", "QH8_03C_U", "QH8_03C_V", "QH8_03C_W", "QH8_03C_X", "QH8_03C_Y", "QH8_03C_Z", "QH8_04_A", "QH8_04_B", "QH8_04_C", "QH8_04_D", "QH8_04_E", "QH8_04_F", "QH8_04_G", "QH8_04_H", "QH8_04_I", "QH8_04_J", "QH8_04_K", "QH8_04_L", "QH8_04_M", "QH8_04_X", "QH8_04_XO", "QH8_05")};
//		cargarDatos();
		return rootView; 
    } 

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo1=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_t1).textSize(21).centrar();
		lblTitulo2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_t2).textSize(16).negrita();
		
		lblpregunta01 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_01).textSize(19);
		lblpregunta02 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_02).textSize(19);
		lblpregunta02_ind = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_02_t).textSize(16).negrita();
		lblpregunta03 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_03).textSize(19);
		lblpregunta04 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_04).textSize(19);
		
		
		lblblanco =new LabelComponent(getActivity()).size(altoComponente, 400).text("").textSize(19);
		lblpregunta03_ind = new LabelComponent(getActivity()).size(altoComponente, 110).text(R.string.hogarqh8_03_ind).textSize(15);
		
		Spanned texto03_1 =Html.fromHtml("<b>PARA USO DEL HOGAR</b>");
		lblpreg03_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpreg03_1.setText(texto03_1);
		Spanned texto03_2 =Html.fromHtml("<b>EDUCATIVOS</b>");
		lblpreg03_2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpreg03_2.setText(texto03_2);
		Spanned texto03_3 =Html.fromHtml("<b>INFORMATIVOS</b>");
		lblpreg03_3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpreg03_3.setText(texto03_3);
		Spanned texto03_4 =Html.fromHtml("<b>OTROS BIENES</b>");
		lblpreg03_4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpreg03_4.setText(texto03_4);
		
		
		rgQH8_01=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh8_01_1, R.string.hogarqh8_01_2, R.string.hogarqh8_01_3).size(MATCH_PARENT,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH8_01ChangeValue");
		
		chbQH8_02_A =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_02_B =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_02_C =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).negrita();
		chbQH8_02_D =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).negrita();
		chbQH8_02_E =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).negrita();
		chbQH8_02_F =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).negrita();
		chbQH8_02_X =new CheckBoxField(this.getActivity(), R.string.hogarqh8_02_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).negrita().callback("onQH8_02ChangeValue");
		txtQH8_02_XO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580).centrar();
		
		
		chbQH8_03_A =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03AChangeValue");
		chbQH8_03_B =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03BChangeValue");
		chbQH8_03_C =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03CChangeValue");
		chbQH8_03_D =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03DChangeValue");
		chbQH8_03_E =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03EChangeValue");
		chbQH8_03_F =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03FChangeValue");
		chbQH8_03_G =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03GChangeValue");
		chbQH8_03_H =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03HChangeValue");
		chbQH8_03_I =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03IChangeValue");
		chbQH8_03_J =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03JChangeValue");
		chbQH8_03_K =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03KChangeValue");
		chbQH8_03_L =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03LChangeValue");
		chbQH8_03_M =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03MChangeValue");
		chbQH8_03_N =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03NChangeValue");
		chbQH8_03_O =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_o, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03OChangeValue");
		chbQH8_03_P =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_p, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03PChangeValue");
		chbQH8_03_Q =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_q, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03QChangeValue");
		chbQH8_03_R =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_r, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03RChangeValue");
		chbQH8_03_S =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_s, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03SChangeValue");
		chbQH8_03_T =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_t, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03TChangeValue");
		chbQH8_03_U =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_u, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03UChangeValue");
		chbQH8_03_V =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_v, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03VChangeValue");
		chbQH8_03_W =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_w, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03WChangeValue");
		chbQH8_03_X =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03XChangeValue");
		chbQH8_03_Y =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03YChangeValue");
		chbQH8_03_Z =new CheckBoxField(this.getActivity(), R.string.hogarqh8_03_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_03ZChangeValue");
		
		txtQH8_03_HO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		txtQH8_03_IO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		txtQH8_03_PO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		txtQH8_03_QO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);		
		txtQH8_03_XO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		txtQH8_03_YO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		txtQH8_03_ZO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580);
		
		txtQH8_03C_A=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_B=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_C=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_D=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_E=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_F=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_G=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_H=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_I=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_J=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_K=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_L=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_M=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_N=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_O=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_P=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_Q=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_R=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_S=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_T=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_U=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_V=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_W=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_X=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_Y=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		txtQH8_03C_Z=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2);
		
		
		chbQH8_04_A =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_B =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_C =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_D =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_E =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_F =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_G =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_H =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_I =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_J =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_K =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_L =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_M =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQH8_04_X =new CheckBoxField(this.getActivity(), R.string.hogarqh8_04_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQH8_04ChangeValue");
		txtQH8_04_XO =new TextField(this.getActivity()).maxLength(800).size(altoComponente, 580).centrar();
		
		

		gridPregunta04 = new GridComponent2(this.getActivity(),App.ESTILO,2,0);  
		gridPregunta04.addComponent(lblblanco);
		gridPregunta04.addComponent(lblpregunta03_ind);
		
		gridPregunta04.addComponent(lblpreg03_1,2);
		gridPregunta04.addComponent(chbQH8_03_A);
		gridPregunta04.addComponent(txtQH8_03C_A);
		gridPregunta04.addComponent(chbQH8_03_B);
		gridPregunta04.addComponent(txtQH8_03C_B);
		gridPregunta04.addComponent(chbQH8_03_C);
		gridPregunta04.addComponent(txtQH8_03C_C);
		gridPregunta04.addComponent(chbQH8_03_D);
		gridPregunta04.addComponent(txtQH8_03C_D);
		gridPregunta04.addComponent(chbQH8_03_E);
		gridPregunta04.addComponent(txtQH8_03C_E);
		gridPregunta04.addComponent(chbQH8_03_F);
		gridPregunta04.addComponent(txtQH8_03C_F);
		gridPregunta04.addComponent(chbQH8_03_G);
		gridPregunta04.addComponent(txtQH8_03C_G);
		gridPregunta04.addComponent(chbQH8_03_H);
		gridPregunta04.addComponent(txtQH8_03C_H);
		gridPregunta04.addComponent(txtQH8_03_HO,2);
		
		gridPregunta04.addComponent(chbQH8_03_I);
		gridPregunta04.addComponent(txtQH8_03C_I);
		gridPregunta04.addComponent(txtQH8_03_IO,2);
		
		gridPregunta04.addComponent(lblpreg03_2,2);
		gridPregunta04.addComponent(chbQH8_03_J);
		gridPregunta04.addComponent(txtQH8_03C_J);
		gridPregunta04.addComponent(chbQH8_03_K);
		gridPregunta04.addComponent(txtQH8_03C_K);
		gridPregunta04.addComponent(chbQH8_03_L);
		gridPregunta04.addComponent(txtQH8_03C_L);
		gridPregunta04.addComponent(chbQH8_03_M);
		gridPregunta04.addComponent(txtQH8_03C_M);
		gridPregunta04.addComponent(chbQH8_03_N);
		gridPregunta04.addComponent(txtQH8_03C_N);		
		gridPregunta04.addComponent(chbQH8_03_O);
		gridPregunta04.addComponent(txtQH8_03C_O);
		gridPregunta04.addComponent(chbQH8_03_P);
		gridPregunta04.addComponent(txtQH8_03C_P);
		gridPregunta04.addComponent(txtQH8_03_PO,2);		
		gridPregunta04.addComponent(chbQH8_03_Q);
		gridPregunta04.addComponent(txtQH8_03C_Q);
		gridPregunta04.addComponent(txtQH8_03_QO,2);
		
		gridPregunta04.addComponent(lblpreg03_3,2);
		gridPregunta04.addComponent(chbQH8_03_R);
		gridPregunta04.addComponent(txtQH8_03C_R);
		gridPregunta04.addComponent(chbQH8_03_S);
		gridPregunta04.addComponent(txtQH8_03C_S);
		gridPregunta04.addComponent(chbQH8_03_T);
		gridPregunta04.addComponent(txtQH8_03C_T);
		gridPregunta04.addComponent(chbQH8_03_U);
		gridPregunta04.addComponent(txtQH8_03C_U);
		gridPregunta04.addComponent(chbQH8_03_V);
		gridPregunta04.addComponent(txtQH8_03C_V);
		gridPregunta04.addComponent(chbQH8_03_W);
		gridPregunta04.addComponent(txtQH8_03C_W);
		gridPregunta04.addComponent(chbQH8_03_X);
		gridPregunta04.addComponent(txtQH8_03C_X);
		gridPregunta04.addComponent(txtQH8_03_XO,2);
		
		gridPregunta04.addComponent(lblpreg03_4,2);
		gridPregunta04.addComponent(chbQH8_03_Y);
		gridPregunta04.addComponent(txtQH8_03C_Y);
		gridPregunta04.addComponent(txtQH8_03_YO,2);
		gridPregunta04.addComponent(chbQH8_03_Z);
		gridPregunta04.addComponent(txtQH8_03C_Z);
		gridPregunta04.addComponent(txtQH8_03_ZO,2);
		
		
		lblpre01a_h_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_01a).textSize(17);
		lblpre05_h_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogarqh8_05).textSize(17);
		btnqh8_01a_h_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
		btnqh8_05_h_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
		lblpre01a_h_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
		lblpre05_h_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
	    
		
		btnqh8_01a_h_ini.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre01a_h_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
		btnqh8_05_h_fin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lblpre05_h_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
			}
		});
		    
		grid_QH8_01a_h = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
		grid_QH8_01a_h.addComponent(btnqh8_01a_h_ini);
		grid_QH8_01a_h.addComponent(lblpre01a_h_ini_f);
		 
		grid_QH8_05_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
		grid_QH8_05_hf.addComponent(btnqh8_05_h_fin);
		grid_QH8_05_hf.addComponent(lblpre05_h_fin_f);
	
	}
		
	@Override
	protected View createUI() {
		buildFields();
		 q0 = createQuestionSection(txtCabecera,lblTitulo1, lblTitulo2);
		 q5 = createQuestionSection(lblpre01a_h_ini_t,grid_QH8_01a_h.component());
		 q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta01,rgQH8_01);
		 
		 LinearLayout lyQH8_02_XO = new LinearLayout(getActivity());
		 lyQH8_02_XO.addView(chbQH8_02_X);
		 lyQH8_02_XO.addView(txtQH8_02_XO);			
		 q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta02,lblpregunta02_ind,chbQH8_02_A, chbQH8_02_B, chbQH8_02_C, chbQH8_02_D, chbQH8_02_E, chbQH8_02_F, lyQH8_02_XO);
		
		 q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta03,gridPregunta04.component());
		 
		 LinearLayout lyQH8_04_XO = new LinearLayout(getActivity());
		 lyQH8_04_XO.addView(chbQH8_04_X);
		 lyQH8_04_XO.addView(txtQH8_04_XO);	
		 q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta04,chbQH8_04_A, chbQH8_04_B, chbQH8_04_C, chbQH8_04_D, chbQH8_04_E, chbQH8_04_F, chbQH8_04_G, chbQH8_04_H, chbQH8_04_I, chbQH8_04_J, chbQH8_04_K, chbQH8_04_L, chbQH8_04_M,lyQH8_04_XO);
		 q6 = createQuestionSection(lblpre05_h_fin_t,grid_QH8_05_hf.component());
	
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q5);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q6);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(inc);
		if (inc!=null) {			
			if (inc.qh8_01!=null) {
				inc.qh8_01=inc.setValue3a8(inc.qh8_01);
			}
		}
		
		inc.qh8_01a =lblpre01a_h_ini_f.getText().toString();
		inc.qh8_05 =lblpre05_h_fin_f.getText().toString();
		
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		try {
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(inc, seccionesGrabado);								
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return false;
		}
//		Log.e("gg.id : ",""+inc.id );
//		Log.e("gg.hogar : ",""+inc.hogar_id );
//		Log.e("gg.qh8_01 : ",""+inc.qh8_01 );
		
		return true;
	}
	
	
	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if(lblpre01a_h_ini_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 1A");
			view = btnqh8_01a_h_ini;
			error = true;
			return false;
		}
		
		
		if (Util.esVacio(inc.qh8_01)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta 1"); 
			view = rgQH8_01;
			error = true;
			return false;
		}
		
		if (!Util.esDiferente(inc.qh8_01,1)){
			if(!EsValidoPreg02()){
				mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
				view = chbQH8_02_A;
				error = true;
				return false;
			}
			if(chbQH8_02_X.isChecked() && Util.esVacio(txtQH8_02_XO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_02_XO;
				error = true;
				return false;
			}
			
			
			
			if(!EsValidoPreg03()){
				mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
				view = chbQH8_03_A;
				error = true;
				return false;
			}
			if(chbQH8_03_A.isChecked() && Util.esVacio(txtQH8_03C_A)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_A;
				error = true;
				return false;
			}
			if(chbQH8_03_B.isChecked() && Util.esVacio(txtQH8_03C_B)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_B;
				error = true;
				return false;
			}
			if(chbQH8_03_C.isChecked() && Util.esVacio(txtQH8_03C_C)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_C;
				error = true;
				return false;
			}
			if(chbQH8_03_D.isChecked() && Util.esVacio(txtQH8_03C_D)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_D;
				error = true;
				return false;
			}
			if(chbQH8_03_E.isChecked() && Util.esVacio(txtQH8_03C_E)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_E;
				error = true;
				return false;
			}
			if(chbQH8_03_F.isChecked() && Util.esVacio(txtQH8_03C_F)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_F;
				error = true;
				return false;
			}
			if(chbQH8_03_G.isChecked() && Util.esVacio(txtQH8_03C_G)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_G;
				error = true;
				return false;
			}
			if(chbQH8_03_H.isChecked() && Util.esVacio(txtQH8_03C_H)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_H;
				error = true;
				return false;
			}
			if(chbQH8_03_H.isChecked() && Util.esVacio(txtQH8_03_HO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_HO;
				error = true;
				return false;
			}
			if(chbQH8_03_I.isChecked() && Util.esVacio(txtQH8_03C_I)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03C_I;
				error = true;
				return false;
			}
			if(chbQH8_03_I.isChecked() && Util.esVacio(txtQH8_03_IO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_IO;
				error = true;
				return false;
			}
			
			if(chbQH8_03_J.isChecked() && Util.esVacio(txtQH8_03C_J)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_J;
				error = true;
				return false;
			}
			if(chbQH8_03_K.isChecked() && Util.esVacio(txtQH8_03C_K)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_K;
				error = true;
				return false;
			}
			if(chbQH8_03_L.isChecked() && Util.esVacio(txtQH8_03C_L)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_L;
				error = true;
				return false;
			}
			if(chbQH8_03_M.isChecked() && Util.esVacio(txtQH8_03C_M)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_M;
				error = true;
				return false;
			}
			if(chbQH8_03_N.isChecked() && Util.esVacio(txtQH8_03C_N)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_N;
				error = true;
				return false;
			}
			
			if(chbQH8_03_O.isChecked() && Util.esVacio(txtQH8_03C_O)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_O;
				error = true;
				return false;
			}
			if(chbQH8_03_P.isChecked() && Util.esVacio(txtQH8_03C_P)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_P;
				error = true;
				return false;
			}
			if(chbQH8_03_P.isChecked() && Util.esVacio(txtQH8_03_PO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_PO;
				error = true;
				return false;
			}
			if(chbQH8_03_Q.isChecked() && Util.esVacio(txtQH8_03C_Q)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_Q;
				error = true;
				return false;
			}

			if(chbQH8_03_Q.isChecked() && Util.esVacio(txtQH8_03_QO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_QO;
				error = true;
				return false;
			}
			if(chbQH8_03_R.isChecked() && Util.esVacio(txtQH8_03C_R)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_R;
				error = true;
				return false;
			}
			if(chbQH8_03_S.isChecked() && Util.esVacio(txtQH8_03C_S)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_S;
				error = true;
				return false;
			}
			if(chbQH8_03_T.isChecked() && Util.esVacio(txtQH8_03C_T)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_T;
				error = true;
				return false;
			}
			if(chbQH8_03_U.isChecked() && Util.esVacio(txtQH8_03C_U)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_U;
				error = true;
				return false;
			}
			if(chbQH8_03_V.isChecked() && Util.esVacio(txtQH8_03C_V)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_V;
				error = true;
				return false;
			}
			if(chbQH8_03_W.isChecked() && Util.esVacio(txtQH8_03C_W)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_W;
				error = true;
				return false;
			}
			
			if(chbQH8_03_X.isChecked() && Util.esVacio(txtQH8_03C_X)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_X;
				error = true;
				return false;
			}
			if(chbQH8_03_X.isChecked() && Util.esVacio(txtQH8_03_XO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_XO;
				error = true;
				return false;
			}
			if(chbQH8_03_Y.isChecked() && Util.esVacio(txtQH8_03C_Y)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_Y;
				error = true;
				return false;
			}
			if(chbQH8_03_Y.isChecked() && Util.esVacio(txtQH8_03_YO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_YO;
				error = true;
				return false;
			}
			if(chbQH8_03_Z.isChecked() && Util.esVacio(txtQH8_03C_Z)){
				mensaje = preguntaVacia.replace("$", "Debe registrar cantidad");
				view = txtQH8_03C_Z;
				error = true;
				return false;
			}
			if(chbQH8_03_Z.isChecked() && Util.esVacio(txtQH8_03_ZO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_03_ZO;
				error = true;
				return false;
			}
			
			
			if(!EsValidoPreg04()){
				mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta");
				view = chbQH8_04_A;
				error = true;
				return false;
			}
			if(chbQH8_04_X.isChecked() && Util.esVacio(txtQH8_04_XO)){
				mensaje = preguntaVacia.replace("$", "Debe registrar especifique");
				view = txtQH8_04_XO;
				error = true;
				return false;
			}
		}
		
		if(lblpre05_h_fin_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 5");
			view = btnqh8_05_h_fin	;
			error = true;
			return false;
		}
		
		
		return true;
	}
	
	
	
	@Override
	public void cargarDatos() {
		inc = getCuestionarioService().getIncentivos(App.getInstance().getIncentivos().id,App.getInstance().getIncentivos().hogar_id,seccionesCargado);		
		if (inc == null) {
			inc = new Incentivos();
			inc.id = App.getInstance().getIncentivos().id;
			inc.hogar_id =App.getInstance().getIncentivos().hogar_id;
			
			
		}
		if (inc!=null) {
			if (inc.qh8_01!=null) {
				inc.qh8_01=inc.getValue8a3(inc.qh8_01);
			}
		}
		if(inc.qh8_01a!=null) {lblpre01a_h_ini_f.setText(inc.qh8_01a);}
		if(inc.qh8_05!=null) {lblpre05_h_fin_f.setText(inc.qh8_05);}

		
//		Log.e("ccx.id : ",""+App.getInstance().getIncentivos().id );
//		Log.e("ccx.hogar : ",""+App.getInstance().getIncentivos().hogar_id );
//		Log.e("cc.qh8_01 : ",""+inc.qh8_01 );
		
		entityToUI(inc);
		inicio();
	}
	
	
	
	public void inicio(){
		onQH8_01ChangeValue();		
		ValidarsiesSupervisora();
	}
	
	
	
	public void onQH8_01ChangeValue(){
		if (MyUtil.incluyeRango(1,1,rgQH8_01.getTagSelected("").toString()) ) {
			Util.lockView(getActivity(), false, chbQH8_02_A, chbQH8_02_B, chbQH8_02_C, chbQH8_02_D, chbQH8_02_E, chbQH8_02_F, chbQH8_02_X, txtQH8_02_XO, chbQH8_03_A, chbQH8_03_B, chbQH8_03_C, chbQH8_03_D, chbQH8_03_E, chbQH8_03_F, chbQH8_03_G, chbQH8_03_H, chbQH8_03_I, chbQH8_03_J, chbQH8_03_K, chbQH8_03_L, chbQH8_03_M, chbQH8_03_N, chbQH8_03_O, chbQH8_03_P, chbQH8_03_Q, chbQH8_03_R, chbQH8_03_S, chbQH8_03_T, chbQH8_03_U, chbQH8_03_V, chbQH8_03_W, chbQH8_03_X, chbQH8_03_Y, chbQH8_03_Z, 
					txtQH8_03_HO, txtQH8_03_IO, txtQH8_03_PO, txtQH8_03_QO, txtQH8_03_XO, txtQH8_03_YO, txtQH8_03_ZO, 
					txtQH8_03C_A, txtQH8_03C_B, txtQH8_03C_C, txtQH8_03C_D, txtQH8_03C_E, txtQH8_03C_F, txtQH8_03C_G, txtQH8_03C_H, txtQH8_03C_I, txtQH8_03C_J, txtQH8_03C_K, txtQH8_03C_L, txtQH8_03C_M, txtQH8_03C_N, txtQH8_03C_O, txtQH8_03C_P, txtQH8_03C_Q, txtQH8_03C_R, txtQH8_03C_S, txtQH8_03C_T, txtQH8_03C_U, txtQH8_03C_V, txtQH8_03C_W, txtQH8_03C_X, txtQH8_03C_Y, txtQH8_03C_Z, 
					chbQH8_04_A, chbQH8_04_B, chbQH8_04_C, chbQH8_04_D, chbQH8_04_E, chbQH8_04_F, chbQH8_04_G, chbQH8_04_H, chbQH8_04_I, chbQH8_04_J, chbQH8_04_K, chbQH8_04_L, chbQH8_04_M, chbQH8_04_X, txtQH8_04_XO);
			
			onQH8_02ChangeValue();
			onQH8_03AChangeValue();
			onQH8_03BChangeValue();
			onQH8_03CChangeValue();
			onQH8_03DChangeValue();
			onQH8_03EChangeValue();
			onQH8_03FChangeValue();
			onQH8_03GChangeValue();
			onQH8_03HChangeValue();
			onQH8_03IChangeValue();
			onQH8_03JChangeValue();
			onQH8_03KChangeValue();
			onQH8_03LChangeValue();
			onQH8_03MChangeValue();
			onQH8_03NChangeValue();
			onQH8_03OChangeValue();
			onQH8_03PChangeValue();
			onQH8_03QChangeValue();
			onQH8_03RChangeValue();
			onQH8_03SChangeValue();
			onQH8_03TChangeValue();
			onQH8_03UChangeValue();
			onQH8_03VChangeValue();
			onQH8_03WChangeValue();
			onQH8_03XChangeValue();
			onQH8_03YChangeValue();
			onQH8_03ZChangeValue();
			onQH8_04ChangeValue();		
			
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
		}
		else{
			Util.cleanAndLockView(getActivity(), 
					chbQH8_02_A, chbQH8_02_B, chbQH8_02_C, chbQH8_02_D, chbQH8_02_E, chbQH8_02_F, chbQH8_02_X, txtQH8_02_XO, 
					chbQH8_03_A, chbQH8_03_B, chbQH8_03_C, chbQH8_03_D, chbQH8_03_E, chbQH8_03_F, chbQH8_03_G, chbQH8_03_H, chbQH8_03_I, chbQH8_03_J, chbQH8_03_K, chbQH8_03_L, chbQH8_03_M, chbQH8_03_N, chbQH8_03_O, chbQH8_03_P, chbQH8_03_Q, chbQH8_03_R, chbQH8_03_S, chbQH8_03_T, chbQH8_03_U, chbQH8_03_V, chbQH8_03_W, chbQH8_03_X, chbQH8_03_Y, chbQH8_03_Z, 
					txtQH8_03_HO, txtQH8_03_IO, txtQH8_03_PO, txtQH8_03_QO, txtQH8_03_XO, txtQH8_03_YO, txtQH8_03_ZO, 
					txtQH8_03C_A, txtQH8_03C_B, txtQH8_03C_C, txtQH8_03C_D, txtQH8_03C_E, txtQH8_03C_F, txtQH8_03C_G, txtQH8_03C_H, txtQH8_03C_I, txtQH8_03C_J, txtQH8_03C_K, txtQH8_03C_L, txtQH8_03C_M, txtQH8_03C_N, txtQH8_03C_O, txtQH8_03C_P, txtQH8_03C_Q, txtQH8_03C_R, txtQH8_03C_S, txtQH8_03C_T, txtQH8_03C_U, txtQH8_03C_V, txtQH8_03C_W, txtQH8_03C_X, txtQH8_03C_Y, txtQH8_03C_Z, 
					chbQH8_04_A, chbQH8_04_B, chbQH8_04_C, chbQH8_04_D, chbQH8_04_E, chbQH8_04_F, chbQH8_04_G, chbQH8_04_H, chbQH8_04_I, chbQH8_04_J, chbQH8_04_K, chbQH8_04_L, chbQH8_04_M, chbQH8_04_X, txtQH8_04_XO );
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
		}
	}
	

	
	public void onQH8_02ChangeValue(){
		if(chbQH8_02_X.isChecked()){
			Util.lockView(getActivity(), false, txtQH8_02_XO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_02_XO );
		}
	}
	
	public void onQH8_04ChangeValue(){
		if(chbQH8_04_X.isChecked()){
			Util.lockView(getActivity(), false, txtQH8_04_XO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_04_XO );
		}
	}

	
	
	public void onQH8_03AChangeValue(){if(chbQH8_03_A.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_A);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_A);}}
	public void onQH8_03BChangeValue(){if(chbQH8_03_B.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_B);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_B);}}
	public void onQH8_03CChangeValue(){if(chbQH8_03_C.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_C);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_C);}}
	public void onQH8_03DChangeValue(){if(chbQH8_03_D.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_D);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_D);}}
	public void onQH8_03EChangeValue(){if(chbQH8_03_E.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_E);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_E);}}
	public void onQH8_03FChangeValue(){if(chbQH8_03_F.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_F);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_F);}}
	public void onQH8_03GChangeValue(){if(chbQH8_03_G.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_G);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_G);}}
	public void onQH8_03HChangeValue(){
		if(chbQH8_03_H.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_H, txtQH8_03_HO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_H, txtQH8_03_HO );
		}
	}
	public void onQH8_03IChangeValue(){
		if(chbQH8_03_I.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_I,txtQH8_03_IO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_I,txtQH8_03_IO);
		}
	}
	public void onQH8_03JChangeValue(){if(chbQH8_03_J.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_J);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_J);}}
	public void onQH8_03KChangeValue(){if(chbQH8_03_K.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_K);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_K);}}
	public void onQH8_03LChangeValue(){if(chbQH8_03_L.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_L);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_L);}}
	public void onQH8_03MChangeValue(){if(chbQH8_03_M.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_M);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_M);}}
	public void onQH8_03NChangeValue(){if(chbQH8_03_N.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_N);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_N);}}
	public void onQH8_03OChangeValue(){if(chbQH8_03_O.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_O);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_O);}}

	public void onQH8_03PChangeValue(){
		if(chbQH8_03_P.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_P,txtQH8_03_PO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_P,txtQH8_03_PO);
		}
	}
	public void onQH8_03QChangeValue(){
		if(chbQH8_03_Q.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_Q,txtQH8_03_QO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_Q,txtQH8_03_QO);
		}
	}
	public void onQH8_03RChangeValue(){if(chbQH8_03_R.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_R);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_R);}}
	public void onQH8_03SChangeValue(){if(chbQH8_03_S.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_S);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_S);}}
	public void onQH8_03TChangeValue(){if(chbQH8_03_T.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_T);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_T);}}
	public void onQH8_03UChangeValue(){if(chbQH8_03_U.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_U);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_U);}}
	public void onQH8_03VChangeValue(){if(chbQH8_03_V.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_V);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_V);}}
	public void onQH8_03WChangeValue(){if(chbQH8_03_W.isChecked()){Util.lockView(getActivity(), false,txtQH8_03C_W);}else{Util.cleanAndLockView(getActivity(), txtQH8_03C_W);}}
	public void onQH8_03XChangeValue(){
		if(chbQH8_03_X.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_X,txtQH8_03_XO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_X,txtQH8_03_XO);
		}
	}
	public void onQH8_03YChangeValue(){
		if(chbQH8_03_Y.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_Y,txtQH8_03_YO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_Y,txtQH8_03_YO);
		}
	}
	public void onQH8_03ZChangeValue(){
		if(chbQH8_03_Z.isChecked()){
			Util.lockView(getActivity(), false,txtQH8_03C_Z,txtQH8_03_ZO);
		}
		else{
			Util.cleanAndLockView(getActivity(), txtQH8_03C_Z,txtQH8_03_ZO);
		}
	}
	
	
	
	
	public boolean EsValidoPreg02(){
		if(!chbQH8_02_A.isChecked() && !chbQH8_02_B.isChecked() && !chbQH8_02_C.isChecked() && !chbQH8_02_D.isChecked() && !chbQH8_02_E.isChecked() && !chbQH8_02_F.isChecked() && !chbQH8_02_X.isChecked())
			return false;
		else 
			return true;
	}
	
	public boolean EsValidoPreg03(){
		if(!chbQH8_03_A.isChecked() && !chbQH8_03_B.isChecked() && !chbQH8_03_C.isChecked() && !chbQH8_03_D.isChecked() && !chbQH8_03_E.isChecked() && !chbQH8_03_F.isChecked() && !chbQH8_03_G.isChecked() && !chbQH8_03_H.isChecked() && !chbQH8_03_I.isChecked() && !chbQH8_03_J.isChecked() && !chbQH8_03_K.isChecked() && !chbQH8_03_L.isChecked() && !chbQH8_03_M.isChecked() && !chbQH8_03_N.isChecked() && !chbQH8_03_O.isChecked() && !chbQH8_03_P.isChecked() && !chbQH8_03_Q.isChecked() && !chbQH8_03_R.isChecked() && !chbQH8_03_S.isChecked() && !chbQH8_03_T.isChecked() && !chbQH8_03_U.isChecked() && !chbQH8_03_V.isChecked() && !chbQH8_03_W.isChecked() && !chbQH8_03_X.isChecked() && !chbQH8_03_Y.isChecked() && !chbQH8_03_Z.isChecked())
			return false;
		else 
			return true;
	}
	
	public boolean EsValidoPreg04(){
		if(!chbQH8_04_A.isChecked() && !chbQH8_04_B.isChecked() && !chbQH8_04_C.isChecked() && !chbQH8_04_D.isChecked() && !chbQH8_04_E.isChecked() && !chbQH8_04_F.isChecked() && !chbQH8_04_G.isChecked() && !chbQH8_04_H.isChecked() && !chbQH8_04_I.isChecked() && !chbQH8_04_J.isChecked() && !chbQH8_04_K.isChecked() && !chbQH8_04_L.isChecked() && !chbQH8_04_M.isChecked() && !chbQH8_04_X.isChecked())
			return false;
		else 
			return true;
	}
	 
	


	 public void ValidarsiesSupervisora(){
			Integer codigo=App.getInstance().getUsuario().cargo_id;
			if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
				rgQH8_01.readOnly();
				chbQH8_02_A.readOnly();
				chbQH8_02_B.readOnly();
				chbQH8_02_C.readOnly();
				chbQH8_02_D.readOnly();
				chbQH8_02_E.readOnly();
				chbQH8_02_F.readOnly();
				chbQH8_02_X.readOnly();
				txtQH8_02_XO.readOnly();
				
				chbQH8_03_A.readOnly();
				chbQH8_03_B.readOnly();
				chbQH8_03_C.readOnly();
				chbQH8_03_D.readOnly();
				chbQH8_03_E.readOnly();
				chbQH8_03_F.readOnly();
				chbQH8_03_G.readOnly();
				chbQH8_03_H.readOnly();
				chbQH8_03_I.readOnly();
				chbQH8_03_J.readOnly();
				chbQH8_03_K.readOnly();
				chbQH8_03_L.readOnly();
				chbQH8_03_M.readOnly();
				chbQH8_03_N.readOnly();
				chbQH8_03_O.readOnly();
				chbQH8_03_P.readOnly();
				chbQH8_03_Q.readOnly();
				chbQH8_03_R.readOnly();
				chbQH8_03_S.readOnly();
				chbQH8_03_T.readOnly();
				chbQH8_03_U.readOnly();
				chbQH8_03_V.readOnly();
				chbQH8_03_W.readOnly();
				chbQH8_03_X.readOnly();
				chbQH8_03_Y.readOnly();
				chbQH8_03_Z.readOnly();
				
				txtQH8_03_HO.readOnly();
				txtQH8_03_IO.readOnly();
				txtQH8_03_PO.readOnly();
				txtQH8_03_QO.readOnly();				
				txtQH8_03_XO.readOnly();
				txtQH8_03_YO.readOnly();
				txtQH8_03_ZO.readOnly();
				
				txtQH8_03C_A.readOnly();
				txtQH8_03C_B.readOnly();
				txtQH8_03C_C.readOnly();
				txtQH8_03C_D.readOnly();
				txtQH8_03C_E.readOnly();
				txtQH8_03C_F.readOnly();
				txtQH8_03C_G.readOnly();
				txtQH8_03C_H.readOnly();
				txtQH8_03C_I.readOnly();
				txtQH8_03C_J.readOnly();
				txtQH8_03C_K.readOnly();
				txtQH8_03C_L.readOnly();
				txtQH8_03C_M.readOnly();
				txtQH8_03C_N.readOnly();
				txtQH8_03C_O.readOnly();
				txtQH8_03C_P.readOnly();
				txtQH8_03C_Q.readOnly();
				txtQH8_03C_R.readOnly();
				txtQH8_03C_S.readOnly();
				txtQH8_03C_T.readOnly();
				txtQH8_03C_U.readOnly();
				txtQH8_03C_V.readOnly();
				txtQH8_03C_W.readOnly();
				txtQH8_03C_X.readOnly();
				txtQH8_03C_Y.readOnly();
				txtQH8_03C_Z.readOnly();
				
				chbQH8_04_A.readOnly();
				chbQH8_04_B.readOnly();
				chbQH8_04_C.readOnly();
				chbQH8_04_D.readOnly();
				chbQH8_04_E.readOnly();
				chbQH8_04_F.readOnly();
				chbQH8_04_G.readOnly();
				chbQH8_04_H.readOnly();
				chbQH8_04_I.readOnly();
				chbQH8_04_J.readOnly();
				chbQH8_04_K.readOnly();
				chbQH8_04_L.readOnly();
				chbQH8_04_M.readOnly();
				chbQH8_04_X.readOnly();
				txtQH8_04_XO.readOnly();		
				btnqh8_01a_h_ini.setEnabled(false);
				btnqh8_05_h_fin.setEnabled(false);
				
			}
	}
	 
	 
	public CuestionarioService getCuestionarioService() {
			if (cuestionarioService == null) {
				cuestionarioService = CuestionarioService.getInstance(getActivity());
			}
			return cuestionarioService;

	}
		
	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	 
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}
