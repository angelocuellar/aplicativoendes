package gob.inei.endes2024.fragment.hogar; 
import java.sql.SQLException; 

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;
import gob.inei.dnce.annotations.FieldAnnotation; 
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.FragmentForm; 
import gob.inei.dnce.components.LabelComponent; 
import gob.inei.dnce.components.MasterActivity; 
import gob.inei.dnce.components.RadioGroupOtherField; 
import gob.inei.dnce.components.TextField; 
import gob.inei.dnce.components.ToastMessage; 
import gob.inei.dnce.util.Util; 
import android.os.Bundle; 
import android.view.Gravity; 
import android.view.LayoutInflater; 
import android.view.View; 
import android.view.ViewGroup; 
import android.widget.LinearLayout; 
import android.widget.ScrollView; 

public class HogarFragment_005 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH62; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQH62_O; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH63; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQH64; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQH64_O; 
	Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta62,lblpregunta63,lblpregunta64;
	public boolean activar;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	SeccionCapitulo[] seccionesGrabado,seccionesGrabadoqh62,seccionesGrabadoqh62qh64; 
	SeccionCapitulo[] seccionesCargado; 

	public HogarFragment_005() {} 
	public HogarFragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH62","QH62_O","QH63","QH64","QH64_O","ID","HOGAR_ID","PERSONA_INFORMANTE_ID","QH61I","QH61J")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH62","QH62_O","QH63","QH64","QH64_O")};
		seccionesGrabadoqh62 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH62","QH62_O","QH63","QH64","QH64_O","QH66","QH68")}; 
		seccionesGrabadoqh62qh64 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH62","QH62_O","QH63","QH64","QH64_O","QH66")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar).textSize(21).centrar().negrita(); 
		rgQH62=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh62_1,R.string.hogarqh62_2,R.string.hogarqh62_3,R.string.hogarqh62_4,R.string.hogarqh62_5,R.string.hogarqh62_6,R.string.hogarqh62_7,R.string.hogarqh62_8,R.string.hogarqh62_9,R.string.hogarqh62_10,R.string.hogarqh62_11,R.string.hogarqh62_12).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH62ChangeValue"); 
		txtQH62_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH62.agregarEspecifique(11,txtQH62_O); 
		rgQH63=new RadioGroupOtherField(this.getActivity(),R.string.rb_si, R.string.rb_no).size(50,200).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQH63ChangeValue");		
		rgQH64=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh64_1,R.string.hogarqh64_2,R.string.hogarqh64_3,R.string.hogarqh64_4,R.string.hogarqh64_5,R.string.hogarqh64_6,R.string.hogarqh64_7,R.string.hogarqh64_8,R.string.hogarqh64_9,R.string.hogarqh64_10,R.string.hogarqh64_11).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQH64_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQH64.agregarEspecifique(10,txtQH64_O); 
		
		lblpregunta62 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh62).textSize(19);
		lblpregunta63 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh63).textSize(19);
		lblpregunta64 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.hogarqh64).textSize(19);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta62,rgQH62); 
		q2 = createQuestionSection(lblpregunta63,rgQH63); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta64,rgQH64); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
    return contenedor; 
    } 
    @Override 
    public boolean grabar() {	
		uiToEntity(hogar); 
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh62!=null) {
				hogar.qh62=hogar.getConvert62(hogar.qh62);
			}
			if (hogar.qh64!=null) {
				hogar.qh64=hogar.getConvert64(hogar.qh64);
			}
			boolean flag=false;
		
			
			if (Util.esDiferente(hogar.qh63,1)) {// es null o 2		
	    		if (!Util.esDiferente(hogar.qh63,2)){
	    			if (!Util.esDiferente(hogar.qh62,1,2,3,4)) {	
//	    				Log.e("aa","11");
						flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoqh62qh64);
	    			}
	    			else{	    		
//	    				Log.e("aa","22");
						flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado);
	    			}
				}
	    		else{	    		
//	    			Log.e("aaa","33");
	    			flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoqh62);
	    		}   	
			}
	    	else{// es 1
	    		if (!Util.esDiferente(hogar.qh62,1,2,3,4) && !Util.esDiferente(hogar.qh64,1,2,3,4,96)) {
//	    			Log.e("aaa","44");
					flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoqh62qh64);
				}
				else{				
//					Log.e("aaa","55");
					flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado);
				}    		
	    	}
					
			if(!flag){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 	
		
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(hogar.qh62)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.62"); 
			view = rgQH62; 
			error = true; 
			return false; 
		} 
		if(!Util.esDiferente(hogar.qh62,12)){ 
			if (Util.esVacio(hogar.qh62_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQH62_O; 
				error = true; 
				return false; 
			} 
		} 
		if (Util.esDiferente(hogar.qh62,11)) {
			if (Util.esVacio(hogar.qh63)) {
				mensaje = preguntaVacia.replace("$", "La pregunta P.63");
				view = rgQH63;
				error = true;
				return false;	
			}
			if (Util.esDiferente(hogar.qh63,2)) {
				if (Util.esVacio(hogar.qh64)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.64");
					view = rgQH64;
					error = true;
					return false;	
				}	
				if(!Util.esDiferente(hogar.qh64,11)){ 
					if (Util.esVacio(hogar.qh64_o)) { 
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQH64_O; 
						error = true; 
						return false; 
					} 
				}
			} 
		}
		
		if (hogar.qh62!=null && hogar.qh64!=null) {
			if (!Util.esDiferente(hogar.qh62,hogar.qh64)) {
				mensaje = ("El combustible no pueden ser iguales"); 
				view = rgQH64; 
				error = true; 
				return false; 
			}
		}
		
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
		hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargado); 
		if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id;  
	    } 
		if (hogar.qh62!=null) {
			hogar.qh62=hogar.setConvert62(hogar.qh62);
		}
		if (hogar.qh64!=null) {
			hogar.qh64=hogar.setConvert64(hogar.qh64);
		}
		entityToUI(hogar); 	
		inicio(); 
    } 

    private void inicio() {   
    	onrgQH62ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH62.readOnly();
			txtQH62_O.readOnly();
			rgQH63.readOnly();
			rgQH64.readOnly();
			txtQH64_O.readOnly();
		}
	}
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public void onrgQH62ChangeValue() { 
    	if (MyUtil.incluyeRango(11,11,rgQH62.getTagSelected("").toString())) {    	
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQH63,rgQH64);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);	
		}
    	else{
    		Util.lockView(getActivity(), false, rgQH63,rgQH64);	
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);	
			rgQH63.requestFocus();
			onrgQH63ChangeValue();
    	}
    }
    
    public void onrgQH63ChangeValue() {   
  		if (MyUtil.incluyeRango(1,1,rgQH63.getTagSelected("").toString())) {
  			Util.lockView(getActivity(), false,rgQH64);	
			q3.setVisibility(View.VISIBLE);
			rgQH64.requestFocus();
		}
		else{
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),rgQH64);
			q3.setVisibility(View.GONE);
		}
    }
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try { 
			if (hogar.qh62!=null) {
				hogar.qh62=hogar.getConvert62(hogar.qh62);
			}
			if (hogar.qh64!=null) {
				hogar.qh64=hogar.getConvert64(hogar.qh64);
			}
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado);
		
			if(!flag){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		}
		return App.HOGAR;
		
	}   
} 
