package gob.inei.endes2024.fragment.hogar;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TableComponent.ALIGN;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HogarFragment_013_Antr extends FragmentForm  {
	public TableComponent tcMef,tcMenoresaSeisanios,tcMenoresaDoceanios,tcReporte, tcpesoytalla, tchemoglobina;
	public GridComponent2 gridResultado,gridResultados, gridseccion09;
	public LabelComponent lblTitulo,lblnombredomestica,lblviolenciadomestica,lblnombreInformante,lblInformanteSalud,lblDatosdeControl,lblTotalPersonas,lblTotalPersonasvalor ,lblmefnro,lblmefnrovalor,lbldocenro,lbldocenrovalor,lblniniosdocenro,lblniniosdocenrovalor,lblniniosseisnro,lblniniosseisnrovalor,lblninioscinconro,lblninioscinconrovalor,lblniniostresnro,lblniniostresnrovalor,lblinformantehogarnro,lblinformantehogarnrovalor,lblinformantesaludnro,lblinformantesaludnrovalor,
	lblnumorden, lblnombre, lblpeso, lbltalla,lblsistolica1, lbldiastolica1,lblsistolica2 , lbldiastolica2, lblperimetro, lblnumordendetalle, lblnombredetalle, lblpesodetalle, lbltalladetalle,
	lblsistolica1detalle,lbldiastolica1detalle, lblsistolica2detalle, lbldiastolica2detalle, lblperimetrodetalle ,
	lblresultado1,lblresultado2, lblresultado3, lblresultado1detalle, lblresultado2detalle, lblresultado3detalle, lbledad, lbledadetalle ;
	public List<Seccion01> ListadoMef,ListMenoresaSeis,ListMenoresaDoce;
	public Seccion01Service seccion01;
	SeccionCapitulo[] seccionesGrabadoHogar;
	private HogarService hogarService;
	public VisitaService visita;
	public Seccion01 informantedesalud,mefdeviolenciafamiliar;
	public List<Seccion01> posiblesinformantessalud;
	public CuestionarioService  cuestionarioService;
	
////ggg//	public Seccion01 informantehogar;
//	
//	public List<Seccion04_05> detalles04_05;
//	private Seccion04_05Service Persona04_05Service;
	
	CAP04_07 cap08_09; 
	
	public Seccion04_05 seleccion; 
	Seccion04_05 secccion04_05; 
	public Seccion01 informantehogar;
	Hogar hogar;
	public List<Seccion01> detalles;
	public List<Seccion04_05> detalles04_05;
	private Seccion01Service Personaservice;
	private Seccion04_05Service Persona04_05Service;
	
	
	LinearLayout q0; 
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	
	SeccionCapitulo[] seccionesCargadoSeccion04, seccionesCargado;
	//public List<Seccion01> detalles;
	public TextField txtCabecera;
	public HogarFragment_013_Antr parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	}
	public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
	}
	@Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 

		listening();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QS900_REF_S9", "QS900A", "QS902A", "QS903A", "QS906A", "QS907A", "QS908A",  "QS900","QS901","QS902","QS902_O","QS903","QS903_1","QS904H","QS904M","QS905","QS905_1","QS906","QS906_O","QS907","QS908","QS908_O",  "QS908B_A", "QS908B_B", "QS908C", "QS908C1", "QS908C2", "QS908C3","QS908D", "QS908D1", "QSREC_09","QSFRC_FECHA09","QSNOM_ANT","QSCOD_ANT","QSNOM_AUX","QSCOD_AUX","QSOBS_ANTRO","QSOBS_ENTREV","QSOBS_SUPER","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QH02_1","QH07","QH204","QH205","QH205C","QH205C1" ,"QH7DD","QH7MM","QH203Y","QH06","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","QH208","QH209","QH210","QH211","QH212AH","QH212AM", "QH212GH","QH212GM" ,  "QH212B_D","QH212B_M","QH212B_Y" ,"QH212H_D","QH212H_M","QH212H_Y","QH212F1","QH212F","QH213","S4DIA","S4MES","S4ANIO","ID","HOGAR_ID","PERSONA_ID","ESTADOS5","QHREC_05")}; 
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHVIOLEN") };
		cargarDatos();
		return rootView; 
    } 

	@Override
	protected void buildFields() {
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text("REPORTE DE MEDICIONES").textSize(21).centrar().negrita();
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMef= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcMenoresaSeisanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcReporte= new TableComponent(getActivity(),this,App.ESTILO).size(500, 1200).headerHeight(altoComponente+10).dataColumHeight(109);
//			tcMenoresaDoceanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//		}
//		else{
			tcMef= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcMenoresaSeisanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 680).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcReporte= new TableComponent(getActivity(),this,App.ESTILO).size(500, 1200).headerHeight(45).dataColumHeight(80).headerTextSize(15);
			tcMenoresaDoceanios= new TableComponent(getActivity(),this,App.ESTILO).size(300, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tchemoglobina = new TableComponent(getActivity(),this,App.ESTILO).size(500, 1670).headerHeight(55).dataColumHeight(50).headerTextSize(15);
//		}
		
		tcMef.addHeader(R.string.seccion01_nro_orden,1f);
		tcMef.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMef.addHeader(R.string.seccion01_edad,1f);
		tcMef.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		
		lblviolenciadomestica = new LabelComponent(this.getActivity()).text(R.string.mefviolenciaDomestica).size(altoComponente+10, 380) .negrita();
		lblInformanteSalud = new LabelComponent(this.getActivity()).text(R.string.informantesalud).size(altoComponente+10, 380) .negrita();
		lblnombredomestica = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 200) .negrita();
		lblnombreInformante = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 200) .negrita();
		
		gridResultado= new GridComponent2(this.getActivity(),2);
		gridResultado.addComponent(lblviolenciadomestica);
		gridResultado.addComponent(lblnombredomestica);
		gridResultado.addComponent(lblInformanteSalud);
		gridResultado.addComponent(lblnombreInformante);
		
		
		lblnumorden = new LabelComponent(this.getActivity(),App.ESTILO2).text("N�\nDE\nORDEN").size(altoComponente+10, 20).negrita().centrar();
		lblnombre = new LabelComponent(this.getActivity(),App.ESTILO2).text("NOMBRE").size(altoComponente+10, 80).negrita().centrar();
		lbledad = new LabelComponent(this.getActivity(),App.ESTILO2).text("EDAD").size(altoComponente+10, 60).negrita().centrar();
		lblpeso =  new LabelComponent(this.getActivity(),App.ESTILO2).text("PESO\n(kg)").size(altoComponente+10, 80).negrita().centrar();
		lbltalla =  new LabelComponent(this.getActivity(),App.ESTILO2).text("TALLA\n(cm)").size(altoComponente+10, 80).negrita().centrar();
		lblresultado1 = new LabelComponent(this.getActivity(),App.ESTILO2).text("RESULTADO\nANTROP.").size(altoComponente+10, 110).negrita().centrar();
		lblsistolica1 =  new LabelComponent(this.getActivity(),App.ESTILO2).text("SIST�LICA\nM. 1").size(altoComponente+10, 80).negrita().centrar();
		lbldiastolica1 =  new LabelComponent(this.getActivity(),App.ESTILO2).text("DIAST�LICA\nM. 1").size(altoComponente+10, 110).negrita().centrar();
		lblsistolica2 =  new LabelComponent(this.getActivity(),App.ESTILO2).text("SIST�LICA\nM. 2").size(altoComponente+10, 80).negrita().centrar();
		lbldiastolica2 =  new LabelComponent(this.getActivity(),App.ESTILO2).text("DIAST�LICA\nM. 2").size(altoComponente+10, 110).negrita().centrar();
		lblresultado2 = new LabelComponent(this.getActivity(),App.ESTILO2).text("RESULTADO\nP.A.").size(altoComponente+10, 110).negrita().centrar();
		lblperimetro =  new LabelComponent(this.getActivity(),App.ESTILO2).text("PER�METRO").size(altoComponente+10, 110).negrita().centrar();
		lblresultado3 = new LabelComponent(this.getActivity(),App.ESTILO2).text("RESULTADO\nPAB.").size(altoComponente+10, 110).negrita().centrar();
		
		lblnumordendetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblnombredetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100);
		lbledadetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 60);
		lblpesodetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lbltalladetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblresultado1detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblsistolica1detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lbldiastolica1detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblsistolica2detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lbldiastolica2detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblresultado2detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblperimetrodetalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		lblresultado3detalle = new LabelComponent(this.getActivity()).text("").size(altoComponente+10, 100).centrar();
		
		gridseccion09= new GridComponent2(this.getActivity(),13);
		gridseccion09.addComponent(lblnumorden);
		gridseccion09.addComponent(lblnombre);
		gridseccion09.addComponent(lbledad);
		gridseccion09.addComponent(lblpeso);
		gridseccion09.addComponent(lbltalla);
		gridseccion09.addComponent(lblresultado1);
		gridseccion09.addComponent(lblsistolica1);
		gridseccion09.addComponent(lbldiastolica1);
		gridseccion09.addComponent(lblsistolica2);
		gridseccion09.addComponent(lbldiastolica2);
		gridseccion09.addComponent(lblresultado2);
		gridseccion09.addComponent(lblperimetro);
		gridseccion09.addComponent(lblresultado3);
		
		gridseccion09.addComponent(lblnumordendetalle);
		gridseccion09.addComponent(lblnombredetalle);
		gridseccion09.addComponent(lbledadetalle);
		gridseccion09.addComponent(lblpesodetalle);
		gridseccion09.addComponent(lbltalladetalle);
		gridseccion09.addComponent(lblresultado1detalle);
		gridseccion09.addComponent(lblsistolica1detalle);
		gridseccion09.addComponent(lbldiastolica1detalle);
		gridseccion09.addComponent(lblsistolica2detalle);
		gridseccion09.addComponent(lbldiastolica2detalle);
		gridseccion09.addComponent(lblresultado2detalle);
		gridseccion09.addComponent(lblperimetrodetalle);
		gridseccion09.addComponent(lblresultado3detalle);
		
		
		tcMenoresaSeisanios.addHeader(R.string.seccion01_nro_orden,1f);
		tcMenoresaSeisanios.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMenoresaSeisanios.addHeader(R.string.seccion01_edad,1f);
		tcMenoresaSeisanios.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		
	
		tchemoglobina.addHeader(R.string.m_orden_b18_hogar, 1.5f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_nombre_hogar, 2f,TableComponent.ALIGN.LEFT);
		tchemoglobina.addHeader(R.string.m_edad_meses_hogar, 1.7f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_edad_hogar, 1.2f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.f_nacimiento, 2.5f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_peso_hogar, 1.5f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_talla_hogar, 1.9f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_talla_hogar2, 1.9f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_resultado_hogarsec4, 2.5f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_hemoglobina_hogar, 3f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_hora_medicion_hogar1, 3f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_fecha_medicion_hogar, 2.5f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_hemoglobina_hogar2, 3f,TableComponent.ALIGN.RIGHT);
		tchemoglobina.addHeader(R.string.m_hora_medicion_hogar2, 2.5f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_fecha_medicion_hogar2, 2.5f,TableComponent.ALIGN.CENTER);
		tchemoglobina.addHeader(R.string.m_resultado_hogarsec5, 2.5f,TableComponent.ALIGN.CENTER);
		
		
		
		tcMenoresaDoceanios.addHeader(R.string.seccion01_nro_orden,1f);
		tcMenoresaDoceanios.addHeader(R.string.seccion01nombres,2f,ALIGN.LEFT);
		tcMenoresaDoceanios.addHeader(R.string.seccion01_edad,1f);
		
		
		tcReporte.addHeader(R.string.seccion01_nro_orden,1f);
		tcReporte.addHeader(R.string.seccion01nombres,3f,ALIGN.LEFT);
		tcReporte.addHeader(R.string.seccion01_edad,2f);
		tcReporte.addHeader(R.string.seccion01_seguro,4f,ALIGN.LEFT);
		tcReporte.addHeader(R.string.seccion01_educacion14,2f);
		tcReporte.addHeader(R.string.seccion01_educacion15,3f,ALIGN.LEFT);
		
//		lblDatosdeControl = new LabelComponent(this.getActivity()).text(R.string.datosdecontrol).size(altoComponente, 250).negrita().textSize(20);
		lblTotalPersonas = new LabelComponent(this.getActivity()).text(R.string.totaldepersonas).size(altoComponente,550).negrita().textSize(16);
		lblmefnro = new LabelComponent(this.getActivity()).text(R.string.mujeres15a49).size(altoComponente, 250).negrita().textSize(16);
		lblniniosdocenro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresadoce).size(altoComponente, 550).negrita().textSize(16);
		lblniniosseisnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresaseis).size(altoComponente, 550).negrita().textSize(16);
		lblninioscinconro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresacinco).size(altoComponente, 550).negrita().textSize(16);
		lblniniostresnro = new LabelComponent(this.getActivity()).text(R.string.niniosmenoresatres).size(altoComponente, 550).negrita().textSize(16);
		lblinformantehogarnro = new LabelComponent(this.getActivity()).text(R.string.informantehogar).size(altoComponente, 550).negrita().textSize(16);
		lblinformantesaludnro = new LabelComponent(this.getActivity()).text(R.string.informantesaludnro).size(altoComponente, 550).negrita().textSize(16);
		lblTotalPersonasvalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblmefnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniosdocenrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniosseisnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblninioscinconrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblniniostresnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 50).negrita().textSize(16);
		lblinformantehogarnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 100).negrita().textSize(16);
		lblinformantesaludnrovalor= new LabelComponent(this.getActivity()).text("").size(altoComponente, 100).negrita().textSize(16);
		gridResultados = new GridComponent2(this.getActivity(),2);
	}
	
	
	@Override
	protected View createUI() {
		buildFields();
		 q0 = createQuestionSection(txtCabecera,lblTitulo);
		 q5 = createQuestionSection(R.string.resumendelhogar,tcReporte.getTableView());
		 q1 = createQuestionSection(R.string.listadomef,tcMef.getTableView());
		 q2 = createQuestionSection(gridResultado.component());
		 q3 = createQuestionSection(R.string.menoresaseis,tcMenoresaSeisanios.getTableView());
		 q4 = createQuestionSection(R.string.menoresadoce,tcMenoresaDoceanios.getTableView());
		 q6 = createQuestionSection(R.string.hemoglobinatitulo,tchemoglobina.getTableView());
		 q7 = createQuestionSection(R.string.seccion09salud, gridseccion09.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		//form.addView(q5);
		form.addView(q6);
		form.addView(q7);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q4);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		// TODO Auto-generated method stub
		
		mefdeviolenciafamiliar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, SeleccionarMefViolenciaDomestica());
		Hogar hogar = new Hogar();
		hogar.id=App.getInstance().getMarco().id;
		hogar.hogar_id=App.getInstance().getHogar().hogar_id;
		if(mefdeviolenciafamiliar!=null){
		hogar.qhviolen = mefdeviolenciafamiliar.persona_id;
		}
		else{
			hogar.qhviolen=0;	
		}
		
		boolean flag = false;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(hogar, dbTX, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		
		
		MensajeSinoTieneGps(hogar.id);
		
		return flag;
	}
	
	private void MensajeSinoTieneGps(Integer id){
		if(EndesCalendario.VerificarSitientePuntoGPS(id)==true){
			MyUtil.MensajeGeneral(getActivity(),App.MENSAJE_GPS);
		}
	}
	
	@Override
	public void cargarDatos() {
		LimpiarEtiquetas();
		cap08_09 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
//		detalles = getCuestionarioService().GetSeccion01ListByMarcoyHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		detalles = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 99);
		tcReporte.setData(detalles, "persona_id","getNombre","qh07","getSegurodeSalud","getPregunta14","getPregunta15");
		Integer persona_id=MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, getServiceVisita(), getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersona(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id, persona_id);
		if(informantedesalud!=null){
			informantedesalud.qh02_2=informantedesalud.qh02_2==null?"":informantedesalud.qh02_2;
			lblnombreInformante.setText(informantedesalud.persona_id + " "+ informantedesalud.qh02_1+" "+informantedesalud.qh02_2);
			
			lblnombredetalle.setText(informantedesalud.qh02_1+" "+informantedesalud.qh02_2);
			lblnumordendetalle.setText(informantedesalud.persona_id.toString());
			
			lbledadetalle.setText(informantedesalud.qh07 ==null?"":informantedesalud.qh07.toString());
			
			if(cap08_09 != null){
				lblpesodetalle.setText(cap08_09 != null && cap08_09.qs900 == null ? "" :cap08_09.getPeso().toString());
			}else{
				lblpesodetalle.setText("");
			}
			
			if(cap08_09 != null){
				lbltalladetalle.setText(cap08_09 != null && cap08_09.qs901 == null ? "" :cap08_09.getTalla().toString());
			}else{
				lbltalladetalle.setText("");
			}
			
			if(cap08_09 != null){
				lblresultado1detalle.setText(cap08_09 != null && cap08_09.qs902 == null ? "" :cap08_09.getResultado1().toString());
			}else{
				lblresultado1detalle.setText("");
			}
			
			if(cap08_09 != null){
				lblresultado2detalle.setText(cap08_09 != null && cap08_09.qs906 == null ? "" :cap08_09.getResultado2().toString());
			}else{
				lblresultado2detalle.setText("");
			}
			
			if(cap08_09 != null){
				lblresultado3detalle.setText(cap08_09 != null && cap08_09.qs908 == null ? "" :cap08_09.getResultado3().toString());
			}else{
				lblresultado3detalle.setText("");
			}
			
			
			lblsistolica1detalle.setText(cap08_09 != null && !Util.esDiferente(cap08_09.qs906,1)  ? cap08_09.qs903.toString() : "");
			lbldiastolica1detalle.setText(cap08_09 != null && !Util.esDiferente(cap08_09.qs906,1)  ? cap08_09.qs903_1.toString(): "");
			lblsistolica2detalle.setText(cap08_09 != null && !Util.esDiferente(cap08_09.qs906,1)  ? cap08_09.qs905.toString() : "");
			lbldiastolica2detalle.setText(cap08_09 != null && !Util.esDiferente(cap08_09.qs906,1)  ? cap08_09.qs905_1.toString() : "");
			lblperimetrodetalle.setText(cap08_09 != null && !Util.esDiferente(cap08_09.qs908,1)  ? cap08_09.getPerimetro().toString(): "");
			lblinformantesaludnrovalor.setText(informantedesalud.persona_id+"");
		}
		mefdeviolenciafamiliar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, SeleccionarMefViolenciaDomestica());
		if(mefdeviolenciafamiliar!=null)
		{mefdeviolenciafamiliar.qh02_2=mefdeviolenciafamiliar.qh02_2==null?"": mefdeviolenciafamiliar.qh02_2;
			lblnombredomestica.setText(mefdeviolenciafamiliar.persona_id+" "+mefdeviolenciafamiliar.qh02_1+" "+mefdeviolenciafamiliar.qh02_2);}
//		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		ListadoMef = getServiceSeccion01().getMujerSeleccionable(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,App.HogEdadMefMin,App.HogEdadMefMax);
		if(ListadoMef.size()==0){
		q1.setVisibility(View.GONE);
		}else{
		tcMef.setData(ListadoMef, "persona_id","qh02_1","qh07","getSegurodeSalud");
//		lblmefnrovalor.setText(ListadoMef.size()+"");
		}
		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 5);
		if(ListMenoresaSeis.size()==0){
			q3.setVisibility(View.GONE);
		}
		else{tcMenoresaSeisanios.setData(ListMenoresaSeis, "persona_id","getNombre","qh07","getSegurodeSalud");
//		 lblniniosseisnrovalor.setText(ListMenoresaSeis.size()+"");
		 }
		ListMenoresaDoce = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 11);
		if(ListMenoresaDoce.size()==0){
			q4.setVisibility(View.GONE);
		}
		else{
		tcMenoresaDoceanios.setData(ListMenoresaDoce, "persona_id","getNombre","qh07");
//		lblniniosdocenrovalor.setText(ListMenoresaDoce.size()+"");
		}
		
		
		detalles04_05= new ArrayList<Seccion04_05>();
		informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
    	detalles04_05= getSeccion04_05Service().getSeccion04_05ListaVista(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,informantehogar.persona_id,seccionesCargadoSeccion04);
   
    	
    	//tablaHOGAR.setData(detalles04_05 ,"persona_id_orden","qh02_1","getEdadMesesSeccion5","qh07","getFecha2","getSexo","getConsentimiento","getHemoglonina", "getHemoglonina2","getFecha4","getResultadoS5");

    	tchemoglobina.setData(detalles04_05 ,"persona_id_orden","qh02_1","getEdadMesesSeccion5","qh07", "getFecha2", "getPeso", "getTalla", "getTalla2", "getResultado" ,"getHemoglonina", "getHoraPmedicion", "getFecha4", "getHemoglonina2", "getHoraSmedicion", "getFecha5", "getResultadoS5");
		
    	
    	
//    	cap08_09 = getCuestionarioService().getCAP04_07(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargado); 
//    	ToastMessage.msgBox(getActivity(), cap08_09.qs901.toString(), ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    	
		
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 97);
//		lblTotalPersonasvalor.setText(ListMenoresaSeis.size()+"");
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 4);
//		lblninioscinconrovalor.setText(ListMenoresaSeis.size()+"");
//		ListMenoresaSeis = getServiceSeccion01().getNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 0, 2);
//		lblniniostresnrovalor.setText(ListMenoresaSeis.size()+"");
//		Seccion01 informantehogar = getServiceSeccion01().getPersonaInformante(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, 1);
//		String datahogar= informantehogar.persona_id+"";
//		lblinformantehogarnrovalor.setText(datahogar);
	}
	public void LimpiarEtiquetas()
	{
		lblTotalPersonasvalor.setText("");
		lblmefnrovalor.setText("");
		lblniniosdocenrovalor.setText("");
		lblniniosseisnrovalor.setText("");
		lblninioscinconrovalor.setText("");
		lblniniostresnrovalor.setText("");
		lblinformantehogarnrovalor.setText("");
		lblinformantesaludnrovalor.setText("");
	}
	
	
	 public Seccion04_05Service getSeccion04_05Service(){
	    	if(Persona04_05Service == null)
	    	{
	    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
	    	}
	    	return Persona04_05Service;
	}
	 
	public Seccion01Service getPersonaService(){
	    	if(Personaservice == null)
	    	{
	    		Personaservice = Seccion01Service.getInstance(getActivity());
	    	}
	    	return Personaservice;
	}
	
	public Integer SeleccionarMefViolenciaDomestica()
	{
		Integer[][] MatrizViolencia = LlenarMatrizViolenciaFamiliar();
		Integer fila =Integer.parseInt(App.getInstance().getMarco().nselv.substring(2,3));
		
		Integer columna = getServiceSeccion01().cantidaddeMef(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id);
		
		
		Integer mefPosition =0; 
		if(columna!=0)
		{
			mefPosition=MatrizViolencia[fila][columna-1];
		}
		
		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		Integer persona_id=-1;
		if(ListadoMef.size()>0)
		{
			persona_id=ListadoMef.get(mefPosition-1).persona_id;
		}
		return persona_id;
	}
	
	public Integer[][] LlenarMatrizViolenciaFamiliar()
	{
		Integer[][] matriz= new Integer[10][10];
		matriz[0][0]=1; matriz[0][1]=2; matriz[0][2]=2; matriz[0][3]=4; matriz[0][4]=3; matriz[0][5]=6; matriz[0][6]=5; matriz[0][7]=4; 
		matriz[1][0]=1; matriz[1][1]=1; matriz[1][2]=3; matriz[1][3]=1; matriz[1][4]=4; matriz[1][5]=1; matriz[1][6]=6; matriz[1][7]=5; 
		matriz[2][0]=1; matriz[2][1]=2; matriz[2][2]=1; matriz[2][3]=2; matriz[2][4]=5; matriz[2][5]=2; matriz[2][6]=7; matriz[2][7]=6; 
		matriz[3][0]=1; matriz[3][1]=1; matriz[3][2]=2; matriz[3][3]=3; matriz[3][4]=1; matriz[3][5]=3; matriz[3][6]=1; matriz[3][7]=7; 
		matriz[4][0]=1; matriz[4][1]=2; matriz[4][2]=3; matriz[4][3]=4; matriz[4][4]=2; matriz[4][5]=4; matriz[4][6]=2;	matriz[4][7]=8;
		matriz[5][0]=1; matriz[5][1]=1; matriz[5][2]=1; matriz[5][3]=1; matriz[5][4]=3; matriz[5][5]=5; matriz[5][6]=3; matriz[5][7]=1; 
		matriz[6][0]=1; matriz[6][1]=2; matriz[6][2]=2; matriz[6][3]=2; matriz[6][4]=4; matriz[6][5]=6; matriz[6][6]=4; matriz[6][7]=2; 
		matriz[7][0]=1; matriz[7][1]=1; matriz[7][2]=3; matriz[7][3]=3; matriz[7][4]=5; matriz[7][5]=1; matriz[7][6]=5; matriz[7][7]=3; 
		matriz[8][0]=1; matriz[8][1]=2; matriz[8][2]=1; matriz[8][3]=4; matriz[8][4]=1; matriz[8][5]=2; matriz[8][6]=6; matriz[8][7]=4; 
		matriz[9][0]=1;	matriz[9][1]=1;	matriz[9][2]=2;	matriz[9][3]=1;	matriz[9][4]=2;	matriz[9][5]=3;	matriz[9][6]=7;	matriz[9][7]=5;
		return matriz;
	}
	
	public Long CantidaddeDias(Calendar fechaInicio,Calendar fechaFin)
	{
		long total=0;
		total=fechaInicio.getTimeInMillis() - fechaFin.getTimeInMillis();
		long difd=total / (1000 * 60 * 60 * 24);
		return difd;
	}

	 private Seccion01Service getServiceSeccion01() {
	        if (seccion01 == null) {
	        	seccion01 = Seccion01Service.getInstance(getActivity());
	        }
	        return seccion01;
	}
	 private VisitaService getServiceVisita() {
	        if (visita == null) {
	        	visita = VisitaService.getInstance(getActivity());
	        }
	        return visita;
	}
	 public CuestionarioService getCuestionarioService() {
			if (cuestionarioService == null) {
				cuestionarioService = CuestionarioService.getInstance(getActivity());
			}
			return cuestionarioService;

	}
	 
	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	 
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
}
