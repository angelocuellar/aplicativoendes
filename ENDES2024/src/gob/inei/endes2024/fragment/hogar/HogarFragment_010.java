package gob.inei.endes2024.fragment.hogar; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.dialog.BeneficiarioDialog;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;

public class HogarFragment_010 extends FragmentForm implements Respondible { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH91; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQH92_1M; 
	@FieldAnnotation(orderIndex=3)
	
	public RadioGroupOtherField rgQH93; 
	public Hogar hogar; 
	public TextField txtCabecera;
	private CuestionarioService cuestionarioService;
	private Seccion01Service seccion01service; 
	private Seccion03Service seccion03service;
	private LabelComponent lblTitulo,lblBeca18,lblTP,lblP91,lblP92,lblP93,lblP93_ind,lblP94; 

	SeccionCapitulo[] seccionesGrabado,seccionesCargado,seccionesCargado92; 
	
	public TableComponent tcB18HOGARES;
	private ButtonComponent btnAgregarBeca18,btnAgregarTrabajaPeru;
	private List<Hogar> detalles;
	public ArrayList<String> nombresAll;
	public ArrayList<Beneficiario> Beneficiaros;
	public Seccion01 entrevistado;
	public Seccion01 elegido;
	public Seccion03 elegidos03;
	public List<Seccion03> beneficiados;
	public List<Seccion03> beneficiadostrabajaperu;
	public TableComponent tcTrabajaPeru;
	private Seccion01Service seccion01;
	private Seccion03Service seccion03;
	private enum ACTION{ELIMINAR,MENSAJE}
	private ACTION action;
	private DialogComponent dialog;
	
	public LinearLayout q0,q1,q2,q3,q4;
	private boolean beca18=false,trabPeru=false;
	public int accion=0;
	
	public HogarFragment_010() {}
	
	public HogarFragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
    @Override 
    public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
    
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH91","QH93","ID","HOGAR_ID","PERSONA_INFORMANTE_ID")}; 
		seccionesCargado92 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QHS3_1A","QHS3_1M","QH02_1","ID","HOGAR_ID","PREGUNTA_ID","PERSONA_ID","ESTADO3")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH91","QH93")}; 
		cargarDatos();
		return rootView; 
    } 
  
    @Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(tcB18HOGARES.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(0, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(0, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
		if (v.equals(tcTrabajaPeru.getListView())) {
			menu.setHeaderTitle("Opciones de Beneficiario");
			/*0*/menu.add(1, 0, 1, "Editar Beneficiario");
			/*1*/menu.add(1, 1, 1, "Eliminar Beneficiarios");
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		}
	}
  
    @Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiados.get(info.position));
						break;
				case 1:	 
						accion = 0;
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro que desea eliminar?");dialog.put("seleccion",
                        (Seccion03) beneficiados.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
				case 0:	EditarSeccion03((Seccion03) beneficiadostrabajaperu.get(info.position));
						break;
				case 1:	 
						accion = 0;
						action = ACTION.ELIMINAR;
		                dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro que desea eliminar?");
		                dialog.put("seleccion",
                        (Seccion03) beneficiadostrabajaperu.get(info.position));
		                dialog.showDialog();
        		        break;
			}
		}
		return super.onContextItemSelected(item);
	}
    
    @Override 
    protected void buildFields() { 
    	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.hogar_3).textSize(21).centrar().negrita();
		lblBeca18 = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		lblTP = new LabelComponent(this.getActivity()).size(altoComponente + 10, 300).text(R.string.hogarqh92_mas).textSize(16).alinearDerecha();
		lblP91 = new LabelComponent(this.getActivity()).text(R.string.hogarqh91).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		rgQH91=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh91_1,R.string.hogarqh91_2,R.string.hogarqh91_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH91");
		lblP92 = new LabelComponent(this.getActivity()).text(R.string.hogarqh92).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP93 = new LabelComponent(this.getActivity()).text(R.string.hogarqh93).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		lblP93_ind = new LabelComponent(this.getActivity()).text(R.string.hogarqh93_ind).size(MATCH_PARENT, MATCH_PARENT).textSize(16).alinearIzquierda().negrita();
		lblP94 = new LabelComponent(this.getActivity()).text(R.string.hogarqh94).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();
		
		rgQH93=new RadioGroupOtherField(this.getActivity(),R.string.hogarqh93_1,R.string.hogarqh93_2,R.string.hogarqh93_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("salto_rgQH93"); 
		
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcB18HOGARES = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//			tcTrabajaPeru = new TableComponent(getActivity(), this,App.ESTILO).size(230, 670).headerHeight(altoComponente+10).dataColumHeight(50);
//		}
//		else{
			tcB18HOGARES = new TableComponent(getActivity(), this,App.ESTILO).size(330, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
			tcTrabajaPeru = new TableComponent(getActivity(), this,App.ESTILO).size(330, 670).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcB18HOGARES.addHeader(R.string.m_orden_b18_hogar, 2f,TableComponent.ALIGN.CENTER);
		tcB18HOGARES.addHeader(R.string.m_nombre_b18_hogar, 3.5f,TableComponent.ALIGN.LEFT);
		tcB18HOGARES.addHeader(R.string.m_anio_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tcB18HOGARES.addHeader(R.string.m_mes_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		
		btnAgregarBeca18 = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 55);
		btnAgregarBeca18.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
					agregarBeneficiario();
			}
		});	
		
		
		tcTrabajaPeru.addHeader(R.string.m_orden_b18_hogar, 2f,TableComponent.ALIGN.CENTER);
		tcTrabajaPeru.addHeader(R.string.m_nombre_b18_hogar, 3.5f,TableComponent.ALIGN.LEFT);
		tcTrabajaPeru.addHeader(R.string.m_anio_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tcTrabajaPeru.addHeader(R.string.m_mes_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		
		btnAgregarTrabajaPeru = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 55);
		btnAgregarTrabajaPeru.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
				agregarBeneficiarioTrabajaPeru();
			}
		});	
		textoNegrita();
    } 
    
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(lblP91,rgQH91);
		LinearLayout q2_1 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblBeca18,btnAgregarBeca18);
		q2 = createQuestionSection(lblP92, q2_1,tcB18HOGARES.getTableView());
		LinearLayout q2_2 = createQuestionSection(0, Gravity.LEFT, LinearLayout.HORIZONTAL,lblTP,btnAgregarTrabajaPeru);
		q3 = createQuestionSection(lblP94, q2_2,tcTrabajaPeru.getTableView());
		q4 = createQuestionSection(lblP93,lblP93_ind,rgQH93);
		ScrollView contenedor = createForm(); 
		
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q4);
		form.addView(q3);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if (hogar.qh91!=null) {
				hogar.qh91=hogar.getConver80a(hogar.qh91);
			}
			if (hogar.qh93!=null) {
				hogar.qh93=hogar.getConver80a(hogar.qh93);
			}
			
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getHogar().qh93=hogar.qh93;
		return true; 
    } 
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(hogar.qh91) && beca18) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.91"); 
			view = rgQH91; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh91, 1,1) && beca18) {
			if (beneficiados.size()<1) { 
				mensaje = "La pregunta P.92 debe tener algun Beneficiario"; 
				view = btnAgregarBeca18;
				error = true; 
				return false; 
			}
		}
		 
		if (Util.esVacio(hogar.qh93) && trabPeru) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.93"); 
			view = rgQH93; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(hogar.qh93, 1,1) && trabPeru) {
			if (beneficiadostrabajaperu.size()<1) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.94_1A"); 
				view = btnAgregarTrabajaPeru; 
				error = true; 
				return false; 
			} 		
		}
		 
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
    	if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    } 
    	if (hogar.qh91!=null) {
			hogar.qh91=hogar.setConver80a(hogar.qh91);
		}
		if (hogar.qh93!=null) {
			hogar.qh93=hogar.setConver80a(hogar.qh93);
		}
		entityToUI(hogar); 
		cargarTabla();
		eliminarBeneficiados();
		inicio(); 
    } 
    
    public void cargarTabla(){
//    		beneficiados = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.BECA18,seccionesCargado92);
//	    	tcB18HOGARES.setData(beneficiados,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//	    	registerForContextMenu(tcB18HOGARES.getListView());
	    	
	    	beneficiados = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.BECA18,seccionesCargado92);
	    	tcB18HOGARES.setData(beneficiados,"persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
	    	if(beneficiadostrabajaperu!=null&& beneficiados.size()>0){
		    	for (int row = 0; row < beneficiados.size(); row++) {
					if (obtenerEstado(beneficiados.get(row)) == 1) {
						// borde de color azul
						tcB18HOGARES.setBorderRow(row, true);
					} else if (obtenerEstado(beneficiados.get(row)) == 2) {
						// borde de color rojo
						tcB18HOGARES.setBorderRow(row, true, R.color.red);
					} else {
						tcB18HOGARES.setBorderRow(row, false);
					}
				}
	    	}
	    	tcB18HOGARES.reloadData();
		   	registerForContextMenu(tcB18HOGARES.getListView());
		   	
	    	
//	    	beneficiadostrabajaperu = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.TRABAJAPERU,seccionesCargado92);
//	    	tcTrabajaPeru.setData(beneficiadostrabajaperu, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
//		   	registerForContextMenu(tcTrabajaPeru.getListView());
	    	
	    	beneficiadostrabajaperu = getSeccion03Service().getBeneficiariosBeca18(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,App.TRABAJAPERU,seccionesCargado92);
	    	tcTrabajaPeru.setData(beneficiadostrabajaperu, "persona_id_orden","qh02_1","qhs3_1a","qhs3_1m");
	    	if(beneficiadostrabajaperu!=null&& beneficiadostrabajaperu.size()>0){
		    	for (int row = 0; row < beneficiadostrabajaperu.size(); row++) {
					if (obtenerEstado(beneficiadostrabajaperu.get(row)) == 1) {
						// borde de color azul
						tcTrabajaPeru.setBorderRow(row, true);
					} else if (obtenerEstado(beneficiadostrabajaperu.get(row)) == 2) {
						// borde de color rojo
						tcTrabajaPeru.setBorderRow(row, true, R.color.red);
					} else {
						tcTrabajaPeru.setBorderRow(row, false);
					}
				}
	    	}
	    	tcTrabajaPeru.reloadData();
		   	registerForContextMenu(tcTrabajaPeru.getListView());
    }
    
    public void refrescarBeca18(Seccion03 beneficiariosbeca18) {
        if(!Util.esDiferente(beneficiariosbeca18.pregunta_id,App.BECA18)){
	    	if (beneficiados.contains(beneficiariosbeca18)) {
	                cargarTabla();
	                return;
	        } else {
	        	beneficiados.add(beneficiariosbeca18);
	                cargarTabla();
	        }
	        }
	        if(!Util.esDiferente(beneficiariosbeca18.pregunta_id, App.TRABAJAPERU))
	        {if (beneficiadostrabajaperu.contains(beneficiariosbeca18)) {
	            cargarTabla();
	            return;
	        } else {
	        	beneficiadostrabajaperu.add(beneficiariosbeca18);
	            cargarTabla();
	        }
        }
}

    private void inicio() { 
    	mostrarPreguntas();
    	salto_rgQH91();
    	salto_rgQH93();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    }
    
    private int obtenerEstado(Seccion03 detalle) {
 		if (!Util.esDiferente(detalle.estado3, 0)) {
 			return 1 ;
 		} else if (!Util.esDiferente(detalle.estado3,1)) {
 			return 2;
 		}
 		return 0;
 	}
    
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    } 
    
    public Seccion01Service getSeccion01Service() { 
		if(seccion01service==null){ 
			seccion01service = Seccion01Service.getInstance(getActivity()); 
		} 
		return seccion01service; 
    }
    
    public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
    
    public void salto_rgQH91() {
		int valor = Integer.parseInt(rgQH91.getTagSelected("0").toString());
	
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarBeca18,tcB18HOGARES);
			
			if(beneficiados.size()>0 && valor!=0){
				accion = App.BECA18;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Beca 18!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}			
			
			rgQH93.requestFocus();
		} else { 
            Util.lockView(getActivity(), false, btnAgregarBeca18,tcB18HOGARES);
            tcB18HOGARES.requestFocus();
		}
	}
    public void salto_rgQH93() {
		int valor = Integer.parseInt(rgQH93.getTagSelected("0").toString());		
		mostrarPreguntas();
		if (Util.esDiferente(valor, 1,1)) {
			Util.cleanAndLockView(this.getActivity(), btnAgregarTrabajaPeru,tcTrabajaPeru);
			
			if(beneficiadostrabajaperu.size()>0 && valor!=0){
				accion = App.TRABAJAPERU;			
				action = ACTION.ELIMINAR;
	            dialog = new DialogComponent(getActivity(), this,
	                    TIPO_DIALOGO.YES_NO, getResources().getString(
	                                    R.string.app_name),
	                    "Se eliminaran los beneficiados de Lurawi Perú/Trabaja Perú!!! ¿Esta seguro que desea continuar?");
	            dialog.showDialog();
			}
			
		} else {
			Util.lockView(getActivity(), false,  btnAgregarTrabajaPeru,tcTrabajaPeru);
			tcTrabajaPeru.requestFocus();
		}
	}
    protected void agregarBeneficiario(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
		Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.BECA18;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetalle(bean);
    }
    protected void agregarBeneficiarioTrabajaPeru(){
    	hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargado);
    	Seccion03 bean = new Seccion03();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.pregunta_id = App.TRABAJAPERU;
		bean.persona_id = hogar.persona_informante_id;
		abrirDetalle(bean);
    }
    
    public void abrirDetalle(Seccion03 tmp) {
    	
		FragmentManager fm = HogarFragment_010.this.getFragmentManager();
		BeneficiarioDialog aperturaDialog = BeneficiarioDialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public void EditarSeccion03(Seccion03 tmp) {  	
    	FragmentManager fm = HogarFragment_010.this.getFragmentManager();
		BeneficiarioDialog aperturaDialog = BeneficiarioDialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
   
    private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
	}
    
    private Seccion03Service getServiceSeccion03() {
        if (seccion03 == null) {
        	seccion03 = Seccion03Service.getInstance(getActivity());
        }
        return seccion03;
	}
    
    public void PosiblesBeneficiarios(){
 		List<Seccion01> PosibleBeca18 = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMABECA18,App.EDADMAXIMABECA18,App.BECA18);
 	   	if(PosibleBeca18.size()>0){
 	   		beca18 = true;
 		}else{beca18=false;}
 	   	
 	   List<Seccion01> PosibleTrabPeru = getSeccion01Service().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMATPERU,App.EDADMAXIMATPERU,App.TRABAJAPERU);
	   	if(PosibleTrabPeru.size()>0){
	   		trabPeru = true;
		}else{trabPeru=false;}
    }
    
    public void mostrarPreguntas(){
    	int val91=0,val93=0;
    	PosiblesBeneficiarios();
    	
    	if(beca18){
    		q1.setVisibility(View.VISIBLE);
    		val91 = Integer.parseInt(rgQH91.getTagSelected("0").toString());
    		if (Util.esDiferente(val91, 1,1)) {
    			q2.setVisibility(View.GONE);
    		}else{
    			q2.setVisibility(View.VISIBLE);
    		}    		
    	}else{
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    	}
    	
    	if(trabPeru){
    		q4.setVisibility(View.VISIBLE);
    		val93 = Integer.parseInt(rgQH93.getTagSelected("0").toString());
    		if (Util.esDiferente(val93, 1,1)) {
    			q3.setVisibility(View.GONE);
    		}else{
    			q3.setVisibility(View.VISIBLE);
    		}    		
    	}else{
    		q4.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    	}
    }
    
   @Override
    public void onCancel() {
	   if(accion==App.BECA18){
       		
       		rgQH91.setTagSelected(1);
       }
	   if(accion==App.TRABAJAPERU){
      		
      		rgQH93.setTagSelected(1);
      }
    }

   @Override 
    public void onAccept() {
        if (action == ACTION.MENSAJE) {
            return;
        }
        if (dialog == null) {
            return;
        }
        if(accion==0){
	        Seccion03 bean = (Seccion03) dialog.get("seleccion");
	        try {
		        if(!Util.esDiferente(bean.pregunta_id, App.BECA18)){    
		        	beneficiados.remove(bean);
	        	}
		        if(!Util.esDiferente(bean.pregunta_id, App.TRABAJAPERU)){  
		        	beneficiadostrabajaperu.remove(bean);
	        	}
	        	boolean borrado=false;
	            borrado =getSeccion03Service().BorrarPersonaPregunta(bean);
	            if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        if(accion==App.BECA18){
        	try {
	            while(beneficiados.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiados.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiados.remove(beneficiados.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        if(accion==App.TRABAJAPERU){
        	try {
	            while(beneficiadostrabajaperu.size()>0){
	            	boolean borrado=false;
	            	borrado = getSeccion03Service().BorrarPersonaPregunta(beneficiadostrabajaperu.get(0));
	            	if (!borrado) {
	                    throw new SQLException("Persona no pudo ser borrada.");
	            	}
	            	beneficiadostrabajaperu.remove(beneficiadostrabajaperu.get(0));
	            }
	        } catch (SQLException e) {                      
	                e.printStackTrace();
	                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
	        }
        }
        
        cargarTabla();
    }
   
   public void eliminarBeneficiados(){
	   boolean eliminob18=false,eliminoTP=false;
	   for(int i=0;i<beneficiados.size();i++){
		   Seccion01 band = new Seccion01();
		   band = getSeccion01Service().getPersonaEdad(beneficiados.get(i).id, beneficiados.get(i).hogar_id, beneficiados.get(i).persona_id_orden);
		   		   
		   if(!MyUtil.incluyeRango(App.EDADMINIMABECA18, App.EDADMAXIMABECA18, band.qh07)){
			   eliminob18 = true;
			   try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiados.get(i));
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
		                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        } 
		   }
	   }
	   
	   for(int i=0;i<beneficiadostrabajaperu.size();i++){
		   Seccion01 band = new Seccion01();
		   band = getSeccion01Service().getPersonaEdad(beneficiadostrabajaperu.get(i).id, beneficiadostrabajaperu.get(i).hogar_id, beneficiadostrabajaperu.get(i).persona_id_orden);
		   		   
		   if(!MyUtil.incluyeRango(App.EDADMINIMATPERU, App.EDADMAXIMATPERU, band.qh07)){
			   eliminoTP = true;
			   try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiadostrabajaperu.get(i));
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
		                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        } 
		   }
	   }
	   
	   if(eliminob18 || eliminoTP){
		   cargarTabla();
	   }
   }
   
   public void textoNegrita(){
  	   Spanned texto91 = Html.fromHtml(lblP91.getText()+"<b> BECA 18?</b>");    	
  	   lblP91.setText(texto91);
  	   
  	   Spanned texto93 = Html.fromHtml(lblP93.getText()+"<b> LURAWI PER&Uacute;/TRABAJA PER&Uacute;?</b>");    	
	   lblP93.setText(texto93);
    }
   
   public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH91.readOnly();
			tcB18HOGARES.setEnabled(false);
			rgQH93.readOnly();
			tcTrabajaPeru.setEnabled(false);
			btnAgregarBeca18.setEnabled(false);
			btnAgregarTrabajaPeru.setEnabled(false);
		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			if (hogar.qh91!=null) {
				hogar.qh91=hogar.getConver80a(hogar.qh91);
			}
			if (hogar.qh93!=null) {
				hogar.qh93=hogar.getConver80a(hogar.qh93);
			}	
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar, seccionesGrabado);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
