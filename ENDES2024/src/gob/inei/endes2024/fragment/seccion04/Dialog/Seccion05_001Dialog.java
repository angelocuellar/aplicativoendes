package gob.inei.endes2024.fragment.seccion04.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_01_03.Dialog.CISECCION_02Fragment_003Dialog;
import gob.inei.endes2024.fragment.seccion04.Seccion04_05Fragment_002;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Seccion04_05_Validar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.math.BigDecimal;


public class Seccion05_001Dialog extends DialogFragmentComponent{
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH208;
	@FieldAnnotation(orderIndex=2) 
	public SpinnerField spnQH209; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH210; 
	@FieldAnnotation(orderIndex=4) 
	public DecimalField txtQH211; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH212;
	
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQH212AH;
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQH212AM;
	
	@FieldAnnotation(orderIndex=8)
	public DateTimeField txtQH212B_D;
	
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQH213;
	@FieldAnnotation(orderIndex=10)
	public TextField txtQH213_O;
	
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQHREC_05;
	
	@FieldAnnotation(orderIndex = 9)
	public RadioGroupOtherField rgQHREC_04;
	
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQH212C;
	
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH212D; 
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQH212E; 
	@FieldAnnotation(orderIndex=14)
	public TextField txtQH212E_O;
	
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQH212F1; 
	
	@FieldAnnotation(orderIndex=16) 
	public DecimalField txtQH212F;
	
	@FieldAnnotation(orderIndex=17) 
	public IntegerField txtQH212GH;
	@FieldAnnotation(orderIndex=18) 
	public IntegerField txtQH212GM;
	
	@FieldAnnotation(orderIndex=19)
	public DateTimeField txtQH212H_D;
	
	@FieldAnnotation(orderIndex=20) 
	public IntegerField txtQH212I;
	
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQH212J; 
	@FieldAnnotation(orderIndex=22)
	public TextField txtQH212J_O;
	
	@FieldAnnotation(orderIndex=23) 
	public DecimalField txtQH212K;
	
	@FieldAnnotation(orderIndex=24) 
	public IntegerField txtQH212L;

	public ButtonComponent btnqh200_h_ini,btnqh200_h_fin;
	
	private LabelComponent lblTitulo;
	private GridComponent2 gridhora, grid2, grid3,gridhora2, gridPrimeraMedicion,grid212c,grid212f,grid212g,grid212h,grid212i;
	public ButtonComponent btnAceptar,btnCancelar;
	private LabelComponent lblPreg208nino,lblPreg208mef,lblPreg209,lblPreg210,lblPreg211,lblPreg211_titulo,lblPreg212d_titulo,lblPreg212,lblPreg213,lblpregunta212a,lblpregunta212ah,lblpregunta212am,lblDiaMedicion, lblDiaMedicion2,lblPreg212B, 
	lblPreg212C, lblPreg212D, lblPreg212E,lblPreg212F, lblPreg212G, lblpregunta212g, lblpregunta212gh, lblpregunta212gm, lblPreg212H, lblPreg212I, lblPreg212J, lblPreg212K, lblPreg212L ;
	private LabelComponent lblpre200_h_ini_t,lblpre200_h_ini_f,lblpre200_h_fin_t,lblpre200_h_fin_f;
	private GridComponent2 grid_QH200_h,grid_QH200_hf;

	SeccionCapitulo[] seccionesGrabado, seccionesCargado,seccionesCargadoCIS01_03;
	private static Seccion04_05Fragment_002 caller;
	Seccion04_05 bean;
	Seccion04_05_Validar valida;
	Auditoria registro;
	
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
   }
	
	CISECCION_01_03  individual=null;
	private CuestionarioService hogarService;
	private CuestionarioService cuestionarioService;
	private Seccion04_05Service seccion04_05Service;
	private Seccion01Service serviceSeccion01;
	public Integer sexo=caller.seleccion.qh06;
	public Integer edad=caller.seleccion.qh07;
	public String hemoglobina;
	LinearLayout q0,q1,q2,q4,q5,q6,q7,q9,q8,q11,q3,q10, q12, q13,q14, q15, q16,q17,q18,q19, q20, q21,q30,q31; 
	private int iniPosition;
    
	boolean iniciando = true;
	
	public static Seccion05_001Dialog newInstance(FragmentForm pagina, Seccion04_05 detalle) {
		caller = (Seccion04_05Fragment_002) pagina;
		Seccion05_001Dialog f = new Seccion05_001Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	public static Seccion05_001Dialog newInstance(FragmentForm pagina,Seccion04_05 detalle, int position, List<Seccion04_05> detalles) {

		caller = (Seccion04_05Fragment_002) pagina;
		Seccion05_001Dialog f = new Seccion05_001Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", (Seccion04_05) detalles.get(position));
		f.iniPosition = position;
		f.setArguments(args);
		return f;
	}
	
	@Override 
	  public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
			bean = (Seccion04_05) getArguments().getSerializable("detalle");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= caller.seleccion.persona_id_orden;
	    String nombre=caller.seleccion.qh02_1;
	    Integer edad= caller.seleccion.qh07;
		getDialog().setTitle("200. N° de Orden: " + orden+"           201. Nombre: "+nombre+"\n202.Edad: "+edad); 
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQH211, 5, 25, null, null); 
		//rango(getActivity(), txtQH212F, 5, 25, null, null); 
		cargarDatos();
		enlazarCajas();
		listening();
		iniciando = false;
		return rootView;
	}
	
	public Seccion05_001Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH200_REF_S5","QH209A", "QH213A", "QH208","QH209","QH210","QH211","QH212","QH212AH","QH212AM", "QH212B_D", "QH212B_M", "QH212B_Y","QH212C","QH212D","QH212E","QH212E_O","QH212F1","QH212F","QH212GH","QH212GM","QH212H_D","QH212H_M","QH212H_Y","QH212I","QH212J","QH212J_O","QH212K","QH212L","QH213","QH213_O","QH203D","QH203M","QH203Y","QHREC_05","QHFRC_FECHA05","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH200_REF_S5","QH209A", "QH213A", "QH208","QH209","QH210","QH211","QH212","QH212AH","QH212AM", "QH212B_D", "QH212B_M", "QH212B_Y","QH212C","QH212D","QH212E","QH212E_O","QH212F1","QH212F","QH212GH","QH212GM","QH212H_D","QH212H_M","QH212H_Y","QH212I","QH212J","QH212J_O","QH212K","QH212L","QH213","QH213_O","QHREC_05","QHFRC_FECHA05")};
		seccionesCargadoCIS01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226")};
	}
	

	@Override
	protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion05_titulo).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		 lblpre200_h_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05_209a).textSize(17);
		 lblpre200_h_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05_213a).textSize(17);
		 lblpre200_h_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
		 lblpre200_h_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
		 
		 
		 btnqh200_h_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
		 btnqh200_h_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
		    
		 btnqh200_h_ini.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					lblpre200_h_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
					btnqh200_h_fin.setEnabled(true);
				}
		 });
		 btnqh200_h_fin.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					lblpre200_h_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
				}
		 });
		    
		 grid_QH200_h = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
		 grid_QH200_h.addComponent(btnqh200_h_ini);
		 grid_QH200_h.addComponent(lblpre200_h_ini_f);
		 
		 grid_QH200_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
		 grid_QH200_hf.addComponent(btnqh200_h_fin);
		 grid_QH200_hf.addComponent(lblpre200_h_fin_f);
		    
		    
		if (MyUtil.incluyeRango(0,5, edad)) {
			rgQH208=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh208_3, R.string.secccion04_05qh208_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP208ChangeValue");
		}
		 else{
			rgQH208=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh208_1, R.string.secccion04_05qh208_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP208ChangeValue");
		}
		    
		spnQH209=new SpinnerField(getActivity()).size(altoComponente+15, 400);
		rgQH210=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh210_1, R.string.secccion04_05qh210_2).size(50,450).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP210ChangeValue");
		
		txtQH211=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP211ChangeValue");
		
//		txtQH211.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//               
//                
//                if(s.toString().contains(".") && s.length() > 2 && s.toString().indexOf('.') == s.toString().lastIndexOf('.') && s.charAt(s.length() - 1) != '.'){
//                	
//                	
//                	
//                	//Toast.makeText(getActivity(), s.toString(), Toast.LENGTH_SHORT).show();
//                	
//                	BigDecimal decimalValue = new BigDecimal(s.toString());
//                	
//	    		    if (decimalValue.compareTo(new BigDecimal("8")) > 0) {
//	    		    	rgQH212E.lockButtons(true,0);
//	    		    }else{
//	    		    	rgQH212E.lockButtons(false,0);
//	    		    }
//                }else{
//                	rgQH212E.lockButtons(false,0);
//                }
//			}
//		});
		
		rgQH212=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212_1, R.string.secccion04_05qh212_2).size(50,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQH213=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh213_1, R.string.secccion04_05qh213_2,R.string.secccion04_05qh213_3,R.string.secccion04_05qh213_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP213ChangeValue"); 
		txtQH213_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH213.agregarEspecifique(3, txtQH213_O);
		
		lblPreg208nino= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh208_nino).textSize(17).alinearIzquierda();
		lblPreg208mef= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh208_mef).textSize(17).alinearIzquierda();
		
		lblPreg209= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh209).textSize(17).alinearIzquierda();
		Spanned texto = Html.fromHtml("210. <b>LEA LA DECLARACIÓN DE CONSENTIMIENTO A CADA MUJER O PERSONA RESPONSABLE, DE ACUERDO AL CASO SELECCIONE CÓDIGO</b>"); 
		lblPreg210= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda();lblPreg210.setText(texto);
		
//		lblPreg211= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh211).textSize(17).alinearIzquierda();
		lblPreg211 = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh211).textSize(18).centrar();
		
		lblPreg211_titulo = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh211_titulo).textSize(18).centrar().negrita().colorFondo(R.color.griscabece);		
		lblPreg212d_titulo = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212d_titulo).textSize(18).centrar().negrita().colorFondo(R.color.griscabece);
		
		Spanned texto2 = Html.fromHtml("212. <b>ACTUALMENTE EMBARAZADA</b> (VEA PGTA. 226 DEL C.I. LUEGO SELECCIONE S. C.)"); 
		lblPreg212= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).alinearIzquierda(); lblPreg212.setText(texto2);
		lblPreg213= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh213).textSize(17).alinearIzquierda();
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		lblpregunta212a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212a).textSize(17);
		lblpregunta212a = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212a).textSize(18).centrar();
		
		lblpregunta212ah = new LabelComponent(getActivity()).size(altoComponente,150).text(R.string.secccion04_05qh212ah).negrita().textSize(19).alinearIzquierda();
		lblpregunta212am = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.secccion04_05qh212am).negrita().textSize(19).alinearIzquierda();
		txtQH212AH = new IntegerField(getActivity()).size(altoComponente, 149).maxLength(2);
		txtQH212AM = new IntegerField(getActivity()).size(altoComponente, 149).maxLength(2);
		
//		lblPreg212B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05fecha_medicions5).textSize(17).alinearIzquierda();
		lblPreg212B = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05fecha_medicions5).textSize(18).centrar();
		
		lblDiaMedicion = new LabelComponent(this.getActivity()).size(60, 290).text(R.string.secccion04_dia).textSize(19).centrar();
		txtQH212B_D  = new DateTimeField(this.getActivity(), TIPO_DIALOGO.FECHA,	"dd/MM/yyyy").size(altoComponente, 290)	.setRangoYear(App.ANIOPORDEFECTOSUPERIOR,App.ANIOPORDEFECTOSUPERIOR);
		
		
//		lblPreg212C = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212c).textSize(17).alinearIzquierda();
		lblPreg212C = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212c).textSize(18).centrar();
		
		txtQH212C = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2).maxLength(1);
		
		lblPreg212D = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212d).textSize(17).alinearIzquierda();
		rgQH212D=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212d_1, R.string.secccion04_05qh212d_2).size(50,450).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP212DChangeValue");
		
		lblPreg212E= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212e).textSize(17).alinearIzquierda();
		rgQH212E=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212e_1, R.string.secccion04_05qh212e_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP212EChangeValue"); 
		txtQH212E_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH212E.agregarEspecifique(1, txtQH212E_O);
		
//		lblPreg212F = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212f).textSize(17).alinearIzquierda();
		
		lblPreg212F = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212f).textSize(18).alinearIzquierda();
		
		rgQH212F1 = new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212f1, R.string.secccion04_05qh212f2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP212F1ChangeValue"); 
		txtQH212F=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP212FChangeValue");
		rgQH212F1.agregarEspecifique(0, txtQH212F);
		
//		txtQH212F.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
//			@Override
//			public void afterTextChanged(Editable s) {
//              if(s.toString().equals("1")){
//      			MyUtil.LiberarMemoria();
//          		Util.cleanAndLockView(getActivity(),txtQH212GH, txtQH212GM,txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O );
//              }else{
//    			MyUtil.LiberarMemoria();
//      			Util.lockView(getActivity(), false,txtQH212GH, txtQH212GM,txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O );
//      			//txtQI105Y.setVisibility(View.VISIBLE);
//              }
//			}
//		});
		
//		if (isChecked){
//			MyUtil.LiberarMemoria();
//    		Util.cleanAndLockView(getActivity(),txtQI105Y);
//  		}else {
//  			MyUtil.LiberarMemoria();
//			Util.lockView(getActivity(), false,txtQI105Y);
//			txtQI105Y.setVisibility(View.VISIBLE);
//			}
//		}
		
//		lblpregunta212g = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212g).textSize(17);
		lblpregunta212g = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212g).textSize(18).centrar();
		
		lblpregunta212gh = new LabelComponent(getActivity()).size(altoComponente,150).text(R.string.secccion04_05qh212ah).negrita().textSize(19).alinearIzquierda();
		lblpregunta212gm = new LabelComponent(getActivity()).size(altoComponente, 150).text(R.string.secccion04_05qh212am).negrita().textSize(19).alinearIzquierda();
		txtQH212GH = new IntegerField(getActivity()).size(altoComponente, 149).maxLength(2);
		txtQH212GM = new IntegerField(getActivity()).size(altoComponente, 149).maxLength(2);
		
//		lblPreg212H = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212h).textSize(17).alinearIzquierda();
		lblPreg212H = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212h).textSize(18).centrar();
		
		lblDiaMedicion2 = new LabelComponent(this.getActivity()).size(60, 290).text(R.string.secccion04_dia2).textSize(19).centrar();
		txtQH212H_D  = new DateTimeField(this.getActivity(), TIPO_DIALOGO.FECHA,	"dd/MM/yyyy").size(altoComponente, 290)	.setRangoYear(App.ANIOPORDEFECTOSUPERIOR,App.ANIOPORDEFECTOSUPERIOR);
		
//		lblPreg212I = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212i).textSize(17).alinearIzquierda();
		lblPreg212I = new LabelComponent(this.getActivity()).size(60, 500).text(R.string.secccion04_05qh212i).textSize(18).centrar();
		txtQH212I = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2).maxLength(1);
		
		lblPreg212J= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212j).textSize(17).alinearIzquierda();
		rgQH212J=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212_1, R.string.secccion04_05qh212_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP212JChangeValue"); 
		txtQH212J_O = new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		rgQH212J.agregarEspecifique(0, txtQH212J_O);
		
		lblPreg212K = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212k).textSize(17).alinearIzquierda();
		txtQH212K=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP212KChangeValue");  
		
		lblPreg212L = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212l).textSize(17).alinearIzquierda();
		txtQH212L = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2).callback("onP212LChangeValue");;
		
		gridhora= new GridComponent2(this.getActivity(), Gravity.LEFT,4);
		gridhora.addComponent(lblpregunta212a,4);
		gridhora.addComponent(lblpregunta212ah,1);
		gridhora.addComponent(txtQH212AH,1);
		gridhora.addComponent(lblpregunta212am,1);
		gridhora.addComponent(txtQH212AM,1);
		
//		gridhora2= new GridComponent2(getActivity(),App.ESTILO,4,0);
//		gridhora2.addComponent(lblpregunta212gh);
//		gridhora2.addComponent(txtQH212GH);
//		gridhora2.addComponent(lblpregunta212gm);
//		gridhora2.addComponent(txtQH212GM);
		
		grid2 = new GridComponent2(this.getActivity(),Gravity.LEFT, 2);
		grid2.addComponent(lblPreg212B,2);
		grid2.addComponent(lblDiaMedicion,1);
		grid2.addComponent(txtQH212B_D,1);
		
//		grid3 = new GridComponent2(this.getActivity(), 2);
//		grid3.addComponent(lblDiaMedicion2);
//		grid3.addComponent(txtQH212H_D);
		
		rgQHREC_05 = new RadioGroupOtherField(this.getActivity(),R.string.seccion04_rec01, R.string.seccion04_rec02).size(50, 350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	
		
		gridPrimeraMedicion = new GridComponent2(this.getActivity(), Gravity.LEFT, 2);
		gridPrimeraMedicion.addComponent(lblPreg211);
		gridPrimeraMedicion.addComponent(txtQH211);
		
		grid212c = new GridComponent2(this.getActivity(), Gravity.LEFT, 2);
		grid212c.addComponent(lblPreg212C);
		grid212c.addComponent(txtQH212C);
		
//		gridPrimeraMedicion= new GridComponent2(getActivity(),App.ESTILO,4,0);
//		gridPrimeraMedicion.addComponent(lblpregunta212ah);
//		gridPrimeraMedicion.addComponent(txtQH212AH);
//		gridPrimeraMedicion.addComponent(lblpregunta212am);
//		gridPrimeraMedicion.addComponent(txtQH212AM);

		grid212f = new GridComponent2(this.getActivity(), Gravity.LEFT, 2);
		grid212f.addComponent(lblPreg212F);
		grid212f.addComponent(txtQH212F);
		
		grid212g= new GridComponent2(this.getActivity(), Gravity.LEFT,4);
		grid212g.addComponent(lblpregunta212g,4);
		grid212g.addComponent(lblpregunta212gh,1);
		grid212g.addComponent(txtQH212GH,1);
		grid212g.addComponent(lblpregunta212gm,1);
		grid212g.addComponent(txtQH212GM,1);
		
		grid212h = new GridComponent2(this.getActivity(),Gravity.LEFT, 2);
		grid212h.addComponent(lblPreg212H,2);
		grid212h.addComponent(lblDiaMedicion2,1);
		grid212h.addComponent(txtQH212H_D,1);
		
		grid212i = new GridComponent2(this.getActivity(), Gravity.LEFT, 2);
		grid212i.addComponent(lblPreg212I);
		grid212i.addComponent(txtQH212I);
		
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
		@Override
			public void onClick(View v) {
					caller.contador=0;
					Seccion05_001Dialog.this.dismiss();
				}
			});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (MyUtil.incluyeRango(0,0, edad)) {
					
					if (!Util.esDiferente(bean.qh213, 4)) {
//						Context context = v.getContext();
//						Toast.makeText(context, edad.toString() , Toast.LENGTH_SHORT).show();
						Util.lockView(getActivity(), false,txtQH213_O);
						//Util.cleanAndLockView(getActivity(),txtQH213_O);
					}
//					Context context = v.getContext();
//					Toast.makeText(context, edad.toString() , Toast.LENGTH_SHORT).show();
					onP207AChangeValue();
					//onP212DChangeValue();
				}
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.contador=0;
				caller.cargarTabla();
				Seccion05_001Dialog.this.dismiss();
			}
		});	
	}
	
	@Override
	protected View createUI() {
		buildFields();

		q0 = createQuestionSection(lblTitulo); 
		 if (MyUtil.incluyeRango(0,5, edad)) {
			 q1 = createQuestionSection(lblPreg208nino,rgQH208);
		 }else{
			 q1 = createQuestionSection(lblPreg208mef,rgQH208);
		 }
		
		q2 = createQuestionSection(lblPreg209,spnQH209); 	
		
		q3 = createQuestionSection(lblpre200_h_ini_t,grid_QH200_h.component());
		q4 = createQuestionSection(lblPreg210,rgQH210);
		q30 = createQuestionSection(lblPreg211_titulo);
		q5 = createQuestionSection(gridPrimeraMedicion.component());
		q6 = createQuestionSection(lblPreg212,rgQH212);
		q7 = createQuestionSection(/*lblpregunta212a,*/gridhora.component());
		q8 = createQuestionSection(/*lblPreg212B,*/ grid2.component());
		
//		q12 = createQuestionSection(lblPreg212C,txtQH212C);
		q12 = createQuestionSection(grid212c.component());
		q13 = createQuestionSection(lblPreg212D,rgQH212D);
		q31 = createQuestionSection(lblPreg212d_titulo);
		q14 = createQuestionSection(lblPreg212E,rgQH212E);
		q15 = createQuestionSection(lblPreg212F,rgQH212F1);
//		q15 = createQuestionSection(grid212f.component());
//		q16 = createQuestionSection(lblpregunta212g,gridhora2.component());
		q16 = createQuestionSection(grid212g.component());
//		q17 = createQuestionSection(lblPreg212H, grid3.component());
		q17 = createQuestionSection(grid212h.component());
//		q18 = createQuestionSection(lblPreg212I,txtQH212I);
		q18 = createQuestionSection(grid212i.component());
		q19 = createQuestionSection(lblPreg212J,rgQH212J);
		q20 = createQuestionSection(lblPreg212K,txtQH212K);
		q21 = createQuestionSection(lblPreg212L,txtQH212L);
		
		q9 = createQuestionSection(lblPreg213,rgQH213);
		q10 = createQuestionSection(lblpre200_h_fin_t,grid_QH200_hf.component());
		
		q11 = createQuestionSection(R.string.seccion04_rec,rgQHREC_05);
		
		
		
		LinearLayout botones = createButtonSection(btnAceptar, /*btnGrabadoParcial,*/ btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q30);
		form.addView(q5); 
		form.addView(q6);
		form.addView(q7);
		if (MyUtil.incluyeRango(0, 5, edad)) {
			form.addView(q8); 
		}
		form.addView(q12);
		form.addView(q13);
		form.addView(q31);
		form.addView(q14);
		form.addView(q15);
		if (MyUtil.incluyeRango(0, 5, edad)) {
			form.addView(q16);
			form.addView(q17);
		}
		form.addView(q18);
		form.addView(q19);
		form.addView(q20);
		form.addView(q21);
		
		form.addView(q9);
		form.addView(q10);
		//form.addView(q11);
		
		if(App.getInstance().getMarco().asignado==4){
			form.addView(q11);
		}
		form.addView(botones);
		return contenedor;
	}

	
	public boolean grabar() { 
    	uiToEntity(bean);
    	if(bean.qh200_ref_s5==null && MyUtil.incluyeRango(1,1, rgQH213.getTagSelected("").toString() )){
			bean.qh200_ref_s5=Util.getFechaActualToString();
		}
    	
    	hemoglobina =txtQH211.getText().toString();
    	if (bean.qh212b_d != null) {
			bean.qh212b_d = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "dd");
			bean.qh212b_m = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "MM");
			bean.qh212b_y = Integer.parseInt(Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "yyyy"));
		}
    	
    	if (bean.qh212h_d != null) {
			bean.qh212h_d = Util.getFechaFormateada((Date) txtQH212H_D.getValue(), "dd");
			bean.qh212h_m = Util.getFechaFormateada((Date) txtQH212H_D.getValue(), "MM");
			bean.qh212h_y = Integer.parseInt(Util.getFechaFormateada((Date) txtQH212H_D.getValue(), "yyyy"));
		}
    	
    	bean.qh209a =lblpre200_h_ini_f.getText().toString();
		bean.qh213a =lblpre200_h_fin_f.getText().toString();
    	
    	
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			if(MyUtil.incluyeRango(18,App.HogEdadMefMax, edad)){
				bean.qh208=2;
				bean.qh209=null;
			}
			
			if(MyUtil.incluyeRango(App.HogEdadMefMin,17, edad)){
				bean.qh208=1;
			}
			
			if (bean.qh213 != null) {
				bean.qh213 = bean.getConvert213(bean.qh213);
			}
			if(bean.qhrec_05!=null && bean.qhrec_05==1 && bean.qhfrc_fecha05==null){
				bean.qhfrc_fecha05=Util.getFechaActualToString();
			}
			flag = getSeccion04_05Service().saveOrUpdate(bean, dbTX,seccionesGrabado);
			
			if(valida.qh208==null 
					&& valida.qh209==null 
					&& valida.qh210==null  
					&& valida.qh211==null  
					&& valida.qh212==null 
					&& valida.qh212ah==null 
					&& valida.qh212am==null 
					&& valida.qh212b_d==null 
					&& valida.qh212b_m==null 
					&& valida.qh212b_y==null 
					&& valida.qh213==null 
					&& valida.qh209a==null 
					&& valida.qh213a==null 
				
				){
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH208", bean.qh208==null?null:bean.qh208+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH209", bean.qh209==null?null:bean.qh209+""),dbTX);			
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH210", bean.qh210==null?null:bean.qh210+""),dbTX);			
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH211", bean.qh211==null?null:bean.qh211+""),dbTX);			
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212", bean.qh212==null?null:bean.qh212+""),dbTX);			
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212AH", bean.qh212ah==null?null:bean.qh212ah+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212AM", bean.qh212am==null?null:bean.qh212am+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_D", bean.qh212b_d==null?null:bean.qh212b_d+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_M", bean.qh212b_m==null?null:bean.qh212b_m+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_Y", bean.qh212b_y==null?null:bean.qh212b_y+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH213", bean.qh213==null?null:bean.qh213+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH209A", bean.qh209a==null?null:bean.qh209a+""),dbTX);
				getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH213A", bean.qh213a==null?null:bean.qh213a+""),dbTX);
			}
			else{
			if(Util.esDiferente(bean.qh208, valida.qh208)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH208", bean.qh208==null?null:bean.qh208+""),dbTX);}
			if(Util.esDiferente(bean.qh209, valida.qh209)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH209", bean.qh209==null?null:bean.qh209+""),dbTX);}			
			if(Util.esDiferente(bean.qh210, valida.qh210)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH210", bean.qh210==null?null:bean.qh210+""),dbTX);}			
			if(Util.esDiferente(bean.qh211, valida.qh211)){getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH211", bean.qh211==null?null:bean.qh211+""),dbTX);}			
			if(Util.esDiferente(bean.qh212, valida.qh212)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212", bean.qh212==null?null:bean.qh212+""),dbTX);}			
			if(Util.esDiferente(bean.qh212ah, valida.qh212ah)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212AH", bean.qh212ah==null?null:bean.qh212ah+""),dbTX);}
			if(Util.esDiferente(bean.qh212am, valida.qh212am)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212AM", bean.qh212am==null?null:bean.qh212am+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_d, valida.qh212b_d)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_D", bean.qh212b_d==null?null:bean.qh212b_d+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_m, valida.qh212b_m)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_M", bean.qh212b_m==null?null:bean.qh212b_m+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_y, valida.qh212b_y)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212B_Y", bean.qh212b_y==null?null:bean.qh212b_y+""),dbTX);}
			if(Util.esDiferente(bean.qh213, valida.qh213)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH213", bean.qh213==null?null:bean.qh213+""),dbTX);}
			if(Util.esDiferente(bean.qh209a, valida.qh209a)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH209A", bean.qh209a==null?null:bean.qh209a+""),dbTX);}
			if(Util.esDiferente(bean.qh213a, valida.qh213a)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH213A", bean.qh213a==null?null:bean.qh213a+""),dbTX);}
			}
			
		
			
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos de la caratula.");
			}
		    getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
//		Log.e("qh209: ",""+bean.qh209);
//		Log.e("qh210: ",""+bean.qh210);
//		Log.e("qh213: ",""+bean.qh213);
		
		return flag;
    } 
	
	private boolean validar() { 
		if(!isInRange()) return false; 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
    	
		
		if(hemoglobina.equals("")){
			hemoglobina="999";
		}
        
        if (Util.esVacio(bean.qh208)){
    		mensaje = preguntaVacia.replace("$", "La pregunta P.208"); 
			view = rgQH208; 
			error = true; 
			return false; 
    	}
        
        if ((cantidadMesesS5()==true && MyUtil.incluyeRango(0,0, edad)) || (MyUtil.incluyeRango(1,49, edad) )) {
        	
        	if ( MyUtil.incluyeRango(0,17, edad) &&  MyUtil.incluyeRango(1,1, rgQH208.getTagSelected("").toString())) {
            	if (Util.esVacio(bean.qh209)) { 
    	    		mensaje = preguntaVacia.replace("$", "La pregunta P.209"); 
    	    		view = spnQH209; 
    	    		error = true; 
    	    		return false; 
    	    	}
            }
            
            if ((MyUtil.incluyeRango(0,5, edad) && MyUtil.incluyeRango(1,1, rgQH208.getTagSelected("").toString()))  || (MyUtil.incluyeRango(App.HogEdadMefMin,App.HogEdadMefMax, edad)) ) {
            	if (Util.esVacio(bean.qh210)) { 
    	    		mensaje = preguntaVacia.replace("$", "La pregunta P.210"); 
    	    		view = rgQH210; 
    	    		error = true; 
    	    		return false; 
    	    	}
            	
            	if (MyUtil.incluyeRango(1,1, rgQH210.getTagSelected("").toString())) {					
    				if(Util.esVacio(bean.qh211)){
    					mensaje = preguntaVacia.replace("$", "La pregunta P.211"); 
    	    			view = txtQH211; 
    	    			error = true; 
    	    			return false;
    				}
    				if (MyUtil.incluyeRango(0,5, edad)){
    					if(Util.esVacio(bean.qh212ah)){
        					mensaje = preguntaVacia.replace("$", "La pregunta P.212H"); 
        	    			view = txtQH212AH; 
        	    			error = true; 
        	    			return false;
        				}
        				
        				if(Util.esVacio(bean.qh212am)){
        					mensaje = preguntaVacia.replace("$", "La pregunta P.212M"); 
        	    			view = txtQH212AM; 
        	    			error = true; 
        	    			return false;
        				}
        				
        				
        				
//        				if(Util.esVacio(bean.qh212gh)){
//        					mensaje = preguntaVacia.replace("$", "La pregunta P.212G_H"); 
//        	    			view = txtQH212GH; 
//        	    			error = true; 
//        	    			return false;
//        				}
//        				
//        				if(Util.esVacio(bean.qh212gm)){
//        					mensaje = preguntaVacia.replace("$", "La pregunta P.212G_M"); 
//        	    			view = txtQH212GM; 
//        	    			error = true; 
//        	    			return false;
//        				}
    				}
    				
    			    if (MyUtil.incluyeRango(0,5, edad)) {
    			       	if(!hemoglobina.equals("999")){
    						if(!MyUtil.incluyeRango(6.0,15.0, hemoglobina)){
    							mensaje ="VALIDAR Si hemoglobina corresponde a edad";
    							validarMensaje(mensaje);
    						}
    					}
    			    }
    			    else{
    			       	if(!hemoglobina.equals("999")){
    			    		if(!MyUtil.incluyeRango(7.0,20.0, bean.qh211.toString())){
    				   			mensaje ="VALIDAR Si hemoglobina corresponde a edad";
    				   			validarMensaje(mensaje);
    				   		 }
    			    	}
    			    }
    			       
    								
    			}
            	
            	if (Util.esVacio(bean.qh213)) { 
        			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
        			view = rgQH213; 
        			error = true; 
        			return false; 
        		}
            	if (!Util.esDiferente(bean.qh213, 4)) {
    				if (Util.esVacio(bean.qh213_o)) {
    					mensaje = "Agregar especifique";
    					view = txtQH213_O;
    					error = true;
    					return false;
    				}
    			}
            }
            
         
    	    boolean punto=false;
    	    punto=tieneDecimal(hemoglobina);
    			if(!hemoglobina.equals("999")){
    				if(!punto){
    					mensaje = "La pregunta P.211 debe tener . decimal"; 
    		    		view = txtQH211; 
    		    		error = true; 
    		    		return false; 
    				 }
    			}
		}
        

		
//		if(!Util.esDiferente(bean.qh213, 1) && MyUtil.incluyeRango(0,5, edad)){
	        if (MyUtil.incluyeRango(0,5, edad)){
//	       	 	if (Util.esVacio(bean.qh212b_d)){
//	         		mensaje = preguntaVacia.replace("$", "La pregunta P.qh212b"); 
//	     			view = txtQH212B_D; 
//	     			error = true; 
//	     			return false; 
//	         	}
//	       }
	        
	        if (bean.qh212b_d != null) {
				Integer dia_m = Integer.parseInt(bean.qh212b_d);
				Integer mes_m = Integer.parseInt(bean.qh212b_m);
				if (mes_m < Integer.parseInt(App.getInstance().getVisita().qhvmes_ini)
						|| mes_m == Integer.parseInt(App.getInstance().getVisita().qhvmes_ini)
						&& dia_m < Integer.parseInt(App.getInstance().getVisita().qhvdia_ini)) {
					mensaje = "Fecha de Medición debe ser mayor o igual al de Apertura";
					view = txtQH212B_D;
					error = true;
					return false;
				}
			}
			
			if (bean.qh212b_d != null) {
				Calendar fecharef = new GregorianCalendar(bean.qh212b_y,	Integer.parseInt(bean.qh212b_m) - 1,Integer.parseInt(bean.qh212b_d));
				if (fechaMayorSistema(fecharef)) {
					mensaje = "Validar Fecha de Medición, no puede ser superior al de la Tablet";
					view = txtQH212B_D;
					error = true;
					return false;
				}
			}
		}
		
		if(lblpre200_h_ini_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 209A");
			view = btnqh200_h_ini;
			error = true;
			return false;
		}
		if(lblpre200_h_fin_f.getText().toString().trim().length()==0){
			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 213A");
			view = btnqh200_h_fin	;
			error = true;
			return false;
		}
		if(lblpre200_h_ini_f.getText().toString().trim().length()!=0 && lblpre200_h_fin_f.getText().toString().trim().length()!=0){
			Calendar fecha_ini = new GregorianCalendar();
    		if(lblpre200_h_ini_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_h_ini_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_ini=cal;			
    		}
    		
    		Calendar fecha_fin = new GregorianCalendar();
        	if(lblpre200_h_fin_f.getText().toString().trim().length()!=0){			
    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    			Date date2 = null;
    			try {
    				date2 = df.parse(lblpre200_h_fin_f.getText().toString());
    			} catch (ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}		
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date2);
    			fecha_fin=cal;			
    		}
        	if (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0) {
        		mensaje = "Fecha final P213A no puede ser menor a la fecha inicial"; 
    			view = lblpre200_h_fin_f; 
    			error = true; 
    			return false;
        	}	
        	if ( (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) == 0)  && (Util.compareTime((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0)) {
        		mensaje = "Hora final P213A no puede ser menor a la hora inicial"; 
    			view = lblpre200_h_fin_f; 
    			error = true; 
    			return false;
        	}	
		}
		
		
//		bean.qh209a =lblpre200_h_ini_f.getText().toString();
//		bean.qh213a =lblpre200_h_fin_f.getText().toString();
//		if (bean.qh212b_d != null) {
//			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//			Datetime date = sdf.parse(strDate);
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date);
//			String fecha = "10/03/2013";
//			Calendar cal = Calendar.getInstance();
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			cal.setTime(sdf.parse(lblpre200_h_ini_f.getText().toString()));
//			Date fechaInicial = Calendar.getInstance().getTime();
//			Calendar fechaInicial = Calendar.getInstance()
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(fechaInicial);
//			fechaInicial.format(lblpre200_h_ini_f.getText().toString());
//			Calendar fechaFinal = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			fechaFinal.format(lblpre200_h_fin_f.getText().toString());
//			Calendar fechaInicial = lblpre200_h_ini_f.getText();
//			Calendar fechaFinal = Calendar.getInstance();
//			if (fechaFinalMayorFechaInicial(fechaInicial,fechaFinal)) {
//				mensaje = "Validar Fecha Final, no puede ser superior a la fecha inicial";
//				view = txtQH212B_D;
//				error = true;
//				return false;
//			}
//		}
		
		if(!Util.esDiferente(bean.qh210, 1)){
		
			
	        if (MyUtil.incluyeRango(0,5, edad)){
	       	 	if (Util.esVacio(bean.qh212b_d)){
	         		mensaje = preguntaVacia.replace("$", "La pregunta P.qh212b"); 
	     			view = txtQH212B_D; 
	     			error = true; 
	     			return false; 
	         	}
	       }
			
//			if (!Util.esDiferente(bean.qh212f1, 1)) {
//				
//				if (!Util.esDiferente(bean.qh212j, 1)) {
//					if (Util.esVacio(bean.qh212j_o)) {
//						mensaje = "Justifique en la pregunta 212J";
//						view = txtQH212J_O;
//						error = true;
//						return false;
//					}
//				}
//				
//				if(Util.esVacio(bean.qh212j)){
//					mensaje = "Debe ingresar información en 212J";
//					error = true;
//					return false;
//				}
//				
//			}
			
	        
	
			
			if(!Util.esVacio(bean.qh212c)){
				
				if (!MyUtil.incluyeRango(3,6, bean.qh212c)){
					mensaje = "Debe ingresar información en el rango de 3 a 6, 212C";
					view = txtQH212C;
					error = true;
					return false;
				}
				
			}else{
				mensaje = "Debe ingresar información en 212C";
				view = txtQH212C;
				error = true;
				return false;
			}
			
			
			/**********************************************/
			if(Util.esVacio(bean.qh212d)){
				mensaje = "Debe ingresar información en 212D";
				error = true;
				return false;
			}
			
//			if (!Util.esDiferente(bean.qh212d, 1)) {
//				if(Util.esVacio(bean.qh212f1)){
//					mensaje = "Debe ingresar información en 212F1";
//					error = true;
//					return false;
//				}
//				
//			}
			
			/**********************************************/
			
			if (!Util.esDiferente(bean.qh212d, 1)) {
				if(Util.esVacio(bean.qh212e)){
					mensaje = "Debe ingresar información en 212E";
					
					error = true;
					return false;
				}
				
				if (!Util.esDiferente(bean.qh212e, 2)) {
					if (Util.esVacio(bean.qh212e_o)) {
						mensaje = "Debe ingresar información en especifique 212E";
						view = txtQH212E_O;
						error = true;
						return false;
					}
				}
				
				if(Util.esVacio(bean.qh212f1)){
					mensaje = "Debe ingresar información en 212F1";
					error = true;
					return false;
				}
				
			}
			
			
		
			
			////////////////////////
			
			
			
			if(!Util.esDiferente(bean.qh212f1, 1)){
				
				
				if(Util.esVacio(bean.qh212f)){
					mensaje = "Debe ingresar información en 212F";
					error = true;
					return false;
				}
				
				
				if(MyUtil.incluyeRango(0,5, edad)){
					if(Util.esVacio(bean.qh212gh)){
						mensaje = "Debe ingresar información en 212GHORA";
						error = true;
						return false;
					}
					if(Util.esVacio(bean.qh212gm)){
						mensaje = "Debe ingresar información en 212GMIN";
						error = true;
						return false;
					}
					
					if(Util.esVacio(bean.qh212h_d)){
						mensaje = "Debe ingresar información en 212H_D";
						error = true;
						return false;
					}
				}
				
//				if(Util.esVacio(bean.qh212h_m)){
//					mensaje = "Debe ingresar información en 212H_M";
//					error = true;
//					return false;
//				}
//				if(Util.esVacio(bean.qh212h_y)){
//					mensaje = "Debe ingresar información en 212H_Y";
//					error = true;
//					return false;
//				}
				
				if(!Util.esVacio(bean.qh212i)){
					if (!MyUtil.incluyeRango(3,6, bean.qh212i)){
						mensaje = "Debe ingresar información en el rango de 3 a 6, 212I";
						view = txtQH212I;
						error = true;
						return false;
					}
					
				}else{
					mensaje = "Debe ingresar información en 212I";
					view = txtQH212I;
					error = true;
					return false;
				}
				
				if (!Util.esDiferente(bean.qh212j, 1)) {
					if (Util.esVacio(bean.qh212j_o)) {
						mensaje = "Justifique en la pregunta 212J";
						view = txtQH212J_O;
						error = true;
						return false;
					}
				}
				
				if(Util.esVacio(bean.qh212j)){
					mensaje = "Debe ingresar información en 212J";
					error = true;
					return false;
				}
			}
		
			
			///////////////////////
			
			if (Util.esVacio(bean.qh212k)){
	    		mensaje = preguntaVacia.replace("$", "La pregunta 212K"); 
				view = txtQH212K; 
				error = true; 
				return false; 
	    	}
			
			if (Util.esVacio(bean.qh212l)){
	    		mensaje = preguntaVacia.replace("$", "La pregunta 212L"); 
				view = txtQH212L; 
				error = true; 
				return false; 
	    	}
			
			
			if(!Util.esVacio(bean.qh212k)){
				if(!bean.qh212k.toString().contains(".")){
					mensaje = "Ingrese solo números decimales 212K";
					view = txtQH212K;
					error = true;
					return false;
				}
			}
			
			if(!Util.esVacio(bean.qh212l)){
				if(bean.qh212l.toString().length() == 1){
					mensaje = "Solo registrar dos dígitos 212L";
					view = txtQH212L;
					error = true;
					return false;
				}
			}
			
			
			
		}
		

		
		

		return true;
    }
	
//	public boolean fechaFinalMayorFechaInicial(Calendar fechaInicial, Calendar fechaFinal) {
////		Calendar fechaactual = Calendar.getInstance();
//		long milis1 = fechaInicial.getTimeInMillis();
//		long milis2 = fechaFinal.getTimeInMillis();
//		long diff = milis2 - milis1;
//		return diff < 0;
//	}
	 
	 public void cargarDatos() {
		bean.persona_id=caller.informantehogar.persona_id;
		bean = getSeccion04_05Service().getSeccion05Dialog(bean.id, bean.hogar_id, bean.persona_id,bean.persona_id_orden,seccionesCargado); 
		Asignarvariables(bean);
		bean.qh212b_d = bean.qh212b_d != null ? bean.qh212b_d + "/"	+ bean.qh212b_m + "/" + bean.qh212b_y : null;
		bean.qh212h_d = bean.qh212h_d != null ? bean.qh212h_d + "/"	+ bean.qh212h_m + "/" + bean.qh212h_y : null;
			
	    MyUtil.llenarPersonaResponsable(this.getActivity() , getServiceSeccion01(), spnQH209, bean.id, bean.hogar_id,bean.persona_id_orden);
	    if(MyUtil.incluyeRango(18,App.HogEdadMefMax, edad)){
	    	bean.qh208=2;
			bean.qh209=null;
	    }
	    
	    if(MyUtil.incluyeRango(App.HogEdadMefMin,17, edad)){
			bean.qh208=1;
		}
	    
	    if (bean.qh213 != null) {
	    	bean.qh213 = bean.setConvert213(bean.qh213);
		}
	    	
	    if(MefEstaEmbarazada()){
			bean.qh212=1;
		}
	    else{
	    	if (individual==null) {
	    		bean.qh212=null;		
			}
	    	else{
	    		bean.qh212=2;
	    	}
		}
	  
	    Double hemog=null;    	
	    if (bean.qh211!=null) {
	    	hemog=bean.Redondear(bean.qh211);
		}
	    
	    Double hemog2=null;    	
	    if (bean.qh212f!=null) {
	    	hemog2=bean.Redondear(bean.qh212f);
		}
	    
	    Double temp=null;    	
	    if (bean.qh212k!=null) {
	    	temp=bean.Redondear(bean.qh212k);
		}
	    
	    
	    
		if(bean.qh209a!=null) {lblpre200_h_ini_f.setText(bean.qh209a);}
		if(bean.qh213a!=null) {lblpre200_h_fin_f.setText(bean.qh213a);}

		if(lblpre200_h_ini_f.getText().toString().trim().length()!=0){btnqh200_h_fin.setEnabled(true);}else{btnqh200_h_fin.setEnabled(false);}
		
	    entityToUI(bean);
	    	
	    if(bean.qh211!=null){
			if(!tieneDecimal(bean.qh211.toString())){
				txtQH211.setText(hemog+".0");
			}
			else{
				txtQH211.setText(hemog!=null?hemog.toString():"");
			}
		   	hemoglobina=hemog.toString();
	    }
	    
	    if(bean.qh212f!=null){
			if(!tieneDecimal(bean.qh212f.toString())){
				txtQH212F.setText(hemog2+".0");
			}
			else{
				txtQH212F.setText(hemog2!=null?hemog2.toString():"");
			}
		   	hemoglobina=hemog2.toString();
	    }

	    
	    if(bean.qh212k!=null){
			if(!tieneDecimal(bean.qh212k.toString())){
				txtQH212K.setText(temp+".0");
			}
			else{
				txtQH212K.setText(temp!=null?temp.toString():"");
			}
		   	//hemoglobina=temp.toString();
	    }

	    
	    
		inicio(); 
		
		
		
		onP210ChangeValue();
		
		
		onP212DChangeValue();
		//onP210ChangeValue();
		if(bean.qh211!=null  && bean.qh212f!=null){
			veirficarRestahemo();
		}
		onP212F1ChangeValue();
		
		if(bean.qh210!=null && bean.qh210 == 2){
			q14.setVisibility(View.GONE);
			q15.setVisibility(View.GONE);
			q16.setVisibility(View.GONE);
			q17.setVisibility(View.GONE);
			q18.setVisibility(View.GONE);
			q19.setVisibility(View.GONE);
			q31.setVisibility(View.GONE);
		}
		
		
		
		menoresA4meses();
		
		if (!Util.esDiferente(bean.qh212e, 1)) {
			Util.cleanAndLockView(getActivity(), txtQH212E_O);
		} 
		
		if (!Util.esDiferente(bean.qh212j, 2)) {
			Util.cleanAndLockView(getActivity(), txtQH212J_O);
		} 
		
	} 
	 
	 
	public boolean fechaMayorSistema(Calendar fechareferencia) {
			Calendar fechaactual = Calendar.getInstance();
			long milis1 = fechareferencia.getTimeInMillis();
			long milis2 = fechaactual.getTimeInMillis();
			long diff = milis2 - milis1;
			return diff < 0;
	}
		 
	
	public boolean tieneDecimal(String numero){
	   	boolean punto=false;
	   	int pos=0;
		for(int i=0;i<numero.length();i++){
			if(numero.charAt(i)=='.'){		
				 pos=i;
				 i=numero.length();
			}
		}
		if(pos==0) pos=9999;
		if(pos+1<numero.length()){
			punto=true;
		}
		else{
			punto=false;
		}
		return punto;
	}
	

	public Auditoria GuardarPreguntaDetalle(String pregunta_id, String pregunta_valor ){
		registro=new Auditoria();
		registro.id = bean.id;
		registro.hogar_id = bean.hogar_id;
		registro.persona_id_orden = bean.persona_id_orden;
		registro.pregunta_id = pregunta_id;
		registro.pregunta_valor = pregunta_valor;
		registro.fecha_reg=Util.getFechaActualToString();
		return registro;
	}
	
	 public boolean cantidadMesesS5(){
	    	boolean band=false;
	    	if(bean.qh203d!=null && bean.qh203m!=null && bean.qh203y!=null && txtQH212B_D.getText().toString().trim().length()!=0){
	    		
	    		Integer dia=Integer.parseInt(bean.qh203d);
	    		Integer mes=Integer.parseInt(bean.qh203m);
	    		Integer año=(bean.qh203y);

	    		bean.qh212b_d = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "dd");
				bean.qh212b_m = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "MM");
				bean.qh212b_y = Integer.parseInt(Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "yyyy"));
				
	    		Integer diam=Integer.parseInt(bean.qh212b_d);
	    		Integer mesm=Integer.parseInt(bean.qh212b_m);
	    		Integer añom=(bean.qh212b_y);
	    		
	    	    		
				Calendar fechaNac = new GregorianCalendar(año, mes-1, dia); 
				
				Calendar FechaMedicions5 = new GregorianCalendar(añom, mesm-1, diam);
				Integer mesesS5=MyUtil.CalcularEdadEnMesesFelix(fechaNac,FechaMedicions5);
				
				if(mesesS5>=4){
					band=true;
				}
//			Log.e("","fecha nacimiento dialog ="+dia +" / "+ mes + " / "+año );	
//			Log.e("","fecha medicion s5 dialog ="+diam +" / "+ mesm + " / "+añom );
//			Log.e("","mesesS5 dialog ="+mesesS5);
//			Log.e("","band dialog ="+band);
	    	}
	    	if(bean.qh203d!=null && bean.qh203m!=null && bean.qh203y!=null && txtQH212B_D.getText().toString().trim().length()==0){
	    		
	    		Integer dia=Integer.parseInt(bean.qh203d);
	    		Integer mes=Integer.parseInt(bean.qh203m);
	    		Integer año=(bean.qh203y);

	    		Date fechaActual = new Date();

	    		bean.qh212b_d = Util.getFechaFormateada(fechaActual, "dd");
	    		bean.qh212b_m = Util.getFechaFormateada(fechaActual, "MM");
	    		bean.qh212b_y = Integer.parseInt(Util.getFechaFormateada(fechaActual, "yyyy"));

	    		Integer diam = Integer.parseInt(bean.qh212b_d);
	    		Integer mesm = Integer.parseInt(bean.qh212b_m);
	    		Integer añom = bean.qh212b_y;
	    		
	    	    		
				Calendar fechaNac = new GregorianCalendar(año, mes-1, dia); 
				
				Calendar FechaMedicions5 = new GregorianCalendar(añom, mesm-1, diam);
				Integer mesesS5=MyUtil.CalcularEdadEnMesesFelix(fechaNac,FechaMedicions5);
				
				if(mesesS5>=4){
					band=true;
				}
	    	}
	        return band;
	    }
	 
	 
	 
	 
		
	private void validarMensaje(String msj) {
		ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
		
	private void inicio() {		
		onP208ChangeValue();
		ValidarsiesSupervisora();
		rgQH208.requestFocus();
		rgQH212.readOnly();
		rgQH208.readOnly();
		
	} 
	
	public void onP207AChangeValue(){
//		Log.e("","cantidadMesesS5="+cantidadMesesS5());
		if(cantidadMesesS5()==true){
			rgQH208.setTagSelected(1);
			onP208ChangeValue();
		}
		else{
			rgQH208.setTagSelected(2);
			onP208ChangeValue();
		}
	}

		
	 
	public boolean MefEstaEmbarazada(){
		individual = getService().getCISECCION_01_03(bean.id, bean.hogar_id,bean.persona_id_orden, seccionesCargadoCIS01_03);
		boolean data=false;
		if (individual !=null && individual.qi226!=null) {
			if(!Util.esDiferente(individual.qi226, 1)){
				data=true;
			}
			else{
				data=false;
			}
		}
		                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      return data;
	 }
	 
	
	public void onP212F1ChangeValue(){
		  if(MyUtil.incluyeRango(2,2, rgQH212F1.getTagSelected("").toString())){
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),txtQH212F,txtQH212GH, txtQH212GM,txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O );
		  }else{
			  	MyUtil.LiberarMemoria();
				Util.lockView(getActivity(), false,txtQH212GH, txtQH212GM,txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O );
				//txtQI105Y.setVisibility(View.VISIBLE);
		  }
			
	}
	
	
	public void menoresA4meses(){
		if (MyUtil.incluyeRango(0,5, edad)) {
			if (MyUtil.incluyeRango(2,2, rgQH208.getTagSelected("").toString())) {		
				 Util.lockView(getActivity(), false,rgQH208,txtQH212B_D);	
				 //q8.setVisibility(View.VISIBLE);
				 q0.setVisibility(View.VISIBLE);
				 q1.setVisibility(View.VISIBLE);
				 
				 Util.cleanAndLockView(getActivity(), spnQH209,rgQH210,txtQH211,rgQH212,txtQH212AH,txtQH212AM,rgQH213);
				 q2.setVisibility(View.GONE);
				 q4.setVisibility(View.GONE);
				 q5.setVisibility(View.GONE);
				 q6.setVisibility(View.GONE);
				 q7.setVisibility(View.GONE);
				 q9.setVisibility(View.GONE);
				 
				 q8.setVisibility(View.GONE);
				 
				 q12.setVisibility(View.GONE);
				 q13.setVisibility(View.GONE);
				 q31.setVisibility(View.GONE);
				 q30.setVisibility(View.GONE);
				 q14.setVisibility(View.GONE);
				 q15.setVisibility(View.GONE);
				 q16.setVisibility(View.GONE);
				 q17.setVisibility(View.GONE);
				 q18.setVisibility(View.GONE);
				 q19.setVisibility(View.GONE);
				 q20.setVisibility(View.GONE);
				 q21.setVisibility(View.GONE);
				 //q10.setVisibility(View.GONE);
				 q11.setVisibility(View.GONE);
				 
			}
				
		}
	}
	


	 
	public void onP208ChangeValue() {
		if (MyUtil.incluyeRango(0,5, edad)) {
			if (MyUtil.incluyeRango(2,2, rgQH208.getTagSelected("").toString())) {		
				 Util.lockView(getActivity(), false,rgQH208,txtQH212B_D);	
				 //q8.setVisibility(View.VISIBLE);
				 q0.setVisibility(View.VISIBLE);
				 q1.setVisibility(View.VISIBLE);
				 
				 Util.cleanAndLockView(getActivity(), spnQH209,rgQH210,txtQH211,rgQH212,txtQH212AH,txtQH212AM,rgQH213);
				 q2.setVisibility(View.GONE);
				 q4.setVisibility(View.GONE);
				 q5.setVisibility(View.GONE);
				 q6.setVisibility(View.GONE);
				 q7.setVisibility(View.GONE);
				 q9.setVisibility(View.GONE);
				 
				 
				// ToastMessage.msgBox(this.getActivity(), "FDGFDGFDGFDGFDG" ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
				 q8.setVisibility(View.GONE);
				 
				 q12.setVisibility(View.GONE);
				 q13.setVisibility(View.GONE);
				 q31.setVisibility(View.GONE);
				 q30.setVisibility(View.GONE);
				 q14.setVisibility(View.GONE);
				 q15.setVisibility(View.GONE);
				 q16.setVisibility(View.GONE);
				 q17.setVisibility(View.GONE);
				 q18.setVisibility(View.GONE);
				 q19.setVisibility(View.GONE);
				 q20.setVisibility(View.GONE);
				 q21.setVisibility(View.GONE);
				 //q10.setVisibility(View.GONE);
				 q11.setVisibility(View.GONE);
				 
			}
			else{
				 Util.lockView(getActivity(), false,rgQH208,txtQH212B_D,spnQH209,rgQH210,txtQH211,rgQH213);	
				 q8.setVisibility(View.VISIBLE);
				 q0.setVisibility(View.VISIBLE);
				 q1.setVisibility(View.VISIBLE);
				 q2.setVisibility(View.VISIBLE);
				 q4.setVisibility(View.VISIBLE);
				 onP210ChangeValue();
			}	
		}
		
		
		if (MyUtil.incluyeRango(App.HogEdadMefMin,17, edad)) {
			bean.qh208=1;
			rgQH208.lockButtons(true,1);
			Util.lockView(getActivity(), false,rgQH208,txtQH212B_D,spnQH209,rgQH210);	
			 q8.setVisibility(View.VISIBLE);
			 q0.setVisibility(View.VISIBLE);
			 q1.setVisibility(View.VISIBLE);			 
			 q2.setVisibility(View.VISIBLE);
			 q4.setVisibility(View.VISIBLE);
			 onP210ChangeValue();
		}
		
		if (MyUtil.incluyeRango(18,App.HogEdadMefMax, edad)) {
			bean.qh208=2;
			bean.qh209=null;
			Util.lockView(getActivity(), false,rgQH208,txtQH212B_D,rgQH208,txtQH212B_D);	
			q8.setVisibility(View.VISIBLE);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			Util.cleanAndLockView(getActivity(), spnQH209);
			q2.setVisibility(View.GONE);
			onP210ChangeValue();
		}
	}
	
	public void onP210ChangeValue() {
		if (MyUtil.incluyeRango(0,5, edad)) {
			
			if (MyUtil.incluyeRango(2,2, rgQH210.getTagSelected("").toString())) {
				Util.cleanAndLockView(getActivity(),txtQH212B_D ,lblPreg211_titulo, txtQH211,lblPreg212d_titulo, rgQH212, txtQH212AH,txtQH212AM, txtQH212C, rgQH212D, rgQH212E, txtQH212E_O, txtQH212F, txtQH212GH, txtQH212GM, txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O, txtQH212K, txtQH212L, rgQH212F1);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);	
				q7.setVisibility(View.GONE);
				q8.setVisibility(View.GONE);
				 
				q12.setVisibility(View.GONE);
				q13.setVisibility(View.GONE);
				q14.setVisibility(View.GONE);
				q15.setVisibility(View.GONE);
				q16.setVisibility(View.GONE);
				q17.setVisibility(View.GONE);
				q18.setVisibility(View.GONE);
				q19.setVisibility(View.GONE);
				q20.setVisibility(View.GONE);
				q21.setVisibility(View.GONE);
				q30.setVisibility(View.GONE);
				q31.setVisibility(View.GONE);
				
				
				Util.lockView(getActivity(), false,rgQH213);
				q9.setVisibility(View.VISIBLE);
				//rgQH213.requestFocus();
				onP211ChangeValue();
		     }
			else{
				Util.lockView(getActivity(), false,txtQH212B_D,lblPreg211_titulo,txtQH211,lblPreg212d_titulo, rgQH213, txtQH212AH, txtQH212AM, txtQH212C, rgQH212D, rgQH212E, txtQH212E_O, txtQH212F, txtQH212GH, txtQH212GM, txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O, txtQH212K, txtQH212L, rgQH212F1);
				q5.setVisibility(View.VISIBLE);
				q7.setVisibility(View.VISIBLE);	
				q8.setVisibility(View.VISIBLE);	
				q9.setVisibility(View.VISIBLE);	
				
				
				q12.setVisibility(View.VISIBLE);
				q13.setVisibility(View.VISIBLE);
				q14.setVisibility(View.VISIBLE);
				q15.setVisibility(View.VISIBLE);
				q16.setVisibility(View.VISIBLE);
				q17.setVisibility(View.VISIBLE);
				q18.setVisibility(View.VISIBLE);
				q19.setVisibility(View.VISIBLE);
				q20.setVisibility(View.VISIBLE);
				q21.setVisibility(View.VISIBLE);
				q30.setVisibility(View.VISIBLE);
				q31.setVisibility(View.VISIBLE);
				
				Util.cleanAndLockView(getActivity(), rgQH212);
				q6.setVisibility(View.GONE);
				
				txtQH211.requestFocus();
				onP211ChangeValue();			
			}
		}
		else{
			if (MyUtil.incluyeRango(2,2, rgQH210.getTagSelected("").toString())) {
				Util.cleanAndLockView(getActivity(), txtQH211,  txtQH212AH,txtQH212AM, txtQH212C, rgQH212D, rgQH212E, txtQH212E_O, txtQH212F, txtQH212I, rgQH212J, txtQH212J_O, txtQH212K, txtQH212L, rgQH212F1);
				q5.setVisibility(View.GONE);
				q6.setVisibility(View.GONE);	
				q7.setVisibility(View.GONE);
//				q8.setVisibility(View.GONE);
				
				q12.setVisibility(View.GONE);
				q13.setVisibility(View.GONE);
				q14.setVisibility(View.GONE);
				q15.setVisibility(View.GONE);
				q18.setVisibility(View.GONE);
				q19.setVisibility(View.GONE);
				q20.setVisibility(View.GONE);
				q21.setVisibility(View.GONE);
				
				q30.setVisibility(View.GONE);
				q31.setVisibility(View.GONE);
				 
				Util.lockView(getActivity(), false,rgQH213);
				q9.setVisibility(View.VISIBLE);
				rgQH213.requestFocus();
				onP211ChangeValue();
		     }
			else{
				Util.cleanAndLockView(getActivity(), txtQH212AH,txtQH212AM);			
				q7.setVisibility(View.GONE);
//				q8.setVisibility(View.GONE);
				
				q12.setVisibility(View.VISIBLE);
				q13.setVisibility(View.VISIBLE);
				q14.setVisibility(View.VISIBLE);
				q15.setVisibility(View.VISIBLE);
				q18.setVisibility(View.VISIBLE);
				q19.setVisibility(View.VISIBLE);
				q20.setVisibility(View.VISIBLE);
				q21.setVisibility(View.VISIBLE);

				q30.setVisibility(View.VISIBLE);
				q31.setVisibility(View.VISIBLE);
				
				Util.lockView(getActivity(), false,txtQH211, rgQH213, txtQH212I, txtQH212K,txtQH212L, txtQH212C, rgQH212D , rgQH212E, txtQH212E_O, txtQH212F, rgQH212J, txtQH212J_O, rgQH212F1);
//				q8.setVisibility(View.VISIBLE);
				q5.setVisibility(View.VISIBLE);
				q6.setVisibility(View.VISIBLE);	
				q9.setVisibility(View.VISIBLE);	
				txtQH211.requestFocus();
				onP211ChangeValue();			
			}
		}
	
		
	}
	
	public void onP211ChangeValue() {
		hemoglobina =txtQH211.getText().toString();
    	
    	if(!hemoglobina.equals("")){
    		rgQH213.lockButtons(false,0);
    		rgQH213.lockButtons(true,1,2,3);
    		
//    		BigDecimal decimalValue = new BigDecimal(hemoglobina);
//
//		    if (decimalValue.compareTo(new BigDecimal("8")) > 0) {
//		    	rgQH212E.lockButtons(true,0);
//		    }else{
//		    	rgQH212E.lockButtons(false,0);
//		    }
    		
    		
    	}
    	else{
    		rgQH213.lockButtons(false,1,2,3);
    		rgQH213.lockButtons(true,0);
    	}
    	
    	if (!Util.esDiferente(bean.qh213, 4)) {
			Util.lockView(getActivity(), false, txtQH213_O);
		}    	    	
	}
	
	public void onP212FChangeValue() {
		txtQH212GH.requestFocus();
		
		int color = getResources().getColor(R.color.redpastel);
		int color2 = getResources().getColor(R.color.light_azul_pastel);
		
		BigDecimal decimalValue1 = new BigDecimal(txtQH211.getText().toString());
		BigDecimal decimalValue2 = new BigDecimal(txtQH212F.getText().toString());
		
		//if(decimalValue2.compareTo(BigDecimal.ZERO) != 1){
			if (decimalValue1.compareTo(BigDecimal.ZERO) != 0 && decimalValue2.compareTo(BigDecimal.ZERO) != 0) {
			    
				BigDecimal resultado = decimalValue1.subtract(decimalValue2);

				if (resultado.compareTo(new BigDecimal("0.5")) > 0 || resultado.compareTo(new BigDecimal("-0.5")) < 0) {
				    
					//ToastMessage.msgBox(this.getActivity(), txtQH211.getText().toString()+" "+txtQH212F.getText().toString()+" "+ decimalValue1.subtract(decimalValue2).toString() ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
					ToastMessage.msgBox(this.getActivity(), "el resultado es mayor a 0.5" ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
					
					q19.setBackgroundColor(color);
				} else {
					q19.setBackgroundColor(color2);
					//ToastMessage.msgBox(this.getActivity(), "el resultado no es mayor a 0.5" ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				}
				
			} else {
			}
			
		if(txtQH212F.getText().toString().equals("1")){
			txtQH212K.requestFocus();
		}else{
			txtQH212GH.requestFocus();
		}
		
		//}
	
	}
	
	public void veirficarRestahemo(){
		int color = getResources().getColor(R.color.redpastel);
		int color2 = getResources().getColor(R.color.light_azul_pastel);
		
		BigDecimal decimalValue1 = new BigDecimal(txtQH211.getText().toString());
		BigDecimal decimalValue2 = new BigDecimal(txtQH212F.getText().toString());
		
	
		if (decimalValue1.compareTo(BigDecimal.ZERO) != 0 && decimalValue2.compareTo(BigDecimal.ZERO) != 0) {
		    
			BigDecimal resultado = decimalValue1.subtract(decimalValue2);

			if (resultado.compareTo(new BigDecimal("0.5")) > 0 || resultado.compareTo(new BigDecimal("-0.5")) < 0) {
			    
				//ToastMessage.msgBox(this.getActivity(), txtQH211.getText().toString()+" "+txtQH212F.getText().toString()+" "+ decimalValue1.subtract(decimalValue2).toString() ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				ToastMessage.msgBox(this.getActivity(), "el resultado es mayor a 0.5" ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
				
				q19.setBackgroundColor(color);
				//rgQH212J.check(1);
//				rgQH212J.check(1);
//				rgQH212J.get
				
			} else {
				q19.setBackgroundColor(color2);
				//ToastMessage.msgBox(this.getActivity(), "el resultado no es mayor a 0.5" ,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			}
			
		} else {
		}
	}
	
	
	public void onP212KChangeValue() {
		txtQH212L.requestFocus();
	}
	
	public void onP212LChangeValue() {
		rgQH213.requestFocus();
	}
	
	
	
	
	public void onP212DChangeValue(){
		if (MyUtil.incluyeRango(2,2,rgQH212D.getTagSelected("").toString())) {
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),lblPreg212d_titulo,rgQH212E,txtQH212E_O, txtQH212F, txtQH212GH,txtQH212GM, txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O , rgQH212F1);
			
			q14.setVisibility(View.GONE);
			q15.setVisibility(View.GONE);
			q16.setVisibility(View.GONE);
			q17.setVisibility(View.GONE);
			q18.setVisibility(View.GONE);
			q19.setVisibility(View.GONE);
			q31.setVisibility(View.GONE);
			txtQH212K.requestFocus();
			
//			if(txtQH211.getText().toString().contains(".") && txtQH211.getText().length() > 2 && txtQH211.getText().toString().indexOf('.') == txtQH211.getText().toString().lastIndexOf('.') && txtQH211.getText().charAt(txtQH211.getText().length() - 1) != '.'){
//				
//				BigDecimal decimalValue = new BigDecimal(txtQH211.getText().toString());
//        	
//			    if (decimalValue.compareTo(new BigDecimal("8")) > 0) {
//			    	//ToastMessage.msgBox(this.getActivity(), "es mayor a 8",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
//			    	
//			    }else{
//			    	ToastMessage.msgBox(this.getActivity(), "Requiere segunda medición, valores por debajo del establecido",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
//			    }
//			}
			
			
			
		} else {
			Util.lockView(getActivity(), false,lblPreg212d_titulo,rgQH212E,txtQH212E_O, txtQH212F, txtQH212GH,txtQH212GM, txtQH212H_D, txtQH212I, rgQH212J, txtQH212J_O, rgQH212F1);
			q14.setVisibility(View.VISIBLE);
			q15.setVisibility(View.VISIBLE);
			q16.setVisibility(View.VISIBLE);
			q17.setVisibility(View.VISIBLE);
			q18.setVisibility(View.VISIBLE);
			q19.setVisibility(View.VISIBLE);
			q31.setVisibility(View.VISIBLE);
			//rgQH212E.requestFocus();
			
//			if(txtQH211.getText().toString().contains(".") && txtQH211.getText().length() > 2 && txtQH211.getText().toString().indexOf('.') == txtQH211.getText().toString().lastIndexOf('.') && txtQH211.getText().charAt(txtQH211.getText().length() - 1) != '.'){
//				BigDecimal decimalValue = new BigDecimal(txtQH211.getText().toString());
//			
//				if (decimalValue.compareTo(new BigDecimal("8")) > 0) {
//			    	rgQH212E.lockButtons(true,0);
//			    }else{
//			    	rgQH212E.lockButtons(false,0);
//			    }
//			}
			
		}
	}
	
	 
	public void Asignarvariables(Seccion04_05 bean){		
		valida= new Seccion04_05_Validar();
		valida.id=bean.id;
		valida.hogar_id=bean.hogar_id;
		valida.persona_id=bean.persona_id;
		valida.persona_id_orden=bean.persona_id_orden;
		valida.qh208=bean.qh208;
		valida.qh209=bean.qh209; 
		valida.qh210=bean.qh210; 
		valida.qh211=bean.qh211;
		valida.qh212=bean.qh212; 
		valida.qh212ah=bean.qh212ah;
		valida.qh212am=bean.qh212am;
		valida.qh212b_d=bean.qh212b_d; 
		valida.qh212b_m=bean.qh212b_m; 
		valida.qh212b_y=bean.qh212b_y; 
		valida.qh213=bean.qh213;
		valida.qh209a=bean.qh209a;
		valida.qh213a=bean.qh213a;
	}
	
	 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH208.readOnly();
			spnQH209.readOnly();
			rgQH210.readOnly();
			txtQH211.readOnly();
			rgQH212.readOnly();
			txtQH212C.readOnly();
			rgQH212D.readOnly();
			rgQH212E.readOnly();
			txtQH212E_O.readOnly();
			txtQH212F.readOnly();
			txtQH212GH.readOnly();
			txtQH212GM.readOnly();
			txtQH212I.readOnly();
			rgQH212J.readOnly();
			txtQH212J_O.readOnly();
			txtQH212K.readOnly();
			txtQH212L.readOnly();
			rgQH213.readOnly();
			txtQH213_O.readOnly();
			txtQH212B_D.readOnly();			
			rgQHREC_05.readOnly();
			btnAceptar.setEnabled(false);
			btnqh200_h_ini.setEnabled(false);
			btnqh200_h_fin.setEnabled(false);
		}
	}	
	 
	
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}
		
	public Seccion04_05Service getSeccion04_05Service() { 
		if(seccion04_05Service==null){ 
			seccion04_05Service = Seccion04_05Service.getInstance(getActivity()); 
		} 
		return seccion04_05Service; 
	}
		 
	public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		} 
		return cuestionarioService; 
	}
}
