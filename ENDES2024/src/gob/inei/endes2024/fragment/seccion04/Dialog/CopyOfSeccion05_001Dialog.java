package gob.inei.endes2024.fragment.seccion04.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion04.Seccion04_05Fragment_002;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Seccion04_05_Validar;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.R;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;


public class CopyOfSeccion05_001Dialog extends DialogFragmentComponent{
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH208;
	@FieldAnnotation(orderIndex=2) 
	public SpinnerField spnQH209; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH210; 
	@FieldAnnotation(orderIndex=4) 
	public DecimalField txtQH211; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH212;
	
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQH212AH;
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQH212AM;
	
	@FieldAnnotation(orderIndex=8)
	public DateTimeField txtQH212B_D;
	
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQH213;
	
	
	
	private LabelComponent lblTitulo;
	private GridComponent2 gridhora, grid2;
	public ButtonComponent  btnAceptar,btnCancelar;
	private LabelComponent lblPreg208nino,lblPreg208mef,lblPreg209,lblPreg210,lblPreg211,lblPreg212,lblPreg213,lblpregunta212a,lblpregunta212ah,lblpregunta212am,lblDiaMedicion,lblPreg212B;


	SeccionCapitulo[] seccionesGrabado, seccionesCargado,seccionesCargadoCIS01_03;
	private static Seccion04_05Fragment_002 caller;
	Seccion04_05 bean;
	Seccion04_05_Validar valida;
	Auditoria registro;
	
	CISECCION_01_03  individual=null;
	private CuestionarioService hogarService;
	private CuestionarioService cuestionarioService;
	private Seccion04_05Service seccion04_05Service;
	private Seccion01Service serviceSeccion01;
	public Integer sexo=caller.seleccion.qh06;
	public Integer edad=caller.seleccion.qh07;
	public String hemoglobina;
	LinearLayout q0,q1,q2,q3,q4,q5,q6,q7,q8,q9,q10; 
	private int iniPosition;
    
	boolean iniciando = true;
	
	public static CopyOfSeccion05_001Dialog newInstance(FragmentForm pagina, Seccion04_05 detalle) {
		caller = (Seccion04_05Fragment_002) pagina;
		CopyOfSeccion05_001Dialog f = new CopyOfSeccion05_001Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	public static CopyOfSeccion05_001Dialog newInstance(FragmentForm pagina,Seccion04_05 detalle, int position, List<Seccion04_05> detalles) {

		caller = (Seccion04_05Fragment_002) pagina;
		CopyOfSeccion05_001Dialog f = new CopyOfSeccion05_001Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", (Seccion04_05) detalles.get(position));
		f.iniPosition = position;
		f.setArguments(args);
		return f;
	}
	
	@Override 
	  public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
			bean = (Seccion04_05) getArguments().getSerializable("detalle");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= caller.seleccion.persona_id_orden;
	    String nombre=caller.seleccion.qh02_1;
	    Integer edad= caller.seleccion.qh07;
		getDialog().setTitle("200. N� de Orden: " + orden+"           201. Nombre: "+nombre+"\n202.Edad: "+edad); 
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQH211, 5, 25, null, null); 
		cargarDatos();
		enlazarCajas();
		listening();
		iniciando = false;
		return rootView;
	}
	
	public CopyOfSeccion05_001Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH208","QH209","QH210","QH211","QH212","QH212AH","QH212AM", "QH212B_D", "QH212B_M", "QH212B_Y","QH213","ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH208","QH209","QH210","QH211","QH212","QH212AH","QH212AM", "QH212B_D", "QH212B_M", "QH212B_Y","QH213")};
		seccionesCargadoCIS01_03 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI226")};
	}
	

	@Override
	protected void buildFields() {
		lblTitulo=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion05_titulo).textSize(21).centrar().negrita().colorFondo(R.color.griscabece); 
		
		 if (MyUtil.incluyeRango(0,5, edad)) {
			rgQH208=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh208_3, R.string.secccion04_05qh208_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP208ChangeValue");
		}
		 else{
			rgQH208=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh208_1, R.string.secccion04_05qh208_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP208ChangeValue");
		}
		    
		spnQH209=new SpinnerField(getActivity()).size(altoComponente+15, 400);
		rgQH210=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh210_1, R.string.secccion04_05qh210_2).size(50,450).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onP210ChangeValue");
		
		txtQH211=new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1).callback("onP211ChangeValue");
		
		rgQH212=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh212_1, R.string.secccion04_05qh212_2).size(50,350).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQH213=new RadioGroupOtherField(this.getActivity(),R.string.secccion04_05qh213_1, R.string.secccion04_05qh213_2,R.string.secccion04_05qh213_3,R.string.secccion04_05qh213_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onP213ChangeValue"); 
		
		lblPreg208nino= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh208_nino).textSize(18).alinearIzquierda();
		lblPreg208mef= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh208_mef).textSize(18).alinearIzquierda();
		
		lblPreg209= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh209).textSize(18).alinearIzquierda();
		Spanned texto = Html.fromHtml("210. <b>LEA LA DECLARACI�N DE CONSENTIMIENTO</b> A CADA MUJER O PERSONA RESPONSABLE, DE ACUERDO AL CASO SELECCIONE C�DIGO"); 
		lblPreg210= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda();lblPreg210.setText(texto);
		lblPreg211= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh211).textSize(18).alinearIzquierda();
		Spanned texto2 = Html.fromHtml("212. <b>ACTUALMENTE EMBARAZADA</b> (VEA PGTA. 226 DEL C.I. LUEGO SELECCIONE S. C.)"); 
		lblPreg212= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18).alinearIzquierda(); lblPreg212.setText(texto2);
		lblPreg213= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh213).textSize(18).alinearIzquierda();
		
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		lblpregunta212a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05qh212a).textSize(18);
		
		lblpregunta212ah = new LabelComponent(getActivity()).size(altoComponente,100).text(R.string.secccion04_05qh212ah).negrita().centrar().textSize(19);
		lblpregunta212am = new LabelComponent(getActivity()).size(altoComponente, 100).text(R.string.secccion04_05qh212am).negrita().centrar().textSize(19);
		txtQH212AH = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2);
		txtQH212AM = new IntegerField(getActivity()).size(altoComponente, 80).maxLength(2);
		
		lblPreg212B = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05fecha_medicions5).textSize(18).alinearIzquierda();
		lblDiaMedicion = new LabelComponent(this.getActivity()).size(60, 200).text(R.string.secccion04_dia).textSize(19).centrar();
		txtQH212B_D = new DateTimeField(this.getActivity(), TIPO_DIALOGO.FECHA,	"dd/MM/yyyy").size(altoComponente, 200)	.setRangoYear(App.ANIOPORDEFECTOSUPERIOR,App.ANIOPORDEFECTOSUPERIOR).callback("onP207AChangeValue");
		
		gridhora= new GridComponent2(getActivity(),App.ESTILO,2,0);
		gridhora.addComponent(lblpregunta212ah);
		gridhora.addComponent(txtQH212AH);
		gridhora.addComponent(lblpregunta212am);
		gridhora.addComponent(txtQH212AM);
		
		grid2 = new GridComponent2(this.getActivity(), 2);
		grid2.addComponent(lblDiaMedicion);
		grid2.addComponent(txtQH212B_D);
		
	
		btnCancelar.setOnClickListener(new View.OnClickListener() {
		@Override
			public void onClick(View v) {
					caller.contador=0;
					CopyOfSeccion05_001Dialog.this.dismiss();
				}
			});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.contador=0;
				caller.cargarTabla();
				CopyOfSeccion05_001Dialog.this.dismiss();
			

			}
		});	
	}
	
	@Override
	protected View createUI() {
		buildFields();

		q1 = createQuestionSection(lblTitulo); 
		 if (MyUtil.incluyeRango(0,5, edad)) {
			 q2 = createQuestionSection(lblPreg208nino,rgQH208);
		 }else{
			 q2 = createQuestionSection(lblPreg208mef,rgQH208);
		 }		
		q4 = createQuestionSection(lblPreg209,spnQH209); 		
		q5 = createQuestionSection(lblPreg210,rgQH210);
		q6 = createQuestionSection(lblPreg211,txtQH211);
		q7 = createQuestionSection(lblPreg212,rgQH212);
		q8 = createQuestionSection(lblpregunta212a,gridhora.component());
		q10 = createQuestionSection(lblPreg212B, grid2.component());
		q9 = createQuestionSection(lblPreg213,rgQH213);
		
		
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		
	   if (MyUtil.incluyeRango(0,5, edad)) {
			form.addView(q1); 
			form.addView(q4); 
			form.addView(q5); 
			form.addView(q6);
			form.addView(q8);
			form.addView(q10);
			form.addView(q9);			
	   }
	   else{
			if (MyUtil.incluyeRango(App.HogEdadMefMin,17, edad)) {
				form.addView(q1); 
				form.addView(q2); 
				form.addView(q4); 
				form.addView(q5); 
				form.addView(q6);
				form.addView(q7);
				form.addView(q9);
			}
			else{
				form.addView(q1); 
				form.addView(q5); 
				form.addView(q6);
				form.addView(q7);
				form.addView(q9);
			}
		}
		form.addView(botones);
		return contenedor;
	}

	
	public boolean grabar() { 
    	uiToEntity(bean);

    	hemoglobina =txtQH211.getText().toString();
    	
    	
//    	Log.e("qh212b_d "," "+bean.qh212b_d);
    	if (bean.qh212b_d != null) {
//    		Log.e("dia "," "+Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "dd"));
			bean.qh212b_d = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "dd");
			bean.qh212b_m = Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "MM");
			bean.qh212b_y = Integer.parseInt(Util.getFechaFormateada((Date) txtQH212B_D.getValue(), "yyyy"));
		}
    	
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			if(MyUtil.incluyeRango(18,App.HogEdadMefMax, edad)){
				bean.qh208=2;
				bean.qh209=null;
			}
			if (bean.qh213 != null) {
				bean.qh213 = bean.getConvert213(bean.qh213);
			}
			flag = getSeccion04_05Service().saveOrUpdate(bean, dbTX,seccionesGrabado);
			
//			Log.e("","valida: "+valida.qh209);
//			Log.e("","bean: "+bean.qh209);
			
			if(Util.esDiferente(bean.qh209, valida.qh209)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH209", bean.qh209+""),dbTX);}			
			if(Util.esDiferente(bean.qh210, valida.qh210)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH210", bean.qh210+""),dbTX);}			
			if(Util.esDiferente(bean.qh211, valida.qh211)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH211", bean.qh211+""),dbTX);}			
			if(Util.esDiferente(bean.qh212, valida.qh212)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212", bean.qh212+""),dbTX);}			
			if(Util.esDiferente(bean.qh212ah, valida.qh212ah)){	getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212ah", bean.qh212ah+""),dbTX);}
			if(Util.esDiferente(bean.qh212am, valida.qh212am)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212am", bean.qh212am+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_d, valida.qh212b_d)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212b_d", bean.qh212b_d+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_m, valida.qh212b_m)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212b_m", bean.qh212b_m+""),dbTX);}
			if(Util.esDiferente(bean.qh212b_y, valida.qh212b_y)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH212b_y", bean.qh212b_y+""),dbTX);}
			if(Util.esDiferente(bean.qh213, valida.qh213)){ getSeccion04_05Service().saveOrUpdate(GuardarPreguntaDetalle("QH213", bean.qh213+""),dbTX);}
			
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		    getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
    } 
	
	private boolean validar() { 
		if(!isInRange()) return false; 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
    	
    	if(hemoglobina.equals("")){
			hemoglobina="999";
		}
        
        if (Util.esVacio(bean.qh208)){
    		mensaje = preguntaVacia.replace("$", "La pregunta P.208"); 
			view = rgQH208; 
			error = true; 
			return false; 
    	}
        
        if (MyUtil.incluyeRango(0,5, edad)) {
        	if (MyUtil.incluyeRango(1,1, rgQH208.getTagSelected("").toString())) {
        		if (Util.esVacio(bean.qh209)) { 
	    			mensaje = preguntaVacia.replace("$", "La pregunta P.209"); 
	    			view = spnQH209; 
	    			error = true; 
	    			return false; 
	    		}
				
				if (Util.esVacio(bean.qh210)) { 
	    			mensaje = preguntaVacia.replace("$", "La pregunta P.210"); 
	    			view = rgQH210; 
	    			error = true; 
	    			return false; 
	    		} 
				
				if (MyUtil.incluyeRango(1,1, rgQH210.getTagSelected("").toString())) {	
					
					if(Util.esVacio(bean.qh212ah)){
						mensaje = preguntaVacia.replace("$", "La pregunta P.212H"); 
		    			view = txtQH212AH; 
		    			error = true; 
		    			return false;
					}
					
					if(Util.esVacio(bean.qh212am)){
						mensaje = preguntaVacia.replace("$", "La pregunta P.212M"); 
		    			view = txtQH212AM; 
		    			error = true; 
		    			return false;
					}
					
					if(Util.esMayor(bean.qh212ah, 24)){
						mensaje = "La hora no puede ser superior a 24"; 
		    			view = txtQH212AH; 
		    			error = true; 
		    			return false;
					}
					
					if(Util.esMayor(bean.qh212am, 59)){
						mensaje = "La hora no puede ser superior a 59"; 
		    			view = txtQH212AM; 
		    			error = true; 
		    			return false;
					}
					if(!hemoglobina.equals("999")){
						if(!MyUtil.incluyeRango(6.0,15.0, hemoglobina)){
							mensaje ="VALIDAR �Si hemoglobina corresponde a edad�";
							validarMensaje(mensaje);
						}
					}
					if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		} 
					
		    	
				}
				else{
					if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		} 
				}
				
				if (Util.esVacio(bean.qh212b_d)) {
					mensaje = preguntaVacia.replace("$", "La pregunta P.212B");
					view = txtQH212B_D;
					error = true;
					return false;
				}
				
				if (bean.qh212b_d != null) {
					Integer dia_m = Integer.parseInt(bean.qh212b_d);
					Integer mes_m = Integer.parseInt(bean.qh212b_m);

					if (mes_m < Integer.parseInt(App.getInstance().getVisita().qhvmes_ini)
							|| mes_m == Integer.parseInt(App.getInstance().getVisita().qhvmes_ini)
							&& dia_m < Integer.parseInt(App.getInstance().getVisita().qhvdia_ini)) {
						mensaje = "Fecha de Medici�n debe ser mayor al de Apertura";
						view = txtQH212B_D;
						error = true;
						return false;
					}
				}
				
				if (bean.qh212b_d != null) {
					Calendar fecharef = new GregorianCalendar(bean.qh212b_y,	Integer.parseInt(bean.qh212b_m) - 1,Integer.parseInt(bean.qh212b_d));
					if (fechaMayorSistema(fecharef)) {
						mensaje = "Validar Fecha de Medici�n, es superior al de la Tablet";
						view = txtQH212B_D;
						error = true;
						return false;
					}
				}
	     }
//    	else {
//	 
//	    	   return true;
//	     }
    	         	
        }
        else{ //Mujer
	    	if (MyUtil.incluyeRango(1,1, rgQH208.getTagSelected("").toString())) {	
				if (Util.esVacio(bean.qh209)) { 
	    			mensaje = preguntaVacia.replace("$", "La pregunta P.209"); 
	    			view = spnQH209; 
	    			error = true; 
	    			return false; 
	    		}
				
				if (Util.esVacio(bean.qh210)) { 
	    			mensaje = preguntaVacia.replace("$", "La pregunta P.210"); 
	    			view = rgQH210; 
	    			error = true; 
	    			return false; 
	    		} 
				
				if (MyUtil.incluyeRango(1,1, rgQH210.getTagSelected("").toString())) {	
		    		
		    		if (Util.esVacio(bean.qh212)) { 
		      			mensaje = preguntaVacia.replace("$", "La pregunta P.212"); 
		      			view = rgQH212; 
		      			error = true; 
		      			return false; 
		      		} 
		    		if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		}
		    		if(!hemoglobina.equals("999")){
		    			if(!MyUtil.incluyeRango(6.0,17.0, hemoglobina)){
			    			mensaje ="VALIDAR �Si hemoglobina corresponde a edad�";
			    			validarMensaje(mensaje);
			    		 }
		    		}
		    		
				}
				else{
					if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		} 		
				}
				 
	        }
	    	else{
	        	if (Util.esVacio(bean.qh210)) { 
	    			mensaje = preguntaVacia.replace("$", "La pregunta P.210"); 
	    			view = rgQH210; 
	    			error = true; 
	    			return false; 
	    		} 
				if (MyUtil.incluyeRango(1,1, rgQH210.getTagSelected("").toString())) {	
		    		
		    		if (Util.esVacio(bean.qh212)) { 
		      			mensaje = preguntaVacia.replace("$", "La pregunta P.212"); 
		      			view = rgQH212; 
		      			error = true; 
		      			return false; 
		      		}
		    		if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		} 
		    		if(!hemoglobina.equals("999")){
		    			if(!MyUtil.incluyeRango(7.0,20.0, bean.qh211.toString())){
			    			mensaje ="VALIDAR �Si hemoglobina corresponde a edad�";
			    			validarMensaje(mensaje);
			    		 }
		    		}
				}
				else{
					if (Util.esVacio(bean.qh213)) { 
		    			mensaje = preguntaVacia.replace("$", "La pregunta P.213"); 
		    			view = rgQH213; 
		    			error = true; 
		    			return false; 
		    		} 
				}
	        }
	    }
	    
	    boolean punto=false;
	    punto=tieneDecimal(hemoglobina);
			if(!hemoglobina.equals("999")){
				if(!punto){
					mensaje = "La pregunta P.211 debe tener . decimal"; 
		    		view = txtQH211; 
		    		error = true; 
		    		return false; 
				 }
			}
	    return true;
    }
	

	 
	 public void cargarDatos() {
		bean.persona_id=caller.informantehogar.persona_id;
		bean = getSeccion04_05Service().getSeccion05Dialog(bean.id, bean.hogar_id, bean.persona_id,bean.persona_id_orden,seccionesCargado); 
		Asignarvariables(bean);
		Log.e("","asas VVV "+valida.qh202);
		Log.e("","bean VVV "+bean.qh202);
		
		bean.qh212b_d = bean.qh212b_d != null ? bean.qh212b_d + "/"	+ bean.qh212b_m + "/" + bean.qh212b_y : null;
			
			
	    MyUtil.llenarPersonaResponsable(this.getActivity() , getServiceSeccion01(), spnQH209, bean.id, bean.hogar_id,bean.persona_id_orden);
	    if(MyUtil.incluyeRango(18,App.HogEdadMefMax, edad)){
	    	bean.qh208=2;
			bean.qh209=null;
	    }
	    if (bean.qh213 != null) {
	    	bean.qh213 = bean.setConvert213(bean.qh213);
		}
	    	
	    if(MefEstaEmbarazada()){
			bean.qh212=1;
		}
	    else{
	    	if (individual==null) {
	    		bean.qh212=null;		
			}
	    	else{
	    	bean.qh212=2;
	    	}
		}
	  
	    Double hemog=null;    	
	    if (bean.qh211!=null) {
	    	hemog=bean.Redondear(bean.qh211);
		}
	    	
	    entityToUI(bean);
	    	
	    if(bean.qh211!=null){
			if(!tieneDecimal(bean.qh211.toString())){
				txtQH211.setText(hemog+".0");
			}
			else{
				txtQH211.setText(hemog!=null?hemog.toString():"");
			}
		   	hemoglobina=hemog.toString();
	    }
		inicio(); 
	} 
	 
	private void inicio() {		
		onP208ChangeValue();
		if(MyUtil.incluyeRango(App.HogEdadMefMin,App.HogEdadMefMax, edad)){
			onP210ChangeValue();
		}
		onP211ChangeValue();
		ValidarsiesSupervisora();
		rgQH212.readOnly();
		rgQH208.requestFocus();
	} 
	
	public Auditoria GuardarPreguntaDetalle(String pregunta_id, String pregunta_valor ){
		registro=new Auditoria();
		registro.id = bean.id;
		registro.hogar_id = bean.hogar_id;
		registro.persona_id_orden = bean.persona_id_orden;
		registro.pregunta_id = pregunta_id;
		registro.pregunta_valor = pregunta_valor;
		registro.fecha_reg=Util.getFechaActualToString();
		return registro;
	}
	
	 
	private void validarMensaje(String msj) {
		ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
		
	 
	public boolean MefEstaEmbarazada(){
		individual = getService().getCISECCION_01_03(bean.id, bean.hogar_id,bean.persona_id_orden, seccionesCargadoCIS01_03);
		boolean data=false;
		if (individual !=null && individual.qi226!=null) {
			if(!Util.esDiferente(individual.qi226, 1)){
				data=true;
			}
			else{
				data=false;
			}
		}
		                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      return data;
	 }
	 
	public boolean fechaMayorSistema(Calendar fechareferencia) {
		Calendar fechaactual = Calendar.getInstance();
		long milis1 = fechareferencia.getTimeInMillis();
		long milis2 = fechaactual.getTimeInMillis();
		long diff = milis2 - milis1;
		return diff < 0;
	}
	 
	public boolean tieneDecimal(String numero){
	    	boolean punto=false;
	    	int pos=0;

			 for(int i=0;i<numero.length();i++){
					if(numero.charAt(i)=='.'){		
						 pos=i;
						 i=numero.length();
					}
			}
			 if(pos==0) pos=9999;
			 if(pos+1<numero.length()){
				 punto=true;
			 }else{
				 punto=false;
			 }
			 return punto;
	}
	 
	public void onP208ChangeValue() {
	
		 if (MyUtil.incluyeRango(0,5, edad)) {
			 if (MyUtil.incluyeRango(2,2, rgQH208.getTagSelected("").toString())) {			
				 Util.cleanAndLockView(getActivity(), spnQH209,rgQH210,txtQH211,rgQH213);
				 q4.setVisibility(View.GONE);
				 q5.setVisibility(View.GONE);
				 q6.setVisibility(View.GONE);
				 q8.setVisibility(View.GONE);
			 }
			 else{
				 Util.lockView(getActivity(), false,spnQH209,rgQH210,txtQH211,rgQH213);	
				 q4.setVisibility(View.VISIBLE);
				 q5.setVisibility(View.VISIBLE);
				 onP210ChangeValue();
				 
			 }	
		}
		else{
			if(MyUtil.incluyeRango(App.HogEdadMefMin,17, edad)) {
				 rgQH208.lockButtons(true,1);
			}
			 else{
				bean.qh208=2;
				bean.qh209=null;
				q2.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				if (MyUtil.incluyeRango(2,2, rgQH208.getTagSelected("").toString())) {
					 Util.cleanAndLockView(getActivity(), spnQH209);	
					 onP210ChangeValue();
				 }
				 else{
					 Util.lockView(getActivity(), false,spnQH209);
				 }
			}
			 
		 }		
	}
	 
	public void onP210ChangeValue() {
		 
		 if (MyUtil.incluyeRango(2,2, rgQH210.getTagSelected("").toString())) {
			 if(!iniciando) { rgQH213.setTagIndexSelected(-1);}
 		     rgQH213.lockButtons(false,1,2,3);
			 rgQH213.lockButtons(true,0);
			 rgQH213.requestFocus();
 	     }
		 else{
 	    	if(!iniciando) { rgQH213.setTagIndexSelected(-1); }
 	    	  rgQH213.lockButtons(false,0,1,2,3);
 	    	  txtQH211.requestFocus();
 	     }
		 
		 if (MyUtil.incluyeRango(0,5,edad)) {
			 if (MyUtil.incluyeRango(2,2, rgQH210.getTagSelected("").toString())) {
				 Util.cleanAndLockView(getActivity(), txtQH211);
				 q6.setVisibility(View.GONE);
			 }
			 else{
				 q6.setVisibility(View.VISIBLE);
				 q8.setVisibility(View.VISIBLE);
				 if (MyUtil.incluyeRango(1,1, rgQH208.getTagSelected("").toString())) {
					 Util.lockView(getActivity(), false,txtQH211);
				}		
				 txtQH211.requestFocus();
			 }	
		}
		else{
			 if (MyUtil.incluyeRango(2,2, rgQH210.getTagSelected("").toString())) {
				 if(MefEstaEmbarazada()){
					 Util.cleanAndLockView(getActivity(), txtQH211);
				 }
				 else{
					 Util.cleanAndLockView(getActivity(), txtQH211);	 
				 }				 	
				 q6.setVisibility(View.GONE);
				 q7.setVisibility(View.GONE);
			 }
			 else{
				 q6.setVisibility(View.VISIBLE);
				 q7.setVisibility(View.VISIBLE);
				 q8.setVisibility(View.VISIBLE);
				 if(MefEstaEmbarazada()){
					 Util.lockView(getActivity(), false,txtQH211);	 
				 }
				 else{
					 Util.lockView(getActivity(), false,txtQH211);
				 }
				 txtQH211.requestFocus();
			 }
		 }	
	}
	 
	public void onP211ChangeValue() {
	    	hemoglobina =txtQH211.getText().toString();
	    	
	    	if(!hemoglobina.equals("")){
	    		if(!iniciando) { rgQH213.setTagIndexSelected(-1); }
	    			rgQH213.lockButtons(false,0);
	    			rgQH213.lockButtons(true,1,2,3);
	    	}
	    	else{
	    		if(!iniciando) { rgQH213.setTagIndexSelected(-1); }
	    		rgQH213.lockButtons(false,1,2,3);
    			rgQH213.lockButtons(true,0);
	    	}
	    	if (MyUtil.incluyeRango(0,5,edad)) {
	    		 rgQH213.requestFocus();
	    	}
	    	else{
	    		rgQH212.requestFocus();
	    	}
	}
	 
	public void onP213ChangeValue() {
		btnAceptar.requestFocus();
	}
	
	public void Asignarvariables(Seccion04_05 bean){		
		valida= new Seccion04_05_Validar();
		valida.id=bean.id;
		valida.hogar_id=bean.hogar_id;
		valida.persona_id=bean.persona_id;
		valida.persona_id_orden=bean.persona_id_orden;
		valida.qh209=bean.qh209; 
		valida.qh210=bean.qh210; 
		valida.qh211=bean.qh211; 
		valida.qh212=bean.qh212; 
		valida.qh212ah=bean.qh212ah;
		valida.qh212am=bean.qh212am;
		valida.qh212b_d=bean.qh212b_d; 
		valida.qh212b_m=bean.qh212b_m; 
		valida.qh212b_y=bean.qh212b_y; 
		valida.qh213=bean.qh213; 
	}
	
	 
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH208.readOnly();
			spnQH209.readOnly();
			rgQH210.readOnly();
			txtQH211.readOnly();
			rgQH212.readOnly();
			rgQH213.readOnly();
			txtQH212B_D.readOnly();			
			btnAceptar.setEnabled(false);
		}
	}	
	 
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}
		
	public Seccion04_05Service getSeccion04_05Service() { 
		if(seccion04_05Service==null){ 
			seccion04_05Service = Seccion04_05Service.getInstance(getActivity()); 
		} 
		return seccion04_05Service; 
	}
		 
	public CuestionarioService getCuestionarioService() {
		if(cuestionarioService==null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		} 
		return cuestionarioService; 
	}
}
