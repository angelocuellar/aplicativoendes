package gob.inei.endes2024.fragment.seccion04.Dialog;

import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.hogar.HogarFragment_016;
import gob.inei.endes2024.model.N_LITERAL;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion06_001Dialog extends DialogFragmentComponent{
	
	public interface CPV0301_DETFragment_004Dialog_02Listener {
		void onFinishEditDialog(String inputText);
	}
	

	
	@FieldAnnotation(orderIndex = 1)
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex = 2)
	public ButtonComponent btnCancelar;

	
	public TableComponent tcN1_literal;
	private List<N_LITERAL> lstN1_Literal;
	


	private static HogarFragment_016 caller;
	private CuestionarioService service;
	private LabelComponent lblSubtitulo,lblTitulo;
	private N_LITERAL seleccionado;
	
	LinearLayout q0, q1, q2, q3, q4;

	private CuestionarioService cuestionarioService;

	TC_ClickListener listener;



	
	public static Seccion06_001Dialog newInstance(FragmentForm pagina) {
		caller = (HogarFragment_016) pagina;
		Seccion06_001Dialog f = new Seccion06_001Dialog();
		f.setParent(caller);
		Bundle args = new Bundle();			
		//args.putSerializable("bean", bean);
		f.setArguments(args);
		return f;
	}

	public Seccion06_001Dialog() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);			
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);

		
		listener = new TC_ClickListener();
		tcN1_literal.getListView().setOnItemClickListener(listener);

		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	private void cargarDatos() {		
//		MyUtil.LiberarMemoria();
		lstN1_Literal=new ArrayList<N_LITERAL>();
		
		N_LITERAL element[]=new N_LITERAL[38];
		
		element[0]=new N_LITERAL();
		element[0].n1="01"; element[0].n1_literal="Blanquita";
		lstN1_Literal.add(element[0]);
		
		
		element[1]=new N_LITERAL();
		element[1].n1="02"; element[1].n1_literal="Brisal";
		lstN1_Literal.add(element[1]);
		
		element[2]=new N_LITERAL();
		element[2].n1="03"; element[2].n1_literal="Chavelita";
		lstN1_Literal.add(element[2]);
		
		element[3]=new N_LITERAL();
		element[3].n1="04"; element[3].n1_literal="Chinita";
		lstN1_Literal.add(element[3]);
		
		element[4]=new N_LITERAL();
		element[4].n1="05"; element[4].n1_literal="Cholisal";
		lstN1_Literal.add(element[4]);
		
		element[5]=new N_LITERAL();
		element[5].n1="06"; element[5].n1_literal="Coste�ita";
		lstN1_Literal.add(element[5]);
		
		element[6]=new N_LITERAL();
		element[6].n1="07"; element[6].n1_literal="El Chalan del Norte";
		lstN1_Literal.add(element[6]);
		
		element[7]=new N_LITERAL();
		element[7].n1="08"; element[7].n1_literal="Elita";
		lstN1_Literal.add(element[7]);
		
		element[8]=new N_LITERAL();
		element[8].n1="09"; element[8].n1_literal="Elita sal";
		lstN1_Literal.add(element[8]);
		
		element[9]=new N_LITERAL();
		element[9].n1="10"; element[9].n1_literal="Emsal";
		lstN1_Literal.add(element[9]);
		
		element[10]=new N_LITERAL();
		element[10].n1="11"; element[10].n1_literal="Esal";
		lstN1_Literal.add(element[10]);
		
		element[11]=new N_LITERAL();
		element[11].n1="12"; element[11].n1_literal="Extra sal";
		lstN1_Literal.add(element[11]);
		
		element[12]=new N_LITERAL();
		element[12].n1="13"; element[12].n1_literal="Finita";
		lstN1_Literal.add(element[12]);
		
		element[13]=new N_LITERAL();
		element[13].n1="14"; element[13].n1_literal="La Cocinera";
		lstN1_Literal.add(element[13]);
		
		element[14]=new N_LITERAL();
		element[14].n1="15"; element[14].n1_literal="Liberte�a";
		lstN1_Literal.add(element[14]);
		
		element[15]=new N_LITERAL();
		element[15].n1="16"; element[15].n1_literal="Marina";
		lstN1_Literal.add(element[15]);
		
		element[16]=new N_LITERAL();
		element[16].n1="17"; element[16].n1_literal="Marisal";
		lstN1_Literal.add(element[16]);
		
		element[17]=new N_LITERAL();
		element[17].n1="18"; element[17].n1_literal="Marsal";
		lstN1_Literal.add(element[17]);
		
		element[18]=new N_LITERAL();
		element[18].n1="19"; element[18].n1_literal="Mi purita";
		lstN1_Literal.add(element[18]);
		
		element[19]=new N_LITERAL();
		element[19].n1="20"; element[19].n1_literal="Norsal";
		lstN1_Literal.add(element[19]);
		
		element[20]=new N_LITERAL();
		element[20].n1="21"; element[20].n1_literal="Norte�o";
		lstN1_Literal.add(element[20]);
		
		element[21]=new N_LITERAL();
		element[21].n1="22"; element[21].n1_literal="Premium";
		lstN1_Literal.add(element[21]);
		
		element[22]=new N_LITERAL();
		element[22].n1="23"; element[22].n1_literal="Prodesmi";
		lstN1_Literal.add(element[22]);
		
		element[23]=new N_LITERAL();
		element[23].n1="24"; element[23].n1_literal="Pura sal";
		lstN1_Literal.add(element[23]);
		
		element[24]=new N_LITERAL();
		element[24].n1="25"; element[24].n1_literal="Rica norte�ita";
		lstN1_Literal.add(element[24]);
		
		element[25]=new N_LITERAL();
		element[25].n1="26"; element[25].n1_literal="Sabrosita";
		lstN1_Literal.add(element[25]);
		
		element[26]=new N_LITERAL();
		element[26].n1="27"; element[26].n1_literal="Salcita";
		lstN1_Literal.add(element[26]);
		
		element[27]=new N_LITERAL();
		element[27].n1="28"; element[27].n1_literal="Salcita var";
		lstN1_Literal.add(element[27]);
		
		element[28]=new N_LITERAL();
		element[28].n1="29"; element[28].n1_literal="Salerin Norte�o";
		lstN1_Literal.add(element[28]);
		
		element[29]=new N_LITERAL();
		element[29].n1="30"; element[29].n1_literal="Sali Sal";
		lstN1_Literal.add(element[29]);
		
		element[30]=new N_LITERAL();
		element[30].n1="31"; element[30].n1_literal="Salimar";
		lstN1_Literal.add(element[30]);
		
		element[31]=new N_LITERAL();
		element[31].n1="32"; element[31].n1_literal="Salina";
		lstN1_Literal.add(element[31]);
		
		element[32]=new N_LITERAL();
		element[32].n1="33"; element[32].n1_literal="Supersaladita";
		lstN1_Literal.add(element[32]);
		
		element[33]=new N_LITERAL();
		element[33].n1="34"; element[33].n1_literal="Suprema";
		lstN1_Literal.add(element[33]);
		
		element[34]=new N_LITERAL();
		element[34].n1="35"; element[34].n1_literal="Varsal";
		lstN1_Literal.add(element[34]);
		
		element[35]=new N_LITERAL();
		element[35].n1="36"; element[35].n1_literal="Viva";
		lstN1_Literal.add(element[35]);
		
		element[36]=new N_LITERAL();
		element[36].n1="37"; element[36].n1_literal="Emsal Marina";
		lstN1_Literal.add(element[36]);
		
		element[37]=new N_LITERAL();
		element[37].n1="96"; element[37].n1_literal="OTROS";
		lstN1_Literal.add(element[37]);
		
		
		
		tcN1_literal.setData(lstN1_Literal, "n1", "n1_literal");
		inicio();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		//caller.refrescarTabla();
		//caller.tcCapPersonas.getListView().requestFocus();
	}

	@Override
	protected View createUI() {
		buildFields();
		q1 = createQuestionSection(lblTitulo);
		q2 = createQuestionSection(tcN1_literal.getTableView());
		tcN1_literal.setSelectorData(R.drawable.selector_sombra_lista);

		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(q2);
		form.addView(botones);
		return contenedor;
	}

	@Override
	protected void buildFields() {
				
		lblTitulo = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.cabecera_buscar).textSize(21).centrar().negrita().colorFondo(R.color.griscabece);
		lblSubtitulo = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).centrar().negrita().colorFondo(R.color.amarillo);
		tcN1_literal = new TableComponent(getActivity(), this, App.ESTILO).size(450, 550).headerHeight(65).dataColumHeight(60).filter("n1_literal");
		
		tcN1_literal.addHeader(R.string.codigo_sal,1.5f,TableComponent.ALIGN.LEFT);
		tcN1_literal.addHeader(R.string.marca_sal,6,TableComponent.ALIGN.LEFT);
		tcN1_literal.setColorCondition("COLOR", Util.getHMObject("1",R.color.celesteclarito,"0",R.color.default_color));
	

		btnAceptar = new ButtonComponent(getParent().getActivity(), App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(), App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Seccion06_001Dialog.this.dismiss();		
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = validar();
				if (!flag) {
					return;
				}
				caller.seleccionar(seleccionado);
				Seccion06_001Dialog.this.dismiss();
			}
		});		
	}
	

	private CuestionarioService getService() {
		if (service == null) {
			service = CuestionarioService.getInstance(getActivity());
		}
		return service;
	}

	private boolean grabar() {
		if (!validar()) {
			if (error) {
				if (!mensaje.equals("")) {
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,	ToastMessage.DURATION_LONG);
				}
				if (view != null) {
					view.requestFocus();
				}
			}
			return false;
		}
		return true;
	}

	private boolean validar() {
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (seleccionado == null) {
			return false;
		}		
		return true;
	}

	private void inicio() {
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		
		return cuestionarioService;

	}
	private class TC_ClickListener implements OnItemClickListener {

		public TC_ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			seleccionado= (N_LITERAL) tcN1_literal.getAdapter().getItem(arg2);
			
			for(int i=0;i<lstN1_Literal.size();i++){
				lstN1_Literal.get(i).color="0";
				if(arg2==i){
					lstN1_Literal.get(i).color="1";
				}
			}
			tcN1_literal.setData(lstN1_Literal, "n1", "n1_literal");
			tcN1_literal.setSeleccionPosition(arg2);
		}
	}
	
}
