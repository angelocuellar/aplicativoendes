package gob.inei.endes2024.fragment.seccion04; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion04.Dialog.Seccion04_001Dialog;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.N_LITERAL;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.UsuarioService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion04_05Fragment_001 extends FragmentForm{
	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnQH215_C;
	
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnQH216_C;
	
	@FieldAnnotation(orderIndex = 3)
	public TextAreaField txtQH_OBS_SECCION04;

	public TableComponent tablaHOGAR;
	private LabelComponent lblTitulo;
	public Seccion04_05 seleccion; 
	public Seccion01 informantehogar;
	Seccion04_05 secccion04_05; 
	 Hogar hogar;
	 
	 private UsuarioService serviceUsuario;
	 public List<Seccion01> detalles;
	 public List<Seccion04_05> detalles04_05;
	 private Seccion01Service Personaservice;
	 private Seccion04_05Service Persona04_05Service;
	 Seccion04ClickListener adapter;
	 public Integer contador=0;
	 public N_LITERAL seleccionado=null;
	
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblDescripcion1;
	private LabelComponent lblDescripcion2;

	private LabelComponent lblObservaciones,lblAntro,lblAux;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	
	private GridComponent2 grid1;
	private GridComponent2 grid2;
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesGrabado2; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoSeccion01;
	SeccionCapitulo[] seccionesCargadoSeccion04;	
	SeccionCapitulo[] seccionesGrabadoAux;  
	SeccionCapitulo[] seccionesCargadoAux; 
	
	public Seccion04_05Fragment_001() {} 
	public Seccion04_05Fragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion04ClickListener();
		tablaHOGAR.getListView().setOnItemClickListener(adapter);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QH02_1","QH07","QH7DD","QH7MM","QH203Y","QH06","QH204","QH205","QH205C","QH205C1","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","ID","HOGAR_ID","PERSONA_ID","ESTADO","QHREC_04")}; 
		
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y")};
		seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")};
		
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID,QH02_1,QH06,QH07,QH7DD,QH7MM", "QH215_C", "QH216_C")};
		
		seccionesCargadoAux = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH215","QH215_C","QH216","QH216_C","QH_OBS_SECCION04","ID","HOGAR_ID","PERSONA_INFORMANTE_ID")};
		seccionesGrabadoAux = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH215","QH215_C","QH216","QH216_C","QH_OBS_SECCION04")}; 
		
		return rootView;
	} 
	
	@Override 
	protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_05).textSize(21).centrar().negrita(); 
		
		tablaHOGAR = new TableComponent(getActivity(), this,App.ESTILO).size(500, 1400).headerHeight(45).dataColumHeight(45);
		tablaHOGAR.addHeader(R.string.m_orden_b18_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_nombre_hogar, 3f,TableComponent.ALIGN.LEFT);
		tablaHOGAR.addHeader(R.string.m_edad_meses_hogar, 1.3f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_edad_hogar, 1.2f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_fecha_nacim_hogar, 2.5f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_sexo, 1.5f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_peso_hogar, 1.9f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_talla_hogar, 1.9f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_talla_hogar2, 1.9f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_medido_hogar, 1.5f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_resultado_hogar, 2f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.addHeader(R.string.m_fecha_medicion_hogar, 2.5f,TableComponent.ALIGN.CENTER);
		tablaHOGAR.setColorCondition("QHREC_04", Util.getHMObject("1",R.color.dark_purpura));
		lblDescripcion1=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_desc1).textSize(15).alinearIzquierda(); 
		lblDescripcion2=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion04_desc2).textSize(15).alinearIzquierda(); 
		
		lblAntro=new LabelComponent(this.getActivity()).size(60,550).text(R.string.secccion04_nombreantro).textSize(15).centrar().negrita(); 
		
		lblAux=new LabelComponent(this.getActivity()).size(60,550).text(R.string.secccion04_nombreaux).textSize(15).centrar().negrita();
		
		lblObservaciones=new LabelComponent(this.getActivity()).size(60, 780).text(R.string.secccion04_observaciones).textSize(15);
		txtQH_OBS_SECCION04 = new TextAreaField(getActivity()).size(200, 650).maxLength(500).alfanumerico();
		txtQH_OBS_SECCION04.setCallback("OcultarTecla");
		
		spnQH216_C = new SpinnerField(getActivity()).size(altoComponente+15, 600);
		spnQH215_C = new SpinnerField(getActivity()).size(altoComponente+15, 600);
		
		grid1 = new GridComponent2(this.getActivity(),Gravity.CENTER, 1,0);
		grid1.addComponent(lblAntro,1);
		grid1.addComponent(spnQH215_C,1);
		
		grid2 = new GridComponent2(this.getActivity(),Gravity.CENTER, 1,0);
		grid2.addComponent(lblAux,1);
		grid2.addComponent(spnQH216_C,1);
    } 

    public void abrirDetalle(Seccion04_05 tmp) {
		FragmentManager fm = Seccion04_05Fragment_001.this.getFragmentManager();
		Seccion04_001Dialog aperturaDialog = Seccion04_001Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
  
  
    @Override 
    protected View createUI() {
    	buildFields(); 
		q0 = createQuestionSection(lblTitulo); 
		q1 = createQuestionSection(tablaHOGAR.getTableView());
		q2 = createQuestionSection(lblDescripcion1);
		q3 = createQuestionSection(lblDescripcion2);
		q4 = createQuestionSection(grid1.component());
		q5 = createQuestionSection(grid2.component()); 
		q6 = createQuestionSection(R.string.secccion04_obs_titulo,lblObservaciones,txtQH_OBS_SECCION04); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {
		uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		
		try {
			Usuario antro = (Usuario) spnQH215_C.getValue();
			Usuario aux = (Usuario) spnQH216_C.getValue();
			if (!antro.nombres.equals("-- SELECCIONE --"))
				hogar.qh215=antro.nombres+" "+antro.apellidos;
			if (!aux.nombres.equals("-- SELECCIONE --"))
				hogar.qh216=aux.nombres+" "+aux.apellidos;
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoAux)){
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		return true; 
    }

	private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		boolean band=false;
		detalles04_05= new ArrayList<Seccion04_05>();
    	detalles04_05= getSeccion04_05Service().getSeccion04_05ListaVista(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,informantehogar.persona_id,seccionesCargadoSeccion04);

		if(detalles04_05!=null){
			for(int i=0;i<detalles04_05.size();i++){
				if(detalles04_05.get(i).qh207!=null){
					if(detalles04_05.get(i).qh207==1 || detalles04_05.get(i).qh207==5){
						band=true;
					}
				}
			}			
			if (!getSeccion04_05Service().TodoLosMiembrosS4Completados(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,informantehogar.persona_id)) {
				mensaje = "Debe completar la P.207 para todos los miembros, antes de pasar a SECCI�N 05";
				view = tablaHOGAR;
				error = true;
				return false;
			}
		}
//		if(band){	
			/*COMENTADO 12/09/2020 SOLICITADO POR CORREO DE METODOLOGIA FCP*/
			/*Usuario antro = (Usuario) spnQH215_C.getValue();
			if (antro.nombres.equals("-- SELECCIONE --")) {
				mensaje = "Debe seleccionar una Antropometrista";
				error = true;
				return false;
			}
			Usuario aux = (Usuario) spnQH216_C.getValue();
			if (aux.nombres.equals("-- SELECCIONE --")) {
				mensaje = "Debe seleccionar una Auxiliar";
				error = true;
				return false;
			}*/
			/*********** FIN 12/09/2020*****/
//		}
		return true; 
    }
	
    @Override 
    public void cargarDatos() {
    	if(App.getInstance().getHogar()!=null){
    	hogar = getCuestionarioService().getHogar(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargadoAux);
    	
    	MyUtil.llenarAntropo(this.getActivity() , getServiceUsuario(), spnQH215_C, App.getInstance().getUsuario().id, hogar.qh215_c, hogar.qh215);        	
    	MyUtil.llenarAuxiliares(this.getActivity() , getServiceUsuario(), spnQH216_C, App.getInstance().getUsuario().id, hogar.qh216_c, hogar.qh216);        	
    	   	
    	if (App.getInstance().getMarco().id!=null && App.getInstance().getHogar()!=null && App.getInstance().getHogar().hogar_id!=null ) { 		
        	informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
        	if(informantehogar!=null){
        		getPersonaService().ModificarInformanteSH04(informantehogar);
//              Log.e("","INFORMANTE: DEL HOGAR: "+informantehogar.persona_id);	
        	}
        		
		}
    	
   	
    	if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id;  
	    }   	
  
		entityToUI(hogar); 
		
		cargarTabla();
		inicio();
    	}
    }
    
    private void inicio() {
//    	BlanquearPantallaCuandoNoDebeMostrar();
    	ValidarsiesSupervisora();
    	onrgQH215ChangeValue();
    	spnQH215_C.requestFocus();
    } 
    private void validarMensaje(String msj) {
		ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
    
//	private void BlanquearPantallaCuandoNoDebeMostrar() {
//		if (App.FRAGMENTO_QHSECCION04==false) {
//			q0.setVisibility(View.GONE);
//			q1.setVisibility(View.GONE);
//			q2.setVisibility(View.GONE);
//			q3.setVisibility(View.GONE);
//			q4.setVisibility(View.GONE);
//			q5.setVisibility(View.GONE);
//			q6.setVisibility(View.GONE);
//		}
//		else{
//			q0.setVisibility(View.VISIBLE);
//			q1.setVisibility(View.VISIBLE);
//			q2.setVisibility(View.VISIBLE);
//			q3.setVisibility(View.VISIBLE);
//			q4.setVisibility(View.VISIBLE);
//			q5.setVisibility(View.VISIBLE);
//			q6.setVisibility(View.VISIBLE);
//		}
//	}
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			spnQH215_C.readOnly();
			spnQH216_C.readOnly();
			txtQH_OBS_SECCION04.setEnabled(false);
		}
	}
    
    public void cargarTabla() {
    	if(informantehogar!=null){
    	detalles04_05= new ArrayList<Seccion04_05>();
    	detalles04_05= getSeccion04_05Service().getSeccion04_05ListaVista(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,informantehogar.persona_id,seccionesCargadoSeccion04);
//    	Log.e("detalles04_05.size:: ",""+detalles04_05.size());
    	tablaHOGAR.setData(detalles04_05 ,"persona_id_orden","qh02_1","getEdadMesesSeccion4","qh07","getFecha2","getSexo","getPeso","getTalla","getTalla2","getMedido","getResultado","getFecha3");
//    	tablaHOGAR.setBorder("estado");
    	for (int row = 0; row < detalles04_05.size(); row++) {
    		
    		if(detalles04_05.get(row).qh205 != null ){
    			//ToastMessage.msgBox(this.getActivity(), detalles04_05.get(row).qh211.toString(),ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
    			tablaHOGAR.setCellTextColor3(row, 7, R.color.black);
    		}else{
    			//ToastMessage.msgBox(this.getActivity(), "sin data",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_SHORT);
    			tablaHOGAR.setCellTextColor2(row, 7, R.color.black);
    		}
    		
    		if( detalles04_05.get(row).qh205c != null ){
    			tablaHOGAR.setCellTextColor3(row, 8, R.color.black);
    		}else{
    			tablaHOGAR.setCellTextColor2(row, 8, R.color.black);
    		}
    		
    		if( /*detalles04_05.get(row).getEdadMesesSeccion5() != null ||*/ detalles04_05.get(row).getEdadMesesSeccion4() != "" ){
    			tablaHOGAR.setCellTextColor3(row, 2, R.color.black);
    		}else{
    			tablaHOGAR.setCellTextColor2(row, 2, R.color.black);
    		}
    		
    		
			if (obtenerEstado(detalles04_05.get(row)) == 1) {
				// borde de color azul
				tablaHOGAR.setBorderRow(row, true);
			} else if (obtenerEstado(detalles04_05.get(row)) == 2) {
				// borde de color rojo
				tablaHOGAR.setBorderRow(row, true, R.color.red);
			} else {
				tablaHOGAR.setBorderRow(row, false);
			}
		}
    	registerForContextMenu(tablaHOGAR.getListView());
    	}
    }
    private int obtenerEstado(Seccion04_05 detalle) {
		if (!Util.esDiferente(detalle.estado, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalle.estado,1)) {
			return 2;
		}
		return 0;
	}

    public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(tablaHOGAR.getWindowToken(), 0);
	}
    
    public void onrgQH215ChangeValue() {
    	//txtQH216.requestFocus();
    }
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public Seccion01Service getPersonaService(){
    	if(Personaservice == null){
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }
    
    public Seccion04_05Service getSeccion04_05Service(){
    	if(Persona04_05Service == null){
    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
    	}
    	return Persona04_05Service;
    }
    private CuestionarioService getService() {
		if (cuestionarioService == null){
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
    public UsuarioService getServiceUsuario() {
		if (serviceUsuario == null) {
			serviceUsuario = UsuarioService.getInstance(getActivity());
		}
		return serviceUsuario;
	}
    public void abrirDetalle(Seccion04_05 tmp, int index, List<Seccion04_05> detalles) {
		FragmentManager fm = Seccion04_05Fragment_001.this.getFragmentManager();
		Seccion04_001Dialog aperturaDialog = Seccion04_001Dialog.newInstance(Seccion04_05Fragment_001.this, tmp, index, detalles);
		aperturaDialog.show(fm, "aperturaDialog");
	}
    
    public class Seccion04ClickListener implements OnItemClickListener {
		public Seccion04ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			seleccion = (Seccion04_05) detalles04_05.get(arg2);
			if(contador==0){
			abrirDetalle(seleccion, arg2, (List<Seccion04_05>) detalles04_05);
			contador=1;
			}
		}
	}

	@Override
	public Integer grabadoParcial() {
		uiToEntity(hogar);
		try {
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoAux);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
}