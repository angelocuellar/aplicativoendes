package gob.inei.endes2024.fragment.seccion04; 
import java.sql.SQLException; 
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion04.Dialog.Seccion05_001Dialog;
import gob.inei.endes2024.model.*;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.R;
import gob.inei.dnce.components.Entity.SeccionCapitulo; 
import gob.inei.dnce.components.FragmentForm; 
import gob.inei.dnce.components.LabelComponent; 
import gob.inei.dnce.components.MasterActivity; 
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage; 
import gob.inei.dnce.util.Util; 
import android.os.Bundle; 
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater; 
import android.view.View; 
import android.view.ViewGroup; 
import android.widget.AdapterView;
import android.widget.LinearLayout; 
import android.widget.ScrollView; 
import android.widget.AdapterView.OnItemClickListener;

public class CopyOfSeccion04_05Fragment_002 extends FragmentForm{ 
	

	public TableComponent tablaHOGAR;

	public Seccion04_05 seleccion; 
	Seccion04_05 secccion04_05; 
	public Seccion01 informantehogar;
	 Hogar hogar;
	 public List<Seccion01> detalles;
	 public List<Seccion04_05> detalles04_05;
	 private Seccion01Service Personaservice;
	 private Seccion04_05Service Persona04_05Service;
	 Seccion05ClickListener adapter;
	
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	private LabelComponent lblDescripcion1;
	
	public TextAreaField txtQH_OBS_SECCION05;
	public Integer contador=0;
	
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;

	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesGrabado2; 
	SeccionCapitulo[] seccionesCargado; 
	SeccionCapitulo[] seccionesCargadoSeccion01;
	SeccionCapitulo[] seccionesCargadoSeccion04;
	
	SeccionCapitulo[] seccionesGrabadoAux;  
	SeccionCapitulo[] seccionesCargadoAux; 
// 
	public CopyOfSeccion04_05Fragment_002() {} 
	public CopyOfSeccion04_05Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion05ClickListener();
		tablaHOGAR.getListView().setOnItemClickListener(adapter);
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","ID","HOGAR_ID","PERSONA_ID")}; 
		seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID_ORDEN","QH02_1","QH07","QH7DD","QH7MM","QH203Y","QH06","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y","QH209","QH210","QH211","QH213","ID","HOGAR_ID","PERSONA_ID","ESTADOS5")}; 
		
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH203D","QH203M","QH203Y","QH204","QH205","QH206","QH207","QH207A_D","QH207A_M","QH207_Y")};
		seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")};
		
		seccionesCargadoSeccion01 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"PERSONA_ID,QH02_1,QH06,QH07,QH7DD,QH7MM")};
		
		seccionesCargadoAux = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH_OBS_SECCION05","ID","HOGAR_ID","PERSONA_INFORMANTE_ID")};
		seccionesGrabadoAux = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH_OBS_SECCION05")}; 
		
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion05_titulo).textSize(21).centrar().negrita(); 
		lblDescripcion1=new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.secccion05_titulo_desc).textSize(15).alinearIzquierda(); 
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tablaHOGAR = new TableComponent(getActivity(), this,App.ESTILO).size(350, 1250)	.headerHeight(65).dataColumHeight(60);			
//		}
//		else{
			tablaHOGAR = new TableComponent(getActivity(), this,App.ESTILO).size(500, 1250)	.headerHeight(45).dataColumHeight(45);
//		}
		
		tablaHOGAR.addHeader(R.string.m_orden_b18_hogar, 1.2f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_nombre_hogar, 2f,TableComponent.ALIGN.LEFT);
		tablaHOGAR.addHeader(R.string.m_edad_hogar, 0.8f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_fecha_nacim_hogar, 2f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_sexo, 1f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_consentimiento_hogar, 2.2f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_hemoglobina_hogar, 2.5f,TableComponent.ALIGN.RIGHT);
		tablaHOGAR.addHeader(R.string.m_resultado_hogar, 1.5f,TableComponent.ALIGN.RIGHT);
		txtQH_OBS_SECCION05= new TextAreaField(getActivity()).size(200, 650).maxLength(500).alfanumerico();
    } 

    public void abrirDetalle(Seccion04_05 tmp) {
		FragmentManager fm = CopyOfSeccion04_05Fragment_002.this.getFragmentManager();
		Seccion05_001Dialog aperturaDialog = Seccion05_001Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");

	}
  
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(lblTitulo); 
		q1 = createQuestionSection(tablaHOGAR.getTableView());
		q2 = createQuestionSection(lblDescripcion1);
		q3 = createQuestionSection(R.string.secccion04_obs_s5,txtQH_OBS_SECCION05); 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q2);
		form.addView(q1);
		form.addView(q3);
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
    	uiToEntity(hogar); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoAux)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		

		
		return true; 
    }
 
   
	private boolean validar() { 

		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	detalles04_05= new ArrayList<Seccion04_05>();
    	detalles04_05= getSeccion04_05Service().getSeccion04_05ListarTabla(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,seccionesCargadoSeccion04);
    	informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
        hogar = getCuestionarioService().getHogar(App.getInstance().getHogar().id, App.getInstance().getHogar().hogar_id,seccionesCargadoAux); 
    	if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id;  
	    } 
		entityToUI(hogar); 
		cargarTabla();
		inicio(); 
    } 
    private void inicio() { 
    	ValidarsiesSupervisora();
    } 
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQH_OBS_SECCION05.setEnabled(false);
		}else{
			txtQH_OBS_SECCION05.setEnabled(true);
		}
			
	}
    
    public void cargarTabla() {
    	detalles04_05= new ArrayList<Seccion04_05>();
    	detalles04_05= getSeccion04_05Service().getSeccion04_05ListaVista(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,informantehogar.persona_id,seccionesCargadoSeccion04);
   
    	tablaHOGAR.setData(detalles04_05 ,"persona_id_orden","qh02_1","qh07","getFecha2","getSexo","getConsentimiento","getHemoglonina","getResultadoS5");
    	tablaHOGAR.setBorder("estados5");
    	registerForContextMenu(tablaHOGAR.getListView());
        
    }

    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    public Seccion01Service getPersonaService(){
    	if(Personaservice == null)
    	{
    		Personaservice = Seccion01Service.getInstance(getActivity());
    	}
    	return Personaservice;
    }
    
    public Seccion04_05Service getSeccion04_05Service(){
    	if(Persona04_05Service == null)
    	{
    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
    	}
    	return Persona04_05Service;
    }
    
//    
//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
//            super.onCreateContextMenu(menu, v, menuInfo);
//            if (v.equals(tablaHOGAR.getListView())) {
//                    menu.setHeaderTitle("Medicion del Hemoglobina");
//                    menu.add(1, 0, 1, "Mujeres de 15 a 49 a�os");
//                    menu.add(1, 1, 1, "Ni�os menores de 6 a�os");
//                    
//                    
//                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//                    seleccion = (Seccion04_05) info.targetView.getTag();
//                    Integer edad=seleccion.qh07;
//                    
//              
//                    
//                    if (MyUtil.incluyeRango(0,5, edad)) {
//                    	menu.getItem(0).setVisible(false);
//                    }else{
//                        	menu.getItem(1).setVisible(false);
//                        
//                    }
//                  
//            }
//    }
//
//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//            if (!getUserVisibleHint())
//                    return false;
//            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
//                            .getMenuInfo();
//                      
//            
//            	   if (item.getGroupId() == 1) {
//            		   switch (item.getItemId()) {
//                       case 0: 
//                       	       RealizarMedicion((Seccion04_05) detalles04_05.get(info.position));
//                               break;
//                       case 1: 
//                   	    		RealizarMedicion((Seccion04_05) detalles04_05.get(info.position));
//                               break;
//                       }
//            	   }
//            
//            return super.onContextItemSelected(item);
//    }
    
    public void RealizarMedicion(Seccion04_05 seccion4) {
	  	abrirDetalle(seccion4);
    }
    
    public void abrirDetalle(Seccion04_05 tmp, int index, List<Seccion04_05> detalles) {
		FragmentManager fm = CopyOfSeccion04_05Fragment_002.this.getFragmentManager();
		Seccion05_001Dialog aperturaDialog = Seccion05_001Dialog.newInstance(CopyOfSeccion04_05Fragment_002.this, tmp, index, detalles);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}

    public boolean cantidadMeses(){
    	boolean band=false;
    	if(seleccion.qh7dd!=null && seleccion.qh7mm!=null && seleccion.qh203y!=null){
    		Integer dia=Integer.parseInt(seleccion.qh7dd);
    		Integer mes=Integer.parseInt(seleccion.qh7mm);
    		Integer a�o=(seleccion.qh203y);
    		
    		Integer diam=Integer.parseInt(seleccion.qh207a_d);
    		Integer mesm=Integer.parseInt(seleccion.qh207a_m);
    		Integer a�om=(seleccion.qh207_y);
    		

		Calendar calendar = new GregorianCalendar(a�o, mes-1, dia); 
		
		Calendar fechaactual = new GregorianCalendar(a�om, mesm-1, diam);
	Integer meses=MyUtil.CalcularEdadEnMesesFelix(calendar,fechaactual);
	if(meses>=4){
		band=true;
	}
	
	
    	}
        return band;
    }
    
    public class Seccion05ClickListener implements OnItemClickListener {
		public Seccion05ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			
			seleccion = (Seccion04_05) detalles04_05.get(arg2);
//			Log.e("","edad selec ="+seleccion.qh07);
			
			if(MyUtil.incluyeRango(0, 5, seleccion.qh07)){
//				Log.e("","cantidad meses="+cantidadMeses());
				if(cantidadMeses() || seleccion.qh07>0){
					 seleccion.qh208=1;
					 if(contador==0){
				     abrirDetalle(seleccion, arg2, (List<Seccion04_05>) detalles04_05);
				     contador=1;
					 }
				}else{
					seleccion.qh208=2;
				}
				
				try {	
					getSeccion04_05Service().saveOrUpdate(seleccion,"QH208");
					
				} catch (Exception e) {
					// TODO: handle exception
				}
			}else{
				if(contador==0){
				abrirDetalle(seleccion, arg2, (List<Seccion04_05>) detalles04_05);
				contador=1;
				}
			}
			
			
		}
	}
    
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(hogar);
		try {
			
			boolean flag=false;
			flag=getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoAux);
			if (!flag) {
				ToastMessage.msgBox(this.getActivity(),	"Los datos no pudieron ser guardados.",	
						ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
				return App.NODEFINIDO;
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			return App.NODEFINIDO;
		}
		return App.HOGAR;
	}
} 
