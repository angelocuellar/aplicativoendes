package gob.inei.endes2024.fragment.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.fragment.MarcoFragment;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_COL04;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.CISECCION_05;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.model.CISECCION_10_03;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.IMIGRACION;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.MIGRACION;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class MarcoModificarViviendaDialog extends DialogFragmentComponent implements Respondible {
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtNSELV;
	@FieldAnnotation(orderIndex=2) 
	private LabelComponent lblvivienda,lblcambiodevivienda; 
	public SpinnerField spnViviendaCambio;
	public ButtonComponent btnAceptar,btnCancelar;
	private static MarcoFragment caller;
	private Marco bean;
	private Caratula caratula;
	public CuestionarioService cuestionarioService;
	public HogarService hogarservice;
	public Seccion01Service personasservice;
	public VisitaService visitas;	
	public Seccion03Service seccion03service;
	public Seccion04_05Service seccion04_5service;
	public MarcoService service;
	
	public List<Seccion01> personas;
	public List<MORTALIDAD> fallecidas;
	public List<MIGRACION> migracion;
	public List<IMIGRACION> imigracion;
	public List<Visita> visitashogar;
	public List<Visita_Viv> visitasvivienda;
	public List<Incentivos> incentivos;
	public List<Seccion03> seccion03;
	public List<Seccion04_05> seccion04_05;
	public List<Auditoria> seccion04_05_det;
	public List<CSVISITA> visitassalud;
	public List<Salud> listasalud;
	public List<CAP04_07> listadoCap04_07;
	public List<CSSECCION_08> listadoninios;
	public List<Individual> listadomef;
	public List<CARATULA_INDIVIDUAL> listadoCaratulamef;
	
	public List<CICALENDARIO_COL01_03> listaCalen01_03;
	public List<CICALENDARIO_COL04> lista_Calen04;
	public List<CICALENDARIO_TRAMO_COL01_03> listaclaenTramo01_03;
	public List<CICALENDARIO_TRAMO_COL4> listaCalenTramo04;
	public List<CISECCION_10_01> listaCI_10_01;
	public List<CISECCION_10_02> listaCI_10_02;
	public List<CISECCION_10_03> listaCI_10_03;
	public List<CISECCION_10_04> listaCI_10_04;
	public List<CISECCION_09> listaCI_09;
	public List<CISECCION_08> listaCI_08;
	public List<CISECCION_05> listaCI_05;
	public List<CISECCION_05_07> listaCI_05_07;
	public List<CISECCION_04B2> listaCI_04B2;
//	public List<CISECCION_04DIT> listaCI_04DIT;
	public List<CISECCION_04DIT_02> listaCI_04DIT_02;
	public List<CISECCION_04B_TARJETA> listaCI_04b_TARJETA;
	public List<CISECCION_04B> listaCI_04B;
	public List<CISECCION_04A> listaCI_04A;
	public List<DISCAPACIDAD> listaDiscapacidad;
	public List<CISECCION_02T> listaCI_02T;
	public List<CISECCION_02> listaCI_02;
	public List<CISECCION_01_03> listaCI_01_03;
	
	
	private SeccionCapitulo[] seccionesCargado,seccionesGrabadoCaratula;
	public List<Hogar> detalles;
	public GridComponent2 grilla;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 

	public MarcoModificarViviendaDialog() {
		super();
	}
	public static MarcoModificarViviendaDialog newInstance(FragmentForm pagina, Marco detalle) {
		caller = (MarcoFragment) pagina;
		MarcoModificarViviendaDialog f = new MarcoModificarViviendaDialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	  @Override 
	  public void onCreate(Bundle savedInstanceState) { 
			super.onCreate(savedInstanceState); 
			bean = (Marco) getArguments().getSerializable("detalle");
			seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID","N_HOGAR","GPS_LAT", "GPS_LONG", "GPS_ALT","RESVIV","QHVRESUL_O","CONGLOMERADO","NSELV","TIPVIA", "TIPVIA_O", "NOMVIA","TIPSEL","NOM_DPTO","PROVINCIA","DISTRITO","CCCPP","NOM_CCPP","ZON_NUM","MZA","AER_INI", "AER_FIN","VIVIENDA","NOMVIA", "PTANUM", "BLOCK", "INTERIOR","PISO", "MZ", "LOTE", "KM")};
			seccionesGrabadoCaratula = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "N_HOGAR","GPS_LAT", "GPS_LONG", "GPS_ALT","RESVIV","QHVRESUL_O","CONGLOMERADO","NSELV","TIPVIA", "TIPVIA_O", "NOMVIA","TIPSEL","NOM_DPTO","PROVINCIA","DISTRITO","CCCPP","NOM_CCPP","ZON_NUM","MZA","AER_INI", "AER_FIN","VIVIENDA","NOMVIA", "PTANUM", "BLOCK", "INTERIOR","PISO", "MZ", "LOTE", "KM") };
		} 
	@Override
	protected View createUI() {
		buildFields(); 
		/*Aca creamos las preguntas*/ 
		q0 = createQuestionSection(grilla.component()); 
		LinearLayout botones = createButtonSection(btnAceptar,btnCancelar);
		///////////////////////////// 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(botones);
		return contenedor;
	}
	
	 @Override 
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
			rootView = createUI(); 
			getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
			initObjectsWithoutXML(this, rootView);
			cargarDatos();
			enlazarCajas(); 
			listening(); 
			return rootView; 
		} 

	@Override
	protected void buildFields(){
//		// TODO Auto-generated method stub
		txtNSELV = new  IntegerField(this.getActivity()).maxLength(3).size(altoComponente, 250).readOnly().centrar();
		spnViviendaCambio = new SpinnerField(getActivity()).size(altoComponente+15, 350);
		btnAceptar = new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200, 55).text(R.string.btnAceptar);
		btnCancelar= new ButtonComponent(getActivity(),App.ESTILO_BOTON).size(200, 55).text(R.string.btnCancelar);
		lblvivienda = new LabelComponent(getActivity()).text(R.string.nrodevivienda).size(MATCH_PARENT, MATCH_PARENT).textSize(18).centrar().negrita();
		lblcambiodevivienda = new LabelComponent(getActivity()).text(R.string.cambiodenrovivienda).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita().centrar();
		grilla = new GridComponent2(getActivity(), 2);
		grilla.addComponent(lblvivienda);
		grilla.addComponent(lblcambiodevivienda);
		grilla.addComponent(txtNSELV);
		grilla.addComponent(spnViviendaCambio);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MarcoModificarViviendaDialog.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!validar()){
					if (error) { 
						if (!mensaje.equals("")) ToastMessage.msgBox(getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
						if (view != null) view.requestFocus(); 
					} 
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), MarcoModificarViviendaDialog.this, TIPO_DIALOGO.YES_NO, getResources()
	                      .getString(R.string.app_name),"EST� SEGURO QUE DESEA CAMBIAR LA INFORMACI�N DE LA VIVIENDA N�: "+bean.nselv+" A LA VIVIENDA N�: "+spnViviendaCambio.getSelectedItem());
					dialog.showDialog();
			}
		});	
	}
	public boolean grabar() {
		boolean flag = false;
        Integer idnuevo =Integer.parseInt(spnViviendaCambio.getSelectedItemKey().toString());
		SQLiteDatabase dbTX = getCuestionarioService().startTX();
		try {
			flag = getCuestionarioService().saveOrUpdate(llenarCaratula(), dbTX, seccionesGrabadoCaratula);
			for(Hogar h: detalles){
				h.id = idnuevo;
				flag = getCuestionarioService().saveOrUpdate(h,dbTX);
			}
			
			visitasvivienda = getVisitaService().getVisitas_viv(bean.id);
			for(Visita_Viv viv: visitasvivienda){
				viv.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(viv, dbTX);
			}		
				
			visitashogar = getVisitaService().getVisitas(bean.id);
			for(Visita v: visitashogar){
				v.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(v, dbTX);
			}
			
			incentivos = getVisitaService().getIncentivos(bean.id);
			for(Incentivos v: incentivos){
				v.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(v, dbTX);
			}
						
			personas = getPersonasService().getPersonasbyId(bean.id);
			for(Seccion01 persona: personas){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona,dbTX);
			}
			fallecidas= getCuestionarioService().getAllMortalidad(bean.id);
			for(MORTALIDAD persona: fallecidas){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona,dbTX);
			}
			
			migracion= getCuestionarioService().getAllMigracion(bean.id);
			for(MIGRACION persona: migracion){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona,dbTX);
			}
			imigracion= getCuestionarioService().getAllImigracion(bean.id);
			for(IMIGRACION persona: imigracion){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona,dbTX);
			}
			
		
			
			seccion03 = getSeccion03Service().getPersonas(bean.id);
			for(Seccion03 personas03:seccion03){
				personas03.id=idnuevo;
				flag = getCuestionarioService().saveOrUpdate(personas03, dbTX);
			}
			seccion04_05 = getSeccion04_05Service().getPersonas(bean.id);
			for(Seccion04_05 persona: seccion04_05){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona, dbTX);
			}
			seccion04_05_det = getSeccion04_05Service().getPersonasSeccion04_05_det(bean.id);
			for(Auditoria persona: seccion04_05_det){
				persona.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(persona, dbTX);
			}
			visitassalud = getVisitaService().getVisitassalud(bean.id);
			for(CSVISITA vsalud: visitassalud){
				vsalud.id=idnuevo;
				flag= getCuestionarioService().saveOrUpdate(vsalud, dbTX);
			}
			listasalud = getCuestionarioService().getCAP01_03List(bean.id);
			for(Salud salud:listasalud){
				salud.id=idnuevo;
				flag= getCuestionarioService().saveOrUpdate(salud, dbTX);
			}
			listadoCap04_07= getCuestionarioService().getCAP04_07List(bean.id);
			for(CAP04_07 cap04_07:listadoCap04_07){
				cap04_07.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(cap04_07, dbTX);
			}
			listadoninios= getCuestionarioService().getCAP08_09List(bean.id);
			for(CSSECCION_08 ninio: listadoninios){
				ninio.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(ninio, dbTX);
			}
			listadoCaratulamef=getCuestionarioService().getListadoCarMefById(bean.id);
			for(CARATULA_INDIVIDUAL mef: listadoCaratulamef){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			//
			listaCalen01_03 =getCuestionarioService().getListadoCalen01_03ById(bean.id);
			for(CICALENDARIO_COL01_03 mef: listaCalen01_03){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			lista_Calen04  =getCuestionarioService().getListadoCalen04ById(bean.id);
			for(CICALENDARIO_COL04 mef: lista_Calen04){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaclaenTramo01_03  =getCuestionarioService().getListadoCalenTramoById(bean.id);
			for(CICALENDARIO_TRAMO_COL01_03 mef: listaclaenTramo01_03){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCalenTramo04  =getCuestionarioService().getListadoCalenTramo4ById(bean.id);
			for(CICALENDARIO_TRAMO_COL4 mef: listaCalenTramo04){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_10_01  =getCuestionarioService().getListadoCI_10_01ById(bean.id);
			for(CISECCION_10_01 mef: listaCI_10_01){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_10_02  =getCuestionarioService().getListadoCI_10_02ById(bean.id);
			for(CISECCION_10_02 mef: listaCI_10_02){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_10_03  =getCuestionarioService().getListadoCI_10_03ById(bean.id);
			for(CISECCION_10_03 mef: listaCI_10_03){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_10_04  =getCuestionarioService().getListadoCI_10_04ById(bean.id);
			for(CISECCION_10_04 mef: listaCI_10_04){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_09  =getCuestionarioService().getListadoCI_09ById(bean.id);
			for(CISECCION_09 mef: listaCI_09){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_08  =getCuestionarioService().getListadoCI_08ById(bean.id);
			for(CISECCION_08 mef: listaCI_08){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_05_07  =getCuestionarioService().getListadoCI_05_07ById(bean.id);
			for(CISECCION_05_07 mef: listaCI_05_07){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_05  =getCuestionarioService().getListadoCI_05ById(bean.id);
			for(CISECCION_05 mef: listaCI_05){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_04B2  =getCuestionarioService().getListadoCI_04B2ById(bean.id);
			for(CISECCION_04B2 mef: listaCI_04B2){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
//			listaCI_04DIT  =getCuestionarioService().getListadoCI_DitById(bean.id);
//			for(CISECCION_04DIT mef: listaCI_04DIT){
//				mef.id=idnuevo;
//				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
//			}
			listaCI_04DIT_02  =getCuestionarioService().getListadoCI_Dit02ById(bean.id);
			for(CISECCION_04DIT_02 mef: listaCI_04DIT_02){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_04b_TARJETA  =getCuestionarioService().getListadoCI_04B_TarjetaById(bean.id);
			for(CISECCION_04B_TARJETA mef: listaCI_04b_TARJETA){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_04B  =getCuestionarioService().getListadoCI_04BById(bean.id);
			for(CISECCION_04B mef: listaCI_04B){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_04A  =getCuestionarioService().getListadoCI_04AById(bean.id);
			for(CISECCION_04A mef: listaCI_04A){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaDiscapacidad = getCuestionarioService().getListado_Discapacidad(bean.id);
			for(DISCAPACIDAD mef: listaDiscapacidad){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_02T  =getCuestionarioService().getListadoCI_02TById(bean.id);
			for(CISECCION_02T mef: listaCI_02T){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_02  =getCuestionarioService().getListadoCI_02ById(bean.id);
			for(CISECCION_02 mef: listaCI_02){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}
			listaCI_01_03  =getCuestionarioService().getListadoCI_01_03ById(bean.id);
			for(CISECCION_01_03 mef: listaCI_01_03){
				mef.id=idnuevo;
				flag=getCuestionarioService().saveOrUpdate(mef, dbTX);
			}

			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_VISITA_VIV, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CARATULA, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_HOGAR, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_INCENTIVOS, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_VISITA, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION01, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_MORTALIDAD, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_MIGRACION, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_IMIGRACION, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION03, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION04_05_DET, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION04_05, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CSVISITA, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SALUD, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CAP04_07, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CSSECCION_08, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_Individual, bean.id, dbTX);
			
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO04, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_01, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_02, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_03, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_04, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_09, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_08, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_05, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_05_07, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B2, bean.id, dbTX);
//			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_DIT, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_DIT_02, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04A, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_DISCAPACIDAD, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_02T, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_02, bean.id, dbTX);
			flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_01_03, bean.id, dbTX);
			
		if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos del hogar.");
			}
			getCuestionarioService().commitTX(dbTX);
			
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(),e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getCuestionarioService().endTX(dbTX);
		}
		return true;
	}
	public boolean validar(){
		if(spnViviendaCambio.getSelectedItemKey()==null){
				mensaje = "Seleccione una vivienda"; 
				view = spnViviendaCambio; 
				error = true; 
				return false; 
		}
		return true;
	}
	public Caratula llenarCaratula(){
		 Integer idnuevo =Integer.parseInt(spnViviendaCambio.getSelectedItemKey().toString());
		 Marco nuevo = (Marco)spnViviendaCambio.getValue();
		 Caratula caratulaanterior;
		 caratulaanterior = getCuestionarioService().getCaratula(bean.id, seccionesCargado);
		caratula = getCuestionarioService().getCaratula(idnuevo, seccionesCargado);
		if (caratula == null) {
			caratula = new Caratula();
			caratula.id = idnuevo;
			caratula.conglomerado =nuevo.conglomerado;
			caratula.nselv = nuevo.nselv;
			caratula.tipvia = nuevo.catvia;
			caratula.nomvia = nuevo.via;
			caratula.nom_dpto=nuevo.departamento;
			caratula.distrito=nuevo.distrito;
			caratula.provincia=nuevo.provincia;
			caratula.nom_ccpp = nuevo.nomccpp;
			caratula.zon_num = nuevo.zona;
			caratula.mza = nuevo.manzana;
			caratula.aer_ini = nuevo.aer_ini;
			caratula.aer_fin = nuevo.aer_fin;
			caratula.vivienda = nuevo.nroorden;
			caratula.ptanum = nuevo.puerta;
			caratula.mz = nuevo.mz;
			caratula.lote = nuevo.lote;
			caratula.piso = nuevo.piso;
			caratula.block = nuevo.block;
			caratula.interior = nuevo.interior;
			caratula.tipsel = nuevo.tselv;
			caratula.p19 = nuevo.p19;
			caratula.km = nuevo.km;
			caratula.resviv = caratulaanterior.resviv;
			detalles = getCuestionarioService().getHogaresById(bean.id);
			caratula.n_hogar=detalles.size();
			caratula.gps_alt = caratulaanterior.gps_alt;
			caratula.gps_lat = caratulaanterior.gps_lat;
			caratula.gps_long = caratulaanterior.gps_long;
		} 
//		Log.e("","CARATULA DPTO: "+nuevo.departamento);
//		Log.e("","CARATULA DSTRO: "+nuevo.distrito);
//		Log.e("","CARATULA PROVINCIA: "+nuevo.provincia);
		return caratula;
	}
	public void cargarDatos(){
		if(bean!=null){
			entityToUI(bean);
			MyUtil.llenarViviendas(getActivity(), getCuestionarioService(), spnViviendaCambio, bean.id, bean.conglomerado);
		}
		
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	public HogarService getHogarService() {
		if (hogarservice == null) {
			hogarservice = HogarService
					.getInstance(getActivity());
		}
		return hogarservice;
	}
	public Seccion01Service getPersonasService() {
		if (personasservice == null) {
			personasservice = Seccion01Service
					.getInstance(getActivity());
		}
		return personasservice;
	}
	public VisitaService getVisitaService() {
		if (visitas == null) {
			visitas = VisitaService
					.getInstance(getActivity());
		}
		return visitas;
	}
	public Seccion03Service getSeccion03Service() {
		if (seccion03service == null) {
			seccion03service = Seccion03Service
					.getInstance(getActivity());
		}
		return seccion03service;
	}
	public Seccion04_05Service getSeccion04_05Service() {
		if (seccion04_5service == null) {
			seccion04_5service = Seccion04_05Service
					.getInstance(getActivity());
		}
		return seccion04_5service;
	}
	
	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(getActivity());
		}
		return service;
	}
	public void DesactivarBototones(){
		btnAceptar.setEnabled(true);
		btnCancelar.setEnabled(true);
	}
	public void ActivarBotones(){
		btnAceptar.setEnabled(false);
		btnCancelar.setEnabled(false);
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		boolean flag=false;
		flag = grabar();
	if (!flag) {
		return;
	}
	Integer periodo=Integer.parseInt(caller.spnPERIODO.getSelectedItemKey().toString());
	caller.marcos= getService().getMarco(periodo,bean.conglomerado);
	caller.recargarLista();
	MarcoModificarViviendaDialog.this.dismiss();
	}
}
