package gob.inei.endes2024.fragment.seccion01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Beneficiario;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class Seccion01Fragment_004 extends FragmentForm {
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH22; 
	@FieldAnnotation(orderIndex=2) 
	public SpinnerField spnQH23; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH24; 
	@FieldAnnotation(orderIndex=4) 
	public SpinnerField spnQH25;
	
	@FieldAnnotation(orderIndex=5)  
	public TextField txtQH25A;	
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH25B;
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQH25CM;
	@FieldAnnotation(orderIndex=8)
	public IntegerField txtQH25CA;
	
	@FieldAnnotation(orderIndex=9)
	public TextAreaField txtQHOBSPERSONA;
	
	public CheckBoxField chbP25A;
	public ButtonComponent btnNosabemes,btnNosabeanio;   
	
	Seccion01 bean; 
	private static Seccion01Fragment_001 caller;
	private SeccionCapitulo[] seccionesGrabado;
	private SeccionCapitulo[] seccionesCargado; 
	private CuestionarioService cuestionarioService;
	private Seccion01Service serviceSeccion01;
	private CuestionarioService hogarService;
	public List<Seccion01> listaMadres;
	public GridComponent2 gridPregunta25a,gridPregunta25cm,gridPregunta25ca;
	public LabelComponent lblespacio1,lblespacio2,lblespacio3,lblmadreviva,lblmadreresideaqui,lblpadrevivo,lblpadreresideaqui,lblTitulo1,lblpregunta25a,lblpregunta25b,lblpregunta25a_esp, lblpregunta25cp1, lblpregunta25cp2, lblpregunta25cm, lblpregunta25ca;
	
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;


	public Seccion01Fragment_004() {} 
	public Seccion01Fragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH22","QH23","QH24","QH25","QH25A","QH25B","QH25CM","QH25CA","ID","HOGAR_ID","PERSONA_ID","QH07","QH02_1","QH03","QH04","QH13FECH_REF","QHINFO","QHOBSPERSONA")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH22","QH23","QH24","QH25","QH25A","QH25B","QH25CM","QH25CA","QH_OBSERVACION","QHOBSPERSONA")}; 
		return rootView; 	
	} 
	
  
	@Override 
	protected void buildFields() { 
		lblTitulo1 = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_tituloNroOrden).textSize(21).centrar().negrita();
		
		rgQH22=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh22_1,R.string.seccion01qh22_2,R.string.seccion01qh22_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP22ChangeValue");
		spnQH23=new SpinnerField(getActivity()).size(altoComponente+15, 400);
				
		rgQH24=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh24_1,R.string.seccion01qh24_2,R.string.seccion01qh24_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP24ChangeValue");
		spnQH25=new SpinnerField(getActivity()).size(altoComponente+15, 400);
	
		lblmadreviva = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh22);
		Spanned texto23 = Html.fromHtml("23. �Reside aqu� la madre natural de (NOMBRE)?");
		lblmadreresideaqui = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT);
		lblmadreresideaqui.setText(texto23);
		lblpadrevivo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh24);
		Spanned texto25 = Html.fromHtml("25. �Reside aqu� el padre natural de (NOMBRE)?");
		lblpadreresideaqui = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT);
		lblpadreresideaqui.setText(texto25);
	
		lblpregunta25a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.seccion01qh25a);
		lblpregunta25a_esp = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.seccion01qh25a_esp);
		txtQH25A = new TextField(this.getActivity()).size(altoComponente,500).maxLength(200).alinearIzquierda().callback("onchbQH25AChangeValue");
		chbP25A=new CheckBoxField(this.getActivity(), R.string.seccion01qh25ap, "1:0").size(60, 200);
		chbP25A.setOnCheckedChangeListener(new OnCheckedChangeListener(){				
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked){
		    		Util.cleanAndLockView(getActivity(),txtQH25A);
		    		onchbQH25AChangeValue();		    		
		  		}
				else {
					Util.lockView(getActivity(), false,txtQH25A);
					txtQH25A.setVisibility(View.VISIBLE);
					onchbQH25AChangeValue();					
				}
			}
		});
		
		gridPregunta25a = new GridComponent2(this.getActivity(),App.ESTILO,1,0);		
		gridPregunta25a.addComponent(chbP25A);
		gridPregunta25a.addComponent(txtQH25A);
			  
		
		lblpregunta25b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.seccion01qh25b);
		rgQH25B=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh25b_1,R.string.seccion01qh25b_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("ValidarPreguntaP25b_text");			
		
			
		lblpregunta25cp1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.seccion01qh25c_p1);
		lblpregunta25cp2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.seccion01qh25c_p2);
		lblpregunta25cm = new LabelComponent(this.getActivity()).size(60, 90).textSize(19).text(R.string.seccion01qh25cm);
		lblpregunta25ca = new LabelComponent(this.getActivity()).size(60, 90).textSize(19).text(R.string.seccion01qh25ca);		
		btnNosabemes = new ButtonComponent(getParent(),App.ESTILO_BOTON).text(R.string.seccion01qh25cns1).size(180, 20);
		btnNosabeanio = new ButtonComponent(getParent(),App.ESTILO_BOTON).text(R.string.seccion01qh25cns2).size(180, 20);
		txtQH25CM=new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(2);
		txtQH25CA=new IntegerField(this.getActivity()).size(altoComponente, 90).maxLength(4);
		lblespacio1 = new LabelComponent(this.getActivity()).size(10,20);
		lblespacio2 = new LabelComponent(this.getActivity()).size(10,20);
		lblespacio3 = new LabelComponent(this.getActivity()).size(10,20);
		
		gridPregunta25cm= new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		gridPregunta25cm.addComponent(lblpregunta25cm);
		gridPregunta25cm.addComponent(txtQH25CM);
		gridPregunta25cm.addComponent(lblespacio1);
		gridPregunta25cm.addComponent(btnNosabemes);
		
		gridPregunta25ca= new GridComponent2(this.getActivity(),App.ESTILO,4,0);
		gridPregunta25ca.addComponent(lblpregunta25ca);
		gridPregunta25ca.addComponent(txtQH25CA);
		gridPregunta25ca.addComponent(lblespacio2);
		gridPregunta25ca.addComponent(btnNosabeanio);
			  
		btnNosabemes.setOnClickListener(new View.OnClickListener() {
			  @Override
			  public void onClick(View v) {
				  txtQH25CM.setText("98");
			  }
		});
		btnNosabeanio.setOnClickListener(new View.OnClickListener() {
			  @Override
			  public void onClick(View v) {
				  txtQH25CA.setText("9998");
			  }
		});
		
		txtQHOBSPERSONA = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
  
    } 
	
    @Override 
    protected View createUI() { 
    	buildFields(); 	
    	q0 = createQuestionSection(lblTitulo1);
    	q1 = createQuestionSection(lblmadreviva,rgQH22); 
    	q2 = createQuestionSection(lblmadreresideaqui,spnQH23); 
    	q3 = createQuestionSection(lblpadrevivo,rgQH24); 
    	q4 = createQuestionSection(lblpadreresideaqui,spnQH25);
    	
    	q5 = createQuestionSection(lblpregunta25a,lblpregunta25a_esp,gridPregunta25a.component());
    	q6 = createQuestionSection(lblpregunta25b,rgQH25B);
    	q7 = createQuestionSection(lblpregunta25cp1, lblpregunta25cp2,gridPregunta25cm.component(),lblespacio3,gridPregunta25ca.component());
    	
    	q8 = createQuestionSection(R.string.seccion01qh_obs_0,txtQHOBSPERSONA);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		form.addView(q7);
		form.addView(q8);
		
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {	
    	uiToEntity(bean);
    	
    	if (chbP25A.isChecked()){
			bean.qh25a="PERUANA/O";
    	}
    	if(bean.qh25cm!=null){			
			bean.qh25cm=txtQH25CM.getText().toString().length()>1?txtQH25CM.getText().toString():"0"+txtQH25CM.getText().toString();
		}
    	
    	if (!validar()) {
    		if (error) {
    			if (!mensaje.equals(""))
    				ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,	ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
	
    	boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return true; 
    } 
    
    private boolean validar() { 
    	String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//    	Integer Anio_Minimo=0;
//    	if(bean.qh07!=null){
//    		Anio_Minimo=App.ANIOPORDEFECTOSUPERIOR-bean.qh07;	
//    	}    	
    	Calendar fecha_ref =null;   
    	Integer Mes_Ref =0;   
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date2 = null;
		if(bean.qh13fech_ref!=null){
			try {
				date2 = df.parse(bean.qh13fech_ref);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_ref = cal;
			Mes_Ref=fecha_ref.get(Calendar.MONTH)+1;
		}
		Integer edad_calculada=0;
		if(bean.qh25ca!=null && bean.qh25cm!=null && fecha_ref!=null){
			Calendar fecha_estimada= new GregorianCalendar(Integer.parseInt(bean.qh25ca), Integer.parseInt(bean.qh25cm)-1,1);
			edad_calculada=MyUtil.CalcularEdadByFechaNacimiento(fecha_estimada, fecha_ref);	
		}
		 
		
		
		
    	
    	if (Util.esMenor(bean.qh07,15)) {
    		if (Util.esVacio(bean.qh22)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.22"); 
    			view = rgQH22; 
    			error = true; 
    			return false; 
    		} 
    		if (Util.esDiferente(bean.qh22,2,3)) {
    			if (Util.esVacio(bean.qh23)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta P.23"); 
    				view = spnQH23; 
    				error = true; 
    				return false; 
    			} 
    		}
    		if (Util.esVacio(bean.qh24)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.24"); 
    			view = rgQH24; 
    			error = true; 
    			return false; 
    		} 
    		if (Util.esDiferente(bean.qh24, 2,3)) {
    			if (Util.esVacio(bean.qh25)) { 
    				mensaje = preguntaVacia.replace("$", "La pregunta P.25"); 
    				view = spnQH25; 
    				error = true; 
    				return false; 
    			}
    		}
    	}
    	
    	if (!chbP25A.isChecked() && Util.esVacio(bean.qh25a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.25A"); 
			view = txtQH25A; 
			error = true; 
			return false; 
		}
    	
    	if(!chbP25A.isChecked()){
    		if(!Util.esDiferente(bean.qh04,2)){
    			if (Util.esVacio(bean.qh25b)) { 
        			mensaje = preguntaVacia.replace("$", "La pregunta P.25B"); 
        			view = rgQH25B; 
        			error = true; 
        			return false; 
        		}	
    		}
    		
        	if (Util.esVacio(bean.qh25cm)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.25CM"); 
    			view = txtQH25CM; 
    			error = true; 
    			return false; 
    		}
        	if(bean.qh25cm!=null && Util.esDiferente(Integer.parseInt(bean.qh25cm), 98,98)){
        		if ((Util.esMenor(Integer.parseInt(bean.qh25cm),1) ||  Util.esMayor(Integer.parseInt(bean.qh25cm),12))) {
    	    		mensaje ="Mes fuera de rango";
    				view = txtQH25CM;
    				error=true;
    				return false;
    	    	}
    	    	if (Integer.parseInt(bean.qh25ca)==App.ANIOPORDEFECTOSUPERIOR && Util.esMayor(Integer.parseInt(bean.qh25cm),Mes_Ref)) {
    	    		mensaje ="Mes fuera de rango.";
    	    		view = txtQH25CM;
    	    		error=true;
    	    		return false;
    	    	}
        	}
       
        	
        	
        	if (Util.esVacio(bean.qh25ca)) { 
    			mensaje = preguntaVacia.replace("$", "La pregunta P.25CA"); 
    			view = txtQH25CA; 
    			error = true; 
    			return false; 
    		}   
        	
        	if(Util.esDiferente(Integer.parseInt(bean.qh25ca), 9998,9998)){
        		if (Util.esMayor(Integer.parseInt(bean.qh25ca),App.ANIOPORDEFECTOSUPERIOR)) {
    	    		mensaje ="A�o fuera de rango.";
    	    		view = txtQH25CA;
    	    		error=true;
    	    		return false;
    	    	}
        		
//        		Log.e("","qh13fech_ref: "+ bean.qh13fech_ref);
//            	Log.e("","edad_calculada: "+ edad_calculada);
            	if (edad_calculada>bean.qh07 && Util.esDiferente(Integer.parseInt(bean.qh25ca), 9998,9998) && Util.esDiferente(Integer.parseInt(bean.qh25cm), 98,98)) {
    	    		mensaje ="Fecha fuera de rango.";
    	    		view = txtQH25CM;
    	    		error=true;
    	    		return false;
    	    	}
        	}
    	}
    	
    	return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	
    	if (App.getInstance().getEditarPersonaSeccion01()!=null){
    		bean = getCuestionarioService().getSeccion01(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getEditarPersonaSeccion01().persona_id,seccionesCargado);
    	
    		if (!Util.esDiferente(bean.qh03,App.HIJO_A)) {
    			if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.MUJER)) {
    				Log.e("","1111");
    				MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,App.JEFE,bean.qh03,bean.qh07);
    				MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
    			}
    			else{
    				Log.e("","2222");
    				MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,App.JEFE,bean.qh03,bean.qh07);
    				MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
    			}
    		}
    		else{
    			MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
    			MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
    		}
    	
    		if (bean == null) {
    			bean = new Seccion01();
    			bean.id = App.getInstance().getHogar().id;
    			bean.hogar_id =App.getInstance().getHogar().hogar_id;
    			bean.persona_id = caller.detalles.size()+1;
    			bean.qh01 = caller.detalles.size()+1;
    		}
    		entityToUI(bean);
    		    		
    		if(bean.qh25cm!=null){  
    			bean.qh25cm= bean.qh25cm.toString().length()>1?bean.qh25cm:"0"+bean.qh25cm;
    			txtQH25CM.setText(bean.qh25cm);
    		}
    		
    		if (bean.qh25a!=null && bean.qh25a.equals("PERUANA/O")){    			
    			chbP25A.setChecked(true);
        	}
    		inicio();
    		
    	}   
    }
    
    public void saltoqh07txt(Integer edad) {
    	if (Util.esMayor(edad,14)) {
    		Util.cleanAndLockView(getActivity(),rgQH22,spnQH23,rgQH24,spnQH25);			
		}
    	else{
    		Util.lockView(getActivity(), false,rgQH22,spnQH23,rgQH24,spnQH25);		    	
    		onP22ChangeValue();	
    		onP24ChangeValue();				
    	
		}	
    }

    public void existeMadreJefa(Integer p3) {	
    	if (Util.esMayor(bean.qh07,14)) {
    		if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.MUJER) && !Util.esDiferente(p3,App.HIJO_A) ) {			
    			rgQH22.lockButtons(true,1,2);
    		}
    		else{			
    			rgQH22.lockButtons(false,1,2);	
    		}
    		if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.HOMBRE) && !Util.esDiferente(p3,App.HIJO_A) ) {			
    			rgQH24.lockButtons(true,1,2);
    		}
    		else{			
    			rgQH24.lockButtons(false,1,2);		
    		}
    	}
    }
    
    
    private void inicio() { 
    	if (Util.esMayor(bean.qh07,14)) {
    		Util.cleanAndLockView(getActivity(),rgQH22,spnQH23,rgQH24,spnQH25);	
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		
		}
    	else{
    		Util.lockView(getActivity(), false,rgQH22,spnQH23,rgQH24,spnQH25);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		
    		existeMadreJefa(bean.qh03);    		
    		saltoqh07txt(bean.qh07);    		 
    	} 
    	RenombrarLabels();
    	onchbQH25AChangeValue();
    	ValidarsiesSupervisora();
    	rgQH22.requestFocus();
    } 
    
    public void onchbQH25AChangeValue() {
    	if (chbP25A.isChecked()) {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQH25B,txtQH25CA, txtQH25CM);    
    		q6.setVisibility(View.GONE);    		 		
    		q7.setVisibility(View.GONE);    
		} 
    	else {   		    		
    		Util.lockView(getActivity(), false,txtQH25CA, txtQH25CM);
    		q7.setVisibility(View.VISIBLE);
    		ValidarPreguntaP25b();
		}    
    }
    
    public void ValidarPreguntaP25b() {
    	if(!Util.esDiferente(bean.qh04,2)){
			Util.lockView(getActivity(), false,rgQH25B);
			q6.setVisibility(View.VISIBLE);
			ValidarPreguntaP25b_text();
		}
		if(!Util.esDiferente(bean.qh04,1)){
			Util.cleanAndLockView(getActivity(),rgQH25B);    
    		q6.setVisibility(View.GONE);
    		ValidarPreguntaP25b_text();
		}	
	}
    
    public void ValidarPreguntaP25b_text() {
    	if(!Util.esDiferente(bean.qh04,2) && MyUtil.incluyeRango(2,2,rgQH25B.getTagSelected("").toString())){
			lblpregunta25cp1.setVisibility(View.GONE);
			lblpregunta25cp2.setVisibility(View.VISIBLE);			
		}
		else{
			lblpregunta25cp1.setVisibility(View.VISIBLE);
			lblpregunta25cp2.setVisibility(View.GONE);
		}    	
    	
    	if(!Util.esDiferente(bean.qh04,2) && !chbP25A.isChecked() && MyUtil.incluyeRango(1,2,rgQH25B.getTagSelected("").toString())){
    		txtQH25CM.requestFocus();
    	}
	}
    
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQH22.readOnly();
			rgQH24.readOnly();
			spnQH23.readOnly();
			spnQH25.readOnly();
			txtQH25A.readOnly();
			chbP25A.readOnly();
			rgQH25B.readOnly();
			txtQH25CA.readOnly();
			txtQH25CM.readOnly();			
			Util.cleanAndLockView(getActivity(), btnNosabeanio);
			Util.cleanAndLockView(getActivity(), btnNosabemes);
			txtQHOBSPERSONA.setEnabled(false);
		}
	}
    
    private void RenombrarLabels(){
    	String replace="(NOMBRE)";
		String Ud="Usted";
		bean.qhinfo=bean.qhinfo==null?0:bean.qhinfo;
		lblmadreviva.setText(lblmadreviva.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblmadreresideaqui.setText(lblmadreresideaqui.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		Spanned texto23 = Html.fromHtml(lblmadreresideaqui.getText()+" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblmadreresideaqui.setText(" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblmadreresideaqui.setText(texto23);
		lblpadrevivo.setText(lblpadrevivo.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblpadreresideaqui.setText(lblpadreresideaqui.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		Spanned texto25 = Html.fromHtml(lblpadreresideaqui.getText() +" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblpadreresideaqui.setText(texto25);
		
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D1",bean.persona_id.toString()));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D2",bean.qh07.toString()));
		
		lblpregunta25a.setText(getResources().getString(R.string.seccion01qh25a));
		lblpregunta25a.setText(lblpregunta25a.getText().toString().replace(replace,  bean.qhinfo==1?Ud:bean.qh02_1));
		
		lblpregunta25b.setText(getResources().getString(R.string.seccion01qh25b));
		lblpregunta25b.setText(lblpregunta25b.getText().toString().replace(replace,  bean.qhinfo==1?Ud:bean.qh02_1));
		
		lblpregunta25cp1.setText(getResources().getString(R.string.seccion01qh25c_p1));
		lblpregunta25cp1.setText(lblpregunta25cp1.getText().toString().replace(replace,  bean.qhinfo==1?Ud:bean.qh02_1));
		
		lblpregunta25cp2.setText(getResources().getString(R.string.seccion01qh25c_p2));
		lblpregunta25cp2.setText(lblpregunta25cp2.getText().toString().replace(replace,  bean.qhinfo==1?Ud:bean.qh02_1));
		
    }
    
	public void onP22ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQH22.getTagSelected("").toString())) {
			Log.e("aa","1");
			Util.lockView(getActivity(), false,spnQH23);
			q2.setVisibility(View.VISIBLE);
			spnQH23.requestFocus();
		}
		else{
			Log.e("aa","2");
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),spnQH23);
			q2.setVisibility(View.GONE);	
			rgQH24.requestFocus();
		}
	}
	
	public void onP24ChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQH24.getTagSelected("").toString())) {
			Log.e("aa","3");
			Util.lockView(getActivity(), false,spnQH25);
			q4.setVisibility(View.VISIBLE);
			spnQH25.requestFocus();
		}
		else{
			Log.e("aa","4");
			MyUtil.LiberarMemoria();
			Util.cleanAndLockView(getActivity(),spnQH25);
			q4.setVisibility(View.GONE);
			chbP25A.requestFocus();
		}
	}
	
    public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
	 	uiToEntity(bean);
	 	if(bean.qh25cm!=null){			
			bean.qh25cm=txtQH25CM.getText().toString().length()>1?txtQH25CM.getText().toString():"0"+txtQH25CM.getText().toString();
		}
	 	
      	boolean flag = true;
		try {
			flag = getCuestionarioService().saveOrUpdate(bean,seccionesGrabado);		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return App.NODEFINIDO;
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			return App.NODEFINIDO;
		} 
		return App.HOGAR;
	}
} 
