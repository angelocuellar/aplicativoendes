package gob.inei.endes2024.fragment.seccion01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion01.Dialog.Seccion01_001Dialog;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion01Fragment_001 extends FragmentForm implements Respondible {

	public List<Seccion01> detalles;
	Hogar bean;
	private CuestionarioService cuestionarioService;
	private HogarService hogarservice;
	private Seccion01Service Personaservice;
	private Seccion04_05Service seccion04service;
	private Seccion03Service seccion03service;
	public ButtonComponent btnAgregar,btnDiscapa;
	public TableComponent tcPersonas;
	private DialogComponent dialog;
	Seccion01ClickListener adapter;
	public LabelComponent lblTitulo;
	
	
	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQHBEBE;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQHNOFAM;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQHTEMPO;
	@FieldAnnotation(orderIndex = 4)
	public TextAreaField txtQH_OBS_SECCION01;
	
	
	public TextField txtCabecera; 
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoHogar;
	SeccionCapitulo[] seccionesGrabadoHogar;
	SeccionCapitulo[] seccionesGrabadoHogarInformante;
	SeccionCapitulo[] seccionesGrabadoHogarInicial,seccionesCargadoInformanteSalud,seccionesCargadoS,seccionesCargadoSaludS1_3,seccionesCargadoSaludS4_7;
	public Seccion01 informantedesalud;
	private VisitaService visitaService;
	public GridComponent2 botones; 
	private HogarService hogarService;
	private Seccion01Service personaService;
	public LabelComponent lblpreguntaInd,lblpregunta1,lblpregunta2,lblpregunta3,lblpreguntapersonas,lblespacio,lblespacio1;
	public Seccion01 mefdeviolenciafamiliar;
	public Seccion01Service seccion01;
	public List<Seccion01> ListadoMef;

	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;

	//
	public Seccion01Fragment_001() {
	}

	public Seccion01Fragment_001 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH01", "QH02_1", "QH02_2","QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD","QH7MM", "QHINFO", "estado") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QH01", "QH02_1", "QH02_2", "QH02_3") };
		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","PERSONA_INFORMANTE_ID") };
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","QHVIOLEN", "PERSONA_INFORMANTE_ID") };
		seccionesGrabadoHogarInformante = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "PERSONA_INFORMANTE_ID") };
		seccionesGrabadoHogarInicial =new  SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QHBEBE", "QHNOFAM", "QHTEMPO", "QH_OBS_SECCION01","PERSONA_INFORMANTE_ID")};
		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
		seccionesCargadoS = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS28", "QS29A", "QS29B")};
		seccionesCargadoSaludS1_3 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS23","QS100","QS200","QS301")};
		seccionesCargadoSaludS4_7 = new SeccionCapitulo[] {new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QS401","QS406","QS409","QS415","QS500","QS601A","QS603","QS700A","QS906")};
		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_titulo).textSize(21).centrar().negrita();
		btnAgregar = new ButtonComponent(getActivity(), App.ESTILO_BOTON).text(R.string.btnAgregar).size(200, 60);
		btnAgregar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				agregarPersona();
				Util.cleanAndLockView(getActivity(), btnAgregar);
			}
		});
		txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 1200).headerHeight(altoComponente + 10).dataColumHeight(60);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(600, 1200).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		
		tcPersonas.addHeader(R.string.seccion01_nro_orden, 0.25f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01nombres, 1f,TableComponent.ALIGN.LEFT);
		tcPersonas.addHeader(R.string.seccion01parentesco,0.59f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01viveaqui, 0.29f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01durmioaqui, 0.39f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01_sexo, 0.29f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01_edad, 0.29f,TableComponent.ALIGN.CENTER);tcPersonas.addHeader(R.string.seccion01fecha_nacimiento, 0.39f,TableComponent.ALIGN.CENTER);
		tcPersonas.setColorCondition("qhinfo", Util.getHMObject(String.valueOf(App.INFORMANTE), R.color.azulacero,String.valueOf(0), R.color.WhiteSmoke));
		
		rgQHBEBE = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qhbebe_1, R.string.seccion01qhbebe_2).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQHNOFAM = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qhnofam_1, R.string.seccion01qhnofam_2).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQHTEMPO = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qhtempo_1, R.string.seccion01qhtempo_2).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		txtQH_OBS_SECCION01 = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
//		txtQH_OBS_SECCION01.setCallback("OCULTARTECLA");
		
		lblespacio = new LabelComponent(this.getActivity()).size(altoComponente,30);
		lblespacio1 = new LabelComponent(this.getActivity()).size(10,MATCH_PARENT);
		lblpregunta1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhbebe).textSize(19);
		lblpreguntaInd = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhInd).textSize(19).negrita();
		lblpregunta1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhbebe).textSize(19);
		lblpregunta2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhnofam).textSize(19);
		lblpregunta3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).text(R.string.seccion01qhtempo).textSize(19);
		Spanned textopreguntapersonas=Html.fromHtml("2. Dígame por favor los <b>nombres y apellidos</b> de las personas que <b>habitualmente</b> viven en su hogar y de los visitantes que pasaron la noche anterior aquí, empezando por el Jefe del Hogar<br>");
		lblpreguntapersonas = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).alinearIzquierda().textSize(19);
		lblpreguntapersonas.setText(textopreguntapersonas);
		btnDiscapa = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnIrDiscapa).size(200,55);
		
		botones = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		botones.addComponent(btnAgregar);		
		botones.addComponent(lblespacio);
		botones.addComponent(btnDiscapa);
		
		
		btnDiscapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if (bean!=null) {
            		mefdeviolenciafamiliar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, SeleccionarMefViolenciaDomestica());
            		if(mefdeviolenciafamiliar!=null){
                	bean.qhviolen = mefdeviolenciafamiliar.persona_id;
            		}
            		else{
            			bean.qhviolen=0;	
            		}
				}
            	boolean flag=grabar2(); 
            	if (!flag) {
					return;
				}
            	parent.nextFragment(CuestionarioFragmentActivity.MORTALIDAD);    
            	App.getInstance().setEditarPersonaSeccion01(null);
            } });
		
	}

	public void agregarPersona() {
		Seccion01 bean = new Seccion01();
		bean.id = App.getInstance().getMarco().id;
		bean.hogar_id = App.getInstance().getHogar().hogar_id;
		bean.persona_id = detalles.size() + 1;
		bean.qh01 = detalles.size() + 1;
		abrirDetalle(bean);
	}

	public void abrirDetalle(Seccion01 tmp) {
		FragmentManager fm = Seccion01Fragment_001.this.getFragmentManager();
		Seccion01_001Dialog aperturaDialog = Seccion01_001Dialog.newInstance(this, tmp);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}

	
	public void EditarDetallePersona(Seccion01 tmp) {
//		MyUtil.LiberarMemoria();
		if(!Util.esDiferente(tmp.persona_id, App.JEFE)){
			if(!grabarAdicionales()){
				return;
			}
		}
		App.getInstance().setEditarPersonaSeccion01(null);
		App.getInstance().setEditarPersonaSeccion01(tmp);
		parent.nextFragment(CuestionarioFragmentActivity.CHSECCION1f_2);
	}

	public void EstablecerComoInformante(Seccion01 informante) {
		try {
//			App.getInstance().SetPersonaSeccion01(informante);
			boolean flag = getPersonaService().UpdateInformante(informante.id,informante.hogar_id);
			 
			SeccionCapitulo[] secciones = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1, -1, "QHINFO") };
			informante.qhinfo = App.INFORMANTE;
			if (!getPersonaService().saveOrUpdate(informante, secciones)) {
				ToastMessage.msgBox(this.getActivity(),"No se pudo seleccionar como informante",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return;
			}
			bean.persona_informante_id = informante.persona_id;
			if(!getService().saveOrUpdate(bean, null, seccionesGrabadoHogarInformante)){
				ToastMessage.msgBox(this.getActivity(),"No se pudo seleccionar como informante",ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
				return;
			}
			cargarTabla();
			
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), "ERROR: " + e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}

	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(txtCabecera,lblTitulo);
		LinearLayout q1 = createQuestionSection(R.string.seccion01msg,Gravity.CENTER);
		LinearLayout q2 = createQuestionSection(lblpreguntapersonas,botones.component(), lblespacio1,tcPersonas.getTableView());
		LinearLayout q3 = createQuestionSection(lblpreguntaInd,lblpregunta1,rgQHBEBE);
		LinearLayout q4 = createQuestionSection(lblpregunta2,rgQHNOFAM);
		LinearLayout q5 = createQuestionSection(lblpregunta3,rgQHTEMPO);
		LinearLayout q6 = createQuestionSection(R.string.seccion01qh_obs_1,txtQH_OBS_SECCION01);
		
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6);
		return contenedor;
	}
	
	public boolean grabar2() {
		
		uiToEntity(bean);
	
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = false;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(bean, dbTX, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		/***NO BORRAR ESTA SECCION ES PARA LA NAVEGACION***/
		App.getInstance().getHogar().qhbebe=bean.qhbebe;
		return flag;
	}
	
	private Seccion01Service getServiceSeccion01() {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(getActivity());
        }
        return seccion01;
    }
	
	public  Integer[][] LlenarMatrizViolenciaFamiliar()
	{
		Integer[][] matriz= new Integer[10][10];
		matriz[0][0]=1; matriz[0][1]=2; matriz[0][2]=2; matriz[0][3]=4; matriz[0][4]=3; matriz[0][5]=6; matriz[0][6]=5; matriz[0][7]=4; 
		matriz[1][0]=1; matriz[1][1]=1; matriz[1][2]=3; matriz[1][3]=1; matriz[1][4]=4; matriz[1][5]=1; matriz[1][6]=6; matriz[1][7]=5; 
		matriz[2][0]=1; matriz[2][1]=2; matriz[2][2]=1; matriz[2][3]=2; matriz[2][4]=5; matriz[2][5]=2; matriz[2][6]=7; matriz[2][7]=6; 
		matriz[3][0]=1; matriz[3][1]=1; matriz[3][2]=2; matriz[3][3]=3; matriz[3][4]=1; matriz[3][5]=3; matriz[3][6]=1; matriz[3][7]=7; 
		matriz[4][0]=1; matriz[4][1]=2; matriz[4][2]=3; matriz[4][3]=4; matriz[4][4]=2; matriz[4][5]=4; matriz[4][6]=2;	matriz[4][7]=8;
		matriz[5][0]=1; matriz[5][1]=1; matriz[5][2]=1; matriz[5][3]=1; matriz[5][4]=3; matriz[5][5]=5; matriz[5][6]=3; matriz[5][7]=1; 
		matriz[6][0]=1; matriz[6][1]=2; matriz[6][2]=2; matriz[6][3]=2; matriz[6][4]=4; matriz[6][5]=6; matriz[6][6]=4; matriz[6][7]=2; 
		matriz[7][0]=1; matriz[7][1]=1; matriz[7][2]=3; matriz[7][3]=3; matriz[7][4]=5; matriz[7][5]=1; matriz[7][6]=5; matriz[7][7]=3; 
		matriz[8][0]=1; matriz[8][1]=2; matriz[8][2]=1; matriz[8][3]=4; matriz[8][4]=1; matriz[8][5]=2; matriz[8][6]=6; matriz[8][7]=4; 
		matriz[9][0]=1;	matriz[9][1]=1;	matriz[9][2]=2;	matriz[9][3]=1;	matriz[9][4]=2;	matriz[9][5]=3;	matriz[9][6]=7;	matriz[9][7]=5;
		return matriz;
	}
	
	public Integer SeleccionarMefViolenciaDomestica()
	{
		Integer[][] MatrizViolencia = LlenarMatrizViolenciaFamiliar();
		Integer fila =Integer.parseInt(App.getInstance().getMarco().nselv.substring(2,3));
		
		Integer columna = getServiceSeccion01().cantidaddeMef(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id);
		
		
		Integer mefPosition =0; 
		if(columna!=0)
		{
			mefPosition=MatrizViolencia[fila][columna-1];
		}
		
		ListadoMef = getServiceSeccion01().getMef(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		Integer persona_id=-1;
		if(ListadoMef.size()>0)
		{
			persona_id=ListadoMef.get(mefPosition-1).persona_id;
		}
		return persona_id;
	}

	public boolean grabarAdicionales(){
		
		uiToEntity(bean);
		if(!validarcion()){
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = false;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(bean, dbTX, seccionesGrabadoHogarInicial);
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	
	@Override
	public boolean grabar() {
			
		return true;
	}

	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public boolean validarcion(){
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esVacio(bean.qhbebe)) {
			mensaje = "Diligencie preguntas adicionales";
			view = rgQHBEBE;
			error = true;
			return false;
		}
		if (Util.esVacio(bean.qhnofam)) {
			mensaje = "Diligencie preguntas adicionales";
			view = rgQHNOFAM;
			error = true;
			return false;
		}
		if (Util.esVacio(bean.qhtempo)) {
			mensaje = "Diligencie preguntas adicionales";
			view = rgQHTEMPO;
			error = true;
			return false;
		}
		if (Util.esVacio(bean.persona_informante_id)) {
			mensaje = "Debe seleccionar un informante";
			view = tcPersonas;
			error = true;
			return false;
		}
		return true;
	}
	

	private boolean validar() {
		if (!isInRange())
			return false;
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
//		if (Util.esVacio(bean.qhbebe)) {
//			mensaje = "Diligencie preguntas adicionales";
//			view = rgQHBEBE;
//			error = true;
//			return false;
//		}
//		if (Util.esVacio(bean.qhnofam)) {
//			mensaje = "Diligencie preguntas adicionales";
//			view = rgQHNOFAM;
//			error = true;
//			return false;
//		}
//		if (Util.esVacio(bean.qhtempo)) {
//			mensaje = "Diligencie preguntas adicionales";
//			view = rgQHTEMPO;
//			error = true;
//			return false;
//		}
		if(detalles.size()==0){
			mensaje = "Agregue miembros del hogar";
			view = btnAgregar;
			error = true;
			return false;
		}
		Integer sexoJefe=getPersonaService().getSexobypersonaid(bean.id, bean.hogar_id,1);
		Integer Esposaid=getPersonaService().getExisteEsposooEsposa(bean.id, bean.hogar_id);
		Integer sexoEsposa=getPersonaService().getSexoDelaEsposaOEsposo(bean.id, bean.hogar_id, Esposaid);
		if(Util.esDiferente(Esposaid,-1) && !Util.esDiferente(sexoJefe, sexoEsposa)){
			mensaje = "El jefe del Hogar no puede tener el mismo sexo que su conyugue";
			view = tcPersonas;
			error = true;
			return false;
		}
		
		if (!getPersonaService().TodoLosMiembrosDelhogarCompletados(bean.id,bean.hogar_id)) {
			mensaje = "Debe completar las preguntas para todo los miembros del hogar antes de pasar a Mortalidad";
			view = tcPersonas;
			error = true;
			return false;
		}
		
		
		Seccion01 informantehogar = getPersonaService().getPersonaInformante(bean.id, bean.hogar_id, 1);
		if(informantehogar!=null){
			bean.persona_informante_id=informantehogar.persona_id;
		}
		if (Util.esMenor(getPersonaService().getEdadByPersonaId(bean.id,	bean.hogar_id, bean.persona_informante_id), 11)) {
			mensaje = "verifique la edad del informante";
			view = tcPersonas;
			error = true;
			return false;
		}
		
		Integer Edadmaxima=getPersonaService().EdadMaximaDeunMiembrodelHogar(bean.id,bean.hogar_id);
		Integer edadInformante=getPersonaService().getEdadByPersonaId(bean.id,bean.hogar_id, bean.persona_informante_id);
		if(Util.esMenor(edadInformante, Edadmaxima) && Util.esMenor(edadInformante, 18)){
			if(Util.esDiferente(edadInformante, Edadmaxima)){
			mensaje = "Informante del Hogar no puese ser menor de Edad porque existe personas mayores";
			view = tcPersonas;
			error = true;
			return false;
			}
		}
	
		if(Util.esDiferente(informantehogar.qh04 , 1) || !Util.esDiferente(informantehogar.qh03, 12)){
			mensaje = "Informante del Hogar no puese ser visitante o empleada";
			view = tcPersonas;
			error = true;
			return false;
		}
		return true;
	}

	@Override
	public void cargarDatos() {
//		MyUtil.LiberarMemoria();
		if(App.getInstance().getHogar()!=null){
			bean = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);
			detalles = getCuestionarioService().GetSeccion01ListByMarcoyHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
			if (bean == null) {
				bean = new Hogar();
				bean.id = App.getInstance().getMarco().id;
				bean.hogar_id = App.getInstance().getHogar().hogar_id;
				detalles = new ArrayList<Seccion01>();
			}
			entityToUI(bean);			
			cargarTabla();
			inicio();
//			App.getInstance().SetPersonaSeccion01(null);
			App.getInstance().setEditarPersonaSeccion01(null);
		}

	
	}

	public void refrescarPersonas(Seccion01 personas) {
		if (detalles.contains(personas)) {
			cargarTabla();
			return;
		} else {
			detalles.add(personas);
			cargarTabla();
		}
	}

	public void cargarTabla() {
		detalles = getCuestionarioService().GetSeccion01ListByMarcoyHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		tcPersonas.setData(detalles, "qh01", "getNombre", "getParentesco","getViveAqui", "getDurmioAqui", "getSexo", "qh07", "getFecha");
		tcPersonas.setBorder("estado");
		registerForContextMenu(tcPersonas.getListView());
	}

	private class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			Seccion01 c = (Seccion01) detalles.get(arg2);
			Integer posicionpersona = -1;
			Integer estado = -1;
			for (int i = 0; i < detalles.size(); i++) {
				if (c.persona_id == detalles.get(i).persona_id) {
					posicionpersona = i;
					break;
				}
			}
			posicionpersona = posicionpersona == 0 ? 0 : posicionpersona - 1;
			for (int i = 0; i < detalles.size(); i++) {
				if (posicionpersona == i) {
					estado = detalles.get(i).estado;
					break;
				}
			}
			if (c.persona_id == App.JEFE) {
//				abrirDetalle(c, arg2, (List<Seccion01>) detalles);
				EditarDetallePersona(c);
			} else {
				if (!Util.esDiferente(estado, 0)) {
					EditarDetallePersona(c);
				}
			}
			
		}
	}
	private void inicio() {
		ValidarsiesSupervisora();
		txtCabecera.requestFocus();
	}
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			Util.cleanAndLockView(getActivity(), btnAgregar);
			rgQHBEBE.readOnly(); 
			rgQHNOFAM.readOnly();
			rgQHTEMPO.readOnly();		
			txtQH_OBS_SECCION01.setEnabled(false);
		}
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}

	
	public HogarService getHogarService(){
		if(hogarservice==null){
			hogarservice=HogarService.getInstance(getActivity());
		}
		return hogarservice;
	}
	public Seccion04_05Service getSeccion04_05Service(){
		if(seccion04service==null){
			seccion04service=Seccion04_05Service.getInstance(getActivity());
		}
		return seccion04service;
	}
	public Seccion03Service getSeccion03Service(){
		if(seccion03service==null){
			seccion03service=Seccion03Service.getInstance(getActivity());
		}
		return seccion03service;
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		String nombre="";
		Integer posicion = 0;
			posicion = detalles.get(info.position).persona_id!=null?detalles.get(info.position).persona_id:0;
			nombre=detalles.get(info.position).qh02_1!=null?detalles.get(info.position).qh02_1:"";
			
		if (v.equals(tcPersonas.getListView())) {
			menu.setHeaderTitle("Opciones del Residente");
			menu.add(1, 0, 1, "Editar Residente: ("+posicion+") "+nombre);
			menu.add(1, 1, 1, "Eliminar Residente: ("+posicion+") "+nombre);
			menu.add(1, 2, 1, "Seleccionar como Informante a: ("+posicion+") "+nombre);
	
			if (posicion > 1) {
				posicion = posicion - 1;
			}
			
			Seccion01 seleccion = (Seccion01) info.targetView.getTag();
			if(App.getInstance().getUsuario().cargo_id==App.CODIGO_SUPERVISORA && App.getInstance().getMarco().asignado==App.VIVIENDAASIGNADASUPERVISORA){
				menu.getItem(0).setVisible(false);
				menu.getItem(1).setVisible(false);
				menu.getItem(2).setVisible(false);
			}
			else{
				if (!getPersonaService().RegistroDePersonaCompletada(detalles.get(info.position).id,detalles.get(info.position).hogar_id, posicion)&& detalles.get(info.position).persona_id != App.JEFE) {
					menu.getItem(0).setVisible(false);
				}
				Seccion01 Info = getPersonaService().getPersona2(detalles.get(info.position).id,detalles.get(info.position).hogar_id,detalles.get(info.position).persona_id);
				if (!Util.esDiferente(Info.qhinfo, 1)) {
					menu.getItem(2).setVisible(false);
					menu.getItem(1).setVisible(false);
				}
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (!getUserVisibleHint())
			return false;
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		Seccion01 seleccion = (Seccion01) info.targetView.getTag();
		if (item.getGroupId() == 1) {
			switch (item.getItemId()) {
			case 0:
				EditarDetallePersona((Seccion01) detalles.get(info.position));
				break;
			case 1:
				action = ACTION.ELIMINAR;
				String nombre="";
				nombre=detalles.get(info.position).qh02_1!=null?detalles.get(info.position).qh02_1:"";
				dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro que desea eliminar a "+nombre+"?");
				dialog.put("seleccion", (Seccion01) detalles.get(info.position));
				dialog.showDialog();
				break;
			case 2:
				EstablecerComoInformante((Seccion01) detalles.get(info.position));
				break;
			}
		}
//		} else if (item.getGroupId() == 2) {
//			App.getInstance().setPersona(seleccion);
//			switch (item.getItemId()) {
//			case 1:
//				goTo(CuestionarioFragmentActivity.C1SECCION1f_2, seleccion);
//				break;
//			}
//		}		
		return super.onContextItemSelected(item);
	}

	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		Seccion01 bean = (Seccion01) dialog.get("seleccion");
		try {
			detalles.remove(bean);
			boolean borrado = false;
			borrado = getPersonaService().borrarPersona(bean);
			LimpiarPreguntasRelacionadasconlaPersona(bean);
			SeleccionarInformantedeSalud();
			ActulializaPersona_idInformantedeSalud();
//			ReenumerarIdEnLasotrasTablas(bean);
			if(detalles.size()>0){
				RealizarConsultasParaNavegacion();
			}
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
		cargarTabla();
	}
	
	
	public void ActulializaPersona_idInformantedeSalud(){
		if(App.getInstance().getPersonaSeccion01()!=null && App.getInstance().getPersonaSeccion01().id!=null && App.getInstance().getPersonaSeccion01().hogar_id!=null && App.getInstance().getPersonaSeccion01().persona_id!=null)
		getServiceSeccion01().ModificarEnlaceInformanteDeSalud(App.getInstance().getPersonaSeccion01().id, App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id);
	}
	
	
	private void RealizarConsultasParaNavegacion(){
	
		SeleccionarInformantedeSalud();
		if(App.getInstance().getPersonaSeccion01()!=null){
//		if(App.getInstance().getPersonaSeccion01().qh06==2 && App.getInstance().getPersonaSeccion01().qh07>14 && App.getInstance().getPersonaSeccion01().qh07<50){
//////		Salud salud = getCuestionarioService().getCAP01_03(App.getInstance().getPersonaSeccion01().id,App.getInstance().getPersonaSeccion01().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoS);
////		if(salud!=null){
////			if(!(salud.qs28!=null && salud.qs29a!=null && salud.qs29b!=null)){
////				mef = true;
////			}
////		}
//		}
		
		Salud salud01 = getCuestionarioService().getSaludNavegacionS1_3(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS1_3);
		if (salud01!=null) {
			App.getInstance().setSaludNavegacionS1_3(salud01);						
			App.getInstance().getSaludNavegacionS1_3().qh06=App.getInstance().getPersonaSeccion01().qh06;						
		}
		CAP04_07 cap04_07 = getCuestionarioService().getSaludNavegacionS4_7(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaSeccion01().persona_id,seccionesCargadoSaludS4_7);
		if (cap04_07!=null) {
			App.getInstance().setSaludNavegacionS4_7(cap04_07);	
			App.getInstance().getSaludNavegacionS4_7().existes8=getPersonaService().getCantidadNiniosPorRangodeEdad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,0,11);
			App.getInstance().getSaludNavegacionS4_7().existemef=getPersonaService().getCantidadMef(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id, 15, 49);
		}
		}
	}
	
	private void LimpiarPreguntasRelacionadasconlaPersona(Seccion01 bean){
		List<Seccion01> modificaciones= new ArrayList<Seccion01>();
		SeccionCapitulo[] seccionesCargadoQH23= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID" ,"QH23","QH25") };
		SeccionCapitulo[] secciongrabadoQH23 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "QH23") };
		SeccionCapitulo[] secciongrabadoQH25 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "QH25") };
		modificaciones=getPersonaService().getPersonasbyHogar(bean, seccionesCargadoQH23);
		boolean flag=false;
		if(modificaciones.size()>0){
			for(Seccion01 s1:modificaciones){
				if(!Util.esDiferente(s1.qh23,bean.persona_id)){
					s1.qh23=null;
					try {
						flag= getPersonaService().saveOrUpdate(s1, secciongrabadoQH23);	
						if(!flag){
							return;
						}
							
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				if(!Util.esDiferente(s1.qh25,bean.persona_id)){
					s1.qh25=null;
					try {
						flag=getPersonaService().saveOrUpdate(s1, secciongrabadoQH25);
						if(!flag){
							return;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
			}
		}
		SeccionCapitulo[] seccioncargadoSeccion04 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "ID","HOGAR_ID","QH209","PERSONA_ID_ORDEN") };
		SeccionCapitulo[] secciongrabadoSeccion04 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "QH209") };
		List<Seccion04_05> listado=  getSeccion04_05Service().getPersonasRelacionadasalborrar(bean.id,bean.hogar_id,seccioncargadoSeccion04);
		if(listado.size()>0){
		for(Seccion04_05 s04:listado){
			if(!Util.esDiferente(s04.qh209,bean.persona_id)){
				s04.qh209=null;
				try {
					flag=getSeccion04_05Service().saveOrUpdate(s04, secciongrabadoSeccion04);
					if(!flag){
						return;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}				
			}
		}
		}
		List<CSSECCION_08> listadoseccion8 = new ArrayList<CSSECCION_08>();
		SeccionCapitulo[] seccioncargadoSeccion08 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "ID","HOGAR_ID","QS802","PERSONA_ID","PERSONA_ID_NINIO") };
		SeccionCapitulo[] secciongrabadoSeccion08 = new SeccionCapitulo[] { new  SeccionCapitulo(-1, -1,-1, "QS802") };
		listadoseccion8= getCuestionarioService().getNiniosaModificaralBorrarPersona(bean.id, bean.hogar_id, seccioncargadoSeccion08);
		if(listadoseccion8.size()>0){
			for(CSSECCION_08 cs8:listadoseccion8){
				if(!Util.esDiferente(cs8.qs802, bean.persona_id)){
					try {
						flag=getCuestionarioService().saveOrUpdate(cs8,secciongrabadoSeccion08);
						if(!flag){
							return;
						}
							
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
	
		
	}
//	public void ReenumerarIdEnLasotrasTablas(Seccion01 persona){
//		List<Seccion03> listadoseccion03= new ArrayList<Seccion03>();
//		List<Seccion04_05> listadoseccion04= new ArrayList<Seccion04_05>();
//		List<CSVISITA> listadovisita= new ArrayList<CSVISITA>();
//		List<Individual> listadovisitaIndidual= new ArrayList<Individual>();
//		Salud salud =new Salud();
//		CAP04_07 salud2= new CAP04_07();
//		List<CSSECCION_08> listadoSeccion8 = new ArrayList<CSSECCION_08>();
//		List<CSSECCION_08> detallesamodificar = new ArrayList<CSSECCION_08>();
//		SeccionCapitulo[] seccionesCargadoSeccion03= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID" ,"PERSONA_ID_ORDEN","PREGUNTA_ID") };
//		SeccionCapitulo[] seccionesCargadoSeccion04= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID" ,"PERSONA_ID_ORDEN") };
//		SeccionCapitulo[] seccionesCargadoVisitaSalud= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID" ,"NRO_VISITA") };
//		SeccionCapitulo[] seccionesCargadoSalud01_03= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID") };
//		SeccionCapitulo[] seccionesCargadoSalud04_07= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID") };
//		SeccionCapitulo[] seccionesCargadoSaludSeccion08= new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO") };
//		SeccionCapitulo[] seccionescargado8= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID","PERSONA_ID_NINIO")};
//		SeccionCapitulo[] seccionescargadoindividual= new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"HOGAR_ID","ID","PERSONA_ID")};
//		
//		listadoseccion03=getSeccion03Service().getPersonasAModificar(persona.id,persona.hogar_id,persona.persona_id,seccionesCargadoSeccion03);
//		listadoseccion04 = getSeccion04_05Service().getPersonasAModificaralBorrar(persona.id, persona.hogar_id,persona.persona_id, seccionesCargadoSeccion04);
//		listadovisita = getCuestionarioService().getVisitasAModificarAlBorrarPersona(persona.id, persona.hogar_id,persona.persona_id, seccionesCargadoVisitaSalud);
//		salud = getCuestionarioService().getSaludAModificarAlBorrarPersona(persona.id, persona.hogar_id, persona.persona_id, seccionesCargadoSalud01_03);
//		salud2 = getCuestionarioService().getSalud04_07AModificarAlBorrarPersona(persona.id,persona.hogar_id,persona.persona_id, seccionesCargadoSalud04_07);
//		detallesamodificar = getCuestionarioService().getInformanteSaludModificarAlBorrarPersona(persona.id,persona.hogar_id,persona.persona_id,seccionescargado8);
//		listadoSeccion8 = getCuestionarioService().getSaludSeccion08AModificarAlBorrarPersona(persona.id, persona.hogar_id,persona.persona_id, seccionesCargadoSaludSeccion08);
//		listadovisitaIndidual = getCuestionarioService().getCaratulaIndividualModificarAlBorrarPersona(persona.id,persona.hogar_id,persona.persona_id, seccionescargadoindividual);
//		boolean flag=false;
//			if(listadoseccion03.size()>0){
//				for(Seccion03 s3: listadoseccion03){
//					flag=getSeccion03Service().ModificarNumerodeOrdenSeccion03(persona.id,persona.hogar_id,s3.persona_id_orden-1,s3.persona_id_orden,s3.pregunta_id);
//				}
//			}
//			
//			if(listadoseccion04.size()>0){
//				for(Seccion04_05 s4:listadoseccion04){
//					flag= getSeccion04_05Service().ModificarNumerodeOrdenSeccion04_05(persona.id, persona.hogar_id, s4.persona_id_orden-1, s4.persona_id_orden);
//				}
//			}
//			if(listadovisita.size()>0){
//				for(CSVISITA v:listadovisita){
//					flag=getCuestionarioService().ModificarNumerodeOrdenVisitaSalud(persona.id,persona.hogar_id,v.persona_id-1,v.persona_id);
//					break;
//				}
//			}
//			if(salud!=null){
//				flag=getCuestionarioService().ModificarNumerodeOrdenSalud01_03(persona.id, persona.hogar_id, salud.persona_id-1, salud.persona_id);
//			}
//			if(salud2!=null){
//				flag= getCuestionarioService().ModificarNumerodeOrdenSalud04_07(persona.id, persona.hogar_id,salud2.persona_id-1, salud2.persona_id);
//			}
//			if(detallesamodificar.size()>0){
//				for (CSSECCION_08 css8: detallesamodificar ) {
//						flag= getCuestionarioService().ModificarNumerodeOrdenInformanteSaludSeccion08(persona.id,persona.hogar_id, css8.persona_id-1,css8.persona_id, css8.persona_id_ninio);
//					}
//			}
//			if(listadoSeccion8.size()>0){
//				for(CSSECCION_08 css8:listadoSeccion8){
//					flag = getCuestionarioService().ModificarNumerodeOrdenSaludSeccion08(persona.id,persona.hogar_id, css8.persona_id_ninio-1, css8.persona_id_ninio,css8.persona_id);
//				}
//			}
//			if(listadovisitaIndidual.size()>0){
//				for(Individual ci: listadovisitaIndidual){
//					flag = getCuestionarioService().ModificarNumerodeOrdenCaratulaIndividual(persona.id,persona.hogar_id,ci.persona_id-1 , ci.persona_id);
//				}
//			}
//		}
	public void SeleccionarInformantedeSalud(){
		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getPersonaService());
		informantedesalud = getPersonaService().getPersonanformanteSalud(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id,persona_id,seccionesCargadoInformanteSalud);
		App.getInstance().SetPersonaSeccion01(informantedesalud);
		}
	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(bean);
		boolean flag=false;
		try {
			flag = getService().saveOrUpdate(bean, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos del hogar.");
			}
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return App.NODEFINIDO;
		} 
		return App.HOGAR;
	}
}
