package gob.inei.endes2024.fragment.seccion01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.GridFormComponent.Valor;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.Seccion03Service;
import gob.inei.endes2024.service.Seccion04_05Service;
import gob.inei.endes2024.service.VisitaService;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
//import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class Seccion01Fragment_002 extends FragmentForm implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public TextField txtQH02_1;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQH02_2;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQH02_3;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQH03;     
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH04;     
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH05; 
	@FieldAnnotation(orderIndex=7)          
	public RadioGroupOtherField rgQH06;     
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQH07;
	@FieldAnnotation(orderIndex=9)          
	public CheckBoxField chbQH07; 
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQH7DD;
	@FieldAnnotation(orderIndex=11)
	public IntegerField txtQH7MM;
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH08;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQH11_A; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQH11_B; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQH11_C; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQH11_D; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQH11_E; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQH11_Y; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQH11_Z; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQH12;
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQH13;
	@FieldAnnotation(orderIndex = 22)
	public TextAreaField txtQHOBSPERSONA;
	private DialogComponent dialog;
    public ButtonComponent btnNosabedia,btnNosabemes;   
    private enum ACTION {
		ELIMINAR, MENSAJE
	}
    private ACTION action;
    
	Seccion01 bean; 
	public Seccion01 informantedesalud;
	private static Seccion01Fragment_001 caller;
	private SeccionCapitulo[] seccionesCargado,seccionesGrabado3a14,seccionesGrabado0A2,seccionesGrabado15A24,seccionesGrabado25A97,seccionesGrabadoClear,seccionesCargadoInformanteSalud,seccionesGrabadoParcial,SeccionesGrabadoHogar;
	private SeccionCapitulo[] seccionesCargadoSeccion04,seccionesGrabadoseccion04,seccionesGrabadoSeccion08,SeccionesGrabadoDiscapacidad;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	private Seccion01Service seccion01Service;
	private Seccion04_05Service Persona04_05Service;
	private Seccion01Service Personaservice;
	public GridComponent2 gridNombres,gridPregunta07,gridPregunta07A;
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco,lblviveaqui,lblDurmioAqui,lblhombreomujer,lblcumpleanios,lblenanios,lbldia,lblmes,lblafiliado,lblafiliado1,lbltitular,lblpregunta13,lblpregunta07_1,lblTitulo1,lblTitulo2;
	public LabelComponent lblpregunta02,lblpregunta07,lblpregunta08;
	public boolean validarCheck=false;
	
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4; 
	LinearLayout q5;                       
	LinearLayout q6;                           
	LinearLayout q7;                                               
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	
	private Seccion03Service seccion03service;	
	
	private List<Visita> visitas;
	VisitaService visitaService;
	
	public Integer info_salud_ant=-1;
	
	String qh13fechref;	
	
	Integer valor = 0;
	

	public Seccion01Fragment_002() {} 
	public Seccion01Fragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","ID","HOGAR_ID","PERSONA_ID","QHINFO","QHOBSPERSONA") };
		seccionesGrabado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA","QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A") };
		seccionesGrabado3a14 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA") };		
		seccionesGrabado15A24 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA","QH22","QH23","QH24","QH25") };
		seccionesGrabado25A97 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","QH22","QH23","QH24","QH25") };
		seccionesGrabadoClear = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","QH22","QH23","QH24","QH25","QH26A1","QH26A2","QH26A3","QH26A4","QH26A5","QH26A6") };
		seccionesGrabadoParcial = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH13FECH_REF","QHOBSPERSONA","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13")};
		seccionesCargadoInformanteSalud = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH06","QH07","QH14","QH15N","QH15Y","QH15G","QH11_C","QH11_A","QH11_B","QH11_D","QH11_E")};
		SeccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "NRO_INFO_S")};
		SeccionesGrabadoDiscapacidad= new  SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "ID","HOGAR_ID","CUESTIONARIO_ID","PERSONA_ID","NINIO_ID")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  
	  lblTitulo1 = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_tituloNroOrden).textSize(21).centrar().negrita();
	  
	  lblnombre = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_1).size(altoComponente, 350).textSize(16);
	  lblApPaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_2).size(altoComponente, 350).textSize(16);
	  lblApMaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_3).size(altoComponente, 350).textSize(16);
	  lblpregunta02 = new LabelComponent(this.getActivity()).text(R.string.seccion01nombres_ediat).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
	  lblpregunta07 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh07).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
	  lblpregunta07_1 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh07_0).size(MATCH_PARENT, MATCH_PARENT).textSize(17).negrita();
	  lblpregunta08 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh08).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		
	  txtQH02_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto().callback("txtonQH201_1ChangeValue");
	  txtQH02_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
	  txtQH02_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto().callback("OcultarTecla");
	  lblparentesco= new LabelComponent(this.getActivity()).text(R.string.seccion01qh03).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
	  rgQH03 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh03_1,R.string.seccion01qh03_2,R.string.seccion01qh03_3,R.string.seccion01qh03_4,R.string.seccion01qh03_5,R.string.seccion01qh03_6,R.string.seccion01qh03_7,R.string.seccion01qh03_8,R.string.seccion01qh03_9,R.string.seccion01qh03_10,R.string.seccion01qh03_11,R.string.seccion01qh03_12).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH03Block");
	  rgQH04=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh04_1,R.string.seccion01qh04_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
	  rgQH05=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh05_1,R.string.seccion01qh05_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
	  rgQH06=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh06_1,R.string.seccion01qh06_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onQH06ChangeValue");
		
	  lblenanios = new LabelComponent(this.getActivity()).size(60, 150).text(R.string.seccion01qh07_1).textSize(16).centrar();
	  txtQH07=new IntegerField(this.getActivity()).size(60, 60).maxLength(2).callback("ontxtQH7ChangeValue");
	  chbQH07=new CheckBoxField(this.getActivity(), R.string.seccion01qh07_2, "1:0").size(70, 400).callback("onchbQH7ChangeValue");
		
	  lbldia = new LabelComponent(this.getActivity()).size(60, 100).text(R.string.seccion01qh7dd).textSize(18).centrar();
	  txtQH7DD=new IntegerField(this.getActivity()).size(60, 80).maxLength(2).centrar();
	  txtQH7MM=new IntegerField(this.getActivity()).size(60, 80).maxLength(2).centrar().callback("OcultarTeclatxtQH7DD");//
	  lblmes = new LabelComponent(this.getActivity()).size(60, 100).text(R.string.seccion01qh7mm).textSize(18).centrar();
	  rgQH08=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh08_1,R.string.seccion01qh08_2,R.string.seccion01qh08_3,R.string.seccion01qh08_4,R.string.seccion01qh08_5,R.string.seccion01qh08_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH08ChangeValue");
		
	  Spanned texto11 = Html.fromHtml("11. ¿(NOMBRE) está afiliada(o) o inscrita(o) en: ESSALUD, Seguro Integral de Salud o en algún otro seguro de salud?");
	  lblafiliado = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
	  lblafiliado.setText(texto11);
		
	  lbltitular = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.seccion01qh12);
	  lblpregunta13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.seccion01qh13);
	  chbQH11_A=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_AChangeValue"); 
//	  chbQH11_A.setOnCheckedChangeListener(new OnCheckedChangeListener() {		
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (isChecked){
//					conteos();
//					
//		  		}
//				else {
//					conteosMenos();
//				}
//			}
//		});
	  
	  chbQH11_B=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_BChangeValue"); 
	  chbQH11_C=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_CChangeValue"); 
	  chbQH11_D=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_DChangeValue"); 
	  chbQH11_E=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_EChangeValue"); 
	  chbQH11_Y=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_YChangeValue"); 
	  chbQH11_Z=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_ZChangeValue"); 
	  rgQH12=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh12_1,R.string.seccion01qh12_2,R.string.seccion01qh12_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
	  rgQH13=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh13_1,R.string.seccion01qh13_2,R.string.seccion01qh13_3,R.string.seccion01qh13_4,R.string.seccion01qh13_5,R.string.seccion01qh13_6,R.string.seccion01qh13_7,R.string.seccion01qh13_8,R.string.seccion01qh13_9,R.string.seccion01qh13_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);

	  lblviveaqui = new LabelComponent(getActivity()).text(R.string.seccion01qh04).size(MATCH_PARENT,MATCH_PARENT).textSize(19);             
	  lblDurmioAqui = new LabelComponent(getActivity()).text(R.string.seccion01qh05).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
	  lblhombreomujer  = new LabelComponent(getActivity()).text(R.string.seccion01qh06).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
	  lblcumpleanios= new LabelComponent(getActivity()).text(R.string.seccion01qh07A).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
	  btnNosabedia = new ButtonComponent(getParent(),App.ESTILO_BOTON).text(R.string.seccion01qh7anosabe).size(150, 20);
	  btnNosabemes = new ButtonComponent(getParent(),App.ESTILO_BOTON).text(R.string.seccion01qh7anosabe).size(150, 20);
		
	  txtQHOBSPERSONA = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
	  txtQHOBSPERSONA.setCallback("OcultarTecla");
		
		
	  gridNombres = new GridComponent2(this.getActivity(),App.ESTILO, 2);
	  gridNombres.addComponent(lblnombre);
	  gridNombres.addComponent(txtQH02_1);
	  gridNombres.addComponent(lblApPaterno);
	  gridNombres.addComponent(txtQH02_2);
	  gridNombres.addComponent(lblApMaterno);
	  gridNombres.addComponent(txtQH02_3);
           
	  gridPregunta07=new GridComponent2(this.getActivity(),App.ESTILO,3,0);
	  gridPregunta07.addComponent(lblenanios);
	  gridPregunta07.addComponent(txtQH07);
	  gridPregunta07.addComponent(chbQH07);
           
	  gridPregunta07A= new GridComponent2(this.getActivity(),App.ESTILO,3,0);
	  gridPregunta07A.addComponent(lbldia);
	  gridPregunta07A.addComponent(txtQH7DD);
	  gridPregunta07A.addComponent(btnNosabedia);
	  gridPregunta07A.addComponent(lblmes);
	  gridPregunta07A.addComponent(txtQH7MM);
	  gridPregunta07A.addComponent(btnNosabemes);
		
	  btnNosabedia.setOnClickListener(new View.OnClickListener() {
		  @Override
		  public void onClick(View v) {
			  txtQH7DD.setText("98");
		  }
	  } );
	  
	  btnNosabemes.setOnClickListener(new View.OnClickListener() {
		  @Override
		  public void onClick(View v) {
			  txtQH7MM.setText("98");
		  }
	  });
			
	
  
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q12 = createQuestionSection(lblTitulo1);
		q0 = createQuestionSection(0,lblafiliado);
		q1 = createQuestionSection(lblpregunta02,gridNombres.component());
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblparentesco,rgQH03);
		q3 = createQuestionSection(lblviveaqui,rgQH04);
		q4 = createQuestionSection(lblDurmioAqui,rgQH05); 
		q5 = createQuestionSection(lblhombreomujer,rgQH06);                       
		q6 = createQuestionSection(lblpregunta07,lblpregunta07_1,gridPregunta07.component());                           
		q7 = createQuestionSection(lblcumpleanios,gridPregunta07A.component());                                               
		q8 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta08,rgQH08); 
		q9 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,q0,chbQH11_A,chbQH11_B,chbQH11_C,chbQH11_D,chbQH11_E,chbQH11_Y,chbQH11_Z); 
		q10 = createQuestionSection(lbltitular,rgQH12); 
		q11 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta13,rgQH13);
		q13 = createQuestionSection(R.string.seccion01qh_obs_0,txtQHOBSPERSONA);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q12); 
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q9); 
		form.addView(q10); 
		form.addView(q11);
		form.addView(q13);
		
    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() {	
		
    	uiToEntity(bean);
		bean.qh07=combinaPregunta07();
		
		if (bean.qh12!=null) {
			bean.qh12=bean.getConvertqh12(bean.qh12);
		}
		if (bean.qh13!=null) {
			bean.qh13=bean.getConvertqh13(bean.qh13);			
		}	
		
		if(qh13fechref == null) {
			bean.qh13fech_ref =  Util.getFechaActualToString();
		}
		else {
			bean.qh13fech_ref = qh13fechref;
		}
		
//		}    
//		else {
//			bean.qh13fech_ref=null;
//		}
		
		if(bean.qh7dd!=null){
			bean.qh7dd=txtQH7DD.getText().toString().length()>1?txtQH7DD.getText().toString():"0"+txtQH7DD.getText().toString();
			bean.qh7mm=txtQH7MM.getText().toString().length()>1?txtQH7MM.getText().toString():"0"+txtQH7MM.getText().toString();
		}	
//		Log.e("","ID ="+bean.id);	
//		Log.e("","HOGAR_ID ="+bean.hogar_id);
//		Log.e("","PERSONA_ID ="+bean.persona_id);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();	
		try { 
//			bean.qh02_11=EndesCalendario.SepararSegundoNombre(bean.qh02_1);
//			bean.qh02_1=EndesCalendario.SepararPrimerNombre(bean.qh02_1);
			/*bean.qh02_11=EndesCalendario.SepararSegundoNombre(bean.qh02_1)==null?null:EndesCalendario.EncriptarDatosSensibles(EndesCalendario.SepararSegundoNombre(bean.qh02_1));*/			
			/*bean.qh02_2=bean.qh02_2==null?null:EndesCalendario.EncriptarDatosSensibles(bean.qh02_2);
			bean.qh02_3=bean.qh02_3==null?null:EndesCalendario.EncriptarDatosSensibles(bean.qh02_3);*/
			if(bean.qh07==null) {
				
				flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabadoClear);
			}
			if (bean.qh07!=null) {
				if (MyUtil.incluyeRango(0,2,bean.qh07)) {
					LimpiarPreguntasporEdad(bean.qh07);
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado0A2); 
				}
				if (MyUtil.incluyeRango(3,14,bean.qh07)) {
					LimpiarPreguntasporEdad(bean.qh07);
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado3a14);
				} 
				if (MyUtil.incluyeRango(15,24,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado15A24);
				}
				if (MyUtil.incluyeRango(25,97,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado25A97);
				}
			}
        	  
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos de la Persona.");
			}
			
			
					
			getService().commitTX(dbTX);
			EliminarPersonaDelaSeccion8SaludAlModificarEdad();
			SeleccionarInformantedeSalud();
			
			//####NO utilizar  o evaluar de forma adecuada
//			if(bean.qh07!=null && bean.qh06!=null && bean.qh06==2 && !MyUtil.incluyeRango(App.HogEdadMefMin,App.HogEdadMefMax,bean.qh07)){
//				Log.e("dd","sssss");
//				getPersonaService().borrarPersonaCuandoCambiaSexo(bean);			
//			}
			
			///se cambió este metodo a con el método que esta debajo 
			//getCuestionarioService().saveOrUpdate(llenerInformantedelHogar(), SeccionesGrabadoHogar);
			ActulializaPersona_idInformantedeSalud();
			EvaluarSeccion4(bean);
			EvaluarCSSeccion8(bean);

			//agregado para grabar discapacidad
//			EvaluarDiscapacidadIndividual(bean);
			//			ActualizarInformanteSalud();
		} 
		catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage()+"Error uno", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage()+"Error dos",ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
//		App.getInstance().SetPersonaSeccion01(bean);
		App.getInstance().setEditarPersonaSeccion01(bean);
		return true; 
    } 
    
    public void ActulializaPersona_idInformantedeSalud(){
    	if(bean!=null && bean.id!=null && bean.hogar_id!=null && App.getInstance().getPersonaSeccion01()!=null && App.getInstance().getPersonaSeccion01().persona_id!=null)
    		getPersonaService().ModificarEnlaceInformanteDeSalud(bean.id,bean.hogar_id, App.getInstance().getPersonaSeccion01().persona_id);
	}

    
    public void EvaluarSeccion4(Seccion01 bean){
    	seccionesCargadoSeccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")};
    	seccionesGrabadoseccion04 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_ORDEN")};
    	Seccion01 informantehogar;
    	informantehogar = getPersonaService().getPersonaInformante(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, 1);
    	Seccion04_05 personas4= getSeccion04_05Service().getSeccion05_persona(bean.id, bean.hogar_id, bean.persona_id, seccionesCargadoSeccion04);
//    	if(personas4!=null &&  (!Util.esDiferente(bean.qh06,2) && ((Util.esMayor(bean.qh07, 5) && Util.esMenor(bean.qh07, 15)) || (Util.esMayor(bean.qh07, 49)))) && !Util.esDiferente(bean.persona_id, personas4.persona_id_orden)){
    	if(personas4!=null &&  (!Util.esDiferente(bean.qh06,2) && ((Util.esMayor(bean.qh07, 5) && Util.esMenor(bean.qh07, App.HogEdadMefMin)) || (Util.esMayor(bean.qh07, App.HogEdadMefMax)))) && !Util.esDiferente(bean.persona_id, personas4.persona_id_orden)){
    		getSeccion04_05Service().BorrarPersona_Seccion04_05(bean);
    	}
    	else if(personas4!=null && !Util.esDiferente(bean.qh06, 1) && Util.esMayor(bean.qh07, 5) && !Util.esDiferente(bean.persona_id, personas4.persona_id_orden)){
    		getSeccion04_05Service().BorrarPersona_Seccion04_05(bean);
    	}
    	else{
    		if(bean.qh06!=null && bean.qh07!=null && !Util.esDiferente(bean.qh06,2,1) && Util.esMenor(bean.qh07,6) || (!Util.esDiferente(bean.qh06,2) && Util.esMayor(bean.qh07,App.HogEdadMefMin-1) && Util.esMenor(bean.qh07,App.HogEdadMefMax+1))  ){
	    		if(personas4==null){
	    			personas4= new Seccion04_05();
	    			personas4.id=bean.id;
	    			personas4.hogar_id=bean.hogar_id;
	    			personas4.persona_id=informantehogar!=null?informantehogar.persona_id:0;
	    			personas4.persona_id_orden=bean.persona_id;
	    		}
	    		try {
	    			getSeccion04_05Service().saveOrUpdate(personas4, seccionesGrabadoseccion04);	
				} catch (SQLException e) {
					ToastMessage.msgBox(this.getActivity(), e.getMessage()+"S04 HOGAR",ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				}
    		}
    	}
    }
    public void EvaluarCSSeccion8(Seccion01 persona){
    	CSSECCION_08 salud=null;
    	seccionesGrabadoSeccion08 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_ID","PERSONA_ID_NINIO" )};
    	salud=getCuestionarioService().getCSSeccion08(bean.id, bean.hogar_id, bean.persona_id, seccionesGrabadoSeccion08);
    	if(salud!=null && !Util.esDiferente(salud.persona_id_ninio, bean.persona_id) && Util.esMayor(persona.qh07, 11)){
    		getPersonaService().BorrarPersonaCSSeccion8(persona);
    	}
    	if(salud==null && Util.esMenor(persona.qh07, 12)){
    		salud= new CSSECCION_08();
    		salud.id=persona.id;
    		salud.hogar_id=persona.hogar_id;
    		salud.persona_id=1;
    		salud.persona_id_ninio=persona.persona_id;
    		try {
    			if(!getCuestionarioService().saveOrUpdate(salud,seccionesGrabadoSeccion08)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados 1.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				}
			} catch (SQLException e) {
				ToastMessage.msgBox(this.getActivity(), e.getMessage()+" CS S08", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
			}
    	}
    }
    public void EvaluarDiscapacidadIndividual(Seccion01 persona){
    	DISCAPACIDAD dicapacidad_individual=null;
    	dicapacidad_individual=getCuestionarioService().getDiscapacidad_byCuestionario_id(persona.id,persona.hogar_id,App.CUEST_ID_CARA_INDIVIDUAL,persona.persona_id,SeccionesGrabadoDiscapacidad);
//    	Log.e("dicapacidad_individual",""+dicapacidad_individual);
    	if(dicapacidad_individual==null && !Util.esDiferente(persona.qh06, 2) && Util.between(persona.qh07, App.HogEdadMefMin,App.HogEdadMefMax)){
    		dicapacidad_individual= new DISCAPACIDAD();
    		dicapacidad_individual.id=persona.id;
    		dicapacidad_individual.hogar_id=persona.hogar_id;
    		dicapacidad_individual.cuestionario_id=App.CUEST_ID_INDIVIDUAL;
    		dicapacidad_individual.persona_id=persona.persona_id;    		
    		dicapacidad_individual.ninio_id=0;
    		try {
//    			Log.e("","Grabar");
    			getCuestionarioService().saveOrUpdate_Discapacidad(dicapacidad_individual, SeccionesGrabadoDiscapacidad);	
			} catch (Exception e) {
				// TODO: handle exception
				ToastMessage.msgBox(this.getActivity(), e.getMessage()+"Error discapacidad ", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
			}
    		
    	}
    	else{
    		if(Util.between(persona.qh07, 0,11) || !Util.esDiferente(persona.qh06, 1) || Util.between(persona.qh07, 50,99)){
//    			Log.e("","Se borro la mef");
    			getCuestionarioService().BorrarPersonadeDiscapacidadporcuestionario(persona.id, persona.hogar_id, persona.persona_id, App.CUEST_ID_INDIVIDUAL);
    		}
    	}
    }
    public Hogar llenerInformantedelHogar(){
    	Hogar hogar = new Hogar();
		hogar.id= App.getInstance().getHogar().id;
		hogar.hogar_id=App.getInstance().getHogar().hogar_id;
		hogar.nro_info_s=App.getInstance().getPersonaSeccion01()==null?null:App.getInstance().getPersonaSeccion01().persona_id;
    	return hogar;
    }
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (Util.esVacio(bean.qh02_1)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.02_1"); 
			view = txtQH02_1; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh03)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.03"); 
			view = rgQH03; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh04)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.04"); 
			view = rgQH04; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh05)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.05"); 
			view = rgQH05; 
			error = true; 
			return false; 
		} 
   
		if ((!Util.esDiferente(bean.qh04,1,1) && !Util.esDiferente(bean.qh05,1,2)) || (!Util.esDiferente(bean.qh04,1,2) && !Util.esDiferente(bean.qh05,1,1))) {
    
			if (Util.esVacio(bean.qh06)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.06"); 
				view = rgQH06; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(bean.qh07)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.07"); 
				view = txtQH07; 
				error = true; 
				return false; 
			}
			if (!MyUtil.incluyeRango(0,11,bean.qh07)) {
				if (MyUtil.incluyeRango(12,14,bean.qh07)) {
					if (Util.esVacio(bean.qh08)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.08"); 
						view = rgQH08; 
						error = true; 
						return false; 
					}
				}
				else{
		   	   	   	if (Util.esVacio(bean.qh7dd)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.7DD"); 
		   	   	   		view = txtQH7DD; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	} 	
		   	   	   	if (Util.esVacio(bean.qh7mm)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.7MM"); 
		   	   	   		view = txtQH7MM; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	}				
		   	   	   	if (Util.esVacio(bean.qh08)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.08"); 
		   	   	   		view = rgQH08; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	}
				}	
			}
			if (!Util.alMenosUnoEsDiferenteA(0,bean.qh11_a,bean.qh11_b,bean.qh11_c,bean.qh11_d,bean.qh11_e,bean.qh11_y,bean.qh11_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQH11_A; 
				error = true; 
				return false; 
			} 
		
			if (chbQH11_A.isChecked() || chbQH11_B.isChecked() || chbQH11_C.isChecked() || chbQH11_D.isChecked() || chbQH11_E.isChecked()) {
				if (Util.esVacio(bean.qh12)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.12"); 
					view = rgQH12; 
					error = true; 
					return false; 
				}
			}
			if (Util.esMayor(bean.qh07,6)) {
				if (Util.esVacio(bean.qh13)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.13"); 
					view = rgQH13; 
					error = true; 
					return false; 
				} 
			}
     	}     


    	if (MyUtil.incluyeRango(0, 11, bean.qh07) && !Util.esDiferente(bean.qh03,App.JEFE)) {
	    		mensaje ="Edad de jefe de hogar no debe ser menor a 12 años";
				view = txtQH07;
				error=true;
				return false;
    	}
    	
      	if (chbQH11_C.isChecked() && Util.esDiferente(bean.qh12,1)) {
    		mensaje ="La persona debe ser Titular del Seguro SIS";
			view = rgQH12;
			error=true;
			return false;
    	}
      	
    	if (chbQH11_A.isChecked() && chbQH11_C.isChecked()) {
    		mensaje ="La persona no puede tener seguro de salud SIS y ESSALUD";
			view = chbQH11_A;
			error=true;
			return false;
    	}
    	
    	if (!Util.esDiferente(bean.qh04,2) && !Util.esDiferente(bean.qh05,2)) {
    		mensaje ="La persona no vive habitualmente ni durmió la noche anterior";
			view = rgQH05;
			error=true;
			return false;
    	}
    	if(bean.qh7mm!=null){
	    	if ((Util.esMenor(Integer.parseInt(bean.qh7mm),1) ||  Util.esMayor(Integer.parseInt(bean.qh7mm),12)) && Util.esDiferente(Integer.parseInt(bean.qh7mm), 98,98)) {
	    		mensaje ="Mes fuera de rango";
				view = txtQH7MM;
				error=true;
				return false;
	    	}
    	}
    	if(bean.qh7dd!=null){
	    	if(Util.esMayor(Integer.parseInt(bean.qh7dd), 31) && Util.esDiferente(Integer.parseInt(bean.qh7dd), 98,98)){
	    		mensaje ="dia fuera de rango";
				view = txtQH7DD;
				error=true;
				return false;
	    	}
    	}
    	if(bean.qh7dd!=null && bean.qh7mm!=null){
	    	if(Util.esMenor(Integer.parseInt(bean.qh7dd), 32) && Util.esMenor(Integer.parseInt(bean.qh7mm), 13)){
	    		Integer dia=Integer.parseInt(bean.qh7dd.toString());
	    		Integer mes=Integer.parseInt(bean.qh7mm.toString());    		
	    		
	    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
	    			mensaje ="Dia no corresponde al mes que eligió";
	    			view = txtQH7DD;
	    			error=true;
	    			return false;
	    		}
	    	}
    	}
//    	if(!Util.esDiferente(bean.qh11_a, 1) && (!Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_c, 1))){
//    		mensaje ="Entidades no compatibles";
//			view = chbQH11_A;
//			error=true;
//			return false;
//    	}
//    	if(!Util.esDiferente(bean.qh11_b, 1) && (!Util.esDiferente(bean.qh11_a, 1) || !Util.esDiferente(bean.qh11_c, 1) || !Util.esDiferente(bean.qh11_d, 1))){
//    		mensaje ="Entidades no compatibles";
//			view = chbQH11_B;
//			error=true;
//			return false;
//    	}
//    	if(!Util.esDiferente(bean.qh11_c, 1) && (!Util.esDiferente(bean.qh11_a, 1) || !Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_d, 1) || !Util.esDiferente(bean.qh11_e,1))){
//    		mensaje ="Entidades no compatibles";
//			view = chbQH11_C;
//			error=true;
//			return false;
//    	}	
//    	if(!Util.esDiferente(bean.qh11_d, 1) && (!Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_c, 1))){
//    		mensaje ="Entidades no compatibles";
//			view = chbQH11_D;
//			error=true;
//			return false;
//    	}
//    	if(!Util.esDiferente(bean.qh11_e, 1) && !Util.esDiferente(bean.qh11_c, 1)){
//    		mensaje ="Entidades no compatibles";
//			view = chbQH11_E;
//			error=true;
//			return false;
//    	}
    	
     	if(!Util.esDiferente(bean.qh11_a, 1) && !Util.esDiferente(bean.qh11_c, 1)){
    		mensaje ="Entidades no compatibles 1";
			view = chbQH11_C;
			error=true;
			return false;
    	}
     	
    	if(!Util.esDiferente(bean.qh11_a, 0) && !Util.esDiferente(bean.qh11_b, 1) && (!Util.esDiferente(bean.qh11_c, 1) || !Util.esDiferente(bean.qh11_d, 1))){
    		mensaje ="Entidades no compatibles 2";
    		view = chbQH11_B;
    		error=true;
    		return false;
    	}
     	
    	
    	if(!Util.esDiferente(bean.qh11_c, 1) && (!Util.esDiferente(bean.qh11_a, 1) || !Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_d, 1) || !Util.esDiferente(bean.qh11_e,1))){
    		mensaje ="Entidades no compatibles 3";
			view = chbQH11_A;
			error=true;
			return false;
    	}	
    	
    	if(!Util.esDiferente(bean.qh11_a, 1) && !Util.esDiferente(bean.qh11_d, 1) && !Util.esDiferente(bean.qh11_c, 1)){
    		mensaje ="Entidades no compatible 4";
			view = chbQH11_D;
			error=true;
			return false;
    	}
    	
    	if(!Util.esDiferente(bean.qh11_a, 0) && !Util.esDiferente(bean.qh11_d, 1) && (!Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_c, 1))){
    		mensaje ="Entidades no compatibles 5";
			view = chbQH11_D;
			error=true;
			return false;
    	}
    	if(!Util.esDiferente(bean.qh11_e, 1) && !Util.esDiferente(bean.qh11_c, 1)){
    		mensaje ="Entidades no compatibles 6";
			view = chbQH11_C;
			error=true;
			return false;
    	}

//    	if(!Util.esDiferente(bean.qh11_d, 1) && !Util.esDiferente(bean.qh11_a, 0)){
//    		mensaje ="Entidades no compatibles 7";
//			view = chbQH11_A;
//			error=true;
//			return false;
//    	}

    	
    	//if(caller.detalles.size()>1){
		if(Util.esMayor(bean.qh01,0)){
			/******** VALIDACION DE SEXO DE CONYUGUE   *******/
			if(!Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA,App.JEFE)){
				if((!Util.esDiferente(getServiceSeccion01().getSexobypersonaid(bean.id, bean.hogar_id, App.JEFE),bean.qh06) && !Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA) 
						||(!Util.esDiferente(bean.persona_id,App.JEFE) && !Util.esDiferente(getServiceSeccion01().getSexobypersonaid(bean.id, bean.hogar_id,getServiceSeccion01().getPersonaIddeEsposo(bean.id, bean.hogar_id)), bean.qh06) ))){
					mensaje ="VALIDAR: Sexo de jefe de hogar no debe ser igual a sexo de conyugue";
					validarMensaje(mensaje);
//					view = rgQH06;
//					error=true;
//					return false;
				}
			}

			/******** VALIDACION DE PADRES   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.PADRE,App.HOMBRE) && !Util.esDiferente(bean.qh06,App.HOMBRE)) {
				if(!Util.esDiferente(bean.qh03,App.PADRE) && !Util.esDiferente(bean.qh06,App.HOMBRE) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.PADRE,App.HOMBRE),bean.persona_id)){ 
					mensaje ="No debe haber más de 2 Padres";
					view = rgQH03;
					error=true;
					return false;
				}
			}
			/******** VALIDACION DE MADRESS   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.MADRE,App.MUJER) && !Util.esDiferente(bean.qh06,App.MUJER) ) {
				if(!Util.esDiferente(bean.qh03,App.MADRE) && !Util.esDiferente(bean.qh06,App.MUJER) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.MADRE,App.MUJER),bean.persona_id)){ 
					mensaje ="No debe haber más de 2 Madres";
					view = rgQH03;
					error=true;
					return false;
				}
			}
			/******** VALIDACION DE SUEGROS   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.SUEGRO,App.HOMBRE) && !Util.esDiferente(bean.qh06,App.HOMBRE)) {
				if(!Util.esDiferente(bean.qh03,App.SUEGRO) && !Util.esDiferente(bean.qh06,App.HOMBRE) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.SUEGRO,App.HOMBRE),bean.persona_id)){ 
					mensaje ="No debe haber más de 2 Suegros";
					view = rgQH03;
					error=true;
					return false;
				}
			}
	       	/******** VALIDACION DE SUEGRAS   *******/
	    	if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.SUEGRA,App.MUJER) && !Util.esDiferente(bean.qh06,App.MUJER) ) {
	    		if(!Util.esDiferente(bean.qh03,App.SUEGRA) && !Util.esDiferente(bean.qh06,App.MUJER) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.SUEGRA,App.MUJER),bean.persona_id)){ 
	    			mensaje ="No debe haber más de 2 Suegras";
	    			view = rgQH03;
	    			error=true;
	    			return false;
			    }
		    }
	    	if (bean.qh07!=null) {
	    		/*********VALIDACION DE EDAD DE LOS HIJOS******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.HIJO_A)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    	  		if(edadjefe!=-1 && (edadjefe-bean.qh07<12))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los hijos no debe ser menor a 12 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DEL JEFE CON EL HIJO MAYOR******/
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadhijo_a=getServiceSeccion01().getEdadHijo_aMayor(bean.id, bean.hogar_id,App.HIJO_A);
		    		if(edadhijo_a!=-1 && bean.qh07-edadhijo_a<12)
		    		{
		    			mensaje ="Diferencia de edad de hijo_a mayor con el jefe de hogar no debe ser menor a 12 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
	    	
//		    	/*********VALIDACION DE EDAD DE LOS SUEGROS ******/
//		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
//		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
//		    			&& (!Util.esDiferente(bean.qh03, App.SUEGRO_SUEGRO )))
//		    	{
//		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
//		    		if(edadjefe!=-1 && (bean.qh07-edadjefe<24))
//		    		{
//		    			mensaje ="Diferencia de edad de jefe de hogar con los suegros no debe ser menor a 24 años";
//		    			view = txtQH07;
//		    			error=true;
//		    			return false;
//		    		}
//		    	}
//		    	/*********VALIDACION DE EDAD DE LOS NIETOS ******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.NIETO_NIETA)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    		if(edadjefe!=-1 && (edadjefe-bean.qh07<24))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los nietos no debe ser menor a 24 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DEL JEFE CON EL NIETO MAYOR******/
		    
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadnieto_nieta=getServiceSeccion01().getEdadHijo_aMayor(bean.id, bean.hogar_id,App.NIETO_NIETA);



		    		if(edadnieto_nieta!=-1 && bean.qh07-edadnieto_nieta<24)
		    		{
		    			mensaje ="Diferencia de edad de Nieto(a) mayor con el jefe de hogar no debe ser menor a 24 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DE LOS PADRES ******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.PADRE_MADRE)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    		if(edadjefe!=-1 && (bean.qh07-edadjefe<12))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los padres no debe ser menor a 12 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		     	/*********VALIDACION DE EDAD DEL JEFE CON EL PADRE MENOR******/
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadpadre_madre=getServiceSeccion01().getEdadMinimaPadreMenor(bean.id, bean.hogar_id,App.PADRE_MADRE);
		    
		    		if(edadpadre_madre!=-1 && edadpadre_madre-bean.qh07<12)	{
		    			mensaje ="Diferencia de edad de Padres con el jefe de hogar no debe ser menor a 12 años";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
	    	
			    	
		    	if (MyUtil.incluyeRango(0, 11, bean.qh07) && !Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA)) {
		    		mensaje ="Edad de esposa(o) no debe ser menor a 12 años";
		    		view = txtQH07;
		    		error=true;
		    		return false;
		    	}
			    	
		    	if (MyUtil.incluyeRango(0, 9, bean.qh07) && !Util.esDiferente(bean.qh03,App.EMPLEADA)) {
		    		mensaje ="Edad de la empleada doméstica no debe ser menor a 10 años";
		    		view = txtQH07;
		    		error=true;
		    		return false;
		    	}
	    	}	    	
		}    		
		return true; 
    } 
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    
    @Override 
    public void cargarDatos() { 
    	MyUtil.LiberarMemoria();
    	bean = getCuestionarioService().getSeccion01(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,App.getInstance().getEditarPersonaSeccion01().persona_id,seccionesCargado);

		
    	if (bean == null) {
			bean = new Seccion01();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id =App.getInstance().getHogar().hogar_id;
			bean.persona_id = caller.detalles.size()+1;
			bean.qh01 = caller.detalles.size()+1;
		}		

		if(bean.qh13!=null)	{
			bean.qh13=bean.setConvertqh13(bean.qh13);
		}
		if(bean.qh12!=null)	{
			bean.qh12=bean.setConvertqh12(bean.qh12);
		}
//		bean.qh02_1=bean.qh02_11==null?bean.qh02_1+"":bean.qh02_1+" "+bean.qh02_11;
		/*bean.qh02_2=EndesCalendario.DesencriptarData(bean.qh02_2);
		bean.qh02_3=EndesCalendario.DesencriptarData(bean.qh02_3);*/
		entityToUI(bean);
		if(bean.qh7dd!=null)
		{bean.qh7dd= bean.qh7dd.toString().length()>1?bean.qh7dd:"0"+bean.qh7dd;
			txtQH7DD.setText(bean.qh7dd);
		}
		if(bean.qh7mm!=null)
		{  bean.qh7mm= bean.qh7mm.toString().length()>1?bean.qh7mm:"0"+bean.qh7mm;
			txtQH7MM.setText(bean.qh7mm);
		}
		if (bean.qh07!=null) {
			muestraPregunta07(bean.qh07);
		}   
		
		qh13fechref = bean.qh13fech_ref;

    	inicio();     	

    } 
    private void inicio() { 
	
    	
//    	
    	//if(verificarCheck()){onchbQH11_AChangeValue();}
    	
    	
  		if (verificarCheck()) {	
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);
  			bloquearchbQH11C();
  		} else {		
			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
			chbQH11_B.requestFocus();				
  					
  		}
    	
    	
		if(chbQH11_Z.isChecked()){onchbQH11_ZChangeValue();}
		if(chbQH11_Y.isChecked()){onchbQH11_YChangeValue();}
   		
		if (bean.qh07!=null) {
			onchbQH7ChangeValue();    	
			ontxtQH7ChangeValue();
		}		
		onQH03Block();
		onrgQH08ChangeValue();
		CambiarNombreLabels();
		ValidarsiesSupervisora();
//		InformanteSalud_Anterior();
		rgQH04.requestFocus();
		
		valor = 0;
    	
		if (bean.qh11_a!=null && !Util.esDiferente(bean.qh11_a, 1)) {
			reciclado(chbQH11_A);
		}
		if (bean.qh11_b!=null && !Util.esDiferente(bean.qh11_b, 1)) {
			reciclado(chbQH11_B);
		}
		if (bean.qh11_c!=null && !Util.esDiferente(bean.qh11_c, 1)) {
			reciclado(chbQH11_C);
		}
		if (bean.qh11_d!=null && !Util.esDiferente(bean.qh11_d, 1)) {
			reciclado(chbQH11_D);
		}
		if (bean.qh11_e!=null && !Util.esDiferente(bean.qh11_e, 1)) {
			reciclado(chbQH11_E);
		}
    	
//    	reciclado(chbQH11_B);
//    	reciclado(chbQH11_C);
//    	reciclado(chbQH11_D);
//    	reciclado(chbQH11_E);
    	
    } 
    public void SeleccionarInformantedeSalud(){
		Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id,  getVisitaService(), getServiceSeccion01());
		informantedesalud = getServiceSeccion01().getPersonanformanteSalud(App.getInstance().getMarco().id ,App.getInstance().getHogar().hogar_id,persona_id,seccionesCargadoInformanteSalud);
		App.getInstance().SetPersonaSeccion01(informantedesalud);
		
		}
    public void txtonQH201_1ChangeValue(){
    	if(txtQH02_1.getText().toString().length()>0){
    		String replace="(NOMBRE)";
    		String nombre=txtQH02_1.getText().toString();
    		String Ud="Usted";
    		String valoralcambiar=bean.qhinfo==1?Ud:nombre;
    		lblparentesco.setText(getResources().getString(R.string.seccion01qh03));
    		lblviveaqui.setText(getResources().getString(R.string.seccion01qh04));
    		lblDurmioAqui.setText(getResources().getString(R.string.seccion01qh05));
    		lblparentesco.setText(lblparentesco.getText().toString().replace(replace,valoralcambiar));
    		lblviveaqui.setText(lblviveaqui.getText().toString().replace(replace,valoralcambiar));
    		lblDurmioAqui.setText(lblDurmioAqui.getText().toString().replace(replace, valoralcambiar));
    		lblhombreomujer.setText(getResources().getString(R.string.seccion01qh06));
    		lblhombreomujer.setText(lblhombreomujer.getText().toString().replace(replace, valoralcambiar));
    		lblcumpleanios.setText(getResources().getString(R.string.seccion01qh07A));
    		lblcumpleanios.setText(lblcumpleanios.getText().toString().replace(replace, valoralcambiar));
    		Spanned texto11 = Html.fromHtml("11. ¿(NOMBRE) está afiliado(a) o inscrito(a) en: ESSALUD, Seguro Integral de Salud o en algún otro seguro de salud?");
    		lblafiliado.setText(texto11);
    		lblafiliado.setText(lblafiliado.getText().toString().replace(replace,valoralcambiar));
    	 	Spanned textoafiliado=Html.fromHtml(lblafiliado.getText()+"<br><br> <b>SI</b>: ¿En cuál? <br> ¿En algún otro seguro de salud?");
        	lblafiliado.setText(textoafiliado);
       		lbltitular.setText(lbltitular.getText().toString().replace(replace,valoralcambiar));
       		lbltitular.setText(getResources().getString(R.string.seccion01qh12));
       		lbltitular.setText(lbltitular.getText().toString().replace(replace, valoralcambiar));
//       		lblpregunta13.setText(getResources().getString(R.string.seccion01qh13));
       		lblpregunta13.setText(lblpregunta13.getText().toString().replace(bean.qh02_1, valoralcambiar));
       		
    		valor = 0;
        	
    		if (bean.qh11_a!=null && !Util.esDiferente(bean.qh11_a, 1)) {
    			reciclado(chbQH11_A);
    		}
    		if (bean.qh11_b!=null && !Util.esDiferente(bean.qh11_b, 1)) {
    			reciclado(chbQH11_B);
    		}
    		if (bean.qh11_c!=null && !Util.esDiferente(bean.qh11_c, 1)) {
    			reciclado(chbQH11_C);
    		}
    		if (bean.qh11_d!=null && !Util.esDiferente(bean.qh11_d, 1)) {
    			reciclado(chbQH11_D);
    		}
    		if (bean.qh11_e!=null && !Util.esDiferente(bean.qh11_e, 1)) {
    			reciclado(chbQH11_E);
    		}
       		
    	}
    }
    public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQH02_1.readOnly();
			txtQH02_2.readOnly();
			txtQH02_3.readOnly();
			rgQH03.readOnly();
			rgQH04.readOnly();
			rgQH05.readOnly();
			rgQH06.readOnly();
			rgQH08.readOnly();
			rgQH12.readOnly();
			rgQH13.readOnly();
			txtQH07.readOnly();
			txtQH7DD.readOnly();
			txtQH7MM.readOnly();
			chbQH07.readOnly();
			chbQH11_A.readOnly();
			chbQH11_B.readOnly();
			chbQH11_C.readOnly();
			chbQH11_D.readOnly();
			chbQH11_E.readOnly();
			chbQH11_Y.readOnly();
			chbQH11_Z.readOnly();
			txtQHOBSPERSONA.setEnabled(false);
			Util.cleanAndLockView(getActivity(), btnNosabedia);
			Util.cleanAndLockView(getActivity(), btnNosabemes);
		}
	}
    
	public Integer combinaPregunta07() {
		Integer p07=null;
		if (txtQH07.getText().toString().trim().length()!=0) {
			p07= Integer.parseInt(txtQH07.getText().toString());
		}
		if (chbQH07.getValue().toString().equals("1")) {
			p07= bean.getConvertqh7(Integer.parseInt(chbQH07.getValue().toString()));
		}
		return p07;         
	}
       
	public void muestraPregunta07(Integer p07) {
		if (p07<=96) {
			txtQH07.setText(p07.toString()); 
			chbQH07.setChecked(false);
		}           
		else {
			if (p07>=97) {          
				chbQH07.setChecked(true);
				txtQH07.setText("");
			}                        
		} 
	}
    public void CambiarNombreLabels(){
		
    	lblparentesco.setText(getResources().getString(R.string.seccion01qh03));
    	lblviveaqui.setText(getResources().getString(R.string.seccion01qh04));
    	lblDurmioAqui.setText(getResources().getString(R.string.seccion01qh05));
    	lblhombreomujer.setText(getResources().getString(R.string.seccion01qh06));
    	lblcumpleanios.setText(getResources().getString(R.string.seccion01qh07A));   	
    	lbltitular.setText(getResources().getString(R.string.seccion01qh12));
    	lblpregunta13.setText(getResources().getString(R.string.seccion01qh13));
    	lblTitulo1.setText(getResources().getString(R.string.seccion01_tituloNroOrden));
    	String valoracambiar="(NOMBRE)";
		String Ud="Usted";
		
//    	Log.e("valoracambiar",""+valoracambiar);
//    	Log.e("bean",""+bean.qhinfo);
    	
		bean.qhinfo=bean.qhinfo==null?0:bean.qhinfo;
		lblparentesco.setText(lblparentesco.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblviveaqui.setText(lblviveaqui.getText().toString().replace(valoracambiar,  bean.qhinfo==1?Ud:bean.qh02_1));
		lblDurmioAqui.setText(lblDurmioAqui.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblhombreomujer.setText(lblhombreomujer.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblcumpleanios.setText(lblcumpleanios.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
    	
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D1",bean.persona_id.toString()));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D2",bean.qh07==null?"":bean.qh07.toString()));
		
		Spanned texto11 = Html.fromHtml("11. ¿(NOMBRE) está afiliado(a) o inscrito(a) en: ESSALUD, Seguro Integral de Salud o en algún otro seguro de salud?");
		lblafiliado.setText(texto11);
		
		lblafiliado.setText(lblafiliado.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud: bean.qh02_1));
    	Spanned textoafiliado=Html.fromHtml(lblafiliado.getText()+"<br><br> <b>SI</b>: ¿En cuál? <br> ¿En algún otro seguro de salud?");
    	lblafiliado.setText(textoafiliado);
   		lbltitular.setText(lbltitular.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
   		
   		
   		Calendar Fecha_Actual = new GregorianCalendar();   		
   		Date date= new Date();
   		if(qh13fechref!=null)
		{			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qh13fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			Fecha_Actual=cal;
			date =date2;
		}
   		
   		int dias=MyUtil.getDayOfTheWeek(date);
   		
   		String primero="";
   		String segundo="";
   		Calendar segunda_fecha;
   		if(dias==Fecha_Actual.SATURDAY && Fecha_Actual.get(Calendar.AM_PM) ==1){
   			primero=Fecha_Actual.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(Fecha_Actual.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(Fecha_Actual, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   			lblnombre.requestFocus();
   		}
   		else{
   			Calendar primera_fecha = MyUtil.PrimeraFecha(Fecha_Actual, dias);
   			primero=primera_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(primera_fecha.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(primera_fecha, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   			lblnombre.requestFocus();
   		}
   		
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F1",segundo));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F2",primero));
   		lblnombre.requestFocus();
	}
    
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH03.getWindowToken(), 0);
	}
	
	public void OcultarTeclatxtQH7DD() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH03.getWindowToken(), 0);
        rgQH08.requestFocus();
	}
	
	public void LimpiarPreguntasporEdad(Integer edad){
		if(edad>=0 && edad<=5){
			bean.qh7dd=null;
			bean.qh7mm=null;
			bean.qh08=null;
			bean.qh13=null;
		}
		else if(edad>=6 && edad<=11){
			bean.qh7dd=null;
			bean.qh7mm=null;
			bean.qh08=null;
		}
		else if(edad>=12 && edad<=14){
			bean.qh7dd=null;
			bean.qh7mm=null;
		}
	}
	
	public void ontxtQH7ChangeValue() {
		if(txtQH07.getText().toString().length()>0){
		lblTitulo1.setText(getResources().getString(R.string.seccion01_tituloNroOrden));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D1", bean.persona_id.toString()));
		lblTitulo1.setText(lblTitulo1.getText().toString().replace("D2",txtQH07.getText().toString()));
		}
    	if((txtQH07.getText().toString().trim().length()>0)){  
    		if (MyUtil.incluyeRango(0,5,txtQH07.getText().toString())) {
//    			Log.e("0a5","0a5");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM,rgQH08,rgQH13);         			
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.GONE);
    			q11.setVisibility(View.GONE);
    			 bloquearchbQH11C();
    			OcultarTecla();
    			chbQH11_A.requestFocus(); 				  
    		}
    		if (MyUtil.incluyeRango(6,11,txtQH07.getText().toString())) {
//    			Log.e("6a11","6a11");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM,rgQH08);         		  		
    			Util.lockView(getActivity(), false,rgQH13);   
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.GONE);  
    			q11.setVisibility(View.VISIBLE);
    			 bloquearchbQH11C();
    			OcultarTecla();
    			chbQH11_A.requestFocus();         		   
    		}
    		if (MyUtil.incluyeRango(12,14,txtQH07.getText().toString())) {
//    			Log.e("12a14","12a14");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM);
    			Util.lockView(getActivity(), false,rgQH08,rgQH13);
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.VISIBLE); 
    			q11.setVisibility(View.VISIBLE);
    			 bloquearchbQH11C();
    			rgQH08.requestFocus();    			
    			OcultarTecla();
    			onrgQH08ChangeValue();
    		}
    		if (MyUtil.incluyeRango(15,96,txtQH07.getText().toString())) {
//    			Log.e("15a96","15a96");
    			Util.cleanAndLockView(getActivity(),chbQH07);
    			Util.lockView(getActivity(), false,txtQH7DD,txtQH7MM,rgQH08,rgQH13);
    			q7.setVisibility(View.VISIBLE);
    			q8.setVisibility(View.VISIBLE);  
    			q11.setVisibility(View.VISIBLE);
    			 bloquearchbQH11C();
    			txtQH7DD.requestFocus();
    			
    			gridPregunta07A.requestFocus();       			
    			OcultarTecla();
    			onrgQH08ChangeValue();
    		}			
    	}
    	else{
//    		Log.e("else del null","97 check");
    		Util.lockView(getActivity(), false,chbQH07,txtQH7DD,txtQH7MM,rgQH08,rgQH13);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);  
    		q11.setVisibility(View.VISIBLE);
    		 bloquearchbQH11C();
    		chbQH07.requestFocus();
    		OcultarTecla();
    		onrgQH08ChangeValue();
    	}
    }	

              
    public void onchbQH7ChangeValue() {
    	if(chbQH07.isChecked()){
//    		Log.e("s","akinomass");
    		Util.cleanAndLockView(getActivity(),txtQH07);    		
    		txtQH7DD.requestFocus();				
    	} 
    	else{
//    		Log.e("s","ffff");
    		Util.lockView(getActivity(), false,txtQH07);
    		txtQH07.requestFocus();	    		   
    	}	   	   
    }       

    public void onQH03Block(){
    	rgQH03.lockButtons(false,0,1,2,3,4,5,6,7,8,9,10,11);
    	rgQH04.lockButtons(false,0,1);
    	if(!Util.esDiferente(bean.persona_id,App.JEFE))	{
    		rgQH03.lockButtons(true,1,2,3,4,5,6,7,8,9,10,11);
	   		rgQH04.lockButtons(true,1);
    	}
	   	else{
//	   		rgQH03.lockButtons(false,0,1,2,3,4,5,6,7,8,9,10,11);
	   		/****** ESPOSO ****/
	   		Integer esposo=null;
	   		if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.ESPOSA),bean.persona_id)){
//   	    		rgQH03.lockButtons(true, 1);
	   			esposo=1;
   	    	}
//   	    	else{
//   	    		rgQH03.lockButtons(false, 1);
//   	    	}
	   		Integer padres=null;
	   		/***PADRES***/
	   		if(getServiceSeccion01().getExisteSoloPadres(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.PADRE),bean.persona_id)){
//	   	    		rgQH03.lockButtons(true, 5);
	   			padres=5;
	   	    	}
//	   	    	else{
//	   	    		rgQH03.lockButtons(false, 5);
//	   	    	}
	   		/***SUEGROSS***/
	   		Integer suegros=null;
	   		if(getServiceSeccion01().getExisteSoloSuegros(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.SUEGRO),bean.persona_id)){
//   	    		rgQH03.lockButtons(true, 6);
	   			suegros=6;
   	    	}
//   	    	else{
//   	    		rgQH03.lockButtons(false, 6);
//   	    	}
	   		
	   		rgQH03.lockButtons(true, 0,esposo==null?0:esposo,padres==null?0:padres,suegros==null?0:suegros);
	   		rgQH04.lockButtons(false,1);
		  	}   		
    }
    
    public void onrgQH08ChangeValue(){
    	rgQH08.lockButtons(false, 0,1,2,3,4,5);
    	Seccion01 seccion1=getServiceSeccion01().getPersona2(bean.id, bean.hogar_id,App.JEFE);      
    	if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) && !Util.esDiferente(bean.persona_id, App.JEFE)){
    		rgQH08.lockButtons(true, 2,3,4,5);
    		lblnombre.requestFocus();
    	}
    	if(seccion1.qh08!=null){
    		Integer block=seccion1.qh08==App.CONIVIENTE?1:0;
    		if((!Util.esDiferente(seccion1.qh08,App.CONIVIENTE) 
    				|| !Util.esDiferente(seccion1.qh08,App.CASADO))
    				&& getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) 
    				&& !Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.ESPOSA),bean.persona_id)) {
    			rgQH08.lockButtons(true,block,2,3,4,5);
    			lblnombre.requestFocus();
    		}
    	}	      
    }
      
    public boolean verificarCheck() {
  		if (chbQH11_A.isChecked() || chbQH11_B.isChecked() || chbQH11_C.isChecked() ||chbQH11_D.isChecked() || chbQH11_E.isChecked() ) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
  	public boolean verificarCheck1() {
  		if (chbQH11_Y.isChecked() || chbQH11_Z.isChecked() ) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
  	
  	public void bloquearchbQH11C() {
  		if (MyUtil.incluyeRango(0,17,txtQH07.getText().toString()) && !chbQH11_C.isChecked() && !chbQH11_E.isChecked()) {			
  			rgQH12.lockButtons(true,0);	
  		}
  		else{
  			rgQH12.lockButtons(false,0);
  		}
  	}
  	
  	public void conteos(){

  		
		valor += 1;
  		
  		//ToastMessage.msgBox(this.getActivity(), valor.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
  	}
  	
  	public void conteosMenos(){

  		
  		valor -= 1;
  		//ToastMessage.msgBox(this.getActivity(), valor.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
  	}
  	
  	public void cambiarTextoPreg12(){
  		String replace="(del seguro de salud indicado)";
  		String replace2="(de alguno de los seguros de salud indicados)";
		String valoralcambiar="";
		
		String replaceNombre="(NOMBRE)";
		String nombre=txtQH02_1.getText().toString();
		String Ud="Usted";
		String valoralcambiarNombre=bean.qhinfo==1?Ud:nombre;

		
  		if(valor > 1){
       		lbltitular.setText(getResources().getString(R.string.seccion01qh12));
       		lbltitular.setText(lbltitular.getText().toString().replace(replace, valoralcambiar));
       		lbltitular.setText(lbltitular.getText().toString().replace(replaceNombre, valoralcambiarNombre));
		}else{
  			//lbltitular.setText(lbltitular.getText().toString().replace(replace2,valoralcambiar));
       		lbltitular.setText(getResources().getString(R.string.seccion01qh12));
       		lbltitular.setText(lbltitular.getText().toString().replace(replace2, valoralcambiar));
       		lbltitular.setText(lbltitular.getText().toString().replace(replaceNombre, valoralcambiarNombre));
		}
  	}
  	
  	public void reciclado(CheckBoxField dev){
  		if(dev.isChecked()){
  			conteos();
  			cambiarTextoPreg12();
  			
  		}else{
  			if(valor == 0){
  			}else{
  				conteosMenos();
  				cambiarTextoPreg12();
  			}
  			
  		}
  	}
  	
  	
  	public void onchbQH11_AChangeValue() {	
  		
  		reciclado(chbQH11_A);

  		if (verificarCheck()) {	 
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);
  			    	
  			  			
  			bloquearchbQH11C();
  		
  		} else {		
//  			lbltitular.setText(getResources().getString(R.string.seccion01qh12));
//  			lbltitular.setText(lbltitular.getText().toString().replace(replace,valoralcambiar2));
//  			
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_A.requestFocus();			
  		}		
  	}
  	public void onchbQH11_BChangeValue() {	

		
  		reciclado(chbQH11_B);
  		
  		
  		if (verificarCheck()) {	
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);
  			bloquearchbQH11C();
  		} else {		
			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
			chbQH11_B.requestFocus();				
  					
  		}		
  	}
  	public void onchbQH11_CChangeValue() {	
  		reciclado(chbQH11_C);
  		if (verificarCheck()) {	
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);	
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_C.requestFocus();			
  		}		
  	}
  	public void onchbQH11_DChangeValue() {	
  		reciclado(chbQH11_D);
  		if (verificarCheck()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);		
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_D.requestFocus();			
  		}		
  	}
  	public void onchbQH11_EChangeValue() {	
  		reciclado(chbQH11_E);
  		if (verificarCheck()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);		
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_E.requestFocus();			
  		}		
  	}
  	public void onchbQH11_YChangeValue() {		
  		if (verificarCheck1()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Z,rgQH12);
  			q10.setVisibility(View.GONE);
  			
  		} else {		
  			Util.lockView(getActivity(), false,chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Z,rgQH12);
  			q10.setVisibility(View.VISIBLE);
  			chbQH11_Y.requestFocus();			
  		}		
  	}
  	public void onchbQH11_ZChangeValue() {		
  		if (verificarCheck1()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Y,rgQH12);
  			q10.setVisibility(View.GONE);
  			
  		} else {	
  			Util.lockView(getActivity(), false,chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Y,rgQH12);
  			q10.setVisibility(View.VISIBLE);
  			chbQH11_Z.requestFocus();			
  		}		
  	}
  	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}	
	private Seccion01Service getServiceSeccion01() {
		if (seccion01Service == null) {
			seccion01Service = Seccion01Service.getInstance(getActivity());
		}
		return seccion01Service;
	}  
	public Seccion03Service getSeccion03Service() { 
		if(seccion03service==null){ 
			seccion03service = Seccion03Service.getInstance(getActivity()); 
		} 
		return seccion03service; 
    }
	public VisitaService getVisitaService()
	{
		if(visitaService==null)
		{
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}
	public void EliminarPersonaDelaSeccion8SaludAlModificarEdad(){
//		SeccionCapitulo[] seccionesCargadoseccion8 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"ID", "HOGAR_ID", "PERSONA_ID","PERSONA_ID_NINIO") };
//		CSSECCION_08 ninio= getCuestionarioService().getPersonaSeccion8Salud(bean.id,bean.hogar_id,bean.persona_id, seccionesCargadoseccion8);
//		boolean flag = false;
//		if(ninio!=null && bean.qh07>11){
//		flag= getCuestionarioService().BorrarPersona(ninio.id, ninio.hogar_id,ninio.persona_id_ninio);
//		}
		SeccionCapitulo[] seccionesCargadoseccion3 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"ID", "HOGAR_ID", "PERSONA_ID", "PERSONA_ID_ORDEN","PREGUNTA_ID") };
		List<Seccion03> beneficiados= getCuestionarioService().getPersonaSeccion03(bean.id,bean.hogar_id,bean.persona_id, seccionesCargadoseccion3);
		boolean flag2 = false,eliminar=false;
		for(int i=0;i<beneficiados.size();i++){
			eliminar = false;
			switch(beneficiados.get(i).pregunta_id){
				case App.BECA18: 
					if(!MyUtil.incluyeRango(App.EDADMINIMABECA18, App.EDADMAXIMABECA18, bean.qh07)){
						eliminar = true;
					}
								break;
				case App.TRABAJAPERU: 
					 if(!MyUtil.incluyeRango(App.EDADMINIMATPERU, App.EDADMAXIMATPERU, bean.qh07)){
						 eliminar = true; 
					 }
								break;
				case App.JUNTOS: 
					if(!MyUtil.incluyeRango(App.EDADMINIMAJUNTOS, App.EDADMAXIMAJUNTOS, bean.qh07)){
						eliminar = true;
					}
								break;
				case App.PENSION65:
					if(!MyUtil.incluyeRango(App.EDADMINIMAPENSION65, App.EDADMAXIMAPENSION65, bean.qh07) || MyUtil.incluyeRango(8, 8, bean.qh13) || !MyUtil.incluyeRango(1, 1, bean.qh04)){
						eliminar = true;
					}
								break;
				case App.VASOLECHE: 
					if(!MyUtil.incluyeRango(App.EDADMINIMAVASOLECHE, App.EDADMAXIMAVASOLECHE, bean.qh07)){
						eliminar = true;
					}
								break;
				case App.COMEDORPOP: 
					if(!MyUtil.incluyeRango(App.EDADMINIMACOMEDORPOP, App.EDADMAXIMACOMEDORPOP, bean.qh07)){
						eliminar = true;
					}
								break;
				case App.WAWAMAS: 
//					if(!MyUtil.incluyeRango(App.EDADMINIMAWAWAMAS, App.EDADMAXIMAWAWAMAS, bean.qh07) || !MyUtil.incluyeRango(1, 1, bean.qh04)){
//						eliminar = true;
//					}
								break;
				case App.QALIWARMA: 
					if(!MyUtil.incluyeRango(App.EDADMINIMAQALIWARMA, App.EDADMAXIMAQALIWARMA, bean.qh07)){
						eliminar = true;
					}
								break;
			}
			if(eliminar){
				try {
		        	boolean borrado=false;
		            borrado =getSeccion03Service().BorrarPersonaPregunta(beneficiados.get(i));
		            if (!borrado) {
		                    throw new SQLException("Persona no pudo ser borrada.");
		            }
		        } catch (SQLException e) {                      
		                e.printStackTrace();
		                ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
		                                ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		        }
			}			
		}
		
		SeccionCapitulo[] seccionesGrabadoHogar,seccionesCargadoHogar;
		
		seccionesCargadoHogar = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"ID","HOGAR_ID","PERSONA_INFORMANTE_ID")}; 
		
		Hogar hogar = getCuestionarioService().getHogarByInformante(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id , seccionesCargadoHogar);
    	if(hogar==null){ 
		  hogar=new Hogar(); 
		  hogar.id=App.getInstance().getHogar().id; 
		  hogar.hogar_id=App.getInstance().getHogar().hogar_id; 
	    }
		
		List<Seccion01> PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMABECA18,App.EDADMAXIMABECA18,App.BECA18);
 	   	if(PosibleBeneficiario.size()==0){
 	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH91") };
	 	   	try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH91 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
 		}
 	   	
 	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMATPERU,App.EDADMAXIMATPERU,App.TRABAJAPERU);
	   	if(PosibleBeneficiario.size()==0){
	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH93") };
	   		try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH93 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	   	
	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAJUNTOS,App.EDADMAXIMAJUNTOS,App.JUNTOS);
 	   	if(PosibleBeneficiario.size()==0){
 	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH95") };
	 	   	try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH95 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
 		}
 	   	
 	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAPENSION65,App.EDADMAXIMAPENSION65,App.PENSION65);
	   	if(PosibleBeneficiario.size()==0){
	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH99") };
	   		try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH99 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	   	
	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAVASOLECHE,App.EDADMAXIMAVASOLECHE,App.VASOLECHE);
	   	if(PosibleBeneficiario.size()==0){
	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH101") };
	   		try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH101 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	   	
	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMACOMEDORPOP,App.EDADMAXIMACOMEDORPOP,App.COMEDORPOP);
	   	if(PosibleBeneficiario.size()==0){
	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH103") };
	   		try {
		 	   	boolean borrado=false;
		        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
		        if (!borrado) {
		                throw new SQLException("P.QH103 no pudo ser borrada.");
		        }
			} catch (SQLException e) { 
				 e.printStackTrace();
	             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
	                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			}
		}
	   	
	   	PosibleBeneficiario = getServiceSeccion01().getPosiblesBeneficiados(App.getInstance().getHogar().id,App.getInstance().getHogar().hogar_id,-1,App.EDADMINIMAWAWAMAS,App.EDADMAXIMAWAWAMAS,App.WAWAMAS);
 	   	if(PosibleBeneficiario.size()==0){
 	   		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH106") };
 	   	try {
	 	   	boolean borrado=false;
	        borrado =getCuestionarioService().saveOrUpdate(hogar,seccionesGrabadoHogar);
	        if (!borrado) {
	                throw new SQLException("P.QH106 no pudo ser borrada.");
	        }
		} catch (SQLException e) { 
			 e.printStackTrace();
             ToastMessage.msgBox(getActivity(), "ERROR: "+ e.getMessage(), 
                             ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
 		}		
	}
	
	public void InformanteSalud_Anterior(){
		SeccionCapitulo[] seccionesCargadoV = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QHVDIA_INI", "QHVMES_INI", "QHVANIO_INI", "QHHORA_INI","QHMIN_INI", "QHVRESUL", "QHVHORA_FIN", "QHVMIN_FIN","QHVDIAP", "QHVMESP", "QHVHORAP", "QHVMINUP", "NRO_VISITA","ID", "HOGAR_ID","QHVRESUL_O") };
		Integer cantidad = getServiceSeccion01().TotalDePersonas(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		visitas = getCuestionarioService().getVisitas(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoV);
		
		if(cantidad>0 && visitas.size()>0){
			info_salud_ant= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,getVisitaService(),getServiceSeccion01());
//			Log.e("Id Salud Anterior: ", ""+info_salud_ant);
			
		}
	}
	
	public void ActualizarInformanteSalud(){
		SeccionCapitulo[] seccionesCargadoV = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QHVDIA_INI", "QHVMES_INI", "QHVANIO_INI", "QHHORA_INI","QHMIN_INI", "QHVRESUL", "QHVHORA_FIN", "QHVMIN_FIN","QHVDIAP", "QHVMESP", "QHVHORAP", "QHVMINUP", "NRO_VISITA","ID", "HOGAR_ID","QHVRESUL_O") };
		Integer cantidad = getServiceSeccion01().TotalDePersonas(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id);
		visitas = getCuestionarioService().getVisitas(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoV);
		
		if(cantidad>0 && visitas.size()>0){
			Integer persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,getVisitaService(),getServiceSeccion01());
//			Log.e("Id Salud: ", ""+persona_id);
			if(!info_salud_ant.equals(persona_id) && info_salud_ant>0){
				getCuestionarioService().ModificarNumerodeOrdenSalud01_03(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, persona_id, info_salud_ant);
				getCuestionarioService().ModificarNumerodeOrdenSalud04_07(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, persona_id, info_salud_ant);
//				getCuestionarioService().ModificarNumerodeOrdenVisitaSalud(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, persona_id, info_salud_ant);
				getCuestionarioService().ModificarNumerodeOrdenVisitaSalud2(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, persona_id, visitas.size());
				App.miembrosdelhogar = getServiceSeccion01().getPersona(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, persona_id);
			}
			persona_id= MyUtil.SeleccionarInformantedeSalud(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,getVisitaService(),getServiceSeccion01());
//			Log.e("Nuevo Id Salud: ", ""+persona_id);			
		}
	}
	public Seccion04_05Service getSeccion04_05Service(){
	    	if(Persona04_05Service == null){
	    		Persona04_05Service = Seccion04_05Service.getInstance(getActivity());
	    	}
	    	return Persona04_05Service;
	}
	  public Seccion01Service getPersonaService(){
	    	if(Personaservice == null){
	    		Personaservice = Seccion01Service.getInstance(getActivity());
	    	}
	    	return Personaservice;
	    }
	public void onQH06ChangeValue(){
		Integer valor = Integer.parseInt(rgQH06.getTagSelected("0").toString());
		if(bean!=null && bean.qh06!=null && bean.qh06==2 && valor==1  && bean.qh07!=null && bean.qh07>14 && bean.qh07<50){
			action = ACTION.ELIMINAR;
			dialog = new DialogComponent(getActivity(), this,TIPO_DIALOGO.YES_NO, getResources().getString(R.string.app_name),"¿Esta seguro  de cambiar el sexo se eliminará el C. individual?");
			dialog.put("seleccion", bean);
			dialog.showDialog();
		}
	}
	@Override
	public void onCancel() {
		rgQH06.setTagIndexSelected(1);
	}

	@Override
	public void onAccept() {
		if (action == ACTION.MENSAJE) {
			return;
		}
		if (dialog == null) {
			return;
		}
		Seccion01 bean = (Seccion01) dialog.get("seleccion");
		try {
			boolean borrado = false;
			borrado = getPersonaService().borrarPersonaCuandoCambiaSexo(bean);			
			if (!borrado) {
				throw new SQLException("Persona no pudo ser borrada.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			ToastMessage.msgBox(getActivity(), "ERROR: " + e.getMessage(),
					ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
		}
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		uiToEntity(bean);
		bean.qh07=combinaPregunta07();
		if (bean.qh12!=null) {
			bean.qh12=bean.getConvertqh12(bean.qh12);
		}
		if (bean.qh13!=null) {
			bean.qh13=bean.getConvertqh13(bean.qh13);
		}    		
		if(bean.qh7dd!=null){
			bean.qh7dd=txtQH7DD.getText().toString().length()>1?txtQH7DD.getText().toString():"0"+txtQH7DD.getText().toString();
			bean.qh7mm=txtQH7MM.getText().toString().length()>1?txtQH7MM.getText().toString():"0"+txtQH7MM.getText().toString();
		}	
		boolean flag=false;
		try {       
			flag = getCuestionarioService().saveOrUpdate(bean,seccionesGrabadoParcial); 
			if (!flag) {
				throw new Exception("Ocurrió un problema al grabar los datos de la Persona.");
			}

		} 
		catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		}
		return App.HOGAR;
	}
} 
