package gob.inei.endes2024.fragment.seccion01;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion01.Dialog.Seccion01_001_4Dialog;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.Seccion01Service;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion01Fragment_005 extends FragmentForm implements Respondible {
//	@FieldAnnotation(orderIndex = 1)
//	public TextAreaField txtQH_OBS_SECCION01;
	
	public List<Seccion01> detalles;
	Hogar bean;
	private CuestionarioService cuestionarioService;
	private LabelComponent lblTitulo, lblintro;
	public TableComponent tcPersonas;
	Seccion01ClickListener adapter;
	SeccionCapitulo[] seccionesGrabado;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesCargadoHogar;
	SeccionCapitulo[] seccionesGrabadoHogar;
	private HogarService hogarService;
	private Seccion01Service personaService;
	public Integer contador=0;
	int dato=-17;
	private enum ACTION {
		ELIMINAR, MENSAJE
	}

	private ACTION action;

	public Seccion01Fragment_005() {
	}

	public Seccion01Fragment_005 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		adapter = new Seccion01ClickListener();
		tcPersonas.getListView().setOnItemClickListener(adapter);
		enlazarCajas();
		listening();
		seccionesCargadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "ID", "HOGAR_ID", "PERSONA_INFORMANTE_ID","QH_OBS_SECCION01","QH110H","QH93")};
		seccionesGrabadoHogar = new SeccionCapitulo[] { new SeccionCapitulo(-1,-1, -1, "QH_OBS_SECCION01") };
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH01", "QH02_1", "QH02_2","QH02_3", "QH07", "estado") };
		return rootView;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(this.getActivity(), App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01_titulo2).textSize(21).centrar().negrita();
		Spanned textointro = Html.fromHtml("A continuaci&oacute;n le har&eacute algunas preguntas para saber si en su hogar vive alguna persona con limitaci&oacute;n o dificultad <b>PERMANENTE</b>, que le impida o dificulte desarrollarse normalmente en sus actividades diarias.");
		lblintro = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblintro.setText(textointro);
//		txtQH_OBS_SECCION01 = new TextAreaField(getActivity()).maxLength(500).size(180, 650).alfanumerico();
//		txtQH_OBS_SECCION01.setCallback("OcultarTecla");
		
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(580, 650).headerHeight(altoComponente + 10).dataColumHeight(50);
//		}
//		else{
			tcPersonas = new TableComponent(getActivity(), this, App.ESTILO).size(680, 650).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcPersonas.addHeader(R.string.seccion01_nro_orden, 0.4f,TableComponent.ALIGN.CENTER);
		tcPersonas.addHeader(R.string.seccion01nombres, 1.3f,TableComponent.ALIGN.LEFT);
	}

	@Override
	protected View createUI() {
		buildFields();

		LinearLayout q0 = createQuestionSection(lblTitulo);
		LinearLayout q1 = createQuestionSection(lblintro);
		LinearLayout q2 = createQuestionSection(tcPersonas.getTableView());
//		LinearLayout q3 = createQuestionSection(R.string.seccion01qh_obs_1,txtQH_OBS_SECCION01);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
//		form.addView(q3);
		return contenedor;
	}

	@Override
	public boolean grabar() {
		uiToEntity(bean);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = false;

		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getService().saveOrUpdate(bean, dbTX, seccionesGrabadoHogar);
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos del hogar.");
			}
			getService().commitTX(dbTX);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_ERROR, ToastMessage.DURATION_LONG);
			return false;
		} finally {
			getService().endTX(dbTX);
		}
		if(App.getInstance().getHogar()!=null){
			App.getInstance().getHogar().qh110h=bean.qh110h;
		}
		return flag;
	}

	private HogarService getService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(tcPersonas.getWindowToken(), 0);
	}

	@Override
	public void cargarDatos() {
		MyUtil.LiberarMemoria();
		bean = getCuestionarioService().getHogar(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargadoHogar);

		detalles = getCuestionarioService().getSeccion01ListDiscapacidad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		if (bean == null) {
			bean = new Hogar();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id = App.getInstance().getHogar().hogar_id;
			detalles = new ArrayList<Seccion01>();
		}
		entityToUI(bean);
		cargarTabla();
		inicio();
	}

	private boolean validar() {

		if (!getPersonaService().TodosLosMiembrosDelHogarDiscapacidadCompletada(bean.id,bean.hogar_id)) {
			mensaje = "Debe completar las preguntas para todo los miembros del hogar antes de pasar a la seccion II";
			view = tcPersonas;
			error = true;
			return false;
		}
		return true;
	}

	public void refrescarPersonas(Seccion01 personas) {
		if (detalles.contains(personas)) {
			cargarTabla();
			return;
		} else {
			detalles.add(personas);
			cargarTabla();
		}
	}

	public void cargarTabla() {
		detalles = getCuestionarioService().getSeccion01ListDiscapacidad(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id, seccionesCargado);
		tcPersonas.setData(detalles, "qh01", "getNombre");
		tcPersonas.setBorder("estado");
		tcPersonas.setEnabled(true);
		registerForContextMenu(tcPersonas.getListView());
	}

	public class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
//			arg0.setEnabled(false);
//			arg1.setEnabled(false);
			Seccion01 c = (Seccion01) detalles.get(arg2);
			Integer posicionpersona = -1;
			Integer estado = -1;
			for (int i = 0; i < detalles.size(); i++) {
				if (c.persona_id == detalles.get(i).persona_id) {
					posicionpersona = i;
					break;
				}
			}
			posicionpersona = posicionpersona == 0 ? 0 : posicionpersona - 1;
			for (int i = 0; i < detalles.size(); i++) {
				if (posicionpersona == i) {
					estado = detalles.get(i).estado;
					break;
				}
			}
			if (c.persona_id == App.JEFE && contador==0) {
				abrirDetalle(c, arg2, (List<Seccion01>) detalles);
				contador++;
//				arg0.setEnabled(true);
			} else {
				if (!Util.esDiferente(estado, 0) && contador==0) {
					abrirDetalle(c, arg2, (List<Seccion01>) detalles);
					contador++;
				}
			}
		}
	}

	private void inicio() {
		ValidarsiesSupervisora();
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//			txtQH_OBS_SECCION01.setEnabled(false);		
		}
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(getActivity());
		}
		return personaService;
	}

	public void abrirDetalle(Seccion01 tmp, int index, List<Seccion01> detalles) {
//		if(Util.esDiferente(dato, index,index)){
//		dato=index;
		
		FragmentManager fm = Seccion01Fragment_005.this.getFragmentManager();
		Seccion01_001_4Dialog aperturaDialog = Seccion01_001_4Dialog.newInstance(Seccion01Fragment_005.this, tmp, index, detalles);
		if(aperturaDialog.getShowsDialog()){
			aperturaDialog.show(fm, "aperturaDialog");
			}
//		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		Log.e("AQUIII ","EEEE");
		
	}
}