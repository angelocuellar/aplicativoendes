package gob.inei.endes2024.fragment.seccion01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_001;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion01_001_3Dialog extends DialogFragmentComponent {
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQH22; 
	@FieldAnnotation(orderIndex=2) 
	public SpinnerField spnQH23; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQH24; 
	@FieldAnnotation(orderIndex=4) 
	public SpinnerField spnQH25; 
	@FieldAnnotation(orderIndex=5)	
	public ButtonComponent btnAceptar; 
//	@FieldAnnotation(orderIndex=6)
//	public TextAreaField txtQH_OBSERVACION; 
	Seccion01 bean; 
	private static Seccion01Fragment_001 caller;
	public ButtonComponent  btnCancelar;
	private SeccionCapitulo[] seccionesGrabado;
	private SeccionCapitulo[] seccionesCargado; 
	private CuestionarioService cuestionarioService;
	private Seccion01Service serviceSeccion01;
	private CuestionarioService hogarService;
	public List<Seccion01> listaMadres;
	public LabelComponent lblmadreviva,lblmadreresideaqui,lblpadrevivo,lblpadreresideaqui;
	
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	
	public static Seccion01_001_3Dialog newInstance(FragmentForm pagina, Seccion01 detalle) {
		caller = (Seccion01Fragment_001) pagina;
		Seccion01_001_3Dialog f = new Seccion01_001_3Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion01) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getDialog().setTitle("N� de Orden:  " + bean.persona_id + "Edad: "+bean.qh07);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	public Seccion01_001_3Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH22","QH23","QH24","QH25","ID","HOGAR_ID","PERSONA_ID","QH07","QH02_1","QH03","QHINFO")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QH22","QH23","QH24","QH25","QH_OBSERVACION")}; 
	}
		
	@Override
	protected void buildFields() {
		
		rgQH22=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh22_1,R.string.seccion01qh22_2,R.string.seccion01qh22_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP22ChangeValue");
		spnQH23=new SpinnerField(getActivity()).size(altoComponente+15, 400);
				
		rgQH24=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh24_1,R.string.seccion01qh24_2,R.string.seccion01qh24_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onP24ChangeValue");
		spnQH25=new SpinnerField(getActivity()).size(altoComponente+15, 400);
	
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		lblmadreviva = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh22);
		Spanned texto23 = Html.fromHtml("23. �Reside aqu� la madre natural de (NOMBRE)?");
		lblmadreresideaqui = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT);
		lblmadreresideaqui.setText(texto23);
		lblpadrevivo = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh24);
		Spanned texto25 = Html.fromHtml("25. �Reside aqu� el padre natural de (NOMBRE)?");
		lblpadreresideaqui = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT);
		lblpadreresideaqui.setText(texto25);
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				Seccion01_001_3Dialog.this.dismiss();
			}
		});
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarPersonas(bean);
				Seccion01_001_3Dialog.this.dismiss();				
			}
		});	
	}

      	
	@Override
	protected View createUI() {
		buildFields();

		 q1 = createQuestionSection(lblmadreviva,rgQH22); 
		 q2 = createQuestionSection(lblmadreresideaqui,spnQH23); 
		 q3 = createQuestionSection(lblpadrevivo,rgQH24); 
		 q4 = createQuestionSection(lblpadreresideaqui,spnQH25); 

		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(botones);
		return contenedor;
	}
	
	public boolean grabar()
	{
		uiToEntity(bean);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,	ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);
		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),	ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	
	public boolean validar()
	{
		String preguntaVacia = this.getResources().getString(
		R.string.pregunta_no_vacia);
		

		if (MyUtil.incluyeRango(0,14, bean.qh07)) {
			if (Util.esVacio(bean.qh22)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.22"); 
				view = rgQH22; 
				error = true; 
				return false; 
			} 
			if (Util.esDiferente(bean.qh22,2,3)) {
				if (Util.esVacio(bean.qh23)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.23"); 
					view = spnQH23; 
					error = true; 
					return false; 
				} 
			}

			if (Util.esVacio(bean.qh24)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.24"); 
				view = rgQH24; 
				error = true; 
				return false; 
			} 
			if (Util.esDiferente(bean.qh24, 2,3)) {
				if (Util.esVacio(bean.qh25)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.25"); 
					view = spnQH25; 
					error = true; 
					return false; 
				}
			}
		} 

		return true;
	}
	private void cargarDatos() {
		if (bean != null) {
		bean = getCuestionarioService().getSeccion01(bean.id,bean.hogar_id,bean.persona_id,seccionesCargado);
		if (!Util.esDiferente(bean.qh03,App.HIJO_A)) {
				if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.MUJER)) {
					MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,App.JEFE,bean.qh03,bean.qh07);
					MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
				}
				else{
					MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,App.JEFE,bean.qh03,bean.qh07);
					MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
				}
			}
		else
		{
			MyUtil.llenarPadres(this.getActivity() , getServiceSeccion01() , spnQH25 , bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
			MyUtil.llenarMadres(this.getActivity() , getServiceSeccion01(), spnQH23, bean.id, bean.hogar_id,bean.persona_id,-1,bean.qh03,bean.qh07);
		}
	}
		
		if (bean == null) {
			bean = new Seccion01();
			bean.id = App.getInstance().getHogar().id;
			bean.hogar_id =App.getInstance().getHogar().hogar_id;
			bean.persona_id = caller.detalles.size()+1;
			bean.qh01 = caller.detalles.size()+1;
			}
		entityToUI(bean);
		caretaker.addMemento("antes", bean.saveToMemento(Seccion01.class));
		inicio();
	}
	
	
	public void saltoqh07txt(Integer edad) {
			if (!MyUtil.incluyeRango(0,14, edad)) {
				Util.cleanAndLockView(getActivity(),rgQH22,spnQH23,rgQH24,spnQH25);	
				btnAceptar.requestFocus();
			}
			else{
				Util.lockView(getActivity(), false,rgQH22,spnQH23,rgQH24,spnQH25);	
				rgQH22.requestFocus();
			}	
	}
	
	public void existeMadreJefa(Integer p3) {	
		if (MyUtil.incluyeRango(0,14,bean.qh07)) {
			if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.MUJER) && !Util.esDiferente(p3,App.HIJO_A) ) {
				
				rgQH22.lockButtons(true,1,2);
			}
			else{
				
				rgQH22.lockButtons(false,1,2);	
			}
			if (getServiceSeccion01().getHijoJefa(bean.id, bean.hogar_id,App.HOMBRE) && !Util.esDiferente(p3,App.HIJO_A) ) {
				
				rgQH24.lockButtons(true,1,2);
			}
			else{
				
				rgQH24.lockButtons(false,1,2);		
			}
		}
	}

	private void inicio() {
		saltoqh07txt(bean.qh07);
		existeMadreJefa(bean.qh03);
		onP22ChangeValue();
		onP24ChangeValue();
		rgQH22.requestFocus();
		RenombrarLabels();
	}
	
	private void RenombrarLabels()
	{String replace="(NOMBRE)";
		String Ud="Usted";
		bean.qhinfo=bean.qhinfo==null?0:bean.qhinfo;
		lblmadreviva.setText(lblmadreviva.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblmadreresideaqui.setText(lblmadreresideaqui.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		Spanned texto23 = Html.fromHtml(lblmadreresideaqui.getText()+" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblmadreresideaqui.setText(" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblmadreresideaqui.setText(texto23);
		lblpadrevivo.setText(lblpadrevivo.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		lblpadreresideaqui.setText(lblpadreresideaqui.getText().toString().replace(replace,bean.qhinfo==1?Ud:bean.qh02_1));
		Spanned texto25 = Html.fromHtml(lblpadreresideaqui.getText() +" <br><br><b>SI:</b>�Cu�l es su nombre?");
		lblpadreresideaqui.setText(texto25);
	}

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
	
	public void onP22ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQH22.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),spnQH23);
			q2.setVisibility(View.GONE);
			rgQH24.requestFocus();			
		} else {
			Util.lockView(getActivity(), false,spnQH23);
			q2.setVisibility(View.VISIBLE);
			spnQH23.requestFocus();			
		}
	}
	
	public void onP24ChangeValue() {
		if (MyUtil.incluyeRango(2,3,rgQH24.getTagSelected("").toString())) {
			Util.cleanAndLockView(getActivity(),spnQH25);
			q4.setVisibility(View.GONE);
			btnAceptar.requestFocus();
			
		} else {
			Util.lockView(getActivity(), false,spnQH25);
			q4.setVisibility(View.VISIBLE);
			spnQH25.requestFocus();			
		}
	}
}
