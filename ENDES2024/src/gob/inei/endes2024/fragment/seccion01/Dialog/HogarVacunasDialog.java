package gob.inei.endes2024.fragment.seccion01.Dialog;
//package gob.inei.endes2024.fragment.seccion01.Dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.DateTimeField;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_03;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import gob.inei.endes2024.service.UbigeoService;
//import gob.inei.endes2024.service.VisitaService;
//
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import android.content.Context;
//import android.database.CursorJoiner;
//import android.database.sqlite.SQLiteDatabase;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class HogarVacunasDialog extends DialogFragmentComponent  {
//	
//	@FieldAnnotation(orderIndex=1) 
//	public SpinnerField spnQH240; 
//	@FieldAnnotation(orderIndex=2) 
//	public RadioGroupOtherField rgQH241;
//	@FieldAnnotation(orderIndex=3) 
//	public RadioGroupOtherField rgQH242;
//	
//	@FieldAnnotation(orderIndex=4) 
//	public CheckBoxField chbQH243_DS1;
//	@FieldAnnotation(orderIndex=5) 
//	public IntegerField txtQH243_D1;
//	@FieldAnnotation(orderIndex=6) 
//	public IntegerField txtQH243_M1;
//	@FieldAnnotation(orderIndex=7) 
//	public IntegerField txtQH243_A1;
//	@FieldAnnotation(orderIndex=8) 
//	public TextField txtQH243_L1;
//	@FieldAnnotation(orderIndex=9) 
//	public RadioGroupOtherField rgQH244_I1;
//	@FieldAnnotation(orderIndex=10) 
//	public RadioGroupOtherField rgQH244_V1;
//	@FieldAnnotation(orderIndex=11) 
//	public TextField txtQH244_VO1;
//	
//	@FieldAnnotation(orderIndex=12) 
//	public CheckBoxField chbQH243_DS2;
//	@FieldAnnotation(orderIndex=13) 
//	public IntegerField txtQH243_D2;
//	@FieldAnnotation(orderIndex=14) 
//	public IntegerField txtQH243_M2;
//	@FieldAnnotation(orderIndex=15) 
//	public IntegerField txtQH243_A2;
//	@FieldAnnotation(orderIndex=16) 
//	public TextField txtQH243_L2;
//	@FieldAnnotation(orderIndex=17) 
//	public RadioGroupOtherField rgQH244_I2;
//	@FieldAnnotation(orderIndex=18) 
//	public RadioGroupOtherField rgQH244_V2;
//	@FieldAnnotation(orderIndex=19) 
//	public TextField txtQH244_VO2;
//	
//	@FieldAnnotation(orderIndex=20) 
//	public CheckBoxField chbQH243_DS3;
//	@FieldAnnotation(orderIndex=21) 
//	public IntegerField txtQH243_D3;
//	@FieldAnnotation(orderIndex=22) 
//	public IntegerField txtQH243_M3;
//	@FieldAnnotation(orderIndex=23) 
//	public IntegerField txtQH243_A3;
//	@FieldAnnotation(orderIndex=24) 
//	public TextField txtQH243_L3;
//	@FieldAnnotation(orderIndex=25) 
//	public RadioGroupOtherField rgQH244_I3;
//	@FieldAnnotation(orderIndex=26) 
//	public RadioGroupOtherField rgQH244_V3;
//	@FieldAnnotation(orderIndex=27) 
//	public TextField txtQH244_VO3;
//	
//	@FieldAnnotation(orderIndex=28) 
//	public CheckBoxField chbQH243_DS4;
//	@FieldAnnotation(orderIndex=29) 
//	public IntegerField txtQH243_D4;
//	@FieldAnnotation(orderIndex=30) 
//	public IntegerField txtQH243_M4;
//	@FieldAnnotation(orderIndex=31) 
//	public IntegerField txtQH243_A4;
//	@FieldAnnotation(orderIndex=32) 
//	public TextField txtQH243_L4;
//	@FieldAnnotation(orderIndex=33) 
//	public RadioGroupOtherField rgQH244_I4;
//	@FieldAnnotation(orderIndex=34) 
//	public RadioGroupOtherField rgQH244_V4;
//	@FieldAnnotation(orderIndex=35) 
//	public TextField txtQH244_VO4;
//	
//	@FieldAnnotation(orderIndex=36) 
//	public CheckBoxField chbQH243_DS5;
//	@FieldAnnotation(orderIndex=37) 
//	public IntegerField txtQH243_D5;
//	@FieldAnnotation(orderIndex=38) 
//	public IntegerField txtQH243_M5;
//	@FieldAnnotation(orderIndex=39) 
//	public IntegerField txtQH243_A5;
//	@FieldAnnotation(orderIndex=40) 
//	public TextField txtQH243_L5;
//	@FieldAnnotation(orderIndex=41) 
//	public RadioGroupOtherField rgQH244_I5;
//	@FieldAnnotation(orderIndex=42) 
//	public RadioGroupOtherField rgQH244_V5;
//	@FieldAnnotation(orderIndex=43) 
//	public TextField txtQH244_VO5;
//	
//	@FieldAnnotation(orderIndex=44) 
//	public CheckBoxField chbQH243_DS6;
//	@FieldAnnotation(orderIndex=45) 
//	public IntegerField txtQH243_D6;
//	@FieldAnnotation(orderIndex=46) 
//	public IntegerField txtQH243_M6;
//	@FieldAnnotation(orderIndex=47) 
//	public IntegerField txtQH243_A6;
//	@FieldAnnotation(orderIndex=48) 
//	public TextField txtQH243_L6;
//	@FieldAnnotation(orderIndex=49) 
//	public RadioGroupOtherField rgQH244_I6;
//	@FieldAnnotation(orderIndex=50) 
//	public RadioGroupOtherField rgQH244_V6;
//	@FieldAnnotation(orderIndex=51) 
//	public TextField txtQH244_VO6;
//	
//	
//	@FieldAnnotation(orderIndex=52)
//	public CheckBoxField chbQH245_A;
//	@FieldAnnotation(orderIndex=53)
//	public CheckBoxField chbQH245_B;
//	@FieldAnnotation(orderIndex=54)
//	public CheckBoxField chbQH245_C;
//	@FieldAnnotation(orderIndex=55)
//	public CheckBoxField chbQH245_D;
//	@FieldAnnotation(orderIndex=56)
//	public CheckBoxField chbQH245_E;
//	@FieldAnnotation(orderIndex=57)
//	public CheckBoxField chbQH245_Y;
//
//	@FieldAnnotation(orderIndex=58) 
//	public RadioGroupOtherField rgQH246;
//	@FieldAnnotation(orderIndex=59) 
//	public RadioGroupOtherField rgQH247;
//	@FieldAnnotation(orderIndex=60) 
//	public TextField txtQH247_O;
//	
//
//	
//
//	
////	@FieldAnnotation(orderIndex=34) 
////	public RadioGroupOtherField rgQH248;
////	@FieldAnnotation(orderIndex=35) 
////	public TextBoxField txtQH248_O;
//	
//	@FieldAnnotation(orderIndex=37)
//	public ButtonComponent btnAceptar;
//	
////	@FieldAnnotation(orderIndex=35)
//	public TextAreaField txtQHVACUNAS_OBS;
//	
//	public static HogarFragment_013_03 caller;	
//	Seccion01 persona;
//	private CuestionarioService cuestionarioService;
//	private CuestionarioService hogarService;
//	private Seccion01Service serviceSeccion01;
//	private VisitaService visitaService;
//	private UbigeoService ubigeoService;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	private static String nombre=null;
//	private String operador="-";
//	String fecha_referencia;
//	
//	public GridComponent2 gridqh243;
//	LabelComponent lblobs_34, lblpreguntaqh240,lblpreguntaqh241,lblpreguntaqh242,lblpreguntaqh242_ind,
//	lblpreguntaqh245,lblpreguntaqh246,lblpreguntaqh247,lblpreguntaqh248,lblpreguntaqh243,
//	lblpreguntaqh244_1,lblpreguntaqh243_n1,lblpreguntaqh243_d1,lblpreguntaqh243_m1,lblpreguntaqh243_a1,lblpreguntaqh243_l1,lblpreguntaqh243_i1,lblpreguntaqh243_v1,
//	lblpreguntaqh244_2,lblpreguntaqh243_n2,lblpreguntaqh243_d2,lblpreguntaqh243_m2,lblpreguntaqh243_a2,lblpreguntaqh243_l2,lblpreguntaqh243_i2,lblpreguntaqh243_v2,
//	lblpreguntaqh244_3,lblpreguntaqh243_n3,lblpreguntaqh243_d3,lblpreguntaqh243_m3,lblpreguntaqh243_a3,lblpreguntaqh243_l3,lblpreguntaqh243_i3,lblpreguntaqh243_v3,
//	lblpreguntaqh244_4,lblpreguntaqh243_n4,lblpreguntaqh243_d4,lblpreguntaqh243_m4,lblpreguntaqh243_a4,lblpreguntaqh243_l4,lblpreguntaqh243_i4,lblpreguntaqh243_v4,
//	lblpreguntaqh244_5,lblpreguntaqh243_n5,lblpreguntaqh243_d5,lblpreguntaqh243_m5,lblpreguntaqh243_a5,lblpreguntaqh243_l5,lblpreguntaqh243_i5,lblpreguntaqh243_v5,
//	lblpreguntaqh244_6,lblpreguntaqh243_n6,lblpreguntaqh243_d6,lblpreguntaqh243_m6,lblpreguntaqh243_a6,lblpreguntaqh243_l6,lblpreguntaqh243_i6,lblpreguntaqh243_v6	
//	;
//	
//	public LabelComponent lblpre240a_h_ini_t, lblpre248a_h_fin_t, lblpre240a_h_ini_f,lblpre248a_h_fin_f;
//	public ButtonComponent btnqh240a_h_ini, btnqh248a_h_fin;
//	private GridComponent2 grid_QH240a_h, grid_QH248a_hf;
//	
//	public ButtonComponent  btnCancelar;
//	public CheckBoxField chbP910;
//	
//	LinearLayout q0;
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5;
//	LinearLayout q55;
//	LinearLayout q6;
//	LinearLayout q7;
//	LinearLayout q8;
//	LinearLayout q9;
//	LinearLayout q10;
//	LinearLayout q11;
//	LinearLayout q12;
//	LinearLayout q13;
//	LinearLayout q14;
//	
//	public static HogarVacunasDialog newInstance(FragmentForm pagina,Seccion01 detalle, int position, List<Seccion01> detalles) {
//		caller = (HogarFragment_013_03) pagina;
////		personas=detalles;
//		HogarVacunasDialog f = new HogarVacunasDialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", (Seccion01) detalles.get(position));
//		f.setArguments(args);
//		return f;
//	}
//	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (Seccion01) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
////		getDialog().setTitle("N� "+persona.qh34a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public HogarVacunasDialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH240A","QH240", "QH241", "QH242", "QH243_REF", "QH243_DS1", "QH243_D1", "QH243_M1", "QH243_A1", "QH243_L1", "QH244_I1", "QH244_V1", "QH244_VO1", "QH243_DS2", "QH243_D2", "QH243_M2", "QH243_A2", "QH243_L2", "QH244_I2", "QH244_V2", "QH244_VO2", "QH243_DS3", "QH243_D3", "QH243_M3", "QH243_A3", "QH243_L3", "QH244_I3", "QH244_V3", "QH244_VO3", "QH243_DS4", "QH243_D4", "QH243_M4", "QH243_A4", "QH243_L4", "QH244_I4", "QH244_V4", "QH244_VO4", "QH243_DS5", "QH243_D5", "QH243_M5", "QH243_A5", "QH243_L5", "QH244_I5", "QH244_V5", "QH244_VO5", "QH243_DS6", "QH243_D6", "QH243_M6", "QH243_A6", "QH243_L6", "QH244_I6", "QH244_V6", "QH244_VO6",     
//												 							  "QH245_A", "QH245_B", "QH245_C", "QH245_D", "QH245_E", "QH245_Y", "QH246", "QH247", "QH247_O", "QH248", "QH248_O", "QHVACUNAS_OBS", "QH248A","QH02_1", "ID", "HOGAR_ID", "PERSONA_ID")};
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH240A","QH240", "QH241", "QH242", "QH243_REF", "QH243_DS1", "QH243_D1", "QH243_M1", "QH243_A1", "QH243_L1", "QH244_I1", "QH244_V1", "QH244_VO1", "QH243_DS2", "QH243_D2", "QH243_M2", "QH243_A2", "QH243_L2", "QH244_I2", "QH244_V2", "QH244_VO2", "QH243_DS3", "QH243_D3", "QH243_M3", "QH243_A3", "QH243_L3", "QH244_I3", "QH244_V3", "QH244_VO3", "QH243_DS4", "QH243_D4", "QH243_M4", "QH243_A4", "QH243_L4", "QH244_I4", "QH244_V4", "QH244_VO4", "QH243_DS5", "QH243_D5", "QH243_M5", "QH243_A5", "QH243_L5", "QH244_I5", "QH244_V5", "QH244_VO5", "QH243_DS6", "QH243_D6", "QH243_M6", "QH243_A6", "QH243_L6", "QH244_I6", "QH244_V6", "QH244_VO6",     
//												 							  "QH245_A", "QH245_B", "QH245_C", "QH245_D", "QH245_E", "QH245_Y", "QH246", "QH247", "QH247_O", "QH248", "QH248_O", "QHVACUNAS_OBS", "QH248A")};
//	}            
//	
//
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		lblpreguntaqh240= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(17).text(R.string.vacuna_p240);
//		lblpreguntaqh241= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p241);
//		lblpreguntaqh242= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p242);
//		lblpreguntaqh242_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(16).text(R.string.vacuna_p242_ind).negrita();
//		
//		lblpreguntaqh245= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p245);
//		lblpreguntaqh246= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p246);
//		lblpreguntaqh247= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p247);
//		lblpreguntaqh248= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(18).text(R.string.vacuna_p248);
//		
//		
//		lblpreguntaqh243= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(17).text(R.string.vacuna_p243);
//		
//		lblpreguntaqh243_n1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		lblpreguntaqh243_n2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		lblpreguntaqh243_n3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		lblpreguntaqh243_n4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		lblpreguntaqh243_n5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		lblpreguntaqh243_n6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(15).text(R.string.vacuna_p243_ind);
//		
//		lblpreguntaqh243_d1= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		lblpreguntaqh243_d2= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		lblpreguntaqh243_d3= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		lblpreguntaqh243_d4= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		lblpreguntaqh243_d5= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		lblpreguntaqh243_d6= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_d).negrita().centrar();
//		
//		lblpreguntaqh243_m1= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		lblpreguntaqh243_m2= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		lblpreguntaqh243_m3= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		lblpreguntaqh243_m4= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		lblpreguntaqh243_m5= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		lblpreguntaqh243_m6= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_m).negrita().centrar();
//		
//		lblpreguntaqh243_a1= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		lblpreguntaqh243_a2= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		lblpreguntaqh243_a3= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		lblpreguntaqh243_a4= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		lblpreguntaqh243_a5= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		lblpreguntaqh243_a6= new LabelComponent(getActivity()).size(35, 100).textSize(16).text(R.string.vacuna_p243_a).negrita().centrar();
//		
//		lblpreguntaqh243_l1= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		lblpreguntaqh243_l2= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		lblpreguntaqh243_l3= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		lblpreguntaqh243_l4= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		lblpreguntaqh243_l5= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		lblpreguntaqh243_l6= new LabelComponent(getActivity()).size(35, 400).textSize(16).text(R.string.vacuna_p243_l).negrita().centrar();
//		
//		lblpreguntaqh244_1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d1);
//		lblpreguntaqh244_2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d2);
//		lblpreguntaqh244_3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d3);
//		lblpreguntaqh244_4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d4);
//		lblpreguntaqh244_5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d5);
//		lblpreguntaqh244_6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.vacuna_p244_d6);
//		
//		lblpreguntaqh243_i1= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		lblpreguntaqh243_i2= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		lblpreguntaqh243_i3= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		lblpreguntaqh243_i4= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		lblpreguntaqh243_i5= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		lblpreguntaqh243_i6= new LabelComponent(getActivity()).size(altoComponente, 300).textSize(16).text(R.string.vacuna_p243_i).negrita();
//		
//		lblpreguntaqh243_v1= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		lblpreguntaqh243_v2= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		lblpreguntaqh243_v3= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		lblpreguntaqh243_v4= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		lblpreguntaqh243_v5= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		lblpreguntaqh243_v6= new LabelComponent(getActivity()).size(altoComponente, 400).textSize(16).text(R.string.vacuna_p243_v).negrita();
//		
//		
//		
//		spnQH240=new SpinnerField(getActivity()).size(altoComponente+15, 500).callback("renombrarNombres");
//		rgQH241 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p241_1,R.string.vacuna_p241_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh241_ChangeValue");		
//		rgQH242 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p242_1,R.string.vacuna_p242_2,R.string.vacuna_p242_3,R.string.vacuna_p242_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh242_ChangeValue");
//		
//		txtQH243_D1=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_D2=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_D3=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_D4=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_D5=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_D6=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		
//		txtQH243_M1=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_M2=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_M3=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_M4=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_M5=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		txtQH243_M6=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(2).centrar();
//		
//		txtQH243_A1=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		txtQH243_A2=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		txtQH243_A3=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		txtQH243_A4=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		txtQH243_A5=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		txtQH243_A6=new IntegerField(this.getActivity()).size(altoComponente, 100).maxLength(4).centrar();
//		
//		txtQH243_L1=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		txtQH243_L2=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		txtQH243_L3=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		txtQH243_L4=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		txtQH243_L5=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		txtQH243_L6=new TextField(this.getActivity()).size(altoComponente, 400).maxLength(100);
//		
//		
//		chbQH243_DS1 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n1, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds1ChangeValue").negrita().colorFondo(R.color.griscabece);
//		chbQH243_DS2 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n2, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds2ChangeValue").negrita().colorFondo(R.color.griscabece);
//		chbQH243_DS3 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n3, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds3ChangeValue").negrita().colorFondo(R.color.griscabece);
//		chbQH243_DS4 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n4, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds4ChangeValue").negrita().colorFondo(R.color.griscabece);
//		chbQH243_DS5 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n5, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds5ChangeValue").negrita().colorFondo(R.color.griscabece);
//		chbQH243_DS6 = new CheckBoxField(this.getActivity(), R.string.vacuna_p243_n6, "1:0").size(altoComponente+5, MATCH_PARENT).callback("onqh243_ds6ChangeValue").negrita().colorFondo(R.color.griscabece);
//		
//		rgQH244_I1 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V1ChangeValue");
//		rgQH244_I2 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V2ChangeValue");
//		rgQH244_I3 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V3ChangeValue");
//		rgQH244_I4 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V4ChangeValue");
//		rgQH244_I5 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V5ChangeValue");
//		rgQH244_I6 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_1_1,R.string.vacuna_p244_1_2,R.string.vacuna_p244_1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqh244_V6ChangeValue");
//		
//		rgQH244_V1 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH244_V2 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH244_V3 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH244_V4 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH244_V5 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH244_V6 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p244_2_1,R.string.vacuna_p244_2_2,R.string.vacuna_p244_2_3,R.string.vacuna_p244_2_4,R.string.vacuna_p244_2_5,R.string.vacuna_p244_2_6,R.string.vacuna_p244_2_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		
//		txtQH244_VO1 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		txtQH244_VO2 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		txtQH244_VO3 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		txtQH244_VO4 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		txtQH244_VO5 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		txtQH244_VO6 = new TextField(getActivity()).size(altoComponente, 500).maxLength(500);
//		
//		rgQH244_V1.agregarEspecifique(6,txtQH244_VO1);
//		rgQH244_V2.agregarEspecifique(6,txtQH244_VO2);
//		rgQH244_V3.agregarEspecifique(6,txtQH244_VO3);
//		rgQH244_V4.agregarEspecifique(6,txtQH244_VO4);
//		rgQH244_V5.agregarEspecifique(6,txtQH244_VO5);
//		rgQH244_V6.agregarEspecifique(6,txtQH244_VO6);
//		
////		gridqh243=new GridComponent2(this.getActivity(),App.ESTILO,4,0);
////		gridqh243.addComponent(lblpreguntaqh243_d1);
////		gridqh243.addComponent(lblpreguntaqh243_m1);
////		gridqh243.addComponent(lblpreguntaqh243_a1);
////		gridqh243.addComponent(lblpreguntaqh243_l1);
////		gridqh243.addComponent(txtQH243_D1);
////		gridqh243.addComponent(txtQH243_M1);
////		gridqh243.addComponent(txtQH243_A1);
////		gridqh243.addComponent(txtQH243_L1);
//		
//		gridqh243 = new GridComponent2(this.getActivity(),App.ESTILO, 4,1);
//		gridqh243.addComponent(chbQH243_DS1,4);
//		gridqh243.addComponent(lblpreguntaqh243_n1,4);		
//		gridqh243.addComponent(lblpreguntaqh243_d1);
//		gridqh243.addComponent(lblpreguntaqh243_m1);
//		gridqh243.addComponent(lblpreguntaqh243_a1);
//		gridqh243.addComponent(lblpreguntaqh243_l1);
//		gridqh243.addComponent(txtQH243_D1);
//		gridqh243.addComponent(txtQH243_M1);
//		gridqh243.addComponent(txtQH243_A1);
//		gridqh243.addComponent(txtQH243_L1);
//		gridqh243.addComponent(lblpreguntaqh244_1,4);
//		gridqh243.addComponent(rgQH244_I1,4);
//		gridqh243.addComponent(rgQH244_V1,4);
//		
//		gridqh243.addComponent(chbQH243_DS2,4);
//		gridqh243.addComponent(lblpreguntaqh243_n2,4);
//		gridqh243.addComponent(lblpreguntaqh243_d2);
//		gridqh243.addComponent(lblpreguntaqh243_m2);
//		gridqh243.addComponent(lblpreguntaqh243_a2);
//		gridqh243.addComponent(lblpreguntaqh243_l2);
//		gridqh243.addComponent(txtQH243_D2);
//		gridqh243.addComponent(txtQH243_M2);
//		gridqh243.addComponent(txtQH243_A2);
//		gridqh243.addComponent(txtQH243_L2);
//		gridqh243.addComponent(lblpreguntaqh244_2,4);
//		gridqh243.addComponent(rgQH244_I2,4);
//		gridqh243.addComponent(rgQH244_V2,4);
//		
//		gridqh243.addComponent(chbQH243_DS3,4);
//		gridqh243.addComponent(lblpreguntaqh243_n3,4);
//		gridqh243.addComponent(lblpreguntaqh243_d3);
//		gridqh243.addComponent(lblpreguntaqh243_m3);
//		gridqh243.addComponent(lblpreguntaqh243_a3);
//		gridqh243.addComponent(lblpreguntaqh243_l3);
//		gridqh243.addComponent(txtQH243_D3);
//		gridqh243.addComponent(txtQH243_M3);
//		gridqh243.addComponent(txtQH243_A3);
//		gridqh243.addComponent(txtQH243_L3);
//		gridqh243.addComponent(lblpreguntaqh244_3,4);
//		gridqh243.addComponent(rgQH244_I3,4);
//		gridqh243.addComponent(rgQH244_V3,4);
//		
//		
//		gridqh243.addComponent(chbQH243_DS4,4);
//		gridqh243.addComponent(lblpreguntaqh243_n4,4);
//		gridqh243.addComponent(lblpreguntaqh243_d4);
//		gridqh243.addComponent(lblpreguntaqh243_m4);
//		gridqh243.addComponent(lblpreguntaqh243_a4);
//		gridqh243.addComponent(lblpreguntaqh243_l4);
//		gridqh243.addComponent(txtQH243_D4);
//		gridqh243.addComponent(txtQH243_M4);
//		gridqh243.addComponent(txtQH243_A4);
//		gridqh243.addComponent(txtQH243_L4);
//		gridqh243.addComponent(lblpreguntaqh244_4,4);
//		gridqh243.addComponent(rgQH244_I4,4);
//		gridqh243.addComponent(rgQH244_V4,4);		
//		
//		gridqh243.addComponent(chbQH243_DS5,4);
//		gridqh243.addComponent(lblpreguntaqh243_n5,4);
//		gridqh243.addComponent(lblpreguntaqh243_d5);
//		gridqh243.addComponent(lblpreguntaqh243_m5);
//		gridqh243.addComponent(lblpreguntaqh243_a5);
//		gridqh243.addComponent(lblpreguntaqh243_l5);
//		gridqh243.addComponent(txtQH243_D5);
//		gridqh243.addComponent(txtQH243_M5);
//		gridqh243.addComponent(txtQH243_A5);
//		gridqh243.addComponent(txtQH243_L5);
//		gridqh243.addComponent(lblpreguntaqh244_5,4);
//		gridqh243.addComponent(rgQH244_I5,4);
//		gridqh243.addComponent(rgQH244_V5,4);				
//		
//		gridqh243.addComponent(chbQH243_DS6,4);
//		gridqh243.addComponent(lblpreguntaqh243_n6,4);
//		gridqh243.addComponent(lblpreguntaqh243_d6);
//		gridqh243.addComponent(lblpreguntaqh243_m6);
//		gridqh243.addComponent(lblpreguntaqh243_a6);
//		gridqh243.addComponent(lblpreguntaqh243_l6);
//		gridqh243.addComponent(txtQH243_D6);
//		gridqh243.addComponent(txtQH243_M6);
//		gridqh243.addComponent(txtQH243_A6);
//		gridqh243.addComponent(txtQH243_L6);
//		gridqh243.addComponent(lblpreguntaqh244_6,4);
//		gridqh243.addComponent(rgQH244_I6,4);
//		gridqh243.addComponent(rgQH244_V6,4);
//		
//		
//		
//		chbQH245_A = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_A, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_AChangeValue");
//		chbQH245_B = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_B, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_BChangeValue");
//		chbQH245_C = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_C, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_CChangeValue");
//		chbQH245_D = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_D, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_DChangeValue");
//		chbQH245_E = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_E, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_EChangeValue");
//		chbQH245_Y = new CheckBoxField(this.getActivity(), R.string.vacuna_p245_Y, "1:0").size(MATCH_PARENT, MATCH_PARENT).callback("onchbQH245_YChangeValue");
//		
//		rgQH246 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p246_1,R.string.vacuna_p246_2,R.string.vacuna_p246_3,R.string.vacuna_p246_4,R.string.vacuna_p246_5,R.string.vacuna_p246_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		rgQH247 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p247_1,R.string.vacuna_p247_2,R.string.vacuna_p247_3,R.string.vacuna_p247_4,R.string.vacuna_p247_5,R.string.vacuna_p247_6,R.string.vacuna_p247_7,R.string.vacuna_p247_8,R.string.vacuna_p247_9,R.string.vacuna_p247_98).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
//		txtQH247_O= new TextField(getActivity()).size(altoComponente, 600).maxLength(500);	
//		rgQH247.agregarEspecifique(8,txtQH247_O);
//		
//		lblobs_34 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//		txtQHVACUNAS_OBS = new TextAreaField(getActivity()).maxLength(1000).size(250, 700).alfanumerico();
//		
////		rgQH248 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p248_1,R.string.vacuna_p248_2,R.string.vacuna_p248_3,R.string.vacuna_p248_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		
//
//		
//		txtQH243_D1.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia1 = Integer.valueOf(s.toString());
//			    	if (inDia1==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M1,txtQH243_A1);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M1,txtQH243_A1);
//				}
//			}
//		});
//		txtQH243_D2.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia2 = Integer.valueOf(s.toString());
//			    	if (inDia2==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M2,txtQH243_A2);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M2,txtQH243_A2);
//				}
//			}
//		});
//		txtQH243_D3.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia3 = Integer.valueOf(s.toString());
//			    	if (inDia3==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M3,txtQH243_A3);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M3,txtQH243_A3);
//				}
//			}
//		});
//		
//		txtQH243_D4.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia4 = Integer.valueOf(s.toString());
//			    	if (inDia4==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M4,txtQH243_A4);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M4,txtQH243_A4);
//				}
//			}
//		});
//		
//		 
//		txtQH243_D5.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia5 = Integer.valueOf(s.toString());
//			    	if (inDia5==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M5,txtQH243_A5);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M5,txtQH243_A5);
//				}
//			}
//		});
//		
//		
//		txtQH243_D6.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
//				// TODO Auto-generated method stub
//			}
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				if(s.toString().length()>0){
//			    	Integer inDia6 = Integer.valueOf(s.toString());
//			    	if (inDia6==44){
//			    		Util.cleanAndLockView(getActivity(),txtQH243_M6,txtQH243_A6);
//			    	}
//			    	else
//			    		Util.lockView(getActivity(), false,txtQH243_M6,txtQH243_A6);
//				}
//			}
//		});
//		
//		lblpre240a_h_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vacuna_hora_ini).textSize(17);
//		lblpre248a_h_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vacuna_hora_fin).textSize(17);
//		btnqh240a_h_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
//		btnqh248a_h_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
//		lblpre240a_h_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
//		lblpre248a_h_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
//	    
//		btnqh240a_h_ini.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				lblpre240a_h_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
//				btnqh248a_h_fin.setEnabled(true);
//			}
//		});
//		btnqh248a_h_fin.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				lblpre248a_h_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
//			}
//		});
//		    
//		grid_QH240a_h = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
//		grid_QH240a_h.addComponent(btnqh240a_h_ini);
//		grid_QH240a_h.addComponent(lblpre240a_h_ini_f);
//		 
//		grid_QH248a_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
//		grid_QH248a_hf.addComponent(btnqh248a_h_fin);
//		grid_QH248a_hf.addComponent(lblpre248a_h_fin_f);
//		
//		
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				HogarVacunasDialog.this.dismiss();
//			}
//		});		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				HogarVacunasDialog.this.dismiss();
//				caller.cargarTabla();
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q0 = createQuestionSection(0,lblpreguntaqh240,spnQH240);
//		q9 = createQuestionSection(lblpre240a_h_ini_t,grid_QH240a_h.component());
//		
//		q1 = createQuestionSection(0,lblpreguntaqh241,rgQH241); 
//		q2 = createQuestionSection(0,lblpreguntaqh242,lblpreguntaqh242_ind,rgQH242);
//		q3 = createQuestionSection(0,lblpreguntaqh243,gridqh243.component()
////				chbQH243_DS1,lblpreguntaqh243_n1,lblpreguntaqh244_1,
////				chbQH243_DS2,lblpreguntaqh243_n2,lblpreguntaqh244_2,
////				chbQH243_DS3,lblpreguntaqh243_n3,lblpreguntaqh244_3
//				);
//		
//		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh245,chbQH245_A,chbQH245_B,chbQH245_C,chbQH245_D,chbQH245_E,chbQH245_Y);
//		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh246,rgQH246);
//		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpreguntaqh247,rgQH247);
//		
//		q8 = createQuestionSection(lblobs_34,txtQHVACUNAS_OBS);
//		q10 = createQuestionSection(lblpre248a_h_fin_t,grid_QH248a_hf.component());
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q0);
//		form.addView(q9);		
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q3);
//		form.addView(q5);
//		form.addView(q6);
//		form.addView(q7);
//		form.addView(q8);
//		form.addView(q10);
//		
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar(){
//		uiToEntity(persona);
//		
//		if(fecha_referencia == null) {
//			persona.qh243_ref = Util.getSoloFechaActualToString();
//		}
//		else {
//			 persona.qh243_ref = fecha_referencia;
//		}
//		
//		
//		if (persona.qh242!=null) {
//			persona.qh242=persona.getConvertqh4a8(persona.qh242);
//		}
//		if (persona.qh246!=null) {
//			persona.qh246=persona.getConvertqh6a8(persona.qh246);
//		}
//		if (persona.qh247!=null) {
//			persona.qh247=persona.getConvertqh10a98(persona.qh247);
//		}
//		persona.qh248=1;	
//		
//		persona.qh240a =lblpre240a_h_ini_f.getText().toString();
//		persona.qh248a =lblpre248a_h_fin_f.getText().toString();
//		
//		
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag=false;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		}
//		
//	
////    	Log.e("fechaactual: ",""+fechaactual);
//    	//Date txtFecha1 = fechaactual.getCalendarType();
////		Date fecha1 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"",persona.qh243_d3+"");
////		Log.e("fecha1::",""+fecha1);
////		Log.e("qh248: ",""+persona.qh248);
//		
//		return flag;
//		
//	}
//
//	
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		
//
//		Calendar fechaactual = new GregorianCalendar();
//    	if(fecha_referencia!=null){			
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fecha_referencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fechaactual=cal;			
//		}
//		Integer anio = fechaactual.get(Calendar.YEAR);
//		Integer mes = fechaactual.get(Calendar.MONTH)+1;
//		Integer dia = fechaactual.get(Calendar.DAY_OF_MONTH);
//		Date fecha_ref = Util.getFecha(anio+"", mes+"", dia+"");
//		//Log.e("fecha11::",""+(Date) fecha_ref1);
//		
//		
//		if (Util.esVacio(persona.qh240)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.240"); 
//			view = spnQH240; 
//			error = true; 
//			return false; 
//		}
//		if(lblpre240a_h_ini_f.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 240A");
//			view = btnqh240a_h_ini;
//			error = true;
//			return false;
//		}
//		
//		
//		if (Util.esVacio(persona.qh241)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.241"); 
//			view = rgQH241; 
//			error = true; 
//			return false; 
//		}
//		
//		
//		
//		if(!Util.esDiferente(persona.qh241,1)){
//			
//			if (Util.esVacio(persona.qh242)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta P.242"); 
//				view = rgQH242; 
//				error = true; 
//				return false; 
//			}
//			
//			if(!chbQH243_DS1.isChecked()){
//				mensaje = preguntaVacia.replace("$", "La pregunta P.243_Dosis 1"); 
//				view = chbQH243_DS1; 
//				error = true; 
//				return false; 
//			}
//			
//			if(chbQH243_DS1.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d1)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D1"); 
//						view = txtQH243_D1; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d1,44)){
//						if (Util.esVacio(persona.qh243_m1)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M1"); 
//							view = txtQH243_M1; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a1)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A1"); 
//							view = txtQH243_A1; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l1)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L1"); 
//							view = txtQH243_L1; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d1!=null && persona.qh243_d1!=98 && (persona.qh243_d1<=0 || persona.qh243_d1>31)) {
//							error = true;
//							view = txtQH243_D1;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 && (persona.qh243_m1<=0 || persona.qh243_m1>12)) {
//							error = true;
//							view = txtQH243_M1;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a1!=null && (persona.qh243_a1<2020 || persona.qh243_a1>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A1;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98){
//							Date fecha1 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							//Log.e("fecha1::",""+(Date) fecha1);
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha1) < 0) {
//					    		mensaje = "La 1ra dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D1; 
//								error = true; 
//								return false;
//					    	}
//						}
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i1)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I1; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i1,1) || !Util.esDiferente(persona.qh244_i1,2)){
//					if (Util.esVacio(persona.qh244_v1)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V1; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v1,7)){ 
//						if (Util.esVacio(persona.qh244_vo1)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO1; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//			
//			
//			if(chbQH243_DS2.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d2)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D2"); 
//						view = txtQH243_D2; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d2,44)){
//						if (Util.esVacio(persona.qh243_m2)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M2"); 
//							view = txtQH243_M2; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a2)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A2"); 
//							view = txtQH243_A2; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l2)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L2"); 
//							view = txtQH243_L2; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d2!=null && persona.qh243_d2!=98 && (persona.qh243_d2<=0 || persona.qh243_d2>31)) {
//							error = true;
//							view = txtQH243_D2;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98 && (persona.qh243_m2<=0 || persona.qh243_m2>12)) {
//							error = true;
//							view = txtQH243_M2;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a2!=null && (persona.qh243_a2<2020 || persona.qh243_a2>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A2;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98){
//							Date fecha2 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha2) < 0) {
//					    		mensaje = "La 2da dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D2; 
//								error = true; 
//								return false;
//					    	}
//						}
//						
//						if(persona!=null && 
//								persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 &&
//								persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98
//								){
//														
//							Date fecha11 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							Date fecha22 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//							if (Util.compare((Date) fecha22,(Date) fecha11) <= 0) {
//					    		mensaje = "La 2da dosis no debe tener fecha menor a la 1ra dosis"; 
//								view = txtQH243_D2; 
//								error = true; 
//								return false;
//					    	}
//							
//						}
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i2)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I2; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i2,1) || !Util.esDiferente(persona.qh244_i2,2)){
//					if (Util.esVacio(persona.qh244_v2)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V2; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v2,7)){ 
//						if (Util.esVacio(persona.qh244_vo2)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO2; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//
//			
//			if(chbQH243_DS3.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d3)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D3"); 
//						view = txtQH243_D3; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d3,44)){
//						if (Util.esVacio(persona.qh243_m3)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M3"); 
//							view = txtQH243_M3; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a3)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A3"); 
//							view = txtQH243_A3; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l3)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L3"); 
//							view = txtQH243_L3; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d3!=null && persona.qh243_d3!=98 && (persona.qh243_d3<=0 || persona.qh243_d3>31)) {
//							error = true;
//							view = txtQH243_D3;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98 && (persona.qh243_m3<=0 || persona.qh243_m3>12)) {
//							error = true;
//							view = txtQH243_M3;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a3!=null && (persona.qh243_a3<2020 || persona.qh243_a3>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A3;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98){
//							Date fecha3 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha3) < 0) {
//					    		mensaje = "La 3ra dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D3; 
//								error = true; 
//								return false;
//					    	}
//						}
//						if(persona!=null && 
//								persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 &&
//								persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98
//								){														
//							Date fecha111 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							Date fecha33 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//							if (Util.compare((Date) fecha33,(Date) fecha111) <= 0) {
//					    		mensaje = "La 3ra dosis no debe tener fecha menor a la 1ra dosis"; 
//								view = txtQH243_D3; 
//								error = true; 
//								return false;
//					    	}
//							
//						}
//						
//						
//						if(persona!=null &&
//								persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98 &&
//								persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98
//								){																					
//							Date fecha222 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//							Date fecha333 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//							if (Util.compare((Date) fecha333,(Date) fecha222) <= 0) {
//					    		mensaje = "La 3ra dosis no debe tener fecha menor a la 2da dosis"; 
//								view = txtQH243_D3; 
//								error = true; 
//								return false;
//					    	}
//						}
//						
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i3)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I3; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i3,1) || !Util.esDiferente(persona.qh244_i3,2)){
//					if (Util.esVacio(persona.qh244_v3)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V3; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v3,7)){ 
//						if (Util.esVacio(persona.qh244_vo3)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO3; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//		
//			
//			
//			//DOSIS 4
//			if(chbQH243_DS4.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d4)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D4"); 
//						view = txtQH243_D4; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d4,44)){
//						if (Util.esVacio(persona.qh243_m4)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M4"); 
//							view = txtQH243_M4; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a4)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A4"); 
//							view = txtQH243_A4; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l4)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L4"); 
//							view = txtQH243_L4; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d4!=null && persona.qh243_d4!=98 && (persona.qh243_d4<=0 || persona.qh243_d4>31)) {
//							error = true;
//							view = txtQH243_D4;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98 && (persona.qh243_m4<=0 || persona.qh243_m4>12)) {
//							error = true;
//							view = txtQH243_M4;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a4!=null && (persona.qh243_a4<2020 || persona.qh243_a4>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A4;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98){
//							Date fecha4 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha4) < 0) {
//					    		mensaje = "La 4ta dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D4; 
//								error = true; 
//								return false;
//					    	}
//						}
//		
//						//compare fechas de dosis
//						if(persona!=null && 
//								persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 &&
//								persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98
//								){														
//							Date fecha11 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							Date fecha44 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//							if (Util.compare((Date) fecha44,(Date) fecha11) <= 0) {
//					    		mensaje = "La 4ta dosis no debe tener fecha menor a la 1ra dosis"; 
//								view = txtQH243_D4; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98 &&
//								persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98
//								){														
//							Date fecha22 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//							Date fecha44 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//							if (Util.compare((Date) fecha44,(Date) fecha22) <= 0) {
//					    		mensaje = "La 4ta dosis no debe tener fecha menor a la 2ra dosis"; 
//								view = txtQH243_D4; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98 &&
//								persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98
//								){														
//							Date fecha33 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//							Date fecha44 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//							if (Util.compare((Date) fecha44,(Date) fecha33) <= 0) {
//					    		mensaje = "La 4ta dosis no debe tener fecha menor a la 3ra dosis"; 
//								view = txtQH243_D4; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i4)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I4; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i4,1) || !Util.esDiferente(persona.qh244_i4,2)){
//					if (Util.esVacio(persona.qh244_v4)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V4; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v4,7)){ 
//						if (Util.esVacio(persona.qh244_vo4)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO4; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//			
//			//DOSIS 5
//			if(chbQH243_DS5.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d5)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D5"); 
//						view = txtQH243_D5; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d5,44)){
//						if (Util.esVacio(persona.qh243_m5)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M5"); 
//							view = txtQH243_M5; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a5)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A5"); 
//							view = txtQH243_A5; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l5)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L5"); 
//							view = txtQH243_L5; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d5!=null && persona.qh243_d5!=98 && (persona.qh243_d5<=0 || persona.qh243_d5>31)) {
//							error = true;
//							view = txtQH243_D5;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98 && (persona.qh243_m5<=0 || persona.qh243_m5>12)) {
//							error = true;
//							view = txtQH243_M5;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a5!=null && (persona.qh243_a5<2020 || persona.qh243_a5>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A5;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98){
//							Date fecha5 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha5) < 0) {
//					    		mensaje = "La 5ta dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D5; 
//								error = true; 
//								return false;
//					    	}
//						}
//						//compare fechas de dosis
//						if(persona!=null && 
//								persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 &&
//								persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98
//								){														
//							Date fecha11 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							Date fecha55 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//							if (Util.compare((Date) fecha55,(Date) fecha11) <= 0) {
//					    		mensaje = "La 5ta dosis no debe tener fecha menor a la 1ra dosis"; 
//								view = txtQH243_D5; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98 &&
//								persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98
//								){														
//							Date fecha22 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//							Date fecha55 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//							if (Util.compare((Date) fecha55,(Date) fecha22) <= 0) {
//					    		mensaje = "La 5ta dosis no debe tener fecha menor a la 2ra dosis"; 
//								view = txtQH243_D5; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98 &&
//								persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98
//								){														
//							Date fecha33 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//							Date fecha55 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//							if (Util.compare((Date) fecha55,(Date) fecha33) <= 0) {
//					    		mensaje = "La 5ta dosis no debe tener fecha menor a la 3ra dosis"; 
//								view = txtQH243_D5; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98 &&
//								persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98
//								){														
//							Date fecha44 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//							Date fecha55 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//							if (Util.compare((Date) fecha55,(Date) fecha44) <= 0) {
//					    		mensaje = "La 5ta dosis no debe tener fecha menor a la 4ta dosis"; 
//								view = txtQH243_D5; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						
//						
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i5)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I5; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i5,1) || !Util.esDiferente(persona.qh244_i5,2)){
//					if (Util.esVacio(persona.qh244_v5)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V5; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v5,7)){ 
//						if (Util.esVacio(persona.qh244_vo5)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO5; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//			
//			//DOSIS 6
//			if(chbQH243_DS6.isChecked()){
//				if(!Util.esDiferente(persona.qh242,1)){				
//					if (Util.esVacio(persona.qh243_d6)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.243_D6"); 
//						view = txtQH243_D6; 
//						error = true; 
//						return false; 
//					}
//					if(Util.esDiferente(persona.qh243_d6,44)){
//						if (Util.esVacio(persona.qh243_m6)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_M6"); 
//							view = txtQH243_M6; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_a6)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_A6"); 
//							view = txtQH243_A6; 
//							error = true; 
//							return false; 
//						}
//						if (Util.esVacio(persona.qh243_l6)) { 
//							mensaje = preguntaVacia.replace("$", "La pregunta P.243_L6"); 
//							view = txtQH243_L6; 
//							error = true; 
//							return false; 
//						}
//						if (persona!=null && persona.qh243_d6!=null && persona.qh243_d6!=98 && (persona.qh243_d6<=0 || persona.qh243_d6>31)) {
//							error = true;
//							view = txtQH243_D6;
//							mensaje = "D�a ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98 && (persona.qh243_m6<=0 || persona.qh243_m6>12)) {
//							error = true;
//							view = txtQH243_M6;
//							mensaje = "Mes ingresado fuera de rango";
//							return false;
//						}
//						if (persona!=null && persona.qh243_a6!=null && (persona.qh243_a6<2020 || persona.qh243_a6>App.ANIOPORDEFECTOSUPERIOR)) {
//							error = true;
//							view = txtQH243_A6;
//							mensaje = "A�o ingresado fuera de rango";
//							return false;
//						}
//						
//						if(persona!=null && persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98){
//							Date fecha6 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//					    	if (Util.compare((Date) fecha_ref,(Date) fecha6) < 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha mayor a la visita"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}
//						}
//						//compare fechas de dosis
//						if(persona!=null && 
//								persona.qh243_d1!=null && persona.qh243_d1!=98 && persona!=null && persona.qh243_m1!=null && persona.qh243_m1!=98 &&
//								persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98
//								){														
//							Date fecha11 = Util.getFecha(persona.qh243_a1+"", persona.qh243_m1+"", persona.qh243_d1+"");
//							Date fecha66 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//							if (Util.compare((Date) fecha66,(Date) fecha11) <= 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha menor a la 1ra dosis"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d2!=null && persona.qh243_d2!=98 && persona!=null && persona.qh243_m2!=null && persona.qh243_m2!=98 &&
//								persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98
//								){														
//							Date fecha22 = Util.getFecha(persona.qh243_a2+"", persona.qh243_m2+"", persona.qh243_d2+"");
//							Date fecha66 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//							if (Util.compare((Date) fecha66,(Date) fecha22) <= 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha menor a la 2ra dosis"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d3!=null && persona.qh243_d3!=98 && persona!=null && persona.qh243_m3!=null && persona.qh243_m3!=98 &&
//								persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98
//								){														
//							Date fecha33 = Util.getFecha(persona.qh243_a3+"", persona.qh243_m3+"", persona.qh243_d3+"");
//							Date fecha66 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//							if (Util.compare((Date) fecha66,(Date) fecha33) <= 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha menor a la 3ra dosis"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d4!=null && persona.qh243_d4!=98 && persona!=null && persona.qh243_m4!=null && persona.qh243_m4!=98 &&
//								persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98
//								){														
//							Date fecha44 = Util.getFecha(persona.qh243_a4+"", persona.qh243_m4+"", persona.qh243_d4+"");
//							Date fecha66 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//							if (Util.compare((Date) fecha66,(Date) fecha44) <= 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha menor a la 4ta dosis"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						if(persona!=null && 
//								persona.qh243_d5!=null && persona.qh243_d5!=98 && persona!=null && persona.qh243_m5!=null && persona.qh243_m5!=98 &&
//								persona.qh243_d6!=null && persona.qh243_d6!=98 && persona!=null && persona.qh243_m6!=null && persona.qh243_m6!=98
//								){														
//							Date fecha55 = Util.getFecha(persona.qh243_a5+"", persona.qh243_m5+"", persona.qh243_d5+"");
//							Date fecha66 = Util.getFecha(persona.qh243_a6+"", persona.qh243_m6+"", persona.qh243_d6+"");
//							if (Util.compare((Date) fecha66,(Date) fecha55) <= 0) {
//					    		mensaje = "La 6ta dosis no debe tener fecha menor a la 5ta dosis"; 
//								view = txtQH243_D6; 
//								error = true; 
//								return false;
//					    	}							
//						}
//						
//						
//						
//					}
//				}
//				
//				if (Util.esVacio(persona.qh244_i6)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.244"); 
//					view = rgQH244_I6; 
//					error = true; 
//					return false; 
//				}
//				if(!Util.esDiferente(persona.qh244_i6,1) || !Util.esDiferente(persona.qh244_i6,2)){
//					if (Util.esVacio(persona.qh244_v6)) { 
//						mensaje = preguntaVacia.replace("$", "La pregunta P.244_V"); 
//						view = rgQH244_V6; 
//						error = true; 
//						return false; 
//					}
//					if(!Util.esDiferente(persona.qh244_v6,7)){ 
//						if (Util.esVacio(persona.qh244_vo6)) { 
//							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//							view = txtQH244_VO6; 
//							error = true; 
//							return false; 
//						} 
//					}
//				}
//			}
//			
//			
//			
//			
//			if(!verificarPregunta245()){
//				mensaje = preguntaVacia.replace("$", "Debe seleccionar al menos una respuesta en pregunta P.245");
//				view = chbQH245_A;
//				error = true;
//				return false;
//			}
//			if(chbQH245_C.isChecked() || chbQH245_D.isChecked()){
//				if (Util.esVacio(persona.qh246)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta P.246"); 
//					view = rgQH246; 
//					error = true; 
//					return false; 
//				}
//			}
//			
//		}
//		
//		if(!Util.esDiferente(persona.qh241,2)){
//			if (Util.esVacio(persona.qh247)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta P.247"); 
//				view = rgQH247; 
//				error = true; 
//				return false; 
//			}
//			if(!Util.esDiferente(persona.qh247,9)){ 
//				if (Util.esVacio(persona.qh247_o)) { 
//					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//					view = txtQH247_O; 
//					error = true; 
//					return false; 
//				} 
//			}
//		}
//		
//		if(lblpre248a_h_fin_f.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 248A");
//			view = btnqh248a_h_fin	;
//			error = true;
//			return false;
//		}
//    	
//    	if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0 && lblpre248a_h_fin_f.getText().toString().trim().length()!=0){
//    		Calendar fecha_ini = new GregorianCalendar();
//    		if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0){			
//    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//    			Date date2 = null;
//    			try {
//    				date2 = df.parse(lblpre240a_h_ini_f.getText().toString());
//    			} catch (ParseException e) {
//    				// TODO Auto-generated catch block
//    				e.printStackTrace();
//    			}		
//    			Calendar cal = Calendar.getInstance();
//    			cal.setTime(date2);
//    			fecha_ini=cal;			
//    		}
//    		
//    		Calendar fecha_fin = new GregorianCalendar();
//        	if(lblpre248a_h_fin_f.getText().toString().trim().length()!=0){			
//    			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//    			Date date2 = null;
//    			try {
//    				date2 = df.parse(lblpre248a_h_fin_f.getText().toString());
//    			} catch (ParseException e) {
//    				// TODO Auto-generated catch block
//    				e.printStackTrace();
//    			}		
//    			Calendar cal = Calendar.getInstance();
//    			cal.setTime(date2);
//    			fecha_fin=cal;			
//    		}
////        	Date Dfecha_ini=Util.getFechaFormateada(fecha_ini.get(Calendar.YEAR)+""+fecha_ini.get(Calendar.MONTH)+""+fecha_ini.get(Calendar.DAY_OF_MONTH));
////        	Date Dfecha_fin=Util.getFechaFormateada(fecha_fin.get(Calendar.YEAR)+""+fecha_fin.get(Calendar.MONTH)+""+fecha_fin.get(Calendar.DAY_OF_MONTH));
////        	Log.e("","FECHA NACIMIENTO: A: "+fecha_fin.get(Calendar.HOUR)+"M: "+fecha_fin.get(Calendar.MINUTE)+" D: "+fecha_fin.get(Calendar.SECOND));
////      
//    		if (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0) {
//        		mensaje = "Fecha final no puede ser menor a la fecha inicial"; 
//    			view = btnqh248a_h_fin; 
//    			error = true; 
//    			return false;
//        	}	
//        	if ( (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) == 0)  && (Util.compareTime((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0)) {
//        		mensaje = "Hora final no puede ser menor a la hora inicial"; 
//    			view = btnqh248a_h_fin; 
//    			error = true; 
//    			return false;
//        	}	
//    	} 
//	    return true;
//	}
//	
//	
//	private void cargarDatos() {      
//		persona = getCuestionarioService().getSeccion01(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,persona.persona_id,seccionesCargado);
//		if (persona == null) {		
//			persona = new Seccion01();
//			persona.id = App.getInstance().getHogar().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.persona_id = persona.persona_id;
//		}
//		fecha_referencia = persona.qh243_ref;
//		MyUtil.llenarPersonasInformanteVacunas(this.getActivity() , getServiceSeccion01(), spnQH240, persona.id, persona.hogar_id);
//		if(persona.qh242!=null)	{
//			persona.qh242=persona.setConvertqh8a4(persona.qh242);
//		}
//		if(persona.qh246!=null)	{
//			persona.qh246=persona.setConvertqh8a6(persona.qh246);
//		}
//		if(persona.qh247!=null)	{
//			persona.qh247=persona.setConvertqh98a10(persona.qh247);
//		}
//		
//		if(persona.qh240a!=null) {lblpre240a_h_ini_f.setText(persona.qh240a);}		
//		if(persona.qh248a!=null) {lblpre248a_h_fin_f.setText(persona.qh248a);}
//		
//		if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0){btnqh248a_h_fin.setEnabled(true);}else{btnqh248a_h_fin.setEnabled(false);}
//		
//		entityToUI(persona);
//		inicio();
//    }
//
//	public void inicio(){		
//	
//		renombrarNombres();
//		onqh241_ChangeValue();	
//		
//		ValidarsiesSupervisora();
//		spnQH240.requestFocus();
//	}
//	
//	public boolean verificarPregunta245(){
//		if(!chbQH245_A.isChecked() && !chbQH245_B.isChecked() && !chbQH245_C.isChecked() && !chbQH245_D.isChecked() && !chbQH245_E.isChecked() && !chbQH245_Y.isChecked())
//		return false;
//		else
//			return true;
//	}
//	
//
//	
//	public void onqh241_ChangeValue(){
//		if(Integer.parseInt(rgQH241.getTagSelected("0").toString())==1){
//			Util.lockView(getActivity(),false ,rgQH242, 
//					txtQH243_D1, txtQH243_M1, txtQH243_A1, txtQH243_L1, rgQH244_I1, rgQH244_V1, 
//					txtQH243_D2, txtQH243_M2, txtQH243_A2, txtQH243_L2, rgQH244_I2, rgQH244_V2, 
//					txtQH243_D3, txtQH243_M3, txtQH243_A3, txtQH243_L3, rgQH244_I3, rgQH244_V3,
//					txtQH243_D4, txtQH243_M4, txtQH243_A4, txtQH243_L4, rgQH244_I4, rgQH244_V4,
//					txtQH243_D5, txtQH243_M5, txtQH243_A5, txtQH243_L5, rgQH244_I5, rgQH244_V5,
//					txtQH243_D6, txtQH243_M6, txtQH243_A6, txtQH243_L6, rgQH244_I6, rgQH244_V6,
//					chbQH245_A, chbQH245_B, chbQH245_C, chbQH245_D, chbQH245_E, chbQH245_Y, rgQH246, 
//					chbQH243_DS1, chbQH243_DS2, chbQH243_DS3, chbQH243_DS4, chbQH243_DS5, chbQH243_DS6);
//			q2.setVisibility(View.VISIBLE);
//			q3.setVisibility(View.VISIBLE);
//			q5.setVisibility(View.VISIBLE);
//			q6.setVisibility(View.VISIBLE);
//			Util.cleanAndLockView(getActivity(),rgQH247, txtQH247_O);
//			q7.setVisibility(View.GONE);			
//			
//			onqh242_ChangeValue();
//			onqh243_ds1ChangeValue();
//			onqh243_ds2ChangeValue();
//			onqh243_ds3ChangeValue();
//			onqh243_ds4ChangeValue();
//			onqh243_ds5ChangeValue();
//			onqh243_ds6ChangeValue();		
//			
//			if(verificarCheck()){
//				onchbQH245_AChangeValue();
//				}
//			if(chbQH245_E.isChecked()){onchbQH245_EChangeValue();}
//			if(chbQH245_Y.isChecked()){onchbQH245_YChangeValue();}
//			
//			
//			rgQH242.requestFocus();
//		}
//		else{
//			Util.cleanAndLockView(getActivity(),rgQH242, 
//					txtQH243_D1, txtQH243_M1, txtQH243_A1, txtQH243_L1, rgQH244_I1, rgQH244_V1, txtQH244_VO1, 
//					txtQH243_D2, txtQH243_M2, txtQH243_A2, txtQH243_L2, rgQH244_I2, rgQH244_V2, txtQH244_VO2, 
//					txtQH243_D3, txtQH243_M3, txtQH243_A3, txtQH243_L3, rgQH244_I3, rgQH244_V3, txtQH244_VO3,
//					txtQH243_D4, txtQH243_M4, txtQH243_A4, txtQH243_L4, rgQH244_I4, rgQH244_V4, txtQH244_VO4,
//					txtQH243_D5, txtQH243_M5, txtQH243_A5, txtQH243_L5, rgQH244_I5, rgQH244_V5, txtQH244_VO5,
//					txtQH243_D6, txtQH243_M6, txtQH243_A6, txtQH243_L6, rgQH244_I6, rgQH244_V6, txtQH244_VO6,
//					chbQH245_A, chbQH245_B, chbQH245_C, chbQH245_D, chbQH245_E, chbQH245_Y, rgQH246, 
//					chbQH243_DS1, chbQH243_DS2, chbQH243_DS3, chbQH243_DS4, chbQH243_DS5, chbQH243_DS6);
//			q2.setVisibility(View.GONE);
//			q3.setVisibility(View.GONE);
//			q5.setVisibility(View.GONE);
//			q6.setVisibility(View.GONE);
//			Util.lockView(getActivity(),false ,rgQH247); 
//			q7.setVisibility(View.VISIBLE);
//			rgQH247.requestFocus();
//		}
//	}
//	
//	public void onqh242_ChangeValue(){
//		if(Integer.parseInt(rgQH242.getTagSelected("0").toString())==1){
//			Integer p243_d1 = txtQH243_D1.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D1.getText().toString()):-1;
//			Integer p243_d2 = txtQH243_D2.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D2.getText().toString()):-1;
//			Integer p243_d3 = txtQH243_D3.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D3.getText().toString()):-1;
//			Integer p243_d4 = txtQH243_D4.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D4.getText().toString()):-1;
//			Integer p243_d5 = txtQH243_D5.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D5.getText().toString()):-1;
//			Integer p243_d6 = txtQH243_D6.getText().toString().trim().length()>0?Integer.parseInt(txtQH243_D6.getText().toString()):-1;
//			Util.lockView(getActivity(),false , 
//												txtQH243_D1,txtQH243_D2,txtQH243_D3,txtQH243_D4,txtQH243_D5,txtQH243_D6,
//												txtQH243_L1,txtQH243_L2,txtQH243_L3,txtQH243_L4,txtQH243_L5,txtQH243_L6
//												);
//			if(p243_d1==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M1, txtQH243_A1);
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M1, txtQH243_A1);
//			}
//			if(p243_d2==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M2, txtQH243_A2);			
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M2, txtQH243_A2);
//			}
//			if(p243_d3==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M3, txtQH243_A3);
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M3, txtQH243_A3);
//			}
//			if(p243_d4==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M4, txtQH243_A4);
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M4, txtQH243_A4);
//			}
//			if(p243_d5==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M5, txtQH243_A5);
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M5, txtQH243_A5);
//			}
//			if(p243_d6==44){
//				Util.cleanAndLockView(getActivity(), txtQH243_M6, txtQH243_A6);
//			}
//			else{
//				Util.lockView(getActivity(),false , txtQH243_M6, txtQH243_A6);
//			}
//			txtQH243_D1.requestFocus();
//			
//			rgQH244_I1.lockButtons(false,0);
//			rgQH244_I2.lockButtons(false,0);
//			rgQH244_I3.lockButtons(false,0);
//			rgQH244_I4.lockButtons(false,0);
//			rgQH244_I5.lockButtons(false,0);
//			rgQH244_I6.lockButtons(false,0);
//		}
//		else{
//			Util.cleanAndLockView(getActivity(), 
//					txtQH243_D1, txtQH243_M1, txtQH243_A1, txtQH243_L1, 
//					txtQH243_D2, txtQH243_M2, txtQH243_A2, txtQH243_L2, 
//					txtQH243_D3, txtQH243_M3, txtQH243_A3, txtQH243_L3,
//					txtQH243_D4, txtQH243_M4, txtQH243_A4, txtQH243_L4,
//					txtQH243_D5, txtQH243_M5, txtQH243_A5, txtQH243_L5,
//					txtQH243_D6, txtQH243_M6, txtQH243_A6, txtQH243_L6
//					);
//			rgQH244_I1.lockButtons(true,0);
//			rgQH244_I2.lockButtons(true,0);
//			rgQH244_I3.lockButtons(true,0);
//			rgQH244_I4.lockButtons(true,0);
//			rgQH244_I5.lockButtons(true,0);
//			rgQH244_I6.lockButtons(true,0);
//			rgQH244_I1.requestFocus();
//		}
//	}
//	
//
//	
//	public void onqh243_ds1ChangeValue() {		
//		if(chbQH243_DS1.isChecked()){
//			lblpreguntaqh243_n1.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d1.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m1.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a1.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l1.setVisibility(View.VISIBLE);
//			txtQH243_D1.setVisibility(View.VISIBLE);
//			txtQH243_M1.setVisibility(View.VISIBLE);
//			txtQH243_A1.setVisibility(View.VISIBLE);
//			txtQH243_L1.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_1.setVisibility(View.VISIBLE);
//			rgQH244_I1.setVisibility(View.VISIBLE);
//			rgQH244_V1.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D1, txtQH243_M1,txtQH243_A1,txtQH243_L1, rgQH244_I1,rgQH244_V1);
//			onqh242_ChangeValue();
//			onqh244_V1ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n1.setVisibility(View.GONE);
//			lblpreguntaqh243_d1.setVisibility(View.GONE);
//			lblpreguntaqh243_m1.setVisibility(View.GONE);
//			lblpreguntaqh243_a1.setVisibility(View.GONE);
//			lblpreguntaqh243_l1.setVisibility(View.GONE);
//			txtQH243_D1.setVisibility(View.GONE);
//			txtQH243_M1.setVisibility(View.GONE);
//			txtQH243_A1.setVisibility(View.GONE);
//			txtQH243_L1.setVisibility(View.GONE);
//			lblpreguntaqh244_1.setVisibility(View.GONE);
//			rgQH244_I1.setVisibility(View.GONE);
//			rgQH244_V1.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D1, txtQH243_M1,txtQH243_A1,txtQH243_L1, rgQH244_I1,rgQH244_V1);
//		}
//	}	
//	public void onqh243_ds2ChangeValue() {		
//		if(chbQH243_DS2.isChecked()){
//			lblpreguntaqh243_n2.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d2.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m2.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a2.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l2.setVisibility(View.VISIBLE);
//			txtQH243_D2.setVisibility(View.VISIBLE);
//			txtQH243_M2.setVisibility(View.VISIBLE);
//			txtQH243_A2.setVisibility(View.VISIBLE);
//			txtQH243_L2.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_2.setVisibility(View.VISIBLE);
//			rgQH244_I2.setVisibility(View.VISIBLE);
//			rgQH244_V2.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D2, txtQH243_M2,txtQH243_A2,txtQH243_L2, rgQH244_I2,rgQH244_V2);
//			onqh242_ChangeValue();
//			onqh244_V2ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n2.setVisibility(View.GONE);
//			lblpreguntaqh243_d2.setVisibility(View.GONE);
//			lblpreguntaqh243_m2.setVisibility(View.GONE);
//			lblpreguntaqh243_a2.setVisibility(View.GONE);
//			lblpreguntaqh243_l2.setVisibility(View.GONE);
//			txtQH243_D2.setVisibility(View.GONE);
//			txtQH243_M2.setVisibility(View.GONE);
//			txtQH243_A2.setVisibility(View.GONE);
//			txtQH243_L2.setVisibility(View.GONE);
//			lblpreguntaqh244_2.setVisibility(View.GONE);
//			rgQH244_I2.setVisibility(View.GONE);
//			rgQH244_V2.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D2, txtQH243_M2,txtQH243_A2,txtQH243_L2, rgQH244_I2,rgQH244_V2);
//		}
//	}
//	public void onqh243_ds3ChangeValue() {		
//		if(chbQH243_DS3.isChecked()){
//			lblpreguntaqh243_n3.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d3.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m3.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a3.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l3.setVisibility(View.VISIBLE);
//			txtQH243_D3.setVisibility(View.VISIBLE);
//			txtQH243_M3.setVisibility(View.VISIBLE);
//			txtQH243_A3.setVisibility(View.VISIBLE);
//			txtQH243_L3.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_3.setVisibility(View.VISIBLE);
//			rgQH244_I3.setVisibility(View.VISIBLE);
//			rgQH244_V3.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D3, txtQH243_M3,txtQH243_A3,txtQH243_L3, rgQH244_I3,rgQH244_V3);
//			onqh242_ChangeValue();
//			onqh244_V3ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n3.setVisibility(View.GONE);
//			lblpreguntaqh243_d3.setVisibility(View.GONE);
//			lblpreguntaqh243_m3.setVisibility(View.GONE);
//			lblpreguntaqh243_a3.setVisibility(View.GONE);
//			lblpreguntaqh243_l3.setVisibility(View.GONE);
//			txtQH243_D3.setVisibility(View.GONE);
//			txtQH243_M3.setVisibility(View.GONE);
//			txtQH243_A3.setVisibility(View.GONE);
//			txtQH243_L3.setVisibility(View.GONE);
//			lblpreguntaqh244_3.setVisibility(View.GONE);
//			rgQH244_I3.setVisibility(View.GONE);
//			rgQH244_V3.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D3, txtQH243_M3,txtQH243_A3,txtQH243_L3, rgQH244_I3,rgQH244_V3);
//		}
//	}
//	
//	public void onqh243_ds4ChangeValue() {		
//		if(chbQH243_DS4.isChecked()){
//			lblpreguntaqh243_n4.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d4.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m4.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a4.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l4.setVisibility(View.VISIBLE);
//			txtQH243_D4.setVisibility(View.VISIBLE);
//			txtQH243_M4.setVisibility(View.VISIBLE);
//			txtQH243_A4.setVisibility(View.VISIBLE);
//			txtQH243_L4.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_4.setVisibility(View.VISIBLE);
//			rgQH244_I4.setVisibility(View.VISIBLE);
//			rgQH244_V4.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D4, txtQH243_M4,txtQH243_A4,txtQH243_L4, rgQH244_I4,rgQH244_V4);
//			onqh242_ChangeValue();
//			onqh244_V4ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n4.setVisibility(View.GONE);
//			lblpreguntaqh243_d4.setVisibility(View.GONE);
//			lblpreguntaqh243_m4.setVisibility(View.GONE);
//			lblpreguntaqh243_a4.setVisibility(View.GONE);
//			lblpreguntaqh243_l4.setVisibility(View.GONE);
//			txtQH243_D4.setVisibility(View.GONE);
//			txtQH243_M4.setVisibility(View.GONE);
//			txtQH243_A4.setVisibility(View.GONE);
//			txtQH243_L4.setVisibility(View.GONE);
//			lblpreguntaqh244_4.setVisibility(View.GONE);
//			rgQH244_I4.setVisibility(View.GONE);
//			rgQH244_V4.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D4, txtQH243_M4,txtQH243_A4,txtQH243_L4, rgQH244_I4,rgQH244_V4);
//		}
//	}
//	
//	public void onqh243_ds5ChangeValue() {		
//		if(chbQH243_DS5.isChecked()){
//			lblpreguntaqh243_n5.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d5.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m5.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a5.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l5.setVisibility(View.VISIBLE);
//			txtQH243_D5.setVisibility(View.VISIBLE);
//			txtQH243_M5.setVisibility(View.VISIBLE);
//			txtQH243_A5.setVisibility(View.VISIBLE);
//			txtQH243_L5.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_5.setVisibility(View.VISIBLE);
//			rgQH244_I5.setVisibility(View.VISIBLE);
//			rgQH244_V5.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D5, txtQH243_M5,txtQH243_A5,txtQH243_L5, rgQH244_I5,rgQH244_V5);
//			onqh242_ChangeValue();
//			onqh244_V5ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n5.setVisibility(View.GONE);
//			lblpreguntaqh243_d5.setVisibility(View.GONE);
//			lblpreguntaqh243_m5.setVisibility(View.GONE);
//			lblpreguntaqh243_a5.setVisibility(View.GONE);
//			lblpreguntaqh243_l5.setVisibility(View.GONE);
//			txtQH243_D5.setVisibility(View.GONE);
//			txtQH243_M5.setVisibility(View.GONE);
//			txtQH243_A5.setVisibility(View.GONE);
//			txtQH243_L5.setVisibility(View.GONE);
//			lblpreguntaqh244_5.setVisibility(View.GONE);
//			rgQH244_I5.setVisibility(View.GONE);
//			rgQH244_V5.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D5, txtQH243_M5,txtQH243_A5,txtQH243_L5, rgQH244_I5,rgQH244_V5);
//		}
//	}
//	
//	public void onqh243_ds6ChangeValue() {		
//		if(chbQH243_DS6.isChecked()){
//			lblpreguntaqh243_n6.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_d6.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_m6.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_a6.setVisibility(View.VISIBLE);
//			lblpreguntaqh243_l6.setVisibility(View.VISIBLE);
//			txtQH243_D6.setVisibility(View.VISIBLE);
//			txtQH243_M6.setVisibility(View.VISIBLE);
//			txtQH243_A6.setVisibility(View.VISIBLE);
//			txtQH243_L6.setVisibility(View.VISIBLE);
//			lblpreguntaqh244_6.setVisibility(View.VISIBLE);
//			rgQH244_I6.setVisibility(View.VISIBLE);
//			rgQH244_V6.setVisibility(View.VISIBLE);
//			Util.lockView(getActivity(),false , txtQH243_D6, txtQH243_M6,txtQH243_A6,txtQH243_L6, rgQH244_I6,rgQH244_V6);
//			onqh242_ChangeValue();
//			onqh244_V6ChangeValue();
//		}
//		else{
//			lblpreguntaqh243_n6.setVisibility(View.GONE);
//			lblpreguntaqh243_d6.setVisibility(View.GONE);
//			lblpreguntaqh243_m6.setVisibility(View.GONE);
//			lblpreguntaqh243_a6.setVisibility(View.GONE);
//			lblpreguntaqh243_l6.setVisibility(View.GONE);
//			txtQH243_D6.setVisibility(View.GONE);
//			txtQH243_M6.setVisibility(View.GONE);
//			txtQH243_A6.setVisibility(View.GONE);
//			txtQH243_L6.setVisibility(View.GONE);
//			lblpreguntaqh244_6.setVisibility(View.GONE);
//			rgQH244_I6.setVisibility(View.GONE);
//			rgQH244_V6.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(),txtQH243_D6, txtQH243_M6,txtQH243_A6,txtQH243_L6, rgQH244_I6,rgQH244_V6);
//		}
//	}
//	
//	public void onqh244_V1ChangeValue(){
//		if(Integer.parseInt(rgQH244_I1.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I1.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V1);
//			rgQH244_V1.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V1.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V1, txtQH244_VO1);
//		}
//	}
//	
//	public void onqh244_V2ChangeValue(){
//		if(Integer.parseInt(rgQH244_I2.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I2.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V2);
//			rgQH244_V2.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V2.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V2, txtQH244_VO2);
//		}
//	}
//	
//	public void onqh244_V3ChangeValue(){
//		if(Integer.parseInt(rgQH244_I3.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I3.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V3);
//			rgQH244_V3.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V3.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V3, txtQH244_VO3);
//		}
//	}
//	public void onqh244_V4ChangeValue(){
//		if(Integer.parseInt(rgQH244_I4.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I4.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V4);
//			rgQH244_V4.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V4.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V4, txtQH244_VO4);
//		}
//	}
//	public void onqh244_V5ChangeValue(){
//		if(Integer.parseInt(rgQH244_I5.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I5.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V5);
//			rgQH244_V5.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V5.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V5, txtQH244_VO5);
//		}
//	}
//	public void onqh244_V6ChangeValue(){
//		if(Integer.parseInt(rgQH244_I6.getTagSelected("0").toString())==1 || Integer.parseInt(rgQH244_I6.getTagSelected("0").toString())==2){
//			Util.lockView(getActivity(),false , rgQH244_V6);
//			rgQH244_V6.setVisibility(View.VISIBLE);			
//		}
//		else{
//			rgQH244_V6.setVisibility(View.GONE);
//			Util.cleanAndLockView(getActivity(), rgQH244_V6, txtQH244_VO6);
//		}
//	}
//
//	
//	
//	
//	 public boolean verificarCheck() {
//	  		if (chbQH245_A.isChecked() || chbQH245_B.isChecked() || chbQH245_C.isChecked() ||chbQH245_D.isChecked()) {
//	  			return true;
//	  		}else{			
//	  			return false;
//	  		}
//	  	}
// 	public boolean verificarCheck1() {
//  		if (chbQH245_E.isChecked() || chbQH245_Y.isChecked() ) {
//  			return true;
//  		}else{			
//  			return false;
//  		}
//  	}
//	
//	public void onchbQH245_AChangeValue() {		
//  		if (verificarCheck()) {	  		
//  			Util.cleanAndLockView(getActivity(),chbQH245_E,chbQH245_Y);
//  		
//  		} else {			
//  			Util.lockView(getActivity(), false,chbQH245_E,chbQH245_Y);
//  		}	
//  		validarPregunta246();
//  	}
//	public void onchbQH245_BChangeValue() {		
//  		if (verificarCheck()) {	  		
//  			Util.cleanAndLockView(getActivity(),chbQH245_E,chbQH245_Y);
//  		
//  		} else {			
//  			Util.lockView(getActivity(), false,chbQH245_E,chbQH245_Y);
//  		}	
//  		validarPregunta246();
//  	}
//	public void onchbQH245_CChangeValue() {		
//  		if (verificarCheck()) {	  		
//  			Util.cleanAndLockView(getActivity(),chbQH245_E,chbQH245_Y);
//  		
//  		} else {			
//  			Util.lockView(getActivity(), false,chbQH245_E,chbQH245_Y);
//  		}		
//  		validarPregunta246();
//  	}
//	public void onchbQH245_DChangeValue() {		
//  		if (verificarCheck()) {	  		
//  			Util.cleanAndLockView(getActivity(),chbQH245_E,chbQH245_Y);
//  		
//  		} else {			
//  			Util.lockView(getActivity(), false,chbQH245_E,chbQH245_Y);
//  		}		
//  		validarPregunta246();
//  	}
// 	public void onchbQH245_EChangeValue() {		
//  		if (verificarCheck1()) {
//  			Util.cleanAndLockView(getActivity(),chbQH245_A,chbQH245_B,chbQH245_C,chbQH245_D,chbQH245_Y);
//  			
//  		} else {		
//  			Util.lockView(getActivity(), false,chbQH245_A,chbQH245_B,chbQH245_C,chbQH245_D,chbQH245_Y);
//  		}		
//  		validarPregunta246();
//  	}
// 	public void onchbQH245_YChangeValue() {		
//  		if (verificarCheck1()) {
//  			Util.cleanAndLockView(getActivity(),chbQH245_A,chbQH245_B,chbQH245_C,chbQH245_D,chbQH245_E);
//  			
//  		} else {	
//  			Util.lockView(getActivity(), false,chbQH245_A,chbQH245_B,chbQH245_C,chbQH245_D,chbQH245_E);
//  		}
//  		validarPregunta246();
//  	}
// 	
// 	public void validarPregunta246() {	
//  		if(chbQH245_C.isChecked() || chbQH245_D.isChecked()){
//  			Util.lockView(getActivity(), false,rgQH246);
//  			q6.setVisibility(View.VISIBLE);  			
//  		}
//  		else{
//  			Util.cleanAndLockView(getActivity(),rgQH246);
//  			q6.setVisibility(View.GONE);
//  		}
//  	}
//	
//	public void renombrarNombres() {
//		String replace="(NOMBRE)";
//		String Ud="Usted";
//		Integer persona_sel=-1;
//		persona_sel= (spnQH240.getSelectedItemKey()==null?-1:Integer.parseInt(spnQH240.getSelectedItemKey().toString()));
//		
//		if(persona_sel!=-1){
//			lblpreguntaqh241.setText(getResources().getString(R.string.vacuna_p241));
//			lblpreguntaqh241.setText(lblpreguntaqh241.getText().toString().replace(replace,persona_sel==persona.persona_id?Ud:persona.qh02_1));
//			lblpreguntaqh242.setText(getResources().getString(R.string.vacuna_p242));
//			lblpreguntaqh242.setText(lblpreguntaqh242.getText().toString().replace(replace,persona_sel==persona.persona_id?Ud:persona.qh02_1));
//			lblpreguntaqh245.setText(getResources().getString(R.string.vacuna_p245));
//			lblpreguntaqh245.setText(lblpreguntaqh245.getText().toString().replace(replace,persona_sel==persona.persona_id?Ud:persona.qh02_1));
//			lblpreguntaqh246.setText(getResources().getString(R.string.vacuna_p246));
//			lblpreguntaqh246.setText(lblpreguntaqh246.getText().toString().replace(replace,persona_sel==persona.persona_id?Ud:persona.qh02_1));
//			lblpreguntaqh247.setText(getResources().getString(R.string.vacuna_p247));
//			lblpreguntaqh247.setText(lblpreguntaqh247.getText().toString().replace(replace,persona_sel==persona.persona_id?Ud:persona.qh02_1));	
//		}
//		
//	}
//	
//	public void OcultarTecla() {
//		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(rgQH241.getWindowToken(), 0);
//	}
//	
//
//	
//	public void ValidarsiesSupervisora(){
//	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		spnQH240.readOnly();
//	    		rgQH241.readOnly();
//	    		rgQH242.readOnly();
//	    		
//	    		txtQH243_D1.readOnly();
//	    		txtQH243_M1.readOnly();
//	    		txtQH243_A1.readOnly();
//	    		txtQH243_L1.readOnly();
//	    		rgQH244_I1.readOnly();
//	    		rgQH244_V1.readOnly();
//	    		txtQH244_VO1.readOnly();
//	    		
//	    		txtQH243_D2.readOnly();
//	    		txtQH243_M2.readOnly();
//	    		txtQH243_A2.readOnly();
//	    		txtQH243_L2.readOnly();
//	    		rgQH244_I2.readOnly();
//	    		rgQH244_V2.readOnly();
//	    		txtQH244_VO2.readOnly();
//	    		
//	    		txtQH243_D3.readOnly();
//	    		txtQH243_M3.readOnly();
//	    		txtQH243_A3.readOnly();
//	    		txtQH243_L3.readOnly();
//	    		rgQH244_I3.readOnly();
//	    		rgQH244_V3.readOnly();
//	    		txtQH244_VO3.readOnly();
//	    		
//	    		txtQH243_D4.readOnly();
//	    		txtQH243_M4.readOnly();
//	    		txtQH243_A4.readOnly();
//	    		txtQH243_L4.readOnly();
//	    		rgQH244_I4.readOnly();
//	    		rgQH244_V4.readOnly();
//	    		txtQH244_VO4.readOnly();
//	    		
//	    		txtQH243_D5.readOnly();
//	    		txtQH243_M5.readOnly();
//	    		txtQH243_A5.readOnly();
//	    		txtQH243_L5.readOnly();
//	    		rgQH244_I5.readOnly();
//	    		rgQH244_V5.readOnly();
//	    		txtQH244_VO5.readOnly();
//	    		
//	    		txtQH243_D6.readOnly();
//	    		txtQH243_M6.readOnly();
//	    		txtQH243_A6.readOnly();
//	    		txtQH243_L6.readOnly();
//	    		rgQH244_I6.readOnly();
//	    		rgQH244_V6.readOnly();
//	    		txtQH244_VO6.readOnly();
//	    		
//	    		chbQH245_A.readOnly();
//	    		chbQH245_B.readOnly();
//	    		chbQH245_C.readOnly();
//	    		chbQH245_D.readOnly();
//	    		chbQH245_E.readOnly();
//	    		chbQH245_Y.readOnly();
//	    		rgQH246.readOnly();
//	    		rgQH247.readOnly();
//	    		txtQH247_O.readOnly();
//	    		txtQHVACUNAS_OBS.setEnabled(false);
//	    		btnAceptar.setEnabled(false);
//	    		btnqh240a_h_ini.setEnabled(false);
//				btnqh248a_h_fin.setEnabled(false);
//	    	}
//	}
//	    
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//	
//	public VisitaService getVisitaService()
//	{
//		if(visitaService==null)
//		{
//				visitaService = VisitaService.getInstance(getActivity());
//		}
//			return visitaService;
//	}
//	private CuestionarioService getService() {
//		if (hogarService == null) {
//			hogarService = CuestionarioService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	public UbigeoService getUbigeoService()
//	{
//		if(ubigeoService==null)		{
//			ubigeoService = UbigeoService.getInstance(getActivity());
//		}
//			return ubigeoService;
//	}
//	public Seccion01Service getServiceSeccion01() {
//		if (serviceSeccion01 == null) {
//			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
//		}
//		return serviceSeccion01;
//	}
//}
