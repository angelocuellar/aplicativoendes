package gob.inei.endes2024.fragment.seccion01.Dialog;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DateTimeField.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_001;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion01_001_1Dialog extends DialogFragmentComponent {
	@FieldAnnotation(orderIndex=1)
	public TextField txtQH02_1;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQH02_2;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQH02_3;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQH03;     
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQH04;     
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQH05; 
	@FieldAnnotation(orderIndex=7)          
	public RadioGroupOtherField rgQH06;     
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQH07;
	@FieldAnnotation(orderIndex=9)          
	public CheckBoxField chbQH07; 
	@FieldAnnotation(orderIndex=10) 
	public DateTimeField txtQH7DD;
	@FieldAnnotation(orderIndex=11)
	public DateTimeField txtQH7MM;
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQH08;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQH11_A; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQH11_B; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQH11_C; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQH11_D; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQH11_E; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQH11_Y; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQH11_Z; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQH12;
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQH13;
	@FieldAnnotation(orderIndex=22) 
	public ButtonComponent btnAceptar; 
    public ButtonComponent btnNosabedia,btnNosabemes;   
           
	Seccion01 bean; 
	private static Seccion01Fragment_001 caller;
	public ButtonComponent  btnCancelar;
	private SeccionCapitulo[] seccionesCargado,seccionesGrabado3a14,seccionesGrabado0A2,seccionesGrabado15A24,seccionesGrabado25A97,seccionesGrabadoClear;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	private Seccion01Service seccion01Service;
	public GridComponent2 gridNombres,gridPregunta07,gridPregunta07A;
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco,lblviveaqui,lblDurmioAqui,lblhombreomujer,lblcumpleanios,lblenanios,lbldia,lblmes,lblafiliado,lbltitular,lblpregunta13,lblpregunta07_1;
	public LabelComponent lblpregunta02,lblpregunta07,lblpregunta08;
	public boolean validarCheck=false;
	
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4; 
	LinearLayout q5;                       
	LinearLayout q6;                           
	LinearLayout q7;                                               
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11;
	
	
	public static Seccion01_001_1Dialog newInstance(FragmentForm pagina, Seccion01 detalle) {
		caller = (Seccion01Fragment_001) pagina;
		Seccion01_001_1Dialog f = new Seccion01_001_1Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
       
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion01) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		getDialog().setTitle("N� de Orden:  " + bean.persona_id);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;
	}

	public Seccion01_001_1Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","ID","HOGAR_ID","PERSONA_ID","QHINFO") };
		seccionesGrabado0A2 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A") };
		seccionesGrabado3a14 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13") };		
		seccionesGrabado15A24 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH22","QH23","QH24","QH25") };
		seccionesGrabado25A97 = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","QH22","QH23","QH24","QH25") };
		seccionesGrabadoClear = new SeccionCapitulo[] { new SeccionCapitulo(0,-1,-1,"QH01", "QH02_1", "QH02_2", "QH02_3", "QH03", "QH04", "QH05", "QH06", "QH07", "QH7DD", "QH7MM", "QH08","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH12","QH13","QH14","QH15N","QH15Y","QH15G","QH16","QH17","QH18N","QH18Y","QH18G","QH19","QH20N","QH20Y","QH20G","QH21","QH21A","QH22","QH23","QH24","QH25","QH26A1","QH26A2","QH26A3","QH26A4","QH26A5","QH26A6") };
	}            

	@Override
	protected void buildFields() {
		lblnombre = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_1).size(altoComponente, 350).textSize(16);
		lblApPaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_2).size(altoComponente, 350).textSize(16);
		lblApMaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_3).size(altoComponente, 350).textSize(16);
		lblpregunta02 = new LabelComponent(this.getActivity()).text(R.string.seccion01nombres_ediat).size(MATCH_PARENT, MATCH_PARENT).textSize(16);
		lblpregunta07 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh07).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta07_1 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh07_0).size(MATCH_PARENT, MATCH_PARENT).textSize(18).negrita();
		lblpregunta08 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh08).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		txtQH02_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
		txtQH02_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
		txtQH02_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
		lblparentesco= new LabelComponent(this.getActivity()).text(R.string.seccion01qh03).size(MATCH_PARENT, MATCH_PARENT).textSize(20);
		rgQH03 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh03_1,R.string.seccion01qh03_2,R.string.seccion01qh03_3,R.string.seccion01qh03_4,R.string.seccion01qh03_5,R.string.seccion01qh03_6,R.string.seccion01qh03_7,R.string.seccion01qh03_8,R.string.seccion01qh03_9,R.string.seccion01qh03_10,R.string.seccion01qh03_11,R.string.seccion01qh03_12).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH03Block");
		rgQH04=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh04_1,R.string.seccion01qh04_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH05=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh05_1,R.string.seccion01qh05_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar(); 
		rgQH06=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh06_1,R.string.seccion01qh06_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lblenanios = new LabelComponent(this.getActivity()).size(60, 120).text(R.string.seccion01qh07_1).textSize(18).centrar();
		txtQH07=new IntegerField(this.getActivity()).size(60, 60).maxLength(2).callback("ontxtQH7ChangeValue");
		chbQH07=new CheckBoxField(this.getActivity(), R.string.seccion01qh07_2, "1:0").size(70, 400).callback("onchbQH7ChangeValue");
		lbldia = new LabelComponent(this.getActivity()).size(60, 200).text(R.string.seccion01qh7dd).textSize(18).centrar();
		txtQH7DD=new DateTimeField(this.getActivity(),TIPO_DIALOGO.FECHA ,"dd").size(altoComponente+20, 80).showObject(DateTimeField.SHOW_HIDE.MONTH_YEAR);
		txtQH7MM = new DateTimeField(this.getActivity(),TIPO_DIALOGO.FECHA ,"MM").size(altoComponente+20, 80).showObject(DateTimeField.SHOW_HIDE.DAY_YEAR);
		lblmes = new LabelComponent(this.getActivity()).size(60, 120).text(R.string.seccion01qh7mm).textSize(18).centrar();
		rgQH08=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh08_1,R.string.seccion01qh08_2,R.string.seccion01qh08_3,R.string.seccion01qh08_4,R.string.seccion01qh08_5,R.string.seccion01qh08_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onrgQH08ChangeValue");
		Spanned texto11 = Html.fromHtml("11. �(NOMBRE) est� afiliado o inscrito en: ESSALUD, Seguro Integral de Salud o en alg�n otro seguro de salud?");
		lblafiliado = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(20);
		lblafiliado.setText(texto11);
		lbltitular = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(20).text(R.string.seccion01qh12);
		lblpregunta13 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(20).text(R.string.seccion01qh13);
		chbQH11_A=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_AChangeValue"); 
		chbQH11_B=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_BChangeValue"); 
		chbQH11_C=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_CChangeValue"); 
		chbQH11_D=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_DChangeValue"); 
		chbQH11_E=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_EChangeValue"); 
		chbQH11_Y=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_YChangeValue"); 
		chbQH11_Z=new CheckBoxField(this.getActivity(), R.string.seccion01qh11_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onchbQH11_ZChangeValue"); 
		rgQH12=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh12_1,R.string.seccion01qh12_2,R.string.seccion01qh12_3).size(70,800).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		rgQH13=new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh13_1,R.string.seccion01qh13_2,R.string.seccion01qh13_3,R.string.seccion01qh13_4,R.string.seccion01qh13_5,R.string.seccion01qh13_6,R.string.seccion01qh13_7,R.string.seccion01qh13_8,R.string.seccion01qh13_9,R.string.seccion01qh13_10).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		lblviveaqui = new LabelComponent(getActivity()).text(R.string.seccion01qh04).size(MATCH_PARENT,MATCH_PARENT).textSize(20);             
		lblDurmioAqui = new LabelComponent(getActivity()).text(R.string.seccion01qh05).size(MATCH_PARENT,MATCH_PARENT).textSize(20);
		lblhombreomujer  = new LabelComponent(getActivity()).text(R.string.seccion01qh06).size(MATCH_PARENT,MATCH_PARENT).textSize(20);
		lblcumpleanios= new LabelComponent(getActivity()).text(R.string.seccion01qh07A).size(MATCH_PARENT, MATCH_PARENT).textSize(20);
		btnNosabedia = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01qh7anosabe).size(150, 20);
		btnNosabemes = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01qh7anosabe).size(150, 20);
		gridNombres = new GridComponent2(this.getActivity(),App.ESTILO, 2);
		gridNombres.addComponent(lblnombre);
		gridNombres.addComponent(txtQH02_1);
		gridNombres.addComponent(lblApPaterno);
		gridNombres.addComponent(txtQH02_2);
		gridNombres.addComponent(lblApMaterno);
		gridNombres.addComponent(txtQH02_3);
             
		gridPregunta07=new GridComponent2(this.getActivity(),3);
		gridPregunta07.addComponent(lblenanios);
		gridPregunta07.addComponent(txtQH07);
		gridPregunta07.addComponent(chbQH07);
             
		gridPregunta07A= new GridComponent2(this.getActivity(),3);
		gridPregunta07A.addComponent(lbldia);
		gridPregunta07A.addComponent(txtQH7DD);
		gridPregunta07A.addComponent(btnNosabedia);
		gridPregunta07A.addComponent(lblmes);
		gridPregunta07A.addComponent(txtQH7MM);
		gridPregunta07A.addComponent(btnNosabemes);
		btnNosabedia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

                   txtQH7DD.setText("98");
			}
		} );
		btnNosabemes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			         txtQH7MM.setText("98");
			}
		});
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
			bean.restoreFromMemento(caretaker.get("antes"));
			Seccion01_001_1Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarPersonas(bean);
				if (!Util.esDiferente(bean.qh04,2,2) && !Util.esDiferente(bean.qh05,2,2)) {
					Seccion01_001_1Dialog.this.dismiss();
				}
				else {
					Seccion01_001_1Dialog.this.dismiss();
					EditarDetallePersona(bean);
				} 
			}
		});    
	}
       
       
	public void EditarDetallePersona(Seccion01 tmp) {
		if (MyUtil.incluyeRango(0,2, bean.qh07)) {
			FragmentManager fm = Seccion01_001_1Dialog.this.getFragmentManager();
			Seccion01_001_3Dialog aperturaDialog = Seccion01_001_3Dialog.newInstance(caller, tmp);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
		}
		else{
			FragmentManager fm = Seccion01_001_1Dialog.this.getFragmentManager();
			Seccion01_001_2Dialog aperturaDialog = Seccion01_001_2Dialog.newInstance(caller, tmp);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
		}     
	}
              
	public Integer combinaPregunta07() {
		Integer p07=null;
		if (txtQH07.getText().toString().trim().length()!=0) {
			p07= Integer.parseInt(txtQH07.getText().toString());
		}
		if (chbQH07.getValue().toString().equals("1")) {
			p07= bean.getConvertqh7(Integer.parseInt(chbQH07.getValue().toString()));
		}
		return p07;         
	}
       
	public void muestraPregunta07(Integer p07) {
		if (p07<=96) {
			txtQH07.setText(p07.toString()); 
			chbQH07.setChecked(false);
		}           
		else {
			if (p07==97) {          
				chbQH07.setChecked(true);
				txtQH07.setText("");
			}                        
		} 
	}
       
       
	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(0,lblafiliado);
		q1 = createQuestionSection(lblpregunta02,gridNombres.component());
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblparentesco,rgQH03);
		q3 = createQuestionSection(lblviveaqui,rgQH04);
		q4 = createQuestionSection(lblDurmioAqui,rgQH05); 
		q5 = createQuestionSection(lblhombreomujer,rgQH06);                       
		q6 = createQuestionSection(lblpregunta07,lblpregunta07_1,gridPregunta07.component());                           
		q7 = createQuestionSection(lblcumpleanios,gridPregunta07A.component());                                               
		q8 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta08,rgQH08); 
		q9 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,q0,chbQH11_A,chbQH11_B,chbQH11_C,chbQH11_D,chbQH11_E,chbQH11_Y,chbQH11_Z); 
		q10 = createQuestionSection(lbltitular,rgQH12); 
		q11 = createQuestionSection(lblpregunta13,rgQH13);           
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q9); 
		form.addView(q10); 
		form.addView(q11); 
		form.addView(botones);
		return contenedor;           
	}
       
	public boolean grabar() {
		uiToEntity(bean);
		bean.qh07=combinaPregunta07();
		if (bean.qh12!=null) {
			bean.qh12=bean.getConvertqh12(bean.qh12);
		}
		if (bean.qh13!=null) {
			bean.qh13=bean.getConvertqh13(bean.qh13);
		}    		
		
		if(bean.qh7dd!=null){
			bean.qh7dd=txtQH7DD.getText().toString().length()>1?txtQH7DD.getText().toString():"0"+txtQH7DD.getText().toString();
			bean.qh7mm=txtQH7MM.getText().toString().length()>1?txtQH7MM.getText().toString():"0"+txtQH7MM.getText().toString();
		}
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
			
		SQLiteDatabase dbTX = getService().startTX();	
		try {       
			if(bean.qh07==null) {
				
				flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabadoClear);
			}
			if (bean.qh07!=null) {
				if (MyUtil.incluyeRango(0,2,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado0A2); 
				}
				if (MyUtil.incluyeRango(3,14,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado3a14);	
				} 
				if (MyUtil.incluyeRango(15,24,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado15A24);
				}
				if (MyUtil.incluyeRango(25,97,bean.qh07)) {
				
					flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado25A97);
				}
			}
        	  
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la Persona.");
			}
					
			getService().commitTX(dbTX);
		} 
		catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
       
	public boolean validar() {
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esVacio(bean.qh02_1)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.02_1"); 
			view = txtQH02_1; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh03)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.03"); 
			view = rgQH03; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh04)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.04"); 
			view = rgQH04; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(bean.qh05)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta P.05"); 
			view = rgQH05; 
			error = true; 
			return false; 
		} 
   
		if ((!Util.esDiferente(bean.qh04,1,1) && !Util.esDiferente(bean.qh05,1,2)) || (!Util.esDiferente(bean.qh04,1,2) && !Util.esDiferente(bean.qh05,1,1))) {
    
			if (Util.esVacio(bean.qh06)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.06"); 
				view = rgQH06; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(bean.qh07)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta P.07"); 
				view = txtQH07; 
				error = true; 
				return false; 
			}
			if (!MyUtil.incluyeRango(0,11,bean.qh07)) {
				if (MyUtil.incluyeRango(12,14,bean.qh07)) {
					if (Util.esVacio(bean.qh08)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.08"); 
						view = rgQH08; 
						error = true; 
						return false; 
					}
				}
				else{
		   	   	   	if (Util.esVacio(bean.qh7dd)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.7DD"); 
		   	   	   		view = txtQH7DD; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	} 	
		   	   	   	if (Util.esVacio(bean.qh7mm)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.7MM"); 
		   	   	   		view = txtQH7MM; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	}				
		   	   	   	if (Util.esVacio(bean.qh08)) { 
		   	   	   		mensaje = preguntaVacia.replace("$", "La pregunta P.08"); 
		   	   	   		view = rgQH08; 
		   	   	   		error = true; 
		   	   	   		return false; 
		   	   	   	}
				}	
			}
			if (!Util.alMenosUnoEsDiferenteA(0,bean.qh11_a,bean.qh11_b,bean.qh11_c,bean.qh11_d,bean.qh11_e,bean.qh11_y,bean.qh11_z)) { 
				mensaje = "DEBE DE HABER UNA ALTERNATIVA SELECCIONADA"; 
				view = chbQH11_A; 
				error = true; 
				return false; 
			} 
		
			if (chbQH11_A.isChecked() || chbQH11_B.isChecked() || chbQH11_C.isChecked() || chbQH11_D.isChecked() || chbQH11_E.isChecked()) {
				if (Util.esVacio(bean.qh12)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.12"); 
					view = rgQH12; 
					error = true; 
					return false; 
				}
			}
			if (Util.esMayor(bean.qh07,6)) {
				if (Util.esVacio(bean.qh13)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta P.13"); 
					view = rgQH13; 
					error = true; 
					return false; 
				} 
			}
     	}     


    	if (MyUtil.incluyeRango(0, 11, bean.qh07) && !Util.esDiferente(bean.qh03,App.JEFE)) {
	    		mensaje ="Edad de jefe de hogar no debe ser menor a 12 a�os";
				view = txtQH07;
				error=true;
				return false;
    	}
    	if (chbQH11_A.isChecked() && chbQH11_C.isChecked()) {
    		mensaje ="La persona no puede tener seguro de salud SIS y ESSALUD";
			view = chbQH11_A;
			error=true;
			return false;
    	}
    	
    	if (!Util.esDiferente(bean.qh04,2) && !Util.esDiferente(bean.qh05,2)) {
    		mensaje ="La persona no vive habitualmente ni durmi� la noche anterior";
			view = rgQH05;
			error=true;
			return false;
    	}
    	
    	if(Util.esDiferente(bean.qh7dd, "98") && Util.esDiferente(bean.qh7mm, "98") && bean.qh7dd!=null && bean.qh7mm!=null){
    		Integer dia=Integer.parseInt(bean.qh7dd.toString());
    		Integer mes=Integer.parseInt(bean.qh7mm.toString());
    		
    		
    		if(!MyUtil.DiaCorrespondeAlMes(dia,mes)){
    			mensaje ="Dia no corresponde al mes que eligi�";
    			view = btnNosabedia;
    			error=true;
    			return false;
    		}
    	}
    	if(!Util.esDiferente(bean.qh11_a, 1) && (!Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_c, 1))){
    		mensaje ="Entidades no compatibles";
			view = chbQH11_A;
			error=true;
			return false;
    	}
    	if(!Util.esDiferente(bean.qh11_b, 1) && (!Util.esDiferente(bean.qh11_a, 1) || !Util.esDiferente(bean.qh11_c, 1) || !Util.esDiferente(bean.qh11_d, 1))){
    		mensaje ="Entidades no compatibles";
			view = chbQH11_B;
			error=true;
			return false;
    	}
    	if(!Util.esDiferente(bean.qh11_c, 1) && (!Util.esDiferente(bean.qh11_a, 1) || !Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_d, 1) || !Util.esDiferente(bean.qh11_e,1))){
    		mensaje ="Entidades no compatibles";
			view = chbQH11_C;
			error=true;
			return false;
    	}	
    	if(!Util.esDiferente(bean.qh11_d, 1) && (!Util.esDiferente(bean.qh11_b, 1) || !Util.esDiferente(bean.qh11_c, 1))){
    		mensaje ="Entidades no compatibles";
			view = chbQH11_D;
			error=true;
			return false;
    	}
    	if(!Util.esDiferente(bean.qh11_e, 1) && !Util.esDiferente(bean.qh11_c, 1)){
    		mensaje ="Entidades no compatibles";
			view = chbQH11_E;
			error=true;
			return false;
    	}
		if(caller.detalles.size()>1){
			/******** VALIDACION DE SEXO DE CONYUGUE   *******/
			if(!Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA,App.JEFE)){
				if((!Util.esDiferente(getServiceSeccion01().getSexobypersonaid(bean.id, bean.hogar_id, App.JEFE),bean.qh06) && !Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA) 
						||(!Util.esDiferente(bean.persona_id,App.JEFE) && !Util.esDiferente(getServiceSeccion01().getSexobypersonaid(bean.id, bean.hogar_id,getServiceSeccion01().getPersonaIddeEsposo(bean.id, bean.hogar_id)), bean.qh06) ))){
					mensaje ="Sexo de jefe de hogar no debe ser igual a sexo de conyugue";
					view = rgQH06;
					error=true;
					return false;
				}
			}

			/******** VALIDACION DE PADRES   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.PADRE,App.HOMBRE) && !Util.esDiferente(bean.qh06,App.HOMBRE)) {
				if(!Util.esDiferente(bean.qh03,App.PADRE) && !Util.esDiferente(bean.qh06,App.HOMBRE) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.PADRE,App.HOMBRE),bean.persona_id)){ 
					mensaje ="No debe haber m�s de 2 Padres";
					view = rgQH03;
					error=true;
					return false;
				}
			}
			/******** VALIDACION DE MADRESS   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.MADRE,App.MUJER) && !Util.esDiferente(bean.qh06,App.MUJER) ) {
				if(!Util.esDiferente(bean.qh03,App.MADRE) && !Util.esDiferente(bean.qh06,App.MUJER) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.MADRE,App.MUJER),bean.persona_id)){ 
					mensaje ="No debe haber m�s de 2 Madres";
					view = rgQH03;
					error=true;
					return false;
				}
			}
			/******** VALIDACION DE SUEGROS   *******/
			if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.SUEGRO,App.HOMBRE) && !Util.esDiferente(bean.qh06,App.HOMBRE)) {
				if(!Util.esDiferente(bean.qh03,App.SUEGRO) && !Util.esDiferente(bean.qh06,App.HOMBRE) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.SUEGRO,App.HOMBRE),bean.persona_id)){ 
					mensaje ="No debe haber m�s de 2 Suegros";
					view = rgQH03;
					error=true;
					return false;
				}
			}
	       	/******** VALIDACION DE SUEGRAS   *******/
	    	if(getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id,App.SUEGRA,App.MUJER) && !Util.esDiferente(bean.qh06,App.MUJER) ) {
	    		if(!Util.esDiferente(bean.qh03,App.SUEGRA) && !Util.esDiferente(bean.qh06,App.MUJER) && Util.esDiferente(getServiceSeccion01().getPersonaIdByParentescoSexo(bean.id, bean.hogar_id,App.SUEGRA,App.MUJER),bean.persona_id)){ 
	    			mensaje ="No debe haber m�s de 2 Suegras";
	    			view = rgQH03;
	    			error=true;
	    			return false;
			    }
		    }
	    	if (bean.qh07!=null) {
	    		/*********VALIDACION DE EDAD DE LOS HIJOS******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.HIJO_A)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    	  		if(edadjefe!=-1 && (edadjefe-bean.qh07<12))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los hijos no debe ser menor a 12 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DEL JEFE CON EL HIJO MAYOR******/
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadhijo_a=getServiceSeccion01().getEdadHijo_aMayor(bean.id, bean.hogar_id,App.HIJO_A);
		    		if(edadhijo_a!=-1 && bean.qh07-edadhijo_a<12)
		    		{
		    			mensaje ="Diferencia de edad de hijo_a mayor con el jefe de hogar no debe ser menor a 12 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
	    	
//		    	/*********VALIDACION DE EDAD DE LOS SUEGROS ******/
//		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
//		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
//		    			&& (!Util.esDiferente(bean.qh03, App.SUEGRO_SUEGRO )))
//		    	{
//		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
//		    		if(edadjefe!=-1 && (bean.qh07-edadjefe<24))
//		    		{
//		    			mensaje ="Diferencia de edad de jefe de hogar con los suegros no debe ser menor a 24 a�os";
//		    			view = txtQH07;
//		    			error=true;
//		    			return false;
//		    		}
//		    	}
//		    	/*********VALIDACION DE EDAD DE LOS NIETOS ******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.NIETO_NIETA)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    		if(edadjefe!=-1 && (edadjefe-bean.qh07<24))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los nietos no debe ser menor a 24 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DEL JEFE CON EL NIETO MAYOR******/
		    
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadnieto_nieta=getServiceSeccion01().getEdadHijo_aMayor(bean.id, bean.hogar_id,App.NIETO_NIETA);



		    		if(edadnieto_nieta!=-1 && bean.qh07-edadnieto_nieta<24)
		    		{
		    			mensaje ="Diferencia de edad de Nieto(a) mayor con el jefe de hogar no debe ser menor a 24 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		    	/*********VALIDACION DE EDAD DE LOS PADRES ******/
		    	if((getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.HOMBRE)
		    			|| getServiceSeccion01().getExisteRelacionconJefe(bean.id, bean.hogar_id, App.JEFE, App.MUJER))
		    			&& (!Util.esDiferente(bean.qh03, App.PADRE_MADRE)))
		    	{
		    		Integer edadjefe=getServiceSeccion01().getEdadByPersonaId(bean.id, bean.hogar_id,App.JEFE);
		    		if(edadjefe!=-1 && (bean.qh07-edadjefe<12))
		    		{
		    			mensaje ="Diferencia de edad de jefe de hogar con los padres no debe ser menor a 12 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
		     	/*********VALIDACION DE EDAD DEL JEFE CON EL PADRE MENOR******/
		    	if(!Util.esDiferente(bean.persona_id, App.JEFE))
		    	{	Integer edadpadre_madre=getServiceSeccion01().getEdadMinimaPadreMenor(bean.id, bean.hogar_id,App.PADRE_MADRE);
		    
		    		if(edadpadre_madre!=-1 && edadpadre_madre-bean.qh07<12)	{
		    			mensaje ="Diferencia de edad de Padres con el jefe de hogar no debe ser menor a 12 a�os";
		    			view = txtQH07;
		    			error=true;
		    			return false;
		    		}
		    	}
	    	
			    	
		    	if (MyUtil.incluyeRango(0, 11, bean.qh07) && !Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA)) {
		    		mensaje ="Edad de esposa(o) no debe ser menor a 12 a�os";
		    		view = txtQH07;
		    		error=true;
		    		return false;
		    	}
			    	
		    	if (MyUtil.incluyeRango(0, 9, bean.qh07) && !Util.esDiferente(bean.qh03,App.EMPLEADA)) {
		    		mensaje ="Edad de la empleada dom�stica no debe ser menor a 10 a�os";
		    		view = txtQH07;
		    		error=true;
		    		return false;
		    	}
			    	
//		    	if(MyUtil.incluyeRango(12, 17, bean.qh07) && Util.esDiferente(bean.qh08, 6)){
//		    		ValidarMensaje("Verificar edad con estado civil");
//		    		error = true;
//		    	}
			}
	    	
		}    
  
		return true;
	}
       
	private void ValidarMensaje(String msj) {
		ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,	ToastMessage.DURATION_LONG);
	}
	
	private void cargarDatos() {        

		bean = getCuestionarioService().getSeccion01(bean.id,bean.hogar_id,bean.persona_id,seccionesCargado);

		if (bean == null) {
			bean = new Seccion01();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id =App.getInstance().getHogar().hogar_id;
			bean.persona_id = caller.detalles.size()+1;
			bean.qh01 = caller.detalles.size()+1;
		}		

		if(bean.qh13!=null)	{
			bean.qh13=bean.setConvertqh13(bean.qh13);
		}
		if(bean.qh12!=null)	{
			bean.qh12=bean.setConvertqh12(bean.qh12);
		}
		entityToUI(bean);
		if(bean.qh7dd!=null)
		{bean.qh7dd= bean.qh7dd.toString().length()>1?bean.qh7dd:"0"+bean.qh7dd;
			txtQH7DD.setText(bean.qh7dd);
		}
		if(bean.qh7mm!=null)
		{  bean.qh7mm= bean.qh7mm.toString().length()>1?bean.qh7mm:"0"+bean.qh7mm;
			txtQH7MM.setText(bean.qh7mm);
		}
		if (bean.qh07!=null) {
			muestraPregunta07(bean.qh07);
		}             
		caretaker.addMemento("antes", bean.saveToMemento(Seccion01.class));
		inicio();
       	}

	
	private void inicio() {		
		rgQH03.requestFocus();
		
		if(verificarCheck()){onchbQH11_AChangeValue();}
		if(chbQH11_Z.isChecked()){onchbQH11_ZChangeValue();}
		if(chbQH11_Y.isChecked()){onchbQH11_YChangeValue();}
   		
		if (bean.qh07!=null) {
			onchbQH7ChangeValue();    	
			ontxtQH7ChangeValue();
		}
		
		onQH03Block();
		onrgQH08ChangeValue();
		CambiarNombreLabels();
	}

	
	public void CambiarNombreLabels(){
		String valoracambiar="(NOMBRE)";
		String Ud="Usted";
		bean.qhinfo=bean.qhinfo==null?0:bean.qhinfo;
		lblparentesco.setText(lblparentesco.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblviveaqui.setText(lblviveaqui.getText().toString().replace(valoracambiar,  bean.qhinfo==1?Ud:bean.qh02_1));
		lblDurmioAqui.setText(lblDurmioAqui.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblhombreomujer.setText(lblhombreomujer.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
		lblcumpleanios.setText(lblcumpleanios.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
    	lblafiliado.setText(lblafiliado.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud: bean.qh02_1));
    	Spanned textoafiliado=Html.fromHtml(lblafiliado.getText()+"<br><br> <b>SI</b>: �En cu�l? <br> �En alg�n otro seguro de salud?");
    	lblafiliado.setText(textoafiliado);
   		lbltitular.setText(lbltitular.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
   		Date date= new Date();
   		int dias=MyUtil.getDayOfTheWeek(date);
   		Calendar Fecha_Actual = new GregorianCalendar();
   		String primero="";
   		String segundo="";
   		Calendar segunda_fecha;
   		if(dias==Fecha_Actual.SATURDAY && Fecha_Actual.get(Calendar.AM_PM) ==1){
   			primero=Fecha_Actual.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(Fecha_Actual.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(Fecha_Actual, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		else{
   			Calendar primera_fecha = MyUtil.PrimeraFecha(Fecha_Actual, dias);
   			primero=primera_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(primera_fecha.get(Calendar.MONTH));
   			segunda_fecha= MyUtil.PrimeraFecha(primera_fecha, 6);
   			segundo=segunda_fecha.get(Calendar.DAY_OF_MONTH)+" de "+MyUtil.Mes(segunda_fecha.get(Calendar.MONTH));
   		}
   		
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace(valoracambiar,bean.qhinfo==1?Ud:bean.qh02_1));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F1",segundo));
   		lblpregunta13.setText(lblpregunta13.getText().toString().replace("F2",primero));
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}	
	private Seccion01Service getServiceSeccion01() {
		if (seccion01Service == null) {
			seccion01Service = Seccion01Service.getInstance(getActivity());
		}
		return seccion01Service;
	}       
       
	public void OcultarTecla() {
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(rgQH03.getWindowToken(), 0);
	}
	
    public void ontxtQH7ChangeValue() {
    	if((txtQH07.getText().toString().trim().length()>0)){  
    		if (MyUtil.incluyeRango(0,5,txtQH07.getText().toString())) {
    			Log.e("0a5","0a5");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM,rgQH08,rgQH13);         			
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.GONE);
    			q11.setVisibility(View.GONE);
    			OcultarTecla();
    			chbQH11_A.requestFocus(); 				  
    		}
    		if (MyUtil.incluyeRango(6,11,txtQH07.getText().toString())) {
    			Log.e("6a11","6a11");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM,rgQH08);         		  		
    			Util.lockView(getActivity(), false,rgQH13);   
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.GONE);  
    			q11.setVisibility(View.VISIBLE);
    			OcultarTecla();
    			chbQH11_A.requestFocus();         		   
    		}
    		if (MyUtil.incluyeRango(12,14,txtQH07.getText().toString())) {
    			Log.e("12a14","12a14");
    			Util.cleanAndLockView(getActivity(),chbQH07,txtQH7DD,txtQH7MM);
    			Util.lockView(getActivity(), false,rgQH08,rgQH13);
    			q7.setVisibility(View.GONE);
    			q8.setVisibility(View.VISIBLE); 
    			q11.setVisibility(View.VISIBLE);
    			rgQH08.requestFocus();    			
    			OcultarTecla();
    			onrgQH08ChangeValue();
    		}
    		if (MyUtil.incluyeRango(15,96,txtQH07.getText().toString())) {
    			Log.e("15a96","15a96");
    			Util.cleanAndLockView(getActivity(),chbQH07);
    			Util.lockView(getActivity(), false,txtQH7DD,txtQH7MM,rgQH08,rgQH13);
    			q7.setVisibility(View.VISIBLE);
    			q8.setVisibility(View.VISIBLE);  
    			q11.setVisibility(View.VISIBLE);
    			txtQH7DD.requestFocus();
    			gridPregunta07A.requestFocus();       			
    			OcultarTecla();
    			onrgQH08ChangeValue();
    		}			
    	}
    	else{
    		Log.e("else del null","97 check");
    		Util.lockView(getActivity(), false,chbQH07,txtQH7DD,txtQH7MM,rgQH08,rgQH13);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);  
    		q11.setVisibility(View.VISIBLE);
    		chbQH07.requestFocus();
    		OcultarTecla();
    		onrgQH08ChangeValue();
    	}
    }	

              
    public void onchbQH7ChangeValue() {
    	if(chbQH07.isChecked()){
    		Log.e("s","akinomas");
    		Util.cleanAndLockView(getActivity(),txtQH07);    		
    		txtQH7DD.requestFocus();				
    	} 
    	else{
    		Log.e("s","ffff");
    		Util.lockView(getActivity(), false,txtQH07);
    		txtQH07.requestFocus();	    		   
    	}	   	   
    }       

    public void onQH03Block(){
    	if(!Util.esDiferente(bean.persona_id,App.JEFE))	{
    		rgQH03.lockButtons(true,1,2,3,4,5,6,7,8,9,10,11);
	   		rgQH04.lockButtons(true,1,2);
    	}
	   	else{
	   		/****** ESPOSO ****/
	   		if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.ESPOSA),bean.persona_id)){
   	    		rgQH03.lockButtons(true, 1);
   	    	}
   	    	else{
   	    		rgQH03.lockButtons(false, 1);
   	    	}
	   		/***PADRES***/
	   		if(getServiceSeccion01().getExisteSoloPadres(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.PADRE),bean.persona_id)){
	   	    		rgQH03.lockButtons(true, 5);
	   	    	}
	   	    	else{
	   	    		rgQH03.lockButtons(false, 5);
	   	    	}
	   		/***SUEGROSS***/
	   		if(getServiceSeccion01().getExisteSoloSuegros(bean.id, bean.hogar_id) && Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.SUEGRO),bean.persona_id)){
   	    		rgQH03.lockButtons(true, 6);
   	    	}
   	    	else{
   	    		rgQH03.lockButtons(false, 6);
   	    	}
	   		rgQH03.lockButtons(true, 0);
	   		rgQH04.lockButtons(false,2);
		  	}   		
    }
    
    public void onrgQH08ChangeValue(){
    	Seccion01 seccion1=getServiceSeccion01().getPersona2(bean.id, bean.hogar_id,App.JEFE);      
    	if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) && !Util.esDiferente(bean.persona_id, App.JEFE)){
    		rgQH08.lockButtons(true, 2,3,4,5);
    	}
    	if(seccion1.qh08!=null){
    		Integer block=seccion1.qh08==App.CONIVIENTE?1:0;
    		if((!Util.esDiferente(seccion1.qh08,App.CONIVIENTE) 
    				|| !Util.esDiferente(seccion1.qh08,App.CASADO))
    				&& getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id) 
    				&& !Util.esDiferente(getServiceSeccion01().getEsParentescoId(bean.id, bean.hogar_id,bean.persona_id,App.ESPOSA),bean.persona_id)) {
    			rgQH08.lockButtons(true,block,2,3,4,5);
    		}
    	}	      
    }
      
    public boolean verificarCheck() {
  		if (chbQH11_A.isChecked() || chbQH11_B.isChecked() || chbQH11_C.isChecked() ||chbQH11_D.isChecked() || chbQH11_E.isChecked() ) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
  	public boolean verificarCheck1() {
  		if (chbQH11_Y.isChecked() || chbQH11_Z.isChecked() ) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
  	
  	public void bloquearchbQH11C() {
  		if (MyUtil.incluyeRango(0,17,txtQH07.getText().toString()) && !chbQH11_C.isChecked() && !chbQH11_E.isChecked()) {			
  			rgQH12.lockButtons(true,0);	
  		}
  		else{
  			rgQH12.lockButtons(false,0);
  		}
  	}
  	
  	public void onchbQH11_AChangeValue() {		
  		if (verificarCheck()) {	
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);
  			bloquearchbQH11C();
  		
  		} else {			
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_A.requestFocus();			
  		}		
  	}
  	public void onchbQH11_BChangeValue() {		
  		if (verificarCheck()) {		
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);
  			bloquearchbQH11C();
  		} else {		
  				Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  				chbQH11_B.requestFocus();				
  					
  		}		
  	}
  	public void onchbQH11_CChangeValue() {		
  		if (verificarCheck()) {	
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);	
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_C.requestFocus();			
  		}		
  	}
  	public void onchbQH11_DChangeValue() {		
  		if (verificarCheck()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);		
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_D.requestFocus();			
  		}		
  	}
  	public void onchbQH11_EChangeValue() {		
  		if (verificarCheck()) {
  			Util.cleanAndLockView(getActivity(),chbQH11_Y,chbQH11_Z);		
  			bloquearchbQH11C();
  		} else {
  			Util.lockView(getActivity(), false,chbQH11_Y,chbQH11_Z);
  			chbQH11_E.requestFocus();			
  		}		
  	}
  	public void onchbQH11_YChangeValue() {		
  		if (verificarCheck1()) {			
  			Util.cleanAndLockView(getActivity(),chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Z,rgQH12);
  			q10.setVisibility(View.GONE);
  			
  		} else {		
  			Util.lockView(getActivity(), false,chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Z,rgQH12);
  			q10.setVisibility(View.VISIBLE);
  			chbQH11_Y.requestFocus();			
  		}		
  	}
  	public void onchbQH11_ZChangeValue() {		
  		if (verificarCheck1()) {		
  			Util.cleanAndLockView(getActivity(),chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Y,rgQH12);
  			q10.setVisibility(View.GONE);
  			
  		} else {	
  			Util.lockView(getActivity(), false,chbQH11_A,chbQH11_B,chbQH11_C, chbQH11_D,chbQH11_E,chbQH11_Y,rgQH12);
  			q10.setVisibility(View.VISIBLE);
  			chbQH11_Z.requestFocus();			
  		}		
  	}

}

