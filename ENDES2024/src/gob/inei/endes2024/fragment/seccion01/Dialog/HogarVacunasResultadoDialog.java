package gob.inei.endes2024.fragment.seccion01.Dialog;
//package gob.inei.endes2024.fragment.seccion01.Dialog;
//
//import gob.inei.dnce.annotations.FieldAnnotation;
//import gob.inei.dnce.components.ButtonComponent;
//import gob.inei.dnce.components.CheckBoxField;
//import gob.inei.dnce.components.DialogFragmentComponent;
//import gob.inei.dnce.components.Entity;
//import gob.inei.dnce.components.FragmentForm;
//import gob.inei.dnce.components.GridComponent2;
//import gob.inei.dnce.components.IntegerField;
//import gob.inei.dnce.components.LabelComponent;
//import gob.inei.dnce.components.RadioGroupOtherField;
//import gob.inei.dnce.components.SpinnerField;
//import gob.inei.dnce.components.TextAreaField;
//import gob.inei.dnce.components.TextField;
//import gob.inei.dnce.components.ToastMessage;
//import gob.inei.dnce.components.Entity.SeccionCapitulo;
//import gob.inei.dnce.util.Caretaker;
//import gob.inei.dnce.util.Util;
//import gob.inei.endes2024.R;
//import gob.inei.endes2024.common.App;
//import gob.inei.endes2024.common.MyUtil;
//import gob.inei.endes2024.fragment.hogar.HogarFragment_013_03;
//import gob.inei.endes2024.model.Seccion01;
//import gob.inei.endes2024.service.CuestionarioService;
//import gob.inei.endes2024.service.Seccion01Service;
//import gob.inei.endes2024.service.UbigeoService;
//import gob.inei.endes2024.service.VisitaService;
//
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//
//public class HogarVacunasResultadoDialog extends DialogFragmentComponent  {
//		
//	@FieldAnnotation(orderIndex=1) 
//	public SpinnerField spnQH240;
//	@FieldAnnotation(orderIndex=2) 
//	public RadioGroupOtherField rgQH248;
//	@FieldAnnotation(orderIndex=3) 
//	public TextField txtQH248_O;
//	
//	@FieldAnnotation(orderIndex=4)
//	public TextAreaField txtQHVACUNAS_OBS;
//	
//	@FieldAnnotation(orderIndex=5)
//	public ButtonComponent btnAceptar;
//	
//
//	
//	public static HogarFragment_013_03 caller;	
//	Seccion01 persona;
//	private CuestionarioService cuestionarioService;
//	private CuestionarioService hogarService;
//	private Seccion01Service serviceSeccion01;
//	private VisitaService visitaService;
//	private UbigeoService ubigeoService;
//	private SeccionCapitulo[] seccionesCargado;
//	private SeccionCapitulo[] seccionesGrabado;
//	private static String nombre=null;
//	private String operador="-";
//	
//	public GridComponent2 gridqh243;
//	LabelComponent lblobs_34, lblpreguntaqh240, lblpreguntaqh248;
//	public LabelComponent lblpre240a_h_ini_t, lblpre248a_h_fin_t, lblpre240a_h_ini_f,lblpre248a_h_fin_f;
//	public ButtonComponent btnqh240a_h_ini,btnqh248a_h_fin;
//	private GridComponent2 grid_QH240a_h,grid_QH248a_hf;
//	
//	public ButtonComponent  btnCancelar;
//	public CheckBoxField chbP910;
//	String fecha_referencia;
//	
//	LinearLayout q0;
//	LinearLayout q1;
//	LinearLayout q2;
//	LinearLayout q3;
//	LinearLayout q4;
//	LinearLayout q5;
//	
//	public static HogarVacunasResultadoDialog newInstance(FragmentForm pagina,Seccion01 detalle, int position, List<Seccion01> detalles) {
//		caller = (HogarFragment_013_03) pagina;
////		personas=detalles;
//		HogarVacunasResultadoDialog f = new HogarVacunasResultadoDialog();
//		f.setParent(pagina);
//		Bundle args = new Bundle();
//		args.putSerializable("detalle", (Seccion01) detalles.get(position));
//		f.setArguments(args);
//		return f;
//	}
//	
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		persona = (Seccion01) getArguments().getSerializable("detalle");
//		caretaker = new Caretaker<Entity>();
//	}
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
////		getDialog().setTitle("N� "+persona.qh34a);
//		final View rootView = createUI();
//		initObjectsWithoutXML(this, rootView);
//		cargarDatos();
//		enlazarCajas();
//		listening();
//		return rootView;
//	}
//	
//	public HogarVacunasResultadoDialog() {
//		super();
//		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH240A","QH240", "QH248", "QH248_O", "QHVACUNAS_OBS", "QH248A", "ID", "HOGAR_ID", "PERSONA_ID")};
//		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1, "QH240A","QH240", "QH248", "QH248_O", "QHVACUNAS_OBS", "QH248A" )};
//	}            
//	
//
//	@Override
//	protected void buildFields(){
//		// TODO Auto-generated method stub
//		lblpreguntaqh240= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(17).text(R.string.vacuna_p240);
//		lblpreguntaqh248= new LabelComponent(getActivity()).size(MATCH_PARENT, 750).textSize(17).text(R.string.vacuna_p248);
//		
//		spnQH240=new SpinnerField(getActivity()).size(altoComponente+15, 500);
//		rgQH248 = new RadioGroupOtherField(this.getActivity(),R.string.vacuna_p248_1,R.string.vacuna_p248_2,R.string.vacuna_p248_3,R.string.vacuna_p248_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);		
//		txtQH248_O= new TextField(getActivity()).size(altoComponente, 600).maxLength(500);	
//		rgQH248.agregarEspecifique(3,txtQH248_O);
//		
//		lblobs_34 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.mortalidad_obs);
//		txtQHVACUNAS_OBS = new TextAreaField(getActivity()).maxLength(1000).size(250, 700).alfanumerico();
//		
//		lblpre240a_h_ini_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vacuna_hora_ini).textSize(17);
//		lblpre248a_h_fin_t	= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.vacuna_hora_fin).textSize(17);
//		btnqh240a_h_ini = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_ini).size(200, 55);
//		btnqh248a_h_fin = new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btn_fecha_fin).size(200, 55);
//		lblpre240a_h_ini_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
//		lblpre248a_h_fin_f	= new LabelComponent(this.getActivity()).size(60, 200).text("").textSize(17);
//	    
//		
//		btnqh240a_h_ini.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				lblpre240a_h_ini_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
//				btnqh248a_h_fin.setEnabled(true);
//			}
//		});
//		btnqh248a_h_fin.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				lblpre248a_h_fin_f.setText(Util.getFechaActualToString().toString().length()>1?Util.getFechaActualToString():"");
//			}
//		});
//		    
//		grid_QH240a_h = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
//		grid_QH240a_h.addComponent(btnqh240a_h_ini);
//		grid_QH240a_h.addComponent(lblpre240a_h_ini_f);
//		 
//		grid_QH248a_hf = new GridComponent2(this.getActivity(),Gravity.LEFT, 2,1);
//		grid_QH248a_hf.addComponent(btnqh248a_h_fin);
//		grid_QH248a_hf.addComponent(lblpre248a_h_fin_f);
//		
//		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
//		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
//		btnCancelar.setOnClickListener(new View.OnClickListener() {		
//			@Override
//			public void onClick(View v) {
//				HogarVacunasResultadoDialog.this.dismiss();
//			}
//		});		
//		btnAceptar.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean flag = grabar();
//				if (!flag) {
//					return;
//				}
//				HogarVacunasResultadoDialog.this.dismiss();
//				caller.cargarTabla();
//			}
//		});   
//	} 
//	
//	@Override
//	protected View createUI() {
//		buildFields();
//		q0 = createQuestionSection(0,lblpreguntaqh240,spnQH240);
//		q3 = createQuestionSection(lblpre240a_h_ini_t,grid_QH240a_h.component());
//		
//		q1 = createQuestionSection(0,lblpreguntaqh248,rgQH248);		
//		q2 = createQuestionSection(lblobs_34,txtQHVACUNAS_OBS);
//		q4 = createQuestionSection(lblpre248a_h_fin_t,grid_QH248a_hf.component());
//		
//		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
//		ScrollView contenedor = createForm();
//		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
//		form.addView(q0);
//		form.addView(q3);		
//		form.addView(q1);
//		form.addView(q2);
//		form.addView(q4);
//		form.addView(botones);
//		return contenedor;          
//	}
//	
//	public boolean grabar(){
//		uiToEntity(persona);
//		
//		persona.qh240a =lblpre240a_h_ini_f.getText().toString();
//		persona.qh248a =lblpre248a_h_fin_f.getText().toString();
//		
//		
//		if (!validar()) {
//			if (error) {
//				if (!mensaje.equals(""))
//					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
//				if (view != null)
//					view.requestFocus();
//			}
//			return false;
//		}
//		boolean flag=false;
//		try {
//			flag = getCuestionarioService().saveOrUpdate(persona, seccionesGrabado);
//		} catch (SQLException e) {
//			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
//		}
//		return flag;
//	}
//
//	
//	public boolean validar(){
//		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
//		if (Util.esVacio(persona.qh240)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.240"); 
//			view = spnQH240; 
//			error = true; 
//			return false; 
//		}
//		if(lblpre240a_h_ini_f.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha inicial pregunta 240A");
//			view = btnqh240a_h_ini;
//			error = true;
//			return false;
//		}
//		
//		
//		if (Util.esVacio(persona.qh248)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta P.248"); 
//			view = rgQH248; 
//			error = true; 
//			return false; 
//		}
//		if(!Util.esDiferente(persona.qh248,4)){ 
//			if (Util.esVacio(persona.qh248_o)) { 
//				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//				view = txtQH248_O; 
//				error = true; 
//				return false; 
//			} 
//		}
//		
//		if(lblpre248a_h_fin_f.getText().toString().trim().length()==0){
//			mensaje = preguntaVacia.replace("$", "Debe registrar la fecha final pregunta 248A");
//			view = btnqh248a_h_fin	;
//			error = true;
//			return false;
//		}
//
//	
//		if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0 && lblpre248a_h_fin_f.getText().toString().trim().length()!=0){
//			Calendar fecha_ini = new GregorianCalendar();
//			if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0){			
//				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//				Date date2 = null;
//				try {
//					date2 = df.parse(lblpre240a_h_ini_f.getText().toString());
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}		
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(date2);
//				fecha_ini=cal;			
//			}
//			
//			Calendar fecha_fin = new GregorianCalendar();
//	    	if(lblpre248a_h_fin_f.getText().toString().trim().length()!=0){			
//				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//				Date date2 = null;
//				try {
//					date2 = df.parse(lblpre248a_h_fin_f.getText().toString());
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}		
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(date2);
//				fecha_fin=cal;			
//			}
//    		if (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0) {
//        		mensaje = "Fecha final no puede ser menor a la fecha inicial"; 
//    			view = btnqh248a_h_fin; 
//    			error = true; 
//    			return false;
//        	}	
//        	if ( (Util.compare((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) == 0)  && (Util.compareTime((Date) fecha_fin.getTime(),(Date) fecha_ini.getTime()) < 0)) {
//        		mensaje = "Hora final no puede ser menor a la hora inicial"; 
//    			view = btnqh248a_h_fin; 
//    			error = true; 
//    			return false;
//        	}	
//    	} 
//	    return true;
//	}
//	
//	private void cargarDatos() {      
//		persona = getCuestionarioService().getSeccion01(App.getInstance().getMarco().id,App.getInstance().getHogar().hogar_id,persona.persona_id,seccionesCargado);
//		if (persona == null) {		
//			persona = new Seccion01();
//			persona.id = App.getInstance().getHogar().id;
//			persona.hogar_id =App.getInstance().getHogar().hogar_id;
//			persona.persona_id = persona.persona_id;
//		}
//		MyUtil.llenarPersonasInformanteVacunas(this.getActivity() , getServiceSeccion01(), spnQH240, persona.id, persona.hogar_id);
//		
//		if(persona.qh240a!=null) {lblpre240a_h_ini_f.setText(persona.qh240a);}		
//		if(persona.qh248a!=null) {lblpre248a_h_fin_f.setText(persona.qh248a);}
//		
//		if(lblpre240a_h_ini_f.getText().toString().trim().length()!=0){btnqh248a_h_fin.setEnabled(true);}else{btnqh248a_h_fin.setEnabled(false);}
//		
//		entityToUI(persona);
//		
//		inicio();
//    }
//
//	public void inicio(){		
//		ValidarsiesSupervisora();
//		rgQH248.lockButtons(true,0);
//		rgQH248.requestFocus();
//	}
//	
//	
//	
//	public void OcultarTecla() {
//		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(rgQH248.getWindowToken(), 0);
//	}
//	
//
//	
//	public void ValidarsiesSupervisora(){
//	    	Integer codigo=App.getInstance().getUsuario().cargo_id;
//	    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
//	    		rgQH248.readOnly();
//	    		txtQHVACUNAS_OBS.setEnabled(false);
//	    		btnAceptar.setEnabled(false);
//	    		btnqh240a_h_ini.setEnabled(false);
//				btnqh248a_h_fin.setEnabled(false);
//	    	}
//	}
//	    
//	public CuestionarioService getCuestionarioService() {
//		if (cuestionarioService == null) {
//			cuestionarioService = CuestionarioService.getInstance(getActivity());
//		}
//		return cuestionarioService;
//	}
//	
//	public VisitaService getVisitaService()
//	{
//		if(visitaService==null)
//		{
//				visitaService = VisitaService.getInstance(getActivity());
//		}
//			return visitaService;
//	}
//	private CuestionarioService getService() {
//		if (hogarService == null) {
//			hogarService = CuestionarioService.getInstance(getActivity());
//		}
//		return hogarService;
//	}
//	public UbigeoService getUbigeoService()
//	{
//		if(ubigeoService==null)		{
//			ubigeoService = UbigeoService.getInstance(getActivity());
//		}
//			return ubigeoService;
//	}
//	public Seccion01Service getServiceSeccion01() {
//		if (serviceSeccion01 == null) {
//			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
//		}
//		return serviceSeccion01;
//	}
//}
