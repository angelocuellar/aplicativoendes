package gob.inei.endes2024.fragment.seccion01.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.EndesCalendario;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_001;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Seccion01_001Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex=1)
	public TextField txtQH02_1;
	@FieldAnnotation(orderIndex=2)
	public TextField txtQH02_2;
	@FieldAnnotation(orderIndex=3)
	public TextField txtQH02_3;
	@FieldAnnotation(orderIndex=4)
	public RadioGroupOtherField rgQH03; 
	@FieldAnnotation(orderIndex=5)
	public ButtonComponent btnAceptar; 
	public ButtonComponent btnGrabadoParcial;
	
	
	 
	Seccion01 bean; 
	private static Seccion01Fragment_001 caller;
	public ButtonComponent  btnCancelar;
	private SeccionCapitulo[] seccionesGrabado, seccionesPersonas;
	private CuestionarioService cuestionarioService;
	private CuestionarioService hogarService;
	public GridComponent2 gridNombres;
	public LabelComponent lblpregunta,lblnombre,lblApPaterno,lblApMaterno,lblparentesco;
	private Seccion01Service seccion01Service;
	public LabelComponent lblpregunta01,lblpregunta03;
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
    }
	public static Seccion01_001Dialog newInstance(FragmentForm pagina, Seccion01 detalle) {
		caller = (Seccion01Fragment_001) pagina;
		Seccion01_001Dialog f = new Seccion01_001Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bean = (Seccion01) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Integer orden= caller.detalles.size()+1;
		getDialog().setTitle("N� de Orden:  " + orden);
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	public Seccion01_001Dialog() {
		super();
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "ID","HOGAR_ID","PERSONA_ID", "QH01", "QH02_1", "QH02_2", "QH02_3","QH03") };
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "PERSONA_ID", "QH01", "QH02_1", "QH02_2","QH02_3", "QH03") };

	}
		
	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q1 = createQuestionSection(lblpregunta01,gridNombres.component());
		LinearLayout q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta03,rgQH03);
		LinearLayout botones = createButtonSection(btnAceptar, btnGrabadoParcial ,btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q1);
		form.addView(q3);
		form.addView(botones);
		return contenedor;
	}

	@Override
	protected void buildFields() {
		lblnombre = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_1).size(altoComponente, 350).textSize(16);
		Spanned textopreguntapersonas=Html.fromHtml("2. D�game por favor los <b>nombres y apellidos</b> de las personas que <b>habitualmente</b> viven en su hogar y de los visitantes que pasaron la noche anterior aqu�, empezando por el Jefe del Hogar");
		lblpregunta01 = new LabelComponent(this.getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19);
		lblpregunta01.setText(textopreguntapersonas);
		lblpregunta03 = new LabelComponent(this.getActivity()).text(R.string.seccion01qh03).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblApPaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_2).size(altoComponente, 350).textSize(16);
		lblApMaterno = new LabelComponent(this.getActivity()).text(R.string.seccion01qh02_3).size(altoComponente, 350).textSize(16);
		txtQH02_1 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto().callback("ontxtQH02_1ChangeValue");
		txtQH02_2 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
		txtQH02_3 = new TextField(this.getActivity()).size(altoComponente, 350).maxLength(50).alinearIzquierda().soloTexto();
		lblparentesco= new LabelComponent(this.getActivity()).text(R.string.seccion01qh03).negrita().size(altoComponente, 290).textSize(16);
		gridNombres = new GridComponent2(this.getActivity(),App.ESTILO, 2);
		gridNombres.addComponent(lblnombre);
		gridNombres.addComponent(txtQH02_1);
		gridNombres.addComponent(lblApPaterno);
		gridNombres.addComponent(txtQH02_2);
		gridNombres.addComponent(lblApMaterno);
		gridNombres.addComponent(txtQH02_3);
		rgQH03 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh03_1,R.string.seccion01qh03_2,R.string.seccion01qh03_3,R.string.seccion01qh03_4,R.string.seccion01qh03_5,R.string.seccion01qh03_6,R.string.seccion01qh03_7,R.string.seccion01qh03_8,R.string.seccion01qh03_9,R.string.seccion01qh03_10,R.string.seccion01qh03_11,R.string.seccion01qh03_12).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onQH03Block");
		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnGrabadoParcial = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.seccion01grabadoparcial ).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bean.restoreFromMemento(caretaker.get("antes"));
				Seccion01_001Dialog.this.dismiss();
				Util.lockView(getParentActivity(), false,caller.btnAgregar);
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
				caller.refrescarPersonas(bean);
				Seccion01_001Dialog.this.dismiss();
				caller.agregarPersona();
			}
		});
		
		btnGrabadoParcial.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean flag=grabadoParcial();
				action=PROCCES.GRABADOPARCIAL;
				boolean flagdetalle=grabadoParcial();
				if(!flagdetalle){
					return;
				}
				DialogComponent dialog = new DialogComponent(getActivity(), Seccion01_001Dialog.this, TIPO_DIALOGO.YES_NO, getResources()
                      .getString(R.string.app_name),"Desea ir a Visita?");
				dialog.showDialog();
			}
		});
	}
	
	public boolean grabar(){
		uiToEntity(bean);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		SQLiteDatabase dbTX = getService().startTX();
		try {
//			bean.qh02_11=EndesCalendario.SepararSegundoNombre(bean.qh02_1);
//			bean.qh02_1=EndesCalendario.SepararPrimerNombre(bean.qh02_1);
	
			/*bean.qh02_11=EndesCalendario.SepararSegundoNombre(bean.qh02_1)==null?null:EndesCalendario.EncriptarDatosSensibles(EndesCalendario.SepararSegundoNombre(bean.qh02_1));
			bean.qh02_1=EndesCalendario.SepararPrimerNombre(bean.qh02_1);
			bean.qh02_2=bean.qh02_2==null?null:EndesCalendario.EncriptarDatosSensibles(bean.qh02_2);
			bean.qh02_3=bean.qh02_3==null?null:EndesCalendario.EncriptarDatosSensibles(bean.qh02_3);*/
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);
		
			if (!flag) {
				throw new Exception("Ocurri� un problema al grabar los datos de la caratula.");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	public boolean grabadoParcial(){
		uiToEntity(bean);
		if (!validarparcial()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,
							ToastMessage.MESSAGE_ERROR,
							ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		
		SQLiteDatabase dbTX = getService().startTX();
		try {
			flag = getCuestionarioService().saveOrUpdate(bean, dbTX,seccionesGrabado);
		
			if (!flag) {
				throw new Exception(
						"Ocurri� un problema al grabar los datos listado de personas");
			}
		getService().commitTX(dbTX);
		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} catch (Exception e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),
					ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} finally {
			getService().endTX(dbTX);
		}
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (bean.qh02_1 == null) {
			error = true;
			view = txtQH02_1;
			mensaje = preguntaVacia.replace("$", "La pregunta P.2");
			return false;
		}
		if (bean.qh03 == null) {
			error = true;
			view = rgQH03;
			mensaje = preguntaVacia.replace("$", "La pregunta P.QH03");
			return false;
		}
		
	    if(caller.detalles.size()>1){      	
	    	if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id)) {
	    		if(!Util.esDiferente(bean.qh03,App.ESPOSO_ESPOSA)){ 
	    			mensaje ="No debe haber m�s de 1 Esposo(a)";
	    			view = rgQH03;
	    			error=true;
	    			return false;
			    }
		    }
	    	
	    	if(getServiceSeccion01().getExisteSoloPadres(bean.id, bean.hogar_id)) {
	    		if(!Util.esDiferente(bean.qh03,App.PADRE_MADRE)){ 
	    			mensaje ="No debe haber m�s de 2 padres";
	    			view = rgQH03;
	    			error=true;
	    			return false;
			    }
		    }
	    	
	       	if(getServiceSeccion01().getExisteSoloSuegros(bean.id, bean.hogar_id)) {
	    		if(!Util.esDiferente(bean.qh03,App.SUEGRO_SUEGRO)){ 
	    			mensaje ="No debe haber m�s de 2 Suegros(as)";
	    			view = rgQH03;
	    			error=true;
	    			return false;
			    }
		    }
	    }	
	    return true;
	}
	
	public boolean validarparcial(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if (bean.qh02_1 == null) {
			error = true;
			view = txtQH02_1;
			mensaje = preguntaVacia.replace("$", "La pregunta P.2");
			return false;
		}
		return true;
	}

	public void ontxtQH02_1ChangeValue(){
		if(txtQH02_1.getText().toString().length()>0){
			lblpregunta03.setText(getResources().getString(R.string.seccion01qh03));
			lblpregunta03.setText(lblpregunta03.getText().toString().replace("(NOMBRE)",txtQH02_1.getText().toString()));
		}
	}
	
	private void cargarDatos() {
		bean=null;
		if (bean != null) {
			bean = getCuestionarioService().getSeccion01(bean.id,bean.hogar_id,1,seccionesPersonas);
		}
		if (bean == null) {
			bean = new Seccion01();
			bean.id = App.getInstance().getMarco().id;
			bean.hogar_id =App.getInstance().getHogar().hogar_id;
			bean.persona_id = caller.detalles.size()+1;
			bean.qh01 = caller.detalles.size()+1;
		}
		entityToUI(bean);
		caretaker.addMemento("antes", bean.saveToMemento(Seccion01.class));
		inicio();
	}
	
	private void inicio() {
		esconderpregunta();
		onQH03Block();
		txtQH02_1.requestFocus();
	}
	
	private void esconderpregunta(){
		if(bean.qh01!=App.JEFE){
			lblpregunta01.setVisibility(View.GONE);
		}
		else{
			lblpregunta01.setVisibility(View.VISIBLE);
		}
	}

	public void onQH03Block()	{
		if(!Util.esDiferente(bean.persona_id,App.JEFE)){
			btnAceptar.requestFocus();
			rgQH03.lockButtons(true,1,2,3,4,5,6,7,8,9,10,11);
			rgQH03.setTagIndexSelected(0);
		}	
		else{
			if(getServiceSeccion01().getExisteSoloEsposa(bean.id, bean.hogar_id)){
		    		rgQH03.lockButtons(true, 1);
			}
			else{
				rgQH03.lockButtons(false, 1);
			}
			/***PADRES***/
	   		if(getServiceSeccion01().getExisteSoloPadres(bean.id, bean.hogar_id)){
	   			rgQH03.lockButtons(true, 5);
	   		}
	   		else{
	   			rgQH03.lockButtons(false, 5);
	   		}
	   		/***SUEGROSS***/
	   		if(getServiceSeccion01().getExisteSoloSuegros(bean.id, bean.hogar_id)){
	   			rgQH03.lockButtons(true, 6);
	   		}
	   		else{
	   			rgQH03.lockButtons(false, 6);
	   		}
	   		rgQH03.lockButtons(true, 0);			
		}
		
		btnAceptar.requestFocus();
		InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(rgQH03.getWindowToken(), 0);
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}
    private Seccion01Service getServiceSeccion01() {
        if (seccion01Service == null) {
     	   seccion01Service = Seccion01Service.getInstance(getActivity());
        }
        return seccion01Service;
  }

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		Seccion01_001Dialog.this.dismiss();
		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
	}
}
