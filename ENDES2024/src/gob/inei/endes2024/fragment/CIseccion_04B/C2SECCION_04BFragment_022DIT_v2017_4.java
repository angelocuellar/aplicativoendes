package gob.inei.endes2024.fragment.CIseccion_04B;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_v2017_4 extends FragmentForm{
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI478D1; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478D2; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478D3; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478D4; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478D5; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478D6; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478D7; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478D8; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478D9A; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478D9B; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478D10;
	@FieldAnnotation(orderIndex=11) 
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI478A;

	CISECCION_04DIT ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo,lblpregunta478d1,lblpregunta478d12,lblpregunta478d13,lblpregunta478d2,lblpregunta478d21,lblpregunta478d22,
	lblpregunta478d3,lblpregunta478d31,lblpregunta478d32,lblpregunta478d33,
	lblpregunta478d4,lblpregunta478d41,lblpregunta478d42,lblpregunta478d43,lblpregunta478d44,lblpregunta478d45,lblpregunta478d46,
	lblpregunta478d5,lblpregunta478d51,lblpregunta478d52,lblpregunta478d53,
	lblpregunta478d6,lblpregunta478d61,
	lblpregunta478d7,lblpregunta478d8,
	lblpregunta478d9,lblpregunta478d91,lblpregunta478d92,lblpregunta478d93,lblpregunta478d94,lblpregunta478d95,lblpregunta478d96,lblpregunta478d97,lblpregunta478d98,lblTramo,
	lblpregunta478d10,lbldiscapacidad,lblpregunta478d99;
	public GridComponent2 gridpregunta478d9,gridDiscapacidad;
	public ButtonComponent  btnCancelar;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_v2017_4() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478D1","QI478D2","QI478D3","QI478D4","QI478D5","QI478D6","QI478D7","QI478D8","QI478D9A","QI478D9B","QI478D10","ID","HOGAR_ID","QI478A","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478D1","QI478D2","QI478D3","QI478D4","QI478D5","QI478D6","QI478D7","QI478D8","QI478D9A","QI478D9B","QI478D10","QI478C10","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_v2017_4 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 

	
	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo53_59).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478a);
		lblTramo = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478a_ind);
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		lblpregunta478d1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d12=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d12).negrita();
		lblpregunta478d13=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478d13);
		lblpregunta478d2 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d21 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d21).negrita();
		lblpregunta478d22 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478d22);
		lblpregunta478d3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478d3);
		lblpregunta478d31= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04b_2qi478d31).negrita();
		lblpregunta478d32= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d32);
		lblpregunta478d33= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d33).negrita();
		lblpregunta478d4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d41= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d41).negrita();
		lblpregunta478d42= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19);
		lblpregunta478d43 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d43).negrita();
		lblpregunta478d44 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d44);
		lblpregunta478d45 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d45).negrita();
		lblpregunta478d46 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d46);
		
		
		lblpregunta478d5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478d5);
		lblpregunta478d51= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d51);
		lblpregunta478d52= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d52).negrita();
		lblpregunta478d53= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d53);
		lblpregunta478d6 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d61=new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d61);
		lblpregunta478d7 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d8 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		lblpregunta478d9= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478d91= new LabelComponent(getActivity()).size(altoComponente+15, 740).textSize(19);
		lblpregunta478d92= new LabelComponent(getActivity()).size(30, 740).textSize(17).text(R.string.c2seccion_04b_2qi478d92).negrita();
		lblpregunta478d93= new LabelComponent(getActivity()).size(altoComponente+50, 740).textSize(19).text(R.string.c2seccion_04b_2qi478d93);
		lblpregunta478d94 = new LabelComponent(getActivity()).size(altoComponente+15,740).textSize(19);
		lblpregunta478d95 = new LabelComponent(getActivity()).size(30,740).textSize(17).text(R.string.c2seccion_04b_2qi478d92).negrita();
		lblpregunta478d96 = new LabelComponent(getActivity()).size(altoComponente+180,740).textSize(19).text(R.string.c2seccion_04b_2qi478d96);
		lblpregunta478d97 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478d97).negrita();
		lblpregunta478d98 =  new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(16);
		lblpregunta478d99 =  new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(16);
		lblpregunta478d10 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		rgQI478D1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d1_1,R.string.c2seccion_04b_2qi478d1_2,R.string.c2seccion_04b_2qi478d1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d2_1,R.string.c2seccion_04b_2qi478d2_2,R.string.c2seccion_04b_2qi478d2_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d3_1,R.string.c2seccion_04b_2qi478d3_2,R.string.c2seccion_04b_2qi478d3_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d4_1,R.string.c2seccion_04b_2qi478d4_2,R.string.c2seccion_04b_2qi478d4_3,R.string.c2seccion_04b_2qi478d4_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d5_1,R.string.c2seccion_04b_2qi478d5_2,R.string.c2seccion_04b_2qi478d5_3,R.string.c2seccion_04b_2qi478d5_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d6_1,R.string.c2seccion_04b_2qi478d6_2,R.string.c2seccion_04b_2qi478d6_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d7_1,R.string.c2seccion_04b_2qi478d7_2,R.string.c2seccion_04b_2qi478d7_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqi478d7ChangeValue"); 
		rgQI478D8=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d8_1,R.string.c2seccion_04b_2qi478d8_2,R.string.c2seccion_04b_2qi478d8_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D9A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d9a_1,R.string.c2seccion_04b_2qi478d9a_2,R.string.c2seccion_04b_2qi478d9a_3).size(altoComponente,740).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D9B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d9b_1,R.string.c2seccion_04b_2qi478d9b_2,R.string.c2seccion_04b_2qi478d9b_3).size(altoComponente,740).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478D10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478d10_1,R.string.c2seccion_04b_2qi478d10_2,R.string.c2seccion_04b_2qi478d10_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		gridpregunta478d9= new GridComponent2(getActivity(),App.ESTILO,1,0);
		gridpregunta478d9.addComponent(lblpregunta478d91);
		gridpregunta478d9.addComponent(lblpregunta478d92);
		gridpregunta478d9.addComponent(lblpregunta478d93);
		gridpregunta478d9.addComponent(rgQI478D9A.centrar());
		gridpregunta478d9.addComponent(lblpregunta478d94);
		gridpregunta478d9.addComponent(lblpregunta478d95);
		gridpregunta478d9.addComponent(lblpregunta478d96);
		gridpregunta478d9.addComponent( rgQI478D9B.centrar());


	} 
	
	
	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo); 
		q12 = createQuestionSection(gridDiscapacidad.component());
		q13 = createQuestionSection(lblTramo);
		q1 = createQuestionSection(0,lblpregunta478d1,lblpregunta478d12,lblpregunta478d13,rgQI478D1); 
		q2 = createQuestionSection(0,lblpregunta478d2,lblpregunta478d21,lblpregunta478d22,rgQI478D2); 
		q3 = createQuestionSection(0,lblpregunta478d31,lblpregunta478d3,lblpregunta478d32,lblpregunta478d33,rgQI478D3); 
		q4 = createQuestionSection(0,lblpregunta478d4,lblpregunta478d41,lblpregunta478d42,lblpregunta478d43,lblpregunta478d44,lblpregunta478d45,lblpregunta478d46,rgQI478D4); 
		q5 = createQuestionSection(0,lblpregunta478d5,lblpregunta478d51,lblpregunta478d52,lblpregunta478d53,rgQI478D5); 
		q6 = createQuestionSection(0,lblpregunta478d6,lblpregunta478d61,rgQI478D6); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478d7,rgQI478D7); 
		q8 = createQuestionSection(0,lblpregunta478d8,rgQI478D8); 
		q10 = createQuestionSection(0,lblpregunta478d9,gridpregunta478d9.component(),lblpregunta478d98,lblpregunta478d99); 
		q11 = createQuestionSection(0,lblpregunta478d10,rgQI478D10);           
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q12);
		form.addView(q13);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q10); 
		form.addView(q11); 
		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			CambiarVariablesparalaBD();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	
	private void CambiarVariablesparalaBD(){
		ninio.qi478d1=ninio.qi478d1!=null?ninio.setConvertcontresvariables(ninio.qi478d1):null;
		ninio.qi478d2=ninio.qi478d2!=null?ninio.setConvertcontresvariables(ninio.qi478d2):null;
		ninio.qi478d3=ninio.qi478d3!=null?ninio.setConvertcontresvariables(ninio.qi478d3):null;
		ninio.qi478d4=ninio.qi478d4!=null?ninio.setConvertconcuatrovariables(ninio.qi478d4):null;
		ninio.qi478d5=ninio.qi478d5!=null?ninio.setConvertconcuatrovariables(ninio.qi478d5):null;
		ninio.qi478d6=ninio.qi478d6!=null?ninio.setConvertcontresvariables(ninio.qi478d6):null;
		ninio.qi478d7=ninio.qi478d7!=null?ninio.setConvertcontresvariables(ninio.qi478d7):null;
		ninio.qi478d8=ninio.qi478d8!=null?ninio.setConvertcontresvariables(ninio.qi478d8):null;
		ninio.qi478d9a=ninio.qi478d9a!=null?ninio.setConvertcontresvariables(ninio.qi478d9a):null;
		ninio.qi478d9b=ninio.qi478d9b!=null?ninio.setConvertcontresvariables(ninio.qi478d9b):null;
		ninio.qi478d10=ninio.qi478d10!=null?ninio.setConvertcontresvariables(ninio.qi478d10):null;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478d1  == null) {
				error = true;
				view = rgQI478D1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D1");
				return false;
			}
			if (ninio.qi478d2  == null) {
				error = true;
				view = rgQI478D2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D2");
				return false;
			}
			if (ninio.qi478d3  == null) {
				error = true;
				view = rgQI478D3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D3");
				return false;
			}
			if (ninio.qi478d4  == null) {
				error = true;
				view = rgQI478D4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D4");
				return false;
			}
			if (ninio.qi478d5  == null) {
				error = true;
				view = rgQI478D5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D5");
				return false;
			}
			if (ninio.qi478d6  == null) {
				error = true;
				view = rgQI478D6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D6");
				return false;
			}
			if (ninio.qi478d7  == null) {
				error = true;
				view = rgQI478D7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D7");
				return false;
			}
			if(!Util.esDiferente(ninio.qi478d7,1)){
				if (ninio.qi478d8  == null) {
					error = true;
					view = rgQI478D8;
					mensaje = preguntaVacia.replace("$", "La pregunta P.478D8");
					return false;
				}	
			}
			if (ninio.qi478d9a  == null) {
				error = true;
				view = rgQI478D9A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D9A");
				return false;
			}
			if (ninio.qi478d9b  == null) {
				error = true;
				view = rgQI478D9B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D9B");
				return false;
			}
			if (ninio.qi478d10  == null) {
				error = true;
				view = rgQI478D10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478D10");
				return false;
			}
		}
	    return true;
	}
	@Override
	public void cargarDatos() {    
		if(App.getInstance().getSeccion04DIT()!=null) {
		ninio = getCuestionarioService().getCISECCION_04B_2(App.getInstance().getSeccion04DIT().id,App.getInstance().getSeccion04DIT().hogar_id,App.getInstance().getSeccion04DIT().persona_id,App.getInstance().getSeccion04DIT().nro_orden_ninio,seccionesCargado);
		if (ninio == null) {
			ninio = new CISECCION_04DIT();
			ninio.id = App.getInstance().getSeccion04DIT().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT().nro_orden_ninio;
		}		
		cambiarvariablesparamostrarausuario();
		entityToUI(ninio);
		inicio();
		}
	}
	
	private void cambiarvariablesparamostrarausuario(){
		ninio.qi478d1 = ninio.qi478d1 !=null ?	ninio.getConvertcontresvariables(ninio.qi478d1):null;
		ninio.qi478d2 = ninio.qi478d2 !=null ? ninio.getConvertcontresvariables(ninio.qi478d2):null;
		ninio.qi478d3 = ninio.qi478d3 !=null ? ninio.getConvertcontresvariables(ninio.qi478d3):null;
		ninio.qi478d4 = ninio.qi478d4 !=null ? ninio.getConvertconcuatrovariables(ninio.qi478d4):null;
		ninio.qi478d5 = ninio.qi478d5 !=null ? ninio.getConvertconcuatrovariables(ninio.qi478d5):null;
		ninio.qi478d6 = ninio.qi478d6 !=null ? ninio.getConvertcontresvariables(ninio.qi478d6):null;
		ninio.qi478d7 = ninio.qi478d7 !=null ? ninio.getConvertcontresvariables(ninio.qi478d7):null;
		ninio.qi478d8 = ninio.qi478d8 !=null ? ninio.getConvertcontresvariables(ninio.qi478d8):null;
		ninio.qi478d9a = ninio.qi478d9a !=null ? ninio.getConvertcontresvariables(ninio.qi478d9a):null;
		ninio.qi478d9b = ninio.qi478d9b !=null ? ninio.getConvertcontresvariables(ninio.qi478d9b):null;
		ninio.qi478d10 = ninio.qi478d10 !=null ? ninio.getConvertcontresvariables(ninio.qi478d10):null;
	}
	public void inicio(){
		onQI478AChangeValue();
		RenombrarEtiquetas();
		chbQI478A.requestFocus();
		ValidarsiesSupervisora();
	}
	public void RenombrarEtiquetas(){
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		lblpregunta478d1.setText(lblpregunta478d1.getText().toString().replace(replace, nombre));
		lblpregunta478d1.setText(Html.fromHtml("478D1. �"+lblpregunta478d1.getText()+" <b>juega a ser</b> otra persona o un animalito?"));
		
		lblpregunta478d2.setText(lblpregunta478d2.getText().toString().replace(replace,nombre));
		lblpregunta478d2.setText(Html.fromHtml("478D2. �"+lblpregunta478d2.getText()+" juega de \"mentirita\", es decir juega con <b>cosas que no tiene en ese momento</b>?"));
		lblpregunta478d3.setText(lblpregunta478d3.getText().toString().replace(replace, nombre));
		
		lblpregunta478d4.setText(lblpregunta478d4.getText().toString().replace(replace, nombre));
		lblpregunta478d4.setText(Html.fromHtml("478D4. Cuando "+lblpregunta478d4.getText()+" <b>explica algo</b> �<b>usa</b> la palabra \"porque\"?"));
		lblpregunta478d42.setText(Html.fromHtml("Por ejemplo, dice \"Com&iacute; todo <b>porque</b> ten&iacute;a mucha hambre\", \" Te quiero <b>porque</b> eres mi mam&aacute;\" "));
		lblpregunta478d5.setText(lblpregunta478d5.getText().toString().replace(replace, nombre));
		lblpregunta478d6.setText(lblpregunta478d6.getText().toString().replace(replace, nombre));
		lblpregunta478d6.setText(Html.fromHtml("478D6. Cuando "+lblpregunta478d6.getText()+" ve una persona que est&aacute; molesta �<b>dice</b> \"ella o &eacute;l est&aacute; molesta(o)\" o <b>frases</b> que signifiquen lo mismo?"));
		
		lblpregunta478d7.setText(lblpregunta478d7.getText().toString().replace(replace,nombre));
		lblpregunta478d7.setText(Html.fromHtml("478D7. Normalmente cuando "+lblpregunta478d7.getText()+" <b>quiere algo y usted le dice que espere </b>�ella (&eacute;l) espera \"tranquilamente\"?"));
		
		lblpregunta478d8.setText(lblpregunta478d8.getText().toString().replace(replace, nombre));
		lblpregunta478d8.setText(Html.fromHtml("478D8. Cuando "+lblpregunta478d8.getText()+"  <b>quiere algo y usted le dice que espere </b>�ella (&eacute;l) se hace da�o, agrede a los dem&aacute;s o a las cosas?"));
		
		lblpregunta478d9.setText(lblpregunta478d9.getText().toString().replace(replace, nombre));
		lblpregunta478d9.setText(Html.fromHtml("478D9. <b>En casa </b>"+lblpregunta478d9.getText()+" tiene:"));
		lblpregunta478d91.setText(Html.fromHtml("<b>A. </b>�Materiales <b>especialmente hechos para jugar</b> como una pelota o una mu�eca?"));
		lblpregunta478d94.setText(Html.fromHtml("<b>B.</b> �Y tiene <b>otros</b> materiales con los que <b>puede jugar</b> como palitos, botellas o l&aacute;pices?"));
		lblpregunta478d98.setText(Html.fromHtml("<b>SELECCIONE �1� CUANDO:</b>"));
		lblpregunta478d99.setText(Html.fromHtml("TIENE <b>AL MENOS UN MATERIAL</b> EN CADA SUBPREGUNTA A Y B."));
		
		lblpregunta478d10.setText(lblpregunta478d10.getText().toString().replace(replace, nombre));
		lblpregunta478d10.setText(Html.fromHtml("478D10. �"+lblpregunta478d10.getText()+" <b>normalmente</b> juega con otras (otros) ni�as (ni�os) <b> de su edad</b> ?"));
		}
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478D1,rgQI478D2,rgQI478D3,rgQI478D4,rgQI478D5,rgQI478D6,rgQI478D7,rgQI478D8,rgQI478D9A,rgQI478D9B,rgQI478D10);
			MyUtil.LiberarMemoria();
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478D1,rgQI478D2,rgQI478D3,rgQI478D4,rgQI478D5,rgQI478D6,rgQI478D7,rgQI478D8,rgQI478D9A,rgQI478D9B,rgQI478D10);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			onqi478d7ChangeValue();
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	public void onqi478d7ChangeValue(){
		if(rgQI478D7!=null) {
			if(MyUtil.incluyeRango(2, 3, rgQI478D7.getTagSelected("").toString())){
				Util.cleanAndLockView(getActivity(), rgQI478D8);
				q8.setVisibility(View.GONE);
				rgQI478D9A.requestFocus();
			}
			else{
				Util.lockView(getActivity(), false,rgQI478D8);
				q8.setVisibility(View.VISIBLE);
				rgQI478D8.requestFocus();
			}
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI478D1.readOnly();
			rgQI478D2.readOnly();
			rgQI478D3.readOnly();
			rgQI478D4.readOnly();
			rgQI478D5.readOnly();
			rgQI478D6.readOnly();
			rgQI478D7.readOnly();
			rgQI478D8.readOnly();
			rgQI478D9A.readOnly();
			rgQI478D9B.readOnly();
			rgQI478D10.readOnly();
			chbQI478A.readOnly();
		}
	}
	@Override
	public Integer grabadoParcial() {
		return null;
	}

}
