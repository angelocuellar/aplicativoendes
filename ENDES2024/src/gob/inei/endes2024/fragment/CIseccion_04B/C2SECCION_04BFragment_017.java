package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class C2SECCION_04BFragment_017 extends FragmentForm { 
		
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI473F; 	
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI474; 
	@FieldAnnotation(orderIndex=3) 
	public TextField txtQI474A; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI474A_A; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI474A_B; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI474A_C; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI474A_D; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI474A_E; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI474A_F; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI474A_G; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI474A_H; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI474A_I; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI474A_J; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI474A_K; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI474A_L; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI474A_M; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI474A_N; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI474A_O; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI474A_P; 
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI474A_X; 
	@FieldAnnotation(orderIndex=21) 
	public TextField txtQI474A_XO; 
	 
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo, lblpregunta473f,lblpregunta474,lblpregunta474a,lblpastillaveces,lbllugar,lblpregunta474a_ind,
	lblpregunta474alug,lblpregunta474ahos,lblpregunta474a_Pu_Ind,lblpregunta474a_Pri_Ind,lblpregunta474a_Org_Ind,lblpregunta474alug_1; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3;
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI473F","QI474","QI474A","QI474A_A","QI474A_B","QI474A_C","QI474A_D","QI474A_E","QI474A_F","QI474A_G","QI474A_H","QI474A_I","QI474A_J","QI474A_K","QI474A_L","QI474A_M","QI474A_N","QI474A_O","QI474A_P","QI474A_X","QI474A_XO")}; 
	SeccionCapitulo[] seccionesCargado; 

	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdPastilla473f,gdLugarSalud;
	public C2SECCION_04BFragment_017() {} 
	public C2SECCION_04BFragment_017 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		rango(getActivity(), txtQI473F, 1, 90,  null, 99);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI473F","QI474","QI474A","QI474A_A","QI474A_B","QI474A_C","QI474A_D","QI474A_E","QI474A_F","QI474A_G","QI474A_H","QI474A_I","QI474A_J","QI474A_K","QI474A_L","QI474A_M","QI474A_N","QI474A_O","QI474A_P","QI474A_X","QI474A_XO","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	   
	    lblpregunta473f = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473f);
	    lblpregunta474 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi474);
	    lblpregunta474a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi474a);
	    lblpregunta474a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi474a_ind);
	    
		txtQI473F=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		
		lblpastillaveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04bqi473f_1).textSize(16).centrar();
		 
		gdPastilla473f = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdPastilla473f.addComponent(lblpastillaveces);
		gdPastilla473f.addComponent(txtQI473F);
		
		
	    rgQI474=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi474_1,R.string.c2seccion_04bqi474_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI474ChangeValue"); 
	    txtQI474A =new TextField(getActivity()).size(altoComponente, 400).maxLength(100).alfanumerico();
		
		chbQI474A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_N=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_O=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_o, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_P=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_p, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI474A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi474a_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI474axChangeValue");
		txtQI474A_XO=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 350).text(R.string.c2seccion_04bqi474anom).textSize(16);
		lblpregunta474alug = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi474alug);
		lblpregunta474alug_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi474alug_1);
		
		Spanned texto474aPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta474a_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta474a_Pu_Ind.setText(texto474aPu);
		
		Spanned texto474aPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta474a_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta474a_Pri_Ind.setText(texto474aPri);
		
		Spanned texto474aOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta474a_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta474a_Org_Ind.setText(texto474aOrg);
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI474A);
		
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 			
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta473f,gdPastilla473f.component()); 		
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta474,rgQI474); 
		
		LinearLayout ly474a = new LinearLayout(getActivity());
		ly474a.addView(chbQI474A_X);
		ly474a.addView(txtQI474A_XO);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta474a,lblpregunta474a_ind,gdLugarSalud.component(),lblpregunta474alug,lblpregunta474alug_1,lblpregunta474a_Pu_Ind,chbQI474A_A,chbQI474A_B,chbQI474A_C,chbQI474A_D,chbQI474A_E,chbQI474A_F,chbQI474A_G,chbQI474A_H,lblpregunta474a_Pri_Ind,chbQI474A_I,chbQI474A_J,chbQI474A_K,chbQI474A_L,chbQI474A_M,chbQI474A_N,lblpregunta474a_Org_Ind,chbQI474A_O,chbQI474A_P,ly474a);//,,,chbQI229_Y
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3);  
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_04b); 
				
		int cont=0;		
		if(c2seccion_04b.qi474a_a==1)
			cont+=1;
		if(c2seccion_04b.qi474a_b==1)
			cont+=1;
		if(c2seccion_04b.qi474a_c==1)
			cont+=1;
		if(c2seccion_04b.qi474a_d==1)
			cont+=1;
		if(c2seccion_04b.qi474a_e==1)
			cont+=1;
		if(c2seccion_04b.qi474a_f==1)
			cont+=1;
		if(c2seccion_04b.qi474a_g==1)
			cont+=1;
		if(c2seccion_04b.qi474a_h==1)
			cont+=1;
		if(c2seccion_04b.qi474a_i==1)
			cont+=1;
		if(c2seccion_04b.qi474a_j==1)
			cont+=1;
		if(c2seccion_04b.qi474a_k==1)
			cont+=1;
		if(c2seccion_04b.qi474a_l==1)
			cont+=1;
		if(c2seccion_04b.qi474a_m==1)
			cont+=1;
		if(c2seccion_04b.qi474a_n==1)
			cont+=1;
		if(c2seccion_04b.qi474a_o==1)
			cont+=1;
		if(c2seccion_04b.qi474a_p==1)
			cont+=1;
		if(c2seccion_04b.qi474a_x==1)
			cont+=1;
		
		
		App.getInstance().getSeccion04B().qi474 = c2seccion_04b.qi474;
		
		App.getInstance().getSeccion04B().filtro474a = cont;
		App.getInstance().getSeccion04B().qi474a_a=c2seccion_04b.qi474a_a;
		App.getInstance().getSeccion04B().qi474a_b=c2seccion_04b.qi474a_b;
		App.getInstance().getSeccion04B().qi474a_c=c2seccion_04b.qi474a_c;
		App.getInstance().getSeccion04B().qi474a_d=c2seccion_04b.qi474a_d;
		App.getInstance().getSeccion04B().qi474a_e=c2seccion_04b.qi474a_e;
		App.getInstance().getSeccion04B().qi474a_f=c2seccion_04b.qi474a_f;
		App.getInstance().getSeccion04B().qi474a_g=c2seccion_04b.qi474a_g;
		App.getInstance().getSeccion04B().qi474a_h=c2seccion_04b.qi474a_h;
		App.getInstance().getSeccion04B().qi474a_i=c2seccion_04b.qi474a_i;
		App.getInstance().getSeccion04B().qi474a_j=c2seccion_04b.qi474a_j;
		App.getInstance().getSeccion04B().qi474a_k=c2seccion_04b.qi474a_k;
		App.getInstance().getSeccion04B().qi474a_l=c2seccion_04b.qi474a_l;
		App.getInstance().getSeccion04B().qi474a_m=c2seccion_04b.qi474a_m;
		App.getInstance().getSeccion04B().qi474a_n=c2seccion_04b.qi474a_n;
		App.getInstance().getSeccion04B().qi474a_o=c2seccion_04b.qi474a_o;
		App.getInstance().getSeccion04B().qi474a_p=c2seccion_04b.qi474a_p;
		App.getInstance().getSeccion04B().qi474a_x=c2seccion_04b.qi474a_x;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if(App.getInstance().getSeccion04B().qi473c==1) {
			if(App.getInstance().getSeccion04B().qi473d_a==1) {
				if (Util.esVacio(c2seccion_04b.qi473f)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI473F"); 
					view = txtQI473F; 
					error = true; 
					return false; 
				} 
			}
		}
		if (Util.esVacio(c2seccion_04b.qi474)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI474"); 
			view = rgQI474; 
			error = true; 
			return false; 
		} 
		if(c2seccion_04b.qi474==1) {
			/*if (Util.esVacio(c2seccion_04b.qi474a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI474A"); 
				view = txtQI474A; 
				error = true; 
				return false; 
			} */
			
			if(!verificarCheck474a()) {
				mensaje = "Seleccione una opci�n en P.474A"; 
				view = chbQI474A_A; 
				error = true; 
				return false; 
			}
			
			
			if (chbQI474A_X.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi474a_xo)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI474A_XO; 
					error = true; 
					return false; 
				}
			}	
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	verificar473c();
    	onqrgQI474ChangeValue();
    	onqrgQI474axChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    
    public void verificar473c() {
    	if(App.getInstance().getSeccion04B().qi473c==1) {
    		Util.lockView(getActivity(),false,txtQI473F);  
    		q1.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    		verificar473e();
    	}
    	else {
    		Util.cleanAndLockView(getActivity(),txtQI473F);  
    		q1.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    }
    
    public void verificar473e() {
    	if(App.getInstance().getSeccion04B().qi473d_a==1) {
    		Util.lockView(getActivity(),false,txtQI473F);  
    		q1.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    	}
    	else {
    		Util.cleanAndLockView(getActivity(),txtQI473F);  
    		q1.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
    }
    
	
	public void onqrgQI474ChangeValue() {		
		if(rgQI474.getValue()!=null) {
			if (rgQI474.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,chbQI474A_A,chbQI474A_B,chbQI474A_C,chbQI474A_D,chbQI474A_E,chbQI474A_F,chbQI474A_G,
						chbQI474A_H,chbQI474A_I,chbQI474A_J,chbQI474A_K,chbQI474A_L,chbQI474A_M,chbQI474A_N,chbQI474A_O,chbQI474A_P,
						chbQI474A_X,txtQI474A_XO);  
				App.getInstance().getSeccion04B().qi474 = 1;
	    		q3.setVisibility(View.VISIBLE);    		
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI474A_A,chbQI474A_B,chbQI474A_C,chbQI474A_D,chbQI474A_E,chbQI474A_F,chbQI474A_G,
	    				chbQI474A_H,chbQI474A_I,chbQI474A_J,chbQI474A_K,chbQI474A_L,chbQI474A_M,chbQI474A_N,chbQI474A_O,chbQI474A_P,
	    				chbQI474A_X,txtQI474A_XO);
	    		if(rgQI474.getValue()!=null)
	    			App.getInstance().getSeccion04B().qi474 = Integer.parseInt(rgQI474.getValue().toString());
	    		q3.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	
	public boolean verificarCheck474a() {
  		if (chbQI474A_A.isChecked() || chbQI474A_B.isChecked() || chbQI474A_C.isChecked() || chbQI474A_D.isChecked() || 
  			chbQI474A_E.isChecked() || chbQI474A_F.isChecked() || chbQI474A_G.isChecked() || chbQI474A_H.isChecked() ||
  			chbQI474A_I.isChecked() || chbQI474A_J.isChecked() || chbQI474A_K.isChecked() || chbQI474A_L.isChecked() ||
  			chbQI474A_M.isChecked() || chbQI474A_N.isChecked() || chbQI474A_O.isChecked() || chbQI474A_P.isChecked() ||
  			chbQI474A_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}			
	
	public void onqrgQI474axChangeValue() {  	
		
		if (chbQI474A_X.isChecked()) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
			Util.lockView(getActivity(),false,txtQI474A_XO);      		
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI474A_XO);    		
    		//Util.lockView(getActivity(),false,chbQI407_Y);  				
    	}	
    }
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta474.setText(lblpregunta474.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI474.readOnly();
			txtQI473F.readOnly();
			txtQI474A.readOnly();
			txtQI474A_XO.readOnly();
			chbQI474A_A.readOnly();
			chbQI474A_B.readOnly();
			chbQI474A_C.readOnly();
			chbQI474A_D.readOnly();
			chbQI474A_E.readOnly();
			chbQI474A_F.readOnly();
			chbQI474A_G.readOnly();
			chbQI474A_H.readOnly();
			chbQI474A_I.readOnly();
			chbQI474A_J.readOnly();
			chbQI474A_K.readOnly();
			chbQI474A_L.readOnly();
			chbQI474A_M.readOnly();
			chbQI474A_N.readOnly();
			chbQI474A_O.readOnly();
			chbQI474A_P.readOnly();
			chbQI474A_X.readOnly();
		}
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		int cont=0;		
		if(c2seccion_04b.qi474a_a==1)
			cont+=1;
		if(c2seccion_04b.qi474a_b==1)
			cont+=1;
		if(c2seccion_04b.qi474a_c==1)
			cont+=1;
		if(c2seccion_04b.qi474a_d==1)
			cont+=1;
		if(c2seccion_04b.qi474a_e==1)
			cont+=1;
		if(c2seccion_04b.qi474a_f==1)
			cont+=1;
		if(c2seccion_04b.qi474a_g==1)
			cont+=1;
		if(c2seccion_04b.qi474a_h==1)
			cont+=1;
		if(c2seccion_04b.qi474a_i==1)
			cont+=1;
		if(c2seccion_04b.qi474a_j==1)
			cont+=1;
		if(c2seccion_04b.qi474a_k==1)
			cont+=1;
		if(c2seccion_04b.qi474a_l==1)
			cont+=1;
		if(c2seccion_04b.qi474a_m==1)
			cont+=1;
		if(c2seccion_04b.qi474a_n==1)
			cont+=1;
		if(c2seccion_04b.qi474a_o==1)
			cont+=1;
		if(c2seccion_04b.qi474a_p==1)
			cont+=1;
		if(c2seccion_04b.qi474a_x==1)
			cont+=1;
		
		
		App.getInstance().getSeccion04B().qi474 = c2seccion_04b.qi474;
		
		App.getInstance().getSeccion04B().filtro474a = cont;
		App.getInstance().getSeccion04B().qi474a_a=c2seccion_04b.qi474a_a;
		App.getInstance().getSeccion04B().qi474a_b=c2seccion_04b.qi474a_b;
		App.getInstance().getSeccion04B().qi474a_c=c2seccion_04b.qi474a_c;
		App.getInstance().getSeccion04B().qi474a_d=c2seccion_04b.qi474a_d;
		App.getInstance().getSeccion04B().qi474a_e=c2seccion_04b.qi474a_e;
		App.getInstance().getSeccion04B().qi474a_f=c2seccion_04b.qi474a_f;
		App.getInstance().getSeccion04B().qi474a_g=c2seccion_04b.qi474a_g;
		App.getInstance().getSeccion04B().qi474a_h=c2seccion_04b.qi474a_h;
		App.getInstance().getSeccion04B().qi474a_i=c2seccion_04b.qi474a_i;
		App.getInstance().getSeccion04B().qi474a_j=c2seccion_04b.qi474a_j;
		App.getInstance().getSeccion04B().qi474a_k=c2seccion_04b.qi474a_k;
		App.getInstance().getSeccion04B().qi474a_l=c2seccion_04b.qi474a_l;
		App.getInstance().getSeccion04B().qi474a_m=c2seccion_04b.qi474a_m;
		App.getInstance().getSeccion04B().qi474a_n=c2seccion_04b.qi474a_n;
		App.getInstance().getSeccion04B().qi474a_o=c2seccion_04b.qi474a_o;
		App.getInstance().getSeccion04B().qi474a_p=c2seccion_04b.qi474a_p;
		App.getInstance().getSeccion04B().qi474a_x=c2seccion_04b.qi474a_x;
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
