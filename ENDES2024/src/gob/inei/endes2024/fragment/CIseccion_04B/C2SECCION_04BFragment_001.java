package gob.inei.endes2024.fragment.CIseccion_04B;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_001 extends FragmentForm { 
	CISECCION_01_03 individual; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo; 
	public GridComponent2 gridPreguntas802B,gridPreguntas802C,gridPreguntas802D;
	public TableComponent tcNacimiento;

	public List<CISECCION_02> detalles;
	CISECCION_04DIT_02 ninio;
	CISECCION_02 hnacimiento=null;
	LinearLayout q0; 
	LinearLayout q1;  

	SeccionCapitulo[] seccionesCargado,seccionesCargadoSeccion01,seccionesCargadoDit; 
	Seccion01ClickListener adapter;
	
	public TextField txtCabecera;
	
	public CISECCION_02 nacimiento;
	public C2SECCION_04BFragment_001() {} 
	public C2SECCION_04BFragment_001 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		adapter = new Seccion01ClickListener();
		tcNacimiento.getListView().setOnItemClickListener(adapter);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","QI215D","QI215M","QI215Y","QI216","QI217","QI218","ESTADOCAP4B")};
		seccionesCargadoDit = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478","QI212_NOM","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO")};

		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
		lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
//		 Integer d=getResources().getDisplayMetrics().densityDpi;
//		  if(!Util.esDiferente(d,App.YOGA8)){
//			  tcNacimiento = new TableComponent(getActivity(), this,App.ESTILO).size(600, 770).headerHeight(altoComponente + 30).dataColumHeight(60);
//			}
//			else{
				tcNacimiento = new TableComponent(getActivity(), this,App.ESTILO).size(700, 770).headerHeight(45).dataColumHeight(40).headerTextSize(15);
//		}
		tcNacimiento.addHeader(R.string.seccion01_nro_ordens2, 0.3f,TableComponent.ALIGN.CENTER);
		tcNacimiento.addHeader(R.string.seccion01nombress2, 1.0f,TableComponent.ALIGN.LEFT);
		tcNacimiento.addHeader(R.string.seccion01_fechanacimiento, 0.6f,TableComponent.ALIGN.LEFT);
		tcNacimiento.addHeader(R.string.seccion01_estavivo, 0.5f,TableComponent.ALIGN.LEFT);
		tcNacimiento.addHeader(R.string.seccion01_anioscumplidos, 0.4f,TableComponent.ALIGN.LEFT);
		tcNacimiento.addHeader(R.string.seccion01_conviviendo, 0.5f,TableComponent.ALIGN.LEFT);
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		
		q0 = createQuestionSection(lblTitulo);
		q1 = createQuestionSection(tcNacimiento.getTableView()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1); 

    return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
    	if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
    	if(App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B2()!=null){
    		App.getInstance().getSeccion04B2().filtro479=getCuestionarioService().ExisteninioMenoraTresAniosyVivo(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    	}
		return true; 
    } 
    private boolean validar() {  
//    	Log.d("Mensaje","test validar booleano "+!getCuestionarioService().getSeccion4BCompletado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id) +" - "+!parent.isEsAvanceDual());
    	if(!getCuestionarioService().getSeccion4BCompletado(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id) && !parent.isEsAvanceDual()) {
			 
			mensaje = "Debe completar todos los registros en 4B"; 
			view = tcNacimiento; 
			error = true; 
			return false; 
    	}
    	
    	return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
//    	Log.d("Mensaje","test carga Datos");
    	Calendar fechaactual = Calendar.getInstance();
    	
    	if(App.getInstance().getHogar()!=null && App.getInstance().getPersonaCuestionarioIndividual()!=null){
    		App.getInstance().getSeccion04B2().filtro479=getCuestionarioService().ExisteninioMenoraTresAniosyVivo(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    		App.getInstance().getSeccion04B2().filtro480=getCuestionarioService().VerificarFiltro480(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id);
    		App.getInstance().getSeccion04B2().filtro481=getCuestionarioService().VerificarFiltro481(App.getInstance().getPersonaCuestionarioIndividual().id, App.getInstance().getPersonaCuestionarioIndividual().hogar_id, App.getInstance().getPersonaCuestionarioIndividual().persona_id, fechaactual.get(Calendar.YEAR)-5);
    		cargarTabla();
    		App.getInstance().setNacimiento(null);
    		inicio();     	
    	}
    } 
    
    public void cargarTabla() {    	
    	detalles = getCuestionarioService().getListaNacimientos4bCompletobyPersona(App.getInstance().getPersonaCuestionarioIndividual().id,App.getInstance().getPersonaCuestionarioIndividual().hogar_id,App.getInstance().getPersonaCuestionarioIndividual().persona_id, seccionesCargado);
    	
    	tcNacimiento.setData(detalles, "getQi212", "getQi212_nom","getQi215","getQi216","getQi217","getQi218");
    	    	
    	for (int row = 0; row < detalles.size(); row++) {
    		if (obtenerEstado(detalles.get(row)) == 1) {
				// borde de color azul
				tcNacimiento.setBorderRow(row, true);
			} else if (obtenerEstado(detalles.get(row)) == 2) {
				// borde de color rojo
				tcNacimiento.setBorderRow(row, true, R.color.red);
			} else {
				tcNacimiento.setBorderRow(row, false);
			}
		}

		tcNacimiento.reloadData();
		
		registerForContextMenu(tcNacimiento.getListView());	
        
		adapter = new Seccion01ClickListener();
		tcNacimiento.getListView().setOnItemClickListener(adapter);
		/*MarcoAdapter adapter = new MarcoAdapter(marcos);
		tcMarco.getListView().setOnItemClickListener(adapter);
    	*/        	    	
    }
    
    private int obtenerEstado(CISECCION_02 detalle) {
//		Log.d("Mensaje", "test Estado cap 4b "+detalle.estadocap4b);
    	if (!Util.esDiferente(detalle.estadocap4b, 0)) {
			return 1 ;
		} else if (!Util.esDiferente(detalle.estadocap4b,1) || !Util.esDiferente(detalle.estadocap4b,2)) {
			return 2;
		}
		return 0;
	}
    
    private void inicio() {
    	parent.setEsAvanceDual(false);
    	//txtCabecera.requestFocus();
    } 
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }

    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                    ContextMenuInfo menuInfo) {
            super.onCreateContextMenu(menu, v, menuInfo);
            if (v.equals(tcNacimiento.getListView())) {
                    menu.setHeaderTitle("Opciones de las Visitas");
                    menu.add(0, 0, 1, "Editar");
            }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	if (!getUserVisibleHint())
    		return false;
    		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            if (item.getGroupId() == 0) {
            	switch (item.getItemId()) {
            	case 0: 
            		nacimiento = new CISECCION_02();
            		nacimiento = (CISECCION_02) detalles.get(info.position);
            		EditarIndividual(nacimiento);
            		break;
            	}
            }
            return super.onContextItemSelected(item);
    }
    
    public void EditarIndividual(CISECCION_02 tmp) {
    	ninio = getCuestionarioService().getCISECCION_04B_dit2(tmp.id,tmp.hogar_id,tmp.persona_id,tmp.qi212,seccionesCargadoDit);
    	App.getInstance().setNacimiento(tmp);
    	App.getInstance().setNacimientoVivos(tmp);
    	parent.setEsAvanceDual(true);  
    	App.getInstance().setSeccion04DIT_02(ninio);
//    		if(ninio!=null && ninio.qi478!=null && ninio.qi478>=9 && ninio.qi478<=60){      			
//    			Log.e("dd",""+"222");
//    			parent.nextFragment(CuestionarioFragmentActivity.CISECCION_04Bf_1+1);
//
//    		}
//    	    Log.e("","NINIO: "+ninio.qi478);
//    	    Log.e("","NINIO: "+App.ANIOPORDEFECTO);
    		if( ninio!=null  && ninio.qi478!=null && ninio.qi478>=App.SOLODITMIN && ninio.qi478<=App.SOLODITMAX && tmp.qi215y<App.ANIOPORDEFECTO){
    			Log.e("frang",""+"222_");
    			parent.nextFragment(CuestionarioFragmentActivity.CISECCION_04Bf_20_6);
    		} 
    		else{
    			Log.e("frag_1",""+"333");
    			parent.nextFragment(CuestionarioFragmentActivity.CISECCION_04Bf_1+1);
    		}
    		
	}
    
    
	
	public class Seccion01ClickListener implements OnItemClickListener {
		public Seccion01ClickListener() {
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			nacimiento = new CISECCION_02();
			nacimiento = (CISECCION_02) detalles.get(arg2);
			EditarIndividual(detalles.get(arg2));
		}
	}

	@Override
	public Integer grabadoParcial() {
		return App.NODEFINIDO;
	}
} 

