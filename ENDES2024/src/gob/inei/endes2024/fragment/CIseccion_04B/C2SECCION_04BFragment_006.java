package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_006 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI465DB_A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI465DB_B; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI465DB_C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI465DB_D; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQI465DB_DX; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI465DD_AR; 
	@FieldAnnotation(orderIndex=7) 
	public IntegerField txtQI465DD_AC; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI465DD_BR; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI465DD_BC; 
	@FieldAnnotation(orderIndex=10) 
	public IntegerField txtQI465DD_CR; 
	@FieldAnnotation(orderIndex=11) 
	public IntegerField txtQI465DD_CC; 
	@FieldAnnotation(orderIndex=12) 
	public IntegerField txtQI465DD_DR; 
	@FieldAnnotation(orderIndex=13) 
	public IntegerField txtQI465DD_DC; 
	
	CISECCION_04B c2seccion_04b; 
	CISECCION_02 ninio;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465db,lblpregunta465dd,lblblanco,lblP465db_1,lblP465db_2,lblP465db_3,lblQI465dbA,
	lblQI465dbA2,lblQI465dbB,lblQI465dbB2,lblQI465dbC,lblQI465dbC2,	 lblQI465dbD,lblQI465dbD2,lblP465dda_1,lblP465dda_2,lblQI465ddA,
	lblQI465ddA2,lblQI465ddB,lblQI465ddB2,lblQI465ddC,lblQI465ddC2,lblQI465ddD,lblQI465ddD2,lblpregunta465db_ind,lblpregunta465dd_ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465DB_A","QI465DB_B","QI465DB_C","QI465DB_D","QI465DB_DX","QI465DD_AR","QI465DD_AC","QI465DD_BR","QI465DD_BC","QI465DD_CR","QI465DD_CC","QI465DD_DR","QI465DD_DC")};
	SeccionCapitulo[] seccionesCargado,seccionesPersonas; 

	public CISECCION_02 nacimiento;
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 grid1,grid1b;
	public C2SECCION_04BFragment_006() {} 
	public C2SECCION_04BFragment_006 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
				
		rango(getActivity(), txtQI465DD_AR, 0, 360,  null, 998); 
		rango(getActivity(), txtQI465DD_AC, 0, 360, null, 998); 
		rango(getActivity(), txtQI465DD_BR, 0, 360, null, 998); 
		rango(getActivity(), txtQI465DD_BC, 0, 360, null, 998); 
		rango(getActivity(), txtQI465DD_CR, 0, 360,  null, 998); 
		rango(getActivity(), txtQI465DD_CC, 0, 360,  null, 998); 
		rango(getActivity(), txtQI465DD_DR, 0, 360,  null, 998); 
		rango(getActivity(), txtQI465DD_DC, 0, 360,  null, 998); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465DB_A","QI465DB_B","QI465DB_C","QI465DB_D","QI465DB_DX","QI465DD_AR","QI465DD_AC","QI465DD_BR","QI465DD_BC","QI465DD_CR","QI465DD_CC","QI465DD_DR","QI465DD_DC","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
		seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI212","QI212_NOM","QI213","QI214","QI215D","QI215M","QI215Y","QI216","QI217","QI217CONS","QI218","QI219","QI220U","QI220N","QI220A","QI221","ID","HOGAR_ID","PERSONA_ID") };
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta465db = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");	
	    lblpregunta465db_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465db_ind);
	    lblpregunta465dd = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dd).negrita();
	    lblpregunta465dd_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dd_ind);
	    txtQI465DB_DX= new TextField(getActivity()).size(altoComponente, 650);
	   
	    
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP465db_1 = new LabelComponent(getActivity()).textSize(16).size(50, 99).text(R.string.c2seccion_04bqi465db_1).centrar();
		lblP465db_2 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi465db_2).centrar();
		lblP465db_3 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi465db_3).centrar();
		 		
		lblQI465dbA=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi465db_a);		
		rgQI465DB_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465db_o1,R.string.c2seccion_04bqi465db_o2,R.string.c2seccion_04bqi465db_o3).size(altoComponente,200).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465dbaChangeValue"); 
				
		lblQI465dbB=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi465db_b);	
		rgQI465DB_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465db_o1,R.string.c2seccion_04bqi465db_o2,R.string.c2seccion_04bqi465db_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465dbbChangeValue"); 
		
		lblQI465dbC=new LabelComponent(getActivity()).textSize(17).size(altoComponente+15, 450).text(R.string.c2seccion_04bqi465db_c);
		rgQI465DB_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465db_o1,R.string.c2seccion_04bqi465db_o2,R.string.c2seccion_04bqi465db_o3).size(altoComponente+15,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465dbcChangeValue");
				
		lblQI465dbD=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi465db_d);
		rgQI465DB_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465db_o1,R.string.c2seccion_04bqi465db_o2,R.string.c2seccion_04bqi465db_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465dbdChangeValue");  
		 
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI465dbA);
		grid1.addComponent(rgQI465DB_A);		
		grid1.addComponent(lblQI465dbB);
		grid1.addComponent(rgQI465DB_B);		
		grid1.addComponent(lblQI465dbC);
		grid1.addComponent(rgQI465DB_C);		
		grid1.addComponent(lblQI465dbD);			
		grid1.addComponent(rgQI465DB_D);		
		grid1.addComponent(txtQI465DB_DX,2);
		
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(altoComponente, 480).negrita().centrar();
		lblP465dda_1 = new LabelComponent(getActivity()).textSize(14).size(60, 130).text(R.string.c2seccion_04bqi465ddr).centrar();
		lblP465dda_2 = new LabelComponent(getActivity()).textSize(15).size(60, 140).text(R.string.c2seccion_04bqi465ddc).centrar();

		lblQI465ddA=new LabelComponent(getActivity()).textSize(17).size(altoComponente*2, 480).text(R.string.c2seccion_04bqi465dd_a);		
		lblQI465ddA2=new LabelComponent(getActivity()).textSize(16).size(altoComponente, 270).text(R.string.c2seccion_04bqi465ddf).centrar();	
		txtQI465DD_AR=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(3); 
		txtQI465DD_AC=new IntegerField(this.getActivity()).size(altoComponente, 140).maxLength(3); 	
		
		lblQI465ddB=new LabelComponent(getActivity()).textSize(17).size(altoComponente*2, 480).text(R.string.c2seccion_04bqi465dd_b);	
		lblQI465ddB2=new LabelComponent(getActivity()).textSize(16).size(altoComponente, 270).text(R.string.c2seccion_04bqi465ddf).centrar();		
		txtQI465DD_BR=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(3); 
		txtQI465DD_BC=new IntegerField(this.getActivity()).size(altoComponente, 140).maxLength(3); 
		
		lblQI465ddC=new LabelComponent(getActivity()).textSize(17).size(altoComponente*2, 480).text(R.string.c2seccion_04bqi465dd_c);
		lblQI465ddC2=new LabelComponent(getActivity()).textSize(16).size(altoComponente, 270).text(R.string.c2seccion_04bqi465ddu).centrar();	
		txtQI465DD_CR=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(3); 
		txtQI465DD_CC=new IntegerField(this.getActivity()).size(altoComponente, 140).maxLength(3).callback("onqi465ddccChangeValue"); 
	
				
		lblQI465ddD=new LabelComponent(getActivity()).textSize(17).size(altoComponente*2, 480).text(R.string.c2seccion_04bqi465dd_d);
		lblQI465ddD2=new LabelComponent(getActivity()).textSize(16).size(altoComponente, 270).text(R.string.c2seccion_04bqi465ddu).centrar();
		txtQI465DD_DR=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(3); 
		txtQI465DD_DC=new IntegerField(this.getActivity()).size(altoComponente, 140).maxLength(3); 
		 
		grid1b=new GridComponent2(this.getActivity(),App.ESTILO,3,1);
		grid1b.addComponent(lblblanco);
		grid1b.addComponent(lblP465dda_1);
		grid1b.addComponent(lblP465dda_2);

		grid1b.addComponent(lblQI465ddA,1,2);
		grid1b.addComponent(lblQI465ddA2,2,1);
		grid1b.addComponent(txtQI465DD_AR);
		grid1b.addComponent(txtQI465DD_AC);
		
		grid1b.addComponent(lblQI465ddB,1,2);
		grid1b.addComponent(lblQI465ddB2,2,1);
		grid1b.addComponent(txtQI465DD_BR);
		grid1b.addComponent(txtQI465DD_BC);
		
		grid1b.addComponent(lblQI465ddC,1,2);
		grid1b.addComponent(lblQI465ddC2,2,1);
		grid1b.addComponent(txtQI465DD_CR);
		grid1b.addComponent(txtQI465DD_CC);
		
		grid1b.addComponent(lblQI465ddD,1,2);
		grid1b.addComponent(lblQI465ddD2,2,1);
		grid1b.addComponent(txtQI465DD_DR);		
		grid1b.addComponent(txtQI465DD_DC);
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465db,lblpregunta465db_ind,grid1.component());
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465dd,lblpregunta465dd_ind,grid1b.component()); 

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi465db_a!=null) {
				c2seccion_04b.qi465db_a=c2seccion_04b.getConvertQi465db_a(c2seccion_04b.qi465db_a);
				App.getInstance().getSeccion04B().qi465db_a =c2seccion_04b.qi465db_a;
			}			
			App.getInstance().getSeccion04B().qi465db_a =c2seccion_04b.qi465db_a;
			if (c2seccion_04b.qi465db_b!=null) {
				c2seccion_04b.qi465db_b=c2seccion_04b.getConvertQi465db_b(c2seccion_04b.qi465db_b);
				
			}	
			App.getInstance().getSeccion04B().qi465db_b =c2seccion_04b.qi465db_b;
			if (c2seccion_04b.qi465db_c!=null) {
				c2seccion_04b.qi465db_c=c2seccion_04b.getConvertQi465db_c(c2seccion_04b.qi465db_c);
				
			}	
			App.getInstance().getSeccion04B().qi465db_c =c2seccion_04b.qi465db_c;
			if (c2seccion_04b.qi465db_d!=null) {
				c2seccion_04b.qi465db_d=c2seccion_04b.getConvertQi465db_d(c2seccion_04b.qi465db_d);
				
			}
			App.getInstance().getSeccion04B().qi465db_d =c2seccion_04b.qi465db_d;
			
			App.getInstance().getSeccion04B().qi465dd_ac =c2seccion_04b.qi465dd_ac;
			App.getInstance().getSeccion04B().qi465dd_bc =c2seccion_04b.qi465dd_bc;
			App.getInstance().getSeccion04B().qi465dd_cc =c2seccion_04b.qi465dd_cc;
			App.getInstance().getSeccion04B().qi465dd_dc =c2seccion_04b.qi465dd_dc;
			
			App.getInstance().getSeccion04B().qi465dd_ar =c2seccion_04b.qi465dd_ar;
			App.getInstance().getSeccion04B().qi465dd_br =c2seccion_04b.qi465dd_br;
			App.getInstance().getSeccion04B().qi465dd_cr =c2seccion_04b.qi465dd_cr;
			App.getInstance().getSeccion04B().qi465dd_dr =c2seccion_04b.qi465dd_dr;
			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(!(App.getInstance().getSeccion04B().qi465db_a!=null && App.getInstance().getSeccion04B().qi465db_a==2 && App.getInstance().getSeccion04B().qi465db_b!=null && App.getInstance().getSeccion04B().qi465db_b==2 && App.getInstance().getSeccion04B().qi465db_c!=null && App.getInstance().getSeccion04B().qi465db_c==2&& App.getInstance().getSeccion04B().qi465db_d!=null && App.getInstance().getSeccion04B().qi465db_d==2) 
            		 &&   !(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi465dd_cc!=null && App.getInstance().getSeccion04B().qi465dd_cc > 0)
            		 &&   !((App.getInstance().getSeccion04B().qi465dd_ac !=null && App.getInstance().getSeccion04B().qi465dd_ar!=null &&  App.getInstance().getSeccion04B().qi465dd_ac < App.getInstance().getSeccion04B().qi465dd_ar) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_bc !=null && App.getInstance().getSeccion04B().qi465dd_br!=null && App.getInstance().getSeccion04B().qi465dd_bc < App.getInstance().getSeccion04B().qi465dd_br) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_cc !=null && App.getInstance().getSeccion04B().qi465dd_cr!=null && App.getInstance().getSeccion04B().qi465dd_cc < App.getInstance().getSeccion04B().qi465dd_cr) || 
            		 		(App.getInstance().getSeccion04B().qi465dd_dc !=null && App.getInstance().getSeccion04B().qi465dd_dr!=null && App.getInstance().getSeccion04B().qi465dd_dc < App.getInstance().getSeccion04B().qi465dd_dr))) {
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_007.seccionesGrabado);
					App.getInstance().getSeccion04B().filtro465de = false;
					App.getInstance().getSeccion04B().filtro465dg = false;
					App.getInstance().getSeccion04B().filtro465di = false;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);					
				} 
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(App.getInstance().getSeccion04B().qi465da)
		{
			if (Util.esVacio(c2seccion_04b.qi465db_a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DB_A"); 
				view = rgQI465DB_A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi465db_b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DB_B"); 
				view = rgQI465DB_B; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi465db_c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DB_C"); 
				view = rgQI465DB_C; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi465db_d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DB_D"); 
				view = rgQI465DB_D; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi465db_d!=null && c2seccion_04b.qi465db_d==1) {
				if (Util.esVacio(c2seccion_04b.qi465db_dx)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI465DB_DX"); 
					view = txtQI465DB_DX; 
					error = true; 
					return false; 
				} 
			}
			if((TotalDeEdadEnDias()/30)<=5.5 && !Util.esDiferente(c2seccion_04b.qi465db_c, 1)){
				 validarMensaje("Verifique Edad en meses del Ni�o(a)");
//				mensaje ="Verificar Ni�o(a) tiene: "+(int)(TotalDeEdadEnDias()/30)+" Meses"; 
//				view = rgQI465DB_C; 
//				error = true; 
//				return false;
			 }
			
			if(verificar465db()) {
				
				if (c2seccion_04b.qi465db_a==1) {
					if (Util.esVacio(c2seccion_04b.qi465dd_ar)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_AR"); 
						view = txtQI465DD_AR; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(c2seccion_04b.qi465dd_ac)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_AC"); 
						view = txtQI465DD_AC; 
						error = true; 
						return false; 
					} 
					
					if(c2seccion_04b.qi465dd_ar<c2seccion_04b.qi465dd_ac ) {
						mensaje = "Valor recibido debe ser mayor que consumido"; 
						view = txtQI465DD_AR; 
						error = true; 
						return false;
					}
				}
				
				if (c2seccion_04b.qi465db_b==1) {
					if (Util.esVacio(c2seccion_04b.qi465dd_br)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_BR"); 
						view = txtQI465DD_BR; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(c2seccion_04b.qi465dd_bc)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_BC"); 
						view = txtQI465DD_BC; 
						error = true; 
						return false; 
					} 
					
					if(c2seccion_04b.qi465dd_br<c2seccion_04b.qi465dd_bc ) {
						mensaje = "Valor recibido debe ser mayor que consumido"; 
						view = txtQI465DD_BR; 
						error = true; 
						return false;
					}
					if((TotalDeEdadEnDias()/30)>=5.6 && !Util.esDiferente(c2seccion_04b.qi465db_b,1)){
						validarMensaje("Verifique Edad en meses del Ni�o(a)");
				    }
				}
				
				if (c2seccion_04b.qi465db_c==1) {
					if (Util.esVacio(c2seccion_04b.qi465dd_cr)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_CR"); 
						view = txtQI465DD_CR; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(c2seccion_04b.qi465dd_cc)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_CC"); 
						view = txtQI465DD_CC; 
						error = true; 
						return false; 
					}
					
					if(c2seccion_04b.qi465dd_cr<c2seccion_04b.qi465dd_cc ) {
						mensaje = "Valor recibido debe ser mayor que consumido"; 
						view = txtQI465DD_CR; 
						error = true; 
						return false;
					}
					Long totaldiasdespuesdecuatromese=TotalDeEdadEnDias()-30*4;
					if(Util.esMayor(c2seccion_04b.qi465dd_cc, (int) (totaldiasdespuesdecuatromese+3))){
						mensaje ="Verificar la cantidad consumida, M�ximo: "+totaldiasdespuesdecuatromese;
						view = txtQI465DD_CC; 
						error = true; 
						return false;
					}
				}
				if (c2seccion_04b.qi465db_d==1) {
					if (Util.esVacio(c2seccion_04b.qi465dd_dr)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_DR"); 
						view = txtQI465DD_DR; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(c2seccion_04b.qi465dd_dc)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465DD_DC"); 
						view = txtQI465DD_DC; 
						error = true; 
						return false; 
					}  
					if(c2seccion_04b.qi465dd_dr<c2seccion_04b.qi465dd_dc ) {
						mensaje = "Valor recibido debe ser mayor que consumido"; 
						view = txtQI465DD_DR; 
						error = true; 
						return false;
					}
				}
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		ninio=getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, seccionesPersonas); 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi465db_a!=null) {
				c2seccion_04b.qi465db_a=c2seccion_04b.setConvertQi465db_a(c2seccion_04b.qi465db_a);
			}			
			if (c2seccion_04b.qi465db_b!=null) {
				c2seccion_04b.qi465db_b=c2seccion_04b.setConvertQi465db_b(c2seccion_04b.qi465db_b);
			}			
			if (c2seccion_04b.qi465db_c!=null) {
				c2seccion_04b.qi465db_c=c2seccion_04b.setConvertQi465db_c(c2seccion_04b.qi465db_c);
			}			
			if (c2seccion_04b.qi465db_d!=null) {
				c2seccion_04b.qi465db_d=c2seccion_04b.setConvertQi465db_d(c2seccion_04b.qi465db_d);
			}			
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	evaluar465da();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
	public Long TotalDeEdadEnDias(){
		Calendar fecha_actual=null;
		if(ninio.qi217cons==null){
			fecha_actual=Calendar.getInstance();
		}
		else{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(ninio.qi217cons);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecha_actual = cal;
		}
		Calendar nacimiento = new GregorianCalendar(ninio.qi215y,Integer.parseInt(ninio.qi215m.toString()),Integer.parseInt(ninio.qi215d.toString()));
		Long totaldias=MyUtil.CantidaddeDias(fecha_actual, nacimiento);
		return totaldias;
	}
	
    public void evaluar465da() {
    	if(App.getInstance().getSeccion04B().qi216==1 && App.getInstance().getSeccion04B().filtro215 && App.getInstance().getSeccion04B().qi218==1) {	
    		Util.lockView(getActivity(),false,rgQI465DB_A,rgQI465DB_B,rgQI465DB_C,rgQI465DB_D,txtQI465DB_DX,txtQI465DD_AC,txtQI465DD_AR,txtQI465DD_BC,txtQI465DD_BR,txtQI465DD_CC,txtQI465DD_CR,txtQI465DD_DC,txtQI465DD_DR);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		App.getInstance().getSeccion04B().qi465da = true;
    		onqrgQI465dbaChangeValue();
        	onqrgQI465dbbChangeValue();
        	onqrgQI465dbcChangeValue();
        	onqrgQI465dbdChangeValue();
						
    	}    	
    	else {
			Util.cleanAndLockView(getActivity(),rgQI465DB_A,rgQI465DB_B,rgQI465DB_C,rgQI465DB_D,txtQI465DB_DX,txtQI465DD_AC,txtQI465DD_AR,txtQI465DD_BC,txtQI465DD_BR,txtQI465DD_CC,txtQI465DD_CR,txtQI465DD_DC,txtQI465DD_DR);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		App.getInstance().getSeccion04B().qi465da = false;
    	}
    }
    
	
	public void mostrar465dd() {
		Util.lockView(getActivity(),false,txtQI465DD_AC,txtQI465DD_AR,txtQI465DD_BC,txtQI465DD_BR,txtQI465DD_CC,txtQI465DD_CR,txtQI465DD_DC,txtQI465DD_DR);			
		if(rgQI465DB_A.getValue()!=null) {
			if(!rgQI465DB_A.getValue().toString().equals("1")) {	
				Util.cleanAndLockView(getActivity(),txtQI465DD_AC,txtQI465DD_AR);
				txtQI465DD_AC.setVisibility(View.GONE);
				txtQI465DD_AR.setVisibility(View.GONE);
				lblQI465ddA.setVisibility(View.GONE);
				lblQI465ddA2.setVisibility(View.GONE);
			}
			else {
				txtQI465DD_AC.setVisibility(View.VISIBLE);
				txtQI465DD_AR.setVisibility(View.VISIBLE);
				lblQI465ddA.setVisibility(View.VISIBLE);
				lblQI465ddA2.setVisibility(View.VISIBLE);
			}
		}
		
		if(rgQI465DB_B.getValue()!=null ) {
			if(!rgQI465DB_B.getValue().toString().equals("1")) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),txtQI465DD_BC,txtQI465DD_BR);
				txtQI465DD_BC.setVisibility(View.GONE);
				txtQI465DD_BR.setVisibility(View.GONE);
				lblQI465ddB.setVisibility(View.GONE);
				lblQI465ddB2.setVisibility(View.GONE);
			}
			else {
				txtQI465DD_BC.setVisibility(View.VISIBLE);
				txtQI465DD_BR.setVisibility(View.VISIBLE);
				lblQI465ddB.setVisibility(View.VISIBLE);
				lblQI465ddB2.setVisibility(View.VISIBLE);
			}
		}
		
		if(rgQI465DB_C.getValue()!=null) {
			if(!rgQI465DB_C.getValue().toString().equals("1")) {	
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),txtQI465DD_CC,txtQI465DD_CR);
				txtQI465DD_CC.setVisibility(View.GONE);
				txtQI465DD_CR.setVisibility(View.GONE);
				lblQI465ddC.setVisibility(View.GONE);
				lblQI465ddC2.setVisibility(View.GONE);
			}
			else {
				txtQI465DD_CC.setVisibility(View.VISIBLE);
				txtQI465DD_CR.setVisibility(View.VISIBLE);
				lblQI465ddC.setVisibility(View.VISIBLE);
				lblQI465ddC2.setVisibility(View.VISIBLE);
			}
		}
		
		if(rgQI465DB_D.getValue()!=null) {
			if(!rgQI465DB_D.getValue().toString().equals("1")) {
				MyUtil.LiberarMemoria();
				Util.cleanAndLockView(getActivity(),txtQI465DD_DC,txtQI465DD_DR);
				txtQI465DD_DC.setVisibility(View.GONE);
				txtQI465DD_DR.setVisibility(View.GONE);
				lblQI465ddD.setVisibility(View.GONE);
				lblQI465ddD2.setVisibility(View.GONE);
			}		
			else {
				txtQI465DD_DC.setVisibility(View.VISIBLE);
				txtQI465DD_DR.setVisibility(View.VISIBLE);
				lblQI465ddD.setVisibility(View.VISIBLE);
				lblQI465ddD2.setVisibility(View.VISIBLE);
			}
		}
		q2.setVisibility(View.VISIBLE);
		
	}
	
	public void ocultar465dd() {
		Util.cleanAndLockView(getActivity(),txtQI465DD_AC,txtQI465DD_AR,txtQI465DD_BC,txtQI465DD_BR,txtQI465DD_CC,txtQI465DD_CR,txtQI465DD_DC,txtQI465DD_DR);				
//		MyUtil.LiberarMemoria();
		q2.setVisibility(View.GONE);
		
	}
	
	public void onqrgQI465dbaChangeValue() {
		if(verificar465db()) {
			mostrar465dd();
			}	
		else { 
			ocultar465dd();
		}		
		if(rgQI465DB_A.getValue()!=null && rgQI465DB_A.getValue().toString().equals("1")) {
			App.getInstance().getSeccion04B().qi465db_a=1;
		} else {
			if(rgQI465DB_A.getValue()!=null)
				App.getInstance().getSeccion04B().qi465db_a= Integer.parseInt(rgQI465DB_A.getValue().toString());
		}
	}
	
	public void onqrgQI465dbbChangeValue() {  
		if(verificar465db()) {
			mostrar465dd();
			}	
		else { 
			ocultar465dd();
		}
		if(rgQI465DB_B.getValue()!=null && rgQI465DB_B.getValue().toString().equals("1")) {
			App.getInstance().getSeccion04B().qi465db_b=1;
		} else {
			if(rgQI465DB_B.getValue()!=null)
				App.getInstance().getSeccion04B().qi465db_b= Integer.parseInt(rgQI465DB_B.getValue().toString());
		}
	 if((TotalDeEdadEnDias()/30)>=5.6 && rgQI465DB_B.getValue()!=null && rgQI465DB_B.getValue().toString().equals("1")){
		 Integer valor=(int) (Math.rint((TotalDeEdadEnDias()/30)*1000)/1000);
		 validarMensaje("Verifique Edad en meses del Ni�o(a)");
	 }
		
	}
	
	private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
	
	public void onqi465ddccChangeValue(){
		Long totaldiasdespuesdecuatromese=TotalDeEdadEnDias()-30*4;
		if(txtQI465DD_CC.getText().toString().trim().length()>0 && Integer.parseInt(txtQI465DD_CC.getText().toString())>totaldiasdespuesdecuatromese){
			 validarMensaje("Verificar la cantidad consumida, M�ximo: "+totaldiasdespuesdecuatromese);
		}
	}
	public void onqrgQI465dbcChangeValue() {  
		if(verificar465db()) {
			mostrar465dd();
			}	
		else { 
			ocultar465dd();
		}
		if(rgQI465DB_C.getValue()!=null && rgQI465DB_C.getValue().toString().equals("1")) {
			App.getInstance().getSeccion04B().qi465db_c=1;
		} else {
			if(rgQI465DB_C.getValue()!=null)
				App.getInstance().getSeccion04B().qi465db_c= Integer.parseInt(rgQI465DB_C.getValue().toString());
		}
		if(rgQI465DB_C.getValue()!=null &&  (TotalDeEdadEnDias()/30)<=5.5 && rgQI465DB_C.getValue().toString().equals("1")){
			 validarMensaje("Verifique Edad en meses del Ni�o(a)");
		 }
	}
	
	public void onqrgQI465dbdChangeValue() {  	
		if(verificar465db()) {
			mostrar465dd();
			}	
		else { 
			ocultar465dd();
		}
		if(rgQI465DB_D.getValue()!=null) {
			if (rgQI465DB_D.getValue().toString().equals("1")) {    		
				App.getInstance().getSeccion04B().qi465db_d=1;
				Util.lockView(getActivity(),false,txtQI465DB_DX);  
	    		
	    	} 
	    	else {	
	    		if(rgQI465DB_D.getValue()!=null)
					App.getInstance().getSeccion04B().qi465db_d= Integer.parseInt(rgQI465DB_D.getValue().toString());
	    		Util.cleanAndLockView(getActivity(),txtQI465DB_DX);    		
	    	}	
		}
    }
	
	public boolean verificar465db() {
		boolean a=false,b=false,c=false,d=false;
		if(rgQI465DB_A.getValue()!=null && rgQI465DB_A.getValue().toString().equals("1")) {			
			a=true;
		}
		
		if(rgQI465DB_B.getValue()!=null && rgQI465DB_B.getValue().toString().equals("1")) {			
			b=true;
		}
		
		if(rgQI465DB_C.getValue()!=null && rgQI465DB_C.getValue().toString().equals("1")) {			
			c=true;
		}
		
		if(rgQI465DB_D.getValue()!=null && rgQI465DB_D.getValue().toString().equals("1")) {			
			d=true;
		}			
		
		return (a || b || c || d);
	}
	
	public void renombrarEtiquetas(){
		lblpregunta465db.setText("(NOMBRE)");
    	lblpregunta465db.setText(lblpregunta465db.getText().toString().replace("(NOMBRE)", nombre_persona));
     	Spanned texto465db =Html.fromHtml("465DB. <b>En los �ltimos 12 meses</b>, �"+lblpregunta465db.getText() +" recibi� del personal del Ministerio de Salud algo para prevenir la anemia como:");
     	lblpregunta465db.setText(texto465db);
    	
    	lblP465dda_2.setText(lblP465dda_2.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta465dd.setText(lblpregunta465dd.getText().toString().replace("(NOMBRE)", nombre_persona));
		
		lblQI465ddA.setText(lblQI465ddA.getText().toString().replace("(NOMBRE)", nombre_persona));
		lblQI465ddB.setText(lblQI465ddB.getText().toString().replace("(NOMBRE)", nombre_persona));
		lblQI465ddC.setText(lblQI465ddC.getText().toString().replace("(NOMBRE)", nombre_persona));
		lblQI465ddD.setText(lblQI465ddD.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI465DB_A.readOnly();
			rgQI465DB_B.readOnly();
			rgQI465DB_C.readOnly();
			rgQI465DB_D.readOnly();
			txtQI465DB_DX.readOnly();
			txtQI465DD_AC.readOnly();
			txtQI465DD_AR.readOnly();
			txtQI465DD_BC.readOnly();
			txtQI465DD_BR.readOnly();
			txtQI465DD_CC.readOnly();
			txtQI465DD_CR.readOnly();
			txtQI465DD_DC.readOnly();
			txtQI465DD_DR.readOnly();
		}
	}
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi465db_a!=null) {
				c2seccion_04b.qi465db_a=c2seccion_04b.getConvertQi465db_a(c2seccion_04b.qi465db_a);
				App.getInstance().getSeccion04B().qi465db_a =c2seccion_04b.qi465db_a;
			}			
			App.getInstance().getSeccion04B().qi465db_a =c2seccion_04b.qi465db_a;
			if (c2seccion_04b.qi465db_b!=null) {
				c2seccion_04b.qi465db_b=c2seccion_04b.getConvertQi465db_b(c2seccion_04b.qi465db_b);
				
			}	
			App.getInstance().getSeccion04B().qi465db_b =c2seccion_04b.qi465db_b;
			if (c2seccion_04b.qi465db_c!=null) {
				c2seccion_04b.qi465db_c=c2seccion_04b.getConvertQi465db_c(c2seccion_04b.qi465db_c);
				
			}	
			App.getInstance().getSeccion04B().qi465db_c =c2seccion_04b.qi465db_c;
			if (c2seccion_04b.qi465db_d!=null) {
				c2seccion_04b.qi465db_d=c2seccion_04b.getConvertQi465db_d(c2seccion_04b.qi465db_d);
				
			}
			App.getInstance().getSeccion04B().qi465db_d =c2seccion_04b.qi465db_d;
			
			App.getInstance().getSeccion04B().qi465dd_ac =c2seccion_04b.qi465dd_ac;
			App.getInstance().getSeccion04B().qi465dd_bc =c2seccion_04b.qi465dd_bc;
			App.getInstance().getSeccion04B().qi465dd_cc =c2seccion_04b.qi465dd_cc;
			App.getInstance().getSeccion04B().qi465dd_dc =c2seccion_04b.qi465dd_dc;
			
			App.getInstance().getSeccion04B().qi465dd_ar =c2seccion_04b.qi465dd_ar;
			App.getInstance().getSeccion04B().qi465dd_br =c2seccion_04b.qi465dd_br;
			App.getInstance().getSeccion04B().qi465dd_cr =c2seccion_04b.qi465dd_cr;
			App.getInstance().getSeccion04B().qi465dd_dr =c2seccion_04b.qi465dd_dr;
			
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(!(App.getInstance().getSeccion04B().qi465db_a!=null && App.getInstance().getSeccion04B().qi465db_a==2 && App.getInstance().getSeccion04B().qi465db_b!=null && App.getInstance().getSeccion04B().qi465db_b==2 && App.getInstance().getSeccion04B().qi465db_c!=null && App.getInstance().getSeccion04B().qi465db_c==2&& App.getInstance().getSeccion04B().qi465db_d!=null && App.getInstance().getSeccion04B().qi465db_d==2) 
            		 &&   !(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi465dd_cc!=null && App.getInstance().getSeccion04B().qi465dd_cc > 0)
            		 &&   !((App.getInstance().getSeccion04B().qi465dd_ac !=null && App.getInstance().getSeccion04B().qi465dd_ar!=null &&  App.getInstance().getSeccion04B().qi465dd_ac < App.getInstance().getSeccion04B().qi465dd_ar) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_bc !=null && App.getInstance().getSeccion04B().qi465dd_br!=null && App.getInstance().getSeccion04B().qi465dd_bc < App.getInstance().getSeccion04B().qi465dd_br) ||
            		 		(App.getInstance().getSeccion04B().qi465dd_cc !=null && App.getInstance().getSeccion04B().qi465dd_cr!=null && App.getInstance().getSeccion04B().qi465dd_cc < App.getInstance().getSeccion04B().qi465dd_cr) || 
            		 		(App.getInstance().getSeccion04B().qi465dd_dc !=null && App.getInstance().getSeccion04B().qi465dd_dr!=null && App.getInstance().getSeccion04B().qi465dd_dc < App.getInstance().getSeccion04B().qi465dd_dr))) {
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_007.seccionesGrabado);
					App.getInstance().getSeccion04B().filtro465de = false;
					App.getInstance().getSeccion04B().filtro465dg = false;
					App.getInstance().getSeccion04B().filtro465di = false;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);					
				} 
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
