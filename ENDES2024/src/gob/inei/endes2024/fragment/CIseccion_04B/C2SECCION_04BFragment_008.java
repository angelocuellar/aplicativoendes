package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.text.DateFormat;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_008 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI465EA; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI465EB; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI465EC;
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI465ED; 
	@FieldAnnotation(orderIndex=5) 
	public TextField txtQI465ED_O;
	
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI465E1_A1;
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI465E1_A2;
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI465E1_A3;
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI465E1_A4;
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI465E1_A5;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI465E1_A6;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI465E1_A7;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI465E1_A8;
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI465E1_A9;
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI465E1_B1;
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI465E1_B2;
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI465E1_B3;
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI465E1_B4;
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI465E1_B5;
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI465E1_B6;
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI465E1_B7;
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI465E1_B8;
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI465E1_B9;
	@FieldAnnotation(orderIndex=24) 
	public CheckBoxField chbQI465E1_C1;
	@FieldAnnotation(orderIndex=25) 
	public CheckBoxField chbQI465E1_C2;
	@FieldAnnotation(orderIndex=26) 
	public CheckBoxField chbQI465E1_C3;
	@FieldAnnotation(orderIndex=27) 
	public CheckBoxField chbQI465E1_C4;
	@FieldAnnotation(orderIndex=28) 
	public CheckBoxField chbQI465E1_C5;
	@FieldAnnotation(orderIndex=29) 
	public CheckBoxField chbQI465E1_C6;
	@FieldAnnotation(orderIndex=30) 
	public CheckBoxField chbQI465E1_C7;
	@FieldAnnotation(orderIndex=31) 
	public CheckBoxField chbQI465E1_C8;
	@FieldAnnotation(orderIndex=32) 
	public CheckBoxField chbQI465E1_C9;
	@FieldAnnotation(orderIndex=33) 
	public CheckBoxField chbQI465E1_D1;
	@FieldAnnotation(orderIndex=34) 
	public CheckBoxField chbQI465E1_D2;
	@FieldAnnotation(orderIndex=35) 
	public CheckBoxField chbQI465E1_D3;
	@FieldAnnotation(orderIndex=36) 
	public CheckBoxField chbQI465E1_D4;
	@FieldAnnotation(orderIndex=37) 
	public CheckBoxField chbQI465E1_D5;
	@FieldAnnotation(orderIndex=38) 
	public CheckBoxField chbQI465E1_D6;
	@FieldAnnotation(orderIndex=39) 
	public CheckBoxField chbQI465E1_D7;
	@FieldAnnotation(orderIndex=40) 
	public CheckBoxField chbQI465E1_D8;
	@FieldAnnotation(orderIndex=41) 
	public CheckBoxField chbQI465E1_D9;
	
	@FieldAnnotation(orderIndex=42) 
	public RadioGroupOtherField rgQI465EB_CC; 
	@FieldAnnotation(orderIndex=43) 
	public TextField txtQI465EB_CC_O;
	
	
	String 	qi465e1_fechref;		
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465e,lblpregunta465e_ind,lblpregunta465eb,lblQI465eA,lblQI465eB,lblQI465eC,lblQI465eD,lblpregunta465eb_ind1,lblpregunta465eb_ind2,lblpregunta465eb_ind3,lblpregunta465e1,
	lblpregunta465e1_ind,lblpregunta465e1_1,lblpregunta465e1_2,lblpregunta465e1_3,lblpregunta465e1_4,lblpregunta465e1_5,lblpregunta465e1_6,
	lblpregunta465e1_7,lblpregunta465e1_8,lblpregunta465e1_9,lblpregunta465e1_x0,lblpregunta465e1_xa,lblpregunta465e1_xb,lblpregunta465e1_xc,
	lblpregunta465e1_xd; 
	
	LinearLayout q0,q1,q2,q3,q4;  
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465EA","QI465EB","QI465EC","QI465ED","QI465ED_O","QI465EB_CC","QI465EB_CC_O","QI465E1CONS","QI465E1_A1", "QI465E1_A2", "QI465E1_A3", "QI465E1_A4", "QI465E1_A5", "QI465E1_A6", "QI465E1_A7", "QI465E1_A8", "QI465E1_A9", "QI465E1_B1", "QI465E1_B2", "QI465E1_B3", "QI465E1_B4", "QI465E1_B5", "QI465E1_B6", "QI465E1_B7", "QI465E1_B8", "QI465E1_B9", "QI465E1_C1", "QI465E1_C2", "QI465E1_C3", "QI465E1_C4", "QI465E1_C5", "QI465E1_C6", "QI465E1_C7", "QI465E1_C8", "QI465E1_C9", "QI465E1_D1", "QI465E1_D2", "QI465E1_D3", "QI465E1_D4", "QI465E1_D5", "QI465E1_D6", "QI465E1_D7", "QI465E1_D8", "QI465E1_D9")};
	SeccionCapitulo[] seccionesCargado; 
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 grid1,gridQH465E1;
	
	public C2SECCION_04BFragment_008() {} 
	public C2SECCION_04BFragment_008 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465EA","QI465EB","QI465EC","QI465ED","QI465ED_O","QI465EB_CC","QI465EB_CC_O","QI465E1CONS","QI465E1_A1", "QI465E1_A2", "QI465E1_A3", "QI465E1_A4", "QI465E1_A5", "QI465E1_A6", "QI465E1_A7", "QI465E1_A8", "QI465E1_A9", "QI465E1_B1", "QI465E1_B2", "QI465E1_B3", "QI465E1_B4", "QI465E1_B5", "QI465E1_B6", "QI465E1_B7", "QI465E1_B8", "QI465E1_B9", "QI465E1_C1", "QI465E1_C2", "QI465E1_C3", "QI465E1_C4", "QI465E1_C5", "QI465E1_C6", "QI465E1_C7", "QI465E1_C8", "QI465E1_C9", "QI465E1_D1", "QI465E1_D2", "QI465E1_D3", "QI465E1_D4", "QI465E1_D5", "QI465E1_D6", "QI465E1_D7", "QI465E1_D8", "QI465E1_D9", "QI454", "ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};  
		return rootView; 
	} 
	
	@Override 
	protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta465e = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");	
	    lblpregunta465e_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465eb_indx);
	    lblpregunta465eb = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465eb);
	    lblpregunta465eb_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465eb_ind1).negrita();
	    lblpregunta465eb_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465eb_ind2);
	    lblpregunta465eb_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465eb_ind3).negrita();
	    	    
	    lblpregunta465e1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465e1);
	    lblpregunta465e1_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465e1_ind);
	    
	    
	    lblpregunta465e1_x0 = new LabelComponent(this.getActivity()).size(altoComponente, 240);
	    lblpregunta465e1_xa = new LabelComponent(this.getActivity()).size(altoComponente, 120).textSize(13).text(R.string.c2seccion_04bqi465e1_xa).negrita().centrar();
	    lblpregunta465e1_xb = new LabelComponent(this.getActivity()).size(altoComponente, 120).textSize(13).text(R.string.c2seccion_04bqi465e1_xb).negrita().centrar();
	    lblpregunta465e1_xc = new LabelComponent(this.getActivity()).size(altoComponente, 120).textSize(13).text(R.string.c2seccion_04bqi465e1_xc).negrita().centrar();
	    lblpregunta465e1_xd = new LabelComponent(this.getActivity()).size(altoComponente, 120).textSize(13).text(R.string.c2seccion_04bqi465e1_xd).negrita().centrar();
	    
	    
	    
	    lblpregunta465e1_1 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_1);
	    lblpregunta465e1_2 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_2);
	    lblpregunta465e1_3 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_3);
	    lblpregunta465e1_4 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_4);
	    lblpregunta465e1_5 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_5);
	    lblpregunta465e1_6 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_6);
	    lblpregunta465e1_7 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_7);
	    lblpregunta465e1_8 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_8);
	    lblpregunta465e1_9 = new LabelComponent(this.getActivity()).size(altoComponente, 230).textSize(13).text(R.string.c2seccion_04bqi465e1_9);
	    
		
		lblQI465eA=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 425).text(R.string.c2seccion_04bqi465e_a);		
		lblQI465eB=new LabelComponent(getActivity()).textSize(17).size(altoComponente+35, 425).text(R.string.c2seccion_04bqi465e_b);	
		lblQI465eC=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 425).text(R.string.c2seccion_04bqi465e_c);
		lblQI465eD=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 425).text(R.string.c2seccion_04bqi465e_d);
		
		rgQI465EA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465e_o1,R.string.c2seccion_04bqi465e_o2,R.string.c2seccion_04bqi465e_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465e_aChangeValue");
		rgQI465EB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465e_o1,R.string.c2seccion_04bqi465e_o2,R.string.c2seccion_04bqi465e_o3).size(altoComponente+35,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465e_bChangeValue");
		rgQI465EC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465e_o1,R.string.c2seccion_04bqi465e_o2,R.string.c2seccion_04bqi465e_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465e_cChangeValue");
		rgQI465ED=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465e_o1,R.string.c2seccion_04bqi465e_o2,R.string.c2seccion_04bqi465e_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465e_dChangeValue");  
		txtQI465ED_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 400); 
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI465eA);
		grid1.addComponent(rgQI465EA);
		grid1.addComponent(lblQI465eB);
		grid1.addComponent(rgQI465EB);
		grid1.addComponent(lblQI465eC);
		grid1.addComponent(rgQI465EC);
		grid1.addComponent(lblQI465eD);
		grid1.addComponent(rgQI465ED);
		grid1.addComponent(txtQI465ED_O,2);
		
		rgQI465EB_CC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465eb_1,R.string.c2seccion_04bqi465eb_2,R.string.c2seccion_04bqi465eb_3,R.string.c2seccion_04bqi465eb_4,R.string.c2seccion_04bqi465eb_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI465EB_CC_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 300); 
		rgQI465EB_CC.agregarEspecifique(4, txtQI465EB_CC_O);
		
		chbQI465E1_A1=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a1ChangeValue");
		chbQI465E1_A2=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a2ChangeValue");
		chbQI465E1_A3=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a3ChangeValue");
		chbQI465E1_A4=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a4ChangeValue");
		chbQI465E1_A5=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a5ChangeValue");
		chbQI465E1_A6=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a6ChangeValue");
		chbQI465E1_A7=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a7ChangeValue");
		chbQI465E1_A8=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a8ChangeValue");
		chbQI465E1_A9=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_a9ChangeValue");
		chbQI465E1_B1=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b1ChangeValue");
		chbQI465E1_B2=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b2ChangeValue");
		chbQI465E1_B3=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b3ChangeValue");
		chbQI465E1_B4=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b4ChangeValue");
		chbQI465E1_B5=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b5ChangeValue");
		chbQI465E1_B6=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b6ChangeValue");
		chbQI465E1_B7=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b7ChangeValue");
		chbQI465E1_B8=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b8ChangeValue");
		chbQI465E1_B9=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_b9ChangeValue");
		chbQI465E1_C1=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c1ChangeValue");
		chbQI465E1_C2=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c2ChangeValue");
		chbQI465E1_C3=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c3ChangeValue");
		chbQI465E1_C4=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c4ChangeValue");
		chbQI465E1_C5=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c5ChangeValue");
		chbQI465E1_C6=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c6ChangeValue");
		chbQI465E1_C7=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c7ChangeValue");
		chbQI465E1_C8=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c8ChangeValue");
		chbQI465E1_C9=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c9ChangeValue");
		chbQI465E1_D1=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_c1ChangeValue");
		chbQI465E1_D2=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d2ChangeValue");
		chbQI465E1_D3=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d3ChangeValue");
		chbQI465E1_D4=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d4ChangeValue");
		chbQI465E1_D5=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d5ChangeValue");
		chbQI465E1_D6=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d6ChangeValue");
		chbQI465E1_D7=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d7ChangeValue");
		chbQI465E1_D8=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d8ChangeValue");
		chbQI465E1_D9=new CheckBoxField(this.getActivity(), "1:0").size(altoComponente, 120).callback("onqrgP465e1_d9ChangeValue");
		
		gridQH465E1=new GridComponent2(this.getActivity(),App.ESTILO,5,1);
		gridQH465E1.addComponent(lblpregunta465e1_x0);
		gridQH465E1.addComponent(lblpregunta465e1_xa);
		gridQH465E1.addComponent(lblpregunta465e1_xb);
		gridQH465E1.addComponent(lblpregunta465e1_xc);
		gridQH465E1.addComponent(lblpregunta465e1_xd);
		gridQH465E1.addComponent(lblpregunta465e1_1);
		gridQH465E1.addComponent(chbQI465E1_A1);
		gridQH465E1.addComponent(chbQI465E1_B1);
		gridQH465E1.addComponent(chbQI465E1_C1);
		gridQH465E1.addComponent(chbQI465E1_D1);
		gridQH465E1.addComponent(lblpregunta465e1_2);
		gridQH465E1.addComponent(chbQI465E1_A2);
		gridQH465E1.addComponent(chbQI465E1_B2);
		gridQH465E1.addComponent(chbQI465E1_C2);
		gridQH465E1.addComponent(chbQI465E1_D2);
		gridQH465E1.addComponent(lblpregunta465e1_3);
		gridQH465E1.addComponent(chbQI465E1_A3);
		gridQH465E1.addComponent(chbQI465E1_B3);
		gridQH465E1.addComponent(chbQI465E1_C3);
		gridQH465E1.addComponent(chbQI465E1_D3);
		gridQH465E1.addComponent(lblpregunta465e1_4);
		gridQH465E1.addComponent(chbQI465E1_A4);
		gridQH465E1.addComponent(chbQI465E1_B4);
		gridQH465E1.addComponent(chbQI465E1_C4);
		gridQH465E1.addComponent(chbQI465E1_D4);
		gridQH465E1.addComponent(lblpregunta465e1_5);
		gridQH465E1.addComponent(chbQI465E1_A5);
		gridQH465E1.addComponent(chbQI465E1_B5);
		gridQH465E1.addComponent(chbQI465E1_C5);
		gridQH465E1.addComponent(chbQI465E1_D5);
		gridQH465E1.addComponent(lblpregunta465e1_6);
		gridQH465E1.addComponent(chbQI465E1_A6);
		gridQH465E1.addComponent(chbQI465E1_B6);
		gridQH465E1.addComponent(chbQI465E1_C6);
		gridQH465E1.addComponent(chbQI465E1_D6);
		gridQH465E1.addComponent(lblpregunta465e1_7);
		gridQH465E1.addComponent(chbQI465E1_A7);
		gridQH465E1.addComponent(chbQI465E1_B7);
		gridQH465E1.addComponent(chbQI465E1_C7);
		gridQH465E1.addComponent(chbQI465E1_D7);
		gridQH465E1.addComponent(lblpregunta465e1_8);
		gridQH465E1.addComponent(chbQI465E1_A8);
		gridQH465E1.addComponent(chbQI465E1_B8);
		gridQH465E1.addComponent(chbQI465E1_C8);
		gridQH465E1.addComponent(chbQI465E1_D8);
		gridQH465E1.addComponent(lblpregunta465e1_9);
		gridQH465E1.addComponent(chbQI465E1_A9);
		gridQH465E1.addComponent(chbQI465E1_B9);
		gridQH465E1.addComponent(chbQI465E1_C9);
		gridQH465E1.addComponent(chbQI465E1_D9);
		
		txtQI465ED_O.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
					ontxtQI465ED_OChangeValue();					
				}
				else {
					resetearLabels();
				}
			}
		});
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465e,lblpregunta465e_ind,grid1.component()); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465e1, lblpregunta465e1_ind,gridQH465E1.component());
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta465eb,lblpregunta465eb_ind1,lblpregunta465eb_ind2,lblpregunta465eb_ind3,rgQI465EB_CC); 
		 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b);
		
		if(qi465e1_fechref == null) {
			c2seccion_04b.qi465e1cons =  Util.getFechaActualToString();
		}
		else {
			c2seccion_04b.qi465e1cons = qi465e1_fechref;
		}
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi465ea!=null) {
				c2seccion_04b.qi465ea=c2seccion_04b.getConvertQi465e_a(c2seccion_04b.qi465ea);
			}	
			if (c2seccion_04b.qi465eb!=null) {
				c2seccion_04b.qi465eb=c2seccion_04b.getConvertQi465e_b(c2seccion_04b.qi465eb);
			}	
			if (c2seccion_04b.qi465ec!=null) {
				c2seccion_04b.qi465ec=c2seccion_04b.getConvertQi465e_c(c2seccion_04b.qi465ec);
			}	
			if (c2seccion_04b.qi465ed!=null) {
				c2seccion_04b.qi465ed=c2seccion_04b.getConvertQi465e_d(c2seccion_04b.qi465ed);
			}	
			if (c2seccion_04b.qi465eb_cc!=null) {
				c2seccion_04b.qi465eb_cc=c2seccion_04b.getConvertQi465eb(c2seccion_04b.qi465eb_cc);
			}
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(c2seccion_04b.qi465ea)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI465EA"); 
			view = rgQI465EA; 
			error = true; 
			return false; 
		}	
		if (Util.esVacio(c2seccion_04b.qi465eb)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI465EB"); 
			view = rgQI465EB; 
			error = true; 
			return false; 
		}		
		if (Util.esVacio(c2seccion_04b.qi465ec)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI465EC"); 
			view = rgQI465EC; 
			error = true; 
			return false; 
		}
		if (Util.esVacio(c2seccion_04b.qi465ed)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI465ED"); 
			view = rgQI465ED; 
			error = true; 
			return false; 
		} 
		if(!Util.esDiferente(c2seccion_04b.qi465ed,3)){ 
			if (Util.esVacio(c2seccion_04b.qi465ed_o)) { 
				mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
				view = txtQI465ED_O; 
				error = true; 
				return false; 
			} 
		} 
		
		if (!Util.esDiferente(c2seccion_04b.qi465ea,1)) { 
			if(!verificarCheckP465e1_a()) {
				mensaje = "Seleccione una opci�n en la pregunta QH465E1_A"; 
				view = chbQI465E1_A1; 
				error = true; 
				return false; 
			}	
		}
		if (!Util.esDiferente(c2seccion_04b.qi465eb,1)) {
			if(!verificarCheckP465e1_b()) {
				mensaje = "Seleccione una opci�n en la pregunta QH465E1_B"; 
				view = chbQI465E1_B1; 
				error = true; 
				return false; 
			}
		}		
		if (!Util.esDiferente(c2seccion_04b.qi465ec,1)) {
			if(!verificarCheckP465e1_c()) {
				mensaje = "Seleccione una opci�n en la pregunta QH465E1_C"; 
				view = chbQI465E1_C1; 
				error = true; 
				return false; 
			}
		}		
		if (!Util.esDiferente(c2seccion_04b.qi465ed,1)) {
			if(!verificarCheckP465e1_d()) {
				mensaje = "Seleccione una opci�n en la pregunta QH465E1_D"; 
				view = chbQI465E1_D1; 
				error = true; 
				return false; 
			}	
		}
		
		
		

		 	
		if(c2seccion_04b.qi465eb==1) {
			if (Util.esVacio(c2seccion_04b.qi465eb_cc)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465EB_CC"); 
				view = rgQI465EB_CC; 
				error = true; 
				return false; 
			} 
			
			if(!Util.esDiferente(c2seccion_04b.qi465eb_cc,96)){ 
				if (Util.esVacio(c2seccion_04b.qi465eb_cc_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465EB_CC_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		
		
		
		
		/*if (Util.esVacio(c2seccion_04b.qi465ec_cc)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI465EC_CC"); 
			view = txtQI465EC_CC; 
			error = true; 
			return false; 
		} */ 
		
		return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    }  
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi465ea!=null) {
				c2seccion_04b.qi465ea=c2seccion_04b.setConvertQi465e_a(c2seccion_04b.qi465ea);
			}	
			if (c2seccion_04b.qi465eb!=null) {
				c2seccion_04b.qi465eb=c2seccion_04b.setConvertQi465e_b(c2seccion_04b.qi465eb);
			}	
			if (c2seccion_04b.qi465ec!=null) {
				c2seccion_04b.qi465ec=c2seccion_04b.setConvertQi465e_c(c2seccion_04b.qi465ec);
			}	
			if (c2seccion_04b.qi465ed!=null) {
				c2seccion_04b.qi465ed=c2seccion_04b.setConvertQi465e_d(c2seccion_04b.qi465ed);
			}	
			if (c2seccion_04b.qi465eb_cc!=null) {
				c2seccion_04b.qi465eb_cc=c2seccion_04b.setConvertQi465eb(c2seccion_04b.qi465eb_cc);
			}
		}
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		qi465e1_fechref = c2seccion_04b.qi465e1cons;
		inicio(); 
    } 
    
    private void inicio() {    	
    	renombrarEtiquetas();
    	renombrarFechaQi465e1();
    	onqrgQI465e_aChangeValue();
    	onqrgQI465e_bChangeValue();
    	onqrgQI465e_cChangeValue();
    	onqrgQI465e_dChangeValue();
    	
    	validarP465e1_a();
    	validarP465e1_b();
    	validarP465e1_c();
    	validarP465e1_d();
    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
	public void ontxtQI465ED_OChangeValue(){
		if(txtQI465ED_O.getText().toString().length()>0){
			resetearLabels();
			lblpregunta465e1_xd.setText(lblpregunta465e1_xd.getText().toString().replace("otra",txtQI465ED_O.getText().toString().toLowerCase()));
		}
	}
	public void resetearLabels() {
		lblpregunta465e1_xd.setText(getResources().getString(R.string.c2seccion_04bqi465e1_xd));
	}
	
	
    public boolean verificarCheckP465e1_a() {
  		if (chbQI465E1_A1.isChecked()||chbQI465E1_A2.isChecked()||chbQI465E1_A3.isChecked()||chbQI465E1_A4.isChecked()||chbQI465E1_A5.isChecked()||chbQI465E1_A6.isChecked()||chbQI465E1_A7.isChecked()||chbQI465E1_A8.isChecked()|| chbQI465E1_A9.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public boolean verificarCheckP465e1_b() {
  		if (chbQI465E1_B1.isChecked()||chbQI465E1_B2.isChecked()||chbQI465E1_B3.isChecked()||chbQI465E1_B4.isChecked()||chbQI465E1_B5.isChecked()||chbQI465E1_B6.isChecked()||chbQI465E1_B7.isChecked()||chbQI465E1_B8.isChecked()|| chbQI465E1_B9.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public boolean verificarCheckP465e1_c() {
  		if (chbQI465E1_C1.isChecked()||chbQI465E1_C2.isChecked()||chbQI465E1_C3.isChecked()||chbQI465E1_C4.isChecked()||chbQI465E1_C5.isChecked()||chbQI465E1_C6.isChecked()||chbQI465E1_C7.isChecked()||chbQI465E1_C8.isChecked()|| chbQI465E1_C9.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public boolean verificarCheckP465e1_d() {
  		if (chbQI465E1_D1.isChecked()||chbQI465E1_D2.isChecked()||chbQI465E1_D3.isChecked()||chbQI465E1_D4.isChecked()||chbQI465E1_D5.isChecked()||chbQI465E1_D6.isChecked()||chbQI465E1_D7.isChecked()||chbQI465E1_D8.isChecked()|| chbQI465E1_D9.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    
    
    public void validarP465e1_a() {
		if(chbQI465E1_A9.isChecked()){
			onqrgP465e1_a9ChangeValue();
		}
		else{
			if(chbQI465E1_A8.isChecked()){
				onqrgP465e1_a8ChangeValue();
			}
			else{
				onqrgP465e1_a1ChangeValue();
				onqrgP465e1_a2ChangeValue();
				onqrgP465e1_a3ChangeValue();
				onqrgP465e1_a4ChangeValue();
				onqrgP465e1_a5ChangeValue();
				onqrgP465e1_a6ChangeValue();
				onqrgP465e1_a7ChangeValue();
			}
		}
	}    
    public void onqrgP465e1_a1ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a2ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a3ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a4ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a5ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a6ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a7ChangeValue(){if (verificarCheckP465e1_a()){Util.cleanAndLockView(getActivity(),chbQI465E1_A8,chbQI465E1_A9);}else{Util.lockView(getActivity(),false,chbQI465E1_A8,chbQI465E1_A9);}}
    public void onqrgP465e1_a8ChangeValue() {  	
    	if (verificarCheckP465e1_a()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A9);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A9);  				
    	}	
    }
    public void onqrgP465e1_a9ChangeValue() {  	
    	if (verificarCheckP465e1_a()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A8);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A8);  				
    	}	
    }
    
    
    
    public void validarP465e1_b() {
		if(chbQI465E1_B9.isChecked()){
			onqrgP465e1_b9ChangeValue();
		}
		else{
			if(chbQI465E1_B8.isChecked()){
				onqrgP465e1_b8ChangeValue();
			}
			else{
				onqrgP465e1_b1ChangeValue();
				onqrgP465e1_b2ChangeValue();
				onqrgP465e1_b3ChangeValue();
				onqrgP465e1_b4ChangeValue();
				onqrgP465e1_b5ChangeValue();
				onqrgP465e1_b6ChangeValue();
				onqrgP465e1_b7ChangeValue();
			}
		}
	}    
    public void onqrgP465e1_b1ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b2ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b3ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b4ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b5ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b6ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b7ChangeValue(){if (verificarCheckP465e1_b()){Util.cleanAndLockView(getActivity(),chbQI465E1_B8,chbQI465E1_B9);}else{Util.lockView(getActivity(),false,chbQI465E1_B8,chbQI465E1_B9);}}
    public void onqrgP465e1_b8ChangeValue() {  	
    	if (verificarCheckP465e1_b()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B9);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B9);  				
    	}	
    }
    public void onqrgP465e1_b9ChangeValue() {  	
    	if (verificarCheckP465e1_b()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B8);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B8);  				
    	}	
    }
    
        
    public void validarP465e1_c() {
		if(chbQI465E1_C9.isChecked()){
			onqrgP465e1_c9ChangeValue();
		}
		else{
			if(chbQI465E1_C8.isChecked()){
				onqrgP465e1_c8ChangeValue();
			}
			else{
				onqrgP465e1_c1ChangeValue();
				onqrgP465e1_c2ChangeValue();
				onqrgP465e1_c3ChangeValue();
				onqrgP465e1_c4ChangeValue();
				onqrgP465e1_c5ChangeValue();
				onqrgP465e1_c6ChangeValue();
				onqrgP465e1_c7ChangeValue();
			}
		}
	}    
    public void onqrgP465e1_c1ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c2ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c3ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c4ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c5ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c6ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c7ChangeValue(){if (verificarCheckP465e1_c()){Util.cleanAndLockView(getActivity(),chbQI465E1_C8,chbQI465E1_C9);}else{Util.lockView(getActivity(),false,chbQI465E1_C8,chbQI465E1_C9);}}
    public void onqrgP465e1_c8ChangeValue() {  	
    	if (verificarCheckP465e1_c()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C9);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C9);  				
    	}	
    }
    public void onqrgP465e1_c9ChangeValue() {  	
    	if (verificarCheckP465e1_c()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C8);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C8);  				
    	}	
    }  
    
    
    
    public void validarP465e1_d() {
		if(chbQI465E1_D9.isChecked()){
			onqrgP465e1_d9ChangeValue();
		}
		else{
			if(chbQI465E1_D8.isChecked()){
				onqrgP465e1_d8ChangeValue();
			}
			else{
				onqrgP465e1_d1ChangeValue();
				onqrgP465e1_d2ChangeValue();
				onqrgP465e1_d3ChangeValue();
				onqrgP465e1_d4ChangeValue();
				onqrgP465e1_d5ChangeValue();
				onqrgP465e1_d6ChangeValue();
				onqrgP465e1_d7ChangeValue();
			}
		}
	}    
    public void onqrgP465e1_d1ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d2ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d3ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d4ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d5ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d6ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d7ChangeValue(){if (verificarCheckP465e1_d()){Util.cleanAndLockView(getActivity(),chbQI465E1_D8,chbQI465E1_D9);}else{Util.lockView(getActivity(),false,chbQI465E1_D8,chbQI465E1_D9);}}
    public void onqrgP465e1_d8ChangeValue() {  	
    	if (verificarCheckP465e1_d()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D9);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D9);  				
    	}	
    }
    public void onqrgP465e1_d9ChangeValue() {  	
    	if (verificarCheckP465e1_d()) {
    		Util.cleanAndLockView(getActivity(),chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D8);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D8);  				
    	}	
    }
    
  
	
	public void onqrgQI465eChangeValue() {
		if (MyUtil.incluyeRango(1,1,rgQI465EA.getTagSelected("").toString()) || MyUtil.incluyeRango(1,1,rgQI465EB.getTagSelected("").toString()) || MyUtil.incluyeRango(1,1,rgQI465EC.getTagSelected("").toString()) || MyUtil.incluyeRango(1,1,rgQI465ED.getTagSelected("").toString()) ) {
			q2.setVisibility(View.VISIBLE);
		}
		else{
			q2.setVisibility(View.GONE);
		}
	}
	
	public void onqrgQI465e_aChangeValue() {  
		if (rgQI465EA.getValue()!=null) {
			onqrgQI465eChangeValue();
			if ( rgQI465EA.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A8,chbQI465E1_A9);  
				lblpregunta465e1_xa.setVisibility(View.VISIBLE);
				chbQI465E1_A1.setVisibility(View.VISIBLE);
				chbQI465E1_A2.setVisibility(View.VISIBLE);
				chbQI465E1_A3.setVisibility(View.VISIBLE);
				chbQI465E1_A4.setVisibility(View.VISIBLE);
				chbQI465E1_A5.setVisibility(View.VISIBLE);
				chbQI465E1_A6.setVisibility(View.VISIBLE);
				chbQI465E1_A7.setVisibility(View.VISIBLE);
				chbQI465E1_A8.setVisibility(View.VISIBLE);
				chbQI465E1_A9.setVisibility(View.VISIBLE);
			} 
			else {	
				Util.cleanAndLockView(getActivity(),chbQI465E1_A1,chbQI465E1_A2,chbQI465E1_A3,chbQI465E1_A4,chbQI465E1_A5,chbQI465E1_A6,chbQI465E1_A7,chbQI465E1_A8,chbQI465E1_A9);    				
				lblpregunta465e1_xa.setVisibility(View.GONE);
				chbQI465E1_A1.setVisibility(View.GONE);
				chbQI465E1_A2.setVisibility(View.GONE);
				chbQI465E1_A3.setVisibility(View.GONE);
				chbQI465E1_A4.setVisibility(View.GONE);
				chbQI465E1_A5.setVisibility(View.GONE);
				chbQI465E1_A6.setVisibility(View.GONE);
				chbQI465E1_A7.setVisibility(View.GONE);
				chbQI465E1_A8.setVisibility(View.GONE);
				chbQI465E1_A9.setVisibility(View.GONE);
			}
			
			rgQI465EB.requestFocus();
		}
	}
	
	public void onqrgQI465e_bChangeValue() {  
		if (rgQI465EB.getValue()!=null) {
			onqrgQI465eChangeValue();
			if ( rgQI465EB.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,rgQI465EB_CC, chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B8,chbQI465E1_B9);  
				q3.setVisibility(View.VISIBLE);
				lblpregunta465e1_xb.setVisibility(View.VISIBLE);
				chbQI465E1_B1.setVisibility(View.VISIBLE);
				chbQI465E1_B2.setVisibility(View.VISIBLE);
				chbQI465E1_B3.setVisibility(View.VISIBLE);
				chbQI465E1_B4.setVisibility(View.VISIBLE);
				chbQI465E1_B5.setVisibility(View.VISIBLE);
				chbQI465E1_B6.setVisibility(View.VISIBLE);
				chbQI465E1_B7.setVisibility(View.VISIBLE);
				chbQI465E1_B8.setVisibility(View.VISIBLE);
				chbQI465E1_B9.setVisibility(View.VISIBLE);
			} 
			else {	
				Util.cleanAndLockView(getActivity(),rgQI465EB_CC, chbQI465E1_B1,chbQI465E1_B2,chbQI465E1_B3,chbQI465E1_B4,chbQI465E1_B5,chbQI465E1_B6,chbQI465E1_B7,chbQI465E1_B8,chbQI465E1_B9);    				
				q3.setVisibility(View.GONE);
				lblpregunta465e1_xb.setVisibility(View.GONE);
				chbQI465E1_B1.setVisibility(View.GONE);
				chbQI465E1_B2.setVisibility(View.GONE);
				chbQI465E1_B3.setVisibility(View.GONE);
				chbQI465E1_B4.setVisibility(View.GONE);
				chbQI465E1_B5.setVisibility(View.GONE);
				chbQI465E1_B6.setVisibility(View.GONE);
				chbQI465E1_B7.setVisibility(View.GONE);
				chbQI465E1_B8.setVisibility(View.GONE);
				chbQI465E1_B9.setVisibility(View.GONE);
			}	
			rgQI465EC.requestFocus();
		}
	}
	
	public void onqrgQI465e_cChangeValue() {  
		if (rgQI465EC.getValue()!=null) {
			onqrgQI465eChangeValue();
			if ( rgQI465EC.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C8,chbQI465E1_C9);  
				lblpregunta465e1_xc.setVisibility(View.VISIBLE);
				chbQI465E1_C1.setVisibility(View.VISIBLE);
				chbQI465E1_C2.setVisibility(View.VISIBLE);
				chbQI465E1_C3.setVisibility(View.VISIBLE);
				chbQI465E1_C4.setVisibility(View.VISIBLE);
				chbQI465E1_C5.setVisibility(View.VISIBLE);
				chbQI465E1_C6.setVisibility(View.VISIBLE);
				chbQI465E1_C7.setVisibility(View.VISIBLE);
				chbQI465E1_C8.setVisibility(View.VISIBLE);
				chbQI465E1_C9.setVisibility(View.VISIBLE);
			} 
			else {	
				Util.cleanAndLockView(getActivity(),chbQI465E1_C1,chbQI465E1_C2,chbQI465E1_C3,chbQI465E1_C4,chbQI465E1_C5,chbQI465E1_C6,chbQI465E1_C7,chbQI465E1_C8,chbQI465E1_C9);    				
				lblpregunta465e1_xc.setVisibility(View.GONE);
				chbQI465E1_C1.setVisibility(View.GONE);
				chbQI465E1_C2.setVisibility(View.GONE);
				chbQI465E1_C3.setVisibility(View.GONE);
				chbQI465E1_C4.setVisibility(View.GONE);
				chbQI465E1_C5.setVisibility(View.GONE);
				chbQI465E1_C6.setVisibility(View.GONE);
				chbQI465E1_C7.setVisibility(View.GONE);
				chbQI465E1_C8.setVisibility(View.GONE);
				chbQI465E1_C9.setVisibility(View.GONE);
			}
			rgQI465ED.requestFocus();
		}
	}
	
	public void onqrgQI465e_dChangeValue() {  	
		if(rgQI465ED.getValue()!=null) {
			onqrgQI465eChangeValue();
			if (rgQI465ED.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI465ED_O,chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D8,chbQI465E1_D9);  
				lblpregunta465e1_xd.setVisibility(View.VISIBLE);
				chbQI465E1_D1.setVisibility(View.VISIBLE);
				chbQI465E1_D2.setVisibility(View.VISIBLE);
				chbQI465E1_D3.setVisibility(View.VISIBLE);
				chbQI465E1_D4.setVisibility(View.VISIBLE);
				chbQI465E1_D5.setVisibility(View.VISIBLE);
				chbQI465E1_D6.setVisibility(View.VISIBLE);
				chbQI465E1_D7.setVisibility(View.VISIBLE);
				chbQI465E1_D8.setVisibility(View.VISIBLE);
				chbQI465E1_D9.setVisibility(View.VISIBLE);
	    		txtQI465ED_O.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI465ED_O,chbQI465E1_D1,chbQI465E1_D2,chbQI465E1_D3,chbQI465E1_D4,chbQI465E1_D5,chbQI465E1_D6,chbQI465E1_D7,chbQI465E1_D8,chbQI465E1_D9);
	    		lblpregunta465e1_xd.setVisibility(View.GONE);
				chbQI465E1_D1.setVisibility(View.GONE);
				chbQI465E1_D2.setVisibility(View.GONE);
				chbQI465E1_D3.setVisibility(View.GONE);
				chbQI465E1_D4.setVisibility(View.GONE);
				chbQI465E1_D5.setVisibility(View.GONE);
				chbQI465E1_D6.setVisibility(View.GONE);
				chbQI465E1_D7.setVisibility(View.GONE);
				chbQI465E1_D8.setVisibility(View.GONE);
				chbQI465E1_D9.setVisibility(View.GONE);
	    		
//	    		if (rgQI465EB.getValue()!=null && rgQI465EB.getValue().toString().equals("1")) {	    			
//	    			rgQI465EB_CC.requestFocus();
//	    		}
//	    		else{
//	    			chbQI1.requestFocus();
//	    		}
	    	}	
		}
    }
	
	

	
	public void renombrarEtiquetas() {
		lblpregunta465e.setText("(NOMBRE)");
		lblpregunta465e.setText(lblpregunta465e.getText().toString().replace("(NOMBRE)", nombre_persona));
     	Spanned texto465e =Html.fromHtml("465E. <b>En los �ltimos siete dias</b>, �"+lblpregunta465e.getText() +" tom�:");
     	lblpregunta465e.setText(texto465e);
    	lblpregunta465eb.setText(lblpregunta465eb.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta465eb_ind2.setText(lblpregunta465eb_ind2.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta465e1.setText(lblpregunta465e1.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void renombrarFechaQi465e1(){
		Calendar Fecha_Actual = new GregorianCalendar();   		
   		new Date();
   		if(qi465e1_fechref!=null){			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(qi465e1_fechref);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			Fecha_Actual=cal;
		}
		
   		//Log.e("Fecha_Actual:", ""+Fecha_Actual.get(Calendar.DAY_OF_MONTH)+""+(Fecha_Actual.get(Calendar.MONTH)+1)+"-"+Fecha_Actual.get(Calendar.YEAR));
   		
   		Calendar primera_fecha = MyUtil.PrimeraFecha(Fecha_Actual,7);
		
		int dias=primera_fecha.get(Calendar.DAY_OF_WEEK);		
   		lblpregunta465e1.setText(lblpregunta465e1.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemana(dias)));
   		
   		Calendar primera_fecha2 = MyUtil.PrimeraFecha(Fecha_Actual,8);	
		int dias2=primera_fecha2.get(Calendar.DAY_OF_WEEK);
		
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_7.setText(lblpregunta465e1_7.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_6.setText(lblpregunta465e1_6.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_5.setText(lblpregunta465e1_5.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_4.setText(lblpregunta465e1_4.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_3.setText(lblpregunta465e1_3.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_2.setText(lblpregunta465e1_2.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
   		dias2=((dias2+1)>7?1:(dias2+1));
   		lblpregunta465e1_1.setText(lblpregunta465e1_1.getText().toString().replace("(DIAA)",MyUtil.DiaDelaSemanaMay(dias2)));
	}
	

	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI465EA.readOnly();
			rgQI465EB.readOnly();
			rgQI465EB_CC.readOnly();
			rgQI465EC.readOnly();
			rgQI465ED.readOnly();
			
			chbQI465E1_A1.readOnly();
			chbQI465E1_A2.readOnly();
			chbQI465E1_A3.readOnly();
			chbQI465E1_A4.readOnly();
			chbQI465E1_A5.readOnly();
			chbQI465E1_A6.readOnly();
			chbQI465E1_A7.readOnly();
			chbQI465E1_A8.readOnly();
			chbQI465E1_A9.readOnly();
			
			chbQI465E1_B1.readOnly();
			chbQI465E1_B2.readOnly();
			chbQI465E1_B3.readOnly();
			chbQI465E1_B4.readOnly();
			chbQI465E1_B5.readOnly();
			chbQI465E1_B6.readOnly();
			chbQI465E1_B7.readOnly();
			chbQI465E1_B8.readOnly();
			chbQI465E1_B9.readOnly();
						
			chbQI465E1_C1.readOnly();
			chbQI465E1_C2.readOnly();
			chbQI465E1_C3.readOnly();
			chbQI465E1_C4.readOnly();
			chbQI465E1_C5.readOnly();
			chbQI465E1_C6.readOnly();
			chbQI465E1_C7.readOnly();
			chbQI465E1_C8.readOnly();
			chbQI465E1_C9.readOnly();
			
			chbQI465E1_D1.readOnly();
			chbQI465E1_D2.readOnly();
			chbQI465E1_D3.readOnly();
			chbQI465E1_D4.readOnly();
			chbQI465E1_D5.readOnly();
			chbQI465E1_D6.readOnly();
			chbQI465E1_D7.readOnly();
			chbQI465E1_D8.readOnly();
			chbQI465E1_D9.readOnly();
						
			txtQI465EB_CC_O.readOnly();
			txtQI465ED_O.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi465ea!=null) {
				c2seccion_04b.qi465ea=c2seccion_04b.getConvertQi465e_a(c2seccion_04b.qi465ea);
			}	
			
			if (c2seccion_04b.qi465eb!=null) {
				c2seccion_04b.qi465eb=c2seccion_04b.getConvertQi465e_b(c2seccion_04b.qi465eb);
			}	
			
			if (c2seccion_04b.qi465ec!=null) {
				c2seccion_04b.qi465ec=c2seccion_04b.getConvertQi465e_c(c2seccion_04b.qi465ec);
			}	
			
			if (c2seccion_04b.qi465ed!=null) {
				c2seccion_04b.qi465ed=c2seccion_04b.getConvertQi465e_d(c2seccion_04b.qi465ed);
			}	
			
			if (c2seccion_04b.qi465eb_cc!=null) {
				c2seccion_04b.qi465eb_cc=c2seccion_04b.getConvertQi465eb(c2seccion_04b.qi465eb_cc);
			}
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				getCuestionarioService().borrar04b(App.getInstance().getNacimientoVivos().id,App.getInstance().getNacimientoVivos().hogar_id,
				App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, "465ec");
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
