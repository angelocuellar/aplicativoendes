package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_6_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_6 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478J1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478J2; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478J3; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478J4_A;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478J4_B;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478J5; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478J6; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478J7; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478J8; 


	public DISCAPACIDAD detalle;
	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478j1, lblpregunta478j1_ind,lblpregunta478j2,lblpregunta478j2_ind, lblpregunta478j3,lblpregunta478j3_ind,lblpregunta478j4, 
	lblpregunta478j4_a,lblpregunta478j4_b, lblpregunta478j5, lblpregunta478j6, lblpregunta478j7, lblpregunta478j8;
	
	public GridComponent2 gridpregunta478J4,gridDiscapacidad;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_6() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478J1","QI478J2","QI478J3","QI478J4_A","QI478J4_B","QI478J5","QI478J6","QI478J7","QI478J8","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478J1","QI478J2","QI478J3","QI478J4_A","QI478J4_B","QI478J5","QI478J6","QI478J7","QI478J8","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_6 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo6de55_71j).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478j1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j7= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j8= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		lblpregunta478j1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j2_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478j3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		
		rgQI478J1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("OnChangeValueQI478J"); 
		rgQI478J2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478J3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478J5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478J6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478j6_1,R.string.c2seccion_04b_2qi478j6_2,R.string.c2seccion_04b_2qi478j6_3,R.string.c2seccion_04b_2qi478j6_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478J7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478J8=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478j8_1,R.string.c2seccion_04b_2qi478j8_2,R.string.c2seccion_04b_2qi478j8_3,R.string.c2seccion_04b_2qi478j8_4,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		

		lblpregunta478j4_a= new LabelComponent(getActivity()).size(altoComponente+210, 600).textSize(18).text("");
		lblpregunta478j4_b= new LabelComponent(getActivity()).size(altoComponente+240, 600).textSize(18).text("");
		rgQI478J4_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+210,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478J4_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+240,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
			
		
		gridpregunta478J4 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridpregunta478J4.addComponent(lblpregunta478j4_a);
		gridpregunta478J4.addComponent(rgQI478J4_A);
		gridpregunta478J4.addComponent(lblpregunta478j4_b);
		gridpregunta478J4.addComponent(rgQI478J4_B);

	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,lblpregunta478j1, lblpregunta478j1_ind,rgQI478J1); 
		q4  = createQuestionSection(0,lblpregunta478j2,lblpregunta478j2_ind,rgQI478J2);
		q5  = createQuestionSection(0,lblpregunta478j3,lblpregunta478j3_ind,rgQI478J3);
		q6  = createQuestionSection(0,lblpregunta478j4,gridpregunta478J4.component());
		q7  = createQuestionSection(0,lblpregunta478j5,rgQI478J5);
		q8  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478j6,rgQI478J6);
		q9  = createQuestionSection(0,lblpregunta478j7,rgQI478J7);
		q10 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478j8,rgQI478J8);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8); 
		form.addView(q9);
		form.addView(q10);             

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478j1  == null) {
				error = true;
				view = rgQI478J1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J1");
				return false;
			}
			if (ninio.qi478j2  == null) {
				error = true;
				view = rgQI478J2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J2");
				return false;
			}
			if (ninio.qi478j3  == null) {
				error = true;
				view = rgQI478J3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J3");
				return false;
			}
			if (ninio.qi478j4_a  == null) {
				error = true;
				view = rgQI478J4_A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J4_A");
				return false;
			}
			if (ninio.qi478j4_b  == null) {
				error = true;
				view = rgQI478J4_B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J4_B");
				return false;
			}
			if (ninio.qi478j5  == null) {
				error = true;
				view = rgQI478J5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J5");
				return false;
			}
			if (ninio.qi478j6  == null) {
				error = true;
				view = rgQI478J6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J6");
				return false;
			}
			if (ninio.qi478j7  == null) {
				error = true;
				view = rgQI478J7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J7");
				return false;
			}
			if (ninio.qi478j8  == null) {
				error = true;
				view = rgQI478J8;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478J8");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	
	public void cambiarVariablesParalaBasedeDatos(){
		ninio.qi478j1=ninio.qi478j1!=null?ninio.setConvertconSieteVariables(ninio.qi478j1):null;
//		ninio.qi478j1=ninio.qi478j1!=null?ninio.setConvertconTresVariables(ninio.qi478j1):null; correcto
		
		ninio.qi478j2=ninio.qi478j2!=null?ninio.setConvertconTresVariables(ninio.qi478j2):null;
		ninio.qi478j3=ninio.qi478j3!=null?ninio.setConvertconTresVariables(ninio.qi478j3):null;
		ninio.qi478j4_a =ninio.qi478j4_a !=null?ninio.setConvertconTresVariables(ninio.qi478j4_a):null;
		ninio.qi478j4_b =ninio.qi478j4_b !=null?ninio.setConvertconTresVariables(ninio.qi478j4_b):null;
		ninio.qi478j5=ninio.qi478j5!=null?ninio.setConvertconTresVariables(ninio.qi478j5):null;
		
		ninio.qi478j6=ninio.qi478j6!=null?ninio.setConvertconTresVariables(ninio.qi478j6):null;
//		ninio.qi478j6=ninio.qi478j6!=null?ninio.setConvertconCuatroVariables(ninio.qi478j6):null; correcto
		
		ninio.qi478j7=ninio.qi478j7!=null?ninio.setConvertconCuatroVariables(ninio.qi478j7):null; 
//		ninio.qi478j7=ninio.qi478j7!=null?ninio.setConvertconTresVariables(ninio.qi478j7):null;  correcto
		
		ninio.qi478j8=ninio.qi478j8!=null?ninio.setConvertconCincoVariables(ninio.qi478j8):null;
	}
	
	private void cambiarVariablesParaMostraraUsuario(){
		ninio.qi478j1 =ninio.qi478j1 !=null?ninio.getConvertconTresVariables(ninio.qi478j1):null;
		ninio.qi478j2 =ninio.qi478j2 !=null?ninio.getConvertconTresVariables(ninio.qi478j2):null;
		ninio.qi478j3 =ninio.qi478j3 !=null?ninio.getConvertconTresVariables(ninio.qi478j3):null;
		ninio.qi478j4_a =ninio.qi478j4_a !=null?ninio.getConvertconTresVariables(ninio.qi478j4_a):null;
		ninio.qi478j4_b =ninio.qi478j4_b !=null?ninio.getConvertconTresVariables(ninio.qi478j4_b):null;		
		ninio.qi478j5 =ninio.qi478j5 !=null?ninio.getConvertconTresVariables(ninio.qi478j5):null;
		
		ninio.qi478j6 =ninio.qi478j6 !=null?ninio.getConvertconTresVariables(ninio.qi478j6):null;
//		ninio.qi478j6 =ninio.qi478j6 !=null?ninio.getConvertconCuatroVariables(ninio.qi478j6):null; correcto
		
		ninio.qi478j7 =ninio.qi478j7 !=null?ninio.getConvertconCuatroVariables(ninio.qi478j7):null;
//		ninio.qi478j7 =ninio.qi478j7 !=null?ninio.getConvertconTresVariables(ninio.qi478j7):null; correcto
		
		ninio.qi478j8=ninio.qi478j8!=null?ninio.getConvertconCincoVariables(ninio.qi478j8):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
		OnChangeValueQI478J();
	}
	
	public void evaluarPregunta() {		
		if((App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=55 && App.getInstance().getSeccion04DIT_02().qi478 <=71)) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478J1,rgQI478J2,rgQI478J3,rgQI478J4_A,rgQI478J4_B,rgQI478J5,rgQI478J6,rgQI478J7,rgQI478J8);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
		else {
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478J1,rgQI478J2,rgQI478J3,rgQI478J4_A,rgQI478J4_B,rgQI478J5,rgQI478J6,rgQI478J7,rgQI478J8);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
		}
	}
	
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478J1,rgQI478J2,rgQI478J3,rgQI478J4_A,rgQI478J4_B,rgQI478J5,rgQI478J6,rgQI478J7,rgQI478J8);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478J1,rgQI478J2,rgQI478J3,rgQI478J4_A,rgQI478J4_B,rgQI478J5,rgQI478J6,rgQI478J7,rgQI478J8);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
		}
	}
	public void abrirDetalle(DISCAPACIDAD modelo) {
		FragmentManager fm = C2SECCION_04BFragment_022DIT_6.this.getFragmentManager();
		C2SECCION_04BFragment_018DIT_6_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_6_Dialog.newInstance(this, modelo);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	public void OnChangeValueQI478J(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(personadis!=null && !Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registr� como Discapacitado! ");
		}
	}
	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478j1.setText("(NOMBRE)");		
		lblpregunta478j1.setText(lblpregunta478j1.getText().toString().replace(replace, nombre));	
		lblpregunta478j1.setText(Html.fromHtml("478J1. "+lblpregunta478j1.getText()+" �juega de �mentirita�, es decir, juega con <b>cosas que no tiene en ese momento</b>?"));
		lblpregunta478j1_ind.setText(Html.fromHtml("SIEMPRE LEA:<br>Por ejemplo, juega a tomar desayuno sin tener las tacitas o hace como si estuviese jugando con un carrito pero en realidad no tiene nada en la mano."));
		
		lblpregunta478j2.setText("(NOMBRE)");
		lblpregunta478j2.setText(lblpregunta478j2.getText().toString().replace(replace, nombre));
		lblpregunta478j2.setText(Html.fromHtml("478J2. Cuando "+lblpregunta478j2.getText()+" <b>juega sola(o)</b> �juega <b>a ser</b> otra persona?"));
		lblpregunta478j2_ind.setText(Html.fromHtml("SIEMPRE LEA: <br>Por ejemplo, juega a ser una(un) profesora(profesor), una(un) doctora(doctor) o enfermera(o)<br><br>REFIERE A SI LA/EL NI�A(O) ASUME EL ROL DE OTRA PERSONA <b>COMO PARTE DE UN JUEGO M�S COMPLEJO</b>."));
		
		lblpregunta478j3.setText("(NOMBRE)");
		lblpregunta478j3.setText(lblpregunta478j3.getText().toString().replace(replace, nombre));
		lblpregunta478j3.setText(Html.fromHtml("478J3. Cuando "+lblpregunta478j3.getText()+" <b>juega con otras(os) ni�as(os)</b> �es un personaje como la mam�, el monstruo o un animalito?"));
		lblpregunta478j3_ind.setText(Html.fromHtml("REFIERE A SI LA (EL) NI�A(O) ASUME EL ROL DE UN PERSONAJE <b>COMO PARTE DE UN JUEGO GRUPAL M�S COMPLEJO</b>"));
		
		lblpregunta478j4.setText("(NOMBRE)");
		lblpregunta478j4.setText(lblpregunta478j4.getText().toString().replace(replace, nombre));
		lblpregunta478j4.setText(Html.fromHtml("478J4. <b>En casa</b> "+lblpregunta478j4.getText()+" tiene:"));
		lblpregunta478j4_a.setText(Html.fromHtml("A. �Materiales <b>especialmente hechos para jugar</b> como una pelota o una mu�eca?<br><br>DE SER NECESARIO, LEA:<br>Considere si en casa tiene materiales que solo pueden ser usados de una forma espec�fica para jugar, como cubos para encajar, pelotas, rompecabezas, mu�ecos, etc."));
		lblpregunta478j4_b.setText(Html.fromHtml("B. �Y tiene <b>otros materiales</b> con los que puede jugar como bloques, palitos, botellas,  l�pices o alg�n tipo de papel?<br><br>SIEMPRE LEA:<br>Considere si en casa tiene materiales que pueden ser utilizados de diferentes formas al jugar como l�pices, papeles, bloques, plastilinas, objetos de la casa (ollas, botellas de pl�stico), objetos de origen natural (conchas, palitos, plantas)."));
				
		lblpregunta478j5.setText("(NOMBRE)");
		lblpregunta478j5.setText(lblpregunta478j5.getText().toString().replace(replace, nombre));
		lblpregunta478j5.setText(Html.fromHtml("478J5. "+lblpregunta478j5.getText()+" �llora, grita o hace pataletas <b>la mayor parte del tiempo</b>? "));
					
		lblpregunta478j6.setText("(NOMBRE)");
		lblpregunta478j6.setText(lblpregunta478j6.getText().toString().replace(replace, nombre));
		lblpregunta478j6.setText(Html.fromHtml("478J6. Cuando "+lblpregunta478j6.getText()+" <b>quiere algo y usted le dice que espere, generalmente</b> �espera �tranquila(o)�?"));
		
		lblpregunta478j7.setText("(NOMBRE)");
		lblpregunta478j7.setText(lblpregunta478j7.getText().toString().replace(replace, nombre));
		lblpregunta478j7.setText(Html.fromHtml("478J7. Cuando "+lblpregunta478j7.getText()+" <b>quiere algo y usted le dice que NO, generalmente</b> �se hace da�o, agrede a los dem�s o da�a las cosas?"));
		
		lblpregunta478j8.setText("(NOMBRE)");
		lblpregunta478j8.setText(lblpregunta478j8.getText().toString().replace(replace, nombre));
		lblpregunta478j8.setText(Html.fromHtml("478J8. En los �ltimos 15 dias, �Cu�ntas veces  usted le ha dado un <b>palmazo, le ha jalado de los cabellos o la oreja o le ha golpeado con un objeto en cualquier parte de su cuerpo</b> a "+lblpregunta478j8.getText()+", de 1 a 3 veces, de 4 a 6 veces o m�s de 6 veces?  "));
		
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478J1.readOnly();
			rgQI478J2.readOnly();
			rgQI478J3.readOnly();
			rgQI478J4_A.readOnly();
			rgQI478J4_B.readOnly();
			rgQI478J5.readOnly();
			rgQI478J6.readOnly();
			rgQI478J7.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
