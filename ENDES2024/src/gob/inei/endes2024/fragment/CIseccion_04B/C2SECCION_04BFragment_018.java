package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_018 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public SpinnerField spnQI474C; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI474D; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI475; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQI475_O; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI475B; 
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta474c,lblpregunta474c_ind,lblpregunta474d,lblpregunta474d_ind,lblpregunta475,lblpregunta475A,lblpregunta475b,lbltratamientoveces; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 

	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI474C","QI474D","QI475","QI475_O","QI475B")}; 
	SeccionCapitulo[] seccionesCargado; 
	public boolean filtro474e;
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdTratamiento474d;
	public C2SECCION_04BFragment_018() {} 
	public C2SECCION_04BFragment_018 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI474A_A","QI474A_B","QI474A_C","QI474A_D","QI474A_E","QI474A_F","QI474A_G","QI474A_H","QI474A_I","QI474A_J","QI474A_K","QI474A_L","QI474A_M","QI474A_N","QI474A_O","QI474A_P","QI474A_X","QI474C","QI474D","QI475","QI475_O","QI475B","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta474c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi474c);
	    lblpregunta474c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi474c_ind).negrita();
	    lblpregunta474d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi474d);
	    lblpregunta474d_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi474d_ind).negrita();
	    lblpregunta475 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi475);
	    lblpregunta475A = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi475A);
	    lblpregunta475b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi475b);
	   
		spnQI474C=new SpinnerField(this.getActivity()).size(altoComponente+15, 500).callback("validar474e"); 
		
		txtQI474D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		lbltratamientoveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04bqi474d_1).textSize(16).centrar();
		
		gdTratamiento474d = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdTratamiento474d.addComponent(lbltratamientoveces);
		gdTratamiento474d.addComponent(txtQI474D);
		
		
		rgQI475=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi475_1,R.string.c2seccion_04bqi475_2,R.string.c2seccion_04bqi475_3,R.string.c2seccion_04bqi475_4,R.string.c2seccion_04bqi475_5,R.string.c2seccion_04bqi475_6,R.string.c2seccion_04bqi475_7,R.string.c2seccion_04bqi475_8,R.string.c2seccion_04bqi475_9,R.string.c2seccion_04bqi475_10,R.string.c2seccion_04bqi475_11).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI475_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI475.agregarEspecifique(10,txtQI475_O); 
		rgQI475B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi475b_1,R.string.c2seccion_04bqi475b_2,R.string.c2seccion_04bqi475b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta474c,lblpregunta474c_ind,spnQI474C); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta474d,lblpregunta474d_ind,gdTratamiento474d.component()); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta475,lblpregunta475A,rgQI475); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta475b,rgQI475B); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi474c!=null) {
				c2seccion_04b.qi474c=c2seccion_04b.getConvertQi474c(c2seccion_04b.qi474c);
			}	
			
			if (c2seccion_04b.qi475b!=null) {
				c2seccion_04b.qi475b=c2seccion_04b.getConvertQi475b(c2seccion_04b.qi475b);
			}	
		}
			
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
				
		if(App.getInstance().getSeccion04B().qi474==1) {
			
			if(App.getInstance().getSeccion04B().filtro474a!=null && App.getInstance().getSeccion04B().filtro474a>1) {
				if (Util.esVacio(c2seccion_04b.qi474c)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI474C"); 
					view = spnQI474C; 
					error = true; 
					return false; 
				}
			}
			

			if (Util.esVacio(c2seccion_04b.qi474d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI474D"); 
				view = txtQI474D; 
				error = true; 
				return false; 
			}
		}
		if(filtro474e) {
			if (Util.esVacio(c2seccion_04b.qi475)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI475"); 
				view = rgQI475; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04b.qi475,11)){ 
				if (Util.esVacio(c2seccion_04b.qi475_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI475_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		
		/*if(App.getInstance().getSeccion04B().qi474==2 || filtro474e ) {
			if (Util.esVacio(c2seccion_04b.qi475)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI475"); 
				view = rgQI475; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04b.qi475,11)){ 
				if (Util.esVacio(c2seccion_04b.qi475_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI475_O; 
					error = true; 
					return false; 
				} 
			} 
		}*/		
		
		if (Util.esVacio(c2seccion_04b.qi475b)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI475B"); 
			view = rgQI475B; 
			error = true; 
			return false; 
		} 
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi474c!=null) {
				c2seccion_04b.qi474c=c2seccion_04b.setConvertQi474c(c2seccion_04b.qi474c);
			}	
			
			if (c2seccion_04b.qi475b!=null) {
				c2seccion_04b.qi475b=c2seccion_04b.setConvertQi475b(c2seccion_04b.qi475b);
			}	
			
		}		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		MyUtil.llenarComboQI474C(c2seccion_04b,spnQI474C,getResources(),getActivity());
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	validar474();
//    	MyUtil.LiberarMemoria();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    
    public void validar474() {
    	if(App.getInstance().getSeccion04B().qi474!=1) {
    		Util.cleanAndLockView(getActivity(),spnQI474C,txtQI474D);   
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		lblpregunta475A.setVisibility(View.VISIBLE);
    		lblpregunta475.setVisibility(View.GONE);
    		
    	}
    	else {
    		Util.lockView(getActivity(),false,spnQI474C,txtQI474D);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		lblpregunta475A.setVisibility(View.GONE);
    		lblpregunta475.setVisibility(View.VISIBLE);
    		validar474b();
    		validar474e();
    	}
    }
    
    public void validar474b() {
    	if(App.getInstance().getSeccion04B().filtro474a!=null && App.getInstance().getSeccion04B().filtro474a>1) {
    		Util.lockView(getActivity(),false,spnQI474C);
    		q1.setVisibility(View.VISIBLE);
    	}
    	else {
    		Util.cleanAndLockView(getActivity(),spnQI474C);
    		q1.setVisibility(View.GONE);    
    		
    	}    	
    }
    
    public void validar474e() {
    	boolean esCodCol2=false;
    	if(App.getInstance().getSeccion04B().filtro474a!=null && App.getInstance().getSeccion04B().filtro474a>1) {
    		
         String opcion = c2seccion_04b.getConvertQi474c(spnQI474C.getSelectedItemKey().toString());		
    	 if(opcion.equals("H")) {
    		 if(App.getInstance().getSeccion04B().qi474a_h==1)
    			 esCodCol2=true;
    	 }
    	 else if(opcion.equals("K")) {
    		 if(App.getInstance().getSeccion04B().qi474a_k==1)
    			 esCodCol2=true;
    	 }
    	 else if(opcion.equals("L")) {
    		 if(App.getInstance().getSeccion04B().qi474a_l==1)
    			 esCodCol2=true;
    	 }
    	 else if(opcion.equals("M")) {
    		 if(App.getInstance().getSeccion04B().qi474a_m==1)
    			 esCodCol2=true;
    	 }
    	 else if(opcion.equals("N")) {
    		 if(App.getInstance().getSeccion04B().qi474a_n==1)
    			 esCodCol2=true;
    	 }
    	 else if(opcion.equals("X")) {
    		 if(App.getInstance().getSeccion04B().qi474a_x==1)
    			 esCodCol2=true;
    	 }
    		
    	 if(esCodCol2) {
    		Util.lockView(getActivity(),false,rgQI475);
     		q3.setVisibility(View.VISIBLE);
     		MyUtil.LiberarMemoria();
     		lblpregunta475.setVisibility(View.VISIBLE);
    		lblpregunta475A.setVisibility(View.GONE);
    	 }
    	 else {
    		MyUtil.LiberarMemoria();
    		Util.cleanAndLockView(getActivity(),rgQI475);
     		q3.setVisibility(View.GONE);   
     	 }    	 
    		
    	}
    	else {    		
    		 if( (App.getInstance().getSeccion04B().qi474a_h!=null && App.getInstance().getSeccion04B().qi474a_h==1) ||
       		 (App.getInstance().getSeccion04B().qi474a_k!=null && App.getInstance().getSeccion04B().qi474a_k==1) ||
    	     (App.getInstance().getSeccion04B().qi474a_l!=null && App.getInstance().getSeccion04B().qi474a_l==1) ||
    	     (App.getInstance().getSeccion04B().qi474a_m!=null && App.getInstance().getSeccion04B().qi474a_m==1) ||
    	     (App.getInstance().getSeccion04B().qi474a_n!=null && App.getInstance().getSeccion04B().qi474a_n==1) ||
    		 (App.getInstance().getSeccion04B().qi474a_x!=null && App.getInstance().getSeccion04B().qi474a_x==1)) {  
    			 esCodCol2=true;
	    		 Util.lockView(getActivity(),false,rgQI475);
	      		 q3.setVisibility(View.VISIBLE);
	      		 lblpregunta475.setVisibility(View.VISIBLE);
	      		 MyUtil.LiberarMemoria();
	      		 lblpregunta475A.setVisibility(View.GONE);
	    		
    		 } 
    		 else {
    			 MyUtil.LiberarMemoria();
    			 Util.cleanAndLockView(getActivity(),rgQI475);
    			 q3.setVisibility(View.GONE);
    			
    		 }    		
    	}    	
    	
    	filtro474e = esCodCol2;
    }
    
	
	public void renombrarEtiquetas() {	
    	lblpregunta474d.setText(lblpregunta474d.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta475A.setText(lblpregunta475A.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta475.setText(lblpregunta475.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta475b.setText(lblpregunta475b.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI475.readOnly();
			rgQI475B.readOnly();
			txtQI474D.readOnly();
			txtQI475_O.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi474c!=null) {
				c2seccion_04b.qi474c=c2seccion_04b.getConvertQi474c(c2seccion_04b.qi474c);
			}	
			if (c2seccion_04b.qi475b!=null) {
				c2seccion_04b.qi475b=c2seccion_04b.getConvertQi475b(c2seccion_04b.qi475b);
			}	
		}
			
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
