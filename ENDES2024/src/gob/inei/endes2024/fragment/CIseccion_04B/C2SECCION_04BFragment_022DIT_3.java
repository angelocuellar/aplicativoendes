package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_3_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_3 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478G1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478G2_A; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478G2_B;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478G2_C;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478G3; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478G4; 

	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478g1, lblpregunta478g1_ind,lblpregunta478g2, lblpregunta478g2_a,lblpregunta478g2_b,
	lblpregunta478g2_c,lblpregunta478g2_aInd,lblpregunta478g2_bInd,lblpregunta478g2_cInd,lblpregunta478g3,
	lblpregunta478g3_ind,lblpregunta478g4, lblpregunta478g4_ind;
	
	public DISCAPACIDAD detalle; 
	public GridComponent2 gridDiscapacidad,gridpregunta478G2;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_3() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478G1","QI478G2_A","QI478G2_B","QI478G2_C","QI478G3","QI478G4","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478G1","QI478G2_A","QI478G2_B","QI478G2_C","QI478G3","QI478G4","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_3 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo3de19_23g).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478g1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478g2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478g3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478g4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");

		lblpregunta478g1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478g3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478g4_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		rgQI478G1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("QI478G1OnChangeValue"); 
		rgQI478G3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478G4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	
		lblpregunta478g2_a= new LabelComponent(getActivity()).size(altoComponente+200, 600).textSize(18).text("");
		lblpregunta478g2_b= new LabelComponent(getActivity()).size(altoComponente+170, 600).textSize(18).text("");
		lblpregunta478g2_c= new LabelComponent(getActivity()).size(altoComponente+220, 600).textSize(18).text("");
		rgQI478G2_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+200,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478G2_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+170,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478G2_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+220,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		gridpregunta478G2=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridpregunta478G2.addComponent(lblpregunta478g2_a);
		gridpregunta478G2.addComponent(rgQI478G2_A);
		gridpregunta478G2.addComponent(lblpregunta478g2_b);
		gridpregunta478G2.addComponent(rgQI478G2_B);
		gridpregunta478G2.addComponent(lblpregunta478g2_c);
		gridpregunta478G2.addComponent(rgQI478G2_C);
	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,lblpregunta478g1, lblpregunta478g1_ind, rgQI478G1); 
		q4  = createQuestionSection(0,lblpregunta478g2, gridpregunta478G2.component());
		q5  = createQuestionSection(0,lblpregunta478g3, lblpregunta478g3_ind, rgQI478G3);
		q6  = createQuestionSection(0,lblpregunta478g4, lblpregunta478g4_ind, rgQI478G4);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478g1  == null) {
				error = true;
				view = rgQI478G1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G1");
				return false;
			}
			if (ninio.qi478g2_a  == null) {
				error = true;
				view = rgQI478G2_A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G2_A");
				return false;
			}
			if (ninio.qi478g2_b  == null) {
				error = true;
				view = rgQI478G2_B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G2_B");
				return false;
			}
			if (ninio.qi478g2_c  == null) {
				error = true;
				view = rgQI478G2_C;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G2_C");
				return false;
			}
			if (ninio.qi478g3  == null) {
				error = true;
				view = rgQI478G3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G3");
				return false;
			}
			if (ninio.qi478g4  == null) {
				error = true;
				view = rgQI478G4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478G4");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	
	public void cambiarVariablesParalaBasedeDatos(){
		ninio.qi478g1=ninio.qi478g1!=null?ninio.setConvertconTresVariables(ninio.qi478g1):null;
		ninio.qi478g2_a=ninio.qi478g2_a!=null?ninio.setConvertconTresVariables(ninio.qi478g2_a):null;
		ninio.qi478g2_b=ninio.qi478g2_b!=null?ninio.setConvertconTresVariables(ninio.qi478g2_b):null;
		ninio.qi478g2_c=ninio.qi478g2_c!=null?ninio.setConvertconTresVariables(ninio.qi478g2_c):null;
		ninio.qi478g3=ninio.qi478g3!=null?ninio.setConvertconTresVariables(ninio.qi478g3):null;
		ninio.qi478g4=ninio.qi478g4!=null?ninio.setConvertconTresVariables(ninio.qi478g4):null;

	}
	
	private void cambiarVariablesParaMostraraUsuario(){
		ninio.qi478g1 =ninio.qi478g1 !=null?ninio.getConvertconTresVariables(ninio.qi478g1):null;
		ninio.qi478g2_a =ninio.qi478g2_a !=null?ninio.getConvertconTresVariables(ninio.qi478g2_a):null;
		ninio.qi478g2_b =ninio.qi478g2_b !=null?ninio.getConvertconTresVariables(ninio.qi478g2_b):null;
		ninio.qi478g2_c =ninio.qi478g2_c !=null?ninio.getConvertconTresVariables(ninio.qi478g2_c):null;
		ninio.qi478g3 =ninio.qi478g3 !=null?ninio.getConvertconTresVariables(ninio.qi478g3):null;
		ninio.qi478g4 =ninio.qi478g4 !=null?ninio.getConvertconTresVariables(ninio.qi478g4):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
//		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
	}
	public void QI478G1OnChangeValue(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(!Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registr� como Discapacitado! ");
		}
	}
	public void evaluarPregunta() {		
		if(App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=19 && App.getInstance().getSeccion04DIT_02().qi478 <=23) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478G1,rgQI478G2_A,rgQI478G2_B, rgQI478G2_C, rgQI478G3,rgQI478G4);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
		else {
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478G1,rgQI478G2_A,rgQI478G2_B, rgQI478G2_C, rgQI478G3,rgQI478G4);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
		}
	}
	
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478G1,rgQI478G2_A,rgQI478G2_B, rgQI478G2_C, rgQI478G3,rgQI478G4);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478G1,rgQI478G2_A,rgQI478G2_B, rgQI478G2_C, rgQI478G3,rgQI478G4);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
		}
	}
	 public void abrirDetalle(DISCAPACIDAD modelo) {
			FragmentManager fm = C2SECCION_04BFragment_022DIT_3.this.getFragmentManager();
			C2SECCION_04BFragment_018DIT_3_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_3_Dialog.newInstance(this, modelo);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
		}
	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478g1.setText("(NOMBRE)");		
		lblpregunta478g1.setText(lblpregunta478g1.getText().toString().replace(replace, nombre));	
		lblpregunta478g1.setText(Html.fromHtml("478G1. "+lblpregunta478g1.getText()+" �nombra las <b>partes de su cuerpo</b>?"));
		lblpregunta478g1_ind.setText(Html.fromHtml("SI LA MADRE RESPONDE S�, PREGUNTE: <br>�Cu�les son las partes de su cuerpo que su hija(o) nombra y c�mo las nombra?.<br><br>SELECCIONE S�  CUANDO LA MADRE REPORTE QUE SU HIJA(O) <b>NOMBRA ADECUADAMENTE 2 PARTES</b> DE SU CUERPO."));
		
		lblpregunta478g2.setText("(NOMBRE)");
		lblpregunta478g2.setText(lblpregunta478g2.getText().toString().replace(replace, nombre));
		lblpregunta478g2.setText(Html.fromHtml("478G2. Cuando "+lblpregunta478g2.getText()+" <b>habla:</b>"));		
		lblpregunta478g2_a.setText(Html.fromHtml("A. �Usa <b>palabras</b>?<br><br>SI LA MADRE RESPONDE S�, PREGUNTE:<br>Por ejemplo �qu� palabras dice?<br><br>MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> PALABRA <b>AUNQUE ESTA SOLO LA ENTIENDA LA MADRE</b>."));
		lblpregunta478g2_b.setText(Html.fromHtml("B. �Usa <b>palabras</b> que <b>todas las personas entienden</b>?<br><br>SI LA MADRE RESPONDE S�, PREGUNTE:<br>Por ejemplo �qu� palabras dice?<br><br>MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> PALABRA QUE TODOS ENTIENDEN PORQUE EXISTE."));
		lblpregunta478g2_c.setText(Html.fromHtml("C. �Usa <b>frases</b> de <b>2 a 4 palabras que todas las personas entienden?</b><br><br>SI LA MADRE RESPONDE S�, PREGUNTE:<br>Por ejemplo �qu� frases dice?<br><br>MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> FRASE COMPUESTA POR <b>2 O M�S PALABRAS QUE EXISTEN</b>."));
		
		lblpregunta478g3.setText("(NOMBRE)");
		lblpregunta478g3.setText(lblpregunta478g3.getText().toString().replace(replace, nombre));
		lblpregunta478g3.setText(Html.fromHtml("478G3. Cuando usted <b>le pide</b> a "+lblpregunta478g3.getText()+" que <b>coja un objeto que no est� a la vista y que luego lo coloque donde usted le indica sin mostrarle c�mo hacerlo</b> �lo hace?"));
		lblpregunta478g3_ind.setText(Html.fromHtml("SIEMPRE LEA: <br>Por ejemplo, si le dice que coloque la pelota encima de la mesa, ella (�l) busca la pelota y la pone sobre la mesa <b>sin que usted le haya mostrado c�mo hacerlo</b>."));
		
		lblpregunta478g4.setText("(NOMBRE)");
		lblpregunta478g4.setText(lblpregunta478g4.getText().toString().replace(replace, nombre));
		lblpregunta478g4.setText(Html.fromHtml("478G4. "+lblpregunta478g4.getText()+" <b>generalmente</b> �<b>�participa�</b> en las conversaciones con adultos?"));
		lblpregunta478g4_ind.setText(Html.fromHtml("SIEMPRE LEA:<br>Por ejemplo, su hija(o) pregunta o responde o se expresa verbalmente <b>como parte</b> de la conversaci�n de los adultos."));
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478G1.readOnly();
			rgQI478G2_A.readOnly();
			rgQI478G2_B.readOnly();
			rgQI478G2_C.readOnly();
			rgQI478G3.readOnly();
			rgQI478G4.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
