package gob.inei.endes2024.fragment.CIseccion_04B;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_v2017_3 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI478C1; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478C2; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478C3; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478C4; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478C5; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478C6; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478C7; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478C8; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478C9;  
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478C10;
	@FieldAnnotation(orderIndex=11) 
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI478A;

	CISECCION_04DIT ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo,lblpregunta478c1,lblpregunta478c2,lblpregunta478c21,lblpregunta478c22,lblpregunta478c23,lblpregunta478c24,lblpregunta478c3,lblpregunta478c31,lblpregunta478c32,
	lblpregunta478c4,lblpregunta478c41,lblpregunta478c42,lblpregunta478c43,lblpregunta478c5,lblpregunta478c6,lblpregunta478c61,lblTramo,
	lblpregunta478c7,lblpregunta478c8,lblpregunta478c9,lblpregunta478c10,lbldiscapacidad;
	public GridComponent2 gridpregunta478b9,gridDiscapacidad;
	public ButtonComponent  btnCancelar;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	
	public String nombre;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_v2017_3() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478C1","QI478C2","QI478C3","QI478C4","QI478C5","QI478C6","QI478C7","QI478C8","QI478C9","QI478C10","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478C1","QI478C2","QI478C3","QI478C4","QI478C5","QI478C6","QI478C7","QI478C8","QI478C9","QI478C10","QI478A")}; 
	}         
	public C2SECCION_04BFragment_022DIT_v2017_3 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo30_36).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478a);
		lblTramo = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478a_ind);
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		lblpregunta478c1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		lblpregunta478c2 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c21 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c21).negrita();
		lblpregunta478c22 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c22);
		lblpregunta478c23 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c23).negrita();
		lblpregunta478c24 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c24);
		lblpregunta478c3 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c31 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c3).negrita();
		lblpregunta478c32 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478c31);
		lblpregunta478c4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c41= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c4).negrita();
		lblpregunta478c42= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478c41);
		lblpregunta478c43= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c42);
		lblpregunta478c5 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c6 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c61 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478c61);
		lblpregunta478c7 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478c8 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		lblpregunta478c9 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		lblpregunta478c10 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		
		rgQI478C1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c1_1,R.string.c2seccion_04b_2qi478c1_2,R.string.c2seccion_04b_2qi478c1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c2_1,R.string.c2seccion_04b_2qi478c2_2,R.string.c2seccion_04b_2qi478c2_3,R.string.c2seccion_04b_2qi478c2_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c3_1,R.string.c2seccion_04b_2qi478c3_2,R.string.c2seccion_04b_2qi478c3_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c4_1,R.string.c2seccion_04b_2qi478c4_2,R.string.c2seccion_04b_2qi478c4_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c5_1,R.string.c2seccion_04b_2qi478c5_2,R.string.c2seccion_04b_2qi478c5_3,R.string.c2seccion_04b_2qi478c5_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI478C6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c6_1,R.string.c2seccion_04b_2qi478c6_2,R.string.c2seccion_04b_2qi478c6_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c7_1,R.string.c2seccion_04b_2qi478c7_2,R.string.c2seccion_04b_2qi478c7_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqi478c7ChangeValue"); 
		rgQI478C8=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c8_1,R.string.c2seccion_04b_2qi478c8_2,R.string.c2seccion_04b_2qi478c8_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C9=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c9_1,R.string.c2seccion_04b_2qi478c9_2,R.string.c2seccion_04b_2qi478c9_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478C10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478c10_1,R.string.c2seccion_04b_2qi478c10_2,R.string.c2seccion_04b_2qi478c10_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);

	} 
	
	
	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q12 = createQuestionSection(gridDiscapacidad.component());
		q13 = createQuestionSection(lblTramo);
		q1 = createQuestionSection(0,lblpregunta478c1,rgQI478C1); 
		q2 = createQuestionSection(0,lblpregunta478c2,lblpregunta478c21,lblpregunta478c22,lblpregunta478c23,lblpregunta478c24,rgQI478C2); 
		q3 = createQuestionSection(0,lblpregunta478c3,lblpregunta478c31,lblpregunta478c32,rgQI478C3); 
		q4 = createQuestionSection(0,lblpregunta478c4,lblpregunta478c41,lblpregunta478c42,lblpregunta478c43,rgQI478C4); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478c5,rgQI478C5); 
		q6 = createQuestionSection(0,lblpregunta478c6,lblpregunta478c61,rgQI478C6); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478c7,rgQI478C7); 
		q8 = createQuestionSection(0,lblpregunta478c8,rgQI478C8); 
		q10 = createQuestionSection(0,lblpregunta478c9,rgQI478C9); 
		q11 = createQuestionSection(0,lblpregunta478c10,rgQI478C10);           

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q12);
		form.addView(q13);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q10); 
		form.addView(q11); 
		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			CambiarVariablesparalaBD();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	private void CambiarVariablesparalaBD(){
		ninio.qi478c1=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c1):null;
		ninio.qi478c2=ninio.qi478c1!=null?ninio.setConvertconcuatrovariables(ninio.qi478c2):null;
		ninio.qi478c3=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c3):null;
		ninio.qi478c4=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c4):null;
		ninio.qi478c5=ninio.qi478c1!=null?ninio.setConvertconcuatrovariables(ninio.qi478c5):null;
		ninio.qi478c6=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c6):null;
		ninio.qi478c7=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c7):null;
		ninio.qi478c8=ninio.qi478c8!=null?ninio.setConvertcontresvariables(ninio.qi478c8):null;
		ninio.qi478c9=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c9):null;
		ninio.qi478c10=ninio.qi478c1!=null?ninio.setConvertcontresvariables(ninio.qi478c10):null;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478c1  == null) {
				error = true;
				view = rgQI478C1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C1");
				return false;
			}
			if (ninio.qi478c2  == null) {
				error = true;
				view = rgQI478C2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C2");
				return false;
			}
			if (ninio.qi478c3  == null) {
				error = true;
				view = rgQI478C3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C3");
				return false;
			}
			if (ninio.qi478c4  == null) {
				error = true;
				view = rgQI478C4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C4");
				return false;
			}
			if (ninio.qi478c5  == null) {
				error = true;
				view = rgQI478C5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C5");
				return false;
			}
			if (ninio.qi478c6  == null) {
				error = true;
				view = rgQI478C6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C6");
				return false;
			}
			if (ninio.qi478c7  == null) {
				error = true;
				view = rgQI478C7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C7");
				return false;
			}
			if(!Util.esDiferente(ninio.qi478c7,1)){
				if (ninio.qi478c8  == null) {
					error = true;
					view = rgQI478C8;
					mensaje = preguntaVacia.replace("$", "La pregunta P.478C8");
					return false;
				}
			}
			if (ninio.qi478c9  == null) {
				error = true;
				view = rgQI478C9;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C9");
				return false;
			}
			if (ninio.qi478c10  == null) {
				error = true;
				view = rgQI478C10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478C10");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {        
		if(App.getInstance().getSeccion04DIT()!=null) {
		ninio = getCuestionarioService().getCISECCION_04B_2(App.getInstance().getSeccion04DIT().id,App.getInstance().getSeccion04DIT().hogar_id,App.getInstance().getSeccion04DIT().persona_id,App.getInstance().getSeccion04DIT().nro_orden_ninio,seccionesCargado);
		if (ninio == null) {
			ninio = new CISECCION_04DIT();
			ninio.id = App.getInstance().getSeccion04DIT().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT().nro_orden_ninio;
		}		
		cambiarvariablesparamostrarausuario();
		entityToUI(ninio);
		nombre = ninio.qi212_nom;
		inicio();
		}
       	}
	private void cambiarvariablesparamostrarausuario(){
		ninio.qi478c1=ninio.qi478c1!=null?ninio.getConvertcontresvariables(ninio.qi478c1):null;
		ninio.qi478c2=ninio.qi478c2!=null?ninio.getConvertconcuatrovariables(ninio.qi478c2):null;
		ninio.qi478c3=ninio.qi478c3!=null?ninio.getConvertcontresvariables(ninio.qi478c3):null;
		ninio.qi478c4=ninio.qi478c4!=null?ninio.getConvertcontresvariables(ninio.qi478c4):null;
		ninio.qi478c5=ninio.qi478c5!=null?ninio.getConvertconcuatrovariables(ninio.qi478c5):null;
		ninio.qi478c6=ninio.qi478c6!=null?ninio.getConvertcontresvariables(ninio.qi478c6):null;
		ninio.qi478c7=ninio.qi478c7!=null?ninio.getConvertcontresvariables(ninio.qi478c7):null;
		ninio.qi478c8=ninio.qi478c8!=null?ninio.getConvertcontresvariables(ninio.qi478c8):null;
		ninio.qi478c9=ninio.qi478c9!=null?ninio.getConvertcontresvariables(ninio.qi478c9):null;
		ninio.qi478c10=ninio.qi478c10!=null?ninio.getConvertcontresvariables(ninio.qi478c10):null;
	}
	public void inicio(){
		onQI478AChangeValue();
		RenombrarEtiquetas();
		chbQI478A.requestFocus();
		ValidarsiesSupervisora();
	}
	public void RenombrarEtiquetas(){
		String replace ="(NOMBRE)";
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		lblpregunta478c1.setText(lblpregunta478c1.getText().toString().replace(replace,nombre));
		lblpregunta478c1.setText(Html.fromHtml("478C1. " + lblpregunta478c1.getText()+" <b>entiende</b> las palabras \"grande\" <b>y</b> \"peque�o\"?"));
		lblpregunta478c2.setText(lblpregunta478c2.getText().toString().replace(replace, nombre));
		lblpregunta478c2.setText(Html.fromHtml("478C2. Cuando "+lblpregunta478c2.getText()+" habla �Qu&eacute; <b>frases</b> dice?"));
		lblpregunta478c3.setText(lblpregunta478c3.getText().toString().replace(replace,nombre));
		lblpregunta478c3.setText(Html.fromHtml("478C3. Cuando "+lblpregunta478c3.getText()+" juega �usa algo <b>como si fuera otra cosa</b>?"));
		lblpregunta478c4.setText(lblpregunta478c4.getText().toString().replace(replace, nombre));
		lblpregunta478c4.setText(Html.fromHtml("478C4. �"+lblpregunta478c4.getText()+" juega a hacer <b>cosas de la vida diaria</b>?"));
		lblpregunta478c5.setText(lblpregunta478c5.getText().toString().replace(replace, nombre));
		lblpregunta478c5.setText(Html.fromHtml("478C5. Cuando "+lblpregunta478c5.getText()+" muestra un garabato o dibujo que ha hecho �le <b>dice lo que dibuj&oacute;</b>?"));
		lblpregunta478c6.setText(lblpregunta478c6.getText().toString().replace(replace, nombre));
		lblpregunta478c6.setText(Html.fromHtml("478C6. Cuando "+lblpregunta478c6.getText()+" ve a una persona que est&aacute; triste �<b>dice</b> \"ella o &eacute;l est&aacute; triste\" o <b>frases</b> que signifiquen lo mismo?"));
		lblpregunta478c7.setText(lblpregunta478c7.getText().toString().replace(replace, nombre));
		lblpregunta478c7.setText(Html.fromHtml("478C7. Normalmente cuando "+lblpregunta478c7.getText()+" <b>quiere algo y usted le dice que espere </b>�ella (&eacute;l) espera \"traquilamente\"?"));
		lblpregunta478c8.setText(lblpregunta478c8.getText().toString().replace(replace, nombre));
		lblpregunta478c8.setText(Html.fromHtml("478C8. Cuando "+lblpregunta478c8.getText()+" <b>quiere algo y usted le dice que espere</b> �ella (&eacute;l) se hace da�o, agrede a los dem&aacute;s o a las cosas?"));
		lblpregunta478c9.setText(lblpregunta478c9.getText().toString().replace(replace, nombre));
		lblpregunta478c9.setText(Html.fromHtml("478C9. En casa �"+lblpregunta478c9.getText()+" tiene cuentos o revistas que le permitan <b>conocer m&aacute;s palabras</b>? <br><br><b>SONDEE: �Cu&aacute;ntos?</b> <br><br><b>SELECCIONE EL CODIGO \"1\" CUANDO:</b> <br>TIENE AL MENOS 02 MATERIALES."));
		lblpregunta478c10.setText(lblpregunta478c10.getText().toString().replace(replace, nombre));
		lblpregunta478c10.setText(Html.fromHtml("478C10. �"+lblpregunta478c10.getText()+" <b>normalmente</b> juega con otros ni�os <b>de su edad</b>?"));
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478C1,rgQI478C2,rgQI478C3,rgQI478C4,rgQI478C5,rgQI478C6,rgQI478C7,rgQI478C8,rgQI478C9,rgQI478C10);
			MyUtil.LiberarMemoria();
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,rgQI478C1,rgQI478C2,rgQI478C3,rgQI478C4,rgQI478C5,rgQI478C6,rgQI478C7,rgQI478C8,rgQI478C9,rgQI478C10);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			onqi478c7ChangeValue();
		}
	}
	
	public void onqi478c7ChangeValue(){
		if(rgQI478C7.getValue()!=null) {
			if(MyUtil.incluyeRango(2, 3,rgQI478C7.getTagSelected("").toString())){
				Util.cleanAndLockView(getActivity(), rgQI478C8);
				q8.setVisibility(View.GONE);
				rgQI478C9.requestFocus();
			}
			else{
				Util.lockView(getActivity(),false, rgQI478C8);
				q8.setVisibility(View.VISIBLE);
				rgQI478C8.requestFocus();
			}
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478C1.readOnly();
			rgQI478C2.readOnly();
			rgQI478C3.readOnly();
			rgQI478C4.readOnly();
			rgQI478C5.readOnly();
			rgQI478C6.readOnly();
			rgQI478C7.readOnly();
			rgQI478C8.readOnly();
			rgQI478C9.readOnly();
			rgQI478C10.readOnly();
		}
	}
	
	@Override
	public Integer grabadoParcial() {
		return null;
	}

}
