package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_011 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI466; 
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI466A_A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI466A_B; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI466A_C; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI466A_D; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI466A_E; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI466A_X; 
	@FieldAnnotation(orderIndex=8) 
	public TextField txtQI466A_X_O; 
	@FieldAnnotation(orderIndex=9) 
	public TextField txtQI466B_A; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI466B; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI466B_O; 
	
	CISECCION_04B c2seccion_04b; 
	CISECCION_02 nacimiento;
	boolean val215=false;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta466,lblpregunta466a,lblpregunta466b,lblpregunta466B_Pu_Ind,lblpregunta466B_Pri_Ind,lblpregunta466a_ind,lblpregunta466b_ind,
			lblpregunta466B_Org_Ind,lbllugar; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdLugarSalud,grid465ED_CC;
	public C2SECCION_04BFragment_011() {} 
	public C2SECCION_04BFragment_011 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466","QI466A_A","QI466A_B","QI466A_C","QI466A_D","QI466A_E","QI466A_X","QI466A_X_O","QI466B","QI466B_A","QI466B_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466","QI466A_A","QI466A_B","QI466A_C","QI466A_D","QI466A_E","QI466A_X","QI466A_X_O","QI466B","QI466B_A","QI466B_O")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
		
	    lblpregunta466 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi466);
	    lblpregunta466a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi466a);
	    lblpregunta466a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi466a_ind);
	    lblpregunta466b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi466b);
	    lblpregunta466b_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi466b_ind);
	
	    rgQI466=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi466_1,R.string.c2seccion_04bqi466_2,R.string.c2seccion_04bqi466_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI466ChangeValue"); 

	    chbQI466A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466A_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466A_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI466A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi466a_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI466axChangeValue");
		txtQI466A_X_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);		
		
		Spanned texto466BPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta466B_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta466B_Pu_Ind.setText(texto466BPu);
		Spanned texto466BPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta466B_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta466B_Pri_Ind.setText(texto466BPri);
		Spanned texto466BOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta466B_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta466B_Org_Ind.setText(texto466BOrg);
						
		txtQI466B_A=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico();
		rgQI466B=new RadioGroupOtherField(this.getActivity()).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI466B.addOption(1,getResources().getText(R.string.c2seccion_04bqi466b_a_1).toString());
	
		rgQI466B.addOption(2,getResources().getText(R.string.c2seccion_04bqi466b_a_2).toString());
		rgQI466B.addOption(3,getResources().getText(R.string.c2seccion_04bqi466b_a_3).toString());
		rgQI466B.addOption(4,getResources().getText(R.string.c2seccion_04bqi466b_a_4).toString());
		rgQI466B.addOption(5,getResources().getText(R.string.c2seccion_04bqi466b_a_5).toString());
		rgQI466B.addOption(6,getResources().getText(R.string.c2seccion_04bqi466b_a_6).toString());
		rgQI466B.addOption(7,getResources().getText(R.string.c2seccion_04bqi466b_a_7).toString());
		rgQI466B.addOption(8,getResources().getText(R.string.c2seccion_04bqi466b_a_8).toString());
		
		rgQI466B.addOption(9,getResources().getText(R.string.c2seccion_04bqi466b_a_9).toString());
		rgQI466B.addOption(10,getResources().getText(R.string.c2seccion_04bqi466b_a_10).toString());
		rgQI466B.addOption(11,getResources().getText(R.string.c2seccion_04bqi466b_a_11).toString());
		
		rgQI466B.addOption(12,getResources().getText(R.string.c2seccion_04bqi466b_a_12).toString());
		txtQI466B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500);
		 
		rgQI466B.agregarEspecifique(11,txtQI466B_O); 
			
		
		rgQI466B.addView(lblpregunta466B_Pu_Ind,0);
		rgQI466B.addView(lblpregunta466B_Pri_Ind,8);
		rgQI466B.addView(lblpregunta466B_Org_Ind,11);
		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04bqi466b_nom).textSize(16);
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI466B_A);	
		
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta466,rgQI466); 
		LinearLayout ly466a = new LinearLayout(getActivity());
		ly466a.addView(chbQI466A_X);
		ly466a.addView(txtQI466A_X_O);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta466a,lblpregunta466a_ind,chbQI466A_A,chbQI466A_B,chbQI466A_C,chbQI466A_D,chbQI466A_E,ly466a); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta466b,lblpregunta466b_ind,gdLugarSalud.component(),rgQI466B); 
	
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		if(c2seccion_04b!=null){			
			if (c2seccion_04b.qi466!=null) {
				c2seccion_04b.qi466=c2seccion_04b.getConvertQi466(c2seccion_04b.qi466);
			}	
		}
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
    	if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(c2seccion_04b.qi466)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI466"); 
			view = rgQI466; 
			error = true; 
			return false; 
		} 
		if(c2seccion_04b.qi466==1) {
			if(!verificarCheck466a()) {
				mensaje = "Seleccione una opci�n en P.466A"; 
				view = chbQI466A_A; 
				error = true; 
				return false; 
			}
				
			if (chbQI466A_X.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi466a_x_o)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI466A_X_O; 
					error = true; 
					return false; 
				}
			}	
			if (Util.esVacio(c2seccion_04b.qi466b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI466B"); 
				view = rgQI466B; 
				error = true; 
				return false; 
			}
			if(!Util.esDiferente(c2seccion_04b.qi466b,12)){ 
				if (Util.esVacio(c2seccion_04b.qi466b_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI466B_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		return true; 
    } 
    
 
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 

		if(c2seccion_04b!=null){			
			if (c2seccion_04b.qi466!=null) {
				c2seccion_04b.qi466=c2seccion_04b.setConvertQi466(c2seccion_04b.qi466);
			}	
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		
		
	   	SeccionCapitulo[] seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215Y","QI215M","QI215D","QI216","QI217CONS","QI218") };
		nacimiento =  getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesPersonas);
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
		Calendar fecharef = new GregorianCalendar();
			
		if(nacimiento!=null && nacimiento.qi216==1) {
			String fechareferencia = nacimiento.qi217cons;
			if(fechareferencia!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(fechareferencia);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}			
		}		
					
		if(MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef) >= 4) {
			val215= true;
		}
			
		entityToUI(c2seccion_04b); 
		inicio(); 
    }
    
    
    private void inicio() { 
    	renombrarEtiquetas();
    	onqrgQI466ChangeValue();
    	onqrgQI466axChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
 
	
	public void onqrgQI466ChangeValue() {		
		if(rgQI466.getValue()!=null ) {
			if (rgQI466.getValue().toString().equals("1")) {
				Util.lockView(getActivity(),false,chbQI466A_A,chbQI466A_B,chbQI466A_C,chbQI466A_D,chbQI466A_E,chbQI466A_X,txtQI466A_X_O,rgQI466B);  
				q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI466A_A,chbQI466A_B,chbQI466A_C,chbQI466A_D,chbQI466A_E,chbQI466A_X,txtQI466A_X_O,rgQI466B);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	public boolean verificarCheck466a() {
  		if (chbQI466A_A.isChecked() || chbQI466A_B.isChecked() || chbQI466A_C.isChecked() || chbQI466A_D.isChecked() || 
  			chbQI466A_E.isChecked() || chbQI466A_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	public void onqrgQI466axChangeValue() {  	
		if (chbQI466A_X.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI466A_X_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI466A_X_O);    		
    	}	
    }
	
	public void renombrarEtiquetas(){	
		lblpregunta466.setText(lblpregunta466.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    	lblpregunta466a.setText(lblpregunta466a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta466b.setText(lblpregunta466b.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI466.readOnly();
			rgQI466B.readOnly();
			txtQI466A_X_O.readOnly();
			txtQI466B_A.readOnly();
			txtQI466B_O.readOnly();
			chbQI466A_A.readOnly();
			chbQI466A_B.readOnly();
			chbQI466A_C.readOnly();
			chbQI466A_D.readOnly();
			chbQI466A_E.readOnly();
			chbQI466A_X.readOnly();
		}
	}


	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
			
		if(c2seccion_04b!=null){			
			if (c2seccion_04b.qi466!=null) {
				c2seccion_04b.qi466=c2seccion_04b.getConvertQi466(c2seccion_04b.qi466);
			}	
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
