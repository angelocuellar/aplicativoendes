package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_019 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI476AN1; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI476AS1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI476AT1; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI476AR1; 
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI476AN2; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI476AS2; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI476AT2; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI476AR2; 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI476AN3; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI476AS3; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI476AT3; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI476AR3; 
	@FieldAnnotation(orderIndex=13) 
	public IntegerField txtQI476AN4; 
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI476AS4; 
	@FieldAnnotation(orderIndex=15) 
	public RadioGroupOtherField rgQI476AT4; 
	@FieldAnnotation(orderIndex=16) 
	public RadioGroupOtherField rgQI476AR4; 
	@FieldAnnotation(orderIndex=17) 
	public IntegerField txtQI476AN5; 
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI476AS5; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQI476AT5; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQI476AR5; 
	@FieldAnnotation(orderIndex=21) 
	public IntegerField txtQI476AN6; 
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQI476AS6; 
	@FieldAnnotation(orderIndex=23) 
	public RadioGroupOtherField rgQI476AT6; 
	@FieldAnnotation(orderIndex=24) 
	public RadioGroupOtherField rgQI476AR6; 
	@FieldAnnotation(orderIndex=25) 
	public IntegerField txtQI476AN7; 
	@FieldAnnotation(orderIndex=26) 
	public RadioGroupOtherField rgQI476AS7; 
	@FieldAnnotation(orderIndex=27) 
	public RadioGroupOtherField rgQI476AT7; 
	@FieldAnnotation(orderIndex=28) 
	public RadioGroupOtherField rgQI476AR7; 
	 
	@FieldAnnotation(orderIndex=29) 
	public CheckBoxField chbQI476B_A;
	@FieldAnnotation(orderIndex=30) 
	public CheckBoxField chbQI476B_B;
	@FieldAnnotation(orderIndex=31) 
	public CheckBoxField chbQI476B_C;
	@FieldAnnotation(orderIndex=32) 
	public CheckBoxField chbQI476B_D;
	@FieldAnnotation(orderIndex=33) 
	public CheckBoxField chbQI476B_E;
	@FieldAnnotation(orderIndex=34) 
	public CheckBoxField chbQI476B_F;
	@FieldAnnotation(orderIndex=35) 
	public CheckBoxField chbQI476B_G;
	@FieldAnnotation(orderIndex=36) 
	public CheckBoxField chbQI476B_H;
	@FieldAnnotation(orderIndex=37) 
	public CheckBoxField chbQI476B_I;
	@FieldAnnotation(orderIndex=38) 
	public CheckBoxField chbQI476B_J;
	@FieldAnnotation(orderIndex=39) 
	public CheckBoxField chbQI476B_K;
	@FieldAnnotation(orderIndex=40) 
	public CheckBoxField chbQI476B_X;
	@FieldAnnotation(orderIndex=41) 
	public TextField txtQI476B_X_O;
	@FieldAnnotation(orderIndex=42) 
	public CheckBoxField chbQI476B_Y;
	@FieldAnnotation(orderIndex=43) 
	public CheckBoxField chbQI476B_Z;
	
	
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta476a, lblQI476a1,lblQI476a2,lblQI476a3,lblQI476a4,lblQI476a5,lblQI476a6,
						   lblQI476a7,lblblanco3,lblP476a_ca1,lblP476a_ca2,lblP476a_ca3,lblP476a_ca4,lblpregunta476a_ind,
						   lblpregunta476b,lblpregunta476b_ind1,lblpregunta476b_ind2,lblpregunta476b_ind3,lblpregunta476b_ind4,lblpregunta476b_ind5,
						   lblpregunta476b_ind6; 
	
	LinearLayout q0,q1,q2,q3; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	GridComponent2 grid1,grid2,grid3,grid4,grid5,grid6,grid7,grid8,grid0;
	String nombre_persona;
	public TextField txtCabecera;
	String fechareferencia;
	public C2SECCION_04BFragment_019() {} 
	public C2SECCION_04BFragment_019 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
  } 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
			
		rango(getActivity(), txtQI476AN1, 0, 9); 
		rango(getActivity(), txtQI476AN2, 0, 9); 
		rango(getActivity(), txtQI476AN3, 0, 9); 
		rango(getActivity(), txtQI476AN4, 0, 9); 
		rango(getActivity(), txtQI476AN5, 0, 9); 
		rango(getActivity(), txtQI476AN6, 0, 9); 
		rango(getActivity(), txtQI476AN7, 0, 9); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI476AN1","QI476AS1","QI476AT1","QI476AR1","QI476AN2","QI476AS2","QI476AT2","QI476AR2","QI476AN3","QI476AS3","QI476AT3","QI476AR3","QI476AN4","QI476AS4","QI476AT4","QI476AR4","QI476AN5","QI476AS5","QI476AT5","QI476AR5","QI476AN6","QI476AS6","QI476AT6","QI476AR6","QI476AN7","QI476AS7","QI476AT7","QI476AR7","QI476B_A", "QI476B_B", "QI476B_C", "QI476B_D", "QI476B_E", "QI476B_F", "QI476B_G", "QI476B_H", "QI476B_I", "QI476B_J", "QI476B_K", "QI476B_X", "QI476B_X_O", "QI476B_Y", "QI476B_Z", "QI477CONS","QI472","QI468","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI476AN1","QI476AS1","QI476AT1","QI476AR1","QI476AN2","QI476AS2","QI476AT2","QI476AR2","QI476AN3","QI476AS3","QI476AT3","QI476AR3","QI476AN4","QI476AS4","QI476AT4","QI476AR4","QI476AN5","QI476AS5","QI476AT5","QI476AR5","QI476AN6","QI476AS6","QI476AT6","QI476AR6","QI476AN7","QI476AS7","QI476AT7","QI476AR7","QI476B_A", "QI476B_B", "QI476B_C", "QI476B_D", "QI476B_E", "QI476B_F", "QI476B_G", "QI476B_H", "QI476B_I", "QI476B_J", "QI476B_K", "QI476B_X", "QI476B_X_O", "QI476B_Y", "QI476B_Z", "QI477CONS")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta476a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476a);
	    lblpregunta476a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi476a_ind);
	    lblpregunta476b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476b);
	    lblpregunta476b_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi476b_ind1).negrita();
	    lblpregunta476b_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476b_ind2);
	    lblpregunta476b_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi476b_ind3).negrita();
	    lblpregunta476b_ind4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476b_ind4);
	    lblpregunta476b_ind5 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi476b_ind5).negrita();
	    lblpregunta476b_ind6 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476b_ind6);
		lblblanco3 = new LabelComponent(getActivity()).textSize(16).size(100, 110).negrita().centrar();
		
		Spanned textolblP476a_ca1 =Html.fromHtml("�Cu�ntas <b>deposiciones</b> present�?");
		lblP476a_ca1 = new LabelComponent(this.getActivity()).size(100, 90).textSize(14).centrar();
		lblP476a_ca1.setText(textolblP476a_ca1);
		Spanned textolblP476a_ca2 =Html.fromHtml("A.\n�Present� <b>sangre</b>?");
		lblP476a_ca2 = new LabelComponent(this.getActivity()).size(100, 175).textSize(14).centrar();
		lblP476a_ca2.setText(textolblP476a_ca2);
		Spanned textolblP476a_ca3 =Html.fromHtml("B.\n�...Tuvo <b>tos</b>?");
		lblP476a_ca3 = new LabelComponent(this.getActivity()).size(100, 175).textSize(14).centrar();
		lblP476a_ca3.setText(textolblP476a_ca3);
		Spanned textolblP476a_ca4 =Html.fromHtml("C.\n�...Tuvo Dificultad <b>para respirar</b>?");
		lblP476a_ca4 = new LabelComponent(this.getActivity()).size(100, 175).textSize(14).centrar();
		lblP476a_ca4.setText(textolblP476a_ca4);
		
		lblQI476a1=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n1).negrita();
		lblQI476a2=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n2).negrita();
		lblQI476a3=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n3).negrita();
		lblQI476a4=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n4).negrita();
		lblQI476a5=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n5).negrita();
		lblQI476a6=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n6).negrita();
		lblQI476a7=new LabelComponent(getActivity()).textSize(17).size(50, MATCH_PARENT).text(R.string.c2seccion_04bqi476a_n7).negrita();
		
		txtQI476AN1=new IntegerField(this.getActivity()).size(altoComponente, 130).maxLength(1).callback("onChange476a1"); 
		rgQI476AS1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
					
		txtQI476AN2=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a2"); 
		rgQI476AS2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI476AN3=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a3"); 
		rgQI476AS3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI476AN4=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a4");
		rgQI476AS4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI476AN5=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a5"); 
		rgQI476AS5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI476AN6=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a6"); 
		rgQI476AS6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AR6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		
		txtQI476AN7=new IntegerField(this.getActivity()).size(altoComponente, MATCH_PARENT).maxLength(1).callback("onChange476a7"); 
		rgQI476AS7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI476AT7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onChangeQI476AT7"); 
		rgQI476AR7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476a_o1,R.string.c2seccion_04bqi476a_o2).size(65,MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
	
		chbQI476B_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_aChangeValue");
		chbQI476B_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_bChangeValue");
		chbQI476B_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_cChangeValue");
		chbQI476B_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_dChangeValue");
		chbQI476B_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_eChangeValue");
		chbQI476B_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_fChangeValue");
		chbQI476B_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_gChangeValue");
		chbQI476B_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_hChangeValue");
		chbQI476B_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_iChangeValue");
		chbQI476B_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_jChangeValue");
		chbQI476B_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_kChangeValue");
		chbQI476B_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_xChangeValue");
		chbQI476B_Y=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_yChangeValue");
		chbQI476B_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476b_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476b_zChangeValue");
	    txtQI476B_X_O=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 600).centrar().alfanumerico();
		
		grid2=new GridComponent2(this.getActivity(),App.ESTILO,5,1);
		grid2.addComponent(lblblanco3);
		grid2.addComponent(lblP476a_ca1);
		grid2.addComponent(lblP476a_ca2);
		grid2.addComponent(lblP476a_ca3);
		grid2.addComponent(lblP476a_ca4);
		grid2.addComponent(lblQI476a1);
		grid2.addComponent(txtQI476AN1);
		grid2.addComponent(rgQI476AS1);
		grid2.addComponent(rgQI476AT1);
		grid2.addComponent(rgQI476AR1);	
		grid2.addComponent(lblQI476a2);
		grid2.addComponent(txtQI476AN2);
		grid2.addComponent(rgQI476AS2);
		grid2.addComponent(rgQI476AT2);
		grid2.addComponent(rgQI476AR2);
		grid2.addComponent(lblQI476a3);
		grid2.addComponent(txtQI476AN3);
		grid2.addComponent(rgQI476AS3);
		grid2.addComponent(rgQI476AT3);
		grid2.addComponent(rgQI476AR3);
		grid2.addComponent(lblQI476a4);
		grid2.addComponent(txtQI476AN4);
		grid2.addComponent(rgQI476AS4);
		grid2.addComponent(rgQI476AT4);
		grid2.addComponent(rgQI476AR4);
		grid2.addComponent(lblQI476a5);
		grid2.addComponent(txtQI476AN5);
		grid2.addComponent(rgQI476AS5);
		grid2.addComponent(rgQI476AT5);
		grid2.addComponent(rgQI476AR5);
		grid2.addComponent(lblQI476a6);
		grid2.addComponent(txtQI476AN6);
		grid2.addComponent(rgQI476AS6);
		grid2.addComponent(rgQI476AT6);
		grid2.addComponent(rgQI476AR6);
		grid2.addComponent(lblQI476a7);
		grid2.addComponent(txtQI476AN7);
		grid2.addComponent(rgQI476AS7);
		grid2.addComponent(rgQI476AT7);
		grid2.addComponent(rgQI476AR7);
	
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta476a,lblpregunta476a_ind,grid2.component());
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta476b,lblpregunta476b_ind1,lblpregunta476b_ind2,lblpregunta476b_ind3,lblpregunta476b_ind4,lblpregunta476b_ind5,lblpregunta476b_ind6,chbQI476B_A,chbQI476B_B,chbQI476B_C,chbQI476B_D,chbQI476B_E,chbQI476B_F,chbQI476B_G,chbQI476B_H,chbQI476B_I,chbQI476B_J,chbQI476B_K,chbQI476B_X,txtQI476B_X_O,chbQI476B_Y,chbQI476B_Z);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
//			if(c2seccion_04b.qi477== null) {
//				c2seccion_04b.qi477cons = null;
//			} 
//			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi477cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi477cons = fechareferencia;
				}			
//			}
			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				
				SeccionCapitulo[] seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO")};
				CISECCION_04DIT_02 ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
				
				if(ninio!=null){		
					App.getInstance().setSeccion04DIT_02(null);
					App.getInstance().setSeccion04DIT_02(ninio);
				}	
				else{ 
					App.getInstance().setSeccion04DIT_02(null);
				}
				
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
				
		if( (App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467==1) || 
	    		(App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468==1) || 
	    		(App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi472==1)) {
			if (Util.esVacio(c2seccion_04b.qi476an1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN1"); 
				view = txtQI476AN1; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an1>0) {
				if (Util.esVacio(c2seccion_04b.qi476as1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS1"); 
					view = rgQI476AS1; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT1"); 
				view = rgQI476AT1; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR1"); 
				view = rgQI476AR1; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(c2seccion_04b.qi476an2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN2"); 
				view = txtQI476AN2; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an2>0) {
				if (Util.esVacio(c2seccion_04b.qi476as2)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS2"); 
					view = rgQI476AS2; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT2"); 
				view = rgQI476AT2; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR2"); 
				view = rgQI476AR2; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476an3)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN3"); 
				view = txtQI476AN3; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an3>0) {
				if (Util.esVacio(c2seccion_04b.qi476as3)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS3"); 
					view = rgQI476AS3; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at3)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT3"); 
				view = rgQI476AT3; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar3)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR3"); 
				view = rgQI476AR3; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476an4)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN4"); 
				view = txtQI476AN4; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an4>0) {
				if (Util.esVacio(c2seccion_04b.qi476as4)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS4"); 
					view = rgQI476AS4; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at4)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT4"); 
				view = rgQI476AT4; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar4)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR4"); 
				view = rgQI476AR4; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476an5)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN5"); 
				view = txtQI476AN5; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an5>0) {
				if (Util.esVacio(c2seccion_04b.qi476as5)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS5"); 
					view = rgQI476AS5; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at5)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT5"); 
				view = rgQI476AT5; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar5)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR5"); 
				view = rgQI476AR5; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476an6)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN6"); 
				view = txtQI476AN6; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an6>0) {
				if (Util.esVacio(c2seccion_04b.qi476as6)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS6"); 
					view = rgQI476AS6; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at6)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT6"); 
				view = rgQI476AT6; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar6)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR6"); 
				view = rgQI476AR6; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476an7)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AN7"); 
				view = txtQI476AN7; 
				error = true; 
				return false; 
			} 
			if(c2seccion_04b.qi476an7>0) {
				if (Util.esVacio(c2seccion_04b.qi476as7)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476AS7"); 
					view = rgQI476AS7; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi476at7)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AT7"); 
				view = rgQI476AT7; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi476ar7)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476AR7"); 
				view = rgQI476AR7; 
				error = true; 
				return false; 
			} 

			if (!Util.esDiferente(c2seccion_04b.qi468,2)) {
				if (   !Util.esDiferente(c2seccion_04b.qi476at1, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at2, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at3, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at4, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at5, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at6, 1)
					|| !Util.esDiferente(c2seccion_04b.qi476at7, 1)) {
					validarMensaje(" Verifique la pregunta P.468"); 
				}
			}
				
			if (!Util.esDiferente(c2seccion_04b.qi472,2)) {				
				if ( 	Util.esDiferente(c2seccion_04b.qi476an1,0)
					 || Util.esDiferente(c2seccion_04b.qi476an2,0)
					 || Util.esDiferente(c2seccion_04b.qi476an3,0)
					 || Util.esDiferente(c2seccion_04b.qi476an4,0)
					 || Util.esDiferente(c2seccion_04b.qi476an5,0)
					 || Util.esDiferente(c2seccion_04b.qi476an6,0)
					 || Util.esDiferente(c2seccion_04b.qi476an7,0)) {
					validarMensaje(" Verifique la pregunta P.472"); 
				}
			}
		}
		if(!verificarCheckP476b()){
			mensaje = "Seleccione una opci�n en la pregunta QI476B_A"; 
			view = chbQI476B_A; 
			error = true; 
			return false; 
		}
		if(chbQI476B_X.isChecked()) {
			if (Util.esVacio(c2seccion_04b.qi476b_x_o)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476B_X_O"); 
				view = txtQI476B_X_O; 
				error = true; 
				return false; 
			} 
		}
		return true; 
    } 
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    }
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		fechareferencia = c2seccion_04b.qi477cons;
		inicio(); 
    } 
    
    private void inicio() { 
    	renombrarEtiquetas();
    	validar476();
    	ejecutaAccionCheckbox();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 

    public void onChangeQI476AT7() {
//    	if (!Util.esDiferente(c2seccion_04b.qi468,1) 
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT7.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT6.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT5.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT4.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT3.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT2.getTagSelected("").toString())
//    			&& MyUtil.incluyeRango(2,2,rgQI476AT1.getTagSelected("").toString())
//    			) {
//    		Log.e("ai","akiii1111");
//    		validarMensaje(" Verifique la pregunta P.468"); 
//    	}
    	if (!Util.esDiferente(c2seccion_04b.qi468,2) && (
    			 MyUtil.incluyeRango(1,1,rgQI476AT7.getTagSelected("").toString())
    			|| MyUtil.incluyeRango(1,1,rgQI476AT6.getTagSelected("").toString())
    			|| MyUtil.incluyeRango(1,1,rgQI476AT5.getTagSelected("").toString())
    			|| MyUtil.incluyeRango(1,1,rgQI476AT4.getTagSelected("").toString())
    			|| MyUtil.incluyeRango(1,1,rgQI476AT3.getTagSelected("").toString())
    			|| MyUtil.incluyeRango(1,1,rgQI476AT2.getTagSelected("").toString())
    			| MyUtil.incluyeRango(1,1,rgQI476AT1.getTagSelected("").toString())
    	    	)) {
    		Log.e("ai","akiii222");
    		validarMensaje(" Verifique la pregunta P.468"); 
    	}
	}
    
    public void validar476() {
    	if(App.getInstance().getSeccion04B()!=null){
	    	if( (App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467==1) || 
	    		(App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468==1) || 
	    		(App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi472==1)) {
	
	    		Util.lockView(getActivity(),false,txtQI476AN1,txtQI476AN2,txtQI476AN3,txtQI476AN4,txtQI476AN5,txtQI476AN6,txtQI476AN7,
	    	    rgQI476AR1,rgQI476AS1,rgQI476AT1,rgQI476AR2,rgQI476AS2,rgQI476AT2,rgQI476AR3,rgQI476AS3,rgQI476AT3,rgQI476AR4,
	    	    rgQI476AS4,rgQI476AT4,rgQI476AR5,rgQI476AS5,rgQI476AT5,rgQI476AR6,rgQI476AS6,rgQI476AT6,rgQI476AR7,rgQI476AS7,rgQI476AT7);
	    		q1.setVisibility(View.VISIBLE);
	    		onChange476a1();
	        	onChange476a2();
	        	onChange476a3();
	        	onChange476a4();
	        	onChange476a5();
	        	onChange476a6();
	        	onChange476a7();
	        	onChangeQI476AT7();
	    	}
	    	else {
	    		Util.cleanAndLockView(getActivity(),txtQI476AN1,txtQI476AN2,txtQI476AN3,txtQI476AN4,txtQI476AN5,txtQI476AN6,txtQI476AN7,
	    	    rgQI476AR1,rgQI476AS1,rgQI476AT1,rgQI476AR2,rgQI476AS2,rgQI476AT2,rgQI476AR3,rgQI476AS3,rgQI476AT3,rgQI476AR4,
	    	    rgQI476AS4,rgQI476AT4,rgQI476AR5,rgQI476AS5,rgQI476AT5,rgQI476AR6,rgQI476AS6,rgQI476AT6,rgQI476AR7,rgQI476AS7,rgQI476AT7);   
	    	    q1.setVisibility(View.GONE);
	    	}
    	}
    }
        
    
	
	
	public void onChange476a1() {
		if(txtQI476AN1.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS1);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS1);   
		}		
	}
	
	public void onChange476a2() {
		if(txtQI476AN2.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS2);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS2);   
		}		
	}
	
	public void onChange476a3() {
		if(txtQI476AN3.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS3);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS3);   
		}		
	}
	
	public void onChange476a4() {
		if(txtQI476AN4.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS4);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS4);   
		}		
	}
	
	public void onChange476a5() {
		if(txtQI476AN5.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS5);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS5);   
		}		
	}
	
	public void onChange476a6() {
		if(txtQI476AN6.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS6);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS6);   
		}		
	}
	
	public void onChange476a7() {
		if(txtQI476AN7.getText().toString().equals("0")){			
			Util.cleanAndLockView(getActivity(),rgQI476AS7);   
		}
		else {
			Util.lockView(getActivity(),false,rgQI476AS7);   
		}	
//	 	if (!Util.esDiferente(c2seccion_04b.qi472,1) 
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN1.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN2.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN3.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN4.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN5.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN6.getText().toString())
//    			&& MyUtil.incluyeRango(0,0,txtQI476AN7.getText().toString())
//    	    	) {
//	 		Log.e("ai","akiii1");
//    		validarMensaje(" Verifique la pregunta P.472"); 
//    	}
		if (!Util.esDiferente(c2seccion_04b.qi472,2) && (
    			   !MyUtil.incluyeRango(0,0,txtQI476AN1.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN2.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN3.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN4.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN5.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN6.getText().toString())
    			|| !MyUtil.incluyeRango(0,0,txtQI476AN7.getText().toString())
    			)) {
			Log.e("ai","akiii2");
    		validarMensaje(" Verifique la pregunta P.472"); 
    	}
   
	}
	

	public boolean verificarCheckP476b() {
  		if (chbQI476B_A.isChecked()||chbQI476B_B.isChecked()||chbQI476B_C.isChecked()||chbQI476B_D.isChecked()||chbQI476B_E.isChecked()||chbQI476B_F.isChecked()||chbQI476B_G.isChecked()||chbQI476B_H.isChecked()||chbQI476B_I.isChecked()||chbQI476B_J.isChecked()||chbQI476B_K.isChecked()||chbQI476B_X.isChecked()||chbQI476B_Y.isChecked()||chbQI476B_Z.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
       
    public void ejecutaAccionCheckbox() {
		if(chbQI476B_Z.isChecked()){
			onqrgQI476b_zChangeValue();
		}
		else{
			if(chbQI476B_Y.isChecked()){
				onqrgQI476b_yChangeValue();
			}
			else{
				onqrgQI476b_aChangeValue();
				onqrgQI476b_bChangeValue();
				onqrgQI476b_cChangeValue();
				onqrgQI476b_dChangeValue();
				onqrgQI476b_eChangeValue();
				onqrgQI476b_fChangeValue();
				onqrgQI476b_gChangeValue();
				onqrgQI476b_hChangeValue();
				onqrgQI476b_iChangeValue();
				onqrgQI476b_jChangeValue();
				onqrgQI476b_kChangeValue();
				onqrgQI476b_xChangeValue();
			}
		}
	}
   	
	public void onqrgQI476b_aChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_bChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_cChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_dChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_eChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_fChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_gChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_hChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_iChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_jChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_kChangeValue(){if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}}
	public void onqrgQI476b_xChangeValue(){
		if (verificarCheckP476b()){Util.cleanAndLockView(getActivity(),chbQI476B_Y,chbQI476B_Z);}else{Util.lockView(getActivity(),false,chbQI476B_Y,chbQI476B_Z);}
		if (chbQI476B_X.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI476B_X_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI476B_X_O);    		
    	}
	}
	public void onqrgQI476b_yChangeValue(){
		if (verificarCheckP476b()){
			Util.cleanAndLockView(getActivity(),chbQI476B_A,chbQI476B_B,chbQI476B_C,chbQI476B_D,chbQI476B_E,chbQI476B_F,chbQI476B_G,chbQI476B_H,chbQI476B_I,chbQI476B_J,chbQI476B_K,chbQI476B_X,chbQI476B_Z,txtQI476B_X_O);
		}
		else{
			Util.lockView(getActivity(),false,chbQI476B_A,chbQI476B_B,chbQI476B_C,chbQI476B_D,chbQI476B_E,chbQI476B_F,chbQI476B_G,chbQI476B_H,chbQI476B_I,chbQI476B_J,chbQI476B_K,chbQI476B_X,chbQI476B_Z);
		}
	}
	public void onqrgQI476b_zChangeValue(){
		if (verificarCheckP476b()){
			Util.cleanAndLockView(getActivity(),chbQI476B_A,chbQI476B_B,chbQI476B_C,chbQI476B_D,chbQI476B_E,chbQI476B_F,chbQI476B_G,chbQI476B_H,chbQI476B_I,chbQI476B_J,chbQI476B_K,chbQI476B_X,chbQI476B_Y, txtQI476B_X_O);
		}
		else{
			Util.lockView(getActivity(),false,chbQI476B_A,chbQI476B_B,chbQI476B_C,chbQI476B_D,chbQI476B_E,chbQI476B_F,chbQI476B_G,chbQI476B_H,chbQI476B_I,chbQI476B_J,chbQI476B_K,chbQI476B_X,chbQI476B_Y);
		}
	}
	
	
	
	public void renombrarEtiquetas(){	
//		lblpregunta479Gbb.text(R.string.c2seccion_04b_2qi479g_bb);
    	Calendar fecharef = new GregorianCalendar();
    	
    	//ToastMessage.msgBox(this.getActivity(), fechareferencia.toString(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG); 
    	
    	if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}
    	Calendar fechainicio = MyUtil.restarDias(fecharef,90);
    	Integer f1 =fechainicio.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio.get(Calendar.MONTH)) ;
    	
    	
    	lblpregunta476a.setText(lblpregunta476a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta476b.setText(lblpregunta476b.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    	lblpregunta476b.setText(lblpregunta476b.getText().toString().replace("#", f1.toString()+" de "+mes1));
    	
    	
//		Calendar fecharef = new GregorianCalendar();
//		if(fechareferencia!=null){
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			Date date2 = null;
//			try {
//				date2 = df.parse(fechareferencia);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}		
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(date2);
//			fecharef=cal;
//		}	
//	
//    	Calendar fechainicio = MyUtil.RestarMeses(fecharef,1);
//    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
//    	Calendar fechados =	MyUtil.RestarMeses(fecharef,11);
//    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));
//
//    	String F3="";
//    	if(F1.equals("Diciembre")){
//    		F3 = "del a�o pasado";
//    	}else{
//    		F3 = "de este a�o,";
//    	}
//    	
//    	Calendar fechaHace3Mes = MyUtil.RestarMeses(fecharef,2);
//    	String F4 = MyUtil.Mes(fechaHace3Mes.get(Calendar.MONTH));
//    	

//    	lblpregunta476b.setText(lblpregunta476b.getText().toString().replace("$", F4));
//    	lblpregunta476b.setText(lblpregunta476b.getText().toString().replace("%", F2));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQI476AN1.readOnly();
			txtQI476AN2.readOnly();
			txtQI476AN3.readOnly();
			txtQI476AN4.readOnly();
			txtQI476AN5.readOnly();
			txtQI476AN6.readOnly();
			txtQI476AN7.readOnly();
			rgQI476AR1.readOnly();
			rgQI476AR2.readOnly();
			rgQI476AR3.readOnly();
			rgQI476AR4.readOnly();
			rgQI476AR5.readOnly();
			rgQI476AR6.readOnly();
			rgQI476AR7.readOnly();
			rgQI476AS1.readOnly();
			rgQI476AS2.readOnly();
			rgQI476AS3.readOnly();
			rgQI476AS4.readOnly();
			rgQI476AS5.readOnly();
			rgQI476AS6.readOnly();
			rgQI476AS7.readOnly();
			rgQI476AT1.readOnly();
			rgQI476AT2.readOnly();
			rgQI476AT3.readOnly();
			rgQI476AT4.readOnly();
			rgQI476AT5.readOnly();
			rgQI476AT6.readOnly();
			rgQI476AT7.readOnly();
			
			chbQI476B_A.readOnly();
			chbQI476B_B.readOnly();
			chbQI476B_C.readOnly();
			chbQI476B_D.readOnly();
			chbQI476B_E.readOnly();
			chbQI476B_F.readOnly();
			chbQI476B_G.readOnly();
			chbQI476B_H.readOnly();
			chbQI476B_I.readOnly();
			chbQI476B_J.readOnly();
			chbQI476B_K.readOnly();
			chbQI476B_X.readOnly();
			txtQI476B_X_O.readOnly();
			chbQI476B_Y.readOnly();
			chbQI476B_Z.readOnly();
			
		}
	}
	
	private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		try { 
			if(c2seccion_04b.qi477== null) {
				c2seccion_04b.qi477cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi477cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi477cons = fechareferencia;
				}			
			}
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
