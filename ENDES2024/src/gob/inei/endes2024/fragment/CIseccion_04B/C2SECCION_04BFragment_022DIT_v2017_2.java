package gob.inei.endes2024.fragment.CIseccion_04B;
import java.sql.SQLException;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

public class C2SECCION_04BFragment_022DIT_v2017_2 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI478B1; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478B2; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478B3; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478B4; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478B5; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478B6; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478B7; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI478B8; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478B9A;  
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478B9B;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI478B9C;
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI478B9D;
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI478B9E;
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI478B10;
	@FieldAnnotation(orderIndex=15) 
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI478A;

	CISECCION_04DIT ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo,lblpregunta478b1,lblpregunta478b1a,lblpregunta478b1b,lblpregunta478b2,lblpregunta478b3,lblpregunta478b4,lblpregunta478b5,lblpregunta478b5a,lblpregunta478b5b,lblpregunta478b6,	lblpregunta478b7,lblpregunta478b8,lblpregunta478b8a,
	lblpregunta478b9,lblpregunta478b9a,lblpregunta478b9b,lblpregunta478b9c,lblpregunta478b9d,lblpregunta478b9e,lblTramo,
     lblpregunta478b10a,lblpregunta478b10,lblpregunta478b10b,lblpregunta47810bc,lbldiscapacidad;
	public GridComponent2 gridpregunta478b9,gridDiscapacidad;
	public ButtonComponent  btnCancelar;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_v2017_2() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478B1","QI478B2","QI478B3","QI478B4","QI478B5","QI478B6","QI478B7","QI478B8","QI478B9A","QI478B9B","QI478B9C","QI478B9D","QI478B9E","QI478B10","ID","QI478A","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478B1","QI478B2","QI478B3","QI478B4","QI478B5","QI478B6","QI478B7","QI478B8","QI478B9A","QI478B9B","QI478B9C","QI478B9D","QI478B9E","QI478B10","QI478A")}; 
	}         
	
	public C2SECCION_04BFragment_022DIT_v2017_2 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override
	protected void buildFields(){
		// TODO Auto-generated method stub
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo15_18).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478a);
		lblTramo = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478a_ind);
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		lblpregunta478b1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b1a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478b1a).negrita();
		lblpregunta478b1b = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478b1b);
		lblpregunta478b2 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b3 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b4 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b5 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b5a= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478b5a).negrita();
		lblpregunta478b5b= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478b5b);
		lblpregunta478b6 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b7 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b8 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b8a= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478b8a);
		lblpregunta478b9=  new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478nombre);
		lblpregunta478b9a = new LabelComponent(getActivity()).size(altoComponente+20, 440).textSize(19);
		lblpregunta478b9b = new LabelComponent(getActivity()).size(altoComponente+20, 440).textSize(19);
		lblpregunta478b9c = new LabelComponent(getActivity()).size(altoComponente+80, 440).textSize(19);
		lblpregunta478b9d = new LabelComponent(getActivity()).size(altoComponente+50, 440).textSize(19);
		lblpregunta478b9e = new LabelComponent(getActivity()).size(altoComponente+40, 440).textSize(19);
		lblpregunta478b10a= new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478b10);
		lblpregunta478b10 = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478b10a);
		lblpregunta478b10b = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(17).text(R.string.c2seccion_04b_2qi478b10b).negrita();
		lblpregunta47810bc = new LabelComponent(getActivity()).size(MATCH_PARENT,MATCH_PARENT).textSize(17).text(R.string.c2seccion_04b_2qi478b10c);
		
		rgQI478B1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b1_1,R.string.c2seccion_04b_2qi478b1_2,R.string.c2seccion_04b_2qi478a1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478B2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b2_1,R.string.c2seccion_04b_2qi478b2_2,R.string.c2seccion_04b_2qi478b2_3,R.string.c2seccion_04b_2qi478b2_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b3_1,R.string.c2seccion_04b_2qi478b3_2,R.string.c2seccion_04b_2qi478b3_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b4_1,R.string.c2seccion_04b_2qi478b4_2,R.string.c2seccion_04b_2qi478b4_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b5_1,R.string.c2seccion_04b_2qi478b5_2,R.string.c2seccion_04b_2qi478b5_3,R.string.c2seccion_04b_2qi478b5_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478B6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b6_1,R.string.c2seccion_04b_2qi478b6_2,R.string.c2seccion_04b_2qi478b6_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b7_1,R.string.c2seccion_04b_2qi478b7_2,R.string.c2seccion_04b_2qi478b7_3,R.string.c2seccion_04b_2qi478b7_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		txtQI478B8=new IntegerField(this.getActivity()).size(altoComponente, 40).maxLength(2); 
		rgQI478B9A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b9a_1,R.string.c2seccion_04b_2qi478b9a_2,R.string.c2seccion_04b_2qi478b9a_3).size(altoComponente,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B9B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b9b_1,R.string.c2seccion_04b_2qi478b9b_2,R.string.c2seccion_04b_2qi478b9b_3).size(altoComponente,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B9C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b9c_1,R.string.c2seccion_04b_2qi478b9c_2,R.string.c2seccion_04b_2qi478b9c_3).size(altoComponente+80,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B9D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b9d_1,R.string.c2seccion_04b_2qi478b9d_2,R.string.c2seccion_04b_2qi478b9d_3).size(altoComponente+50,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B9E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b9e_1,R.string.c2seccion_04b_2qi478b9e_2,R.string.c2seccion_04b_2qi478b9e_3).size(altoComponente+40,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478B10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478b10_1,R.string.c2seccion_04b_2qi478b10_2,R.string.c2seccion_04b_2qi478b10_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		gridpregunta478b9=new GridComponent2(this.getActivity(),Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 2,1);
		gridpregunta478b9.addComponent(lblpregunta478b9a);
		gridpregunta478b9.addComponent(rgQI478B9A);
		gridpregunta478b9.addComponent(lblpregunta478b9b);
		gridpregunta478b9.addComponent(rgQI478B9B);
		gridpregunta478b9.addComponent(lblpregunta478b9c);
		gridpregunta478b9.addComponent(rgQI478B9C);
		gridpregunta478b9.addComponent(lblpregunta478b9d);
		gridpregunta478b9.addComponent(rgQI478B9D);
		gridpregunta478b9.addComponent(lblpregunta478b9e);
		gridpregunta478b9.addComponent(rgQI478B9E);

		
	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q12 = createQuestionSection(gridDiscapacidad.component());
		q13 = createQuestionSection(lblTramo);
		q1 = createQuestionSection(0,lblpregunta478b1,lblpregunta478b1a,lblpregunta478b1b,rgQI478B1); 
		q2 = createQuestionSection(0,lblpregunta478b2,rgQI478B2); 
		q3 = createQuestionSection(0,lblpregunta478b3,rgQI478B3); 
		q4 = createQuestionSection(0,lblpregunta478b4,rgQI478B4); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478b5,lblpregunta478b5a,lblpregunta478b5b,rgQI478B5); 
		q6 = createQuestionSection(0,lblpregunta478b6,rgQI478B6); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478b7,rgQI478B7); 
		q8 = createQuestionSection(0,lblpregunta478b8,lblpregunta478b8a,txtQI478B8); 
		q10 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478b9,gridpregunta478b9.component()); 
		q11 = createQuestionSection(0,lblpregunta478b10a,lblpregunta478b10,lblpregunta47810bc,lblpregunta478b10b,rgQI478B10);           

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q12);
		form.addView(q13);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q10); 
		form.addView(q11); 

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			CambiarVariablesparalaBD();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	private void CambiarVariablesparalaBD(){
		ninio.qi478b1=ninio.qi478b1!=null?ninio.setConvertcontresvariables(ninio.qi478b1):null;
		ninio.qi478b2=ninio.qi478b2!=null?ninio.setConvertconcuatrovariables(ninio.qi478b2):null;
		ninio.qi478b3=ninio.qi478b3!=null?ninio.setConvertcontresvariables(ninio.qi478b3):null;
		ninio.qi478b4=ninio.qi478b4!=null?ninio.setConvertcontresvariables(ninio.qi478b4):null;
		ninio.qi478b6=ninio.qi478b5!=null?ninio.setConvertcontresvariables(ninio.qi478b6):null;
		ninio.qi478b9a=ninio.qi478b9a!=null?ninio.setConvertcontresvariables(ninio.qi478b9a):null;
		ninio.qi478b9b=ninio.qi478b9b!=null?ninio.setConvertcontresvariables(ninio.qi478b9b):null;
		ninio.qi478b9c=ninio.qi478b9c!=null?ninio.setConvertcontresvariables(ninio.qi478b9c):null;
		ninio.qi478b9d=ninio.qi478b9d!=null?ninio.setConvertcontresvariables(ninio.qi478b9d):null;
		ninio.qi478b9e=ninio.qi478b9e!=null?ninio.setConvertcontresvariables(ninio.qi478b9e):null;
		ninio.qi478b10=ninio.qi478b10!=null?ninio.setConvertcontresvariables(ninio.qi478b10):null;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a,1)){
			if (ninio.qi478b1  == null) {
				error = true;
				view = rgQI478B1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B1");
				return false;
			}
			if (ninio.qi478b2  == null) {
				error = true;
				view = rgQI478B2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B2");
				return false;
			}
			if (ninio.qi478b3  == null) {
				error = true;
				view = rgQI478B3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B3");
				return false;
			}
			if (ninio.qi478b4  == null) {
				error = true;
				view = rgQI478B4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B4");
				return false;
			}
			if (ninio.qi478b5  == null) {
				error = true;
				view = rgQI478B5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B5");
				return false;
			}
			if (ninio.qi478b6  == null) {
				error = true;
				view = rgQI478B6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B6");
				return false;
			}
			if (ninio.qi478b7  == null) {
				error = true;
				view = rgQI478B7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B7");
				return false;
			}
			if (ninio.qi478b8  == null) {
				error = true;
				view = txtQI478B8;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B8");
				return false;
			}
			if (ninio.qi478b9a  == null) {
				error = true;
				view = rgQI478B9A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B9A");
				return false;
			}
			if (ninio.qi478b9b  == null) {
				error = true;
				view = rgQI478B9B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B9B");
				return false;
			}
			if (ninio.qi478b9c  == null) {
				error = true;
				view = rgQI478B9C;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B9C");
				return false;
			}
			if (ninio.qi478b9d  == null) {
				error = true;
				view = rgQI478B9D;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B9D");
				return false;
			}
			if (ninio.qi478b9e  == null) {
				error = true;
				view = rgQI478B9E;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B9E");
				return false;
			}
			if (ninio.qi478b10  == null) {
				error = true;
				view = rgQI478B10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478B10");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {        
		if(App.getInstance().getSeccion04DIT()!=null) {
		ninio = getCuestionarioService().getCISECCION_04B_2(App.getInstance().getSeccion04DIT().id,App.getInstance().getSeccion04DIT().hogar_id,App.getInstance().getSeccion04DIT().persona_id,App.getInstance().getSeccion04DIT().nro_orden_ninio,seccionesCargado);
		if (ninio == null) {
			ninio = new CISECCION_04DIT();
			ninio.id = App.getInstance().getSeccion04DIT().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT().nro_orden_ninio;
		}		
		cambiarvariablesparamostrarausuario();
		entityToUI(ninio);

		inicio();
		}
       	}
	private void cambiarvariablesparamostrarausuario(){
		ninio.qi478b1=ninio.qi478b1!=null? ninio.getConvertcontresvariables(ninio.qi478b1):null;
		ninio.qi478b2=ninio.qi478b2!=null? ninio.getConvertconcuatrovariables(ninio.qi478b2):null;
		ninio.qi478b3=ninio.qi478b3!=null? ninio.getConvertcontresvariables(ninio.qi478b3):null;
		ninio.qi478b4=ninio.qi478b4!=null? ninio.getConvertcontresvariables(ninio.qi478b4):null;
		ninio.qi478b6=ninio.qi478b6!=null?ninio.getConvertcontresvariables(ninio.qi478b6):null;
		ninio.qi478b9a=ninio.qi478b9a!=null?ninio.getConvertcontresvariables(ninio.qi478b9a):null;
		ninio.qi478b9b=ninio.qi478b9b!=null?ninio.getConvertcontresvariables(ninio.qi478b9b):null;
		ninio.qi478b9c=ninio.qi478b9c!=null?ninio.getConvertcontresvariables(ninio.qi478b9c):null;
		ninio.qi478b9d=ninio.qi478b9d!=null? ninio.getConvertcontresvariables(ninio.qi478b9d):null;
		ninio.qi478b9e=ninio.qi478b9e!=null?ninio.getConvertcontresvariables(ninio.qi478b9e):null;
		ninio.qi478b10=ninio.qi478b10!=null?ninio.getConvertcontresvariables(ninio.qi478b10):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		onQI478AChangeValue();
		chbQI478A.requestFocus();
		ValidarsiesSupervisora();
	}
	public void RenombrarEtiquetas(){
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		lblpregunta478b1.setText(lblpregunta478b1.getText().toString().replace(replace,nombre));
		lblpregunta478b1.setText(Html.fromHtml("478B1. Cuando se le pide a " + lblpregunta478b1.getText()+" que haga algo <b>sin mostrarle c�mo hacerlo</b> �lo hace?"));
		lblpregunta478b2.setText(lblpregunta478b2.getText().toString().replace(replace, nombre));
		lblpregunta478b2.setText(Html.fromHtml("478B2. Cuando "+lblpregunta478b2.getText()+" quiere algo, �lo pide <b>con palabras</b> de tal manera que <b>usted entiende</b>?"));
		lblpregunta478b3.setText(lblpregunta478b3.getText().toString().replace(replace, nombre));
		lblpregunta478b3.setText(Html.fromHtml("478B3. Mientras "+lblpregunta478b3.getText()+" <b>est&aacute; jugando sola(solo)</b> �<b>en alg&uacute;n momento</b> ella(&eacute;l) la busca a usted y despu&eacute;s <b>vuelve a jugar sola(solo)</b>?"));
		lblpregunta478b4.setText(lblpregunta478b4.getText().toString().replace(replace, nombre));
		lblpregunta478b4.setText(Html.fromHtml("478B4. Cuando "+lblpregunta478b4.getText()+" est&aacute; fastidiada (fastidiado) y usted <b>trata de calmarla (calmarlo)</b> �ella (&eacute;l) se calma <b>f&aacute;cilmente</b>?"));
		lblpregunta478b5.setText(lblpregunta478b5.getText().toString().replace(replace, nombre));
		lblpregunta478b5.setText(Html.fromHtml("478B5. Si "+lblpregunta478b5.getText()+" llora <b>cuando usted esta haciendo algo </b> �qu&eacute; hace usted?"));
		lblpregunta478b6.setText(lblpregunta478b6.getText().toString().replace(replace,nombre));
		lblpregunta478b6.setText(Html.fromHtml("478B6. �Le es <b>f&aacute;cil entender </b> por qu&eacute; "+lblpregunta478b6.getText()+" <b>llora</b>?"));
		lblpregunta478b7.setText(lblpregunta478b7.getText().toString().replace(replace, nombre));
		lblpregunta478b7.setText(Html.fromHtml("478B7. En las &uacute;ltimas dos semanas, aproximadamente �cuantas horas <b>al d&iacute;a se separ&oacute; de </b>"+lblpregunta478b7.getText()+"?"));
		lblpregunta478b8.setText(lblpregunta478b8.getText().toString().replace(replace,nombre));
		lblpregunta478b8.setText(Html.fromHtml("478B8. Adem&aacute;s de usted, <b>normalmente</b> �cu&aacute;ntas personas <b>le hablan a </b>"+lblpregunta478b8.getText()+"?"));
		
		lblpregunta478b9.setText(lblpregunta478b9.getText().toString().replace(replace, nombre));
		lblpregunta478b9.setText(Html.fromHtml("478B9.- En el lugar donde "+lblpregunta478b9.getText()+" <b>normalmente juega:</b>" ));
		
		lblpregunta478b9a.setText(Html.fromHtml("<b>A.</b> �Hay objetos pesados que <b>le pueden caer</b> encima?"));
		lblpregunta478b9b.setText(Html.fromHtml("<b>B.</b> �Hay objetos con los que <b>se pueda cortar</b>?"));
		lblpregunta478b9c.setText(Html.fromHtml("<b>C.</b> El lugar donde normalmente juega, est&aacute; <b>cerca</b> de desperdicios o <b>basura</b> como excrementos, restos de alimentos?"));
		lblpregunta478b9d.setText(Html.fromHtml("<b>D.</b> �Este lugar est&aacute; <b>cerca</b> de <b>elementos t&oacute;xicos</b> como detergentes, insecticidas?"));
		lblpregunta478b9e.setText(Html.fromHtml("<b>E.</b> �Este lugar est&aacute; <b>cerca de</b> pistas, carreteras, acequias <b>y</b> abismos?"));
		lblpregunta478b10a.setText(Html.fromHtml("478B10.- <b>MUESTRA LA CARTILLA 2</b>"));
		lblpregunta478b10.setText(lblpregunta478b10.getText().toString().replace(replace, nombre));
	}
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478B1,rgQI478B2,rgQI478B3,rgQI478B4,rgQI478B5,rgQI478B6,rgQI478B7,txtQI478B8,rgQI478B9A,rgQI478B9B,rgQI478B9C,rgQI478B9D,rgQI478B9E,rgQI478B10);
			MyUtil.LiberarMemoria();
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,rgQI478B1,rgQI478B2,rgQI478B3,rgQI478B4,rgQI478B5,rgQI478B6,rgQI478B7,txtQI478B8,rgQI478B9A,rgQI478B9B,rgQI478B9C,rgQI478B9D,rgQI478B9E,rgQI478B10);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
		}
	}
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI478B1.readOnly();
			rgQI478B2.readOnly();
			rgQI478B3.readOnly();
			rgQI478B4.readOnly();
			rgQI478B5.readOnly();
			rgQI478B6.readOnly();
			rgQI478B7.readOnly();
			rgQI478B9A.readOnly();
			rgQI478B9B.readOnly();
			rgQI478B9C.readOnly();
			rgQI478B9D.readOnly();
			rgQI478B9E.readOnly();
			rgQI478B10.readOnly();
			txtQI478B8.readOnly();
			chbQI478A.readOnly();
		}
	}

	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
