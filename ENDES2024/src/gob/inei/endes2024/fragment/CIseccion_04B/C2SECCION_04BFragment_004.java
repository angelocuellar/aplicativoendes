package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_004 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI461E; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI461F; 
	
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI461G;
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI461H;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI461I;
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI461J;
	
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI462; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI462A; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI462B; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI463; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI463A; 
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI463B; 
	@FieldAnnotation(orderIndex=13) 
	
//	public RadioGroupOtherField rgQI464; 
//	@FieldAnnotation(orderIndex=12) 
//	public CheckBoxField chbQI464A_A; 
//	@FieldAnnotation(orderIndex=13) 
//	public CheckBoxField chbQI464A_B; 
//	@FieldAnnotation(orderIndex=14) 
//	public CheckBoxField chbQI464A_C; 
//	@FieldAnnotation(orderIndex=15) 
//	public CheckBoxField chbQI464A_X; 
//	@FieldAnnotation(orderIndex=16) 
//	public TextField txtQI464A_XI; 
	
	public CheckBoxField chb461f,chb461h,chb461j,chb462a;
	
//	public final static int CAMPANIA_2013 = App.ANIOPORDEFECTOSUPERIOR-3;
//	public final static int CAMPANIA_2014 = App.ANIOPORDEFECTOSUPERIOR-2;
//	public final static int CAMPANIA_2015 = App.ANIOPORDEFECTOSUPERIOR-1;
	
	CISECCION_04B c2seccion_04b; 
	CISECCION_02 c2seccion_2;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta461e,lblpregunta461f,lblpregunta462,
	lblpregunta462a,lblpregunta462b,lblpregunta463,lblpregunta463a,lblpregunta464,lblpregunta464a,lblvacunaveces,lblvacunaveces2,lblvacunaveces3,lblpregunta464a_ind,
	lblpregunta461g,lblpregunta461h,lblpregunta461i,lblpregunta461j,lblpregunta463b,lblvacunaveces4,lblvacunaveces5;
	
	public GridComponent2 gdVacuna461f,gdVacuna461h,gdVacuna461j,gdVacuna462a;
	public ButtonComponent btnNoSabeVacuna461d,btnNoSabeVacuna461f,btnNoSabeVacuna462a;
	LinearLayout q0; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10,q11,q12,q13,q14,q15,q16,q17; 
//	LinearLayout q11; 
//	LinearLayout q12; 
//	LinearLayout q13; 
	
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI461E","QI461F","QI461G","QI461H","QI461I","QI461J","QI462","QI462A","QI462B","QI463","QI463A","QI463B","QI464","QI464A_A","QI464A_B","QI464A_C","QI464A_X","QI464A_XI")}; 
	SeccionCapitulo[] seccionesCargado,seccionesCargado2; 
	String nombre_persona;
	public TextField txtCabecera;
	public C2SECCION_04BFragment_004() {} 
	public C2SECCION_04BFragment_004 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		 
		rango(getActivity(), txtQI461F, 1, 7); 
		rango(getActivity(), txtQI462A, 1, 7); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI461E","QI461F","QI461G","QI461H","QI461I","QI461J","QI462","QI462A","QI462B","QI463","QI463A","QI463B","QI464","QI464A_A","QI464A_B","QI464A_C","QI464A_X","QI464A_XI","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI215Y","ID","HOGAR_ID","PERSONA_ID","QI212")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    
	    lblpregunta461e = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461e);
	    lblpregunta461f = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461f);
	    lblpregunta461g = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461g);
	    lblpregunta461h = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461h);
	    lblpregunta461i = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461i);
	    lblpregunta461j = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461j);
	    lblpregunta462 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi462);
	    lblpregunta462a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi462a);
	    lblpregunta462b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi462b);
	    lblpregunta463 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi463);
	    lblpregunta463a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi463a);
	    lblpregunta463b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi463b);
	    lblpregunta464 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi464);
	    lblpregunta464a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi464a);
	    lblpregunta464a_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi464a_ind);
	    
	   
	
		
		rgQI461E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461e_1,R.string.c2seccion_04bqi461e_2,R.string.c2seccion_04bqi461e_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI461eChangeValue"); 
		txtQI461F=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		lblvacunaveces2  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi461f_1).textSize(16).centrar();
		
		chb461f=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi461f_2, "1:0").size(altoComponente, 200);
		chb461f.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb461f.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI461F);
			}
			else {
				Util.lockView(getActivity(),false, txtQI461F);	
			}
		}
		});
		
		gdVacuna461f = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna461f.addComponent(lblvacunaveces2);
		gdVacuna461f.addComponent(txtQI461F);
		gdVacuna461f.addComponent(chb461f);
		
		
		
		rgQI461G=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461e_1,R.string.c2seccion_04bqi461e_2,R.string.c2seccion_04bqi461e_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI461gChangeValue");
		txtQI461H=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		lblvacunaveces4  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi461f_1).textSize(16).centrar();
		
		chb461h=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi461f_2, "1:0").size(altoComponente, 200);
		chb461h.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb461h.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI461H);
			}
			else {
				Util.lockView(getActivity(),false, txtQI461H);	
			}
		}
		});
		
		gdVacuna461h = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna461h.addComponent(lblvacunaveces4);
		gdVacuna461h.addComponent(txtQI461H);
		gdVacuna461h.addComponent(chb461h);
		
		
		
		rgQI461I=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461e_1,R.string.c2seccion_04bqi461e_2,R.string.c2seccion_04bqi461e_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI461iChangeValue");
		txtQI461J=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		lblvacunaveces5  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi461f_1).textSize(16).centrar();
		
		chb461j=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi461f_2, "1:0").size(altoComponente, 200);
		chb461j.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb461j.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI461J);
			}
			else {
				Util.lockView(getActivity(),false, txtQI461J);	
			}
		}
		});
		
		gdVacuna461j = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna461j.addComponent(lblvacunaveces5);
		gdVacuna461j.addComponent(txtQI461J);
		gdVacuna461j.addComponent(chb461j);
		
		
		rgQI462=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi462_1,R.string.c2seccion_04bqi462_2,R.string.c2seccion_04bqi462_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI462ChangeValue"); 
		
		txtQI462A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		lblvacunaveces3  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi462a_1).textSize(16).centrar();
		
		chb462a=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi462a_2, "1:0").size(altoComponente, 200);
		chb462a.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb462a.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI462A);
			}
			else {
				Util.lockView(getActivity(),false, txtQI462A);	
			}
		}
		});		
		
		gdVacuna462a = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna462a.addComponent(lblvacunaveces3);
		gdVacuna462a.addComponent(txtQI462A);
		gdVacuna462a.addComponent(chb462a);
		
		
		rgQI462B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi462b_1,R.string.c2seccion_04bqi462b_2,R.string.c2seccion_04bqi462b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
  
	    
		rgQI463=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi463_1,R.string.c2seccion_04bqi463_2,R.string.c2seccion_04bqi463_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI463A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi463a_1,R.string.c2seccion_04bqi463a_2,R.string.c2seccion_04bqi463a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI463B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi463a_1,R.string.c2seccion_04bqi463a_2,R.string.c2seccion_04bqi463a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
//		rgQI464=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi464_1,R.string.c2seccion_04bqi464_2,R.string.c2seccion_04bqi464_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI464ChangeValue"); 
//		
//		
//		chbQI464A_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi464a_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbQI464A_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi464a_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbQI464A_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi464a_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
//		chbQI464A_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi464a_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI464axChangeValue");
//			
//		txtQI464A_XI=new TextField(this.getActivity()).size(altoComponente, 500).maxLength(100).soloTextoNumero(); 
		} 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo);  

		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461e,rgQI461E); 
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461f,gdVacuna461f.component());
		
		q12 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461g,rgQI461G); 
		q13 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461h,gdVacuna461h.component()); 
		q14 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461i,rgQI461I); 
		q15 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461j,gdVacuna461j.component());
		
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta462,rgQI462); 
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta462a,gdVacuna462a.component()); 
		q8 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta462b,rgQI462B); 
		q9 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta463,rgQI463); 
		q10 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta463a,rgQI463A);
		q11 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta463b,rgQI463B);
		
//		q11 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta464,rgQI464); 
		
//		LinearLayout ly464a = new LinearLayout(getActivity());
//		ly464a.addView(chbQI464A_X);
//		ly464a.addView(txtQI464A_XI);
//		q12 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta464a,lblpregunta464a_ind,chbQI464A_A,
//				chbQI464A_B,chbQI464A_C,ly464a); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q4); 
		form.addView(q5);
		form.addView(q12);
		form.addView(q13);
		form.addView(q14);
		form.addView(q15);
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
		form.addView(q9); 
		form.addView(q10); 
		form.addView(q11); 
//		form.addView(q12); 
		
    return contenedor; 
    } 
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_04b); 
		if(c2seccion_04b!=null){
			
			
			
			if (c2seccion_04b.qi461e!=null) {
				c2seccion_04b.qi461e=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461e);
			}
			if (c2seccion_04b.qi461g!=null) {
				c2seccion_04b.qi461g=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461g);
			}
			if (c2seccion_04b.qi461i!=null) {
				c2seccion_04b.qi461i=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461i);
			}
			
			if (c2seccion_04b.qi462!=null) {
				c2seccion_04b.qi462=c2seccion_04b.getConvertQi462(c2seccion_04b.qi462);
			}
			
			if (c2seccion_04b.qi462b!=null) {
				c2seccion_04b.qi462b=c2seccion_04b.getConvertQi462b(c2seccion_04b.qi462b);
			}
			
			if (c2seccion_04b.qi463!=null) {
				c2seccion_04b.qi463=c2seccion_04b.getConvertQi463(c2seccion_04b.qi463);
			}
			
			if (c2seccion_04b.qi463a!=null) {
				c2seccion_04b.qi463a=c2seccion_04b.getConvertQi463a(c2seccion_04b.qi463a);
			}
			if (c2seccion_04b.qi463b!=null) {
				c2seccion_04b.qi463b=c2seccion_04b.getConvertQi463a(c2seccion_04b.qi463b);
			}
			
//			if (c2seccion_04b.qi464!=null) {
//				c2seccion_04b.qi464=c2seccion_04b.getConvertQi464(c2seccion_04b.qi464);
//			}
		}
		
	
		
		if(chb461f.isChecked() ) {
			c2seccion_04b.qi461f=8;			
		}
		if(chb461h.isChecked() ) {
			c2seccion_04b.qi461h=8;			
		}
		if(chb461j.isChecked() ) {
			c2seccion_04b.qi461j=8;			
		}
		
		if(chb462a.isChecked() ) {
			c2seccion_04b.qi462a=8;			
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		
		if(App.getInstance().getSeccion04B().qi458==1) {
		
			
			if (Util.esVacio(c2seccion_04b.qi461e)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI461E"); 
				view = rgQI461E; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi461e==1) {
				if (Util.esVacio(c2seccion_04b.qi461f) && !chb461f.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461F"); 
					view = txtQI461F; 
					error = true; 
					return false; 
				} 
			}
			


			if (Util.esVacio(c2seccion_04b.qi461g)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI461G"); 
				view = rgQI461G; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi461g==1) {
				if (Util.esVacio(c2seccion_04b.qi461h) && !chb461h.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461H"); 
					view = txtQI461H; 
					error = true; 
					return false; 
				} 
			}
			
			
			
			if (Util.esVacio(c2seccion_04b.qi461i)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI461I"); 
				view = rgQI461I; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi461i==1) {
				if (Util.esVacio(c2seccion_04b.qi461j) && !chb461j.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461J"); 
					view = txtQI461J; 
					error = true; 
					return false; 
				} 
			}
			
			
			
			if (Util.esVacio(c2seccion_04b.qi462)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI462"); 
				view = rgQI462; 
				error = true; 
				return false; 
			} 
			
			if (c2seccion_04b.qi462==1) {
				if (Util.esVacio(c2seccion_04b.qi462a) && !chb462a.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI462A"); 
					view = txtQI462A; 
					error = true; 
					return false; 
				} 
			}
			if (Util.esVacio(c2seccion_04b.qi462b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI462B"); 
				view = rgQI462B; 
				error = true; 
				return false; 
			} 
			
			
			if (Util.esVacio(c2seccion_04b.qi463)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI463"); 
				view = rgQI463; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi463a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI463A"); 
				view = rgQI463A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi463b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI463B"); 
				view = rgQI463B; 
				error = true; 
				return false; 
			} 
		}
		
//		if (Util.esVacio(c2seccion_04b.qi464)) { 
//			mensaje = preguntaVacia.replace("$", "La pregunta QI464"); 
//			view = rgQI464; 
//			error = true; 
//			return false; 
//		} 
		/*if (Util.esVacio(c2seccion_04b.qi464a_a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI464A_A"); 
			view = txtQI464A_A; 
			error = true; 
			return false; 
		} */
		
//		if(c2seccion_04b.qi464==1) {
//		
//			if(!verificarCheck464a()) {
//				mensaje = "Seleccione una opci�n en P.464a"; 
//				view = chbQI464A_A; 
//				error = true; 
//				return false; 
//			}
//			
//			if(c2seccion_2.qi215y!=null ) {
//				  
//				 //Integer s464a = Integer.parseInt(c2seccion_04b.qi464a_xi);
//				 
//				 if(chbQI464A_A.isChecked()) {
//					 if(CAMPANIA_2013 < c2seccion_2.qi215y) {
//							mensaje = "El a�o seleccionado es menor que el a�o de nacimiento"; 
//							view = chbQI464A_A ; 
//							error = true; 
//							return false;
//							 
//						 }
//				 }
//				 
//				 if(chbQI464A_B.isChecked()) {
//					 if(CAMPANIA_2014 < c2seccion_2.qi215y) {
//							mensaje = "El a�o seleccionado es menor que el a�o de nacimiento"; 
//							view = chbQI464A_A ; 
//							error = true; 
//							return false;
//							 
//						 }
//				 }
//				 
//				 if(chbQI464A_C.isChecked()) {
//					 if(CAMPANIA_2015 < c2seccion_2.qi215y) {
//							mensaje = "El a�o seleccionado es menor que el a�o de nacimiento"; 
//							view = chbQI464A_A ; 
//							error = true; 
//							return false;
//							 
//						 }
//				 }
//				 
//				
//			}
//			
//			
//			if (chbQI464A_X.isChecked()) {
//				if (Util.esVacio(c2seccion_04b.qi464a_xi)) { 
//					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
//					view = txtQI464A_XI; 
//					error = true; 
//					return false; 
//				}
//				
//				
//			}	
//		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		
    	c2seccion_2 = getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado2);
    	
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi461e!=null) {
				c2seccion_04b.qi461e=c2seccion_04b.setConvertQi461e(c2seccion_04b.qi461e);
			}
			if (c2seccion_04b.qi461g!=null) {
				c2seccion_04b.qi461g=c2seccion_04b.setConvertQi461e(c2seccion_04b.qi461g);
			}
			if (c2seccion_04b.qi461i!=null) {
				c2seccion_04b.qi461i=c2seccion_04b.setConvertQi461e(c2seccion_04b.qi461i);
			}
			
			if (c2seccion_04b.qi462!=null) {
				c2seccion_04b.qi462=c2seccion_04b.setConvertQi462(c2seccion_04b.qi462);
			}
			
			if (c2seccion_04b.qi462b!=null) {
				c2seccion_04b.qi462b=c2seccion_04b.setConvertQi462b(c2seccion_04b.qi462b);
			}
			
			if (c2seccion_04b.qi463!=null) {
				c2seccion_04b.qi463=c2seccion_04b.setConvertQi463(c2seccion_04b.qi463);
			}
			
			if (c2seccion_04b.qi463a!=null) {
				c2seccion_04b.qi463a=c2seccion_04b.setConvertQi463a(c2seccion_04b.qi463a);
			}
			if (c2seccion_04b.qi463b!=null) {
				c2seccion_04b.qi463b=c2seccion_04b.setConvertQi463a(c2seccion_04b.qi463b);
			}
			
//			if (c2seccion_04b.qi464!=null) {
//				c2seccion_04b.qi464=c2seccion_04b.setConvertQi464(c2seccion_04b.qi464);
//			}
		}
		
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;				
		entityToUI(c2seccion_04b); 
		
	
		
		if(c2seccion_04b.qi461f!=null && c2seccion_04b.qi461f==8) {
			chb461f.setChecked(true);
			txtQI461F.setText("");
		}
		if(c2seccion_04b.qi461h!=null && c2seccion_04b.qi461h==8) {
			chb461h.setChecked(true);
			txtQI461H.setText("");
		}
		if(c2seccion_04b.qi461j!=null && c2seccion_04b.qi461j==8) {
			chb461j.setChecked(true);
			txtQI461J.setText("");
		}
		
		if(c2seccion_04b.qi462a!=null && c2seccion_04b.qi462a==8) {
			chb462a.setChecked(true);
			txtQI462A.setText("");
		}
		inicio(); 
    } 
    private void inicio() { 
    	
    	evaluar458();
    	
//    	if(c2seccion_04b.qi464!=null)
//    		onqrgQI464ChangeValue();
    	
//    	onqrgQI464axChangeValue();
    	renombrarEtiquetas();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluar458() {
    	if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi458!=1) {
    		Util.cleanAndLockView(getActivity(),rgQI461E,txtQI461F,rgQI461G,txtQI461H,rgQI461I,txtQI461J,chb461f,rgQI462,txtQI462A,chb462a,rgQI462B,rgQI463,rgQI463A,rgQI463B);
    		q0.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
    		q10.setVisibility(View.GONE);
    		q11.setVisibility(View.GONE);
    		q12.setVisibility(View.GONE);
    		q13.setVisibility(View.GONE);
    		q14.setVisibility(View.GONE);
    		q15.setVisibility(View.GONE);
    	}    	
    	else {
    		Util.lockView(getActivity(),false,rgQI461E,txtQI461F,rgQI461G,txtQI461H,rgQI461I,txtQI461J,chb461f,rgQI462,txtQI462A,chb462a,rgQI462B,rgQI463,rgQI463A,rgQI463B);
    		q0.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		q10.setVisibility(View.VISIBLE);
    		q11.setVisibility(View.VISIBLE);
    		q12.setVisibility(View.VISIBLE);
    		q13.setVisibility(View.VISIBLE);
    		q14.setVisibility(View.VISIBLE);
    		q15.setVisibility(View.VISIBLE);
    		
    		
        	if(c2seccion_04b.qi461e!=null)
        		onqrgQI461eChangeValue();
        	if(c2seccion_04b.qi461g!=null)
        		onqrgQI461gChangeValue();
        	if(c2seccion_04b.qi461i!=null)
        		onqrgQI461iChangeValue();
        	if(c2seccion_04b.qi462!=null)
        		onqrgQI462ChangeValue();
        	
        	
        	if (c2seccion_04b.qi461f!=null && c2seccion_04b.qi461f==8) { Util.cleanAndLockView(getActivity(),txtQI461F); } else{ Util.lockView(getActivity(),false,txtQI461F);}
        	if (c2seccion_04b.qi461h!=null && c2seccion_04b.qi461h==8) { Util.cleanAndLockView(getActivity(),txtQI461H); } else{ Util.lockView(getActivity(),false,txtQI461H);}
        	if (c2seccion_04b.qi461j!=null && c2seccion_04b.qi461j==8) { Util.cleanAndLockView(getActivity(),txtQI461J); } else{ Util.lockView(getActivity(),false,txtQI461J);}
        	if (c2seccion_04b.qi462a!=null && c2seccion_04b.qi462a==8) { Util.cleanAndLockView(getActivity(),txtQI462A); } else{ Util.lockView(getActivity(),false,txtQI462A);}
        	
    	}
    }
    
  
	
	
	public void onqrgQI461eChangeValue() {  
		if(rgQI461E.getValue()!=null){
			if (rgQI461E.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI461F,chb461f);  
	    		q5.setVisibility(View.VISIBLE);
	    		txtQI461F.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI461F,chb461f);
	    		q5.setVisibility(View.GONE);
	    		rgQI461G.requestFocus();
	    		MyUtil.LiberarMemoria();
	    		
	    	}	
		}
    }
	
	public void onqrgQI461gChangeValue() {  
		if(rgQI461G.getValue()!=null){
			if (rgQI461G.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI461H,chb461h);  
	    		q13.setVisibility(View.VISIBLE);
	    		txtQI461H.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI461H,chb461h);
	    		q13.setVisibility(View.GONE);
	    		rgQI461I.requestFocus();
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI461iChangeValue() {  
		if(rgQI461I.getValue()!=null){
			if (rgQI461I.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI461J,chb461j);  
	    		q15.setVisibility(View.VISIBLE);
	    		txtQI461J.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI461J,chb461j);
	    		q15.setVisibility(View.GONE);
	    		rgQI462.requestFocus();
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	public void onqrgQI462ChangeValue() {  	
		if(rgQI462.getValue()!=null) {
			if (rgQI462.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,txtQI462A,chb462a);  
	    		q7.setVisibility(View.VISIBLE);
	    		txtQI462A.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI462A,chb462a);
	    		q7.setVisibility(View.GONE);
	    		rgQI462B.requestFocus();
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
//	public void onqrgQI464ChangeValue() {  	
//		if(rgQI464.getValue()!=null) {
//			if (rgQI464.getValue().toString().equals("1")) {    		
//	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
//				Util.lockView(getActivity(),false,chbQI464A_A,chbQI464A_B,chbQI464A_C,chbQI464A_X,txtQI464A_XI);  
//	    		q12.setVisibility(View.VISIBLE);
//	    		onqrgQI464axChangeValue();
////	    		MyUtil.LiberarMemoria();
//	    	} 
//	    	else {	
//	    		Util.cleanAndLockView(getActivity(),chbQI464A_A,chbQI464A_B,chbQI464A_C,chbQI464A_X,txtQI464A_XI);
//	    		q12.setVisibility(View.GONE);
//	    		MyUtil.LiberarMemoria();
//	    	}	
//		}
//    }
	

	
	
	 
//    public void onqrgQI464axChangeValue() {  	
//    	if (chbQI464A_X.isChecked()){		
//  			Util.lockView(getActivity(),false,txtQI464A_XI);
//  			/*if (verificarCheck407()) {  			
//  				//Util.cleanAndLockView(getActivity(),chbQI408_X);
//  			}*/
//  			txtQI464A_XI.requestFocus();
//  		} 
//  		else {
//  			Util.cleanAndLockView(getActivity(),txtQI464A_XI);  
//  			/*if (!verificarCheck407()) {  			
//  				Util.lockView(getActivity(),false,chbQI407_Y);  				
//			}*/
//  		}	
//    }
	
//	public boolean verificarCheck464a() {
//  		if (chbQI464A_A.isChecked() || chbQI464A_B.isChecked() || chbQI464A_C.isChecked() || chbQI464A_X.isChecked()) {
//  			return true;
//  		}else{			
//  			return false;
//  		}
//  	}
	
	
	public void renombrarEtiquetas(){	
    	
    	lblpregunta461e.setText(lblpregunta461e.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta461g.setText(lblpregunta461g.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta461i.setText(lblpregunta461i.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta462.setText(lblpregunta462.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta462b.setText(lblpregunta462b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta463.setText(lblpregunta463.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta463a.setText(lblpregunta463a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta463b.setText(lblpregunta463b.getText().toString().replace("(NOMBRE)", nombre_persona));
//    	lblpregunta464.setText(lblpregunta464.getText().toString().replace("(NOMBRE)", nombre_persona));
//    	lblpregunta464a.setText(lblpregunta464a.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			
			rgQI461E.readOnly();
			rgQI462.readOnly();
			rgQI462B.readOnly();
			rgQI463.readOnly();
			rgQI463A.readOnly();
//			rgQI464.readOnly();
			
			chb461f.readOnly();
			chb462a.readOnly();
//			chbQI464A_A.readOnly();
//			chbQI464A_B.readOnly();
//			chbQI464A_C.readOnly();
//			chbQI464A_X.readOnly();
			
			txtQI461F.readOnly();
			txtQI462A.readOnly();
			
			rgQI461G.readOnly();
			txtQI461H.readOnly();
			rgQI461I.readOnly();
			txtQI461J.readOnly();
			rgQI463B.readOnly();
			
//			txtQI464A_XI.readOnly();
		}
	}
	
	  public CuestionarioService getCuestionarioService() { 
			if(cuestionarioService==null){ 
				cuestionarioService = CuestionarioService.getInstance(getActivity()); 
			} 
			return cuestionarioService; 
	  }
	  
		@Override
		public Integer grabadoParcial() {
			uiToEntity(c2seccion_04b); 
			if(c2seccion_04b!=null){
				
					
				
				if (c2seccion_04b.qi461e!=null) {
					c2seccion_04b.qi461e=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461e);
				}
				if (c2seccion_04b.qi461e!=null) {
					c2seccion_04b.qi461e=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461e);
				}
				if (c2seccion_04b.qi461g!=null) {
					c2seccion_04b.qi461g=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461g);
				}
				if (c2seccion_04b.qi461i!=null) {
					c2seccion_04b.qi461i=c2seccion_04b.getConvertQi461e(c2seccion_04b.qi461i);
				}
				
				if (c2seccion_04b.qi462!=null) {
					c2seccion_04b.qi462=c2seccion_04b.getConvertQi462(c2seccion_04b.qi462);
				}
				
				if (c2seccion_04b.qi462b!=null) {
					c2seccion_04b.qi462b=c2seccion_04b.getConvertQi462b(c2seccion_04b.qi462b);
				}
				
				if (c2seccion_04b.qi463!=null) {
					c2seccion_04b.qi463=c2seccion_04b.getConvertQi463(c2seccion_04b.qi463);
				}
				
				if (c2seccion_04b.qi463a!=null) {
					c2seccion_04b.qi463a=c2seccion_04b.getConvertQi463a(c2seccion_04b.qi463a);
				}
				if (c2seccion_04b.qi463b!=null) {
					c2seccion_04b.qi463b=c2seccion_04b.getConvertQi463a(c2seccion_04b.qi463b);
				}
				
//				if (c2seccion_04b.qi464!=null) {
//					c2seccion_04b.qi464=c2seccion_04b.getConvertQi464(c2seccion_04b.qi464);
//				}
			}
			
			
			
			if(chb461f.isChecked() ) {
				c2seccion_04b.qi461f=8;			
			}
			if(chb461h.isChecked() ) {
				c2seccion_04b.qi461h=8;			
			}
			if(chb461j.isChecked() ) {
				c2seccion_04b.qi461j=8;			
			}
			
			if(chb462a.isChecked() ) {
				c2seccion_04b.qi462a=8;			
			}
			
			try { 
				if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
					ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
					return App.NODEFINIDO; 
				} 
			} catch (SQLException e) { 
				ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			return App.INDIVIDUAL; 
		} 
} 
