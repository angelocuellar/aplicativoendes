package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.DecimalField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_010 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI465ED_CC_A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI465ED_CC_A1;
	@FieldAnnotation(orderIndex=3)
	public IntegerField txtQI465ED_CC_A1_D;
	@FieldAnnotation(orderIndex=4)
	public IntegerField txtQI465ED_CC_A1_M;
	@FieldAnnotation(orderIndex=5)
	public TextField txtQI465ED_CC_A1_F1;
	@FieldAnnotation(orderIndex=6)
	public TextField txtQI465ED_CC_A1_F2;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI465ED_CC_B; 
	@FieldAnnotation(orderIndex=8)
	public RadioGroupOtherField rgQI465ED_CC_B1; 
	@FieldAnnotation(orderIndex =9)
	public DecimalField txtQI465ED_CC_B1_1;
	@FieldAnnotation(orderIndex =10)
	public DecimalField txtQI465ED_CC_B1_2;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI465ED_CC_C; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI465ED_CC_D; 
	
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI465EF_1;
	@FieldAnnotation(orderIndex=14)
	public IntegerField txtQI465EF_1_D;
	@FieldAnnotation(orderIndex=15)
	public IntegerField txtQI465EF_1_M;
	@FieldAnnotation(orderIndex=16)
	public IntegerField txtQI465EF_1_A;
	@FieldAnnotation(orderIndex=17)
	public DecimalField txtQI465EF_1_R;
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI465EF_2;
	@FieldAnnotation(orderIndex=19)
	public IntegerField txtQI465EF_2_D;
	@FieldAnnotation(orderIndex=20)
	public IntegerField txtQI465EF_2_M;
	@FieldAnnotation(orderIndex=21)
	public IntegerField txtQI465EF_2_A;
	@FieldAnnotation(orderIndex=22)
	public DecimalField txtQI465EF_2_R;
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI465EF_3;
	@FieldAnnotation(orderIndex=24)
	public IntegerField txtQI465EF_3_D;
	@FieldAnnotation(orderIndex=25)
	public IntegerField txtQI465EF_3_M;
	@FieldAnnotation(orderIndex=26)
	public IntegerField txtQI465EF_3_A;
	@FieldAnnotation(orderIndex=27)
	public DecimalField txtQI465EF_3_R;
	@FieldAnnotation(orderIndex=28) 
	public CheckBoxField chbQI465EF_4;
	@FieldAnnotation(orderIndex=29)
	public IntegerField txtQI465EF_4_D;
	@FieldAnnotation(orderIndex=30)
	public IntegerField txtQI465EF_4_M;
	@FieldAnnotation(orderIndex=31)
	public IntegerField txtQI465EF_4_A;
	@FieldAnnotation(orderIndex=32)
	public DecimalField txtQI465EF_4_R;
	@FieldAnnotation(orderIndex=33) 
	public CheckBoxField chbQI465EF_7;
	@FieldAnnotation(orderIndex=34) 
	public CheckBoxField chbQI465EF_8;
	
	CISECCION_04B c2seccion_04b; 
	CISECCION_02 nacimiento;
	boolean val215=false;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465ed_cc,lblpregunta465ed_cc_A,lblpregunta465ed_cc_A_ind,lblpregunta465ed_cc_A_f1,lblpregunta465ed_cc_B,
						   lblpregunta465ed_cc_C,lblpregunta465ed_cc_D,lblpregunta465ed_cc_B_ind1,lblpregunta465ed_cc_B_ind2,lblpregunta465ef,
						   lblpregunta465ef_ind,lblpregunta465ef_0,lblpregunta465ef_d,lblpregunta465ef_m,lblpregunta465ef_a,lblpregunta465ef_r,
						   lblpregunta465ed_cc_C_ind1,lblpregunta465ed_cc_C_ind2,lblpregunta465ed_cc_A_f2; 
	LinearLayout q0,q1,q2,q3,q4,q5; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdLugarSalud,grid465EF;
	public C2SECCION_04BFragment_010() {} 
	public C2SECCION_04BFragment_010 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465ED_CC_A","QI465ED_CC_B","QI465ED_CC_C","QI465ED_CC_D","QI465ED_CC_A1", "QI465ED_CC_A1_D", "QI465ED_CC_A1_M", "QI465ED_CC_A1_F1", "QI465ED_CC_A1_F2", "QI465ED_CC_B1", "QI465ED_CC_B1_1", "QI465ED_CC_B1_2","QI465EF_1", "QI465EF_1_D", "QI465EF_1_M", "QI465EF_1_A", "QI465EF_1_R", "QI465EF_2", "QI465EF_2_D", "QI465EF_2_M", "QI465EF_2_A", "QI465EF_2_R", "QI465EF_3", "QI465EF_3_D", "QI465EF_3_M", "QI465EF_3_A", "QI465EF_3_R", "QI465EF_4", "QI465EF_4_D", "QI465EF_4_M", "QI465EF_4_A", "QI465EF_4_R", "QI465EF_7", "QI465EF_8", "ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465ED_CC_A","QI465ED_CC_B","QI465ED_CC_C","QI465ED_CC_D","QI465ED_CC_A1", "QI465ED_CC_A1_D", "QI465ED_CC_A1_M", "QI465ED_CC_A1_F1", "QI465ED_CC_A1_F2", "QI465ED_CC_B1", "QI465ED_CC_B1_1", "QI465ED_CC_B1_2","QI465EF_1", "QI465EF_1_D", "QI465EF_1_M", "QI465EF_1_A", "QI465EF_1_R", "QI465EF_2", "QI465EF_2_D", "QI465EF_2_M", "QI465EF_2_A", "QI465EF_2_R", "QI465EF_3", "QI465EF_3_D", "QI465EF_3_M", "QI465EF_3_A", "QI465EF_3_R", "QI465EF_4", "QI465EF_4_D", "QI465EF_4_M", "QI465EF_4_A", "QI465EF_4_R", "QI465EF_7", "QI465EF_8")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta465ed_cc = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465ed_cc);
	    lblpregunta465ed_cc_A  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_a).textSize(19);
	    lblpregunta465ed_cc_A_ind  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_a_ind2).textSize(16);
	    lblpregunta465ed_cc_A_f1  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_af1).textSize(16).centrar();
	    lblpregunta465ed_cc_A_f2  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_af2).textSize(16).centrar();
	    
	    lblpregunta465ed_cc_B  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_b).textSize(19);
	    lblpregunta465ed_cc_B_ind1  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_b_ind1).textSize(17);
	    lblpregunta465ed_cc_B_ind2  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_b_ind2).textSize(16);
	    lblpregunta465ed_cc_C  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_c).textSize(19);
	    lblpregunta465ed_cc_C_ind1  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_c_ind1).textSize(16).negrita();
	    lblpregunta465ed_cc_C_ind2  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_c_ind2).textSize(19);
	    lblpregunta465ed_cc_D  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ed_cc_d).textSize(19);
	    
	    
	    lblpregunta465ef  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ef).textSize(16);
	    lblpregunta465ef_ind  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04bqi465ef_ind).textSize(16);
	    lblpregunta465ef_0  = new LabelComponent(this.getActivity()).size(altoComponente, 100).textSize(16);
	    lblpregunta465ef_d  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04bqi465ef_d).textSize(16).negrita().centrar();
	    lblpregunta465ef_m  = new LabelComponent(this.getActivity()).size(altoComponente, 100).text(R.string.c2seccion_04bqi465ef_m).textSize(16).negrita().centrar();
	    lblpregunta465ef_a  = new LabelComponent(this.getActivity()).size(altoComponente, 120).text(R.string.c2seccion_04bqi465ef_a).textSize(16).negrita().centrar();
	    lblpregunta465ef_r  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04bqi465ef_r).textSize(16).negrita().centrar();
	    
	    
	    //rgQI465ED_CC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc1,R.string.c2seccion_04bqi465ed_cc2,R.string.c2seccion_04bqi465ed_cc3, R.string.c2seccion_04bqi465ed_cc8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	    rgQI465ED_CC_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc1,R.string.c2seccion_04bqi465ed_cc2,R.string.c2seccion_04bqi465ed_cc8).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQI465ED_CC_AChangeValue");
	    rgQI465ED_CC_A1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc_a1,R.string.c2seccion_04bqi465ed_cc_a2,R.string.c2seccion_04bqi465ed_cc_a3).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
	    txtQI465ED_CC_A1_D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	    txtQI465ED_CC_A1_M=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	    rgQI465ED_CC_A1.agregarEspecifique(0,txtQI465ED_CC_A1_D);
	    rgQI465ED_CC_A1.agregarEspecifique(1,txtQI465ED_CC_A1_M);
	    txtQI465ED_CC_A1_F1=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 400).centrar().alfanumerico();
	    txtQI465ED_CC_A1_F2=new TextField(this.getActivity()).maxLength(400).size(altoComponente, 400).centrar().alfanumerico();
	    
	    rgQI465ED_CC_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc1,R.string.c2seccion_04bqi465ed_cc2,R.string.c2seccion_04bqi465ed_cc8).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQI465ED_CC_BChangeValue");
	    rgQI465ED_CC_B1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc_b1,R.string.c2seccion_04bqi465ed_cc_b2,R.string.c2seccion_04bqi465ed_cc_b3).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	    txtQI465ED_CC_B1_1 =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1);
	    txtQI465ED_CC_B1_2 =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente, 100).decimales(1);
	    rgQI465ED_CC_B1.agregarEspecifique(0,txtQI465ED_CC_B1_1);
	    rgQI465ED_CC_B1.agregarEspecifique(1,txtQI465ED_CC_B1_2);
	    
	    rgQI465ED_CC_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc1,R.string.c2seccion_04bqi465ed_cc2,R.string.c2seccion_04bqi465ed_cc8).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQI465ED_CC_CChangeValue");
	    rgQI465ED_CC_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465ed_cc1,R.string.c2seccion_04bqi465ed_cc2,R.string.c2seccion_04bqi465ed_cc8).size(MATCH_PARENT, MATCH_PARENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		
	    chbQI465EF_1=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_1, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_1ChangeValue");
	    chbQI465EF_2=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_2, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_2ChangeValue");
	    chbQI465EF_3=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_3, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_3ChangeValue");
	    chbQI465EF_4=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_4, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_4ChangeValue");
	    chbQI465EF_7=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_7, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_7ChangeValue");
	    chbQI465EF_8=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ef_8, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ef_8ChangeValue");
	    txtQI465EF_1_D=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_2_D=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_3_D=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_4_D=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);	    
	    txtQI465EF_1_M=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_2_M=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_3_M=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);
	    txtQI465EF_4_M=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(2);	    
	    txtQI465EF_1_A=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(4);
	    txtQI465EF_2_A=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(4);
	    txtQI465EF_3_A=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(4);
	    txtQI465EF_4_A=new IntegerField(this.getActivity()).size(WRAP_CONTENT, WRAP_CONTENT).maxLength(4);	    
	    txtQI465EF_1_R =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente+10, WRAP_CONTENT).decimales(1);
	    txtQI465EF_2_R =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente+10, WRAP_CONTENT).decimales(1);
	    txtQI465EF_3_R =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente+10, WRAP_CONTENT).decimales(1);
	    txtQI465EF_4_R =new DecimalField(this.getActivity()).maxLength(4).size(altoComponente+10, WRAP_CONTENT).decimales(1);
	    
	    grid465EF=new GridComponent2(this.getActivity(),App.ESTILO,5,1);
	    grid465EF.addComponent(lblpregunta465ef_0);
	    grid465EF.addComponent(lblpregunta465ef_d);
	    grid465EF.addComponent(lblpregunta465ef_m);
	    grid465EF.addComponent(lblpregunta465ef_a);
	    grid465EF.addComponent(lblpregunta465ef_r);
	    grid465EF.addComponent(chbQI465EF_1);
	    grid465EF.addComponent(txtQI465EF_1_D);
	    grid465EF.addComponent(txtQI465EF_1_M);
	    grid465EF.addComponent(txtQI465EF_1_A);
	    grid465EF.addComponent(txtQI465EF_1_R);
	    grid465EF.addComponent(chbQI465EF_2);
	    grid465EF.addComponent(txtQI465EF_2_D);
	    grid465EF.addComponent(txtQI465EF_2_M);
	    grid465EF.addComponent(txtQI465EF_2_A);
	    grid465EF.addComponent(txtQI465EF_2_R);
	    grid465EF.addComponent(chbQI465EF_3);
	    grid465EF.addComponent(txtQI465EF_3_D);
	    grid465EF.addComponent(txtQI465EF_3_M);
	    grid465EF.addComponent(txtQI465EF_3_A);
	    grid465EF.addComponent(txtQI465EF_3_R);
	    grid465EF.addComponent(chbQI465EF_4);
	    grid465EF.addComponent(txtQI465EF_4_D);
	    grid465EF.addComponent(txtQI465EF_4_M);
	    grid465EF.addComponent(txtQI465EF_4_A);
	    grid465EF.addComponent(txtQI465EF_4_R);
	    grid465EF.addComponent(chbQI465EF_7,5);
	    grid465EF.addComponent(chbQI465EF_8,5);
	    
//		grid465ED_CC=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
//		grid465ED_CC.addComponent(lblpregunta465ed_cc_A);
//		grid465ED_CC.addComponent(rgQI465ED_CC_A);
//		grid465ED_CC.addComponent(rgQI465ED_CC_A1,2);
//		grid465ED_CC.addComponent(lblpregunta465ed_cc_B);
//		grid465ED_CC.addComponent(rgQI465ED_CC_B);
//		grid465ED_CC.addComponent(lblpregunta465ed_cc_C);
//		grid465ED_CC.addComponent(rgQI465ED_CC_C);
//		grid465ED_CC.addComponent(lblpregunta465ed_cc_D);
//		grid465ED_CC.addComponent(rgQI465ED_CC_D);
	    

	    txtQI465EF_1_D.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQI465EF_1_M,txtQI465EF_1_A);
			    	}
			    	else{
			    		Util.lockView(getActivity(), false,txtQI465EF_1_M,txtQI465EF_1_A);
			    	}
				}
			}
		});
	    txtQI465EF_2_D.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQI465EF_2_M,txtQI465EF_2_A);
			    	}
			    	else{
			    		Util.lockView(getActivity(), false,txtQI465EF_2_M,txtQI465EF_2_A);
			    	}
				}
			}
		});
	    txtQI465EF_3_D.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQI465EF_3_M,txtQI465EF_3_A);
			    	}
			    	else{
			    		Util.lockView(getActivity(), false,txtQI465EF_3_M,txtQI465EF_3_A);
			    	}
				}
			}
		});
	    txtQI465EF_4_D.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQI465EF_4_M,txtQI465EF_4_A);
			    	}
			    	else{
			    		Util.lockView(getActivity(), false,txtQI465EF_4_M,txtQI465EF_4_A);
			    	}
				}
			}
		});
		
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ed_cc,lblpregunta465ed_cc_A,rgQI465ED_CC_A,lblpregunta465ed_cc_A_ind,rgQI465ED_CC_A1,txtQI465ED_CC_A1_F1,lblpregunta465ed_cc_A_f1, txtQI465ED_CC_A1_F2, lblpregunta465ed_cc_A_f2);
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ed_cc_B,rgQI465ED_CC_B,lblpregunta465ed_cc_B_ind1,lblpregunta465ed_cc_B_ind2,rgQI465ED_CC_B1);
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ed_cc_C,lblpregunta465ed_cc_C_ind1,lblpregunta465ed_cc_C_ind2,rgQI465ED_CC_C);
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ed_cc_D,rgQI465ED_CC_D);
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ef,lblpregunta465ef_ind, grid465EF.component());
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		if(c2seccion_04b!=null){			
			if (c2seccion_04b.qi465ed_cc_a!=null) {
				c2seccion_04b.qi465ed_cc_a=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_a);
			}
			if (c2seccion_04b.qi465ed_cc_b!=null) {
				c2seccion_04b.qi465ed_cc_b=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_b);
			}	
			if (c2seccion_04b.qi465ed_cc_c!=null) {
				c2seccion_04b.qi465ed_cc_c=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_c);
			}	
			if (c2seccion_04b.qi465ed_cc_d!=null) {
				c2seccion_04b.qi465ed_cc_d=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_d);
			}	
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
	private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
//		if (nacimiento.qi216==1 && val215==true && nacimiento.qi218==1) {	
			if (Util.esVacio(c2seccion_04b.qi465ed_cc_a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_A"); 
				view = rgQI465ED_CC_A; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_a,1)) { 
				if (Util.esVacio(c2seccion_04b.qi465ed_cc_a1)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_A1"); 
					view = rgQI465ED_CC_A1; 
					error = true; 
					return false; 
				}
				if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_a1,1)) {
					if (Util.esVacio(c2seccion_04b.qi465ed_cc_a1_d)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_A1_D"); 
						view = txtQI465ED_CC_A1_D; 
						error = true; 
						return false; 
					}
				}
				if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_a1,2)) {
					if (Util.esVacio(c2seccion_04b.qi465ed_cc_a1_m)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_A1_M"); 
						view = txtQI465ED_CC_A1_M; 
						error = true; 
						return false; 
					}
				}
				
				
				if (Util.esVacio(c2seccion_04b.qi465ed_cc_b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_B"); 
					view = rgQI465ED_CC_B; 
					error = true; 
					return false; 
				}				
				if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_b,1) || !Util.esDiferente(c2seccion_04b.qi465ed_cc_b,2)) {
					if (Util.esVacio(c2seccion_04b.qi465ed_cc_b1)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_B1"); 
						view = rgQI465ED_CC_B1; 
						error = true; 
						return false; 
					}
					if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_b1,1)) {
						if (Util.esVacio(c2seccion_04b.qi465ed_cc_b1_1)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_B1_1"); 
							view = txtQI465ED_CC_B1_1; 
							error = true; 
							return false; 
						}
					}
					if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_b1,2)) {
						if (Util.esVacio(c2seccion_04b.qi465ed_cc_b1_2)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_B1_2"); 
							view = txtQI465ED_CC_B1_2; 
							error = true; 
							return false; 
						}
					}
				}
				
				if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_b,1)){					
					if (Util.esVacio(c2seccion_04b.qi465ed_cc_c)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_C"); 
						view = rgQI465ED_CC_C; 
						error = true; 
						return false; 
					}
				
					if (!Util.esDiferente(c2seccion_04b.qi465ed_cc_c,1)) {
						if (Util.esVacio(c2seccion_04b.qi465ed_cc_d)) { 
							mensaje = preguntaVacia.replace("$", "La pregunta QI46ED_CC_D"); 
							view = rgQI465ED_CC_D; 
							error = true; 
							return false; 
						}
					}
				}
			}
			
			
			
			if(!verificarCheckP465ef()) {
				mensaje = "Seleccione una opci�n en la pregunta QI465EF"; 
				view = chbQI465EF_1; 
				error = true; 
				return false; 
			}
			
			if(chbQI465EF_1.isChecked()) {
				if (Util.esVacio(txtQI465EF_1_D.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta DIA1"); 
					view = txtQI465EF_1_D; 
					error = true; 
					return false; 
				} 				
				int id1 = txtQI465EF_1_D.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_1_D.getText().toString());
				if (!(id1>=1 && id1<=31) && id1!=98 && id1!=44) {
					error = true;
					view = txtQI465EF_1_D;
					mensaje = "Valor del campo invalido";
					return false;		
				}
				if(id1!=44){
					if (Util.esVacio(txtQI465EF_1_M.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta MES1"); 
						view = txtQI465EF_1_M; 
						error = true; 
						return false; 
					} 
					int im1 = txtQI465EF_1_M.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_1_M.getText().toString());
					if (!(im1>=1 && im1<=12)) {
						error = true;
						view = txtQI465EF_1_M;
						mensaje = "Valor del campo invalido";
						return false;
					}
					
					if (Util.esVacio(txtQI465EF_1_A.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta A�O1"); 
						view = txtQI465EF_1_A; 
						error = true; 
						return false; 
					} 
					if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQI465EF_1_D.getText().toString()), Integer.parseInt(txtQI465EF_1_M.getText().toString())) && id1!=98 ){
						mensaje = "d�a no corresponde al mes"; 
						view = txtQI465EF_1_D; 
						error = true; 
						return false; 
					}
					Calendar cal2 = Calendar.getInstance();
					Integer anio = Integer.parseInt(txtQI465EF_1_A.getText().toString());
					if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
						error = true;
						view = txtQI465EF_1_A;
						mensaje = "Valor del campo fuera de rango";
						return false;
					}
					if (Util.esVacio(c2seccion_04b.qi465ef_1_r)) { 
						validarMensaje("Verifique el campo Resultado 1");
					} 					
				}		
			}
			if(chbQI465EF_2.isChecked()) {
				if (Util.esVacio(txtQI465EF_2_D.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta DIA2"); 
					view = txtQI465EF_2_D; 
					error = true; 
					return false; 
				} 				
				int id1 = txtQI465EF_2_D.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_2_D.getText().toString());
				if (!(id1>=1 && id1<=31) && id1!=98 && id1!=44) {
					error = true;
					view = txtQI465EF_2_D;
					mensaje = "Valor del campo invalido";
					return false;		
				}
				if(id1!=44){
					if (Util.esVacio(txtQI465EF_2_M.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta MES2"); 
						view = txtQI465EF_2_M; 
						error = true; 
						return false; 
					} 
					int im1 = txtQI465EF_2_M.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_2_M.getText().toString());
					if (!(im1>=1 && im1<=12)) {
						error = true;
						view = txtQI465EF_2_M;
						mensaje = "Valor del campo invalido";
						return false;
					}
					
					if (Util.esVacio(txtQI465EF_2_A.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta A�O2"); 
						view = txtQI465EF_2_A; 
						error = true; 
						return false; 
					} 
					if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQI465EF_2_D.getText().toString()), Integer.parseInt(txtQI465EF_2_M.getText().toString())) && id1!=98 ){
						mensaje = "d�a no corresponde al mes"; 
						view = txtQI465EF_2_D; 
						error = true; 
						return false; 
					}
					Calendar cal2 = Calendar.getInstance();
					Integer anio = Integer.parseInt(txtQI465EF_2_A.getText().toString());
					if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
						error = true;
						view = txtQI465EF_2_A;
						mensaje = "Valor del campo fuera de rango";
						return false;
					}
					if (Util.esVacio(c2seccion_04b.qi465ef_2_r)) { 
						validarMensaje("Verifique el campo Resultado 2");
					} 					
				}		
			}
			if(chbQI465EF_3.isChecked()) {
				if (Util.esVacio(txtQI465EF_3_D.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta DIA3"); 
					view = txtQI465EF_3_D; 
					error = true; 
					return false; 
				} 				
				int id1 = txtQI465EF_3_D.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_3_D.getText().toString());
				if (!(id1>=1 && id1<=31) && id1!=98 && id1!=44) {
					error = true;
					view = txtQI465EF_3_D;
					mensaje = "Valor del campo invalido";
					return false;		
				}
				if(id1!=44){
					if (Util.esVacio(txtQI465EF_3_M.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta MES3"); 
						view = txtQI465EF_3_M; 
						error = true; 
						return false; 
					} 
					int im1 = txtQI465EF_3_M.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_3_M.getText().toString());
					if (!(im1>=1 && im1<=12)) {
						error = true;
						view = txtQI465EF_3_M;
						mensaje = "Valor del campo invalido";
						return false;
					}
					
					if (Util.esVacio(txtQI465EF_3_A.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta A�O3"); 
						view = txtQI465EF_3_A; 
						error = true; 
						return false; 
					} 
					if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQI465EF_3_D.getText().toString()), Integer.parseInt(txtQI465EF_3_M.getText().toString())) && id1!=98 ){
						mensaje = "d�a no corresponde al mes"; 
						view = txtQI465EF_3_D; 
						error = true; 
						return false; 
					}
					Calendar cal2 = Calendar.getInstance();
					Integer anio = Integer.parseInt(txtQI465EF_3_A.getText().toString());
					if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
						error = true;
						view = txtQI465EF_3_A;
						mensaje = "Valor del campo fuera de rango";
						return false;
					}
					if (Util.esVacio(c2seccion_04b.qi465ef_3_r)) { 
						validarMensaje("Verifique el campo Resultado 3");
					} 					
				}		
			}
			if(chbQI465EF_4.isChecked()) {
				if (Util.esVacio(txtQI465EF_4_D.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta DIA4"); 
					view = txtQI465EF_4_D; 
					error = true; 
					return false; 
				} 				
				int id1 = txtQI465EF_4_D.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_4_D.getText().toString());
				if (!(id1>=1 && id1<=31) && id1!=98 && id1!=44) {
					error = true;
					view = txtQI465EF_4_D;
					mensaje = "Valor del campo invalido";
					return false;		
				}
				if(id1!=44){
					if (Util.esVacio(txtQI465EF_4_M.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta MES4"); 
						view = txtQI465EF_4_M; 
						error = true; 
						return false; 
					} 
					int im1 = txtQI465EF_4_M.getText().toString() == null ? 0 : Integer.parseInt(txtQI465EF_4_M.getText().toString());
					if (!(im1>=1 && im1<=12)) {
						error = true;
						view = txtQI465EF_4_M;
						mensaje = "Valor del campo invalido";
						return false;
					}
					
					if (Util.esVacio(txtQI465EF_4_A.getText().toString())) { 
						mensaje = preguntaVacia.replace("$", "La pregunta A�O4"); 
						view = txtQI465EF_4_A; 
						error = true; 
						return false; 
					} 
					if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQI465EF_4_D.getText().toString()), Integer.parseInt(txtQI465EF_4_M.getText().toString())) && id1!=98 ){
						mensaje = "d�a no corresponde al mes"; 
						view = txtQI465EF_4_D; 
						error = true; 
						return false; 
					}
					Calendar cal2 = Calendar.getInstance();
					Integer anio = Integer.parseInt(txtQI465EF_4_A.getText().toString());
					if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
						error = true;
						view = txtQI465EF_4_A;
						mensaje = "Valor del campo fuera de rango";
						return false;
					}
					if (Util.esVacio(c2seccion_04b.qi465ef_4_r)) { 
						validarMensaje("Verifique el campo Resultado 4");						 
					} 					
				}		
			}
			
//		}
		return true; 
	}
    
 
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){			
			if (c2seccion_04b.qi465ed_cc_a!=null) {
				c2seccion_04b.qi465ed_cc_a=c2seccion_04b.setConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_a);
			}
			if (c2seccion_04b.qi465ed_cc_b!=null) {
				c2seccion_04b.qi465ed_cc_b=c2seccion_04b.setConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_b);
			}
			if (c2seccion_04b.qi465ed_cc_c!=null) {
				c2seccion_04b.qi465ed_cc_c=c2seccion_04b.setConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_c);
			}
			if (c2seccion_04b.qi465ed_cc_d!=null) {
				c2seccion_04b.qi465ed_cc_d=c2seccion_04b.setConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_d);
			}
		}

		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		
	   	SeccionCapitulo[] seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215Y","QI215M","QI215D","QI216","QI217CONS","QI218") };
		nacimiento =  getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesPersonas);
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
		Calendar fecharef = new GregorianCalendar();
			
		if(nacimiento!=null && nacimiento.qi216==1) {
			String fechareferencia = nacimiento.qi217cons;
			if(fechareferencia!=null){
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date2 = null;
				try {
					date2 = df.parse(fechareferencia);
				} catch (ParseException e) {
					e.printStackTrace();
				}		
				Calendar cal = Calendar.getInstance();
				cal.setTime(date2);
				fecharef=cal;
			}			
		}		
					
		if(MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef) >= 4) {
			val215= true;
		}
			
		entityToUI(c2seccion_04b); 
		inicio(); 
    }
    
    
    private void inicio() { 
    	renombrarEtiquetas();
    	onqrgQI465ED_CC_AChangeValue();
//    	onqrgQI465EDChangeValue();
    	ejecutaAccionCheckbox();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    private void renombrarEtiquetas(){
    	lblpregunta465ed_cc_A.text(R.string.c2seccion_04bqi465ed_cc_a);
    	lblpregunta465ed_cc_A.setText(lblpregunta465ed_cc_A.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
    private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }
    
    public void ejecutaAccionCheckbox() {
		if(chbQI465EF_8.isChecked()){
			onqrgQI465ef_8ChangeValue();
		}
		else{
			if(chbQI465EF_7.isChecked()){
				onqrgQI465ef_7ChangeValue();
			}
			else{
				onqrgQI465ef_1ChangeValue();
		    	onqrgQI465ef_2ChangeValue();
		    	onqrgQI465ef_3ChangeValue();
		    	onqrgQI465ef_4ChangeValue();
			}
		}
	}
    
    public boolean verificarCheckP465ef() {
  		if (chbQI465EF_1.isChecked()||chbQI465EF_2.isChecked()||chbQI465EF_3.isChecked()||chbQI465EF_4.isChecked()||chbQI465EF_7.isChecked()||chbQI465EF_8.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    
    public void onqrgQI465ef_1ChangeValue() {
    	if (verificarCheckP465ef()){Util.cleanAndLockView(getActivity(),chbQI465EF_7,chbQI465EF_8);}else{Util.lockView(getActivity(),false,chbQI465EF_7,chbQI465EF_8);}
    	if (chbQI465EF_1.isChecked()){
  			Util.lockView(getActivity(),false,txtQI465EF_1_D,txtQI465EF_1_M,txtQI465EF_1_A,txtQI465EF_1_R);
  			txtQI465EF_1_D.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI465EF_1_D,txtQI465EF_1_M,txtQI465EF_1_A,txtQI465EF_1_R);  
  		}	
    }
    public void onqrgQI465ef_2ChangeValue() {
    	if (verificarCheckP465ef()){Util.cleanAndLockView(getActivity(),chbQI465EF_7,chbQI465EF_8);}else{Util.lockView(getActivity(),false,chbQI465EF_7,chbQI465EF_8);}
    	if (chbQI465EF_2.isChecked()){
  			Util.lockView(getActivity(),false,txtQI465EF_2_D,txtQI465EF_2_M,txtQI465EF_2_A,txtQI465EF_2_R);
  			txtQI465EF_2_D.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI465EF_2_D,txtQI465EF_2_M,txtQI465EF_2_A,txtQI465EF_2_R);  
  		}	
    }
    public void onqrgQI465ef_3ChangeValue() {
    	if (verificarCheckP465ef()){Util.cleanAndLockView(getActivity(),chbQI465EF_7,chbQI465EF_8);}else{Util.lockView(getActivity(),false,chbQI465EF_7,chbQI465EF_8);}
    	if (chbQI465EF_3.isChecked()){
  			Util.lockView(getActivity(),false,txtQI465EF_3_D,txtQI465EF_3_M,txtQI465EF_3_A,txtQI465EF_3_R);
  			txtQI465EF_3_D.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI465EF_3_D,txtQI465EF_3_M,txtQI465EF_3_A,txtQI465EF_3_R);  
  		}	
    }
    public void onqrgQI465ef_4ChangeValue() {
    	if (verificarCheckP465ef()){Util.cleanAndLockView(getActivity(),chbQI465EF_7,chbQI465EF_8);}else{Util.lockView(getActivity(),false,chbQI465EF_7,chbQI465EF_8);}
    	if (chbQI465EF_4.isChecked()){
  			Util.lockView(getActivity(),false,txtQI465EF_4_D,txtQI465EF_4_M,txtQI465EF_4_A,txtQI465EF_4_R);
  			txtQI465EF_4_D.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI465EF_4_D,txtQI465EF_4_M,txtQI465EF_4_A,txtQI465EF_4_R);  
  		}	
    }
    public void onqrgQI465ef_7ChangeValue() {  	
    	if (chbQI465EF_7.isChecked()){		
  			Util.cleanAndLockView(getActivity(), chbQI465EF_1,chbQI465EF_2,chbQI465EF_3,chbQI465EF_4,chbQI465EF_8,txtQI465EF_1_D,txtQI465EF_1_M,txtQI465EF_1_A,txtQI465EF_1_R,txtQI465EF_2_D,txtQI465EF_2_M,txtQI465EF_2_A,txtQI465EF_2_R,txtQI465EF_3_D,txtQI465EF_3_M,txtQI465EF_3_A,txtQI465EF_3_R,txtQI465EF_4_D,txtQI465EF_4_M,txtQI465EF_4_A,txtQI465EF_4_R);
    	} 
  		else {
  			Util.lockView(getActivity(),false, chbQI465EF_1,chbQI465EF_2,chbQI465EF_3,chbQI465EF_4,chbQI465EF_8);
  		}	
    }
    public void onqrgQI465ef_8ChangeValue() {  	
    	if (chbQI465EF_8.isChecked()){		
  			Util.cleanAndLockView(getActivity(), chbQI465EF_1,chbQI465EF_2,chbQI465EF_3,chbQI465EF_4,chbQI465EF_7,txtQI465EF_1_D,txtQI465EF_1_M,txtQI465EF_1_A,txtQI465EF_1_R,txtQI465EF_2_D,txtQI465EF_2_M,txtQI465EF_2_A,txtQI465EF_2_R,txtQI465EF_3_D,txtQI465EF_3_M,txtQI465EF_3_A,txtQI465EF_3_R,txtQI465EF_4_D,txtQI465EF_4_M,txtQI465EF_4_A,txtQI465EF_4_R);
    	} 
  		else {
  			Util.lockView(getActivity(),false, chbQI465EF_1,chbQI465EF_2,chbQI465EF_3,chbQI465EF_4,chbQI465EF_7);
  		}	
    }
    
    
 
//    public void onqrgQI465EDChangeValue() {    	
//    	if (nacimiento.qi216==1 && val215==true && nacimiento.qi218==1) {
//    		Util.lockView(getActivity(),false,rgQI465ED_CC_A,rgQI465ED_CC_B,rgQI465ED_CC_C,rgQI465ED_CC_D);  
//			q1.setVisibility(View.VISIBLE);
////			onqrgQI465ED_CC_AChangeValue();
//		}
//    	else{
//    		Util.cleanAndLockView(getActivity(),rgQI465ED_CC_A,rgQI465ED_CC_B,rgQI465ED_CC_C,rgQI465ED_CC_D);
//    		q1.setVisibility(View.GONE);
//    	}
//    }
    
    public void onqrgQI465ED_CC_AChangeValue() {
    	Integer qi465ED_CC_A=Integer.parseInt(rgQI465ED_CC_A.getTagSelected("0").toString());
//    	if (MyUtil.incluyeRango(2,8,rgQI465ED_CC_A.getTagSelected("").toString())) {
    	if (!Util.esDiferente(qi465ED_CC_A, 2,3,8)) {
			Util.cleanAndLockView(getActivity(),rgQI465ED_CC_A1, txtQI465ED_CC_A1_D, txtQI465ED_CC_A1_M, txtQI465ED_CC_A1_F1,txtQI465ED_CC_A1_F2, rgQI465ED_CC_B,rgQI465ED_CC_B1, txtQI465ED_CC_B1_1, txtQI465ED_CC_B1_2,rgQI465ED_CC_C,rgQI465ED_CC_D);
			MyUtil.LiberarMemoria();
			
			lblpregunta465ed_cc_A_ind.setVisibility(View.GONE);
			rgQI465ED_CC_A1.setVisibility(View.GONE);
			txtQI465ED_CC_A1_F1.setVisibility(View.GONE);
			txtQI465ED_CC_A1_F2.setVisibility(View.GONE);
			lblpregunta465ed_cc_A_f1.setVisibility(View.GONE);
			lblpregunta465ed_cc_A_f2.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			chbQI465EF_1.requestFocus();
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI465ED_CC_A1, txtQI465ED_CC_A1_F1,txtQI465ED_CC_A1_F2, rgQI465ED_CC_B, rgQI465ED_CC_B1, rgQI465ED_CC_C,rgQI465ED_CC_D);	
    		lblpregunta465ed_cc_A_ind.setVisibility(View.VISIBLE);
			rgQI465ED_CC_A1.setVisibility(View.VISIBLE);
			txtQI465ED_CC_A1_F1.setVisibility(View.VISIBLE);
			txtQI465ED_CC_A1_F2.setVisibility(View.VISIBLE);
			lblpregunta465ed_cc_A_f1.setVisibility(View.VISIBLE);
			lblpregunta465ed_cc_A_f2.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
    		onqrgQI465ED_CC_BChangeValue();
    		rgQI465ED_CC_A1.requestFocus();
    	}
    }
    
    
    public void onqrgQI465ED_CC_BChangeValue() {
    	Integer qi465ED_CC_B=Integer.parseInt(rgQI465ED_CC_B.getTagSelected("0").toString());
    	if (!Util.esDiferente(qi465ED_CC_B, 1)) {
    		Util.lockView(getActivity(), false,rgQI465ED_CC_B1, rgQI465ED_CC_C,rgQI465ED_CC_D);
    		lblpregunta465ed_cc_B_ind1.setVisibility(View.VISIBLE);
			lblpregunta465ed_cc_B_ind2.setVisibility(View.VISIBLE);
			rgQI465ED_CC_B1.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		onqrgQI465ED_CC_CChangeValue();
    		rgQI465ED_CC_B1.requestFocus();
    	}
    	
    	if (!Util.esDiferente(qi465ED_CC_B, 2)) {
			Util.cleanAndLockView(getActivity(),rgQI465ED_CC_C,rgQI465ED_CC_D);
			Util.lockView(getActivity(), false,rgQI465ED_CC_B1);
			MyUtil.LiberarMemoria();	
			
			lblpregunta465ed_cc_B_ind1.setVisibility(View.VISIBLE);
			lblpregunta465ed_cc_B_ind2.setVisibility(View.VISIBLE);
			rgQI465ED_CC_B1.setVisibility(View.VISIBLE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			rgQI465ED_CC_B1.requestFocus();
    	}
    	if (!Util.esDiferente(qi465ED_CC_B, 3,8)) {
			Util.cleanAndLockView(getActivity(), rgQI465ED_CC_B1, txtQI465ED_CC_B1_1, txtQI465ED_CC_B1_2, rgQI465ED_CC_C,rgQI465ED_CC_D);
			MyUtil.LiberarMemoria();		
			
			lblpregunta465ed_cc_B_ind1.setVisibility(View.GONE);
			lblpregunta465ed_cc_B_ind2.setVisibility(View.GONE);
			rgQI465ED_CC_B1.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			chbQI465EF_1.requestFocus();
    	}
    	
    }
    
    public void onqrgQI465ED_CC_CChangeValue() {
    	Integer qi465ED_CC_C=Integer.parseInt(rgQI465ED_CC_C.getTagSelected("0").toString());
//    	if (MyUtil.incluyeRango(2,8,rgQI465ED_CC_C.getTagSelected("").toString())) {
    	if (!Util.esDiferente(qi465ED_CC_C, 2,3,8)) {
			Util.cleanAndLockView(getActivity(),rgQI465ED_CC_D);
			MyUtil.LiberarMemoria();
			q4.setVisibility(View.GONE);
			chbQI465EF_1.requestFocus();
    	}
    	else{
    		Util.lockView(getActivity(), false,rgQI465ED_CC_D);
    		q4.setVisibility(View.VISIBLE);
    		rgQI465ED_CC_D.requestFocus();
    	}
    }
	
//	public void renombrarEtiquetas(){	
//		lblpregunta465ed_cc.setText(lblpregunta465ed_cc.getText().toString().replace("(NOMBRE)", nombre_persona));
//    	lblpregunta465ed_cc_C.setText("c. �Le indicaron tratamiento con hierro?");
//    	Spanned texto465ed_cc_c =Html.fromHtml(lblpregunta465ed_cc_C.getText()+ "<br><b>SI DICE 'NO' SONDEE</b>�Qu� indicaci�n le dieron?");
//    	lblpregunta465ed_cc_C.setText(texto465ed_cc_c);
//    	
//    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI465ED_CC_A.readOnly();
			rgQI465ED_CC_B.readOnly();
			rgQI465ED_CC_C.readOnly();
			rgQI465ED_CC_D.readOnly();
			
			rgQI465ED_CC_A1.readOnly();
			txtQI465ED_CC_A1_D.readOnly();
			txtQI465ED_CC_A1_M.readOnly();
			txtQI465ED_CC_A1_F1.readOnly();
			txtQI465ED_CC_A1_F2.readOnly();
			rgQI465ED_CC_B1.readOnly();
			txtQI465ED_CC_B1_1.readOnly();
			txtQI465ED_CC_B1_2.readOnly();
			chbQI465EF_1.readOnly();
			txtQI465EF_1_D.readOnly();
			txtQI465EF_1_M.readOnly();
			txtQI465EF_1_A.readOnly();
			txtQI465EF_1_R.readOnly();
			chbQI465EF_2.readOnly();
			txtQI465EF_2_D.readOnly();
			txtQI465EF_2_M.readOnly();
			txtQI465EF_2_A.readOnly();
			txtQI465EF_2_R.readOnly();
			chbQI465EF_3.readOnly();
			txtQI465EF_3_D.readOnly();
			txtQI465EF_3_M.readOnly();
			txtQI465EF_3_A.readOnly();
			txtQI465EF_3_R.readOnly();
			chbQI465EF_4.readOnly();
			txtQI465EF_4_D.readOnly();
			txtQI465EF_4_M.readOnly();
			txtQI465EF_4_A.readOnly();
			txtQI465EF_4_R.readOnly();
			chbQI465EF_7.readOnly();
			chbQI465EF_8.readOnly();
		}
	}

 
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){	
			if (c2seccion_04b.qi465ed_cc_a!=null) {
				c2seccion_04b.qi465ed_cc_a=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_a);
			}
			if (c2seccion_04b.qi465ed_cc_b!=null) {
				c2seccion_04b.qi465ed_cc_b=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_b);
			}	
			if (c2seccion_04b.qi465ed_cc_c!=null) {
				c2seccion_04b.qi465ed_cc_c=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_c);
			}	
			if (c2seccion_04b.qi465ed_cc_d!=null) {
				c2seccion_04b.qi465ed_cc_d=c2seccion_04b.getConvertQi465ed_cc(c2seccion_04b.qi465ed_cc_d);
			}
		}

		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
