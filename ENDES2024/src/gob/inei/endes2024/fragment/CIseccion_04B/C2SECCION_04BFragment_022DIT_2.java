package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_2_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_2 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478F1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478F2_A; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478F2_B;
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478F2_C;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478F2_D;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478F2_E;
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478F3; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478F4; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478F5; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI478F6; 

	

	
	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478f1, lblpregunta478f1_ind,lblpregunta478f2, lblpregunta478f2_a,lblpregunta478f2_b,lblpregunta478f2_c,
	lblpregunta478f2_d,lblpregunta478f2_e, lblpregunta478f3, lblpregunta478f3_ind,lblpregunta478f4,lblpregunta478f5, lblpregunta478f6, lblpregunta478f6_ind;
	public DISCAPACIDAD detalle; 
	public GridComponent2 gridDiscapacidad,gridpregunta478F2;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_2() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478F1","QI478F2_A","QI478F2_B","QI478F2_C","QI478F2_D","QI478F2_E","QI478F3","QI478F4","QI478F5","QI478F6","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478F1","QI478F2_A","QI478F2_B","QI478F2_C","QI478F2_D","QI478F2_E","QI478F3","QI478F4","QI478F5","QI478F6","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_2 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo2de13_18f).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478f1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478f2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478f3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478f4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478f5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478f6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");

		lblpregunta478f1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478f3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478f6_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		rgQI478F1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478f1_1,R.string.c2seccion_04b_2qi478f1_2,R.string.c2seccion_04b_2qi478f1_3,R.string.c2seccion_04b_2qi478f1_4,R.string.c2seccion_04b_2qi478f1_5,R.string.c2seccion_04b_2qi478f1_6,R.string.c2seccion_04b_2qi478f1_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("QI478F1OnChangeValue"); 
		rgQI478F3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	
		lblpregunta478f2_a= new LabelComponent(getActivity()).size(altoComponente+30, 440).textSize(19).text("");
		lblpregunta478f2_b= new LabelComponent(getActivity()).size(altoComponente+30, 440).textSize(19).text("");
		lblpregunta478f2_c= new LabelComponent(getActivity()).size(altoComponente+70, 440).textSize(19).text("");
		lblpregunta478f2_d= new LabelComponent(getActivity()).size(altoComponente+90, 440).textSize(19).text("");
		lblpregunta478f2_e= new LabelComponent(getActivity()).size(altoComponente+90, 440).textSize(19).text("");
		rgQI478F2_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+30,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F2_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+30,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F2_C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+70,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F2_D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+90,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478F2_E=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+90,315).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
				
		gridpregunta478F2=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridpregunta478F2.addComponent(lblpregunta478f2_a);
		gridpregunta478F2.addComponent(rgQI478F2_A);
		gridpregunta478F2.addComponent(lblpregunta478f2_b);
		gridpregunta478F2.addComponent(rgQI478F2_B);
		gridpregunta478F2.addComponent(lblpregunta478f2_c);
		gridpregunta478F2.addComponent(rgQI478F2_C);
		gridpregunta478F2.addComponent(lblpregunta478f2_d);
		gridpregunta478F2.addComponent(rgQI478F2_D);
		gridpregunta478F2.addComponent(lblpregunta478f2_e);
		gridpregunta478F2.addComponent(rgQI478F2_E);
	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478f1, lblpregunta478f1_ind,rgQI478F1); 
		q4  = createQuestionSection(0,lblpregunta478f2,gridpregunta478F2.component());
		q5  = createQuestionSection(0,lblpregunta478f3,lblpregunta478f3_ind,rgQI478F3);
		q6  = createQuestionSection(0,lblpregunta478f4,rgQI478F4);
		q7  = createQuestionSection(0,lblpregunta478f5,rgQI478F5);
		q8  = createQuestionSection(0,lblpregunta478f6,lblpregunta478f6_ind,rgQI478F6);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8); 
		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478f1  == null) {
				error = true;
				view = rgQI478F1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F1");
				return false;
			}
			if (ninio.qi478f2_a  == null) {
				error = true;
				view = rgQI478F2_A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F2_A");
				return false;
			}
			if (ninio.qi478f2_b  == null) {
				error = true;
				view = rgQI478F2_B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F2_B");
				return false;
			}
			if (ninio.qi478f2_c  == null) {
				error = true;
				view = rgQI478F2_C;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F2_C");
				return false;
			}
			if (ninio.qi478f2_d  == null) {
				error = true;
				view = rgQI478F2_D;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F2_D");
				return false;
			}
			if (ninio.qi478f2_e  == null) {
				error = true;
				view = rgQI478F2_E;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F2_E");
				return false;
			}
			if (ninio.qi478f3  == null) {
				error = true;
				view = rgQI478F3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F3");
				return false;
			}
			if (ninio.qi478f4  == null) {
				error = true;
				view = rgQI478F4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F4");
				return false;
			}
			if (ninio.qi478f5  == null) {
				error = true;
				view = rgQI478F5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F5");
				return false;
			}
			if (ninio.qi478f6  == null) {
				error = true;
				view = rgQI478F6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478F6");
				return false;
			}

		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	
	public void cambiarVariablesParalaBasedeDatos(){
		ninio.qi478f1=ninio.qi478f1!=null?ninio.setConvertconSieteVariables(ninio.qi478f1):null;
		ninio.qi478f2_a=ninio.qi478f2_a!=null?ninio.setConvertconTresVariables(ninio.qi478f2_a):null;
		ninio.qi478f2_b=ninio.qi478f2_b!=null?ninio.setConvertconTresVariables(ninio.qi478f2_b):null;
		ninio.qi478f2_c=ninio.qi478f2_c!=null?ninio.setConvertconTresVariables(ninio.qi478f2_c):null;
		ninio.qi478f2_d=ninio.qi478f2_d!=null?ninio.setConvertconTresVariables(ninio.qi478f2_d):null;
		ninio.qi478f2_e=ninio.qi478f2_e!=null?ninio.setConvertconTresVariables(ninio.qi478f2_e):null;
		ninio.qi478f3=ninio.qi478f3!=null?ninio.setConvertconTresVariables(ninio.qi478f3):null;
		ninio.qi478f4=ninio.qi478f4!=null?ninio.setConvertconTresVariables(ninio.qi478f4):null;
		ninio.qi478f5=ninio.qi478f5!=null?ninio.setConvertconTresVariables(ninio.qi478f5):null;
		ninio.qi478f6=ninio.qi478f6!=null?ninio.setConvertconTresVariables(ninio.qi478f6):null;

	}
	
	private void cambiarVariablesParaMostraraUsuario(){
		ninio.qi478f1 =ninio.qi478f1 !=null?ninio.getConvertconSieteVariables(ninio.qi478f1):null;
		ninio.qi478f2_a =ninio.qi478f2_a !=null?ninio.getConvertconTresVariables(ninio.qi478f2_a):null;
		ninio.qi478f2_b =ninio.qi478f2_b !=null?ninio.getConvertconTresVariables(ninio.qi478f2_b):null;
		ninio.qi478f2_c =ninio.qi478f2_c !=null?ninio.getConvertconTresVariables(ninio.qi478f2_c):null;
		ninio.qi478f2_d =ninio.qi478f2_d !=null?ninio.getConvertconTresVariables(ninio.qi478f2_d):null;
		ninio.qi478f2_e =ninio.qi478f2_e !=null?ninio.getConvertconTresVariables(ninio.qi478f2_e):null;
		ninio.qi478f3 =ninio.qi478f3 !=null?ninio.getConvertconTresVariables(ninio.qi478f3):null;
		ninio.qi478f4 =ninio.qi478f4 !=null?ninio.getConvertconTresVariables(ninio.qi478f4):null;
		ninio.qi478f5 =ninio.qi478f5 !=null?ninio.getConvertconTresVariables(ninio.qi478f5):null;
		ninio.qi478f6 =ninio.qi478f6 !=null?ninio.getConvertconTresVariables(ninio.qi478f6):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
		QI478F1OnChangeValue();
	}
	public void QI478F1OnChangeValue(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(!Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registr� como Discapacitado! ");
		}
	}
	public void evaluarPregunta() {		
		if(App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=13 && App.getInstance().getSeccion04DIT_02().qi478 <=18) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478F1,rgQI478F2_A,rgQI478F2_B, rgQI478F2_C, rgQI478F2_D,rgQI478F3,rgQI478F4,rgQI478F5,rgQI478F6);
			Log.e("11","22");
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);		
			onQI478AChangeValue();
		}
		else {
			Log.e("11","22");
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478F1,rgQI478F2_A,rgQI478F2_B, rgQI478F2_C, rgQI478F2_D,rgQI478F3,rgQI478F4,rgQI478F5,rgQI478F6);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);			
		}
	}
	
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478F1,rgQI478F2_A,rgQI478F2_B, rgQI478F2_C, rgQI478F2_D,rgQI478F3,rgQI478F4,rgQI478F5,rgQI478F6);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478F1,rgQI478F2_A,rgQI478F2_B, rgQI478F2_C, rgQI478F2_D,rgQI478F3,rgQI478F4,rgQI478F5,rgQI478F6);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
		}
	}
	 public void abrirDetalle(DISCAPACIDAD modelo) {
			FragmentManager fm = C2SECCION_04BFragment_022DIT_2.this.getFragmentManager();
			C2SECCION_04BFragment_018DIT_2_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_2_Dialog.newInstance(this, modelo);
			aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
			aperturaDialog.show(fm, "aperturaDialog");
		}
	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478f1.setText("(NOMBRE)");		
		lblpregunta478f1.setText(lblpregunta478f1.getText().toString().replace(replace, nombre));	
		lblpregunta478f1.setText(Html.fromHtml("478F1. <b>MUESTRE LA CARTILLA 2.</b> <br>De estas figuras <b>�cu�l o cu�les</b> son las que "+lblpregunta478f1.getText()+" <b>generalmente realiza</b>?"));
		lblpregunta478f1_ind.setText(Html.fromHtml("LEA EN VOZ ALTA LA(S) OPCI�N(ES) ELEGIDA(S) Y <b>SELECCIONE LA DE MAYOR NUMERACI�N</b>"));
		
		lblpregunta478f2.setText("(NOMBRE)");
		lblpregunta478f2.setText(lblpregunta478f2.getText().toString().replace(replace, nombre));
		lblpregunta478f2_d.setText(Html.fromHtml("D. En ese lugar �hay elementos t�xicos como detergentes, insecticidas <b>al alcance de</b> "+lblpregunta478f2.getText()+" ?"));
		lblpregunta478f2.setText(Html.fromHtml("478F2. En el lugar donde "+lblpregunta478f2.getText()+" <b>generalmente juega:</b>"));
		lblpregunta478f2_a.setText(Html.fromHtml("A. �Hay objetos pesados que <b>le pueden caer encima?</b>"));
		lblpregunta478f2_b.setText(Html.fromHtml("B. �Hay objetos <b>con los que se puede cortar</b>?"));
		lblpregunta478f2_c.setText(Html.fromHtml("C. El lugar donde generalmente juega �<b>est� cerca de</b> desperdicios o basura como restos de alimentos?"));
		
		lblpregunta478f2_e.setText(Html.fromHtml("E. El lugar donde generalmente juega �est� <b>fuera de la casa y cerca de</b> pistas, carreteras, acequias o abismos?"));
		
		lblpregunta478f3.setText("(NOMBRE)");
		lblpregunta478f3.setText(lblpregunta478f3.getText().toString().replace(replace, nombre));
		lblpregunta478f3.setText(Html.fromHtml("478F3. Cuando "+lblpregunta478f3.getText()+" quiere algo, �pide <b>con palabras</b>? <br><br>SI LA MADRE RESPONDE S�, PREGUNTE: Por ejemplo �qu� palabras usa?"));
		lblpregunta478f3_ind.setText(Html.fromHtml("MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> PALABRA AUNQUE ESTA SOLO LA ENTIENDA LA MADRE."));
		
		lblpregunta478f4.setText("(NOMBRE)");
		lblpregunta478f4.setText(lblpregunta478f4.getText().toString().replace(replace, nombre));
		lblpregunta478f4.setText(Html.fromHtml("478F4. Cuando se le pide a "+lblpregunta478f4.getText()+" que <b>lleve de un lugar a otro</b> un <b>objeto que conoce</b>, como alguno de sus juguetes, �lo hace? <br><br>DE SER NECESARIO, LEA: <br>Por ejemplo, cuando usted le pide a su hija(o) que lleve su pelota a su cuarto �lo hace?"));
				
		lblpregunta478f5.setText("(NOMBRE)");
		lblpregunta478f5.setText(lblpregunta478f5.getText().toString().replace(replace, nombre));
		lblpregunta478f5.setText(Html.fromHtml("478F5. Cuando se le pide a "+lblpregunta478f5.getText()+" que haga algo <b>sin usted mostrarle c�mo hacerlo</b> �lo hace? <br><br>SIEMPRE LEA: <br>Por ejemplo, cuando usted le dice a su hija(o) que se despida de su abuela, ella(�l) �le da un besito o le dice chao con la mano <b>sin necesidad de que usted o alguien m�s le muestre c�mo hacerlo</b>?"));
		
					
		lblpregunta478f6.setText("(NOMBRE)");
		lblpregunta478f6.setText(lblpregunta478f6.getText().toString().replace(replace, nombre));
		lblpregunta478f6.setText(Html.fromHtml("478F6. Cuando est� con "+lblpregunta478f6.getText()+" �usted <b>le habla de lo que est�n haciendo</b> en ese momento?"));
		lblpregunta478f6_ind.setText(Html.fromHtml("SIEMPRE LEA: <br>Por ejemplo, usted le dice: �estamos comiendo tu papita� <b>mientras comen.</b>"));
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478F1.readOnly();
			rgQI478F2_A.readOnly();
			rgQI478F2_B.readOnly();
			rgQI478F2_C.readOnly();
			rgQI478F2_D.readOnly();
			rgQI478F2_E.readOnly();
			rgQI478F3.readOnly();
			rgQI478F4.readOnly();
			rgQI478F5.readOnly();
			rgQI478F6.readOnly();				
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
