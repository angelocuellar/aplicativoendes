package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_013 extends FragmentForm { 
		
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI469C; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI469D; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI469D_A; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI469D_B; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI469D_C; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI469D_D; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI469D_E; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI469D_F; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI469D_G; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI469D_H; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI469D_I; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI469D_J; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI469D_K; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI469D_L; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI469D_M; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI469D_N; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI469D_O; 
	@FieldAnnotation(orderIndex=18) 
	public CheckBoxField chbQI469D_P; 
	@FieldAnnotation(orderIndex=19) 
	public CheckBoxField chbQI469D_X; 
	@FieldAnnotation(orderIndex=20) 
	public TextField txtQI469D_XO; 
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta469c,lblpregunta469d,lbllugar,lblpregunta469dlug,lblpregunta469dhos,lblpregunta469d_ind,lblpregunta469d_Pu_Ind,lblpregunta469d_Pri_Ind,lblpregunta469d_Org_Ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 

	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI469C","QI469D","QI469D_A","QI469D_B","QI469D_C","QI469D_D","QI469D_E","QI469D_F","QI469D_G","QI469D_H","QI469D_I","QI469D_J","QI469D_K","QI469D_L","QI469D_M","QI469D_N","QI469D_O","QI469D_P","QI469D_X","QI469D_XO")}; 
	SeccionCapitulo[] seccionesCargado; 
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdLugarSalud;
	public C2SECCION_04BFragment_013() {} 
	public C2SECCION_04BFragment_013 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI469C","QI469D","QI469D_A","QI469D_B","QI469D_C","QI469D_D","QI469D_E","QI469D_F","QI469D_G","QI469D_H","QI469D_I","QI469D_J","QI469D_K","QI469D_L","QI469D_M","QI469D_N","QI469D_O","QI469D_P","QI469D_X","QI469D_XO","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
			    
	    lblpregunta469c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469c);
	    lblpregunta469d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469d);
	    lblpregunta469d_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi469d_ind);
	    
	    rgQI469C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi469c_1,R.string.c2seccion_04bqi469c_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI469cChangeValue"); 
	    txtQI469D=new TextField(this.getActivity()).size(altoComponente, 430).maxLength(100).alfanumerico(); 
		
		chbQI469D_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_N=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_O=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_o, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_P=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_p, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI469D_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi469d_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI469dxChangeValue");
		txtQI469D_XO=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 

		
		lbllugar = new LabelComponent(this.getActivity()).size(altoComponente, 340).text(R.string.c2seccion_04bqi469dnom).textSize(16);
		lblpregunta469dlug = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi469dlug);
		
		Spanned texto469dPu =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta469d_Pu_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta469d_Pu_Ind.setText(texto469dPu);
		
		Spanned texto469dPri =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta469d_Pri_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta469d_Pri_Ind.setText(texto469dPri);
		
		Spanned texto469dOrg =Html.fromHtml("<b>ORGANISMOS NO GUBERNAMENTALES</b>");
		lblpregunta469d_Org_Ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta469d_Org_Ind.setText(texto469dOrg);
		
		
		gdLugarSalud = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdLugarSalud.addComponent(lbllugar);
		gdLugarSalud.addComponent(txtQI469D);
		
    } 
    @Override 
    protected View createUI() { 
    	buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta469c,rgQI469C); 
		LinearLayout ly469d = new LinearLayout(getActivity());
		ly469d.addView(chbQI469D_X);
		ly469d.addView(txtQI469D_XO);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta469d,lblpregunta469d_ind,gdLugarSalud.component(),lblpregunta469dlug,lblpregunta469d_Pu_Ind,chbQI469D_A,chbQI469D_B,chbQI469D_C,chbQI469D_D,chbQI469D_E,chbQI469D_F,chbQI469D_G,chbQI469D_H,lblpregunta469d_Pri_Ind,chbQI469D_I,chbQI469D_J,chbQI469D_K,chbQI469D_L,chbQI469D_M,chbQI469D_N,lblpregunta469d_Org_Ind,chbQI469D_O,chbQI469D_P,ly469d);//,,,chbQI229_Y
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		int cont=0;		
		if(c2seccion_04b.qi469d_a==1)
			cont+=1;
		if(c2seccion_04b.qi469d_b==1)
			cont+=1;
		if(c2seccion_04b.qi469d_c==1)
			cont+=1;
		if(c2seccion_04b.qi469d_d==1)
			cont+=1;
		if(c2seccion_04b.qi469d_e==1)
			cont+=1;
		if(c2seccion_04b.qi469d_f==1)
			cont+=1;
		if(c2seccion_04b.qi469d_g==1)
			cont+=1;
		if(c2seccion_04b.qi469d_h==1)
			cont+=1;
		if(c2seccion_04b.qi469d_i==1)
			cont+=1;
		if(c2seccion_04b.qi469d_j==1)
			cont+=1;
		if(c2seccion_04b.qi469d_k==1)
			cont+=1;
		if(c2seccion_04b.qi469d_l==1)
			cont+=1;
		if(c2seccion_04b.qi469d_m==1)
			cont+=1;
		if(c2seccion_04b.qi469d_n==1)
			cont+=1;
		if(c2seccion_04b.qi469d_o==1)
			cont+=1;
		if(c2seccion_04b.qi469d_p==1)
			cont+=1;
		if(c2seccion_04b.qi469d_x==1)
			cont+=1;
		
		App.getInstance().getSeccion04B().qi469c=c2seccion_04b.qi469c;
		
		App.getInstance().getSeccion04B().filtro469d = cont;
		App.getInstance().getSeccion04B().qi469d_a=c2seccion_04b.qi469d_a;
		App.getInstance().getSeccion04B().qi469d_b=c2seccion_04b.qi469d_b;
		App.getInstance().getSeccion04B().qi469d_c=c2seccion_04b.qi469d_c;
		App.getInstance().getSeccion04B().qi469d_d=c2seccion_04b.qi469d_d;
		App.getInstance().getSeccion04B().qi469d_e=c2seccion_04b.qi469d_e;
		App.getInstance().getSeccion04B().qi469d_f=c2seccion_04b.qi469d_f;
		App.getInstance().getSeccion04B().qi469d_g=c2seccion_04b.qi469d_g;
		App.getInstance().getSeccion04B().qi469d_h=c2seccion_04b.qi469d_h;
		App.getInstance().getSeccion04B().qi469d_i=c2seccion_04b.qi469d_i;
		App.getInstance().getSeccion04B().qi469d_j=c2seccion_04b.qi469d_j;
		App.getInstance().getSeccion04B().qi469d_k=c2seccion_04b.qi469d_k;
		App.getInstance().getSeccion04B().qi469d_l=c2seccion_04b.qi469d_l;
		App.getInstance().getSeccion04B().qi469d_m=c2seccion_04b.qi469d_m;
		App.getInstance().getSeccion04B().qi469d_n=c2seccion_04b.qi469d_n;
		App.getInstance().getSeccion04B().qi469d_o=c2seccion_04b.qi469d_o;
		App.getInstance().getSeccion04B().qi469d_p=c2seccion_04b.qi469d_p;
		App.getInstance().getSeccion04B().qi469d_x=c2seccion_04b.qi469d_x;
		
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(c2seccion_04b.qi469c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI469C"); 
			view = rgQI469C; 
			error = true; 
			return false; 
		} 
		
		if(c2seccion_04b.qi469c==1) {
			/*if (Util.esVacio(c2seccion_04b.qi469d)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI469D"); 
				view = txtQI469D; 
				error = true; 
				return false; 
			} */
			
			if(!verificarCheck469d()) {
				mensaje = "Seleccione una opci�n en P.469D"; 
				view = chbQI469D_A; 
				error = true; 
				return false; 
			}
			
			if (chbQI469D_X.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi469d_xo)) {
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI469D_XO; 
					error = true; 
					return false; 
				}
			}	
		}
		
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	validar469();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 

	public void validar469() {
		
		if (App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467==1 || 
			App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468==1) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
			Util.lockView(getActivity(),false,rgQI469C,txtQI469D,chbQI469D_A,chbQI469D_B,chbQI469D_C,chbQI469D_D,chbQI469D_E,chbQI469D_F,chbQI469D_G,chbQI469D_H,chbQI469D_I,chbQI469D_J,chbQI469D_K,chbQI469D_L,chbQI469D_M,chbQI469D_N,chbQI469D_O,chbQI469D_P,chbQI469D_X,txtQI469D_XO);  
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		
        	onqrgQI469cChangeValue();
        	onqrgQI469dxChangeValue();
    		
//    		MyUtil.LiberarMemoria();
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),rgQI469C,txtQI469D,chbQI469D_A,chbQI469D_B,chbQI469D_C,chbQI469D_D,chbQI469D_E,chbQI469D_F,chbQI469D_G,chbQI469D_H,chbQI469D_I,chbQI469D_J,chbQI469D_K,chbQI469D_L,chbQI469D_M,chbQI469D_N,chbQI469D_O,chbQI469D_P,chbQI469D_X,txtQI469D_XO);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
	}
			
			
	public void onqrgQI469dxChangeValue() {  	
    	if (chbQI469D_X.isChecked()){		
  			Util.lockView(getActivity(),false,txtQI469D_XO);
  			/*if (verificarCheck407()) {  			
  				//Util.cleanAndLockView(getActivity(),chbQI408_X);
  			}*/
  			txtQI469D_XO.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQI469D_XO);  
  			/*if (!verificarCheck407()) {  			
  				Util.lockView(getActivity(),false,chbQI407_Y);  				
			}*/
  		}	
    }
	
	public boolean verificarCheck469d() {
  		if (chbQI469D_A.isChecked() || chbQI469D_B.isChecked() || chbQI469D_C.isChecked() || chbQI469D_D.isChecked() || 
  			chbQI469D_E.isChecked() || chbQI469D_F.isChecked() || chbQI469D_G.isChecked() || chbQI469D_H.isChecked() ||
  			chbQI469D_I.isChecked() || chbQI469D_J.isChecked() || chbQI469D_K.isChecked() || chbQI469D_L.isChecked() ||
  			chbQI469D_M.isChecked() || chbQI469D_N.isChecked() || chbQI469D_O.isChecked() || chbQI469D_P.isChecked() || 
  			chbQI469D_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	public void onqrgQI469cChangeValue() {	
		if(rgQI469C.getValue()!=null) {
			if (rgQI469C.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,chbQI469D_A,chbQI469D_B,chbQI469D_C,chbQI469D_D,chbQI469D_E,chbQI469D_F,chbQI469D_G,chbQI469D_H,chbQI469D_I,chbQI469D_J,chbQI469D_K,chbQI469D_L,chbQI469D_M,chbQI469D_N,chbQI469D_O,chbQI469D_P,chbQI469D_X,txtQI469D,txtQI469D_XO);  
	    		App.getInstance().getSeccion04B().qi469c = 1;
				q2.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI469D_A,chbQI469D_B,chbQI469D_C,chbQI469D_D,chbQI469D_E,chbQI469D_F,chbQI469D_G,chbQI469D_H,chbQI469D_I,chbQI469D_J,chbQI469D_K,chbQI469D_L,chbQI469D_M,chbQI469D_N,chbQI469D_O,chbQI469D_P,chbQI469D_X,txtQI469D,txtQI469D_XO);
	    		if(rgQI469C.getValue()!=null)
	    			App.getInstance().getSeccion04B().qi469c = 2;
	    		q2.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI469C.readOnly();
			txtQI469D.readOnly();
			txtQI469D_XO.readOnly();
			chbQI469D_A.readOnly();
			chbQI469D_B.readOnly();
			chbQI469D_C.readOnly();
			chbQI469D_D.readOnly();
			chbQI469D_E.readOnly();
			chbQI469D_F.readOnly();
			chbQI469D_G.readOnly();
			chbQI469D_H.readOnly();
			chbQI469D_I.readOnly();
			chbQI469D_J.readOnly();
			chbQI469D_K.readOnly();
			chbQI469D_L.readOnly();
			chbQI469D_M.readOnly();
			chbQI469D_N.readOnly();
			chbQI469D_O.readOnly();
			chbQI469D_P.readOnly();
			chbQI469D_X.readOnly();
			
		}
	}
	
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta469c.setText(lblpregunta469c.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		int cont=0;		
		if(c2seccion_04b.qi469d_a==1)
			cont+=1;
		if(c2seccion_04b.qi469d_b==1)
			cont+=1;
		if(c2seccion_04b.qi469d_c==1)
			cont+=1;
		if(c2seccion_04b.qi469d_d==1)
			cont+=1;
		if(c2seccion_04b.qi469d_e==1)
			cont+=1;
		if(c2seccion_04b.qi469d_f==1)
			cont+=1;
		if(c2seccion_04b.qi469d_g==1)
			cont+=1;
		if(c2seccion_04b.qi469d_h==1)
			cont+=1;
		if(c2seccion_04b.qi469d_i==1)
			cont+=1;
		if(c2seccion_04b.qi469d_j==1)
			cont+=1;
		if(c2seccion_04b.qi469d_k==1)
			cont+=1;
		if(c2seccion_04b.qi469d_l==1)
			cont+=1;
		if(c2seccion_04b.qi469d_m==1)
			cont+=1;
		if(c2seccion_04b.qi469d_n==1)
			cont+=1;
		if(c2seccion_04b.qi469d_o==1)
			cont+=1;
		if(c2seccion_04b.qi469d_p==1)
			cont+=1;
		if(c2seccion_04b.qi469d_x==1)
			cont+=1;
		
		App.getInstance().getSeccion04B().qi469c=c2seccion_04b.qi469c;
		
		App.getInstance().getSeccion04B().filtro469d = cont;
		App.getInstance().getSeccion04B().qi469d_a=c2seccion_04b.qi469d_a;
		App.getInstance().getSeccion04B().qi469d_b=c2seccion_04b.qi469d_b;
		App.getInstance().getSeccion04B().qi469d_c=c2seccion_04b.qi469d_c;
		App.getInstance().getSeccion04B().qi469d_d=c2seccion_04b.qi469d_d;
		App.getInstance().getSeccion04B().qi469d_e=c2seccion_04b.qi469d_e;
		App.getInstance().getSeccion04B().qi469d_f=c2seccion_04b.qi469d_f;
		App.getInstance().getSeccion04B().qi469d_g=c2seccion_04b.qi469d_g;
		App.getInstance().getSeccion04B().qi469d_h=c2seccion_04b.qi469d_h;
		App.getInstance().getSeccion04B().qi469d_i=c2seccion_04b.qi469d_i;
		App.getInstance().getSeccion04B().qi469d_j=c2seccion_04b.qi469d_j;
		App.getInstance().getSeccion04B().qi469d_k=c2seccion_04b.qi469d_k;
		App.getInstance().getSeccion04B().qi469d_l=c2seccion_04b.qi469d_l;
		App.getInstance().getSeccion04B().qi469d_m=c2seccion_04b.qi469d_m;
		App.getInstance().getSeccion04B().qi469d_n=c2seccion_04b.qi469d_n;
		App.getInstance().getSeccion04B().qi469d_o=c2seccion_04b.qi469d_o;
		App.getInstance().getSeccion04B().qi469d_p=c2seccion_04b.qi469d_p;
		App.getInstance().getSeccion04B().qi469d_x=c2seccion_04b.qi469d_x;
		
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
