package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_012 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public IntegerField txtQI466C; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI467; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI468; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI468A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI468B; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI468B_O; 
	
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI469A; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI469B; 
	
	public CheckBoxField chb466c;
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta466c,lblpregunta467,lblpregunta468,lblpregunta468a,lblpregunta468b,lblpregunta469a,lblpregunta466c_ind,
	lblpregunta469b,lblcontrolveces; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7;  
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	public ButtonComponent btnNoSabeControl466c;
	public GridComponent2 gdControl466c;
	String nombre_persona;
	public TextField txtCabecera;
	
	String fechareferencia,fechareferencia2;	
	public C2SECCION_04BFragment_012() {} 
	public C2SECCION_04BFragment_012 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		rango(getActivity(), txtQI466C, 0, 40); 		
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466C","QI467","QI467CONS","QI468","QI468CONS","QI468A","QI468B","QI468B_O","QI469A","QI469B","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI466C","QI467","QI467CONS","QI468","QI468CONS","QI468A","QI468B","QI468B_O","QI469A","QI469B")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta466c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi466c);
	    lblpregunta466c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi466c_ind);
	    lblpregunta467 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi467);
	    lblpregunta468 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi468);
	    lblpregunta468a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi468a);
	    lblpregunta468b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi468b);
	    lblpregunta469a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469a);
	    lblpregunta469b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469b);
	    
	    txtQI466C=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
	   
	    lblcontrolveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi466c_1).textSize(16).centrar();
	    
	    chb466c=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi466c_2, "1:0").size(altoComponente, 200);
		chb466c.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb466c.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI466C);
			}
			else {
				Util.lockView(getActivity(),false, txtQI466C);	
			}
		}
		});
		
		gdControl466c = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdControl466c.addComponent(lblcontrolveces);
		gdControl466c.addComponent(txtQI466C);
		gdControl466c.addComponent(chb466c);
	    
	    
	    rgQI467=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi467_1,R.string.c2seccion_04bqi467_2,R.string.c2seccion_04bqi467_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI467ChangeValue"); 
		rgQI468=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi468_1,R.string.c2seccion_04bqi468_2,R.string.c2seccion_04bqi468_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI468ChangeValue"); 
		rgQI468A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi468a_1,R.string.c2seccion_04bqi468a_2,R.string.c2seccion_04bqi468a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI468aChangeValue"); 
		rgQI468B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi468b_1,R.string.c2seccion_04bqi468b_2,R.string.c2seccion_04bqi468b_3,R.string.c2seccion_04bqi468b_4,R.string.c2seccion_04bqi468b_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI468B_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI468B.agregarEspecifique(3,txtQI468B_O); 	    
	    
		rgQI469A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi469a_1,R.string.c2seccion_04bqi469a_2,R.string.c2seccion_04bqi469a_3,R.string.c2seccion_04bqi469a_4,R.string.c2seccion_04bqi469a_5,R.string.c2seccion_04bqi469a_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI469B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi469b_1,R.string.c2seccion_04bqi469b_2,R.string.c2seccion_04bqi469b_3,R.string.c2seccion_04bqi469b_4,R.string.c2seccion_04bqi469b_5,R.string.c2seccion_04bqi469b_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 		
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta466c,lblpregunta466c_ind,gdControl466c.component()); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta467,rgQI467); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta468,rgQI468); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta468a,rgQI468A); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta468b,rgQI468B);
		q6 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta469a,rgQI469A); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta469b,rgQI469B); 
		 
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		//uiToEntity(entity); 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi467!=null) {
				c2seccion_04b.qi467=c2seccion_04b.getConvertQi467(c2seccion_04b.qi467);
			}	
			
			if (c2seccion_04b.qi468!=null) {
				c2seccion_04b.qi468=c2seccion_04b.getConvertQi468(c2seccion_04b.qi468);
			}	
			
			if (c2seccion_04b.qi468a!=null) {
				c2seccion_04b.qi468a=c2seccion_04b.getConvertQi468a(c2seccion_04b.qi468a);
			}	
			
			if (c2seccion_04b.qi468b!=null) {
				c2seccion_04b.qi468b=c2seccion_04b.getConvertQi468b(c2seccion_04b.qi468b);
			}	
			
			if (c2seccion_04b.qi469a!=null) {
				c2seccion_04b.qi469a=c2seccion_04b.getConvertQi469a(c2seccion_04b.qi469a);
			}	
			
			if (c2seccion_04b.qi469b!=null) {
				c2seccion_04b.qi469b=c2seccion_04b.getConvertQi469b(c2seccion_04b.qi469b);
			}	
		}
		
		App.getInstance().getSeccion04B().qi467 = c2seccion_04b.qi467;
		App.getInstance().getSeccion04B().qi468 = c2seccion_04b.qi468;
		
		
		if(chb466c.isChecked() ) {
			c2seccion_04b.qi466c=98;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(c2seccion_04b.qi467== null) {
				c2seccion_04b.qi467cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi467cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi467cons = fechareferencia;
				}			
			}
			
			if(c2seccion_04b.qi468== null) {
				c2seccion_04b.qi468cons = null;
			} 
			else {
				if(fechareferencia2 == null) {
					c2seccion_04b.qi468cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi468cons = fechareferencia2;
				}			
			}
			
			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467!=1 && App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468!=1) {
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_013.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_014.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI471","QI471A","QI471B_A","QI471B_B","QI471B_C","QI471B_D","QI471B_E","QI471B_F","QI471B_X","QI471B_XO","QI471B_Z","QI471CA","QI471CB","QI471CC","QI471CD","QI471CE","QI471CF");					
					App.getInstance().getSeccion04B().qi469d_a=null;
					App.getInstance().getSeccion04B().qi469d_b=null;
					App.getInstance().getSeccion04B().qi469d_c=null;
					App.getInstance().getSeccion04B().qi469d_d=null;
					App.getInstance().getSeccion04B().qi469d_e=null;
					App.getInstance().getSeccion04B().qi469d_f=null;
					App.getInstance().getSeccion04B().qi469d_g=null;
					App.getInstance().getSeccion04B().qi469d_h=null;
					App.getInstance().getSeccion04B().qi469d_i=null;
					App.getInstance().getSeccion04B().qi469d_j=null;
					App.getInstance().getSeccion04B().qi469d_k=null;
					App.getInstance().getSeccion04B().qi469d_l=null;
					App.getInstance().getSeccion04B().qi469d_m=null;
					App.getInstance().getSeccion04B().qi469d_n=null;
					App.getInstance().getSeccion04B().qi469d_o=null;
					App.getInstance().getSeccion04B().qi469d_p=null;
					App.getInstance().getSeccion04B().qi469d_x=null;
					App.getInstance().getSeccion04B().qi469c=null;
					App.getInstance().getSeccion04B().filtro469d = null;					
					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
					
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (Util.esVacio(c2seccion_04b.qi466c) && !chb466c.isChecked()) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI466C"); 
			view = txtQI466C; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi467)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI467"); 
			view = rgQI467; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi468)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI468"); 
			view = rgQI468; 
			error = true; 
			return false; 
		} 
		
		if(c2seccion_04b.qi468==1) {
			if (Util.esVacio(c2seccion_04b.qi468a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI468A"); 
				view = rgQI468A; 
				error = true; 
				return false; 
			}
			if(c2seccion_04b.qi468a==1) {
				if (Util.esVacio(c2seccion_04b.qi468b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI468B"); 
					view = rgQI468B; 
					error = true; 
					return false; 
				} 
				if(!Util.esDiferente(c2seccion_04b.qi468b,6)){ 
					if (Util.esVacio(c2seccion_04b.qi468b_o)) { 
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI468B_O; 
						error = true; 
						return false; 
					} 
				} 		
			}
		}
		
		if((rgQI467.getValue()!=null && rgQI467.getValue().toString().equals("1")) ||
			(rgQI468.getValue()!=null && rgQI468.getValue().toString().equals("1"))) {
			if (Util.esVacio(c2seccion_04b.qi469a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI469A"); 
				view = rgQI469A; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi469b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI469B"); 
				view = rgQI469B; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi467!=null) {
				c2seccion_04b.qi467=c2seccion_04b.setConvertQi467(c2seccion_04b.qi467);
			}	
			
			if (c2seccion_04b.qi468!=null) {
				c2seccion_04b.qi468=c2seccion_04b.setConvertQi468(c2seccion_04b.qi468);
			}	
			
			if (c2seccion_04b.qi468a!=null) {
				c2seccion_04b.qi468a=c2seccion_04b.setConvertQi468a(c2seccion_04b.qi468a);
			}	
			
			if (c2seccion_04b.qi468b!=null) {
				c2seccion_04b.qi468b=c2seccion_04b.setConvertQi468b(c2seccion_04b.qi468b);
			}	
			
			if (c2seccion_04b.qi469a!=null) {
				c2seccion_04b.qi469a=c2seccion_04b.setConvertQi469a(c2seccion_04b.qi469a);
			}	
			
			if (c2seccion_04b.qi469b!=null) {
				c2seccion_04b.qi469b=c2seccion_04b.setConvertQi469b(c2seccion_04b.qi469b);
			}	
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b);
		
		if(c2seccion_04b.qi466c!=null && c2seccion_04b.qi466c==98) {
			chb466c.setChecked(true);
			txtQI466C.setText("");
		}	
		
		fechareferencia = c2seccion_04b.qi467cons;
		fechareferencia2 = c2seccion_04b.qi468cons;
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	onqrgQI467ChangeValue();
    	onqrgQI468ChangeValue();
    	onqrgQI468aChangeValue();
    	validar469();
    	
    	if (c2seccion_04b.qi466c!=null && c2seccion_04b.qi466c==98) { Util.cleanAndLockView(getActivity(),txtQI466C); } else{ Util.lockView(getActivity(),false,txtQI466C);}
    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
  
    

	
	public void onqrgQI467ChangeValue() {		
		if(rgQI467.getValue()!=null) {
			if (rgQI467.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				App.getInstance().getSeccion04B().qi467 = 1;
				//validar469();
	    	} 
	    	else {	
	    		if(rgQI467.getValue()!=null)
	    			App.getInstance().getSeccion04B().qi467 =  Integer.parseInt(rgQI467.getValue().toString());
	    	}	
			onqrgQI468ChangeValue();
		}
		
    }
	
	public void onqrgQI468ChangeValue() {		
		if(rgQI468.getValue()!=null) {	
			if (rgQI468.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,rgQI468A,rgQI468B);
				App.getInstance().getSeccion04B().qi468 = 1;
				
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),rgQI468A,rgQI468B);
	    		if(rgQI468.getValue()!=null)
	    			App.getInstance().getSeccion04B().qi468 = Integer.parseInt(rgQI468.getValue().toString());
	    		q4.setVisibility(View.GONE);
	    		q5.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
			validar469();
		}
    }
	
	public void validar469() {
		
		if ((rgQI467.getValue()!=null && rgQI467.getValue().toString().equals("1")) ||
			(rgQI468.getValue()!=null && rgQI468.getValue().toString().equals("1"))) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
			Util.lockView(getActivity(),false,rgQI469A,rgQI469B);  
    		q6.setVisibility(View.VISIBLE);
    		q7.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),rgQI469A,rgQI469B);
    		q6.setVisibility(View.GONE);
    		q7.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();
    	}
	}
	
	public void onqrgQI468aChangeValue() {		
		if(rgQI468A.getValue()!=null) {
			if (rgQI468A.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,rgQI468B);  
	    		q5.setVisibility(View.VISIBLE);
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),rgQI468B);
	    		q5.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta466c.setText(lblpregunta466c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	
    	Calendar fecharef = new GregorianCalendar();
    	Calendar fecharef2 = new GregorianCalendar();
		if(fechareferencia!=null)
		{
			//fecharef = new GregorianCalendar(salud.qs802ca, salud.qs802cm-1, salud.qs802cd);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}
		
		if(fechareferencia2!=null)
		{
			//fecharef = new GregorianCalendar(salud.qs802ca, salud.qs802cm-1, salud.qs802cd);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia2);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef2=cal;
		}
		
    	Calendar fechainicio = MyUtil.restarDias(fecharef,14);
    	Integer f1 =fechainicio.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio.get(Calendar.MONTH)) ;
    
    	Calendar fechados =	MyUtil.restarDias(fecharef2,14);
    	Integer f2 = fechados.get(Calendar.DAY_OF_MONTH) ;
    	String mes2=MyUtil.Mes(fechados.get(Calendar.MONTH)) ;
    	//lblpregunta330.text(R.string.ciseccion_03qi330);
    	    	
    	lblpregunta467.setText(lblpregunta467.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta467.setText(lblpregunta467.getText().toString().replace("$", f1.toString()+" de "+mes1));
    	lblpregunta468.setText(lblpregunta468.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta468.setText(lblpregunta468.getText().toString().replace("$", f2.toString()+" de "+mes2));
    	lblpregunta468a.setText(lblpregunta468a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta469a.setText(lblpregunta469a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta469b.setText(lblpregunta469b.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI467.readOnly();
			rgQI468.readOnly();
			rgQI468A.readOnly();
			rgQI468B.readOnly();
			rgQI469A.readOnly();
			rgQI469B.readOnly();
			chb466c.readOnly();
			txtQI466C.readOnly();
			txtQI468B_O.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi467!=null) {
				c2seccion_04b.qi467=c2seccion_04b.getConvertQi467(c2seccion_04b.qi467);
			}	
			
			if (c2seccion_04b.qi468!=null) {
				c2seccion_04b.qi468=c2seccion_04b.getConvertQi468(c2seccion_04b.qi468);
			}	
			
			if (c2seccion_04b.qi468a!=null) {
				c2seccion_04b.qi468a=c2seccion_04b.getConvertQi468a(c2seccion_04b.qi468a);
			}	
			
			if (c2seccion_04b.qi468b!=null) {
				c2seccion_04b.qi468b=c2seccion_04b.getConvertQi468b(c2seccion_04b.qi468b);
			}	
			
			if (c2seccion_04b.qi469a!=null) {
				c2seccion_04b.qi469a=c2seccion_04b.getConvertQi469a(c2seccion_04b.qi469a);
			}	
			
			if (c2seccion_04b.qi469b!=null) {
				c2seccion_04b.qi469b=c2seccion_04b.getConvertQi469b(c2seccion_04b.qi469b);
			}	
		}
		
		App.getInstance().getSeccion04B().qi467 = c2seccion_04b.qi467;
		App.getInstance().getSeccion04B().qi468 = c2seccion_04b.qi468;
		
		
		if(chb466c.isChecked() ) {
			c2seccion_04b.qi466c=98;			
		}
		
		try { 
			if(c2seccion_04b.qi467== null) {
				c2seccion_04b.qi467cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi467cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi467cons = fechareferencia;
				}			
			}
			
			if(c2seccion_04b.qi468== null) {
				c2seccion_04b.qi468cons = null;
			} 
			else {
				if(fechareferencia2 == null) {
					c2seccion_04b.qi468cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi468cons = fechareferencia2;
				}			
			}
			
			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi467!=1 && App.getInstance().getSeccion04B().qi468!=null && App.getInstance().getSeccion04B().qi468!=1) {
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_013.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_014.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI471","QI471A","QI471B_A","QI471B_B","QI471B_C","QI471B_D","QI471B_E","QI471B_F","QI471B_X","QI471B_XO","QI471B_Z","QI471CA","QI471CB","QI471CC","QI471CD","QI471CE","QI471CF");					
					App.getInstance().getSeccion04B().qi469d_a=null;
					App.getInstance().getSeccion04B().qi469d_b=null;
					App.getInstance().getSeccion04B().qi469d_c=null;
					App.getInstance().getSeccion04B().qi469d_d=null;
					App.getInstance().getSeccion04B().qi469d_e=null;
					App.getInstance().getSeccion04B().qi469d_f=null;
					App.getInstance().getSeccion04B().qi469d_g=null;
					App.getInstance().getSeccion04B().qi469d_h=null;
					App.getInstance().getSeccion04B().qi469d_i=null;
					App.getInstance().getSeccion04B().qi469d_j=null;
					App.getInstance().getSeccion04B().qi469d_k=null;
					App.getInstance().getSeccion04B().qi469d_l=null;
					App.getInstance().getSeccion04B().qi469d_m=null;
					App.getInstance().getSeccion04B().qi469d_n=null;
					App.getInstance().getSeccion04B().qi469d_o=null;
					App.getInstance().getSeccion04B().qi469d_p=null;
					App.getInstance().getSeccion04B().qi469d_x=null;
					App.getInstance().getSeccion04B().qi469c=null;
					App.getInstance().getSeccion04B().filtro469d = null;					
					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
					
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
