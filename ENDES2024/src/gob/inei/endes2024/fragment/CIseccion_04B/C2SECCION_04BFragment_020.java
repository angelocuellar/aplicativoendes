package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_020 extends FragmentForm { 

	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI476C;
	@FieldAnnotation(orderIndex=2)
	public IntegerField txtQI476C_N;
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI476C_A;
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI476C_B;
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI476C_C;
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI476C_D;
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI476C_E;
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI476C_F;
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI476C_X;
	@FieldAnnotation(orderIndex=10) 
	public TextField txtQI476C_X_O;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI476C_Z;
	@FieldAnnotation(orderIndex=12)
	public RadioGroupOtherField rgQI477;
	@FieldAnnotation(orderIndex=13)
	public RadioGroupOtherField rgQI477_1;
	@FieldAnnotation(orderIndex=14) 
	public IntegerField txtQI477_1_N;
	@FieldAnnotation(orderIndex=15)
	public RadioGroupOtherField rgQI477_2;
	@FieldAnnotation(orderIndex=16) 
	public TextField txtQI477_2_O;
	
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta476c,lblpregunta477,lblpregunta476c_ind,lblpregunta477_1,lblpregunta477_2,lblpregunta477_2a_ti,lblpregunta477_2b_ti,lblpregunta477_2c_ti; 
	LinearLayout q0,q1,q2,q3,q4; 

	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	String nombre_persona;
	public TextField txtCabecera;
	String fechareferencia;
	public C2SECCION_04BFragment_020() {} 
	public C2SECCION_04BFragment_020 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI476C", "QI476C_N", "QI476C_A", "QI476C_B", "QI476C_C", "QI476C_D", "QI476C_E", "QI476C_F", "QI476C_X", "QI476C_X_O", "QI476C_Z", "QI477", "QI477_1", "QI477_1_N", "QI477_2", "QI477_2_O", "QI477CONS", "QI472","QI468","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI476C", "QI476C_N", "QI476C_A", "QI476C_B", "QI476C_C", "QI476C_D", "QI476C_E", "QI476C_F", "QI476C_X", "QI476C_X_O", "QI476C_Z", "QI477","QI477_1", "QI477_1_N", "QI477_2", "QI477_2_O","QI477CONS")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    lblpregunta476c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476c);
	    lblpregunta476c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi476c_ind);
	    lblpregunta477 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477);
	    lblpregunta477_1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477_1);
	    lblpregunta477_2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477_2);
	    
	    Spanned texto477_2a =Html.fromHtml("<b>SECTOR P�BLICO</b>");
		lblpregunta477_2a_ti = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta477_2a_ti.setText(texto477_2a);		
		Spanned texto477_2b =Html.fromHtml("<b>SECTOR PRIVADO</b>");
		lblpregunta477_2b_ti = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta477_2b_ti.setText(texto477_2b);		
		Spanned texto477_2c =Html.fromHtml("<b>ORGANISMO NO GUBERNAMENTAL</b>");
		lblpregunta477_2c_ti = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(18);
		lblpregunta477_2c_ti.setText(texto477_2c);
		
		
		
	    rgQI476C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi476c_1,R.string.c2seccion_04bqi476c_2,R.string.c2seccion_04bqi476c_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onqrgQI476cChangeValue");
	    txtQI476C_N=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	    rgQI476C.agregarEspecifique(0,txtQI476C_N);
	    
	    chbQI476C_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_aChangeValue");
	    chbQI476C_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_bChangeValue");
	    chbQI476C_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_cChangeValue");
	    chbQI476C_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_dChangeValue");
	    chbQI476C_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_eChangeValue");
	    chbQI476C_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_fChangeValue");
	    chbQI476C_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_xChangeValue");
	    chbQI476C_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi476c_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI476c_zChangeValue");
	    txtQI476C_X_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500).centrar().alfanumerico();
	    
	    rgQI477=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi477_x1,R.string.c2seccion_04bqi477_x2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI477ChangeValue");
	    
	    rgQI477_1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi477_1_1,R.string.c2seccion_04bqi477_1_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
	    txtQI477_1_N=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2);
	    rgQI477_1.agregarEspecifique(0,txtQI477_1_N);
	    
	    rgQI477_2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi477_2_1,R.string.c2seccion_04bqi477_2_2,R.string.c2seccion_04bqi477_2_3,R.string.c2seccion_04bqi477_2_4,R.string.c2seccion_04bqi477_2_5,R.string.c2seccion_04bqi477_2_6,R.string.c2seccion_04bqi477_2_7,R.string.c2seccion_04bqi477_2_8,R.string.c2seccion_04bqi477_2_9,R.string.c2seccion_04bqi477_2_10,R.string.c2seccion_04bqi477_2_11,R.string.c2seccion_04bqi477_2_12,R.string.c2seccion_04bqi477_2_13).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
	    txtQI477_2_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500).centrar().alfanumerico();
	    rgQI477_2.agregarEspecifique(11,txtQI477_2_O);
	    rgQI477_2.addView(lblpregunta477_2a_ti,0);
	    rgQI477_2.addView(lblpregunta477_2b_ti,6);
	    rgQI477_2.addView(lblpregunta477_2c_ti,11);
	    
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL, lblpregunta476c, rgQI476C, lblpregunta476c_ind, chbQI476C_A,chbQI476C_B,chbQI476C_C,chbQI476C_D,chbQI476C_E,chbQI476C_F,chbQI476C_X,txtQI476C_X_O,chbQI476C_Z);
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta477,rgQI477);
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta477_1,rgQI477_1);
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta477_2,rgQI477_2);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try {
			if(c2seccion_04b.qi477== null) {
				c2seccion_04b.qi477cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi477cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi477cons = fechareferencia;
				}			
			}
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				SeccionCapitulo[] seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO")};
				CISECCION_04DIT_02 ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
				
				if(ninio!=null){		
					App.getInstance().setSeccion04DIT_02(null);
					App.getInstance().setSeccion04DIT_02(ninio);
				}	
				else{ 
					App.getInstance().setSeccion04DIT_02(null);
				}
				
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (Util.esVacio(c2seccion_04b.qi476c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI476C"); 
			view = rgQI476C; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(c2seccion_04b.qi476c,1)) {
			if (Util.esVacio(c2seccion_04b.qi476c_n)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI476C_N"); 
				view = txtQI476C_N; 
				error = true; 
				return false; 
			} 
			if(!verificarCheckP476c()){
				mensaje = "Seleccione una opci�n en la pregunta QI476C_A"; 
				view = chbQI476C_A; 
				error = true; 
				return false; 
			}
			if(chbQI476C_X.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi476c_x_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI476C_X_O"); 
					view = txtQI476C_X_O; 
					error = true; 
					return false; 
				} 
			}
		}
		
		if (Util.esVacio(c2seccion_04b.qi477)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI477"); 
			view = rgQI477; 
			error = true; 
			return false; 
		}
		
		if (!Util.esDiferente(c2seccion_04b.qi477,1)) {
			if (Util.esVacio(c2seccion_04b.qi477_1)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI477_1"); 
				view = rgQI477_1; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(c2seccion_04b.qi477_1,1)) {
				if (Util.esVacio(c2seccion_04b.qi477_1_n)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI477_1_N"); 
					view = txtQI477_1_N; 
					error = true; 
					return false; 
				}
			}
			
			if (Util.esVacio(c2seccion_04b.qi477_2)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI477_2"); 
				view = rgQI477_2; 
				error = true; 
				return false; 
			}
			if (!Util.esDiferente(c2seccion_04b.qi477_2,12)) {
				if (Util.esVacio(c2seccion_04b.qi477_2_o)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI477_2_O"); 
					view = txtQI477_2_O; 
					error = true; 
					return false; 
				}
			}
			
		}
		
		

		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		fechareferencia = c2seccion_04b.qi477cons;
		inicio(); 
    } 
    private void inicio() { 
    	Log.e("","ggggg: "+c2seccion_04b.qi477cons);
    	renombrarEtiquetas();
    	onqrgQI476cChangeValue();
    	onqrgQI477ChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 

    public void onqrgQI476cChangeValue() {
    	if(rgQI476C.getValue()!=null ) {
			if (rgQI476C.getValue().toString().equals("1")) {
				Util.lockView(getActivity(),false,chbQI476C_A,chbQI476C_B,chbQI476C_C,chbQI476C_D,chbQI476C_E,chbQI476C_F,chbQI476C_X,chbQI476C_Z);  
				chbQI476C_A.setVisibility(View.VISIBLE);
				chbQI476C_B.setVisibility(View.VISIBLE);
				chbQI476C_C.setVisibility(View.VISIBLE);
				chbQI476C_D.setVisibility(View.VISIBLE);
				chbQI476C_E.setVisibility(View.VISIBLE);
				chbQI476C_F.setVisibility(View.VISIBLE);
				chbQI476C_X.setVisibility(View.VISIBLE);
				txtQI476C_X_O.setVisibility(View.VISIBLE);
				chbQI476C_Z.setVisibility(View.VISIBLE);
				ejecutaAccionCheckbox();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI476C_A,chbQI476C_B,chbQI476C_C,chbQI476C_D,chbQI476C_E,chbQI476C_F,chbQI476C_X,chbQI476C_Z);
	    		chbQI476C_A.setVisibility(View.GONE);
				chbQI476C_B.setVisibility(View.GONE);
				chbQI476C_C.setVisibility(View.GONE);
				chbQI476C_D.setVisibility(View.GONE);
				chbQI476C_E.setVisibility(View.GONE);
				chbQI476C_F.setVisibility(View.GONE);
				chbQI476C_X.setVisibility(View.GONE);
				txtQI476C_X_O.setVisibility(View.GONE);
				chbQI476C_Z.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
	}
   
    public void onqrgQI477ChangeValue() {
    	if(rgQI477.getValue()!=null ) {
			if (rgQI477.getValue().toString().equals("1")) {
				Util.lockView(getActivity(),false,rgQI477_1, rgQI477_2);  
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
			}
			else{
				Util.cleanAndLockView(getActivity(),rgQI477_1, txtQI477_1_N, rgQI477_2, txtQI477_2_O);
				MyUtil.LiberarMemoria();
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
			}
				
		}
    }
    
    public boolean verificarCheckP476c() {
  		if (chbQI476C_A.isChecked()||chbQI476C_B.isChecked()||chbQI476C_C.isChecked()||chbQI476C_D.isChecked()||chbQI476C_E.isChecked()||chbQI476C_F.isChecked()||chbQI476C_X.isChecked()||chbQI476C_Z.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
       
    public void ejecutaAccionCheckbox() {
    	if(chbQI476C_Z.isChecked()){
    		onqrgQI476c_zChangeValue();
		}
		else{
			onqrgQI476c_aChangeValue();
			onqrgQI476c_bChangeValue();
			onqrgQI476c_cChangeValue();
			onqrgQI476c_dChangeValue();
			onqrgQI476c_eChangeValue();
			onqrgQI476c_fChangeValue();
			onqrgQI476c_xChangeValue();
		}
	}
    
    public void onqrgQI476c_aChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_bChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_cChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_dChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_eChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_fChangeValue(){if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}}
    public void onqrgQI476c_xChangeValue(){
    if (verificarCheckP476c()){Util.cleanAndLockView(getActivity(),chbQI476C_Z);}else{Util.lockView(getActivity(),false,chbQI476C_Z);}
    	if (chbQI476C_X.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI476C_X_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI476C_X_O);    		
    	}
    }
    
    public void onqrgQI476c_zChangeValue(){
    	if (verificarCheckP476c()){
    		Util.cleanAndLockView(getActivity(),chbQI476C_A,chbQI476C_B,chbQI476C_C,chbQI476C_D,chbQI476C_E,chbQI476C_F,chbQI476C_X, txtQI476C_X_O);
    	}
    	else{
    		Util.lockView(getActivity(),false,chbQI476C_A,chbQI476C_B,chbQI476C_C,chbQI476C_D,chbQI476C_E,chbQI476C_F,chbQI476C_X);
    	}
    }
    
    
	public void renombrarEtiquetas(){	
    	Calendar fecharef = new GregorianCalendar();    	
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = null;

			try {
				date = df.parse(fechareferencia);
				//date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			//Calendar cal2 = Calendar.getInstance();
			cal.setTime(date);
			//cal2.setTime(date);
			fecharef=cal;
		}	
		
				
		//Log.e("fecha: ",""+fecharef);		
		Calendar fechainicio2 = MyUtil.restarDias(fecharef,90);
    	Integer f21 =fechainicio2.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio2.get(Calendar.MONTH));    	
    	lblpregunta476c.setText(lblpregunta476c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta476c.setText(lblpregunta476c.getText().toString().replace("#", f21.toString()+" de "+mes1));
    	
    	
    	Calendar fecharef2 = new GregorianCalendar();
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = null;
			//Date date2 = null;
			try {
				date = df.parse(fechareferencia);
				//date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			//Calendar cal2 = Calendar.getInstance();
			cal.setTime(date);
			//cal2.setTime(date);			
			fecharef2=cal;
		}
		
    	
		//Log.e("fecha2_: ",""+fecharef2);
    	Calendar fechainicio = MyUtil.RestarMeses(fecharef2,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecharef2,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));

    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado";
    	}else{
    		F3 = "de este a�o,";
    	}
    	
    	
    	lblpregunta477.setText(lblpregunta477.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta477_1.setText(lblpregunta477_1.getText().toString().replace("(NOMBRE)", nombre_persona));
    	
    	
//    	lblpregunta476c.setText(lblpregunta476c.getText().toString().replace("$", F4));
//    	lblpregunta476c.setText(lblpregunta476c.getText().toString().replace("%", F2));    	
    	lblpregunta477.setText(lblpregunta477.getText().toString().replace("$", F2));
    	lblpregunta477.setText(lblpregunta477.getText().toString().replace("#", F1));
    	lblpregunta477.setText(lblpregunta477.getText().toString().replace("%", F3));
    	
    	
    	
    	
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI476C.readOnly();
			txtQI476C_N.readOnly();
			chbQI476C_A.readOnly();
			chbQI476C_B.readOnly();
			chbQI476C_C.readOnly();
			chbQI476C_D.readOnly();
			chbQI476C_E.readOnly();
			chbQI476C_F.readOnly();
			chbQI476C_X.readOnly();
			txtQI476C_X_O.readOnly();
			chbQI476C_Z.readOnly();
			rgQI477.readOnly();			
			rgQI477_1.readOnly();
			txtQI477_1_N.readOnly();
			rgQI477_2.readOnly();
			txtQI477_2_O.readOnly();
		}
	}
	
	private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		try { 			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
