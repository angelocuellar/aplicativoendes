package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_003 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI458; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI459; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI460; 
	@FieldAnnotation(orderIndex=4) 
	public IntegerField txtQI460A; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI461; 
	@FieldAnnotation(orderIndex=6) 
	public IntegerField txtQI461A; 
	
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI461B; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI461CU; 
	/*@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI461CN;*/ 
	@FieldAnnotation(orderIndex=9) 
	public IntegerField txtQI461D; 
	
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta458,lblpregunta459,lblpregunta460,lblpregunta460a,
	lblpregunta461,lblpregunta461a,lblvacunaveces,lblvacunaveces2,lblvacio461c1,lblpregunta461b,lblpregunta461c,lblpregunta461d; 
	public IntegerField txtQI461CC1,txtQI461CC2;
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7,q8,q9; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI458","QI459","QI460","QI460A","QI461","QI461A","QI461B","QI461CU","QI461CN","QI461D")}; 
	SeccionCapitulo[] seccionesCargado; 
	public ButtonComponent btnNoSabeVacuna460a,btnNoSabeVacuna461a;
	public GridComponent2 gdVacuna460a,gdVacuna461a,gdVacuna461d,gridPreguntas461c;
	String nombre_persona;
	public TextField txtCabecera;
	
	public CheckBoxField chb460a,chb461a,chb461d;
	
	public C2SECCION_04BFragment_003() {} 
	public C2SECCION_04BFragment_003 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		rango(getActivity(), txtQI461D, 1, 7);
		rango(getActivity(), txtQI460A, 1, 7); 
		rango(getActivity(), txtQI461A, 1, 7); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI458","QI459","QI460","QI460A","QI461","QI461A","QI461B","QI461CU","QI461CN","QI461D","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta461b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");
	    lblpregunta461c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461cu);
	    lblpregunta461d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461d);
	    
	    rgQI461B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461b_1,R.string.c2seccion_04bqi461b_2,R.string.c2seccion_04bqi461b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI461bChangeValue"); ; 
	    rgQI461CU=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461cu_1,R.string.c2seccion_04bqi461cu_2,R.string.c2seccion_04bqi461cu_3).size(225,400).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI461CChangeValue"); 
	    
		txtQI461CC1=new IntegerField(this.getActivity()).size(75, 100).maxLength(2); 
		txtQI461CC2=new IntegerField(this.getActivity()).size(75, 100).maxLength(2);
		lblvacio461c1 = new LabelComponent(this.getActivity()).size(75, 100);		
		
		gridPreguntas461c = new GridComponent2(this.getActivity(),App.ESTILO,2,3);
		gridPreguntas461c.addComponent(rgQI461CU,1,3);		
		gridPreguntas461c.addComponent(txtQI461CC1);
		gridPreguntas461c.addComponent(txtQI461CC2);
		gridPreguntas461c.addComponent(lblvacio461c1);		
		
		txtQI461D=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		
		lblvacunaveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi461d_1).textSize(16).centrar();
		
		chb461d=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi461d_2, "1:0").size(altoComponente, 200);
		chb461d.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb461d.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI461D);
			}
			else {
				Util.lockView(getActivity(),false, txtQI461D);	
			}
		}
		});
		
		gdVacuna461d = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna461d.addComponent(lblvacunaveces);
		gdVacuna461d.addComponent(txtQI461D);
		gdVacuna461d.addComponent(chb461d);
	    
	    lblpregunta458 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi458);
	    lblpregunta459 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");//new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi459);
	    lblpregunta460 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi460);
	    lblpregunta460a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi460a);
	    lblpregunta461 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461);
	    lblpregunta461a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi461a);
	    
	     
		rgQI458=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi458_1,R.string.c2seccion_04bqi458_2,R.string.c2seccion_04bqi458_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI458ChangeValue"); 
        rgQI459=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi459_1,R.string.c2seccion_04bqi459_2,R.string.c2seccion_04bqi459_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI460=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi460_1,R.string.c2seccion_04bqi460_2,R.string.c2seccion_04bqi460_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI460ChangeValue"); 
		txtQI460A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		lblvacunaveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi460a_1).textSize(16).centrar();
		
		chb460a=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi460a_2, "1:0").size(altoComponente, 200);
		chb460a.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb460a.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI460A);
			}
			else {
				Util.lockView(getActivity(),false, txtQI460A);	
			}
		}
		});
		
		
		gdVacuna460a = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna460a.addComponent(lblvacunaveces);
		gdVacuna460a.addComponent(txtQI460A);
		gdVacuna460a.addComponent(chb460a);
			
		
		rgQI461=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi461_1,R.string.c2seccion_04bqi461_2,R.string.c2seccion_04bqi461_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI461ChangeValue"); ; 
		txtQI461A=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1);
		lblvacunaveces2  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi461a_1).textSize(16).centrar();
		
		chb461a=new CheckBoxField(this.getActivity(), R.string.c2seccion_04aqi461a_2, "1:0").size(altoComponente, 200);
		chb461a.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb461a.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI461A);
			}
			else {
				Util.lockView(getActivity(),false, txtQI461A);	
			}
		}
		});
		
		gdVacuna461a = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna461a.addComponent(lblvacunaveces2);
		gdVacuna461a.addComponent(txtQI461A);
		gdVacuna461a.addComponent(chb461a);
  } 

  @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta458,rgQI458); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta459,rgQI459); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta460,rgQI460); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta460a,gdVacuna460a.component()); 
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461,rgQI461); 
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461a,gdVacuna461a.component()); 
		
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461b,rgQI461B); 
		q8 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461c,gridPreguntas461c.component()); 
		q9 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta461d,gdVacuna461d.component()); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		
		form.addView(q7); 
		form.addView(q8); 
		form.addView(q9); 
    return contenedor; 
    } 
  
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null) {
						
			if (c2seccion_04b.qi458!=null) {
				c2seccion_04b.qi458=c2seccion_04b.getConvertQi458(c2seccion_04b.qi458);
			}	
			
			if (c2seccion_04b.qi459!=null) {
				c2seccion_04b.qi459=c2seccion_04b.getConvertQi459(c2seccion_04b.qi459);
			}
			
			if (c2seccion_04b.qi460!=null) {
				c2seccion_04b.qi460=c2seccion_04b.getConvertQi460(c2seccion_04b.qi460);
			}	
			
			if (c2seccion_04b.qi461!=null) {
				c2seccion_04b.qi461=c2seccion_04b.getConvertQi461(c2seccion_04b.qi461);
			}	
			
			if (c2seccion_04b.qi461b!=null) {
				c2seccion_04b.qi461b=c2seccion_04b.getConvertQi461b(c2seccion_04b.qi461b);
			}	
					
			if (c2seccion_04b.qi461cu!=null) {
				c2seccion_04b.qi461cu=c2seccion_04b.getConvertQi461c(c2seccion_04b.qi461cu);
			}	
			
		}
		
		if(App.getInstance().getSeccion04B()!=null) {
			App.getInstance().getSeccion04B().qi458 =c2seccion_04b.qi458;
		}
		else {
			App.getInstance().setSeccion04B(c2seccion_04b);
		}	
		
		if(chb460a.isChecked() ) {
			c2seccion_04b.qi460a=8;			
		}
		
		if(chb461a.isChecked() ) {
			c2seccion_04b.qi461a=8;			
		}
		if (c2seccion_04b.qi461cu!=null) {
			if (!Util.esDiferente(c2seccion_04b.qi461cu,1) && txtQI461CC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04b.qi461cn=Integer.parseInt(txtQI461CC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04b.qi461cu,2) && txtQI461CC2.getText().toString().trim().length()!=0) {
				c2seccion_04b.qi461cn=Integer.parseInt(txtQI461CC2.getText().toString().trim());
			}
			else {
				c2seccion_04b.qi461cn=null;
			}
		}	
		if (c2seccion_04b.qi461cu==null)
			c2seccion_04b.qi461cn=null;
		
		if(chb461d.isChecked() ) {
			c2seccion_04b.qi461d=8;			
		}
		
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458!=1) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_004.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI465A");					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		
		
		if (Util.esVacio(c2seccion_04b.qi458)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI458"); 
			view = rgQI458; 
			error = true; 
			return false; 
		} 		
		
		if(c2seccion_04b.qi458==1) {
		
			if (Util.esVacio(c2seccion_04b.qi459)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI459"); 
				view = rgQI459; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi460)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI460"); 
				view = rgQI460; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi460==1) {
			
				if (Util.esVacio(c2seccion_04b.qi460a) && !chb460a.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI460A"); 
					view = txtQI460A; 
					error = true; 
					return false; 
				} 
			
			}
			if (Util.esVacio(c2seccion_04b.qi461)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI461"); 
				view = rgQI461; 
				error = true; 
				return false; 
			}
			if(c2seccion_04b.qi461==1) {
				if (Util.esVacio(c2seccion_04b.qi461a) && !chb461a.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461A"); 
					view = txtQI461A; 
					error = true; 
					return false; 
				} 
			}
			
			
			if (Util.esVacio(c2seccion_04b.qi461b)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI461B"); 
				view = rgQI461B; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi461b==1) {
				if (Util.esVacio(c2seccion_04b.qi461cu)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461CU"); 
					view = rgQI461CU; 
					error = true; 
					return false; 
				} 	
				
				if (!Util.esDiferente(c2seccion_04b.qi461cu,1)) {
					if (Util.esVacio(c2seccion_04b.qi461cn)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.461CC1"); 
						view = txtQI461CC1; 
						error = true; 
						return false; 
					}
					if ( Util.esMenor(c2seccion_04b.qi461cn,0) || Util.esMayor(c2seccion_04b.qi461cn,90)) {
						mensaje = "Valores validos en D�as de 0 a 90"; 
						view = txtQI461CC1; 
						error = true; 
						return false; 
					}
				}	
				
				if (!Util.esDiferente(c2seccion_04b.qi461cu,2)) {
					if (Util.esVacio(c2seccion_04b.qi461cn)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta P.461CC2"); 
						view = txtQI461CC2; 
						error = true; 
						return false; 
					}	
					if ( Util.esMenor(c2seccion_04b.qi461cn,1) || Util.esMayor(c2seccion_04b.qi461cn,90)) {
						mensaje = "Valores validos en Semanas de 1 a 90"; 
						view = txtQI461CC2; 
						error = true; 
						return false; 
					}
				}
				
				if (Util.esVacio(c2seccion_04b.qi461d) && !chb461d.isChecked()) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI461D"); 
					view = txtQI461D; 
					error = true; 
					return false; 
				} 
			
			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null) {
			if (c2seccion_04b.qi458!=null) {
				c2seccion_04b.qi458=c2seccion_04b.setConvertQi458(c2seccion_04b.qi458);
			}	
			
			if (c2seccion_04b.qi459!=null) {
				c2seccion_04b.qi459=c2seccion_04b.setConvertQi459(c2seccion_04b.qi459);
			}
			
			if (c2seccion_04b.qi460!=null) {
				c2seccion_04b.qi460=c2seccion_04b.setConvertQi460(c2seccion_04b.qi460);
			}	
			
			if (c2seccion_04b.qi461!=null) {
				c2seccion_04b.qi461=c2seccion_04b.setConvertQi461(c2seccion_04b.qi461);
			}	
			if (c2seccion_04b.qi461b!=null) {
				c2seccion_04b.qi461b=c2seccion_04b.setConvertQi461b(c2seccion_04b.qi461b);
			}	
					
			if (c2seccion_04b.qi461cu!=null) {
				c2seccion_04b.qi461cu=c2seccion_04b.setConvertQi461c(c2seccion_04b.qi461cu);
			}	
			
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		
		if(c2seccion_04b.qi460a!=null && c2seccion_04b.qi460a==8) {
			chb460a.setChecked(true);
			txtQI460A.setText("");
		}		
		
		if(c2seccion_04b.qi461a!=null && c2seccion_04b.qi461a==8) {
			chb461a.setChecked(true);
			txtQI461A.setText("");
		}
		
		if (c2seccion_04b.qi461cn!=null) {	
			if (!Util.esDiferente(c2seccion_04b.qi461cu,1)) {
    			txtQI461CC1.setText(c2seccion_04b.qi461cn.toString());
    		}
    		if (!Util.esDiferente(c2seccion_04b.qi461cu,2)) {
    			txtQI461CC2.setText(c2seccion_04b.qi461cn.toString());
    		}
    	}
		
		if(c2seccion_04b.qi461d!=null && c2seccion_04b.qi461d==8) {
			chb461d.setChecked(true);
			txtQI461D.setText("");
		}	
		
		inicio(); 
    } 
    private void inicio() {
    	renombrarEtiquetas();
    	evaluarPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluarPregunta() {
		if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null) {
			Util.cleanAndLockView(getActivity(),rgQI458,rgQI459,rgQI460,txtQI460A,rgQI461,txtQI461A,rgQI461B,rgQI461CU,txtQI461D,chb461d);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		q5.setVisibility(View.GONE);
    		q6.setVisibility(View.GONE); 
    		q7.setVisibility(View.GONE);
    		q8.setVisibility(View.GONE);
    		q9.setVisibility(View.GONE);
		}
		else {
			Util.lockView(getActivity(),false,rgQI458,rgQI459,rgQI460,txtQI460A,rgQI461,txtQI461A,rgQI461B,rgQI461CU,txtQI461D,chb461d);  
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		q5.setVisibility(View.VISIBLE);
    		q6.setVisibility(View.VISIBLE);  
    		q7.setVisibility(View.VISIBLE);
    		q8.setVisibility(View.VISIBLE);
    		q9.setVisibility(View.VISIBLE);
    		
    		if(c2seccion_04b.qi458!=null) 
    	    	onqrgQI458ChangeValue();
    	    if(c2seccion_04b.qi460!=null) 
    	    	onqrgQI460ChangeValue();
    	    if(c2seccion_04b.qi461!=null) 
    	    	onqrgQI461ChangeValue();
    	    
    	    if(c2seccion_04b.qi461b!=null)
            	onqrgQI461bChangeValue();	
            	onqrgQI461CChangeValue();
            	
    	    if (c2seccion_04b.qi460a!=null && c2seccion_04b.qi460a==8) { Util.cleanAndLockView(getActivity(),txtQI460A); } else{ Util.lockView(getActivity(),false,txtQI460A);}
    	    if (c2seccion_04b.qi461a!=null && c2seccion_04b.qi461a==8) { Util.cleanAndLockView(getActivity(),txtQI461A); } else{ Util.lockView(getActivity(),false,txtQI461A);}
    	    if (c2seccion_04b.qi461d!=null && c2seccion_04b.qi461d==8) { Util.cleanAndLockView(getActivity(),txtQI461D); } else{ Util.lockView(getActivity(),false,txtQI461D);}
		}
    }
    

	
	public void onqrgQI458ChangeValue() {  	
		if(rgQI458.getValue()!=null) {
			if (rgQI458.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,rgQI459,rgQI460,txtQI460A,chb460a,rgQI461,txtQI461A,chb461a);  
	    		q2.setVisibility(View.VISIBLE);
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
	    		q6.setVisibility(View.VISIBLE);
	    		q7.setVisibility(View.VISIBLE);
	    		q8.setVisibility(View.VISIBLE);
	    		q9.setVisibility(View.VISIBLE);
	    		
	    		
	    		if(App.getInstance().getSeccion04B()!=null) {
					c2seccion_04b.qi458 = 1;
					App.getInstance().getSeccion04B().qi458 =c2seccion_04b.qi458;
				}			
				else {
					c2seccion_04b.qi458 = 1;
					App.getInstance().setSeccion04B(c2seccion_04b);	
				}
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),rgQI459,rgQI460,txtQI460A,chb460a,rgQI461,txtQI461A,chb461a);
	    		q2.setVisibility(View.GONE);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		q5.setVisibility(View.GONE);
	    		q6.setVisibility(View.GONE);
	    		q7.setVisibility(View.GONE);
	    		q8.setVisibility(View.GONE);
	    		q9.setVisibility(View.GONE);
	    		
	    		if(App.getInstance().getSeccion04B()!=null) {
					c2seccion_04b.qi458 = Integer.parseInt(rgQI458.getValue().toString());
					App.getInstance().getSeccion04B().qi458 =c2seccion_04b.qi458;
				}			
					else {
					c2seccion_04b.qi458 = Integer.parseInt(rgQI458.getValue().toString());
					App.getInstance().setSeccion04B(c2seccion_04b);	
					}
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI460ChangeValue() {  	
		if(rgQI460.getValue()!=null) {
			if (rgQI460.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,txtQI460A,chb460a);  
	    		q4.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI460A,chb460a);
	    		q4.setVisibility(View.GONE);
	    		//Util.lockView(getActivity(),false,chbQI407_Y);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI461ChangeValue() {  	
		if(rgQI461.getValue()!=null) {
			if (rgQI461.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,txtQI461A,chb461a);  
	    		q6.setVisibility(View.VISIBLE);  
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI461A,chb461a);
	    		q6.setVisibility(View.GONE);
	    		//Util.lockView(getActivity(),false,chbQI407_Y);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI461bChangeValue() {  	
		if(rgQI461B.getValue()!=null){
			if (rgQI461B.getValue().toString().equals("1")) {    		
				Util.lockView(getActivity(),false,rgQI461CU,txtQI461CC1,txtQI461CC2,txtQI461D,chb461d);  
	    		q8.setVisibility(View.VISIBLE);
	    		q9.setVisibility(View.VISIBLE);
	    		rgQI461CU.requestFocus();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),rgQI461CU,txtQI461CC1,txtQI461CC2,txtQI461D,chb461d);
	    		q8.setVisibility(View.GONE);
	    		q9.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	public void onqrgQI461CChangeValue() {
		if(rgQI461CU.getValue()!=null) {
			if (MyUtil.incluyeRango(1,1,rgQI461CU.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI461CC2);
				c2seccion_04b.qi461cn=null;
				Util.lockView(getActivity(),false,txtQI461CC1);				
				txtQI461CC1.requestFocus();				
			} 
			else if (MyUtil.incluyeRango(2,2,rgQI461CU.getTagSelected("").toString())) {			
				Util.cleanAndLockView(getActivity(),txtQI461CC1);	
				c2seccion_04b.qi461cn=null;
				Util.lockView(getActivity(),false,txtQI461CC2);			
				txtQI461CC2.requestFocus();				
			}
			
			else if (MyUtil.incluyeRango(3,3,rgQI461CU.getTagSelected("").toString())) {
				c2seccion_04b.qi461cn=null;		
				Util.cleanAndLockView(getActivity(),txtQI461CC1,txtQI461CC2);			
			    			
			}
		}		
	}
	
	public void renombrarEtiquetas(){	
		lblpregunta458.text(R.string.c2seccion_04bqi458);
		lblpregunta459.setText("(NOMBRE)");
		lblpregunta460.text(R.string.c2seccion_04bqi460);
		lblpregunta461.text(R.string.c2seccion_04bqi461);
		lblpregunta461b.setText("(NOMBRE)");
		lblpregunta461c.text(R.string.c2seccion_04bqi461cu);
    	
    	lblpregunta458.setText(lblpregunta458.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta459.setText(lblpregunta459.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta460.setText(lblpregunta460.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta461.setText(lblpregunta461.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta461b.setText(lblpregunta461b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta461c.setText(lblpregunta461c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	
    	Spanned texto459 =Html.fromHtml("459. D�game por favor si "+lblpregunta459.getText() +"  recibi� alguna de las siguientes vacunas:<br><br> �La vacuna BCG contra la tuberculosis, esto es una inyecci�n que se aplica al <b>reci�n nacido</b> en el hombro y deja una cicatriz?");
    	lblpregunta459.setText(texto459);
    	Spanned texto461b =Html.fromHtml("461B. �Recibi� "+lblpregunta461b.getText() +"  una vacuna contra la HEPATITIS B, que es una inyecci�n que se pone al <b>reci�n nacido</b> en el muslo o el brazo?");
    	lblpregunta461b.setText(texto461b);
    	
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI458.readOnly();
			rgQI459.readOnly();
			rgQI460.readOnly();
			rgQI461.readOnly();
			chb460a.readOnly();
			chb461a.readOnly();
			txtQI460A.readOnly();
			txtQI461A.readOnly();
			rgQI461B.readOnly();
			rgQI461CU.readOnly();
			chb461d.readOnly();
			txtQI461CC1.readOnly();
			txtQI461CC2.readOnly();
			txtQI461D.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null) {
						
			if (c2seccion_04b.qi461b!=null) {
				c2seccion_04b.qi461b=c2seccion_04b.getConvertQi461b(c2seccion_04b.qi461b);
			}	
					
			if (c2seccion_04b.qi461cu!=null) {
				c2seccion_04b.qi461cu=c2seccion_04b.getConvertQi461c(c2seccion_04b.qi461cu);
			}
			
			if (c2seccion_04b.qi458!=null) {
				c2seccion_04b.qi458=c2seccion_04b.getConvertQi458(c2seccion_04b.qi458);
			}	
			
			if (c2seccion_04b.qi459!=null) {
				c2seccion_04b.qi459=c2seccion_04b.getConvertQi459(c2seccion_04b.qi459);
			}
			
			if (c2seccion_04b.qi460!=null) {
				c2seccion_04b.qi460=c2seccion_04b.getConvertQi460(c2seccion_04b.qi460);
			}	
			
			if (c2seccion_04b.qi461!=null) {
				c2seccion_04b.qi461=c2seccion_04b.getConvertQi461(c2seccion_04b.qi461);
			}	
			
		}
		
		if(App.getInstance().getSeccion04B()!=null) {
			App.getInstance().getSeccion04B().qi458 =c2seccion_04b.qi458;
		}
		else {
			App.getInstance().setSeccion04B(c2seccion_04b);
		}	
		
		if(chb460a.isChecked() ) {
			c2seccion_04b.qi460a=8;			
		}
		
		if(chb461a.isChecked() ) {
			c2seccion_04b.qi461a=8;			
		}
		
		if (c2seccion_04b.qi461cu!=null) {
			if (!Util.esDiferente(c2seccion_04b.qi461cu,1) && txtQI461CC1.getText().toString().trim().length()!=0 ) {
				c2seccion_04b.qi461cn=Integer.parseInt(txtQI461CC1.getText().toString().trim());
			}
			else if (!Util.esDiferente(c2seccion_04b.qi461cu,2) && txtQI461CC2.getText().toString().trim().length()!=0) {
				c2seccion_04b.qi461cn=Integer.parseInt(txtQI461CC2.getText().toString().trim());
			}
			else {
				c2seccion_04b.qi461cn=null;
			}
		}	
		if (c2seccion_04b.qi461cu==null)
			c2seccion_04b.qi461cn=null;
		
		if(chb461d.isChecked() ) {
			c2seccion_04b.qi461d=8;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458!=1) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_004.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI465A");					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
