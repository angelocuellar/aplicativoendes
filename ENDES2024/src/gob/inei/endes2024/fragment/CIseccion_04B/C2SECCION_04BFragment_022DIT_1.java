package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_1_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_1 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478E1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478E2; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478E3; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478E4; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478E5; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478E6; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478E7; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478E8; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478E9; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI478E10;
	

	
	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478e1, lblpregunta478e1_ind,lblpregunta478e2,lblpregunta478e2_ind, lblpregunta478e3,lblpregunta478e3_ind,lblpregunta478e4, 
	lblpregunta478e4_ind,lblpregunta478e5, lblpregunta478e6, lblpregunta478e6_ind, lblpregunta478e7, lblpregunta478e8, lblpregunta478e9, lblpregunta478e10,lblpregunta478e9_ind,lblpregunta478e10_ind;
	
	public DISCAPACIDAD detalle; 
	
	public GridComponent2 gridpregunta4789,gridDiscapacidad;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_1() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478E1","QI478E2","QI478E3","QI478E4","QI478E5","QI478E6","QI478E7","QI478E8","QI478E9","QI478E10","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478E1","QI478E2","QI478E3","QI478E4","QI478E5","QI478E6","QI478E7","QI478E8","QI478E9","QI478E10","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_1 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo1de9_12e).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478e1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e7= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e8= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e9= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e10= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		lblpregunta478e1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478e2_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478e3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478e4_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478e6_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478e9_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478e10_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		rgQI478E1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478e1_1,R.string.c2seccion_04b_2qi478e1_2,R.string.c2seccion_04b_2qi478e1_3,R.string.c2seccion_04b_2qi478e1_4,R.string.c2seccion_04b_2qi478e1_5,R.string.c2seccion_04b_2qi478e1_6,R.string.c2seccion_04b_2qi478e1_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("QI4781EChangeVelue"); 
		rgQI478E2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E8=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E9=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478E10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478e10_1,R.string.c2seccion_04b_2qi478e10_2,R.string.c2seccion_04b_2qi478e10_3,R.string.c2seccion_04b_2qi478e10_4,R.string.c2seccion_04b_2qi478e10_5,R.string.c2seccion_04b_2qi478e10_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);

	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478e1, lblpregunta478e1_ind,rgQI478E1); 
		q4  = createQuestionSection(0,lblpregunta478e2,lblpregunta478e2_ind,rgQI478E2);
		q5  = createQuestionSection(0,lblpregunta478e3,lblpregunta478e3_ind,rgQI478E3);
		q6  = createQuestionSection(0,lblpregunta478e4,lblpregunta478e4_ind,rgQI478E4);
		q7  = createQuestionSection(0,lblpregunta478e5,rgQI478E5);
		q8  = createQuestionSection(0,lblpregunta478e6,lblpregunta478e6_ind,rgQI478E6);
		q9  = createQuestionSection(0,lblpregunta478e7,rgQI478E7);
		q10 = createQuestionSection(0,lblpregunta478e8,rgQI478E8);
		q11 = createQuestionSection(0,lblpregunta478e9,lblpregunta478e9_ind,rgQI478E9);
		q12 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478e10,lblpregunta478e10_ind,rgQI478E10);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8); 
		form.addView(q9);
		form.addView(q10);             
		form.addView(q11); 
		form.addView(q12); 

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478e1  == null) {
				error = true;
				view = rgQI478E1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E1");
				return false;
			}
			if (ninio.qi478e2  == null) {
				error = true;
				view = rgQI478E2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E2");
				return false;
			}
			if (ninio.qi478e3  == null) {
				error = true;
				view = rgQI478E3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E3");
				return false;
			}
			if (ninio.qi478e4  == null) {
				error = true;
				view = rgQI478E4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E4");
				return false;
			}
			if (ninio.qi478e5  == null) {
				error = true;
				view = rgQI478E5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E5");
				return false;
			}
			if (ninio.qi478e6  == null) {
				error = true;
				view = rgQI478E6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E6");
				return false;
			}
			if (ninio.qi478e7  == null) {
				error = true;
				view = rgQI478E7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E7");
				return false;
			}
			if (ninio.qi478e8  == null) {
				error = true;
				view = rgQI478E8;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E8");
				return false;
			}
			if (ninio.qi478e9  == null) {
				error = true;
				view = rgQI478E9;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E9");
				return false;
			}		
			if (ninio.qi478e10  == null) {
				error = true;
				view = rgQI478E10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478E10");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	public void cambiarVariablesParalaBasedeDatos(){

		ninio.qi478e1=ninio.qi478e1!=null?ninio.setConvertconSieteVariables(ninio.qi478e1):null;
		ninio.qi478e2=ninio.qi478e2!=null?ninio.setConvertconTresVariables(ninio.qi478e2):null;
		ninio.qi478e3=ninio.qi478e3!=null?ninio.setConvertconTresVariables(ninio.qi478e3):null;
		ninio.qi478e4=ninio.qi478e4!=null?ninio.setConvertconTresVariables(ninio.qi478e4):null;
		ninio.qi478e5=ninio.qi478e5!=null?ninio.setConvertconTresVariables(ninio.qi478e5):null;
		ninio.qi478e6=ninio.qi478e6!=null?ninio.setConvertconTresVariables(ninio.qi478e6):null;
		ninio.qi478e7=ninio.qi478e7!=null?ninio.setConvertconTresVariables(ninio.qi478e7):null;
		ninio.qi478e8=ninio.qi478e8!=null?ninio.setConvertconTresVariables(ninio.qi478e8):null;
		ninio.qi478e9=ninio.qi478e9!=null?ninio.setConvertconTresVariables(ninio.qi478e9):null;
		ninio.qi478e10=ninio.qi478e10!=null?ninio.setConvertconSeisVariables(ninio.qi478e10):null;
	}
	
	private void cambiarVariablesParaMostraraUsuario(){

		ninio.qi478e1 =ninio.qi478e1 !=null?ninio.getConvertconSieteVariables(ninio.qi478e1):null;
		ninio.qi478e2 =ninio.qi478e2 !=null?ninio.getConvertconTresVariables(ninio.qi478e2):null;
		ninio.qi478e3 =ninio.qi478e3 !=null?ninio.getConvertconTresVariables(ninio.qi478e3):null;
		ninio.qi478e4 =ninio.qi478e4 !=null?ninio.getConvertconTresVariables(ninio.qi478e4):null;
		ninio.qi478e5 =ninio.qi478e5 !=null?ninio.getConvertconTresVariables(ninio.qi478e5):null;
		ninio.qi478e6 =ninio.qi478e6 !=null?ninio.getConvertconTresVariables(ninio.qi478e6):null;
		ninio.qi478e7 =ninio.qi478e7 !=null?ninio.getConvertconTresVariables(ninio.qi478e7):null;
		ninio.qi478e8 =ninio.qi478e8 !=null?ninio.getConvertconTresVariables(ninio.qi478e8):null;
		ninio.qi478e9 =ninio.qi478e9 !=null?ninio.getConvertconTresVariables(ninio.qi478e9):null;
		ninio.qi478e10=ninio.qi478e10!=null?ninio.getConvertconSeisVariables(ninio.qi478e10):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
		QI4781EChangeVelue();
	}
	public void QI4781EChangeVelue(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(personadis!=null && !Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registro como Discapacitado! ");
		}
	}
	
//	public void evaluarPregunta() {
//		
//		if(App.getInstance().getSeccion04B()!=null && (App.getInstance().getSeccion04DIT()==null || 
//		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=15 && App.getInstance().getSeccion04DIT().qi478 <=18) || 
//		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=30 && App.getInstance().getSeccion04DIT().qi478 <=36) ||
//		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=53 && App.getInstance().getSeccion04DIT().qi478 <=59)
//				) ) {
		
	public void evaluarPregunta() {		
		if(App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=9 && App.getInstance().getSeccion04DIT_02().qi478 <=12) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478E1,rgQI478E2,rgQI478E3,rgQI478E4,rgQI478E5,rgQI478E6,rgQI478E7,rgQI478E8,rgQI478E9,rgQI478E10);
			Log.e("11","22");
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
		else {
			Log.e("11","33");
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478E1,rgQI478E2,rgQI478E3,rgQI478E4,rgQI478E5,rgQI478E6,rgQI478E7,rgQI478E8,rgQI478E9,rgQI478E10);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
		}
	}
    public void abrirDetalle(DISCAPACIDAD modelo) {
		FragmentManager fm = C2SECCION_04BFragment_022DIT_1.this.getFragmentManager();
		C2SECCION_04BFragment_018DIT_1_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_1_Dialog.newInstance(this, modelo);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478E1,rgQI478E2,rgQI478E3,rgQI478E4,rgQI478E5,rgQI478E6,rgQI478E7,rgQI478E8,rgQI478E9,rgQI478E10);
			Log.e("11","44");
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Log.e("11","55");
			Util.lockView(getActivity(), false,rgQI478E1,rgQI478E2,rgQI478E3,rgQI478E4,rgQI478E5,rgQI478E6,rgQI478E7,rgQI478E8,rgQI478E9,rgQI478E10);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
		}
	}
	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478e1.setText("(NOMBRE)");		
		lblpregunta478e1.setText(lblpregunta478e1.getText().toString().replace(replace, nombre));	
		lblpregunta478e1.setText(Html.fromHtml("478E1. <b>MUESTRE LA CARTILLA 1.</b> <br>De estas figuras �<b>cu�l o cu�les</b> son las que "+lblpregunta478e1.getText()+" <b>generalmente realiza</b>?"));
		lblpregunta478e1_ind.setText(Html.fromHtml("LEA EN VOZ ALTA LA(S) OPCI�N(ES) ELEGIDA(S) Y <b>SELECCIONE LA DE MAYOR NUMERACI�N.</b>"));
		
		lblpregunta478e2.setText("(NOMBRE)");
		lblpregunta478e2.setText(lblpregunta478e2.getText().toString().replace(replace, nombre));
		lblpregunta478e2.setText(Html.fromHtml("478E2. En el lugar en el cual "+lblpregunta478e2.getText()+" <b>pasa mayor tiempo</b> �tiene un <b>espacio sin objetos</b> en el que pueda desplazarse/caminar libremente?<br> <br> DE SER NECESARIO PREGUNTE:<br> 1) �De qu� tama�o es?<br> 2) �C�mo es el piso de ese lugar? <br>"));
		lblpregunta478e2_ind.setText(Html.fromHtml("MARQUE S� CUANDO LA MADRE REPORTE <b>UN ESPACIO DE PISO FIRME</b> Y LIBRE DE OBJETOS DE 3 METROS LINEALES APROXIMADAMENTE"));
		
		lblpregunta478e3.setText("(NOMBRE)");
		lblpregunta478e3.setText(lblpregunta478e3.getText().toString().replace(replace, nombre));
		lblpregunta478e3.setText(Html.fromHtml("478E3. "+lblpregunta478e3.getText()+" �<b>trata</b> de <b>imitar las palabras</b> que escucha?"));
		lblpregunta478e3_ind.setText(Html.fromHtml("MARQUE S�, CUANDO LA MADRE REPORTE QUE SU HIJA/O EMITE <b>VOCALES</b> (A, O), <b>S�LABAS</b> (BA, TA) <b>O PALABRAS</b> (PAP�, MAM�) COMO <b>IMITACI�N O EN RESPUESTA A LAS VERBALIZACIONES DE OTRAS PERSONAS.</b>"));
		
		lblpregunta478e4.setText("(NOMBRE)");
		lblpregunta478e4.setText(lblpregunta478e4.getText().toString().replace(replace, nombre));
		lblpregunta478e4.setText(Html.fromHtml("478E4. "+lblpregunta478e4.getText()+" �<b>entiende</b> cuando usted le dice <b>�NO�</b> aunque no le haga caso?"));
		lblpregunta478e4_ind.setText(Html.fromHtml("REFIERE A SI LA (EL) NI�A(O) <b>ENTIENDE EL SIGNIFICADO</b> DE LA PALABRA �NO� "));
				
		lblpregunta478e5.setText("(NOMBRE)");
		lblpregunta478e5.setText(lblpregunta478e5.getText().toString().replace(replace, nombre));
		lblpregunta478e5.setText(Html.fromHtml("478E5. "+lblpregunta478e5.getText()+" �<b>entiende</b> una <b>orden sencilla</b> como por ejemplo �dame� o �toma� ?"));
					
		lblpregunta478e6.setText("(NOMBRE)");
		lblpregunta478e6.setText(lblpregunta478e6.getText().toString().replace(replace, nombre));
		lblpregunta478e6.setText(Html.fromHtml("478E6. Cuando est� con "+lblpregunta478e6.getText()+" �usted <b>le habla de lo que est�n haciendo</b> en ese momento?"));
		lblpregunta478e6_ind.setText(Html.fromHtml("SIEMPRE LEA: Por ejemplo, usted le dice: �estamos comiendo tu papita� <b>mientras comen</b>."));
		
		lblpregunta478e7.setText("(NOMBRE)");
		lblpregunta478e7.setText(lblpregunta478e7.getText().toString().replace(replace, nombre));
		lblpregunta478e7.setText(Html.fromHtml("478E7. Cuando <b>usted carga a</b> "+lblpregunta478e7.getText()+", ella(�l) <b>generalmente</b> �se tira hacia atr�s, se niega a que se le cargue, se pone tiesa(o) o la empuja a usted?"));
		
		lblpregunta478e8.setText("(NOMBRE)");
		lblpregunta478e8.setText(lblpregunta478e8.getText().toString().replace(replace, nombre));
		lblpregunta478e8.setText(Html.fromHtml("478E8. Cuando "+lblpregunta478e8.getText()+" <b>est� con usted</b>, ella(�l) <b>generalmente</b> �est� tensa(o), ansiosa(o), angustiada(o), indiferente o aburrida(o)?"));
		
		lblpregunta478e9.setText("(NOMBRE)");
		lblpregunta478e9.setText(lblpregunta478e9.getText().toString().replace(replace, nombre));
		lblpregunta478e9.setText(Html.fromHtml("478E9. "+lblpregunta478e9.getText()+" <b>generalmente</b> �es <b>impaciente, protesta y persiste</b> a menos que usted haga lo que ella(�l) quiere?"));
		lblpregunta478e9_ind.setText(Html.fromHtml("MARQUE S� CUANDO LA MADRE REPORTE QUE LA(EL) NI�A(O) ES IMPACIENTE Y PROTESTA Y PERSISTE <b>(LAS 3 CONDICIONES)</b>"));
		
		lblpregunta478e10.setText("(NOMBRE)");
		lblpregunta478e10.setText(lblpregunta478e10.getText().toString().replace(replace, nombre));
		lblpregunta478e10.setText(Html.fromHtml("478E10. Si "+lblpregunta478e10.getText()+" llora <b>cuando usted est� haciendo algo, generalmente</b> �qu� hace usted?"));
		lblpregunta478e10_ind.setText(Html.fromHtml("DE SER NECESARIO, LEA:<br>Por ejemplo, si usted est� lavando o hablando por tel�fono y su hija(o) llora, �qu� hace generalmente? <br><br>DE SER NECESARIO,PREGUNTE: <br>1) �qu� hace usted frente a lo que est� haciendo? <br>2) �qu� hace frente al llanto de su hija(o)?"));
		
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478E1.readOnly();
			rgQI478E2.readOnly();
			rgQI478E3.readOnly();
			rgQI478E4.readOnly();
			rgQI478E5.readOnly();
			rgQI478E6.readOnly();
			rgQI478E7.readOnly();
			rgQI478E9.readOnly();
			rgQI478E10.readOnly();			
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
