package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_014 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public SpinnerField spnQI469F; 
	@FieldAnnotation(orderIndex=2) 
	public IntegerField txtQI469G; 	
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI470; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQI470_O; 
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta469f,lblpregunta469f_ind,lblpregunta469g,lblpregunta470,lbltratamientoveces,lblpregunta469g_ind; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI469F","QI469G","QI470","QI470_O")}; 
	SeccionCapitulo[] seccionesCargado; 
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 gdTratamiento469g;
	public boolean filtro469h;
	public C2SECCION_04BFragment_014() {} 
	public C2SECCION_04BFragment_014 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 

  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		rango(getActivity(), txtQI469G, 0, 98, null, 99); 
		
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI469D_A","QI469D_B","QI469D_C","QI469D_D","QI469D_E","QI469D_F","QI469D_G","QI469D_H","QI469D_I","QI469D_J","QI469D_K","QI469D_L","QI469D_M","QI469D_N","QI469D_O","QI469D_P","QI469D_X","QI469F","QI469G","QI470","QI470_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();

	    lblpregunta469f = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469f);
	    lblpregunta469f_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi469f_ind).negrita();
	    lblpregunta469g = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi469g);
	    lblpregunta469g_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi469g_ind).negrita();
	    lblpregunta470 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi470);
	    
	    spnQI469F=new SpinnerField(getActivity()).size(altoComponente+15, 500).callback("validar469h");
	    txtQI469G=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
	    lbltratamientoveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04aqi469g_1).textSize(16).centrar();
	    
		gdTratamiento469g = new GridComponent2(this.getActivity(),App.ESTILO,2,0);
		gdTratamiento469g.addComponent(lbltratamientoveces);
		gdTratamiento469g.addComponent(txtQI469G);
	    
	  	rgQI470=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi470_1,R.string.c2seccion_04bqi470_2,R.string.c2seccion_04bqi470_3,R.string.c2seccion_04bqi470_4,R.string.c2seccion_04bqi470_5,R.string.c2seccion_04bqi470_6,R.string.c2seccion_04bqi470_7,R.string.c2seccion_04bqi470_8,R.string.c2seccion_04bqi470_9,R.string.c2seccion_04bqi470_10,R.string.c2seccion_04bqi470_11).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI470_O=new TextField(this.getActivity()).maxLength(100).size(altoComponente, 500); 
		rgQI470.agregarEspecifique(10,txtQI470_O); 
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta469f,lblpregunta469f_ind,spnQI469F); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta469g,lblpregunta469g_ind,gdTratamiento469g.component()); 
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta470,rgQI470); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi469f!=null) {
				c2seccion_04b.qi469f=c2seccion_04b.getConvertQi469f(c2seccion_04b.qi469f);
			}		
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if(App.getInstance().getSeccion04B().filtro469d!=null && App.getInstance().getSeccion04B().filtro469d>1) {
			if (Util.esVacio(c2seccion_04b.qi469f)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI469F"); 
				view = spnQI469F; 
				error = true; 
				return false; 
			} 
		}

		
		if(App.getInstance().getSeccion04B().qi469c!=null && App.getInstance().getSeccion04B().qi469c==1) {
				if (Util.esVacio(c2seccion_04b.qi469g)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI469G"); 
					view = txtQI469G; 
					error = true; 
					return false; 
				} 		
		}
		
		if(App.getInstance().getSeccion04B().qi469c==2 || filtro469h ) {
			if (Util.esVacio(c2seccion_04b.qi470)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI470"); 
				view = rgQI470; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04b.qi470,11)){ 
				if (Util.esVacio(c2seccion_04b.qi470_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI470_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi469f!=null) {
				c2seccion_04b.qi469f=c2seccion_04b.setConvertQi469f(c2seccion_04b.qi469f);
			}		
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		llenarCombo();
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();   
    	validar469c();    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void validar469c() {
    	if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi469c!=null){
	    	if(App.getInstance().getSeccion04B().qi469c!=1) {
	    		Util.cleanAndLockView(getActivity(),spnQI469F,txtQI469G);
	    		q1.setVisibility(View.GONE);
	    		q2.setVisibility(View.GONE);
	    		lblpregunta470.text(R.string.c2seccion_04bqi470a);
	    		renombrarEtiquetas();
	    		
//	    		MyUtil.LiberarMemoria();
	    	}
	    	else {
	    		Util.lockView(getActivity(),false,spnQI469F,txtQI469G);
	    		q1.setVisibility(View.VISIBLE);
	    		q2.setVisibility(View.VISIBLE);
	    		lblpregunta470.text(R.string.c2seccion_04bqi470);
	    		renombrarEtiquetas();
	    		validar469e();
	    		validar469h();
//	    		MyUtil.LiberarMemoria();
	    	} 
	    }
    }
    
    public void validar469e() {
    	if(App.getInstance().getSeccion04B().filtro469d>1) {
    		Util.lockView(getActivity(),false,spnQI469F);
    		q1.setVisibility(View.VISIBLE);
    	}
    	else {
    		Util.cleanAndLockView(getActivity(),spnQI469F);
    		q1.setVisibility(View.GONE);    		
    	}    	
    }
    
    public void validar469h() {
    	
    	boolean esCodCol2=false;
    	if(App.getInstance().getSeccion04B().filtro469d>1) {    	
    		
         String opcion = c2seccion_04b.getConvertQi469f(spnQI469F.getSelectedItemKey().toString());		
    	 
         if(opcion.equals("H")) {
    		 if(App.getInstance().getSeccion04B().qi469d_h==1)
    			 esCodCol2=true;
    	 }
    	 if(opcion.equals("K")) {
    		 if(App.getInstance().getSeccion04B().qi469d_k==1)
    			 esCodCol2=true;
    	 }
    	 if(opcion.equals("L")) {
    		 if(App.getInstance().getSeccion04B().qi469d_l==1)
    			 esCodCol2=true;
    	 }
    	 if(opcion.equals("M")) {
    		 if(App.getInstance().getSeccion04B().qi469d_m==1)
    			 esCodCol2=true;
    	 }
    	 if(opcion.equals("N")) {
    		 if(App.getInstance().getSeccion04B().qi469d_n==1)
    			 esCodCol2=true;
    	 }
    	 if(opcion.equals("X")) {
    		 if(App.getInstance().getSeccion04B().qi469d_x==1)
    			 esCodCol2=true;
    	 }
    	 	
    	 if(esCodCol2) {
    		Util.lockView(getActivity(),false,rgQI470);
     		q3.setVisibility(View.VISIBLE);
    	 }
    	 else {
    		 Util.cleanAndLockView(getActivity(),rgQI470);
    		 MyUtil.LiberarMemoria(); 
    		 q3.setVisibility(View.GONE);
    	 }    	 
    	}
    	else {    		
    		 if(
    		 App.getInstance().getSeccion04B().qi469d_h==1 ||
       		 App.getInstance().getSeccion04B().qi469d_k==1 ||
    	     App.getInstance().getSeccion04B().qi469d_l==1 ||
    	     App.getInstance().getSeccion04B().qi469d_m==1 ||
    	     App.getInstance().getSeccion04B().qi469d_n==1 ||
    		 App.getInstance().getSeccion04B().qi469d_x==1) {  
    			 esCodCol2=true;
    			 Util.lockView(getActivity(),false,rgQI470);
	      		 q3.setVisibility(View.VISIBLE);
    		 }  
    		 else {
    			 Util.cleanAndLockView(getActivity(),rgQI470);
    			 MyUtil.LiberarMemoria(); 
    	     	 q3.setVisibility(View.GONE);    	     		
    		 }    		
    	}
    	filtro469h = esCodCol2;
    }
    
    private void llenarCombo(){
    	List<Integer> valores = new ArrayList<Integer>();
    	valores.add(c2seccion_04b.qi469d_a);    	valores.add(c2seccion_04b.qi469d_b);
    	valores.add(c2seccion_04b.qi469d_c);    	valores.add(c2seccion_04b.qi469d_d);
    	valores.add(c2seccion_04b.qi469d_e);    	valores.add(c2seccion_04b.qi469d_f);
    	valores.add(c2seccion_04b.qi469d_g);    	valores.add(c2seccion_04b.qi469d_h);
    	valores.add(c2seccion_04b.qi469d_i);    	valores.add(c2seccion_04b.qi469d_j);
    	valores.add(c2seccion_04b.qi469d_k);    	valores.add(c2seccion_04b.qi469d_l);
    	valores.add(c2seccion_04b.qi469d_m);    	valores.add(c2seccion_04b.qi469d_n);
    	valores.add(c2seccion_04b.qi469d_o);    	valores.add(c2seccion_04b.qi469d_p);
    	valores.add(c2seccion_04b.qi469d_x);
    	
    	List<SegmentacionFiltro> lista =new ArrayList<SegmentacionFiltro>();
    	SegmentacionFiltro obj = new SegmentacionFiltro();
    	obj.id=null;
    	obj.nombre="--SELECCIONE--";
    	lista.add(obj);
    	for(int i = 0;i<valores.size(); i++) {
    		if(valores.get(i)!=null && valores.get(i)==1) {
    			obj = new SegmentacionFiltro();
    			obj.id=i+1;  
    
    			 if(i==0)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_a);
    			else if(i==1)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_b);
    			else if(i==2)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_c);
    			else if(i==3)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_d);
    			else if(i==4)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_e);
    			else if(i==5)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_f);
    			else if(i==6)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_g);
    			else if(i==7)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_h);
    			else if(i==8)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_i);
    			else if(i==9)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_j);
    			else if(i==10)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_k);
    			else if(i==11)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_l);
    			else if(i==12)
    				obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_m);
    			else if(i==13)
        			obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_n);
    			else if(i==14)
        			obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_o);
    			else if(i==15)
        			obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_p);
    			else if(i==16)
        			obj.nombre=getResources().getString(R.string.c2seccion_04bqi469d_x);
    			if(obj.nombre!=null)
    				lista.add(obj);
    		}
    	}
    	if(lista.size()>0) {
			EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(getActivity(), android.R.layout.simple_spinner_item, lista);
			List<Object> keysAdapter = new ArrayList<Object>();
			for (int i = 0; i < lista.size(); i++) {
				SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
				keysAdapter.add(d.id+"");
			}
			spnQI469F.setAdapterWithKey(spinnerAdapter, keysAdapter);
    	}
    } 
    
    public void ValidarsiesSupervisora(){
    	Integer codigo=App.getInstance().getUsuario().cargo_id;
    	if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
    		rgQI470.readOnly();
    		txtQI469G.readOnly();
    		txtQI470_O.readOnly();
    	}
    }
	
	public void renombrarEtiquetas() {	
    	lblpregunta469g.setText(lblpregunta469g.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta470.setText(lblpregunta470.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
	uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi469f!=null) {
				c2seccion_04b.qi469f=c2seccion_04b.getConvertQi469f(c2seccion_04b.qi469f);
			}		
		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
