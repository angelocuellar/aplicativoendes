package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_016 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI473; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI473A; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI473BA; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI473BB; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI473BC; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI473C; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI473D_A; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI473D_B; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI473D_C ; 
	@FieldAnnotation(orderIndex=0) 
	public CheckBoxField chbQI473D_D; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI473D_E; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI473D_F; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI473D_X; 
	@FieldAnnotation(orderIndex=13) 
	public TextField txtQI473D_XO; 
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta473,lblpregunta473a,lblpregunta473b,lblpregunta473c,lblpregunta473d,lblpregunta473d_ind,
			lblblanco,lblP473b_1,lblP473b_2,lblP473b_3,lblQI473bA,lblQI473bA2,lblQI473bB,lblQI473bB2,lblQI473bC,lblQI473bC2; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI473","QI473A","QI473BA","QI473BB","QI473BC","QI473C","QI473D_A","QI473D_B","QI473D_C","QI473D_D","QI473D_E","QI473D_F","QI473D_X","QI473D_XO")}; 
	SeccionCapitulo[] seccionesCargado; 

	public GridComponent2 grid1,grid2,grid3,grid4;
	String nombre_persona;
	public TextField txtCabecera;
	public C2SECCION_04BFragment_016() {} 
	public C2SECCION_04BFragment_016 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
			
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI473","QI473A","QI473BA","QI473BB","QI473BC","QI473C","QI473D_A","QI473D_B","QI473D_C","QI473D_D","QI473D_E","QI473D_F","QI473D_X","QI473D_XO","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta473 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473);
	    lblpregunta473a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473a);	    
	    lblpregunta473b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473b);
	    lblpregunta473c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473c);
	    lblpregunta473d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi473d);
	    lblpregunta473d_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi473d_ind);
	    
	    rgQI473=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473_1,R.string.c2seccion_04bqi473_2,R.string.c2seccion_04bqi473_3,R.string.c2seccion_04bqi473_4,R.string.c2seccion_04bqi473_5,R.string.c2seccion_04bqi473_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI473A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473a_1,R.string.c2seccion_04bqi473a_2,R.string.c2seccion_04bqi473a_3,R.string.c2seccion_04bqi473a_4,R.string.c2seccion_04bqi473a_5,R.string.c2seccion_04bqi473a_6).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
				
		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP473b_1 = new LabelComponent(getActivity()).textSize(16).size(50, 99).text(R.string.c2seccion_04bqi473b_1).centrar();
		lblP473b_2 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi473b_2).centrar();
		lblP473b_3 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi473b_3).centrar();
		
		
		lblQI473bA=new LabelComponent(getActivity()).textSize(17).size(altoComponente+35, 450).text(R.string.c2seccion_04bqi473b_a);		
		rgQI473BA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473b_o1,R.string.c2seccion_04bqi473b_o2,R.string.c2seccion_04bqi473b_o3).size(altoComponente+35,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
				
		lblQI473bB=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi473b_b);	
		rgQI473BB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473b_o1,R.string.c2seccion_04bqi473b_o2,R.string.c2seccion_04bqi473b_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
		
		lblQI473bC=new LabelComponent(getActivity()).textSize(17).size(altoComponente+35, 450).text(R.string.c2seccion_04bqi473b_c);
		rgQI473BC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473b_o1,R.string.c2seccion_04bqi473b_o2,R.string.c2seccion_04bqi473b_o3).size(altoComponente+35,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
				
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI473bA);
		grid1.addComponent(rgQI473BA);
		
		grid1.addComponent(lblQI473bB);
		grid1.addComponent(rgQI473BB);
		
		grid1.addComponent(lblQI473bC);
		grid1.addComponent(rgQI473BC);
		
		
		rgQI473C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi473c_1,R.string.c2seccion_04bqi473c_2,R.string.c2seccion_04bqi473c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI473cChangeValue"); 
		
		chbQI473D_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI473daChangeValue");
		chbQI473D_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI473D_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI473D_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI473D_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI473D_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI473D_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi473d_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI473dxChangeValue");
		
		txtQI473D_XO =new TextField(this.getActivity()).size(altoComponente, 500).maxLength(100);
  } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta473,rgQI473); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta473a,rgQI473A); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta473b,grid1.component()); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta473c,rgQI473C); 
		LinearLayout ly463d = new LinearLayout(getActivity());
		ly463d.addView(chbQI473D_X);
		ly463d.addView(txtQI473D_XO);
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta473d,lblpregunta473d_ind,chbQI473D_A,chbQI473D_B,chbQI473D_C,chbQI473D_D,chbQI473D_E,chbQI473D_F,ly463d); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);
		form.addView(q5);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi473!=null) {
				c2seccion_04b.qi473=c2seccion_04b.getConvertQi473(c2seccion_04b.qi473);
			}	
			
			if (c2seccion_04b.qi473a!=null) {
				c2seccion_04b.qi473a=c2seccion_04b.getConvertQi473a(c2seccion_04b.qi473a);
			}	
			
			if (c2seccion_04b.qi473ba!=null) {
				c2seccion_04b.qi473ba=c2seccion_04b.getConvertQi473b_a(c2seccion_04b.qi473ba);
			}	
			
			if (c2seccion_04b.qi473bb!=null) {
				c2seccion_04b.qi473bb=c2seccion_04b.getConvertQi473b_b(c2seccion_04b.qi473bb);
			}	
			
			if (c2seccion_04b.qi473bc!=null) {
				c2seccion_04b.qi473bc=c2seccion_04b.getConvertQi473b_c(c2seccion_04b.qi473bc);
			}	
			
			if (c2seccion_04b.qi473c!=null) {
				c2seccion_04b.qi473c=c2seccion_04b.getConvertQi473c(c2seccion_04b.qi473c);
			}				
		}	
		
		App.getInstance().getSeccion04B().qi473c = c2seccion_04b.qi473c;
		App.getInstance().getSeccion04B().qi473d_a = c2seccion_04b.qi473d_a;
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		
		if (Util.esVacio(c2seccion_04b.qi473)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473"); 
			view = rgQI473; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi473a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473A"); 
			view = rgQI473A; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi473ba)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473BA"); 
			view = rgQI473BA; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi473bb)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473BB"); 
			view = rgQI473BB; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi473bc)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473BC"); 
			view = rgQI473BC; 
			error = true; 
			return false; 
		} 
		if (Util.esVacio(c2seccion_04b.qi473c)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI473C"); 
			view = rgQI473C; 
			error = true; 
			return false; 
		} 
		if(c2seccion_04b.qi473c==1) {
			if(!verificarCheck473d()) {
				mensaje = "Seleccione una opci�n en P.473D"; 
				view = chbQI473D_A; 
				error = true; 
				return false; 
			}
			
			
			if (chbQI473D_X.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi473d_xo)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI473D_XO; 
					error = true; 
					return false; 
				}
			}	
		}
			
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi473!=null) {
				c2seccion_04b.qi473=c2seccion_04b.setConvertQi473(c2seccion_04b.qi473);
			}	
			
			if (c2seccion_04b.qi473a!=null) {
				c2seccion_04b.qi473a=c2seccion_04b.setConvertQi473a(c2seccion_04b.qi473a);
			}	
			
			if (c2seccion_04b.qi473ba!=null) {
				c2seccion_04b.qi473ba=c2seccion_04b.setConvertQi473b_a(c2seccion_04b.qi473ba);
			}	
			
			if (c2seccion_04b.qi473bb!=null) {
				c2seccion_04b.qi473bb=c2seccion_04b.setConvertQi473b_b(c2seccion_04b.qi473bb);
			}	
			
			if (c2seccion_04b.qi473bc!=null) {
				c2seccion_04b.qi473bc=c2seccion_04b.setConvertQi473b_c(c2seccion_04b.qi473bc);
			}	
			
			if (c2seccion_04b.qi473c!=null) {
				c2seccion_04b.qi473c=c2seccion_04b.setConvertQi473c(c2seccion_04b.qi473c);
			}	
			
		}
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	evaluaPregunta();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluaPregunta() {
    	if(App.getInstance().getSeccion04B().qi472!=null) {
	    	if(App.getInstance().getSeccion04B().qi472!=1) {
	    		Util.cleanAndLockView(getActivity(), rgQI473,rgQI473A,rgQI473BA,rgQI473BB,rgQI473BC, rgQI473C,chbQI473D_A,chbQI473D_B,chbQI473D_C,chbQI473D_D,chbQI473D_E,chbQI473D_F,chbQI473D_X);
	    		q1.setVisibility(View.GONE);
	    		q2.setVisibility(View.GONE);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		q5.setVisibility(View.GONE);
	    	}
	    	else {
	    		Util.lockView(getActivity(),false, rgQI473,rgQI473A,rgQI473BA,rgQI473BB,rgQI473BC, rgQI473C,chbQI473D_A,chbQI473D_B,chbQI473D_C,chbQI473D_D,chbQI473D_E,chbQI473D_F,chbQI473D_X);
	    		q1.setVisibility(View.VISIBLE);
	    		q2.setVisibility(View.VISIBLE);
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    		q5.setVisibility(View.VISIBLE);
	    		
	    		onqrgQI473cChangeValue();
	        	onqrgQI473daChangeValue();
	        	onqrgQI473dxChangeValue();
	    	}
    	}
    	else {
    		onqrgQI473cChangeValue();
        	onqrgQI473daChangeValue();
        	onqrgQI473dxChangeValue();
    	}
    	
    }
    
 
	
	public boolean verificarCheck473d() {
  		if (chbQI473D_A.isChecked() || chbQI473D_B.isChecked() || chbQI473D_C.isChecked() || chbQI473D_D.isChecked() || 
  				chbQI473D_E.isChecked() || chbQI473D_F.isChecked() || chbQI473D_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}			
	
	public void onqrgQI473dxChangeValue() {  	
		
		if (chbQI473D_X.isChecked()) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
			Util.lockView(getActivity(),false,txtQI473D_XO);      		
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI473D_XO);    		
    		//Util.lockView(getActivity(),false,chbQI407_Y);  				
    	}	
    }
	
	public void onqrgQI473daChangeValue() {  	
		
		if (chbQI473D_A.isChecked()) {    		
			App.getInstance().getSeccion04B().qi473d_a =1 ;      		
    	} 
    	else {	
    		App.getInstance().getSeccion04B().qi473d_a =0 ;
    	}	
    }
	
	
	
	public void onqrgQI473cChangeValue() {		
		if(rgQI473C.getValue()!=null) {
			if (rgQI473C.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,chbQI473D_A,chbQI473D_B,chbQI473D_C,chbQI473D_D,chbQI473D_E,chbQI473D_F,chbQI473D_X,txtQI473D_XO);  
	    		App.getInstance().getSeccion04B().qi473c =1 ;
				q5.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI473D_A,chbQI473D_B,chbQI473D_C,chbQI473D_D,chbQI473D_E,chbQI473D_F,chbQI473D_X,txtQI473D_XO);
	    		if(rgQI473C.getValue()!=null)
	    			App.getInstance().getSeccion04B().qi473c =Integer.parseInt(rgQI473C.getValue().toString()) ;
	    		q5.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	
	public void renombrarEtiquetas()
    {	
    	lblpregunta473.setText(lblpregunta473.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta473a.setText(lblpregunta473a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta473b.setText(lblpregunta473b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta473c.setText(lblpregunta473c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta473d.setText(lblpregunta473d.getText().toString().replace("(NOMBRE)", nombre_persona));    	
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI473.readOnly();
			rgQI473A.readOnly();
			rgQI473BA.readOnly();
			rgQI473BB.readOnly();
			rgQI473BC.readOnly();
			rgQI473C.readOnly();
			txtQI473D_XO.readOnly();
			chbQI473D_A.readOnly();
			chbQI473D_B.readOnly();
			chbQI473D_C.readOnly();
			chbQI473D_D.readOnly();
			chbQI473D_E.readOnly();
			chbQI473D_F.readOnly();
			chbQI473D_X.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi473!=null) {
				c2seccion_04b.qi473=c2seccion_04b.getConvertQi473(c2seccion_04b.qi473);
			}	
			
			if (c2seccion_04b.qi473a!=null) {
				c2seccion_04b.qi473a=c2seccion_04b.getConvertQi473a(c2seccion_04b.qi473a);
			}	
			
			if (c2seccion_04b.qi473ba!=null) {
				c2seccion_04b.qi473ba=c2seccion_04b.getConvertQi473b_a(c2seccion_04b.qi473ba);
			}	
			
			if (c2seccion_04b.qi473bb!=null) {
				c2seccion_04b.qi473bb=c2seccion_04b.getConvertQi473b_b(c2seccion_04b.qi473bb);
			}	
			
			if (c2seccion_04b.qi473bc!=null) {
				c2seccion_04b.qi473bc=c2seccion_04b.getConvertQi473b_c(c2seccion_04b.qi473bc);
			}	
			
			if (c2seccion_04b.qi473c!=null) {
				c2seccion_04b.qi473c=c2seccion_04b.getConvertQi473c(c2seccion_04b.qi473c);
			}				
		}	
		
		App.getInstance().getSeccion04B().qi473c = c2seccion_04b.qi473c;
		App.getInstance().getSeccion04B().qi473d_a = c2seccion_04b.qi473d_a;
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
