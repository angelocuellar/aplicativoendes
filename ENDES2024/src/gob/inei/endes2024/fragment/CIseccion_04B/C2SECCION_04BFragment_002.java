package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_002 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI454; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI455; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI456X;
	@FieldAnnotation(orderIndex=4) 
	public TextAreaField txtQI456X_O;
	@FieldAnnotation(orderIndex=5) 
	public IntegerField txtQI456N; 
	@FieldAnnotation(orderIndex=6) 
	public TextField txtQI456D; 
	@FieldAnnotation(orderIndex=7) 
	public TextField txtQI456M; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI456Y; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI457;
	
	public CheckBoxField cbQI1;
	
	public CheckBoxField cbQI2;
	
	public CheckBoxField cbQI3;
	
	public CheckBoxField cbQI4;
	
	public CheckBoxField cbQI5;
	
	public CheckBoxField cbQI6;
	
//	public CheckBoxField cbQI7;
	
	
	public IntegerField txtQID1,txtQIM1,txtQIA1,txtQID2,txtQIM2,txtQIA2,txtQID3,txtQIM3,txtQIA3,txtQID4,txtQIM4,txtQIA4,
		   txtQID5,txtQIM5,txtQIA5,txtQID6,txtQIM6,txtQIA6;
	
	public GridComponent2 grid1,grid2,grid3,grid4,grid5,grid6,grid7;
	
	CISECCION_04B c2seccion_04b; 
	CISECCION_02 ninio;
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta454,lblpregunta455,lblpregunta456x,lblP456x_1,lblP456x_2,lblP456x_3,lblP456x_4,lblpregunta457,lblpregunta456x_ind,lblpregunta457_ind,lblqi456x_o; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4;
	SeccionCapitulo[] seccionesGrabado,seccionesGrabado2,seccionesCargadoNinio; 
	SeccionCapitulo[] seccionesCargado,seccionesCargado2; 

	public TextField txtCabecera;
	public String nombre_persona;
	LinearLayout form;
	public C2SECCION_04BFragment_002() {} 
	public C2SECCION_04BFragment_002 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI454","QI455","QI456X","QI456X_O","QI457","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI454","QI455","QI456X","QI456X_O","QI457")};
		seccionesCargadoNinio = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI212","QI215D","QI215M","QI215Y","ID","HOGAR_ID","PERSONA_ID")};
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"INDICE","DIA","MES","ANIO","PREGUNTA","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
		seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"INDICE","DIA","MES","ANIO","PREGUNTA")};
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	     
	    lblpregunta454 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi454);
	    lblpregunta455 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi455);
	    lblpregunta456x = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04bqi456x);
	    lblpregunta456x_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi456x_ind).negrita();
	    lblpregunta457 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi457);
	    lblpregunta457_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04bqi457_ind);
	    
		rgQI454=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi454_1,R.string.c2seccion_04bqi454_2,R.string.c2seccion_04bqi454_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI454hChangeValue"); 
		rgQI455=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi455_1,R.string.c2seccion_04bqi455_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI455ChangeValue"); 
		rgQI456X=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi456x_1,R.string.c2seccion_04bqi456x_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI456xChangeValue"); 
		lblqi456x_o  = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04bqi456x_o).negrita();
		txtQI456X_O = new TextAreaField(getActivity()).maxLength(1000).size(180, 650).alfanumerico();
		
		cbQI1=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o1, "1:0").size(altoComponente, 150);
		cbQI1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				  onqrgQI456x1ChangeValue();
			}
		}); 
		cbQI2=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o2, "2:0").size(altoComponente, 150);
		cbQI2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				onqrgQI456x2ChangeValue();
			}
		}); 
		cbQI3=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o3, "3:0").size(altoComponente, 150);
		cbQI3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				onqrgQI456x3ChangeValue();
			}
		}); 
		cbQI4=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o4, "4:0").size(altoComponente, 150);
		cbQI4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				onqrgQI456x4ChangeValue();
			}
		}); 
		cbQI5=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o5, "5:0").size(altoComponente, 150);
		cbQI5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				onqrgQI456x5ChangeValue();
			}
		}); 
		cbQI6=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi456x_o6, "6:0").size(altoComponente, 150);
		cbQI6.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				onqrgQI456x6ChangeValue();
			}
		}); 

		lblP456x_1 = new LabelComponent(getActivity()).textSize(16).size(50, 150).text(R.string.c2seccion_04bqi456x_c1).centrar().negrita();
		lblP456x_2 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi456x_c2).centrar().negrita();
		lblP456x_3 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi456x_c3).centrar().negrita();
		lblP456x_4 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi456x_c4).centrar().negrita();
				
		
		txtQID1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID1.getText()!=null)
					if(!txtQID1.getText().toString().isEmpty()) {
						Integer id1 = Integer.parseInt(txtQID1.getText().toString());
						if(id1==44 || id1 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM1,txtQIA1);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM1,txtQIA1);
						}
						txtQIM1.requestFocus();					
					}
			}
		});
		txtQIM1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM1.getText()!=null)
					if(!txtQIM1.getText().toString().isEmpty())
						txtQIA1.requestFocus();
			}
		});
		txtQIA1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA1.getText()!=null)
					if(!txtQIA1.getText().toString().isEmpty()){
						cbQI2.requestFocus();
						if(txtQID1.getValue()!=null && txtQIM1.getValue()!=null){
							Integer anio=Integer.parseInt(txtQIA1.getText().toString());
							Integer mes =Integer.parseInt(txtQIM1.getText().toString())-1;
							Integer dia =Integer.parseInt(txtQID1.getText().toString()); 
							Calendar fechareferencia = new GregorianCalendar(anio, mes, dia);
							Calendar nacimiento = new GregorianCalendar(ninio.qi215y,Integer.parseInt(ninio.qi215m.toString())-1, Integer.parseInt(ninio.qi215d.toString()));
							Integer edad =MyUtil.CalcularEdadEnMesesFelix(nacimiento, fechareferencia);
							if(edad<6){
								MyUtil.MensajeGeneral(getActivity() , "Verificar fecha ingresada, ni�o menor de 6 meses");
							}
						}
					}
			}
		});
		
		txtQID2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID2.getText()!=null)
					if(!txtQID2.getText().toString().isEmpty()) {
						Integer id2 = Integer.parseInt(txtQID2.getText().toString());
						if(id2==44 || id2 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM2,txtQIA2);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM2,txtQIA2);
						}
						txtQIM2.requestFocus();
					}
			}
		});
		txtQIM2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM2.getText()!=null)
					if(!txtQIM2.getText().toString().isEmpty())
						txtQIA2.requestFocus();
			}
		});
		txtQIA2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA2.getText()!=null)
					if(!txtQIA2.getText().toString().isEmpty())
						cbQI3.requestFocus();
			}
		});
		
		
		txtQID3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID3.getText()!=null)
					if(!txtQID3.getText().toString().isEmpty()) {
						Integer id3 = Integer.parseInt(txtQID3.getText().toString());
						if(id3==44 || id3 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM3,txtQIA3);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM3,txtQIA3);
						}
						txtQIM3.requestFocus();
						
					}
			}
		});
		txtQIM3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM3.getText()!=null)
					if(!txtQIM3.getText().toString().isEmpty())
						txtQIA3.requestFocus();
			}
		});
		txtQIA3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA3.getText()!=null)
					if(!txtQIA3.getText().toString().isEmpty())
						cbQI4.requestFocus();
			}
		});
		
		
		txtQID4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID4.getText()!=null)
					if(!txtQID4.getText().toString().isEmpty()) {
						Integer id4 = Integer.parseInt(txtQID4.getText().toString());
						if(id4==44 || id4 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM4,txtQIA4);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM4,txtQIA4);
						}
						txtQIM4.requestFocus();
					}
			}
		});
		
				
		txtQIM4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM4.getText()!=null)
					if(!txtQIM4.getText().toString().isEmpty())
						txtQIA4.requestFocus();
			}
		});
		
		txtQIA4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA4.getText()!=null)
					if(!txtQIA4.getText().toString().isEmpty())
						cbQI5.requestFocus();
			}
		});
		txtQID5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID5.getText()!=null)
					if(!txtQID5.getText().toString().isEmpty()) {
						Integer id5 = Integer.parseInt(txtQID5.getText().toString());
						if(id5==44 || id5 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM5,txtQIA5);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM5,txtQIA5);
						}
						txtQIM5.requestFocus();
					}
			}
		});
		txtQIM5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM5.getText()!=null)
					if(!txtQIM5.getText().toString().isEmpty())
						txtQIA5.requestFocus();
			}
		});
		txtQIA5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA5.getText()!=null)
					if(!txtQIA5.getText().toString().isEmpty())
						cbQI6.requestFocus();
			}
		});
		txtQID6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID6.getText()!=null)
					if(!txtQID6.getText().toString().isEmpty()) {
						Integer id6 = Integer.parseInt(txtQID6.getText().toString());
						if(id6==44 || id6 ==66) {
							Util.cleanAndLockView(getActivity(), txtQIM6,txtQIA6);
						}
						else {
							Util.lockView(getActivity(),false, txtQIM6,txtQIA6);
						}
						txtQIM6.requestFocus();
					}
			}
		});
		txtQIM6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM6.getText()!=null)
					if(!txtQIM6.getText().toString().isEmpty())
						txtQIA6.requestFocus();
			}
		});
		
		txtQIA6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA6.getText()!=null)
					if(!txtQIA6.getText().toString().isEmpty())
						rgQI457.requestFocus();
			}
		});
				
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,4);
		grid1.addComponent(lblP456x_1);
		grid1.addComponent(lblP456x_2);
		grid1.addComponent(lblP456x_3);
		grid1.addComponent(lblP456x_4);
		
		grid1.addComponent(cbQI1);
		grid1.addComponent(txtQID1);
		grid1.addComponent(txtQIM1);
		grid1.addComponent(txtQIA1);
		
		
		grid1.addComponent(cbQI2);
		grid1.addComponent(txtQID2);
		grid1.addComponent(txtQIM2);
		grid1.addComponent(txtQIA2);
		
		
		grid1.addComponent(cbQI3);
		grid1.addComponent(txtQID3);
		grid1.addComponent(txtQIM3);
		grid1.addComponent(txtQIA3);
	
		
		grid1.addComponent(cbQI4);
		grid1.addComponent(txtQID4);
		grid1.addComponent(txtQIM4);
		grid1.addComponent(txtQIA4);
				
		
		grid1.addComponent(cbQI5);
		grid1.addComponent(txtQID5);
		grid1.addComponent(txtQIM5);
		grid1.addComponent(txtQIA5);
		
		
		grid1.addComponent(cbQI6);
		grid1.addComponent(txtQID6);
		grid1.addComponent(txtQIM6);
		grid1.addComponent(txtQIA6);
		
		LabelComponent lbl457_1_detalle = new LabelComponent(getActivity()).size(WRAP_CONTENT, WRAP_CONTENT);
		lbl457_1_detalle.setText(R.string.c2seccion_04bqi457_1_detalle);
		rgQI457=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi457_1,R.string.c2seccion_04bqi457_2,R.string.c2seccion_04bqi457_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("onqrgQI457ChangeValue");
		rgQI457.addView(lbl457_1_detalle, 1);
  }
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta454,rgQI454); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta455,rgQI455); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta456x,lblpregunta456x_ind,rgQI456X,grid1.component(),lblqi456x_o,txtQI456X_O);
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta457,lblpregunta457_ind,rgQI457);

		ScrollView contenedor = createForm(); 
		form = (LinearLayout) contenedor.getChildAt(0); 
		return contenedor; 
    }
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null) {
			if (c2seccion_04b.qi457!=null) {
				c2seccion_04b.qi457=c2seccion_04b.getConvertQi457(c2seccion_04b.qi457);
			}
		}
		if(App.getInstance().getSeccion04B()!=null) {
			App.getInstance().getSeccion04B().qi454 =c2seccion_04b.qi454;
			App.getInstance().getSeccion04B().qi455 =c2seccion_04b.qi455;
			App.getInstance().getSeccion04B().qi456x =c2seccion_04b.qi456x;
			App.getInstance().getSeccion04B().qi457 =c2seccion_04b.qi457;
		}
		else {
			App.getInstance().setSeccion04B(c2seccion_04b);
		}		
		
		SeccionCapitulo[] seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215Y","QI215M","QI215D","QI216","QI217CONS","QI218") };
		
		CISECCION_02 nacimiento =  getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesPersonas);
		
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
		Calendar fecharef = new GregorianCalendar();
		
		if(nacimiento.qi216==1) {
		String fechareferencia = nacimiento.qi217cons;
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}			
		}
		
		boolean val215=false;
		
		if(MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef) >= 4) {
			val215= true;
		}
		
		App.getInstance().getSeccion04B().filtro215 = val215;
		App.getInstance().getSeccion04B().qi216 = nacimiento.qi216;
		App.getInstance().getSeccion04B().qi218 = nacimiento.qi218;
		
//		Log.e("","EDAD: "+MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef));
//		Log.e("","val215: "+val215 );
//		Log.e("","filtro215: "+App.getInstance().getSeccion04B().filtro215 );
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				//if(verificarCheck456x()) {
				getCuestionarioService().borrar04b(App.getInstance().getNacimientoVivos().id,App.getInstance().getNacimientoVivos().hogar_id,
				App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, "456x");
				grabarDetalle();
				
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_003.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI461B","QI461CU","QI461CN","QI461D","QI461E","QI461F","QI461G","QI461H","QI461I","QI461J","QI462","QI462A","QI462B","QI463","QI463A","QI463B");					
					App.getInstance().getSeccion04B().qi458 = null;
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
				
				//}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		}
		if(App.getInstance().getPersonaCuestionarioIndividual()!=null){
			Integer total= getCuestionarioService().getNiniosConCarnetbyPersonaId(c2seccion_04b.id,c2seccion_04b.hogar_id,c2seccion_04b.persona_id);
			App.getInstance().getPersonaCuestionarioIndividual().viotartarjeta=total>0?true:false;
		}
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		if (Util.esVacio(c2seccion_04b.qi454)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI454"); 
			view = rgQI454; 
			error = true; 
			return false; 
		} 
		
		if(c2seccion_04b.qi454==3) {
			if (Util.esVacio(c2seccion_04b.qi455)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI455"); 
				view = rgQI455; 
				error = true; 
				return false; 
			} 
		}
		
		if(c2seccion_04b.qi454==1) {
			if (Util.esVacio(c2seccion_04b.qi456x)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI456X"); 
				view = rgQI456X; 
				error = true; 
				return false; 
			} 
		
			if(c2seccion_04b.qi456x==1 && !verificarCheck456x()) {
				mensaje = "Debe seleccionar al menos una opci�n"; 
				view = cbQI1; 
				error = true; 
				return false;
			}		
		}		
		
		Calendar cal2 = Calendar.getInstance();
				
		if(cbQI1.isChecked()) {
			
			if (Util.esVacio(txtQID1.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA1"); 
				view = txtQID1; 
				error = true; 
				return false; 
			} 
			
			int id1 = Integer.parseInt(txtQID1.getText().toString());
			
			if (!((id1>=1 && id1<=31) || id1==98 || id1==44 || id1==66) ) {
				error = true;
				view = txtQID1;
				mensaje = "Valor del campo invalido 1";
				return false;		
			}
			
			if (!(id1==44 || id1==66) ) {
			
				if (Util.esVacio(txtQIM1.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES1"); 
					view = txtQIM1; 
					error = true; 
					return false; 
				} 
				
				if (Util.esVacio(txtQIA1.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O1"); 
					view = txtQIA1; 
					error = true; 
					return false; 
				} 
				
				int im1 = Integer.parseInt(txtQIM1.getText().toString());
				
				
				if (!(im1>=1 && im1<=12)) {
					error = true;
					view = txtQIM1;
					mensaje = "Valor del campo invalido 2";
					return false;
				}
				
				if(id1!=98) {
					if(!MyUtil.DiaCorrespondeAlMes(id1, im1)){
						mensaje = "d�a no corresponde al mes"; 
						view = txtQID1; 
						error = true; 
						return false; 
					}		
				}
				
				int ia1 = Integer.parseInt(txtQIA1.getText().toString());
				
				
				if(!(ia1>=cal2.get(Calendar.YEAR)-5 && ia1<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA1;
					mensaje = "Valor del campo fuera de rango";
					return false;
				}  		
				
			}
			
			//int id1 = Integer.parseInt(txtQID1.getText().toString());
			
			
		}
		
		
		if(cbQI2.isChecked()) {
			
			if (Util.esVacio(txtQID2.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA2"); 
				view = txtQID2; 
				error = true; 
				return false; 
			} 
			
			int id2 = Integer.parseInt(txtQID2.getText().toString());

			if (!(id2>=1 && id2<=31 || id2==98 || id2==44 || id2==66 ) ) {
				error = true;
				view = txtQID2;
				mensaje = "Valor del campo invalido 3";
				return false;		
			}
			
			if (!(id2==44 || id2==66) ) {
			
			if (Util.esVacio(txtQIM2.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIMES2"); 
				view = txtQIM2; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(txtQIA2.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIA�O2"); 
				view = txtQIA2; 
				error = true; 
				return false; 
			} 			
			
			
			int im2 = Integer.parseInt(txtQIM2.getText().toString());
			
			if (!(im2>=1 && im2<=12)) {
				error = true;
				view = txtQIM2;
				mensaje = "Valor del campo invalido 4";
				return false;
			}
			
			if(id2!=98) {
				if(!MyUtil.DiaCorrespondeAlMes(id2, im2)){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQID2; 
					error = true; 
					return false; 
				}
			}
			
			int ia2 = Integer.parseInt(txtQIA2.getText().toString());
			
			if(!(ia2>=cal2.get(Calendar.YEAR)-5 && ia2<=cal2.get(Calendar.YEAR)) ) {
				error = true;
				view = txtQIA2;
				mensaje = "Valor del campo fuera de rango 5";
				return false;
			}  	
			
			}
		}
		
		if(cbQI3.isChecked()) {
			
			if (Util.esVacio(txtQID3.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA3"); 
				view = txtQID3; 
				error = true; 
				return false; 
			} 
			
			int id3 = Integer.parseInt(txtQID3.getText().toString());
			if (!(id3>=1 && id3<=31 || id3==98 || id3==44 || id3==66 ) ) {
				error = true;
				view = txtQID3;
				mensaje = "Valor del campo invalido 6";
				return false;		
			}
			
			if (!(id3==44 || id3==66) ) {
					
			if (Util.esVacio(txtQIM3.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIMES3"); 
				view = txtQIM3; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(txtQIA3.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIA�O3"); 
				view = txtQIA3; 
				error = true; 
				return false; 
			} 
						
			int im3 = Integer.parseInt(txtQIM3.getText().toString());
			
			if (!(im3>=1 && im3<=12)) {
				error = true;
				view = txtQIM3;
				mensaje = "Valor del campo invalido 7";
				return false;
			}		
			
			if(id3!=98) {
				if(!MyUtil.DiaCorrespondeAlMes(id3, im3)){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQID3; 
					error = true; 
					return false; 
				}
			}
			
			int ia3 = Integer.parseInt(txtQIA3.getText().toString());
			
			
			if(!(ia3>=cal2.get(Calendar.YEAR)-5 && ia3<=cal2.get(Calendar.YEAR)) ) {
				error = true;
				view = txtQIA3;
				mensaje = "Valor del campo fuera de rango 8";
				return false;
			}  
			}
		}
		

		if(cbQI4.isChecked()) {
			
			if (Util.esVacio(txtQID4.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA4"); 
				view = txtQID4; 
				error = true; 
				return false; 
			} 
			
			int id4 = Integer.parseInt(txtQID4.getText().toString());
			
			if (!(id4>=1 && id4<=31 || id4==98 || id4==44 || id4==66) ) {
				error = true;
				view = txtQID4;
				mensaje = "Valor del campo invalido 9";
				return false;		
			}
			
			if (!(id4==44 || id4==66) ) {
					
			if (Util.esVacio(txtQIM4.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIMES4"); 
				view = txtQIM4; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(txtQIA4.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIA�O4"); 
				view = txtQIA4; 
				error = true; 
				return false; 
			} 
			
			int im4 = Integer.parseInt(txtQIM4.getText().toString());

			if (!(im4>=1 && im4<=12)) {
				error = true;
				view = txtQIM4;
				mensaje = "Valor del campo invalido 10";
				return false;
			}
			
			if(id4!=98) {
				if(!MyUtil.DiaCorrespondeAlMes(id4, im4)){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQID4; 
					error = true; 
					return false; 
				}
			}
			
			int ia4 = Integer.parseInt(txtQIA4.getText().toString());
			
			if(!(ia4>=cal2.get(Calendar.YEAR)-5 && ia4<=cal2.get(Calendar.YEAR)) ) {
				error = true;
				view = txtQIA4;
				mensaje = "Valor del campo fuera de rango 11";
				return false;
			}  
			}
		}
		
		if(cbQI5.isChecked()) {
			
			if (Util.esVacio(txtQID5.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA5"); 
				view = txtQID5; 
				error = true; 
				return false; 
			}  
			
			int id5 = Integer.parseInt(txtQID5.getText().toString());
			
			if (!(id5>=1 && id5<=31 || id5==98 || id5==44 || id5==66) ) {
				error = true;
				view = txtQID5;
				mensaje = "Valor del campo invalido 12";
				return false;		
			}
						
			if (!(id5==44 || id5==66) ) {
			if (Util.esVacio(txtQIM5.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIMES5"); 
				view = txtQIM5; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(txtQIA5.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIA�O5"); 
				view = txtQIA5; 
				error = true; 
				return false; 
			} 
			
			
				
			int im5 = Integer.parseInt(txtQIM5.getText().toString());
			
			if (!(im5>=1 && im5<=12)) {
				error = true;
				view = txtQIM5;
				mensaje = "Valor del campo invalido 13";
				return false;
			}
			
			if(id5!=98) {
				if(!MyUtil.DiaCorrespondeAlMes(id5, im5)){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQID5; 
					error = true; 
					return false; 
				}
			}
			
			int ia5 = Integer.parseInt(txtQIA5.getText().toString());
			
			
			if(!(ia5>=cal2.get(Calendar.YEAR)-5 && ia5<=cal2.get(Calendar.YEAR)) ) {
				error = true;
				view = txtQIA5;
				mensaje = "Valor del campo fuera de rango 14";
				return false;
			}  
			}
		}
		
		
		if(cbQI6.isChecked()) {
			
			if (Util.esVacio(txtQID6.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA6"); 
				view = txtQID6; 
				error = true; 
				return false; 
			} 
			
			int id6 = Integer.parseInt(txtQID6.getText().toString());
			
			if (!(id6>=1 && id6<=31 || id6==98 || id6==44 || id6==66) ) {
				error = true;
				view = txtQID6;
				mensaje = "Valor del campo invalido 15";
				return false;		
			}		
			
			if (!(id6==44 || id6==66) ) {
			
			if (Util.esVacio(txtQIM6.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIMES6"); 
				view = txtQIM6; 
				error = true; 
				return false; 
			} 
			
			if (Util.esVacio(txtQIA6.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIA�O6"); 
				view = txtQIA6; 
				error = true; 
				return false; 
			} 		
			
				

			int im6 = Integer.parseInt(txtQIM6.getText().toString());
			
			if (!(im6>=1 && im6<=12)) {
				error = true;
				view = txtQIM6;
				mensaje = "Valor del campo invalido 16";
				return false;
			}
			
			if(id6!=98) {
				if(!MyUtil.DiaCorrespondeAlMes(id6, im6)){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQID6; 
					error = true; 
					return false; 
				}
			}
			
			int ia6 = Integer.parseInt(txtQIA6.getText().toString());
			if(!(ia6>=cal2.get(Calendar.YEAR)-5 && ia6<=cal2.get(Calendar.YEAR)) ) {
				error = true;
				view = txtQIA6;
				mensaje = "Valor del campo fuera de rango 17";
				return false;
			}  
			}
		}
		
		
		if(c2seccion_04b.qi454==1) {
			if (Util.esVacio(c2seccion_04b.qi457)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI457"); 
				view = rgQI457; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	
    	if(parent.isEsAvanceDual() ) {
    		form.removeAllViews();
    		form.addView(q0); 
    		form.addView(q1); 
    		form.addView(q2); 
    		form.addView(q3); 
    		form.addView(q4);
    	}
    	else {
    		ocultarCampos(true);
    	}
    	
    	if(App.getInstance().getNacimientoVivos()!=null) {
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		ninio = getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargadoNinio);
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null) {
			if (c2seccion_04b.qi457!=null) {
				c2seccion_04b.qi457=c2seccion_04b.setConvertQi457(c2seccion_04b.qi457);
			}	
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		cargarDetalle();
		inicio();
		
    	}
    } 
    private void inicio() {
    	renombrarEtiquetas();
    	saltoInicio();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void ocultarCampos(boolean estado) {
    	int ocultar = View.GONE;
    	if(!estado) 
    		ocultar = View.VISIBLE;
    	q0.setVisibility(ocultar);
    	q1.setVisibility(ocultar);
		q2.setVisibility(ocultar);
		q3.setVisibility(ocultar);
		q4.setVisibility(ocultar);	
    	
    }
    
    private void saltoInicio() {
    	if(!parent.isEsAvanceDual() ) {
    		Util.cleanAndLockView(getActivity(),rgQI454,rgQI455,rgQI456X,txtQI456N,txtQI456D,txtQI456M,txtQI456Y,rgQI457, cbQI1, 
    				cbQI2, cbQI3, cbQI4, cbQI5, cbQI6,  txtQID1,txtQIM1,txtQIA1,txtQID2,txtQIM2,txtQIA2,txtQID3,txtQIM3,
    				txtQIA3,txtQID4,txtQIM4,txtQIA4,txtQID5,txtQIM5,txtQIA5,txtQID6,txtQIM6,txtQIA6);
    		ocultarCampos(true);
    	}
    	else {
    		Util.lockView(getActivity(),false,rgQI454,rgQI455,rgQI456X,txtQI456N,txtQI456D,txtQI456M,txtQI456Y,rgQI457, cbQI1, 
    				cbQI2, cbQI3, cbQI4, cbQI5, cbQI6,  txtQID1,txtQIM1,txtQIA1,txtQID2,txtQIM2,txtQIA2,txtQID3,txtQIM3,
    				txtQIA3,txtQID4,txtQIM4,txtQIA4,txtQID5,txtQIM5,txtQIA5,txtQID6,txtQIM6,txtQIA6);
    		ocultarCampos(false);
    	    
    	    if(c2seccion_04b.qi454!=null) 
        		onqrgQI454hChangeValue();
        	if(c2seccion_04b.qi455!=null) 
        	onqrgQI455ChangeValue();
        	if(c2seccion_04b.qi456x!=null) 
        		onqrgQI456xChangeValue();
        	ejecutaAccionCheckbox();
        	if(c2seccion_04b.qi457!=null) 
        		onqrgQI457ChangeValue();
    	}
    }

    public void ejecutaAccionCheckbox() {
    	
    	onqrgQI456x1ChangeValue();
    	onqrgQI456x2ChangeValue();
    	onqrgQI456x3ChangeValue();
    	onqrgQI456x4ChangeValue();
    	onqrgQI456x5ChangeValue();
    	onqrgQI456x6ChangeValue();
    }
    
	
	
	
	public void renombrarEtiquetas()
    {	
		lblpregunta454.text(R.string.c2seccion_04bqi454);
		lblpregunta455.text(R.string.c2seccion_04bqi455);
		lblpregunta457.text(R.string.c2seccion_04bqi457);
		
    	lblpregunta454.setText(lblpregunta454.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta455.setText(lblpregunta455.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta457.setText(lblpregunta457.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void onqrgQI456x1ChangeValue() {  	
    	if (cbQI1.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID1,txtQIM1,txtQIA1,txtQI456X_O);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			if(txtQID1.getText()!=null && !txtQID1.getText().toString().isEmpty()) {
  			Integer id1 = Integer.parseInt(txtQID1.getText().toString());
  			if(id1==44 || id1==66) {
  				Util.cleanAndLockView(getActivity(),txtQIM1,txtQIA1);
  			}
  			
  			}
  			
  			txtQID1.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID1,txtQIM1,txtQIA1,txtQI456X_O);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	
	public void onqrgQI456x2ChangeValue() {  	
    	if (cbQI2.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID2,txtQIM2,txtQIA2);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			
  			if(txtQID2.getText()!=null && !txtQID2.getText().toString().isEmpty()) {
  	  			Integer id2 = Integer.parseInt(txtQID2.getText().toString());
  	  			if(id2==44 || id2==66) {
  	  				Util.cleanAndLockView(getActivity(),txtQIM2,txtQIA2);
  	  			}
  	  			
  	  			}
  			txtQID2.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID2,txtQIM2,txtQIA2);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	
	public void onqrgQI456x3ChangeValue() {  	
    	if (cbQI3.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID3,txtQIM3,txtQIA3);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			
  			if(txtQID3.getText()!=null && !txtQID3.getText().toString().isEmpty()) {
  	  			Integer id3 = Integer.parseInt(txtQID3.getText().toString());
  	  			if(id3==44 || id3==66) {
  	  				Util.cleanAndLockView(getActivity(),txtQIM3,txtQIA3);
  	  			}
  	  			
  	  			}
  			
  			txtQID3.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID3,txtQIM3,txtQIA3);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	
	public void onqrgQI456x4ChangeValue() {  	
    	if (cbQI4.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID4,txtQIM4,txtQIA4);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			if(txtQID4.getText()!=null && !txtQID4.getText().toString().isEmpty()) {
  	  			Integer id4 = Integer.parseInt(txtQID4.getText().toString());
  	  			if(id4==44 || id4==66) {
  	  				Util.cleanAndLockView(getActivity(),txtQIM4,txtQIA4);
  	  			}
  	  			
  	  			}
  			
  			txtQID4.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID4,txtQIM4,txtQIA4);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	public void onqrgQI456x5ChangeValue() {  	
    	if (cbQI5.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID5,txtQIM5,txtQIA5);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			if(txtQID5.getText()!=null && !txtQID5.getText().toString().isEmpty()) {
  	  			Integer id5 = Integer.parseInt(txtQID5.getText().toString());
  	  			if(id5==44 || id5==66) {
  	  				Util.cleanAndLockView(getActivity(),txtQIM5,txtQIA5);
  	  			}
  	  			
  	  			}
  			
  			txtQID5.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID5,txtQIM5,txtQIA5);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	public void onqrgQI456x6ChangeValue() {  	
    	if (cbQI6.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID6,txtQIM6,txtQIA6);
  			/*if (verificarCheck426()) {  			
  				Util.cleanAndLockView(getActivity(),cbQI426_Y);
  			}*/
  			if(txtQID6.getText()!=null && !txtQID6.getText().toString().isEmpty()) {
  	  			Integer id6 = Integer.parseInt(txtQID6.getText().toString());
  	  			if(id6==44 || id6==66) {
  	  				Util.cleanAndLockView(getActivity(),txtQIM6,txtQIA6);
  	  			}
  	  			
  	  			}
  			
  			txtQID6.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID6,txtQIM6,txtQIA6);  
  			/*if (!verificarCheck426()) {  			
  				Util.lockView(getActivity(),false,cbQI426_Y);  				
			}*/
  		}	
    }
	
	public CISECCION_04B_TARJETA getValoresCheck(int indice) {
		
		CISECCION_04B_TARJETA objc204b=new CISECCION_04B_TARJETA();
		objc204b.id=App.getInstance().getNacimientoVivos().id; 
		objc204b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		objc204b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		objc204b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
		objc204b.pregunta = "456x";
		objc204b.indice=indice;
		if(indice==1) {
			objc204b.dia = txtQID1.getText().toString();
			Integer idia=Integer.parseInt(txtQID1.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM1.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA1.getText().toString());
			}
		}
		else if(indice==2) {
			objc204b.dia = txtQID2.getText().toString();
			Integer idia=Integer.parseInt(txtQID2.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM2.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA2.getText().toString());
			}
		}
		else if(indice==3) {
			objc204b.dia = txtQID3.getText().toString();
			Integer idia=Integer.parseInt(txtQID3.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM3.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA3.getText().toString());
			}
		}
		else if(indice==4) {
			objc204b.dia = txtQID4.getText().toString();
			Integer idia=Integer.parseInt(txtQID4.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM4.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA4.getText().toString());
			}			
		}
		else if(indice==5) {
			objc204b.dia = txtQID5.getText().toString();
			Integer idia=Integer.parseInt(txtQID5.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM5.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA5.getText().toString());
			}
		}
		else if(indice==6) {
			objc204b.dia = txtQID6.getText().toString();
			Integer idia=Integer.parseInt(txtQID6.getText().toString());
			if(idia!=44 && idia!=66 ) {
				objc204b.mes = txtQIM6.getText().toString();
				objc204b.anio = Integer.parseInt(txtQIA6.getText().toString());
			}
		}
			
		return objc204b;
	}
	
	public boolean grabarDetalle() throws SQLException {
		
		CISECCION_04B_TARJETA objc204b=null;
		if(cbQI1.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();			
			objc204b = getValoresCheck(1);			
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(cbQI2.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(2);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
				}
		if(cbQI3.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(3);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(cbQI4.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(4);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(cbQI5.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(5);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(cbQI6.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(6);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		
		
	return true;	
	}
	
	public void cargarDetalle() {
		CISECCION_04B_TARJETA objc204b=null;
		List<CISECCION_04B_TARJETA> lista = null;  
		lista = getCuestionarioService().getSecciones04BTarjeta(App.getInstance().getNacimientoVivos().id, 
				App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,
				App.getInstance().getNacimientoVivos().qi212,"456x", seccionesCargado2);
		 
		if(lista!=null) {
			for(CISECCION_04B_TARJETA obj : lista){
				if(obj.indice==1) {
					cbQI1.setChecked(true);
					txtQID1.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
						txtQIM1.setText(obj.mes);
						txtQIA1.setText(obj.anio.toString());
					}
					
				}
				if(obj.indice==2) {
					cbQI2.setChecked(true);
					txtQID2.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
					txtQIM2.setText(obj.mes);
					txtQIA2.setText(obj.anio.toString());
					}
				}
				if(obj.indice==3) {
					cbQI3.setChecked(true);
					txtQID3.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
					txtQIM3.setText(obj.mes);
					txtQIA3.setText(obj.anio.toString());
					}
				}
				if(obj.indice==4) {
					cbQI4.setChecked(true);
					txtQID4.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
					txtQIM4.setText(obj.mes);
					txtQIA4.setText(obj.anio.toString());
					}
				}
				if(obj.indice==5) {
					cbQI5.setChecked(true);
					txtQID5.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
					txtQIM5.setText(obj.mes);
					txtQIA5.setText(obj.anio.toString());
					}
				}
				if(obj.indice==6) {
					cbQI6.setChecked(true);
					txtQID6.setText(obj.dia);
					Integer idia = Integer.parseInt(obj.dia);
					if(idia!=44 && idia!=66) {
					txtQIM6.setText(obj.mes);
					txtQIA6.setText(obj.anio.toString());
					}
				}
								
			}
						
		}
		/*if(c2seccion_04b==null){ 
		  c2seccion_04b=new C2SECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.qi212=App.getInstance().getNacimientoVivos().qi212;
	    } */		
		
	}
	
	public boolean verificarCheck456x() {
  		if (cbQI1.isChecked() || cbQI2.isChecked() || cbQI3.isChecked() || cbQI4.isChecked() || 
  				cbQI5.isChecked() || cbQI6.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	public void onqrgQI454hChangeValue() {  	
		if(rgQI454.getValue()!=null) {
			if (rgQI454.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),cbQI407_Y);
	
				Util.lockView(getActivity(),false,rgQI456X,txtQI456X_O,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
						txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,
						cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6,rgQI457);
	
				Util.cleanAndLockView(getActivity(),rgQI455);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.VISIBLE);
				q4.setVisibility(View.VISIBLE);
				MyUtil.LiberarMemoria();
				//onqrgQI456xChangeValue();
	    	}
			else if (rgQI454.getValue().toString().equals("2")) {    		
	    		//Util.cleanAndLockView(getActivity(),cbQI407_Y);
				Util.cleanAndLockView(getActivity(),rgQI455,rgQI456X,txtQI456X_O,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
						txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,
						cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6,rgQI457);
				q2.setVisibility(View.GONE);
				q3.setVisibility(View.GONE);
				q4.setVisibility(View.GONE);
				MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,rgQI455);
				
	    		Util.cleanAndLockView(getActivity(),rgQI456X,txtQI456X_O,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
						txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,
						cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6,rgQI457);
	    		
				q2.setVisibility(View.VISIBLE);
				//onqrgQI455ChangeValue();
				q3.setVisibility(View.GONE); 
				q4.setVisibility(View.GONE);
	
				MyUtil.LiberarMemoria();
				//onqrgQI456xChangeValue();
	    	}	
			
			if (!rgQI454.getValue().toString().isEmpty()) { 

			}
			else {

		}
		}
    }
	
	public void onqrgQI455ChangeValue() {  	
		if(rgQI455.getValue()!=null) {
			if (rgQI455.getValue().toString().equals("1") || rgQI455.getValue().toString().equals("2")) {    		
	    		//Util.cleanAndLockView(getActivity(),cbQI407_Y);
				Util.cleanAndLockView(getActivity(),rgQI456X,txtQI456X_O,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
						txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,
						cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6);
				q3.setVisibility(View.GONE);
				
	    	}
			else {
				Util.lockView(getActivity(),false,rgQI456X,txtQI456X_O,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
						txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,
						cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6);

				q3.setVisibility(View.VISIBLE);

			}
		}
		 
    }
	
	
	public void onqrgQI456xChangeValue() {  
		
		if(rgQI456X.getValue()!=null) {
	 		if (rgQI456X.getValue().toString().equals("2")) {    		

				ocultarTabla456x();
//				MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		mostrarTabla456x();
	    		ejecutaAccionCheckbox();
//	    		MyUtil.LiberarMemoria();
				
	    	}
		}
    }
	
	public void ocultarGrids(boolean estado) {
		int ocultar = View.GONE;
		if(!estado)
			ocultar = View.VISIBLE;
		grid1.setVisibility(ocultar);
		txtQI456X_O.setVisibility(ocultar);
		lblqi456x_o.setVisibility(ocultar);
		/*grid2.setVisibility(ocultar);
		grid3.setVisibility(ocultar);
		grid4.setVisibility(ocultar);
		grid5.setVisibility(ocultar);
		grid6.setVisibility(ocultar);
		grid7.setVisibility(ocultar);*/
		
	}
	
	public void mostrarTabla456x() {
		Util.lockView(getActivity(),false,txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,
				txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
				cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6,lblqi456x_o,txtQI456X_O
				);  
		ocultarGrids(false);		
	}
	
	
	public void ocultarTabla456x() {
		Util.cleanAndLockView(getActivity(),txtQID1,txtQID2,txtQID3,txtQID4,txtQID5,txtQID6,
				txtQIM1,txtQIM2,txtQIM3,txtQIM4,txtQIM5,txtQIM6,txtQIA1,txtQIA2,txtQIA3,txtQIA4,txtQIA5,txtQIA6,
				cbQI1,cbQI2,cbQI3,cbQI4,cbQI5,cbQI6,lblqi456x_o,txtQI456X_O);
		
		ocultarGrids(true);
	}
	
	public void onqrgQI457ChangeValue() {  	
		if(rgQI457.getValue()!= null) {
			int dato=Integer.parseInt(rgQI457.getTagSelected("0").toString());
			if (!Util.esDiferente(dato, 1,1)) { 
				MyUtil.MensajeGeneral(getActivity(), "Registre vacunas que correspondan");
			}
//			else {
//
//			}
		}
	}
	
	public void onqrgQI454ChangeValue() {  	
		if(rgQI454.getValue()!= null) {
		
			if (!rgQI454.getValue().toString().isEmpty()) { 

			}
			else {

			}
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI454.readOnly();
			rgQI455.readOnly();
			rgQI456X.readOnly();
			txtQI456X_O.setEnabled(false);
			rgQI457.readOnly();
			txtQIA1.readOnly();
			txtQIA2.readOnly();
			txtQIA3.readOnly();
			txtQIA4.readOnly();
			txtQIA5.readOnly();
			txtQIA6.readOnly();
			txtQID1.readOnly();
			txtQID2.readOnly();
			txtQID3.readOnly();
			txtQID4.readOnly();
			txtQID5.readOnly();
			txtQID6.readOnly();
			txtQIM1.readOnly();
			txtQIM2.readOnly();
			txtQIM3.readOnly();
			txtQIM4.readOnly();
			txtQIM5.readOnly();
			txtQIM6.readOnly();
			cbQI1.readOnly();
			cbQI2.readOnly();
			cbQI3.readOnly();
			cbQI4.readOnly();
			cbQI5.readOnly();
			cbQI6.readOnly();
		}
	}
	
    
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
    @Override
	public Integer grabadoParcial() {
    	uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null) {
			if (c2seccion_04b.qi457!=null) {
				c2seccion_04b.qi457=c2seccion_04b.getConvertQi457(c2seccion_04b.qi457);
			}
		}
		if(App.getInstance().getSeccion04B()!=null) {
			App.getInstance().getSeccion04B().qi454 =c2seccion_04b.qi454;
			App.getInstance().getSeccion04B().qi455 =c2seccion_04b.qi455;
			App.getInstance().getSeccion04B().qi456x =c2seccion_04b.qi456x;
			App.getInstance().getSeccion04B().qi457 =c2seccion_04b.qi457;
		}
		else {
			App.getInstance().setSeccion04B(c2seccion_04b);
		}		
		
		SeccionCapitulo[] seccionesPersonas = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "QI215Y","QI215M","QI215D","QI216","QI217CONS","QI218") };
		
		CISECCION_02 nacimiento =  getCuestionarioService().getSeccion01_03Nacimiento(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesPersonas);
		
		Calendar fechadenacimiento = new GregorianCalendar(nacimiento.qi215y, Integer.parseInt(nacimiento.qi215m)-1, Integer.parseInt(nacimiento.qi215d));
		Calendar fecharef = new GregorianCalendar();
		
		if(nacimiento.qi216==1) {
		String fechareferencia = nacimiento.qi217cons;
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}			
		}
		
		boolean val215=false;
		if(MyUtil.CalcularEdadEnMesesFelix(fechadenacimiento,fecharef) >= 4) {
			val215= true;
		}
		
		App.getInstance().getSeccion04B().filtro215 = val215;
		App.getInstance().getSeccion04B().qi216 = nacimiento.qi216;
		App.getInstance().getSeccion04B().qi218 = nacimiento.qi218;
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				//if(verificarCheck456x()) {
				getCuestionarioService().borrar04b(App.getInstance().getNacimientoVivos().id,App.getInstance().getNacimientoVivos().hogar_id,
				App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, "456x");
				grabarDetalle();
				
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi457!=null) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_003.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs,"QI461B","QI461CU","QI461CN","QI461D","QI461E","QI461F","QI462","QI462A","QI462B","QI463","QI463A");					
					App.getInstance().getSeccion04B().qi458 = null;
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
				
				//}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
