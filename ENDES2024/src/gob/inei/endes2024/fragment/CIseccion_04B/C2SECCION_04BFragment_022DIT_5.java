package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_5_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_5 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478I1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478I2; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478I3; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478I4_A;
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478I4_B;
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478I5; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478I6; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478I7; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478I8; 


	public DISCAPACIDAD detalle;
	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478i1, lblpregunta478i1_ind,lblpregunta478i2,lblpregunta478i2_ind, lblpregunta478i3,lblpregunta478i3_ind,lblpregunta478i4, 
	lblpregunta478i4_a,lblpregunta478i4_b, lblpregunta478i5, lblpregunta478i6, lblpregunta478i7, lblpregunta478i8;
	
	public GridComponent2 gridpregunta478I4,gridDiscapacidad;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_5() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478I1","QI478I2","QI478I3","QI478I4_A","QI478I4_B","QI478I5","QI478I6","QI478I7","QI478I8","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478I1","QI478I2","QI478I3","QI478I4_A","QI478I4_B","QI478I5","QI478I6","QI478I7","QI478I8","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_5 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo5de37_54i).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478i1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i7= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i8= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		lblpregunta478i1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta478i2_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478i3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		
		rgQI478I1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478i4_1,R.string.c2seccion_04b_2qi478i4_2,R.string.c2seccion_04b_2qi478i4_3,R.string.c2seccion_04b_2qi478i4_4,R.string.c2seccion_04b_2qi478i4_5,R.string.c2seccion_04b_2qi478i4_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL).callback("QI478I1onChangeValue"); 
		rgQI478I2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478I3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478I5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478I6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478i6_1,R.string.c2seccion_04b_2qi478i6_2,R.string.c2seccion_04b_2qi478i6_3,R.string.c2seccion_04b_2qi478i6_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478I7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478I8=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478i8_1,R.string.c2seccion_04b_2qi478i8_2,R.string.c2seccion_04b_2qi478i8_3,R.string.c2seccion_04b_2qi478i8_4,R.string.c2seccion_04b_2qi478i8_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		

		lblpregunta478i4_a= new LabelComponent(getActivity()).size(altoComponente+210, 600).textSize(18).text("");
		lblpregunta478i4_b= new LabelComponent(getActivity()).size(altoComponente+240, 600).textSize(18).text("");
		rgQI478I4_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+210,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478I4_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+220,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
			
		
		gridpregunta478I4 = new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridpregunta478I4.addComponent(lblpregunta478i4_a);
		gridpregunta478I4.addComponent(rgQI478I4_A);
		gridpregunta478I4.addComponent(lblpregunta478i4_b);
		gridpregunta478I4.addComponent(rgQI478I4_B);

	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,lblpregunta478i1, lblpregunta478i1_ind,rgQI478I1); 
		q4  = createQuestionSection(0,lblpregunta478i2,lblpregunta478i2_ind,rgQI478I2);
		q5  = createQuestionSection(0,lblpregunta478i3,lblpregunta478i3_ind,rgQI478I3);
		q6  = createQuestionSection(0,lblpregunta478i4,gridpregunta478I4.component());
		q7  = createQuestionSection(0,lblpregunta478i5,rgQI478I5);
		q8  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478i6,rgQI478I6);
		q9  = createQuestionSection(0,lblpregunta478i7,rgQI478I7);
		q10 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478i8,rgQI478I8);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8); 
		form.addView(q9);
		form.addView(q10);             

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478i1  == null) {
				error = true;
				view = rgQI478I1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I1");
				return false;
			}
			if (ninio.qi478i2  == null) {
				error = true;
				view = rgQI478I2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I2");
				return false;
			}
			if (ninio.qi478i3  == null) {
				error = true;
				view = rgQI478I3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I3");
				return false;
			}
			if (ninio.qi478i4_a  == null) {
				error = true;
				view = rgQI478I4_A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I4_A");
				return false;
			}
			if (ninio.qi478i4_b  == null) {
				error = true;
				view = rgQI478I4_B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I4_B");
				return false;
			}
			if (ninio.qi478i5  == null) {
				error = true;
				view = rgQI478I5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I5");
				return false;
			}
			if (ninio.qi478i6  == null) {
				error = true;
				view = rgQI478I6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I6");
				return false;
			}
			if (ninio.qi478i7  == null) {
				error = true;
				view = rgQI478I7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I7");
				return false;
			}
			if (ninio.qi478i8  == null) {
				error = true;
				view = rgQI478I8;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478I8");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	public void evaluarPregunta() {		
		if((App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=37 && App.getInstance().getSeccion04DIT_02().qi478 <=54)) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478I1,rgQI478I2,rgQI478I3,rgQI478I4_A,rgQI478I4_B,rgQI478I5,rgQI478I6,rgQI478I7,rgQI478I8);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
		else {
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478I1,rgQI478I2,rgQI478I3,rgQI478I4_A,rgQI478I4_B,rgQI478I5,rgQI478I6,rgQI478I7,rgQI478I8);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
		}
	}
	
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478I1,rgQI478I2,rgQI478I3,rgQI478I4_A,rgQI478I4_B,rgQI478I5,rgQI478I6,rgQI478I7,rgQI478I8);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478I1,rgQI478I2,rgQI478I3,rgQI478I4_A,rgQI478I4_B,rgQI478I5,rgQI478I6,rgQI478I7,rgQI478I8);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
		}
	}
	public void abrirDetalle(DISCAPACIDAD modelo) {
		FragmentManager fm = C2SECCION_04BFragment_022DIT_5.this.getFragmentManager();
		C2SECCION_04BFragment_018DIT_5_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_5_Dialog.newInstance(this, modelo);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	
	public void cambiarVariablesParalaBasedeDatos(){
		ninio.qi478i1=ninio.qi478i1!=null?ninio.setConvertconSieteVariables(ninio.qi478i1):null;
//		ninio.qi478i1=ninio.qi478i1!=null?ninio.setConvertconSeisVariables(ninio.qi478i1):null; correcto
		
		ninio.qi478i2=ninio.qi478i2!=null?ninio.setConvertconTresVariables(ninio.qi478i2):null;
		ninio.qi478i3=ninio.qi478i3!=null?ninio.setConvertconTresVariables(ninio.qi478i3):null;
		ninio.qi478i4_a =ninio.qi478i4_a !=null?ninio.setConvertconTresVariables(ninio.qi478i4_a):null;
		ninio.qi478i4_b =ninio.qi478i4_b !=null?ninio.setConvertconTresVariables(ninio.qi478i4_b):null;
		ninio.qi478i5=ninio.qi478i5!=null?ninio.setConvertconTresVariables(ninio.qi478i5):null;
		
		ninio.qi478i6=ninio.qi478i6!=null?ninio.setConvertconTresVariables(ninio.qi478i6):null;
//		ninio.qi478i6=ninio.qi478i6!=null?ninio.setConvertconCuatroVariables(ninio.qi478i6):null; correcto
		
		ninio.qi478i7=ninio.qi478i7!=null?ninio.setConvertconCuatroVariables(ninio.qi478i7):null;
//		ninio.qi478i7=ninio.qi478i7!=null?ninio.setConvertconTresVariables(ninio.qi478i7):null; correcto
		
		ninio.qi478i8=ninio.qi478i8!=null?ninio.setConvertconCincoVariables(ninio.qi478i8):null;
	}
	
	private void cambiarVariablesParaMostraraUsuario(){
		ninio.qi478i1 =ninio.qi478i1 !=null?ninio.getConvertconTresVariables(ninio.qi478i1):null;
//		ninio.qi478i1 =ninio.qi478i1 !=null?ninio.getConvertconSeisVariables(ninio.qi478i1):null;correcto
		
		ninio.qi478i2 =ninio.qi478i2 !=null?ninio.getConvertconTresVariables(ninio.qi478i2):null;
		ninio.qi478i3 =ninio.qi478i3 !=null?ninio.getConvertconTresVariables(ninio.qi478i3):null;
		ninio.qi478i4_a =ninio.qi478i4_a !=null?ninio.getConvertconTresVariables(ninio.qi478i4_a):null;
		ninio.qi478i4_b =ninio.qi478i4_b !=null?ninio.getConvertconTresVariables(ninio.qi478i4_b):null;		
		ninio.qi478i5 =ninio.qi478i5 !=null?ninio.getConvertconTresVariables(ninio.qi478i5):null;
		
		ninio.qi478i6 =ninio.qi478i6 !=null?ninio.getConvertconTresVariables(ninio.qi478i6):null;
//		ninio.qi478i6 =ninio.qi478i6 !=null?ninio.getConvertconCuatroVariables(ninio.qi478i6):null; correcto
		
		ninio.qi478i7 =ninio.qi478i7 !=null?ninio.getConvertconCuatroVariables(ninio.qi478i7):null;
//		ninio.qi478i7 =ninio.qi478i7 !=null?ninio.getConvertconTresVariables(ninio.qi478i7):null; correcto
		
		ninio.qi478i8=ninio.qi478i8!=null?ninio.getConvertconCincoVariables(ninio.qi478i8):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
		QI478I1onChangeValue();
	}

	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478i1.setText("(NOMBRE)");		
		lblpregunta478i1.setText(lblpregunta478i1.getText().toString().replace(replace, nombre));	
		lblpregunta478i1.setText(Html.fromHtml("478I1. <b>MUESTRE LA CARTILLA 3.</b> <br>Cuando "+lblpregunta478i1.getText()+" <b>dibuja una persona</b> �a cu�l de estas figuras <b>se parece m�s</b> su dibujo? "));
		lblpregunta478i1_ind.setText(Html.fromHtml("LEA EN VOZ ALTA LA(S) OPCI�N(ES) ELEGIDA(S) Y CIRCULE LA DE MAYOR NUMERACI�N QUE GENERALMENTE REALIZA LA/EL NI�A(O)."));
		
		lblpregunta478i2.setText("(NOMBRE)");
		lblpregunta478i2.setText(lblpregunta478i2.getText().toString().replace(replace, nombre));
		lblpregunta478i2.setText(Html.fromHtml("478I2. Cuando "+lblpregunta478i2.getText()+" juega �<b>ella/�l dice que las cosas o mu�ecos tienen emociones o sensaciones</b>, es decir, que est�n contentos, molestos, tienen hambre o fr�o, etc?"));
		lblpregunta478i2_ind.setText(Html.fromHtml("SIEMPRE LEA:<br>Por ejemplo, dice que su carrito est� triste o contento, o que su mu�eca tiene hambre."));
		
		lblpregunta478i3.setText("(NOMBRE)");
		lblpregunta478i3.setText(lblpregunta478i3.getText().toString().replace(replace, nombre));
		lblpregunta478i3.setText(Html.fromHtml("478I3. "+lblpregunta478i3.getText()+" �juega <b>a ser un personaje</b> de televisi�n o de cuentos como superh�roes, princesas, el lobo fer�z o <b>un animalito</b>?"));
		lblpregunta478i3_ind.setText(Html.fromHtml("REFIERE  A SI LA (EL) NI�A(O) JUEGA COMO SI FUESE UN <b>PERSONAJE CON QUIEN NO TIENE INTERACCI�N DIRECTA</b>."));
		
		lblpregunta478i4.setText("(NOMBRE)");
		lblpregunta478i4.setText(lblpregunta478i4.getText().toString().replace(replace, nombre));
		lblpregunta478i4.setText(Html.fromHtml("478I4. <b>En casa</b> "+lblpregunta478i4.getText()+" tiene:"));
		lblpregunta478i4_a.setText(Html.fromHtml("A. �Materiales <b>especialmente hechos para jugar</b> como una pelota o una mu�eca?<br><br>DE SER NECESARIO, LEA:<br>Considere si en casa tiene materiales que solo pueden ser usados de una forma espec�fica para jugar, como cubos para encajar, pelotas, rompecabezas, mu�ecos, etc."));
		lblpregunta478i4_b.setText(Html.fromHtml("B. �Y tiene <b>otros materiales</b> con los que puede jugar como bloques, palitos, botellas, l�pices o alg�n tipo de papel? <br><br>SIEMPRE LEA:<br>Considere si en casa tiene materiales que pueden ser utilizados de diferentes formas al jugar como l�pices, papeles, bloques, plastilinas, objetos de la casa (ollas, botellas de pl�stico), objetos de origen natural (conchas, palitos, plantas)."));
				
		lblpregunta478i5.setText("(NOMBRE)");
		lblpregunta478i5.setText(lblpregunta478i5.getText().toString().replace(replace, nombre));
		lblpregunta478i5.setText(Html.fromHtml("478I5. "+lblpregunta478i5.getText()+" �llora, grita o hace pataletas la <b>mayor parte del tiempo</b>?"));
					
		lblpregunta478i6.setText("(NOMBRE)");
		lblpregunta478i6.setText(lblpregunta478i6.getText().toString().replace(replace, nombre));
		lblpregunta478i6.setText(Html.fromHtml("478I6. Cuando "+lblpregunta478i6.getText()+" <b>quiere algo y usted le dice que espere, generalmente</b> �espera �tranquila(o)�? "));
		
		lblpregunta478i7.setText("(NOMBRE)");
		lblpregunta478i7.setText(lblpregunta478i7.getText().toString().replace(replace, nombre));
		lblpregunta478i7.setText(Html.fromHtml("478I7. Cuando "+lblpregunta478i7.getText()+" <b>quiere algo y usted le dice que NO, generalmente</b> �se hace da�o, agrede a los dem�s o da�a las cosas?"));
		
		lblpregunta478i8.setText("(NOMBRE)");
		lblpregunta478i8.setText(lblpregunta478i8.getText().toString().replace(replace, nombre));
		lblpregunta478i8.setText(Html.fromHtml("478I8. En los �ltimos 15 dias, �cu�ntas veces  usted le ha dado un <b>palmazo, le ha jalado de los cabellos o la oreja o le ha golpeado con un objeto en cualquier parte de su cuerpo</b> a "+lblpregunta478i8.getText()+", de 1 a 3 veces, de 4 a 6 veces o m�s de 6 veces?  "));
		
	}
	public void QI478I1onChangeValue(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(!Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registr� como Discapacitado! ");
		}
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478I1.readOnly();
			rgQI478I2.readOnly();
			rgQI478I3.readOnly();
			rgQI478I4_A.readOnly();
			rgQI478I4_B.readOnly();
			rgQI478I5.readOnly();
			rgQI478I6.readOnly();
			rgQI478I7.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
