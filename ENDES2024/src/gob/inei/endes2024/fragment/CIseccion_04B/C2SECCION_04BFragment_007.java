package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_007 extends FragmentForm { 
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI465DF; 
	@FieldAnnotation(orderIndex=2) 
	public TextField txtQI465DF_O; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI465DH; 
	@FieldAnnotation(orderIndex=4) 
	public TextField txtQI465DH_O; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI465DJ_A; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI465DJ_B; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI465DJ_C; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI465DJ_D; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI465DJ_E; 
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI465DJ_F; 
	@FieldAnnotation(orderIndex=11) 
	public TextField txtQI465DJF_O; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI465DJ_G; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI465DJ_H; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI465DJ_I; 
	@FieldAnnotation(orderIndex=15) 
	public TextField txtQI465DJ_O; 
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465df,lblpregunta465dh,lblpregunta465dj,lblpregunta465dh_ind1,lblpregunta465dh_ind2,lblpregunta465dh_ind3,lblpregunta465dj_ind1,lblpregunta465dj_ind2,lblpregunta465dj_ind3,lblpregunta465dj_c1,lblpregunta465dj_c2,lblpregunta465dj_c3,lblpregunta465dj_c4; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	LinearLayout q9; 
	LinearLayout q10; 
	LinearLayout q11; 
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465DF","QI465DF_O","QI465DH","QI465DH_O","QI465DJ_A","QI465DJ_B","QI465DJ_C","QI465DJ_D","QI465DJ_E","QI465DJ_F","QI465DJF_O","QI465DJ_G","QI465DJ_H","QI465DJ_I","QI465DJ_O")}; 
	SeccionCapitulo[] seccionesCargado; 
	String nombre_persona;
	public TextField txtCabecera;
	public C2SECCION_04BFragment_007() {} 
	public C2SECCION_04BFragment_007 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465DF","QI465DF_O","QI465DH","QI465DH_O","QI465DJ_A","QI465DJ_B","QI465DJ_C","QI465DJ_D","QI465DJ_E","QI465DJ_F","QI465DJF_O","QI465DJ_G","QI465DJ_H","QI465DJ_I","QI465DJ_O","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta465df = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");	
	    lblpregunta465dh = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465dh);
	    lblpregunta465dh_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dh_ind1).negrita();
	    lblpregunta465dh_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465dh_ind2);
	    lblpregunta465dh_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dh_ind3).negrita();
	    lblpregunta465dj = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465dj);
	    lblpregunta465dj_ind1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465dj_ind1).negrita();
	    lblpregunta465dj_ind2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_ind2).negrita();
	    lblpregunta465dj_ind3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_ind3);
	    lblpregunta465dj_c1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_c1).negrita();
	    lblpregunta465dj_c2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_c2).negrita();
	    lblpregunta465dj_c3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_c3).negrita();
	    lblpregunta465dj_c4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465dj_c4).negrita();
	    
	    rgQI465DF=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465df_1,R.string.c2seccion_04bqi465df_2,R.string.c2seccion_04bqi465df_3,R.string.c2seccion_04bqi465df_4,R.string.c2seccion_04bqi465df_5,R.string.c2seccion_04bqi465df_6,R.string.c2seccion_04bqi465df_7).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI465DF_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500); 
		rgQI465DF.agregarEspecifique(6,txtQI465DF_O); 
	    
		rgQI465DH=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465dh_1,R.string.c2seccion_04bqi465dh_2,R.string.c2seccion_04bqi465dh_3,R.string.c2seccion_04bqi465dh_4,R.string.c2seccion_04bqi465dh_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI465DH_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500); 
		txtQI465DJF_O =new TextField(this.getActivity()).maxLength(500).size(altoComponente, 700);
		rgQI465DH.agregarEspecifique(4,txtQI465DH_O); 
		
		chbQI465DJ_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQI465DJ_FChangeValue");
		chbQI465DJ_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT);
		chbQI465DJ_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT); 
		chbQI465DJ_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465dj_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onQI465DJ_IChangeValue");


		txtQI465DJ_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 600); 
	    
		
    } 
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta465df,rgQI465DF); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta465dh,lblpregunta465dh_ind1,lblpregunta465dh_ind2,lblpregunta465dh_ind3,rgQI465DH); 
		LinearLayout ly465dj = new LinearLayout(getActivity());
		LinearLayout ly465djf = new LinearLayout(getActivity());
		ly465dj.addView(chbQI465DJ_I);
		ly465dj.addView(txtQI465DJ_O);
//		ly465djf.addView(chbQI465DJ_F);
//		ly465djf.addView(txtQI465DJF_O);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta465dj,lblpregunta465dj_ind1,lblpregunta465dj_ind2,lblpregunta465dj_ind3,lblpregunta465dj_c1,chbQI465DJ_A,chbQI465DJ_B,chbQI465DJ_C,chbQI465DJ_D,chbQI465DJ_E,lblpregunta465dj_c2,chbQI465DJ_F,txtQI465DJF_O,lblpregunta465dj_c3,chbQI465DJ_G,lblpregunta465dj_c4,chbQI465DJ_H,ly465dj); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		return contenedor; 
    } 
    public void onQI465DJ_IChangeValue(){
    	if(chbQI465DJ_I.isChecked()){
    		Util.lockView(getActivity(),false,txtQI465DJ_O);  
    	}
    	else{
    		Util.cleanAndLockView(getActivity(),txtQI465DJ_O);
    	}
    }
    public void onQI465DJ_FChangeValue(){
    	if(chbQI465DJ_F.isChecked()){
    		Util.lockView(getActivity(),false,txtQI465DJF_O);  
    	}
    	else{
    		Util.cleanAndLockView(getActivity(),txtQI465DJF_O);
    	}
    }
    @Override 
    public boolean grabar() { 
    	uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi465df!=null) {
				c2seccion_04b.qi465df=c2seccion_04b.getConvertQi465df(c2seccion_04b.qi465df);
			}	
			if (c2seccion_04b.qi465dh!=null) {
				c2seccion_04b.qi465dh=c2seccion_04b.getConvertQi465dh(c2seccion_04b.qi465dh);
			}	
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if(App.getInstance().getSeccion04B().filtro465de) {
			if (Util.esVacio(c2seccion_04b.qi465df)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DF"); 
				view = rgQI465DF; 
				error = true; 
				return false; 
			} 
				
			if(!Util.esDiferente(c2seccion_04b.qi465df,96)){ 
				if (Util.esVacio(c2seccion_04b.qi465df)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465DF_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		
		if( App.getInstance().getSeccion04B().filtro465dg) {
			if (Util.esVacio(c2seccion_04b.qi465dh)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465DH"); 
				view = rgQI465DH; 
				error = true; 
				return false; 
			} 
			if(!Util.esDiferente(c2seccion_04b.qi465dh,96)){ 
				if (Util.esVacio(c2seccion_04b.qi465dh)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465DH_O; 
					error = true; 
					return false; 
				} 
			} 
		}
		if(App.getInstance().getSeccion04B().filtro465di) {
			if(!verificarCheck465dj()) {
				mensaje = "Seleccione una opci�n en P.465dj"; 
				view = chbQI465DJ_A; 
				error = true; 
				return false; 
			}
			if (chbQI465DJ_F.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi465djf_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465DJF_O; 
					error = true; 
					return false; 
				}
			}	
			
			if (chbQI465DJ_I.isChecked()) {
				if (Util.esVacio(c2seccion_04b.qi465dj_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465DJ_O; 
					error = true; 
					return false; 
				}
			}	
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			if (c2seccion_04b.qi465df!=null) {
				c2seccion_04b.qi465df=c2seccion_04b.setConvertQi465df(c2seccion_04b.qi465df);
			}	
			if (c2seccion_04b.qi465dh!=null) {
				c2seccion_04b.qi465dh=c2seccion_04b.setConvertQi465dh(c2seccion_04b.qi465dh);
			}	
		}
		
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		inicio(); 
    } 

    private void inicio() { 
    	renombrarEtiquetas();
    	onQI465DJ_FChangeValue();
    	onQI465DJ_IChangeValue();
    	evaluar465de();
    	evaluar465dg();
    	evaluar465di();
    	onqrgQI465djxChangeValue();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void evaluar465de() {
    	if(App.getInstance().getSeccion04B().qi465db_a!=null && App.getInstance().getSeccion04B().qi465db_a==2 && App.getInstance().getSeccion04B().qi465db_b!=null && App.getInstance().getSeccion04B().qi465db_b==2 && App.getInstance().getSeccion04B().qi465db_c!=null && App.getInstance().getSeccion04B().qi465db_c==2&& App.getInstance().getSeccion04B().qi465db_d!=null && App.getInstance().getSeccion04B().qi465db_d==2) {
			Util.lockView(getActivity(),false, rgQI465DF);
			App.getInstance().getSeccion04B().filtro465de = true;
			q1.setVisibility(View.VISIBLE);
    	}    	
    	else {
			Util.cleanAndLockView(getActivity(),rgQI465DF);
			App.getInstance().getSeccion04B().filtro465de = false;
			q1.setVisibility(View.GONE);
    	}
    }
    
    public void evaluar465dg() {
    	if( App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi465dd_cc!=null && App.getInstance().getSeccion04B().qi465dd_cc > 0) {
			Util.lockView(getActivity(),false, rgQI465DH );
			App.getInstance().getSeccion04B().filtro465dg = true;
			q2.setVisibility(View.VISIBLE);
    	}    	
    	else {
			Util.cleanAndLockView(getActivity(),rgQI465DH);
			App.getInstance().getSeccion04B().filtro465dg = false;
			q2.setVisibility(View.GONE);
    	}
    }
    
    public void evaluar465di() {
		if( (App.getInstance().getSeccion04B().qi465dd_ac !=null && App.getInstance().getSeccion04B().qi465dd_ar!=null && App.getInstance().getSeccion04B().qi465dd_ac < App.getInstance().getSeccion04B().qi465dd_ar) ||
			(App.getInstance().getSeccion04B().qi465dd_bc !=null && App.getInstance().getSeccion04B().qi465dd_br!=null && App.getInstance().getSeccion04B().qi465dd_bc < App.getInstance().getSeccion04B().qi465dd_br) ||
			(App.getInstance().getSeccion04B().qi465dd_cc !=null && App.getInstance().getSeccion04B().qi465dd_cr!=null && App.getInstance().getSeccion04B().qi465dd_cc < App.getInstance().getSeccion04B().qi465dd_cr) || 
		    (App.getInstance().getSeccion04B().qi465dd_dc !=null && App.getInstance().getSeccion04B().qi465dd_dr!=null && App.getInstance().getSeccion04B().qi465dd_dc < App.getInstance().getSeccion04B().qi465dd_dr)) {
		    Util.lockView(getActivity(),false, chbQI465DJ_A,chbQI465DJ_B,chbQI465DJ_C,chbQI465DJ_D,chbQI465DJ_E,chbQI465DJ_F,chbQI465DJ_G,chbQI465DJ_H,chbQI465DJ_I,txtQI465DJ_O);
		    App.getInstance().getSeccion04B().filtro465di = true;
		    q3.setVisibility(View.VISIBLE);
    	}    	
    	else {
			Util.cleanAndLockView(getActivity(), chbQI465DJ_A,chbQI465DJ_B,chbQI465DJ_C,chbQI465DJ_D,chbQI465DJ_E,chbQI465DJ_F,chbQI465DJ_G,chbQI465DJ_H,chbQI465DJ_I,txtQI465DJ_O );
			App.getInstance().getSeccion04B().filtro465di = false;
			q3.setVisibility(View.GONE);
    	}		
    }
    

	
	public boolean verificarCheck465dj() {
  		if (chbQI465DJ_A.isChecked() || chbQI465DJ_B.isChecked() || chbQI465DJ_C.isChecked() || chbQI465DJ_D.isChecked() || 
  			chbQI465DJ_E.isChecked() || chbQI465DJ_F.isChecked() || chbQI465DJ_G.isChecked() || chbQI465DJ_H.isChecked() || chbQI465DJ_I.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	public void renombrarEtiquetas() {	
		lblpregunta465df.setText("(NOMBRE)");
    	lblpregunta465df.setText(lblpregunta465df.getText().toString().replace("(NOMBRE)", nombre_persona));
    	Spanned texto465df =Html.fromHtml("465DF. D�game por favor, �Cu�l es la raz�n por la cual no recibi� hierro para "+lblpregunta465df.getText() +" de parte del <b>Ministerio de Salud</b>?");
    	lblpregunta465df.setText(texto465df);
    	
    	lblpregunta465dh.setText(lblpregunta465dh.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta465dj.setText(lblpregunta465dj.getText().toString().replace("(NOMBRE)", nombre_persona));    
    	lblpregunta465dh_ind2.setText(lblpregunta465dh_ind2.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void onqrgQI465djxChangeValue() {  	
		if (chbQI465DJ_I.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI465DJ_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI465DJ_O);    		
    	}	
    }

	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI465DF.readOnly();
			rgQI465DH.readOnly();
			chbQI465DJ_A.readOnly();
			chbQI465DJ_B.readOnly();
			chbQI465DJ_C.readOnly();
			chbQI465DJ_D.readOnly();
			chbQI465DJ_E.readOnly();
			chbQI465DJ_F.readOnly();
			chbQI465DJ_G.readOnly();
			chbQI465DJ_H.readOnly();
			chbQI465DJ_I.readOnly();
			txtQI465DF_O.readOnly();
			txtQI465DH_O.readOnly();
			txtQI465DJ_O.readOnly();
			txtQI465DJF_O.readOnly();
		}
	}

	public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
	}
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi465df!=null) {
				c2seccion_04b.qi465df=c2seccion_04b.getConvertQi465df(c2seccion_04b.qi465df);
			}	
			
			if (c2seccion_04b.qi465dh!=null) {
				c2seccion_04b.qi465dh=c2seccion_04b.getConvertQi465dh(c2seccion_04b.qi465dh);
			}	

		}
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
	
} 
