package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
// 
// 
public class C2SECCION_04BFragment_005 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI465A; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI465B; 
	@FieldAnnotation(orderIndex=3) 
	public IntegerField txtQI465C; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI465D; 
	
	public CheckBoxField chb465c;
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465a,lblpregunta465b,lblpregunta465c,lblpregunta465d,lblvacunaveces; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado,seccionesCargado2; 
// 
	String nombre_persona;
	public TextField txtCabecera;
	public ButtonComponent btnNoSabeVacuna465c;
	public GridComponent2 gdVacuna465c;
	public C2SECCION_04BFragment_005() {} 
	public C2SECCION_04BFragment_005 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		rango(getActivity(), txtQI465C, 1, 6); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465A","QI465B","QI465C","QI465D","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465A","QI465B","QI465C","QI465D")}; 
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"INDICE","DIA","MES","ANIO","PREGUNTA","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	   
	    lblpregunta465a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465a);
	    lblpregunta465b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465b);
	    lblpregunta465c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465c);
	    lblpregunta465d = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465d);
	    
	    rgQI465A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465a_1,R.string.c2seccion_04bqi465a_2,R.string.c2seccion_04bqi465a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onrgQI465aChangeValue"); 
		rgQI465B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465b_1,R.string.c2seccion_04bqi465b_2,R.string.c2seccion_04bqi465b_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI465bChangeValue"); 
		txtQI465C=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(1); 
		
		lblvacunaveces  = new LabelComponent(this.getActivity()).size(altoComponente, 150).text(R.string.c2seccion_04bqi465c_1).textSize(16).centrar();
			
		chb465c=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465c_2, "1:0").size(altoComponente, 200);
		chb465c.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(chb465c.isChecked()) {
				Util.cleanAndLockView(getActivity(), txtQI465C);
			}
			else {
				Util.lockView(getActivity(),false, txtQI465C);	
			}
		}
		});
		
		gdVacuna465c = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdVacuna465c.addComponent(lblvacunaveces);
		gdVacuna465c.addComponent(txtQI465C);
		gdVacuna465c.addComponent(chb465c);
				
		rgQI465D=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi465d_1,R.string.c2seccion_04bqi465d_2,R.string.c2seccion_04bqi465d_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 		
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465a,rgQI465A); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465b,rgQI465B); 
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465c,gdVacuna465c.component()); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465d,rgQI465D); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4);  
    return contenedor; 
    } 

    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
		
			if (c2seccion_04b.qi465a!=null) {
				c2seccion_04b.qi465a=c2seccion_04b.getConvertQi465a(c2seccion_04b.qi465a);
			}	
			
			if (c2seccion_04b.qi465b!=null) {
				c2seccion_04b.qi465b=c2seccion_04b.getConvertQi465b(c2seccion_04b.qi465b);
			}
			
			if (c2seccion_04b.qi465d!=null) {
				c2seccion_04b.qi465d=c2seccion_04b.getConvertQi465d(c2seccion_04b.qi465d);
			}
		}
		
		if(chb465c.isChecked() ) {
			c2seccion_04b.qi465c=8;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && !(App.getInstance().getSeccion04B().qi216==1 && App.getInstance().getSeccion04B().filtro215 && App.getInstance().getSeccion04B().qi218==1)){
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_006.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_007.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_008.seccionesGrabado);
					App.getInstance().getSeccion04B().filtro465de = false;
					App.getInstance().getSeccion04B().filtro465dg = false;
					App.getInstance().getSeccion04B().filtro465di = false;
					
					App.getInstance().getSeccion04B().qi465da =false;
					App.getInstance().getSeccion04B().qi465db_a=null;
					App.getInstance().getSeccion04B().qi465db_b=null;
					App.getInstance().getSeccion04B().qi465db_c=null;
					App.getInstance().getSeccion04B().qi465db_d=null;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);		
				}
				
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
//		if( (App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458==1) && 
//			(App.getInstance().getSeccion04B().qi456x!=null && App.getInstance().getSeccion04B().qi456x==1)
//			&& (App.getInstance().getSeccion04B().qi454!=null && App.getInstance().getSeccion04B().qi454==1)
//				) {
		    
		if( App.getInstance().getSeccion04B().qi456x!=null && App.getInstance().getSeccion04B().qi456x==1) {
			if (Util.esVacio(c2seccion_04b.qi465a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI465A"); 
				view = rgQI465A; 
				error = true; 
				return false; 
			} 
		}
		
			if(c2seccion_04b.qi465a==null) {
				if (Util.esVacio(c2seccion_04b.qi465b)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI465B"); 
					view = rgQI465B; 
					error = true; 
					return false; 
				} 
				if(c2seccion_04b.qi465b==1) {
					if (Util.esVacio(c2seccion_04b.qi465c) && !chb465c.isChecked()) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465C"); 
						view = txtQI465C; 
						error = true; 
						return false; 
					} 
					if (Util.esVacio(c2seccion_04b.qi465d)) { 
						mensaje = preguntaVacia.replace("$", "La pregunta QI465D"); 
						view = rgQI465D; 
						error = true; 
						return false; 
					} 
				}
			}
		
		else {
			if(c2seccion_04b.qi465a!=null) {
				if (Util.esVacio(c2seccion_04b.qi465d)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI465D"); 
					view = rgQI465D; 
					error = true; 
					return false; 
				} 
			}
//			if (Util.esVacio(c2seccion_04b.qi465b)) { 
//				mensaje = preguntaVacia.replace("$", "La pregunta QI465B"); 
//				view = rgQI465B; 
//				error = true; 
//				return false; 
//			} 
//			if(c2seccion_04b.qi465b==1) {
//				if (Util.esVacio(c2seccion_04b.qi465c)) { 
//					mensaje = preguntaVacia.replace("$", "La pregunta QI465C"); 
//					view = txtQI465C; 
//					error = true; 
//					return false; 
//				} 
//			}
		}
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi465a!=null) {
				c2seccion_04b.qi465a=c2seccion_04b.setConvertQi465a(c2seccion_04b.qi465a);
			}	
			
			if (c2seccion_04b.qi465b!=null) {
				c2seccion_04b.qi465b=c2seccion_04b.setConvertQi465b(c2seccion_04b.qi465b);
			}
			if (c2seccion_04b.qi465d!=null) {
				c2seccion_04b.qi465d=c2seccion_04b.setConvertQi465d(c2seccion_04b.qi465d);
			}
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		
		if(c2seccion_04b.qi465c!=null && c2seccion_04b.qi465c==8) {
			chb465c.setChecked(true);
			txtQI465C.setText("");
		}	
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	
    	if(App.getInstance().getSeccion04B().qi458!=null && App.getInstance().getSeccion04B().qi458!=1) {
    		evaluar458();
    	}
    	else {
    		if(App.getInstance().getSeccion04B().qi456x!=null || App.getInstance().getSeccion04B().qi454!=null)
        		evaluar465();
    	}
    	
    	if (c2seccion_04b.qi465c!=null && c2seccion_04b.qi465c==8) { Util.cleanAndLockView(getActivity(),txtQI465C); } else{ Util.lockView(getActivity(),false,txtQI465C);}
    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 

    
    public void evaluar465() {
//    	Log.d("Mensaje", "test "+App.getInstance().getSeccion04B().qi456x+" - "+  App.getInstance().getSeccion04B().qi454);
    	if((App.getInstance().getSeccion04B().qi456x!=null && App.getInstance().getSeccion04B().qi456x!=1) || (App.getInstance().getSeccion04B().qi454!=null && App.getInstance().getSeccion04B().qi454!=1)) {
    		Util.cleanAndLockView(getActivity(),rgQI465A);
    		q1.setVisibility(View.GONE);
    		
    		if(c2seccion_04b.qi465b!=null)
        		onqrgQI465bChangeValue();
    	}
    	else {
    		Util.lockView(getActivity(),false,rgQI465A);
    		q1.setVisibility(View.VISIBLE); 
    		if(c2seccion_04b.qi465a!=null)
        		onrgQI465aChangeValue();
    	}
    	
    	
    }
    
    public void evaluar458() {
//    	Log.d("Mensaje","Evaluar 458");
    	
    	if(App.getInstance().getSeccion04B().qi458!=1) {
    		Util.cleanAndLockView(getActivity(),rgQI465A);
    		q1.setVisibility(View.GONE);
    		if(c2seccion_04b.qi465b!=null)
        		onqrgQI465bChangeValue();
    	}    	
    	else {
    		Util.lockView(getActivity(),false,rgQI465A);
    		q1.setVisibility(View.VISIBLE);    		    	
    		if(c2seccion_04b.qi465a!=null)
        		onrgQI465aChangeValue();
    	}
    }


	
	public void onrgQI465aChangeValue() {	
		if(rgQI465A.getValue()!=null) {
		Integer valor = rgQI465A.getValue()!=null?Integer.parseInt(rgQI465A.getValue().toString()):0;
			if (!Util.esDiferente(valor,0)) {
				Util.lockView(getActivity(),false,rgQI465B,txtQI465C,chb465c);  
	    		q2.setVisibility(View.VISIBLE);
	    		q3.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    		if(c2seccion_04b.qi465b!=null)
	        		onqrgQI465bChangeValue();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),rgQI465B,txtQI465C,chb465c);
	    		q2.setVisibility(View.GONE);
	    		q3.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	
	    	}	
		}
	}
	
	public void onqrgQI465bChangeValue() {		
		if(rgQI465B.getValue()!=null) {
			if (rgQI465B.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
//				Log.d("Mensaje","Evaludar 465b mostrar");
				Util.lockView(getActivity(),false,txtQI465C,chb465c,rgQI465D);  
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
//	    		Log.d("Mensaje","Evaludar 465b ocultar");
	    		Util.cleanAndLockView(getActivity(),txtQI465C,chb465c,rgQI465D);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	public void renombrarEtiquetas()
    {	
		CISECCION_04B_TARJETA tarjeta = getCuestionarioService().getMayorRegistro(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,"456x");
    	
		lblpregunta465a.setText(lblpregunta465a.getText().toString().replace("(NOMBRE)", nombre_persona));
		
		if(tarjeta!=null && tarjeta.mes!=null) {
			Integer mes = Integer.parseInt(tarjeta.mes); 
			String smes = MyUtil.Mes(mes-1);
			lblpregunta465a.setText(lblpregunta465a.getText().toString().replace("(MES Y A�O DE LA DOSIS M�S RECIENTE)", " "+smes + " del "+ tarjeta.anio+" "));
		
		}
    	lblpregunta465b.setText(lblpregunta465b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta465d.setText(lblpregunta465d.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI465A.readOnly();
			rgQI465B.readOnly();
			rgQI465D.readOnly();
			chb465c.readOnly();
			txtQI465C.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
		
			if (c2seccion_04b.qi465a!=null) {
				c2seccion_04b.qi465a=c2seccion_04b.getConvertQi465a(c2seccion_04b.qi465a);
			}	
			
			if (c2seccion_04b.qi465b!=null) {
				c2seccion_04b.qi465b=c2seccion_04b.getConvertQi465b(c2seccion_04b.qi465b);
			}
			
			if (c2seccion_04b.qi465d!=null) {
				c2seccion_04b.qi465d=c2seccion_04b.getConvertQi465d(c2seccion_04b.qi465d);
			}
		}
		
		if(chb465c.isChecked() ) {
			c2seccion_04b.qi465c=8;			
		}
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getPersonaCuestionarioIndividual()!=null && App.getInstance().getSeccion04B()!=null && !(App.getInstance().getSeccion04B().qi216==1 && App.getInstance().getSeccion04B().filtro215 && App.getInstance().getSeccion04B().qi218==1)){
					
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_006.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_007.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_008.seccionesGrabado);
					App.getInstance().getSeccion04B().filtro465de = false;
					App.getInstance().getSeccion04B().filtro465dg = false;
					App.getInstance().getSeccion04B().filtro465di = false;
					
					App.getInstance().getSeccion04B().qi465da =false;
					App.getInstance().getSeccion04B().qi465db_a=null;
					App.getInstance().getSeccion04B().qi465db_b=null;
					App.getInstance().getSeccion04B().qi465db_c=null;
					App.getInstance().getSeccion04B().qi465db_d=null;
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);					
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);		
				}
				
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	} 
} 
