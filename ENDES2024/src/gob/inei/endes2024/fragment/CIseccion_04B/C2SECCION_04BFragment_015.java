package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_015 extends FragmentForm { 
	
	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI471; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI471A; 
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI471B_A; 
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI471B_B; 
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI471B_C; 
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI471B_D; 
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI471B_E; 
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI471B_F; 
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI471B_X; 
	@FieldAnnotation(orderIndex=10) 
	public TextField txtQI471B_XO; 
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI471B_Z; 
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI471CA; 
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI471CB; 
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI471CC; 
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI471CD; 
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI471CE; 
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI471CF; 
	
	@FieldAnnotation(orderIndex=18) 
	public RadioGroupOtherField rgQI472; 
	@FieldAnnotation(orderIndex=19) 
	public RadioGroupOtherField rgQI472AA; 
	@FieldAnnotation(orderIndex=20) 
	public RadioGroupOtherField rgQI472AB; 
	@FieldAnnotation(orderIndex=21) 
	public RadioGroupOtherField rgQI472AC; 
	@FieldAnnotation(orderIndex=22) 
	public RadioGroupOtherField rgQI472AD; 
	@FieldAnnotation(orderIndex=23) 
	public IntegerField txtQI472B; 
	@FieldAnnotation(orderIndex=24) 
	public RadioGroupOtherField rgQI472C; 
	
	public CheckBoxField chb472b;
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta471,lblpregunta471a,lblpregunta471b,lblpregunta471b_ind,lblpregunta471c,lblpregunta471c_ind,lblpregunta472,lblpregunta472a,
	lblpregunta472b,lblpregunta472c,lblblanco,lblP472a_1,lblP472a_2,lblP472a_3,lblQI472aA,lblQI472aA2,lblQI472aB,lblQI472aB2,lblQI472aC,
	lblQI472aC2,lblQI472aD,lblQI472aD2,lbldeposicionveces; 
	LinearLayout q0; 
	LinearLayout q1; 
	LinearLayout q2; 
	LinearLayout q3; 
	LinearLayout q4; 
	LinearLayout q5; 
	LinearLayout q6; 
	LinearLayout q7; 
	LinearLayout q8; 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	GridComponent2 grid1,grid2,grid3,grid4,grid5,gdDeposicion472b;
	String nombre_persona;
	public TextField txtCabecera;
	public ButtonComponent btnNoSabeDeposicion472b;
	String fechareferencia;
	public C2SECCION_04BFragment_015() {} 
	public C2SECCION_04BFragment_015 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI471","QI471A","QI471B_A","QI471B_B","QI471B_C","QI471B_D","QI471B_E","QI471B_F","QI471B_X","QI471B_XO","QI471B_Z","QI471CA","QI471CB","QI471CC","QI471CD","QI471CE","QI471CF","QI472","QI472CONS","QI472AA","QI472AB","QI472AC","QI472AD","QI472B","QI472C","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI471","QI471A","QI471B_A","QI471B_B","QI471B_C","QI471B_D","QI471B_E","QI471B_F","QI471B_X","QI471B_XO","QI471B_Z","QI471CA","QI471CB","QI471CC","QI471CD","QI471CE","QI471CF","QI472","QI472CONS","QI472AA","QI472AB","QI472AC","QI472AD","QI472B","QI472C")}; 
		return rootView; 
	} 
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta471 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi471);
	    lblpregunta471a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi471a);
	    lblpregunta471b= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi471b);
	    lblpregunta471b_ind= new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi471b_ind);
	    lblpregunta471c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi471c);
	    lblpregunta471c_ind = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi471c_ind);
	    lblpregunta472 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi472);
	    lblpregunta472a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi472a);
	    lblpregunta472b = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi472b);
	    lblpregunta472c = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi472c);
	    	    
	    rgQI471=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi471_1,R.string.c2seccion_04bqi471_2,R.string.c2seccion_04bqi471_3,R.string.c2seccion_04bqi471_4,R.string.c2seccion_04bqi471_5).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI471A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi471a_1,R.string.c2seccion_04bqi471a_2,R.string.c2seccion_04bqi471a_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI471aChangeValue"); 
		
		chbQI471B_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BaChangeValue");
		chbQI471B_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BbChangeValue");
		chbQI471B_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BcChangeValue");
		chbQI471B_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BdChangeValue");
		chbQI471B_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BeChangeValue");
		chbQI471B_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BfChangeValue");
		chbQI471B_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471bxChangeValue");
		chbQI471B_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471b_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471BzChangeValue");
			
		txtQI471B_XO=new TextField(getActivity()).size(altoComponente, 500).maxLength(100);
		
		chbQI471CA=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471ca, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CaChangeValue");
		chbQI471CB=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471cb, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CbChangeValue");
		chbQI471CC=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471cc, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CcChangeValue");
		chbQI471CD=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471cd, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CdChangeValue");
		chbQI471CE=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471ce, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CeChangeValue");
		chbQI471CF=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi471cf, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI471CfChangeValue");
		
		rgQI472=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472_1,R.string.c2seccion_04bqi472_2,R.string.c2seccion_04bqi472_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("onqrgQI472ChangeValue"); 

		lblblanco = new LabelComponent(getActivity()).textSize(18).size(50, 370).negrita().centrar();
		lblP472a_1 = new LabelComponent(getActivity()).textSize(16).size(50, 99).text(R.string.c2seccion_04bqi472a_1).centrar();
		lblP472a_2 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi472a_2).centrar();
		lblP472a_3 = new LabelComponent(getActivity()).textSize(16).size(50, 98).text(R.string.c2seccion_04bqi472a_3).centrar();
		
		
		lblQI472aA=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi472a_a);		
		rgQI472AA=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472a_o1,R.string.c2seccion_04bqi472a_o2,R.string.c2seccion_04bqi472a_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
				
		lblQI472aB=new LabelComponent(getActivity()).textSize(17).size(altoComponente+10, 450).text(R.string.c2seccion_04bqi472a_b);	
		rgQI472AB=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472a_o1,R.string.c2seccion_04bqi472a_o2,R.string.c2seccion_04bqi472a_o3).size(altoComponente+10,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
		
		lblQI472aC=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi472a_c);
		rgQI472AC=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472a_o1,R.string.c2seccion_04bqi472a_o2,R.string.c2seccion_04bqi472a_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);  
				
		lblQI472aD=new LabelComponent(getActivity()).textSize(17).size(altoComponente, 450).text(R.string.c2seccion_04bqi472a_d);
		rgQI472AD=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472a_o1,R.string.c2seccion_04bqi472a_o2,R.string.c2seccion_04bqi472a_o3).size(altoComponente,300).centrar().orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);//.callback("onqrgQI472adChangeValue");  
		 
		
		grid1=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		grid1.addComponent(lblQI472aA);
		grid1.addComponent(rgQI472AA);
		
		grid1.addComponent(lblQI472aB);
		grid1.addComponent(rgQI472AB);
		
		grid1.addComponent(lblQI472aC);
		grid1.addComponent(rgQI472AC);
		
		grid1.addComponent(lblQI472aD);
		grid1.addComponent(rgQI472AD);
		
		
		
		txtQI472B=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		lbldeposicionveces  = new LabelComponent(this.getActivity()).size(altoComponente, 250).text(R.string.c2seccion_04bqi472b_1).textSize(16).centrar();
		
		chb472b=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi472b_2, "1:0").size(altoComponente, 200);
			chb472b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chb472b.isChecked()) {
					Util.cleanAndLockView(getActivity(), txtQI472B);
				}
				else {
					Util.lockView(getActivity(),false, txtQI472B);	
				}
			}
			});
		
		gdDeposicion472b = new GridComponent2(this.getActivity(),App.ESTILO,3,0);
		gdDeposicion472b.addComponent(lbldeposicionveces);
		gdDeposicion472b.addComponent(txtQI472B);
		gdDeposicion472b.addComponent(chb472b);
		
		rgQI472C=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi472c_1,R.string.c2seccion_04bqi472c_2,R.string.c2seccion_04bqi472c_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
  	} 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta471, rgQI471); 
		q2 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta471a,rgQI471A); 
		LinearLayout ly471a = new LinearLayout(getActivity());
		ly471a.addView(chbQI471B_X);
		ly471a.addView(txtQI471B_XO);
		q3 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta471b,lblpregunta471b_ind, chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,ly471a,chbQI471B_Z); 
		q4 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta471c,lblpregunta471c_ind, chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);		
		q5 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta472,rgQI472); 		
		q6 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta472a,grid1.component());
		q7 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta472b,gdDeposicion472b.component()); 
		q8 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta472c,rgQI472C); 
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1); 
		form.addView(q2); 
		form.addView(q3); 
		form.addView(q4); 
		form.addView(q5); 
		form.addView(q6); 
		form.addView(q7); 
		form.addView(q8);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi471!=null) {
				c2seccion_04b.qi471=c2seccion_04b.getConvertQi471(c2seccion_04b.qi471);
			}	
			
			if (c2seccion_04b.qi471a!=null) {
				c2seccion_04b.qi471a=c2seccion_04b.getConvertQi471a(c2seccion_04b.qi471a);
			}	
			
			if (c2seccion_04b.qi472!=null) {
				c2seccion_04b.qi472=c2seccion_04b.getConvertQi472(c2seccion_04b.qi472);
			}	
			
			if (c2seccion_04b.qi472aa!=null) {
				c2seccion_04b.qi472aa=c2seccion_04b.getConvertQi472a_a(c2seccion_04b.qi472aa);
			}	
			
			if (c2seccion_04b.qi472ab!=null) {
				c2seccion_04b.qi472ab=c2seccion_04b.getConvertQi472a_b(c2seccion_04b.qi472ab);
			}
			
			if (c2seccion_04b.qi472ac!=null) {
				c2seccion_04b.qi472ac=c2seccion_04b.getConvertQi472a_c(c2seccion_04b.qi472ac);
			}
			
			if (c2seccion_04b.qi472ad!=null) {
				c2seccion_04b.qi472ad=c2seccion_04b.getConvertQi472a_d(c2seccion_04b.qi472ad);
			}				
			
			if (c2seccion_04b.qi472c!=null) {
				c2seccion_04b.qi472c=c2seccion_04b.getConvertQi472c(c2seccion_04b.qi472c);
			}	
		}
		
		App.getInstance().getSeccion04B().qi472 = c2seccion_04b.qi472;
		
		if(chb472b.isChecked() ) {
			c2seccion_04b.qi472b=98;			
		}
		
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			
			if(c2seccion_04b.qi472== null) {
				c2seccion_04b.qi472cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi472cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi472cons = fechareferencia;
				}			
			}
			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi472!=1) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_016.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_017.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_018.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);
					App.getInstance().getSeccion04B().qi473d_a=null;
					App.getInstance().getSeccion04B().qi473c = null;
					App.getInstance().getSeccion04B().qi474 = null;
					App.getInstance().getSeccion04B().filtro474a =null;
					App.getInstance().getSeccion04B().qi474a_a=null;
					App.getInstance().getSeccion04B().qi474a_b=null;
					App.getInstance().getSeccion04B().qi474a_c=null;
					App.getInstance().getSeccion04B().qi474a_d=null;
					App.getInstance().getSeccion04B().qi474a_e=null;
					App.getInstance().getSeccion04B().qi474a_f=null;
					App.getInstance().getSeccion04B().qi474a_g=null;
					App.getInstance().getSeccion04B().qi474a_h=null;
					App.getInstance().getSeccion04B().qi474a_i=null;
					App.getInstance().getSeccion04B().qi474a_j=null;
					App.getInstance().getSeccion04B().qi474a_k=null;
					App.getInstance().getSeccion04B().qi474a_l=null;
					App.getInstance().getSeccion04B().qi474a_m=null;
					App.getInstance().getSeccion04B().qi474a_n=null;
					App.getInstance().getSeccion04B().qi474a_o=null;
					App.getInstance().getSeccion04B().qi474a_p=null;
					App.getInstance().getSeccion04B().qi474a_x=null;
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		
		if (App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi468!=null) {
			if(App.getInstance().getSeccion04B().qi467==1 || App.getInstance().getSeccion04B().qi468==1) {
				if (Util.esVacio(c2seccion_04b.qi471)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI471"); 
					view = rgQI471; 
					error = true; 
					return false; 
				} 
				if (Util.esVacio(c2seccion_04b.qi471a)) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QI471A"); 
					view = rgQI471A; 
					error = true; 
					return false; 
				} 
				
				if(c2seccion_04b.qi471a==1) {
					if(!verificarCheck471b() && !chbQI471B_Z.isChecked()) {
						mensaje = "Seleccione una opci�n en P.471b"; 
						view = chbQI471B_A; 
						error = true; 
						return false; 
					}
					
					if (chbQI471B_X.isChecked()) {
						if (Util.esVacio(c2seccion_04b.qi471b_xo)) { 
							mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
							view = txtQI471B_XO; 
							error = true; 
							return false; 
						}
					}	
					if(!(c2seccion_04b.qi471b_f==1 || c2seccion_04b.qi471b_x==1 || c2seccion_04b.qi471b_z==1)) {
						if(!verificarCheck471C() && !chbQI471CF.isChecked()) {
							mensaje = "Seleccione una opci�n en P.471C"; 
							view = chbQI471CA; 
							error = true; 
							return false; 
						}
					}
			}
		  }
		}
		else {
			if (Util.esVacio(c2seccion_04b.qi471)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI471"); 
				view = rgQI471; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi471a)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI471A"); 
				view = rgQI471A; 
				error = true; 
				return false; 
			} 
			
			if(c2seccion_04b.qi471a==1) {
				if(!verificarCheck471b() && !chbQI471B_Z.isChecked()) {
					mensaje = "Seleccione una opci�n en P.471b"; 
					view = chbQI471B_A; 
					error = true; 
					return false; 
				}
				
				if (chbQI471B_X.isChecked()) {
					if (Util.esVacio(c2seccion_04b.qi471b_xo)) { 
						mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
						view = txtQI471B_XO; 
						error = true; 
						return false; 
					}
				}	
				if(!(c2seccion_04b.qi471b_f==1 || c2seccion_04b.qi471b_x==1 || c2seccion_04b.qi471b_z==1)) {
					if(!verificarCheck471C() && !chbQI471CF.isChecked()) {
						mensaje = "Seleccione una opci�n en P.471C"; 
						view = chbQI471CA; 
						error = true; 
						return false; 
					}
				}
			}
		}
			  		
		
		if (Util.esVacio(c2seccion_04b.qi472)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI472"); 
			view = rgQI472; 
			error = true; 
			return false; 
		} 
		if(c2seccion_04b.qi472==1) {
			if (Util.esVacio(c2seccion_04b.qi472aa)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472AA"); 
				view = rgQI472AA; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi472ab)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472AB"); 
				view = rgQI472AB; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi472ac)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472AC"); 
				view = rgQI472AC; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi472ad)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472AD"); 
				view = rgQI472AD; 
				error = true; 
				return false; 
			} 
			if (Util.esVacio(c2seccion_04b.qi472b) && !chb472b.isChecked()) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472B"); 
				view = txtQI472B; 
				error = true; 
				return false; 
			} 
			if (c2seccion_04b.qi472b!=null && (Util.esMenor(c2seccion_04b.qi472b,1) || Util.esMayor(c2seccion_04b.qi472b, 30)) && !chb472b.isChecked()) { 
				mensaje = "Valor fuera de rango pregunta 472B"; 
				view = txtQI472B; 
				error = true; 
				return false; 
			}
			if (Util.esVacio(c2seccion_04b.qi472c)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI472C"); 
				view = rgQI472C; 
				error = true; 
				return false; 
			} 
		}
		
		return true; 
    } 
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi471!=null) {
				c2seccion_04b.qi471=c2seccion_04b.setConvertQi471(c2seccion_04b.qi471);
			}	
			
			if (c2seccion_04b.qi471a!=null) {
				c2seccion_04b.qi471a=c2seccion_04b.setConvertQi471a(c2seccion_04b.qi471a);
			}	
			
			if (c2seccion_04b.qi472!=null) {
				c2seccion_04b.qi472=c2seccion_04b.setConvertQi472(c2seccion_04b.qi472);
			}	
			
			if (c2seccion_04b.qi472aa!=null) {
				c2seccion_04b.qi472aa=c2seccion_04b.setConvertQi472a_a(c2seccion_04b.qi472aa);
			}	
			
			if (c2seccion_04b.qi472ab!=null) {
				c2seccion_04b.qi472ab=c2seccion_04b.setConvertQi472a_b(c2seccion_04b.qi472ab);
			}
			
			if (c2seccion_04b.qi472ac!=null) {
				c2seccion_04b.qi472ac=c2seccion_04b.setConvertQi472a_c(c2seccion_04b.qi472ac);
			}
			
			if (c2seccion_04b.qi472ad!=null) {
				c2seccion_04b.qi472ad=c2seccion_04b.setConvertQi472a_d(c2seccion_04b.qi472ad);
			}				
			
			if (c2seccion_04b.qi472c!=null) {
				c2seccion_04b.qi472c=c2seccion_04b.setConvertQi472c(c2seccion_04b.qi472c);
			}	
		}
		
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		if(c2seccion_04b.qi472b!=null && c2seccion_04b.qi472b==98) {
			chb472b.setChecked(true);
			txtQI472B.setText("");
		}
		
		fechareferencia = c2seccion_04b.qi472cons;
		inicio(); 
    } 
    private void inicio() { 
    	renombrarEtiquetas();
    	validar469();
    	onqrgQI472ChangeValue();
    	
    	if (c2seccion_04b.qi472b!=null && c2seccion_04b.qi472b==98) { Util.cleanAndLockView(getActivity(),txtQI472B); } else{ Util.lockView(getActivity(),false,txtQI472B);}
    	
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public void validar469() {
		if(App.getInstance().getSeccion04B().qi467!=null && App.getInstance().getSeccion04B().qi468!=null ) {
		if (App.getInstance().getSeccion04B().qi467!=1 && App.getInstance().getSeccion04B().qi468!=1) {    		
    		Util.cleanAndLockView(getActivity(),rgQI471,rgQI471A,chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X,chbQI471B_Z,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);
    		q1.setVisibility(View.GONE);
    		q2.setVisibility(View.GONE);
    		q3.setVisibility(View.GONE);
    		q4.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();			
    	} 
    	else {
    		Util.lockView(getActivity(),false,rgQI471,rgQI471A,chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X,chbQI471B_Z,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);
    		q1.setVisibility(View.VISIBLE);
    		q2.setVisibility(View.VISIBLE);
    		q3.setVisibility(View.VISIBLE);
    		q4.setVisibility(View.VISIBLE);
    		
    		onqrgQI471aChangeValue();
        	onqrgQI471CaChangeValue();
        	onqrgQI471CfChangeValue();
        	
    		
    		}
		}
		else {
			onqrgQI471aChangeValue();
        	onqrgQI471CaChangeValue();
        	onqrgQI471CfChangeValue();
		}
	}
    

	
	public boolean verificarCheck471b() {
  		if (chbQI471B_A.isChecked() || chbQI471B_B.isChecked() || chbQI471B_C.isChecked() || chbQI471B_D.isChecked() || 
  				chbQI471B_E.isChecked() || chbQI471B_F.isChecked() || chbQI471B_X.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
	
	
	public void onqrgQI471aChangeValue() {		
		if(rgQI471A.getValue()!=null) {
			if (rgQI471A.getValue().toString().equals("1")) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X,chbQI471B_Z,txtQI471B_XO,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);  
	    		q3.setVisibility(View.VISIBLE);
	    		q4.setVisibility(View.VISIBLE);
	    		onqrgQI471bChangeValue();
	    		onqrgQI471bxChangeValue();
	        	onqrgQI471BzChangeValue();
//	    		MyUtil.LiberarMemoria();
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X,chbQI471B_Z,txtQI471B_XO,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);
	    		q3.setVisibility(View.GONE);
	    		q4.setVisibility(View.GONE);
	    		MyUtil.LiberarMemoria();
	    	}	
		}
    }
	
	
	public void onqrgQI471bChangeValue() {		
		if ((chbQI471B_F.isChecked() || chbQI471B_X.isChecked() || chbQI471B_Z.isChecked()) && 
			(chbQI471B_A.isChecked() || chbQI471B_B.isChecked() || chbQI471B_C.isChecked() || chbQI471B_D.isChecked() || 
  			chbQI471B_E.isChecked())) {    		
    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
			Util.lockView(getActivity(),false,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);  
    		q4.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    	} 
		else if (chbQI471B_F.isChecked() || chbQI471B_X.isChecked() || chbQI471B_Z.isChecked()) { 
			
			Util.cleanAndLockView(getActivity(),chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);
    		q4.setVisibility(View.GONE);
    		MyUtil.LiberarMemoria();

			    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE,chbQI471CF);  
    		q4.setVisibility(View.VISIBLE);
//    		MyUtil.LiberarMemoria();
    	}	
    }
	
	public void onqrgQI471BaChangeValue() {  
		onqrgQI471bChangeValue();
    	if (verificarCheck471b()) {
    		Util.cleanAndLockView(getActivity(),chbQI471B_Z);	
    	} 
    	else {	
    		Util.lockView(getActivity(),false,chbQI471B_Z);  				
    	}	
    }
    public void onqrgQI471BbChangeValue() {
    	onqrgQI471bChangeValue();
    	onqrgQI471BaChangeValue();
    }
    public void onqrgQI471BcChangeValue() {
    	onqrgQI471bChangeValue();
    	onqrgQI471BaChangeValue();
    }
    public void onqrgQI471BdChangeValue() {
    	onqrgQI471bChangeValue();
    	onqrgQI471BaChangeValue();
    }
    public void onqrgQI471BeChangeValue() {  
    	onqrgQI471bChangeValue();
    	onqrgQI471BaChangeValue();	
    }
    
    public void onqrgQI471BfChangeValue() {  
    	onqrgQI471bChangeValue();
    	onqrgQI471BaChangeValue();	
    }
         
    public void onqrgQI471BzChangeValue() {   
    	onqrgQI471bChangeValue();
  		if(chbQI471B_Z.getValue()!=null) {
	    	if (chbQI471B_Z.isChecked()){  		
	  			Util.cleanAndLockView(getActivity(),chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X);  	
	  		} 
	  		else {
	  			Util.lockView(getActivity(),false,chbQI471B_A,chbQI471B_B,chbQI471B_C,chbQI471B_D,chbQI471B_E,chbQI471B_F,chbQI471B_X);  					
	  		}	
  		}
  	}		
	
	public void onqrgQI471bxChangeValue() {  	
		 onqrgQI471bChangeValue();
		 onqrgQI471BaChangeValue();
		if(chbQI471B_X.getValue()!=null) {
			if (chbQI471B_X.isChecked()) {    		
	    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
				Util.lockView(getActivity(),false,txtQI471B_XO);      		
	    	} 
	    	else {	
	    		Util.cleanAndLockView(getActivity(),txtQI471B_XO);    		
	    		//Util.lockView(getActivity(),false,chbQI407_Y);  				
	    	}	
		}
    }
	
	public boolean verificarCheck471C() {
	  	if (chbQI471CA.isChecked() || chbQI471CB.isChecked() || chbQI471CC.isChecked() || chbQI471CD.isChecked() || chbQI471CE.isChecked() ) {
	  		return true;
	  	}else{			
	  		return false;
	  	}
  	}	
	
		public void onqrgQI471CaChangeValue() {  	
	    	if (verificarCheck471C()) {
	    		Util.cleanAndLockView(getActivity(),chbQI471CF);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI471CF);  				
	    	}	
	    }
	    public void onqrgQI471CbChangeValue() {  	
	    	if (verificarCheck471C()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI471CF);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI471CF);  				
	    	}	
	    }
	    public void onqrgQI471CcChangeValue() {  	
	    	if (verificarCheck471C()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI471CF);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI471CF);  				
	    	}	
	    }
	    public void onqrgQI471CdChangeValue() {  	
	    	if (verificarCheck471C()) {    	
	    		Util.cleanAndLockView(getActivity(),chbQI471CF);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI471CF);  				
	    	}	
	    }
	    public void onqrgQI471CeChangeValue() {  	
	    	if (verificarCheck471C()) {    		
	    		Util.cleanAndLockView(getActivity(),chbQI471CF);	
	    	} 
	    	else {	
	    		Util.lockView(getActivity(),false,chbQI471CF);  				
	    	}	
	    }
	 
	 
	    public void onqrgQI471CfChangeValue() {  
	    	if(chbQI471CF.getValue()!=null) {
		  		if (chbQI471CF.isChecked()){  		
		  			Util.cleanAndLockView(getActivity(),chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE);  	
		  		} 
		  		else {
		  			Util.lockView(getActivity(),false,chbQI471CA,chbQI471CB,chbQI471CC,chbQI471CD,chbQI471CE);  					
		  		}	
	    	}
	  	}
	
	    
	    public void onqrgQI472ChangeValue() {		
			if(rgQI472.getValue()!=null) {
	 	    	if (rgQI472.getValue().toString().equals("1")) {    		
		    		//Util.cleanAndLockView(getActivity(),chbQI407_Y);
					Util.lockView(getActivity(),false,rgQI472AA,rgQI472AB,rgQI472AC,rgQI472AD,txtQI472B,chb472b,rgQI472C);
					App.getInstance().getSeccion04B().qi472 = 1;
					q6.setVisibility(View.VISIBLE);
					q7.setVisibility(View.VISIBLE);
		    		q8.setVisibility(View.VISIBLE);
//		    		MyUtil.LiberarMemoria();
		    	} 
		    	else {	
		    		Util.cleanAndLockView(getActivity(),rgQI472AA,rgQI472AB,rgQI472AC,rgQI472AD,txtQI472B,chb472b,rgQI472C);
	    			App.getInstance().getSeccion04B().qi472 = Integer.parseInt(rgQI472.getValue().toString());
		    		q6.setVisibility(View.GONE);
		    		q7.setVisibility(View.GONE);
		    		q8.setVisibility(View.GONE);
		    		MyUtil.LiberarMemoria();
		    	}	
 	    	}
	    }
		
	    
	public void renombrarEtiquetas()
    {	
    	lblpregunta471.setText(lblpregunta471.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta471c.setText(lblpregunta471c.getText().toString().replace("(NOMBRE)", nombre_persona));
    	
    	Calendar fecharef = new GregorianCalendar();
    	
		if(fechareferencia!=null)
		{
			//fecharef = new GregorianCalendar(salud.qs802ca, salud.qs802cm-1, salud.qs802cd);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}
    	Calendar fechainicio = MyUtil.restarDias(fecharef,14);
    	Integer f1 =fechainicio.get(Calendar.DAY_OF_MONTH) ;
    	String mes1=MyUtil.Mes(fechainicio.get(Calendar.MONTH)) ;
    	
    	lblpregunta472.setText(lblpregunta472.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta472.setText(lblpregunta472.getText().toString().replace("$",  f1.toString()+" de "+mes1));
    	lblpregunta472a.setText(lblpregunta472a.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta472b.setText(lblpregunta472b.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta472c.setText(lblpregunta472c.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI471.readOnly();
			rgQI471A.readOnly();
			rgQI472.readOnly();
			rgQI472AA.readOnly();
			rgQI472AB.readOnly();
			rgQI472AC.readOnly();
			rgQI472AD.readOnly();
			rgQI472C.readOnly();
			txtQI471B_XO.readOnly();
			txtQI472B.readOnly();
			chb472b.readOnly();
			chbQI471B_A.readOnly();
			chbQI471B_B.readOnly();
			chbQI471B_C.readOnly();
			chbQI471B_D.readOnly();
			chbQI471B_E.readOnly();
			chbQI471B_F.readOnly();
			chbQI471B_X.readOnly();
			chbQI471B_Z.readOnly();
			chbQI471CA.readOnly();
			chbQI471CB.readOnly();
			chbQI471CC.readOnly();
			chbQI471CD.readOnly();
			chbQI471CE.readOnly();
			chbQI471CF.readOnly();
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		if(c2seccion_04b!=null){
			
			if (c2seccion_04b.qi471!=null) {
				c2seccion_04b.qi471=c2seccion_04b.getConvertQi471(c2seccion_04b.qi471);
			}	
			
			if (c2seccion_04b.qi471a!=null) {
				c2seccion_04b.qi471a=c2seccion_04b.getConvertQi471a(c2seccion_04b.qi471a);
			}	
			
			if (c2seccion_04b.qi472!=null) {
				c2seccion_04b.qi472=c2seccion_04b.getConvertQi472(c2seccion_04b.qi472);
			}	
			
			if (c2seccion_04b.qi472aa!=null) {
				c2seccion_04b.qi472aa=c2seccion_04b.getConvertQi472a_a(c2seccion_04b.qi472aa);
			}	
			
			if (c2seccion_04b.qi472ab!=null) {
				c2seccion_04b.qi472ab=c2seccion_04b.getConvertQi472a_b(c2seccion_04b.qi472ab);
			}
			
			if (c2seccion_04b.qi472ac!=null) {
				c2seccion_04b.qi472ac=c2seccion_04b.getConvertQi472a_c(c2seccion_04b.qi472ac);
			}
			
			if (c2seccion_04b.qi472ad!=null) {
				c2seccion_04b.qi472ad=c2seccion_04b.getConvertQi472a_d(c2seccion_04b.qi472ad);
			}				
			
			if (c2seccion_04b.qi472c!=null) {
				c2seccion_04b.qi472c=c2seccion_04b.getConvertQi472c(c2seccion_04b.qi472c);
			}	
		}
		
		App.getInstance().getSeccion04B().qi472 = c2seccion_04b.qi472;
		
		if(chb472b.isChecked() ) {
			c2seccion_04b.qi472b=98;			
		}

		try { 
			
			if(c2seccion_04b.qi472== null) {
				c2seccion_04b.qi472cons = null;
			} 
			else {
				if(fechareferencia == null) {
					c2seccion_04b.qi472cons =  Util.getFechaActualToString();
				}
				else {
					c2seccion_04b.qi472cons = fechareferencia;
				}			
			}
			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				if(App.getInstance().getSeccion04B()!=null && App.getInstance().getSeccion04B().qi472!=1) {
					List<SeccionCapitulo[]> seccs = new ArrayList<SeccionCapitulo[]>();
					seccs.add(C2SECCION_04BFragment_016.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_017.seccionesGrabado);
					seccs.add(C2SECCION_04BFragment_018.seccionesGrabado);
					SeccionCapitulo[] seccion = MyUtil.agregarCampos(seccs);
					App.getInstance().getSeccion04B().qi473d_a=null;
					App.getInstance().getSeccion04B().qi473c = null;
					App.getInstance().getSeccion04B().qi474 = null;
					App.getInstance().getSeccion04B().filtro474a =null;
					App.getInstance().getSeccion04B().qi474a_a=null;
					App.getInstance().getSeccion04B().qi474a_b=null;
					App.getInstance().getSeccion04B().qi474a_c=null;
					App.getInstance().getSeccion04B().qi474a_d=null;
					App.getInstance().getSeccion04B().qi474a_e=null;
					App.getInstance().getSeccion04B().qi474a_f=null;
					App.getInstance().getSeccion04B().qi474a_g=null;
					App.getInstance().getSeccion04B().qi474a_h=null;
					App.getInstance().getSeccion04B().qi474a_i=null;
					App.getInstance().getSeccion04B().qi474a_j=null;
					App.getInstance().getSeccion04B().qi474a_k=null;
					App.getInstance().getSeccion04B().qi474a_l=null;
					App.getInstance().getSeccion04B().qi474a_m=null;
					App.getInstance().getSeccion04B().qi474a_n=null;
					App.getInstance().getSeccion04B().qi474a_o=null;
					App.getInstance().getSeccion04B().qi474a_p=null;
					App.getInstance().getSeccion04B().qi474a_x=null;
					getCuestionarioService().saveOrUpdate(c2seccion_04b,seccion);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL;
	} 
} 
