package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_v2017_1 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public RadioGroupOtherField rgQI478A1; 
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478A2; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478A3; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478A4; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478A5; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478A6; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478A7; 
	@FieldAnnotation(orderIndex=8) 
	public IntegerField txtQI478A8; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478A9A; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478A9B; 
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI478A10;
	@FieldAnnotation(orderIndex=12) 
	public ButtonComponent btnAceptar;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI478A;
	//public static C2SECCION_04BFragment_018 caller;
	CISECCION_04DIT ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo,lblpregunta478a1,lblpregunta478a1a,lblpregunta4782,lblpregunta4782a,lblpregunta4782b,lblpregunta4782c,lblpregunta4782d,lblpregunta4783,
	lblpregunta4784,lblpregunta4785,lblpregunta4785a,lblpregunta4785b,lblpregunta4786,lblpregunta4787,lblpregunta4788,lblpregunta4788a,
	lblpregunta4789,lblpregunta4789a,lblpregunta4789b,lblpregunta4789c,lblpregunta47810a,lblpregunta47810,lblpregunta47810b,lblpregunta47810c,lbldiscapacidad,lblTramo;
	public GridComponent2 gridpregunta4789,gridDiscapacidad;
	public ButtonComponent  btnCancelar;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_v2017_1() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478A1","QI478A2","QI478A3","QI478A4","QI478A5","QI478A6","QI478A7","QI478A8","QI478A9A","QI478A9B","QI478A10","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478A1","QI478A2","QI478A3","QI478A4","QI478A5","QI478A6","QI478A7","QI478A8","QI478A9A","QI478A9B","QI478A10","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_v2017_1 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo9_12).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478a);
		lblTramo = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478a_ind);
				
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478a1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478a1a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		lblpregunta4782 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta4782a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a2a).negrita();
		lblpregunta4782b = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a2b);
		lblpregunta4782c = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a2c).negrita();
		lblpregunta4782d = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a2d);
		lblpregunta4783 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta4784 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("478A4.- Cuando (NOMBRE)");
		lblpregunta4785 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta4785a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a5a).negrita();
		lblpregunta4785b = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a5b);
		lblpregunta4786 =  new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");
		lblpregunta4787 =  new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");
		lblpregunta4788 = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("(NOMBRE)");
		lblpregunta4788a = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a8a);
		lblpregunta4789 = new LabelComponent(getActivity()).size(altoComponente,MATCH_PARENT).textSize(19).text("(NOMBRE)");
		lblpregunta4789a = new LabelComponent(getActivity()).size(altoComponente, 290).textSize(19).text("");
		lblpregunta4789b = new LabelComponent(getActivity()).size(altoComponente+60, 290).textSize(19).text("");
		lblpregunta4789c = new LabelComponent(getActivity()).size(altoComponente+40, 755).textSize(16).text("");
		lblpregunta47810= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478a10);
		lblpregunta47810a= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(17).text(R.string.c2seccion_04b_2qi478a10a).negrita();
		lblpregunta47810b= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a10b);
		lblpregunta47810c= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04b_2qi478a10c).negrita();
		
		rgQI478A1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a1_1,R.string.c2seccion_04b_2qi478a1_2,R.string.c2seccion_04b_2qi478a1_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a2_1,R.string.c2seccion_04b_2qi478a2_2,R.string.c2seccion_04b_2qi478a2_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a3_1,R.string.c2seccion_04b_2qi478a3_2,R.string.c2seccion_04b_2qi478a3_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a4_1,R.string.c2seccion_04b_2qi478a4_2,R.string.c2seccion_04b_2qi478a4_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a5_1,R.string.c2seccion_04b_2qi478a5_2,R.string.c2seccion_04b_2qi478a5_3,R.string.c2seccion_04b_2qi478a5_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		rgQI478A6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a6_1,R.string.c2seccion_04b_2qi478a6_2,R.string.c2seccion_04b_2qi478a6_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a7_1,R.string.c2seccion_04b_2qi478a7_2,R.string.c2seccion_04b_2qi478a7_3,R.string.c2seccion_04b_2qi478a7_4).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL); 
		txtQI478A8=new IntegerField(this.getActivity()).size(altoComponente, 80).maxLength(2); 
		rgQI478A9A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a9a_1,R.string.c2seccion_04b_2qi478a9a_2,R.string.c2seccion_04b_2qi478a9a_3,R.string.c2seccion_04b_2qi478a9a_4).size(altoComponente,465).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A9B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a9b_1,R.string.c2seccion_04b_2qi478a9b_2,R.string.c2seccion_04b_2qi478a9b_3,R.string.c2seccion_04b_2qi478a9b_4).size(altoComponente+50,465).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL); 
		rgQI478A10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478a10_1,R.string.c2seccion_04b_2qi478a10_2,R.string.c2seccion_04b_2qi478a10_3).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		
		gridpregunta4789=new GridComponent2(this.getActivity(),Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 2,1);
		gridpregunta4789.addComponent(lblpregunta4789a);
		gridpregunta4789.addComponent(rgQI478A9A);
		gridpregunta4789.addComponent(lblpregunta4789b);
		gridpregunta4789.addComponent(rgQI478A9B);
		gridpregunta4789.addComponent(lblpregunta4789c,2);

	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q12 = createQuestionSection(gridDiscapacidad.component());
		q13 = createQuestionSection(lblTramo);
		q1 = createQuestionSection(0,lblpregunta478a1,lblpregunta478a1a,rgQI478A1); 
		q2 = createQuestionSection(0,lblpregunta4782,lblpregunta4782a,lblpregunta4782b,lblpregunta4782c,lblpregunta4782d,rgQI478A2); 
		q3 = createQuestionSection(0,lblpregunta4783,rgQI478A3); 
		q4 = createQuestionSection(0,lblpregunta4784,rgQI478A4); 
		q5 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta4785,lblpregunta4785a,lblpregunta4785b,rgQI478A5); 
		q6 = createQuestionSection(0,lblpregunta4786,rgQI478A6); 
		q7 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta4787,rgQI478A7); 
		q8 = createQuestionSection(0,lblpregunta4788,lblpregunta4788a,txtQI478A8); 
		q10 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta4789,gridpregunta4789.component()); 
		q11 = createQuestionSection(0,lblpregunta47810a,lblpregunta47810,lblpregunta47810b,lblpregunta47810c,rgQI478A10);           

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q12);
		form.addView(q13);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4); 
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8);             
		form.addView(q10); 
		form.addView(q11); 

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesparalabasededatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		return flag;
	}
	public void cambiarVariablesparalabasededatos(){
		ninio.qi478a1=ninio.qi478a1!=null?ninio.setConvertcontresvariables(ninio.qi478a1):null;
		ninio.qi478a2=ninio.qi478a2!=null?ninio.setConvertcontresvariables(ninio.qi478a2):null;
		ninio.qi478a3=ninio.qi478a3!=null?ninio.setConvertcontresvariables(ninio.qi478a3):null;
		ninio.qi478a4=ninio.qi478a4!=null?ninio.setConvertcontresvariables(ninio.qi478a4):null;
		ninio.qi478a6=ninio.qi478a6!=null?ninio.setConvertcontresvariables(ninio.qi478a6):null;
		ninio.qi478a9a=ninio.qi478a9a!=null?ninio.setConvertconcuatrovariables(ninio.qi478a9a):null;
		ninio.qi478a9b=ninio.qi478a9b!=null?ninio.setConvertconcuatrovariables(ninio.qi478a9b):null;
		ninio.qi478a10=ninio.qi478a10!=null?ninio.setConvertcontresvariables(ninio.qi478a10):null;
	}
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478a1  == null) {
				error = true;
				view = rgQI478A1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A1");
				return false;
			}
			if (ninio.qi478a2  == null) {
				error = true;
				view = rgQI478A2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A2");
				return false;
			}
			if (ninio.qi478a3  == null) {
				error = true;
				view = rgQI478A3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A3");
				return false;
			}
			if (ninio.qi478a4  == null) {
				error = true;
				view = rgQI478A4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A4");
				return false;
			}
			if (ninio.qi478a5  == null) {
				error = true;
				view = rgQI478A5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A5");
				return false;
			}
			if (ninio.qi478a6  == null) {
				error = true;
				view = rgQI478A6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A6");
				return false;
			}
			if (ninio.qi478a7  == null) {
				error = true;
				view = rgQI478A7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A7");
				return false;
			}
			if (ninio.qi478a8  == null) {
				error = true;
				view = txtQI478A8;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A8");
				return false;
			}
			if (ninio.qi478a9a  == null) {
				error = true;
				view = rgQI478A9A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A9A");
				return false;
			}
			if (ninio.qi478a9b  == null) {
				error = true;
				view = rgQI478A9B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A9B");
				return false;
			}
			if (ninio.qi478a10  == null) {
				error = true;
				view = rgQI478A10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478A10");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT()!=null) {
			
			
		ninio = getCuestionarioService().getCISECCION_04B_2(App.getInstance().getSeccion04DIT().id,App.getInstance().getSeccion04DIT().hogar_id,App.getInstance().getSeccion04DIT().persona_id,App.getInstance().getSeccion04DIT().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT();
			ninio.id = App.getInstance().getSeccion04DIT().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT().persona_id;
			//ninio.niniodit_id= App.getInstance().getSeccion04DIT().niniodit_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT().nro_orden_ninio;
		}
		cambiarvariablesparamostrarausuario();
		entityToUI(ninio);
		inicio();
		}
       	}
	private void cambiarvariablesparamostrarausuario(){
		ninio.qi478a1 =ninio.qi478a1!=null?ninio.getConvertcontresvariables(ninio.qi478a1):null;
		ninio.qi478a2=ninio.qi478a2!=null?ninio.getConvertcontresvariables(ninio.qi478a2):null;
		ninio.qi478a3=ninio.qi478a3!=null?ninio.getConvertcontresvariables(ninio.qi478a3):null;
		ninio.qi478a4=ninio.qi478a4!=null?ninio.getConvertcontresvariables(ninio.qi478a4):null;
		ninio.qi478a6=ninio.qi478a6!=null?ninio.getConvertcontresvariables(ninio.qi478a6):null;
		ninio.qi478a9a=ninio.qi478a9a!=null?ninio.getConvertconcuatrovariables(ninio.qi478a9a):null;
		ninio.qi478a9b=ninio.qi478a9b!=null?ninio.getConvertconcuatrovariables(ninio.qi478a9b):null;
		ninio.qi478a10=ninio.qi478a10!=null?ninio.getConvertcontresvariables(ninio.qi478a10):null;
	}
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
	}
	
	public void evaluarPregunta() {
		
		if(App.getInstance().getSeccion04B()!=null && (App.getInstance().getSeccion04DIT()==null || 
		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=15 && App.getInstance().getSeccion04DIT().qi478 <=18) || 
		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=30 && App.getInstance().getSeccion04DIT().qi478 <=36) ||
		  (App.getInstance().getSeccion04DIT()!=null && App.getInstance().getSeccion04DIT().qi478 >=53 && App.getInstance().getSeccion04DIT().qi478 <=59)
				) ) {
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478A1,rgQI478A2,rgQI478A3,rgQI478A4,rgQI478A5,rgQI478A6,rgQI478A7,txtQI478A8,rgQI478A9A,rgQI478A9B,rgQI478A10);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
		}
		else {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478A1,rgQI478A2,rgQI478A3,rgQI478A4,rgQI478A5,rgQI478A6,rgQI478A7,txtQI478A8,rgQI478A9A,rgQI478A9B,rgQI478A10);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
	}
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478A1,rgQI478A2,rgQI478A3,rgQI478A4,rgQI478A5,rgQI478A6,rgQI478A7,txtQI478A8,rgQI478A9A,rgQI478A9B,rgQI478A10);
			MyUtil.LiberarMemoria();
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
		}
		else{
			Util.lockView(getActivity(), false,rgQI478A1,rgQI478A2,rgQI478A3,rgQI478A4,rgQI478A5,rgQI478A6,rgQI478A7,txtQI478A8,rgQI478A9A,rgQI478A9B,rgQI478A10);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
		}
	}
	public void RenombrarEtiquetas(){
		
		lblpregunta478a1.setText("");
		lblpregunta4785.setText("(NOMBRE)");
		String replace="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		Spanned texto4718a1 = Html.fromHtml("478A1. Cuando llaman a (NOMBRE)");
		lblpregunta478a1.setText(texto4718a1);
		lblpregunta478a1.setText(lblpregunta478a1.getText().toString().replace("(NOMBRE)",ninio.qi212_nom));
		Spanned textoadicional=Html.fromHtml(lblpregunta478a1.getText()+" <b>por su nombre</b> �ella(&eacute;l) <b>responde</b> con alg&uacute;n movimiento o haciendo alg&uacute;n sonido?");
		lblpregunta478a1.setText(textoadicional);
		
		Spanned texto=Html.fromHtml("<b>SELECCIONE \"1\" CUANDO:</b><br>LA MADRE REPORTA ALGUNA DE LAS SIGUIENTES RESPUESTAS O SIMILARES: <br>- VOLTEA LA CABEZA<br>- MUEVE SUS BRAZOS O PIERNAS<br>- REALIZA ALG&Uacute;N SONIDO<br>- ABRE INTENCIONALMENTE LOS OJOS");
		lblpregunta478a1a.setText(texto); 
		
		Spanned texto1=Html.fromHtml("478A2. Cuando (NOMBRE)");
		lblpregunta4782.setText(texto1);
		lblpregunta4782.setText(lblpregunta4782.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		Spanned texto2 = Html.fromHtml(lblpregunta4782.getText()+"<b> est� intentando hablar</b> �Qu� sonido hace?");
		lblpregunta4782.setText(texto2);
		Spanned texto4783 = Html.fromHtml("478A3. Mientras (NOMBRE)");
		lblpregunta4783.setText(texto4783);
		lblpregunta4783.setText(lblpregunta4783.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		Spanned texto47831=Html.fromHtml(lblpregunta4783.getText()+"<b> est� jugando sola (solo)</b> �<b>en alg�n momento</b> ella (�l) la busca a usted y despu�s<b> vuelve a jugar   sola (solo)</b>?");
		lblpregunta4783.setText(texto47831);
		lblpregunta4784.setText(lblpregunta4784.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		Spanned texto4784=Html.fromHtml(lblpregunta4784.getText()+" est� fastidiada (fastidiado) y usted<b> trata de calmarla (calmarlo) </b>�ella (�l) se calma <b>f�cilmente</b>?");
		lblpregunta4784.setText(texto4784);
		lblpregunta4785.setText(lblpregunta4785.getText().toString().replace("(NOMBRE)",ninio.qi212_nom));
		lblpregunta4785.setText(Html.fromHtml("478A5. Si "+lblpregunta4785.getText()+ " llora<b> cuando usted est� haciendo algo</b> �qu� hace usted?"));
		lblpregunta4786.text("(NOMBRE)");
		lblpregunta4786.setText(lblpregunta4786.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		lblpregunta4786.setText(Html.fromHtml("478A6. �Le es <b>f�cil entender</b> por qu� "+ lblpregunta4786.getText()+"<b> llora</b>?"));
		lblpregunta4787.text("(NOMBRE)");
		lblpregunta4787.setText(lblpregunta4787.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		lblpregunta4787.setText(Html.fromHtml(getResources().getString(R.string.c2seccion_04b_2qi478a7)+" <b>al d�a se separ�</b> de "+lblpregunta4787.getText()+"?"));
		lblpregunta4788.text("(NOMBRE)");
		lblpregunta4788.setText(lblpregunta4788.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		lblpregunta4788.setText(Html.fromHtml("478A8. Adem�s de usted, <b>normalmente</b> �cu�ntas personas <b>le hablan a </b>"+lblpregunta4788.getText()+"?"));
		lblpregunta4789.text("(NOMBRE)");
		lblpregunta4789.setText(lblpregunta4789.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
		lblpregunta4789.setText(Html.fromHtml("478A9. El piso donde "+lblpregunta4789.getText()+" juega <b>normalmente</b>:"));
		lblpregunta4789a.setText(Html.fromHtml("<b>A.</b>�Es plano?"));
		lblpregunta4789b.setText(Html.fromHtml("<b>B.</b>�Est� cubierto con <b>una manta o algo similar</b>?"));
		lblpregunta4789c.setText(Html.fromHtml("<b>SELECCIONE \"3\" EN CADA SUBPREGUNTA CUANDO:</b>\nLA MADRE REPORTA QUE LA NI�A O EL NI�O NORMALMENTE NO JUEGA EN EL PISO"));
		lblpregunta47810.setText(lblpregunta47810.getText().toString().replace("(NOMBRE)", ninio.qi212_nom));
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI478A1.readOnly();
			rgQI478A2.readOnly();
			rgQI478A3.readOnly();
			rgQI478A4.readOnly();
			rgQI478A5.readOnly();
			rgQI478A6.readOnly();
			rgQI478A7.readOnly();
			rgQI478A9A.readOnly();
			rgQI478A9B.readOnly();
			rgQI478A10.readOnly();
			txtQI478A8.readOnly();
			chbQI478A.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
