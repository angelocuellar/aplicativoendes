package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_021 extends FragmentForm { 

	@FieldAnnotation(orderIndex=1) 	
	public RadioGroupOtherField rgQI477_3;
	
	@FieldAnnotation(orderIndex=2) 
	public CheckBoxField chbQI477_4_A;
	@FieldAnnotation(orderIndex=3) 
	public CheckBoxField chbQI477_4_B;
	@FieldAnnotation(orderIndex=4) 
	public CheckBoxField chbQI477_4_C;
	@FieldAnnotation(orderIndex=5) 
	public CheckBoxField chbQI477_4_D;
	@FieldAnnotation(orderIndex=6) 
	public CheckBoxField chbQI477_4_E;
	@FieldAnnotation(orderIndex=7) 
	public CheckBoxField chbQI477_4_F;
	@FieldAnnotation(orderIndex=8) 
	public CheckBoxField chbQI477_4_G;
	@FieldAnnotation(orderIndex=9) 
	public CheckBoxField chbQI477_4_H;
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI477_4_I;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI477_4_J;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI477_4_K;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI477_4_X;
	@FieldAnnotation(orderIndex=14) 
	public TextField txtQI477_4_X_O;
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI477_4_Y;
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI477_4_Z;
	
	@FieldAnnotation(orderIndex=17) 	
	public RadioGroupOtherField rgQI477A; 
	@FieldAnnotation(orderIndex=18) 
	public TextAreaField txtQIOBS_4B;
	
	
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblobservacion,lblpregunta477a,lblpregunta477_3,lblpregunta477_4; 
	LinearLayout q0,q1,q2,q3,q4; 
 
	SeccionCapitulo[] seccionesGrabado; 
	SeccionCapitulo[] seccionesCargado; 

	GridComponent2 grid1,grid2,grid3,grid4,grid5,grid6,grid7,grid8,grid0;
	String nombre_persona;
	public TextField txtCabecera;
	String fechareferencia;
	public C2SECCION_04BFragment_021() {} 
	public C2SECCION_04BFragment_021 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
  @Override 
  public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
  
  @Override 
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView);
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI477_3", "QI477_4_A", "QI477_4_B", "QI477_4_C", "QI477_4_D", "QI477_4_E", "QI477_4_F", "QI477_4_G", "QI477_4_H", "QI477_4_I", "QI477_4_J", "QI477_4_K", "QI477_4_X", "QI477_4_X_O", "QI477_4_Y", "QI477_4_Z", "QI477A","QIOBS_4B", "QI477CONS","QI472","QI468","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")}; 
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI477_3", "QI477_4_A", "QI477_4_B", "QI477_4_C", "QI477_4_D", "QI477_4_E", "QI477_4_F", "QI477_4_G", "QI477_4_H", "QI477_4_I", "QI477_4_J", "QI477_4_K", "QI477_4_X", "QI477_4_X_O", "QI477_4_Y", "QI477_4_Z", "QI477A","QIOBS_4B")}; 
		return rootView; 
  } 
  
  @Override 
  protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	    
	    lblpregunta477_3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477_3);
	    lblpregunta477_4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477_4a);
	    lblpregunta477a = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi477a);
	    lblobservacion =  new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqiobservacion);
		
	    rgQI477_3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi477_3_1,R.string.c2seccion_04bqi477_3_2,R.string.c2seccion_04bqi477_3_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar().callback("onQI477_3ChangeValue");
	    chbQI477_4_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_aChangeValue");
	    chbQI477_4_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_bChangeValue");
	    chbQI477_4_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_cChangeValue");
	    chbQI477_4_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_dChangeValue");
	    chbQI477_4_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_eChangeValue");
	    chbQI477_4_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_fChangeValue");
	    chbQI477_4_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_gChangeValue");
	    chbQI477_4_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_hChangeValue");
	    chbQI477_4_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_iChangeValue");
	    chbQI477_4_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_jChangeValue");
	    chbQI477_4_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_kChangeValue");
	    chbQI477_4_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_xChangeValue");
	    chbQI477_4_Y=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_y, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_yChangeValue");
	    chbQI477_4_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi477_4_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI477_4_zChangeValue");
	    txtQI477_4_X_O=new TextField(this.getActivity()).maxLength(500).size(altoComponente, 500).centrar().alfanumerico();
	    
		rgQI477A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04bqi477a_1,R.string.c2seccion_04bqi477a_2).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		txtQIOBS_4B= new TextAreaField(getActivity()).size(200, 700).maxLength(1000).alfanumerico();
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta477_3,rgQI477_3);
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta477_4,chbQI477_4_A,chbQI477_4_B,chbQI477_4_C,chbQI477_4_D,chbQI477_4_E,chbQI477_4_F,chbQI477_4_G,chbQI477_4_H,chbQI477_4_I,chbQI477_4_J,chbQI477_4_K,chbQI477_4_X,txtQI477_4_X_O,chbQI477_4_Y,chbQI477_4_Z);
		q3 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta477a,rgQI477A); 
		q4 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblobservacion,txtQIOBS_4B);

		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		form.addView(q3); 
		form.addView(q4); 
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b); 
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 			
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				SeccionCapitulo[] seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478","ID","HOGAR_ID","PERSONA_ID","NRO_ORDEN_NINIO")};
				CISECCION_04DIT_02 ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
				if(ninio!=null){		
					App.getInstance().setSeccion04DIT_02(null);
					App.getInstance().setSeccion04DIT_02(ninio);
				}	
				else{ 
					App.getInstance().setSeccion04DIT_02(null);
				}
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);

		if (Util.esVacio(c2seccion_04b.qi477_3)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI477_3"); 
			view = rgQI477_3; 
			error = true; 
			return false; 
		} 
		
		if(!verificarCheckP477_4()){
			mensaje = "Seleccione una opci�n en la pregunta QI477_4_A"; 
			view = chbQI477_4_A; 
			error = true; 
			return false; 
		}
		if(chbQI477_4_X.isChecked()) {
			if (Util.esVacio(c2seccion_04b.qi477_4_x_o)) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QI477_4_X_O"); 
				view = txtQI477_4_X_O; 
				error = true; 
				return false; 
			} 
		}
		
		if (Util.esVacio(c2seccion_04b.qi477a)) { 
			mensaje = preguntaVacia.replace("$", "La pregunta QI477A"); 
			view = rgQI477A; 
			error = true; 
			return false; 
		} 
		return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    } 
		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		fechareferencia = c2seccion_04b.qi477cons;
		inicio(); 
    } 
    
    
    private void inicio() { 
    	renombrarEtiquetas();
    	ejecutaAccionCheckbox();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    
    public boolean verificarCheckP477_4() {
  		if (chbQI477_4_A.isChecked()||chbQI477_4_B.isChecked()||chbQI477_4_C.isChecked()||chbQI477_4_D.isChecked()||chbQI477_4_E.isChecked()||chbQI477_4_F.isChecked()||chbQI477_4_G.isChecked()||chbQI477_4_H.isChecked()||chbQI477_4_I.isChecked()||chbQI477_4_J.isChecked()||chbQI477_4_K.isChecked()||chbQI477_4_X.isChecked()||chbQI477_4_Y.isChecked()||chbQI477_4_Z.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    public void ejecutaAccionCheckbox() {
		if(chbQI477_4_Z.isChecked()){
			onqrgQI477_4_zChangeValue();
		}
		else{
			if(chbQI477_4_Y.isChecked()){
				onqrgQI477_4_yChangeValue();
			}
			else{
				onqrgQI477_4_aChangeValue();
				onqrgQI477_4_bChangeValue();
				onqrgQI477_4_cChangeValue();
				onqrgQI477_4_dChangeValue();
				onqrgQI477_4_eChangeValue();
				onqrgQI477_4_fChangeValue();
				onqrgQI477_4_gChangeValue();
				onqrgQI477_4_hChangeValue();
				onqrgQI477_4_iChangeValue();
				onqrgQI477_4_jChangeValue();
				onqrgQI477_4_kChangeValue();
				onqrgQI477_4_xChangeValue();
			}
		}
	}
	
    public void onqrgQI477_4_aChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_bChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_cChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_dChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_eChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_fChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_gChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_hChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_iChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_jChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_kChangeValue(){if (verificarCheckP477_4()){Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);}else{Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);}}
    public void onqrgQI477_4_xChangeValue(){
    	if (verificarCheckP477_4()){
    		Util.cleanAndLockView(getActivity(),chbQI477_4_Y,chbQI477_4_Z);
    	}
    	else{
    		Util.lockView(getActivity(),false,chbQI477_4_Y,chbQI477_4_Z);
    	}
    	if (chbQI477_4_X.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI477_4_X_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI477_4_X_O);    		
    	}
    }
    public void onqrgQI477_4_yChangeValue(){
    	if (verificarCheckP477_4()){
    		Util.cleanAndLockView(getActivity(),chbQI477_4_A,chbQI477_4_B,chbQI477_4_C,chbQI477_4_D,chbQI477_4_E,chbQI477_4_F,chbQI477_4_G,chbQI477_4_H,chbQI477_4_I,chbQI477_4_J,chbQI477_4_K,chbQI477_4_X,chbQI477_4_Z);
    	}
    	else{
    		Util.lockView(getActivity(),false,chbQI477_4_A,chbQI477_4_B,chbQI477_4_C,chbQI477_4_D,chbQI477_4_E,chbQI477_4_F,chbQI477_4_G,chbQI477_4_H,chbQI477_4_I,chbQI477_4_J,chbQI477_4_K,chbQI477_4_X,chbQI477_4_Z);
    	}
    }
    public void onqrgQI477_4_zChangeValue(){
    	if (verificarCheckP477_4()){
    		Util.cleanAndLockView(getActivity(),chbQI477_4_A,chbQI477_4_B,chbQI477_4_C,chbQI477_4_D,chbQI477_4_E,chbQI477_4_F,chbQI477_4_G,chbQI477_4_H,chbQI477_4_I,chbQI477_4_J,chbQI477_4_K,chbQI477_4_X,chbQI477_4_Y);
    	}
    	else{
    		Util.lockView(getActivity(),false,chbQI477_4_A,chbQI477_4_B,chbQI477_4_C,chbQI477_4_D,chbQI477_4_E,chbQI477_4_F,chbQI477_4_G,chbQI477_4_H,chbQI477_4_I,chbQI477_4_J,chbQI477_4_K,chbQI477_4_X,chbQI477_4_Y);
    	}
    }
    
	public void renombrarEtiquetas(){	
		Calendar fecharef = new GregorianCalendar();
		if(fechareferencia!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date2 = null;
			try {
				date2 = df.parse(fechareferencia);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			Calendar cal = Calendar.getInstance();
			cal.setTime(date2);
			fecharef=cal;
		}	
	
    	Calendar fechainicio = MyUtil.RestarMeses(fecharef,1);
    	String F1 = MyUtil.Mes(fechainicio.get(Calendar.MONTH));
    	Calendar fechados =	MyUtil.RestarMeses(fecharef,11);
    	String F2 = MyUtil.Mes(fechados.get(Calendar.MONTH));

    	String F3="";
    	if(F1.equals("Diciembre")){
    		F3 = "del a�o pasado, ";
    	}else{
    		F3 = "de este a�o, ";
    	}
    	lblpregunta477_3.setText(lblpregunta477_3.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta477_3.setText(lblpregunta477_3.getText().toString().replace("$", F2));
    	lblpregunta477_3.setText(lblpregunta477_3.getText().toString().replace("#", F1));
    	lblpregunta477_3.setText(lblpregunta477_3.getText().toString().replace("%", F3));

    	if(!Util.esDiferente(c2seccion_04b.qi477_3, 2,3)){
			lblpregunta477_4.text(R.string.c2seccion_04bqi477_4b);
		}
		else{
			lblpregunta477_4.text(R.string.c2seccion_04bqi477_4a);
		}
    	lblpregunta477_4.setText(lblpregunta477_4.getText().toString().replace("(NOMBRE)", nombre_persona));
    	lblpregunta477a.setText(lblpregunta477a.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
	
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQI477_3.readOnly();
			chbQI477_4_A.readOnly();
			chbQI477_4_B.readOnly();
			chbQI477_4_C.readOnly();
			chbQI477_4_D.readOnly();
			chbQI477_4_E.readOnly();
			chbQI477_4_F.readOnly();
			chbQI477_4_G.readOnly();
			chbQI477_4_H.readOnly();
			chbQI477_4_I.readOnly();
			chbQI477_4_J.readOnly();
			chbQI477_4_K.readOnly();
			chbQI477_4_X.readOnly();
			txtQI477_4_X_O.readOnly();
			chbQI477_4_Y.readOnly();
			chbQI477_4_Z.readOnly();
			rgQI477A.readOnly();
			txtQIOBS_4B.setEnabled(false);
		}
	}
	public void onQI477_3ChangeValue(){
		int valor=Integer.parseInt(rgQI477_3.getTagSelected("0").toString());
		if(!Util.esDiferente(valor, 2,3)){
			lblpregunta477_4.text(R.string.c2seccion_04bqi477_4b);
		}
		else{
			lblpregunta477_4.text(R.string.c2seccion_04bqi477_4a);
		}
		lblpregunta477_4.setText(lblpregunta477_4.getText().toString().replace("(NOMBRE)", nombre_persona));
	}
	private void validarMensaje(String msj) {
        ToastMessage.msgBox(getActivity(), msj, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
    }

	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		try { 	
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
