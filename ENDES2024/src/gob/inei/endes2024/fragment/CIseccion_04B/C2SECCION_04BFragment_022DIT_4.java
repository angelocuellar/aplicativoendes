package gob.inei.endes2024.fragment.CIseccion_04B;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.CIseccion_04B.Dialog.C2SECCION_04BFragment_018DIT_4_Dialog;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_022DIT_4 extends FragmentForm {

	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI478A;
	@FieldAnnotation(orderIndex=2) 
	public RadioGroupOtherField rgQI478H1; 
	@FieldAnnotation(orderIndex=3) 
	public RadioGroupOtherField rgQI478H2; 
	@FieldAnnotation(orderIndex=4) 
	public RadioGroupOtherField rgQI478H3; 
	@FieldAnnotation(orderIndex=5) 
	public RadioGroupOtherField rgQI478H4; 
	@FieldAnnotation(orderIndex=6) 
	public RadioGroupOtherField rgQI478H5; 
	@FieldAnnotation(orderIndex=7) 
	public RadioGroupOtherField rgQI478H6; 
	@FieldAnnotation(orderIndex=8) 
	public RadioGroupOtherField rgQI478H7; 
	@FieldAnnotation(orderIndex=9) 
	public RadioGroupOtherField rgQI478H8_A; 
	@FieldAnnotation(orderIndex=10) 
	public RadioGroupOtherField rgQI478H8_B;
	@FieldAnnotation(orderIndex=11) 
	public RadioGroupOtherField rgQI478H9; 
	@FieldAnnotation(orderIndex=12) 
	public RadioGroupOtherField rgQI478H10;
	@FieldAnnotation(orderIndex=13) 
	public RadioGroupOtherField rgQI478H11;
	@FieldAnnotation(orderIndex=14) 
	public RadioGroupOtherField rgQI478H12;
	

	
	CISECCION_04DIT_02 ninio;
	private CuestionarioService cuestionarioService;
	private SeccionCapitulo[] seccionesCargado;
	private SeccionCapitulo[] seccionesGrabado;
	LabelComponent lbltitulo, lbldiscapacidad,lblIndicacion, lblpregunta478h1, lblpregunta478h1_ind,lblpregunta478h2,lblpregunta478h2_ind, lblpregunta478h3,lblpregunta478h3_ind,lblpregunta478h4, 
	lblpregunta478h4_ind,lblpregunta478h5,lblpregunta478h5_ind, lblpregunta478h6,lblpregunta478h6_ind, lblpregunta478h7_ind, lblpregunta478h7,lblpregunta478h8, lblpregunta478h8_a,lblpregunta478h8_b, lblpregunta478h9, lblpregunta478h10,lblpregunta478h11,lblpregunta478h12;
	public DISCAPACIDAD detalle;
	public GridComponent2 gridpregunta478H8,gridDiscapacidad;
	LinearLayout q0;
	LinearLayout q1;
	LinearLayout q2;
	LinearLayout q3;
	LinearLayout q4;
	LinearLayout q5;
	LinearLayout q6;
	LinearLayout q7;
	LinearLayout q8;
	LinearLayout q9;
	LinearLayout q10;
	LinearLayout q11;
	LinearLayout q12;
	LinearLayout q13;
	LinearLayout q14;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);		
		enlazarCajas();
		listening();
		return rootView;
	}
	public C2SECCION_04BFragment_022DIT_4() {
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478H1","QI478H2","QI478H3","QI478H4","QI478H5","QI478H6","QI478H7","QI478H8_A","QI478H8_B","QI478H9","QI478H10","QI478H11","QI478H12","QI478A","ID","HOGAR_ID","PERSONA_ID","QI212_NOM","NRO_ORDEN_NINIO")};
		seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI478H1","QI478H2","QI478H3","QI478H4","QI478H5","QI478H6","QI478H7","QI478H8_A","QI478H8_B","QI478H9","QI478H10","QI478H11","QI478H12","QI478A")}; 
	}            
	
	public C2SECCION_04BFragment_022DIT_4 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	

	@Override
	protected void buildFields(){
		lbltitulo = new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_dit_tramo4de24_36h).textSize(21).centrar();
		lbldiscapacidad = new LabelComponent(getActivity()).size(altoComponente+10,715).textSize(16).text(R.string.c2seccion_04b_2qi478);
//		lblIndicacion = new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04b_2qi478_ind);
		Spanned lblIndicaciontxt = Html.fromHtml("A continuaci�n le voy a formular algunas preguntas para conocer aspectos importantes del desarrollo de su hija(o). <br><br>Le pido que cuando usted responda las preguntas, piense en las cosas que <b>generalmente</b> hace su hija(o); adem�s, considere las cosas que hizo <b>en estas �ltimas dos semanas</b>.");
		lblIndicacion = new LabelComponent(this.getActivity()).textSize(18).size(MATCH_PARENT, MATCH_PARENT);
		lblIndicacion.setText(lblIndicaciontxt);
		
		
		chbQI478A = new CheckBoxField(this.getActivity(),"1:0").size(WRAP_CONTENT, 40).callback("onQI478AChangeValue"); 
		gridDiscapacidad = new GridComponent2(getActivity(), App.ESTILO,2,0);
		gridDiscapacidad.addComponent(lbldiscapacidad);
		gridDiscapacidad.addComponent(chbQI478A);
		
		lblpregunta478h1= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h2= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h3= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h4= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h5= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h6= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h7= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h8= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h9= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h10= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h11= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h12= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		
		lblpregunta478h1_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h2_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h3_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h4_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h5_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h6_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text("");
		lblpregunta478h7_ind= new LabelComponent(getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text("");
		
		rgQI478H1=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).callback("QI478H1OnChangeValue"); 
		rgQI478H2=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H3=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H4=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H5=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H6=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H7=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H9=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H10=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478h10_1,R.string.c2seccion_04b_2qi478h10_2,R.string.c2seccion_04b_2qi478h10_3,R.string.c2seccion_04b_2qi478h10_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478H11=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL);
		rgQI478H12=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478h12_1,R.string.c2seccion_04b_2qi478h12_2,R.string.c2seccion_04b_2qi478h12_3,R.string.c2seccion_04b_2qi478h12_4,R.string.c2seccion_04b_2qi478h12_8).size(WRAP_CONTENT,WRAP_CONTENT).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		
		lblpregunta478h8_a= new LabelComponent(getActivity()).size(altoComponente+200, 600).textSize(18).text("");
		lblpregunta478h8_b= new LabelComponent(getActivity()).size(altoComponente+300, 600).textSize(18).text("");
		rgQI478H8_A=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+200,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
		rgQI478H8_B=new RadioGroupOtherField(this.getActivity(),R.string.c2seccion_04b_2qi478_1,R.string.c2seccion_04b_2qi478_2,R.string.c2seccion_04b_2qi478_8).size(altoComponente+240,160).orientation(RadioGroupOtherField.ORIENTATION.VERTICAL);
			
		
		gridpregunta478H8=new GridComponent2(this.getActivity(),App.ESTILO,2,1);
		gridpregunta478H8.addComponent(lblpregunta478h8_a);
		gridpregunta478H8.addComponent(rgQI478H8_A);
		gridpregunta478H8.addComponent(lblpregunta478h8_b);
		gridpregunta478H8.addComponent(rgQI478H8_B);

	} 
	
	@Override
	protected View createUI() {
		buildFields();
		q0  = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lbltitulo);
		q1  = createQuestionSection(gridDiscapacidad.component());
		q2  = createQuestionSection(lblIndicacion);
		q3  = createQuestionSection(0,lblpregunta478h1,lblpregunta478h1_ind,rgQI478H1); 
		q4  = createQuestionSection(0,lblpregunta478h2,lblpregunta478h2_ind,rgQI478H2);
		q5  = createQuestionSection(0,lblpregunta478h3,lblpregunta478h3_ind,rgQI478H3);
		q6  = createQuestionSection(0,lblpregunta478h4,lblpregunta478h4_ind,rgQI478H4);
		q7  = createQuestionSection(0,lblpregunta478h5,lblpregunta478h5_ind,rgQI478H5);
		q8  = createQuestionSection(0,lblpregunta478h6,lblpregunta478h6_ind,rgQI478H6);
		q9  = createQuestionSection(0,lblpregunta478h7,lblpregunta478h7_ind,rgQI478H7);
		q10 = createQuestionSection(0,lblpregunta478h8,gridpregunta478H8.component());
		q11 = createQuestionSection(0,lblpregunta478h9,rgQI478H9);
		q12 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478h10,rgQI478H10);
		q13 = createQuestionSection(0,lblpregunta478h11,rgQI478H11);
		q14 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta478h12,rgQI478H12);

		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q1);
		form.addView(q2);
		form.addView(q3);
		form.addView(q4);
		form.addView(q5);
		form.addView(q6); 
		form.addView(q7);
		form.addView(q8); 
		form.addView(q9);
		form.addView(q10);             
		form.addView(q11); 
		form.addView(q12); 
		form.addView(q13); 
		form.addView(q14); 

		return contenedor;          
	}
	
	@Override
	public boolean grabar(){
		
		uiToEntity(ninio);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			cambiarVariablesParalaBasedeDatos();
			flag = getCuestionarioService().saveOrUpdate(ninio, seccionesGrabado);
		  } catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		} 
		VerificarPersonaDiscapacitado();
		return flag;
	}
	
	public boolean validar(){
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);
		if(Util.esDiferente(ninio.qi478a, 1)){
			if (ninio.qi478h1  == null) {
				error = true;
				view = rgQI478H1;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H1");
				return false;
			}
			if (ninio.qi478h2  == null) {
				error = true;
				view = rgQI478H2;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H2");
				return false;
			}
			if (ninio.qi478h3  == null) {
				error = true;
				view = rgQI478H3;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H3");
				return false;
			}
			if (ninio.qi478h4  == null) {
				error = true;
				view = rgQI478H4;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H4");
				return false;
			}
			if (ninio.qi478h5  == null) {
				error = true;
				view = rgQI478H5;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H5");
				return false;
			}
			if (ninio.qi478h6  == null) {
				error = true;
				view = rgQI478H6;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H6");
				return false;
			}
			if (ninio.qi478h7  == null) {
				error = true;
				view = rgQI478H7;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H7");
				return false;
			}
			if (ninio.qi478h8_a  == null) {
				error = true;
				view = rgQI478H8_A;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H8_A");
				return false;
			}
			if (ninio.qi478h8_b  == null) {
				error = true;
				view = rgQI478H8_B;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H8_B");
				return false;
			}
			if (ninio.qi478h9  == null) {
				error = true;
				view = rgQI478H9;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H9");
				return false;
			}		
			if (ninio.qi478h10  == null) {
				error = true;
				view = rgQI478H10;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H10");
				return false;
			}
			if (ninio.qi478h11  == null) {
				error = true;
				view = rgQI478H11;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H11");
				return false;
			}
			if (ninio.qi478h12  == null) {
				error = true;
				view = rgQI478H12;
				mensaje = preguntaVacia.replace("$", "La pregunta P.478H11");
				return false;
			}
		}
	    return true;
	}
	
	@Override
	public void cargarDatos() {   

		if(App.getInstance().getSeccion04DIT_02()!=null) {
						
		ninio = getCuestionarioService().getCISECCION_04B_dit2(App.getInstance().getSeccion04DIT_02().id,App.getInstance().getSeccion04DIT_02().hogar_id,App.getInstance().getSeccion04DIT_02().persona_id,App.getInstance().getSeccion04DIT_02().nro_orden_ninio,seccionesCargado);
		
		if (ninio == null) {
			ninio = new CISECCION_04DIT_02();
			ninio.id = App.getInstance().getSeccion04DIT_02().id;
			ninio.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			ninio.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			ninio.nro_orden_ninio= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
		}
		cambiarVariablesParaMostraraUsuario();
		entityToUI(ninio);
		inicio();
		}
    }
	public void cambiarVariablesParalaBasedeDatos(){
		ninio.qi478h1=ninio.qi478h1!=null?ninio.setConvertconTresVariables(ninio.qi478h1):null;
		ninio.qi478h2=ninio.qi478h2!=null?ninio.setConvertconTresVariables(ninio.qi478h2):null;
		ninio.qi478h3=ninio.qi478h3!=null?ninio.setConvertconTresVariables(ninio.qi478h3):null;
		ninio.qi478h4=ninio.qi478h4!=null?ninio.setConvertconTresVariables(ninio.qi478h4):null;
		ninio.qi478h5=ninio.qi478h5!=null?ninio.setConvertconTresVariables(ninio.qi478h5):null;
		ninio.qi478h6=ninio.qi478h6!=null?ninio.setConvertconTresVariables(ninio.qi478h6):null;
		ninio.qi478h7=ninio.qi478h7!=null?ninio.setConvertconTresVariables(ninio.qi478h7):null;
		ninio.qi478h8_a=ninio.qi478h8_a!=null?ninio.setConvertconTresVariables(ninio.qi478h8_a):null;
		ninio.qi478h8_b=ninio.qi478h8_b!=null?ninio.setConvertconTresVariables(ninio.qi478h8_b):null;
		ninio.qi478h9=ninio.qi478h9!=null?ninio.setConvertconTresVariables(ninio.qi478h9):null;		
		ninio.qi478h10=ninio.qi478h10!=null?ninio.setConvertconCuatroVariables(ninio.qi478h10):null;
		ninio.qi478h11=ninio.qi478h11!=null?ninio.setConvertconTresVariables(ninio.qi478h11):null;
		ninio.qi478h12=ninio.qi478h12!=null?ninio.setConvertconCincoVariables(ninio.qi478h12):null;
	}
	
	private void cambiarVariablesParaMostraraUsuario(){
		ninio.qi478h1 =ninio.qi478h1 !=null?ninio.getConvertconTresVariables(ninio.qi478h1):null;
		ninio.qi478h2 =ninio.qi478h2 !=null?ninio.getConvertconTresVariables(ninio.qi478h2):null;
		ninio.qi478h3 =ninio.qi478h3 !=null?ninio.getConvertconTresVariables(ninio.qi478h3):null;
		ninio.qi478h4 =ninio.qi478h4 !=null?ninio.getConvertconTresVariables(ninio.qi478h4):null;
		ninio.qi478h5 =ninio.qi478h5 !=null?ninio.getConvertconTresVariables(ninio.qi478h5):null;
		ninio.qi478h6 =ninio.qi478h6 !=null?ninio.getConvertconTresVariables(ninio.qi478h6):null;
		ninio.qi478h7 =ninio.qi478h7 !=null?ninio.getConvertconTresVariables(ninio.qi478h7):null;
		ninio.qi478h8_a=ninio.qi478h8_a!=null?ninio.getConvertconTresVariables(ninio.qi478h8_a):null;
		ninio.qi478h8_b=ninio.qi478h8_b!=null?ninio.getConvertconTresVariables(ninio.qi478h8_b):null;
		ninio.qi478h9 =ninio.qi478h9 !=null?ninio.getConvertconTresVariables(ninio.qi478h9):null;		
		ninio.qi478h10=ninio.qi478h10!=null?ninio.getConvertconCuatroVariables(ninio.qi478h10):null;
		ninio.qi478h11 =ninio.qi478h11 !=null?ninio.getConvertconTresVariables(ninio.qi478h11):null;
		ninio.qi478h12=ninio.qi478h12!=null?ninio.getConvertconCincoVariables(ninio.qi478h12):null;
	}
	
	public void inicio(){
		RenombrarEtiquetas();
		evaluarPregunta();
		ValidarsiesSupervisora();
		chbQI478A.requestFocus();
		QI478H1OnChangeValue();
	}
	public void QI478H1OnChangeValue(){
		VerificarPersonaDiscapacitado();
	}
	public void  VerificarPersonaDiscapacitado(){
		DISCAPACIDAD personadis= getCuestionarioService().getSeccion02IndividualParaDiscapacidad(ninio.id,ninio.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), ninio.persona_id,ninio.nro_orden_ninio, seccionesCargado);
		boolean existediscapacidad=false;
		if(!Util.esDiferente(personadis.qd333_1, 1)|| !Util.esDiferente(personadis.qd333_2, 1) || !Util.esDiferente(personadis.qd333_3, 1) || !Util.esDiferente(personadis.qd333_4, 1)|| !Util.esDiferente(personadis.qd333_5, 1)|| !Util.esDiferente(personadis.qd333_6, 1)){
			existediscapacidad=true;
		}
		if(existediscapacidad){
			MyUtil.MensajeGeneral(getActivity(), " Verificar Ni�o se registr� como Discapacitado! ");
		}
	}
	public void evaluarPregunta() {		
		if(App.getInstance().getSeccion04DIT_02()!=null && App.getInstance().getSeccion04DIT_02().qi478 >=24 && App.getInstance().getSeccion04DIT_02().qi478 <=36) {
			Util.lockView(getActivity(), false,chbQI478A, rgQI478H1,rgQI478H2,rgQI478H3,rgQI478H4,rgQI478H5,rgQI478H6,rgQI478H7,rgQI478H8_A, rgQI478H8_B,rgQI478H9,rgQI478H10, rgQI478H11,rgQI478H12);
			q0.setVisibility(View.VISIBLE);
			q1.setVisibility(View.VISIBLE);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			q14.setVisibility(View.VISIBLE);
			onQI478AChangeValue();
		}
		else {
			Util.cleanAndLockView(getActivity(),chbQI478A, rgQI478H1,rgQI478H2,rgQI478H3,rgQI478H4,rgQI478H5,rgQI478H6,rgQI478H7,rgQI478H8_A, rgQI478H8_B,rgQI478H9,rgQI478H10, rgQI478H11,rgQI478H12);
			q0.setVisibility(View.GONE);
			q1.setVisibility(View.GONE);
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q14.setVisibility(View.GONE);
		}
	}
	
	public void onQI478AChangeValue(){
		if(chbQI478A.isChecked()){
			Util.cleanAndLockView(getActivity(), rgQI478H1,rgQI478H2,rgQI478H3,rgQI478H4,rgQI478H5,rgQI478H6,rgQI478H7,rgQI478H8_A, rgQI478H8_B,rgQI478H9,rgQI478H10, rgQI478H11,rgQI478H12);
			MyUtil.LiberarMemoria();
			q2.setVisibility(View.GONE);
			q3.setVisibility(View.GONE);
			q4.setVisibility(View.GONE);
			q5.setVisibility(View.GONE);
			q6.setVisibility(View.GONE);
			q7.setVisibility(View.GONE);
			q8.setVisibility(View.GONE);
			q9.setVisibility(View.GONE);
			q10.setVisibility(View.GONE);
			q11.setVisibility(View.GONE);
			q12.setVisibility(View.GONE);
			q13.setVisibility(View.GONE);
			q14.setVisibility(View.GONE);
			detalle= new DISCAPACIDAD();
			detalle.id= App.getInstance().getSeccion04DIT_02().id;
			detalle.hogar_id =App.getInstance().getSeccion04DIT_02().hogar_id;
			detalle.persona_id = App.getInstance().getSeccion04DIT_02().persona_id;
			detalle.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			abrirDetalle(detalle);
		}
		else{
			Util.lockView(getActivity(), false, rgQI478H1,rgQI478H2,rgQI478H3,rgQI478H4,rgQI478H5,rgQI478H6,rgQI478H7,rgQI478H8_A, rgQI478H8_B,rgQI478H9,rgQI478H10, rgQI478H11,rgQI478H12);
			q2.setVisibility(View.VISIBLE);
			q3.setVisibility(View.VISIBLE);
			q4.setVisibility(View.VISIBLE);
			q5.setVisibility(View.VISIBLE);
			q6.setVisibility(View.VISIBLE);
			q7.setVisibility(View.VISIBLE);
			q8.setVisibility(View.VISIBLE);
			q9.setVisibility(View.VISIBLE);
			q10.setVisibility(View.VISIBLE);
			q11.setVisibility(View.VISIBLE);
			q12.setVisibility(View.VISIBLE);
			q13.setVisibility(View.VISIBLE);
			q14.setVisibility(View.VISIBLE);
		}
	}
	public void abrirDetalle(DISCAPACIDAD modelo) {
		FragmentManager fm = C2SECCION_04BFragment_022DIT_4.this.getFragmentManager();
		C2SECCION_04BFragment_018DIT_4_Dialog aperturaDialog = C2SECCION_04BFragment_018DIT_4_Dialog.newInstance(this, modelo);
		aperturaDialog.setAncho(LinearLayout.LayoutParams.MATCH_PARENT);
		aperturaDialog.show(fm, "aperturaDialog");
	}
	public void RenombrarEtiquetas(){
		
		String replace ="(NOMBRE)";
		String nombre = ninio.qi212_nom;
		
		lbldiscapacidad.setText(lbldiscapacidad.getText().toString().replace(replace, nombre));
		
		lblpregunta478h1.setText("(NOMBRE)");		
		lblpregunta478h1.setText(lblpregunta478h1.getText().toString().replace(replace, nombre));	
		lblpregunta478h1.setText(Html.fromHtml("478H1. Cuando "+lblpregunta478h1.getText()+" <b>habla</b> �dice frases con un sujeto y una acci�n como �beb� llora�?"));
		lblpregunta478h1_ind.setText(Html.fromHtml("SI LA MADRE RESPONDE S�, PREGUNTE<br>Por ejemplo, �qu� frases dice?<br><br>MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> FRASE QUE TIENE POR LO MENOS <b>UN SUJETO Y UN VERBO O ACCI�N</b>"));
		
		lblpregunta478h2.setText("(NOMBRE)");
		lblpregunta478h2.setText(lblpregunta478h2.getText().toString().replace(replace, nombre));
		lblpregunta478h2.setText(Html.fromHtml("478H2. Cuando "+lblpregunta478h2.getText()+" <b>habla</b> �dice oraciones como �vamos <b>a la</b> casa�, �d�nde est� <b>la</b> abuela�, �<b>mi</b> mam� es linda�?"));
		lblpregunta478h2_ind.setText(Html.fromHtml("SI LA MADRE RESPONDE S�, PREGUNTE:<br>Por ejemplo, �qu� oraciones dice?<br><br>MARQUE S� CUANDO LA MADRE REPORTE <b>UNA</b> ORACI�N QUE <b>INCLUYE ART�CULOS</b> (POR EJEMPLO EL, LA , LOS) <b>Y/O PREPOSICIONES</b> (POR EJEMPLO MI, PARA, EN)."));
		
		lblpregunta478h3.setText("(NOMBRE)");
		lblpregunta478h3.setText(lblpregunta478h3.getText().toString().replace(replace, nombre));
		lblpregunta478h3.setText(Html.fromHtml("478H3. "+lblpregunta478h3.getText()+" �<b>entiende</b> palabras que indican la posici�n de las cosas como �dentro� y �fuera� <b>o</b> �encima� y �debajo�?"));
		lblpregunta478h3_ind.setText(Html.fromHtml("MARQUE S� CUANDO LA MADRE REPORTE QUE LA/EL NI�A(O) ENTIENDE UNO DE LOS PARES DE PALABRAS."));
		
		lblpregunta478h4.setText("(NOMBRE)");
		lblpregunta478h4.setText(lblpregunta478h4.getText().toString().replace(replace, nombre));
		lblpregunta478h4.setText(Html.fromHtml("478H4. "+lblpregunta478h4.getText()+" <b>generalmente</b> �<b>�participa�</b> en las conversaciones con adultos?"));
		lblpregunta478h4_ind.setText(Html.fromHtml("SIEMPRE LEA:<br>Por ejemplo, su hija(o) pregunta o responde o se expresa verbalmente <b>como parte</b> de la conversaci�n de los adultos."));
				
		lblpregunta478h5.setText("(NOMBRE)");
		lblpregunta478h5.setText(lblpregunta478h5.getText().toString().replace(replace, nombre));
		lblpregunta478h5.setText(Html.fromHtml("478H5. Cuando "+lblpregunta478h5.getText()+" hace un garabato o dibujo �<b>dice lo que dibuj�</b>?"));
		lblpregunta478h5_ind.setText(Html.fromHtml("SIEMPRE LEA:<br>Por ejemplo, le dice que ha dibujado a una mam�, a una ni�a o un carro aunque su dibujo no se parezca a ninguno de estos objetos."));
					
		lblpregunta478h6.setText("(NOMBRE)");
		lblpregunta478h6.setText(lblpregunta478h6.getText().toString().replace(replace, nombre));
		lblpregunta478h6.setText(Html.fromHtml("478H6. "+lblpregunta478h6.getText()+" �imita lo que hace una <b>persona o personaje</b> cuando <b>esta(este) no se encuentra presente</b>?"));
		lblpregunta478h6_ind.setText(Html.fromHtml("DE SER NECESARIO LEA:<br>Por ejemplo, su hija(o) imita lo que hace o dice su t�a(o) <b>cuando ella(�l) no est�</b> o imita un personaje como el hombre ara�a <b>cuando no lo ve</b>."));
		
		lblpregunta478h7.setText("(NOMBRE)");
		lblpregunta478h7.setText(lblpregunta478h7.getText().toString().replace(replace, nombre));
		lblpregunta478h7.setText(Html.fromHtml("478H7. "+lblpregunta478h7.getText()+" �<b>le habla</b> a sus mu�ecos o  juguetes?"));
		lblpregunta478h7_ind.setText(Html.fromHtml("REFIERE A SI LA (EL) NI�A (O) HABLA CON SUS JUGUETES <b>CUANDO JUEGA</b>."));

		lblpregunta478h8.setText("(NOMBRE)");
		lblpregunta478h8.setText(lblpregunta478h8.getText().toString().replace(replace, nombre));
		lblpregunta478h8.setText(Html.fromHtml("478H8. <b>En casa</b> "+lblpregunta478h8.getText()+" tiene:"));
		lblpregunta478h8_a.setText(Html.fromHtml("A. �Materiales <b>especialmente hechos para jugar</b> como una pelota o una mu�eca?<br><br>DE SER NECESARIO, LEA:<br>Considere si en casa tiene materiales que solo pueden ser usados de una forma espec�fica para jugar, como cubos para encajar, pelotas, rompecabezas, mu�ecos, etc."));
		lblpregunta478h8_b.setText(Html.fromHtml("B. �Y tiene <b>otros materiales</b> con los que puede jugar como bloques, palitos, botellas, l�pices o alg�n tipo de papel?<br><br>SIEMPRE LEA:<br>Considere si en casa tiene materiales que pueden ser utilizados de diferentes formas al jugar como l�pices, papeles, bloques, plastilinas, objetos de la casa (ollas, botellas de pl�stico), objetos de origen natural (conchas, palitos, plantas)."));
		
		lblpregunta478h9.setText("(NOMBRE)");
		lblpregunta478h9.setText(lblpregunta478h9.getText().toString().replace(replace, nombre));
		lblpregunta478h9.setText(Html.fromHtml("478H9. "+lblpregunta478h9.getText()+" �llora, grita o hace pataletas <b>la mayor parte del tiempo</b>? "));
		
		lblpregunta478h10.setText("(NOMBRE)");
		lblpregunta478h10.setText(lblpregunta478h10.getText().toString().replace(replace, nombre));
		lblpregunta478h10.setText(Html.fromHtml("478H10. Cuando "+lblpregunta478h10.getText()+" <b>quiere algo y usted le dice que espere, generalmente</b> �espera �tranquila(o)�? "));
		
		lblpregunta478h11.setText("(NOMBRE)");
		lblpregunta478h11.setText(lblpregunta478h11.getText().toString().replace(replace, nombre));
		lblpregunta478h11.setText(Html.fromHtml("478H11. Cuando "+lblpregunta478h11.getText()+" <b>quiere algo y usted le dice que NO, generalmente</b> �se hace da�o, agrede a los dem�s o da�a las cosas?"));
		
		lblpregunta478h12.setText("(NOMBRE)");
		lblpregunta478h12.setText(lblpregunta478h12.getText().toString().replace(replace, nombre));
		lblpregunta478h12.setText(Html.fromHtml("478H12. En los �ltimos 15 dias, �cu�ntas veces usted le ha dado un <b>palmazo, le ha jalado de los cabellos o la oreja o le ha golpeado con un objeto en cualquier parte de su cuerpo</b> a "+lblpregunta478h12.getText()+", de 1 a 3 veces, de 4 a 6 veces o m�s de 6 veces?"));
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			chbQI478A.readOnly();
			rgQI478H1.readOnly();
			rgQI478H2.readOnly();
			rgQI478H3.readOnly();
			rgQI478H4.readOnly();
			rgQI478H5.readOnly();
			rgQI478H6.readOnly();
			rgQI478H7.readOnly();
			rgQI478H8_A.readOnly();
			rgQI478H8_B.readOnly();
			rgQI478H9.readOnly();
			rgQI478H10.readOnly();
			rgQI478H11.readOnly();
			rgQI478H12.readOnly();
		}
	}
	
	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return null;
	}
}
