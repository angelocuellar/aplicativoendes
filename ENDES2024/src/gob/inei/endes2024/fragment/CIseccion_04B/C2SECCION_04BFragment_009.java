package gob.inei.endes2024.fragment.CIseccion_04B; 
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.IntegerField;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.TextAreaField;
import gob.inei.dnce.components.TextField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_009 extends FragmentForm { 
	

	
	@FieldAnnotation(orderIndex=1) 
	public CheckBoxField chbQI1;
	@FieldAnnotation(orderIndex=2)
	public CheckBoxField chbQI2;
	@FieldAnnotation(orderIndex=3)
	public CheckBoxField chbQI3;
	@FieldAnnotation(orderIndex=4)
	public CheckBoxField chbQI4;
	@FieldAnnotation(orderIndex=5)
	public CheckBoxField chbQI5;
	@FieldAnnotation(orderIndex=6)
	public CheckBoxField chbQI6;
	@FieldAnnotation(orderIndex=7)
	public CheckBoxField chbQI7;
	@FieldAnnotation(orderIndex=8)
	public CheckBoxField chbQI8;
	@FieldAnnotation(orderIndex=9) 
	public TextAreaField txtQI465EC_O; 
	
	@FieldAnnotation(orderIndex=10) 
	public CheckBoxField chbQI465EC1_A;
	@FieldAnnotation(orderIndex=11) 
	public CheckBoxField chbQI465EC1_B;
	@FieldAnnotation(orderIndex=12) 
	public CheckBoxField chbQI465EC1_C;
	@FieldAnnotation(orderIndex=13) 
	public CheckBoxField chbQI465EC1_D;
	@FieldAnnotation(orderIndex=14) 
	public CheckBoxField chbQI465EC1_E;
	@FieldAnnotation(orderIndex=15) 
	public CheckBoxField chbQI465EC1_F;
	@FieldAnnotation(orderIndex=16) 
	public CheckBoxField chbQI465EC1_G;
	@FieldAnnotation(orderIndex=17) 
	public CheckBoxField chbQI465EC1_H;	 
	@FieldAnnotation(orderIndex=18) 	
	public CheckBoxField chbQI465EC1_I;
	@FieldAnnotation(orderIndex=19) 
	public TextField txtQI465EC1_I_O;
	@FieldAnnotation(orderIndex=20) 
	public CheckBoxField chbQI465EC1_J;
	@FieldAnnotation(orderIndex=21) 
	public CheckBoxField chbQI465EC1_K;
	@FieldAnnotation(orderIndex=22) 
	public CheckBoxField chbQI465EC1_L;
	@FieldAnnotation(orderIndex=23) 
	public CheckBoxField chbQI465EC1_M;
	@FieldAnnotation(orderIndex=24) 
	public CheckBoxField chbQI465EC1_N;
	@FieldAnnotation(orderIndex=25) 
	public CheckBoxField chbQI465EC1_O;
	@FieldAnnotation(orderIndex=26) 
	public CheckBoxField chbQI465EC1_X;
	@FieldAnnotation(orderIndex=27) 
	public TextField txtQI465EC1_X_O;
	@FieldAnnotation(orderIndex=28) 
	public CheckBoxField chbQI465EC1_Z;
	
	
	
	
	public IntegerField txtQID1,txtQIM1,txtQIA1,txtQID2,txtQIM2,txtQIA2,txtQID3,txtQIM3,txtQIA3,txtQID4,txtQIM4,txtQIA4,
	   txtQID5,txtQIM5,txtQIA5,txtQID6,txtQIM6,txtQIA6;
		
	CISECCION_04B c2seccion_04b; 
	private CuestionarioService cuestionarioService; 
	private LabelComponent lblTitulo,lblpregunta465ec,lblP465ec_1,lblP465ec_2,lblP465ec_3,
	lblP465ec_4,lblP465ec_7,lblP465ec_8,lblP4654ec_o,lblpregunta465ec1,lblpregunta465ec1_x1,lblpregunta465ec1_x2,lblpregunta465ec1_x3,lblpregunta465ec1_x4; 
	
	LinearLayout q0,q1,q2;  
	public static SeccionCapitulo[] seccionesGrabado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465EC_O", "QI465EC1_A", "QI465EC1_B", "QI465EC1_C", "QI465EC1_D", "QI465EC1_E", "QI465EC1_F", "QI465EC1_G", "QI465EC1_H", "QI465EC1_I", "QI465EC1_I_O", "QI465EC1_J", "QI465EC1_K", "QI465EC1_L", "QI465EC1_M", "QI465EC1_N", "QI465EC1_O", "QI465EC1_X", "QI465EC1_X_O", "QI465EC1_Z")};
	SeccionCapitulo[] seccionesCargado,seccionesCargado2,seccionesGrabado2; 
	String nombre_persona;
	public TextField txtCabecera;
	public GridComponent2 griP465EC;
	
	public C2SECCION_04BFragment_009() {} 
	public C2SECCION_04BFragment_009 parent(MasterActivity parent) { 
		this.parent = parent; 
		return this; 
	} 
	
	@Override 
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
	} 
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		rootView = createUI(); 
		initObjectsWithoutXML(this, rootView); 
		
		enlazarCajas(); 
		listening(); 
		seccionesCargado = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI465EC_O", "QI465EC1_A", "QI465EC1_B", "QI465EC1_C", "QI465EC1_D", "QI465EC1_E", "QI465EC1_F", "QI465EC1_G", "QI465EC1_H", "QI465EC1_I", "QI465EC1_I_O", "QI465EC1_J", "QI465EC1_K", "QI465EC1_L", "QI465EC1_M", "QI465EC1_N", "QI465EC1_O", "QI465EC1_X", "QI465EC1_X_O", "QI465EC1_Z", "QI454","QI465EA","QI465EB","QI465EC","QI465ED", "ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};  
		seccionesCargado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"INDICE","DIA","MES","ANIO","PREGUNTA","ID","HOGAR_ID","PERSONA_ID","NINIO_ID")};
		seccionesGrabado2 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"INDICE","DIA","MES","ANIO","PREGUNTA")};
		
		return rootView; 
	} 
	
	@Override 
	protected void buildFields() { 
	  	txtCabecera = new TextField(getActivity()).size(0, 0).alinearDerecha();
	    lblTitulo=new LabelComponent(this.getActivity(),App.ESTILO).size(MATCH_PARENT, MATCH_PARENT).text(R.string.c2seccion_04b).textSize(21).centrar();
	   	   
	    lblpregunta465ec = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec);
		lblP465ec_1 = new LabelComponent(getActivity()).textSize(18).size(50, 150).centrar();
		lblP465ec_2 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi465ec_c2).centrar().negrita();
		lblP465ec_3 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi465ec_c3).centrar().negrita();
		lblP465ec_4 = new LabelComponent(getActivity()).textSize(16).size(50, 120).text(R.string.c2seccion_04bqi465ec_c4).centrar().negrita();
		lblP465ec_7 = new LabelComponent(getActivity()).textSize(16).size(50, 360).text(R.string.c2seccion_04bqi465ec_chb7);
		lblP465ec_8 = new LabelComponent(getActivity()).textSize(16).size(50, 360).text(R.string.c2seccion_04bqi465ec_chb8);
		lblP4654ec_o = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec_o).negrita();
		
		lblpregunta465ec1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(19).text(R.string.c2seccion_04bqi465ec1);
		lblpregunta465ec1_x1 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec1_x1).negrita();
		lblpregunta465ec1_x2 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec1_x2).negrita();
		lblpregunta465ec1_x3 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec1_x3).negrita();
		lblpregunta465ec1_x4 = new LabelComponent(this.getActivity()).size(MATCH_PARENT, MATCH_PARENT).textSize(16).text(R.string.c2seccion_04bqi465ec1_x4).negrita();
		
		txtQI465EC_O = new TextAreaField(getActivity()).maxLength(500).size(100, 650).alfanumerico();		
	    chbQI1=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o1, "1:0").size(altoComponente, 150).callback("onqrgQI465ec1ChangeValue");
		chbQI2=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o2, "2:0").size(altoComponente, 150).callback("onqrgQI465ec2ChangeValue");
		chbQI3=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o3, "3:0").size(altoComponente, 150).callback("onqrgQI465ec3ChangeValue");
		chbQI4=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o4, "4:0").size(altoComponente, 150).callback("onqrgQI465ec4ChangeValue");
		chbQI5=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o5, "5:0").size(altoComponente, 150).callback("onqrgQI465ec5ChangeValue");
		chbQI6=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o6, "6:0").size(altoComponente, 150).callback("onqrgQI465ec6ChangeValue");
		chbQI7=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o7, "7:0").size(altoComponente, 150).callback("onqrgQI465ec7ChangeValue");
		chbQI8=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec_o8, "8:0").size(altoComponente, 150).callback("onqrgQI465ec8ChangeValue");
		
		chbQI465EC1_A=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_a, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_aChangeValue");
		chbQI465EC1_B=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_b, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_bChangeValue");
		chbQI465EC1_C=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_c, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_cChangeValue");
		chbQI465EC1_D=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_d, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_dChangeValue");
		chbQI465EC1_E=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_e, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_eChangeValue");
		chbQI465EC1_F=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_f, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_fChangeValue");
		chbQI465EC1_G=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_g, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_gChangeValue");
		chbQI465EC1_H=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_h, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_hChangeValue");
		chbQI465EC1_I=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_i, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_iChangeValue");
		chbQI465EC1_J=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_j, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_jChangeValue");
		chbQI465EC1_K=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_k, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_kChangeValue");
		chbQI465EC1_L=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_l, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_lChangeValue");
		chbQI465EC1_M=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_m, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_mChangeValue");
		chbQI465EC1_N=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_n, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_nChangeValue");
		chbQI465EC1_O=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_o, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_oChangeValue");
		chbQI465EC1_X=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_x, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_xChangeValue");
		chbQI465EC1_Z=new CheckBoxField(this.getActivity(), R.string.c2seccion_04bqi465ec1_z, "1:0").size(WRAP_CONTENT, WRAP_CONTENT).callback("onqrgQI465ec1_zChangeValue");
		
		txtQI465EC1_I_O=new TextField(this.getActivity()).size(altoComponente, 600).maxLength(900).alfanumerico();
		txtQI465EC1_X_O=new TextField(this.getActivity()).size(altoComponente, 600).maxLength(900).alfanumerico();
		
		txtQID1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID1.getText()!=null)
					if(!txtQID1.getText().toString().isEmpty())
						txtQIM1.requestFocus();
			}
		});
		txtQID1.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM1,txtQIA1);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM1,txtQIA1);
				}
			}
		});
		
		txtQIM1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM1.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(txtQIM1.getText()!=null)
							if(!txtQIM1.getText().toString().isEmpty())
								txtQIA1.requestFocus();
					}
				});
		
		txtQIA1=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA1.getText()!=null)
					if(!txtQIA1.getText().toString().isEmpty())
						chbQI2.requestFocus();
			}
		});
		txtQID2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID2.getText()!=null)
					if(!txtQID2.getText().toString().isEmpty())
						txtQIM2.requestFocus();
			}
		});
		txtQID2.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM2,txtQIA2);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM2,txtQIA2);
				}
			}
		});
		txtQIM2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM2.getText()!=null)
					if(!txtQIM2.getText().toString().isEmpty())
						txtQIA2.requestFocus();
			}
		});
		txtQIA2=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA2.getText()!=null)
					if(!txtQIA2.getText().toString().isEmpty())
						chbQI3.requestFocus();
			}
		});
		txtQID3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID3.getText()!=null)
					if(!txtQID3.getText().toString().isEmpty())
						txtQIM3.requestFocus();
			}
		});
		txtQID3.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM3,txtQIA3);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM3,txtQIA3);
				}
			}
		});
		txtQIM3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM3.getText()!=null)
					if(!txtQIM3.getText().toString().isEmpty())
						txtQIA3.requestFocus();
			}
		});
		txtQIA3=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA3.getText()!=null)
					if(!txtQIA3.getText().toString().isEmpty())
						chbQI4.requestFocus();
			}
		});
		txtQID4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID4.getText()!=null)
					if(!txtQID4.getText().toString().isEmpty())
						txtQIM4.requestFocus();
			}
		});
		txtQID4.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM4,txtQIA4);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM4,txtQIA4);
				}
			}
		});
		txtQIM4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM4.getText()!=null)
					if(!txtQIM4.getText().toString().isEmpty())
						txtQIA4.requestFocus();
			}
		});
		txtQIA4=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA4.getText()!=null)
					if(!txtQIA4.getText().toString().isEmpty())
						chbQI5.requestFocus();
			}
		});
		txtQID5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID5.getText()!=null)
					if(!txtQID5.getText().toString().isEmpty())
						txtQIM5.requestFocus();
			}
		});
		txtQID5.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM5,txtQIA5);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM5,txtQIA5);
				}
			}
		});
		txtQIM5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM5.getText()!=null)
					if(!txtQIM5.getText().toString().isEmpty())
						txtQIA5.requestFocus();
			}
		});
		txtQIA5=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA5.getText()!=null)
					if(!txtQIA5.getText().toString().isEmpty())
						chbQI6.requestFocus();
			}
		});
		txtQID6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQID6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQID6.getText()!=null)
					if(!txtQID6.getText().toString().isEmpty())
						txtQIM6.requestFocus();
			}
		});
		txtQID6.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()>0){
			    	Integer inDia = Integer.valueOf(s.toString());
			    	if (inDia==44){
			    		Util.cleanAndLockView(getActivity(),txtQIM6,txtQIA6);
			    	}
			    	else
			    		Util.lockView(getActivity(), false,txtQIM6,txtQIA6);
				}
			}
		});
		txtQIM6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(2);
		txtQIM6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIM6.getText()!=null)
					if(!txtQIM6.getText().toString().isEmpty())
						txtQIA6.requestFocus();
			}
		});
		txtQIA6=new IntegerField(this.getActivity()).size(altoComponente, 120).maxLength(4);
		txtQIA6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtQIA6.getText()!=null)
					if(!txtQIA6.getText().toString().isEmpty())
						chbQI7.requestFocus();
			}
		});
		
		griP465EC=new GridComponent2(this.getActivity(),App.ESTILO,4,1);
		griP465EC.addComponent(lblP465ec_1);
		griP465EC.addComponent(lblP465ec_2);
		griP465EC.addComponent(lblP465ec_3);
		griP465EC.addComponent(lblP465ec_4);
		griP465EC.addComponent(chbQI1);
		griP465EC.addComponent(txtQID1);
		griP465EC.addComponent(txtQIM1);
		griP465EC.addComponent(txtQIA1);
		griP465EC.addComponent(chbQI2);
		griP465EC.addComponent(txtQID2);
		griP465EC.addComponent(txtQIM2);
		griP465EC.addComponent(txtQIA2);
		griP465EC.addComponent(chbQI3);
		griP465EC.addComponent(txtQID3);
		griP465EC.addComponent(txtQIM3);
		griP465EC.addComponent(txtQIA3);
		griP465EC.addComponent(chbQI4);
		griP465EC.addComponent(txtQID4);
		griP465EC.addComponent(txtQIM4);
		griP465EC.addComponent(txtQIA4);
		griP465EC.addComponent(chbQI5);
		griP465EC.addComponent(txtQID5);
		griP465EC.addComponent(txtQIM5);
		griP465EC.addComponent(txtQIA5);
		griP465EC.addComponent(chbQI6);
		griP465EC.addComponent(txtQID6);
		griP465EC.addComponent(txtQIM6);
		griP465EC.addComponent(txtQIA6);
		griP465EC.addComponent(chbQI7);
		griP465EC.addComponent(lblP465ec_7,3);
		griP465EC.addComponent(chbQI8);
		griP465EC.addComponent(lblP465ec_8,3);
    } 
  
    @Override 
    protected View createUI() { 
		buildFields(); 
		q0 = createQuestionSection(txtCabecera,lblTitulo); 
		q1 = createQuestionSection(0,Gravity.CENTER|Gravity.CENTER_VERTICAL,lblpregunta465ec,griP465EC.component(),lblP4654ec_o,txtQI465EC_O); 
		q2 = createQuestionSection(0,Gravity.LEFT|Gravity.CENTER_VERTICAL,lblpregunta465ec1,lblpregunta465ec1_x1,chbQI465EC1_A,chbQI465EC1_B,chbQI465EC1_C,chbQI465EC1_D,chbQI465EC1_E,lblpregunta465ec1_x2,chbQI465EC1_F,chbQI465EC1_G,chbQI465EC1_H,chbQI465EC1_I,txtQI465EC1_I_O,lblpregunta465ec1_x3,chbQI465EC1_J,chbQI465EC1_K,chbQI465EC1_L,chbQI465EC1_M,lblpregunta465ec1_x4,chbQI465EC1_N,chbQI465EC1_O,chbQI465EC1_X,txtQI465EC1_X_O,chbQI465EC1_Z);
		
		ScrollView contenedor = createForm(); 
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0); 
		form.addView(q0); 
		form.addView(q1);
		form.addView(q2);
		return contenedor; 
    } 
    
    @Override 
    public boolean grabar() { 
		uiToEntity(c2seccion_04b);
		if (!validar()) { 
			if (error) { 
				if (!mensaje.equals("")) ToastMessage.msgBox(this.getActivity(), mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG); 
				if (view != null) view.requestFocus(); 
			} 
			return false; 
		} 
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
			else {
				getCuestionarioService().borrar04b(App.getInstance().getNacimientoVivos().id,App.getInstance().getNacimientoVivos().hogar_id,
				App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, "465ec");
				grabarDetalle();
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return false; 
		} 
		return true; 
    } 
    
    
    private boolean validar() { 
		if(!isInRange()) return false; 
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia); 
		
		if (!Util.esDiferente(c2seccion_04b.qi454,1) && chbQI8.isChecked()) {
			MyUtil.MensajeGeneral(this.getActivity(),"Verificar Pregunta QI454");
		}
		
		if(!validarCheckbox()) {
			mensaje = "Debe de seleccionar al menos una opci�n"; 
			view = chbQI1; 
			error = true; 
			return false;
		}
		
		if(chbQI1.isChecked()) {
			if (Util.esVacio(txtQID1.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA1"); 
				view = txtQID1; 
				error = true; 
				return false; 
			} 
			
			int id1 = txtQID1.getText().toString() == null ? 0 : Integer.parseInt(txtQID1.getText().toString());
			if (!(id1>=1 && id1<=31) && id1!=98 && id1!=44) {
				error = true;
				view = txtQID1;
				mensaje = "Valor del campo invalido";
				return false;		
			}

			if(id1!=44){
				if (Util.esVacio(txtQIM1.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES1"); 
					view = txtQIM1; 
					error = true; 
					return false; 
				} 
				int im1 = txtQIM1.getText().toString() == null ? 0 : Integer.parseInt(txtQIM1.getText().toString());
				if (!(im1>=1 && im1<=12)) {
					error = true;
					view = txtQIM1;
					mensaje = "Valor del campo invalido";
					return false;
				}
				
				if (Util.esVacio(txtQIA1.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O1"); 
					view = txtQIA1; 
					error = true; 
					return false; 
				} 
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID1.getText().toString()), Integer.parseInt(txtQIM1.getText().toString())) && id1!=98 ){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM1; 
					error = true; 
					return false; 
				}
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA1.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA1;
					mensaje = "Valor del campo fuera de rango";
					return false;
				}  
			}		
		}
		
		if(chbQI2.isChecked()) {
			if (Util.esVacio(txtQID2.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA2"); 
				view = txtQID2; 
				error = true; 
				return false; 
			} 
			
			int id2 = txtQID2.getText().toString() == null ? 0 : Integer.parseInt(txtQID2.getText().toString());
			if (!(id2>=1 && id2<=31) && id2!=98 && id2!=44 ) {
				error = true;
				view = txtQID2;
				mensaje = "Valor del campo invalido";
				return false;		
			}
			if(id2!=44){
				if (Util.esVacio(txtQIM2.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES2"); 
					view = txtQIM2; 
					error = true; 
					return false; 
				} 
				
				int im2 = txtQIM2.getText().toString() == null ? 0 : Integer.parseInt(txtQIM2.getText().toString());
				if (!(im2>=1 && im2<=12)) {
					error = true;
					view = txtQIM2;
					mensaje = "Valor del campo invalido";
					return false;
				}
	
				
				if (Util.esVacio(txtQIA2.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O2"); 
					view = txtQIA2; 
					error = true; 
					return false; 
				} 
				
				
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID2.getText().toString()), Integer.parseInt(txtQIM2.getText().toString())) && id2!=98  ){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM2; 
					error = true; 
					return false; 
				}
				
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA2.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA2;
					mensaje = "Valor del campo fuera de rango";
					return false;
				} 
			}
		}
		
		if(chbQI3.isChecked()) {
			if (Util.esVacio(txtQID3.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA3"); 
				view = txtQID3; 
				error = true; 
				return false; 
			} 
			
			int id3 = txtQID3.getText().toString() == null ? 0 : Integer.parseInt(txtQID3.getText().toString());
			if (!(id3>=1 && id3<=31) && id3!=98 && id3!=44 ) {
				error = true;
				view = txtQID3;
				mensaje = "Valor del campo invalido";
				return false;		
			}
			
			if( id3!=44){
				if (Util.esVacio(txtQIM3.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES3"); 
					view = txtQIM3; 
					error = true; 
					return false; 
				} 
				
				int im3 = txtQIM3.getText().toString() == null ? 0 : Integer.parseInt(txtQIM3.getText().toString());
				if (!(im3>=1 && im3<=12)) {
					error = true;
					view = txtQIM3;
					mensaje = "Valor del campo invalido";
					return false;
				}
				
				if (Util.esVacio(txtQIA3.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O3"); 
					view = txtQIA3; 
					error = true; 
					return false; 
				} 
				
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID3.getText().toString()), Integer.parseInt(txtQIM3.getText().toString())) && id3!=98 ){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM3; 
					error = true; 
					return false; 
				}
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA3.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA3;
					mensaje = "Valor del campo fuera de rango";
					return false;
				} 
			}
		}

		if(chbQI4.isChecked()) {
			if (Util.esVacio(txtQID4.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA4"); 
				view = txtQID4; 
				error = true; 
				return false; 
			} 
			
			int id4 = txtQID4.getText().toString() == null ? 0 : Integer.parseInt(txtQID4.getText().toString());
			if (!(id4>=1 && id4<=31) && id4!=98 && id4!=44) {
				error = true;
				view = txtQID4;
				mensaje = "Valor del campo invalido";
				return false;		
			}
			
			if(id4!=44){
				if (Util.esVacio(txtQIM4.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES4"); 
					view = txtQIM4; 
					error = true; 
					return false; 
				} 
				int im4 = txtQIM4.getText().toString() == null ? 0 : Integer.parseInt(txtQIM4.getText().toString());
				if (!(im4>=1 && im4<=12)) {
					error = true;
					view = txtQIM4;
					mensaje = "Valor del campo invalido";
					return false;
				}
				if (Util.esVacio(txtQIA4.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O4"); 
					view = txtQIA4; 
					error = true; 
					return false; 
				} 
				
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID4.getText().toString()), Integer.parseInt(txtQIM4.getText().toString())) && id4!=98){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM4; 
					error = true; 
					return false; 
				}
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA4.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA4;
					mensaje = "Valor del campo fuera de rango";
					return false;
				} 
			}
		}
		
		if(chbQI5.isChecked()) {
			if (Util.esVacio(txtQID5.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA5"); 
				view = txtQID5; 
				error = true; 
				return false; 
			} 
			
			int id5 = txtQID5.getText().toString() == null ? 0 : Integer.parseInt(txtQID5.getText().toString());
			if (!(id5>=1 && id5<=31) && id5!=98 && id5!=44) {
				error = true;
				view = txtQID5;
				mensaje = "Valor del campo invalido";
				return false;		
			}
			
			if(id5!=44){
				if (Util.esVacio(txtQIM5.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES5"); 
					view = txtQIM5; 
					error = true; 
					return false; 
				} 
				int im5 = txtQIM5.getText().toString() == null ? 0 : Integer.parseInt(txtQIM5.getText().toString());
				if (!(im5>=1 && im5<=12)) {
					error = true;
					view = txtQIM5;
					mensaje = "Valor del campo invalido";
					return false;
				}
				if (Util.esVacio(txtQIA5.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O5"); 
					view = txtQIA5; 
					error = true; 
					return false; 
				} 
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID5.getText().toString()), Integer.parseInt(txtQIM5.getText().toString())) && id5!=98){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM5; 
					error = true; 
					return false; 
				}
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA5.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA5;
					mensaje = "Valor del campo fuera de rango";
					return false;
				} 
			}
		}
		
		if(chbQI6.isChecked()) {
			if (Util.esVacio(txtQID6.getText().toString())) { 
				mensaje = preguntaVacia.replace("$", "La pregunta QIDIA6"); 
				view = txtQID6; 
				error = true; 
				return false; 
			} 
			
			int id6 = txtQID6.getText().toString() == null ? 0 : Integer.parseInt(txtQID6.getText().toString());
			if (!(id6>=1 && id6<=31) && id6!=98 && id6!=44) {
				error = true;
				view = txtQID6;
				mensaje = "Valor del campo invalido";
				return false;		
			}
			if(id6!=44){
				if (Util.esVacio(txtQIM6.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIMES6"); 
					view = txtQIM6; 
					error = true; 
					return false; 
				} 
				
				int im6 = txtQIM6.getText().toString() == null ? 0 : Integer.parseInt(txtQIM6.getText().toString());
				if (!(im6>=1 && im6<=12)) {
					error = true;
					view = txtQIM6;
					mensaje = "Valor del campo invalido";
					return false;
				}
				if (Util.esVacio(txtQIA6.getText().toString())) { 
					mensaje = preguntaVacia.replace("$", "La pregunta QIA�O6"); 
					view = txtQIA6; 
					error = true; 
					return false; 
				} 
				if(!MyUtil.DiaCorrespondeAlMes(Integer.parseInt(txtQID6.getText().toString()), Integer.parseInt(txtQIM6.getText().toString())) && id6!=98){
					mensaje = "d�a no corresponde al mes"; 
					view = txtQIM6; 
					error = true; 
					return false; 
				}
				Calendar cal2 = Calendar.getInstance();
				Integer anio = Integer.parseInt(txtQIA6.getText().toString());
				if(!(anio>=1975 && anio<=cal2.get(Calendar.YEAR)) ) {
					error = true;
					view = txtQIA6;
					mensaje = "Valor del campo fuera de rango";
					return false;
				} 
			}
			
			
		}
		
		if (!Util.esDiferente(c2seccion_04b.qi465ea,2) && !Util.esDiferente(c2seccion_04b.qi465eb,2) && !Util.esDiferente(c2seccion_04b.qi465ec,2) && !Util.esDiferente(c2seccion_04b.qi465ed,2)) { 
			if(!verificarCheckP465ec1()) {
				mensaje = "Seleccione una opci�n en la pregunta QH465EC1"; 
				view = chbQI465EC1_A; 
				error = true; 
				return false; 
			}	
			if(chbQI465EC1_I.isChecked()){
				if (Util.esVacio(c2seccion_04b.qi465ec1_i_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465EC1_I_O; 
					error = true; 
					return false; 
				}
			}
			if(chbQI465EC1_X.isChecked()){
				if (Util.esVacio(c2seccion_04b.qi465ec1_x_o)) { 
					mensaje = "Debe ingresar informaci\u00f3n en Especifique"; 
					view = txtQI465EC1_X_O; 
					error = true; 
					return false; 
				}
			}
		}
		
		
		return true; 
    } 
    
    
    @Override 
    public void cargarDatos() { 
    	c2seccion_04b = getCuestionarioService().getCISECCION_04B(App.getInstance().getNacimientoVivos().id, App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212,seccionesCargado);
		 
		if(c2seccion_04b==null){ 
		  c2seccion_04b=new CISECCION_04B(); 
		  c2seccion_04b.id=App.getInstance().getNacimientoVivos().id; 
		  c2seccion_04b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		  c2seccion_04b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		  c2seccion_04b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
	    }  

		nombre_persona = App.getInstance().getNacimientoVivos().qi212_nom;
		entityToUI(c2seccion_04b); 
		cargarDetalle();
		inicio(); 
    } 
    
    private void inicio() {   
    	renombrarEtiquetas();
    	ejecutaAccionCheckbox();
    	validarPregunta465ec1();
    	ValidarsiesSupervisora();
    	txtCabecera.requestFocus();
    } 
    public void renombrarEtiquetas() {
    	lblpregunta465ec1.text(R.string.c2seccion_04bqi465ec1);
    	lblpregunta465ec1.setText(lblpregunta465ec1.getText().toString().replace("(NOMBRE)", nombre_persona));
    }
    public boolean verificarCheckP465ec1() {
  		if (chbQI465EC1_A.isChecked()||chbQI465EC1_B.isChecked()||chbQI465EC1_C.isChecked()||chbQI465EC1_D.isChecked()||chbQI465EC1_E.isChecked()||chbQI465EC1_F.isChecked()||chbQI465EC1_G.isChecked()||chbQI465EC1_H.isChecked()||chbQI465EC1_I.isChecked()||chbQI465EC1_J.isChecked()||chbQI465EC1_K.isChecked()||chbQI465EC1_L.isChecked()||chbQI465EC1_M.isChecked()||chbQI465EC1_N.isChecked()||chbQI465EC1_O.isChecked()||chbQI465EC1_X.isChecked()||chbQI465EC1_Z.isChecked()) {
  			return true;
  		}else{			
  			return false;
  		}
  	}
    
    public void validarPregunta465ec1() {
    	Integer qi465ea=c2seccion_04b.qi465ea==null?0:c2seccion_04b.qi465ea;
    	Integer qi465eb=c2seccion_04b.qi465eb==null?0:c2seccion_04b.qi465eb;
    	Integer qi465ec=c2seccion_04b.qi465ec==null?0:c2seccion_04b.qi465ec;
    	Integer qi465ed=c2seccion_04b.qi465ed==null?0:c2seccion_04b.qi465ed;
    	
    	if (qi465ea!=2 || qi465eb!=2 || qi465ec!=2 || qi465ed!=2  ) {
    		Util.cleanAndLockView(getActivity(),chbQI465EC1_A,chbQI465EC1_B,chbQI465EC1_C,chbQI465EC1_D,chbQI465EC1_E,chbQI465EC1_F,chbQI465EC1_G,chbQI465EC1_H,chbQI465EC1_I,chbQI465EC1_J,chbQI465EC1_K,chbQI465EC1_L,chbQI465EC1_M,chbQI465EC1_N,chbQI465EC1_O,chbQI465EC1_X,chbQI465EC1_Z, txtQI465EC1_I_O, txtQI465EC1_X_O);
    		q2.setVisibility(View.GONE);
    	}
    	else{
    		Util.lockView(getActivity(),false,chbQI465EC1_A,chbQI465EC1_B,chbQI465EC1_C,chbQI465EC1_D,chbQI465EC1_E,chbQI465EC1_F,chbQI465EC1_G,chbQI465EC1_H,chbQI465EC1_I,chbQI465EC1_J,chbQI465EC1_K,chbQI465EC1_L,chbQI465EC1_M,chbQI465EC1_N,chbQI465EC1_O,chbQI465EC1_X,chbQI465EC1_Z);
    		q2.setVisibility(View.VISIBLE);
    		validarP465ec1();
    	}
	}
    
    public void validarP465ec1() {
		if(chbQI465EC1_Z.isChecked()){
			onqrgQI465ec1_zChangeValue();
		}
		else{			
			onqrgQI465ec1_aChangeValue();
			onqrgQI465ec1_bChangeValue();
			onqrgQI465ec1_cChangeValue();
			onqrgQI465ec1_dChangeValue();
			onqrgQI465ec1_eChangeValue();
			onqrgQI465ec1_fChangeValue();
			onqrgQI465ec1_gChangeValue();
			onqrgQI465ec1_hChangeValue();
			onqrgQI465ec1_iChangeValue();
			onqrgQI465ec1_jChangeValue();
			onqrgQI465ec1_kChangeValue();
			onqrgQI465ec1_lChangeValue();
			onqrgQI465ec1_mChangeValue();
			onqrgQI465ec1_nChangeValue();
			onqrgQI465ec1_oChangeValue();
			onqrgQI465ec1_xChangeValue();
		}
	}
    
    public void onqrgQI465ec1_aChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_bChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_cChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_dChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_eChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_fChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_gChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_hChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_iChangeValue() { 
		if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}
		if (chbQI465EC1_I.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI465EC1_I_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI465EC1_I_O);    		
    	}	
    }
    public void onqrgQI465ec1_jChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_kChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_lChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_mChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_nChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
    public void onqrgQI465ec1_oChangeValue(){if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}}
	public void onqrgQI465ec1_xChangeValue() {  	
		if (verificarCheckP465ec1()){Util.cleanAndLockView(getActivity(),chbQI465EC1_Z);}else{Util.lockView(getActivity(),false,chbQI465EC1_Z);}
		if (chbQI465EC1_X.isChecked()) {    		
			Util.lockView(getActivity(),false,txtQI465EC1_X_O);  
    	} 
    	else {	
    		Util.cleanAndLockView(getActivity(),txtQI465EC1_X_O);    		
    	}	
    }
	public void onqrgQI465ec1_zChangeValue() {  	
	 	if (verificarCheckP465ec1()) {
	   		Util.cleanAndLockView(getActivity(),chbQI465EC1_A,chbQI465EC1_B,chbQI465EC1_C,chbQI465EC1_D,chbQI465EC1_E,chbQI465EC1_F,chbQI465EC1_G,chbQI465EC1_H,chbQI465EC1_I,chbQI465EC1_J,chbQI465EC1_K,chbQI465EC1_L,chbQI465EC1_M,chbQI465EC1_N,chbQI465EC1_O,chbQI465EC1_X);	
	   	} 
	   	else {	
	   		Util.lockView(getActivity(),false,chbQI465EC1_A,chbQI465EC1_B,chbQI465EC1_C,chbQI465EC1_D,chbQI465EC1_E,chbQI465EC1_F,chbQI465EC1_G,chbQI465EC1_H,chbQI465EC1_I,chbQI465EC1_J,chbQI465EC1_K,chbQI465EC1_L,chbQI465EC1_M,chbQI465EC1_N,chbQI465EC1_O,chbQI465EC1_X);  				
	   	}	
	}	
	
	
    
    
    public CISECCION_04B_TARJETA getValoresCheck(int indice) {		
		CISECCION_04B_TARJETA objc204b=new CISECCION_04B_TARJETA();
		objc204b.id=App.getInstance().getNacimientoVivos().id; 
		objc204b.hogar_id=App.getInstance().getNacimientoVivos().hogar_id; 
		objc204b.persona_id=App.getInstance().getNacimientoVivos().persona_id;
		objc204b.ninio_id=App.getInstance().getNacimientoVivos().qi212;
		objc204b.pregunta = "465ec";
		objc204b.indice=indice;
		if(indice==1) {
			objc204b.dia = txtQID1.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null:txtQIM1.getText().toString();
			objc204b.anio= objc204b.dia.equals("44")?null:Integer.parseInt(txtQIA1.getText().toString());
		}
		else if(indice==2) {
			objc204b.dia = txtQID2.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null:txtQIM2.getText().toString();
			objc204b.anio = objc204b.dia.equals("44")?null:Integer.parseInt(txtQIA2.getText().toString());
		}
		else if(indice==3) {
			objc204b.dia = txtQID3.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null: txtQIM3.getText().toString();
			objc204b.anio = objc204b.dia.equals("44")?null:Integer.parseInt(txtQIA3.getText().toString());
		}
		else if(indice==4) {
			objc204b.dia = txtQID4.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null:txtQIM4.getText().toString();
			objc204b.anio = objc204b.dia.equals("44")?null: Integer.parseInt(txtQIA4.getText().toString());
		}
		else if(indice==5) {
			objc204b.dia = txtQID5.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null: txtQIM5.getText().toString();
			objc204b.anio = objc204b.dia.equals("44")?null: Integer.parseInt(txtQIA5.getText().toString());
		}
		else if(indice==6) {
			objc204b.dia = txtQID6.getText().toString();
			objc204b.mes = objc204b.dia.equals("44")?null:txtQIM6.getText().toString();
			objc204b.anio = objc204b.dia.equals("44")?null: Integer.parseInt(txtQIA6.getText().toString());
		}
		return objc204b;
	}
	
	
    
    public boolean grabarDetalle() throws SQLException {		
		CISECCION_04B_TARJETA objc204b=null;
		if(chbQI1.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();			
			objc204b = getValoresCheck(1);			
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(chbQI2.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(2);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
				}
		if(chbQI3.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(3);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(chbQI4.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(4);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(chbQI5.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(5);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		if(chbQI6.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(6);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		
		if(chbQI7.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(7);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
		
		if(chbQI8.isChecked()) {
			objc204b = new CISECCION_04B_TARJETA();
			objc204b = getValoresCheck(8);
			if(!getCuestionarioService().saveOrUpdate(objc204b,seccionesGrabado2)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return false; 
			} 
		}
	return true;	
	}
	
	public void cargarDetalle() {
		List<CISECCION_04B_TARJETA> lista = null;  
		lista = getCuestionarioService().getSecciones04BTarjeta(App.getInstance().getNacimientoVivos().id, 
				App.getInstance().getNacimientoVivos().hogar_id, App.getInstance().getNacimientoVivos().persona_id,
				App.getInstance().getNacimientoVivos().qi212,"465ec", seccionesCargado2);
		 
		if(lista!=null) {
			for(CISECCION_04B_TARJETA obj : lista){
				if(obj.indice==1) {
					chbQI1.setChecked(true);
					txtQID1.setText(obj.dia);
					txtQIM1.setText(obj.mes);
					if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA1.setText(obj.anio.toString());
				}
				if(obj.indice==2) {
					chbQI2.setChecked(true);
					txtQID2.setText(obj.dia);
					txtQIM2.setText(obj.mes);
				if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA2.setText(obj.anio.toString());
				}
				if(obj.indice==3) {
					chbQI3.setChecked(true);
					txtQID3.setText(obj.dia);
					txtQIM3.setText(obj.mes);
				if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA3.setText(obj.anio.toString());
				}
				if(obj.indice==4) {
					chbQI4.setChecked(true);
					txtQID4.setText(obj.dia);
					txtQIM4.setText(obj.mes);
				if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA4.setText(obj.anio.toString());
				}
				if(obj.indice==5) {
					chbQI5.setChecked(true);
					txtQID5.setText(obj.dia);
					txtQIM5.setText(obj.mes);
				if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA5.setText(obj.anio.toString());
				}
				if(obj.indice==6) {
					chbQI6.setChecked(true);
					txtQID6.setText(obj.dia);
					txtQIM6.setText(obj.mes);
				if(!obj.dia.toString().isEmpty() && !obj.dia.equals("44"))
					txtQIA6.setText(obj.anio.toString());
				}
				if(obj.indice==7) {
					chbQI7.setChecked(true);
				}
				if(obj.indice==8) {
					chbQI8.setChecked(true);
				}
			}
		}
	}
	
	public void ejecutaAccionCheckbox() {	    	
			onqrgQI465ec1ChangeValue();
			onqrgQI465ec2ChangeValue();
			onqrgQI465ec3ChangeValue();
			onqrgQI465ec4ChangeValue();
			onqrgQI465ec5ChangeValue();
			onqrgQI465ec6ChangeValue();
	}
	
	public void onqrgQI465ec1ChangeValue() {  	
    	if (chbQI1.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID1,txtQIM1,txtQIA1);
  			txtQID1.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID1,txtQIM1,txtQIA1);  
  		}	
    }
	
	
	public void onqrgQI465ec2ChangeValue() {  	
    	if (chbQI2.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID2,txtQIM2,txtQIA2);
  			txtQID2.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID2,txtQIM2,txtQIA2);  
  		}	
    }
	
	
	public void onqrgQI465ec3ChangeValue() {  	
    	if (chbQI3.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID3,txtQIM3,txtQIA3);
  			txtQID3.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID3,txtQIM3,txtQIA3);  
  		}	
    }
	
	
	public void onqrgQI465ec4ChangeValue() {  	
    	if (chbQI4.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID4,txtQIM4,txtQIA4);
  			txtQID4.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID4,txtQIM4,txtQIA4);  
  		}	
    }
	
	public void onqrgQI465ec5ChangeValue() {  	
    	if (chbQI5.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID5,txtQIM5,txtQIA5);
  			txtQID5.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID5,txtQIM5,txtQIA5);  
  		}	
    }
	
	public void onqrgQI465ec6ChangeValue() {  	
    	if (chbQI6.isChecked()){		
  			Util.lockView(getActivity(),false,txtQID6,txtQIM6,txtQIA6);
  			txtQID6.requestFocus();
  		} 
  		else {
  			Util.cleanAndLockView(getActivity(),txtQID6,txtQIM6,txtQIA6);  
  		}	
    }
	
	public void onqrgQI465ec7ChangeValue() {  	
    	if (chbQI7.isChecked()){		
  			Util.cleanAndLockView(getActivity(), chbQI1,chbQI2,chbQI3,chbQI4,chbQI5,chbQI6,chbQI8);
  			ejecutaAccionCheckbox();
    	} 
  		else {
  			Util.lockView(getActivity(),false, chbQI1,chbQI2,chbQI3,chbQI4,chbQI5,chbQI6,chbQI8);
  		}	
    }
	
	public void onqrgQI465ec8ChangeValue() {  	
    	if (chbQI8.isChecked()){		
  			Util.cleanAndLockView(getActivity(), chbQI1,chbQI2,chbQI3,chbQI4,chbQI5,chbQI6,chbQI7);
  			ejecutaAccionCheckbox();
    	} 
  		else {
  			Util.lockView(getActivity(),false, chbQI1,chbQI2,chbQI3,chbQI4,chbQI5,chbQI6,chbQI7);
  		}	
    }

	
	public boolean validarCheckbox() {
		boolean res=false;
		if(chbQI1.isChecked() || chbQI2.isChecked() || chbQI3.isChecked() || chbQI4.isChecked() || chbQI5.isChecked() || chbQI6.isChecked() || chbQI7.isChecked() || chbQI8.isChecked()) {
			res = true;
		}
		return res;
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			txtQIA1.readOnly();
			txtQIA2.readOnly();
			txtQIA3.readOnly();
			txtQIA4.readOnly();
			txtQIA5.readOnly();
			txtQIA6.readOnly();
			txtQIM1.readOnly();
			txtQIM2.readOnly();
			txtQIM3.readOnly();
			txtQIM4.readOnly();
			txtQIM5.readOnly();
			txtQIM6.readOnly();
			txtQID1.readOnly();
			txtQID2.readOnly();
			txtQID3.readOnly();
			txtQID4.readOnly();
			txtQID5.readOnly();
			txtQID6.readOnly();
			chbQI1.readOnly();
			chbQI2.readOnly();
			chbQI3.readOnly();
			chbQI4.readOnly();
			chbQI5.readOnly();
			chbQI6.readOnly();
			chbQI7.readOnly();
			chbQI8.readOnly();
			txtQI465EC_O.setEnabled(false);
			chbQI465EC1_A.readOnly();
			chbQI465EC1_B.readOnly();
			chbQI465EC1_C.readOnly();
			chbQI465EC1_D.readOnly();
			chbQI465EC1_E.readOnly();
			chbQI465EC1_F.readOnly();
			chbQI465EC1_G.readOnly();
			chbQI465EC1_H.readOnly();
			chbQI465EC1_I.readOnly();
			txtQI465EC1_I_O.readOnly();
			chbQI465EC1_J.readOnly();
			chbQI465EC1_K.readOnly();
			chbQI465EC1_L.readOnly();
			chbQI465EC1_M.readOnly();
			chbQI465EC1_N.readOnly();
			chbQI465EC1_O.readOnly();
			chbQI465EC1_X.readOnly();
			txtQI465EC1_X_O.readOnly();
			chbQI465EC1_Z.readOnly();
			
			
		}
	}
	
    public CuestionarioService getCuestionarioService() { 
		if(cuestionarioService==null){ 
			cuestionarioService = CuestionarioService.getInstance(getActivity()); 
		} 
		return cuestionarioService; 
    }
    
	@Override
	public Integer grabadoParcial() {
		uiToEntity(c2seccion_04b); 
		
		try { 
			if(!getCuestionarioService().saveOrUpdate(c2seccion_04b,seccionesGrabado)){ 
				ToastMessage.msgBox(this.getActivity(), "Los datos no pudieron ser guardados.", ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
				return App.NODEFINIDO; 
			} 
			else {
				getCuestionarioService().borrar04b(App.getInstance().getNacimientoVivos().id,App.getInstance().getNacimientoVivos().hogar_id,
				App.getInstance().getNacimientoVivos().persona_id,App.getInstance().getNacimientoVivos().qi212, "465ec");
				grabarDetalle();
			}
		} catch (SQLException e) { 
			ToastMessage.msgBox(this.getActivity(), e.getMessage(), ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT); 
			return App.NODEFINIDO; 
		} 
		return App.INDIVIDUAL; 
	}
} 
