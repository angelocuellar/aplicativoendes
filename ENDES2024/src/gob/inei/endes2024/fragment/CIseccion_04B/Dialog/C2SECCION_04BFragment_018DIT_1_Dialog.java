package gob.inei.endes2024.fragment.CIseccion_04B.Dialog;
import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.RadioGroupOtherField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Caretaker;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.fragment.CIseccion_01_03.CISECCION_03Fragment_000;
import gob.inei.endes2024.fragment.CIseccion_04B.C2SECCION_04BFragment_022DIT_1;
import gob.inei.endes2024.fragment.seccion01.Seccion01Fragment_005;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.sql.SQLException;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class C2SECCION_04BFragment_018DIT_1_Dialog extends DialogFragmentComponent implements Respondible {

	@FieldAnnotation(orderIndex = 1)
	public RadioGroupOtherField rgQD333_1;
	@FieldAnnotation(orderIndex = 2)
	public RadioGroupOtherField rgQD333_2;
	@FieldAnnotation(orderIndex = 3)
	public RadioGroupOtherField rgQD333_3;
	@FieldAnnotation(orderIndex = 4)
	public RadioGroupOtherField rgQD333_4;
	@FieldAnnotation(orderIndex = 5)
	public RadioGroupOtherField rgQD333_5;
	@FieldAnnotation(orderIndex = 6)
	public RadioGroupOtherField rgQD333_6;
	@FieldAnnotation(orderIndex = 7)
	public ButtonComponent btnAceptar;
	
//	Seccion01 bean;
	DISCAPACIDAD modelo;
	private static C2SECCION_04BFragment_022DIT_1 caller;
	public ButtonComponent btnCancelar;
	private SeccionCapitulo[] seccionesGrabado;
	private SeccionCapitulo[] seccionesCargado;
	private CuestionarioService cuestionarioService;
	private Seccion01Service serviceSeccion01;
	private CuestionarioService hogarService;
	public GridComponent2 gridPreguntas26;
	public LabelComponent lbl26a1, lbl26a2, lbl26a3, lbl26a4, lbl26a5, lbl26a6;
	public List<Seccion01> listaMadres;
	public LabelComponent lbllimitaciones;
	private int iniPosition;
	public List<Seccion01> detallesq;
	public Integer contador = 0;
	public TableComponent tcPersonasDialog;
	private PROCCES action = null;
	private enum PROCCES {
		 GRABADOPARCIAL
    }

	public static C2SECCION_04BFragment_018DIT_1_Dialog newInstance(FragmentForm pagina,DISCAPACIDAD detalle) {
		caller = (C2SECCION_04BFragment_022DIT_1) pagina;
		C2SECCION_04BFragment_018DIT_1_Dialog f = new C2SECCION_04BFragment_018DIT_1_Dialog();
		f.setParent(pagina);
		Bundle args = new Bundle();
		args.putSerializable("detalle", detalle);
		f.setArguments(args);
		return f;
	}

	public interface Seccion01Fragment_002_BListener {
		void onFinishEditDialog(String inputText);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		modelo = (DISCAPACIDAD) getArguments().getSerializable("detalle");
		caretaker = new Caretaker<Entity>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().setTitle("N� DE ORDEN:  " +modelo.ninio_id);
		
		final View rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		cargarDatos();
		enlazarCajas();
		listening();
		return rootView;

	}

	public C2SECCION_04BFragment_018DIT_1_Dialog() {
		super();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
	}

	@Override
	protected void buildFields() {

		lbl26a1 = new LabelComponent(this.getActivity()).size(altoComponente, 450).text(R.string.seccion01qh26a1).textSize(18).alinearIzquierda();
		rgQD333_1 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a1_1, R.string.seccion01qh26a1_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a2 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a2).textSize(18).alinearIzquierda();
		rgQD333_2 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a2_1, R.string.seccion01qh26a2_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a3 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a3).textSize(18).alinearIzquierda();
		rgQD333_3 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a3_1, R.string.seccion01qh26a3_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a4 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a4).textSize(18).alinearIzquierda();
		rgQD333_4 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a4_1, R.string.seccion01qh26a4_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a5 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a5).textSize(18).alinearIzquierda();
		rgQD333_5 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a5_1, R.string.seccion01qh26a5_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();
		lbl26a6 = new LabelComponent(this.getActivity()).size(altoComponente, 400).text(R.string.seccion01qh26a6).textSize(18).alinearIzquierda();
		rgQD333_6 = new RadioGroupOtherField(this.getActivity(),R.string.seccion01qh26a6_1, R.string.seccion01qh26a6_2).size(90, 300).orientation(RadioGroupOtherField.ORIENTATION.HORIZONTAL).centrar();

		btnAceptar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnAceptar).size(200, 60);
		btnCancelar = new ButtonComponent(getParent().getActivity(),App.ESTILO_BOTON).text(R.string.btnCancelar).size(200, 60);

		lbllimitaciones = new LabelComponent(getActivity()).textSize(20).size(MATCH_PARENT, MATCH_PARENT).text(R.string.seccion01qh26);
		
		gridPreguntas26 = new GridComponent2(this.getActivity(), App.ESTILO, 2);
		gridPreguntas26.addComponent(lbl26a1);
		gridPreguntas26.addComponent(rgQD333_1);
		gridPreguntas26.addComponent(lbl26a2);
		gridPreguntas26.addComponent(rgQD333_2);
		gridPreguntas26.addComponent(lbl26a3);
		gridPreguntas26.addComponent(rgQD333_3);
		gridPreguntas26.addComponent(lbl26a4);
		gridPreguntas26.addComponent(rgQD333_4);
		gridPreguntas26.addComponent(lbl26a5);
		gridPreguntas26.addComponent(rgQD333_5);
		gridPreguntas26.addComponent(lbl26a6);
		gridPreguntas26.addComponent(rgQD333_6);

		btnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				modelo.restoreFromMemento(caretaker.get("antes"));
//				caller.cargarTabla();
//				caller.contador=0;
				C2SECCION_04BFragment_018DIT_1_Dialog.this.dismiss();
			}
		});
		
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}
//				caller.cargarTabla();
				C2SECCION_04BFragment_018DIT_1_Dialog.this.dismiss();
			}
		});
		
	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q1 = createQuestionSection(lbllimitaciones,gridPreguntas26.component());
		LinearLayout botones = createButtonSection(btnAceptar, btnCancelar);
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);

		form.addView(q1);
		form.addView(botones);
		return contenedor;
	}

	public boolean grabar() {
		uiToEntity(modelo);
		if (!validar()) {
			if (error) {
				if (!mensaje.equals(""))
					ToastMessage.msgBox(this.getActivity(), mensaje,ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
				if (view != null)
					view.requestFocus();
			}
			return false;
		}
		boolean flag = true;
		try {
			modelo.cuestionario_id=modelo.cuestionario_id==null?App.CUEST_ID_INDIVIDUAL:modelo.cuestionario_id;
			flag = getCuestionarioService().saveOrUpdate_Discapacidad(modelo, seccionesGrabado);

		} catch (SQLException e) {
			ToastMessage.msgBox(this.getActivity(), e.getMessage(),ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_LONG);
		}
		return flag;
	}

	public boolean validar() {
		String preguntaVacia = this.getResources().getString(R.string.pregunta_no_vacia);

		if (Util.esVacio(modelo.qd333_1)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A1");
			view = rgQD333_1;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_2)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A2");
			view = rgQD333_2;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_3)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A3");
			view = rgQD333_3;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_4)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A4");
			view = rgQD333_4;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_5)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A5");
			view = rgQD333_5;
			error = true;
			return false;
		}
		if (Util.esVacio(modelo.qd333_6)) {
			mensaje = preguntaVacia.replace("$", "La pregunta P.26A6");
			view = rgQD333_6;
			error = true;
			return false;
		}
		return true;
	}

	private void cargarDatos() {

		if (modelo != null) {
			modelo = getCuestionarioService().getSeccion02IndividualParaDiscapacidad(modelo.id,modelo.hogar_id,Integer.valueOf(App.CUEST_ID_INDIVIDUAL), modelo.persona_id,modelo.ninio_id, seccionesCargado);
		}
		if (modelo == null) {
			modelo = new DISCAPACIDAD();
			modelo.id = App.getInstance().getHogar().id;
			modelo.hogar_id = App.getInstance().getHogar().hogar_id;
			modelo.persona_id =App.getInstance().getSeccion04DIT_02().persona_id;
			modelo.ninio_id= App.getInstance().getSeccion04DIT_02().nro_orden_ninio;
			modelo.cuestionario_id=App.CUEST_ID_INDIVIDUAL;
		}
		entityToUI(modelo);
		caretaker.addMemento("antes", modelo.saveToMemento(DISCAPACIDAD.class));
		inicio();
	}
	private void inicio() {
		rgQD333_1.requestFocus();
		ValidarsiesSupervisora();
		RenombrarLabels();
	}
	
	public void ValidarsiesSupervisora(){
		Integer codigo=App.getInstance().getUsuario().cargo_id;
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, codigo,codigo) && !Util.esDiferente(App.getInstance().getMarco().asignado, App.VIVIENDAASIGNADASUPERVISORA)){
			rgQD333_1.readOnly();
			rgQD333_2.readOnly();
			rgQD333_3.readOnly();
			rgQD333_4.readOnly();
			rgQD333_5.readOnly();
			rgQD333_6.readOnly();			
		}
	}

	private void RenombrarLabels() {
		String replace = "(NOMBRE)";
		String Ud = "Usted";
//		bean.qhinfo = bean.qhinfo == null ? 0 : bean.qhinfo;
//		lbllimitaciones.setText(lbllimitaciones.getText().toString().replace(replace, bean.qhinfo == 1 ? Ud : bean.qh02_1));
//		Spanned texto828 = Html.fromHtml(lbllimitaciones.getText()+ " <b>permanente:</b>");
		Spanned texto828 = Html.fromHtml(lbllimitaciones.getText()+ " permanente para:");
		lbllimitaciones.setText(texto828);
	}
	

	public CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	public Seccion01Service getServiceSeccion01() {
		if (serviceSeccion01 == null) {
			serviceSeccion01 = Seccion01Service.getInstance(getActivity());
		}
		return serviceSeccion01;
	}

	private CuestionarioService getService() {
		if (hogarService == null) {
			hogarService = CuestionarioService.getInstance(getActivity());
		}
		return hogarService;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		C2SECCION_04BFragment_018DIT_1_Dialog.this.dismiss();
//		caller.contador=0;
//		caller.getParent().nextFragment(CuestionarioFragmentActivity.VISITA);
//		caller.nextFragment(CuestionarioFragmentActivity.CHSECCION1f_2);
	}

}
