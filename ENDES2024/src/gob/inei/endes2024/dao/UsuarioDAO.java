package gob.inei.endes2024.dao;

import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Usuario;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.dao.SQLiteDAO.Query;
import gob.inei.dnce.util.Util;

import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UsuarioDAO extends SQLiteDAO {
	
	public static UsuarioDAO INSTANCE; 
	public static final String TABLA = "T_USUARIO"; 

	private UsuarioDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	
	public static UsuarioDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new UsuarioDAO(dbh);
		}
		return INSTANCE;
	}
	
	public Usuario getUsuario(String login, String clave) {
		SQLiteDatabase dbr = dbh.getReadableDatabase();		
		Usuario bean = new Usuario();
		if ("SIS".equals(login) && "123".equals(clave)) {
			Log.e("ss","ssssssssssss");
			bean.id = -1;
			bean.usuario = login;
			bean.clave = clave;
			bean.cargo_id = 21;
			bean.nombres="SIS";
			cursor = dbr.rawQuery("SELECT 1", null);
			return bean;
		}
		if ("ADM".equals(login) && "159753".equals(clave)) {
			bean.id = 0;
			bean.usuario = login;
			bean.clave = clave;
			bean.cargo_id=25;
			cursor = dbr.rawQuery("SELECT 1", null);
			return bean;
		}
		return (Usuario) getBean(TABLA, "USUARIO=? AND CLAVE=?", Usuario.class, login, clave);
	}
	
	public List<Usuario> getAuxiliares(Integer id) {
		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		String equipo="";
		query.append("select EQUIPO ")
		.append(" FROM ").append(TABLA)
		.append(" WHERE ID = ? ");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString()});
		if(cursor.moveToNext()){
			equipo= Util.getText(getString("EQUIPO"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT usuario, NOMBRES , APELLIDOS").append(" ")  //
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE EQUIPO = ? AND (CARGO_ID = 21 OR CARGO_ID = 22) ").append(" ");
			Query query2 = new Query(sb.toString(), equipo);
//			Usuario nuevo = new Usuario();
//			nuevo.
		return getBeans(query2, Usuario.class, "usuario", "nombres", "apellidos");// 
    }
	
	public List<Usuario> getAntropo(Integer id) {
		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		String equipo="";
		query.append("select EQUIPO ")
		.append(" FROM ").append(TABLA)
		.append(" WHERE ID = ? ");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString()});
		if(cursor.moveToNext()){
			equipo= Util.getText(getString("EQUIPO"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT usuario, NOMBRES , APELLIDOS").append(" ")  //
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE EQUIPO = ? AND CARGO_ID = 25").append(" ");
			Query query2 = new Query(sb.toString(), equipo);
		return getBeans(query2, Usuario.class, "usuario", "nombres", "APELLIDOS");// 
    }
	
	public boolean saveOrUpdate(Usuario bean) {			
		return this.saveOrUpdate(bean, null);
	}
	
	public boolean saveOrUpdate(Usuario bean, SQLiteDatabase dbTX) {
		if (dbTX == null) {
			dbTX = dbh.getWritableDatabase();
		}
		String oper = existeRegistro(dbTX, "ID", TABLA, "ID = ?", bean.id.toString()) ? "edit":"add";
		ContentValues content = new ContentValues();	
		String[] campos = bean.getFieldsSaveNames();	
		content = bean.getContentValues(content, -1, -1, campos);		
		boolean result;
		if (oper.equals("add")) {
			result = dbTX.insertOrThrow(TABLA, null, content)!=-1;
		} else {
			result = dbTX.update(TABLA, content, "ID = ?", new String[]{bean.id.toString()})>0;
		}		
		SQLiteDatabase.releaseMemory();		
		return result;
	}
//	public Usuario getUsuarioById(Integer id){
//		StringBuilder sb = new StringBuilder();
//		String[] campos = new Usuario().getFieldsNames();
//		sb.append("SELECT * ").append(" ")
//		.append(" FROM ").append(TABLA).append(" ")
//		.append(" WHERE ID=? ");
//		Query query= new Query(sb.toString(), new String[]{id.toString()});
//		return (Usuario) getBean(query,Usuario.class,campos);
//	}
	public Usuario getUsuarioById(String usuario){
		StringBuilder sb = new StringBuilder();
		String[] campos = new Usuario().getFieldsNames();
		sb.append("SELECT DISTINCT * ").append(" ")
		.append(" FROM ").append(TABLA).append(" ")
		.append(" WHERE USUARIO=? ");
		Query query= new Query(sb.toString(), new String[]{usuario.toString()});
		
		return (Usuario) getBean(query,Usuario.class,campos);
		
	}
}
