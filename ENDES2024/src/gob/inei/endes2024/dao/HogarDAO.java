package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.endes2024.model.Hogar;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class HogarDAO extends SQLiteDAO {
	
	public static HogarDAO INSTANCE; 
	public static final String TABLA = "T_05_DIG_HOGAR"; 
	public static final String VISTA_INFORMENTE="V_INFORMANTE";
	public static final String VISTA_ESTADO_HOGAR="V_ESTADO_HOGAR";
	public static final String TABLA_HOGAR = "T_05_DIG_HOGAR";
	
	private HogarDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	
	public static HogarDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new HogarDAO(dbh);
		}
		return INSTANCE;
	}

	public boolean saveOrUpdate(Hogar bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.hogar_id == null) {
			bean.hogar_id = nextID(dbTX, "HOGAR_ID", TABLA, "ID=?", bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA, "ID=? AND HOGAR_ID=?", bean, 
				new String[]{"id", "hogar_id"}, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public List<Hogar> getHogares(Integer id, SeccionCapitulo... secciones) {
		return getBeans(VISTA_ESTADO_HOGAR, "ID=? AND HOGAR_ID NOT IN(0) "
				, new String[]{id.toString()},"ID",Hogar.class,secciones);
//		StringBuilder sbr = new StringBuilder();
//		String[] campos = new Hogar().getFieldsNames();
//		String[] camposFinal = new String[campos.length + 4];
//		camposFinal = Arrays.copyOf(campos, camposFinal.length);
//		camposFinal[camposFinal.length-4] = "qhvresul";
//		camposFinal[camposFinal.length-3] = "qh02_3";
//		camposFinal[camposFinal.length-2] = "qh02_2";
//		camposFinal[camposFinal.length-1] = "qh02_1";
//		sbr.append("SELECT h.*, p.QH02_1, p.QH02_2, p.QH02_3,V.QHVRESUL ").append(" ")
//			.append("FROM ").append(TABLA).append(" h ")
//			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.QH01 = 1 ")
//			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_VISITA).append(" V ON h.ID=V.ID AND h.HOGAR_ID=V.HOGAR_ID AND V.QHVRESUL=1 ")
//			.append(" WHERE h.ID = ? ")
//			.append(" ORDER BY h.HOGAR_ID");
//		Query query = new Query(sbr.toString(), id.toString());
//		return getBeans(query, Hogar.class, camposFinal);
	}
	
	public Hogar getHogar(Integer id, Integer hogar) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Hogar().getFieldsNames();
		String[] camposFinal = new String[campos.length];
		camposFinal = Arrays.copyOf(campos, campos.length);
		camposFinal[camposFinal.length-1] = "c1p202";
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(TABLA).append(" h ")
			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString());
		return (Hogar) getBean(query, Hogar.class, camposFinal);
	}

	public Hogar getHogar(Integer id, Integer hogar, SeccionCapitulo... capitulos) {
		StringBuilder sbr = new StringBuilder();
		Hogar tmp = new Hogar();		
		List<String> listaCampos = new ArrayList<String>();
		for (int i = 0; i < capitulos.length; i++) {
			SeccionCapitulo seccion = capitulos[i];
			listaCampos.addAll(tmp.getFieldMatches(seccion.getSeccion(),
					seccion.getSubseccion(), seccion.getInicio(),
					seccion.getFin(), seccion.getSubPregI(),
					seccion.getSubPregF()));
			if (seccion.getCampos() != null) {
				for (int j = 0; j < seccion.getCampos().length; j++) {
					listaCampos.add(seccion.getCampos()[j]);
				}
			}
		}
		String[] campos = listaCampos.toArray(new String[listaCampos.size()]);
		String[] camposFinal = new String[campos.length + 1];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-1] = "c1p202";		
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(TABLA).append(" h ")
			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString());
		return (Hogar) getBean(query, Hogar.class, camposFinal);
	}
	
	public Hogar getHogarId(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return (Hogar) getBean(TABLA,"ID=? AND HOGAR_ID=?",new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Hogar.class,secciones);
	}
	
	public boolean borrarHogar(Hogar bean, SQLiteDatabase dbTX) {
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
	
			boolean 
					flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION04_05_DET, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION04_05, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION03, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_INCENTIVOS, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_HOGAR, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION01, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_MORTALIDAD, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_MIGRACION, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_IMIGRACION, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_VISITA, "ID=? AND HOGAR_ID=?", bean.id.toString(), bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CSVISITA,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_SALUD, "ID=? AND HOGAR_ID=? ",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CAP04_07, "ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_04 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_03 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_02 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_01 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_09 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_08 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_05 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_05_07 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B2 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
//					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT_02 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B_TARJETA ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04A ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_DISCAPACIDAD ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO4 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02T ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_01_03 ,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
					flag = borrar(dbTX, CuestionarioDAO.TABLA_Individual,"ID=? AND HOGAR_ID=?",bean.id.toString(),bean.hogar_id.toString());
			
			Integer totalhogares = MaximaCantidaddeHogares(bean, dbTX);
			Integer id_nuevo=bean.hogar_id+1;
			if(bean.hogar_id<totalhogares)
			{
				for (int i = bean.hogar_id; i < totalhogares; i++) {
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_INCENTIVOS, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_HOGAR, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_VISITA, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION01, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_MORTALIDAD, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_MIGRACION, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_IMIGRACION, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION03, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION04_05_DET, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION04_05, bean.id,id_nuevo,i, dbTX);						
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CSVISITA, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SALUD, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CAP04_07, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CSSECCION_08, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_Individual, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_01_03, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_02, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_02T, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_DISCAPACIDAD, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_04A, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_04B, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_04B2, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, bean.id,id_nuevo,i, dbTX);
//						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_DIT ,bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_DIT_02 ,bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_05_07, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_05, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_08, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_09, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_10_01, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_10_02, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_10_03, bean.id,id_nuevo,i, dbTX);
						ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CISECCION_10_04, bean.id,id_nuevo,i, dbTX);
						id_nuevo++;
				}
			}
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				dbTX.close();
				SQLiteDatabase.releaseMemory();
			}
//			ReordenarNumerodeOrdendeHogares(bean,null);
		return flag;
	}
	public boolean ModificarNumerodeOrdenHogar(String nombreTabla,Integer id,Integer hogar_id_nuevo,Integer hogar_id_anterior, SQLiteDatabase dbTX){	
		ContentValues valores = new ContentValues();
		valores.put("HOGAR_ID", hogar_id_anterior);
		if (dbTX.update(
				nombreTabla,
				valores,
				"ID=? AND HOGAR_ID=?",
				new String[] { id.toString(),hogar_id_nuevo.toString() }) == 1) {
			return true;
		} else {
			return false;
		}
	}
//	public boolean saveOrUpdate(Hogar bean, int from, int to,String... campos) throws java.sql.SQLException {
//		return this.saveOrUpdate(TABLA_HOGAR,"ID=? AND HOGAR_ID=?", bean, new String[] {"id", "hogar_id" }, from, to, campos);
//	}
	
//	public void ReordenarNumerodeOrdendeHogares(Hogar bean,SQLiteDatabase dbTX)
//	{	boolean isTX = true;
//		if (dbTX == null) {
//		dbTX = startTX();
//		isTX = false;			
//		}
//		
//		Integer orden=MaximaCantidaddeHogares(bean,dbTX);
//		boolean flag=false;
//		Integer id_nuevo=bean.hogar_id+1;
//		if(bean.hogar_id<orden)
//		{
//			for (int i = bean.hogar_id; i < orden; i++) {
//			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_HOGAR, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_VISITA, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION01, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION03, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SECCION04_05, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CSVISITA, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_SALUD, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CAP04_07, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_CSSECCION_08, bean.id,id_nuevo,i, dbTX);
////			flag=ModificarNumerodeOrdenHogar(CuestionarioDAO.TABLA_Individual, bean.id,id_nuevo,i, dbTX);
//			id_nuevo++;
//			}
//		}
//		if (!isTX) {
//			if(flag){
//				commitTX(dbTX);
//			}
//			endTX(dbTX);
//			dbTX.close();
//			SQLiteDatabase.releaseMemory();
//		}
//	}
	public Integer MaximaCantidaddeHogares(Hogar bean,SQLiteDatabase dbTX){
		return max(dbTX,"HOGAR_ID",CuestionarioDAO.TABLA_HOGAR,"ID=?",bean.id.toString());
	}
}