package gob.inei.endes2024.dao;

import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.endes2024.model.UsuarioRuta;

import java.sql.SQLException;

import android.database.sqlite.SQLiteDatabase;

public class UsuarioRutaDAO extends SQLiteDAO {
	
	public static UsuarioRutaDAO INSTANCE; 
	public static final String TABLA = "T_USUARIO_RUTA"; 

	private UsuarioRutaDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	
	public static UsuarioRutaDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new UsuarioRutaDAO(dbh);
		}
		return INSTANCE;
	}
	
	public boolean saveOrUpdate(UsuarioRuta bean, SQLiteDatabase dbTX) throws SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return saveOrUpdate(dbTX, TABLA, "SEGMENTACION_ID=? AND USUARIO_ID=?", bean, new String[]{"segmentacion_id","usuario_id"}, -1, -1, campos);
	}
}
