package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.DatabaseHelper;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.dao.SQLiteDAO.Query;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Seccion01;

import java.io.StreamCorruptedException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Seccion01DAO extends SQLiteDAO{
	public static Seccion01DAO INSTANCE; 
	public static final String TABLA_HOGAR = "T_05_DIG_HOGAR";
	public static final String TABLA_SECCION01 = "T_05_DIG_SECCION01";
	public static final String TABLA_SECCION03 = "T_05_DIG_SECCION03";
	public static final String TABLA_SECCION04 = "T_05_DIG_SECCCION04_05";
	public static final String VISTA_LISTADO_SECCION03 = "V_SECCION3";
	public static final String TABLA_CISECCION_01_03="T_05_DIG_CISECCION_01_03";
	public static final String TABLA_CISECCION_02="T_05_DIG_CISECCION_02";
	public static final String TABLA_CISECCION_02T="T_05_DIG_CISECCION_02T";
	public static final String TABLA_CISECCION_04A="T_05_DIG_CISECCION_04A";
	public static final String TABLA_CISECCION_04B ="T_05_DIG_CISECCION_04B";
	public static final String TABLA_CISECCION_04B_TARJETA ="T_05_DIG_CISECCION_04B_TARJETA";
	
//	public static final String TABLA_CISECCION_DIT="T_05_DIG_CISECCION_04DIT";
	public static final String TABLA_CISECCION_DIT_02="T_05_DIG_CISECCION_04DIT_02";
	public static final String TABLA_CISECCION_05_07="T_05_DIG_CISECCION_05_07";
	public static final String TABLA_CISECCION_05= "T_05_DIG_CISECCION_05";
	public static final String TABLA_CISECCION_08= "T_05_DIG_CISECCION_08";
	public static final String TABLA_CISECCION_10_01= "T_05_DIG_CISECCION_10_01";
	public static final String TABLA_CISECCION_10_02= "T_05_DIG_CISECCION_10_02";
	public static final String TABLA_CISECCION_10_03= "T_05_DIG_CISECCION_10_03";
	public static final String TABLA_CISECCION_10_04= "T_05_DIG_CISECCION_10_04";
	public static final String TABLA_CALENDARIO="T_05_DIG_CICALENDARIO_COL01_03";
	public static final String TABLA_CALENDARIO04="T_05_DIG_CICALENDARIO_COL04";
	public static final String TABLA_CALENDARIO_TRAMO="T_05_DIG_CICALENDARIO_TRAMO_COL01_03";
	public static final String TABLA_CALENDARIO_TRAMO4="T_05_DIG_CICALENDARIO_TRAMO_COL4";
	public static final String TABLA_CISECCION_09 = "T_05_DIG_CISECCION_09";
	public static final String TABLA_CISECCION_04B2 = "T_05_DIG_CISECCION_04B2";
	public static final String VISTA_MEFCOMPLETADO="V_MEFCOMPLETADO";
	public static final String TABLA_CSSECCION08 ="T_05_DIG_CSSECCION_08";	
	public static final String TABLA_SECCION04_05 = "T_05_DIG_SECCCION04_05";
	public static final String TABLA_SECCION04_05_DET = "T_05_DIG_AUDITORIA";
	public static final String TABLA_CSVISITA = "T_05_DIG_CSVISITA";
	public static final String TABLA_SALUD = "T_05_DIG_SALUD";
	public static final String TABLA_CSSECCION_08 = "T_05_DIG_CSSECCION_08";
	public static final String TABLA_Individual = "T_05_DIG_CARATULA_INDIVIDUAL";
	public static final String TABLA_CAP04_07 = "T_05_DIG_CAP04_07"; 
	public static final String TABLA_MORTALIDAD ="T_05_DIG_MORTALIDAD";
	public static final String TABLA_DISCAPACIDAD ="T_05_DIG_DISCAPACIDAD";
	
	public static final String VISTA_ESTADOPERSONAS="V_ESTADODEREGISTROS";
	
	public Seccion01DAO(DatabaseHelper dbh) {
		super(dbh);
		// TODO Auto-generated constructor stub
	}
	public static Seccion01DAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion01DAO(dbh);
		}
		return INSTANCE;
	}
	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "HOGAR_ID", TABLA_SECCION01, "ID=?", bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_SECCION01, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, 
				new String[]{"id", "hogar_id","persona_id"}, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public List<Seccion01> getPersonas(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Hogar().getFieldsNames();
		String[] camposFinal = new String[campos.length + 3];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		sbr.append("SELECT h.*").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" h ")
			.append("WHERE h.ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Seccion01.class, camposFinal);
	}
    public List<Seccion01> getPersonasbyId(Integer id) {
		
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion01().getFieldsNames();
		String[] camposFinal = new String[campos.length + 3];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		sbr.append("SELECT h.*").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" h ")
			.append("WHERE h.ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Seccion01.class, camposFinal);
	}
	public List<Seccion01> getPersonasbyHogar(Seccion01 bean, SeccionCapitulo... secciones){
		return getBeans(TABLA_SECCION01,"ID=? AND HOGAR_ID=?",new String[]{bean.id.toString(),bean.hogar_id.toString()},"ID,HOGAR_ID,PERSONA_ID",Seccion01.class,secciones);
	}
	public Seccion01 getPersona2(Integer id, Integer hogar, Integer persona_id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion01().getFieldsNames();
		sbr.append("SELECT p.*").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" p ")
			.append("WHERE p.ID = ? AND p.HOGAR_ID = ? AND p.PERSONA_ID=?");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString(),persona_id.toString());
		return (Seccion01) getBean(query, Seccion01.class, campos);
	}
	
	public Seccion01 getPersona(Integer id, Integer hogar,Integer persona_id, SeccionCapitulo... capitulos) {
		StringBuilder sbr = new StringBuilder();
		Seccion01 tmp = new Seccion01();		
		List<String> listaCampos = new ArrayList<String>();
		for (int i = 0; i < capitulos.length; i++) {
			SeccionCapitulo seccion = capitulos[i];
			listaCampos.addAll(tmp.getFieldMatches(seccion.getSeccion(),
					seccion.getSubseccion(), seccion.getInicio(),
					seccion.getFin(), seccion.getSubPregI(),
					seccion.getSubPregF()));
			if (seccion.getCampos() != null) {
				for (int j = 0; j < seccion.getCampos().length; j++) {
					listaCampos.add(seccion.getCampos()[j]);
				}
			}
		}
		String[] campos = listaCampos.toArray(new String[listaCampos.size()]);
		String[] camposFinal = new String[campos.length + 1];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-1] = "c1p202";		
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(CuestionarioDAO.TABLA_HOGAR).append(" h ")
			.append("LEFT JOIN ").append(TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? AND PERSONA_ID=?")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString(),persona_id.toString());
		return (Seccion01) getBean(query, Seccion01.class, camposFinal);
	}
	
	public Integer MaximoMiembrosdelHogar(Seccion01 bean)
	{
	SQLiteDatabase dbTX = dbh.getWritableDatabase();
	StringBuilder query = new StringBuilder();
	Integer nrodeorden=-1;
	query.append("SELECT MAX(PERSONA_ID) AS ORDEN ")
		 .append(" FROM ").append(TABLA_SECCION01)
	 .append(" WHERE ID=? AND HOGAR_ID=?");
	cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString()});
	if(cursor.moveToNext())
	{nrodeorden= Util.getInt(getString("ORDEN"));
	}
	cursor.close();
	cursor=null;
	SQLiteDatabase.releaseMemory();
	return nrodeorden;
		
	}
	public void ReordenarNumerodeOrdenMiembrosdelHogar(Seccion01 bean){
		Integer orden=MaximoMiembrosdelHogar(bean);
		Integer id_nuevo=bean.persona_id+1;
		if(bean.persona_id<orden)
		{
			for (int i = bean.persona_id; i < orden; i++) {
					if(ModificarNumerodeOrden(bean.id,bean.hogar_id,id_nuevo,i))
						id_nuevo++;
			}
		}
		getInformantedelHogar(bean.id,bean.hogar_id);
	}
	public void getInformantedelHogar(Integer id, Integer hogar_id){
		Integer persona=-1;
		StringBuilder sb = new StringBuilder();
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		sb.append("SELECT PERSONA_ID ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append("  ")
			.append("WHERE ID=? AND HOGAR_ID=? AND IFNULL(QHINFO,0)=1 ");
			cursor = dbTX.rawQuery(sb.toString(),new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){
			persona= getInt("PERSONA_ID");
			ContentValues valores = new ContentValues();
			valores.put("PERSONA_INFORMANTE_ID", persona);
			
			ContentValues valores1 = new ContentValues();
			valores1.put("PERSONA_ID", persona);
			dbTX.update(TABLA_HOGAR, valores, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
			dbTX.update(TABLA_SECCION04_05, valores1, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
		}
	}
	public boolean ModificarNumerodeOrden(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID", persona_id_anterior);
		valores.put("QH01", persona_id_anterior);
		
		ContentValues valores2 = new ContentValues();
		valores2.put("PERSONA_ID", persona_id_anterior);
		ContentValues valores3 = new ContentValues();
		valores3.put("PERSONA_ID_ORDEN", persona_id_anterior);
		
		ContentValues valores4 = new ContentValues();
		valores4.put("PERSONA_ID_NINIO", persona_id_anterior);
		
		if (dbTX.update(
				TABLA_SECCION01,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()}) == 1) {
			dbTX.update(
					TABLA_MORTALIDAD,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()});
			
			dbTX.update(
					TABLA_SECCION03,
					valores3,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_SECCION04_05_DET,
					valores3,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()});
			dbTX.update(
						TABLA_SECCION04,
						valores3,
						"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",
						new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CSSECCION08,
					valores4,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()});
			
			dbTX.update(
					TABLA_CALENDARIO_TRAMO,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			
			dbTX.update(
					TABLA_CISECCION_10_01,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_03,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_04,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_09,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_08,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_05,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_05_07,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04B2,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
//			dbTX.update(
//					TABLA_CISECCION_DIT,
//					valores2,
//					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
//					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			
			dbTX.update(
					TABLA_CISECCION_DIT_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			
			dbTX.update(
					TABLA_CISECCION_04B_TARJETA,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04B,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04A,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			
			dbTX.update(
				TABLA_DISCAPACIDAD,
				valores2,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
	
			
			dbTX.update(
					TABLA_CISECCION_02T,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			
		  dbTX.update(
					TABLA_CISECCION_01_03,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_CSVISITA,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_SALUD,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_CAP04_07,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_Individual,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_CALENDARIO,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_CALENDARIO04,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
		  dbTX.update(
				  TABLA_CALENDARIO_TRAMO4,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
		  
			return true;
		} else {
			return false;
		}
	}
	public boolean borrarPersonaCuandoCambiaSexo(Seccion01 bean){
		SQLiteDatabase dbTX=null;
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
			boolean 
			flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION04_05_DET, 	"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_SECCION04_05, 		"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_Individual,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO_TRAMO,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO_TRAMO4,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_01,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_02,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_03,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_04,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_09,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_08,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_05,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_05_07,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B2,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
//			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT_02,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B_TARJETA,	"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04A,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_DISCAPACIDAD,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_02T,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_02,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_01_03,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());

			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
		return true;
	}
	
	public boolean BorrarPersona(Seccion01 bean, SQLiteDatabase dbTX)
	{
	
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
			boolean 
			flag = borrar(dbTX,CuestionarioDAO.TABLA_SECCION04_05_DET, 		"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", 	bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_SECCION04_05, 			"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", 	bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_SECCION03,      		"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",	bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_SECCION01,      		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 	 	bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CSVISITA,       		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_SALUD,          		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CAP04_07,       		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CSSECCION_08 ,  		"ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?",	bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString()); 
			flag = borrar(dbTX,CuestionarioDAO.TABLA_Individual,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_01_03, 		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 	    bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO04,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO_TRAMO,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO_TRAMO4,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_01,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_02,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_03,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_04,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_09,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_08,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_05,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_05_07,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B2,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
//			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT_02,		"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B_TARJETA,	"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04A,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_DISCAPACIDAD,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_02T,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_02,			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",			bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
		  	flag = borrar(dbTX, CuestionarioDAO.TABLA_MORTALIDAD, 			"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", 		bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());			
			/*ELIMINACION DE LOS DEMAS*/
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_04,      "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_03,      "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_10_02,      "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CALENDARIO_TRAMO,     "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B_TARJETA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04A,        "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_04B,        "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_DISCAPACIDAD,         "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NINIO_ID=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
//			flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT,        "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NRO_ORDEN_NINIO=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_DIT_02,     "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND NRO_ORDEN_NINIO=(SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	flag = borrar(dbTX,CuestionarioDAO.TABLA_CISECCION_02,         "ID=? AND HOGAR_ID=? AND PERSONA_ID=(SELECT PERSONA_ID FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+") AND QI212=   (SELECT QI212 FROM T_05_DIG_CISECCION_02 WHERE ID="+bean.id+" AND HOGAR_ID="+bean.hogar_id+" AND QI219="+bean.persona_id+")", bean.id.toString(), bean.hogar_id.toString());
		  	
			

			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
			ReordenarNumerodeOrdenMiembrosdelHogar(bean);
		return flag;
		
	}
	public boolean BorrarPersonaCSSeccion8(Seccion01 bean){
		this.borrar(TABLA_CSSECCION08, "ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?", new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
		return true;
	}
	
	public boolean UpdateInformante(Integer id,Integer hogar_id)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();


	
	ContentValues valores = new ContentValues();
	valores.put("QHINFO", "0");
	if (dbTX.update(
			TABLA_SECCION01,
			valores,
			"ID=? AND HOGAR_ID=?",
			new String[] { id.toString(), hogar_id.toString() }) == 1) {
		return true;
	} else {
		return false;
	}
	}
	
	public List<Seccion01> getPersonasSeccion04(Integer id,Integer hogar_id,SeccionCapitulo... capitulos) {
		StringBuilder sbr = new StringBuilder();
		Seccion01 tmp = new Seccion01();	
		List<String> listaCampos = new ArrayList<String>();
		for (int i = 0; i < capitulos.length; i++) {
			SeccionCapitulo seccion = capitulos[i];
			listaCampos.addAll(tmp.getFieldMatches(seccion.getSeccion(),
					seccion.getSubseccion(), seccion.getInicio(),
					seccion.getFin(), seccion.getSubPregI(),
					seccion.getSubPregF()));
			if (seccion.getCampos() != null) {
				for (int j = 0; j < seccion.getCampos().length; j++) {
					listaCampos.add(seccion.getCampos()[j]);
				}
			}
		}
		String[] campos = listaCampos.toArray(new String[listaCampos.size()]);
		String[] camposFinal = new String[campos.length + 6];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-1] = "qh06";
		camposFinal[camposFinal.length-2] = "qh7mm";
		camposFinal[camposFinal.length-3] = "qh7dd";
		camposFinal[camposFinal.length-4] = "qh07";
		camposFinal[camposFinal.length-5] = "qh02_1";
		camposFinal[camposFinal.length-6] = "persona_id";
		
		sbr.append("SELECT PERSONA_ID,QH02_1,QH07,QH7DD,QH7MM,QH06").append(" ")
			.append("FROM ").append(CuestionarioDAO.TABLA_SECCION01)
			.append(" WHERE ID=? AND Hogar_ID=? AND ((QH06='2' AND (QH07>=15 and QH07<=49)) OR (QH07<6))");
		Query query = new Query(sbr.toString(),id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class,camposFinal);
	}

	public List<Seccion01> getSeccion1Beneficiados(Integer pregunta_id, Integer id,Integer hogar_id) {
		String[] camposSeccion1= new Seccion01().getFieldsNames();
		String[] camposSeccion1Final = new String[camposSeccion1.length + 2];
		camposSeccion1Final = Arrays.copyOf(camposSeccion1, camposSeccion1Final.length);
		camposSeccion1Final[camposSeccion1Final.length - 2] = "qhs3_1a";
		camposSeccion1Final[camposSeccion1Final.length - 1] = "qhs3_1m";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT a.*,")
			    .append("c.qhs3_1a,c.qhs3_1m")
				.append(" ").append("FROM ").append(TABLA_SECCION01).append(" a")
				.append(" ").append("inner join ").append(TABLA_SECCION03)
				.append(" c on a.id=c.id and a.hogar_id=c.hogar_id and a.persona_id=c.persona_id_orden and c.pregunta_id=?")
				.append(" where a.id=? and a.hogar_id=? ")
				.append("order by a.persona_id");
		Query query = new Query(sb.toString(),pregunta_id.toString(),id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, camposSeccion1Final);
	}
	
	public List<Seccion01> getListadoPregunta92(Integer id, Integer hogar_id,Integer pregunta)
	{
		String[] camposSeccion1= new Seccion01().getFieldsNames();
		String[] camposSeccion1Final = new String[camposSeccion1.length + 4];
		camposSeccion1Final = Arrays.copyOf(camposSeccion1, camposSeccion1Final.length);
		
		camposSeccion1Final[camposSeccion1Final.length - 4] = "qh02_1";
		camposSeccion1Final[camposSeccion1Final.length - 3] = "persona_id";
		camposSeccion1Final[camposSeccion1Final.length - 2] = "qhs3_1a";
		camposSeccion1Final[camposSeccion1Final.length - 1] = "qhs3_1m";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT A.QH02_1,A.PERSONA_ID,C.QHS3_1A,C.QHS3_1M ").
			append(" FROM T_05_DIG_SECCION01 A ").
			append(" INNER JOIN T_05_DIG_SECCION03 C ").
			append(" ON A.ID=C.ID AND A.HOGAR_ID=C.HOGAR_ID ").
			append(" WHERE A.ID=? AND A.HOGAR_ID=? AND C.PREGUNTA_ID=?");
		
		Query query = new Query(sb.toString(),id.toString(),hogar_id.toString(),pregunta.toString());
		
		
		List<Seccion01> detalles = new ArrayList<Seccion01>();
		detalles =getBeans(query, Seccion01.class, camposSeccion1Final);
	return detalles;

	}
	public Integer getSexobyPersonaId(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer sexo=-1;
		query.append("SELECT IFNULL(QH06,0) AS SEXO ")
			 .append(" FROM ").append(TABLA_SECCION01)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QH03 IN (1)");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext())
		{sexo= Util.getInt(getString("SEXO"));
		
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return sexo;
	}
	public Integer getExisteEsposooEsposa(Integer id,Integer hogar_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("SELECT PERSONA_ID ")
			 .append(" FROM ").append(TABLA_SECCION01)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND QH03 IN (2)");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext())
		{cantidad= Util.getInt(getString("PERSONA_ID"));
		
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	public Integer getSexoDelaEsposaOEsposo(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("SELECT IFNULL(QH06,-1) AS CANTIDAD ")
			 .append(" FROM ").append(TABLA_SECCION01)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext())
		{cantidad= Util.getInt(getString("CANTIDAD"));
		
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	public boolean getExisteEsposos(Integer id,Integer hogar_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(2)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext())
		{
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==1;
	}

	public Integer getPersonaIddeEsposo(Integer id,Integer hogar_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer personaid=-1;
		query.append("select PERSONA_ID as Persona ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(2)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext())
		{
			personaid= Util.getInt(getString("Persona"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return personaid;
	}
	
	public Integer getRelacionConJefe(Integer id, Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		boolean existe=false;
		Integer cantidad=-1;
		query.append("select QH03 as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QH03 in(2)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext())
		{
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	public List<Seccion01> getMadres(Integer id, Integer hogar_id, Integer persona_id,Integer parentesco, Integer edadPersona) {
			String condicion="";
			if(!Util.esDiferente(parentesco,2,4,5,6,7,8,9,10,11,12)){
			condicion=" AND PERSONA_ID NOT IN("+persona_id+","+1+")";
			}
			else{
				condicion=" AND PERSONA_ID NOT IN ("+persona_id+")";
			}
			edadPersona=edadPersona+12;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
				.append("FROM ").append(TABLA_SECCION01).append(" ")
				.append(" WHERE ID = ? AND HOGAR_ID = ? AND QH06=2 AND QH07 BETWEEN "+edadPersona+" AND 65").append(condicion);
				Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
				return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
	}
	public boolean getHijoJefa(Integer id,Integer hogar_id, Integer sexo)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select QH03 as Existe")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID = ? AND HOGAR_ID = ? AND QH03=1 AND QH06=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),sexo.toString()});
		if(cursor.moveToNext()){		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==1;
	}
	
	
	public List<Seccion01> getPadres(Integer id, Integer hogar_id, Integer persona_id, Integer parentesco,Integer edadPersona) {
		String condicion="";
		if(!Util.esDiferente(parentesco,2,4,5,6,7,8,9,10,11,12)){
			condicion=" AND PERSONA_ID NOT IN("+persona_id+","+1+")";
			}
			else{
				condicion=" AND PERSONA_ID NOT IN ("+persona_id+")";
			}
		edadPersona=edadPersona+12;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append("WHERE ID = ? AND HOGAR_ID = ? AND QH06=1 AND QH07 BETWEEN "+edadPersona+" AND 97 ").append(condicion);
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
    }
	public List<Seccion01> getResponsableSalud(Integer id,Integer hogar_id)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append("WHERE ID = ? AND HOGAR_ID = ? AND QH07 BETWEEN 15 AND 97").append(" ");
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
	}
	
    public List<Seccion01> getPersonaResponsable(Integer id, Integer hogar_id,Integer persona_id) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append("WHERE ID = ? AND HOGAR_ID = ? AND PERSONA_ID NOT IN(").append(persona_id).append(") AND (QH07 BETWEEN 18 AND 99)").append(" ");
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
    }
	public List<Seccion01> getPosiblesInformantesCuestionarioSalud(Integer id, Integer hogar_id)
	{
		StringBuilder sb = new StringBuilder();
	sb.append("SELECT PERSONA_ID,QH02_1,QH02_2,QH7DD,QH7MM ").append(" ")
	.append("FROM ").append(TABLA_SECCION01).append(" ")
	.append("WHERE ID = ? AND HOGAR_ID = ? AND (QH07 BETWEEN 15 AND 97) AND QH7MM NOT IN ('98') ").append(" ");
	Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
	return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH7DD","QH7MM");
		
	}
	public List<Seccion01> getPosiblesInformantesCuestionarioSaludPordefecto(Integer id, Integer hogar_id)
	{
		StringBuilder sb = new StringBuilder();
	sb.append("SELECT PERSONA_ID,QH02_1,QH02_2,QH7DD,QH7MM ").append(" ")
	.append("FROM ").append(TABLA_SECCION01).append(" ")
	.append("WHERE ID = ? AND HOGAR_ID = ? AND (QH07 BETWEEN 15 AND 97) AND QH7MM IN ('98') ").append(" ");
	Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
	return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH7DD","QH7MM");
		
	}
	public Seccion01 ListadoInformantedeSaludMenordeEdad(Integer id, Integer hogar_id)
	{
		StringBuilder sb = new StringBuilder();
	sb.append("SELECT PERSONA_ID,QH02_1,QH02_2,MAX(QH07)").append(" ")
	.append("FROM ").append(TABLA_SECCION01).append(" ")
	.append("WHERE ID = ? AND HOGAR_ID = ? ").append(" ");
	Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
	return (Seccion01) getBean(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
		
	}
	public List<Seccion01> getBeca18(Integer id,Integer hogar_id,Integer persona_id_orden, Integer edadmin,Integer edadmax,Integer pregunta_id)
	{	
		String condicion="",condicion2="",condicion3="", condicion4="";
		if(pregunta_id==App.PENSION65){
			condicion2 = " AND QH04 IN (1) ";
		}
		List<Seccion01> personas= new ArrayList<Seccion01>();
		List<Seccion01> personasaretornar= new ArrayList<Seccion01>();
		Seccion01 persona= new Seccion01();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ?  AND QH07 BETWEEN ").append(edadmin).append(" AND ").append(edadmax)
		.append(condicion)
		.append(condicion2)
		.append(condicion3)
		.append(condicion4)
		.append(" AND PERSONA_ID NOT IN (SELECT PERSONA_ID_ORDEN ").append(" FROM ")
		.append(CuestionarioDAO.TABLA_SECCION03).append(" WHERE ID=? AND HOGAR_ID=? AND PREGUNTA_ID=?").append(")");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),id.toString(),hogar_id.toString(),pregunta_id.toString());
		personasaretornar = getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
		if(persona_id_orden!=-1)
		{	
			persona= getPersona(id, hogar_id, persona_id_orden);
			personas.add(persona);
			personas.addAll(personasaretornar);
		}
		else{
			personas.addAll(personasaretornar);
		}
		return personas;
	}
	public List<Seccion01> getCunamas(Integer id,Integer hogar_id,Integer persona_id_orden,Integer pregunta_id)
	{	
		String condicion="",condicion2="",condicion3="", condicion4="";
//		if(pregunta_id==App.CUNAMAS_100B){
//			condicion2 = " AND QH04 IN (1) ";
//		}
		List<Seccion01> personas= new ArrayList<Seccion01>();
		List<Seccion01> personasaretornar= new ArrayList<Seccion01>();
		Seccion01 persona= new Seccion01();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ? AND ( (QH06=2  AND QH07 BETWEEN ").append(App.EDADMINIMACUNAMASMEF100B).append(" AND ").append(App.EDADMAXIMACUNAMASMEF100B).append(" )")
		.append(" OR (QH07 BETWEEN ").append(App.EDADMINIMACUNAMASNINIO100B).append(" AND ").append(App.EDADMAXIMACUNAMASNINIO100B).append(" )")
		.append(condicion)
		.append(condicion2)
		.append(condicion3)
		.append(condicion4)
		.append(" ) AND PERSONA_ID NOT IN (SELECT PERSONA_ID_ORDEN ").append(" FROM ")
		.append(CuestionarioDAO.TABLA_SECCION03).append(" WHERE ID=? AND HOGAR_ID=? AND PREGUNTA_ID=?").append(")");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),id.toString(),hogar_id.toString(),pregunta_id.toString());
		personasaretornar = getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
		if(persona_id_orden!=-1)
		{	
			persona= getPersona(id, hogar_id, persona_id_orden);
			personas.add(persona);
			personas.addAll(personasaretornar);
		}
		else{
			personas.addAll(personasaretornar);
		}
		return personas;
	}
	public List<Seccion01> getCunamasvalidacion(Integer id,Integer hogar_id,Integer pregunta_id)
	{	
		String condicion="",condicion2="",condicion3="", condicion4="";
		/*if(pregunta_id==App.CUNAMAS_100B){
			condicion2 = " AND QH04 IN (1) ";
		}*/
		List<Seccion01> personas= new ArrayList<Seccion01>();
		List<Seccion01> personasaretornar= new ArrayList<Seccion01>();
		Seccion01 persona= new Seccion01();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ? AND ( (QH06=2  AND QH07 BETWEEN ").append(App.EDADMINIMACUNAMASMEF100B).append(" AND ").append(App.EDADMAXIMACUNAMASMEF100B).append(" )")
		.append(" OR (QH07 BETWEEN ").append(App.EDADMINIMACUNAMASNINIO100B).append(" AND ").append(App.EDADMAXIMACUNAMASNINIO100B).append(" )")
		.append(condicion2)
		.append(" ) ");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		personasaretornar = getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");

		return personasaretornar;
	}
	public List<Seccion01> getPosiblesBeneficiados(Integer id,Integer hogar_id,Integer persona_id_orden, Integer edadmin,Integer edadmax,Integer pregunta_id){	
		String condicion="",condicion2="";
		if(pregunta_id==App.QALIWARMA){
			condicion = " AND QH18N IN (0,1) AND QH21A IN (1)";
		}
		if(pregunta_id==App.PENSION65){
			condicion2 = " AND QH04 IN (1) ";
		}
		List<Seccion01> personas= new ArrayList<Seccion01>();
		List<Seccion01> personasaretornar= new ArrayList<Seccion01>();
		Seccion01 persona= new Seccion01();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ? AND QH07 BETWEEN ").append(edadmin).append(" AND ").append(edadmax)
		.append(condicion).append(condicion2);
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		personasaretornar = getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
		if(persona_id_orden!=-1){	
			persona= getPersona(id, hogar_id, persona_id_orden);
			personas.add(persona);
			personas.addAll(personasaretornar);
		}
		else{
			personas.addAll(personasaretornar);
		}
		return personas;
	}	
	public Seccion01 getPersonanformanteSalud(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones){
		Seccion01 bean = (Seccion01) getBean(TABLA_SECCION01, "ID=?AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(),hogar_id.toString(),persona_id.toString()}, Seccion01.class, secciones);
		return bean;

	}
	public Seccion01 getPersona(Integer id,Integer hogar_id,Integer persona_id){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * ").append(" ")
		.append(" FROM ").append(TABLA_SECCION01).append("")
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),persona_id.toString());
		return (Seccion01) getBean(query, Seccion01.class, "ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2", "QH07","QH06","QH14","QH15N","QH15Y","QH15G");
	}
	
	public Seccion01 getPersonaEdad(Integer id,Integer hogar_id,Integer persona_id){	
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ID,HOGAR_ID,PERSONA_ID,QH07,QH13,QHINFO,QH02_1,QH15N,QH15Y,QH7MM,QH7DD").append(" ")
		.append(" FROM ").append(TABLA_SECCION01).append("")
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),persona_id.toString());
		return (Seccion01) getBean(query, Seccion01.class, "ID","HOGAR_ID","PERSONA_ID", "QH07","QH13","QHINFO","QH02_1","QH15N","QH15Y","QH7MM","QH7DD");
	}
	
	public Seccion01 getPersonaEliminarSeccion3(Integer id,Integer hogar_id,Integer persona_id){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ID,HOGAR_ID,PERSONA_ID,QH04,QH07,QH13,QH18N,QH21A").append(" ")
		.append(" FROM ").append(TABLA_SECCION01).append("")
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),persona_id.toString());
		return (Seccion01) getBean(query, Seccion01.class, "ID","HOGAR_ID","PERSONA_ID","QH04", "QH07", "QH13", "QH18N", "QH21A");
	}
	
	public Seccion01 getPersonaInformante(Integer id,Integer hogar_id,Integer qhinfo){
	StringBuilder sb = new StringBuilder();
	sb.append("SELECT ID,HOGAR_ID,PERSONA_ID,QH02_1,QH02_2,QH04,QH03").append(" ")
	.append(" FROM ").append(TABLA_SECCION01).append("")
	.append(" WHERE ID=? AND HOGAR_ID=? AND QHINFO=? ");
	Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),qhinfo.toString());
	return (Seccion01) getBean(query, Seccion01.class, "ID","HOGAR_ID","PERSONA_ID", "QH02_1", "QH02_2","QH04","QH03");
		
	}
	public boolean ModificarInformanteSH04(Seccion01 persona){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores= new ContentValues();
		valores.put("PERSONA_ID",persona.persona_id);
		if(dbTX.update(TABLA_SECCION04_05,valores, "ID=? AND HOGAR_ID=?", new String[]{persona.id.toString(), persona.hogar_id.toString()})==1){
			return true;
		}
		else
			return false;
	}
	
	
	public Integer CantidaddeMef(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select COUNT(PERSONA_ID) cantidad ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID = ? AND HOGAR_ID = ? AND QH06=2 AND QH07 BETWEEN 15 AND 49 ");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){		
			cantidad= Util.getInt(getString("cantidad"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	public List<Seccion01> getMef(Integer id, Integer hogar_id){
		StringBuilder sb = new StringBuilder();
		sb.append("select * ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH07 BETWEEN 15 AND 49 AND QH06=2  ORDER BY PERSONA_ID ASC");
		Query query = new Query(sb.toString(), id.toString(), hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH07","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z");
		
	}
	public List<Seccion01> getMujerSeleccionable(Integer id, Integer hogar_id,Integer edadminima,Integer edadmaxima){
		StringBuilder sb = new StringBuilder();
		sb.append("select * ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH07 BETWEEN ").append(edadminima)
		.append(" AND ").append(edadmaxima).append(" AND QH06=2  ORDER BY PERSONA_ID ASC");
		Query query = new Query(sb.toString(), id.toString(), hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH07","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z");
		
	}
	public List<Seccion01> getNiniosPorRangodeEdad(Integer id, Integer hogar_id,Integer edadmin,Integer edadmax){
		StringBuilder sb = new StringBuilder();
		sb.append("select * ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH07 BETWEEN ").append(edadmin).append(" AND ").append(edadmax).append(" ORDER BY PERSONA_ID ASC");
		Query query = new Query(sb.toString(), id.toString(), hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH07","QH11_A","QH11_B","QH11_C","QH11_D","QH11_E","QH11_Y","QH11_Z","QH15Y","QH15N","QH15G","QH14");
		
	}
	public boolean getCantidadNiniosPorRangodeEdad(Integer id, Integer hogar_id,Integer edadmin,Integer edadmax){
		int total = count(
				"PERSONA_ID",
				TABLA_SECCION01,
				"ID = ? AND HOGAR_ID = ? AND QH07 BETWEEN "+edadmin+" AND "+edadmax+"",
				id.toString(), hogar_id.toString());
		return total>0;
		
	}
	public boolean getCantidadMef(Integer id, Integer hogar_id,Integer edadmin,Integer edadmax){
		int total = count(
				"PERSONA_ID",
				TABLA_SECCION01,
				"ID = ? AND HOGAR_ID = ? AND QH06=2 AND QH07 BETWEEN "+edadmin+" AND "+edadmax+"",
				id.toString(), hogar_id.toString());
		return total>0;
		
	}
	public Integer TotalDePersonas(Integer id,Integer hogar_id){
		int total = count(
				"PERSONA_ID",
				TABLA_SECCION01,
				"ID = ? AND HOGAR_ID = ?",
				id.toString(), hogar_id.toString());
		return total;
	}
	public boolean getExisteSoloEsposa(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(2)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==1;
	}
	
	public boolean getExisteSoloPadres(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(6)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==2;
	}
	
	public Integer getEsParentescoId(Integer id,Integer hogar_id,Integer persona_id,Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select PERSONA_ID as ID ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND PERSONA_ID=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			cantidad= Util.getInt(getString("ID"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	
	public boolean getExisteSoloSuegros(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(7)");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==2;
	}
	
	
	public Integer getPersonaIdByParentescoSexo(Integer id,Integer hogar_id, Integer parentesco, Integer sexo){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer personaid=-1;
		query.append("select PERSONA_ID as Persona ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND QH06=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString(),sexo.toString()});
		if(cursor.moveToNext())
		{
			personaid= Util.getInt(getString("Persona"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return personaid;
	}
	public boolean getPersonaIdByParentesco(Integer id,Integer hogar_id, Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer personaid=-1;
		query.append("select PERSONA_ID as Persona ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND QH06=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString()});
		if(cursor.moveToNext())
		{
			personaid= Util.getInt(getString("Persona"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return personaid==1;
	}
	
	public boolean getExisteRelacionconJefe(Integer id,Integer hogar_id, Integer parentesco, Integer sexo){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND QH06=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(), parentesco.toString(), sexo.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==1;
	}
	
	public Integer getEdadByparentescoysexo(Integer id,Integer hogar_id,Integer parentesco,Integer sexo){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select QH07 as EDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND QH06=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(), parentesco.toString(), sexo.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("EDAD"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	public Integer getEdadByPersonaId(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select QH07 as EDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(), persona_id.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("EDAD"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad;
	}
	
	public Integer getEdadHijo_aMayor(Integer id,Integer hogar_id,Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		if(ExisteParentesco(id, hogar_id, parentesco))
		{query.append("select MAX(QH07) as EDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString()});
		if(cursor.moveToNext())	{		
			
			cantidad= Util.getInt(getString("EDAD"));
			
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		}
		return cantidad;
	}
	public boolean ExisteAlgunPadreConEdad(Integer id, Integer hogar_id, Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select COUNT(QH07) as CANTIDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=? AND QH07 IS NOT NULL");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("CANTIDAD"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad>0;
	}
	
	public boolean ExisteParentesco(Integer id,Integer hogar_id,Integer parentesco)	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select COUNT(QH07) as CANTIDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("CANTIDAD"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad>0;
	}
	public Integer getEdadMinimaPadreMenor(Integer id,Integer hogar_id,Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		if(ExisteAlgunPadreConEdad(id,hogar_id,parentesco))
		{
		query.append("select MIN(QH07) as EDAD")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),parentesco.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("EDAD"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		}
		return cantidad;
	}
	
	public boolean getCuentaRelacionconJefe(Integer id,Integer hogar_id, Integer parentesco){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(), parentesco.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==2;
	}	

	public boolean getExisteMadre(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=-1;
		query.append("select count(QH03) as Existe ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH03 in(6) AND QH06=2");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext())	{		
			cantidad= Util.getInt(getString("Existe"));	
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad==1;
	}
	
	public List<Seccion01> ListadoReporte(Integer id,Integer hogar_id,Integer edadmin,Integer edadmax,String sexo){	
		StringBuilder sb = new StringBuilder();
		sb.append("select * ")
		.append(" FROM ").append(TABLA_SECCION01)
		.append(" WHERE ID=? AND HOGAR_ID=? AND QH07 BETWEEN ").append(edadmin).append(" AND ").append(edadmax).append(" QH06 in(").append(sexo).append(")");
		Query query = new Query(sb.toString(), id.toString(), hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2","QH07");
	}
	public boolean RegistroDePersonaCompletada(Integer id,Integer hogar_id,Integer persona_id){	
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT ESTADO ")
			.append("FROM V_REGISTROCOMPLETO ")
			.append("WHERE ID=? AND HOGAR_ID = ? AND PERSONA_ID = ? ");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString(), persona_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("ESTADO"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}
	public boolean TodoLosMiembrosDelhogarCompletados(Integer id,Integer hogar_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append("FROM V_REGISTROCOMPLETO ")
			.append("WHERE ID=? AND HOGAR_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}
	public boolean TodasLasMefCompletadas(Integer id,Integer hogar_id)
	{
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append(" FROM ").append(VISTA_MEFCOMPLETADO)
			.append(" WHERE ID=? AND HOGAR_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}
	public boolean TodosLosMiembrosDelHogarDiscapacidadCompletada(Integer id, Integer hogar_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append("FROM V_DISCAPACIDAD_COMPLETADA ")
			.append("WHERE ID=? AND HOGAR_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}
	public Integer EdadMaximaDeunMiembrodelHogar(Integer id,Integer hogar_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT MAX(QH07) as EDAD")
			.append(" FROM ").append(TABLA_SECCION01)
			.append(" WHERE ID=? AND HOGAR_ID = ? AND QH04 IN (1) ");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("EDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total;
	}
	public List<Seccion01> getEspososparalaMef(Integer id,Integer hogar_id){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ? AND QH06 IN (1) AND QH07 BETWEEN ").append(" 15 ").append(" AND ").append(" 95 ");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
	}
	public Integer getTotalDehijosDelaMef(Integer id,Integer hogar_id,Integer persona_id){
		return this.count("QH23", TABLA_SECCION01,"ID=? AND HOGAR_ID=? AND QH23=? AND QH04 IN (1) AND QH05 IN (1) AND QH03 IN (3) AND QH06 IN(1) " , new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	}
	public Integer getTotalDehijasDelaMef(Integer id,Integer hogar_id,Integer persona_id){
		return this.count("QH23", TABLA_SECCION01,"ID=? AND HOGAR_ID=? AND QH23=? AND QH04 IN (1) AND QH05 IN (1) AND QH03 IN (3) AND QH06 IN(2) " , new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	}
	
	/****CABE MENCIONAR QUE ESTE METODO SOLO FUNCIONA CUANDO SE ELIMINA A LA PERSONA DESDE LA SECCION 01***/
	//ASI MISMO  LA ACTUALIZACION SOLO LO REALIZA A LAS SIGUIENTES TABLAS T_05_DIG_HOGAR:  NRO_INFO_S  Y T_05_DIG_CSSECCION_08 : PERSONA_ID
	public boolean ModificarEnlaceInformanteDeSalud(Integer id,Integer hogar_id,Integer informantesalud_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID", informantesalud_id);

		ContentValues valores1 = new ContentValues();
		valores1.put("NRO_INFO_S", informantesalud_id);

		
		dbTX.update(
				TABLA_CSSECCION08,
				valores,
				"ID=? AND HOGAR_ID=?",
				new String[] { id.toString(), hogar_id.toString() });
		dbTX.update(
				TABLA_HOGAR,
				valores1,
				"ID=? AND HOGAR_ID=?",
				new String[] { id.toString(), hogar_id.toString() });
		return true;
	}

	public List<Seccion01> getresResponsablesPreCensos(Integer id, Integer hogar_id) {
//		String condicion="";
//		if(!Util.esDiferente(parentesco,2,4,5,6,7,8,9,10,11,12)){
//		condicion=" AND PERSONA_ID NOT IN("+persona_id+","+1+")";
//		}
//		else{
//			condicion=" AND PERSONA_ID NOT IN ("+persona_id+")";
//		}
//		edadPersona=edadPersona+12;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append(" WHERE ID = ? AND HOGAR_ID = ? AND QH07>=12 ");
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
		return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
}
	public List<Seccion01> getCovid(Integer id,Integer hogar_id,Integer persona_id_orden, Integer edadmin,Integer edadmax,Integer pregunta_id){	
		String condicion="",condicion2="",condicion3="", condicion4="";
		if(pregunta_id==App.PERSONAS_COVID){
			condicion2 = " AND QH04 IN (1) ";
		}
		List<Seccion01> personas= new ArrayList<Seccion01>();
		List<Seccion01> personasaretornar= new ArrayList<Seccion01>();
		Seccion01 persona= new Seccion01();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
		.append("FROM ").append(TABLA_SECCION01).append(" ")
		.append("WHERE ID = ? AND HOGAR_ID = ?  AND QH07 BETWEEN ").append(edadmin).append(" AND ").append(edadmax)
		.append(condicion)
		.append(condicion2)
		.append(condicion3)
		.append(condicion4)
		.append(" AND PERSONA_ID NOT IN (SELECT PERSONA_ID_ORDEN ").append(" FROM ")
		.append(CuestionarioDAO.TABLA_SECCION03).append(" WHERE ID=? AND HOGAR_ID=? AND PREGUNTA_ID=?").append(")");
		Query query = new Query(sb.toString(), id.toString(),hogar_id.toString(),id.toString(),hogar_id.toString(),pregunta_id.toString());
		personasaretornar = getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
		if(persona_id_orden!=-1)
		{	
			persona= getPersona(id, hogar_id, persona_id_orden);
			personas.add(persona);
			personas.addAll(personasaretornar);
		}
		else{
			personas.addAll(personasaretornar);
		}
		return personas;
	}
	
	
	public List<Seccion01> getInformateVacunas(Integer id, Integer hogar_id) {
		String condicion="";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append(" WHERE ID = ? AND HOGAR_ID = ? AND QH07>=12 ").append("");
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
			return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
	}
	
	public List<Seccion01> getInformateMigracion(Integer id, Integer hogar_id) {
		String condicion="";
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT PERSONA_ID,QH02_1,QH02_2 ").append(" ")
			.append("FROM ").append(TABLA_SECCION01).append(" ")
			.append(" WHERE ID = ? AND HOGAR_ID = ? ").append("");
			Query query = new Query(sb.toString(), id.toString(),hogar_id.toString());
			return getBeans(query, Seccion01.class, "PERSONA_ID", "QH02_1", "QH02_2");
	}
	
//	public boolean TodoLosMiembrosDelhogarVacunasCompletados(Integer id,Integer hogar_id){
//		SQLiteDatabase dbr = dbh.getWritableDatabase();
//		StringBuilder query = new StringBuilder();
//		int total = 0;
//		query.append("SELECT SUM(CASE WHEN QH248 IN(1,2,3,4) THEN 0 ELSE 1 END) as CANTIDAD ")
//			.append("FROM ").append(TABLA_SECCION01).append(" ")
//			.append("WHERE ID=? AND HOGAR_ID = ?");
//				cursor = dbr.rawQuery(query.toString(),
//			new String[] { id.toString(), hogar_id.toString()});
//			if (cursor.moveToNext()) {
//				total = Util.getInt(getString("CANTIDAD"));
//			}
//			cursor.close();
//			cursor = null;
//			SQLiteDatabase.releaseMemory();
//			return total == 0;
//	}
	
	public boolean TodoLosMiembrosDelhogarVacunasCompletados(Integer id,Integer hogar_id){
		Integer total =0;
		total=this.sum("ESTADO2",VISTA_ESTADOPERSONAS , "ID=? AND HOGAR_ID=? ", id.toString(),hogar_id.toString());
		return total==0;
	}
}

