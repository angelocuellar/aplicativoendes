package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.dao.xml.XMLDataObject;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_COL04;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CIHERMANOS;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_04;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.CISECCION_05;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.model.CISECCION_10_03;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Endes_Resultado_vivienda;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.IMIGRACION;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.MIGRACION;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.ReportDatesofBirth;
import gob.inei.endes2024.model.ReporteHMS;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CuestionarioDAO extends SQLiteDAO {
	public static CuestionarioDAO INSTANCE;
	public static final String TABLA_VIVIENDA = "T_MARCO";
	public static final String TABLA_CARATULA = "T_05_DIG_CARATULA";
	public static final String TABLA_VISITA = "T_05_DIG_VISITA";
	public static final String TABLA_VISITA_VIV = "T_05_DIG_VISITA_VIV";
	
	public static final String TABLA_SECCION01 = "T_05_DIG_SECCION01";
	public static final String TABLA_HOGAR = "T_05_DIG_HOGAR";
	public static final String TABLA_INCENTIVOS = "T_05_DIG_INCENTIVOS";
	public static final String TABLA_SECCION03 = "T_05_DIG_SECCION03";	
	public static final String TABLA_SECCION04_05 = "T_05_DIG_SECCCION04_05";
	public static final String TABLA_SECCION04_05_DET = "T_05_DIG_AUDITORIA";
	
	
	public static final String VISTA_ESTADOPERSONAS="V_ESTADODEREGISTROS";
	public static final String CUESTIONARIO_HOGARCOMPLETADA = "V_HOGAR_COMPLETADO";
	public static final String SECCION3_COMPLETADA = "V_SECCION3_COMPLETADO";

	public static final String CUESTIONARIO_SALUDCOMPLETADA = "V_SALUD_COMPLETADO";
	public static final String CUESTIONARIO_SALUD01_07COMPLETADA = "V_SALUD_01_07_COMPLETADO";

	public static final String TABLA_SALUD = "T_05_DIG_SALUD";

	
	public static final String TABLA_CSVISITA = "T_05_DIG_CSVISITA"; 

	public static final String TABLA_CAP04_07 = "T_05_DIG_CAP04_07"; 
	public static final String TABLA_CSSECCION_08 = "T_05_DIG_CSSECCION_08";
		
	public static final String VISTA_SALUD = "V_SALUD_INFORMANTE";	
	public static final String VISTA_INFORMENTE="V_INFORMANTE";	
	public static final String VISTA_DISCAPACIDADHOGAR="V_ESTADO_DISCAPACIDAD";
	public static final String V_SALUD_SECCION8 ="V_SECCION08SALUD";
	public static final String V_SALUD_SECCION8P840 ="V_SECCION08SALUDP840";
	public static final String VISTA_ESTADO_HOGAR="V_ESTADO_HOGAR";
	public static final String TABLA_Individual = "T_05_DIG_CARATULA_INDIVIDUAL";
	public static final String V_INDIVIDUAL ="V_INDIVIDUAL";
	public static final String TABLA_CISECCION_01_03="T_05_DIG_CISECCION_01_03";
	public static final String V_SECCION2_COMPLETADO ="V_SECCION2_COMPLETADO";
	public static final String TABLA_CISECCION04="T_05_DIG_CISECCION_04";
	public static final String V_SECCION4A_COMPLETADO ="V_SECCION4A_COMPLETADO";
	public static final String V_SECCION4B_COMPLETADO ="V_SECCION4B_COMPLETADO";
//	public static final String V_SECCION4DIT_COMPLETADO ="V_SECCION4DIT_COMPLETADO";
	public static final String V_SECCION4DIT_02_COMPLETADO ="V_SECCION4DIT_02_COMPLETADO";
	public static final String TABLA_CISECCION_04A="T_05_DIG_CISECCION_04A";
	public static final String TABLA_CISECCION_04B ="T_05_DIG_CISECCION_04B";
	public static final String TABLA_CISECCION_04B2 = "T_05_DIG_CISECCION_04B2";
	public static final String TABLA_CISECCION_05_07="T_05_DIG_CISECCION_05_07";
	public static final String TABLA_CISECCION_05= "T_05_DIG_CISECCION_05";
	public static final String TABLA_CISECCION_08= "T_05_DIG_CISECCION_08";
	public static final String TABLA_CISECCION_10_01= "T_05_DIG_CISECCION_10_01";
	public static final String TABLA_CISECCION_10_02= "T_05_DIG_CISECCION_10_02";
	public static final String TABLA_CISECCION_10_03= "T_05_DIG_CISECCION_10_03";
	public static final String TABLA_CISECCION_10_04= "T_05_DIG_CISECCION_10_04";
	public static final String TABLA_CIVISITA= "T_05_DIG_CARATULA_INDIVIDUAL";
	public static final String TABLA_CALENDARIO="T_05_DIG_CICALENDARIO_COL01_03";
	public static final String TABLA_CALENDARIO04="T_05_DIG_CICALENDARIO_COL04";
	public static final String TABLA_CISECCION_09 = "T_05_DIG_CISECCION_09";
	public static final String TABLA_CISECCION_DIT="T_05_DIG_CISECCION_04DIT";
	public static final String TABLA_CISECCION_DIT_02="T_05_DIG_CISECCION_04DIT_02";
	public static final String TABLA_CISECCION_04B_TARJETA ="T_05_DIG_CISECCION_04B_TARJETA";
	public static final String TABLA_CISECCION_02="T_05_DIG_CISECCION_02";
	public static final String TABLA_CISECCION_02T="T_05_DIG_CISECCION_02T";
	public static final String TABLA_MORTALIDAD="T_05_DIG_MORTALIDAD";
	
	public static final String TABLA_CALENDARIO_TRAMO="T_05_DIG_CICALENDARIO_TRAMO_COL01_03";
	public static final String TABLA_CALENDARIO_TRAMO4="T_05_DIG_CICALENDARIO_TRAMO_COL4";
	public static final String V_DESARROLLOINFANTIL="V_DESARROLLOINFANTIL";
	public static final String V_DESARROLLOINFANTIL_02="V_DESARROLLOINFANTIL_02";
	public static final String V_CALENDARIO="V_CALENDARIO";
	
	public static final String VISTA_LISTADOGEMELOS="V_LISTADODEGEMELOS";
	
	/******vistas para validacion de exportacion */
	public static final String V_RESULTADO_ULTIMAVISITAHOGAR ="RESULTADO_ULTIMAVISITAHOGAR";
	public static final String V_VISITA_COMPLETADA="V_VISITA_COMPLETADA";
	public static final String V_HOGAR_COMPLETADO="HOGAR_COMPLETADO";
	public static final String V_COMPLETADOS_SECCION04_05="COMPLETADOS_SECCION04_05";
	public static final String V_VISITASALUD_COMPLETADA ="V_VISITASALUD_COMPLETADA";
	public static final String V_CS_COMPLETADO="V_CS_COMPLETADO";
	public static final String V_CS_COMPLETADO_PARA_EXPORTACION="V_CARATULA_SALUD_COMPLETADA";
	public static final String V_VIVIENDAGPS="V_VIVIENDAGPS";
	public static final String V_SECCION08COMPLETADO="V_SECCION08COMPLETADO";
	/*** fin de declaracion de las vistas******/
	
	//vistas para completado cuestionario individual//
	public static final String V_SECCION1Y3_COMPLETADO = "V_SECCION1Y3_COMPLETADO";
	public static final String V_SECCION4B2_COMPLETADO = "V_SECCION4B2_COMPLETADO";
	public static final String V_SECCION5Y7_COMPLETADO = "V_SECCION5Y7_COMPLETADO";
	public static final String V_SECCION08COMPLETADO_INDIVIDUAL="V_SECCION8_INDIVIDUALCOMPLETADO";
	public static final String V_SECCION10_01_COMPLETADO ="V_SECCION10_01_COMPLETADO";
	public static final String V_SECCION5_COMPLETADO ="V_SECCION5_COMPLETADO";
	public static final String V_SECCION9_COMPLETADO = "V_SECCION9_COMPLETADO";
	//fin vistas para cuestionario indivisual//
	
//	public static final String VISITA_INDIVIDUALCOMPLETO ="VISITA_INDIVIDUALCOMPLETO";
	public static final String V_MORTALIDAD_HOGAR="V_COMPLETADO_MORTALIDAD_HOGAR";
	
	public static final String VISTA_FECHA_NACIMIENTO="V_FECHASNACIMIENTO_TODOLOSCUESTIONARIOS";
	
	public static final String VISTA_LISTA_HERMANOS_S9="LISTA_HERMANOS_S9";
	
	public static final String VISTA_HOGARPREGUNTAQH224="V_RESULTADOPREGUNTAQH224";
	
	///agregado 07/07/2017 fcp
	public static final String VISTA_COMPLETADO_VIVIENDA_FINAL="V_COMPLETADO_VIVIENDA_FINAL";
	public static final String VISTA_V_HORA_ULTIMA_VISITA="V_HORA_ULTIMA_VISITA";
	
	public static final String VISTA_DISCAPACIDAD_INDIVIDUAL="V_DISCAPACIDAD_INDIVIDUAL";
	
	public static final String TABLA_DISCAPACIDAD="T_05_DIG_DISCAPACIDAD";
	
	public static final String VISTA_ESTADOPERSONAS_PRECENSOS="V_ESTADODEREGISTROS_PRECENSOS";
	
	public static final String TABLA_USUARIO="T_USUARIO";
	public static final String TABLA_SEGMENTACION="T_SEGMENTACION";
	public static final String TABLA_USUARIO_RUTA="T_USUARIO_RUTA";
	
	
	public static final String TABLA_MIGRACION="T_05_DIG_MIGRACION";
	public static final String TABLA_IMIGRACION="T_05_DIG_IMIGRACION";		
	public static final String V_MIGRACION_HOGAR="V_COMPLETADO_MIGRACION_HOGAR";
	public static final String V_IMIGRACION_HOGAR="V_COMPLETADO_IMIGRACION_HOGAR";
	
	
	public Seccion01Service seccion01;
	private CuestionarioDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}

	public static CuestionarioDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new CuestionarioDAO(dbh);
		}
		return INSTANCE;
	}

	// MODEL : Caratula
	public Caratula getCaratula(Integer id) {
		Caratula bean = (Caratula) getBean(TABLA_CARATULA, "ID=?",
				Caratula.class, id.toString());
		return bean;
	}

	public List<Caratula> getCaratulas(Integer id) {
		return getBeans(TABLA_CARATULA, "ID=?", Caratula.class,
				id.toString());
	}

	public Caratula getCaratula(Integer id, SeccionCapitulo... secciones) {
		Caratula bean = (Caratula) getBean(TABLA_CARATULA, "ID=?",
				new String[] { id.toString() }, Caratula.class, secciones);
		return bean;
	}

	public Caratula getCaratula(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		Caratula bean = (Caratula) getBean(TABLA_CARATULA, "ID=?",
				new String[] { id.toString() }, Caratula.class, secciones);
		return bean;
	}

	public List<Caratula> getCaratulas(Integer id,
			SeccionCapitulo... secciones) {
		return getBeans(TABLA_CARATULA, "ID=?",
				new String[] { id.toString() }, "ID", Caratula.class,
				secciones);
	}

	public boolean saveOrUpdate(Caratula bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CARATULA, "ID=?", bean,
				new String[] { "id" }, secciones);
	}

	public boolean saveOrUpdate(Caratula bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_CARATULA, "ID=?", bean,
				new String[] { "id" }, -1, -1, campos);
	}

	public List<Caratula> getCaratulaList(Integer id) {
		return getBeans(TABLA_CARATULA, "ID=?", "id", Caratula.class,
				id.toString());
	}

	// MODEL : Hogar
	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_HOGAR, "ID=? AND Hogar_ID=?",
				bean, new String[] { "id", "hogar_id" }, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(Hogar bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_HOGAR, "ID=? AND Hogar_ID=?", bean,
				new String[] { "id", "hogar_id" }, secciones);
	}
	public boolean saveOrUpdate(Incentivos bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_INCENTIVOS, "ID=? AND Hogar_ID=?", bean,
				new String[] { "id", "hogar_id" }, secciones);
	}
	
	public boolean saveOrUpdate(MORTALIDAD bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_MORTALIDAD, "ID=? AND Hogar_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?", bean,
				new String[] { "id", "hogar_id","persona_id","nro_orden_id" }, secciones);
	}
	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.hogar_id == null) {
			bean.hogar_id = nextID(dbTX, "HOGAR_ID", TABLA_HOGAR, "ID=?",
					bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_HOGAR,
				"ID=? AND HOGAR_ID=?", bean, new String[] { "id", "hogar_id" },
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public boolean saveOrUpdate(Caratula bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.id == null) {
			bean.id = nextID(dbTX, "ID", TABLA_CARATULA, "ID=?",
					bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_CARATULA,
				"ID=?", bean, new String[] { "id"},
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public boolean saveOrUpdate(Salud bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "HOGAR_ID", TABLA_SALUD, "ID=?",
					bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_SALUD,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[] { "id","hogar_id","persona_id"},
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public boolean saveOrUpdate(CAP04_07 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "HOGAR_ID", TABLA_CAP04_07, "ID=?",
					bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_CAP04_07,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[] { "id","hogar_id","persona_id"},
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public Hogar getHogar(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		Hogar bean = (Hogar) getBean(TABLA_HOGAR, "ID=? AND Hogar_ID=?",
				new String[] { id.toString(), hogar_id.toString() },
				Hogar.class, secciones);
		return bean;
	}
	public Integer getViolenHogar(Integer id, Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT QHVIOLEN AS ORDEN ")
			 .append(" FROM ").append(TABLA_HOGAR)
		 .append(" WHERE ID=? AND HOGAR_ID=?");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){
			nrodeorden= Util.getInt(getString("ORDEN"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return nrodeorden;
	}
	
	public Integer getnumOrdenMaxVisitaCI(Integer id, Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT MAX(NRO_VISITA) AS ORDEN ")
			 .append(" FROM ").append(TABLA_Individual)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			nrodeorden= Util.getInt(getString("ORDEN"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return nrodeorden;
	}
	
	public Hogar getHogarConInformante(Integer id, Integer hogar_id,SeccionCapitulo... secciones){		
		Hogar bean = (Hogar) getBean(VISTA_INFORMENTE, "ID=? AND Hogar_ID=?",
		new String[] { id.toString(), hogar_id.toString() },
		Hogar.class, secciones);
		return bean;
	}
	public Incentivos getIncentivos(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return (Incentivos)getBean(TABLA_INCENTIVOS,"ID=? AND HOGAR_ID=?",
		new String[]{id.toString(),hogar_id.toString()},
		Incentivos.class,secciones);
	}

	public List<Hogar> getHogars(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		return getBeans(TABLA_HOGAR, "ID=? AND Hogar_ID=?",
				new String[] { id.toString(), hogar_id.toString() },
				"ID,HOGAR_ID", Hogar.class, secciones);
	}
	
	public List<Hogar> getHogarList(Integer id) {
		return getBeans(TABLA_HOGAR, "ID=?", "id", Hogar.class, id.toString());
	}
	
	public List<Hogar> getHogares(Integer id) {
//		return getBeans(VISTA_ESTADO_HOGAR, "ID=?"
//				, new String[]{id.toString()},"ID",Hogar.class,secciones);
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Hogar().getFieldsNames();
		String[] camposFinal = new String[campos.length + 11];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length - 11] = "qh103";
		camposFinal[camposFinal.length - 10] = "qh101";
		camposFinal[camposFinal.length - 9] = "qh224";
		camposFinal[camposFinal.length - 8] = "qh93";
		camposFinal[camposFinal.length - 7] = "qh91";
		camposFinal[camposFinal.length - 6] = "qh40";
		camposFinal[camposFinal.length - 5] = "qhbebe";
		camposFinal[camposFinal.length - 4] = "estado";
		camposFinal[camposFinal.length - 3] = "qh02_3";
		camposFinal[camposFinal.length - 2] = "qh02_2";
		camposFinal[camposFinal.length - 1] = "qh02_1";
		sbr.append("SELECT h.id,h.hogar_id,h.r_final,p.QH02_1, p.QH02_2,h.qhbebe,h.qh40,h.qh91,h.qh93,h.qh101,h.qh103,h.qh224,h.nro_info_s, p.QH02_3,CASE WHEN ((SELECT QHVRESUL FROM T_05_DIG_VISITA WHERE ID=h.ID AND HOGAR_ID=h.HOGAR_ID AND IFNULL(QHVRESUL,0)=1)==1) THEN 0 ELSE NULL END AS ESTADO")
				.append(" ")
				.append("FROM ")
				.append(TABLA_HOGAR)
				.append(" h ")
				.append("LEFT JOIN ")
				.append(CuestionarioDAO.TABLA_SECCION01)
				.append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.QH01 = 1 ")
				.append("WHERE h.ID = ? AND h.HOGAR_ID NOT IN(0)").append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString());
		// return getBeans(TABLA, "ID = ?", "HOGAR_ID", Hogar.class,
		// id.toString());
		return getBeans(query, Hogar.class, camposFinal);
	}
	public List<Hogar> getHogaresById(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Hogar().getFieldsNames();
		sbr.append("SELECT * ")
				.append(" ")
				.append("FROM ")
				.append(TABLA_HOGAR)
			    .append(" WHERE ID = ? ").append(" ORDER BY HOGAR_ID ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Hogar.class, campos);
	}
	public List<Marco> getViviendas(Integer id,String conglomerado) {
			StringBuilder sb = new StringBuilder();
			String[] campos = new Marco().getFieldsNames();
			String[] camposMarcoFinal = new String[campos.length + 4];
			camposMarcoFinal = Arrays.copyOf(campos, camposMarcoFinal.length);
			camposMarcoFinal[camposMarcoFinal.length - 4] = "resviv";
			camposMarcoFinal[camposMarcoFinal.length - 3] = "departamento";
			camposMarcoFinal[camposMarcoFinal.length - 2] = "provincia";
			camposMarcoFinal[camposMarcoFinal.length - 1] = "distrito";
			sb.append("SELECT DISTINCT ").append(getCamposSelect("m", campos)).append(",c.resviv")
			.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO, ")
			.append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as USUARIO_ASIG ")
			.append(" ").append("FROM ").append(TABLA_VIVIENDA).append(" m")
			.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
			.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
			.append("LEFT JOIN ")
			.append(CuestionarioDAO.TABLA_CARATULA)
			.append(" c ON m.ID = c.ID ")
			.append("WHERE m.ID NOT IN (").append(id).append(") AND m.CONGLOMERADO=? AND m.ASIGNADO IN(1) AND m.ID NOT IN (SELECT ID FROM ").append(CuestionarioDAO.TABLA_HOGAR).append(" ) ")
			.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
			Query query = new Query(sb.toString(), new String[]{conglomerado});
			
			return getBeans(query, Marco.class,camposMarcoFinal);
	    }

	// MODEL : Seccion04_05
	public boolean saveOrUpdate(Seccion04_05 bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_SECCION04_05,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?", bean, new String[] {
						"id", "hogar_id", "persona_id","persona_id_orden" }, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(Auditoria bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_SECCION04_05_DET,
				"ID=? AND Hogar_ID=? AND PERSONA_ID_ORDEN=?", bean, new String[] {
						"id", "hogar_id","persona_id_orden" }, -1, -1, campos);
	}

	public boolean saveOrUpdate(Seccion04_05 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_SECCION04_05,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?", bean, new String[] {
						"id", "hogar_id", "persona_id","persona_id_orden"}, secciones);
	}
	//INDIVIDUAL
	public boolean saveOrUpdate(CISECCION_01_03 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_01_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
	public CISECCION_01_03 getCISECCION_01_03(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_01_03)getBean(TABLA_CISECCION_01_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_01_03.class,secciones);
	}
	public boolean saveOrUpdate(CISECCION_04 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
	public boolean saveOrUpdate(CISECCION_04A individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_04A,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",individual,new String[]{"id","hogar_id","persona_id","ninio_id"}, secciones);
	}

//	public boolean saveOrUpdate(C2SECCION_04B individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return this.saveOrUpdate(TABLA_CISECCION_04B,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
//	}
//	public C2SECCION_04B getCISECCION_04B(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return (C2SECCION_04B)getBean(TABLA_CISECCION_04A,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},C2SECCION_04B.class,secciones);
//	}
//	public boolean saveOrUpdate(CISECCION_04B_2 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return this.saveOrUpdate(TABLA_CISECCION_04B_2,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
//	}
//	public CISECCION_04B_2 getCISECCION_04B_2(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return (CISECCION_04B_2)getBean(TABLA_CISECCION_04B_2,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_04B_2.class,secciones);
//	}
	
	//

	public CISECCION_05_07 getCISECCION_05_07(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_05_07)getBean(TABLA_CISECCION_05_07,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_05_07.class,secciones);
	}
//	public boolean saveOrUpdate(CISECCION_08_09 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return this.saveOrUpdate(TABLA_CISECCION_08_09,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
//	}
//	public CISECCION_08_09 getCISECCION_08_09(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return (CISECCION_08_09)getBean(TABLA_CISECCION_08_09,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_08_09.class,secciones);
//	}
	
	public boolean saveOrUpdate(CISECCION_10_01 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_10_01,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
//	public CISECCION_10_01 getCISECCION_10_01(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return (CISECCION_10_01)getBean(TABLA_CISECCION_10_01,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_08_09.class,secciones);
//	}
	public boolean saveOrUpdate(CISECCION_10_02 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_10_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",individual,new String[]{"id","hogar_id","persona_id","ninio_id"}, secciones);
	}
//	public CISECCION_10_02 getCISECCION_10_02(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return (CISECCION_10_02)getBean(TABLA_CISECCION_10_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_08_09.class,secciones);
//	}
//	public boolean saveOrUpdate(CIVISITA individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return this.saveOrUpdate(TABLA_CIVISITA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
//	}
	
//	getCISECCION_04B
	public Seccion04_05 getSeccion04_05(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		Seccion04_05 bean = (Seccion04_05) getBean(
				TABLA_SECCION04_05,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString() }, Seccion04_05.class, secciones);
		return bean;
	}

	public List<Seccion04_05> getSeccion04_05s(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_SECCION04_05,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString() }, "ID,HOGAR_ID,PERSONA_ID",
				Seccion04_05.class, secciones);
	}

	public List<Seccion04_05> getSeccion04_05List(Integer id) {
		return getBeans(TABLA_SECCION04_05, "ID=?", "id", Seccion04_05.class,
				id.toString());
	}

	// MODEL : Seccion01
	public boolean saveOrUpdate(Seccion03 bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_SECCION03,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?", bean, new String[] {
						"id", "hogar_id", "persona_id","persona_id_orden" }, -1, -1, campos);
	}
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_SECCION01,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id" }, -1, -1, campos);
	}
	public boolean saveOrUpdate(MORTALIDAD bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_MORTALIDAD,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id", "nro_orden_id" }, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "PERSONA_ID", TABLA_SECCION01,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),
					bean.hogar_id.toString(),bean.persona_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_SECCION01,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id" }, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public boolean saveOrUpdate(MORTALIDAD bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "NRO_ORDEN_ID", TABLA_MORTALIDAD,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID", bean.id.toString(),
					bean.hogar_id.toString(),bean.persona_id.toString(),bean.nro_orden_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_MORTALIDAD,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id","nro_orden_id" }, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_SECCION01,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id" }, secciones);
	}

	public Seccion01 getSeccion01(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		Seccion01 bean = (Seccion01) getBean(
				TABLA_SECCION01,"ID=? AND HOGAR_ID=? AND  PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString() }, Seccion01.class, secciones);
		return bean;
	}	
	
	public List<Seccion01> getSeccion01s(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_SECCION01,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString() }, "ID,HOGAR_ID,PERSONA_ID",
				Seccion01.class, secciones);
	}
	
	public List<Seccion01> getSeccion01PosiblesBeneficiarios(Integer id, Integer hogar_id, Integer pregunta_id,
			Integer edad_min,Integer edad_max,SeccionCapitulo... secciones) {
		String condicion=" AND ";
		
		if(pregunta_id==92){
			condicion = " AND QH04='1' AND QH16='1' AND ";	
		}
		
		return getBeans(
				TABLA_SECCION01,
				"ID=? AND Hogar_ID=? AND (QH07 BETWEEN '"+edad_min+"' AND '"+edad_max+"')" +condicion+
				"(cast(id as varchar)||cast(hogar_id as varchar)||cast(persona_id as varchar)) not in " + 
				"(select cast(id as varchar)||cast(hogar_id as varchar)||cast(PERSONA_ID_ORDEN as varchar) "+
				"from t_05_dig_seccion03  where pregunta_id='"+pregunta_id+"')",
				new String[] { id.toString(), hogar_id.toString()}, "ID,HOGAR_ID",
				Seccion01.class, secciones);
	}

	public List<Seccion01> getSeccion01List(Integer id) {
		return getBeans(TABLA_SECCION01, "HOGAR_ID=?", "hogar_id", Seccion01.class,
				id.toString());
	}
	public List<Seccion01> getSeccion01ListbyMarcoyHogar(Integer id,Integer hogar_id,SeccionCapitulo... secciones) {
		return getBeans(VISTA_ESTADOPERSONAS, "ID=? AND HOGAR_ID=? "
				, new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}

	public List<Seccion01> getSeccion01ListDiscapacidad(Integer id,Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getBeans(VISTA_DISCAPACIDADHOGAR, "ID=? AND HOGAR_ID=? "
				, new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}
	public List<Seccion01> getDiscapacidadParaIndividual(Integer id, Integer hogar_id,Integer persona_id,Integer cuestionario_id, SeccionCapitulo...secciones){
//		Log.e("id",""+id);
//		Log.e("hogar_id",""+hogar_id);
//		Log.e("persona_id",""+persona_id);
		return getBeans(VISTA_DISCAPACIDAD_INDIVIDUAL, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}
	public List<Seccion01> getListadoSeccion8Salud(Integer id,Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getBeans(TABLA_SECCION01, "ID=? AND HOGAR_ID=? AND QH07 BETWEEN 0 AND 11 "
				, new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}
	public List<CSSECCION_08> getListadoSeccion8(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return getBeans(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",CSSECCION_08.class,secciones);
	}
	// Model: MEF
	public List<Seccion01> getListadoMEF(Integer id,Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getBeans(TABLA_SECCION01, "ID=? AND HOGAR_ID=? AND QH07 BETWEEN "+App.HogEdadMefMin+" AND "+ App.HogEdadMefMax+" AND QH06=2 "
				, new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}
	public boolean getListadoMEF(Integer id,Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT COUNT(*) as CANTIDAD ")
			.append("FROM ")
			.append(TABLA_CISECCION_01_03)
			.append(" WHERE ID=? AND HOGAR_ID = ? AND PERSONA_ID = ?");
			cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString(), persona_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total > 0;
	}
	
	public boolean BorrarMEF(Individual bean)
	{
		return borrar(
				TABLA_Individual,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				bean.id.toString(), bean.hogar_id.toString(),
				bean.persona_id.toString());
	}
	///Eliminar MEF
	public boolean ModificarNumerodeOrden(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores2 = new ContentValues();
		valores2.put("PERSONA_ID", persona_id_anterior);
		if (dbTX.update(
				TABLA_CALENDARIO,
				valores2,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id_nuevo.toString()}) == 1) {
			//
			dbTX.update(
					TABLA_CALENDARIO04,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CALENDARIO_TRAMO,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CALENDARIO_TRAMO4,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_01,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_03,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_10_04,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_09,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_08,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_05,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_05_07,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04B2,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
//			dbTX.update(
//					TABLA_CISECCION_DIT,
//					valores2,
//					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
//					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_DIT_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04B_TARJETA,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04B,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_04A,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_DISCAPACIDAD,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_02T,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			dbTX.update(
					TABLA_CISECCION_01_03,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					new String[] { id.toString(), hogar_id.toString() ,persona_id_nuevo.toString()});
			return true;
		} else {
			return false;
		}
	}
	public Integer MaximoMiembroIndividual(Seccion01 bean)
	{
	SQLiteDatabase dbTX = dbh.getWritableDatabase();
	StringBuilder query = new StringBuilder();
	Integer nrodeorden=-1;
	query.append("SELECT MAX(PERSONA_ID) AS ORDEN ")
		 .append(" FROM ").append(TABLA_CISECCION_01_03)
	 .append(" WHERE ID=? AND HOGAR_ID=?");
	cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString()});
	if(cursor.moveToNext())
	{nrodeorden= Util.getInt(getString("ORDEN"));
	}
	cursor.close();
	cursor=null;
	SQLiteDatabase.releaseMemory();
	return nrodeorden;
		
	}
	public void ReordenarNumerodeOrdenMiembrosdelHogar(Seccion01 bean)
	{	Integer orden=MaximoMiembroIndividual(bean);
		
		Integer id_nuevo=bean.persona_id+1;
		if(bean.persona_id<orden)
		{
			for (int i = bean.persona_id; i < orden; i++) {
					if(ModificarNumerodeOrden(bean.id,bean.hogar_id,id_nuevo,i))
						id_nuevo++;
			}
		}
	}
	
	public boolean EliminarMEF(Seccion01 bean)
	{
		boolean flag = borrar(CuestionarioDAO.TABLA_Individual, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CALENDARIO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CALENDARIO04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_10_01,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_10_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_10_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_10_04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_09,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_05,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_05_07,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_04B2,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
//		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_DIT_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_04B,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_04A,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_DISCAPACIDAD,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_02T,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		if(flag) {flag = borrar(CuestionarioDAO.TABLA_CISECCION_01_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(), bean.hogar_id.toString(),bean.persona_id.toString());}
		//ReordenarNumerodeOrdenMiembrosdelHogar(bean);
		return flag;
	}
	
	// MODEL : Visita
	public boolean saveOrUpdate(Visita bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_VISITA,
				"ID=? AND Hogar_ID=? AND NRO_Visita=?", bean, new String[] {
						"id", "hogar_id", "nro_visita" }, -1, -1, campos);
	}

	public boolean saveOrUpdate(Visita bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VISITA,
				"ID=? AND Hogar_ID=? AND NRO_Visita=?", bean, new String[] {
						"id", "hogar_id", "nro_visita" }, secciones);
	}
	
	public boolean saveOrUpdate(Visita_Viv bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_VISITA_VIV, "ID=?  AND NRO_Visita=?", bean, new String[] {
						"id", "nro_visita" }, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(Incentivos bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_INCENTIVOS, "ID=? AND HOGAR_ID=?", bean, new String[] {
						"id", "hogar_id" }, -1, -1, campos);
	}

	public Visita getVisita(Integer id, Integer hogar_id, Integer nro_visita,SeccionCapitulo... secciones) {
		Visita bean = (Visita) getBean(TABLA_VISITA,"ID=? AND Hogar_ID=? AND NRO_Visita=?",new String[] { id.toString(), hogar_id.toString(),nro_visita.toString() }, Visita.class, secciones);
		return bean;
	}
	
	public Visita_Viv getVisita_Viv(Integer id, Integer nro_visita,SeccionCapitulo... secciones) {
		Visita_Viv bean = (Visita_Viv) getBean(TABLA_VISITA_VIV,"ID=? AND NRO_Visita=?",new String[] { id.toString(), nro_visita.toString() }, Visita_Viv.class, secciones);
		return bean;
	}
	
	public List<Visita> getVisitas(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		return getBeans(TABLA_VISITA,"ID=? AND HOGAR_ID=?",new String[] {id.toString(), hogar_id.toString()}, "id,hogar_id",
				Visita.class, secciones);
	}
	
	public List<Visita_Viv> getVisitas_Viv(Integer id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_VISITA_VIV,"ID=?",new String[] {id.toString()}, "id",Visita_Viv.class, secciones);
	}

	public List<Visita> getVisitaList(Integer id) {
		return getBeans(TABLA_VISITA, "ID=?", "id", Visita.class, id.toString());
	}

	// MODEL : Vivienda
	public Marco getVivienda(Integer id, SeccionCapitulo... secciones) {
		Marco bean = (Marco) getBean(TABLA_VIVIENDA, "ID=?",
				new String[] { id.toString() }, Marco.class, secciones);
		return bean;
	}

	public List<Marco> getViviendas(Integer id, SeccionCapitulo... secciones) {
		return getBeans(TABLA_VIVIENDA, "ID=?", new String[] { id.toString() },
				"ID", Marco.class, secciones);
	}

	public boolean saveOrUpdate(Marco bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VIVIENDA, "ID=?", bean, new String[] {"id"}, secciones);
	}

	public boolean saveOrUpdate(Marco bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_VIVIENDA, "ID=?", bean,
				new String[] { "id" }, -1, -1, campos);
	}

	public List<Marco> getViviendaList(Integer id) {
		return getBeans(TABLA_VIVIENDA, "ID=?", "id", Marco.class,
				id.toString());
	}
	public boolean getCuestionarioDelHogarCompletado(Integer id,Integer hogar_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append(CUESTIONARIO_HOGARCOMPLETADA)
		     .append(" WHERE ID=? AND HOGAR_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;
	
	}
	public boolean getSeccion3Completado(Integer id,Integer hogar_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append(SECCION3_COMPLETADA)
		     .append(" WHERE ID=? AND HOGAR_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;	
	}
	public boolean getCuestionarioDelSaludCompletado(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append(CUESTIONARIO_SALUDCOMPLETADA)
		     .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;
	
	}
	public boolean getRegistroBucalCompletado(Integer id,Integer hogar_id)
	{
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append("FROM V_BUCAL_COMPLETADO ")
			.append("WHERE ID=? AND HOGAR_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	
	}
	public boolean getCuestionarioDelSalud01_07Completado(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append(CUESTIONARIO_SALUD01_07COMPLETADA)
		     .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;
	
	}
	public boolean getCuestionarioDelSaludComboCompletado(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append("V_SALUD_COMBO_COMPLETADO")
		     .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;
	
	}
	public boolean getCuestionarioDelSaludVacio(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		int estado=-1;
		query.append("SELECT ESTADO ")
		     .append(" FROM ").append("V_SALUD_VACIO")
		     .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		 cursor =db.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		 if(cursor.moveToNext())
		 {
			 estado = Util.getInt(getString("ESTADO"));
		 }
		 	cursor.close();
			cursor = null;
			db.close();
			SQLiteDatabase.releaseMemory();
		 return estado==0;
	
	}
	public boolean existeGPS(String latitud, String longitud, String omision) {
		SQLiteDatabase db = this.dbh.getReadableDatabase();
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT GPS_LAT AS LAT, GPS_LONG AS LON").append(" ")
				.append("FROM ").append(TABLA_CARATULA).append(" ")
				.append("WHERE GPS_LAT=? AND GPS_LONG=?").append(" ");
		cursor = db.rawQuery(queryBuilder.toString(), new String[] { latitud,
				longitud });
		if (cursor.getCount() == 0) {
			cursor = null;
			db.close();
			return false;
		} else {
			boolean flag = true;
			if (!cursor.moveToNext()) {
				flag = false;
			} else {
				String lat = getString("LAT");
				if (App.GPS_OMISION.equals(lat)) {
					flag = false;
				}
			}
			cursor = null;
			db.close();
			return flag;
		}
	}

	public static class CounterObservable extends Observable {
		private int cantidadInsertado;

		public CounterObservable() {
			this(0);
		}

		public CounterObservable(int cantidadAvanzado) {
			cantidadInsertado = cantidadAvanzado;
		}

		private void insertado(int cantidad) {
			cantidadInsertado += 1;
			setChanged();
			Map<String, Integer> map = new HashMap<String, Integer>();
			map.put("INSERTADO", cantidadInsertado);
			notifyObservers(map);
			
			// cantidadInsertado);
		}
	}

	public boolean save(SQLiteDatabase dbTX, XMLDataObject dataObjects,
			CounterObservable contadorObserver) throws java.sql.SQLException {
		boolean isTX = true;
		boolean flag = true;
		if (dbTX == null) {
			isTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		for (int i = 0; i < dataObjects.getRegistros().size() && flag; i++) {
			if (dataObjects
					.getRegistros()
					.get(i)
					.get(dataObjects.getPkFields().get(
							dataObjects.getPkFields().size() - 1)) == null) {
				int nextID = super.nextID(
						dbTX,
						dataObjects.getPkFields().get(
								dataObjects.getPkFields().size() - 1),
						dataObjects.getTableName(), dataObjects
								.getStringWhereNextID(), dataObjects
								.getWhereValuesNextID(dataObjects
										.getRegistros().get(i)));
				dataObjects
						.getRegistros()
						.get(i)
						.put(dataObjects.getPkFields().get(
								dataObjects.getPkFields().size() - 1), nextID);
			}
			try {
				flag = super.saveOrUpdate(
						dbTX,
						dataObjects.getTableName(),
						dataObjects.getRegistros().get(i),
						dataObjects.getPkFields().toArray(
								new String[dataObjects.getPkFields().size()]));	
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if (contadorObserver != null) {
				contadorObserver.insertado(1);
			}
		}
		if (!isTX) {
			dbTX.close();
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		
		return flag;
	}

	public boolean borrarCuestionario(String tablename,Integer id, SQLiteDatabase dbTX) {
		boolean esTX = true;
		if (dbTX == null) {
			dbTX = this.dbh.getWritableDatabase();
			esTX = false;
		}
		dbTX.delete(tablename, "ID=?", new String[] { id.toString() });
		SQLiteDatabase.releaseMemory();
		if (!esTX) {
			dbTX.close();
		}
		return true;
	}

	public boolean saveOrUpdate(SQLiteDatabase dbTX, XMLDataObject dataObjects,
			CounterObservable contadorObserver) throws java.sql.SQLException {
		boolean isTX = true;
		boolean flag = true;
		if (dbTX == null) {
			isTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		for (int i = 0; i < dataObjects.getRegistros().size() && flag; i++) {
			if (dataObjects
					.getRegistros()
					.get(i)
					.get(dataObjects.getPkFields().get(
							dataObjects.getPkFields().size() - 1)) == null) {
				
				// dataObjects.getTableName() + " pk vacia: " +
				// dataObjects.getPkFields().get(dataObjects.getPkFields().size()-1));
				int nextID = super.nextID(
						dbTX,
						dataObjects.getPkFields().get(
								dataObjects.getPkFields().size() - 1),
						dataObjects.getTableName(), dataObjects
								.getStringWhereNextID(), dataObjects
								.getWhereValuesNextID(dataObjects
										.getRegistros().get(i)));
				dataObjects
						.getRegistros()
						.get(i)
						.put(dataObjects.getPkFields().get(
								dataObjects.getPkFields().size() - 1), nextID);
			}
			flag = super.saveOrUpdate(
					dbTX,
					dataObjects.getTableName(),
					dataObjects.getRegistros().get(i),
					dataObjects.getPkFields().toArray(
							new String[dataObjects.getPkFields().size()]));
			if (contadorObserver != null) {
				contadorObserver.insertado(1);
			}
		}
		if (!isTX) {
			dbTX.close();
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	
//	ADD 07122015
	
	//MODEL :  CAP01_03 
		public boolean saveOrUpdate(Salud bean, SQLiteDatabase dbwTX) throws java.sql.SQLException { 
			String[] campos = bean.getFieldsSaveNames(); 
			return this.saveOrUpdate(dbwTX, TABLA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos); 
		} 
		public boolean saveOrUpdate(Salud bean, SeccionCapitulo... secciones) throws java.sql.SQLException { 
			return this.saveOrUpdate(TABLA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, secciones); 
		} 
		public Salud getCAP01_03(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
			
			Salud bean = (Salud) getBean(VISTA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},Salud.class,secciones); 
			return bean; 
		} 
		public List<Salud> getCAP01_03s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
			return getBeans(TABLA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",Salud.class,secciones); 
		} 
		public List<Salud> getCAP01_03List(Integer id) { 
			return getBeans(TABLA_SALUD, "ID=?","id", Salud.class, id.toString()); 
		} 
		//MODEL :  CAP04_07 
		public boolean saveOrUpdate(CAP04_07 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException { 
			String[] campos = bean.getFieldsSaveNames(); 
			return this.saveOrUpdate(dbwTX, TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos); 
		} 
		public boolean saveOrUpdate(CAP04_07 bean, SeccionCapitulo... secciones) throws java.sql.SQLException { 
			return this.saveOrUpdate(TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, secciones); 
		} 
		public CAP04_07 getCAP04_07(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
			CAP04_07 bean = (CAP04_07) getBean(TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CAP04_07.class,secciones); 
			return bean; 
		} 
		public List<CAP04_07> getCAP04_07s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
			return getBeans(TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CAP04_07.class,secciones); 
		} 
		public List<CAP04_07> getCAP04_07List(Integer id) { 
			return getBeans(TABLA_CAP04_07, "ID=?","id", CAP04_07.class, id.toString()); 
		} 
		//MODEL :  CAP08_09 
		public boolean saveOrUpdate(CSSECCION_08 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException { 
			String[] campos = bean.getFieldsSaveNames(); 
			return this.saveOrUpdate(dbwTX, TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_NINIO=?", bean, new String[]{"id","hogar_id","persona_id","persona_id_ninio"}, -1, -1, campos); 
		}
		//CAP INDIVIDUAL
		public boolean saveOrUpdate(Individual bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_Individual, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean, new String[]{"id","hogar_id","persona_id","nro_visita"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CARATULA_INDIVIDUAL bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_Individual, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean, new String[]{"id","hogar_id","persona_id","nro_visita"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CICALENDARIO_COL01_03 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CICALENDARIO_COL04 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CALENDARIO04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CICALENDARIO_TRAMO_COL4 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_10_01 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_10_01, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_10_02 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_10_03 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_10_03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_10_04 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_10_04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_09 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_09, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_08 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_05 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_05, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PAREJA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_05_07 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_05_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_04B2 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_04B2, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
//		public boolean saveOrUpdate(CISECCION_04DIT bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
//			String[] campos = bean.getFieldsSaveNames();
//			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_DIT, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?", bean, new String[]{"id","hogar_id","persona_id","nro_orden_ninio"}, -1, -1, campos);
//		}
		public boolean saveOrUpdate(CISECCION_04DIT_02 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_DIT_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?", bean, new String[]{"id","hogar_id","persona_id","nro_orden_ninio"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_04B_TARJETA bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND PREGUNTA=? AND INDICE=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_04B bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_04A bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_02T bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_02T, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_02 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		public boolean saveOrUpdate(CISECCION_01_03 bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CISECCION_01_03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean, new String[]{"id","hogar_id","persona_id"}, -1, -1, campos);
		}
		//
		public boolean saveOrUpdate(CSSECCION_08 bean, SeccionCapitulo... secciones) throws java.sql.SQLException {
			return this.saveOrUpdate(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_NINIO=?", bean, new String[]{"id","hogar_id","persona_id","persona_id_ninio"}, secciones);
		}
		
		public boolean Update(CSSECCION_08 bean,SeccionCapitulo... secciones) throws java.sql.SQLException {
			return this.saveOrUpdate(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?",bean, new String[]{"id","hogar_id","persona_id_ninio"}, secciones);
		}
		public CSSECCION_08 getCAP08_09(Integer id,Integer hogar_id,Integer persona_id,Integer persona_id_ninio, SeccionCapitulo... secciones) {
			
			CSSECCION_08 bean = (CSSECCION_08) getBean(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),persona_id_ninio.toString()},CSSECCION_08.class,secciones); 
			return bean; 
		}
		public CSSECCION_08 getCSSeccion08(Integer id,Integer hogar_id,Integer persona_id_ninio, SeccionCapitulo... secciones) {
			return  (CSSECCION_08) getBean(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id_ninio.toString()},CSSECCION_08.class,secciones); 
		} 
		public List<CSSECCION_08> getCAP08_09s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
			return getBeans(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CSSECCION_08.class,secciones); 
		} 
		public List<CSSECCION_08> getCAP08_09List(Integer id) {
			return getBeans(TABLA_CSSECCION_08, "ID=?","id", CSSECCION_08.class, id.toString()); 
		} 
//		getListadoMefById
		public List<Individual> getListadoMefById(Integer id) {
			return getBeans(TABLA_Individual, "ID=?","id", Individual.class, id.toString());
		}
		public List<CARATULA_INDIVIDUAL> getListadoCarMefById(Integer id) {
			return getBeans(TABLA_Individual, "ID=?","id", CARATULA_INDIVIDUAL.class, id.toString());
		}
		public List<CICALENDARIO_COL01_03> getListadoCalen01_03ById(Integer id) {
			return getBeans(TABLA_CALENDARIO, "ID=?","id", CICALENDARIO_COL01_03.class, id.toString());
		}
		public List<CICALENDARIO_COL04> getListadoCalen04ById(Integer id) {
			return getBeans(TABLA_CALENDARIO04, "ID=?","id", CICALENDARIO_COL04.class, id.toString());
		}
		public List<CICALENDARIO_TRAMO_COL01_03> getListadoCalenTramoById(Integer id) {
			return getBeans(TABLA_CALENDARIO_TRAMO, "ID=?","id", CICALENDARIO_TRAMO_COL01_03.class, id.toString());
		}
		public List<CICALENDARIO_TRAMO_COL4> getListadoCalenTramo4ById(Integer id) {
			return getBeans(TABLA_CALENDARIO_TRAMO4, "ID=?","id", CICALENDARIO_TRAMO_COL4.class, id.toString());
		}
		public List<CISECCION_10_01> getListadoCI_10_01ById(Integer id) {
			return getBeans(TABLA_CISECCION_10_01, "ID=?","id", CISECCION_10_01.class, id.toString());
		}
		public List<CISECCION_10_02> getListadoCI_10_02ById(Integer id) {
			return getBeans(TABLA_CISECCION_10_02, "ID=?","id", CISECCION_10_02.class, id.toString());
		}
		public List<CISECCION_10_03> getListadoCI_10_03ById(Integer id) {
			return getBeans(TABLA_CISECCION_10_03, "ID=?","id", CISECCION_10_03.class, id.toString());
		}
		public List<CISECCION_10_04> getListadoCI_10_04ById(Integer id) {
			return getBeans(TABLA_CISECCION_10_04, "ID=?","id", CISECCION_10_04.class, id.toString());
		}
		public List<CISECCION_09> getListadoCI_09ById(Integer id) {
			return getBeans(TABLA_CISECCION_09, "ID=?","id", CISECCION_09.class, id.toString());
		}
		public List<CISECCION_08> getListadoCI_08ById(Integer id) {
			return getBeans(TABLA_CISECCION_08, "ID=?","id", CISECCION_08.class, id.toString());
		}
		public List<CISECCION_05> getListadoCI_05ById(Integer id) {
			return getBeans(TABLA_CISECCION_05, "ID=?","id", CISECCION_05.class, id.toString());
		}
		public List<CISECCION_05_07> getListadoCI_05_07ById(Integer id) {
			return getBeans(TABLA_CISECCION_05_07, "ID=?","id", CISECCION_05_07.class, id.toString());
		}
		public List<CISECCION_04B2> getListadoCI_04B2ById(Integer id) {
			return getBeans(TABLA_CISECCION_04B2, "ID=?","id", CISECCION_04B2.class, id.toString());
		}
//		public List<CISECCION_04DIT> getListadoCI_DitById(Integer id) {
//			return getBeans(TABLA_CISECCION_DIT, "ID=?","id", CISECCION_04DIT.class, id.toString());
//		}
		public List<CISECCION_04DIT_02> getListadoCI_Dit02ById(Integer id) {
			return getBeans(TABLA_CISECCION_DIT_02, "ID=?","id", CISECCION_04DIT_02.class, id.toString());
		}
		
		public List<CISECCION_04B_TARJETA> getListadoCI_04B_TarjetaById(Integer id) {
			return getBeans(TABLA_CISECCION_04B_TARJETA, "ID=?","id", CISECCION_04B_TARJETA.class, id.toString());
		}
		public List<CISECCION_04B> getListadoCI_04BById(Integer id) {
			return getBeans(TABLA_CISECCION_04B, "ID=?","id", CISECCION_04B.class, id.toString());
		}
		public List<CISECCION_04A> getListadoCI_04AById(Integer id) {
			return getBeans(TABLA_CISECCION_04A, "ID=?","id", CISECCION_04A.class, id.toString());
		}
		public List<CISECCION_02T> getListadoCI_02TById(Integer id) {
			return getBeans(TABLA_CISECCION_02T, "ID=?","id", CISECCION_02T.class, id.toString());
		}
		public List<CISECCION_02> getListadoCI_02ById(Integer id) {
			return getBeans(TABLA_CISECCION_02, "ID=?","id", CISECCION_02.class, id.toString());
		}
		public List<CISECCION_01_03> getListadoCI_01_03ById(Integer id) {
			return getBeans(TABLA_CISECCION_01_03, "ID=?","id", CISECCION_01_03.class, id.toString());
		}
		
		// ADD 07122015
		// MODEL : CAPVISITA
		public boolean saveOrUpdate(CSVISITA bean, SQLiteDatabase dbwTX)
				throws java.sql.SQLException {
			String[] campos = bean.getFieldsSaveNames();
			return this.saveOrUpdate(dbwTX, TABLA_CSVISITA,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean,
					new String[] { "id", "hogar_id", "persona_id", "nro_visita" },
					-1, -1, campos);
		}

		public boolean saveOrUpdate(CSVISITA bean, SeccionCapitulo... secciones)
				throws java.sql.SQLException {
			return this.saveOrUpdate(TABLA_CSVISITA,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean,
					new String[] { "id", "hogar_id", "persona_id", "nro_visita" },
					secciones);
		}



		public List<CSVISITA> getCAPVISITAs(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
			return getBeans(
					TABLA_CSVISITA,
					"ID=? AND HOGAR_ID=? ",
					new String[] { id.toString(), hogar_id.toString() },
					"ID,HOGAR_ID", CSVISITA.class, secciones);
		}
		
		public List<CSVISITA> getCAPVISITAs2(Integer id, Integer hogar_id, Integer persona_id,SeccionCapitulo... secciones) {
			return getBeans(
					TABLA_CSVISITA,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
					new String[] { id.toString(), hogar_id.toString(), persona_id.toString()},
					"ID,HOGAR_ID,PERSONA_ID", CSVISITA.class, secciones);
		}

		public List<CSVISITA> getCAPVISITAList(Integer id) {
			return getBeans(TABLA_CSVISITA, "ID=?", "id", CSVISITA.class,
					id.toString());
		}
	public CSVISITA getVisitaSaludCompletada(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return (CSVISITA)getBean(TABLA_CSVISITA,"ID=? AND HOGAR_ID=? AND QSVRESUL IN (1)", new String[]{id.toString(),hogar_id.toString()},CSVISITA.class,secciones);
	}
	public CSVISITA getCAPVISITA(Integer id, Integer hogar_id, SeccionCapitulo... secciones) {
			CSVISITA bean = (CSVISITA) getBean(
				TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=?",
				new String[] { id.toString(), hogar_id.toString()}, CSVISITA.class, secciones);
		return bean;
	}
	public CSVISITA getCAPVISITASALUD(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
		CSVISITA visita=null;
		Integer max=this.max("NRO_VISITA",TABLA_CSVISITA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(max!=0){
		visita = (CSVISITA) getBean(
				TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),max.toString()}, CSVISITA.class, secciones);
		}
		return visita==null?null:visita;
	}
	public boolean getExisteSeccion01_03(Integer id,Integer hogar_id,Integer persona_id){
		Integer dataresultado=-1;
		dataresultado=this.count("QS23",TABLA_SALUD,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QS23 IS NOT NULL",  new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return dataresultado==1;
		
	}
	public boolean getExisteSeccion04_07Salud(Integer id,Integer hogar_id,Integer persona_id){
		Integer data=-1;
		data=this.count("PERSONA_ID", TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QS500 IS NOT NULL", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return data==1;
	}
	public CSVISITA getCAPVISITA(Integer id, Integer hogar_id, Integer nro_visita,Integer persona_id,SeccionCapitulo... secciones) {
		CSVISITA bean = (CSVISITA) getBean(
				TABLA_CSVISITA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),nro_visita.toString() }, CSVISITA.class, secciones);
		return bean;
	}
	public List<CSSECCION_08> getListadoNiniosSeccion08(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
		return getBeans(
				V_SALUD_SECCION8,
				"ID=? AND Hogar_ID=? AND QH07 BETWEEN 0 AND 11 ",
				new String[] { id.toString(), hogar_id.toString() }, 
				               "ID,HOGAR_ID",
				               CSSECCION_08.class, secciones);
	}
	public boolean getExistealMenosUnNinio(Integer id, Integer hogar_id) {
		Integer contador=0;
		contador=this.count("ID",TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=?",new String[]{id.toString(),hogar_id.toString()});
		return contador>0;
	}
	public List<CSSECCION_08> getListadoNiniosSeccion08Preg840(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
		return getBeans(
				V_SALUD_SECCION8P840, " ID=? AND Hogar_ID=? AND QS802D BETWEEN 1 AND 11 AND QS809 IN (1) ",
				new String[] { id.toString(), hogar_id.toString() }, 
				               "ID,HOGAR_ID",
				               CSSECCION_08.class, secciones);
	}
	public boolean getListadoNiniosSeccion08Botones(Integer id,Integer hogar_id){
		/*SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();*/
		Integer total = 0;
		total=this.count("ID", TABLA_CSSECCION_08, "ID=? AND HOGAR_ID = ? AND QS802D BETWEEN 1 AND 11 AND QS809 IN (1)", new String[]{id.toString(),hogar_id.toString()});
		/*query.append("SELECT COUNT(QS802D) as CANTIDAD ")
			.append("FROM ")
			.append(TABLA_CSSECCION_08)
			.append(" WHERE ID=? AND HOGAR_ID = ? AND QS802D BETWEEN 1 AND 11 AND QS809 IN (1) ");
			cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total > 0;*/
		return total>0;
	}
	
	public boolean getRegistroCompletadoSaludSeccion8(Integer id,Integer hogar_id)
	{
	SQLiteDatabase dbr = dbh.getWritableDatabase();
	StringBuilder query = new StringBuilder();
	int total = 0;
	query.append("SELECT SUM(ESTADO) as CANTIDAD ")
		.append("FROM V_SECCION08SALUDCOMPLETADO ")
		.append("WHERE ID=? AND HOGAR_ID = ?");
			cursor = dbr.rawQuery(query.toString(),
		new String[] { id.toString(), hogar_id.toString()});
		if (cursor.moveToNext()) {
			total = Util.getInt(getString("CANTIDAD"));
		}
		cursor.close();
		cursor = null;
		SQLiteDatabase.releaseMemory();
		return total == 0;
	}
	
	//model Individual
	public boolean saveOrUpdate(Individual bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_Individual,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?", bean, new String[] {
						"id", "hogar_id", "persona_id" }, secciones);
	}
	public List<Individual> getListadoIndividual(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
		return getBeans(
				V_INDIVIDUAL,
				"ID=? AND Hogar_ID=? ",
				new String[] { id.toString(), hogar_id.toString() }, 
				               "ID,HOGAR_ID",
				               Individual.class, secciones);
	}
	
	public Individual getListadoIndividualPorPersona(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
		
		return (Individual)getBean(V_INDIVIDUAL,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},Individual.class, secciones);
	}
	
	public List<CSSECCION_08> getNiniosaModificaralBorrarPersona(Integer id, Integer hogar_id, SeccionCapitulo... secciones){
		return getBeans(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",CSSECCION_08.class,secciones);
	}
	
	public List<CSVISITA> getVisitasAModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer pesona_id, SeccionCapitulo...secciones){
		return getBeans(TABLA_CSVISITA,"ID=? AND HOGAR_ID=? AND PERSONA_ID>? ", new String[]{id.toString(),hogar_id.toString(),pesona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CSVISITA.class,secciones);
	}
	public boolean ModificarNumerodeOrdenVisitaSalud(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_CSVISITA,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString() , persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean ModificarNumerodeOrdenVisitaSalud2(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer numero_visita)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_CSVISITA,
				valores,
				"ID=? AND HOGAR_ID=? AND NRO_VISITA=? ",
				new String[] { id.toString(), hogar_id.toString() , numero_visita.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public Salud getSaludAModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return (Salud)getBean(TABLA_SALUD,"ID=? AND HOGAR_ID=? AND PERSONA_ID>? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},Salud.class,secciones);
	}
	public boolean ModificarNumerodeOrdenSalud01_03(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_SALUD,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString() , persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public CAP04_07 getSalud04_07AModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return (CAP04_07)getBean(TABLA_CAP04_07,"ID=? AND HOGAR_ID=? AND PERSONA_ID>? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CAP04_07.class,secciones);
	}
	
	public boolean ModificarNumerodeOrdenSalud04_07(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_CAP04_07,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString() , persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<CSSECCION_08> getInformanteSaludModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getBeans(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID>? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID,PERSONA_ID_NINIO",CSSECCION_08.class,secciones);
	}
	public boolean ModificarNumerodeOrdenInformanteSaludSeccion08(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer persona_id_ninio)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_CSSECCION_08,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_NINIO=? ",
				new String[] { id.toString(), hogar_id.toString() ,persona_id_anterior.toString(),persona_id_ninio.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<CSSECCION_08> getSaludSeccion08AModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer pesona_id, SeccionCapitulo...secciones){
		return getBeans(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO>? ", new String[]{id.toString(),hogar_id.toString(),pesona_id.toString()},"ID,HOGAR_ID,PERSONA_ID_NINIO",CSSECCION_08.class,secciones);
	}
	public boolean ModificarNumerodeOrdenSaludSeccion08(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer informante)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID_NINIO",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_CSSECCION_08,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_NINIO=? ",
				new String[] { id.toString(), hogar_id.toString() ,informante.toString(), persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<Individual> getCaratulaIndividualModificarAlBorrarPersona(Integer id, Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getBeans(TABLA_Individual,"ID=? AND HOGAR_ID=? AND PERSONA_ID>?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString() },"ID,HOGAR_ID,PERSONA_ID",Individual.class,secciones);
	}
	public boolean ModificarNumerodeOrdenCaratulaIndividual(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_Individual,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString(), persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<ReporteHMS> getReporteHMS(String codccpp,Context ctx) {
		List<ReporteHMS> reporte = null;
		List<ReporteHMS> reporteh = null;
		List<ReporteHMS> reportes = null;
		List<ReporteHMS> reportei = null;
		
		StringBuilder sb = new StringBuilder();
		String[] camposFinal = new ReporteHMS().getFieldsNames();
		Query q = new Query();				
//		
	     sb.append(" SELECT distinct CAST(m.NSELV AS INT) as VIVIENDA, h.HOGAR_ID as HOGAR,p.PERSONA_ID as LINEA, ")
		 .append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as ENTREVISTADORA, ")
		 .append(" 'HOGAR' AS SECCION,CASE WHEN (p.QH02_3 is null) THEN p.QH02_1||' '||ifnull(p.QH02_2,'') ELSE (p.QH02_1||' '||ifnull(p.QH02_2,'') ||' '||ifnull(p.QH02_3,'')) end as NOMBRE, ")
		 .append(" CASE WHEN (v.QHVRESUL==1) THEN 'Completa' ")
		 .append(" WHEN (v.QHVRESUL==2) THEN 'Hogar presente pero entrevistado competente ausente' ")
		 .append(" WHEN (v.QHVRESUL==3) THEN 'Hogar ausente'		")
		 .append(" WHEN (v.QHVRESUL==4) THEN 'Aplazada' ")
		 .append(" WHEN (v.QHVRESUL==5) THEN 'Rechazada' ")
		 .append(" WHEN (v.QHVRESUL==6) THEN 'Vivienda desocupada o no es vivienda' ")
		 .append(" WHEN (v.QHVRESUL==7) THEN 'Vivienda destruida' ")
		 .append(" WHEN (v.QHVRESUL==8) THEN 'Vivienda no encontrada' ")
		 .append(" WHEN (v.QHVRESUL==9) THEN 'Otra' ")
		 .append(" ELSE NULL END as RESULTADO, ")
		 .append(" (select Max(v2.NRO_VISITA) from T_05_DIG_VISITA v2 where h.ID = v2.ID AND h.HOGAR_ID = v2.HOGAR_ID) as VISITAS, ")
		 .append(" '' as PESO_TALLA,'' as HEMOG, ")
		 .append(" (select count(*) from T_05_DIG_SECCCION04_05 p1 ")
		 .append(" where p1.ID=h.ID AND p1.HOGAR_ID=h.HOGAR_ID AND p1.QH207=1 AND ")
		 .append(" p1.PERSONA_ID_ORDEN in (select p2.PERSONA_ID from T_05_DIG_SECCION01 p2 where p2.ID=h.ID AND p2.HOGAR_ID=h.HOGAR_ID AND P2.QH07 BETWEEN 0 AND 5)) as PESOTALLA6, ")
		 .append(" (select count(*) from T_05_DIG_SECCCION04_05 p1 where p1.ID=h.ID AND p1.HOGAR_ID=h.HOGAR_ID AND p1.QH213=1 AND ")
		 .append(" p1.PERSONA_ID_ORDEN in (select p2.PERSONA_ID from T_05_DIG_SECCION01 p2 where p2.ID=h.ID AND p2.HOGAR_ID=h.HOGAR_ID AND P2.QH07 BETWEEN 0 AND 5)) as HEMO6, ")
		 .append(" (select count(*) from T_05_DIG_SECCION01 p2 where p2.ID=h.ID AND p2.HOGAR_ID=h.HOGAR_ID AND P2.QH07 BETWEEN 0 AND 5) as MENOR6, ")
        .append(" (CASE WHEN (h.QH224>=1) THEN 'SI' ELSE 'NO' END) as SAL, ")
        .append(" '' as VIOLENCIA, '' as PRESION_ARTERIAL,'' as SALUD12, '' as MENOR12, '' as CEPILLO, '' as MENOR1_12, h.ID as ID, m.NSELV as NSELV, ")
        .append(" (CASE WHEN C.COMPLETADO_GPS==1 THEN 'SI'")
        .append("   WHEN C.COMPLETADO_GPS==2 THEN 'PARC'")
        .append("   ELSE 'NO' END) as GPS,")
        .append(" CASE WHEN (CC.RESVIV==0) THEN 'INICIAR ENTREVISTA' ")
        .append(" WHEN (CC.RESVIV==2) THEN 'PRESENTE PERO ENTREVISTADO AUSENTE' ")
        .append(" WHEN (CC.RESVIV==3) THEN 'AUSENTE' ")
        .append(" WHEN (CC.RESVIV==4) THEN 'APLAZADA' ")
        .append(" WHEN (CC.RESVIV==5) THEN 'RECHAZADA' ")
        .append(" WHEN (CC.RESVIV==6) THEN 'VIVIENDA DESOCUPADA O NO ES VIVIENDA' ")
        .append(" WHEN (CC.RESVIV==7) THEN 'VIVIENDA DESTRUIDA' ")
        .append(" WHEN (CC.RESVIV==8) THEN 'VIVIENDA NO ENCONTRADA' ")
        .append(" WHEN (CC.RESVIV==9) THEN 'OTRA' ")
        .append(" ELSE NULL END as RES_VIV ")
        .append(" FROM T_MARCO m ")
        .append(" JOIN T_UBIGEO u ON m.UBIGEO = u.UBIGEO ")
        .append(" LEFT JOIN (SELECT M.ID, CASE WHEN SUM(M.DATA)=COUNT(M.ID) THEN 1")
        .append("       WHEN SUM(M.DATA)<COUNT(M.ID) AND SUM(M.DATA)<>0 THEN 2      ")
        .append("       ELSE 3 END AS COMPLETADO_GPS")
        .append("      FROM(")
        .append("      SELECT ID,CASE WHEN QVGPS_LONG=='9999999999' THEN 0 ELSE 1 END AS  DATA  FROM T_05_DIG_VISITA_VIV")
        .append("      )M  ")
        .append("      GROUP BY  M.ID) c ON m.ID = c.ID                       ")
    	 .append(" LEFT JOIN (SELECT ID,QVVRESUL AS RESVIV FROM T_05_DIG_VISITA_VIV V WHERE NRO_VISITA=(SELECT MAX(NRO_VISITA) FROM T_05_DIG_VISITA_VIV WHERE ID=V.ID )) CC ON CC.ID=C.ID")
        .append(" LEFT JOIN T_05_DIG_HOGAR h ON h.ID = c.ID AND h.HOGAR_ID not in(0) ")
        .append(" LEFT JOIN T_05_DIG_SECCION01 p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.QH01 = 1 ")
        .append(" LEFT JOIN T_05_DIG_VISITA v on h.ID = v.ID AND h.HOGAR_ID = v.HOGAR_ID AND ")
        .append(" v.NRO_VISITA in (select Max(v1.NRO_VISITA) from T_05_DIG_VISITA v1 where h.ID = v1.ID AND h.HOGAR_ID = v1.HOGAR_ID) ")
        .append(" WHERE m.CONGLOMERADO = '")
        .append(codccpp)
        .append("' AND m.ASIGNADO IN (1,3) ")
		 .append("  order by VIVIENDA, HOGAR  ");
		q.setStatement(sb.toString());
		reporteh = getBeans(q, ReporteHMS.class, camposFinal);
		
		sb = new StringBuilder();
		q = new Query();	
		
		sb.append("SELECT distinct CAST(m.NSELV AS INT) as VIVIENDA, h.HOGAR_ID as HOGAR,p.PERSONA_ID as LINEA, ")
				.append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as ENTREVISTADORA, ")
				.append(" 'SALUD' AS SECCION,CASE WHEN (p.QH02_3 is null) THEN p.QH02_1||' '||p.QH02_2 ELSE (p.QH02_1||' '||p.QH02_2 ||' '||p.QH02_3) end as NOMBRE, ")
				.append(" CASE WHEN (v.QSVRESUL==1) THEN 'Completa' ")
				.append(" WHEN (v.QSVRESUL==2) THEN 'Ausente' ")
				.append(" WHEN (v.QSVRESUL==4) THEN 'Rechazada' ")				
				.append(" WHEN (v.QSVRESUL==5) THEN 'Incompleta' ")
				.append(" WHEN (v.QSVRESUL==6) THEN 'Discapacitada(o)' ")
				.append(" WHEN (v.QSVRESUL==9) THEN 'Otra' ")
				.append(" ELSE NULL END as RESULTADO, ")
				.append(" (select Max(v2.NRO_VISITA) from T_05_DIG_CSVISITA v2 where h.ID = v2.ID AND h.HOGAR_ID = v2.HOGAR_ID) as VISITAS, ")
				.append(" (select CASE WHEN (s9.QS902==1) THEN '1-MEDIDO(A)' ")
				.append(" WHEN (s9.QS902==2) THEN '2-NO PRESENTE' ")
				.append(" WHEN (s9.QS902==3) THEN '3-RECHAZO' ")
				.append(" WHEN (s9.QS902==4) THEN '4-FUE EVALUADA EN C. HOGAR' ")
				.append(" WHEN (s9.QS902==5) THEN '5-MEDIDO(A) PARCIALMENTE' ")
				.append(" WHEN (s9.QS902==6) THEN '6-OTRO' ")
				.append(" ELSE NULL END from  T_05_DIG_CAP04_07 s9 where h.ID=s9.ID AND h.HOGAR_ID = s9.HOGAR_ID AND p.PERSONA_ID=s9.PERSONA_ID) as PESO_TALLA, ")
				.append(" '' as HEMOG, '' as PESOTALLA6, '' as HEMO6, '' as MENOR6, '' as SAL, '' as VIOLENCIA, ")
				.append(" (select CASE WHEN (s9.QS906==1) THEN '1-MEDIDO(A)' ")
				.append(" WHEN (s9.QS906==2) THEN '2-NO PRESENTE' ")
		        .append(" WHEN (s9.QS906==3) THEN '3-RECHAZO' ")
		        .append(" WHEN (s9.QS906==6) THEN '6-OTRO' ")
		        .append(" ELSE NULL END from  T_05_DIG_CAP04_07 s9 where h.ID=s9.ID AND h.HOGAR_ID = s9.HOGAR_ID AND p.PERSONA_ID=s9.PERSONA_ID) as PRESION_ARTERIAL, ")
		        .append(" (select count(*) from T_05_DIG_CSSECCION_08 s8 ")
		        .append(" where s8.ID=h.ID AND s8.HOGAR_ID=h.HOGAR_ID AND ")
		        .append(" ((s8.QS802 IS NOT NULL AND s8.QS802A IS NOT NULL AND s8.QS802 IS NOT NULL AND s8.QS802CD IS NOT NULL AND ")
		        .append(" s8.QS802D IS NOT NULL AND s8.QS803 IS NOT NULL AND s8.QS806 IS NOT NULL AND s8.QS838A_H IS NOT NULL AND ")
		        .append(" s8.QS838A_M IS NOT NULL) OR (s8.QS802 IS NOT NULL AND s8.QS802A IS NOT NULL))) as SALUD12, ")
		        .append(" (select count(*) from T_05_DIG_SECCION01 p2 where p2.ID=h.ID AND p2.HOGAR_ID=h.HOGAR_ID AND P2.QH07 BETWEEN 0 AND 11) as MENOR12, ")
		        .append(" (select count(*) from T_05_DIG_CSSECCION_08 s8 where s8.ID=h.ID AND s8.HOGAR_ID=h.HOGAR_ID AND s8.QS809 in (1,2)) as CEPILLO, ")
		        .append(" (select count(*) from T_05_DIG_SECCION01 p2 where p2.ID=h.ID AND p2.HOGAR_ID=h.HOGAR_ID AND P2.QH07 BETWEEN 1 AND 11) as MENOR1_12, ")
		        .append(" NULL as GPS ")
		        .append(" FROM T_MARCO m ")
		        .append(" JOIN T_UBIGEO u ON m.UBIGEO = u.UBIGEO ")
		        .append(" inner JOIN T_05_DIG_CARATULA c ON m.ID = c.ID ")
		        .append(" inner JOIN T_05_DIG_HOGAR h ON h.ID = c.ID AND h.HOGAR_ID not in(0) ")
		        .append(" inner join T_05_DIG_SECCION01 p ON c.ID=P.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.PERSONA_ID = h.NRO_INFO_S ")
		        .append(" inner join T_05_DIG_CSVISITA v ON h.ID=v.ID AND h.HOGAR_ID=v.HOGAR_ID AND v.PERSONA_ID = p.PERSONA_ID ")
		        .append("  AND v.NRO_VISITA =(SELECT MAX(NRO_VISITA) FROM T_05_DIG_CSVISITA VS WHERE VS.ID = h.ID AND VS.HOGAR_ID = h.HOGAR_ID AND VS.PERSONA_ID=p.PERSONA_ID) ")
		        .append(" left join T_05_DIG_SALUD s ON h.ID = s.ID AND h.HOGAR_ID = s.HOGAR_ID AND s.PERSONA_ID=p.PERSONA_ID ")
//		        .append(" inner JOIN T_05_DIG_CSVISITA v on h.ID = v.ID AND h.HOGAR_ID = v.HOGAR_ID AND ")
//		        .append(" v.FECCRE in (select Max(v1.FECCRE) from T_05_DIG_CSVISITA v1 where h.ID = v1.ID AND h.HOGAR_ID = v1.HOGAR_ID) ")
//		        .append(" inner JOIN T_05_DIG_SALUD s ON h.ID = s.ID AND h.HOGAR_ID = s.HOGAR_ID AND v.PERSONA_ID=s.PERSONA_ID ")
//		        .append(" inner JOIN T_05_DIG_SECCION01 p ON s.ID = p.ID AND s.HOGAR_ID = p.HOGAR_ID AND s.PERSONA_ID=p.PERSONA_ID ")
		        .append(" WHERE m.CONGLOMERADO = '")
		        .append(codccpp)
		        .append("' AND m.ASIGNADO IN (1,3) AND h.HOGAR_ID>=1 ")
				.append(" order by VIVIENDA, HOGAR ");
		q.setStatement(sb.toString());
		reportes = getBeans(q, ReporteHMS.class, camposFinal);
		
		sb = new StringBuilder();
		q = new Query();	
		
		sb.append("SELECT distinct CAST(m.NSELV AS INT) as VIVIENDA, h.HOGAR_ID as HOGAR,p.PERSONA_ID as LINEA, ")
				.append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as ENTREVISTADORA, ")
				.append(" 'MEF' AS SECCION,CASE WHEN (p.QH02_3 is null) THEN p.QH02_1||' '||ifnull(p.QH02_2,'') ELSE (p.QH02_1||' '||ifnull(p.QH02_2,'') ||' '||ifnull(p.QH02_3,'')) end as NOMBRE, ")
				.append(" (select ")
				.append(" CASE WHEN (v.QIVRESUL==1) THEN 'Completa' ")
				.append(" WHEN (v.QIVRESUL==2) THEN 'Ausente' ")
				.append(" WHEN (v.QIVRESUL==3) THEN 'Aplazada' ")
				.append(" WHEN (v.QIVRESUL==4) THEN 'Rechazada' ")
				.append(" WHEN (v.QIVRESUL==5) THEN 'Incompleta' ")
				.append(" WHEN (v.QIVRESUL==6) THEN 'Discapacitada' ")
				.append(" WHEN (v.QIVRESUL==7) THEN 'Otra' ")
				.append(" ELSE NULL END ")
				.append(" from T_05_DIG_CARATULA_INDIVIDUAL v where v.ID=p.ID AND v.HOGAR_ID=p.HOGAR_ID AND v.PERSONA_ID=p.PERSONA_ID AND v.NRO_VISITA=(SELECT MAX(NRO_VISITA) FROM T_05_DIG_CARATULA_INDIVIDUAL WHERE ID=v.ID AND HOGAR_ID=v.HOGAR_ID AND PERSONA_ID=p.PERSONA_ID)) as RESULTADO, ")
				.append(" (select count(*) from T_05_DIG_CARATULA_INDIVIDUAL v2 where v2.ID=p.ID AND v2.HOGAR_ID=p.HOGAR_ID AND v2.PERSONA_ID=p.PERSONA_ID AND v2.NRO_VISITA is not null) as VISITAS, ")
				.append(" (select CASE WHEN (s9.QH207==1) THEN '1-MEDIDA' ")
				.append(" WHEN (s9.QH207==2) THEN '2-NO PRESENTE' ")
				.append(" WHEN (s9.QH207==3) THEN '3-RECHAZO' ")
				.append(" WHEN (s9.QH207==5) THEN '5-MEDIDA PARCIALMENTE' ")
				.append(" WHEN (s9.QH207==6) THEN '6-OTRO' ")
				.append(" ELSE NULL END from  T_05_DIG_SECCCION04_05 s9 where p.ID=s9.ID AND p.HOGAR_ID = s9.HOGAR_ID AND p.PERSONA_ID=s9.PERSONA_ID_ORDEN) as PESO_TALLA, ")
				.append(" (select CASE WHEN (s9.QH213==1) THEN '1-MEDIDA' ")
				.append(" WHEN (s9.QH213==2) THEN '2-NO PRESENTE' ")
		        .append(" WHEN (s9.QH213==3) THEN '3-RECHAZO' ")
		        .append(" WHEN (s9.QH213==6) THEN '6-OTRO' ")
		        .append(" ELSE NULL END from  T_05_DIG_SECCCION04_05 s9 where p.ID=s9.ID AND p.HOGAR_ID = s9.HOGAR_ID AND p.PERSONA_ID=s9.PERSONA_ID_ORDEN)  as HEMOG, ")
		        .append(" '' as PESOTALLA6, '' as HEMO6, '' as MENOR6, '' as SAL, '' as VIOLENCIA, '' as PRESION_ARTERIAL, ")
		        .append(" '' as SALUD12, '' as MENOR12, '' as CEPILLO, '' as MENOR1_12, NULL as GPS ")
		        .append(" FROM T_MARCO m ")
//		        .append(" JOIN T_UBIGEO u ON m.UBIGEO = u.UBIGEO ")
		        .append(" INNER JOIN T_05_DIG_CARATULA c ON m.ID = c.ID ")
		        .append(" INNER JOIN T_05_DIG_HOGAR h ON h.ID = c.ID AND h.HOGAR_ID not in(0) ")
		        .append(" INNER JOIN T_05_DIG_SECCION01 p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.QH07 BETWEEN "+ App.HogEdadMefMin + " AND " + App.HogEdadMefMax + " AND p.QH06=2 ")
		        .append(" WHERE cast(m.CONGLOMERADO as INT) = cast('")
		        .append(codccpp)
		        .append("' as INT) AND m.ASIGNADO IN (1,3) AND h.HOGAR_ID>=1 ")
				.append(" order by VIVIENDA, HOGAR, LINEA ");
		q.setStatement(sb.toString());
		reportei = getBeans(q, ReporteHMS.class, camposFinal);
		
		reporte = new ArrayList<ReporteHMS>();
		
//		Integer prueba =  SeleccionarMefViolenciaDomestica(11,1,"007",ctx);
//		
//		Log.e("prueba: ",""+prueba);
		
//		Log.e("reporteh.size: ",""+reporteh.size());
//		Log.e("reportei.size: ",""+reportei.size());
//		Log.e("reportes.size: ",""+reportes.size());
		
		Integer viv = -1;
		
//		for(int j=0;j<reporteh.size();j++){
//			Log.e("reporteh.get(j).vivienda: ","("+j+")"+reporteh.get(j).vivienda);
//			Log.e("reporteh.get(j).hogar: ","("+j+")"+reporteh.get(j).hogar);
//			Log.e("reporteh.get(j).linea: ","("+j+")"+reporteh.get(j).linea);			
//		}
//		
		for(int i=0;i<reporteh.size();i++){
			if(!reporteh.get(i).vivienda.equals(viv)){
				viv = reporteh.get(i).vivienda; 
				reporteh.get(i).gps_mostrar = true;
			}
			reporte.add(reporteh.get(i));
//			Log.e("reporteh.get(i).vivienda: ","("+i+")"+reporteh.get(i).vivienda);
//			Log.e("reporteh.get(i).hogar: ","("+i+")"+reporteh.get(i).hogar);
//			Log.e("reporteh.get(i).visitas: ","("+i+")"+reporteh.get(i).visitas);
			if(reporteh.get(i).visitas!=null){
				if(reporteh.get(i).getResultado().equals("Completa")){
					int mef = SeleccionarMefViolenciaDomestica(reporteh.get(i).id, reporteh.get(i).hogar,reporteh.get(i).nselv,ctx);
					for(int j=0;j<reportei.size();j++){
//						Log.e("reportei.get(j).vivienda: ","("+j+")"+reportei.get(j).vivienda);
//						Log.e("reportei.get(j).hogar: ","("+j+")"+reportei.get(j).hogar);
//						Log.e("reportei.get(j).linea: ","("+j+")"+reportei.get(j).linea);
						if(reporteh.get(i).vivienda.equals(reportei.get(j).vivienda) && reporteh.get(i).hogar.equals(reportei.get(j).hogar)){
							if(reportei.get(j).linea!=null){
								reportei.get(j).violencia = "NO";
								if(reportei.get(j).linea==mef){
									reportei.get(j).violencia = "SI";
								}
								reporte.add(reportei.get(j));
							}
							reportei.remove(j);
							j--;
						}
					}
					for(int j=0;j<reportes.size();j++){
						if(reporteh.get(i).vivienda.equals(reportes.get(j).vivienda) && reporteh.get(i).hogar.equals(reportes.get(j).hogar)){
							reporte.add(reportes.get(j));
							reportes.remove(j);
							j = reportes.size();
						}
					}
				}				
			}			
		}
		
		return reporte;
//		return reporteh;
//		return reportes;
//		return reportei;
	}
	public Integer SeleccionarMefViolenciaDomestica(Integer id, Integer hogar_id, String nselv,Context ctx)
	{	List<Seccion01> ListadoMef;
		Integer[][] MatrizViolencia = LlenarMatrizViolenciaFamiliar();
		Integer fila =Integer.parseInt(nselv.substring(2,3));
//		Log.e("vivienda seleccionada",App.getInstance().getMarco().nselv);
		Integer columna = getServiceSeccion01(ctx).cantidaddeMef(id,hogar_id);
//		Log.e("COLUMNA",""+columna);
//		Log.e("FILA",""+fila);
		Integer mefPosition =0; 
		if(columna!=0)
		{
			mefPosition=MatrizViolencia[fila][columna-1];
		}
//		Log.e("POSICION SELECCIONADA",""+mefPosition);
		
		ListadoMef = getServiceSeccion01(ctx).getMef(id, hogar_id);
		Integer persona_id=-1;
		if(ListadoMef.size()>0)
		{
			persona_id=ListadoMef.get(mefPosition-1).persona_id;
		}
		return persona_id;
	}
	
	public Integer[][] LlenarMatrizViolenciaFamiliar()
	{
		Integer[][] matriz= new Integer[10][10];
		matriz[0][0]=1; matriz[0][1]=2; matriz[0][2]=2; matriz[0][3]=4; matriz[0][4]=3; matriz[0][5]=6; matriz[0][6]=5; matriz[0][7]=4; 
		matriz[1][0]=1; matriz[1][1]=1; matriz[1][2]=3; matriz[1][3]=1; matriz[1][4]=4; matriz[1][5]=1; matriz[1][6]=6; matriz[1][7]=5; 
		matriz[2][0]=1; matriz[2][1]=2; matriz[2][2]=1; matriz[2][3]=2; matriz[2][4]=5; matriz[2][5]=2; matriz[2][6]=7; matriz[2][7]=6; 
		matriz[3][0]=1; matriz[3][1]=1; matriz[3][2]=2; matriz[3][3]=3; matriz[3][4]=1; matriz[3][5]=3; matriz[3][6]=1; matriz[3][7]=7; 
		matriz[4][0]=1; matriz[4][1]=2; matriz[4][2]=3; matriz[4][3]=4; matriz[4][4]=2; matriz[4][5]=4; matriz[4][6]=2;	matriz[4][7]=8;
		matriz[5][0]=1; matriz[5][1]=1; matriz[5][2]=1; matriz[5][3]=1; matriz[5][4]=3; matriz[5][5]=5; matriz[5][6]=3; matriz[5][7]=1; 
		matriz[6][0]=1; matriz[6][1]=2; matriz[6][2]=2; matriz[6][3]=2; matriz[6][4]=4; matriz[6][5]=6; matriz[6][6]=4; matriz[6][7]=2; 
		matriz[7][0]=1; matriz[7][1]=1; matriz[7][2]=3; matriz[7][3]=3; matriz[7][4]=5; matriz[7][5]=1; matriz[7][6]=5; matriz[7][7]=3; 
		matriz[8][0]=1; matriz[8][1]=2; matriz[8][2]=1; matriz[8][3]=4; matriz[8][4]=1; matriz[8][5]=2; matriz[8][6]=6; matriz[8][7]=4; 
		matriz[9][0]=1;	matriz[9][1]=1;	matriz[9][2]=2;	matriz[9][3]=1;	matriz[9][4]=2;	matriz[9][5]=3;	matriz[9][6]=7;	matriz[9][7]=5;
		return matriz;
	}
	
	private Seccion01Service getServiceSeccion01(Context ctx) {
        if (seccion01 == null) {
        	seccion01 = Seccion01Service.getInstance(ctx);
        }
        return seccion01;
    }
	public List<ReportDatesofBirth> getListadodeFechasporConglomerado(String conglomerado){
		StringBuilder sb = new StringBuilder();
		String[] camposFinal = new ReportDatesofBirth().getFieldsNames();
		Query q = new Query();	
		sb.append("SELECT * FROM ")
		  .append(VISTA_FECHA_NACIMIENTO)
		  .append(" WHERE CONGLOMERADO=")
		  .append(conglomerado);
		q.setStatement(sb.toString());
		return getBeans(q, ReportDatesofBirth.class, camposFinal);
	}
	
	public Salud getSaludNavegacionS1_3(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		Salud salud=(Salud)getBean(TABLA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",Salud.class,secciones);
		if(salud==null){
			salud = new Salud();
			salud.id=id;
			salud.hogar_id=hogar_id;
			salud.persona_id=persona_id;
		}
		return salud;
	}
	public CAP04_07 getSaludNavegacionS4_7(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		CAP04_07 cap04_07=(CAP04_07)getBean(TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CAP04_07.class,secciones);
		if(cap04_07==null){
			cap04_07= new CAP04_07();
			cap04_07.id=id;
			cap04_07.hogar_id =hogar_id;
			cap04_07.persona_id=persona_id;
		}
		return cap04_07;
	}
  	public CSSECCION_08 getPersonaSeccion8Salud(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
  		return (CSSECCION_08)getBean(TABLA_CSSECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString() },"ID,HOGAR_ID,PERSONA_ID",CSSECCION_08.class,secciones);
  	}
  	
  	public List<Seccion03> getPersonaSeccion03(Integer id,Integer hogar_id,Integer persona_id_orden,SeccionCapitulo... secciones){
  		return getBeans(TABLA_SECCION03,"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", new String[]{id.toString(),hogar_id.toString(),persona_id_orden.toString() },"ID,HOGAR_ID,PERSONA_ID_ORDEN",Seccion03.class,secciones);
  	}
  	
  	public boolean BorrarPersona(Integer id,Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		dbTX.close();
		return true;
	}
  	public boolean BorrarSalud1(Integer id,Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_SALUD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		dbTX.close();
		return true;
	}
  	public boolean BorrarSalud2(Integer id,Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CAP04_07, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		dbTX.close();
		return true;
	}
  	public boolean BorrarVisitaSalud(Integer id,Integer hogar_id, Integer numero_visita){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CSVISITA, "ID=? AND HOGAR_ID=? AND NRO_VISITA=?", new String[]{id.toString(),hogar_id.toString(),numero_visita.toString()});
		dbTX.close();
		return true;
	}
  	/******validacion para exportacion por vonglomerado**/
  	public boolean getResultadodelaUltimaVisitaHogar(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT QHVRESUL ")
  			.append(" FROM RESULTADO_ULTIMAVISITAHOGAR ").append("")
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("QHVRESUL"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total == 1;
  	}
	public boolean getExisteResultadodelasVisitas(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_VISITA_COMPLETADA)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getHogarCompletado(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_HOGAR_COMPLETADO)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getSeccion04_05Completado(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_COMPLETADOS_SECCION04_05)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getVisitaSaludCompletada(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_VISITASALUD_COMPLETADA)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getExisteCuestionarioDeSalud(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_CS_COMPLETADO_PARA_EXPORTACION)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getSeccion08Completado(Integer id,Integer hogar_id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_SECCION08COMPLETADO)
  			.append(" WHERE ID=? AND HOGAR_ID = ?");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString(), hogar_id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public boolean getExisteCaratulaDelaVivienda(Integer id){
		Integer cantidad= this.count("ID", TABLA_CARATULA, "ID=?", new String[]{id.toString()});
		return cantidad==1;
	}
	public boolean getViviendaTieneGPS(Integer id){
  		SQLiteDatabase dbr = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		int total = 0;
  		query.append("SELECT SUM(ESTADO) CANTIDAD ")
  			.append("FROM ").append(V_VIVIENDAGPS)
  			.append(" WHERE ID=? ");
  				cursor = dbr.rawQuery(query.toString(),
  			new String[] { id.toString()});
  			if (cursor.moveToNext()) {
  				total = Util.getInt(getString("CANTIDAD"));
  			}
  			cursor.close();
  			cursor = null;
  			SQLiteDatabase.releaseMemory();
  			return total==0;
  	}
	public List<Seccion01> getSeccion01ListbyMarcoyHogarMenorEdad(Integer id,Integer hogar_id,Integer persona_id,Integer qi219,Integer qh06,SeccionCapitulo... secciones) {
		
		StringBuilder query = new StringBuilder();
		query.append("ID=? AND HOGAR_ID=? AND QH07 < (SELECT (QH07-10) FROM ").append(TABLA_SECCION01).append(" WHERE ID = '").
		append(id).append("' AND HOGAR_ID = '").append(hogar_id).append("' AND PERSONA_ID = '").append(persona_id).
		append("')  AND PERSONA_ID NOT IN (SELECT QI219 FROM ").append(TABLA_CISECCION_02).append(" WHERE ID = '").append(id).append("' AND HOGAR_ID = '").append(hogar_id).
		append("' AND QI219 <> '").append(qi219).append("')");
		
		if(qh06!=null)
			query.append(" AND QH06 = '").append(qh06).append("'");
		return getBeans(VISTA_ESTADOPERSONAS, query.toString(), new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones); 
		/*SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		List<Seccion01> lista = new ArrayList<Seccion01>();
  		query.append("SELECT * FROM (");
  		query.append("SELECT ID, HOGAR_ID, PERSONA_ID, QH01, QH02_1, QH02_2,QH02_3, QH03, QH04, QH05, QH06, QH07, QH7DD,QH7MM, QHINFO FROM T_05_DIG_SECCION01 WHERE ID='"+id+"' AND HOGAR_ID='"+hogar_id+"' ").
  		append(" AND  QH07 <= (SELECT (QH07 - 10)   FROM T_05_DIG_SECCION01 WHERE ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"') ) WHERE ID='"+id+"' AND HOGAR_ID='"+hogar_id+"' ").
  		append(" AND PERSONA_ID NOT IN (SELECT QI219 FROM T_05_DIG_CISECCION_02  WHERE ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND QI219 <> '"+qi219+"')");
  		
  		cursor = dbTX.rawQuery(query.toString(),new String[]{});
  		if(cursor.moveToNext()){
  			Seccion01 secc = new Seccion01();
  			secc.id= Integer.parseInt(getString("ID"));
  			secc.hogar_id=Integer.parseInt(getString("HOGAR_ID"));
  			secc.persona_id=Integer.parseInt(getString("PERSONA_ID"));
  			secc.qh01=Integer.parseInt(getString("QH01"));
  			secc.qh02_1=getString("QH02_1");
  			secc.qh02_2=getString("QH02_2");
  			secc.qh02_3=getString("QH02_3");
  			secc.qh03=getString("QH03")==null ? null:Integer.parseInt(getString("QH03"));
  			secc.qh04=getString("QH04")==null ? null:Integer.parseInt(getString("QH04"));
  			secc.qh05=getString("QH05")==null ? null:Integer.parseInt(getString("QH05"));
  			secc.qh06=getString("QH06")==null ? null:Integer.parseInt(getString("QH06"));
  			secc.qh07=getString("QH07")==null ? null:Integer.parseInt(getString("QH07"));
  			secc.qh7dd=getString("QH7DD");
  			secc.qh7mm=getString("QH7MM");
  			secc.qhinfo=getString("QHINFO")==null ? null:Integer.parseInt(getString("QHINFO"));
  			//secc.estado	=getString("ESTADO")==null ? null:Integer.parseInt(getString("ESTADO"));
  			lista.add(secc);
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return lista;*/
	}
	
	public HashMap<String,Integer> getValoresNacimmientos(Integer id,Integer hogar_id,Integer persona_id,Integer qi212) {
		 
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		HashMap<String,Integer> lista = new HashMap<String,Integer>();
  		query.append("SELECT ").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=1 AND QI216=1 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"'AND QI212 NOT IN('"+qi212+"')) AS HVIVOS,").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=1 AND QI216=2 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"'AND QI212 NOT IN('"+qi212+"')) AS HMUERTOS,").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=2 AND QI216=1 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"'AND QI212 NOT IN('"+qi212+"')) AS MVIVAS,").   
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=2 AND QI216=2 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"'AND QI212 NOT IN('"+qi212+"')) AS MMUERTAS");
  		
  		cursor = dbTX.rawQuery(query.toString(),new String[]{});
  		if(cursor.moveToNext()){  			
  			lista.put("HVIVOS", Integer.parseInt(getString("HVIVOS")));
  			lista.put("HMUERTOS", Integer.parseInt(getString("HMUERTOS")));
  			lista.put("MVIVAS", Integer.parseInt(getString("MVIVAS")));
  			lista.put("MMUERTAS", Integer.parseInt(getString("MMUERTAS")));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return lista;
	}
	
	public HashMap<String,Integer> getValoresNacimmientos(Integer id,Integer hogar_id,Integer persona_id) {
		 
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		HashMap<String,Integer> lista = new HashMap<String,Integer>();
  		query.append("SELECT ").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=1 AND QI216=1 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"') AS HVIVOS,").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=1 AND QI216=2 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"') AS HMUERTOS,").
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=2 AND QI216=1 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"') AS MVIVAS,").   
  		append("(SELECT COUNT(QI214) FROM T_05_DIG_CISECCION_02 WHERE QI214=2 AND QI216=2 AND ID = '"+id+"' AND HOGAR_ID = '"+hogar_id+"' AND PERSONA_ID = '"+persona_id+"') AS MMUERTAS");
  		
  		cursor = dbTX.rawQuery(query.toString(),new String[]{});
  		if(cursor.moveToNext()){  			
  			lista.put("HVIVOS", Integer.parseInt(getString("HVIVOS")));
  			lista.put("HMUERTOS", Integer.parseInt(getString("HMUERTOS")));
  			lista.put("MVIVAS", Integer.parseInt(getString("MVIVAS")));
  			lista.put("MMUERTAS", Integer.parseInt(getString("MMUERTAS")));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return lista;
	}
	public List<CISECCION_02> getFechasdegemelos(Integer id, Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getBeans(VISTA_LISTADOGEMELOS, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_02.class,secciones);
	}
	public CISECCION_08 getCISECCION_08(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_08)getBean(TABLA_CISECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_08.class,secciones);
	}
	public List<CISECCION_02> getNacimientosListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212,HOGAR_ID",CISECCION_02.class,secciones);
	}
	
	public List<CISECCION_02> getListaNacimientosCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		/*return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? "dd
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212,HOGAR_ID",CISECCION_02.class,secciones);*/
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT C2.QI212,C2.QI212_NOM,C2.ID,C2.HOGAR_ID,C2.PERSONA_ID,C2.QI215D,C2.QI215M,C2.QI215Y,C2.QI216,C2.QI217,C2.QI218,VC2.ESTADO AS ESTADOCAP2 ").
		append(" FROM ").append(TABLA_CISECCION_02).append(" C2 LEFT JOIN ").append(V_SECCION2_COMPLETADO).
		append(" VC2 ON C2.ID = VC2.ID AND  C2.HOGAR_ID=VC2.HOGAR_ID AND C2.PERSONA_ID=VC2.PERSONA_ID AND C2.QI212 = VC2.QI212  ").
		append(" WHERE C2.ID=? AND C2.HOGAR_ID=? AND C2.PERSONA_ID=?  ORDER BY C2.QI212 ASC");
				
		Query query = new Query(sb.toString(), new String[] { id.toString(),hogar_id.toString(),persona_id.toString() });
		return getBeans(query, CISECCION_02.class, secciones[0].getCampos());
	}
	
	public boolean getSeccion2Completado(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT SUM(ESTADO) AS ESTADO ")
  			 .append(" FROM ").append(V_SECCION2_COMPLETADO).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?  " );
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				});
  						
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ESTADO"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident==0;
	}
	
	public List<CISECCION_02> getNacimientosListbyPersonaMayor1Anio(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI217>0 AND QI218=1 AND QI215Y>="+App.ANIOPORDEFECTO
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212 DESC,HOGAR_ID",CISECCION_02.class,secciones);
	}
	public boolean getNacimientosMayoresaunAnio(Integer id,Integer hogar_id,Integer persona_id){
		Integer contado=0;
			contado= this.count("QI212", TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND qi217>0 AND QI218=1 AND QI215Y>="+App.ANIOPORDEFECTO,
					new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
			return contado>0;
	}
	
	public boolean getNacimientosCompletadosSec10(Integer id,Integer hogar_id,Integer persona_id){
	Integer contado=0;
		contado= this.count("QI212", TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND qi217>0 AND QI218=1 AND QI215Y>="+App.ANIOPORDEFECTO,
				new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return contado>0;
	}
	public List<CISECCION_05> getParejasListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(V_SECCION5_COMPLETADO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"PAREJA_ID",CISECCION_05.class,secciones);
	}
	public boolean getCompletadoSeccion05(Integer id,Integer hogar_id,Integer persona_id) {
		Integer total=0;
	total=this.sum("ESTADO", V_SECCION5_COMPLETADO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	return total==0;
	}
	
	public List<CISECCION_02> getNacimientosVivoListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI216=1 AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+" "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212 DESC,HOGAR_ID DESC",CISECCION_02.class,secciones);
	}
	
	public List<CISECCION_02> getNacimientosMayor2011ListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+" "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212 DESC,HOGAR_ID DESC",CISECCION_02.class,secciones);
	}
	public boolean ExisteninioMenoraTresAniosyVivo(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		query.append("SELECT COUNT(*) AS CANTIDAD ")
  			 .append(" FROM ").append(TABLA_CISECCION_02)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI216 =1 AND QI217 BETWEEN 0 AND 2 ");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  			});
  		if(cursor.moveToNext()){
  			cantidad= Util.getInt(getString("CANTIDAD"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cantidad>0;
	}

	public boolean VerificarFiltro481(Integer id,Integer hogar_id,Integer persona_id, Integer anio){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		query.append("SELECT COUNT(*) AS CANTIDAD ")
  			 .append(" FROM ").append(TABLA_CISECCION_02)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI218=1 AND QI215Y>=? ");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),anio.toString()});
  		if(cursor.moveToNext()){
  			cantidad= Util.getInt(getString("CANTIDAD"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cantidad>0;
	}
	public boolean VerificarFiltro720(Integer id,Integer hogar_id,Integer persona_id, Integer edad){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		query.append("SELECT COUNT(*) AS CANTIDAD ")
  			 .append(" FROM ").append(TABLA_CISECCION_02)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI218=1 AND QI217<?");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),edad.toString()});
  		if(cursor.moveToNext()){
  			cantidad= Util.getInt(getString("CANTIDAD"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cantidad>0;
	}
	public CARATULA_INDIVIDUAL getCAPIVISITA(Integer id, Integer hogar_id, Integer persona_id, SeccionCapitulo... secciones) {
		CARATULA_INDIVIDUAL bean = (CARATULA_INDIVIDUAL) getBean(
				TABLA_CIVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(), persona_id.toString()}, CARATULA_INDIVIDUAL.class, secciones);
		return bean;
	}
	
//	public CARATULA_INDIVIDUAL getCAPIVISITAPORVISTA(Integer id, Integer hogar_id, Integer persona_id, SeccionCapitulo... secciones) {
//		CARATULA_INDIVIDUAL bean = (CARATULA_INDIVIDUAL) getBean(
//				VISITA_INDIVIDUALCOMPLETO,
//				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
//				new String[] { id.toString(), hogar_id.toString(), persona_id.toString()}, CARATULA_INDIVIDUAL.class, secciones);
//		return bean;
//	}
	
	public List<CARATULA_INDIVIDUAL> getCAPVISITAsI(Integer id, Integer hogar_id, Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_CIVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString(), persona_id.toString()},
				"ID,HOGAR_ID,PERSONA_ID", CARATULA_INDIVIDUAL.class, secciones);
	}
	public CARATULA_INDIVIDUAL getVisitaI(Integer id, Integer hogar_id, Integer persona_id,Integer nro_visita,
			SeccionCapitulo... secciones) {
		CARATULA_INDIVIDUAL bean = (CARATULA_INDIVIDUAL) getBean(
				TABLA_CIVISITA,
				"ID=? AND Hogar_ID=? AND persona_id=? AND NRO_Visita=?",
				new String[] { id.toString(), hogar_id.toString(), persona_id.toString(),
						nro_visita.toString() }, CARATULA_INDIVIDUAL.class, secciones);
		return bean;
	}
	public boolean borrarNacimiento(CISECCION_02 bean, SQLiteDatabase dbTX)
	{
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
	
			boolean flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			/*flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIODIT_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_03 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID IN ( SELECT QITRAMO_ID  FROM "+TABLA_CALENDARIO_TRAMO+ " WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? )",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			*/
			flag = borrarTablasEnlaceSeccion2(bean,dbTX, flag);	
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
			reordenarNumerodeOrdenNacimientoSecc2(bean);	
		return true;
		
	}
	
	public boolean borrarEnlaceNacimiento(CISECCION_02 bean, SQLiteDatabase dbTX)
	{
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
		boolean	flag = false;
		 flag = borrarTablasEnlaceSeccion2(bean,dbTX, flag);	
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
		return true;
		
	}
	
	public boolean borrarTablaDiscapacidad(CISECCION_02 bean){
		boolean flag = false;
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		if (dbTX == null) {
			dbTX = startTX();
		}
		flag = borrar(dbTX, CuestionarioDAO.TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),App.CUEST_ID_INDIVIDUAL.toString(),bean.persona_id.toString(),bean.qi212.toString());
		return flag;
	}
	
	public boolean borrarTablasEnlaceSeccion2(CISECCION_02 bean, SQLiteDatabase dbTX,boolean flag){
		flag = borrar(dbTX, CuestionarioDAO.TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),App.CUEST_ID_INDIVIDUAL.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
//		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_03 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_04 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
//		flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID IN ( SELECT QITRAMO_ID  FROM "+TABLA_CALENDARIO_TRAMO+ " WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? )",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
		return flag;
	} 
	
	public boolean borrarNacimientos(Integer id,Integer hogar_id, Integer persona_id,SQLiteDatabase dbTX)
	{
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
	
			boolean 
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID NOT IN (0)", id.toString(),hogar_id.toString(),App.CUEST_ID_INDIVIDUAL.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", id.toString(),hogar_id.toString(),persona_id.toString());
//			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_03 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_04 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",id.toString(),hogar_id.toString(),persona_id.toString());
//			flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO , "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID IN ( SELECT QITRAMO_ID  FROM "+TABLA_CALENDARIO_TRAMO+ " WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? )",id.toString(),hogar_id.toString(),persona_id.toString());
			flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID IS NOT NULL ",id.toString(),hogar_id.toString(),persona_id.toString());
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
		return true;
		
	}
  	public void reordenarNumerodeOrdenNacimientoSecc2(CISECCION_02 bean)
	{	Integer orden=maximoNacimientosSecc2(bean);
		
		Integer id_nuevo=bean.qi212+1;
		if(bean.qi212<orden)
		{
			for (int i = bean.qi212; i < orden; i++) {
					if(modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,id_nuevo,i))
						id_nuevo++;
			}
		}
	}
  	
  	
  	public void reordenarSeccion2(CISECCION_02 bean,Integer indice_intercambio) {

  		if(bean.qi212 < indice_intercambio) {
  			modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,0);
  			for(int i =bean.qi212;i<indice_intercambio;i++) {
  				
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i+1,i);  					
  			}   			
  			modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,0,indice_intercambio);
  		}
  		if(bean.qi212 > indice_intercambio) {
  			modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,bean.qi212,0);
  			for(int i =bean.qi212;i>indice_intercambio;i--) {
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i-1,i);  					
  			}   			
  			modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,0,indice_intercambio);
  		}
  		
  	}
  	
  	public void reordenarSeccion2Gemelos(CISECCION_02 bean,Integer indice_intercambio,Integer indice_menor, Integer indice_mayor) {
  		
  		Integer dif = indice_mayor - indice_menor;
  		
  		if(bean.qi212 < indice_intercambio) {
  			
  			for(int i=indice_menor;i<=indice_mayor;i++) {
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i,-i);
  			}
  			for(int i =indice_mayor;i<indice_intercambio;i++) {
  				
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i+1,i-dif);  					
  			}   			
  			
  			for(int i=-indice_menor,j=dif;i>=-indice_mayor;i--,j--) {
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i,indice_intercambio-j);
  			}
  		
  		}
  		if(bean.qi212 > indice_intercambio) {  			
  			for(int i=indice_menor;i<=indice_mayor;i++) {
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i,-i);
  			}
  			
  			for(int i =indice_menor;i>indice_intercambio;i--) {
  				
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i-1,i+dif);  					
  			} 
  			
  			for(int i=-indice_menor,j=0;i>=-indice_mayor;i--,j++) {
  				modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i,indice_intercambio+j);
  			}
  		}
  		
  	}
  	
  	
  	public void ReordenarNumerodeOrdenPareja(CISECCION_05 bean)
	{	Integer orden=MaximoPareja_Id(bean);
		
		Integer id_nuevo=bean.pareja_id+1;
		if(bean.pareja_id<orden)
		{
			for (int i = bean.pareja_id; i < orden; i++) {
					if(ModificarNumerodeOrdenPareja(bean.id,bean.hogar_id,bean.persona_id,id_nuevo,i))
						id_nuevo++;
			}
		}
	}
  	public Integer maximoNacimientosSecc2(CISECCION_02 bean)
	{
	SQLiteDatabase dbTX = dbh.getWritableDatabase();
	StringBuilder query = new StringBuilder();
	Integer nrodeorden=-1;
	query.append("SELECT MAX(QI212) AS ORDEN ")
		 .append(" FROM ").append(TABLA_CISECCION_02)
	 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
	cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
	if(cursor.moveToNext())
	{nrodeorden= Util.getInt(getString("ORDEN"));
	}
	cursor.close();
	cursor=null;
	SQLiteDatabase.releaseMemory();
	return nrodeorden;
	}
  	public Integer MaximoPareja_Id(CISECCION_05 bean)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT MAX(PAREJA_ID) AS ORDEN ")
			 .append(" FROM ").append(TABLA_CISECCION_05)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
		if(cursor.moveToNext())
		{nrodeorden= Util.getInt(getString("ORDEN"));
	}
	cursor.close();
	cursor=null;
	SQLiteDatabase.releaseMemory();
	return nrodeorden;
	}
  	public boolean modificarNumerodeOrdenSecc2(Integer id,Integer hogar_id,Integer persona_id,Integer qi212_anterior ,Integer qi212_nuevo )
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("QI212", qi212_nuevo);
		
		ContentValues valores2 = new ContentValues();
		valores2.put("NINIO_ID", qi212_nuevo);
		
		ContentValues valores3 = new ContentValues();
		valores3.put("NRO_ORDEN_NINIO", qi212_nuevo);
		//valores.put("QH01", persona_id_anterior);
		if (dbTX.update(
				TABLA_CISECCION_02,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()}) == 1) {
			
			dbTX.update(
					TABLA_DISCAPACIDAD,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND NINIO_ID NOT IN(0)",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			dbTX.update(
					TABLA_CISECCION_04A,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			dbTX.update(
					TABLA_CISECCION_04B,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			dbTX.update(
					TABLA_CISECCION_04B_TARJETA,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
//			dbTX.update(
//					TABLA_CISECCION_DIT,
//					valores3,
//					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",
//					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			dbTX.update(
					TABLA_CISECCION_DIT_02,
					valores3,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
						
			dbTX.update(
					TABLA_CISECCION_10_02,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			dbTX.update(
					TABLA_CISECCION_10_03,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			dbTX.update(
					TABLA_CALENDARIO_TRAMO,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_anterior.toString()});
			
			
			/*dbTX.update(
					TABLA_CALENDARIO,
					valores2,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",
					new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_nuevo.toString()}); */
			
			return true;
		} else {
			return false;
		}
	}
  	public boolean modificarNumerodeOrdenCISec9(Integer id,Integer hogar_id,Integer persona_id,Integer qi212_nuevo,Integer qi212_anterior){
  		
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("QINRO_ORDEN", qi212_anterior);
		
		if (dbTX.update(
				TABLA_CISECCION_09,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qi212_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
 	public boolean modificarNumerodeOrdenCISeccionMortalidadHogar(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_nuevo,Integer nro_orden_anterior){
  		
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("NRO_ORDEN_ID", nro_orden_anterior);
		
		if (dbTX.update(
				TABLA_MORTALIDAD,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=? ",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),nro_orden_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
  	public boolean ModificarNumerodeOrdenPareja(Integer id,Integer hogar_id,Integer persona_id,Integer pareja_id_nuevo,Integer pareja_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PAREJA_ID", pareja_id_anterior);
		
		if (dbTX.update(
				TABLA_CISECCION_05,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PAREJA_ID=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),pareja_id_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
  	public List<CISECCION_02> getNacimientosListbyPersonaDIT(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI216 IN (1) AND QI218 IN (1) "
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212,HOGAR_ID",CISECCION_02.class,secciones);
	}
  	public boolean saveOrUpdate(CISECCION_04DIT individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",individual,new String[]{"id","hogar_id","persona_id","nro_orden_ninio"}, secciones);
	}
  	
  	
  	public List<CISECCION_02> getListaNacimientos4abyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+""
		, new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QI212 DESC,HOGAR_ID DESC",CISECCION_02.class,secciones);
  	
  	}
  	
  	public List<CISECCION_02> getListaNacimientos4aCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT C2.QI212,C2.QI212_NOM,C2.ID,C2.HOGAR_ID,C2.PERSONA_ID,C2.QI215D,C2.QI215M,C2.QI215Y,C2.QI216,C2.QI217,C2.QI218,C2.QI219,VC4.ESTADO AS ESTADOCAP4A ").
		append(" FROM ").append(TABLA_CISECCION_02).append(" C2 LEFT JOIN ").append(V_SECCION4A_COMPLETADO).
		append(" VC4 ON C2.ID = VC4.ID AND  C2.HOGAR_ID=VC4.HOGAR_ID AND C2.PERSONA_ID=VC4.PERSONA_ID AND C2.QI212=VC4.NINIO_ID ").
		append(" WHERE C2.ID=? AND C2.HOGAR_ID=? AND C2.PERSONA_ID=? AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+" ORDER BY C2.QI212 DESC");
				
		Query query = new Query(sb.toString(), new String[] { id.toString(),hogar_id.toString(),persona_id.toString() });
	    return getBeans(query, CISECCION_02.class, secciones[0].getCampos());
  	}
  	
  	public boolean getSeccion4ACompletado(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT SUM( CASE WHEN ESTADO IS NULL THEN 1 ELSE ESTADO END ) AS ESTADO ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" TC2 LEFT JOIN ").append(V_SECCION4A_COMPLETADO).append(" VS4A ")
  			 .append(" ON TC2.ID = VS4A.ID AND TC2.HOGAR_ID = VS4A.HOGAR_ID AND TC2.PERSONA_ID = VS4A.PERSONA_ID AND TC2.QI212 = VS4A.NINIO_ID  ")
  			 .append(" WHERE TC2.ID=? AND TC2.HOGAR_ID=? AND TC2.PERSONA_ID=?  AND TC2.QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+"  " );

  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				});
  						
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ESTADO"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident==0;
	}
  	
  	public List<CISECCION_02> getListaNacimientos4bCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		StringBuilder sb = new StringBuilder();
		
//		sb.append("SELECT C2.QI212,C2.QI212_NOM,C2.ID,C2.HOGAR_ID,C2.PERSONA_ID,C2.QI215D,C2.QI215M,C2.QI215Y,C2.QI216,C2.QI217,C2.QI218,(VC4.ESTADO + CASE WHEN VC4DIT.ESTADO IS NULL THEN 0 ELSE VC4DIT.ESTADO END) AS ESTADOCAP4B ").
		sb.append("SELECT C2.QI212,C2.QI212_NOM,C2.ID,C2.HOGAR_ID,C2.PERSONA_ID,C2.QI215D,C2.QI215M,C2.QI215Y,C2.QI216,C2.QI217,C2.QI218,(IFNULL(VC4.ESTADO,0) +  CASE WHEN VC4DIT.ESTADO IS NULL THEN 0 ELSE VC4DIT.ESTADO END) AS ESTADOCAP4B ")
		.append(" FROM ")
		.append(TABLA_CISECCION_02)
		.append(" C2 LEFT JOIN ")
		.append(V_SECCION4B_COMPLETADO)
		.append(" VC4 ON C2.ID = VC4.ID AND  C2.HOGAR_ID=VC4.HOGAR_ID AND C2.PERSONA_ID=VC4.PERSONA_ID AND C2.QI212=VC4.NINIO_ID ")
		.append(" LEFT JOIN ")
		.append(V_SECCION4DIT_02_COMPLETADO)
		.append(" VC4DIT ON C2.ID = VC4DIT.ID AND  C2.HOGAR_ID=VC4DIT.HOGAR_ID AND C2.PERSONA_ID=VC4DIT.PERSONA_ID AND C2.QI212=VC4DIT.NRO_ORDEN_NINIO ").
		append(" WHERE C2.ID=? AND C2.HOGAR_ID=? AND C2.PERSONA_ID=? AND (QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+" or QI478 BETWEEN 9 and 71 ) AND C2.QI216=1 ORDER BY C2.QI212 DESC");

		Query query = new Query(sb.toString(), new String[] { id.toString(),hogar_id.toString(),persona_id.toString() });

		return getBeans(query, CISECCION_02.class, secciones[0].getCampos());
  	}
  	
  	public List<CISECCION_02> getListaNacimientos4bTarjetabyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT C2.QI212,C2.QI212_NOM ,C2.QI217,C2.QI215D,C2.QI215M,C2.QI215Y, C2.ID,C2.HOGAR_ID,C2.PERSONA_ID ").
		append(" FROM ").append(TABLA_CISECCION_04B).append(" VC4 LEFT JOIN ").append(TABLA_CISECCION_02).
		append(" C2 ON C2.ID = VC4.ID AND  C2.HOGAR_ID=VC4.HOGAR_ID AND C2.PERSONA_ID=VC4.PERSONA_ID AND C2.QI212=VC4.NINIO_ID ").
		append(" WHERE VC4.ID=? AND VC4.HOGAR_ID=? AND VC4.PERSONA_ID=? AND QI454 = 1 AND C2.QI215Y>= ").append(App.ANIOPORDEFECTO).append(" ORDER BY VC4.NINIO_ID DESC");
				
		Query query = new Query(sb.toString(), new String[] { id.toString(),hogar_id.toString(),persona_id.toString() });

		return getBeans(query, CISECCION_02.class, secciones[0].getCampos());
  	}
  	
  	public boolean getSeccion4BCompletado(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT SUM(IFNULL(VS4B.ESTADO,0) + CASE WHEN VC4DIT.ESTADO IS NULL THEN 0 ELSE VC4DIT.ESTADO END) AS ESTADO ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" TC2 LEFT JOIN ").append(V_SECCION4B_COMPLETADO).append(" VS4B ")
  			 .append(" ON TC2.ID = VS4B.ID AND TC2.HOGAR_ID = VS4B.HOGAR_ID AND TC2.PERSONA_ID = VS4B.PERSONA_ID AND TC2.QI212 = VS4B.NINIO_ID  ")
//  			 .append(" LEFT JOIN ").append(V_SECCION4DIT_02_COMPLETADO)
  			 .append(" LEFT JOIN ").append(V_SECCION4DIT_02_COMPLETADO)
  			 .append(" VC4DIT ON TC2.ID = VC4DIT.ID AND  TC2.HOGAR_ID=VC4DIT.HOGAR_ID AND TC2.PERSONA_ID=VC4DIT.PERSONA_ID AND TC2.QI212=VC4DIT.NRO_ORDEN_NINIO ")
  			 .append(" WHERE TC2.ID=? AND TC2.HOGAR_ID=? AND TC2.PERSONA_ID=? AND (TC2.QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+" or QI478 BETWEEN 9 and 71 ) AND TC2.QI216=1  " );
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				});
  						
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ESTADO"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident==0;
	}
  	
  	public List<CISECCION_02T> getListarTerminaciones(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_02T,"ID=? AND Hogar_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString()},
				"ID,HOGAR_ID,PERSONA_ID,TERMINACION_ID",CISECCION_02T.class, secciones);
	}
	public CISECCION_02 getUltimoNacimiento(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer ninio_id=0;
		query.append("SELECT MAX(QI215Y),QI212")
		 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			ninio_id=getInt("QI212");
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return getSeccion01_03Nacimiento(id,hogar_id,persona_id,ninio_id,secciones);	
	}
	public CISECCION_02 getUltimoNacimientoidMayor(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer ninio_id=0;
		query.append("SELECT MAX(QI215Y),MAX(QI212) QI212")
		 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			ninio_id=getInt("QI212");
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return getSeccion01_03Nacimiento(id,hogar_id,persona_id,ninio_id,secciones);	
	}
	
	public CISECCION_02 getUltimoNacimientoidMayorVivo(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer ninio_id=0;
		query.append("SELECT MAX(QI215Y),MAX(QI212) QI212, QI218")
		 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI218 IN (1)");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			ninio_id=getInt("QI212");
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return getSeccion01_03Nacimiento(id,hogar_id,persona_id,ninio_id,secciones);	
	}
	
	public CISECCION_02 getSeccion01_03Nacimiento(Integer id, Integer hogar_id,
			Integer persona_id,Integer qi212, SeccionCapitulo... secciones) {
		CISECCION_02 bean = (CISECCION_02) getBean(
				TABLA_CISECCION_02,"ID=? AND Hogar_ID=? AND  PERSONA_ID=? AND QI212 = ?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString(),qi212.toString() }, CISECCION_02.class, secciones);
		return bean;
	}
	
	public boolean getExisteGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi212,Integer qi215y,String qi215m,String qi215d) {
		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer contar=0;
		query.append("SELECT COUNT(ID) AS TOTAL ")
			 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y=? AND QI215M=? AND QI215D=? "
		 		+ " AND QI212 NOT IN ('"+qi212+"') ");		
		
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),
				 persona_id.toString(),qi215y.toString(),qi215m,qi215d});
		if(cursor.moveToNext()){
			contar = Util.getInt(getString("TOTAL"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return contar>=1;
	}
	
	public HashMap<String,Integer> getExtremosGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi215y,String qi215m,String qi215d) {
		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer mayor=0,menor=0;
		query.append("SELECT (SELECT MAX(QI212)  ")
			 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y=? AND QI215M=? AND QI215D=? ) AS MAYOR, ")
		 .append(" (SELECT MIN(QI212)  ")
			 .append(" FROM ").append(TABLA_CISECCION_02)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y=? AND QI215M=? AND QI215D=? ) AS MENOR");
		
		
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),
				 persona_id.toString(),qi215y.toString(),qi215m,qi215d,id.toString(),hogar_id.toString(),
				 persona_id.toString(),qi215y.toString(),qi215m,qi215d});
		if(cursor.moveToNext()){
			mayor = Util.getInt(getString("MAYOR"));
			menor = Util.getInt(getString("MENOR"));
		}
		
		HashMap<String, Integer> mapa = new HashMap<String, Integer>();
		mapa.put("mayor", mayor);
		mapa.put("menor", menor);
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return mapa;
	}
	
	
	public boolean actualizarFechasGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi215y,String qi215m,String qi215d,Integer qi215ynuevo,String qi215mnuevo,String qi215dnuevo,Integer qi217nuevo,Integer qi220a) {
		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("QI215Y", qi215ynuevo);
		valores.put("QI215M", qi215mnuevo);
		valores.put("QI215D", qi215dnuevo);
//		valores.put("QI217", qi217nuevo);
		valores.put("QI220A", qi220a);
		
		if (dbTX.update(
				TABLA_CISECCION_02,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y=? AND QI215M=? AND QI215D=?  ",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(), qi215y.toString(),qi215m,qi215d}) == 1) {
			return true;
		} else {
			return false;
		}
	}
		
	public CISECCION_01_03 getTerminaciones(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return (CISECCION_01_03)getBean(TABLA_CISECCION_01_03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_01_03.class,secciones);
	}
	public List<CISECCION_02> getNacimientosparacalendario(Integer id,Integer hogar_id,Integer persona_id,Integer anio, SeccionCapitulo... secciones){
		return getBeans(TABLA_CISECCION_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y>=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),anio.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_02.class,secciones);
	}
	public List<CICALENDARIO_TRAMO_COL01_03> getTramosdelcalendario(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...capitulos){
		return getBeans(TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QIANIO_INI,QIMES_INI",CICALENDARIO_TRAMO_COL01_03.class, capitulos);
	}
	public Integer getUltimotramoidRegistrado(Integer id, Integer hogar_id, Integer persona_id){
		return this.max("QITRAMO_ID", TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaNacimientos(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoporFechadeInicio(Integer id,Integer hogar_id,Integer persona_id, Integer mes,Integer anio,SeccionCapitulo...secciones){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIMES_FIN=? AND QIANIO_FIN=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),mes.toString(),anio.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaTerminaciones(Integer id,Integer hogar_id,Integer persona_id,Integer terminacion_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),terminacion_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaEmbarazo(Integer id,Integer hogar_id,Integer persona_id,Integer embarazo_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND EMBARAZO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),embarazo_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaMetodoActual(Integer id,Integer hogar_id,Integer persona_id,Integer metodo_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND METODO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),metodo_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaNingunMetodo(Integer id,Integer hogar_id,Integer persona_id,Integer ninguno_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINGUNO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninguno_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaporTramoId(Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id,SeccionCapitulo...secciones ){
		return (CICALENDARIO_TRAMO_COL01_03)getBean(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),tramo_id.toString()},CICALENDARIO_TRAMO_COL01_03.class, secciones);
	}
	public List<CICALENDARIO_TRAMO_COL01_03> getTramosAfectados(CICALENDARIO_TRAMO_COL01_03 tramo,SeccionCapitulo...secciones ){
		return getBeans(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIANIO_INI >=? AND QIMES_INI>=? AND QIANIO_FIN <=? AND QIMES_FIN <=? ",
				new String[]{tramo.id.toString(),tramo.hogar_id.toString(),tramo.persona_id.toString(),tramo.qianio_ini.toString(), tramo.qimes_ini.toString(), tramo.qianio_fin.toString(),tramo.qimes_fin.toString()},"ID,HOGAR_ID,PERSONA_ID",CICALENDARIO_TRAMO_COL01_03.class,secciones);
	}
	public Integer saveOrUpdate(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qitramo_id == null) {
			bean.qitramo_id = nextID(dbTX, "QITRAMO_ID", TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_CALENDARIO_TRAMO,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", bean, new String[] { "id", "hogar_id","persona_id","qitramo_id" },
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return bean.qitramo_id;
	}
	public boolean saveOrUpdateTRAMO(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qitramo_id == null) {
			bean.qitramo_id = nextID(dbTX, "QITRAMO_ID", TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_CALENDARIO_TRAMO,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", bean, new String[] { "id", "hogar_id","persona_id","qitramo_id" },
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public boolean saveOrUpdate(CICALENDARIO_COL01_03 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=?", bean,
				new String[] { "id","hogar_id","persona_id","qiindice"}, secciones);
	}
	public boolean CalendarioCompletado(Integer id,Integer hogar_id,Integer persona_id,Calendar fecha_actual){
	Integer total=0;
	total=this.sum("QICANTIDAD", TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	Integer completo=73-MyUtil.DeterminarIndice(fecha_actual.get(Calendar.YEAR), fecha_actual.get(Calendar.MONTH)+1);
	return total>=completo;
	}
	public Integer CalendarioCompletadoPorcantidad(Integer id,Integer hogar_id,Integer persona_id,Calendar fecha_actual){
		Integer total=0;
		total=this.sum("QICANTIDAD", TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return total;
	}
	
	public List<CICALENDARIO_COL01_03> getAllCalendario(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
//		return getBeans(TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CICALENDARIO_COL01_03.class,secciones);
		return getBeans(V_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CICALENDARIO_COL01_03.class,secciones);
	}
	public List<CISECCION_02T> getTerminacionesParaCalendario(Integer id,Integer hogar_id,Integer persona_id,Integer anio, SeccionCapitulo...capitulos){
		return getBeans(TABLA_CISECCION_02T, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI235_Y>=? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),anio.toString() },"ID,HOGAR_ID,PERSONA_ID",CISECCION_02T.class,capitulos);
	}
	public boolean Deletetramo(String Tablename,Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id){
		return this.borrar(Tablename, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),tramo_id.toString()});
	}
	public void ReordenarIntermedioNumerodeOrdenNacimiento(CISECCION_02 bean)
	{	Integer orden=maximoNacimientosSecc2(bean);
		Integer id_nuevo=orden+1;
		//if(bean.qi212<orden)
		//{
			for (int i = orden+1 ; i > bean.qi212 ; i--) {
					if(modificarNumerodeOrdenSecc2(bean.id,bean.hogar_id,bean.persona_id,i-1,id_nuevo))
						id_nuevo--;
			}
		//}
	}
	public boolean saveOrUpdate(CISECCION_02T individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_02T,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID=?",individual,new String[]{"id","hogar_id","persona_id","terminacion_id"}, secciones);
	}
	public boolean saveOrUpdate(CISECCION_02 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qi212 == null) {
			bean.qi212 = nextID(dbTX, "QI212", TABLA_CISECCION_02,
					"ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),
					bean.hogar_id.toString(),bean.persona_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_CISECCION_02,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", bean, new String[] {
						"id", "hogar_id", "persona_id", "qi212" }, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public CISECCION_02T getCISECCION_02T(Integer id,Integer hogar_id,Integer persona_id,Integer terminacion_id,SeccionCapitulo... secciones){
		return (CISECCION_02T)getBean(TABLA_CISECCION_02T,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),terminacion_id.toString()},CISECCION_02T.class,secciones);
	}
	public CICALENDARIO_COL01_03 getCAlendarioByIndice(Integer id,Integer hogar_id,Integer persona_id,Integer indice,SeccionCapitulo...secciones){
		return (CICALENDARIO_COL01_03) getBean(TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),indice.toString() },CICALENDARIO_COL01_03.class,secciones);
	}
	public String getColumna1delCalendario(Integer id, Integer hogar_id,Integer persona_id,Integer tramo_id,String col1){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		String retornar="";
  		query.append("SELECT QICOL1 ")
  			 .append(" FROM ").append(TABLA_CALENDARIO).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=? AND QICOL1<>?")
  			 .append(" GROUP BY QICOL1 ");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),tramo_id.toString(),col1
  				});
  		if(cursor.moveToNext()){
  			retornar =  getString("QICOL1");
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return retornar!=""?retornar:col1;
	}
	public CICALENDARIO_COL01_03 getRegistro(Integer id,Integer hogar_id,Integer persona_id,Integer indice, SeccionCapitulo... secciones){
		return (CICALENDARIO_COL01_03)getBean(TABLA_CALENDARIO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),indice.toString()},CICALENDARIO_COL01_03.class,secciones);
	}
	public List<CISECCION_02> getNombreDelosNacimientosMultiples(Integer id,Integer hogar_id,Integer persona_id,String mes, Integer anio, Integer qi213,SeccionCapitulo... secciones){
		return getBeans(TABLA_CISECCION_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215M=? AND QI215Y=? AND QI213=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),mes,anio.toString(),qi213.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_02.class, secciones);
	}
	public String getNombreDelNacido(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id, SeccionCapitulo... secciones){
		CISECCION_02 Nacimiento= new CISECCION_02();
		String nombres="";
		String nombrestodos="";
		Nacimiento= (CISECCION_02)getBean(TABLA_CISECCION_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},CISECCION_02.class,secciones);
		nombres=Nacimiento!=null?Nacimiento.qi212_nom:"";
		if(Nacimiento!=null){
			List<CISECCION_02> nacimientos= getNombreDelosNacimientosMultiples(Nacimiento.id,Nacimiento.hogar_id,Nacimiento.persona_id,Nacimiento.qi215m,Nacimiento.qi215y,Nacimiento.qi213,secciones);
			if(nacimientos.size()>0){
				for(CISECCION_02 n: nacimientos){
					if(Util.esDiferente(Nacimiento.qi212, n.qi212)){
						nombrestodos=n.qi212_nom+" , "+nombrestodos;
					}
				}
			}
		}
		nombrestodos=nombrestodos!=""?nombrestodos.substring(0, nombrestodos.length()-2):nombrestodos;
		return nombrestodos!=""?nombrestodos+" y "+ nombres:nombres;
	}
	public CISECCION_04A getCISECCION_04A(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return (CISECCION_04A)getBean(TABLA_CISECCION_04A,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},CISECCION_04A.class,secciones);
	}
	public boolean esPrimerRegistroCap4a(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT MAX(QI212) AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND  QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+"  " );
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				});
  				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident==ninio_id;
	}
	public String PrimerNombreCap4aVivo(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer maxNinio_id=MaxRegistroCap4aVivo(id, hogar_id, persona_id);
  		String nombre="";
  		query.append("SELECT QI212_NOM ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?" );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),maxNinio_id.toString()
  				});
  		if(cursor.moveToNext()){
  			nombre =  getString("QI212_NOM");
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return nombre;
	}
	
	public CISECCION_04A primerRegistroCap4a(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer maxNinio_id=maxiRegistroCap4a(id, hogar_id, persona_id);
  		String nombre="";
  		query.append("SELECT ID,HOGAR_ID,PERSONA_ID,NINIO_ID,QI409,QI405,QI411_H ")
  			 .append(" FROM ").append(TABLA_CISECCION_04A).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?" );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),maxNinio_id.toString() });
  		CISECCION_04A ci4a = new CISECCION_04A();;
  		if(cursor.moveToNext()){  			
  			ci4a.id =  Integer.parseInt(getString("ID"));
  			ci4a.hogar_id =  Integer.parseInt(getString("HOGAR_ID"));
  			ci4a.persona_id =  Integer.parseInt(getString("PERSONA_ID"));
  			ci4a.ninio_id =  Integer.parseInt(getString("NINIO_ID"));
  			ci4a.qi409 = getString("QI409")!=null? Integer.parseInt(getString("QI409")):0;
  			ci4a.qi405 =getString("QI405")!=null? Integer.parseInt(getString("QI405")):0;
  			ci4a.qi411_h =getString("QI411_H")!=null? Integer.parseInt(getString("QI411_H")):0;
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ci4a;
	}
	
	public CISECCION_02 PrimerRegistroCap2(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer maxNinio_id=maxRegistroCap2(id, hogar_id, persona_id);
  		String nombre="";
  		query.append("SELECT QI212,QI215Y,QI215D,QI215M,QI217,QI220A")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?" );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),maxNinio_id.toString()
  				});
  		
  		CISECCION_02 cis2 = null;
  		
  		if(cursor.moveToNext()){
  			cis2 = new CISECCION_02();
  			cis2.qi212 = Integer.parseInt(getString("QI212"));
  			cis2.qi215d = getString("QI215D");
  			cis2.qi215m = getString("QI215M");
  			cis2.qi215y = Integer.parseInt(getString("QI215Y"));
  			cis2.qi217 = getString("QI217") == null ? null : Integer.parseInt(getString("QI217"));
  			cis2.qi220a= getString("QI220A")==null ? null : Integer.parseInt(getString("QI220A"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cis2;
	}
	
	public CISECCION_02 primerNacimientoCap2(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer maxNinio_id=minRegistroCap2(id, hogar_id, persona_id);
  		String nombre="";
  		query.append("SELECT QI212,QI215Y,QI215D,QI215M,QI217,QI220A")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?" );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),maxNinio_id.toString()
  				});
  		
  		CISECCION_02 cis2 = null;
  		
  		if(cursor.moveToNext()){
  			cis2 = new CISECCION_02();
  			cis2.qi212 = getString("QI212")==null?null: Integer.parseInt(getString("QI212"));
  			cis2.qi215d = getString("QI215D")==null?null:getString("QI215D");
  			cis2.qi215m = getString("QI215M")==null?null:getString("QI215M");
  			cis2.qi215y = getString("QI215Y")==null?null: Integer.parseInt(getString("QI215Y"));
  			cis2.qi217 = getString("QI217")==null ? null : Integer.parseInt(getString("QI217"));
  			cis2.qi220a= getString("QI220A")==null ? null : Integer.parseInt(getString("QI220A"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cis2;
	}
	
	public CISECCION_02T primeraTerminacionCap1_3(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer minTerminacion_id=minRegistroTerminacion(id, hogar_id, persona_id);
  		String nombre="";
  		query.append("SELECT TERMINACION_ID,QI235_M,QI235_Y,QI235_D  ")
  			 .append(" FROM ").append(TABLA_CISECCION_02T).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID = ? " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),minTerminacion_id.toString()
  				});
  		
  		CISECCION_02T cis2 = null;
  		
  		if(cursor.moveToNext()){
  			cis2 = new CISECCION_02T();
  			cis2.terminacion_id= Integer.parseInt(getString("TERMINACION_ID"));
  			cis2.qi235_d = Integer.parseInt(getString("QI235_D"));
  			cis2.qi235_m = Integer.parseInt(getString("QI235_M"));
  			cis2.qi235_y = Integer.parseInt(getString("QI235_Y"));

  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cis2;
	}
	
	public Integer MaxRegistroCap4aVivo(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT MAX(QI212) AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI216='1' AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+"  " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				}); 		
				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident;
	}
	
	public Integer maxiRegistroCap4a(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT MAX(QI212) AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+"  " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				}); 		
				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident;
	}
	
	public Integer maxRegistroCap2(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT MAX(QI212) AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI215Y BETWEEN "+App.ANIOPORDEFECTO+" AND "+App.ANIOPORDEFECTOSUPERIOR+"  " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				}); 		
				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident;
	}
	
	public Integer minRegistroCap2(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT MIN(QI212) AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?  " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				}); 		
				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident;
	}
	
	public Integer minRegistroTerminacion(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT TERMINACION_ID AS ID2 ")
  			 .append(" FROM ").append(TABLA_CISECCION_02T).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ORDER BY QI235_Y ASC,QI235_M ASC  " );
  	
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  				}); 		
				
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ID2"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident;
	}
	
	public boolean saveOrUpdate(CISECCION_04B individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_04B,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",individual,new String[]{"id","hogar_id","persona_id","ninio_id"}, secciones);
	}
	public CISECCION_04B getCISECCION_04B(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return (CISECCION_04B)getBean(TABLA_CISECCION_04B,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},CISECCION_04B.class,secciones);
	}
	public boolean borrar04b(Integer id,Integer hogar_id, Integer persona_id,Integer ninio_id,String pregunta){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND PREGUNTA=?", 
				new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),pregunta});
		dbTX.close();
		return true;
	}
	
//	public boolean borrar04bDit(Integer id,Integer hogar_id, Integer persona_id,Integer ninio_id){
//		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		dbTX.delete(TABLA_CISECCION_DIT, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=? ", 
//				new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()});
//		dbTX.close();
//		return true;
//	}
	public boolean saveOrUpdate(CISECCION_04B_TARJETA bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CISECCION_04B_TARJETA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID= ? AND PREGUNTA=? AND INDICE=?", bean, new String[] {
						"id", "hogar_id", "persona_id","ninio_id","pregunta","indice" }, secciones);
	}
	public CISECCION_04B_TARJETA getSeccion04BTarjeta(Integer id, Integer hogar_id,
			Integer persona_id,Integer ninio_id,String pregunta,Integer indice, SeccionCapitulo... secciones) {
		CISECCION_04B_TARJETA bean = (CISECCION_04B_TARJETA) getBean(
				TABLA_CISECCION_04B_TARJETA,"ID=? AND Hogar_ID=? AND  PERSONA_ID=? AND NINIO_ID = ? AND PREGUNTA= ? AND INDICE=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString(),ninio_id.toString(),pregunta, indice.toString() }, CISECCION_04B_TARJETA.class, secciones);
		return bean;
	}
	public List<CISECCION_04B_TARJETA> getSecciones04BTarjeta(Integer id, Integer hogar_id,
			Integer persona_id,Integer ninio_id,String pregunta, SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_CISECCION_04B_TARJETA,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND PREGUNTA=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString(),ninio_id.toString(),pregunta }, "ID,HOGAR_ID,PERSONA_ID,NINIO_ID,PREGUNTA,INDICE",
						CISECCION_04B_TARJETA.class, secciones);
	}
 	public CISECCION_04B_TARJETA getMayorRegistro(Integer id, Integer hogar_id,Integer persona_id,Integer qi212,String pregunta)
	{
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT INDICE,DIA,MES,ANIO,PREGUNTA,ID,HOGAR_ID,PERSONA_ID,NINIO_ID ")
			 .append(" FROM ").append(TABLA_CISECCION_04B_TARJETA)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND PREGUNTA=? ORDER BY INDICE DESC");
		
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString(),pregunta});
		CISECCION_04B_TARJETA bean = new CISECCION_04B_TARJETA();
		
		if(cursor.moveToNext())
		{
			bean.indice= Util.getInt(getString("INDICE"));
			bean.dia= getString("DIA");
			bean.mes= getString("MES");
			bean.anio= Util.getInt(getString("ANIO"));
			bean.pregunta= getString("PREGUNTA");
			bean.id= Util.getInt(getString("ID"));
			bean.hogar_id= Util.getInt(getString("HOGAR_ID"));
			bean.persona_id= Util.getInt(getString("PERSONA_ID"));
			bean.ninio_id= Util.getInt(getString("NINIO_ID"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return bean;
	}
 	
 	public List<CISECCION_04DIT> getListadoDit(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getBeans(V_DESARROLLOINFANTIL,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID", CISECCION_04DIT.class,secciones);
 	}
 	/*public List<CISECCION_04DIT> getListadoCompleto4Dit(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){

		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT VDI.ID,VDI.QI212_NOM,VDI.HOGAR_ID,VDI.PERSONA_ID,VDI.NRO_ORDEN_NINIO,VDI.QI478,VDIC.ESTADO AS ESTADOCAP4DIT ").
		append(" FROM ").append(V_DESARROLLOINFANTIL).append(" VDI LEFT JOIN ").append(V_SECCION4DIT_COMPLETADO).
		append(" VDIC ON VDI.ID = VDIC.ID AND  VDI.HOGAR_ID=VDIC.HOGAR_ID AND VDI.PERSONA_ID=VDIC.PERSONA_ID AND VDI.NINIODIT_ID=VDIC.NINIODIT_ID ").
		append(" WHERE VDI.ID=? AND VDI.HOGAR_ID=? AND VDI.PERSONA_ID=? ORDER BY VDI.ID,VDI.HOGAR_ID,VDI.PERSONA_ID");
				
		Query query = new Query(sb.toString(), new String[] { id.toString(),hogar_id.toString(),persona_id.toString() });

		return getBeans(query, CISECCION_04DIT.class, secciones[0].getCampos());
 	
 	}*/
 	
 	public boolean getSeccion4DITCompletado(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer ident=0;
  		query.append("SELECT SUM(ESTADO) AS ESTADO ")
//  		.append(" FROM ").append(V_SECCION4DIT_COMPLETADO).append(" ")	 
  		.append(" FROM ").append(V_SECCION4DIT_02_COMPLETADO).append(" ")
  			 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?  " );
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
  						
  		if(cursor.moveToNext()){
  			ident =  Util.getInt(getString("ESTADO"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return ident==0;
	}
  	
 	
	public boolean saveOrUpdate(CISECCION_04B2 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CISECCION_04B2, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean,
				new String[] { "id","hogar_id","persona_id" }, secciones);
	}
	public CISECCION_04B2 getCISECCION_04B2(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_04B2)getBean(TABLA_CISECCION_04B2,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_04B2.class,secciones);
	}
	public boolean VerificarFiltro480(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		query.append("SELECT COUNT(*) AS CANTIDAD ")
  			 .append(" FROM ").append(TABLA_CISECCION_04B)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND (QI473BA = 1 OR QI473BB=1) ");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  			});
  		if(cursor.moveToNext()){
  			cantidad= Util.getInt(getString("CANTIDAD"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cantidad>0;
	}
	public String getNombreDelNacidoMenor(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones){
		CISECCION_02 Nacimiento= new CISECCION_02();
		Nacimiento= (CISECCION_02)getBean(TABLA_CISECCION_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI218 IN(1) ",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_02.class,secciones);
		return Nacimiento!=null?Nacimiento.qi212_nom:"";
	}
	public CISECCION_04DIT getCISECCION_04B_2(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_ninio,SeccionCapitulo... secciones){
//		return (CISECCION_DIT)getBean(TABLA_CISECCION_DIT,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIODIT_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),niniodit_id.toString()},CISECCION_DIT.class,secciones);
		return (CISECCION_04DIT)getBean(V_DESARROLLOINFANTIL,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),nro_orden_ninio.toString()},CISECCION_04DIT.class,secciones);
	}
	public CISECCION_05 getCISECCION_05(Integer id,Integer hogar_id,Integer persona_id,Integer pareja_id,SeccionCapitulo... secciones){
		return (CISECCION_05)getBean(TABLA_CISECCION_05,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PAREJA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),pareja_id.toString()},CISECCION_05.class,secciones);
	}
	public List<CICALENDARIO_COL01_03> getAllCalendarioDescendente(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getBeans(TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QIINDICE DESC",CICALENDARIO_COL01_03.class,secciones);
	}
	public boolean saveOrUpdate(CISECCION_05_07 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_05_07,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
	public boolean saveOrUpdate(CISECCION_05 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_05,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PAREJA_ID=?",individual,new String[]{"id","hogar_id","persona_id","pareja_id"}, secciones);
	}
	public boolean saveOrUpdate(CISECCION_08 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_08,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
	public List<CISECCION_09> getHermanosdeMef(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
//  		return getBeans(TABLA_CISECCION_09, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_09.class, secciones);
		return getBeans(V_SECCION9_COMPLETADO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_09.class,secciones);
  	}
	
	public List<CIHERMANOS> getHermanosdeMefdiferencia(Integer id,Integer hogar_id,Integer persona_id) {
        StringBuilder sbr = new StringBuilder();
        String[] campos = new CIHERMANOS().getFieldsNames();
        sbr.append("SELECT id,hogar_id,persona_id,qinro_orden as persona,qi907 AS EDAD ").append(" ")
               .append(" FROM  T_05_DIG_CISECCION_09 ")
               .append(" WHERE ID = ? ")
               .append(" AND HOGAR_ID = ? ")
               .append(" AND PERSONA_ID = ? ")
        	   .append(" UNION ALL ")
        	   .append(" SELECT id,hogar_id,persona_id, 0 as persona,qi106 as EDAD ")
        	   .append(" FROM  T_05_DIG_CISECCION_01_03 ")
        	   .append(" WHERE ID = ? ")
        	   .append(" AND HOGAR_ID = ? ")
        	   .append(" AND PERSONA_ID = ? ")
 	   		   .append(" ORDER BY EDAD DESC ");
        Query query = new Query(sbr.toString(), id.toString(),hogar_id.toString(),persona_id.toString(),id.toString(),hogar_id.toString(),persona_id.toString());
        List<CIHERMANOS> data= getBeans(query, CIHERMANOS.class, campos);
        return data;
	}
	
	
	public List<MORTALIDAD> getMortalidad(Integer id,Integer hogar_id, SeccionCapitulo...secciones){
		return getBeans(V_MORTALIDAD_HOGAR, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()}, "ID,HOGAR_ID", MORTALIDAD.class, secciones);
//		return getBeans(TABLA_MORTALIDAD, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()}, "ID,HOGAR_ID", MORTALIDAD.class, secciones);
	}
	public boolean TodoLosMiembrosMortalidadhogarCompletados(Integer id,Integer hogar_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append("FROM V_COMPLETADO_MORTALIDAD_HOGAR ")
			.append("WHERE ID=? AND HOGAR_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}
	public boolean getCompletadoTodoLosRegistrosHermanos(Integer id,Integer hogar_id,Integer persona_id){
	Integer total =0;
	total=this.sum("ESTADO",V_SECCION9_COMPLETADO , "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", id.toString(),hogar_id.toString(),persona_id.toString());
	return total==0;
	}

	public boolean DeleteMortalidadHogarTodo(Integer id,Integer hogar_id,Integer persona_id){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		dbTX.delete(TABLA_MORTALIDAD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
  		dbTX.close();
  		return true;
  	}
  	public boolean DeleteMortalidadHogar(MORTALIDAD familiar){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		dbTX.delete(TABLA_MORTALIDAD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?", new String[]{familiar.id.toString(),familiar.hogar_id.toString(),familiar.persona_id.toString(),familiar.nro_orden_id.toString()});
  		Integer maximo = getMaximoRegistroMortalidadHogar(familiar);
	  		if(familiar.nro_orden_id<maximo){
	  			for(int i=familiar.nro_orden_id;i<=maximo-1;i++){
	  				ModificarNumerodeOrdenMortalidaHogar(familiar.id,familiar.hogar_id,familiar.persona_id,i+1,i);
			}
  		}
  		dbTX.close();
  		return true;
  	}
	public Integer getMaximoRegistroMortalidadHogar(MORTALIDAD bean){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer nrodeorden=-1;
  		query.append("SELECT MAX(NRO_ORDEN_ID) AS ORDEN ")
  			 .append(" FROM ").append(TABLA_MORTALIDAD)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),
  			});
  		if(cursor.moveToNext()){
  			nrodeorden= Util.getInt(getString("ORDEN"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return nrodeorden;
  	}
	public boolean ModificarNumerodeOrdenMortalidaHogar(Integer id,Integer hogar_id,Integer persona_id,Integer qinro_orden_nuevo,Integer qinro_orden_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("NRO_ORDEN_ID", qinro_orden_anterior);
		if (dbTX.update(
				TABLA_MORTALIDAD,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qinro_orden_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
  	public boolean DeleteMortalidadMaterna(CISECCION_09 hermano){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		dbTX.delete(TABLA_CISECCION_09, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?", new String[]{hermano.id.toString(),hermano.hogar_id.toString(),hermano.persona_id.toString(),hermano.qinro_orden.toString()});
  		Integer maximo = getMaximoRegistro(hermano);
	  		if(hermano.qinro_orden<maximo){
	  			for(int i=hermano.qinro_orden;i<=maximo-1;i++){
					ModificarNumerodeOrdenMortalidadMaterna(hermano.id,hermano.hogar_id,hermano.persona_id,i+1,i);
			}
  		}
  		dbTX.close();
  		return true;
  	}
  	public Integer getMaximoRegistro(CISECCION_09 bean){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer nrodeorden=-1;
  		query.append("SELECT MAX(QINRO_ORDEN) AS ORDEN ")
  			 .append(" FROM ").append(TABLA_CISECCION_09)
  		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),
  			});
  		if(cursor.moveToNext())
  		{nrodeorden= Util.getInt(getString("ORDEN"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return nrodeorden;
  	}
	public boolean ModificarNumerodeOrdenMortalidadMaterna(Integer id,Integer hogar_id,Integer persona_id,Integer qinro_orden_nuevo,Integer qinro_orden_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("QINRO_ORDEN", qinro_orden_anterior);
		if (dbTX.update(
				TABLA_CISECCION_09,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id.toString(),qinro_orden_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
  	public boolean saveOrUpdate(CISECCION_09 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
  		//reordenarNumerodeOrdenHermano(bean);
		return this.saveOrUpdate(TABLA_CISECCION_09, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?", bean,
				new String[] { "id","hogar_id","persona_id","qinro_orden" }, secciones);
	}
  	public boolean actualizarOrdenCISEc09(CISECCION_09 bean, Integer ordenSource, Integer ordenTarget){
  		
		if(ordenSource>ordenTarget) {
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenTarget,0);
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenSource,ordenTarget);
			for (int i = ordenSource; i > ordenTarget+1; i--) {
					if(modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenSource-1,i))
						ordenSource--;
			}
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,0,ordenTarget+1);
		}
		if(ordenSource<ordenTarget) {
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenTarget,0);
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenSource,ordenTarget);
			for (int i = ordenSource; i < ordenTarget-1; i++) {
					if(modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,ordenSource+1,i))
						ordenSource++;
			}
			modificarNumerodeOrdenCISec9(bean.id,bean.hogar_id,bean.persona_id,0,ordenTarget-1);
		}
		return true;
	}
  	
public boolean actualizarOrdenCHMortalidad(MORTALIDAD bean, Integer ordenSource, Integer ordenTarget){
  		
		if(ordenSource>ordenTarget) {
//			Log.e("","DA");
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenTarget,0);
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenSource,ordenTarget);
			for (int i = ordenSource; i > ordenTarget+1; i--) {
					if(modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenSource-1,i))
						ordenSource--;
			}
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,0,ordenTarget+1);
		}
		if(ordenSource<ordenTarget) {
//			Log.e("","DADSE");
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenTarget,0);
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenSource,ordenTarget);
			for (int i = ordenSource; i < ordenTarget-1; i++) {
					if(modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,ordenSource+1,i))
						ordenSource++;
			}
			modificarNumerodeOrdenCISeccionMortalidadHogar(bean.id,bean.hogar_id,bean.persona_id,0,ordenTarget-1);
		}
		return true;
	}
  
	public boolean ModificacionNroOrdenMortalidad(MORTALIDAD hermano,Integer position){
		Integer maximo=MaximoIDdeMortalidad(hermano);
		Integer contador=maximo;
		if(position<=maximo){
			Log.e("","ANTES");
			MoficarNroOrdenMortalidad(hermano.id,hermano.hogar_id,0,position);
			Log.e("","DESPUES");
			for(int i=maximo; i>position;i--){
				if(getExistsMortalidad(hermano.id,hermano.hogar_id,i)){
					Log.e("","ID: ANTERIOR: "+i);
					Log.e("","ID: NUEVO: "+(maximo-1));
					MoficarNroOrdenMortalidad(hermano.id,hermano.hogar_id,maximo-1,i);
					maximo--;
				}
			}
			MoficarNroOrdenMortalidad(hermano.id,hermano.hogar_id,position+1,0);
		}
		
		return true;
	}
	public boolean getExistsMortalidad(Integer id,Integer hogar_id,Integer nro_orden_id){
		StringBuilder query = new StringBuilder();
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		boolean flag=false;
		query.append("SELECT * ").append(" FROM ")
		.append(TABLA_MORTALIDAD).append(" WHERE ")
		.append("ID=? AND HOGAR_ID=? AND NRO_ORDEN_ID=? AND QH32M_M IS NOT NULL AND QH32M_Y IS NOT NULL ");
		cursor = dbTX.rawQuery(query.toString(), new String[] { id.toString(),
		hogar_id.toString(), nro_orden_id.toString()});
		if (cursor.moveToNext()) {
			flag = true;
		}
		cursor.close();
		cursor = null;
		return flag;
	}
	public Integer MaximoIDdeMortalidad(MORTALIDAD hermano){
		return this.max("NRO_ORDEN_ID", CuestionarioDAO.TABLA_MORTALIDAD , "ID=? AND HOGAR_ID=? AND QH32M_M IS NOT NULL AND QH32M_Y IS NOT NULL ", new String[]{hermano.id.toString(),hermano.hogar_id.toString()});
	}
	public boolean MoficarNroOrdenMortalidad(Integer id,Integer hogar_id,Integer anterior,Integer nuevo){
		SQLiteDatabase dbwTX = this.dbh.getWritableDatabase();
		ContentValues valores= new ContentValues();
		valores.put("NRO_ORDEN_ID", anterior);
		if(dbwTX.update(TABLA_MORTALIDAD, valores,"ID=? AND HOGAR_ID=? AND NRO_ORDEN_ID=?", new String[]{id.toString(),hogar_id.toString(),nuevo.toString()})==1)
		return true;
		else
		return false;
	}
  	public Integer maximoNumHermano(CISECCION_09 bean){
  		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT MAX(QINRO_ORDEN) AS ORDEN ")
			 .append(" FROM ").append(TABLA_CISECCION_09)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
		if(cursor.moveToNext())
			nrodeorden= Util.getInt(getString("ORDEN"));
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return nrodeorden;
	}
  	
    public Integer combioNumHermano(CISECCION_09 bean){
  		
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer nrodeorden=-1;
		query.append("SELECT MAX(QINRO_ORDEN) AS ORDEN ")
			 .append(" FROM ").append(TABLA_CISECCION_09)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=?");
		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
		if(cursor.moveToNext())
			nrodeorden= Util.getInt(getString("ORDEN"));
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return nrodeorden;
	}

	public CISECCION_09 getMortalidadMaterna(Integer id,Integer hogar_id,Integer persona_id,Integer qinro_orden,SeccionCapitulo... secciones){
		return (CISECCION_09)getBean(TABLA_CISECCION_09,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QINRO_ORDEN=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qinro_orden.toString()},CISECCION_09.class,secciones);
	}
	public MORTALIDAD getPersonafallecida(Integer id,Integer hogar_id,Integer persona_id, Integer nro_orden_id,SeccionCapitulo... secciones){
		return (MORTALIDAD)getBean(TABLA_MORTALIDAD,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),nro_orden_id.toString()}, MORTALIDAD.class, secciones);
	}
	public CISECCION_10_01 getCISECCION_10_01(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_10_01)getBean(TABLA_CISECCION_10_01,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_10_01.class,secciones);
	}
	public boolean BorrarNacimiento(Integer id,Integer hogar_id, Integer persona_id, Integer qi212){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
//		dbTX = dbh.getWritableDatabase();
//		dbTX.delete(TABLA_CISECCION_DIT, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
//		dbTX.close();

		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_DIT_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_10_03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CALENDARIO_TRAMO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID IN (SELECT QITRAMO_ID FROM T_05_DIG_CICALENDARIO WHERE ID="+id+" AND HOGAR_ID="+hogar_id+" AND PERSONA_ID="+persona_id+" AND NINIO_ID="+qi212+")", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		dbTX.close();
		
		dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CALENDARIO, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),qi212.toString()});
		dbTX.close();
		
		CISECCION_02 bean = new CISECCION_02(); 
		bean.id = id;
		bean.hogar_id = hogar_id;
		bean.persona_id = persona_id;
		bean.qi212 = qi212;
		reordenarNumerodeOrdenNacimientoSecc2(bean);		
		return true;
	}
	public boolean BorrarPareja(Integer id,Integer hogar_id, Integer persona_id, Integer pareja_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_05, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PAREJA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),pareja_id.toString()});
		dbTX.close();
		
		CISECCION_05 bean = new CISECCION_05(); 
		bean.id = id;
		bean.hogar_id = hogar_id;
		bean.persona_id = persona_id;
		bean.pareja_id = pareja_id;
		ReordenarNumerodeOrdenPareja(bean);
		return true;
	}
	public boolean BorrarTodasLasparejas(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_05, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return true;
	}
	public CISECCION_10_02 getCISECCION_10_02(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return (CISECCION_10_02)getBean(TABLA_CISECCION_10_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},CISECCION_10_02.class,secciones);
	}
	public boolean getFiltro1038byPersona(Integer id,Integer hogar_id,Integer persona_id){
	Integer contador=0;
		contador=this.count("PERSONA_ID", TABLA_CISECCION_10_01, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND (QI1005A IN(1) OR QI1005B IN (1) OR QI1005C IN (1) OR QI1005D IN(1) OR QI1005E IN(1) OR QI1005F IN(1) OR QI1005G IN(1) OR QI1005H IN(1) OR QI1005I IN(1))", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return contador>0;
	}
	public boolean saveOrUpdate(CISECCION_10_03 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_10_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND VACUNA_ID=?",individual,new String[]{"id","hogar_id","persona_id","ninio_id","vacuna_id"}, secciones);
	}
	public CISECCION_10_03 getCISECCION_10_03(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer vacuna_id,SeccionCapitulo... secciones){
		return (CISECCION_10_03)getBean(TABLA_CISECCION_10_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND VACUNA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),vacuna_id.toString()},CISECCION_10_03.class,secciones);
	}
	public List<CISECCION_10_03> getCISECCION_10_03All(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return getBeans(TABLA_CISECCION_10_03,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},"ID,HOGAR_ID,PERSONA_ID",CISECCION_10_03.class,secciones);
	}
  	public boolean BorrarControl(Integer id,Integer hogar_id, Integer persona_id, Integer ninio_id, Integer qi_ctrl){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_10_04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),qi_ctrl.toString()});
		dbTX.close();
		
		CISECCION_10_04 bean = new CISECCION_10_04(); 
		bean.id = id;
		bean.hogar_id = hogar_id;
		bean.persona_id = persona_id;
		bean.ninio_id = ninio_id;
		bean.qi_ctrl = qi_ctrl;
		ReordenarNumerodeOrdenControl(bean);
		return true;
	}
  	public void ReordenarNumerodeOrdenControl(CISECCION_10_04 bean)
	{	Integer orden=MaximoControl(bean);
		Integer id_nuevo=bean.qi_ctrl+1;
		if(bean.qi_ctrl<orden)
		{
			for (int i = bean.qi_ctrl; i < orden; i++) {
					if(ModificarNumerodeOrdenControl(bean.id,bean.hogar_id,bean.persona_id,bean.ninio_id,id_nuevo,i))
						id_nuevo++;
			}
		}
	}
  	public Integer MaximoControl(CISECCION_10_04 bean)
	{
	SQLiteDatabase dbTX = dbh.getWritableDatabase();
	StringBuilder query = new StringBuilder();
	Integer nrodeorden=-1;
	query.append("SELECT MAX(QI_CTRL) AS ORDEN ")
		 .append(" FROM ").append(TABLA_CISECCION_10_04)
	 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?");
	cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.ninio_id.toString()});
	if(cursor.moveToNext())
	{nrodeorden= Util.getInt(getString("ORDEN"));
	}
	cursor.close();
	cursor=null;
	SQLiteDatabase.releaseMemory();
	return nrodeorden;
	}
 	public boolean ModificarNumerodeOrdenControl(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id, Integer qi212_nuevo,Integer qi212_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("QI_CTRL", qi212_anterior);

		dbTX.update(
				TABLA_CISECCION_10_04,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=?  AND NINIO_ID=? AND QI_CTRL=?",
				new String[] { id.toString(), hogar_id.toString(), persona_id.toString(), ninio_id.toString(), qi212_nuevo.toString()});
		return true;
	}
	public CISECCION_10_04 getCISECCION_10_04(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer qi_ctrl,SeccionCapitulo... secciones){
		return (CISECCION_10_04)getBean(TABLA_CISECCION_10_04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),qi_ctrl.toString()},CISECCION_10_04.class,secciones);
	}
	public CISECCION_10_04 getCICONTROL_BYFECHA(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer dia,Integer mes,Integer anio,SeccionCapitulo... secciones){
		return (CISECCION_10_04)getBean(TABLA_CISECCION_10_04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL_DIA=? AND QI_CTRL_MES=? AND QI_CTRL_ANIO=? ",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),dia.toString(),mes.toString(),anio.toString()},CISECCION_10_04.class,secciones);
	}
	public CISECCION_10_04 getCICONTROL_BYFECHASOLOHASTAMES(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer mes,Integer anio,SeccionCapitulo... secciones){
		return (CISECCION_10_04)getBean(TABLA_CISECCION_10_04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL_MES=? AND QI_CTRL_ANIO=? ",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString(),mes.toString(),anio.toString()},CISECCION_10_04.class,secciones);
	}
	public boolean saveOrUpdate(CARATULA_INDIVIDUAL individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CIVISITA,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",individual,new String[]{"id","hogar_id","persona_id"}, secciones);
	}
	public List<CISECCION_10_04> getCISECCION_10_04(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones) {
		return getBeans(TABLA_CISECCION_10_04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?"
				, new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()},"QI_CTRL",CISECCION_10_04.class,secciones);
	}
	public boolean saveOrUpdate(CISECCION_10_04 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_10_04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=? AND QI_CTRL=?",individual,new String[]{"id","hogar_id","persona_id","ninio_id","qi_ctrl"}, secciones);
	}
	public List<CICALENDARIO_TRAMO_COL4> getTramoscol04ByPersona(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getBeans(TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"QIANIO_INI,QIMES_INI",CICALENDARIO_TRAMO_COL4.class,secciones);
	}
	public boolean getCompletadoTramosCol4(Integer id,Integer hogar_id,Integer persona_id, Calendar fecha_actual){
		Integer suma=0;
		suma=this.sum("QICANTIDAD", TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		Integer completo=73-MyUtil.DeterminarIndice(fecha_actual.get(Calendar.YEAR), fecha_actual.get(Calendar.MONTH)+1);
		return suma>=completo;
	}
	public Integer saveOrUpdate(CICALENDARIO_TRAMO_COL4  bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qitramo_id == null) {
			bean.qitramo_id = nextID(dbTX, "QITRAMO_ID", TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
					bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_CALENDARIO_TRAMO4,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?", bean, new String[] { "id", "hogar_id","persona_id","qitramo_id" },
				-1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return bean.qitramo_id;
	}
	public List<CICALENDARIO_COL04> getAllCalendariocolumna04(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getBeans(TABLA_CALENDARIO04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID,QIINDICE ASC",CICALENDARIO_COL04.class,secciones);
	}
	public boolean saveOrUpdate(CICALENDARIO_COL04 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CALENDARIO04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QIINDICE=?", bean,
				new String[] { "id","hogar_id","persona_id","qiindice" }, secciones);
	}
	public CICALENDARIO_TRAMO_COL4 getTramoCasoUnion(Integer id,Integer hogar_id, Integer persona_id,Integer conviviente_id,SeccionCapitulo...secciones){
		return (CICALENDARIO_TRAMO_COL4)getBean(TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND CONVIVIENTE_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),conviviente_id.toString()},CICALENDARIO_TRAMO_COL4.class,secciones);
	}
	public CICALENDARIO_TRAMO_COL4 getTramoByTramoId(Integer id,Integer hogar_id, Integer persona_id,Integer tramo_id,SeccionCapitulo...secciones){
		return (CICALENDARIO_TRAMO_COL4)getBean(TABLA_CALENDARIO_TRAMO4, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QITRAMO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),tramo_id.toString()},CICALENDARIO_TRAMO_COL4.class,secciones);
	}
	public boolean getAlgunNacidoVivo(Integer id,Integer hogar_id, Integer persona_id){
		Integer data= this.count("QI212", TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI216 IN (1)", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return data>0;
	}
	public boolean getAlgunaHijaoHijoQueViveEnelHogar(Integer id, Integer hogar_id,Integer persona_id){
	Integer data = this.count("QI212", TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI219<>0", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
	return data>0;
	}
	public boolean getCompletadoSeccion0103CI(Integer id, Integer hogar_id, Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer estado=-1;
		query.append("SELECT ESTADO ")
			 .append(" FROM ").append(V_SECCION1Y3_COMPLETADO)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			estado = Util.getInt(getString("ESTADO"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return estado==0;
	}
	public boolean getCompletadoSeccion0507CI(Integer id, Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer estado=-1;
		query.append("SELECT ESTADO ")
			 .append(" FROM ").append(V_SECCION5Y7_COMPLETADO)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			estado = Util.getInt(getString("ESTADO"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return estado==0;
	}
	public boolean getCompletadoSeccion008CI(Integer id, Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer estado=-1;
		query.append("SELECT ESTADO ")
			 .append(" FROM ").append(V_SECCION08COMPLETADO_INDIVIDUAL)

		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			estado = Util.getInt(getString("ESTADO"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return estado==0;
	}
	public boolean getCompletadoSeccion10CI(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer estado=-1;
		query.append("SELECT ESTADO ")
			 .append(" FROM ").append(V_SECCION10_01_COMPLETADO)
		 .append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			estado = Util.getInt(getString("ESTADO"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return estado==0;
	}
	public boolean getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer estado=null;
		query.append("SELECT * ")
			 .append(" FROM (") // append(VISITA_INDIVIDUALCOMPLETO)
			 .append(" SELECT S1.ID,S1.HOGAR_ID,S1.PERSONA_ID,S1.QH02_1,S1.QH02_2,CI.NRO_VISITA,CI.QIVRESUL ") 
			 .append(" FROM  T_05_DIG_SECCION01 S1 ")
			.append(" LEFT JOIN  T_05_DIG_CARATULA_INDIVIDUAL CI ON CI.ID=S1.ID AND CI.HOGAR_ID=S1.HOGAR_ID AND CI.PERSONA_ID=S1.PERSONA_ID ")
			.append(" WHERE S1.QH06=2 AND (S1.QH07 BETWEEN 12 AND 49) AND CI.NRO_VISITA=(SELECT MAX(NRO_VISITA) FROM T_05_DIG_CARATULA_INDIVIDUAL WHERE ID=CI.ID AND HOGAR_ID=CI.HOGAR_ID AND PERSONA_ID=CI.PERSONA_ID) ")
			.append(" ) a  WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? ");
			
		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		if(cursor.moveToNext()){
			estado =Util.getInt(getString("QIVRESUL"));
		}
//		Log.e("QIVRESUL",""+estado);
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return estado!=null?!Util.esDiferente(estado, App.INDIVIDUAL_RESULTADO_COMPLETADO) :false;
	}
	
	public Integer getNiniosMenoresACincoAniosporMef(Integer id,Integer hogar_id,Integer persona_id){
		Integer contador=0;
		contador=this.count("QI212", TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI217<5", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return contador;
	}
	public Integer getNiniosConCarnetbyPersonaId(Integer id,Integer hogar_id,Integer persona_id){
		Integer contador=0;
		contador=this.count("NINIO_ID", TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND (QI454 IN(1))", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return contador;
	}
	public Integer getNiniosConCarnetVistabyPersonaId(Integer id,Integer hogar_id,Integer persona_id){
		Integer contador=0;
		contador=this.count("NINIO_ID", TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI454 IN (1)", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return contador;
	}
	public boolean BorrarTerminaciones(Integer id,Integer hogar_id,Integer persona_id, Integer terminacion_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_02T, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND TERMINACION_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),terminacion_id.toString()});
		return true;
	}
	
	public CISECCION_04A getCISECCION_04A411h(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return (CISECCION_04A)getBean(TABLA_CISECCION_04A,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CISECCION_04A.class,secciones);
	}
	public boolean getvalidarSiesqueLeHicieronExamenDeVIH(Integer id, Integer hogar_id, Integer persona_id){
		Integer cantidad=0;
		cantidad= this.count("QI411_H", TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI411_H IN(1)", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		return cantidad>0;
	}
	public CSSECCION_08 getFecha_NacimientoNinioSeccion08(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return (CSSECCION_08)getBean(TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID_NINIO=?",new String[]{id.toString(),hogar_id.toString(), ninio.toString()},CSSECCION_08.class,capitulos);
	}
	public Seccion04_05 getFecha_NacimientoNinioSeccion04_05(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return (Seccion04_05)getBean(TABLA_SECCION04_05, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",new String[]{id.toString(),hogar_id.toString(), ninio.toString()},Seccion04_05.class,capitulos);
	}
	public CISECCION_02 getFecha_NacimientoNinioSeccionCIS2(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return (CISECCION_02)getBean(TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND QI219=?",new String[]{id.toString(),hogar_id.toString(), ninio.toString()},CISECCION_02.class,capitulos);
	}
	
	public List<CISECCION_04B> getNiniosVivosYquetienenLatarjetadevacunacion(Integer id,Integer hogar_id){
		return getBeans(TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND QI454 IN (1) ", CISECCION_04B.class,id.toString(),hogar_id.toString());
	}
	public boolean getTieneAlMenosUnControl(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		Integer cantidad=this.count("QI_CTRL", TABLA_CISECCION_10_04, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()});
		return cantidad>0;
	}
	public boolean getTieneAlMenosUnaVacuna(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		Integer cantidad=this.count("VACUNA_ID", TABLA_CISECCION_10_03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NINIO_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()});
		return cantidad>0;
	}
	public List<MORTALIDAD> getAllMortalidad(Integer id){
	StringBuilder sbr = new StringBuilder();
	String[] campos = new MORTALIDAD().getFieldsNames();
	sbr.append("SELECT * ").append(" ")
		.append("FROM ").append(TABLA_MORTALIDAD)
		.append(" WHERE ID = ? ");
	Query query = new Query(sbr.toString(), id.toString());
	return getBeans(query, MORTALIDAD.class, campos);
	}
	public boolean getExisteAlgunHogarquefalteResponderPregunta224(Integer id,Integer hogar_id){
		Integer cantidad=-1;
	    cantidad=this.count("HOGAR_ID", VISTA_HOGARPREGUNTAQH224, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
		return cantidad==0;
	}
	public Endes_Resultado_vivienda getResultadovivienda_general(Integer id,SeccionCapitulo...secciones){
		Endes_Resultado_vivienda viv=	 (Endes_Resultado_vivienda)getBean(VISTA_COMPLETADO_VIVIENDA_FINAL,"ID=?",new String[]{ id.toString()},Endes_Resultado_vivienda.class,secciones);
		Log.e("resultadovivi",""+viv);
		 return viv;
	}
	public Visita_Viv getUltima_visitaVivienda(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Visita_Viv().getFieldsNames();
		sbr.append(" SELECT * ").append(" ")
			.append(" FROM ").append(TABLA_VISITA_VIV)
			.append(" WHERE ID = "+id+" AND NRO_VISITA=(SELECT MAX(NRO_VISITA)  AS NRO_VISITA FROM "+TABLA_VISITA_VIV+" WHERE ID = "+id+") ");
		Query query = new Query(sbr.toString());
		return (Visita_Viv) getBean(query, Visita_Viv.class, campos);
		
	}
	
	public Visita getResultadosVisitaHogar(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Visita().getFieldsNames();
		sbr.append(" SELECT *").append(" ")
			.append(" FROM ").append(TABLA_VISITA)
			.append(" WHERE ID = "+id+" AND QHVRESUL IS NULL ");
		Query query = new Query(sbr.toString());
		return (Visita) getBean(query, Visita.class, campos);
		
	}
	public boolean saveOrUpdate_VisitaVivienda(Visita_Viv bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VISITA_VIV, "ID=? AND NRO_VISITA=?", bean,
				new String[] { "id", "nro_visita"}, secciones);
	}
	
//	public String getUltimaFechaDelCuestionario(Integer id) {
//		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		StringBuilder sbr = new StringBuilder();
//		String fecha="";
//		sbr.append(" SELECT *").append(" ")
//		.append(" FROM ").append(VISTA_V_HORA_ULTIMA_VISITA)
//		.append(" WHERE ID = ?");
//		cursor = dbTX.rawQuery(sbr.toString(),new String[]{id.toString()});
//		if(cursor.moveToNext()){
//			fecha =getString("MAX_FECHA_FIN");
//		}
////		cursor.close();
//		cursor=null;
//		SQLiteDatabase.releaseMemory();
//		return fecha;	
//	}
	
	public boolean EliminarUltimaVisitaVivienda(Integer id,Integer nro_visita){
		this.borrar(TABLA_VISITA_VIV, "ID=? AND NRO_VISITA=?", new String[]{id.toString(),nro_visita.toString()});
		return true;
	}
	public DISCAPACIDAD getDiscapacidad_byCuestionario_id(Integer id,Integer hogar_id,Integer cuestionario_id,Integer persona_id,SeccionCapitulo... secciones) {
		return  (DISCAPACIDAD) getBean(TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? ", new String[]{id.toString(),hogar_id.toString(),cuestionario_id.toString(),persona_id.toString()},DISCAPACIDAD.class,secciones); 
	}
	public boolean BorrarPersonadeDiscapacidadporcuestionario(Integer id,Integer hogar_id, Integer persona_id,Integer cuestionario_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND CUESTIONARIO_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),cuestionario_id.toString()});
		dbTX.close();
		return true;
	}
	public boolean saveOrUpdate_Discapacidad(DISCAPACIDAD bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID=?", bean,
				new String[] { "id", "hogar_id","cuestionario_id","persona_id","ninio_id"}, secciones);
	}
	
	public DISCAPACIDAD getSeccion02IndividualParaDiscapacidad(Integer id, Integer hogar_id, Integer cuestionario_id,Integer persona_id,Integer ninio_id, SeccionCapitulo... secciones) {
		/*DISCAPACIDAD bean = (DISCAPACIDAD) getBean(
				TABLA_DISCAPACIDAD,"ID=? AND HOGAR_ID=? AND  PERSONA_ID=? AND CUESTIONARIO_ID=? AND NINIO_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString(),cuestionario_id.toString(),ninio_id.toString() }, DISCAPACIDAD.class, secciones);*/
		StringBuilder sbr = new StringBuilder();
		String[] campos = new DISCAPACIDAD().getFieldsNames();
		sbr.append(" SELECT *").append(" ")
			.append(" FROM ").append(TABLA_DISCAPACIDAD)
			.append(" WHERE ID = "+id+" AND HOGAR_ID = "+hogar_id+" AND CUESTIONARIO_ID = "+cuestionario_id+" AND PERSONA_ID = "+persona_id+" AND NINIO_ID = "+ninio_id);
		Query query = new Query(sbr.toString());
		return (DISCAPACIDAD) getBean(query, DISCAPACIDAD.class, campos);
//		return bean;
	}
	public List<DISCAPACIDAD> getListado_Discapacidad(Integer id) {
		return getBeans(TABLA_DISCAPACIDAD, "ID=?","id", DISCAPACIDAD.class, id.toString());
	}
	public boolean saveOrUpdate(DISCAPACIDAD bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=?  AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID=? ", bean, new String[]{"id","hogar_id","cuestionario_id","persona_id","ninio_id"}, -1, -1, campos);
	}

	public CISECCION_04DIT_02 getCISECCION_04B_dit2(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_ninio,SeccionCapitulo... secciones){
		return (CISECCION_04DIT_02)getBean(V_DESARROLLOINFANTIL_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),nro_orden_ninio.toString()},CISECCION_04DIT_02.class,secciones);
	}
	public boolean saveOrUpdate(CISECCION_04DIT_02 individual, SeccionCapitulo... secciones) throws java.sql.SQLException{
		return this.saveOrUpdate(TABLA_CISECCION_DIT_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=?",individual,new String[]{"id","hogar_id","persona_id","nro_orden_ninio"}, secciones);
	}
	public List<CISECCION_04DIT_02> getListadoDit_02(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getBeans(V_DESARROLLOINFANTIL_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID", CISECCION_04DIT_02.class,secciones);
 	}
	public boolean borrar04bDit_02(Integer id,Integer hogar_id, Integer persona_id,Integer ninio_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_CISECCION_DIT_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_ORDEN_NINIO=? ", new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),ninio_id.toString()});
		dbTX.close();
		return true;
	}
	public boolean BorrarDiscapacidad(Integer id,Integer hogar_id,Integer persona_id, Integer cuestionario_id,Integer ninio_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		dbTX.delete(TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=? AND NINIO_ID=?", new String[]{id.toString(),hogar_id.toString(),cuestionario_id.toString(),persona_id.toString(),ninio_id.toString()});
		return true;
	}
	public List<Seccion01> getSeccion01ListbyMarcoyHogar_precenso(Integer id,Integer hogar_id,SeccionCapitulo... secciones) {
		return getBeans(VISTA_ESTADOPERSONAS_PRECENSOS, "ID=? AND HOGAR_ID=? "
				, new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion01.class,secciones);
	}
	
	public boolean getCompletadoSeccionDiscapacidadCI(Integer id, Integer hogar_id,Integer cuestionario_id,Integer persona_id){
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();

		Integer total= this.sum("ESTADO", VISTA_DISCAPACIDAD_INDIVIDUAL, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=?",new String[]{id.toString(),hogar_id.toString(),cuestionario_id.toString(),persona_id.toString()});
		Log.e("","TOTAL: "+total);
		return total==0;
//		int total = -1;
//		Log.e("ID ",""+id);
//		Log.e("HOGAR_ID ",""+hogar_id);
//		Log.e("CUESTIONARIO_ID ",""+cuestionario_id);
//		Log.e("PERSONA_ID ",""+persona_id);
//		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
//			.append("FROM V_DISCAPACIDAD_INDIVIDUAL ")
//			.append("WHERE ID=? AND HOGAR_ID = ? AND CUESTIONARIO_ID=? AND PERSONA_ID=?");
//				cursor = dbr.rawQuery(query.toString(),
//			new String[] { id.toString(), hogar_id.toString(), cuestionario_id.toString(), persona_id.toString()});
//			if (cursor.moveToNext()) {
//				Log.e("","nunca entro");
//				total = Util.getInt(getString("CANTIDAD"));
//			}
//			cursor.close();
//			cursor = null;
//			Log.e("suma",""+total);
//			SQLiteDatabase.releaseMemory();
//			return total == 0;
	}
	
	
	public boolean ExisteninioDe61Hasta71ParaDit(Integer id,Integer hogar_id,Integer persona_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer cantidad=0;
  		query
  		.append("SELECT COUNT(*) AS CANTIDAD ")
  		.append(" FROM ").append(TABLA_CISECCION_02).append(" T2 ") 
  		.append(" INNER JOIN ").append(TABLA_CISECCION_DIT_02).append(" TD ") 
  		.append(" ON T2.ID=TD.ID AND T2.HOGAR_ID=TD.HOGAR_ID AND T2.PERSONA_ID=TD.PERSONA_ID ") 
  		.append(" WHERE T2.ID=? AND T2.HOGAR_ID=? AND T2.PERSONA_ID=? AND T2.QI218 =1 AND TD.QI478 BETWEEN "+App.SOLODITMIN+" AND "+App.SOLODITMAX+" ");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString(),persona_id.toString()
  			});
  		if(cursor.moveToNext()){
  			cantidad= Util.getInt(getString("CANTIDAD"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return cantidad>0;
	}

	public boolean getPersonasMenoresa16Seccion1(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer TotalMenorde16=0;
  		Integer TotalPersonas=0;
  		query  		
  		.append(" SELECT C.ID AS ID , C.HOGAR_ID AS HOGAR_ID , TotalPersonas , TotalMenorde16 FROM ( ")
  		.append(" (SELECT ID, HOGAR_ID, COUNT(HOGAR_ID) AS TotalPersonas  FROM ")
  		.append(TABLA_SECCION01) .append("  GROUP BY ID, HOGAR_ID) A  ")
  		.append(" LEFT JOIN   ")
  		.append(" (SELECT ID, HOGAR_ID, SUM(case when QH07 BETWEEN 0 AND 15   then 1  else 0 end ) AS TotalMenorde16  FROM ")
  		.append(TABLA_SECCION01) .append(" GROUP BY ID, HOGAR_ID ) B   ON A.ID=B.ID AND A.HOGAR_ID=B.HOGAR_ID ")
  		.append(" ) C ")
  		.append(" WHERE C.ID=? AND C.HOGAR_ID=? ");
  		
  		cursor = dbTX.rawQuery(query.toString(),new String[]{id.toString(),hogar_id.toString()});
  		if(cursor.moveToNext()){
  			TotalPersonas= Util.getInt(getString("TotalPersonas"));
  			TotalMenorde16= Util.getInt(getString("TotalMenorde16"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return TotalPersonas==TotalMenorde16;
	}
	public boolean BorrarCuestionarioIndividual(Seccion01 bean, SQLiteDatabase dbTX){
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
			boolean flag=true;
			//= borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = BorrarTablasDelCuestionarioIndivual(bean,dbTX, flag);	
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
		return true;
		
	}
	public boolean BorrarTablasDelCuestionarioIndivual(Seccion01 bean, SQLiteDatabase dbTX,boolean flag){
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02T ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_DISCAPACIDAD, "ID=? AND HOGAR_ID=? AND CUESTIONARIO_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),App.CUEST_ID_INDIVIDUAL.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04A, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_DIT_02,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_01, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_03 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_10_04 , "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CALENDARIO_TRAMO,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_05 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_08 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_09 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_04B2 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_05_07 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_01_03 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		return flag;
	}
	public boolean BorrarCuestionarioDeSalud(Seccion01 bean, SQLiteDatabase dbTX){
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
			boolean flag=true;
			//= borrar(dbTX, CuestionarioDAO.TABLA_CISECCION_02, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND QI212=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString(),bean.qi212.toString());
			flag = BorrarTablasDelCuestionarioSalud(bean,dbTX, flag);	
			if (!isTX) {
				if(flag){
					commitTX(dbTX);
				}
				endTX(dbTX);
				SQLiteDatabase.releaseMemory();
			}
		return true;
		
	}
	public boolean BorrarTablasDelCuestionarioSalud(Seccion01 bean, SQLiteDatabase dbTX,boolean flag){
		//flag = borrar(dbTX, CuestionarioDAO.TABLA_CSSECCION_08, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_SALUD ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		flag = borrar(dbTX, CuestionarioDAO.TABLA_CAP04_07 ,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString());
		return flag;
	}
	public boolean ModificarRecuperacionDeLaMef(Seccion01 bean){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("ESTADO_REC_CI", 1);
		valores.put("F_REC_CI", Util.getFechaActualToString());
		if (dbTX.update(TABLA_SECCION01,valores,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[] { bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean ModificarRecuperacionDeCuestionarioSalud(Seccion01 bean){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("ESTADO_REC_CS", 1);
		valores.put("F_REC_CS", Util.getFechaActualToString());
		if (dbTX.update(TABLA_SECCION01,valores,"ID=? AND HOGAR_ID=? AND PERSONA_ID=?",new String[] { bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean ModificarNumeroDeVivienda(Integer id,String nselv){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("NSELV",nselv);
		if (dbTX.update(
				TABLA_VIVIENDA,
				valores,
				"ID=? ",
				new String[] { id.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}

	
	
	
	public boolean saveOrUpdate(MIGRACION bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_MIGRACION,
				"ID=? AND Hogar_ID=? AND QH34A=?", bean, new String[] {
						"id", "hogar_id", "qh34a" }, -1, -1, campos);
	}
	public boolean saveOrUpdate(IMIGRACION bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_IMIGRACION,
				"ID=? AND Hogar_ID=? AND QH35A=?", bean, new String[] {
						"id", "hogar_id", "qh35a" }, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(MIGRACION bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_MIGRACION, "ID=? AND Hogar_ID=? AND QH34A=?", bean,
				new String[] { "id", "hogar_id","qh34a" }, secciones);
	}
	public boolean saveOrUpdate(IMIGRACION bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_IMIGRACION, "ID=? AND Hogar_ID=? AND QH35A=?", bean,
				new String[] { "id", "hogar_id","qh35a" }, secciones);
	}

	public boolean saveOrUpdate(MIGRACION bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qh34a == null) {
			bean.qh34a = nextID(dbTX, "QH34A", TABLA_MIGRACION,
					"ID=? AND HOGAR_ID=? AND QH34A", bean.id.toString(),
					bean.hogar_id.toString(),bean.qh34a.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_MIGRACION,
				"ID=? AND HOGAR_ID=? AND QH34A=?", bean, new String[] {
						"id", "hogar_id", "qh34a" }, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public boolean saveOrUpdate(IMIGRACION bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.qh35a == null) {
			bean.qh35a = nextID(dbTX, "QH35A", TABLA_IMIGRACION,
					"ID=? AND HOGAR_ID=? AND QH35A", bean.id.toString(),
					bean.hogar_id.toString(),bean.qh35a.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}

		boolean flag = saveOrUpdate(dbTX, TABLA_IMIGRACION,
				"ID=? AND HOGAR_ID=? AND QH35A=?", bean, new String[] {
						"id", "hogar_id", "qh35a" }, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public List<MIGRACION> getMigracion(Integer id,Integer hogar_id, SeccionCapitulo...secciones){
		return getBeans(V_MIGRACION_HOGAR, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()}, "ID,HOGAR_ID", MIGRACION.class, secciones);
	}
	public List<IMIGRACION> getImigracion(Integer id,Integer hogar_id, SeccionCapitulo...secciones){
		return getBeans(V_IMIGRACION_HOGAR, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()}, "ID,HOGAR_ID", IMIGRACION.class, secciones);
	}
	
	public List<MIGRACION> getAllMigracion(Integer id){
		StringBuilder sbr = new StringBuilder();
		String[] campos = new MIGRACION().getFieldsNames();
		sbr.append("SELECT * ").append(" ")
			.append("FROM ").append(TABLA_MIGRACION)
			.append(" WHERE ID = ? ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, MIGRACION.class, campos);
	}	
	public List<IMIGRACION> getAllImigracion(Integer id){
		StringBuilder sbr = new StringBuilder();
		String[] campos = new IMIGRACION().getFieldsNames();
		sbr.append("SELECT * ").append(" ")
			.append("FROM ").append(TABLA_IMIGRACION)
			.append(" WHERE ID = ? ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, IMIGRACION.class, campos);
	}
	
	
	public boolean DeleteMigracionHogarTodo(Integer id,Integer hogar_id){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		dbTX.delete(TABLA_MIGRACION, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
  		dbTX.close();
  		return true;
  	}
	public boolean DeleteImigracionHogarTodo(Integer id,Integer hogar_id){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		dbTX.delete(TABLA_IMIGRACION, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
  		dbTX.close();
  		return true;
  	}
	public boolean DeleteMigracionHogar(MIGRACION familiar){
	  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
	  		dbTX.delete(TABLA_MIGRACION, "ID=? AND HOGAR_ID=? AND QH34A=?", new String[]{familiar.id.toString(),familiar.hogar_id.toString(),familiar.qh34a.toString()});
	  		Integer maximo = getMaximoRegistroMigracionHogar(familiar);
		  		if(familiar.qh34a<maximo){
		  			for(int i=familiar.qh34a;i<=maximo-1;i++){
		  				ModificarNumerodeOrdenMigracionHogar(familiar.id,familiar.hogar_id,i+1,i);
				}
	  		}
	  		dbTX.close();
	  		return true;
	}
		
	public boolean DeleteImigracionHogar(IMIGRACION familiar){
	  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
	  		dbTX.delete(TABLA_IMIGRACION, "ID=? AND HOGAR_ID=? AND QH35A=?", new String[]{familiar.id.toString(),familiar.hogar_id.toString(),familiar.qh35a.toString()});
	  		Integer maximo = getMaximoRegistroImigracionHogar(familiar);
		  		if(familiar.qh35a<maximo){
		  			for(int i=familiar.qh35a;i<=maximo-1;i++){
		  				ModificarNumerodeOrdenImigracionHogar(familiar.id,familiar.hogar_id,i+1,i);
				}
	  		}
	  		dbTX.close();
	  		return true;
	}
	
	
	
	public Integer getMaximoRegistroMigracionHogar(MIGRACION bean){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer nrodeorden=-1;
  		query.append("SELECT MAX(QH34A) AS ORDEN ")
  			 .append(" FROM ").append(TABLA_MIGRACION)
  		 .append(" WHERE ID=? AND HOGAR_ID=?");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),
  			});
  		if(cursor.moveToNext()){
  			nrodeorden= Util.getInt(getString("ORDEN"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return nrodeorden;
  	}
	public Integer getMaximoRegistroImigracionHogar(IMIGRACION bean){
  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
  		StringBuilder query = new StringBuilder();
  		Integer nrodeorden=-1;
  		query.append("SELECT MAX(QH35A) AS ORDEN ")
  			 .append(" FROM ").append(TABLA_IMIGRACION)
  		 .append(" WHERE ID=? AND HOGAR_ID=?");
  		cursor = dbTX.rawQuery(query.toString(),new String[]{bean.id.toString(),bean.hogar_id.toString(),
  			});
  		if(cursor.moveToNext()){
  			nrodeorden= Util.getInt(getString("ORDEN"));
  		}
  		cursor.close();
  		cursor=null;
  		SQLiteDatabase.releaseMemory();
  		return nrodeorden;
  	}
	public boolean ModificarNumerodeOrdenMigracionHogar(Integer id,Integer hogar_id,Integer qinro_orden_nuevo,Integer qinro_orden_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("QH34A", qinro_orden_anterior);
		if (dbTX.update(
				TABLA_MIGRACION,
				valores,
				"ID=? AND HOGAR_ID=? AND QH34A=?",
				new String[] { id.toString(), hogar_id.toString() , qinro_orden_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean ModificarNumerodeOrdenImigracionHogar(Integer id,Integer hogar_id,Integer qinro_orden_nuevo,Integer qinro_orden_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();
		ContentValues valores = new ContentValues();
		valores.put("QH35A", qinro_orden_anterior);
		if (dbTX.update(
				TABLA_IMIGRACION,
				valores,
				"ID=? AND HOGAR_ID=? AND QH35A=?",
				new String[] { id.toString(), hogar_id.toString() , qinro_orden_nuevo.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public MIGRACION getPersonaMigracion(Integer id,Integer hogar_id, Integer qh34a,SeccionCapitulo... secciones){
		return (MIGRACION)getBean(TABLA_MIGRACION,"ID=? AND HOGAR_ID=? AND QH34A=?", new String[]{id.toString(),hogar_id.toString(),qh34a.toString()}, MIGRACION.class, secciones);
	}
	public IMIGRACION getPersonaImigracion(Integer id,Integer hogar_id, Integer qh35a,SeccionCapitulo... secciones){
		return (IMIGRACION)getBean(TABLA_IMIGRACION,"ID=? AND HOGAR_ID=? AND QH35A=?", new String[]{id.toString(),hogar_id.toString(),qh35a.toString()}, IMIGRACION.class, secciones);
	}
	
	public boolean TodoLosMiembrosMigracionhogarCompletados(Integer id,Integer hogar_id){
		Integer total =0;
		total=this.sum("ESTADO",V_MIGRACION_HOGAR , "ID=? AND HOGAR_ID=? ", id.toString(),hogar_id.toString());
		return total==0;
	}
	public boolean TodoLosMiembrosImigracionhogarCompletados(Integer id,Integer hogar_id){
		Integer total =0;
		total=this.sum("ESTADO",V_IMIGRACION_HOGAR , "ID=? AND HOGAR_ID=? ", id.toString(),hogar_id.toString());
		return total==0;
	}
	
	
	
//	
//	
//	public boolean actualizarOrdenCHMigracion(MIGRACION bean, Integer ordenSource, Integer ordenTarget){
//  		
//		if(ordenSource>ordenTarget) {
////			Log.e("","DA");
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenTarget,0);
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenSource,ordenTarget);
//			for (int i = ordenSource; i > ordenTarget+1; i--) {
//					if(modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenSource-1,i))
//						ordenSource--;
//			}
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,0,ordenTarget+1);
//		}
//		if(ordenSource<ordenTarget) {
////			Log.e("","DADSE");
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenTarget,0);
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenSource,ordenTarget);
//			for (int i = ordenSource; i < ordenTarget-1; i++) {
//					if(modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,ordenSource+1,i))
//						ordenSource++;
//			}
//			modificarNumerodeOrdenCISeccionMigracionHogar(bean.id,bean.hogar_id,0,ordenTarget-1);
//		}
//		return true;
//	}
//	public boolean actualizarOrdenCHImigracion(IMIGRACION bean, Integer ordenSource, Integer ordenTarget){
//  		
//		if(ordenSource>ordenTarget) {
////			Log.e("","DA");
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenTarget,0);
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenSource,ordenTarget);
//			for (int i = ordenSource; i > ordenTarget+1; i--) {
//					if(modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenSource-1,i))
//						ordenSource--;
//			}
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,0,ordenTarget+1);
//		}
//		if(ordenSource<ordenTarget) {
////			Log.e("","DADSE");
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenTarget,0);
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenSource,ordenTarget);
//			for (int i = ordenSource; i < ordenTarget-1; i++) {
//					if(modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,ordenSource+1,i))
//						ordenSource++;
//			}
//			modificarNumerodeOrdenCISeccionImigracionHogar(bean.id,bean.hogar_id,0,ordenTarget-1);
//		}
//		return true;
//	}
//	public boolean modificarNumerodeOrdenCISeccionMigracionHogar(Integer id,Integer hogar_id,Integer nro_orden_nuevo,Integer nro_orden_anterior){
//  		
//  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		ContentValues valores = new ContentValues();
//		valores.put("QH34A", nro_orden_anterior);
//		
//		if (dbTX.update(
//				TABLA_MIGRACION,
//				valores,
//				"ID=? AND HOGAR_ID=? AND QH34A=? ",
//				new String[] { id.toString(), hogar_id.toString(),nro_orden_nuevo.toString()}) == 1) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//	public boolean modificarNumerodeOrdenCISeccionImigracionHogar(Integer id,Integer hogar_id,Integer nro_orden_nuevo,Integer nro_orden_anterior){
//  		
//  		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		ContentValues valores = new ContentValues();
//		valores.put("QH35A", nro_orden_anterior);
//		
//		if (dbTX.update(
//				TABLA_IMIGRACION,
//				valores,
//				"ID=? AND HOGAR_ID=? AND QH35A=? ",
//				new String[] { id.toString(), hogar_id.toString(),nro_orden_nuevo.toString()}) == 1) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//	
//	
//	public boolean ModificacionNroOrdenMigracion(MIGRACION hermano,Integer position){
//		Integer maximo=MaximoIDdeMigracion(hermano);
//		Integer contador=maximo;
//		if(position<=maximo){
//			Log.e("","ANTES");
//			MoficarNroOrdenMigracion(hermano.id,hermano.hogar_id,0,position);
//			Log.e("","DESPUES");
//			for(int i=maximo; i>position;i--){
//				if(getExistsMigracion(hermano.id,hermano.hogar_id,i)){
//					Log.e("","ID: ANTERIOR: "+i);
//					Log.e("","ID: NUEVO: "+(maximo-1));
//					MoficarNroOrdenMigracion(hermano.id,hermano.hogar_id,maximo-1,i);
//					maximo--;
//				}
//			}
//			MoficarNroOrdenMigracion(hermano.id,hermano.hogar_id,position+1,0);
//		}
//		
//		return true;
//	}
//	public boolean ModificacionNroOrdenImigracion(IMIGRACION hermano,Integer position){
//		Integer maximo=MaximoIDdeImigracion(hermano);
//		Integer contador=maximo;
//		if(position<=maximo){
//			Log.e("","ANTES");
//			MoficarNroOrdenImigracion(hermano.id,hermano.hogar_id,0,position);
//			Log.e("","DESPUES");
//			for(int i=maximo; i>position;i--){
//				if(getExistsImigracion(hermano.id,hermano.hogar_id,i)){
//					Log.e("","ID: ANTERIOR: "+i);
//					Log.e("","ID: NUEVO: "+(maximo-1));
//					MoficarNroOrdenImigracion(hermano.id,hermano.hogar_id,maximo-1,i);
//					maximo--;
//				}
//			}
//			MoficarNroOrdenImigracion(hermano.id,hermano.hogar_id,position+1,0);
//		}
//		
//		return true;
//	}
//	public boolean getExistsMigracion(Integer id,Integer hogar_id,Integer qh34a){
//		StringBuilder query = new StringBuilder();
//		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		boolean flag=false;
//		query.append("SELECT * ").append(" FROM ")
//		.append(TABLA_MIGRACION).append(" WHERE ")
//		.append("ID=? AND HOGAR_ID=? AND QH34A=? AND QH34H IS NOT NULL ");
//		cursor = dbTX.rawQuery(query.toString(), new String[] { id.toString(),
//		hogar_id.toString(), qh34a.toString()});
//		if (cursor.moveToNext()) {
//			flag = true;
//		}
//		cursor.close();
//		cursor = null;
//		return flag;
//	}
//	public boolean getExistsImigracion(Integer id,Integer hogar_id,Integer qh35a){
//		StringBuilder query = new StringBuilder();
//		SQLiteDatabase dbTX = dbh.getWritableDatabase();
//		boolean flag=false;
//		query.append("SELECT * ").append(" FROM ")
//		.append(TABLA_IMIGRACION).append(" WHERE ")
//		.append("ID=? AND HOGAR_ID=? AND QH35A=? AND QH35E IS NOT NULL ");
//		cursor = dbTX.rawQuery(query.toString(), new String[] { id.toString(),
//		hogar_id.toString(), qh35a.toString()});
//		if (cursor.moveToNext()) {
//			flag = true;
//		}
//		cursor.close();
//		cursor = null;
//		return flag;
//	}
//	public Integer MaximoIDdeMigracion(MIGRACION hermano){
//		return this.max("QH34A", CuestionarioDAO.TABLA_MIGRACION , "ID=? AND HOGAR_ID=? AND QH34H IS NOT NULL ", new String[]{hermano.id.toString(),hermano.hogar_id.toString()});
//	}
//	public Integer MaximoIDdeImigracion(IMIGRACION hermano){
//		return this.max("QH35A", CuestionarioDAO.TABLA_IMIGRACION , "ID=? AND HOGAR_ID=? AND QH35H IS NOT NULL ", new String[]{hermano.id.toString(),hermano.hogar_id.toString()});
//	}
//	public boolean MoficarNroOrdenMigracion(Integer id,Integer hogar_id,Integer anterior,Integer nuevo){
//		SQLiteDatabase dbwTX = this.dbh.getWritableDatabase();
//		ContentValues valores= new ContentValues();
//		valores.put("QH34A", anterior);
//		if(dbwTX.update(TABLA_MIGRACION, valores,"ID=? AND HOGAR_ID=? AND QH34A=?", new String[]{id.toString(),hogar_id.toString(),nuevo.toString()})==1)
//		return true;
//		else
//		return false;
//	}
//	public boolean MoficarNroOrdenImigracion(Integer id,Integer hogar_id,Integer anterior,Integer nuevo){
//		SQLiteDatabase dbwTX = this.dbh.getWritableDatabase();
//		ContentValues valores= new ContentValues();
//		valores.put("QH35A", anterior);
//		if(dbwTX.update(TABLA_IMIGRACION, valores,"ID=? AND HOGAR_ID=? AND QH35A=?", new String[]{id.toString(),hogar_id.toString(),nuevo.toString()})==1)
//		return true;
//		else
//		return false;
//	}
//	

//	

//	
//	

	
}