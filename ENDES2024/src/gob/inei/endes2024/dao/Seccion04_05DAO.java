package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.DatabaseHelper;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Seccion04_05DAO extends SQLiteDAO{
	public static Seccion04_05DAO INSTANCE; 
	public static final String TABLA_SECCION01 = "T_05_DIG_SECCION01";
	public static final String TABLA_SECCION04 = "T_05_DIG_SECCCION04_05";
	public static final String TABLA_SECCION04_DET = "T_05_DIG_AUDITORIA";
	public static final String VISTA_LISTADO_SECCION04 = "V_SECCION4"; 

	
	public Seccion04_05DAO(DatabaseHelper dbh) {
		super(dbh);
		// TODO Auto-generated constructor stub
	}
	public static Seccion04_05DAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion04_05DAO(dbh);
		}
		return INSTANCE;
	}
	public boolean saveOrUpdate(Seccion04_05 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion04_05 bean, int from, int to, String... campos) throws java.sql.SQLException {
		
		return this.saveOrUpdate(TABLA_SECCION04,"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", bean, new String[] {
						"id", "hogar_id","persona_id_orden" }, from, to, campos);
	}
	
	public boolean saveOrUpdate(Seccion04_05 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_SECCION04, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", bean, 
				new String[]{"id", "hogar_id","persona_id_orden"}, -1, -1, campos);
		
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public Seccion04_05 getSeccion04_05(Integer id, Integer hogar_id,
			Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
     	SQLiteDatabase dbTX = dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		boolean existe=false;
		Seccion04_05 datos=null;
		Integer cantidad=-1;
		query.append("select *")
		.append(" FROM ").append(VISTA_LISTADO_SECCION04)
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),persona_id_orden.toString()});
		if(cursor.moveToNext())
		{   datos= new Seccion04_05();
			datos.id=Util.getInt(getString("ID"));
			datos.hogar_id=Util.getInt(getString("HOGAR_ID"));
			datos.persona_id= Util.getInt(getString("PERSONA_ID"));
			datos.persona_id_orden=Util.getInt(getString("PERSONA_ID_ORDEN"));
			datos.qh7dd = getString("QH7DD");
			datos.qh7mm = getString("QH7MM");
		}
		cursor.close();
		cursor=null;
		dbTX.close();
		SQLiteDatabase.releaseMemory();
		
//     	Seccion04_05 bean = (Seccion04_05) getBean(VISTA_LISTADO_SECCION04,"ID=?",new String[] { id.toString()},"id", Seccion04_05.class, secciones);

//		Seccion04_05 bean = (Seccion04_05) getBean(VISTA_LISTADO_SECCION04,
//				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?",
//				new String[] { id.toString(), hogar_id.toString(),
//						persona_id.toString(),persona_id_orden.toString() },"id,hogar_id,persona_id,persona_id_orden", Seccion04_05.class, secciones);
		return datos;
	}
	public Seccion04_05 getSeccion05_persona(Integer id,Integer hogar_id,Integer persona_id_orden, SeccionCapitulo...secciones){
		return (Seccion04_05)getBean(TABLA_SECCION04, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",new String[]{id.toString(),hogar_id.toString(),persona_id_orden.toString()},Seccion04_05.class,secciones);
	}
	public boolean BorrarPersona_Seccion04_05(Seccion01 bean){
		this.borrar(TABLA_SECCION04, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?", new String[]{bean.id.toString(),bean.hogar_id.toString(),bean.persona_id.toString()});
		return true;
	}
	public Seccion04_05 getSeccion05Dialog(Integer id, Integer hogar_id,Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
		/*SQLiteDatabase dbTX = dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		
		Seccion04_05 datos=null;
		query.append("select *")
		.append(" FROM ").append(TABLA_SECCION04)
		.append(" WHERE ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString(),persona_id.toString(),persona_id_orden.toString()});
		if(cursor.moveToNext())
		{   datos= new Seccion04_05();
			datos.id=Util.getInt(getString("ID"));
			datos.hogar_id=Util.getInt(getString("HOGAR_ID"));
			datos.persona_id= Util.getInt(getString("PERSONA_ID"));
			datos.persona_id_orden=Util.getInt(getString("PERSONA_ID_ORDEN"));
			datos.qh208 = Util.getInt(getString("QH208"));
			datos.qh209 = Util.getInt(getString("QH209"));
			datos.qh210 = Util.getInt(getString("QH210"));
			datos.qh211 = getBigDecimal("QH211");
			datos.qh212 = Util.getInt(getString("QH212"));
			datos.qh213 = Util.getInt(getString("QH213"));
		}
		cursor.close();
		cursor=null;
		dbTX.close();
		SQLiteDatabase.releaseMemory();
		return datos;*/
		return (Seccion04_05)getBean(TABLA_SECCION04, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=?",new String[]{id.toString(),hogar_id.toString(),persona_id_orden.toString()},Seccion04_05.class,secciones);
		
	}
	public Seccion04_05 getSeccion04_05Tabla(Integer id,Integer hogar_id, Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones)
	{
		Seccion04_05 bean = (Seccion04_05) getBean(
				TABLA_SECCION04,"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id.toString(),persona_id_orden.toString() }, Seccion04_05.class, secciones);
		return bean;
	}
	
	
	public List<Seccion04_05> getSeccion04_05Lista(Integer id, Integer hogar_id,Integer persona_id) {
		String[] camposSeccion1= new Seccion01().getFieldsNames();
		String[] camposSeccion1Final = new String[camposSeccion1.length + 5];
		camposSeccion1Final = Arrays.copyOf(camposSeccion1, camposSeccion1Final.length);
		
		camposSeccion1Final[camposSeccion1Final.length - 5] = "qh02_1";
		camposSeccion1Final[camposSeccion1Final.length - 4] = "qh07";
		camposSeccion1Final[camposSeccion1Final.length - 3] = "qh7dd";
		camposSeccion1Final[camposSeccion1Final.length - 2] = "qh7mm";
		camposSeccion1Final[camposSeccion1Final.length - 1] = "qh06";
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT S4.*,S1.QH02_1,S1.QH07,S1.QH7DD,S1.QH7MM,S1.QH06")
				.append(" ").append("FROM ").append(TABLA_SECCION01).append(" S1")
				.append(" ").append("INNER JOIN ").append(TABLA_SECCION04)
				.append(" S4 ON S1.PERSONA_ID=S4.PERSONA_ID_ORDEN")
				.append(" WHERE S4.ID=? AND S4.HOGAR_ID=? AND S4.PERSONA_ID=? AND S1.QH06=2 AND (S1.QH07 BETWEEN 15 and 49) OR (S1.QH06 BETWEEN 1 AND 6)")
//				.append(" WHERE S4.ID=? AND S4.HOGAR_ID=? AND S4.PERSONA_ID=? AND S1.QH06=2 AND (S1.QH07 BETWEEN "+ App.HogEdadMefMin +" and "+App.HogEdadMefMax+") OR (S1.QH06 BETWEEN 1 AND 6)")
				.append(" ORDER BY S4.PERSONA_ID_ORDEN");
		Query query = new Query(sb.toString(),id.toString(),hogar_id.toString(),persona_id.toString());
	
		return getBeans(query, Seccion04_05.class, camposSeccion1Final);
	}
	
	public boolean TodoLosMiembrosS4Completados(Integer id,Integer hogar_id,Integer persona_id)
	{
		SQLiteDatabase dbr = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		int total = 0;
		query.append("SELECT SUM(ESTADO) as CANTIDAD ")
			.append("FROM V_SECCION04_COMPLETADO ")
			.append("WHERE ID=? AND HOGAR_ID = ? AND PERSONA_ID = ?");
				cursor = dbr.rawQuery(query.toString(),
			new String[] { id.toString(), hogar_id.toString(),persona_id.toString()});
			if (cursor.moveToNext()) {
				total = Util.getInt(getString("CANTIDAD"));
			}
			cursor.close();
			cursor = null;
			SQLiteDatabase.releaseMemory();
			return total == 0;
	}

	public List<Seccion04_05> getSeccion04_05List(Integer id) {
		return getBeans(TABLA_SECCION04, "ID=?", "id", Seccion04_05.class,
				id.toString());
	}
	public List<Seccion04_05> getListadoVistaSeccion4(Integer id,Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getBeans(VISTA_LISTADO_SECCION04, "ID=? AND HOGAR_ID=?", 
			   new String[]{id.toString(),hogar_id.toString()},"id,hogar_id",Seccion04_05.class,secciones);
	}
	public List<Seccion04_05> getSeccion04_05Lista(Integer id, Integer hogar_id,
			Integer persona_id, Integer persona_id_orden,SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_SECCION04,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),persona_id_orden.toString() }, 
				               "ID,HOGAR_ID,PERSONA_ID,PERSONA_ID_ORDEN",
				Seccion04_05.class, secciones);
	}
	public List<Seccion04_05> getSeccion04_05ListaVista(Integer id, Integer hogar_id,
			Integer persona_id, Integer persona_id_orden,SeccionCapitulo... secciones) {
		return getBeans(
				VISTA_LISTADO_SECCION04,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),persona_id_orden.toString() }, 
				               "ID,HOGAR_ID,PERSONA_ID,PERSONA_ID_ORDEN",
				Seccion04_05.class, secciones);
	}
	public List<Seccion04_05> getSeccion04_05ListaVista(Integer id, Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getBeans(
				VISTA_LISTADO_SECCION04,
				"ID=? AND Hogar_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString() }, 
				               "ID,HOGAR_ID,PERSONA_ID",
				Seccion04_05.class, secciones);
	}
	public boolean getEntraaSeccion04_05(Integer id,Integer hogar_id){
		SQLiteDatabase dbTX = dbh.getReadableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=null;
		query.append("SELECT COUNT(*) AS CANTIDAD ")
		.append(" FROM ").append(CuestionarioDAO.TABLA_SECCION01)
//		.append(" WHERE ((QH06=2 AND (QH07 BETWEEN 15 and 49)) OR (QH07 BETWEEN 0 AND 5)) AND ID=? AND HOGAR_ID=?");
		.append(" WHERE ((QH06=2 AND (QH07 BETWEEN "+App.HogEdadMefMin+ " and "+App.HogEdadMefMax+ ")) OR (QH07 BETWEEN 0 AND 5)) AND ID=? AND HOGAR_ID=?");
		cursor=dbTX.rawQuery(query.toString(), new String[]{id.toString(),hogar_id.toString()});
		if(cursor.moveToNext()){
			cantidad= Util.getInt(getString("CANTIDAD"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad>0;
		}
	public List<Seccion04_05> getPersonasRelacionadasalborrar(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return getBeans(TABLA_SECCION04,"ID=? AND HOGAR_ID=?",new String[]{id.toString(),hogar_id.toString()},"ID,HOGAR_ID",Seccion04_05.class,secciones);
	}
	public List<Seccion04_05> getPersonasAModificaralBorrar(Integer id,Integer hogar_id,Integer persona_id ,SeccionCapitulo... secciones){
		return getBeans(TABLA_SECCION04,"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN>?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},"ID,HOGAR_ID,PERSONA_ID_ORDEN",Seccion04_05.class,secciones);
	}
	public boolean ModificarNumerodeOrdenSeccion04_05(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID_ORDEN",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_SECCION04,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=? ",
				new String[] { id.toString(), hogar_id.toString() , persona_id_anterior.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<Seccion04_05> getPersonas(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion04_05().getFieldsNames();
		sbr.append("SELECT * ").append(" ")
			.append("FROM ").append(TABLA_SECCION04)
			.append(" WHERE ID = ? ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Seccion04_05.class, campos);
	}
	
	public List<Auditoria> getPersonasSeccion04_05_det(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Auditoria().getFieldsNames();
		sbr.append("SELECT * ").append(" ")
			.append("FROM ").append(TABLA_SECCION04_DET)
			.append(" WHERE ID = ? ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Auditoria.class, campos);
	}
	
	public boolean saveOrUpdate(Auditoria bean, SQLiteDatabase dbwTX)	throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		boolean isTX=false;
		if (dbwTX == null) {
			isTX = false;
			dbwTX = this.dbh.getWritableDatabase();
		}
		if (bean.orden_id == null) {
			Integer nextID = nextID(
				dbwTX,
				"ORDEN_ID",	TABLA_SECCION04_DET,"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=? AND PREGUNTA_ID=?",
				bean.id.toString(), 
				bean.hogar_id.toString(),
				bean.persona_id_orden.toString(),
				bean.pregunta_id.toString());
				bean.orden_id = nextID;
		}
		
		 this
				.saveOrUpdate(dbwTX,
				TABLA_SECCION04_DET,"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=? AND PREGUNTA_ID=? AND ORDEN_ID=?",
				bean, new String[] { "id", "hogar_id", "persona_id_orden","pregunta_id", "orden_id" }, -1, -1, campos);
		if (!isTX) {
			dbwTX.close();
			dbwTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return true;
	}
	
}
