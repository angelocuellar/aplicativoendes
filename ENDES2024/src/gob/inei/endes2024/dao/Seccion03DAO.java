package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.DatabaseHelper;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.endes2024.model.Seccion03;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class Seccion03DAO extends SQLiteDAO{
	public static Seccion03DAO INSTANCE; 
	public static final String TABLA_SECCION03 = "T_05_DIG_SECCION03";
	public static final String VISTA_LISTADO_SECCION03 = "V_SECCION3"; 
	public Seccion03DAO(DatabaseHelper dbh) {
		super(dbh);
		// TODO Auto-generated constructor stub
	}
	public static Seccion03DAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion03DAO(dbh);
		}
		return INSTANCE;
	}
	public boolean saveOrUpdate(Seccion03 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion03 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA_SECCION03, "ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=? AND PREGUNTA_ID=?", bean, 
				new String[]{"id", "hogar_id","persona_id","persona_id_orden","pregunta_id"}, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	
	public List<Seccion03> getPersonas(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion03().getFieldsNames();
		sbr.append("SELECT * ").append(" ")
			.append("FROM ").append(TABLA_SECCION03)
			.append(" WHERE ID = ? ");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Seccion03.class, campos);
	}
	
	public List<Seccion03> getPersonasAModificar(Integer id,Integer hogar_id,Integer persona_id_orden, SeccionCapitulo...secciones){
		return getBeans(TABLA_SECCION03, "ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN> ?",new String[]{id.toString(),hogar_id.toString(),persona_id_orden.toString()} ,"ID,HOGAR_ID,PERSONA_ID,PERSONA_ID_ORDEN",Seccion03.class,secciones);
	}
	
	public Seccion03 getPersona(Integer id, Integer hogar, Integer persona_id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion03().getFieldsNames();
		String[] camposFinal = new String[campos.length];
		camposFinal = Arrays.copyOf(campos, campos.length);
		camposFinal[camposFinal.length-1] = "c1p202";
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(CuestionarioDAO.TABLA_HOGAR).append(" h ")
			.append("LEFT JOIN ").append(TABLA_SECCION03).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? AND PERSONA_ID=?")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString(),persona_id.toString());
		return (Seccion03) getBean(query, Seccion03.class, camposFinal);
	}
	
	public Seccion03 getPersona(Integer id, Integer hogar,Integer persona_id, SeccionCapitulo... capitulos) {
		StringBuilder sbr = new StringBuilder();
		Seccion03 tmp = new Seccion03();		
		List<String> listaCampos = new ArrayList<String>();
		for (int i = 0; i < capitulos.length; i++) {
			SeccionCapitulo seccion = capitulos[i];
			listaCampos.addAll(tmp.getFieldMatches(seccion.getSeccion(),
					seccion.getSubseccion(), seccion.getInicio(),
					seccion.getFin(), seccion.getSubPregI(),
					seccion.getSubPregF()));
			if (seccion.getCampos() != null) {
				for (int j = 0; j < seccion.getCampos().length; j++) {
					listaCampos.add(seccion.getCampos()[j]);
				}
			}
		}
		String[] campos = listaCampos.toArray(new String[listaCampos.size()]);
		String[] camposFinal = new String[campos.length + 1];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-1] = "c1p202";		
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(CuestionarioDAO.TABLA_HOGAR).append(" h ")
			.append("LEFT JOIN ").append(TABLA_SECCION03).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? AND PERSONA_ID=?")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString(),persona_id.toString());
		return (Seccion03) getBean(query, Seccion03.class, camposFinal);
	}
	public boolean BorrarPersona(Seccion03 bean)
	{
		return borrar(
				TABLA_SECCION03,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				bean.id.toString(), bean.hogar_id.toString(),
				bean.persona_id.toString());
	}
	
	public boolean BorrarPersonaPregunta(Seccion03 bean)
	{
		return borrar(
				TABLA_SECCION03,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND PERSONA_ID_ORDEN=? AND PREGUNTA_ID=? ",
				bean.id.toString(), bean.hogar_id.toString(),
				bean.persona_id.toString(),bean.persona_id_orden.toString(),bean.pregunta_id.toString());
	}
	public List<Seccion03> getListadoPregunta92(Integer id,Integer hogar_id,Integer pregunta, SeccionCapitulo... secciones)
	{
		return getBeans(VISTA_LISTADO_SECCION03, "ID=? AND HOGAR_ID=? AND PREGUNTA_ID=?", new String[]{id.toString(),hogar_id.toString(),pregunta.toString()},"id,hogar_id,pregunta_id",Seccion03.class,secciones);
	}
	public Seccion03 getBeneficiario92(Integer id, Integer hogar_id,Integer persona_id_orden,Integer pregunta,SeccionCapitulo... secciones){
		Seccion03 bean = (Seccion03) getBean(
				TABLA_SECCION03,"ID=? AND Hogar_ID=? AND  PERSONA_ID_ORDEN=? AND PREGUNTA_ID=?",
				new String[] { id.toString(), hogar_id.toString(),
						persona_id_orden.toString(),pregunta.toString() }, Seccion03.class, secciones);
		return bean;		
	}
	public boolean ModificarNumerodeOrdenSeccion03(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer pregunta_id)
	{	SQLiteDatabase dbTX = dbh.getWritableDatabase();

		ContentValues valores = new ContentValues();
		valores.put("PERSONA_ID_ORDEN",persona_id_nuevo  );
		if (dbTX.update(
				TABLA_SECCION03,
				valores,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID_ORDEN=? AND PREGUNTA_ID=?",
				new String[] { id.toString(), hogar_id.toString() , persona_id_anterior.toString(),pregunta_id.toString()}) == 1) {
			return true;
		} else {
			return false;
		}
	}
	public List<Seccion03> getListadoCovid(Integer id,Integer hogar_id,Integer pregunta, SeccionCapitulo... secciones){
		return getBeans(VISTA_LISTADO_SECCION03, "ID=? AND HOGAR_ID=? AND PREGUNTA_ID=?", new String[]{id.toString(),hogar_id.toString(),pregunta.toString()},"id,hogar_id,pregunta_id",Seccion03.class,secciones);
	}
	
}
