package gob.inei.endes2024.dao;

import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Endes_omisiones;
import gob.inei.endes2024.model.Marco;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MarcoDAO extends SQLiteDAO {

	public static MarcoDAO INSTANCE;
	public static final String TABLA = "T_MARCO";
	public static final String V_COBERTURA_VIVIENDA = "V_COBERTURA_VIVIENDA";
	public static final String V_COBERTURA_VIVIENDA_C1 = "V_COBERTURA_VIVIENDA_C1";
	public static final String V_COBERTURA_VIVIENDA_C2 = "V_COBERTURA_VIVIENDA_C2";
	public static final String V_OMISIONES_CUESTIONARIOS="V_OMISIONES_CUESTIONARIOS";
	private final String TAG = MarcoDAO.this.getClass().getSimpleName();

	private MarcoDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}

	public static MarcoDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new MarcoDAO(dbh);
		}
		return INSTANCE;
	}

	public boolean save(Marco bean, SQLiteDatabase dbTX) throws SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return save(dbTX, TABLA, "ID=?", bean, new String[] { "id" }, -1, -1,
				campos);
	}

	public List<Marco> getMarco(Integer anio, String mes, Integer periodo,
			String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT ")
				.append(getCamposSelect("m", camposMarco))
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO")
				.append(" ")
				.append("FROM ")
				.append(TABLA)
				.append(" m")
				.append(" ")
				.append("JOIN ")
				.append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO")
				.append(" ")
				.append("WHERE m.ANIO = ? AND m.MES = ? AND m.CONGLOMERADO = ?")
				.append(" ")
				.append("ORDER BY m.ANIO,m.MES,m.UBIGEO,m.NROORDEN")
				.append(" ");
		Query query = new Query(sb.toString(), new String[] { anio.toString(),
				mes, conglomerado });
		String[] camposFinal = new String[(camposMarco.length + 3)];
		camposFinal = Arrays.copyOf(camposMarco, camposFinal.length);
		camposFinal[camposFinal.length - 3] = "departamento";
		camposFinal[camposFinal.length - 2] = "provincia";
		camposFinal[camposFinal.length - 1] = "distrito";
		return getBeans(query, Marco.class, camposFinal);
	}

	public List<Marco> getVivsReem(String conglomerado, String mes,
			Integer id) {

		//No borrar
//		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT DISTINCT m.ID AS ID, m.NSELV AS NSELV ").append(" ")
//		.append("FROM ").append(TABLA).append(" m").append(" ")
//		.append("JOIN ").append(UbigeoDAO.TABLA).append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
//		.append("INNER JOIN T_05_DIG_C1CARATULA C ON M.ID=C.ID").append(" ")
//		.append("WHERE m.VIVREM IS NOT NULL AND m.CONGLOMERADO = ? AND m.MES = ?").append(" ")
//		.append("AND m.VIVREM=0 AND C.RFINAL IN (7,8,9,11)").append(" ")
//		.append("UNION ALL").append(" ")
//		.append("SELECT DISTINCT m.ID, m.NSELV").append(" ")
//		.append("FROM ").append(TABLA).append(" m").append(" ")
//		.append("JOIN ").append(UbigeoDAO.TABLA).append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
//		.append("WHERE m.VIVREM IS NOT NULL AND m.CONGLOMERADO = ? AND m.MES = ?").append(" ")
//		.append("AND m.VIVREM=1").append(" ")
//		.append("AND m.ID<>?").append(" ")
//		.append("ORDER BY m.NSELV");
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT m.ID AS ID, m.NSELV AS NSELV ").append(" ")
		.append("FROM ").append(TABLA).append(" m").append(" ")
		.append("JOIN ").append(UbigeoDAO.TABLA).append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
		.append("WHERE m.ANIO = ? AND m.MES = ? AND m.CONGLOMERADO = ?")
		.append("AND m.ID<>?").append(" ")
		.append("ORDER BY m.NSELV");
		
		Query query = new Query(sb.toString(), new String[] { conglomerado,mes,id.toString() });
		return getBeans(query, Marco.class, "id", "nselv");
	}

	public List<Marco> getMarcoTotal(Integer anio, String mes,
			String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT ")
				.append(getCamposSelect("m", camposMarco))
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO")
				.append(" ")
				.append("FROM ")
				.append(TABLA)
				.append(" m")
				.append(" ")
				.append("JOIN ")
				.append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO")
				.append(" ")
				.append("WHERE m.ANIO = ? AND m.MES = ? AND m.CONGLOMERADO = ?")
				.append(" ")
				.append("ORDER BY m.ANIO,m.MES,m.UBIGEO,m.NROORDEN")
				.append(" ");
		Query query = new Query(sb.toString(), new String[] { anio.toString(),
				mes, conglomerado });
		String[] camposFinal = new String[(camposMarco.length + 3)];
		camposFinal = Arrays.copyOf(camposMarco, camposFinal.length);
		camposFinal[camposFinal.length - 3] = "departamento";
		camposFinal[camposFinal.length - 2] = "provincia";
		camposFinal[camposFinal.length - 1] = "distrito";
		return getBeans(query, Marco.class, camposFinal);
	}

	public Marco getMarco(Integer id) {
		String[] campos = new Marco().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", campos))
				.append(" ").append("FROM ").append(TABLA).append(" m ")
				.append("WHERE ID = ?");
		Query query = new Query(sb.toString(), id.toString());
		
		return (Marco) getBean(query, Marco.class, campos);
	}

	public boolean saveOrUpdate(Marco bean, SQLiteDatabase dbTX)
			throws SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return saveOrUpdate(dbTX, TABLA, "ID=?", bean, new String[] { "id" },
				-1, -1, campos);
	}

	public int getTotalViendas(int anio, String mes, String conglomerado) {
		return countdistinct("ID", TABLA,"ANIO=? AND MES=? AND CONGLOMERADO=?",String.valueOf(anio), mes, conglomerado);
	}
	
	public int getTotalViendas(Integer periodo, String conglomerado) {
		return countdistinct("ID", TABLA,"CONGLOMERADO=?",conglomerado);
	}
	
	public int getTotalViendasAdi(Integer periodo, String conglomerado) {
		return countdistinct("ID", TABLA,"CONGLOMERADO=?",conglomerado);
	}

//	public List<Marco> getMarco(Integer periodo, String conglomerado) {
//		String[] camposMarco = new Marco().getFieldsNames();
//		String[] camposMarcoFinal = new String[camposMarco.length + 5];
//		camposMarcoFinal = Arrays.copyOf(camposMarco, camposMarcoFinal.length);
//		String condicion="";
//		camposMarcoFinal[camposMarcoFinal.length - 5] = "usuario_asig";
//		camposMarcoFinal[camposMarcoFinal.length - 4] = "resviv";
//		camposMarcoFinal[camposMarcoFinal.length - 3] = "departamento";
//		camposMarcoFinal[camposMarcoFinal.length - 2] = "provincia";
//		camposMarcoFinal[camposMarcoFinal.length - 1] = "distrito";
//		StringBuilder sb = new StringBuilder();
//		
//		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, App.getInstance().getUsuario().cargo_id,App.getInstance().getUsuario().cargo_id)){
//			condicion="AND m.ASIGNADO IN (1,3,4)";
//		}
//		else{
//			condicion=" AND m.ASIGNADO IN (1,4) ";
//		}
//		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco)).append(",c.resviv")
//				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO, ")
////				.append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as USUARIO_ASIG ")
//				.append(" (select u.NOMBRES from T_USUARIO u where u.USUARIO=m.USU_ID) as USUARIO_ASIG ")
//				.append(" ").append("FROM ").append(TABLA).append(" m")
//				.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
//				.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
//				.append("LEFT JOIN ")
//				.append(CuestionarioDAO.TABLA_CARATULA)
//				.append(" c ON m.ID = c.ID ")
//				.append("WHERE m.CONGLOMERADO = ? ")
//				.append(condicion)
//				.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
//		
//		
//		
//		Query query = new Query(sb.toString(), new String[] { conglomerado });
//	
//		return getBeans(query, Marco.class, camposMarcoFinal);
//	}
	
	public List<Marco> getMarco(Integer periodo, String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		String[] camposMarcoFinal = new String[camposMarco.length + 5];
		camposMarcoFinal = Arrays.copyOf(camposMarco, camposMarcoFinal.length);
		String condicion="";
		camposMarcoFinal[camposMarcoFinal.length - 5] = "usuario_asig";
		camposMarcoFinal[camposMarcoFinal.length - 4] = "resviv";
		camposMarcoFinal[camposMarcoFinal.length - 3] = "departamento";
		camposMarcoFinal[camposMarcoFinal.length - 2] = "provincia";
		camposMarcoFinal[camposMarcoFinal.length - 1] = "distrito";
		StringBuilder sb = new StringBuilder();
		
		if(!Util.esDiferente(App.CODIGO_SUPERVISORA, App.getInstance().getUsuario().cargo_id,App.getInstance().getUsuario().cargo_id)){
			condicion="AND m.ASIGNADO IN (1,3,4)";
		}
		else{
			condicion=" AND m.ASIGNADO IN (1,4) ";
		}
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco)).append(",c.resviv")
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO, ")
//				.append(" (select u.NOMBRES from T_USUARIO u where u.ID=m.USU_ID) as USUARIO_ASIG ")
				.append(" (select u.NOMBRES from T_USUARIO u where u.USUARIO=m.USU_ID) as USUARIO_ASIG ")
				.append(" ").append("FROM ").append(TABLA).append(" m")
				.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
				.append("LEFT JOIN ")
				.append(CuestionarioDAO.TABLA_CARATULA)
				.append(" c ON m.ID = c.ID ")
				.append("WHERE m.CONGLOMERADO = ? ")
				.append(condicion)
				.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
		
		
		
		Query query = new Query(sb.toString(), new String[] { conglomerado });
	
		return getBeans(query, Marco.class, camposMarcoFinal);
	}
	
	public List<Marco> getMarcoAsignacion(Integer periodo, String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		String[] camposMarcoFinal = new String[camposMarco.length + 4];
		camposMarcoFinal = Arrays.copyOf(camposMarco, camposMarcoFinal.length);
		
		camposMarcoFinal[camposMarcoFinal.length - 4] = "resviv";
		camposMarcoFinal[camposMarcoFinal.length - 3] = "departamento";
		camposMarcoFinal[camposMarcoFinal.length - 2] = "provincia";
		camposMarcoFinal[camposMarcoFinal.length - 1] = "distrito";
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco)).append(",c.resviv")
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO")
				.append(" ").append("FROM ").append(TABLA).append(" m")
				.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
				.append("LEFT JOIN ")
				.append(CuestionarioDAO.TABLA_CARATULA)
				.append(" c ON m.ID = c.ID ")
				.append("WHERE m.CONGLOMERADO = ? AND IFNULL(m.ASIGNADO,0)=0 ")
				.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
		Query query = new Query(sb.toString(), new String[] { conglomerado });
		return getBeans(query, Marco.class, camposMarcoFinal);
	}
	
	public List<Marco> getMarcoCobertura(Integer periodo, String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		String[] camposMarcoFinal = new String[camposMarco.length + 7];
		camposMarcoFinal = Arrays.copyOf(camposMarco, camposMarcoFinal.length);
		
		camposMarcoFinal[camposMarcoFinal.length - 7] = "estado_envio";
		camposMarcoFinal[camposMarcoFinal.length - 6] = "gps";
		camposMarcoFinal[camposMarcoFinal.length - 5] = "estado";
		camposMarcoFinal[camposMarcoFinal.length - 4] = "resviv";
		camposMarcoFinal[camposMarcoFinal.length - 3] = "departamento";
		camposMarcoFinal[camposMarcoFinal.length - 2] = "provincia";
		camposMarcoFinal[camposMarcoFinal.length - 1] = "distrito";
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco))
			.append(",CASE WHEN c.ESTADO_ENVIO=2 THEN 'Cerrado' ELSE 'Abierto' END AS ESTADO_ENVIO")
			.append(",CASE WHEN c.GPS_LONG IS NOT NULL AND c.GPS_LONG <> '9999999999' THEN 'SI' ELSE 'NO' END AS GPS")
			.append(",CASE WHEN c.RESVIV IN (3,5,9,10) THEN 0 ELSE cob.ESTADO END AS ESTADO")
			.append(",c.RESVIV")
			.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO").append(" ")
			.append("FROM ").append(TABLA).append(" m").append(" ")
			.append("JOIN ").append(UbigeoDAO.TABLA).append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
			.append("LEFT JOIN ")
			.append(CuestionarioDAO.TABLA_CARATULA)
			.append(" c ON m.ID = c.ID ")
			.append("LEFT JOIN ").append(V_COBERTURA_VIVIENDA).append(" cob ON cob.ID = m.ID").append(" ")
			.append("WHERE m.PERIODO = ? AND m.CONGLOMERADO = ?")
			.append("ORDER BY m.ANIO,m.MES,m.UBIGEO,m.NROORDEN");
		Query query  = new Query(sb.toString(), new String[] {periodo.toString(),conglomerado});
	
		return getBeans(query, Marco.class, camposMarcoFinal);
	}
	
	public List<Marco> getMarcoTotal(Integer periodo, String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco))
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO")
				.append(" ").append("FROM ").append(TABLA).append(" m")
				.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
				.append("WHERE m.CONGLOMERADO = ?")
				.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
		Query query = new Query(sb.toString(), new String[] { conglomerado });
		String[] camposFinal = new String[(camposMarco.length + 3)];
		camposFinal = Arrays.copyOf(camposMarco, camposFinal.length);
		camposFinal[camposFinal.length - 3] = "departamento";
		camposFinal[camposFinal.length - 2] = "provincia";
		camposFinal[camposFinal.length - 1] = "distrito";
		return getBeans(query, Marco.class, camposFinal);
	}
	
	public boolean borrarHogares(Marco bean, SQLiteDatabase dbTX) {
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;			
		}
		
//		boolean flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP600, "ID=?", bean.id.toString());
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP500, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP411, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP405, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP402, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP400, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP300, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP200, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1VISITA, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CAP100, "ID=?", bean.id.toString());}
//		if(flag) {flag = borrar(dbTX, CuestionarioDAO.TABLA_C1CARATULA, "ID=?", bean.id.toString());}	
//			
//		if (!isTX) {
//			if(flag){
//				commitTX(dbTX);
//			}
//			endTX(dbTX);
//			dbTX.close();
//			SQLiteDatabase.releaseMemory();
//		}
		
		return true;
	}
	public List<Endes_omisiones> getOmisiones(String conglomerado){
		String[] camposomision= new Endes_omisiones().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		      sb.append("SELECT * ")
				.append(" FROM ").append(V_OMISIONES_CUESTIONARIOS)
				.append(" WHERE CONGLOMERADO = ?");
				//.append(" GROUP BY CONGLOMERADO,VIVIENDA ");
		Query query = new Query(sb.toString(), new String[] { conglomerado });
		return getBeans(query, Endes_omisiones.class, camposomision);
	}
}