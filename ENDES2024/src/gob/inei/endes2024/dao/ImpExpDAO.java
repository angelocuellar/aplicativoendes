package gob.inei.endes2024.dao;

import gob.inei.dnce.dao.SQLiteDAO;

import java.util.List;
import java.util.Map;

public class ImpExpDAO extends SQLiteDAO {
	public static ImpExpDAO INSTANCE;

	private ImpExpDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	public static ImpExpDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new ImpExpDAO(dbh);
		}
		return INSTANCE;
	}
	
	public List<Map<String, Object>> getRegistros(String tableName, String where, String...whereValues) {
		return super.getMaps(tableName, new String[]{"*"}, where, whereValues);
	}
}
