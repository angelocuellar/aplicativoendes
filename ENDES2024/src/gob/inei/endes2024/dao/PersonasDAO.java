package gob.inei.endes2024.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.DatabaseHelper;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.dao.SQLiteDAO.Query;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;

public class PersonasDAO extends SQLiteDAO{
	public static PersonasDAO INSTANCE; 
	public static final String TABLA = "T_05_DIG_SECCION01"; 
	
	public PersonasDAO(DatabaseHelper dbh) {
		super(dbh);
		// TODO Auto-generated constructor stub
	}

	public static PersonasDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new PersonasDAO(dbh);
		}
		return INSTANCE;
	}
	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		boolean esTX = true;
		if (dbTX == null) {
			esTX = false;
			dbTX = this.dbh.getWritableDatabase();
		}
		if (bean.persona_id == null) {
			bean.persona_id = nextID(dbTX, "HOGAR_ID","PERSONA_ID", TABLA, "ID=?", bean.id.toString());
		}
		String[] campos;
		if (secciones == null || secciones.length == 0) {
			campos = bean.getFieldsNames();
		} else {
			List<String> listaCampos = new ArrayList<String>();
			for (int i = 0; i < secciones.length; i++) {
				SeccionCapitulo seccion = secciones[i];
				listaCampos.addAll(bean.getFieldMatches(seccion.getSeccion(),
						seccion.getSubseccion(), seccion.getInicio(),
						seccion.getFin(), seccion.getSubPregI(),
						seccion.getSubPregF()));
				if (seccion.getCampos() != null) {
					for (int j = 0; j < seccion.getCampos().length; j++) {
						listaCampos.add(seccion.getCampos()[j]);
					}
				}
			}
			campos = listaCampos.toArray(new String[listaCampos.size()]);
		}
		boolean flag = saveOrUpdate(dbTX, TABLA, "ID=? AND HOGAR_ID=? AND PERSONA_ID", bean, 
				new String[]{"id", "hogar_id","persona_id"}, -1, -1, campos);
		if (!esTX) {
			dbTX = null;
			SQLiteDatabase.releaseMemory();
		}
		return flag;
	}
	public List<Seccion01> getPersonas(Integer id) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Seccion01().getFieldsNames();
		String[] camposFinal = new String[campos.length + 3];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-3] = "qh02_3";
		camposFinal[camposFinal.length-2] = "qh02_2";
		camposFinal[camposFinal.length-1] = "qh02_1";
		sbr.append("SELECT h.*, p.QH02_1, p.QH02_2, p.QH02_3").append(" ")
			.append("FROM ").append(TABLA).append(" h ")
			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.QH01 = 1 ")
			.append("WHERE h.ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString());
		return getBeans(query, Seccion01.class, camposFinal);
	}
	public Seccion01 getPersona(Integer id, Integer hogar) {
		StringBuilder sbr = new StringBuilder();
		String[] campos = new Hogar().getFieldsNames();
		String[] camposFinal = new String[campos.length];
		camposFinal = Arrays.copyOf(campos, campos.length);
		camposFinal[camposFinal.length-1] = "c1p202";
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(TABLA).append(" h ")
			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString());
		return (Seccion01) getBean(query, Seccion01.class, camposFinal);
	}
	public Seccion01 getPersona(Integer id, Integer hogar, SeccionCapitulo... capitulos) {
		StringBuilder sbr = new StringBuilder();
		Seccion01 tmp = new Seccion01();		
		List<String> listaCampos = new ArrayList<String>();
		for (int i = 0; i < capitulos.length; i++) {
			SeccionCapitulo seccion = capitulos[i];
			listaCampos.addAll(tmp.getFieldMatches(seccion.getSeccion(),
					seccion.getSubseccion(), seccion.getInicio(),
					seccion.getFin(), seccion.getSubPregI(),
					seccion.getSubPregF()));
			if (seccion.getCampos() != null) {
				for (int j = 0; j < seccion.getCampos().length; j++) {
					listaCampos.add(seccion.getCampos()[j]);
				}
			}
		}
		String[] campos = listaCampos.toArray(new String[listaCampos.size()]);
		String[] camposFinal = new String[campos.length + 1];
		camposFinal = Arrays.copyOf(campos, camposFinal.length);
		camposFinal[camposFinal.length-1] = "c1p202";		
		sbr.append("SELECT h.*, p.C1P202").append(" ")
			.append("FROM ").append(TABLA).append(" h ")
			.append("LEFT JOIN ").append(CuestionarioDAO.TABLA_SECCION01).append(" p ON h.ID = p.ID AND h.HOGAR_ID = p.HOGAR_ID AND p.C1P201 = 1 ")
			.append("WHERE h.ID = ? AND h.HOGAR_ID = ? ")
			.append("ORDER BY h.HOGAR_ID");
		Query query = new Query(sbr.toString(), id.toString(), hogar.toString());
		return (Seccion01) getBean(query, Seccion01.class, camposFinal);
	}
	
	
	
}
