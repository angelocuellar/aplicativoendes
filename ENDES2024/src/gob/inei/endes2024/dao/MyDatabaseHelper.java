package gob.inei.endes2024.dao;

import gob.inei.dnce.dao.DatabaseHelper;
import gob.inei.endes2024.R;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
/*import com.google.code.microlog4android.Logger;
 import com.google.code.microlog4android.LoggerFactory;
 import com.google.code.microlog4android.config.PropertyConfigurator;*/

public class MyDatabaseHelper extends DatabaseHelper {
	/**********************************************/
	private static String TAG = "MyDatabaseHelper";
	// protected static final Logger logger = LoggerFactory.getLogger();

	protected static Context context;

	public MyDatabaseHelper(Context ctx, String databaseName,
			int databaseVersion) {
		super(ctx, databaseName, databaseVersion);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 1 && newVersion == 2) {
		} else if (oldVersion == 1 && newVersion == 3) {
		} else if (oldVersion == 1 && newVersion == 4) {
		} else if (oldVersion == 2 && newVersion == 3) {
		} else if (oldVersion == 2 && newVersion == 4) {
		} else if (oldVersion == 3 && newVersion == 4) {
		}
		crearVistasCobertura(db);
	}

	@Override
	public void leerRAW(SQLiteDatabase db) {
		cargarXML(db, R.raw.sql_sistema, "sql_sistema");
		cargarXML(db, R.raw.sql_usuarios, "sql_usuario");
		cargarXML(db, R.raw.sql_segmentacion, "sql_segmentacion");
		cargarXML(db, R.raw.sql_ubigeo, "sql_ubigeo");
		cargarXML(db, R.raw.sql_marco, "sql_marco");
		cargarXML(db, R.raw.sqlcaratula, "sqlcaratula");
		//se agrego la tabla visita vivienda
		cargarXML(db, R.raw.sqlvisita_viv, "sqlvisita_viv");
		
		cargarXML(db, R.raw.sqlhogar, "sqlhogar");
		cargarXML(db, R.raw.sqlseccion03, "sqlseccion03");
		cargarXML(db, R.raw.sqlvisita, "sqlvisita");
		cargarXML(db, R.raw.sqlseccion01, "sqlseccion01");
		cargarXML(db, R.raw.sql_h_mortalidad, "sql_h_mortalidad");
		cargarXML(db, R.raw.sqlseccion04_05, "sqlsecccion04_05");
		cargarXML(db, R.raw.sqlseccion04_05_det, "sqlsecccion04_05_det");
		cargarXML(db, R.raw.sqlcapvisita, "sqlcapvisita");
		cargarXML(db, R.raw.sqlcap01_03, "sqlcap01_03");
		cargarXML(db, R.raw.sqlcap04_07, "sqlcap04_07");
		cargarXML(db, R.raw.sqlcap08_09, "sqlcap08_09");
		cargarXML(db, R.raw.sql_caratula_ci, "sql_caratula_ci");
		
		cargarXML(db, R.raw.sqlciseccion_01_03 , "sqlciseccion_01_03");
		cargarXML(db, R.raw.sqlciseccion_02, "sqlciseccion_02");
		cargarXML(db, R.raw.sqlcidiscapacidad, "sqlcidiscapacidad");
		cargarXML(db, R.raw.sqlciseccion_04dit , "sqlciseccion_dit");
		cargarXML(db, R.raw.sqlciseccion_04dit_02 , "sqlciseccion_dit_02");
		cargarXML(db, R.raw.sqlciseccion_02t , "sqlciseccion_02t");
		cargarXML(db, R.raw.sqlciseccion_04a , "sqlciseccion_04a");
		cargarXML(db, R.raw.sqlciseccion_04b , "sqlciseccion_04b");
		cargarXML(db, R.raw.sqlciseccion_04b_tarjeta , "sqlciseccion_04b_tarjeta");
		cargarXML(db, R.raw.sqlciseccion_04b2,"sqlciseccion_04b2");
		cargarXML(db, R.raw.sqlciseccion_05_07 , "sqlciseccion_05_07");
		cargarXML(db, R.raw.sqlciseccion_05, "sqlciseccion_05");
		cargarXML(db, R.raw.sqlcalendariocol4, "sqlcalendariocol4");
		cargarXML(db, R.raw.sqlicalendario_tramocol4,"sqlicalendario_tramocol4");
		cargarXML(db, R.raw.sqlciseccion_08 , "sqlciseccion_08");
		cargarXML(db, R.raw.sqlciseccion_09, "sqlciseccion_09");
		cargarXML(db, R.raw.sqlciseccion_10_01 , "sqlciseccion_10_01");
		cargarXML(db, R.raw.sqlciseccion_10_02 , "sqlciseccion_10_02");
		cargarXML(db, R.raw.sqlciseccion_10_03 , "sqlciseccion_10_03");
		cargarXML(db, R.raw.sqlciseccion_10_04 , "sqlciseccion_10_04");
		cargarXML(db, R.raw.sqlcalendario , "sqlcalendario");
		cargarXML(db, R.raw.sqlcicalendario_tramo, "sqlcicalendario_tramo");
		cargarXML(db, R.raw.sqlincentivos, "sqlincentivos");
		
		cargarXML(db, R.raw.sqlmigracion, "sqlmigracion");
		cargarXML(db, R.raw.sqlimigracion, "sqlimigracion");
		
		
		crearVistasCobertura(db);
	}

	@Override
	public void crearVistasCobertura(SQLiteDatabase db) {
		cargarXML(db, R.raw.sql_vistas, "view");
		cargarXML(db, R.raw.sql_vistas_individual, "view");
	}

}
