package gob.inei.endes2024.dao;


import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Region;
import android.util.Log;

public class SegmentacionDAO extends SQLiteDAO {
	
	public static SegmentacionDAO INSTANCE; 
	public static final String TABLA = "T_SEGMENTACION"; 
	public static final String TABLA_IE = "T_SEGMENTACION_IE";
	public static final String V_SEGMENTACION = "V_SEGMENTACION";
	public static final String V_ANIO = "V_ANIO";
	public static final String V_MES = "V_MES";
	public static final String V_PERIODO = "V_PERIODO";
	public static final String V_CONGLOMERADO = "V_CONGLOMERADO";

	private SegmentacionDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	
	public static SegmentacionDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new SegmentacionDAO(dbh);
		}
		return INSTANCE;
	}
	
	public List<SegmentacionFiltro> getAnios() {
		StringBuilder sbr = new StringBuilder();
		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
			.append("FROM ").append(V_ANIO).append(" ")
			.append("WHERE ((ENC_ID=").append(App.getInstance().getUsuario().id).append(" OR ENC_ID=0) ").append(" ")
			.append("  OR (SUP_ID=").append(App.getInstance().getUsuario().id).append(" OR COR_ID=").append(App.getInstance().getUsuario().id).append("))").append(" ")
			.append("ORDER BY ORDEN, ID");
		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
	}
	
	public List<SegmentacionFiltro> getMes(Integer anio) {
		StringBuilder sbr = new StringBuilder();
		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
			.append("FROM ").append(V_MES).append(" ")
			.append("WHERE (ANIO=").append(anio).append(" OR ANIO=0)").append(" ")
			.append("  AND ((ENC_ID=").append(App.getInstance().getUsuario().id).append(" OR ENC_ID=0) ").append(" ")
			.append("  OR (SUP_ID=").append(App.getInstance().getUsuario().id).append(" OR COR_ID=").append(App.getInstance().getUsuario().id).append("))").append(" ")
			.append("ORDER BY ORDEN, ID");
		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
	}
	
//	public List<SegmentacionFiltro> getPeriodos(Integer anio, String mes) {
//		StringBuilder sbr = new StringBuilder();
//		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
//			.append("FROM ").append(V_PERIODO).append(" ")
//			.append("WHERE (ANIO=").append(anio).append(" OR ANIO=0) AND (MES='").append(mes).append("' OR MES='0')").append(" ")
//			.append("  AND ((ENC_ID=").append(App.getInstance().getUsuario().id).append(" OR ENC_ID=0) ").append(" ")
//			.append("  OR (SUP_ID=").append(App.getInstance().getUsuario().id).append(" OR COR_ID=").append(App.getInstance().getUsuario().id).append("))").append(" ")
//			.append("ORDER BY ORDEN, ID");
//		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
//	}
	
//	public List<SegmentacionFiltro> getConglomerados(Integer anio, String mes, Integer periodo) {
//		StringBuilder sbr = new StringBuilder();
//		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
//			.append("FROM ").append(V_CONGLOMERADO).append(" ")
//			.append("WHERE (ANIO=").append(anio).append(" OR ANIO=0) AND (MES='").append(mes).append("' OR MES='0')")
//			.append("  AND (PERIODO=").append(periodo).append(" OR PERIODO=0)").append(" ")
//			.append("  AND ((ENC_ID=").append(App.getInstance().getUsuario().id).append(" OR ENC_ID=0) ").append(" ")
//			.append("  OR (SUP_ID=").append(App.getInstance().getUsuario().id).append(" OR COR_ID=").append(App.getInstance().getUsuario().id).append(")").append(" ")
//			.append("  OR (CAL_ID=").append(App.getInstance().getUsuario().id).append(" ))").append(" ")
//			.append("ORDER BY ORDEN, ID");
//		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
//	}
	
	public List<Marco> getMarco(Integer periodo, String conglomerado) {
		String[] camposMarco = new Marco().getFieldsNames();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT ").append(getCamposSelect("m", camposMarco))
				.append(" , u.DEPARTAMENTO, u.PROVINCIA, u.DISTRITO")
				.append(" ").append("FROM ").append(TABLA).append(" m")
				.append(" ").append("JOIN ").append(UbigeoDAO.TABLA)
				.append(" u ON m.UBIGEO = u.UBIGEO").append(" ")
				.append("WHERE m.VIVREM IS NOT NULL AND m.CONGLOMERADO = ?")
				.append(" ").append("ORDER BY m.UBIGEO,m.NROORDEN").append(" ");
		Query query = new Query(sb.toString(), new String[] { conglomerado });
		String[] camposFinal = new String[(camposMarco.length + 3)];
		camposFinal = Arrays.copyOf(camposMarco, camposFinal.length);
		camposFinal[camposFinal.length - 3] = "departamento";
		camposFinal[camposFinal.length - 2] = "provincia";
		camposFinal[camposFinal.length - 1] = "distrito";
		return getBeans(query, Marco.class, camposFinal);
	}
	
	
	public List<SegmentacionFiltro> getConglomerados(Integer periodo) {
		StringBuilder sbr = new StringBuilder();
		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
			.append("FROM ").append(V_CONGLOMERADO).append(" ")
			.append("WHERE ")
			.append("(PERIODO=").append(periodo).append(" OR PERIODO=0)").append(" ")
			.append("  AND ((ENC_USUARIO='").append(App.getInstance().getUsuario().usuario).append("' OR ENC_USUARIO=0) ").append(" ")
			.append("  OR (SUP_USUARIO='").append(App.getInstance().getUsuario().usuario)
			.append("' OR COR_USUARIO= '").append(App.getInstance().getUsuario().usuario).append("')").append(" ")
			.append("  OR (CAL_USUARIO='").append(App.getInstance().getUsuario().usuario).append("'))").append(" ")
			.append("ORDER BY ORDEN, ID");
		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
	}

	public boolean getConglomeradoTieneAsignado(String conglomerado){
		SQLiteDatabase dbTX = dbh.getWritableDatabase();
		StringBuilder query = new StringBuilder();
		Integer cantidad=0;
		query.append("SELECT COUNT(*) AS CANTIDAD ")
			.append(" FROM ").append(SegmentacionDAO.TABLA).append(" S ")
			.append(" INNER JOIN ").append(CuestionarioDAO.TABLA_VIVIENDA).append(" M ")
			.append(" ON S.CONGLOMERADO=M.CONGLOMERADO ")
			.append(" WHERE M.ASIGNADO IN (1,3,4) AND M.CONGLOMERADO=? ");
		cursor = dbTX.rawQuery(query.toString(),new String[]{conglomerado});
		if(cursor.moveToNext())
		{cantidad= Util.getInt(getString("CANTIDAD"));
		}
		cursor.close();
		cursor=null;
		SQLiteDatabase.releaseMemory();
		return cantidad>0;
		
	}
	
	public boolean saveOrUpdate(Segmentacion bean, SQLiteDatabase dbTX) throws SQLException {
		String oper = existeRegistro(dbTX, "ID", TABLA, "ID = ?", bean.id.toString()) ? "edit":"add";
		ContentValues content = new ContentValues();
		String[] campos = bean.getFieldsSaveNames();	
		content = bean.getContentValues(content, -1, -1, campos);		
		boolean result;
		if (oper.equals("add")) {
			result = dbTX.insertOrThrow(TABLA, null, content)!=-1;
		} else {
			result = dbTX.update(TABLA, content, "ID=?", new String[]{bean.id.toString()})>0;
		}		
		SQLiteDatabase.releaseMemory();		
		return result;
	}
	public List<SegmentacionFiltro> getPeriodos() {
		StringBuilder sbr = new StringBuilder();
		sbr.append("SELECT DISTINCT ID, NOMBRE").append(" ")
			.append("FROM ").append(V_PERIODO).append(" ")
			.append("WHERE").append(" ")
			.append("((ENC_USUARIO= '").append(App.getInstance().getUsuario().usuario).append("' OR ENC_USUARIO=0) ").append(" ")
			.append("  OR (SUP_USUARIO= '").append(App.getInstance().getUsuario().usuario)
			.append("' OR COR_USUARIO='").append(App.getInstance().getUsuario().usuario).append("')").append(" ")
			.append("  OR (CAL_USUARIO= '").append(App.getInstance().getUsuario().usuario).append("' ))").append(" ")
			.append("ORDER BY ORDEN, ID");
		return getBeans(new Query(sbr.toString()), SegmentacionFiltro.class, "ID","NOMBRE");
	}
	
	public Segmentacion getSegmentacion(Integer anio, String mes, Integer periodo, String conglomerado) {
		Segmentacion bean = new Segmentacion();
		StringBuilder sbr = new StringBuilder();		
		sbr.append("SELECT DISTINCT *").append(" ")
			.append("FROM ").append(V_SEGMENTACION).append(" ")
			.append("WHERE ANIO=? AND MES=? AND PERIODO=? AND CONGLOMERADO=?").append(" ")
			.append("ORDER BY ID");
		SQLiteDatabase dbr = dbh.getReadableDatabase();	
		cursor = dbr.rawQuery(sbr.toString(), new String[]{anio.toString(), mes, periodo.toString(), conglomerado});
		String[] campos = bean.getFieldsNames();
		if (cursor.moveToNext()) {
			bean = (Segmentacion)bean.fillEntity(cursor, campos);
			if (getInt("ENC_ID", 0) != 0) {
				bean.encuestador = new Usuario();
				bean.encuestador.id = getInt("ENC_ID");
				bean.encuestador.usuario = getString("ENC_USUARIO");
				bean.encuestador.nombres = getString("ENC_NOMBRES", null);
				bean.encuestador.apellidos = getString("ENC_APELLIDOS", null);
				bean.encuestador.dni = getString("ENC_DNI", null);
			}
			if (getInt("SUP_ID", 0) != 0) {
				bean.supervisor = new Usuario();
				bean.supervisor.id = getInt("SUP_ID");
				bean.supervisor.usuario = getString("SUP_USUARIO");
				bean.supervisor.nombres = getString("SUP_NOMBRES", null);
				bean.supervisor.apellidos = getString("SUP_APELLIDOS", null);
				bean.supervisor.dni = getString("SUP_DNI", null);
			}
			if (getInt("COR_ID", 0) != 0) {
				bean.coordinador = new Usuario();
				bean.coordinador.id = getInt("COR_ID");
				bean.coordinador.usuario = getString("COR_USUARIO");
				bean.coordinador.nombres = getString("COR_NOMBRES", null);
				bean.coordinador.apellidos = getString("COR_APELLIDOS", null);
				bean.coordinador.dni = getString("COR_DNI", null);
			}
		} else {
			bean = null;
		}
		return bean;
	}
	public Segmentacion getSegmentacion(Integer periodo, String conglomerado) {
		Segmentacion bean = new Segmentacion();
		StringBuilder sbr = new StringBuilder();		
		sbr.append("SELECT DISTINCT *").append(" ")
			.append("FROM ").append(V_SEGMENTACION).append(" ")
			.append("WHERE PERIODO=? AND CONGLOMERADO=?").append(" ")
			.append("ORDER BY ID");
		SQLiteDatabase dbr = dbh.getReadableDatabase();	
		cursor = dbr.rawQuery(sbr.toString(), new String[]{periodo.toString(), conglomerado});
		String[] campos = bean.getFieldsNames();
		if (cursor.moveToNext()) {
			bean = (Segmentacion)bean.fillEntity(cursor, campos);
			if (getInt("ENC_ID", 0) != 0) {
				bean.encuestador = new Usuario();
				bean.encuestador.id = getInt("ENC_ID");
				bean.encuestador.usuario = getString("ENC_USUARIO");
				bean.encuestador.nombres = getString("ENC_NOMBRES", null);
				bean.encuestador.apellidos = getString("ENC_APELLIDOS", null);
				bean.encuestador.dni = getString("ENC_DNI", null);
			}
			if (getInt("SUP_ID", 0) != 0) {
				bean.supervisor = new Usuario();
				bean.supervisor.id = getInt("SUP_ID");
				bean.supervisor.usuario = getString("SUP_USUARIO");
				bean.supervisor.nombres = getString("SUP_NOMBRES", null);
				bean.supervisor.apellidos = getString("SUP_APELLIDOS", null);
				bean.supervisor.dni = getString("SUP_DNI", null);
			}
			if (getInt("COR_ID", 0) != 0) {
				bean.coordinador = new Usuario();
				bean.coordinador.id = getInt("COR_ID");
				bean.coordinador.usuario = getString("COR_USUARIO");
				bean.coordinador.nombres = getString("COR_NOMBRES", null);
				bean.coordinador.apellidos = getString("COR_APELLIDOS", null);
				bean.coordinador.dni = getString("COR_DNI", null);
			}
		} else {
			bean = null;
		}
		return bean;
	}
	
//	public List<String> getRutas(String region) {
//		List<String> rutas = new ArrayList<String>();
//		SQLiteDatabase dbr = dbh.getReadableDatabase();
//		StringBuilder query = new StringBuilder();
//		query.append(
//				"SELECT '--- RUTA ---' as RUTA")
//				.append(" ");
//		query.append("UNION ALL").append(" ");
//		query.append("SELECT DISTINCT RUTA").append(" ")
//			.append("FROM ").append(TABLA).append(" ")
//			.append("WHERE COD_REGION=?").append(" ").append(" ORDER BY RUTA");
//		Cursor result = dbr.rawQuery(query.toString(), new String[]{region});		
//		while(result.moveToNext()){
//			String ruta = result.getString(result.getColumnIndex("RUTA"));
//			rutas.add(ruta);
//		}		
//		result.close();
//		result = null;
//		dbr.close();
//		SQLiteDatabase.releaseMemory();
//		return rutas;
//	}
}