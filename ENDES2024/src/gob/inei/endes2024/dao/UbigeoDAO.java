package gob.inei.endes2024.dao;

import gob.inei.endes2024.model.Ubigeo;
import gob.inei.dnce.dao.SQLiteDAO;

import java.util.List;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class UbigeoDAO extends SQLiteDAO {
	
	public static UbigeoDAO INSTANCE; 
	public static final String TABLA = "T_UBIGEO"; 

	private UbigeoDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}
	
	public static UbigeoDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new UbigeoDAO(dbh);
		}
		return INSTANCE;
	}
	
	public boolean saveOrUpdate(Ubigeo bean) {
		return this.saveOrUpdate(bean, null);
	}
	
	public boolean saveOrUpdate(Ubigeo bean, SQLiteDatabase dbTX) {
		if (dbTX == null) {
			dbTX = dbh.getWritableDatabase();
		}
		String oper = existeRegistro(dbTX, "UBIGEO", TABLA, "UBIGEO = ?", bean.ubigeo) ? "edit":"add";
		ContentValues content = new ContentValues();	
		String[] campos = bean.getFieldsSaveNames();	
		content = bean.getContentValues(content, -1, -1, campos);		
		boolean result;
		if (oper.equals("add")) {
			result = dbTX.insertOrThrow(TABLA, null, content)!=-1;
		} else {
			result = dbTX.update(TABLA, content, "UBIGEO = ?", new String[]{bean.ubigeo})>0;
		}		
		SQLiteDatabase.releaseMemory();		
		return result;
	}
	
	public List<Ubigeo> getDepartamentos() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT CCDD AS UBIGEO, CCDD, DEPARTAMENTO").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("ORDER BY CCDD").append(" ");
		Query query = new Query(sb.toString());
		return getBeans(query, Ubigeo.class, "UBIGEO", "CCDD","DEPARTAMENTO");
	}
	
	public Ubigeo getDepartamento(String ccdd) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT CCDD AS UBIGEO, CCDD, DEPARTAMENTO").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD =?").append(" ")
			.append("ORDER BY CCDD").append(" ");
		Query query = new Query(sb.toString(), ccdd);
		return (Ubigeo) getBean(query, Ubigeo.class, "UBIGEO", "CCDD","DEPARTAMENTO");
	}
	
	public List<Ubigeo> getProvincias(String ccdd) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT CCDD||CCPP AS UBIGEO, CCDD, DEPARTAMENTO, CCPP, PROVINCIA").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD = ?").append(" ")
			.append("ORDER BY CCDD, CCPP").append(" ");
		Query query = new Query(sb.toString(), ccdd);
		return getBeans(query, Ubigeo.class, "UBIGEO", "CCDD","DEPARTAMENTO", "CCPP", "PROVINCIA");
	}
	
	public Ubigeo getProvincia(String ccdd, String ccpp) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT CCDD||CCPP AS UBIGEO, CCDD, DEPARTAMENTO, CCPP, PROVINCIA").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD=? AND CCPP=?").append(" ")
			.append("ORDER BY CCDD").append(" ");
		Query query = new Query(sb.toString(), ccdd, ccpp);
		return (Ubigeo) getBean(query, Ubigeo.class, "UBIGEO", "CCDD", "DEPARTAMENTO", "CCPP", "PROVINCIA");
	}
	
	public List<Ubigeo> getDistritos(String ccdd, String ccpp) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT UBIGEO, CCDD, DEPARTAMENTO, CCPP, PROVINCIA, CCDI, DISTRITO").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD = ? AND CCPP = ?").append(" ")
			.append("ORDER BY CCDD, CCPP, CCDI").append(" ");
		Query query = new Query(sb.toString(), ccdd, ccpp);
		return getBeans(query, Ubigeo.class, "UBIGEO", "CCDD", "DEPARTAMENTO", "CCPP", "PROVINCIA", "CCDI", "DISTRITO");
	}
	
	public Ubigeo getDistrito(String ccdd, String ccpp, String ccdi) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT UBIGEO, CCDD, DEPARTAMENTO, CCPP, PROVINCIA, CCDI, DISTRITO").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD=? AND CCPP=? AND CCDI=?").append(" ")
			.append("ORDER BY CCDD").append(" ");
		Query query = new Query(sb.toString(), ccdd, ccpp, ccdi);
		return (Ubigeo) getBean(query, Ubigeo.class, "UBIGEO", "CCDD", "DEPARTAMENTO", "CCPP", "PROVINCIA", "CCDI", "DISTRITO");
	}
	public List<Ubigeo> getDistritoByccdd(String ccdd) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT UBIGEO, CCPP, PROVINCIA, CCDI, DISTRITO").append(" ")
			.append("FROM ").append(TABLA).append(" ")
			.append("WHERE CCDD=? ").append(" ")
			.append("ORDER BY CCDD").append(" ");
		Query query = new Query(sb.toString(), ccdd);
		return getBeans(query, Ubigeo.class, "UBIGEO","CCPP", "PROVINCIA", "CCDI", "DISTRITO");
	}
}
