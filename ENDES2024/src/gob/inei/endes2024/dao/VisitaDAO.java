package gob.inei.endes2024.dao;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;

import java.util.Arrays;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class VisitaDAO extends SQLiteDAO {

	public static VisitaDAO INSTANCE;
	public static final String TABLA_VISITAC1 = "T_05_DIG_VISITA";
	public static final String TABLA_VISITA_VIV = "T_05_DIG_VISITA_VIV";
	public static final String TABLA_VISITAI = "T_05_DIG_CARATULA_INDIVIDUAL";
	public static final String C3_TABLA_VISITA = "T_05_DIG_C3VISITA";
	public static final String TABLA_CSVISITA = "T_05_DIG_CSVISITA";
	public static final String TABLA_HOGAR = "T_05_DIG_HOGAR";
	public static final String TABLA_INCENTIVOS = "T_05_DIG_INCENTIVOS";

	private VisitaDAO(MyDatabaseHelper dbh) {
		super(dbh);
	}

	public static VisitaDAO getInstance(MyDatabaseHelper dbh) {
		if (INSTANCE == null) {
			INSTANCE = new VisitaDAO(dbh);
		}
		return INSTANCE;
	}

	public Visita getVisita(Integer id, Integer hogar_id, Integer visita_id) {
		Visita bean = (Visita) getBean(TABLA_VISITAC1,
				"ID=? AND HOGAR_ID=? AND NRO_VISITA=?", Visita.class,
				id.toString(), hogar_id.toString(), visita_id.toString());
		return bean;
	}

	public List<Visita> getVisitas(Integer id, Integer hogar_id,
			String... campos) {
		return getBeans(TABLA_VISITAC1, "ID=? AND  HOGAR_ID=?", new String[] {
				id.toString(), hogar_id.toString() }, "ID, HOGAR_ID",
				Visita.class, -1, -1, campos);
	}

	public List<Visita> getVisitas(Integer id) {
		String[] campos = new Visita().getFieldsNames();
		return getBeans(TABLA_VISITAC1, "ID=?", new String[] { id.toString() },
				"ID, NRO_VISITA", Visita.class, -1, -1, campos);
	}
	
	public List<Visita_Viv> getVisitas_viv(Integer id) {
		String[] campos = new Visita_Viv().getFieldsNames();
		return getBeans(TABLA_VISITA_VIV, "ID=?", new String[] { id.toString() },"ID, NRO_VISITA", Visita_Viv.class, -1, -1, campos);
	}
	
	public List<Incentivos> getIncentivos(Integer id) {
		String[] campos = new Incentivos().getFieldsNames();
		return getBeans(TABLA_INCENTIVOS, "ID=?", new String[] { id.toString() },"ID, HOGAR_ID", Incentivos.class, -1, -1, campos);
	}

	public List<Visita> getUltimasVisitas(Integer id) {
		String[] campos = new Visita().getFieldsNames();
		String[] camposVisitaFinal = new String[campos.length + 2];
		camposVisitaFinal = Arrays.copyOf(campos, camposVisitaFinal.length);
		camposVisitaFinal[camposVisitaFinal.length - 2] = "DNIENCUES";
		camposVisitaFinal[camposVisitaFinal.length - 1] = "NOMENCUES";
		
		StringBuilder sbr = new StringBuilder();
		
		sbr.append(
				"SELECT DISTINCT h.ID, h.HOGAR_ID, v.NRO_VISITA, v.E1DIA,v.E1MES INT,v.EINIHORA INT,v.EINIMINUTO INT,")
				.append(" ")
				.append("v.EFINHORA INT,v.EFINMINUTO INT,v.EDIAPROX INT,v.EMESPROX INT,")
				.append(" ")
				.append("v.EHORAPROX INT,v.EMINUTOPROX INT,v.ERESULT, v.ERESULT_O,v.SDIA INT,")
				.append(" ")
				.append("v.SMES INT,v.SINIHORA INT,v.SINIMINUTO INT,v.SFINHORA INT,")
				.append(" ")
				.append("v.SFINMINUTO INT,v.SRESULT INT,v.SRESULT_O,h.DNIENCUES, h.NOMENCUES")
				.append(" ")
				.append("FROM ")
				.append(CuestionarioDAO.TABLA_HOGAR)
				.append(" h ")
				.append("LEFT JOIN ")
				.append(TABLA_VISITAC1)
				.append(" AS v ON h.ID = v.ID AND h.HOGAR_ID = v.HOGAR_ID")
				.append(" ")
				.append("LEFT JOIN (SELECT ID, HOGAR_ID, MAX(NRO_VISITA) AS VISITA_ID FROM ")
				.append(TABLA_VISITAC1)
				.append(") AS t ON t.ID = v.ID AND t.HOGAR_ID = v.HOGAR_ID AND t.NRO_VISITA = v.NRO_VISITA")
				.append(" ").append("WHERE h.ID=?")
				.append(" ").append("ORDER BY h.ID, h.HOGAR_ID");
		return getBeans(new Query(sbr.toString(), id.toString()),
				Visita.class, camposVisitaFinal);
	}

	public Visita getPrimeraVisitaAceptada(Integer id, Integer hogar_id) {
		Visita v1;
		Visita v2;
		if (hogar_id == null) {
			StringBuilder sbr = new StringBuilder();
			String[] campos = new Visita().getFieldsNames();
			sbr.append("SELECT v.* FROM ").append(TABLA_VISITAC1).append(" v ")
					.append(" JOIN ").append(CuestionarioDAO.TABLA_HOGAR)
					.append(" h ON h.ID = v.ID AND h.HOGAR_ID = v.HOGAR_ID")
					.append(" ").append(" WHERE v.ID=?").append(" ");
			v1 = (Visita) getBean(new Query(sbr.toString()
					+ " AND v.QHVRESUL IN (1,2) ORDER BY v.NRO_VISITA, h.HOGAR",
					id.toString()), Visita.class, campos);
			v2 = (Visita) getBean(new Query(sbr.toString()
					+ " AND v.QHVRESUL IS NULL ORDER BY v.NRO_VISITA, h.HOGAR",
					id.toString()), Visita.class, campos);
		} else {
			v1 = (Visita) getBean(TABLA_VISITAC1,
					"ID=? AND HOGAR_ID=? AND QHVRESUL IN (1,2) ", "NRO_VISITA",
					Visita.class, id.toString(), hogar_id.toString());
			v2 = (Visita) getBean(TABLA_VISITAC1,
					"ID=? AND HOGAR_ID=? AND QHVRESUL IS NULL", "NRO_VISITA",
					Visita.class, id.toString(), hogar_id.toString());
		}
		if (v1 != null && v2 != null) {
			if (v1.nro_visita <= v2.nro_visita) {
				return v1;
			} else {
				return v2;
			}
		}
		if (v1 != null && v2 == null) {
			return v1;
		} else {
			return v2;
		}
	}
	public Visita getMaximaVisitaAceptada(Integer id,Integer hogar_id){
		Visita visita=null;
		Integer nro_visita=this.max("NRO_VISITA", TABLA_VISITAC1, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
		if(nro_visita!=0){
			visita=(Visita)getBean(TABLA_VISITAC1, "ID=? AND HOGAR_ID=? AND NRO_VISITA=?","NRO_VISITA",Visita.class,id.toString(),hogar_id.toString(),nro_visita.toString());
		}
		return visita==null?null:visita;
	}
	public Visita getUltimaVisita(Integer id, Integer hogar_id,
			String... campos) {
		return (Visita) getBean(TABLA_VISITAC1, "ID=? AND HOGAR_ID=? ",
				new String[] { id.toString(), hogar_id.toString() },
				"NRO_VISITA DESC", Visita.class, -1, -1, campos);
	}
		
	public CSVISITA getUltimaVisitaSalud(Integer id, Integer hogar_id,String... campos) {
		return (CSVISITA) getBean(TABLA_CSVISITA, "ID=? AND HOGAR_ID=? ",
				new String[] { id.toString(), hogar_id.toString() },
				"NRO_VISITA DESC", CSVISITA.class, -1, -1, campos);
	}
	public CARATULA_INDIVIDUAL getUltimaVisitaIndividual(Integer id, Integer hogar_id,Integer persona_id,String... campos) {
		return (CARATULA_INDIVIDUAL) getBean(TABLA_VISITAI, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?",
				new String[] { id.toString(), hogar_id.toString() },
				"NRO_VISITA DESC", CSVISITA.class, -1, -1, campos);
	}

	public Visita getMenorVisita(Integer id, Integer hogar_id,
			String... campos) {
		return (Visita) getBean(TABLA_VISITAC1,
				"ID=? AND HOGAR_ID=? AND ERESULT IS NOT NULL", new String[] {
						id.toString(), hogar_id.toString() }, "ERESULT",
				Visita.class, -1, -1, campos);
	}

	public Visita getMayorVisita(Integer id, Integer hogar_id,
			String... campos) {
		return (Visita) getBean(TABLA_VISITAC1,
				"ID=? AND HOGAR_ID=? AND ERESULT IS NOT NULL", new String[] {
						id.toString(), hogar_id.toString() }, "ERESULT DESC",
				Visita.class, -1, -1, campos);
	}

	public boolean saveOrUpdate(Visita bean, int from, int to,String... campos) throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VISITAC1,"ID=? AND HOGAR_ID=? AND  NRO_VISITA=?", bean, new String[] {"id", "hogar_id", "nro_visita" }, from, to, campos);
	}
		
	public boolean saveOrUpdate(Visita_Viv bean, int from, int to,String... campos) throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VISITA_VIV,"ID=? AND NRO_VISITA=?", bean, new String[] {"id", "nro_visita" }, from, to, campos);
	}
	
	public boolean saveOrUpdate(Hogar bean, int from, int to,String... campos) throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_HOGAR,"ID=? AND HOGAR_ID=?", bean, new String[] {"id", "hogar_id" }, from, to, campos);
	}
	
	public boolean saveOrUpdate(CARATULA_INDIVIDUAL bean, int from, int to,
			String... campos) throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_VISITAI,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND  NRO_VISITA=?", bean, new String[] {
						"id", "hogar_id", "persona_id", "nro_visita" }, from, to, campos);
	}
	public boolean saveOrUpdate(Visita bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		String[] campos = new Visita().getFieldsNames();
		return this.saveOrUpdate(dbTX, TABLA_VISITAC1,
				"ID=? AND HOGAR_ID=? AND  NRO_VISITA=?", bean, new String[] {
						"id", "hogar_id", "nro_visita" }, -1, -1, campos);
	}

	public boolean eliminar(SQLiteDatabase dbTX, Integer id, Integer hogar_id,
			Integer nro_visita) {
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;
		}
		this.borrar(
				dbTX,
				TABLA_VISITAC1,
				"ID=? AND HOGAR_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),
						nro_visita.toString() });
		if (!isTX) {
			commitTX(dbTX);
			endTX(dbTX);
			dbTX.close();
			SQLiteDatabase.releaseMemory();
		}
		return true;
	}
	public boolean eliminarSalud(SQLiteDatabase dbTX, Integer id, Integer hogar_id,
			Integer nro_visita) {
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;
		}
		this.borrar(
				dbTX,
				TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),
						nro_visita.toString() });
		if (!isTX) {
			commitTX(dbTX);
			endTX(dbTX);
			dbTX.close();
			SQLiteDatabase.releaseMemory();
		}
		return true;
	}
	
	// ADD 07122015
	// MODEL : CAPVISITA
	public boolean saveOrUpdate(CSVISITA bean, SQLiteDatabase dbwTX)
			throws java.sql.SQLException {
		String[] campos = bean.getFieldsSaveNames();
		return this.saveOrUpdate(dbwTX, TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean,
				new String[] { "id", "hogar_id", "persona_id", "nro_visita" },
				-1, -1, campos);
	}

	public boolean saveOrUpdate(CSVISITA bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return this.saveOrUpdate(TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?", bean,
				new String[] { "id", "hogar_id", "persona_id", "nro_visita" },
				secciones);
	}
	public List<CSVISITA> getCAPVISITAs(Integer id, Integer hogar_id,	Integer persona_id, 
			SeccionCapitulo... secciones) {
		return getBeans(
				TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? ",
				new String[] { id.toString(), hogar_id.toString(),	persona_id.toString() },
				"ID,HOGAR_ID,PERSONA_ID", CSVISITA.class, secciones);
	}

	public List<CSVISITA> getCAPVISITAList(Integer id) {
		return getBeans(TABLA_CSVISITA, "ID=?", "id", CSVISITA.class,
				id.toString());
	}
	
	public CSVISITA getCAPVISITA(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		CSVISITA bean = (CSVISITA) getBean(TABLA_CSVISITA, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()},CSVISITA.class,secciones); 
		return bean; 
	} 
	
	public boolean saveOrUpdate(CSVISITA bean, int from, int to, String... campos) throws java.sql.SQLException {
	
		return this.saveOrUpdate(TABLA_CSVISITA,
				"ID=? AND HOGAR_ID=? AND  PERSONA_ID=? AND NRO_VISITA=?", bean, new String[] {
						"id", "hogar_id", "persona_id","nro_visita" }, from, to, campos);
	}

	public Visita getVisitaCompletada(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		Integer nro_visita=this.max("NRO_VISITA", TABLA_VISITAC1, "ID=? AND HOGAR_ID=?", new String[]{id.toString(),hogar_id.toString()});
		Visita bean = (Visita) getBean(
				TABLA_VISITAC1,
				"ID=? AND Hogar_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),nro_visita.toString()}, Visita.class, secciones);
		return bean;
	}
	public CARATULA_INDIVIDUAL getVisitaIndividualCompletada(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		Integer nro_visita=this.max("NRO_VISITA", TABLA_VISITAI, "ID=? AND HOGAR_ID=? AND PERSONA_ID=?", new String[]{id.toString(),hogar_id.toString(),persona_id.toString()});
		CARATULA_INDIVIDUAL visita = null;
		if(nro_visita!=0){
			visita = (CARATULA_INDIVIDUAL) getBean(
					TABLA_VISITAI,
					"ID=? AND Hogar_ID=? AND PERSONA_ID=? AND NRO_VISITA=?",
					new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),nro_visita.toString()}, CARATULA_INDIVIDUAL.class, secciones);
		}
		return visita;
	}
	public List<CSVISITA> getVisitassalud(Integer id) {
		String[] campos = new CSVISITA().getFieldsNames();
		return getBeans(TABLA_CSVISITA, "ID=?", new String[] { id.toString() },
				"ID, NRO_VISITA", CSVISITA.class, -1, -1, campos);
	}
	public boolean EliminarVisitaIndividual(SQLiteDatabase dbTX, Integer id, Integer hogar_id, Integer persona_id,Integer nro_visita) {
		boolean isTX = true;
		if (dbTX == null) {
			dbTX = startTX();
			isTX = false;
		}
		this.borrar(
				dbTX,
				TABLA_VISITAI,
				"ID=? AND HOGAR_ID=? AND PERSONA_ID=? AND NRO_VISITA=?",
				new String[] { id.toString(), hogar_id.toString(),persona_id.toString(),
						nro_visita.toString() });
		if (!isTX) {
			commitTX(dbTX);
			endTX(dbTX);
			dbTX.close();
			SQLiteDatabase.releaseMemory();
		}
		return true;
	}
}