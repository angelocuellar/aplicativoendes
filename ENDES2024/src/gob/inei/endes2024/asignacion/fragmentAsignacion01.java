package gob.inei.endes2024.asignacion;

import gob.inei.dnce.annotations.FieldAnnotation;
import gob.inei.dnce.components.ButtonComponent;
import gob.inei.dnce.components.CheckBoxField;
import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.DialogComponent.TIPO_DIALOGO;
import gob.inei.dnce.components.DialogComponent.TIPO_ICONO;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.LabelComponent;
import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.TableComponent;
import gob.inei.dnce.components.TableComponent.ALIGN;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.interfaces.FieldComponent;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.R;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.common.MyUtil;
import gob.inei.endes2024.fragment.MarcoFragment;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.SegmentacionService;
import gob.inei.endes2024.service.UbigeoService;
import gob.inei.endes2024.service.VisitaService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class fragmentAsignacion01 extends FragmentForm {
	@FieldAnnotation(orderIndex = 1)
	public SpinnerField spnPERIODO;
	@FieldAnnotation(orderIndex = 2)
	public SpinnerField spnCONGLOMERADO;
	private String TAG = fragmentAsignacion01.this.getClass().getSimpleName();

	private enum ACTION {
		APERTURARC1, BORRAR, ELIMINAR_FILA, APERTURARC2, UPDATE_MARCOADI
	};

	private ACTION action;
	private DialogComponent dialog;
	public static SegmentacionFiltro PERIODO;
	public static SegmentacionFiltro ANIO;
	public static SegmentacionFiltro MES;
	public static SegmentacionFiltro CONGLOMERADO;
	private LabelComponent lblViviendas, lblHogares;

	private TableComponent tcMarco, tcHogar;
	private LabelComponent lblTitulo, lblperiodo, lblconglomerado;
	private UbigeoService ubigeoService;
	private GridComponent2 grid1;
	private MarcoService service;
	// private HogarService hogarService;
	public Integer tselv;

	private List<Marco> marcos;
	private List<Hogar> hogares;
	private SegmentacionService segmentacionService;
	private HogarService hogarService;
	private CuestionarioService cuestionarioService;
	private List<ViviendaSeleccionada> viviendaseleccionada;
	private VisitaService visitaService;
	SeccionCapitulo[] seccionesCargado;
	SeccionCapitulo[] seccionesGrabado;
	public ButtonComponent btnAsignar;
	public fragmentAsignacion01 parent(MasterActivity parent) {
		this.parent = parent;
		return this;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = createUI();
		initObjectsWithoutXML(this, rootView);
		enlazarCajas();
		listening();
		seccionesCargado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ID", "HOGAR_ID", "QH02_1", "QH02_2","QH02_3", "estado") };
		return rootView;
	}

	@Override
	protected View createUI() {
		buildFields();
		LinearLayout q0 = createQuestionSection(lblTitulo);
		LinearLayout q1 = createQuestionSection(0, Gravity.CENTER,
				LinearLayout.HORIZONTAL, grid1.component());
		LinearLayout q5 = createQuestionSection(q1);
		LinearLayout q2 = createQuestionSection(lblViviendas,btnAsignar,
				tcMarco.getTableView());
		ScrollView contenedor = createForm();
		LinearLayout form = (LinearLayout) contenedor.getChildAt(0);
		form.addView(q0);
		form.addView(q5);
		form.addView(q2);
		return contenedor;
	}

	@Override
	protected void buildFields() {
		lblTitulo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_marco).size(altoComponente, MATCH_PARENT).centrar().textSize(20);
		lblViviendas = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_vivienda).size(50, MATCH_PARENT).centrar().textSize(20);
		lblHogares = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_hogares).size(50, MATCH_PARENT).centrar().textSize(20);
		lblperiodo = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_periodo).size(50, MATCH_PARENT).centrar().textSize(18);
		lblconglomerado = new LabelComponent(getActivity(),App.ESTILO).text(R.string.titulo_conglomerado).size(50, MATCH_PARENT).centrar().textSize(18);
		btnAsignar= new ButtonComponent(getActivity(),App.ESTILO_BOTON).text(R.string.btnAsignar).size(200, 55);
		
//		Integer d=getResources().getDisplayMetrics().densityDpi;
//		if(!Util.esDiferente(d,App.YOGA8)){
//			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(700, 770).headerHeight(90).headerTextSize(19).dataColumHeight(60);	
//		}
//		else{
			tcMarco = new TableComponent(getActivity(), this,App.ESTILO).size(670, 700).dataColumHeight(50).headerHeight(50).headerTextSize(15);
//		}
		tcMarco.addHeader(R.string.m_resultado_viv, 0.15f, CheckBoxField.class,ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_orden_vivienda, 0.1f);
		tcMarco.addHeader(R.string.m_nselv_vivienda, 0.15f, ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_conglomerado, 0.2f, ALIGN.CENTER);
		tcMarco.addHeader(R.string.m_tself_vivienda, 0.1f, ALIGN.CENTER);	
		
		
		tcMarco.addCallback(0, "tcMarcoChangeValue");
		spnPERIODO = new SpinnerField(getActivity()).size(altoComponente + 15,250).callback("onPeriodoChangeValue");
		spnCONGLOMERADO = new SpinnerField(getActivity()).size(altoComponente + 15, 250).callback("onConglomeradoChangeValue");
		MyUtil.llenarPeriodo(getActivity(), getSegmentacionService(),spnPERIODO);
		grid1 = new GridComponent2(getActivity(),App.ESTILO, 2);
		grid1.addComponent(lblperiodo);
		grid1.addComponent(lblconglomerado);
		grid1.addComponent(spnPERIODO);
		grid1.addComponent(spnCONGLOMERADO);
		btnAsignar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean flag = grabar();
				if (!flag) {
					return;
				}	
				recargarLista();
			}
		});
	}
	private void recargarLista() {
		marcos = getService().getMarcoAsignacion(PERIODO.id , CONGLOMERADO.nombre);
		viviendaseleccionada = new ArrayList<fragmentAsignacion01.ViviendaSeleccionada>();
		for(Marco m:marcos){
			viviendaseleccionada.add(new ViviendaSeleccionada(0, m));
		}
		tcMarco.setData(viviendaseleccionada,"seleccionada","getNroOrden","getNroDeSeleccionVivienda","getConglomerado",
				"getTipoVivienda");
		registerForContextMenu(tcMarco.getListView());

	}
	
	public void tcMarcoChangeValue(Object entity,Integer row,Integer opcion){
		if (entity != null) {
			ViviendaSeleccionada tmp = (ViviendaSeleccionada) entity;
			tmp.seleccionada = opcion;
			
		}
	}

	public void onPeriodoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) component.getValue();
		MyUtil.llenarConglomerado(getActivity(), getSegmentacionService(),
				spnCONGLOMERADO, periodo);
	}

	public void onConglomeradoChangeValue(FieldComponent component) {
		SegmentacionFiltro periodo = (SegmentacionFiltro) spnPERIODO.getValue();
		SegmentacionFiltro conglomerado = (SegmentacionFiltro) component
				.getValue();
		if (periodo == null || conglomerado == null) {
			return;
		}
		cargarMarco(periodo, conglomerado);
	}

	public void cargarMarco(SegmentacionFiltro periodo,
			SegmentacionFiltro conglomerado) {
		PERIODO = periodo;
		CONGLOMERADO = conglomerado;
		if (conglomerado == null || periodo == null) {
			return;
		}
		marcos = getService().getMarcoAsignacion(periodo.id, conglomerado.nombre);
		viviendaseleccionada = new ArrayList<fragmentAsignacion01.ViviendaSeleccionada>();
		for(Marco m:marcos){
			viviendaseleccionada.add(new ViviendaSeleccionada(0, m));
		}
			
		Segmentacion seg = getSegmentacionService().getSegmentacion(periodo.id,
				conglomerado.nombre);
		App.getInstance().setSegmentacion(seg);
		recargarLista();

	}

	@Override
	public boolean grabar() {
		List<Marco> confirmarvivienda=new ArrayList<Marco>();
		if(viviendaseleccionada==null){
			return false;
		}
		for (ViviendaSeleccionada vs: viviendaseleccionada) {
				if(!Util.esDiferente(vs.seleccionada, 1)){
					confirmarvivienda.add(vs.marco);
			}
		}

		if(confirmarvivienda.size()<1){
			ToastMessage.msgBox(this.getActivity(), "SELECCIONE AL MENOS UNA VIVIENDA.", 
					ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
			return false;
		}
		seccionesGrabado = new SeccionCapitulo[] { new SeccionCapitulo(-1, -1,-1, "ASIGNADO","USU_ID") };
		boolean flag=false;
		for(Marco mco : confirmarvivienda){
			mco.asignado=App.VIVIENDAASIGNADA;
//			mco.usu_id=App.getUsuario().id;
			mco.usu_id=App.getUsuario().usuario;
			try {
				flag=getCuestionarioService().saveOrUpdate(mco, seccionesGrabado);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		 ToastMessage.msgBox(getActivity(), confirmarvivienda.size()+" viviendas asignadas correctamente",
                 ToastMessage.MESSAGE_INFO, ToastMessage.DURATION_SHORT);
		
		return true;
	}

	private CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService
					.getInstance(getActivity());
		}
		return cuestionarioService;
	}

	@Override
	public void cargarDatos() {
		App.getInstance().setMarco(null);
		App.getInstance().setHogar(null);
		if (PERIODO != null) {
			spnPERIODO.setSelectionKey(PERIODO.id);
			onPeriodoChangeValue(spnPERIODO);
		}
		if (CONGLOMERADO != null) {
			spnCONGLOMERADO.setSelectionKey(CONGLOMERADO.id);
			onConglomeradoChangeValue(spnCONGLOMERADO);
		}

		((CuestionarioFragmentActivity) parent).setTitulo(null);
		((CuestionarioFragmentActivity) parent).setSubTitulo(null);

	}

	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(getActivity());
		}
		return service;
	}

	public SegmentacionService getSegmentacionService() {
		if (segmentacionService == null) {
			segmentacionService = SegmentacionService
					.getInstance(getActivity());
		}
		return segmentacionService;
	}

	public UbigeoService getUbigeoService() {
		if (ubigeoService == null) {
			ubigeoService = UbigeoService.getInstance(getActivity());
		}
		return ubigeoService;
	}


	public VisitaService getVisitaService() {
		if (visitaService == null) {
			visitaService = VisitaService.getInstance(getActivity());
		}
		return visitaService;
	}

	
	public static class ViviendaSeleccionada implements IDetailEntityComponent {
		public Integer seleccionada=null;
		public Marco marco=null;
		public ViviendaSeleccionada(Integer seleccionada,Marco marco){
			super();
			this.seleccionada=seleccionada;
			this.marco=marco;
		}
		@Override
		public void cleanEntity() {
		}

		@Override
		public boolean isTitle() {
			// TODO Auto-generated method stub
			return false;
		}
		public String getNroOrden(){
			return marco.nroorden;
		}
		public String getConglomerado(){
			return marco.conglomerado;
		}
		public String getNroDeSeleccionVivienda(){
			return marco.nselv;
		}
		public String getTipoVivienda() {
			if (marco.tselv == null) {
				return "Indefinido";
			} else if (!Util.esDiferente(marco.tselv, 1)) {
				return "Urbano";
			} else {
				return "Rural";
			}
		}
		
	}
	public boolean AsignarVivienda(){
		
		return true;
	}
	
	@Override
	public Integer grabadoParcial() {
		// TODO Auto-generated method stub
		return App.NODEFINIDO;
	}
	
}
