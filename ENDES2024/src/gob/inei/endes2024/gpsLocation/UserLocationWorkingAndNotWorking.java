package gob.inei.endes2024.gpsLocation;

import gob.inei.endes2024.asyncTask.UserTimerTask;

import java.util.Timer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class UserLocationWorkingAndNotWorking extends Service {

	  private Looper mServiceLooper;
	  private ServiceHandler mServiceHandler;

	  private class ServiceHandler extends Handler {
	      public ServiceHandler(Looper looper) {
	          super(looper);
	      }
	      @Override
	      public void handleMessage(Message msg) {
	          try {
	              Thread.sleep(500);
	          } catch (InterruptedException e) {
	              Thread.currentThread().interrupt();
	          }
	          stopSelf(msg.arg1);
	      }
	  }

	  @Override
	  public void onCreate() {
		  HandlerThread thread = new HandlerThread("ServiceStartArguments",Process.THREAD_PRIORITY_BACKGROUND);
	      thread.start();
	      mServiceLooper = thread.getLooper();
	      mServiceHandler = new ServiceHandler(mServiceLooper);
	  }

	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
		  Toast.makeText(this, "Servicio Iniciado", Toast.LENGTH_SHORT).show();
		  //String tabletId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
		  final TelephonyManager telephonyManager= (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		  String tabletId = telephonyManager.getDeviceId()==null?Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID):telephonyManager.getDeviceId();
		  String model = android.os.Build.MODEL;
		  String serie = android.os.Build.SERIAL;
//		  Log.e("","T: "+tabletId);
		  Timer timer = new Timer();
		  UserTimerTask task = new UserTimerTask(this, tabletId, model, serie,cargaBateria(),getTemperature());
		  //cada 5 minutos equivale a (300000 milisegundos) realiza el envio de los puntos gps
		  //cada 6 segundos equivale a (6000 milisegundos) realiza el envio de los puntos gps
		  //cada 60 minutos equivale a (3600000 milisegundos) realiza el envio de los puntos gps
		  timer.schedule(task, 0, 3600000);

	      Message msg = mServiceHandler.obtainMessage();
	      msg.arg1 = startId;
	      mServiceHandler.sendMessage(msg);
	      return super.onStartCommand(intent, flags, startId);
//	      return START_STICKY;
	  }

	  @Override
	  public IBinder onBind(Intent intent) {
	      return null;
	  }

	  @Override
	  public void onDestroy() {
	    Toast.makeText(this, "Servicio Terminado !!!", Toast.LENGTH_SHORT).show();
	  }
	  
		public int cargaBateria () { 
	        try 
	        { 
	           IntentFilter batIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED); 
	            Intent battery =this.registerReceiver(null, batIntentFilter); 
	            int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1); 
	            return nivelBateria; 
	        } 
	        catch (Exception e) 
	        {            
	           Toast.makeText(this, 
	                    "Error al obtener estado de la bater�a", 
	                    Toast.LENGTH_SHORT).show(); 
	           return 0; 
	        }        
	    }
		
		public int getTemperature(){
			SensorManager mSensorManager;
			Sensor mTempSensor;
			try {
				 mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
			     mTempSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
			     int temperatura=mTempSensor.TYPE_AMBIENT_TEMPERATURE;
			     return temperatura;
			} catch (Exception e) {
				Toast.makeText(this, 
	                    "Error al obtener estado de la bater�a", 
	                    Toast.LENGTH_SHORT).show(); 
				return 0;
			}
		}
		
}

