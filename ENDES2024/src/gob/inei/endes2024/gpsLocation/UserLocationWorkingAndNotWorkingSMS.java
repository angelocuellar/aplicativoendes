package gob.inei.endes2024.gpsLocation;


import gob.inei.endes2024.asyncTask.UserTimerTask;

import java.util.Timer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class UserLocationWorkingAndNotWorkingSMS extends Service {

	  private Looper mServiceLooper;
	  private ServiceHandler mServiceHandler;
	  private final long CADA_CUARENTA_MINUTOS=2400000;
//	  private final long CADA_CUARENTA_MINUTOS=6000;
	  private final class ServiceHandler extends Handler {
	      public ServiceHandler(Looper looper) {
	          super(looper);
	      }
	      @Override
	      public void handleMessage(Message msg) {
	          try {
//	        	  Thread.sleep(200);
	              Thread.sleep(500);
	          } catch (InterruptedException e) {
	              Thread.currentThread().interrupt();
	          }
	          stopSelf(msg.arg1);
	      }
	  }

	  @Override
	  public void onCreate() {
		  HandlerThread thread = new HandlerThread("ServiceStartArguments",Process.THREAD_PRIORITY_BACKGROUND);
	      thread.start();
	      mServiceLooper = thread.getLooper();
	      mServiceHandler = new ServiceHandler(mServiceLooper);
	  }

	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
//		  Toast.makeText(this, "servicio iniciado !!!", Toast.LENGTH_SHORT).show();
		  final TelephonyManager telephonyManager= (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		  String tabletId = telephonyManager.getDeviceId()==null?Secure.getString(getBaseContext().getContentResolver(), Secure.ANDROID_ID):telephonyManager.getDeviceId();
//		  Log.e("","IMEI: "+tabletId);
		  String model = android.os.Build.MODEL;
		  String serie = android.os.Build.SERIAL;
		  
          IntentFilter batIntentFilter =  new IntentFilter(Intent.ACTION_BATTERY_CHANGED); 
          Intent battery = this.registerReceiver(null, batIntentFilter); 
          int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		  int temp = android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE;
	  
		  Timer timer = new Timer();
		  UserTimerTask task = new UserTimerTask(this, tabletId, model, serie, nivelBateria, temp);
		  //cada 30 minutos equivale a (1800000 milisegundos) realiza el envio de los puntos gps
		  //cada 6 segundos equivale a (6000 milisegundos) realiza el envio de los puntos gps
		  //cada 6 segundos equivale a (3600000 milisegundos) realiza el envio de los puntos gps
		//cada 40 segundos equivale a (2400000 milisegundos) realiza el envio de los puntos gps
//		  timer.schedule(task, 0, 1800000);
//		  timer.schedule(task, 0, 30000);
		  timer.schedule(task, 0, CADA_CUARENTA_MINUTOS);
	      Message msg = mServiceHandler.obtainMessage();
	      msg.arg1 = startId;
	      mServiceHandler.sendMessage(msg);
	      return START_STICKY;
	  }

	  @Override
	  public IBinder onBind(Intent intent) {
	      return null;
	  }

	  @Override
	  public void onDestroy() {
//	    Toast.makeText(this, "servicio terminado !!!", Toast.LENGTH_SHORT).show();
		  super.onDestroy();
	  }
}
