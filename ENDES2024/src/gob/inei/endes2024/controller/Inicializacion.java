package gob.inei.endes2024.controller;

import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.gpsLocation.UserLocationWorkingAndNotWorking;
import gob.inei.endes2024.gpsLocation.UserLocationWorkingAndNotWorkingSMS;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;
import android.content.ContextWrapper;

public class Inicializacion extends AsyncTask<String, String, String> implements
		Respondible {
	private Activity activity;
	private ProgressDialog pDialog;
	private UsuarioService usuarioService;

	private static final String ns = null;
	private String msg;
	private String titulo;
	private Usuario usuarioLogueado;
	private String usuario, clave;

	public Inicializacion(Activity activity) {
		super();
		this.activity = activity;
		this.titulo = activity.getResources().getString(R.string.app_name);
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(this.activity);
		pDialog.setMessage("Iniciando aplicación. Por favor espere...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	protected String doInBackground(String... args) {
		try {
			loguear();
			App.getInstance().setUsuario(usuarioLogueado);
//			App.USER_LOCATION_STATUS=App.APP_ON;
			msg = "Bienvenido " + usuarioLogueado.nombres + ".";
			Intent pushIntentNotWorking = new Intent(this.activity, UserLocationWorkingAndNotWorkingSMS.class);
            this.activity.startService(pushIntentNotWorking);
		} catch (NullPointerException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = "Error de ausencia de datos.";
		} catch (SQLException e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = e.getMessage();
		} catch (Exception e) {
			Log.e(this.getClass().toString(), e.getMessage(), e);
			msg = e.getMessage();
		} finally {
		}
		return null;
	}

	private void loguear() throws Exception {
		usuarioLogueado = getUsuarioService().getUsuario(usuario, clave);
		if (usuarioLogueado == null) {
			App.getInstance().setUsuario(null);
			throw new Exception("Usuario o clave no existe.");
		}
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		this.activity.runOnUiThread(new Runnable() {
			public void run() {
				DialogComponent dlg = new DialogComponent(activity,
						Inicializacion.this,
						DialogComponent.TIPO_DIALOGO.NEUTRAL, titulo, msg);
				dlg.showDialog();
			}
		});
	}

	private UsuarioService getUsuarioService() {
		if (usuarioService == null) {
			usuarioService = UsuarioService.getInstance(this.activity
					.getApplicationContext());
		}
		return usuarioService;
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccept() {
		if (App.getInstance().getUsuario() == null) {
			return;
		}
		Intent intent = null;
		intent = new Intent(activity, CuestionarioFragmentActivity.class);
		activity.startActivity(intent);
		activity.finish();
	}
}
