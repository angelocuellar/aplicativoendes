package gob.inei.endes2024.controller;

import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.dao.xml.XMLDataObject;
import gob.inei.dnce.dao.xml.XMLObject;
import gob.inei.dnce.dao.xml.XMLWriter;
import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
import gob.inei.dnce.dao.xml.XMLWriter.Atributo;
import gob.inei.dnce.dao.xml.XMLReader;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.dao.CuestionarioDAO.CounterObservable;
import gob.inei.endes2024.model.IExportacion;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Parameter;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Ubigeo;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.model.UsuarioRuta;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.ImpExpService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.ParameterService;
import gob.inei.endes2024.service.SegmentacionService;
import gob.inei.endes2024.service.UbigeoService;
import gob.inei.endes2024.service.UsuarioRutaService;
import gob.inei.endes2024.service.UsuarioService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class Importacion extends AsyncTask<String, String, String> implements
		Respondible, Observer,IExportacion {

	private Activity activity;
	private ProgressDialog pDialog;
	private List<Usuario> usuarios;
	private List<UsuarioRuta> usuariosRuta;
	private List<Ubigeo> ubigeos;
	private List<Marco> marcos;
	private List<Segmentacion> segmentaciones;
	private List<Parameter> parameters;
	private String extensionArchivo;
	private MarcoService marcoService;
	private ParameterService parameterService;
	private UsuarioService usuarioService;
	private SegmentacionService segmentacionService;
	private String msg;
	private String titulo;
	private List<File> archivos;
	private int cantidadCargar;
	private int cantidadCargado;
	private UbigeoService ubigeoService;

	private CuestionarioService cuestionarioService;
	private UsuarioRutaService usuarioRutaService;
	private ImpExpService exportarService;
	private FragmentForm fragmentForm;
	public Importacion(Activity activity, String titulo) {
		super();
		this.activity = activity;
		this.titulo = titulo;
	}

	public List<File> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<File> archivos) {
		this.archivos = archivos;
	}

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(this.activity);
		pDialog.setMessage(titulo + " Por favor espere...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	protected String doInBackground(String... args) {
		try {
			procesarXML();
			msg = "Importación completada correctamente.";
		} catch (NullPointerException e) {
			
			msg = "Error de ausencia de datos.";
		} catch (SQLException e) {
			
			msg = e.getMessage();
		} catch (Exception e) {
			
			msg = e.getMessage();
		} finally {
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(String... progress) {
		pDialog.setProgress(Integer.parseInt(progress[0]));
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		this.activity.runOnUiThread(new Runnable() {
			public void run() {
				DialogComponent dlg = new DialogComponent(activity,
						Importacion.this, DialogComponent.TIPO_DIALOGO.NEUTRAL,
						titulo, msg);
				dlg.showDialog();
			}
		});
	}


	private void procesarXML() throws Exception {

		try {
			String proyecto = "ENDES";//appName;
			File carpetaTemporal = null;
			for (File archivo : archivos) {	
				cantidadCargar = 0;
				cantidadCargado = 0;			
				XMLDataObject[] dataObjects = crearColecciones();
				File xml;
				extensionArchivo = archivo.getName().substring(archivo.getName().length()-3, archivo.getName().length());
				if (extensionArchivo.equals("zip")) {
					extensionArchivo = "xml";
					String rutaCarpeta = archivo.getAbsolutePath().replace(archivo.getName(), "");

					carpetaTemporal = new File(rutaCarpeta+"tmp");
					if (!carpetaTemporal.exists()) {
						carpetaTemporal.mkdirs();
					}

					Util.unzip(archivo.getAbsolutePath(), carpetaTemporal.getAbsolutePath());
					String rutaXML = carpetaTemporal.getAbsolutePath() + File.separator + archivo.getName().replace("zip", "xml");
					xml = new File(rutaXML);
					if (!xml.exists()) {
						extensionArchivo = "cfg";
						rutaXML = carpetaTemporal.getAbsolutePath() + File.separator + archivo.getName().replace("zip", "cfg");
						xml = new File(rutaXML);
						if (!xml.exists()) {
							throw new Exception("Error al descomprimir archivo.");
						}
					}
				} else {
					xml = archivo;
				}
				XMLReader.getInstance().deleteObservers();
				XMLReader.getInstance().addObserver(this);
				XMLReader.getInstance().getMaps(xml, proyecto, dataObjects);
				grabar(dataObjects);				
			}		
			
			if (carpetaTemporal != null) {
				if (carpetaTemporal.exists()) {
					Util.deleteRecursive(carpetaTemporal);
				}
			}
		} catch (FileNotFoundException e) {
			
			throw new Exception("El archivo no pudo ser encontrado.");
		} catch (XmlPullParserException e) {
			
			throw new Exception("El archivo no pudo ser cargado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			
			throw new Exception(
					"No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			
		} catch (SQLException e) {
			
			throw new Exception(e.getMessage());
		} finally {
		}
	
	}

	private XMLDataObject[] crearColecciones() {
		List<XMLDataObject> objects = new ArrayList<XMLDataObject>();
		objects.add(new XMLDataObject("T_MARCO", "Marco", XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID"));
		objects.add(new XMLDataObject("T_UBIGEO", "Ubigeo", XMLDataObject.ACTUALIZAR_PRIMERO).pk("UBIGEO"));
		objects.add(new XMLDataObject("T_USUARIO_RUTA", "UsuarioRuta", XMLDataObject.ACTUALIZAR_PRIMERO).pk("SEGMENTACION_ID").pk("USUARIO_ID"));
		objects.add(new XMLDataObject("T_USUARIO", "Usuario", XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID"));
		objects.add(new XMLDataObject("T_SEGMENTACION", "Segmentacion", XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID"));
		objects.add(new XMLDataObject("T_SISTEMA", "Parameter", XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID"));
		
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_VISITA_VIV, "Visita_viv", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("NRO_VISITA"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CARATULA,"Caratula",XMLDataObject.BORRAR_PRIMERO).pk("ID"));		
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_VISITA, "Visita", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("NRO_VISITA"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_SECCION01,"Seccion01", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_MORTALIDAD,"MORTALIDAD", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NRO_ORDEN_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_MIGRACION,"MIGRACION", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("QH34A"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_IMIGRACION,"IMIGRACION", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("QH35A"));
		
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_HOGAR, "Hogar", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_INCENTIVOS, "Incentivos", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_SECCION03, "Seccion03", XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PREGUNTA_ID").pk("PERSONA_ID_ORDEN"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05, "Seccion04_05",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID_ORDEN"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05_DET, "Seccion04_05_det",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID_ORDEN"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CSVISITA, "CSVISITA",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NRO_VISITA"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_SALUD, "Salud",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CAP04_07, "CAP04_07",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CSSECCION_08, "CSSECCION_08",XMLDataObject.BORRAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("PERSONA_ID_NINIO"));
		
		//individual
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_Individual, "CARATULA_INDIVIDUAL",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NRO_VISITA"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_01_03, "CISECCION_01_03",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02, "CISECCION_02",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QI212"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02T, "CISECCION_02T",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("TERMINACION_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, "CICALENDARIO_TRAMO",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QITRAMO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO, "CICALENDARIO",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QIINDICE"));
/*AGREGADO*/	objects.add(new XMLDataObject(CuestionarioDAO.TABLA_DISCAPACIDAD, "DISCAPACIDAD",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID").pk("CUESTIONARIO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04A, "CISECCION_04A",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B, "CISECCION_04B",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "CISECCION_04B_TARJETA",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID").pk("PREGUNTA").pk("INDICE"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B2, "CISECCION_04B2",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
//		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT, "CISECCION_04DIT",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NRO_ORDEN_NINIO"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT_02, "CISECCION_04DIT_02",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NRO_ORDEN_NINIO"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05_07, "CISECCION_05_07",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, "CICALENDARIO_TRAMO_COL4",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QITRAMO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO04, "CICALENDARIO_COL04",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QIINDICE"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05, "CISECCION_05",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("PAREJA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_08, "CISECCION_08",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_09, "CISECCION_09",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("QINRO_ORDEN"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_01, "CISECCION_10_01",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_02, "CISECCION_10_02",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_03, "CISECCION_10_03",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID").pk("VACUNA_ID"));
		objects.add(new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_04, "CISECCION_10_04",XMLDataObject.ACTUALIZAR_PRIMERO).pk("ID").pk("HOGAR_ID").pk("PERSONA_ID").pk("NINIO_ID").pk("QI_CTRL"));
		return objects.toArray(new XMLDataObject[objects.size()]);
	}
	

	private void calcularPorcentajeProcesado() {
		pDialog.incrementProgressBy(1);
		publishProgress(String.valueOf(cantidadCargado));
	}

	@Override
	public void update(Observable observable, Object data) {
		if (data == null) {
			return;
		}
		if (data instanceof Map) {
			Map<String, Integer> procesado = (Map<String, Integer>) data;
			this.cantidadCargado = Util.getInt(procesado.get("INSERTADO"));
			calcularPorcentajeProcesado();
//			pDialog.setMax(this.cantidadCargar);			
		} else if (data instanceof XMLObject.BeansProcesados) {
			XMLObject.BeansProcesados bp = (BeansProcesados) data;
			this.cantidadCargado = bp.getProcesado();
			this.cantidadCargar = bp.getTotal();
			calcularPorcentajeProcesado();
			pDialog.setMax(this.cantidadCargar);
		}
	}

	private MarcoService getMarcoService() {
		if (marcoService == null) {
			marcoService = MarcoService.getInstance(this.activity
					.getApplicationContext());
		}
		return marcoService;
	}

	private ParameterService getParameterService() {
		if (parameterService == null) {
			parameterService = ParameterService.getInstance(this.activity
					.getApplicationContext());
		}
		return parameterService;
	}

	private UsuarioService getUsuarioService() {
		if (usuarioService == null) {
			usuarioService = UsuarioService.getInstance(this.activity
					.getApplicationContext());
		}
		return usuarioService;
	}

	private UsuarioRutaService getUsuarioRutaService() {
		if (usuarioRutaService == null) {
			usuarioRutaService = UsuarioRutaService.getInstance(this.activity
					.getApplicationContext());
		}
		return usuarioRutaService;
	}

	private UbigeoService getUbigeoService() {
		if (ubigeoService == null) {
			ubigeoService = UbigeoService.getInstance(this.activity
					.getApplicationContext());
		}
		return ubigeoService;
	}

	private CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(this.activity
					.getApplicationContext());
		}
		return cuestionarioService;
	}

	private SegmentacionService getSegmentacionService() {
		if (segmentacionService == null) {
			segmentacionService = SegmentacionService.getInstance(this.activity
					.getApplicationContext());
		}
		return segmentacionService;
	}
	
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub

	}
	private void grabar(XMLDataObject[] dataObjects) throws Exception {
		boolean flag = true;
		String versionImportacion = App.getInstance().getVersion(activity);
		List<String> conglomerados= new ArrayList<String>();
		String conglomeradouno="";
		for (int i = 0; i < dataObjects.length && flag; i++) {
		if ("T_MARCO".equals(dataObjects[i].getTableName())) {
			for(Map<String, Object> m:dataObjects[i].getRegistros()){
				if(!conglomeradouno.equals(m.get("CONGLOMERADO").toString())){
					conglomeradouno=m.get("CONGLOMERADO").toString();
					conglomerados.add(conglomeradouno);
				}
			}
			}
		}
		ExportarAntesDeImportar(conglomerados);
		SQLiteDatabase dbTX = getMarcoService().startTX();
		XMLDataObject parameters = null;
		Integer id =0;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		try {
			CounterObservable contadorObserver = new CounterObservable(cantidadCargado); 
			contadorObserver.deleteObservers();
			contadorObserver.addObserver(this);
			for (int i = 0; i < dataObjects.length && flag; i++) {
				
				if ("T_MARCO".equals(dataObjects[i].getTableName())) { //solo graba no actualiza
					if (dataObjects[i].getRegistros() == null) {
						continue;
					}
					for(Map<String, Object> m:dataObjects[i].getRegistros()){
						if(!App.getInstance().getUsuario().usuario.equals("SIS")){
                           if("3".equals((String)m.get("ASIGNADO")) && App.getInstance().getUsuario().cargo_id==App.CODIGO_ENTREVISTADORA){
                        	   m.put("ASIGNADO", "1");
                           } 
						}
                    }
					for(Map<String, Object> m:dataObjects[i].getRegistros()){
						id=Integer.parseInt(m.get("ID").toString());
						
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CARATULA, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_VISITA_VIV, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_HOGAR, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_INCENTIVOS, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_VISITA, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION01,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_MORTALIDAD,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_MIGRACION,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_IMIGRACION,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION03, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION04_05, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SECCION04_05_DET, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CSVISITA, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_SALUD, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CAP04_07, id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CSSECCION_08,id, dbTX);
						//individual
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CIVISITA ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_01_03 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_02 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_02T ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO_TRAMO ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO ,id, dbTX);
//						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_DIT ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_DIT_02 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04A ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_04B2 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_05_07 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CALENDARIO04 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_05 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_08 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_09 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_01 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_02 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_03 ,id, dbTX);
						flag = getCuestionarioService().borrarCuestionario(CuestionarioDAO.TABLA_CISECCION_10_04 ,id, dbTX);
					}
					if(!getCuestionarioService().save(dbTX, dataObjects[i], contadorObserver)) {
						flag = false;
						break;
					}
                    List<Map<String, Object>> mp = new ArrayList<Map<String,Object>>();
                    for(Map<String, Object> m:dataObjects[i].getRegistros()){
                           if(!"0".equals((String)m.get("ASIGNADO"))) 
                        	   mp.add(m);
                           
                    }
                    dataObjects[i].getRegistros().clear();
                    dataObjects[i].addAll(mp);

				} else if ("T_MARCO".equals(dataObjects[i].getTableName())) { //solo graba no actualiza
					if (dataObjects[i].getRegistros() == null) {
						continue;
					}
					
				} else {					
					if (dataObjects[i].getRegistros() == null) {
						continue;
					}
					if ("T_SISTEMA".equals(dataObjects[i].getTableName())) {
						parameters = dataObjects[i];
					}
					
					if(!getCuestionarioService().saveOrUpdate(dbTX, dataObjects[i], contadorObserver)) {
						
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				getMarcoService().commitTX(dbTX);			
			}	
		} catch (java.sql.SQLException e) {
			throw new Exception("Error: " + e.getMessage());
		} finally {
			getMarcoService().endTX(dbTX);			
		}
		if (parameters == null) {
			return;
		}
		for (Map<String, Object> map : parameters.getRegistros()) {
			if (Util.getText(map.get("PARAMETER")).startsWith("V_")) {
				if (!Util.getText(map.get("VALUE1"),"").isEmpty()) {
					getParameterService().executeQuery(Util.getText(map.get("VALUE1")));
				}				
				if (!Util.getText(map.get("VALUE2"),"").isEmpty()) {
					getParameterService().executeQuery(Util.getText(map.get("VALUE2")));
				}
				if (!Util.getText(map.get("VALUE3"),"").isEmpty()) {
					getParameterService().executeQuery(Util.getText(map.get("VALUE3")));
				}
			}
		}
	}
	  public void ExportarAntesDeImportar(List<String> conglomerados) throws Exception {
		  /*File prueba = new File(Environment.getExternalStorageDirectory().getPath()+"/VIVIENDA COMPARTIDA");
			if (!prueba.exists()) {
				prueba.mkdirs();
			}*/
			/*File pruebaint = new File(Environment.getExternalStorageDirectory().getPath()+"/CARPETA COMPARTIDA/dev");
			if (!pruebaint.exists()) {
				pruebaint.mkdirs();
			}*/
			
		  
		  String directorio_nuevo="ARCHIVOS NUEVOS";
		  File directorio = new File(Environment.getExternalStorageDirectory().getPath()+File.separator+directorio_nuevo);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
		String condicion ="";
		if(!Util.esDiferente(App.getInstance().getUsuario().cargo_id,App.CODIGO_ENTREVISTADORA)){
			condicion=" ASIGNADO IN (1,4) AND ";
		}
		else{
			condicion=" ASIGNADO IN (1,3,4) AND ";
		}
		for (String m: conglomerados){
			File directorio_final = new File(Environment.getExternalStorageDirectory().getPath()+File.separator+directorio_nuevo+File.separator+m);
			if (!directorio_final.exists()) {
				directorio_final.mkdirs();
			}
			Calendar c= new GregorianCalendar();
			String dia=(c.get(Calendar.DAY_OF_MONTH)+"").toString().length()<2?("0"+c.get(Calendar.DAY_OF_MONTH)):""+c.get(Calendar.DAY_OF_MONTH);
			String mes=(c.get(Calendar.MONTH)+"").toString().length()<2?("0"+(c.get(Calendar.MONTH)+1)):""+(c.get(Calendar.MONTH)+1);
			String hora=(c.get(Calendar.HOUR_OF_DAY)+"").toString().length()<2?("0"+c.get(Calendar.HOUR_OF_DAY)):""+c.get(Calendar.HOUR_OF_DAY);
			String minuto=(c.get(Calendar.MINUTE)+"").toString().length()<2?("0"+c.get(Calendar.MINUTE)):""+c.get(Calendar.MINUTE);
			String segundo=(c.get(Calendar.SECOND)+"").toString().length()<2?("0"+c.get(Calendar.SECOND)):""+c.get(Calendar.SECOND);
			String completaconglo=Util.completarCadena(m.toString(), "0", 4, COMPLETAR.IZQUIERDA);
			String nombreArchivo=App.ANIOPORDEFECTOSUPERIOR+completaconglo+"_"+dia+"_"+mes+"_"+c.get(Calendar.YEAR)+"_"+hora+"_"+minuto+"_"+segundo;
			File xmlregistros = new File(directorio_final+File.separator+nombreArchivo+".xml");
			if (xmlregistros.exists()) {
				xmlregistros.delete();
			}
			xmlregistros.createNewFile();
			XMLWriter.getInstance().deleteObservers();
			XMLWriter.getInstance().addObserver(this);
			List<Map<String,Object>> data = getExportarService().getRegistros(CuestionarioDAO.TABLA_CARATULA, "rtrim(ltrim(CONGLOMERADO))=?",m.toString());
			if(data!=null && data.size()!=0){
				Integer contador=0; 
				Integer[] ids = new Integer[data.size()];
				for(Map<String,Object> map: data){
//					if(!Util.esDiferente(App.getInstance().getUsuario().cargo_id,App.CODIGO_ENTREVISTADORA)){
//						map.put("ASIGNADO", 3);
//				}
				ids[contador] = Integer.parseInt(map.get("ID").toString());
				contador++;
				}
			
			String inicio="(";
			String todo="";
			for(Integer d: ids){
				todo=todo+"\'"+d.toString()+"\'"+","; 
			}
		todo=todo.substring(0,todo.length()-1);
		String fin=")";
		inicio=inicio+todo+fin;
		
			XMLWriter.getInstance().putMaps(xmlregistros, "ENDES", new Atributo[]{},	
				new XMLDataObject(CuestionarioDAO.TABLA_VIVIENDA , "Marco", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>)
						getExportarService().getRegistros(CuestionarioDAO.TABLA_VIVIENDA, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_VISITA_VIV, "Visita_viv", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_VISITA_VIV, "ID IN  "+inicio)),
						
				new XMLDataObject(CuestionarioDAO.TABLA_CARATULA, "Caratula", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>)
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CARATULA, "ID IN  "+inicio)),

				new XMLDataObject(CuestionarioDAO.TABLA_VISITA, "Visita", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_VISITA, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_SECCION01, "Seccion01", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION01, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_MORTALIDAD, "MORTALIDAD", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_MORTALIDAD, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_MIGRACION, "MIGRACION", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_MIGRACION, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_IMIGRACION, "IMIGRACION", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_IMIGRACION, "ID IN  "+inicio)),			
							
				new XMLDataObject(CuestionarioDAO.TABLA_HOGAR, "Hogar", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
				    getExportarService().getRegistros(CuestionarioDAO.TABLA_HOGAR, "ID IN  "+ inicio)),
				
				new XMLDataObject(CuestionarioDAO.TABLA_INCENTIVOS, "Incentivos", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
				    getExportarService().getRegistros(CuestionarioDAO.TABLA_INCENTIVOS, "ID IN  "+ inicio)),
						    
						    
				new XMLDataObject(CuestionarioDAO.TABLA_SECCION03, "Seccion03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION03, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05, "Seccion04_05", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION04_05, "ID IN  "+inicio)), 
				new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05_DET, "Seccion04_05_det", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION04_05_DET, "ID IN  "+inicio)), 
				new XMLDataObject(CuestionarioDAO.TABLA_CSVISITA, "CSVISITA", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
				    getExportarService().getRegistros(CuestionarioDAO.TABLA_CSVISITA, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_SALUD, "Salud", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
				    getExportarService().getRegistros(CuestionarioDAO.TABLA_SALUD, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CAP04_07, "CAP04_07", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CAP04_07, "ID IN  "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CSSECCION_08, "CSSECCION_08", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
				    getExportarService().getRegistros(CuestionarioDAO.TABLA_CSSECCION_08, "ID IN "+inicio)),
				    
				
				//individual
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_01_03, "CISECCION_01_03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_01_03, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_Individual, "CARATULA_INDIVIDUAL", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_Individual, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02, "CISECCION_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_02, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02T, "CISECCION_02T", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_02T, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, "CICALENDARIO_TRAMO", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO, "CICALENDARIO", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO, "ID IN "+inicio)),
				
				new XMLDataObject(CuestionarioDAO.TABLA_DISCAPACIDAD, "DISCAPACIDAD", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_DISCAPACIDAD, "ID IN "+inicio)),
				
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04A, "CISECCION_04A", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04A, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B, "CISECCION_04B", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "CISECCION_04B_TARJETA", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B2, "CISECCION_04B2", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B2, "ID IN "+inicio)),
//				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT, "CISECCION_04DIT", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
//					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_DIT, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT_02, "CISECCION_04DIT_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
							getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_DIT_02, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05_07 , "CISECCION_05_07", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_05_07, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4 , "CICALENDARIO_TRAMO_COL4", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO04 , "CICALENDARIO_COL04", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>)
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO04, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05 , "CISECCION_05", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_05, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_08 , "CISECCION_08", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_08, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_09 , "CISECCION_09", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_09, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_01 , "CISECCION_10_01", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_01, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_02 , "CISECCION_10_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_02, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_03 , "CISECCION_10_03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_03, "ID IN "+inicio)),
				new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_04 , "CISECCION_10_04", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_04, "ID IN "+inicio)),
					
				new XMLDataObject(CuestionarioDAO.TABLA_USUARIO , Usuario.class.getSimpleName(), XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_USUARIO, "1=1")),
				new XMLDataObject(CuestionarioDAO.TABLA_SEGMENTACION , Segmentacion.class.getSimpleName(), XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_SEGMENTACION, "rtrim(ltrim(CONGLOMERADO))=?",m.toString())),
				new XMLDataObject(CuestionarioDAO.TABLA_USUARIO_RUTA , UsuarioRuta.class.getSimpleName(), XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					getExportarService().getRegistros(CuestionarioDAO.TABLA_USUARIO_RUTA, "1=1"))
			 ); 	
			 String rutaZip = xmlregistros.getAbsolutePath().replace("xml", "zip");

			if (!rutaZip.isEmpty()) {
				Util.zip(rutaZip, xmlregistros);
			}
			ids=null;
			}
			xmlregistros.delete();
		}
	  }
	private ImpExpService getExportarService() {
		if (exportarService == null) {
			exportarService = ImpExpService.getInstance(this.activity);
		}
		return exportarService;
	}

	@Override
	public FragmentForm getFragmentForm() {
		// TODO Auto-generated method stub
		return this.fragmentForm;
	}
}