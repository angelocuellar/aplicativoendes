package gob.inei.endes2024.controller;

import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.dao.xml.XMLDataObject;
import gob.inei.dnce.dao.xml.XMLObject;
import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
import gob.inei.dnce.dao.xml.XMLWriter;
import gob.inei.dnce.dao.xml.XMLWriter.Atributo;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.model.IExportacion;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.ImpExpService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class Exportacion  extends AsyncTask<String, String, String> implements Respondible, Observer, IExportacion {
	
	private Activity activity;
	private FragmentForm fragmentForm;
	private List<SegmentacionFiltro> exportables;	
	private ProgressDialog pDialog;		
//	private List<Local> locales;	
	private String rutaArchivo;
	private int cantidadEscribir = 0;
	private int cantidadEscrito = 0;	
	private String msg;
	private String titulo;
	private Marco marco;
	private ImpExpService exportarService;
	
	private String vivi; 
//	private ExportarService exportarService;
	
	public Exportacion(FragmentForm fragmentForm, String rutaArchivo, String titulo, String vivi) {
		super();
		this.fragmentForm = fragmentForm;
		this.rutaArchivo = rutaArchivo;
		this.titulo = titulo;		
		this.vivi = vivi;
	}

//	public void setRegistros(List<Marco> registrosCab) {
//		this.exportables = registrosCab;
//	}
	public void setRegistros(List<SegmentacionFiltro> registrosCab){
		
		//Log.e("registrosxxxxnum", "reg"+vivi);
		this.exportables=registrosCab;
	}

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(this.fragmentForm.getActivity());
		pDialog.setMessage(titulo+" Por favor espere...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
		pDialog.show();
	}
	public void setMarco(Marco marco){
		this.marco=marco;
	}
		
	
	protected String doInBackground(String... args) {
		try {
			
			escribirXML();
			msg = "Exportación completada correctamente.";
		} catch (NullPointerException e) {
        	
			msg = "Error de ausencia de datos.";
		} catch (SQLException e) {
        	
			msg = e.getMessage();
		} catch (Exception e) {
        	
			msg = e.getMessage();
		} finally {
		}
		return null;
	}
	
	@Override
	protected void onProgressUpdate(String... progress) {
		pDialog.setProgress(Integer.parseInt(progress[0]));
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		this.fragmentForm.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				DialogComponent dlg = new DialogComponent(Exportacion.this.fragmentForm.getActivity(), Exportacion.this, 
						DialogComponent.TIPO_DIALOGO.NEUTRAL, titulo, msg);
				dlg.showDialog();
			}
		});
	}
	
	private void crearArchivo(String ruta, SegmentacionFiltro... locales) throws Exception {
		int contador = 0;
		Calendar fecha = new GregorianCalendar();
		String condicion ="";
		if(!Util.esDiferente(App.getInstance().getUsuario().cargo_id,App.CODIGO_ENTREVISTADORA)){
			condicion=" ASIGNADO IN (1,4) AND ";
		}
		else{
			condicion=" ASIGNADO IN (1,3,4) AND ";
		}
		for (SegmentacionFiltro r : locales) {
			//String nombreArchivo = App.ANIOPORDEFECTOSUPERIOR+r.nombre;
			String nombreArchivo = App.ANIOPORDEFECTOSUPERIOR+r.nombre+App.getInstance().getUsuario().usuario;
//			String nombreArchivo = A+r.nombre;
			
			File directorio = new File(Environment.getExternalStorageDirectory().getPath()+"/ARCHIVOS EXPORTADOS");
				if (!directorio.exists()) {
					directorio.mkdirs();
				}
				File directorioultimonivel = new File(Environment.getExternalStorageDirectory().getPath()+"/ARCHIVOS EXPORTADOS"+"/"+r.nombre);
				if (!directorioultimonivel.exists()) {
					directorioultimonivel.mkdirs();
				}
				    ////////////
				 	File xmlRegistrospruebadir = new File(Environment.getExternalStorageDirectory().getPath()+"/VIVIENDA COMPARTIDA2");
					if(!xmlRegistrospruebadir.exists()){
						xmlRegistrospruebadir.mkdirs();
					}
					
					File dircompartido = new File(Environment.getExternalStorageDirectory().getPath()+"/VIVIENDA COMPARTIDA2"+"/"+r.nombre);
					if(!dircompartido.exists()){
						dircompartido.mkdirs();
					}
					
					File xmlregiscopiacompar = new File(dircompartido+"/"+nombreArchivo+".xml");
					if(!xmlregiscopiacompar.exists()){
						xmlregiscopiacompar.delete();
					}
				 
				
				File xmlRegistroscopia = new File(directorioultimonivel+"/"+nombreArchivo+".xml");
				if(!xmlRegistroscopia.exists()){
					xmlRegistroscopia.delete();
				}
			File xmlregistros = new File(ruta + "/"+ nombreArchivo +".xml");
			if (xmlregistros.exists()) {
				xmlregistros.delete();
			}		
			xmlregistros.createNewFile();

			XMLWriter.getInstance().deleteObservers();
			XMLWriter.getInstance().addObserver(this);
			String idconglomerado = Util.completarCadena(r.nombre.toString(), "0", 4, COMPLETAR.IZQUIERDA);
			List<Map<String,Object>> data = getExportarService().getRegistros(CuestionarioDAO.TABLA_VIVIENDA, condicion+" CONGLOMERADO=?",idconglomerado);
			//Log.e("controladorDDDDD","dd"+data.get(1).get("NSELV"));
			
			Log.e("pruebasssss","dd"+App.getInstance().getUsuario().cargo_id.toString());
			
			

	        // Obtener un iterador para recorrer la lista
	        /*Iterator<Map<String, Object>> iterator = data.iterator();

	        if(!(App.getInstance().getUsuario().cargo_id.toString()).equals("22")){
	        	
	        	// Recorrer la lista y eliminar los elementos que no cumplan la condición
		        while (iterator.hasNext()) {
		            Map<String, Object> elemento = iterator.next();

		            // Verificar la condición y eliminar el elemento si no se cumple
		            if (  !(App.getInstance().getUsuario().usuario).toString().equals((elemento.get("USU_ID")).toString()) ) {
		                iterator.remove();
		            }
		            Log.e("fffff", "DD"+elemento.get("USU_ID"));
		        }
	        }*/
	        
			
	        
	        if(vivi.toString().length()>0){
				// crear un iterador para la lista
				Iterator<Map<String, Object>> iterador = data.iterator();
				
				List<Map<String, Object>> nuevaLista = new ArrayList<Map<String, Object>>();
	
				// iterar sobre la lista y agregar el elemento deseado a la nueva lista
				while (iterador.hasNext()) {
				    Map<String, Object> elemento = iterador.next();
				    if (elemento.get("NSELV").equals(vivi)) {
				        nuevaLista.add(elemento);
				    }
				}
				// asignar la nueva lista a la original
				data = nuevaLista;
			}else{
				
				Iterator<Map<String, Object>> iterator = data.iterator();

		        if(!(App.getInstance().getUsuario().cargo_id.toString()).equals("22")){
		        	
		        	// Recorrer la lista y eliminar los elementos que no cumplan la condición
			        while (iterator.hasNext()) {
			            Map<String, Object> elemento = iterator.next();

			            // Verificar la condición y eliminar el elemento si no se cumple
			            if (  !(App.getInstance().getUsuario().usuario).toString().equals((elemento.get("USU_ID")).toString()) ) {
			                iterator.remove();
			            }
			            Log.e("fffff", "DD"+elemento.get("USU_ID"));
			        }
		        }
			}
			
			
			/*for(Map<String,Object> map: data){
				Log.e("dedddd", "ddd"+map.get("NSELV"));
				//Log.e("jjjj", "jjjs"+map.remove("NSELV"));
				Log.e("pruebasssss","dd"+data.get(1).get("USU_ID"));
				Log.e("nomusu", App.getInstance().getUsuario().usuario);
			}*/
			
			
//			for(Map<String,Object> map: data){
//				Log.e("dedddd", "ddd"+map.get("NSELV"));
//				//Log.e("jjjj", "jjjs"+map.remove("NSELV"));
//				if(vivi == map.get("NSELV")){
//					map.remove("NSELV");
//				}
//				
//			}
			
	/*		for(Integer i = 0 ; i< data.size(); i++){
				
//				if((data.get(i).get("NSELV").toString()).equals("0"+vivi)){
//					Log.e("ingresovvv", "con el id"+data.get(i).get("NSELV"));
//					data.remove(data.get(i));
//					i--;
//					//Log.e("ingreso", "con el id"+data.get(i));
//				}else{
//					Log.e("ingreso else1", "con el id"+data.get(i).get("NSELV"));
//					Log.e("ingreso else2", "con el id"+0+vivi);
//				}
				
				Log.e("prueba viviendas", "vivi"+data.get(i).get("NSELV"));
				
				if(vivi.contains(data.get(i).get("NSELV").toString())){
					Log.e("si contiene la vivienda", "con el id"+data.get(i).get("NSELV"));
					data.remove(data.get(i));
					i--;
				}else{
					Log.e("no continene", " la vivienda"+data.get(i).get("NSELV"));
				}
				
				//Log.e("prueba", "nnn"+data.get(i)+"---"+data.get(i).get("NSELV"));
				//Log.e("prueba", "nnn"+data.get(i));
			}*/
			
			//data.remove(data.get(Integer.parseInt(vivi)));
			Integer[] ids = new Integer[data.size()];
			if(data!=null){
				for(Map<String,Object> map: data){
					if(!Util.esDiferente(App.getInstance().getUsuario().cargo_id,App.CODIGO_ENTREVISTADORA)){
						map.put("ASIGNADO", 3);
					}
				ids[contador] = Integer.parseInt(map.get("ID").toString());
				contador++;
				}
				
				if(data.size() == 1 && vivi.toString().length()>0 ){
					//Log.e("deco", "fiii"+data.size());
					data.get(0).put("ASIGNADO", 1);
				}
			
			String inicio="(";
			 String todo="";
			for(Integer d: ids){
				todo=todo+"\'"+d.toString()+"\'"+","; 
			}
			todo=todo.substring(0,todo.length()-1);
			String fin=")";
			inicio=inicio+todo+fin;
				XMLWriter.getInstance().putMaps(xmlregistros, "ENDES", new Atributo[]{},	
					new XMLDataObject(CuestionarioDAO.TABLA_VIVIENDA , "Marco", XMLDataObject.ACTUALIZAR_PRIMERO,data),
					
					new XMLDataObject(CuestionarioDAO.TABLA_VISITA_VIV, "Visita_viv", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
							getExportarService().getRegistros(CuestionarioDAO.TABLA_VISITA_VIV, "ID IN  "+inicio)),
								
							
					new XMLDataObject(CuestionarioDAO.TABLA_CARATULA, "Caratula", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>)
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CARATULA, "ID IN  "+inicio)),
					
					
								
					new XMLDataObject(CuestionarioDAO.TABLA_VISITA, "Visita", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_VISITA, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_SECCION01, "Seccion01", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION01, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_MORTALIDAD, "MORTALIDAD", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_MORTALIDAD, "ID IN  "+inicio)),
					
					new XMLDataObject(CuestionarioDAO.TABLA_MIGRACION, "MIGRACION", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_MIGRACION, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_IMIGRACION, "IMIGRACION", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_IMIGRACION, "ID IN  "+inicio)),
								
					new XMLDataObject(CuestionarioDAO.TABLA_HOGAR, "Hogar", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					    getExportarService().getRegistros(CuestionarioDAO.TABLA_HOGAR, "ID IN  "+ inicio)),
					    
					new XMLDataObject(CuestionarioDAO.TABLA_INCENTIVOS, "Incentivos", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
							    getExportarService().getRegistros(CuestionarioDAO.TABLA_INCENTIVOS, "ID IN  "+ inicio)),
							    
					new XMLDataObject(CuestionarioDAO.TABLA_SECCION03, "Seccion03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION03, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05, "Seccion04_05", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION04_05, "ID IN  "+inicio)), 
					new XMLDataObject(CuestionarioDAO.TABLA_SECCION04_05_DET, "Seccion04_05_det", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_SECCION04_05_DET, "ID IN  "+inicio)), 
					new XMLDataObject(CuestionarioDAO.TABLA_CSVISITA, "CSVISITA", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					    getExportarService().getRegistros(CuestionarioDAO.TABLA_CSVISITA, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_SALUD, "Salud", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					    getExportarService().getRegistros(CuestionarioDAO.TABLA_SALUD, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CAP04_07, "CAP04_07", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CAP04_07, "ID IN  "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CSSECCION_08, "CSSECCION_08", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
					    getExportarService().getRegistros(CuestionarioDAO.TABLA_CSSECCION_08, "ID IN "+inicio)),
					    
					
					//individual
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_01_03, "CISECCION_01_03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_01_03, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_Individual, "CARATULA_INDIVIDUAL", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_Individual, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02, "CISECCION_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_02, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_02T, "CISECCION_02T", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_02T, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, "CICALENDARIO_TRAMO", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO_TRAMO, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO, "CICALENDARIO", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO, "ID IN "+inicio)),
					
					new XMLDataObject(CuestionarioDAO.TABLA_DISCAPACIDAD, "DISCAPACIDAD", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_DISCAPACIDAD, "ID IN "+inicio)),
					
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04A, "CISECCION_04A", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04A, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B, "CISECCION_04B", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "CISECCION_04B_TARJETA", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B_TARJETA, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_04B2, "CISECCION_04B2", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_04B2, "ID IN "+inicio)),
//					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT, "CISECCION_04DIT", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
//						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_DIT, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_DIT_02, "CISECCION_04DIT_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
								getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_DIT_02, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05_07 , "CISECCION_05_07", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_05_07, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4 , "CICALENDARIO_TRAMO_COL4", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO_TRAMO4, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CALENDARIO04 , "CICALENDARIO_COL04", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>)
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CALENDARIO04, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_05 , "CISECCION_05", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_05, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_08 , "CISECCION_08", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_08, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_09 , "CISECCION_09", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_09, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_01 , "CISECCION_10_01", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_01, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_02 , "CISECCION_10_02", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_02, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_03 , "CISECCION_10_03", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_03, "ID IN "+inicio)),
					new XMLDataObject(CuestionarioDAO.TABLA_CISECCION_10_04 , "CISECCION_10_04", XMLDataObject.ACTUALIZAR_PRIMERO, (List<Map<String, Object>>) 
						getExportarService().getRegistros(CuestionarioDAO.TABLA_CISECCION_10_04, "ID IN "+inicio))); 	
				 String rutaZip = xmlregistros.getAbsolutePath().replace("xml", "zip");
				 String rutados = xmlRegistroscopia.getAbsolutePath().replace("xml","zip");
				 String rutadoscompar = xmlregiscopiacompar.getAbsolutePath().replace("xml","zip");
				if (!rutaZip.isEmpty()) {
					Util.zip(rutaZip, xmlregistros);
					Util.zip(rutados, xmlregistros);
					Util.zip(rutadoscompar, xmlregistros);
				}
				xmlregistros.delete();
				ids=null;
				contador=0;
				}
		}
	}
	private ImpExpService getExportarService() {
		if (exportarService == null) {
			exportarService = ImpExpService.getInstance(this.fragmentForm.getActivity().getApplicationContext());
		}
		return exportarService;
	}

	private void escribirXML() throws Exception {		
		try {			
            crearArchivo(this.rutaArchivo, exportables.toArray(new SegmentacionFiltro[exportables.size()]));
        } catch (FileNotFoundException e) {
        	
        	throw new Exception("El archivo no pudo ser encontrado.");
		} catch (IOException e) {
			throw new Exception("El archivo no pudo ser leido.");
		} catch (IllegalArgumentException e) {
			
			throw new Exception("No se pudo cargar los datos en la Base de Datos.");
		} catch (IllegalAccessException e) {
			
		} finally {
        }
	}		

	@Override
	public void onCancel() {
	}

	@Override
	public void onAccept() {
	}

	@Override
	public void update(Observable observable, Object data) {
		if (data == null) {
			return;
		}
		XMLObject.BeansProcesados procesado = (BeansProcesados) data;
		this.cantidadEscribir = procesado.getTotal();
		this.cantidadEscrito = procesado.getProcesado();
//		calcularPorcentajeProcesado();
		pDialog.setMax(this.cantidadEscribir);
		publishProgress(String.valueOf(this.cantidadEscrito));
	}
	
	private void calcularPorcentajeProcesado() {
		int cantidadProcesado = 0;
		if (cantidadEscribir == cantidadEscrito) {
			cantidadProcesado = 100;
		} else {
			cantidadProcesado += (cantidadEscrito*100)/cantidadEscribir;
		}		
		publishProgress(String.valueOf(cantidadProcesado));
	}

	@Override
	public FragmentForm getFragmentForm() {
		// TODO Auto-generated method stub
		return this.fragmentForm;
	}
	
	
}
