package gob.inei.endes2024.receivers;

import gob.inei.endes2024.activity.LoginActivity;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.gpsLocation.UserLocationWorkingAndNotWorking;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class InicioMovilReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
		 	App.USER_LOCATION_STATUS = App.APP_OFF; 
            Intent pushIntent = new Intent(context, UserLocationWorkingAndNotWorking.class);
            context.startService(pushIntent);
            
//            Intent i = new Intent(context, LoginActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(i);
     }
	}

}
