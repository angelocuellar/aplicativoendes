package gob.inei.endes2024.listener;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;



public class UserLocationListenerSMS extends Service implements LocationListener {
    
	android.location.Location location;
    
    public double latitude;
    public double longitude;
    public double altitude;
    public int tipoEnvio=9;
    public double accuracy=9;
    
    
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    
	private final Context mContext;
	
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    protected LocationManager locationManager;

    public UserLocationListenerSMS(Context context) {
        this.mContext = context;
        getLocation();
    }

    public android.location.Location getLocation() {
        try {
        	
			locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		 
//			Log.e("NNN isGPSEnabled", "pppp"+isGPSEnabled);
//			Log.e("NNN isNetworkEnabled", "aaaa"+isNetworkEnabled);
		 
			if (!isGPSEnabled && !isNetworkEnabled) {
//				Log.e("GGGGGGGGGG", "GGGGGGGGGGGGGG");
				
			} 
			else {
				this.canGetLocation = true;

//				Log.e("PARA MENSAJES", "SSSSSSSSSSSSSS");
				
				if (isGPSEnabled) {
					locationManager.removeUpdates(this);
					locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//					Log.e("GPS Enabled", "GPS Enabled");
//					Log.e("ZZZZZZZZZZZZZZZZZ", "" + locationManager);
					//locationManager.removeUpdates(this);
					// locationManager.removeUpdates(listener);

					if (locationManager != null) {
//						Log.e("AAAAAAAAAAAA", "AAAAAAAAAAAAAA");

						location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						// location=null;
//						Log.e("AAAAA location", "" + location);
//						Log.e("satellites","" + location.getExtras().getInt("satellites"));
						if (location == null /*|| (location != null && location.getExtras().getInt("satellites") <= 4)*/) {
							location = null;
							//tipoEnvio = 3;
						} else {
//							Log.e("satellites22","satellites22" + location.getExtras().getInt("satellites"));
//							Log.e("satelite", "satelitelll");
							updateGPSCoordinates();
							tipoEnvio = 1;//envio por GPS la tablet

						}
					}
				}
				
				if (isNetworkEnabled) {
//					Log.e("SSSSSSSSSSSSSS", "SSSSSSSSSSSSSS");
					//Log.e("location", ""+location);
					if (location == null) {
//						Log.e("nett", "netttt");
						locationManager.removeUpdates(this);
						locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//						Log.e("Network", "Network");
						if (locationManager != null) {
							location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//							Log.e("getAccuracy ", ""+location.getAccuracy());
							updateGPSCoordinates();
							tipoEnvio=2;
						}
					}
				}				

			}
		} catch (Exception e) {
//			Log.e("Error : Location","Impossible to connect to LocationManager", e);
//			Intent pushIntentNotWorking = new Intent(this.activity, UserLocationWorkingAndNotWorking.class);
//			pushIntentNotWorking.
//			this.startService(pushIntentNotWorking);
		}
        
        

		return location;
    }

    public void updateGPSCoordinates() {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            altitude = location.getAltitude();
            accuracy = location.getAccuracy();
            
//			if(latitude==0.0){
//				location = null;									
//			}
//			else{
//				tipoEnvio=6;
//			}
        }
    }

    public boolean canGetLocation() {
            return this.canGetLocation;
    }

    public void onLocationChanged(Location location) {
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
