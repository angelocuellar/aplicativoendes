package gob.inei.endes2024.listener;

import gob.inei.dnce.components.MasterActivity;
import gob.inei.dnce.listeners.NavigationClickListener;
import gob.inei.endes2024.activity.CuestionarioFragmentActivity;
import gob.inei.endes2024.activity.CuestionarioSaludFragmentActivity;
import gob.inei.endes2024.activity.ExportacionFragmentActivity;
import gob.inei.endes2024.activity.ReporteHMSFragmentActivity;
import gob.inei.endes2024.fragment.ExportacionFragment;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MyNavigationClickListener extends NavigationClickListener {
	
	public MyNavigationClickListener(MasterActivity activity, int opcionId,
			ListView drawerList, DrawerLayout drawerLayout) {
		super(activity, opcionId, drawerList, drawerLayout);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Intent i = null;
		position++;
		switch (position) {
		case 1:			
			i = new Intent(getActivity(),CuestionarioSaludFragmentActivity.class);
			break;
		case 2:
			i = new Intent(getActivity(),CuestionarioFragmentActivity.class);
			break;
		case 3:
			i = new Intent(getActivity(),ReporteHMSFragmentActivity.class);
			break;
		case 4:
			i= new Intent(getActivity(),ExportacionFragmentActivity.class);
			ExportacionFragment.PERIODO=null;
			break;
		case 5:
			getActivity().uploadData();
			break;
		case 6:
			getActivity().uploadMarco();
			break;	
		default:
			i = null;
			break;
		}		
		if (i == null) {
			return;
		}
		getDrawerList().setItemChecked(position, true);
		getDrawerLayout().closeDrawer(getDrawerList());
		if (position != getOpcionId()) {
			getActivity().startActivity(i);		
			getActivity().finish();
		} else {
			getActivity().nextFragment(0);
		}
	}
	
}
