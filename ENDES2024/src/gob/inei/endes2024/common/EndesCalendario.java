package gob.inei.endes2024.common;

import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.util.Util;
import gob.inei.dnce.util.Util.COMPLETAR;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Endes_Resultado_vivienda;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class EndesCalendario {
	private static Context context;
	private static CuestionarioService cuestionarioService;
	private static Seccion01Service serviceseccion01;
	private static List<CICALENDARIO_TRAMO_COL01_03> lista;
	private static Seccion01 vacioseccion01 = null;
	public static Auditoria registro=null;
	public static String espacio=" ";
	private static SeccionCapitulo[] seccionesCargadoTramosCol01_03 = new SeccionCapitulo[] { new SeccionCapitulo(
			0, -1, -1, "QITRAMO_ID", "QIMES_INI", "QIANIO_INI", "QIR_INICIAL",
			"QIMES_FIN", "QIANIO_FIN", "QIR_FINAL", "NINIO_ID",
			"TERMINACION_ID", "EMBARAZO_ID", "METODO_ID", "NINGUNO_ID", "ID",
			"QICOL2", "QICOL3", "HOGAR_ID", "PERSONA_ID", "QICANTIDAD") };
	private static SeccionCapitulo[] SeccionescargadoSeccion01_03 = new SeccionCapitulo[] { new SeccionCapitulo(
			0, -1, -1, "QI106", "QI105D", "QI105M", "QI105Y", "QI105CONS",
			"QI208", "QI226", "QI227", "QI226CONS", "QI230", "QI231_M",
			"QI231_Y", "QI233", "QI234", "QI235A", "QI235B_M", "QI235B_Y",
			"QI302_01", "QI302_02", "QI302_03", "QI302_04", "QI302_06",
			"QI302_05A","QI302_05B", "QI302_07", "QI302_08", "QI302_09", "QI302_10",
			"QI302_11", "QI302_12", "QI302_13", "QI302_14", "QI230", "QI231_M",
			"QI231_Y", "QI233", "QI310", "QI311_A", "QI311_B", "QI311_C",
			"QI311_D", "QI311_E", "QI311_F", "QI311_G", "QI311_H", "QI311_I",
			"QI311_J", "QI311_K", "QI311_L", "QI311_M", "QI311_X", "QI311_O",
			"QI312", "QI316M", "QI316Y", "QI226", "QI227", "QI234", "QI320",
			"QI315M", "QI315Y", "QICAL_CONS", "QIOBS_CAL", "QI315Y", "QI316Y",
			"QI_FANT", "QI_FANT_O", "ID", "HOGAR_ID", "PERSONA_ID", "QI320") };
	private static SeccionCapitulo[] seccionesGrabadoTramosCol01_03 = new SeccionCapitulo[] { new SeccionCapitulo(
			0, -1, -1, "QITRAMO_ID", "QIMES_INI", "QIANIO_INI", "QIR_INICIAL",
			"QIMES_FIN", "QIANIO_FIN", "QIR_FINAL", "NINIO_ID",
			"TERMINACION_ID", "EMBARAZO_ID", "METODO_ID", "NINGUNO_ID",
			"QICANTIDAD", "QICOL3") };
	private static SeccionCapitulo[] seccionesCargadoNinio = new SeccionCapitulo[] { new SeccionCapitulo(
			0, -1, -1, "QI212", "QI212_NOM", "QI213", "QI214", "QI215D",
			"QI215M", "QI215Y", "QI216", "QI217", "QI217CONS", "QI218",
			"QI219", "QI220U", "QI220N", "QI220A", "QI221", "ID", "HOGAR_ID",
			"PERSONA_ID") };
	private static SeccionCapitulo[] seccionesCargadoTerminaciones = new SeccionCapitulo[] { new SeccionCapitulo(
			0, -1, -1, "TERMINACION_ID", "QI235_M", "QI235_Y", "QI235_D", "ID",
			"HOGAR_ID", "PERSONA_ID") };
	
	public static SeccionCapitulo[] seccionesGrabadoDiscapacidad = new SeccionCapitulo[] { new SeccionCapitulo(0, -1,-1, "QD333_1", "QD333_2", "QD333_3", "QD333_4", "QD333_5", "QD333_6","ID", "HOGAR_ID", "PERSONA_ID","NINIO_ID","CUESTIONARIO_ID") };
	private static CISECCION_01_03 seccion01_03;
	private static Calendar fecha_actual = null;
	private static List<CISECCION_02> ninios;
	private static CICALENDARIO_TRAMO_COL01_03 tramo1 = null;
	private static CICALENDARIO_TRAMO_COL01_03 tramo2 = null;
	private static CICALENDARIO_TRAMO_COL01_03 tramo;
	private static boolean tienenacimientoenfechaactual = false;

	// vivienda
	private static Endes_Resultado_vivienda resultado_viv = null;
	private static Visita_Viv viv_visita = null;
	private static Visita vivitashogarvacias = null;

	private static SeccionCapitulo[] seccionescargadoresultado_final = new SeccionCapitulo[] { new SeccionCapitulo(0, -1, -1, "CONGLOMERADO", "ID", "NSELV", "RESULTADOVIV_FINAL",
			"NUM_HOGARES") };
	private static SeccionCapitulo[] seccionesgrabado_visita_final = new SeccionCapitulo[] { new SeccionCapitulo(0, -1, -1, "QVVRESUL", "QVVMIN_FIN", "QVVHORA_FIN") };

	public EndesCalendario(Context context) {
		EndesCalendario.context = context;
	}

	public static void VerificacionCalendarioColumnas_01_03(Integer id,
			Integer hogar_id, Integer persona_id, Calendar fecha) {
		seccion01_03 = getCuestionarioService().getCISECCION_01_03(id,
				hogar_id, persona_id, SeccionescargadoSeccion01_03);
		lista = getCuestionarioService().getTramosdelcalendario(id, hogar_id,
				persona_id, seccionesCargadoTramosCol01_03);
		fecha_actual = fecha;
		ninios = getCuestionarioService().getNacimientosparacalendario(id,
				hogar_id, persona_id, fecha_actual.get(Calendar.YEAR) - 5,
				seccionesCargadoNinio);
		if ((!Util.esDiferente(seccion01_03.qi302_01, 2)
				&& !Util.esDiferente(seccion01_03.qi302_02, 2)
				&& !Util.esDiferente(seccion01_03.qi302_03, 2)
				&& !Util.esDiferente(seccion01_03.qi302_04, 2)
				&& !Util.esDiferente(seccion01_03.qi302_05a, 2)
				&& !Util.esDiferente(seccion01_03.qi302_05b, 2)
				&& !Util.esDiferente(seccion01_03.qi302_06, 2)
				&& !Util.esDiferente(seccion01_03.qi302_07, 2)
				&& !Util.esDiferente(seccion01_03.qi302_08, 2)
				&& !Util.esDiferente(seccion01_03.qi302_09, 2)
				&& !Util.esDiferente(seccion01_03.qi302_10, 2)
				&& !Util.esDiferente(seccion01_03.qi302_11, 2)
				&& !Util.esDiferente(seccion01_03.qi302_12, 2)
				&& !Util.esDiferente(seccion01_03.qi302_13, 2) && !Util
					.esDiferente(seccion01_03.qi302_14, 2))
				|| ((seccion01_03.qi315y != null && seccion01_03.qi315y < App.ANIOPORDEFECTO) || (seccion01_03.qi316y != null && seccion01_03.qi316y < App.ANIOPORDEFECTO))) {
			if (lista.size() > 0) {
				List<CICALENDARIO_TRAMO_COL01_03> copia = new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
				copia.addAll(lista);
				for (CICALENDARIO_TRAMO_COL01_03 tramo : copia) {
					lista.remove(tramo);
					EliminacionDeTablas(tramo.id, tramo.hogar_id,
							tramo.persona_id, tramo.qitramo_id);
				}
			}

			if (lista.size() == 0 && seccion01_03.qi320 != null) {
				RegistrarNacimientosPrimeroenTramos();
				Registrarterminaciones();
				TerminacionesSiesqueExistiera(id, hogar_id, persona_id);
				RegistrarEmbarazoOMetodoAnticonceptivoONinguno();
				tramo1 = null;
				tramo2 = null;
				tramo = null;
				if (!CalendarioCompletado(id, hogar_id, persona_id,
						fecha_actual)) {
					ExisteEspaciosEnBlancoEnElCalendarioTramo(id, hogar_id,
							persona_id);
				}
			}
		}
	}

	public static void RegistrarNacimientosPrimeroenTramos() {
		Integer tramoinicial = -1;
		boolean multiple = true;
		Calendar fechadenacimientoanterior = null;
		Calendar fechadenacimientoactual = null;
		Integer id = null;
		if (ninios.size() > 0) {
			fechadenacimientoanterior = new GregorianCalendar(ninios.get(0).qi215y,Integer.parseInt(ninios.get(0).qi215m) - 1,Integer.parseInt(ninios.get(0).qi215d));
			id = ninios.get(0).qi212;
			for (CISECCION_02 ninio : ninios) {
				Integer mes = Integer.parseInt(ninio.qi215m.toString());
				if (!Util.esDiferente(mes, (fecha_actual.get(Calendar.MONTH)+1)) && !Util.esDiferente(ninio.qi215y,	fecha_actual.get(Calendar.YEAR))) {
					tienenacimientoenfechaactual = true;
				}
				CICALENDARIO_TRAMO_COL01_03 tramo = new CICALENDARIO_TRAMO_COL01_03();
				CICALENDARIO_TRAMO_COL01_03 verificacion_tramo = new CICALENDARIO_TRAMO_COL01_03();
				fechadenacimientoactual = new GregorianCalendar(ninio.qi215y,
						Integer.parseInt(ninio.qi215m.toString()) - 1,
						Integer.parseInt(ninio.qi215d.toString()));
				if (fechadenacimientoanterior.compareTo(fechadenacimientoactual) == 0 && id != ninio.qi212) {
					fechadenacimientoanterior = fechadenacimientoactual;
					continue;
				} else {
					fechadenacimientoanterior = fechadenacimientoactual;
				}
				tramo.id = ninio.id;
				verificacion_tramo = getCuestionarioService().getTramoparaNacimientos(ninio.id, ninio.hogar_id,ninio.persona_id, ninio.qi212,seccionesCargadoTramosCol01_03);
				if (verificacion_tramo != null) {
					EliminacionDeTablas(ninio.id, ninio.hogar_id,ninio.persona_id, verificacion_tramo.qitramo_id);
					tramo.qitramo_id = verificacion_tramo.qitramo_id;
					tramoinicial = verificacion_tramo.qitramo_id;
				} else {
					tramo.qitramo_id = null;
				}
				tramo.hogar_id = ninio.hogar_id;
				tramo.persona_id = ninio.persona_id;
				tramo.ninio_id = ninio.qi212;
				tramo.terminacion_id = null;
				Integer indice = MyUtil.DeterminarIndice(ninio.qi215y,Integer.parseInt(ninio.qi215m.toString()))+ ninio.qi220a;
				indice = indice - 1;
				indice = indice > 72 ? 72 : indice;
				tramo.qimes_ini = MyUtil.DeterminarMesInicial(indice);
				tramo.qianio_ini = MyUtil.DeterminarAnioInicial(indice);
				tramo.qimes_fin = Integer.parseInt(ninio.qi215m.toString());
				tramo.qianio_fin = ninio.qi215y;
				tramo.qir_inicial = App.CALENDARIOEMBARAZO;
				tramo.qir_final = App.CALENDARIONACIMIENTO;
				tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,
						tramo.qimes_ini)
						- MyUtil.DeterminarIndice(tramo.qianio_fin,
								tramo.qimes_fin) + 1;
				tramoinicial = grabar(tramo);
			}
		}
	}

	public static void Registrarterminaciones() {
		Integer tramoinicial = -1;
		CICALENDARIO_TRAMO_COL01_03 tramo = new CICALENDARIO_TRAMO_COL01_03();
		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo = new CICALENDARIO_TRAMO_COL01_03();
		if (seccion01_03 != null && seccion01_03.qi230 != null
				&& !Util.esDiferente(seccion01_03.qi230, 1)
				&& seccion01_03.qi231_y != null
				&& seccion01_03.qi231_y >= App.ANIOPORDEFECTO) {
			verificacion_tramo = getCuestionarioService()
					.getTramoparaTerminaciones(seccion01_03.id,
							seccion01_03.hogar_id, seccion01_03.persona_id,
							App.TERMINACION_IDDEFAULT,
							seccionesCargadoTramosCol01_03);
			if (verificacion_tramo != null) {
				EliminacionDeTablas(seccion01_03.id, seccion01_03.hogar_id,
						seccion01_03.persona_id, verificacion_tramo.qitramo_id);
				tramo.qitramo_id = verificacion_tramo.qitramo_id;
				tramoinicial = verificacion_tramo.qitramo_id;
			} else {
				tramo.qitramo_id = null;
			}
			tramo.id = seccion01_03.id;
			tramo.hogar_id = seccion01_03.hogar_id;
			tramo.persona_id = seccion01_03.persona_id;
			Integer indice = MyUtil.DeterminarIndice(seccion01_03.qi231_y,
					Integer.parseInt(seccion01_03.qi231_m.toString()))
					+ seccion01_03.qi233;
			indice = indice - 1;
			indice = indice > 72 ? 72 : indice;
			tramo.qimes_ini = MyUtil.DeterminarMesInicial(indice);
			tramo.qianio_ini = MyUtil.DeterminarAnioInicial(indice);
			tramo.qimes_fin = Integer.parseInt(seccion01_03.qi231_m.toString());
			tramo.qianio_fin = seccion01_03.qi231_y;
			tramo.terminacion_id = App.TERMINACION_IDDEFAULT;
			tramo.qir_inicial = seccion01_03.qi233 > 1 ? App.CALENDARIOEMBARAZO
					: App.CALENDARIOTERMINACION;
			tramo.qir_final = App.CALENDARIOTERMINACION;
			// tramo.qicantidad=seccion01_03.qi233;
			tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,
					tramo.qimes_ini)
					- MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)
					+ 1;
			tramoinicial = grabar(tramo);
		}
	}

	public static void TerminacionesSiesqueExistiera(Integer id,
			Integer hogar_id, Integer persona_id) {
		Integer tramoinicial = -1;
		if (seccion01_03 != null && seccion01_03.qi234 != null
				&& !Util.esDiferente(seccion01_03.qi234, 1)) {
			List<CISECCION_02T> embarazoentermino = getCuestionarioService()
					.getTerminacionesParaCalendario(id, hogar_id, persona_id,
							App.ANIOPORDEFECTO, seccionesCargadoTerminaciones);
			if (embarazoentermino.size() > 0) {
				for (CISECCION_02T termino : embarazoentermino) {
					CICALENDARIO_TRAMO_COL01_03 tramo = new CICALENDARIO_TRAMO_COL01_03();
					CICALENDARIO_TRAMO_COL01_03 verificacion_tramo = new CICALENDARIO_TRAMO_COL01_03();
					verificacion_tramo = getCuestionarioService()
							.getTramoparaTerminaciones(termino.id,
									termino.hogar_id, termino.persona_id,
									termino.terminacion_id,
									seccionesCargadoTramosCol01_03);
					if (verificacion_tramo != null) {
						EliminacionDeTablas(termino.id, termino.hogar_id,
								termino.persona_id,
								verificacion_tramo.qitramo_id);
						tramo.qitramo_id = verificacion_tramo.qitramo_id;
						tramoinicial = verificacion_tramo.qitramo_id;
					} else {
						tramo.qitramo_id = null;
					}
					tramo.id = termino.id;
					tramo.hogar_id = termino.hogar_id;
					tramo.persona_id = termino.persona_id;
					Integer indice = MyUtil.DeterminarIndice(termino.qi235_y,
							termino.qi235_m) + termino.qi235_d;
					indice = indice - 1;
					indice = indice > 72 ? 72 : indice;
					tramo.qimes_ini = MyUtil.DeterminarMesInicial(indice);
					tramo.qianio_ini = MyUtil.DeterminarAnioInicial(indice);
					tramo.qimes_fin = termino.qi235_m;
					tramo.qianio_fin = termino.qi235_y;
					tramo.terminacion_id = termino.terminacion_id;
					tramo.qir_inicial = termino.qi235_d > 1 ? App.CALENDARIOEMBARAZO
							: App.CALENDARIOTERMINACION;
					tramo.qir_final = App.CALENDARIOTERMINACION;
					tramo.qicantidad = MyUtil.DeterminarIndice(
							tramo.qianio_ini, tramo.qimes_ini)
							- MyUtil.DeterminarIndice(tramo.qianio_fin,
									tramo.qimes_fin) + 1;
					// tramo.qicantidad=termino.qi235_d;
					tramoinicial = grabar(tramo);
				}
			}
		}
	}

	public static void RegistrarEmbarazoOMetodoAnticonceptivoONinguno() {
		Integer tramoinicial = -1;
		CICALENDARIO_TRAMO_COL01_03 tramo = new CICALENDARIO_TRAMO_COL01_03();
		CICALENDARIO_TRAMO_COL01_03 verificacion_tramo = new CICALENDARIO_TRAMO_COL01_03();
		CICALENDARIO_COL01_03 verificarmetodoactual = new CICALENDARIO_COL01_03();
		Integer mesactual = fecha_actual.get(Calendar.MONTH) + 1;
		Integer anioactual = fecha_actual.get(Calendar.YEAR);
		if (seccion01_03 != null && seccion01_03.qi226 != null
				&& !Util.esDiferente(seccion01_03.qi226, 1)) {// embarazo
			verificacion_tramo = getCuestionarioService().getTramoparaEmbarazo(
					seccion01_03.id, seccion01_03.hogar_id,
					seccion01_03.persona_id, App.EMBARAZO_IDDEFAULT,
					seccionesCargadoTramosCol01_03);
			if (verificacion_tramo != null) {
				EliminacionDeTablas(seccion01_03.id, seccion01_03.hogar_id,
						seccion01_03.persona_id, verificacion_tramo.qitramo_id);
				tramo.qitramo_id = verificacion_tramo.qitramo_id;
				tramoinicial = verificacion_tramo.qitramo_id;
			} else {
				tramo.qitramo_id = null;
			}
			tramo.id = seccion01_03.id;
			tramo.hogar_id = seccion01_03.hogar_id;
			tramo.persona_id = seccion01_03.persona_id;
			Integer indice = MyUtil.DeterminarIndice(anioactual, mesactual)
					+ seccion01_03.qi227;
			indice = indice - 1;
			indice = indice > 72 ? 72 : indice;
			tramo.qimes_ini = MyUtil.DeterminarMesInicial(indice);
			tramo.qianio_ini = MyUtil.DeterminarAnioInicial(indice);
			tramo.qimes_fin = mesactual;
			tramo.qianio_fin = anioactual;
			tramo.embarazo_id = App.EMBARAZO_IDDEFAULT;
			tramo.qir_inicial = App.CALENDARIOEMBARAZO;
			tramo.qir_final = App.CALENDARIOEMBARAZO;
			tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,
					tramo.qimes_ini)
					- MyUtil.DeterminarIndice(tramo.qianio_fin, tramo.qimes_fin)
					+ 1;
			// tramo.qicantidad=seccion01_03.qi227;
			grabar(tramo);
		} else if (seccion01_03 != null
				&& !tienenacimientoenfechaactual
				&& (seccion01_03.qi310 != null && !Util.esDiferente(
						seccion01_03.qi310, 1))
				|| (seccion01_03.qi311_a != 0 && !Util.esDiferente(
						seccion01_03.qi311_a, 1))) {// Metodo actual
			if (seccion01_03.qi316m != null || seccion01_03.qi315m != null) {
				Integer mesmetodo = seccion01_03.qi316m == null ? Integer
						.parseInt(seccion01_03.qi315m) : Integer
						.parseInt(seccion01_03.qi316m);
				Integer aniometodo = seccion01_03.qi316y == null ? seccion01_03.qi315y
						: seccion01_03.qi316y;

				if (aniometodo < App.ANIOPORDEFECTO) {
					mesmetodo = App.MESPORDEFECTO;
					aniometodo = App.ANIOPORDEFECTO;
				}
				verificacion_tramo = getCuestionarioService()
						.getTramoparaMetodoActual(seccion01_03.id,
								seccion01_03.hogar_id, seccion01_03.persona_id,
								App.METODO_IDDEFAULT,
								seccionesCargadoTramosCol01_03);
				boolean mesesigualalnacimiento = false;
				if (ninios.size() > 0 && CodigoCorrecto().equals("1")
						|| CodigoCorrecto().equals("2")
						|| CodigoCorrecto().equals("5")) {
					for (CISECCION_02 n : ninios) {
						if (n.qi215m != null) {
							Integer mes = Integer.parseInt(n.qi215m.toString());
							if (!Util.esDiferente(mesmetodo, mes)
									&& !Util.esDiferente(n.qi215y, aniometodo)) {
								mesesigualalnacimiento = true;
								break;
							}
						}
					}
				}
				if (mesesigualalnacimiento) {
					mesmetodo = mesmetodo + 1;
				}
				Integer cantidad = MyUtil.DeterminarIndice(aniometodo,
						mesmetodo)
						- MyUtil.DeterminarIndice(anioactual, mesactual);
				Integer indice = MyUtil.DeterminarIndice(anioactual, mesactual)
						+ cantidad;
				if (verificacion_tramo != null
						&& verificacion_tramo.qicol3 == null) { // &&
																// verificacion_tramo.qimes_ini!=null
																// &&
																// verificacion_tramo.qianio_ini!=null
																// &&
																// (verificacion_tramo.qimes_ini+1)!=mesmetodo
																// ||
																// verificacion_tramo.qianio_ini!=aniometodo){
					EliminacionDeTablas(seccion01_03.id, seccion01_03.hogar_id,
							seccion01_03.persona_id,
							verificacion_tramo.qitramo_id);
					tramo.qitramo_id = verificacion_tramo.qitramo_id;
					tramoinicial = verificacion_tramo.qitramo_id;
				} else {
					if (verificacion_tramo != null) {
						tramo.qitramo_id = verificacion_tramo.qitramo_id != null ? verificacion_tramo.qitramo_id
								: null;
						tramo.qicol3 = verificacion_tramo.qicol3 != null ? verificacion_tramo.qicol3
								: null;
					}
				}
				tramo.id = seccion01_03.id;
				tramo.hogar_id = seccion01_03.hogar_id;
				tramo.persona_id = seccion01_03.persona_id;
				indice = indice > 72 ? 72 : indice;
				tramo.qimes_ini = MyUtil.DeterminarMesInicial(indice);
				tramo.qianio_ini = MyUtil.DeterminarAnioInicial(indice);
				tramo.qimes_fin = mesactual;
				tramo.qianio_fin = anioactual;
				tramo.metodo_id = App.METODO_IDDEFAULT;
				tramo.qir_inicial = CodigoCorrecto();
				tramo.qir_final = CodigoCorrecto();
				if ("1".equals(tramo.qir_final) || "2".equals(tramo.qir_final)) {
					tramo.qicol3 = ObtenerCol3SiesEsterilizacion();
				} else {
					tramo.qicol3 = seccion01_03.qi_fant;
					tramo.qicol3_o = seccion01_03.qi_fant_o;
				}
				tramo.qicantidad = cantidad + 1;
				tramoinicial = grabar(tramo);
			}

		} else {
			if (seccion01_03 != null && seccion01_03.qi226 != null
					&& Util.esDiferente(seccion01_03.qi226, 1)
					&& Util.esDiferente(seccion01_03.qi310, 1)
					&& !tienenacimientoenfechaactual) {
				lista = getCuestionarioService()
						.getTramosdelcalendario(seccion01_03.id,
								seccion01_03.hogar_id, seccion01_03.persona_id,
								seccionesCargadoTramosCol01_03);

				tramo.id = seccion01_03.id;
				tramo.hogar_id = seccion01_03.hogar_id;
				tramo.persona_id = seccion01_03.persona_id;
				verificacion_tramo = getCuestionarioService()
						.getTramoparaNingunMetodo(seccion01_03.id,
								seccion01_03.hogar_id, seccion01_03.persona_id,
								App.NINGUNMETODO_IDDEFAULT,
								seccionesCargadoTramosCol01_03);
				if (verificacion_tramo != null) {
					EliminacionDeTablas(seccion01_03.id, seccion01_03.hogar_id,
							seccion01_03.persona_id,
							verificacion_tramo.qitramo_id);
					tramo.qitramo_id = verificacion_tramo.qitramo_id;
					tramoinicial = verificacion_tramo.qitramo_id;
				} else {
					tramo.qitramo_id = null;
				}
				if (lista.size() == 0) {
					tramo.qimes_ini = App.MESPORDEFECTO;
					tramo.qianio_ini = App.ANIOPORDEFECTO;
				} else {
					tramo.qimes_ini = mesactual;
					tramo.qianio_ini = anioactual;
				}
				tramo.qimes_fin = mesactual;
				tramo.qianio_fin = anioactual;
				tramo.ninguno_id = App.NINGUNMETODO_IDDEFAULT;
				tramo.qir_inicial = App.CALENDARIONINGUNMETODO;
				tramo.qir_final = App.CALENDARIONINGUNMETODO;
				tramo.qicantidad = MyUtil.DeterminarIndice(tramo.qianio_ini,
						tramo.qimes_ini)
						- MyUtil.DeterminarIndice(anioactual, mesactual) + 1;
				tramoinicial = grabar(tramo);
			}
		}
	}

	public static String CodigoCorrecto() {
		if (seccion01_03.qi311_a != null
				&& !Util.esDiferente(seccion01_03.qi311_a, 1))
			return "1";
		if (seccion01_03.qi311_b != null
				&& !Util.esDiferente(seccion01_03.qi311_b, 1))
			return "2";
		if (seccion01_03.qi311_c != null
				&& !Util.esDiferente(seccion01_03.qi311_c, 1))
			return "3";
		if (seccion01_03.qi311_d != null
				&& !Util.esDiferente(seccion01_03.qi311_d, 1))
			return "4";
		if (seccion01_03.qi311_e != null
				&& !Util.esDiferente(seccion01_03.qi311_e, 1))
			return "5";
		if (seccion01_03.qi311_f != null
				&& !Util.esDiferente(seccion01_03.qi311_f, 1))
			return "6";
		if (seccion01_03.qi311_g != null
				&& !Util.esDiferente(seccion01_03.qi311_g, 1))
			return "7";
		if (seccion01_03.qi311_h != null
				&& !Util.esDiferente(seccion01_03.qi311_h, 1))
			return "8";
		if (seccion01_03.qi311_i != null
				&& !Util.esDiferente(seccion01_03.qi311_i, 1))
			return "9";
		if (seccion01_03.qi311_j != null
				&& !Util.esDiferente(seccion01_03.qi311_j, 1))
			return "J";
		if (seccion01_03.qi311_k != null
				&& !Util.esDiferente(seccion01_03.qi311_k, 1))
			return "K";
		if (seccion01_03.qi311_l != null
				&& !Util.esDiferente(seccion01_03.qi311_l, 1))
			return "L";
		if (seccion01_03.qi311_m != null
				&& !Util.esDiferente(seccion01_03.qi311_m, 1))
			return "M";
		if (seccion01_03.qi311_x != null
				&& !Util.esDiferente(seccion01_03.qi311_x, 1))
			return "X";
		else
			return "";

	}

	public static String ObtenerCol3SiesEsterilizacion() {
		String retorna = "";
		if (!Util.esDiferente(seccion01_03.qi312, 10)) {
			retorna = "1";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 11)) {
			retorna = "2";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 12)) {
			retorna = "5";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 13)) {
			retorna = "6";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 14)) {
			retorna = "7";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 15)) {
			retorna = "8";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 16)) {
			retorna = "A";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 20)) {
			retorna = "B";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 21)) {
			retorna = "D";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 31)) {
			retorna = "F";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 32)) {
			retorna = "H";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 96)) {
			retorna = "X";
		}
		if (!Util.esDiferente(seccion01_03.qi312, 98)) {
			retorna = "9";
		}
		return retorna;
	}

	public static void ExisteEspaciosEnBlancoEnElCalendarioTramo(Integer id,
			Integer hogar_id, Integer persona_id) {
		List<CICALENDARIO_TRAMO_COL01_03> todoslostramos = getCuestionarioService()
				.getTramosdelcalendario(id, hogar_id, persona_id,
						seccionesCargadoTramosCol01_03);
		List<CICALENDARIO_TRAMO_COL01_03> todossegundorecorrido = new ArrayList<CICALENDARIO_TRAMO_COL01_03>();
		Collections.reverse(todoslostramos);
		todossegundorecorrido.addAll(todoslostramos);
		if (todoslostramos.size() != 0) {
			for (CICALENDARIO_TRAMO_COL01_03 item : todoslostramos) {
				todossegundorecorrido.remove(item);
				if (tramo2 != null) {
					break;
				}
				tramo1 = null;
				tramo1 = item;
				for (CICALENDARIO_TRAMO_COL01_03 tramo : todossegundorecorrido) {
					Integer Mesesfin = tramo.qimes_fin;
					Integer AnioFin = tramo.qianio_fin;
					if (Mesesfin == 12) {
						Mesesfin = 0;
						AnioFin++;
					}
					Mesesfin = Mesesfin + 1;
					if (!Util.esDiferente(item.qianio_ini, AnioFin)
							&& !Util.esDiferente(item.qimes_ini, Mesesfin)
							&& Util.esDiferente(item.qitramo_id,
									tramo.qitramo_id)) {
						break;
					} else {
						tramo2 = tramo;
						break;
					}
				}
			}
			if (tramo2 == null) {
				tramo2 = new CICALENDARIO_TRAMO_COL01_03();
				tramo2.qianio_fin = App.ANIOPORDEFECTO;
				tramo2.qimes_fin = App.MESPORDEFECTO;
				tramo2.qir_final = "";
			}
			if (tramo == null) {
				tramo = new CICALENDARIO_TRAMO_COL01_03();
				tramo.id = id;
				tramo.hogar_id = hogar_id;
				tramo.persona_id = persona_id;
				tramo.qitramo_id = getCuestionarioService()
						.getUltimotramoidRegistrado(tramo.id, tramo.hogar_id,
								tramo.persona_id) + 1;
			}
			if (!CalendarioCompletado(id, hogar_id, persona_id, fecha_actual)) {
				RegistrarCuandoNoUsoNingunMetodoTramo();
				tramo1 = null;
				tramo2 = null;
				tramo = null;
				ExisteEspaciosEnBlancoEnElCalendarioTramo(id, hogar_id,
						persona_id);
			}
		}
	}

	public static void RegistrarCuandoNoUsoNingunMetodoTramo() {
		Integer Mesinicial = 0;
		Integer Mesfinal = 0;
		if (tramo2.qianio_fin != App.ANIOPORDEFECTO
				&& tramo2.qimes_fin != App.MESPORDEFECTO) {
			Mesinicial = tramo2.qimes_fin + 1;
			if (Mesinicial == 13) {
				Mesinicial = 1;
				tramo2.qianio_fin++;
			}
			Mesfinal = tramo1.qimes_ini - 1;
			if (Mesfinal == 0) {
				Mesfinal = 12;
				tramo1.qianio_ini--;
			}
		} else {
			if (!Util.esDiferente(tramo2.qimes_fin, App.MESPORDEFECTO)
					&& !Util.esDiferente(tramo2.qianio_fin, App.ANIOPORDEFECTO)) {
				Mesinicial = !tramo2.qir_final.equals("") ? tramo2.qimes_fin + 1
						: tramo2.qimes_fin;
			} else {
				Mesinicial = Mesinicial == 0 ? tramo2.qimes_fin + 1
						: Mesinicial;
			}
			Mesfinal = tramo1.qimes_ini - 1;
			if (Mesfinal == 0) {
				Mesfinal = 12;
				tramo1.qianio_ini--;
			}
			if (Mesinicial == 13) {
				Mesinicial = 1;
				tramo2.qianio_fin++;
			}
		}
		tramo.qianio_ini = tramo2.qianio_fin;
		tramo.qimes_ini = Mesinicial;
		tramo.qimes_fin = Mesfinal;
		tramo.qianio_fin = tramo1.qianio_ini;
		tramo.qir_inicial = "0";
		tramo.qir_final = "0";
		tramo.qicantidad = MyUtil
				.DeterminarIndice(tramo.qianio_ini, Mesinicial)
				- MyUtil.DeterminarIndice(tramo.qianio_fin, Mesfinal);
		tramo.qicantidad++;
		grabar(tramo);
		tramo = null;
	}

	public static boolean CalendarioCompletado(Integer id, Integer hogar_id,
			Integer persona_id, Calendar fecha) {
		boolean calendariocompleto = false;
		if (getCuestionarioService().CalendarioCompletado(id, hogar_id,
				persona_id, fecha)) {
			calendariocompleto = true;
		}
		return calendariocompleto;
	}

	public static Integer grabar(CICALENDARIO_TRAMO_COL01_03 tramo) {
		Integer tramoinicial = -1;
		try {
			getCuestionarioService().saveOrUpdate(tramo, null,
					seccionesGrabadoTramosCol01_03);
		} catch (Exception e) {
			e.getMessage();
		}
		return tramoinicial;
	}

	public static void EliminacionDeTablas(Integer id, Integer hogar_id,
			Integer persona_id, Integer tramo_id) {
		getCuestionarioService().Deletetramo(
				CuestionarioDAO.TABLA_CALENDARIO_TRAMO, id, hogar_id,
				persona_id, tramo_id);
	}

	public static CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(context);
		}
		return cuestionarioService;
	}

	public static void ModificaVisitaVivienda_final(Integer id) {
		resultado_viv = getCuestionarioService().getResultadovivienda_general(id, seccionescargadoresultado_final);
		viv_visita = getCuestionarioService().getUltima_visitaVivienda(id);
//		String fecha = getCuestionarioService().getUltimaFechaDelCuestionario(id);
		
		Log.e("resultado11",""+resultado_viv);
		Log.e("resultadovis",""+viv_visita);
		
		if (viv_visita != null &&  resultado_viv != null && resultado_viv.resultadoviv_final != null) {
			Calendar calendar = new GregorianCalendar();
			viv_visita.qvvhora_fin = calendar.get(Calendar.HOUR_OF_DAY) + "";
			viv_visita.qvvmin_fin = calendar.get(Calendar.MINUTE) + "";
			vivitashogarvacias = getCuestionarioService().getResultadosVisitaHogar(id);

			try {
				if (resultado_viv != null && vivitashogarvacias == null	&& resultado_viv.resultadoviv_final != null) {
					viv_visita.qvvresul = resultado_viv.resultadoviv_final;
					
//					viv_visita.qvvhora_fin = fecha != null ? fecha.substring(11, 13) : null;
//					viv_visita.qvvmin_fin = fecha != null ? fecha.substring(14,16) : null;
					
					// Log.e("HORA",""+viv_visita.qvvhora_fin);
					// Log.e("MINUTO",""+viv_visita.qvvmin_fin);
					Log.e("resultado11",""+resultado_viv);
					getCuestionarioService().saveOrUpdate_VisitaVivienda(viv_visita, seccionesgrabado_visita_final);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static boolean VerificarSitientePuntoGPS(Integer id) {
		viv_visita = getCuestionarioService().getUltima_visitaVivienda(id);
		boolean retornar = false;
		if (viv_visita != null && viv_visita.qvgps_alt.equals(App.GPS_OMISION)) {
			retornar = true;
		}
		return retornar;
	}
	
	/*grabado de Discapacidad*/
	public static void GrabadoGeneralDiscapacidad(DISCAPACIDAD persona_con_capacidadesdiferentes){
		if(persona_con_capacidadesdiferentes!=null){
			try {
				getCuestionarioService().saveOrUpdate_Discapacidad(persona_con_capacidadesdiferentes, seccionesGrabadoDiscapacidad);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*LISTADO PRE CENSO RESPONSABLES */
	public static void llenarResponsablesPreCensos(Activity activity, SpinnerField spinner, Integer id,Integer hogar_id) { 
        List<Seccion01> responsable = new ArrayList<Seccion01>();
        responsable.add(getvacio());       
        responsable.addAll(getSeccion01Service().getresResponsablesPreCensos (id, hogar_id));
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : responsable) {
                keysAdapter.add(u.persona_id);
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);		
	}
	public static Seccion01 getvacio() {
		if(vacioseccion01==null)
			vacioseccion01=new Seccion01();
			vacioseccion01.persona_id=null;
			vacioseccion01.qh02_1="-- SELECCIONE --";
			vacioseccion01.qh02_2="";
			return vacioseccion01;
	}
	public static Seccion01Service getSeccion01Service() {
		if (serviceseccion01 == null) {
			serviceseccion01 = Seccion01Service.getInstance(context);
		}
		return 	serviceseccion01;
	}	
	
	
	
	
	/**metodo que completa el resultado del cuestionario individual**/
	
	public static CARATULA_INDIVIDUAL CuestionarioIndividualEstaCompleto(Integer id,Integer hogar_id,Integer persona_id,Integer nro_visita){
		  CARATULA_INDIVIDUAL visitaci= new CARATULA_INDIVIDUAL();
		  
		  visitaci.id=id;
		  visitaci.hogar_id=hogar_id;
		  visitaci.persona_id=persona_id;
		  visitaci.nro_visita=nro_visita;
		  Log.e("","ESTADO:1-03  "+getCuestionarioService().getCompletadoSeccion0103CI(id, hogar_id, persona_id));
		  Log.e("","ESTADO:5-07  "+getCuestionarioService().getCompletadoSeccion0507CI(id, hogar_id, persona_id));
		  Log.e("","ESTADO:08  "+getCuestionarioService().getCompletadoSeccion008CI(id, hogar_id, persona_id));
		  Log.e("","ESTADO:10  "+getCuestionarioService().getCompletadoSeccion10CI(id, hogar_id, persona_id));
		  Log.e("","ESTADO:DISC  "+getCuestionarioService().getCompletadoSeccionDiscapacidadCI(id, hogar_id,App.CUEST_ID_INDIVIDUAL, persona_id));
		  Log.e("","ESTADO:4A  "+ getCuestionarioService().getSeccion4ACompletado(id, hogar_id, persona_id));
		  Log.e("","ESTADO:4B  "+ getCuestionarioService().getSeccion4BCompletado(id, hogar_id, persona_id));
		  if(
			 getCuestionarioService().getCompletadoSeccion0103CI(id, hogar_id, persona_id) 
			  && getCuestionarioService().getCompletadoSeccionDiscapacidadCI(id, hogar_id,App.CUEST_ID_INDIVIDUAL, persona_id)
			  && getCuestionarioService().getSeccion4ACompletado(id, hogar_id, persona_id)
			  && getCuestionarioService().getSeccion4BCompletado(id, hogar_id, persona_id)
			  && getCuestionarioService().getCompletadoSeccion0507CI(id, hogar_id, persona_id)
			  && getCuestionarioService().getCompletadoSeccion008CI(id, hogar_id, persona_id)
			  && getCuestionarioService().getCompletadoSeccion10CI(id, hogar_id, persona_id)
				  ){
			  visitaci.qivresul=App.INDIVIDUAL_RESULTADO_COMPLETADO;
		  }
		  else{
			  visitaci.qivresul=null;
		  }
		  return visitaci;
	  }
	  
	public static  Auditoria GuardarPreguntaDetalle(String pregunta_id, String pregunta_valor, Integer id, Integer hogar_id,Integer persona_id){
		registro=new Auditoria();
		registro.id = id;
		registro.hogar_id = hogar_id;
		registro.persona_id_orden = persona_id;
		registro.pregunta_id = pregunta_id;
		registro.pregunta_valor = pregunta_valor;
		registro.fecha_reg=Util.getFechaActualToString();
		return registro;
	}
	 public static String EncriptarDatosSensibles(String cadena){
		   char[] todo= cadena.toCharArray();
		   int x=0;
		   String cadena_encriptada="";
		   char caracter;
		   Random rnd=new Random();
		   String letrabinario="";
		   String Letraencriptada="";
		   char[] textobinario=null;
		   int primerasuma=0;
		   int segundasuma=0;
		   while(x<cadena.length()){
			   caracter=todo[x];
			   int llave=rnd.nextInt(20);
			   letrabinario=Decimal_a_Base(((int)caracter+llave),2)+Decimal_a_Base(llave, 2);
			   Letraencriptada=(char)((int)caracter+llave) + (char)llave+"";
			   letrabinario=letrabinario.replace("1", "2");
			   letrabinario=letrabinario.replace("0", "1");
			   letrabinario=letrabinario.replace("2", "0");
//			   Log.e("BINARIO: ",""+letrabinario);
			   textobinario=letrabinario.toCharArray();
			   primerasuma=Integer.parseInt(textobinario[0]+"")*128+Integer.parseInt(textobinario[1]+"")*64+Integer.parseInt(textobinario[2]+"")*32+Integer.parseInt(textobinario[3]+"")*16+Integer.parseInt(textobinario[4]+"")*8+Integer.parseInt(textobinario[5]+"")*4+Integer.parseInt(textobinario[6]+"")*2+Integer.parseInt(textobinario[7]+"")*1;
			   segundasuma=Integer.parseInt(textobinario[8]+"")*128+Integer.parseInt(textobinario[9]+"")*64+Integer.parseInt(textobinario[10]+"")*32+Integer.parseInt(textobinario[11]+"")*16+Integer.parseInt(textobinario[12]+"")*8+Integer.parseInt(textobinario[13]+"")*4+Integer.parseInt(textobinario[14]+"")*2+Integer.parseInt(textobinario[15]+"")*1;
			   Letraencriptada=((char)primerasuma)+"";
			   Letraencriptada=Letraencriptada+(char)segundasuma+"";
//			   Log.e("PRIMERA SUMA: ",""+primerasuma);
//			   Log.e("SEGUNDA SUMA: ",""+segundasuma);
			   cadena_encriptada=cadena_encriptada + Letraencriptada;
			    x=x+1;
		   }
		   Log.e("CADENA",""+cadena_encriptada);
//		   Log.e("DESCE", ""+DesencriptarData(cadena_encriptada));
		   return cadena_encriptada;
	   }
	   public static String Decimal_a_Base(int n,Integer base){
		   String numero_de_caracteres="0123456789ABCDEF";
		   char[] data=numero_de_caracteres.toCharArray();
		   String Resultado="";
		   Integer a=null;
		   if(base==2 || base==8 || base==10 || base==16){
			   while(n>0){
				   a=(n%base);
				   Resultado=data[a]+Resultado;
				   n=(int) Math.floor(n/base);
			   }	
		   }
		   return Util.completarCadena(Resultado, "0", 8,COMPLETAR.IZQUIERDA);
	   }
	   public static String DesencriptarData(String cadena){
		   String letrabinario;
		   String letradesencriptada = "";
		   String llavebinario;
		   if(cadena==null)
			   letradesencriptada= null;
		   else{
			   char[] todo=cadena.toCharArray();
			   int x=0;
			   char caracter;
			   char llave;
			   char[] textobinario=null;
			   char[] llavecaracter=null;
			   int primerasuma=0;
			   int segundasuma=0;
			   char letra;
			   while(x<cadena.length()){
				   caracter=todo[x];
				   llave=todo[x+1];
				   letrabinario=Decimal_a_Base((int)caracter, 2);
				   llavebinario=Decimal_a_Base((int)llave,2);
				   letrabinario=letrabinario.replace("1", "2");
				   letrabinario=letrabinario.replace("0", "1");
				   letrabinario=letrabinario.replace("2", "0");
				   llavebinario=llavebinario.replace("1", "2");
				   llavebinario=llavebinario.replace("0", "1");
				   llavebinario=llavebinario.replace("2", "0");
				   textobinario=letrabinario.toCharArray();
				   llavecaracter=llavebinario.toCharArray();
				   primerasuma=Integer.parseInt(textobinario[0]+"")*128+
						   	   Integer.parseInt(textobinario[1]+"")*64+
						   	   Integer.parseInt(textobinario[2]+"")*32+
						   	   Integer.parseInt(textobinario[3]+"")*16+
						   	   Integer.parseInt(textobinario[4]+"")*8+
						   	   Integer.parseInt(textobinario[5]+"")*4+
				   			   Integer.parseInt(textobinario[6]+"")*2+
				   			   Integer.parseInt(textobinario[7]+"")*1;
				   segundasuma=Integer.parseInt(llavecaracter[0]+"")*128+
				   			   Integer.parseInt(llavecaracter[1]+"")*64+
				   			   Integer.parseInt(llavecaracter[2]+"")*32+
				   			   Integer.parseInt(llavecaracter[3]+"")*16+
				   			   Integer.parseInt(llavecaracter[4]+"")*8+
				   			   Integer.parseInt(llavecaracter[5]+"")*4+
				   			   Integer.parseInt(llavecaracter[6]+"")*2+
				   			   Integer.parseInt(llavecaracter[7]+"")*1;
				   letra=(char)(primerasuma-segundasuma);
	//			   letradesencriptada=letradesencriptada.trim() +letra;
				   letradesencriptada=letradesencriptada +letra;
				   x=x+2;
			   }
		   }
		   return letradesencriptada;
	   }
//		public static String SepararSegundoNombre(String nombre){
//			String segundonombre="";
//			
//			segundonombre=nombre.replace(" ", ",");
//			String[] todo=segundonombre.split(",");
//			segundonombre="";
//			for(int i=0;i<todo.length;i++){
//				if(i>=1 && todo[i].trim().length()!=0){
//					segundonombre=segundonombre+espacio+todo[i];
//				}
//			}
//			return segundonombre==""?null:segundonombre;	
//		}
//		public static String SepararPrimerNombre(String nombre){
//			String primernombre="";
//			primernombre=nombre.replace(" ", ",");
//			String[] todo=primernombre.split(",");
//			primernombre="";
//			for(int i=0;i<todo.length;i++){
//				if(i==0 && todo[i].trim().length()!=0){
//					primernombre=primernombre+todo[i];
//					break;
//				}
//			}
//			return primernombre==""?null:primernombre;	
//		}
}
