package gob.inei.endes2024.common;

import gob.inei.dnce.R;
import gob.inei.dnce.components.DateTimeField;
import gob.inei.dnce.components.DialogFragmentComponent;
import gob.inei.dnce.components.Entity;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.GridComponent2;
import gob.inei.dnce.components.ListViewComponent;
import gob.inei.dnce.dao.SQLiteDAO;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Parameter;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;
import gob.inei.endes2024.service.ParameterService;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;

public final class App {
	
	private int textQuestionSize = 19;
	private int leftMargin = 30;
	private int rightMargin = 30;
	private int topMargin = 10;
	private int bottomMargin = 10;
	private int cellspacing = 2;
	private int leftPadding = 7;
	private int rightPadding = 7;
	private int topPadding = 7;
	private int bottomPadding = 7;
	private int heightSize = 54;
	private String version = null;	
	private static Usuario usuario = null;
	private Map<Integer, Parameter> parameters = null;
	static Calendar  fecha=new GregorianCalendar();
	public static final int ANIOPORDEFECTO_MIGRACION= 2019;  //fecha.get(Calendar.YEAR)-5;
	public static final int ANIOPORDEFECTO= 2019;  //fecha.get(Calendar.YEAR)-5;
	public static final int ANIOPORDEFECTOSUPERIOR=2024; //fecha.get(Calendar.YEAR);
	public static final int ANIOINICIOQALIWARMA= 2013;  //fecha.get(Calendar.YEAR)-5;
	public static final String RUTA_BASE = Environment.getExternalStorageDirectory().getPath() + "/endes"+App.ANIOPORDEFECTOSUPERIOR+"/";
	public static final String RUTA_ENVIO_SERVIDOR = Environment.getExternalStorageDirectory().getPath() + "/ARCHIVOS EXPORTADOS/";
	private Segmentacion segmentacion = null;
	private Marco marco = null;
	private Hogar hogar = null;
	private Incentivos incentivos =null;
//	private Hogar salud = null;
	private Seccion01 persona2=null;
	public static Seccion01 miembrosdelhogar= null;
	private Seccion01 personaseccion01=null;
	private Salud persona_salud=null;
	private CSSECCION_08 personasalud=null;
	private Seccion04_05 persona = null;
	private List<Seccion04_05> detalles04_05=null;
	private CAP04_07 persona_04_07=null; 
	private Visita visita=null;
	private Visita_Viv visita_viv=null;
	private CARATULA_INDIVIDUAL visitaI=null;
	private CSVISITA c1visita,nro_visita=null;
	private Salud salud=null;
	private CAP04_07 cap04_07 =null;
	private CISECCION_01_03 c_individual=null;
	private CISECCION_04B2 c_seccion04b2;
	private Seccion01 personaseccion02=null;
	private Integer qhviolen=null;
	private Integer numMef=null;

	private CISECCION_02 c_nacimiento;
	private CISECCION_02 c_nacimientovivos;
	private CISECCION_04A c_embarazo;
	private CISECCION_04B c_vacuna;
	private CISECCION_04DIT c_niniodit;
	private CISECCION_04DIT_02 c_niniodit_02;
	private CISECCION_05_07 ciseccion05_07=null;
	private CISECCION_08 ciseccion_08=null;
	private CISECCION_10_01 ciseccion_10_01=null;
	
		
	public static final int ESTILO = R.style.btnStyleHeaderAzul ;
	public static final int ESTILO2 = R.style.btnStyleHeaderAzul2 ;
	public static final int ESTILO_BOTON = R.style.btnStyleButtonAzul;
	public static final int INFORMANTE =1;
	public static final int MUJER=2;
	public static final int HOMBRE=1;
	public static final int JEFE=1;	
	public static final int ESPOSO_ESPOSA=2;
	public static final int ESPOSO=2;
	public static final int ESPOSA=2;
	public static final int HIJO_A=3;
	public static final int NIETO_NIETA=5;
	public static final int PADRE_MADRE=6;
	public static final int PADRE=6;
	public static final int MADRE=6;
	public static final int SUEGRO_SUEGRO=7;
	public static final int SUEGRO=7;
	public static final int SUEGRA=7;
	public static final int EMPLEADA=12;
	public static final int CONIVIENTE=1;
	public static final int CASADO=2;
	/**/
	public static final int HOGAR=1;
	public static final int SALUD=2;
	public static final int INDIVIDUAL=3;
	public static final int NODEFINIDO=-1;

	/**/
	/*SECCION 03*/
	public static final int BECA18 =92;
	public static final int EDADMINIMABECA18=16;
	public static final int EDADMAXIMABECA18=25;
	public static final int TRABAJAPERU=94;
	public static final int EDADMINIMATPERU=18;
	public static final int EDADMAXIMATPERU=97;
	public static final int JUNTOS =96;
	public static final int EDADMINIMAJUNTOS=15;
	public static final int EDADMAXIMAJUNTOS=97;
	public static final int PENSION65=100;
	public static final int CUNAMAS_100B=200;
	public static final int EDADMINIMAPENSION65=65;
	public static final int EDADMAXIMAPENSION65=97;
	public static final int PERSONAS_COVID =237;
	/*agregado para cunamas*/
	public static final int EDADMINIMACUNAMASNINIO100B=0;
	public static final int EDADMAXIMACUNAMASNINIO100B=3;
	
	public static final int EDADMINIMACUNAMASMEF100B=12;
	public static final int EDADMAXIMACUNAMASMEF100B=49;
	
	
	public static final int VASOLECHE =102;
	public static final int EDADMINIMAVASOLECHE=0;
	public static final int EDADMAXIMAVASOLECHE=97;
	public static final int COMEDORPOP =104;
	public static final int EDADMINIMACOMEDORPOP=0;
	public static final int EDADMAXIMACOMEDORPOP=97;	
	public static final int WAWAMAS =107;
	public static final int EDADMINIMAWAWAMAS=0;
	public static final int EDADMAXIMAWAWAMAS=3;
	public static final int QALIWARMA =109;
	public static final int EDADMINIMAQALIWARMA=0;
	public static final int EDADMAXIMAQALIWARMA=24;
	public static final String GPS_OMISION = "9999999999";
	public static final int HOGAR_RESULTADO_COMPLETADO=1;
	public static final int SALUD_RESULTADO_COMPLETADO=1;
	public static final int INDIVIDUAL_RESULTADO_COMPLETADO=1;
	public static final int VIVIENDAASIGNADA=1;
	public static final int VIVIENDADESAGNADA=0;
	public static final int VIVIENDARECUPERADA=4;
	public static final int CODIGO_SUPERVISORA=22;
	public static final int CODIGO_ENTREVISTADORA=21;
	public static final int VIVIENDAASIGNADASUPERVISORA=3;
	public static final int DESDECAL04=1;
	public static final int HASTACAL04=2;
	/******CALENDARIO*/
	public static final String CALENDARIONACIMIENTO="N";
	public static final String CALENDARIOEMBARAZO="E";
	public static final String CALENDARIOTERMINACION="T";
	public static final String CALENDARIONINGUNMETODO="0";
	
	public static final String FECHA_MINIMA_P220 = "01/01/"+App.ANIOPORDEFECTO+" 00:00:00";
	public static final int MESPORDEFECTO=1;
	public static final int TERMINACION_IDDEFAULT=0;
	public static final int EMBARAZO_IDDEFAULT=0;
	public static final int METODO_IDDEFAULT=0;
	public static final int NINGUNMETODO_IDDEFAULT=0;
	public static final int CODIGOCOLUMNA04=1;
	
	public static final int ACTUALMENTE_EN_UNION=1;
	public static final int ACTUALMENTE_NO_EN_UNION=2;
	
	public static final String URL_PUBLICADO = "http://webinei.inei.gob.pe/PyENDES/service/TransferenciaEncuesta/uploadFileForAndroidEncuesta";
	public static final String URL_LOCAL = "http://172.17.10.48/PyENDES/service/TransferenciaEncuesta/uploadFileForAndroidEncuesta";
	
	public static final int SERVER_RESPONSE_CODE = 200;
	
	/*SECCION PARA DIFERENTES TABLETS*/
	public static final int YOGA8_2=320;
	public static final int YOGA8=213;
	
	//60 debido a que los ni�os que cumplen a�os en diciembre de hace 5 a�os cumplen 60 meses ejemplo (12/12/2013 calcular edad con 04/01/2019) .
	public static final int SOLODITMIN=60;
	public static final int SOLODITMAX=71;
	
	
	/* variables para geolocalizacion*/
	public static int USER_LOCATION_STATUS 	= -1;
	public static final int APP_OFF 		= 0;
	public static final int APP_ON 			= 1;
	public static final int APP_SAVE 		= 2;
//	public static final int APP_UPDATE 		= 3;
//	public static final int APP_DELETE 		= 4;
	
	public static String GEO_CONGLOMERADO="";
	public static String GEO_CODCCPP="";
	public static String GEO_MANZANA_ID="";
	public static String GEO_VIVIENDA="";
	
	public static String MENSAJE_GPS="Falta capturar punto GPS en la �ltima visita de la vivienda";
	
	public static Integer HogEdadMefMin=12;
	public static Integer HogEdadMefMax=49;
	
	public static Integer IndEdadMefMin=12;
	public static Integer IndEdadMefMax=49;
	
	//Claves de los cuestionarios
	public static Integer CUEST_ID_HOGAR=1;
	public static Integer CUEST_ID_SALUD=2;
	public static Integer CUEST_ID_INDIVIDUAL=3;
	public static Integer CUEST_ID_CARA_HOGAR=4;
	public static Integer CUEST_ID_CARA_SALUD=5;
	public static Integer CUEST_ID_CARA_INDIVIDUAL=6;
	
	//Variables para blanquear fragmento postcensal condicion inicial false = blanqueo 
	public static boolean FRAGMENTO_POSTCENSAL=false;
//	public static boolean FRAGMENTO_QHSECCION04=false;
	
	//variable por defecto para Seccion 03 A
	public static Integer PREGUNTA_BECA18_MAXIMO_ANIO=9;
	public static Integer PREGUNTA_TRABAJAPERU_MAXIMO_ANIO=8;
	public static Integer PREGUNTA_JUNTOS_MAXIMO_ANIO=13;
	public static Integer PREGUNTA_PENSION65_MAXIMO_ANIO=8;
	
	public static double anteriorLatitud=0.0;
	public static double anteriorLongitud=0.0;
	
	private App() {
		Entity.PatronVariable pv = new Entity.PatronVariable("P$[A-Z0-9_]*#;", -1, -1, 1);
		Entity.PATRON_VARIABLE = pv;
		DateTimeField.THEME = DateTimeField.DEFAULT;
		ListViewComponent.HAS_ANIMATION = true;
		FragmentForm.COLOR_LINEA_SECCION_PREGUNTA = R.color.azulacero;
		DialogFragmentComponent.COLOR_LINEA_SECCION_PREGUNTA = R.color.azulacero;
		GridComponent2.COLOR_FONDO = R.color.azulacero;
	}
	
	public static App getInstance() {
		return AppHolder.INSTANCE;
	}
	
	private static class AppHolder {
		private static App INSTANCE = new App();		
	}

	public int getTextQuestionSize() {
		return textQuestionSize;
	}

	public int getLeftMargin() {
		return leftMargin;
	}

	public int getRightMargin() {
		return rightMargin;
	}

	public int getTopMargin() {
		return topMargin;
	}

	public int getBottomMargin() {
		return bottomMargin;
	}

	public int getCellspacing() {
		return cellspacing;
	}

	public int getLeftPadding() {
		return leftPadding;
	}

	public int getRightPadding() {
		return rightPadding;
	}

	public int getTopPadding() {
		return topPadding;
	}

	public int getBottomPadding() {
		return bottomPadding;
	}

	public String getVersion(Context context) throws NameNotFoundException {
		if (version == null || "".equals(version)) {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = pInfo.versionName;
		}
		return version;
	}
	
	public static Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		SQLiteDAO.USER_ID = usuario == null ? null : usuario.id;
		this.usuario = usuario;
	}

	public int getDefaultHeightSize() {
		return heightSize;
	}

	public Map<Integer, Parameter> getParameters(Context context) {
		if (parameters == null) {
			parameters = ParameterService.getInstance(context).getParameters();
		}
		return parameters;
	}

	public void setParameters(Map<Integer, Parameter> parameters) {
		this.parameters = parameters;
	}
	
	public Segmentacion getSegmentacion() {
		return segmentacion;
	}

	public void setSegmentacion(Segmentacion segmentacion) {
		this.segmentacion = segmentacion;
	}
	
	public Marco getMarco() {
		return marco;
	}

	public void setMarco(Marco marco) {
		this.marco = marco;
	}
		
	
	public Hogar getHogar() {
		return hogar;
	}
//	public Salud setSaludS1() {
//		return salud;
//	}

	public void setHogar(Hogar hogar) {
		this.hogar = hogar;
	}
//	public void setSaludS1(Salud salud) {
//		this.salud = salud;
//	}
	

	
	public void SetPersonaSeccion01(Seccion01 persona)
	{
		this.miembrosdelhogar= persona;
	}
	public Seccion01 getPersonaSeccion01()
	{
		return this.miembrosdelhogar;
	}
	public void setEditarPersonaSeccion01(Seccion01 personaseccion01){
		this.personaseccion01=personaseccion01;
	}
	public Seccion01 getEditarPersonaSeccion01(){
		return this.personaseccion01;
	}
	
	public void setIncentivos(Incentivos incentivos) {
		this.incentivos = incentivos;
	}	
	public Incentivos getIncentivos() {
		return this.incentivos;
	}
	
	public Salud getPersonabySalud()
	{
		return this.persona_salud;
	}
	public void setPersonabySalud(Salud persona)
	{
		this.persona_salud=persona;
	}
	public CSSECCION_08 getPersonabySeccion8Salud()
	{
		return this.personasalud;
	}
	public void setPersonabySeccion8Salud(CSSECCION_08 persona)
	{
		this.personasalud=persona;
	}
	public Visita getVisita() {
		return visita;
	}

	public void setVisita(Visita visita) {
		this.visita = visita;
	}
	
	public void setVisita_Viv(Visita_Viv visita_viv) {
		this.visita_viv = visita_viv;
	}
	
	public void setVisitaI(CARATULA_INDIVIDUAL visitaI) {
		this.visitaI = visitaI;
	}
	
	public CARATULA_INDIVIDUAL getVisitaI() {
		return visitaI;
	}
	
	public CSVISITA getC1Visita() {
		return c1visita;
	}

	public void setC1Visita(CSVISITA c1visita) {
		this.c1visita = c1visita;
	}
	
	public void setC1VisitaI(CARATULA_INDIVIDUAL visitaI) {
		this.visitaI = visitaI;
	}
	
	
	public Seccion04_05 getPersona() {
		return persona;
	}

	public CSVISITA getPersona1() {
		return nro_visita;
	}

	public void setPersona( ) {
		this.c1visita = c1visita;
	}
	
	
	
	public void setPersona(CSVISITA nrovisita) {
		this.nro_visita = nrovisita;
	}
	
	
	public void setPersona(Seccion04_05 persona) {
		this.persona = persona;
	}
	
	
	public void setPersona(Seccion01 persona) {
		this.persona2 = persona;
	}

	public CAP04_07 getPersona_04_07() {
		return this.persona_04_07;
	}

	public void setPersona_04_07(CAP04_07 persona_04_07) {
		this.persona_04_07 = persona_04_07;
	}
	
	public void setSaludNavegacionS1_3(Salud salud){
		this.salud=salud;
	}
	public Salud getSaludNavegacionS1_3(){
		return this.salud;
	}	
	public void setSaludNavegacionS4_7(CAP04_07 cap04_07){
		this.cap04_07=cap04_07;
	}
	public CAP04_07 getSaludNavegacionS4_7(){
		return this.cap04_07;
	}
	public void setPersonaCuestionarioI(Seccion01 personaseccion02){
		this.personaseccion02=personaseccion02;
	}
	public void setPersonaCuestionarioIndividual(CISECCION_01_03 persona){
		this.c_individual=persona;
	}
	public CISECCION_01_03 getPersonaCuestionarioIndividual(){
		return this.c_individual;
	}
	public void setQhviolen(Integer persona_id){
		this.qhviolen=persona_id;
	}
	public Integer getQhviolen(){
		return this.qhviolen;
	}
	public void setNumMef(Integer numMef){
		this.numMef=numMef;
	}
	public Integer getNumMef(){
		return this.numMef;
	}
	
	public void setSeccion04B2(CISECCION_04B2 persona){
		this.c_seccion04b2=persona;
	}
	public CISECCION_04B2 getSeccion04B2(){
		return this.c_seccion04b2;
	}
	
	public Seccion01 getPersonaCuestionarioI(){
		return this.personaseccion02;
	}
	public void setNacimiento(CISECCION_02 nacimiento){
		this.c_nacimiento=nacimiento;
	}
	public CISECCION_02 getNacimiento(){
		return this.c_nacimiento;
	}
	
	public void setNacimientoVivos(CISECCION_02 nacimiento){
		this.c_nacimientovivos=nacimiento;
	}
	public CISECCION_02 getNacimientoVivos(){
		return this.c_nacimientovivos;
	}
	public void setCiseccion05_07(CISECCION_05_07 Individual){
		this.ciseccion05_07=Individual;
	}
	public CISECCION_05_07 getCiseccion05_07(){
		return this.ciseccion05_07;
	}
	public void setCiseccion_08(CISECCION_08 ciseccion08){
		this.ciseccion_08=ciseccion08;
	}
	public CISECCION_08 getCiseccion_08(){
		return this.ciseccion_08;
	}

	public CISECCION_10_01 getCiseccion_10_01(){
		return this.ciseccion_10_01;
	}
	public void setCiseccion_10_01(CISECCION_10_01 ciseccion_10_01){
		this.ciseccion_10_01=ciseccion_10_01;
	}


	public CISECCION_04A getSeccion04A() {
		return c_embarazo;
	}

	public void setSeccion4A(CISECCION_04A c_embarazo) {
		this.c_embarazo = c_embarazo;
	}

	public CISECCION_04B getSeccion04B() {
		return c_vacuna;
	}

	public void setSeccion04B(CISECCION_04B c_vacuna) {
		this.c_vacuna = c_vacuna;
	}
	
	public CISECCION_04DIT getSeccion04DIT() {
		return c_niniodit;
	}

	public void setSeccion04DIT(CISECCION_04DIT c_niniodit) {
		this.c_niniodit = c_niniodit;
	}
	
	public CISECCION_04DIT_02 getSeccion04DIT_02() {
		return c_niniodit_02;
	}

	public void setSeccion04DIT_02(CISECCION_04DIT_02 c_niniodit_02) {
		this.c_niniodit_02 = c_niniodit_02;
	}
	
	public static final String[] DEPARTAMENTOS = new String[] {"01 AMAZONAS","02 ANCASH","03 APURIMAC","04 AREQUIPA","05 AYACUCHO","06 CAJAMARCA","07 CALLAO","08 CUSCO","09 HUANCAVELICA","10 HUANUCO","11 ICA","12 JUNIN","13 LA LIBERTAD","14 LAMBAYEQUE","15 LIMA","16 LORETO","17 MADRE DE DIOS","18 MOQUEGUA","19 PASCO","20 PIURA","21 PUNO","22 SAN MARTIN","23 TACNA","24 TUMBES","25 UCAYALI"};
	
	public static final String[] PAISES = new String[]{"AFGANIST�N","ALBANIA","ALEMANIA","ANDORRA","ANGOLA","ANTIGUA Y BARBUDA","ARABIA SAUDITA","ARGELIA","ARGENTINA","ARMENIA","AUSTRALIA","AUSTRIA","AZERBAIY�N","BAHAMAS","BANGLAD�S","BARBADOS","BAR�IN","B�LGICA","BELICE","BEN�N","BIELORRUSIA","BIRMANIA","BOLIVIA","BOSNIA Y HERZEGOVINA","BOTSUANA","BRASIL","BRUN�I","BULGARIA","BURKINA FASO","BURUNDI","BUT�N","CABO VERDE","CAMBOYA","CAMER�N","CANAD�","CATAR","CHAD","CHILE","CHINA","CHIPRE","CIUDAD DEL VATICANO","COLOMBIA","COMORAS","COREA DEL NORTE","COREA DEL SUR","COSTA DE MARFIL","COSTA RICA","CROACIA","CUBA","DINAMARCA","DOMINICA","ECUADOR","EGIPTO","EL SALVADOR","EMIRATOS �RABES UNIDOS","ERITREA","ESLOVAQUIA","ESLOVENIA","ESPA�A","ESTADOS UNIDOS","ESTONIA","ETIOP�A","FILIPINAS","FINLANDIA","FIYI","FRANCIA","GAB�N","GAMBIA","GEORGIA","GHANA","GRANADA","GRECIA","GUATEMALA","GUYANA","GUINEA","GUINEA ECUATORIAL","GUINEA-BIS�U","HAIT�","HONDURAS","HUNGR�A","INDIA","INDONESIA","IRAK","IR�N","IRLANDA","ISLANDIA","ISLAS MARSHALL","ISLAS SALOM�N","ISRAEL","ITALIA","JAMAICA","JAP�N","JORDANIA","KAZAJIST�N","KENIA","KIRGUIST�N","KIRIBATI","KUWAIT","LAOS","LESOTO","LETONIA","L�BANO","LIBERIA","LIBIA","LIECHTENSTEIN","LITUANIA","LUXEMBURGO","MACEDONIA DEL NORTE","MADAGASCAR","MALASIA","MALAUI","MALDIVAS","MAL�","MALTA","MARRUECOS","MAURICIO","MAURITANIA","M�XICO","MICRONESIA","MOLDAVIA","M�NACO","MONGOLIA","MONTENEGRO","MOZAMBIQUE","NAMIBIA","NAURU","NEPAL","NICARAGUA","N�GER","NIGERIA","NORUEGA","NUEVA ZELANDA","OM�N","PA�SES BAJOS","PAKIST�N","PALAOS","PANAM�","PAP�A NUEVA GUINEA","PARAGUAY","POLONIA","PORTUGAL","REINO UNIDO","REP�BLICA CENTROAFRICANA","REP�BLICA CHECA","REP�BLICA DEL CONGO","REP�BLICA DEMOCR�TICA DEL CONGO","REP�BLICA DOMINICANA","RUANDA","RUMAN�A","RUSIA","SAMOA","SAN CRIST�BAL Y NIEVES","SAN MARINO","SAN VICENTE Y LAS GRANADINAS","SANTA LUC�A","SANTO TOM� Y PR�NCIPE","SENEGAL","SERBIA","SEYCHELLES","SIERRA LEONA","SINGAPUR","SIRIA","SOMALIA","SRI LANKA","SUAZILANDIA","SUD�FRICA","SUD�N","SUD�N DEL SUR","SUECIA","SUIZA","SURINAM","TAILANDIA","TANZANIA","TAYIKIST�N","TIMOR ORIENTAL","TOGO","TONGA","TRINIDAD Y TOBAGO","T�NEZ","TURKMENIST�N","TURQU�A","TUVALU","UCRANIA","UGANDA","URUGUAY","UZBEKIST�N","VANUATU","VENEZUELA","VIETNAM","YEMEN","YIBUTI","ZAMBIA","ZIMBABUE"};


	
	
}