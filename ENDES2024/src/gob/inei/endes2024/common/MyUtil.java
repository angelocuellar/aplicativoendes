package gob.inei.endes2024.common;

import gob.inei.dnce.adapter.EntitySpinnerAdapter;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.ToastMessage;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Ubigeo;
import gob.inei.endes2024.model.Usuario;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.service.SegmentacionService;
import gob.inei.endes2024.service.UbigeoService;
import gob.inei.endes2024.service.UsuarioService;
import gob.inei.endes2024.service.VisitaService;
import gob.inei.endes2024.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v4.util.ArrayMap;
import android.util.Log;

public class MyUtil {
	private static Marco vaciomarco = null;
	private static Ubigeo vacio = null;
	private static Seccion01 vacioseccion01 = null;
	private static Seccion01 vacioseccion02 = null;
	private static Usuario vacioseccionusuario = null;
	
	private static SeccionCapitulo[] seccionesCargadoS1_3;

	public static enum INFORMANTE {
		C1CAP200, C1CAP300, C1CAP400;
	}

	private MyUtil() {
	}

	private static Ubigeo getVacio() {
		if (vacio == null) {
			vacio = new Ubigeo();
			vacio.ubigeo = null;
			vacio.ccdd = null;
			vacio.ccpp = null;
			vacio.ccdi = null;
			vacio.departamento = "-- SELECCIONE --";
			vacio.provincia = "-- SELECCIONE --";
			vacio.distrito = "-- SELECCIONE --";
		}
		return vacio;
	}
	public static Seccion01 getvacio() {
		if(vacioseccion01==null)
			vacioseccion01=new Seccion01();
			vacioseccion01.persona_id=null;
			vacioseccion01.qh02_1="-- SELECCIONE --";
			vacioseccion01.qh02_2="";
			return vacioseccion01;
	}
	public static Usuario getvacioUsuario() {
		if(vacioseccionusuario==null)
			vacioseccionusuario=new Usuario();
			vacioseccionusuario.usuario=null;
			vacioseccionusuario.nombres="-- SELECCIONE --";
			vacioseccionusuario.apellidos="";
			return vacioseccionusuario;
	}
	public static Seccion01 getnoReside() {
		if(vacioseccion02==null)
			vacioseccion02=new Seccion01();
			vacioseccion02.persona_id=0;
			vacioseccion02.qh02_1="NO";
			vacioseccion02.qh02_2="RESIDE";
			return vacioseccion02;
	}
	public static Seccion01 getnoReside2() {
		if(vacioseccion02==null)
			vacioseccion02=new Seccion01();
			vacioseccion02.persona_id=0;
			vacioseccion02.qh02_1="NO";
			vacioseccion02.qh02_2="FUE LISTADO";
			return vacioseccion02;
	}
	public static void llenarAnio(Activity activity,
			SegmentacionService service, SpinnerField spinner) {
		List<SegmentacionFiltro> lista = service.getAnios();
		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
				activity, android.R.layout.simple_spinner_item, lista);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (int i = 0; i < lista.size(); i++) {
			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
			keysAdapter.add(d.id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	
	public static void llenarPeriodo(Activity activity,
			SegmentacionService service, SpinnerField spinner) {
		List<SegmentacionFiltro> lista = service.getPeriodos();
		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
				activity, android.R.layout.simple_spinner_item, lista);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (int i = 0; i < lista.size(); i++) {
			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
			keysAdapter.add(d.id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}

	public static void llenarMes(Activity activity,
			SegmentacionService service, SpinnerField spinner,
			SegmentacionFiltro anio) {
		List<SegmentacionFiltro> lista = service.getMes(anio.id);
		
		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
				activity, android.R.layout.simple_spinner_item, lista);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (int i = 0; i < lista.size(); i++) {
			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
			keysAdapter.add(d.id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}

//	public static void llenarPeriodo(Activity activity,
//			SegmentacionService service, SpinnerField spinner,
//			SegmentacionFiltro anio, SegmentacionFiltro mes) {
//		List<SegmentacionFiltro> lista = service.getPeriodos(anio.id,
//				mes.nombre);
//		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
//				activity, android.R.layout.simple_spinner_item, lista);
//		List<Object> keysAdapter = new ArrayList<Object>();
//		for (int i = 0; i < lista.size(); i++) {
//			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
//			keysAdapter.add(d.id);
//		}
//		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
//	}

//	public static void llenarConglomerado(Activity activity,
//			SegmentacionService service, SpinnerField spinner,
//			SegmentacionFiltro anio, SegmentacionFiltro mes,
//			SegmentacionFiltro periodo) {
//		List<SegmentacionFiltro> lista = service.getConglomerados(anio.id,
//				mes.nombre, periodo.id);
//		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
//				activity, android.R.layout.simple_spinner_item, lista);
//		List<Object> keysAdapter = new ArrayList<Object>();
//		for (int i = 0; i < lista.size(); i++) {
//			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
//			keysAdapter.add(d.id);
//		}
//		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
//	}

	public static void llenarConglomerado(Activity activity,
			SegmentacionService service, SpinnerField spinner,
			SegmentacionFiltro periodo) {
		List<SegmentacionFiltro> lista = service.getConglomerados(periodo.id);
		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
				activity, android.R.layout.simple_spinner_item, lista);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (int i = 0; i < lista.size(); i++) {
			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
			keysAdapter.add(d.id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}

	public static void llenarDepartamento(Activity activity,
			UbigeoService service, SpinnerField spinner) {
		List<Ubigeo> departamentos = new ArrayList<Ubigeo>();
		departamentos.add(getVacio());
		departamentos.addAll(service.getDepartamentos());
		EntitySpinnerAdapter<Ubigeo> spinnerAdapter = new EntitySpinnerAdapter<Ubigeo>(
				activity, android.R.layout.simple_spinner_item, departamentos);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Ubigeo u : departamentos) {
			keysAdapter.add(u.ccdd);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	public static void llenarViviendas(Activity activity, CuestionarioService service, SpinnerField spinner, Integer id,String conglomerado) { 
        List<Marco> responsable = new ArrayList<Marco>();
        responsable.add(getVacioMarco());
        responsable.addAll(service.getViviendas(id, conglomerado));
        EntitySpinnerAdapter<Marco> spinnerAdapter = new EntitySpinnerAdapter<Marco>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Marco u : responsable) {
                keysAdapter.add(u.id);
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }
	public static void llenarProvincia(Activity activity,
			UbigeoService service, SpinnerField spinner, String ccdd) {
		if (ccdd == null) {
			return;
		}
		List<Ubigeo> provincias = new ArrayList<Ubigeo>();
		provincias.add(getVacio());
		provincias.addAll(service.getProvincias(ccdd));
		EntitySpinnerAdapter<Ubigeo> spinnerAdapter = new EntitySpinnerAdapter<Ubigeo>(
				activity, android.R.layout.simple_spinner_item, provincias);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Ubigeo u : provincias) {
			keysAdapter.add(u.ccpp);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}

	public static void llenarDistrito(Activity activity, UbigeoService service,
			SpinnerField spinner, String ccdd, String ccpp) {
		if (ccdd == null || ccpp == null) {
			return;
		}
		List<Ubigeo> distritos = new ArrayList<Ubigeo>();
		distritos.add(getVacio());
		distritos.addAll(service.getDistritos(ccdd, ccpp));
		EntitySpinnerAdapter<Ubigeo> spinnerAdapter = new EntitySpinnerAdapter<Ubigeo>(
				activity, android.R.layout.simple_spinner_item, distritos);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Ubigeo u : distritos) {
			keysAdapter.add(u.ccdi);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	
	//Funcion RANGO
	
	public static boolean incluyeRango(Integer rangoIni, Integer rangoFin,String pregunta) {
		pregunta=pregunta==null?"":pregunta;
		boolean bandera = false;
		for (int i = rangoIni; i <= rangoFin; i++) {
			if (pregunta.equals(i + "")) {
				bandera = true;
			}
		}
		return bandera;
	}
	public static boolean incluyeRango(Double rangoIni,Double rangoFin,String pregunta) {
		
		boolean bandera = false;
		
		Double num=Double.parseDouble(pregunta);
		if(num>=rangoIni && num<=rangoFin){
			bandera=true;
		}
		return bandera;
	}
	
	public static boolean incluyeRango(Integer rangoIni, Integer rangoFin,	Integer pregunta) {
			boolean bandera = false;
			boolean valornose=false;
			valornose = pregunta==null? false: true;
			if (valornose) {
				for (int i = rangoIni; i <= rangoFin; i++) {
					if (pregunta.equals(i)) {
						bandera = true;
					}
				}
			}				
			return bandera;				
	}

	private static Marco getVacioMarco() {
		if (vaciomarco == null) {
			vaciomarco = new Marco();
			vaciomarco.id = null;
			vaciomarco.nselv = "--Seleccione--";
		}
		return vaciomarco;
	}
	public static void llenarPersonaResponsable(Activity activity, Seccion01Service service, SpinnerField spinner, Integer id,Integer hogar_id,Integer persona_id) { 
        List<Seccion01> responsable = new ArrayList<Seccion01>();
        responsable.add(getvacio());
        responsable.add(getnoReside2());
        responsable.addAll(service.getPersonaResponsable(id, hogar_id,persona_id));
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : responsable) {
                keysAdapter.add(u.persona_id);
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }
	
	public static void llenarAuxiliares(Activity activity, UsuarioService service, SpinnerField spinner, Integer id, String qh215_c, String qh215) { 
        List<Usuario> responsable = new ArrayList<Usuario>();
        responsable.add(getvacioUsuario());
        responsable.addAll(service.getAuxiliares(id));
        if (qh215!=null &&  qh215_c !=null) {
        	Usuario nuevo = new Usuario();
        	nuevo.usuario=qh215_c;
        	nuevo.nombres=qh215;  
        	for (Usuario usuario : responsable) {
				if (usuario.usuario!=qh215_c) {
					responsable.add(nuevo);	
					break;
				}
			}
        	boolean existe= false;
        	for (Usuario usuario : responsable) {
				if (usuario.usuario!=null && usuario.usuario.equals(nuevo.usuario)) {
					existe=true;
					break;
				}
			}
        	if (!existe) {
        		responsable.add(nuevo);	
			}
        	
		}
        
        
        EntitySpinnerAdapter<Usuario> spinnerAdapter = new EntitySpinnerAdapter<Usuario>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Usuario u : responsable) {
        	if (u.usuario==null) {
        		keysAdapter.add(u.usuario);
			}else {
				keysAdapter.add(u.usuario);
			}
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }
	public static void llenarAntropo(Activity activity, UsuarioService service, SpinnerField spinner, Integer id, String qh216_c, String qh216) { 
        List<Usuario> responsable = new ArrayList<Usuario>();
        responsable.add(getvacioUsuario());
        responsable.addAll(service.getAntropo(id));
        
        if (qh216!=null &&  qh216_c !=null) {
        	Usuario nuevo = new Usuario();
        	nuevo.usuario=qh216_c;
        	nuevo.nombres=qh216;  
        	boolean existe= false;
        	for (Usuario usuario : responsable) {
				if (usuario.usuario!=null && usuario.usuario.equals(nuevo.usuario)) {
					existe=true;
					break;
				}
			}
        	if (!existe) {
        		responsable.add(nuevo);	
			}
		}
        
        EntitySpinnerAdapter<Usuario> spinnerAdapter = new EntitySpinnerAdapter<Usuario>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Usuario u : responsable) {
        	if (u.usuario==null) {
        		keysAdapter.add(u.usuario);
			}else {
				keysAdapter.add(u.usuario);
			}
                
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }
	public static void llenarMadres(Activity activity, Seccion01Service service, SpinnerField spinner, Integer id,Integer hogar_id, Integer persona_id, Integer jefe,Integer parentesco, Integer edadPersona) { 
        List<Seccion01> madres = new ArrayList<Seccion01>();
        madres.add(getvacio());       
        if (jefe==1) {
			madres.add(service.getPersona(id, hogar_id, jefe));
		}
        else{
        madres.add(getnoReside());
        madres.addAll(service.getMadres(id, hogar_id,persona_id,parentesco, edadPersona));
        }
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(activity, android.R.layout.simple_spinner_item, madres);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : madres) {
                keysAdapter.add(u.persona_id);
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);		
	}
			    
	public static void llenarPadres(Activity activity, Seccion01Service service, SpinnerField spinner, Integer id,Integer hogar_id, Integer persona_id, Integer jefe,Integer parentesco,Integer edaPersona)
	{
        List<Seccion01> padres = new ArrayList<Seccion01>();
        padres.add(getvacio());       
        if (jefe==1) {
        	padres.add(service.getPersona(id, hogar_id, jefe));
		}
        else{
        	 padres.add(getnoReside());
         padres.addAll(service.getPadres(id, hogar_id,persona_id,parentesco,edaPersona));
        }
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                        activity, android.R.layout.simple_spinner_item, padres);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : padres) {
                keysAdapter.add(u.persona_id);
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	public static void llenarResponsableCuestionarioSalud(Activity activity, Seccion01Service service, SpinnerField spinner, Integer id,Integer hogar_id)
	{
		List<Seccion01> Responsable = new ArrayList<Seccion01>();
		Responsable.add(getvacio());
		Responsable.add(getnoReside());
		Responsable.addAll(service.getResponsableSalud(id, hogar_id));
		EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                activity, android.R.layout.simple_spinner_item, Responsable);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Seccion01 u : Responsable) {
        keysAdapter.add(u.persona_id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	public static void llenarBeneficiariosBeca18(Activity activity,Seccion01Service service,SpinnerField spinner, Integer id, Integer hogar_id,Integer persona_id_orden,Integer edadMinima,Integer edadMaxima,Integer pregunta_id)
	{
		List<Seccion01> Beca18 = new ArrayList<Seccion01>();
		Beca18.add(getvacio());
		Beca18.addAll(service.getBeca18(id,hogar_id,persona_id_orden,edadMinima,edadMaxima,pregunta_id));
		EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                activity, android.R.layout.simple_spinner_item, Beca18);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Seccion01 u : Beca18) {
		        keysAdapter.add(u.persona_id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	public static void llenarBeneficiarioscunamas(Activity activity,Seccion01Service service,SpinnerField spinner, Integer id, Integer hogar_id,Integer persona_id_orden,Integer pregunta_id)
	{
		List<Seccion01> Beca18 = new ArrayList<Seccion01>();
		Beca18.add(getvacio());
		Beca18.addAll(service.getCunamas(id,hogar_id,persona_id_orden,pregunta_id));
		EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                activity, android.R.layout.simple_spinner_item, Beca18);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Seccion01 u : Beca18) {
		        keysAdapter.add(u.persona_id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}

	public static void llenarMenoresHogar(Activity activity, CuestionarioService service, SpinnerField spinner, Integer id,Integer hogar_id,Integer persona_id,Integer qi219,Integer qh06, SeccionCapitulo... secciones) { 
        List<Seccion01> responsable = new ArrayList<Seccion01>();
        responsable.add(getvacio());
        responsable.add(getnoReside2());
        responsable.addAll(service.getSeccion01ListbyMarcoyHogarMenorEdad(id, hogar_id,persona_id,qi219,qh06, secciones));
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : responsable) {
                keysAdapter.add(u.persona_id); 
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }
	
	
	public static boolean ValidaDia(Integer dia, Integer mes,Integer a�o) {
		boolean bandera = false;
		int dias = 0;
		switch(mes){
		case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: dias=31; break;
        case 4:
        case 6:
        case 9:
        case 11:dias=30; break;
        case 2:
            if ( ((a�o % 4 == 0) && !(a�o % 100 == 0))|| (a�o % 400 == 0) )
                dias = 29;
            else
                dias = 28;
            break;
		}
		if(dia>=1 && dia<=dias){
			bandera=true;
		}
	    
		return bandera;				
    }
	public static boolean ValidaA�o(Integer a�o) {
		boolean bandera = false;
		if(a�o>=App.ANIOPORDEFECTO-2 && a�o<=App.ANIOPORDEFECTOSUPERIOR){
			bandera=true;
		}
		return bandera;	
	}
	public static int getDayOfTheWeek(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);		
	}
	
	public static String DiaDelaSemana(int d){
		String Retornar="";
		switch(d){
			case 1:Retornar="Domingo";break;
			case 2:Retornar="Lunes";break;
			case 3:Retornar="Martes";break;
			case 4:Retornar="Mi�rcoles";break;
			case 5:Retornar="Jueves";break;
			case 6:Retornar="Viernes";break;
			case 7:Retornar="S�bado";break;
		}
		return Retornar;
	}
	
	public static String DiaDelaSemanaMay(int d){
		String Retornar="";
		switch(d){
			case 1:Retornar="DOMINGO";break;
			case 2:Retornar="LUNES";break;
			case 3:Retornar="MARTES";break;
			case 4:Retornar="MI�RCOLES";break;
			case 5:Retornar="JUEVES";break;
			case 6:Retornar="VIERNES";break;
			case 7:Retornar="S�BADO";break;
		}
		return Retornar;
	}
	
	public static Calendar PrimeraFecha(Calendar calendario,int dias)
	{
		calendario.add(Calendar.DATE,-dias);
		return calendario;
	}
	public static String Mes(int mes)
	{
		String mesaretornar="";
		switch (mes) {
		case 0:mesaretornar="Enero";break;
		case 1:mesaretornar="Febrero";break;
		case 2:mesaretornar="Marzo";break;
		case 3:mesaretornar="Abril";break;
		case 4:mesaretornar="Mayo";break;
		case 5:mesaretornar="Junio";break;
		case 6:mesaretornar="Julio";break;
		case 7:mesaretornar="Agosto";break;
		case 8:mesaretornar="Septiembre";break;
		case 9:mesaretornar="Octubre";break;
		case 10:mesaretornar="Noviembre";break;
		case 11:mesaretornar="Diciembre";break;
		}
		return mesaretornar;
	}
	public static String MesNombreCorto(int mes){
		String mesaretornar="";
		switch (mes) {
		case 0:mesaretornar="ENE";break;
		case 1:mesaretornar="FEB";break;
		case 2:mesaretornar="MAR";break;
		case 3:mesaretornar="ABR";break;
		case 4:mesaretornar="MAY";break;
		case 5:mesaretornar="JUN";break;
		case 6:mesaretornar="JUL";break;
		case 7:mesaretornar="AGO";break;
		case 8:mesaretornar="SEP";break;
		case 9:mesaretornar="OCT";break;
		case 10:mesaretornar="NOV";break;
		case 11:mesaretornar="DIC";break;
		}
		return mesaretornar;
	}
	 public static Calendar RestarMeses(Calendar calendario,int mes) 
	 {
	    	calendario.add(Calendar.MONTH ,-mes);
	    	return calendario;
	 }
	 
	 public static Calendar restarDias(Calendar calendario,int dias) 
	 {
	    	calendario.add(Calendar.DAY_OF_MONTH ,-dias);
	    	return calendario;
	 }
	
	public static boolean DiaCorrespondeAlMes(Integer dia,Integer mes){
		Integer diasdelmes=-1;
		switch (mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8: 
		case 10:
		case 12:diasdelmes=31;break;
		case 2: diasdelmes=29;break;
		case 4:
		case 6:
		case 9:
		case 11: diasdelmes=30;break;
		}
		return diasdelmes>=dia;
		}
	public static boolean Comparefechas(Calendar fecha_inicio,Calendar fechafin)
	{
		if(fecha_inicio.get(Calendar.DATE)>fechafin.get(Calendar.DATE)){
		return true;
		}
		else{return false;
		}
	}
public static Integer SeleccionarInformantedeSalud(Integer id,Integer hogar_id, VisitaService visitaservice, Seccion01Service seccion01service){
	List<Seccion01> posiblesinformantessalud;
	List<Seccion01> informantedesaludpordefecto;
	Seccion01 listadomenoresdeedad;
	SeccionCapitulo[]	seccionesCargadoVisita = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"NRO_VISITA","QHVDIA_INI","QHVMES_INI")};
	Visita visitacompletada = visitaservice.getVisitaCompletada(App.getInstance().getMarco().id, App.getInstance().getHogar().hogar_id , seccionesCargadoVisita);
	posiblesinformantessalud = seccion01service.getPosiblesInformantesCuestionarioSalud(id, hogar_id);
	informantedesaludpordefecto=seccion01service.getPosiblesInformantesCuestionarioSaludPordefecto(id, hogar_id);
	listadomenoresdeedad = seccion01service.ListadoInformantedeSaludMenordeEdad(id, hogar_id);
	Integer persona_id=0;
	Calendar fechavisita= new GregorianCalendar(2030,Integer.parseInt(visitacompletada.qhvmes_ini)-1,Integer.parseInt(visitacompletada.qhvdia_ini));
	Calendar fechafinal;
	List<Personas> datos = new ArrayList<Personas>();
	List<Personas> datomesesigulales = new ArrayList<MyUtil.Personas>();
	List<Personas> datosmesesigualesdiasmayores= new ArrayList<MyUtil.Personas>();
	List<Personas> datosmayores = new ArrayList<Personas>();
	List<Personas> datosmenores = new ArrayList<Personas>();
	Integer personaid98=0;
	
	if(!Util.esDiferente(posiblesinformantessalud.size(), 1,1)){
		persona_id=posiblesinformantessalud.get(0).persona_id;
	}
	else{
		Integer mesigualavisita=0;
		for (int i = 0; i < posiblesinformantessalud.size(); i++) {
			Integer dianacimiento=Integer.parseInt(posiblesinformantessalud.get(i).qh7dd);
//			Log.e("","DIA "+dianacimiento);
//			Log.e("","MESSSS "+ Integer.parseInt(posiblesinformantessalud.get(i).qh7mm));
			if(!Util.esDiferente(dianacimiento, 98)){
				fechafinal = new GregorianCalendar(2030,Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1,29);
			}
			else{
				fechafinal = new GregorianCalendar(2030,Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1,dianacimiento);
			}
			Personas detalle= new Personas();
			detalle.setPersona_id(posiblesinformantessalud.get(i).persona_id);
			detalle.setMes(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1);
			detalle.setDias(CantidaddeDias(fechavisita, fechafinal));
			detalle.setDiaNacimiento(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd));
			datos.add(detalle);
//			Log.e(""," FECHA VISITA "+fechavisita.get(Calendar.MONTH));
//			Log.e(""," MESS NACIMIENTO "+fechafinal.get(Calendar.MONTH)+" ID "+posiblesinformantessalud.get(i).persona_id);
			if(!Util.esDiferente(fechavisita.get(Calendar.MONTH),fechafinal.get(Calendar.MONTH),fechafinal.get(Calendar.MONTH))){
				mesigualavisita++;
				if(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd)<=fechavisita.get(Calendar.DAY_OF_MONTH)){
					Personas nueva = new Personas();
					nueva.setPersona_id(posiblesinformantessalud.get(i).persona_id);
					nueva.setMes(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1);
					nueva.setDias(CantidaddeDias(fechavisita, fechafinal));
					nueva.setDiaNacimiento(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd));
					datomesesigulales.add(nueva);
				}
				else{
					Personas datitos= new Personas();
					datitos.setPersona_id(posiblesinformantessalud.get(i).persona_id);
					datitos.setMes(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1);
					datitos.setDias(CantidaddeDias(fechavisita, fechafinal));
					datitos.setDiaNacimiento(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd));
					datosmesesigualesdiasmayores.add(datitos);
				}
			}
			if(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)>=fechavisita.get(Calendar.MONTH)){
				Personas nueva = new Personas();
				nueva.setPersona_id(posiblesinformantessalud.get(i).persona_id);
				nueva.setMes(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1);
				nueva.setDias(CantidaddeDias(fechavisita, fechafinal));
				nueva.setDiaNacimiento(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd));
				datosmayores.add(nueva);
			}
			if(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)<=fechavisita.get(Calendar.MONTH)){
				Personas nueva= new Personas();
				nueva.setPersona_id(posiblesinformantessalud.get(i).persona_id);
				nueva.setMes(Integer.parseInt(posiblesinformantessalud.get(i).qh7mm)-1);
				nueva.setDias(CantidaddeDias(fechavisita, fechafinal));
				nueva.setDiaNacimiento(Integer.parseInt(posiblesinformantessalud.get(i).qh7dd));
				datosmenores.add(nueva);
			}
		}
//		Log.e("VISITA ","IGUAL "+mesigualavisita);
		Integer messeleccion=0;
		Integer diaseleccion=0;
		if(Util.esDiferente(mesigualavisita, 0)){
//			Log.e("MESESSSS","IGUALESSSS ");
			for(Personas p: datomesesigulales){
//				Log.e("","id "+p.getPersona_id()+" dia "+p.getDiaNacimiento());
				if(p.getDiaNacimiento()>diaseleccion && !Util.esDiferente(p.getMes(), fechavisita.get(Calendar.MONTH))){
//					Log.e("EEEEE",""+p.getPersona_id()+ " jj"+diaseleccion);
					persona_id=p.getPersona_id();
					messeleccion=p.getMes();
					diaseleccion=p.getDiaNacimiento();
				}
			}
//			Log.e("","POR AQUI -----");
			if(persona_id==0){
				for(Personas pp: datos){
//					Log.e("DDD ","ID= "+pp.getPersona_id()+"  dd "+pp.getDias());
					if(!Util.esDiferente(pp.getDiaNacimiento(), 98) && !Util.esDiferente(pp.getMes(), fechavisita.get(Calendar.MONTH))){
//						Log.e("","POR   "+pp.getPersona_id());
//						Log.e("","DIA   "+pp.getDiaNacimiento());
						persona_id=pp.getPersona_id();
						messeleccion=pp.getMes();
						break;
					}
				}
			}
			diaseleccion=0;
			if(persona_id==0 && datosmesesigualesdiasmayores.size()==datos.size()){
				for(Personas ppmm: datosmesesigualesdiasmayores){
					if(ppmm.getDiaNacimiento()>=diaseleccion && !Util.esDiferente(ppmm.getMes(), fechavisita.get(Calendar.MONTH))){
						persona_id=ppmm.getPersona_id();
						messeleccion=ppmm.getMes();
						diaseleccion=ppmm.getDiaNacimiento();
					}
				}
			}
		}
//		Log.e("SEEEEEE ",""+persona_id);
		if(Util.esDiferente(messeleccion, 0) && Util.esDiferente(diaseleccion, 0)){
			for(Personas p: datos){
				if(!Util.esDiferente(p.getMes(), messeleccion) && !Util.esDiferente(p.getDiaNacimiento(), diaseleccion) && persona_id>p.getPersona_id()){
					persona_id=p.getPersona_id();
				}
			}
		}
		if(datosmayores.size()>0 && datosmenores.size()==0 && persona_id==0){
//			Log.e("MAYORES","MMM");
			long anterior=0;
			for(Personas p: datosmayores){
				p.setDias(p.getDias()<0?p.getDias()*-1:p.getDias());
//				Log.e("ppp",""+p.getPersona_id()+" -- "+p.getDias());
				if(p.getDias()>anterior){
					persona_id=p.getPersona_id();
					anterior=p.getDias();
				}
			}
		}
//		Log.e("", "TOTAL MENORES "+datosmayores.size());
		if(datosmenores.size()>0 && persona_id==0){
//			Log.e("MENORES","mmm");
			long anterior=8787;
			Integer messeleccionid=0;
			Integer messeleccion98=0;
			long anterior98=0;
			Integer mes98=0;
			for(Personas p: datosmenores){
				p.setDias(p.getDias()<0?p.getDias()*-1:p.getDias());
//				Log.e("ppp ",""+p.getPersona_id()+" -- "+p.getDias());
				if(Util.esDiferente(p.getDiaNacimiento(),98)){
					if(p.getDias()<anterior){
					persona_id=p.getPersona_id();
					anterior=p.getDias();
					messeleccionid=p.getMes();
//					Log.e("AAAAA","PPPPP");
					}
				}
				else{
//					Log.e("aaa","eeee");
					if(p.getDias()>anterior98){
						personaid98=p.getPersona_id() ;
						anterior98=p.getDias();
						messeleccion98=p.getMes();
					}
				}
			}
			if(persona_id!=0 && personaid98!=0){
				if(messeleccionid<messeleccion98){
					persona_id=personaid98;
				}
			}
			else{
				persona_id=personaid98!=0?personaid98:persona_id;
			}
		}
//		Log.e("PERSONA_ID ",""+persona_id);
//		Log.e("PERSONA_ID 98"," "+personaid98);
		}
	if(!Util.esDiferente(persona_id, 0) && informantedesaludpordefecto.size()>0){
		persona_id=informantedesaludpordefecto.get(0).persona_id;
	}
	if(!Util.esDiferente(persona_id, 0) && datos.size()>0){
		persona_id=datos.get(0).Persona_id;
	}
	if(!Util.esDiferente(persona_id, 0) && listadomenoresdeedad!=null && listadomenoresdeedad.persona_id!=null){
		persona_id=listadomenoresdeedad.persona_id;
	}
	return persona_id;
}
	public static Long CantidaddeDias(Calendar fechaInicio,Calendar fechaFin)
	{
		long total=0;
//		Log.e("","ACTUAL: "+fechaInicio.getTimeInMillis());
//		Log.e("","NACIMIENTO: "+fechaFin.getTimeInMillis());
		total=fechaInicio.getTimeInMillis() - fechaFin.getTimeInMillis();
//		Log.e("","RESTA:=> "+total);
		long difd=total / (1000 * 60 * 60 * 24);
//		Log.e("","TOTAL DIAS: => "+difd);
		return difd;
	}
	public static class Personas {
		private Integer Persona_id;
		private Integer Mes;
		private long Dias;
		private Integer DiaNacimiento;
		public Integer getPersona_id() {
			return Persona_id;
		}
		public void setPersona_id(Integer persona_id) {
			Persona_id = persona_id;
		}
		public Integer getMes() {
			return Mes;
		}
		public void setMes(Integer mes) {
			Mes = mes;
		}
		public long getDias() {
			return Dias;
		}
		public void setDias(long dias) {
			Dias = dias;
		}
		public Integer getDiaNacimiento() {
			return DiaNacimiento;
		}
		public void setDiaNacimiento(Integer diaNacimiento) {
			DiaNacimiento = diaNacimiento;
		}
	}
	
	public static void LiberarMemoria(){
    	Runtime garbage = Runtime.getRuntime();
	    garbage.gc();
	}
	
	
    public static Visita RegistrarVisitaCompletada(CuestionarioService cuestionarioService, Seccion01Service  seccion01Service, Integer id, Integer  hogar_id , Integer nro_visita, Integer resultadoultimavisita ){		
    	Visita visita= new Visita();
    	visita.id=id;
    	visita.hogar_id=hogar_id;
    	visita.nro_visita=nro_visita;
    	visita.qhvresul=App.HOGAR_RESULTADO_COMPLETADO;
    	Log.e("AAA:","AAAA: "+cuestionarioService.getCuestionarioDelHogarCompletado(id,hogar_id));
    	Log.e("AAA:","BBBB: "+seccion01Service.TodoLosMiembrosDelhogarCompletados(id,hogar_id));
    	Log.e("AAA:","CCCC: "+cuestionarioService.getSeccion3Completado(id,hogar_id));
    	if (Util.esDiferente(resultadoultimavisita,0) 
    			&& cuestionarioService.getCuestionarioDelHogarCompletado(id,hogar_id) 
    			&& seccion01Service.TodoLosMiembrosDelhogarCompletados(id,hogar_id)
//    			&& seccion01Service.TodosLosMiembrosDelHogarDiscapacidadCompletada(id,hogar_id)
    			&& cuestionarioService.getSeccion3Completado(id,hogar_id)) {    			
        		visita.qhvresul=App.HOGAR_RESULTADO_COMPLETADO;
    	}   
    	else{  
    		visita.qhvresul=null;
    	}    	    	
    	return visita;
    }
    
    public static CSVISITA RegistrarVisitaSaludCompletada(CuestionarioService cuestionarioService, Integer id, Integer  hogar_id,Integer persona_id, Integer nro_visita, Integer resultadoultimavisita ){		
    	CSVISITA visita= new CSVISITA();
    	visita.id=id;
    	visita.hogar_id=hogar_id;
    	visita.nro_visita=nro_visita;
    	visita.persona_id=persona_id;
    	visita.qsvresul=null;
    	
//    	Log.e("","resul ultima vistita= "+resultadoultimavisita);
//    	Log.e("","salud completo="+cuestionarioService.getCuestionarioDelSaludCompletado(id,hogar_id,persona_id));
//    	Log.e("","s8 completo="+cuestionarioService.getRegistroCompletadoSaludSeccion8(id,hogar_id));
//    	Log.e("","s8 bucal="+cuestionarioService.getRegistroBucalCompletado(id,hogar_id));
    	
    	if (Util.esDiferente(resultadoultimavisita,0) 
    			&& cuestionarioService.getCuestionarioDelSaludCompletado(id,hogar_id,persona_id) 
    			&& cuestionarioService.getRegistroCompletadoSaludSeccion8(id,hogar_id)
    			&& cuestionarioService.getRegistroBucalCompletado(id,hogar_id)
    			) {
        		visita.qsvresul=App.SALUD_RESULTADO_COMPLETADO;
    	}   
    	else{  
    		visita.qsvresul=null;
    	}   
    	return visita;
    }
    public static void llenarEspososParaMef(Activity activity, Seccion01Service service, SpinnerField spinner, Integer id,Integer hogar_id) { 
        List<Seccion01> responsable = new ArrayList<Seccion01>();
        responsable.add(getvacio());
        responsable.add(getnoReside2());
        responsable.addAll(service.getEspososparalaMef(id, hogar_id));
        EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
                        activity, android.R.layout.simple_spinner_item, responsable);
        List<Object> keysAdapter = new ArrayList<Object>();
        for (Seccion01 u : responsable) {
                keysAdapter.add(u.persona_id); 
        }
        spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
   }	
    
  public static SeccionCapitulo[] agregarCampos(List<SeccionCapitulo[]> secciones,String... campos) {
		
    	List<String> camp = new ArrayList<String>(); 
    	for(SeccionCapitulo[] scaps:secciones) {
    		for(SeccionCapitulo scap:scaps) {
    		String[] ss = scap.getCampos();
    			for(String s:ss) {
    				camp.add(s);
    			}
    		}
    	}
    	
    	for(String campo:campos) {
    		camp.add(campo);
    	}
    	String[] values = new String[camp.size()];
    			values = camp.toArray(values);
    	SeccionCapitulo[] sc = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,values)}; 
    	
    	return sc;
    	
    }
  
  public static void llenarComboQI474C(CISECCION_04B c2seccion_04b,SpinnerField spinner,Resources resources,Activity activity){
  	List<Integer> valores = new ArrayList<Integer>();
  	valores.add(c2seccion_04b.qi474a_a);    	valores.add(c2seccion_04b.qi474a_b);
  	valores.add(c2seccion_04b.qi474a_c);    	valores.add(c2seccion_04b.qi474a_d);
  	valores.add(c2seccion_04b.qi474a_e);    	valores.add(c2seccion_04b.qi474a_f);
  	valores.add(c2seccion_04b.qi474a_g);    	valores.add(c2seccion_04b.qi474a_h);
  	valores.add(c2seccion_04b.qi474a_i);    	valores.add(c2seccion_04b.qi474a_j);
  	valores.add(c2seccion_04b.qi474a_k);    	valores.add(c2seccion_04b.qi474a_l);
  	valores.add(c2seccion_04b.qi474a_m);    	valores.add(c2seccion_04b.qi474a_n);
  	valores.add(c2seccion_04b.qi474a_o);    	valores.add(c2seccion_04b.qi474a_p);
  	valores.add(c2seccion_04b.qi474a_x);
  	
  	List<SegmentacionFiltro> lista =new ArrayList<SegmentacionFiltro>();
  	SegmentacionFiltro obj = new SegmentacionFiltro();
  	obj.id=null;
  	obj.nombre="--SELECCIONE--";
  	lista.add(obj);
  	for(int i = 0;i<valores.size(); i++) {
  		if(valores.get(i)!=null && valores.get(i)==1) {
  			obj = new SegmentacionFiltro();
  			obj.id=i+1;  
  			if(i==0)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_a);
  			else if(i==1)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_b);
  			else if(i==2)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_c);
  			else if(i==3)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_d);
  			else if(i==4)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_e);
  			else if(i==5)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_f);
  			else if(i==6)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_g);
  			else if(i==7)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_h);
  			else if(i==8)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_i);
  			else if(i==9)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_j);
  			else if(i==10)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_k);
  			else if(i==11)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_l);
  			else if(i==12)
  				obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_m);
  			else if(i==13)
      			obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_n);
  			else if(i==14)
      			obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_o);
  			else if(i==15)
      			obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_p);
  			else if(i==16)
      			obj.nombre=resources.getString(R.string.c2seccion_04bqi474a_x);
  			
  			if(obj.nombre!=null)
  				lista.add(obj);
  		}
  	}
  	 if(lista.size()>0) {  
		EntitySpinnerAdapter<SegmentacionFiltro> spinnerAdapter = new EntitySpinnerAdapter<SegmentacionFiltro>(
				activity, android.R.layout.simple_spinner_item, lista);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (int i = 0; i < lista.size(); i++) {
			SegmentacionFiltro d = (SegmentacionFiltro) lista.get(i);
			keysAdapter.add(d.id+"");
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
  	 }
  }
  public static Integer DeterminarIndice(Integer anio,Integer mes){
		 Integer indice=0;
		 Calendar fecha = Calendar.getInstance();
		 if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR))){
			 indice=13-mes;
		 }
		if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR)-1)){
			indice=25-mes;
		}
		if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR)-2)){
			indice=37-mes;
		}
		if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR)-3)){
			indice=49-mes;
		}
		if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR)-4)){
			indice=61-mes;
		}
		if(!Util.esDiferente(anio,fecha.get(Calendar.YEAR)-5)){
			indice=73-mes;
		}
		return indice;
	 }
  
  public static ArrayMap<String,Integer> determinarMesAnio(Integer indice) {
	  ArrayMap<String,Integer> valores = new ArrayMap<String,Integer>();
	  Calendar calendar = new GregorianCalendar();
	  
	  if(indice>=1 && indice<=12) {
		  Integer mes = 13-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR));
	  }
	  if(indice>=13 && indice<=24) {
		  Integer mes = 25-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR)-1);
	  }
	  if(indice>=25 && indice<=36) {
		  Integer mes = 37-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR)-2);
	  }	  
	  if(indice>=37 && indice<=48) {
		  Integer mes = 49-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR)-3);
	  }
		  
	  if(indice>=49 && indice<=60) {
		  Integer mes = 61-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR)-4);
	  }
		  
	  if(indice>=61 && indice<=72) {
		  Integer mes = 73-indice;
		  valores.put("mes",mes);
		  valores.put("anio",calendar.get(Calendar.YEAR)-5);
	  }
	  
	  return valores;
  }
  
  
//  public static CARATULA_INDIVIDUAL CuestionarioIndividualEstaCompleto(CuestionarioService getService,Integer id,Integer hogar_id,Integer persona_id,Integer nro_visita){
//	  CARATULA_INDIVIDUAL visitaci= new CARATULA_INDIVIDUAL();
//	  seccionesCargadoS1_3 = new SeccionCapitulo[]{new SeccionCapitulo(0,-1,-1,"QI106","QI201","QI206","QI226","QI230","ID","HOGAR_ID","PERSONA_ID")};
//	  
////	  CISECCION_01_03 individual=  getService.getCISECCION_01_03(id, hogar_id, persona_id, seccionesCargadoS1_3);
//	  
//	  visitaci.id=id;
//	  visitaci.hogar_id=hogar_id;
//	  visitaci.persona_id=persona_id;
//	  visitaci.nro_visita=nro_visita;
//	  Log.e("","ESTADO:1-03  "+getService.getCompletadoSeccion0103CI(id, hogar_id, persona_id));
//	  Log.e("","ESTADO:5-07  "+getService.getCompletadoSeccion0507CI(id, hogar_id, persona_id));
//	  Log.e("","ESTADO:08  "+getService.getCompletadoSeccion008CI(id, hogar_id, persona_id));
//	  Log.e("","ESTADO:10  "+getService.getCompletadoSeccion10CI(id, hogar_id, persona_id));
//	  Log.e("","ESTADO:DISC  "+getService.getCompletadoSeccionDiscapacidadCI(id, hogar_id,App.CUEST_ID_INDIVIDUAL, persona_id));
//	  Log.e("","ESTADO:4A  "+ getService.getSeccion4ACompletado(id, hogar_id, persona_id));
//	  Log.e("","ESTADO:4B  "+ getService.getSeccion4BCompletado(id, hogar_id, persona_id));
//	  if(
////	   ((individual.qi106>=15 && individual.qi106>=49) || (individual.qi106>=15 && individual.qi106>=49 && (individual.qi201==1 || individual.qi206==1 || individual.qi226==1 || individual.qi230==1)))
////	  && 
//	  getService.getCompletadoSeccion0103CI(id, hogar_id, persona_id) 
//	  && getService.getCompletadoSeccionDiscapacidadCI(id, hogar_id,App.CUEST_ID_INDIVIDUAL, persona_id)
//	  && getService.getSeccion4ACompletado(id, hogar_id, persona_id)
//	  && getService.getSeccion4BCompletado(id, hogar_id, persona_id)
//	  && getService.getCompletadoSeccion0507CI(id, hogar_id, persona_id)
//	  && getService.getCompletadoSeccion008CI(id, hogar_id, persona_id)
//	  && getService.getCompletadoSeccion10CI(id, hogar_id, persona_id)
//	  
//	  
//			  ){
//		  visitaci.qivresul=App.INDIVIDUAL_RESULTADO_COMPLETADO;
//	  }
//	  else{
//		  visitaci.qivresul=null;
//	  }
//	  return visitaci;
//  }
//  

public static String ordinal(int numero) {
	String retorno="";
	String unidad[]={"", "primer", "segundo", "tercer",  
	        "cuarto", "quinto", "sexto", "septimo", "octavo", "noveno"};  
	      String decena[]={"", "decimo", "vigesimo", "trigesimo",  
	      "cuadragesimo","quincuagesimo", "sexagesimo", "septuagesimo",  
	      "octogesimo", "nonagesimo"};  
	      String centena[]={"", "centesimo", "ducentesimo", "tricentesimo",  
	        " cuadringentesimo", " quingentesimo", " sexcentesimo",   
	        " septingentesimo"," octingentesimo", " noningentesimo"};  
	        
	      int u=numero%10;  
	      int d=(numero/10)%10;  
	      int c=numero/100;  
	      if(numero>=100){  
	          retorno = centena[c]+" "+decena[d]+" "+unidad[u];  
	      }else{  
	          if(numero>=10){  
	           retorno = decena[d]+" "+unidad[u];  
	          }else{  
	          retorno = unidad[numero];  
	          }  
	      }  

return retorno;
}
	
public  static Integer CalcularEdadbyMesAnhio(Calendar fechadenacimiento,Calendar fechareferencia) {
	Calendar fechaactual=fechareferencia;
	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
	 if(mes<0){
		anio--;
	}
	return anio;
}
public  static Integer CalcularEdadByFechaNacimiento(Calendar fechadenacimiento,Calendar fechareferencia) {
	Calendar fechaactual=fechareferencia;
	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
	if(mes==0){
		if(gob.inei.dnce.util.Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fechadenacimiento.get(Calendar.DAY_OF_MONTH))){
			anio--;
		}
	}
	 if(mes<0){
		anio--;
	}
	return anio;
}

//public  static Integer CalcularMesesByFechaNacimiento(Calendar fechadenacimiento,Calendar fechareferencia) {
//	Calendar fechaactual=fechareferencia;
//	Integer anio = fechaactual.get(Calendar.YEAR)-fechadenacimiento.get(Calendar.YEAR);
//	Integer mes = fechaactual.get(Calendar.MONTH)-fechadenacimiento.get(Calendar.MONTH);
//	Integer dia = fechaactual.get(Calendar.DAY_OF_MONTH)-fechadenacimiento.get(Calendar.DAY_OF_MONTH);
//	
//	if(mes==0){
//		if(gob.inei.dnce.util.Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fechadenacimiento.get(Calendar.DAY_OF_MONTH))){
//			anio--;
//		}
//	}
//	int meses=0;
//	if(mes<0){
//		anio--;
//		meses = 12 +mes;
//	}
//	
//	if(anio>0) 
//		meses+=anio*12;
//		
//	if(mes>0) 
//		meses+=mes;
//	
//	if(dia<0)
//		meses-=1;
//
//	return meses;
//}

public static Integer getRangoDit(Integer meses) {
	Integer rango=0;
	if(meses>=9 && meses<=12) {
		rango= 1;
	}
	if(meses>=15 && meses<=18){
		rango= 2;
	}
	if(meses>=30 && meses<=36) {
		rango= 3;
	}
	if(meses>=53 && meses<=59) {
		rango= 4;
	}
	return rango;
}
public static Integer DeterminarAnioInicial(Integer indice){
	Calendar calendar=Calendar.getInstance();
	Integer anio=0;
	if(!Util.esDiferente(indice, 1,2,3,4,5,6,7,8,9,10,11,12)){
		anio=calendar.get(Calendar.YEAR);
	}
	if(!Util.esDiferente(indice, 13,14,15,16,17,18,19,20,21,22,23,24)){
		anio=calendar.get(Calendar.YEAR)-1;
	}
	if(!Util.esDiferente(indice, 25,26,27,28,29,30,31,32,33,34,35,36)){
		anio=calendar.get(Calendar.YEAR)-2;
	}
	if(!Util.esDiferente(indice, 37,38,39,40,41,42,43,44,45,46,47,48)){
		anio=calendar.get(Calendar.YEAR)-3;
	}
	if(!Util.esDiferente(indice,49,50,51,52,53,54,55,56,57,58,59,60)){
		anio=calendar.get(Calendar.YEAR)-4;
	}
	if(!Util.esDiferente(indice,61,62,63,64,65,66,67,68,69,70,71,72)){
		anio=calendar.get(Calendar.YEAR)-5;
	}
	return anio;
}
public static Integer DeterminarMesInicial(Integer indice){
	Integer mes=0;
	if(!Util.esDiferente(indice, 1,13,25,37,49,61)){
		mes= 12;
	}
	if(!Util.esDiferente(indice, 2,14,26,38,50,62)){
		mes= 11;
	}
	if(!Util.esDiferente(indice, 3,15,27,39,51,63)){
		mes= 10;
	}
	if(!Util.esDiferente(indice, 4,16,28,40,52,64)){
		mes= 9;
    }
	if(!Util.esDiferente(indice, 5,17,29,41,53,65)){
		mes= 8;
    }
	if(!Util.esDiferente(indice, 6,18,30,42,54,66)){
		mes= 7;
    }
	if(!Util.esDiferente(indice, 7,19,31,43,55,67)){
		mes= 6;
    }
	if(!Util.esDiferente(indice, 8,20,32,44,56,68)){
		mes= 5;
    }
	if(!Util.esDiferente(indice, 9,21,33,45,57,69)){
		mes= 4;
    }
	if(!Util.esDiferente(indice, 10,22,34,46,58,70)){
		mes= 3;
    }
	if(!Util.esDiferente(indice, 11,23,35,47,59,71)){
		mes= 2;
    }
	if(!Util.esDiferente(indice, 12,24,36,48,60,72)){
		mes= 1;
    }
	return mes;
}

public static Integer CalcularEdadEnMesesFelix(Calendar fecha_nacimiento,Calendar fechareferencia){
	Log.e("","FECHA NACIMIENTO: A: "+fecha_nacimiento.get(Calendar.YEAR)+"M: "+fecha_nacimiento.get(Calendar.MONTH)+" D: "+fecha_nacimiento.get(Calendar.DAY_OF_MONTH));
	Log.e("","FECHA REFERENCIA: A: "+fechareferencia.get(Calendar.YEAR)+"M: "+fechareferencia.get(Calendar.MONTH)+" D: "+fechareferencia.get(Calendar.DAY_OF_MONTH));
	Integer anio = fechareferencia.get(Calendar.YEAR)-fecha_nacimiento.get(Calendar.YEAR);
	Integer mes = fechareferencia.get(Calendar.MONTH)-fecha_nacimiento.get(Calendar.MONTH);
	Integer meses=0;
	if(anio==0 && mes==0){
		meses=0;
	}
	else{
		if(anio==0 && mes>0){
			meses=mes;
			if(Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
	    		Log.e("","EDAD: 000--");
				meses--;
			}
			Log.e("","EDAD: 000");
		}
		else{
	    	if(mes==0){
	    		Log.e("","EDAD: 111111 ");
	    		if(Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
	    			anio--;
	    		}
	    		Log.e("","anio"+ anio);
	    	}
	    	else if(mes<0){
	    		Log.e("","EDAD: 2222");
	    		anio--;
	    	}
	    	
	    	if(anio==0){
	    		Log.e("","EDAD: 33333");
	    		Integer mesesantes = 11-fecha_nacimiento.get(Calendar.MONTH);
	    		meses = fechareferencia.get(Calendar.MONTH)+mesesantes+1;
	    		if(Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
	    			meses--;
	    		}
	    	}else{
	    		Log.e("","EDAD: 44444");
	    		meses=anio*12;
		    	if(Util.esMayor(fechareferencia.get(Calendar.MONTH),fecha_nacimiento.get(Calendar.MONTH))){
		    		meses=meses+(fechareferencia.get(Calendar.MONTH)-fecha_nacimiento.get(Calendar.MONTH));
		    		if(Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
		    			meses--;
		    			Log.e("","EDAD: IF ");
		    		}
		    	}
		    	else if(Util.esMayor(fecha_nacimiento.get(Calendar.MONTH),fechareferencia.get(Calendar.MONTH))){
		    		Log.e("","EDAD: 33333_--");
		    			meses=meses+fechareferencia.get(Calendar.MONTH)+1;	
		    			meses = meses + (11-fecha_nacimiento.get(Calendar.MONTH));
		    			if(Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
			    			meses--;
			    		}
		    		}
		    	else if(!Util.esDiferente(fechareferencia.get(Calendar.MONTH),fecha_nacimiento.get(Calendar.MONTH),fecha_nacimiento.get(Calendar.MONTH)) && Util.esMenor(fechareferencia.get(Calendar.DAY_OF_MONTH),fecha_nacimiento.get(Calendar.DAY_OF_MONTH))){
		    		Log.e("","EDAD: 5555");
		    		meses= meses +11;
		    	}
		    }
		}
	}
 	return meses;
}
public static CSVISITA VerificarCompletadoVisitaDeSalud(CSVISITA visitasalud,SeccionCapitulo[] secciones_08, CuestionarioService service,SeccionCapitulo...secciones09){
	boolean existeseccion08=false;
	existeseccion08=service.getExistealMenosUnNinio(visitasalud.id,visitasalud.hogar_id);
	CAP04_07 cap04_07=null;
	cap04_07=service.getCAP04_07(visitasalud.id,visitasalud.hogar_id,visitasalud.persona_id,secciones09);
//	Log.e("","EXISTE: "+cap04_07);
	Calendar fecha= Calendar.getInstance();
//	Log.e("","COMPLETO: "+VerificarSeccion08CompletadoTodosYCompletadoSeccion09Salud(visitasalud.id,visitasalud.hogar_id,visitasalud.persona_id,secciones_08,service,secciones09));
	Integer resultsectioneight=VerificarSeccion08CompletadoTodosYCompletadoSeccion09Salud(visitasalud.id,visitasalud.hogar_id,visitasalud.persona_id,secciones_08,service,secciones09);
	//Integer resultsectionnine = ResultSectionNinehealth(visitasalud.id,visitasalud.hogar_id,visitasalud.persona_id,service,secciones09);
//	Log.e("","visitasalud.qsresult: "+visitasalud.qsresult);
//	Log.e("","existeseccion08: "+existeseccion08);
//	Log.e("","resultsectioneight: "+resultsectioneight);
//	Log.e("","qsvresul: "+visitasalud.qsvresul);
//	Log.e("","resultsectionnine: "+resultsectionnine);
	
	
	//if(visitasalud.qsresult!=null && ((existeseccion08 && resultsectioneight==1 && resultsectionnine ==1) || (!existeseccion08 && resultsectionnine ==1))){
	if(cap04_07!=null && visitasalud.qsresult!=null && ((existeseccion08 && resultsectioneight==1)  || (!existeseccion08))){
		Log.e("1","1");
		if(cap04_07.qs731ah!=null && cap04_07.qs731am!=null){
			visitasalud.qsvhora_fin=cap04_07.qs731ah;
			visitasalud.qsvminuto_fin=cap04_07.qs731am;
		}
		visitasalud.qsvresul=1;
		visitasalud.qsresult=1;
	}
	else{
		if(visitasalud.qsresult!=null || cap04_07!=null){
		 if(visitasalud.qsresult!=null && ((visitasalud.qsresult==2 && existeseccion08 && resultsectioneight==2)|| (visitasalud.qsresult==2  && !existeseccion08))){
				Log.e("1","2");
				visitasalud.qsvresul=2;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		 }
		 else if(visitasalud.qsresult!=null && ((visitasalud.qsresult==4 && existeseccion08 && resultsectioneight==4)|| (visitasalud.qsresult==4  && !existeseccion08))){
				Log.e("1","4");
				visitasalud.qsvresul=4;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
		 else if(visitasalud.qsresult!=null && ((visitasalud.qsresult==6 && existeseccion08 && resultsectioneight==6)|| (visitasalud.qsresult==6  && !existeseccion08))){
				Log.e("1","6");
				visitasalud.qsvresul=6;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
		 else if(visitasalud.qsresult!=null && ((visitasalud.qsresult==9 && existeseccion08 && resultsectioneight==9)|| (visitasalud.qsresult==9  && !existeseccion08))){
				Log.e("1","9");
				visitasalud.qsvresul=9;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
		 else if(visitasalud.qsresult!=null && ((visitasalud.qsresult==3 && existeseccion08 && resultsectioneight==3)|| (visitasalud.qsresult==3  && !existeseccion08))){
				Log.e("1","3");
				visitasalud.qsvresul=3;
				visitasalud.qsvhora_fin=  visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin= visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
		 else if(visitasalud.qsresult!=null && ((visitasalud.qsresult==5 && existeseccion08 && resultsectioneight==5)|| (visitasalud.qsresult==5  && !existeseccion08))){
				Log.e("1","5");
				visitasalud.qsvresul=5;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
		else{
			Log.e("1","55 else ");
			if(visitasalud.qsresult==null)
				visitasalud.qsresult=5;
				
			visitasalud.qsvresul=5;
			visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY)+"";
			visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE)+"";
		}
	  }
	}
/*	
	if(VerificarSeccion08CompletadoTodosYCompletadoSeccion09Salud(visitasalud.id,visitasalud.hogar_id,visitasalud.persona_id,secciones_08,service,secciones09)){
		if(cap04_07.qs731ah!=null && cap04_07.qs731am!=null){
			visitasalud.qsvhora_fin=Integer.parseInt(cap04_07.qs731ah);
			visitasalud.qsvminuto_fin=Integer.parseInt(cap04_07.qs731am);
		}
		visitasalud.qsvresul=App.SALUD_RESULTADO_COMPLETADO;
		visitasalud.qsresult=App.SALUD_RESULTADO_COMPLETADO;
	}
	else{
		if(visitasalud.qsresult!=null || cap04_07!=null){
			if(visitasalud.qsresult!=null && visitasalud.qsresult==2 && !existeseccion08){
				Log.e("1","2");
				visitasalud.qsvresul=2;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
			}else if(visitasalud.qsresult!=null && visitasalud.qsresult==3 && !existeseccion08){
				Log.e("1","3");
				visitasalud.qsvresul=3;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
			}
			else if(visitasalud.qsresult!=null && visitasalud.qsresult==4 && !existeseccion08){
				Log.e("1","4");
				visitasalud.qsvresul=4;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
			}
			else if(visitasalud.qsresult!=null && visitasalud.qsresult==6 && !existeseccion08){
				Log.e("1","5");
				visitasalud.qsvresul=6;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
			}
			else if(visitasalud.qsresult!=null && visitasalud.qsresult==9 && !existeseccion08){
				Log.e("1","6");
				visitasalud.qsvresul=9;
				visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
				visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
			}
			else{
				Log.e("","ENTRO AQUI: CAMBIO");
				visitasalud.qsvresul=5;
				visitasalud.qsresult=visitasalud.qsresult;
				if(cap04_07 !=null && cap04_07.qs731ah!=null && cap04_07.qs731am!=null){
					visitasalud.qsvhora_fin=Integer.parseInt(cap04_07.qs731ah);
					visitasalud.qsvminuto_fin=Integer.parseInt(cap04_07.qs731am);
				}
				else{
					visitasalud.qsvhora_fin= visitasalud.qsvhora_fin!=null?visitasalud.qsvhora_fin:fecha.get(Calendar.HOUR_OF_DAY);
					visitasalud.qsvminuto_fin=visitasalud.qsvminuto_fin!=null?visitasalud.qsvminuto_fin:fecha.get(Calendar.MINUTE);
				}
			}
	  }
	}*/
	return visitasalud; 
}

public static Integer VerificarSeccion08CompletadoTodosYCompletadoSeccion09Salud(Integer id,Integer hogar_id, Integer persona_id,SeccionCapitulo[] secciones_08, CuestionarioService service,SeccionCapitulo...secciones09){
	boolean completadoseccion08=false;
	boolean completadoseccion09=false;
	List<CSSECCION_08> Listado= new ArrayList<CSSECCION_08>();
	
	Listado=service.getListadoNiniosSeccion08(id,hogar_id,secciones_08);

	Integer contador=0;
	if(Listado.size()==0){
		contador=-1;
		completadoseccion08=true;
	}
	Integer accountantone=0;
	Integer accountanttwo=0;
	Integer accountantfour=0;
	Integer accountantfive=0;
	Integer accountantsix=0;
	Integer accountantnine=0;
	Integer valuetoreturn=-1;
	for(CSSECCION_08 ninio:Listado){
		if(ninio.qs802a!=null && ninio.qs802a==1 && ninio.estado!=null){
			accountantone++;
		}
		if(ninio.qs802a!=null && ninio.qs802a==2){
			accountanttwo++;
		}
		if(ninio.qs802a!=null && ninio.qs802a==4){
			accountantfour++;
		}
		if(ninio.qs802a!=null && ninio.qs802a==5){
			accountantfive++;
		}
		if(ninio.qs802a!=null && ninio.qs802a==6){
			accountantsix++;
		}
		if(ninio.qs802a!=null && ninio.qs802a==9){
			accountantnine++;
		}
	}
	
	if(Listado.size()!=0){
		if(!Util.esDiferente(accountantone, Listado.size()))
			valuetoreturn=1;
		else if(!Util.esDiferente(accountanttwo, Listado.size()))
			valuetoreturn=2;
		else if(!Util.esDiferente(accountantfour, Listado.size()))
			valuetoreturn=4;
		else if(!Util.esDiferente(accountantsix, Listado.size()))
			valuetoreturn=6;
		else if(!Util.esDiferente(accountantnine, Listado.size()))
			valuetoreturn=9;
		else
			valuetoreturn=5;
	}

return valuetoreturn;
	
	
//	for(CSSECCION_08 ninio:Listado){
//		if(ninio.qs802a!=null && ninio.qs802a==1){
//			contador++;
//		}
//	}
	
	
	
	
//	Log.e("","COMPLETADO: S9 "+completadoseccion09);
//	Log.e("","COMPLETADO: 08 "+completadoseccion08);
//	return completadoseccion08 && completadoseccion09;
}
public static Integer ResultSectionNinehealth(Integer id,Integer hogar_id, Integer persona_id, CuestionarioService service,SeccionCapitulo...secciones09){
	CAP04_07 cap04_07=null;
	cap04_07=service.getCAP04_07(id,hogar_id,persona_id,secciones09);
	Integer valuetoreturn=-1;
		
	if(cap04_07==null) {
		valuetoreturn=5;
	}
	if(cap04_07!=null){
		if(cap04_07.qs902!=null && cap04_07.qs906!=null){
			if( (cap04_07.qs902==1 || cap04_07.qs902==4) && cap04_07.qs906==1)
				valuetoreturn=1;
			else if(!Util.esDiferente(cap04_07.qs902, 2)) 
				valuetoreturn=2;
			else if(!Util.esDiferente(cap04_07.qs902, 3))
				valuetoreturn=3;
			else if(!Util.esDiferente(cap04_07.qs902, 4))
				valuetoreturn=4;
			else if(!Util.esDiferente(cap04_07.qs902, 5))
				valuetoreturn=5;
			else if(!Util.esDiferente(cap04_07.qs902, 6))
				valuetoreturn=6;
		}
	}
	return valuetoreturn;
}
	public static void MensajeGeneral(Activity actividad,String mensaje){
		ToastMessage.msgBox(actividad, mensaje, ToastMessage.MESSAGE_ERROR,ToastMessage.DURATION_LONG);
	}
	public static void GetDatosDeGeolocalizacion(){
		Log.e("","PASO: ");
		App.USER_LOCATION_STATUS=App.APP_SAVE;
		if(App.getInstance().getMarco()!=null && App.getInstance().getMarco().conglomerado!=null){
			App.GEO_CONGLOMERADO=App.getInstance().getMarco().conglomerado;
			if(App.getInstance().getMarco().conglomerado!=null){
				App.GEO_CODCCPP=App.getInstance().getMarco().codccpp;
			}
			if(App.getInstance().getMarco().manzana!=null){
				App.GEO_MANZANA_ID=App.getInstance().getMarco().manzana;
			}
			if(App.getInstance().getMarco().nselv!=null){
				App.GEO_VIVIENDA=App.getInstance().getMarco().nselv;
			}
		}
		
	}
   public static void DelDatosGeolocalizacion(){
	   App.USER_LOCATION_STATUS=App.APP_ON;
	   App.GEO_CONGLOMERADO="";
	   App.GEO_CODCCPP="";
	   App.GEO_MANZANA_ID="";
	   App.GEO_VIVIENDA="";
   }
   public static String CompletarCerosalaIzquierdaParadosDigitos(String valor){
	   String retornar="";
	   if(valor!=null){
		   retornar=valor.length()<2?"0"+valor:valor;
	   }
	   return retornar;
   }
   public static void llenarBeneficiariosCovid(Activity activity,Seccion01Service service,SpinnerField spinner, Integer id, Integer hogar_id,Integer persona_id_orden,Integer edadMinima,Integer edadMaxima,Integer pregunta_id){
		List<Seccion01> covid = new ArrayList<Seccion01>();
		covid.add(getvacio());
		covid.addAll(service.getCovid(id,hogar_id,persona_id_orden,edadMinima,edadMaxima,pregunta_id));
		EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
               activity, android.R.layout.simple_spinner_item, covid);
		List<Object> keysAdapter = new ArrayList<Object>();
		for (Seccion01 u : covid) {
		        keysAdapter.add(u.persona_id);
		}
		spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
   

	 public static void llenarPersonasInformanteVacunas(Activity activity,Seccion01Service service,SpinnerField spinner, Integer id, Integer hogar_id){
			List<Seccion01> covid = new ArrayList<Seccion01>();
			covid.add(getvacio());
			covid.addAll(service.getInformateVacunas(id,hogar_id));
			EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
	               activity, android.R.layout.simple_spinner_item, covid);
			List<Object> keysAdapter = new ArrayList<Object>();
			for (Seccion01 u : covid) {
			        keysAdapter.add(u.persona_id);
			}
			spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	 
	 public static void llenarPersonasInformanteMigracion(Activity activity,Seccion01Service service,SpinnerField spinner, Integer id, Integer hogar_id){
			List<Seccion01> covid = new ArrayList<Seccion01>();
			covid.add(getvacio());
			covid.add(getnoReside2());
			covid.addAll(service.getInformateMigracion(id,hogar_id));
			EntitySpinnerAdapter<Seccion01> spinnerAdapter = new EntitySpinnerAdapter<Seccion01>(
	               activity, android.R.layout.simple_spinner_item, covid);
			List<Object> keysAdapter = new ArrayList<Object>();
			for (Seccion01 u : covid) {
			        keysAdapter.add(u.persona_id);
			}
			spinner.setAdapterWithKey(spinnerAdapter, keysAdapter);
	}
	   
}