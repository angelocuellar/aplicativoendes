package gob.inei.endes2024.asyncTask;

import gob.inei.dnce.interfaces.Respondible;
import gob.inei.endes2024.common.App;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;


public class UploadLocationWorkAndNotWork extends AsyncTask<String, String, String> implements Respondible, Observer {

	private String tabletId;
	private String conglomerado;
	private String codccpp;
	private String manzana_id;
	private String vivienda;
	private String latitude;
	private String longitude;
	private String altitude;
	private String bateria;
	private String temperatura_bateria;
	private String model;
	private String serie;
	private String conexion;
	private String apk_version;
	private String status;
	
	
	public UploadLocationWorkAndNotWork(String id,String conglomerado,String codccpp, String manzana_id,String vivienda, double lat, double lon, double alt, int bateria, int temperatura_bateria, String model, String serie,String conexion,String apk_version,int userLocationStatus){
		super();
		this.tabletId 		= id;
		this.conglomerado	=conglomerado;
		this.codccpp		=codccpp;
		this.manzana_id		=manzana_id;
		this.vivienda		=vivienda;
		this.latitude 		= lat == 0 ? "" : Double.toString(lat);
		this.longitude 		= lon == 0 ? "" : Double.toString(lon);
		this.altitude 		= alt == 0 ? "" : Double.toString(alt);
		this.status 		= String.valueOf(userLocationStatus);
		this.bateria 		= String.valueOf(bateria);
		this.temperatura_bateria=String.valueOf(temperatura_bateria);
		this.model 			= model;
		this.serie 			= serie;
		this.apk_version	=apk_version;
		this.conexion		=conexion;
		
	}
	
	public UploadLocationWorkAndNotWork(){
		super();
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void update(Observable observable, Object data) {
		// TODO Auto-generated method stub
	}

	@Override
	protected String doInBackground(String... params) {	
		
		App.USER_LOCATION_STATUS = App.USER_LOCATION_STATUS != 0 ? 1 : 0;
		
		int serverResponseCode = 0;
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		try {
//				URL url = new URL("http://webinei.inei.gob.pe/PyENDES/service/LocationGPSEncuesta/UserLocationGPSEncuesta");
				URL url = new URL("");
				conn = (HttpURLConnection) url.openConnection();
		        conn.setDoInput(true);
		        conn.setDoOutput(true);
		        conn.setUseCaches(false);
		        conn.setRequestMethod("POST");
		        conn.setRequestProperty("Connection", "Keep-Alive");
		        conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		        conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
	
		        dos = new DataOutputStream(conn.getOutputStream());	                 
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"tabletId\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.tabletId);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"conglomerado\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.conglomerado);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"codccpp\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.codccpp);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"manzana_id\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.manzana_id);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"vivienda\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.vivienda);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"latitude\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.latitude);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"longitude\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.longitude);
		        dos.writeBytes(lineEnd); 
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"altitude\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.altitude);
		        dos.writeBytes(lineEnd); 
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"model\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.model);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"bateria\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.bateria);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"temp_bateria\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.temperatura_bateria);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"conexion\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.conexion);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"apk_version\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.apk_version);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"serie\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.serie);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"status\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.status);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(twoHyphens + boundary + lineEnd);
		        dos.writeBytes("Content-Disposition: form-data; name=\"dateCreate\""+ lineEnd);
		        dos.writeBytes(lineEnd);
		        dos.writeBytes(this.GetDateTimeNow());
		        dos.writeBytes(lineEnd);
		        serverResponseCode = conn.getResponseCode();
		        String serverResponseMessage = conn.getResponseMessage();
		                   
		        Log.e("localizacion", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode + " : " + this.status);
		                  		                 
		        if(serverResponseCode == 200) {
		        	dos.flush();
		        	dos.close();
		        	return "Process SucessFully";
		        }
		        else {
		        	dos.flush();
		        	dos.close();
		            return "Process Error"; 
		        }
			} catch (MalformedURLException ex) {
				return "Upload file to server " + ex.getMessage() + ex;
			} catch (Exception e) {
				return e.getMessage() + e;
			}
	}
	
	@SuppressLint("SimpleDateFormat")
	private String GetDateTimeNow(){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(c.getTime());
	}
}
