package gob.inei.endes2024.asyncTask;


import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.components.SpinnerField;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.interfaces.IDetailEntityComponent;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;
import gob.inei.endes2024.service.CuestionarioService;
import gob.inei.endes2024.service.HogarService;
import gob.inei.endes2024.service.MarcoService;
import gob.inei.endes2024.service.Seccion01Service;
import gob.inei.endes2024.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import paul.arian.fileselector.FolderSelectionActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View.OnClickListener;

public class ExportData extends AsyncTask<String, String, String> implements Respondible, Observer {
	public SpinnerField spnPERIODO;
	private FragmentForm fragment;
	private static enum PROCESS{EXPORTAR,VALIDAR,SERVER};
	public List<SegmentacionFiltro> viviendasexportables,viviendasexportadasavalidar;
	private List<ViviendaSeleccionada> viviendaseleccionada;
	private List<Marco> marcos;
	private MarcoService service;
	private PROCESS accionp;
	private String ruta;
	private CuestionarioService cuestionarioService;
	private Seccion01Service personaService;
	private List<Hogar> hogares;
	private HogarService hogarService;
	private SeccionCapitulo[] seccionesCargado; 
	
	public ExportData(FragmentForm fragment, SpinnerField spnPERIODO, SeccionCapitulo[] seccionesCargado){
		this.fragment = fragment;
		this.spnPERIODO = spnPERIODO;
		this.seccionesCargado = seccionesCargado;
	}

	@Override
	public void update(Observable observable, Object data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String doInBackground(String... params) {
		DialogComponent dlg = new DialogComponent(ExportData.this.fragment.getActivity(), ExportData.this, DialogComponent.TIPO_DIALOGO.NEUTRAL, 
				ExportData.this.fragment.getResources().getString(R.string.app_name), "Seleccione alg�n item.");
		 viviendasexportables = new ArrayList<SegmentacionFiltro>();
		 viviendasexportadasavalidar = new ArrayList<SegmentacionFiltro>();
		 if(viviendaseleccionada==null){
			 return "";
		}
		for (ViviendaSeleccionada mco: viviendaseleccionada) {
			if(!Util.esDiferente(mco.seleccionada, 1)){
				viviendasexportadasavalidar.add(mco.marco);
			}
		}
		if(viviendasexportadasavalidar.size()==0){
			dlg.showDialog();
			return "";
		}
		else{
			String cadena="";
			String mensajegeneral="";
			for(SegmentacionFiltro detalle:viviendasexportadasavalidar){
				cadena=ValidaciondeViviendasaExportar(detalle);
				if(cadena.equals("completo")){
					viviendasexportables.add(detalle);
				}
				else{
					mensajegeneral="Conglomerado: "+detalle.nombre+" "+cadena+"\n"+mensajegeneral;
					viviendasexportables.add(detalle);
				}
				cadena="";
			}
			if(mensajegeneral.toString().length()>0){
				DialogComponent dialogo = new DialogComponent(ExportData.this.fragment.getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, 
						ExportData.this.fragment.getResources().getString(R.string.app_name),mensajegeneral);
				dialogo.showDialog();
				
				accionp=PROCESS.VALIDAR;
			}
			else{
				accionp = PROCESS.EXPORTAR;
				seleccionarRuta();
			}
		}
		return null;
	}
	
	private String ValidaciondeViviendasaExportar(SegmentacionFiltro conglomerado){
		boolean flag=false;
		String mensaje="completo";
			marcos= getService().getMarco(Integer.parseInt(spnPERIODO.getSelectedItemKey().toString()),conglomerado.nombre);
			for(Marco m:marcos){
				if(!mensaje.equals("completo")){
					break;
				}
				if(m.id!=null){
					flag = getCuestionarioService().getExisteCaratulaDelaVivienda(m.id);
					if(!flag){
						mensaje="La Vivienda: "+m.nselv+" Falta aperturar";
						break;
					}
				}
				if(m.resviv!=null){
					flag=getCuestionarioService().getViviendaTieneGPS(m.id);
					if(!flag){
						mensaje="La Vivienda: "+m.nselv+" no tiene punto GPS";
						break;
					}
					hogares = getHogarService().getHogares(m.id, seccionesCargado);
					for(Hogar h: hogares){
						if(getCuestionarioService().getResultadodelaUltimaVisitaHogar(h.id, h.hogar_id)){
							flag=getCuestionarioService().getExisteResultadodelasVisitas(h.id, h.hogar_id);
							if(!flag){
								mensaje=" Falta resultado a la visita de la Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getHogarCompletado(h.id,h.hogar_id);
							if(!flag){
								mensaje="Falta Completar Hogar Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag =getPersonaService().TodoLosMiembrosDelhogarCompletados(h.id,h.hogar_id);
							if(!flag){
								mensaje="Completar Cuestionario para todo los miembros del hogar la Vivienda: "+m.nselv+" Hogar:"+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getSeccion04_05Completado(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta completa Secci�n 04 o Secci�n 05 Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getVisitaSaludCompletada(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta aperturar visita de salud existe un seleccionado en Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getExisteCuestionarioDeSalud(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta Completar Cuestionario de Salud Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag=getCuestionarioService().getSeccion08Completado(h.id,h.hogar_id);
							if(!flag){
								mensaje="Falta completar secci�n 08 salud Vivienda: "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							flag = getPersonaService().TodasLasMefCompletadas(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta poner resultado a alguna mef en la vivienda "+m.nselv+" Hogar: "+h.hogar_id;
								break;
							}
							if(flag)
								mensaje="completo";
						}
						else{
							flag=getCuestionarioService().getExisteResultadodelasVisitas(h.id, h.hogar_id);
							if(!flag){
								mensaje="Falta resultado a la visita de la Vivienda: "+m.nselv+ " Hogar: "+h.hogar_id;
								break;
							}
							if(flag){
								mensaje="completo";
							}
						}
							
					}
				}
				else{
					mensaje="completo";
				}
			}
	 return mensaje;
	}
	
	public MarcoService getService() {
		if (service == null) {
			service = MarcoService.getInstance(ExportData.this.fragment.getActivity());
		}
		return service;
	}
	
	private void seleccionarRuta() {
		ruta = App.RUTA_BASE + "/backups/";
		File directorio = new File(ruta);
		if (!directorio.exists()) {
			directorio.mkdirs();
		}
		Intent intent = new Intent(ExportData.this.fragment.getActivity(), FolderSelectionActivity.class);
		intent.putExtra(FolderSelectionActivity.START_FOLDER, ruta);
		ActivityResultTask(intent);
	}
	
	private void ActivityResultTask(Intent data){
		File carpeta = (File) data.getExtras().getSerializable(FolderSelectionActivity.FILES_TO_UPLOAD);
		ruta = carpeta.getAbsolutePath();
		DialogComponent dlg = new DialogComponent(ExportData.this.fragment.getActivity(), this, DialogComponent.TIPO_DIALOGO.YES_NO, ExportData.this.fragment.getResources().getString(R.string.app_name),"Esta seguro de guardar los archivos en: "+ ruta+"?");
		dlg.showDialog();
	}
	
	private static class ViviendaSeleccionada implements IDetailEntityComponent {
		public Integer seleccionada=null;
		private SegmentacionFiltro marco=null;
		public ViviendaSeleccionada(Integer seleccionada,SegmentacionFiltro marco){
			super();
			this.seleccionada=seleccionada;
			this.marco=marco;
		}
		@Override
		public void cleanEntity() {
		}

		@Override
		public boolean isTitle() {
			// TODO Auto-generated method stub
			return false;
		}
		public String getNombre(){
			return marco.nombre;
		}
			
	}
	
	private CuestionarioService getCuestionarioService() {
		if (cuestionarioService == null) {
			cuestionarioService = CuestionarioService.getInstance(ExportData.this.fragment.getActivity());
		}
		return cuestionarioService;
	}
	
	public Seccion01Service getPersonaService() {
		if (personaService == null) {
			personaService = Seccion01Service.getInstance(ExportData.this.fragment.getActivity());
		}
		return personaService;
	}
	public HogarService getHogarService() {
		if (hogarService == null) {
			hogarService = HogarService.getInstance(ExportData.this.fragment.getActivity());
		}
		return hogarService;
	}
}	