package gob.inei.endes2024.asyncTask;

import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;
import gob.inei.endes2024.listener.UserLocationListenerSMS;

import java.util.TimerTask;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class UserTimerTask extends TimerTask {
	private String tabletId;
	private String model;
	private String serie;
	private Integer bateria;
	private Integer temperatura_bateria;
	private String numero_para_mensajes = "931968056".toString();//901158681
	private BroadcastReceiver receiver1;
	private Context mContext;
	private final Handler handler = new Handler();
	
	public UserTimerTask (Context context, String tabletId, String model, String serie,Integer bateria,Integer temperatura_bateria){
		this.mContext = context;
		this.tabletId = tabletId;
		this.model = model;
		this.serie = serie;
		this.bateria=bateria;
		this.temperatura_bateria=temperatura_bateria;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		handler.post(new Runnable() {
            public void run() {
                try {      
                	UserLocationListenerSMS locationSMS = new UserLocationListenerSMS(mContext);
                	String gps = Util.getText(locationSMS.latitude, null) + ","+ Util.getText(locationSMS.longitude, null)+" , "+ Util.getText(locationSMS.altitude, null)  +" , "+ Util.getText(locationSMS.tipoEnvio,null) ;
                    String msg = gps+","+Util.getFechaActualToString()+","+App.getInstance().getVersion(mContext)+","+App.GEO_CONGLOMERADO+","+App.GEO_CODCCPP+","+App.GEO_MANZANA_ID+","+App.GEO_VIVIENDA+","+bateria+","+getNetworkType(mContext)+",ENC";
                    if(!Util.getText(locationSMS.latitude, null).equals("0.0")){
                    	if(locationSMS.latitude!=App.anteriorLatitud || locationSMS.longitude!=App.anteriorLongitud){
                    		App.anteriorLatitud=locationSMS.latitude;
                    		App.anteriorLongitud=locationSMS.longitude; 
                    		sendSMS(numero_para_mensajes, msg);
                    	}
                    	                    	
                    }
                		
                		/*UserLocationListener location = new UserLocationListener(mContext);
                		String conexion=getNetworkType(mContext);
                		String apk_version=App.getInstance().getVersion(mContext);
                		UploadLocationWorkAndNotWork upload = new UploadLocationWorkAndNotWork(
                		tabletId,
                		App.GEO_CONGLOMERADO,
                		App.GEO_CODCCPP,
                		App.GEO_MANZANA_ID,
                		App.GEO_VIVIENDA,
						location.latitude,
						location.longitude,
						location.altitude,
						bateria,
						temperatura_bateria,
						model,
						serie,
						conexion,
						apk_version,
						App.USER_LOCATION_STATUS);
                		upload.execute();*/                		
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                }
            }
        });
	}
	
	public static String getNetworkType(Context context) {
	    TelephonyManager mTelephonyManager = (TelephonyManager)
	            context.getSystemService(Context.TELEPHONY_SERVICE);
	    int networkType = mTelephonyManager.getNetworkType();
	    switch (networkType) {
	        case TelephonyManager.NETWORK_TYPE_GPRS:
	        case TelephonyManager.NETWORK_TYPE_EDGE:
	        case TelephonyManager.NETWORK_TYPE_CDMA:
	        case TelephonyManager.NETWORK_TYPE_1xRTT:
	        case TelephonyManager.NETWORK_TYPE_IDEN:
	            return "2g";
	        case TelephonyManager.NETWORK_TYPE_UMTS:
	        case TelephonyManager.NETWORK_TYPE_EVDO_0:
	        case TelephonyManager.NETWORK_TYPE_EVDO_A:

	        case TelephonyManager.NETWORK_TYPE_HSDPA:
	        case TelephonyManager.NETWORK_TYPE_HSUPA:
	        case TelephonyManager.NETWORK_TYPE_HSPA:
	        case TelephonyManager.NETWORK_TYPE_EVDO_B:
	        case TelephonyManager.NETWORK_TYPE_EHRPD:
	        case TelephonyManager.NETWORK_TYPE_HSPAP:
	            return "3g";
	        case TelephonyManager.NETWORK_TYPE_LTE:
	            return "4g";
	        default:
	            return "Notfound";
	    }
	}
	private void sendSMS(String phoneNumber, String message) {
		Log.e("","INGRESO: ");
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0,
				new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0,
				new Intent(DELIVERED), 0);

		receiver1 = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// do something based on the intent's action
				switch (getResultCode()) {
				case Activity.RESULT_OK:
						deleteMessage(mContext);
//					Toast.makeText(mContext, "SMS enviado", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//					Toast.makeText(mContext, "Error Generico",Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
//					Toast.makeText(mContext, "Sin servicio", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
//					Toast.makeText(mContext, "Null PDU", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
//					Toast.makeText(mContext, "Radio off", Toast.LENGTH_SHORT).show();
					break;
				}
				if (receiver1 != null) {
					mContext.unregisterReceiver(receiver1);
					receiver1 = null;
				}
			}
		};

		mContext.registerReceiver(receiver1, new IntentFilter(SENT));
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//		deleteMessage(mContext);
//		Log.e("","CLAVE: "+sms.getDefault());

	}
	
	private void sendSMS_NO_ENVIADOS(String phoneNumber, String message) {
//		Log.e("","INGRESO: ");
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0,
				new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0,
				new Intent(DELIVERED), 0);

		receiver1 = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// do something based on the intent's action
				switch (getResultCode()) {
				case Activity.RESULT_OK:
//					deleteMessage(mContext);
//					Toast.makeText(mContext, "SMS enviado", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//					Toast.makeText(mContext, "Error Generico",Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
//					Toast.makeText(mContext, "Sin servicio", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
//					Toast.makeText(mContext, "Null PDU", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
//					Toast.makeText(mContext, "Radio off", Toast.LENGTH_SHORT).show();
					break;
				}
				if (receiver1 != null) {
					mContext.unregisterReceiver(receiver1);
					receiver1 = null;
				}
			}
		};

		mContext.registerReceiver(receiver1, new IntentFilter(SENT));
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//		deleteMessage(mContext);
//		Log.e("","CLAVE: "+sms.getDefault());

	}

private int deleteMessage(Context context) {
	Log.e("","ENTRO: EEE");
    Uri deleteUri = Uri.parse("content://sms");
    int count = 0;
    Cursor c = context.getContentResolver().query(deleteUri, null, null, null, null);
    while (c.moveToNext()) {
        try {
            // Delete the SMS
            String pid = c.getString(0); // Get id;
            String numero = c.getString(2);
            String uri = "content://sms/" + pid;
            Integer estado=c.getInt(9);
            String cuerpo=c.getString(12);
            if(estado==2){
             count = context.getContentResolver().delete(Uri.parse(uri),null, null);
            }
            else if(estado==5 && numero.equals(numero_para_mensajes)){
//            	Log.e("","INGRESO AQUI");
            	sendSMS_NO_ENVIADOS(numero.toString(), cuerpo.toString());
            	count = context.getContentResolver().delete(Uri.parse(uri),null, null);
            }
        } catch (Exception e) {
        }
    }
    return count;
}
}
