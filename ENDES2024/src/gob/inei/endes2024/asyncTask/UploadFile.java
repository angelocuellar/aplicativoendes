package gob.inei.endes2024.asyncTask;

import gob.inei.dnce.components.DialogComponent;
import gob.inei.dnce.components.FragmentForm;
import gob.inei.dnce.dao.xml.XMLObject;
import gob.inei.dnce.dao.xml.XMLObject.BeansProcesados;
import gob.inei.dnce.interfaces.Respondible;
import gob.inei.dnce.util.Util;
import gob.inei.endes2024.common.App;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Path;
import android.os.AsyncTask;
import android.util.Log;

public class UploadFile extends AsyncTask<String, String, String> implements Respondible, Observer {
	
	public int ServerResponseCode = 0;
	public String result = "";
	
	private List<File> files;
	private FragmentForm fragment;
	private String title;
	private ProgressDialog pDialog;
	private int cantidadCargar = 0;
	private long cantidadCargado = 0;
	private long lengthTotalFiles = 0;
	private String DNI = "";
	
	/*Region Construct*/
	public UploadFile(FragmentForm fragment,  String title, String dni){
		super();
		this.fragment = fragment;
		this.title = title;
		this.DNI = dni;
	}
	/*End Region*/
	
	
	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}
	
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(this.fragment.getActivity());
		pDialog.setMessage(title + " Por favor espere...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
		pDialog.show();
	}
	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		this.fragment.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				DialogComponent dlg = new DialogComponent(UploadFile.this.fragment.getActivity(), UploadFile.this, 
						DialogComponent.TIPO_DIALOGO.NEUTRAL, title, result);
				dlg.showDialog();
			}
		});
	}
	protected void onProgressUpdate (Float... valores) {
        int p = Math.round(100*valores[0]);
        pDialog.setProgress(p);
    }
	@Override
	protected String doInBackground(String... params) {
		
		
		cantidadCargar = 0;
		cantidadCargado = 0;	
		
		for(File file : files) {
			lengthTotalFiles = lengthTotalFiles + file.length(); 
		}
		
		for (File file : files) {
			String sourceFileUri = file.getAbsolutePath();
			int serverResponseCode = 0;
	        String fileName = sourceFileUri;
	        HttpURLConnection conn = null;
	        DataOutputStream dos = null;  
	        String lineEnd = "\r\n";
	        String twoHyphens = "--";
	        String boundary = "*****";
	        int bytesRead, bytesAvailable, bufferSize;
	        String texto = "";
	        byte[] buffer;
	        int maxBufferSize = 1 * 1024 * 1024; 
	        File sourceFile = new File(sourceFileUri); 
	        if (!sourceFile.isFile()) {
	        	 this.result = "No existe el archivo o las ruta no coinciden";
	        	 return result();
	        }
	        else
	        {
	             try {
	                 FileInputStream fileInputStream = new FileInputStream(sourceFile);
	                 URL url = new URL(App.URL_PUBLICADO);
	                 //URL url = new URL(App.URL_LOCAL);
	                 //URL url = new URL("");
	                 conn = (HttpURLConnection) url.openConnection();
	                 conn.setDoInput(true);
	                 conn.setDoOutput(true);
	                 conn.setUseCaches(false);
	                 conn.setRequestMethod("POST");
	                 conn.setRequestProperty("Connection", "Keep-Alive");
	                 conn.setRequestProperty("ENCTYPE", "multipart/form-data");
	                 conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
	                 dos = new DataOutputStream(conn.getOutputStream());	                 
	                 dos.writeBytes(twoHyphens + boundary + lineEnd);
	                 dos.writeBytes("Content-Disposition: form-data; name=\"userEvaluator\""+ lineEnd);
	                 dos.writeBytes(lineEnd);
	                 dos.writeBytes(this.DNI);
	                 dos.writeBytes(lineEnd);
	                 dos.writeBytes(twoHyphens + boundary + lineEnd);
	                 dos.writeBytes(twoHyphens + boundary + lineEnd); 
	                 dos.writeBytes("Content-Disposition: form-data; name=\"uploadFiles\";filename=\""+ fileName + "\"" + lineEnd);
	                 dos.writeBytes(lineEnd);
	                 bytesAvailable = fileInputStream.available(); 
	                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
	                 buffer = new byte[bufferSize];
	                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
	                 while (bytesRead > 0) {
	                   dos.write(buffer, 0, bufferSize);
	                   bytesAvailable = fileInputStream.available();
	                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
	                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	                   cantidadCargado = cantidadCargado + ((file.length() * 100 )/(lengthTotalFiles/2));
		               calcularPorcentajeProcesado();
	                  }
	                 dos.writeBytes(lineEnd);
	                 dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	                 
	                 InputStream lectura=conn.getInputStream();
	                 BufferedReader lecturaBuffer =new BufferedReader(new InputStreamReader(lectura));
	                 String linea="";
	                 while(linea!=null){
	                	 linea=lecturaBuffer.readLine();
	                	 texto=texto+(linea==null?"":linea);
	                 }
	                 JSONObject reader = new JSONObject(texto);
                     JSONObject resultado=reader.getJSONObject("resultado");
                     String mensaje=resultado.getString("mensaje");
	                 serverResponseCode = conn.getResponseCode();
//	                 String serverResponseMessage = conn.getResponseMessage();
//	                 Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode );
	                 this.ServerResponseCode = serverResponseCode;
	                 if(serverResponseCode == 200){
//	                	 this.result = result + "Archivo subido exitosamente : " + file.getName() + "\n";
	                	 this.result=result+mensaje+"\n";
	                 }
	                 else{
	                	 this.result=result+mensaje+"\n";
//	                	 this.result = result + "No se pudo subir el archivo : " + file.getName() + "\n";
	                 }
	                 if(serverResponseCode == 200){
	                	 file.delete();
	                 }
	                 fileInputStream.close();
	                 dos.flush();
	                 dos.close();
	                   
	            } catch (MalformedURLException ex) {
	            	this.result = "Upload file to server " + ex.getMessage() + ex;
	            	return this.result();
	            } catch (Exception e) {
	            	this.result = " Verificar Configuraciones \n"
	            			+ "1.- No existe Conexion ha Internet \n"
	            			+ "2.- Se perdio la comunicación con el Servidor de Archivos \n"
	            			+ "3.- El servidor se encuentra saturado \n"
	            			+ "Nota: Volver ha realizar la transferencia \n";
	            	return this.result();
	            }
	             
	         }
	        
		}
		
        
         return this.result();
	}
	private String result() {
		return this.result;
	}
	@Override
	public void update(Observable observable, Object data) {
		if (data == null) {
			return;
		}
		if (data instanceof Map) {
			Map<String, Integer> procesado = (Map<String, Integer>) data;
			this.cantidadCargado = Util.getInt(procesado.get("INSERTADO"));
			calcularPorcentajeProcesado();
	
		} else if (data instanceof XMLObject.BeansProcesados) {
			XMLObject.BeansProcesados bp = (BeansProcesados) data;
			this.cantidadCargado = bp.getProcesado();
			this.cantidadCargar = bp.getTotal();
			calcularPorcentajeProcesado();
			pDialog.setMax(this.cantidadCargar);
		}
	}
	private void calcularPorcentajeProcesado() {
		pDialog.incrementProgressBy((int)cantidadCargado);
		publishProgress(String.valueOf(cantidadCargado));
	}
	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccept() {
		// TODO Auto-generated method stub
		
	}
}	