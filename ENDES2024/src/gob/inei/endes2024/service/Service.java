 package gob.inei.endes2024.service;

import gob.inei.endes2024.common.App;
import gob.inei.endes2024.dao.MyDatabaseHelper;
import gob.inei.dnce.dao.SQLiteDAO;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class Service {

	protected SQLiteDAO sqliteDAO;
	private Context context;
	protected MyDatabaseHelper dbh;
	public static final String DATABASE_NAME = "bdendes"+App.ANIOPORDEFECTOSUPERIOR+"_produccion.db";

	public static final int DATABASE_VERSION = 29;


	public Service(Context ctx) {
		this.context=ctx;
		this.dbh = new MyDatabaseHelper(ctx, DATABASE_NAME, DATABASE_VERSION);
	}
	
	public SQLiteDatabase startTX() {
		return null;
	}
		
	public void commitTX(SQLiteDatabase dbTX) {
		
	}
	
	public void endTX(SQLiteDatabase dbTX) {
		
	}
	
}
