package gob.inei.endes2024.service;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.Seccion03DAO;
import gob.inei.endes2024.model.Seccion03;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Seccion03Service  extends Service {

	public static Seccion03Service INSTANCE = null;
	public Seccion03Service(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	public static Seccion03Service getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion03Service(ctx);
		}
		return INSTANCE;
	}
	
	private Seccion03DAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = Seccion03DAO.getInstance(this.dbh);
		}
		return (Seccion03DAO)sqliteDAO;
	}
	public boolean saveOrUpdate(Seccion03 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion03 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public List<Seccion03> getPersonas(Integer id) {
		return getDao().getPersonas(id);
	}
	
	public Seccion03 getPersona(Integer id, Integer hogar,Integer persona_id) {
		return getDao().getPersona(id, hogar, persona_id);
	}
	
	public Seccion03 getPersona(Integer id, Integer hogar,Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getPersona(id, hogar, persona_id, secciones);
	}
	
	public boolean borrarPersona(Seccion03 bean) {
		return getDao().BorrarPersona(bean);
	}
	
	public boolean BorrarPersonaPregunta(Seccion03 bean) {
		return getDao().BorrarPersonaPregunta(bean);
	}
	
	public List<Seccion03> getBeneficiariosBeca18(Integer id, Integer hogar_id,Integer pregunta,SeccionCapitulo... secciones)
	{
		return getDao().getListadoPregunta92(id, hogar_id, pregunta,secciones);
	}
			
	public Seccion03 getBeneficiario(Integer id, Integer hogar_id,Integer persona_id_orden,Integer pregunta,SeccionCapitulo... secciones)
	{
		return getDao().getBeneficiario92(id, hogar_id,persona_id_orden, pregunta,secciones);
	}
	public List<Seccion03> getPersonasAModificar(Integer id,Integer hogar_id,Integer persona_id_orden, SeccionCapitulo... secciones){
		return getDao().getPersonasAModificar(id,hogar_id,persona_id_orden,secciones);
	}
	public boolean ModificarNumerodeOrdenSeccion03(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer pregunta_id){
		return getDao().ModificarNumerodeOrdenSeccion03(id, hogar_id, persona_id_nuevo, persona_id_anterior,pregunta_id);
	}
//	public List<Seccion03> Listado(Integer id, Integer hogar_id, Integer pregunta, SeccionCapitulo... secciones)
//	{
//		return getDao().getListadoPrueba(id, hogar_id, pregunta, secciones);
//	}
	public List<Seccion03> getListadoCovid(Integer id,Integer hogar_id,Integer pregunta, SeccionCapitulo... secciones){
		return getDao().getListadoCovid(id, hogar_id, pregunta, secciones);
	}
}
