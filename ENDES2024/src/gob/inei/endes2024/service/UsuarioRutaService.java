package gob.inei.endes2024.service;
import gob.inei.endes2024.dao.UsuarioRutaDAO;
import gob.inei.endes2024.model.UsuarioRuta;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class UsuarioRutaService extends Service {

	public static UsuarioRutaService INSTANCE = null;
	
	private UsuarioRutaService(Context ctx) {
		super(ctx);
	}
	
	public static UsuarioRutaService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new UsuarioRutaService(ctx);
		}
		return INSTANCE;
	}
	
	private UsuarioRutaDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = UsuarioRutaDAO.getInstance(this.dbh);
		}
		return (UsuarioRutaDAO)sqliteDAO;
	}
	
	public boolean saveOrUpdate(UsuarioRuta bean, SQLiteDatabase dbTX) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

}
