package gob.inei.endes2024.service;

import java.sql.SQLException;
import java.util.List;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.HogarDAO;
import gob.inei.endes2024.dao.PersonasDAO;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class PersonasService extends Service{
	public static PersonasService INSTANCE = null;

	public PersonasService(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	public static PersonasService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new PersonasService(ctx);
		}
		return INSTANCE;
	}
	
	private PersonasDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = PersonasDAO.getInstance(this.dbh);
		}
		return (PersonasDAO)sqliteDAO;
	}
	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public List<Seccion01> getPersonas(Integer id) {
		return getDao().getPersonas(id);
	}
	
	public Seccion01 getPersona(Integer id, Integer hogar) {
		return getDao().getPersona(id, hogar);
	}
	public Seccion01 getHogar(Integer id, Integer hogar, SeccionCapitulo... secciones) {
		return getDao().getPersona(id, hogar,secciones);
	}
	
}
