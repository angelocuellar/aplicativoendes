package gob.inei.endes2024.service;

import gob.inei.endes2024.dao.UbigeoDAO;
import gob.inei.endes2024.model.Ubigeo;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class UbigeoService extends Service {

	public static UbigeoService INSTANCE = null;
	
	private UbigeoService(Context ctx) {
		super(ctx);
	}
	
	public static UbigeoService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new UbigeoService(ctx);
		}
		return INSTANCE;
	}
	
	private UbigeoDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = UbigeoDAO.getInstance(this.dbh);
		}
		return (UbigeoDAO)sqliteDAO;
	}
	
	public boolean saveOrUpdate(Ubigeo bean) {
		return getDao().saveOrUpdate(bean);
	}
	
	public boolean saveOrUpdate(Ubigeo bean, SQLiteDatabase dbTX) {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public List<Ubigeo> getDepartamentos() {
		return getDao().getDepartamentos();
	}
	
	public Ubigeo getDepartamento(String ccdd) {
		return getDao().getDepartamento(ccdd);
	}
	
	public List<Ubigeo> getProvincias(String ccdd) {
		return getDao().getProvincias(ccdd);
	}
	
	public Ubigeo getProvincia(String ccdd, String ccpp) {
		return getDao().getProvincia(ccdd, ccpp);
	}
	
	public List<Ubigeo> getDistritos(String ccdd, String ccpp) {
		return getDao().getDistritos(ccdd, ccpp);		
	}
	
	public Ubigeo getDistrito(String ccdd, String ccpp, String ccdi) {
		return getDao().getDistrito(ccdd, ccpp, ccdi);
	}
	public List<Ubigeo> getDistritoByccdd(String ccdd){
		return getDao().getDistritoByccdd(ccdd);
	}
}
