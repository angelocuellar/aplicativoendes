package gob.inei.endes2024.service;

import gob.inei.endes2024.dao.ImpExpDAO;

import java.util.List;
import java.util.Map;

import android.content.Context;

public class ImpExpService  extends Service{
	
	private static ImpExpService INSTANCE=null;
	
	

	public ImpExpService(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	
    public static ImpExpService getInstance(Context ctx) {
        if (INSTANCE == null) {
               INSTANCE = new ImpExpService(ctx);
        }
        return INSTANCE;
  }
    private ImpExpDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = ImpExpDAO.getInstance(this.dbh);
		}
		return (ImpExpDAO)sqliteDAO;
	}
	
    public List<Map<String, Object>> getRegistros(String tableName, String where, String...wherevalues){
    	return  getDao().getRegistros(tableName, where, wherevalues);
    }
    

}
