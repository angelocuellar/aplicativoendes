package gob.inei.endes2024.service;
import gob.inei.endes2024.dao.SegmentacionDAO;
import gob.inei.endes2024.model.Segmentacion;
import gob.inei.endes2024.model.Segmentacion.SegmentacionFiltro;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class SegmentacionService extends Service {

public static SegmentacionService INSTANCE = null;
	
	private SegmentacionService(Context ctx) {
		super(ctx);
	}
	
	public static SegmentacionService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new SegmentacionService(ctx);			
		}
		return INSTANCE;
	}
	
	private SegmentacionDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = SegmentacionDAO.getInstance(this.dbh);
		}
		return (SegmentacionDAO)sqliteDAO;
	}
	
	public SQLiteDatabase startTX() {
		return getDao().startTX();
	}
		
	public void commitTX(SQLiteDatabase dbTX) {
		getDao().commitTX(dbTX);
	}
	
	public void endTX(SQLiteDatabase dbTX) {
		getDao().endTX(dbTX);
	}
	
	public List<SegmentacionFiltro> getAnios() {
		return getDao().getAnios();
	}
	
	public List<SegmentacionFiltro> getMes(Integer anio) {
		return getDao().getMes(anio);
	}
	
//	public List<SegmentacionFiltro> getPeriodos(Integer anio, String mes) {
//		return getDao().getPeriodos(anio, mes);
//	}
	
//	public List<SegmentacionFiltro> getConglomerados(Integer anio, String mes, Integer periodo) {
//		return getDao().getConglomerados(anio, mes, periodo);
//	}
	
	public List<SegmentacionFiltro> getConglomerados(Integer periodo) {
		return getDao().getConglomerados( periodo);
	}
	public boolean getConglomeradoTieneAsignado(String conglomerado){
		return getDao().getConglomeradoTieneAsignado(conglomerado);
	}
	public boolean saveOrUpdate(Segmentacion bean, SQLiteDatabase dbTX) {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public Segmentacion getSegmentacion(Integer anio, String mes, Integer periodo, String conglomerado) {
		return getDao().getSegmentacion(anio, mes, periodo, conglomerado);
	}
	public List<SegmentacionFiltro> getPeriodos() {
		return getDao().getPeriodos();
	}
	public Segmentacion getSegmentacion(Integer periodo, String conglomerado) {
		return getDao().getSegmentacion( periodo, conglomerado);
	}
}