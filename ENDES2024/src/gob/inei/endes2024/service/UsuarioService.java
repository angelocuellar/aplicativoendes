package gob.inei.endes2024.service;
import gob.inei.endes2024.dao.UsuarioDAO;
import gob.inei.endes2024.model.Usuario;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class UsuarioService extends Service {

	public static UsuarioService INSTANCE = null;
	
	private UsuarioService(Context ctx) {
		super(ctx);
	}
	
	public static UsuarioService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new UsuarioService(ctx);
		}
		return INSTANCE;
	}
	
	private UsuarioDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = UsuarioDAO.getInstance(this.dbh);
		}
		return (UsuarioDAO)sqliteDAO;
	}
	
	public Usuario getUsuario(String login, String clave) {
		return getDao().getUsuario(login, clave);
	}
	
	public boolean saveOrUpdate(Usuario bean) {
		return getDao().saveOrUpdate(bean);
	}
	
	public boolean saveOrUpdate(Usuario bean, SQLiteDatabase dbTX) {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public List<Usuario> getAuxiliares(Integer id) {
		return getDao().getAuxiliares(id);
	}
	
	public List<Usuario> getAntropo(Integer id) {
		return getDao().getAntropo(id);
	}
//	public Usuario getUsuarioById(Integer id){
//		return getDao().getUsuarioById(id);
//	}
	public Usuario getUsuarioById(String usuario){
		return getDao().getUsuarioById(usuario);
	}
}
