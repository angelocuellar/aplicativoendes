package gob.inei.endes2024.service;


import gob.inei.endes2024.dao.MarcoDAO;
import gob.inei.endes2024.model.Endes_omisiones;
import gob.inei.endes2024.model.Marco;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class MarcoService extends Service {

	public static MarcoService INSTANCE = null;
	
	private MarcoService(Context ctx) {
		super(ctx);
	}
	
	public static MarcoService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new MarcoService(ctx);
		}
		return INSTANCE;
	}
	
	private MarcoDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = MarcoDAO.getInstance(this.dbh);
		}
		return (MarcoDAO)sqliteDAO;
	}
	
	public SQLiteDatabase startTX() {
		return getDao().startTX();
	}

	public void commitTX(SQLiteDatabase dbTX) {
		getDao().commitTX(dbTX);
	}

	public void endTX(SQLiteDatabase dbTX) {
		getDao().endTX(dbTX);
	}	
	
	public boolean save(Marco bean, SQLiteDatabase dbTX) throws SQLException {
		return getDao().save(bean, dbTX);
	}
	
	public List<Marco> getMarco(Integer anio, String mes, Integer periodo, String conglomerado) {
		return getDao().getMarco(anio, mes, periodo, conglomerado);
	}
	
	public List<Marco> getVivsReem(String conglomerado, String mes, Integer id) {
		return getDao().getVivsReem(conglomerado, mes, id);
	}
	
	public List<Marco> getMarcoTotal(Integer anio, String mes, String conglomerado) {
		return getDao().getMarcoTotal(anio, mes, conglomerado);
	}
	
	public Marco getMarco(Integer id) {
		return getDao().getMarco(id);
	}
	
	public boolean saveOrUpdate(Marco bean) throws SQLException {
		return this.saveOrUpdate(bean, null);
	}	

	public boolean saveOrUpdate(Marco bean, SQLiteDatabase dbTX) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}	
	
	public int getTotalViendas(int anio, String mes, String conglomerado) {
		return getDao().getTotalViendas(anio, mes, conglomerado);
	}
	public List<Marco> getMarco(Integer periodo, String conglomerado) {
		return getDao().getMarco(periodo, conglomerado);
	}
	public List<Marco> getMarcoAsignacion(Integer periodo, String conglomerado){
		return getDao().getMarcoAsignacion(periodo, conglomerado);
	}
	public int getTotalViendas(Integer periodo, String conglomerado) {
		return getDao().getTotalViendas(periodo, conglomerado);
	}
	
	public List<Marco> getMarcoTotal(Integer periodo, String conglomerado) {
		return getDao().getMarcoTotal(periodo, conglomerado);
	}
	
	public int getTotalViendasAdi(Integer periodo, String conglomerado) {
		return getDao().getTotalViendasAdi(periodo, conglomerado);
	}
	
	public boolean borrarHogares(Marco bean) {
		return this.borrarHogares(bean, null);
	}
	
	private boolean borrarHogares(Marco bean, SQLiteDatabase dbTX) {
		return getDao().borrarHogares(bean,dbTX);
	}
	
	public List<Marco> getMarcoCobertura(Integer periodo, String conglomerado) {
		return getDao().getMarcoCobertura(periodo, conglomerado);
	}
	public List<Endes_omisiones> getOmisiones(String conglomerado){
		return getDao().getOmisiones(conglomerado);
	}
}