package gob.inei.endes2024.service;
import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.HogarDAO;
import gob.inei.endes2024.model.Hogar;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class HogarService extends Service {

	public static HogarService INSTANCE = null;
	
	private HogarService(Context ctx) {
		super(ctx);
	}
	
	public static HogarService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new HogarService(ctx);
		}
		return INSTANCE;
	}
	
	private HogarDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = HogarDAO.getInstance(this.dbh);
		}
		return (HogarDAO)sqliteDAO;
	}
	
	public boolean saveOrUpdate(Hogar bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	
	public List<Hogar> getHogares(Integer id,SeccionCapitulo... secciones) {
		return getDao().getHogares(id,secciones);
	}
	
	public Hogar getHogar(Integer id, Integer hogar) {
		return getDao().getHogar(id, hogar);
	}
	
	public Hogar getHogar(Integer id, Integer hogar, SeccionCapitulo... secciones) {
		return getDao().getHogar(id, hogar, secciones);
	}
	
	public boolean borrarHogar(Hogar bean) {
		return this.borrarHogar(bean, null);
	}
	
	private boolean borrarHogar(Hogar bean, SQLiteDatabase dbTX) {
		return getDao().borrarHogar(bean,dbTX);
	}
	public Hogar getHogarId(Integer id,Integer hogar_id, SeccionCapitulo... secciones){
		return getDao().getHogarId(id, hogar_id, secciones);
	}
//	public boolean saveOrUpdate(Hogar bean, String... campos) throws java.sql.SQLException {
//		return getDao().saveOrUpdate(bean, -1, -1, campos);
//	}
}
