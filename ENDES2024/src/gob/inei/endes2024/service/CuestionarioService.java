package gob.inei.endes2024.service;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.dnce.dao.xml.XMLDataObject;
import gob.inei.endes2024.dao.CuestionarioDAO;
import gob.inei.endes2024.dao.CuestionarioDAO.CounterObservable;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CAP04_07;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CICALENDARIO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_COL04;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL01_03;
import gob.inei.endes2024.model.CICALENDARIO_TRAMO_COL4;
import gob.inei.endes2024.model.CIHERMANOS;
import gob.inei.endes2024.model.CISECCION_01_03;
import gob.inei.endes2024.model.CISECCION_02;
import gob.inei.endes2024.model.CISECCION_02T;
import gob.inei.endes2024.model.CISECCION_04;
import gob.inei.endes2024.model.CISECCION_04A;
import gob.inei.endes2024.model.CISECCION_04B;
import gob.inei.endes2024.model.CISECCION_04B2;
import gob.inei.endes2024.model.CISECCION_04B_TARJETA;
import gob.inei.endes2024.model.CISECCION_04DIT;
import gob.inei.endes2024.model.CISECCION_04DIT_02;
import gob.inei.endes2024.model.CISECCION_05;
import gob.inei.endes2024.model.CISECCION_05_07;
import gob.inei.endes2024.model.CISECCION_08;
import gob.inei.endes2024.model.CISECCION_09;
import gob.inei.endes2024.model.CISECCION_10_01;
import gob.inei.endes2024.model.CISECCION_10_02;
import gob.inei.endes2024.model.CISECCION_10_03;
import gob.inei.endes2024.model.CISECCION_10_04;
import gob.inei.endes2024.model.CSSECCION_08;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Caratula;
import gob.inei.endes2024.model.DISCAPACIDAD;
import gob.inei.endes2024.model.Endes_Resultado_vivienda;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.IMIGRACION;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Individual;
import gob.inei.endes2024.model.MIGRACION;
import gob.inei.endes2024.model.MORTALIDAD;
import gob.inei.endes2024.model.Marco;
import gob.inei.endes2024.model.ReportDatesofBirth;
import gob.inei.endes2024.model.ReporteHMS;
import gob.inei.endes2024.model.Salud;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class CuestionarioService extends Service {
	private static CuestionarioService INSTANCE = null;

	private CuestionarioService(Context ctx) {
		super(ctx);
	}

	public static CuestionarioService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new CuestionarioService(ctx);
		}
		return INSTANCE;
	}

	private CuestionarioDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = CuestionarioDAO.getInstance(this.dbh);
		}
		return (CuestionarioDAO) sqliteDAO;
	}

	// MODEL : Caratula
	public Caratula getCaratula(Integer id) {
		return getDao().getCaratula(id);
	}

	public List<Caratula> getCaratulas(Integer id) {
		return getDao().getCaratulas(id);
	}

	public Caratula getCaratula(Integer id, SeccionCapitulo... secciones) {
		return getDao().getCaratula(id, secciones);
	}

	public Caratula getCaratula(Integer id, Integer hogar) {
		return getDao().getCaratula(id, hogar);
	}

	// MODEL : Hogar
	public Hogar getHogar(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		return getDao().getHogar(id, hogar_id, secciones);
	}
	public Integer getViolenHogar(Integer id, Integer hogar_id) {
		return getDao().getViolenHogar(id, hogar_id);
	}
	public Hogar getHogarByInformante(Integer id, Integer hogar_id,SeccionCapitulo... secciones)
	{
		return getDao().getHogarConInformante(id, hogar_id, secciones);
	}
	public Incentivos getIncentivos(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
		return getDao().getIncentivos(id,hogar_id,secciones);
	}
	
	public List<Hogar> getHogars(Integer id, Integer hogar_id,
			SeccionCapitulo... secciones) {
		return getDao().getHogars(id, hogar_id, secciones);
	}

	public boolean saveOrUpdate(Hogar bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean saveOrUpdate(Incentivos bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean saveOrUpdate(Caratula bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean saveOrUpdate(MORTALIDAD bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}

	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public boolean saveOrUpdate(Caratula bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public boolean saveOrUpdate(Salud bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public boolean saveOrUpdate(CAP04_07 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public boolean saveOrUpdate(Hogar bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

	public List<Hogar> getHogarList(Integer id) {
		return getDao().getHogarList(id);
	}

	public List<Hogar> getHogares(Integer id) {
		return getDao().getHogares(id);
	}
	public List<Hogar> getHogaresById(Integer id) {
		return getDao().getHogaresById(id);
	}
	public List<Marco> getViviendas(Integer id,String conglomerado){
		return getDao().getViviendas(id, conglomerado);
	}
	// MODEL : Seccion04_05
	public Seccion04_05 getSeccion04_05(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05(id, hogar_id, persona_id, secciones);
	}

	public List<Seccion04_05> getSeccion04_05s(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05s(id, hogar_id, persona_id, secciones);
	}

	public boolean saveOrUpdate(Seccion04_05 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}

	public boolean saveOrUpdate(Seccion04_05 bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public boolean saveOrUpdate(Auditoria bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

	public List<Seccion04_05> getSeccion04_05List(Integer id) {
		return getDao().getSeccion04_05List(id);
	}
// MODEL :Individual
	public boolean  saveOrUpdate(CISECCION_01_03 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public CISECCION_01_03 getCISECCION_01_03(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_01_03(id,hogar_id,persona_id,secciones);
	}
	public boolean  saveOrUpdate(CISECCION_04 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}

	public boolean  saveOrUpdate(CISECCION_04A individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}

//	C2SECCION_04B
//	public boolean  saveOrUpdate(C2SECCION_04B individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return getDao().saveOrUpdate(individual,secciones);
//	}
//	public C2SECCION_04B getCISECCION_04B(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return getDao().getCISECCION_04B(id,hogar_id,persona_id,secciones);
//	}
////	CISECCION_04B_2
//	public boolean  saveOrUpdate(CISECCION_04B_2 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return getDao().saveOrUpdate(individual,secciones);
//	}
//	public CISECCION_04B_2 getCISECCION_04B_2(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return getDao().getCISECCION_04B_2(id,hogar_id,persona_id,secciones);
//	}
//	CISECCION_05_07
	public boolean  saveOrUpdate(CISECCION_05_07 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public CISECCION_05_07 getCISECCION_05_07(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_05_07(id,hogar_id,persona_id,secciones);
	}
	//CISECCION_08_09
//	public boolean  saveOrUpdate(CISECCION_08_09 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return getDao().saveOrUpdate(individual,secciones);
//	}
//	public CISECCION_08_09 getCISECCION_08_09(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return getDao().getCISECCION_08_09(id,hogar_id,persona_id,secciones);
//	}
//	CISECCION_10_01
	public boolean  saveOrUpdate(CISECCION_10_01 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
//	public CISECCION_10_01 getCISECCION_10_01(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return getDao().getCISECCION_10_01(id,hogar_id,persona_id,secciones);
//	}
//	CISECCION_10_02
	public boolean  saveOrUpdate(CISECCION_10_02 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
//	public CISECCION_10_02 getCISECCION_10_02(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
//		return getDao().getCISECCION_10_02(id,hogar_id,persona_id,secciones);
//	}
//	CIVISITA
//	public boolean  saveOrUpdate(CIVISITA individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
//		return getDao().saveOrUpdate(individual,secciones);
//	}
	// MODEL : Seccion01
	public Seccion01 getSeccion01(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion01(id, hogar_id, persona_id, secciones);
	}

	public List<Seccion01> getSeccion01s(Integer id, Integer hogar_id,
			Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion01s(id, hogar_id, persona_id, secciones);
	}	
		
	public List<Seccion01> getSeccion01PosiblesBeneficiarios(Integer id, Integer hogar_id, Integer pregunta_id,
			Integer edad_min,Integer edad_max, SeccionCapitulo... secciones) {
		return getDao().getSeccion01PosiblesBeneficiarios(id, hogar_id,pregunta_id,edad_min,edad_max, secciones);
	}

	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}

	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(MORTALIDAD bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(Seccion03 bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... campos) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, campos);
	}
	public boolean saveOrUpdate(MORTALIDAD bean, SQLiteDatabase dbTX,
			SeccionCapitulo... campos) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, campos);
	}

	public List<Seccion01> getSeccion01List(Integer id) {
		return getDao().getSeccion01List(id);
	}

	public List<Seccion01> GetSeccion01ListByMarcoyHogar(Integer id, Integer hogar_id,SeccionCapitulo... secciones)
	{
		return getDao().getSeccion01ListbyMarcoyHogar(id, hogar_id,secciones);
	}
	public List<Seccion01> getSeccion01ListDiscapacidad(Integer id, Integer hogar_id,SeccionCapitulo... secciones)
	{
		return getDao().getSeccion01ListDiscapacidad(id, hogar_id,secciones);
	}
	public List<Seccion01> getDiscapacidadParaIndividual(Integer id, Integer hogar_id,Integer persona_id,Integer cuestionario_id, SeccionCapitulo...secciones){
		return getDao().getDiscapacidadParaIndividual(id, hogar_id, persona_id,cuestionario_id, secciones);
	}
	public List<Seccion01> getListadoSeccion8Salud(Integer id, Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getDao().getListadoSeccion8Salud(id, hogar_id, secciones);
	}
	
	//MODEL: MEF
	public List<Seccion01> getListadoMEF(Integer id, Integer hogar_id, SeccionCapitulo... secciones)
	{
		return getDao().getListadoMEF(id, hogar_id, secciones);
	}
	public boolean getListadoMEF(Integer id, Integer hogar_id, Integer persona_id)
	{
		return getDao().getListadoMEF(id, hogar_id,persona_id);
	}
	public boolean BorrarMEF(Individual bean) {
		return getDao().BorrarMEF(bean);
	}
	public boolean EliminarMEF(Seccion01 bean) {
		return getDao().EliminarMEF(bean);
	}
	

	public List<CSSECCION_08> getListadoSeccion8(Integer id, Integer hogar_id,SeccionCapitulo... secciones){
		return getDao().getListadoSeccion8(id, hogar_id, secciones);
	}
	// MODEL : Visita
	public Visita getVisita(Integer id, Integer hogar_id, Integer nro_visita,SeccionCapitulo... secciones) {
		return getDao().getVisita(id, hogar_id, nro_visita, secciones);
	}
	
	public Visita_Viv getVisita_Viv(Integer id, Integer nro_visita,SeccionCapitulo... secciones) {
		return getDao().getVisita_Viv(id, nro_visita, secciones);
	}
	
	public List<Visita> getVisitas(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
		return getDao().getVisitas(id, hogar_id, secciones);
	}
	public List<Visita_Viv> getVisitas_Viv(Integer id,SeccionCapitulo... secciones) {
		return getDao().getVisitas_Viv(id, secciones);
	}

	public boolean saveOrUpdate(Visita bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean getCuestionarioDelHogarCompletado(Integer id, Integer hogar_id)
	{
		return getDao().getCuestionarioDelHogarCompletado(id, hogar_id);
	}
	public boolean getSeccion3Completado(Integer id, Integer hogar_id)
	{
		return getDao().getSeccion3Completado(id, hogar_id);
	}
	public boolean getCuestionarioDelSaludCompletado(Integer id, Integer hogar_id,Integer persona_id)
	{
		return getDao().getCuestionarioDelSaludCompletado(id, hogar_id,persona_id);
	}
	public boolean getRegistroBucalCompletado(Integer id, Integer hogar_id)
	{
		return getDao().getRegistroBucalCompletado(id, hogar_id);
	}
	public boolean getCuestionarioDelSalud01_07Completado(Integer id, Integer hogar_id,Integer persona_id)
	{
		return getDao().getCuestionarioDelSalud01_07Completado(id, hogar_id,persona_id);
	}
	public boolean getCuestionarioDelSaludComboCompletado(Integer id, Integer hogar_id,Integer persona_id)
	{
		return getDao().getCuestionarioDelSaludComboCompletado(id, hogar_id,persona_id);
	}
	public boolean getCuestionarioDelSaludVacio(Integer id, Integer hogar_id,Integer persona_id)
	{
		return getDao().getCuestionarioDelSaludVacio(id, hogar_id,persona_id);
	}

	public boolean saveOrUpdate(Visita bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public boolean saveOrUpdate(Visita_Viv bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public boolean saveOrUpdate(Incentivos bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

	public List<Visita> getVisitaList(Integer id) {
		return getDao().getVisitaList(id);
	}

	// MODEL : Vivienda
	public Marco getVivienda(Integer id, SeccionCapitulo... secciones) {
		return getDao().getVivienda(id, secciones);
	}

	public List<Marco> getViviendas(Integer id, SeccionCapitulo... secciones) {
		return getDao().getViviendas(id, secciones);
	}

	public boolean saveOrUpdate(Marco bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}

	public boolean saveOrUpdate(Marco bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

	public List<Marco> getViviendaList(Integer id) {
		return getDao().getViviendaList(id);
	}

	public boolean save(SQLiteDatabase dbTX, XMLDataObject dataObjects,
			CounterObservable contadorObserver) throws java.sql.SQLException {
		return getDao().save(dbTX, dataObjects, contadorObserver);
	}

	public boolean borrarCuestionario(String tablename,Integer id, SQLiteDatabase dbTX)
			throws SQLException {
		return getDao().borrarCuestionario(tablename,id, dbTX);
	}
	public boolean existeGPS(String latitud, String longitud, String omision) {
		return getDao().existeGPS(latitud, longitud, omision);
	}
	// OTROS
	public boolean saveOrUpdate(SQLiteDatabase dbTX, XMLDataObject dataObjects,
			CounterObservable contadorObserver) throws java.sql.SQLException {
		return getDao().saveOrUpdate(dbTX, dataObjects, contadorObserver);
	}
	
	
	
	//ADD 07122015
	//MODEL :  CAP01_03 
	public Salud getCAP01_03(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) { 
		return getDao().getCAP01_03(id,hogar_id,persona_id, secciones); 
	} 
	public List<Salud> getCAP01_03s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		return getDao().getCAP01_03s(id,hogar_id,persona_id,secciones); 
	} 
	public boolean saveOrUpdate(Salud bean, SeccionCapitulo... secciones) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, secciones); 
	} 
	public boolean saveOrUpdate(Salud bean, SQLiteDatabase dbTX) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, dbTX); 
	} 
	public List<Salud> getCAP01_03List(Integer id) { 
		return getDao().getCAP01_03List(id); 
	} 
	//MODEL :  CAP04_07 
	public CAP04_07 getCAP04_07(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) { 
		return getDao().getCAP04_07(id,hogar_id,persona_id, secciones); 
	} 
	public List<CAP04_07> getCAP04_07s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		return getDao().getCAP04_07s(id,hogar_id,persona_id,secciones); 
	} 
	public boolean saveOrUpdate(CAP04_07 bean, SeccionCapitulo... secciones) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, secciones); 
	} 
	public boolean saveOrUpdate(CAP04_07 bean, SQLiteDatabase dbTX) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, dbTX); 
	} 
	public List<CAP04_07> getCAP04_07List(Integer id) { 
		return getDao().getCAP04_07List(id); 
	} 
	//MODEL :  CAP08_09 
	public CSSECCION_08 getCAP08_09(Integer id,Integer hogar_id,Integer persona_id,Integer persona_id_ninio ,SeccionCapitulo... secciones) { 
		return getDao().getCAP08_09(id,hogar_id,persona_id,persona_id_ninio, secciones); 
	} 
	public CSSECCION_08 getCSSeccion08(Integer id,Integer hogar_id,Integer persona_id_ninio, SeccionCapitulo... secciones) {
		return getDao().getCSSeccion08(id, hogar_id, persona_id_ninio, secciones);
	}
	public List<CSSECCION_08> getCAP08_09s(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		return getDao().getCAP08_09s(id,hogar_id,persona_id,secciones); 
	} 
	public boolean saveOrUpdate(CSSECCION_08 bean, SeccionCapitulo... secciones) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, secciones); 
	} 
	public boolean Update(CSSECCION_08 bean,SeccionCapitulo... secciones) throws java.sql.SQLException {
		return getDao().Update(bean, secciones);
	}
	public boolean saveOrUpdate(CSSECCION_08 bean, SQLiteDatabase dbTX) throws java.sql.SQLException { 
		return getDao().saveOrUpdate(bean, dbTX); 
	}
	public List<CSSECCION_08> getCAP08_09List(Integer id) {
		return getDao().getCAP08_09List(id); 
	}
	public boolean saveOrUpdate(Individual bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CARATULA_INDIVIDUAL bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CICALENDARIO_COL01_03 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CICALENDARIO_COL04 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CICALENDARIO_TRAMO_COL4 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_10_01 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_10_02 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_10_03 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_10_04 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_09 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_08 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_05 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_05_07 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_04B2 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
//	public boolean saveOrUpdate(CISECCION_04DIT bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
//		return getDao().saveOrUpdate(bean, dbTX);
//	}
	public boolean saveOrUpdate(CISECCION_04DIT_02 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_04B_TARJETA bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_04B bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_04A bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_02T bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_02 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(CISECCION_01_03 bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	//ADD07122015
		//MODEL :  CAPVISITA 
		public CSVISITA getCAPVISITA(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
			return getDao().getCAPVISITA(id, hogar_id,secciones);
		}
		public CSVISITA getCAPVISITASALUD(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
			return getDao().getCAPVISITASALUD(id,hogar_id,persona_id,secciones);
		}
		public CSVISITA getCAPVISITA(Integer id, Integer hogar_id,Integer nro_visita,Integer persona_id,SeccionCapitulo... secciones) {
			return getDao().getCAPVISITA(id, hogar_id,nro_visita,persona_id,secciones);
		}
		public boolean getExisteSeccion01_03(Integer id,Integer hogar_id,Integer persona_id){
			return getDao().getExisteSeccion01_03(id, hogar_id, persona_id);
		}
		public boolean getExisteSeccion04_07(Integer id,Integer hogar_id,Integer persona_id){
			return getDao().getExisteSeccion04_07Salud(id, hogar_id, persona_id);
		}
		public List<CSVISITA> getCAPVISITAs(Integer id, Integer hogar_id,SeccionCapitulo... secciones) {
			return getDao().getCAPVISITAs(id, hogar_id,	secciones);
		}
		public List<CSVISITA> getCAPVISITAs2(Integer id, Integer hogar_id, Integer persona_id,SeccionCapitulo... secciones) {
			return getDao().getCAPVISITAs2(id, hogar_id, persona_id, secciones);
		}
		public CSVISITA getVisitaSaludCompletada(Integer id,Integer hogar_id,SeccionCapitulo... secciones){
			return getDao().getVisitaSaludCompletada(id, hogar_id, secciones);
		}
		public boolean saveOrUpdate(CSVISITA bean, SeccionCapitulo... secciones)
				throws java.sql.SQLException {
			return getDao().saveOrUpdate(bean, secciones);
		}

		public boolean saveOrUpdate(CSVISITA bean, SQLiteDatabase dbTX)
				throws java.sql.SQLException {
			return getDao().saveOrUpdate(bean, dbTX);
		}

		public List<CSVISITA> getCAPVISITAList(Integer id) {
			return getDao().getCAPVISITAList(id);
		}

	public List<CSSECCION_08> getListadoNiniosSeccion08(Integer id,Integer hogar_id,  SeccionCapitulo... secciones){
		return getDao().getListadoNiniosSeccion08(id, hogar_id, secciones);
	}
	public boolean getExistealMenosUnNinio(Integer id, Integer hogar_id) {
		return getDao().getExistealMenosUnNinio(id,hogar_id);
	}
	public List<CSSECCION_08> getListadoNiniosSeccion08Preg840(Integer id,Integer hogar_id,  SeccionCapitulo... secciones)
	{
		return getDao().getListadoNiniosSeccion08Preg840(id, hogar_id, secciones);
	}
	public boolean getRegistroCompletadoSaludSeccion8(Integer id,Integer hogar_id)
	{
		return getDao().getRegistroCompletadoSaludSeccion8(id, hogar_id);
	}	
	public boolean getListadoNiniosSeccion08Botones(Integer id,Integer hogar_id)
	{
		return getDao().getListadoNiniosSeccion08Botones(id, hogar_id);
	}
	
	//Model Individual
	public boolean saveOrUpdate(Individual bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public List<Individual> getListadoMefById(Integer id) {
		return getDao().getListadoMefById(id);
	}
	public List<CARATULA_INDIVIDUAL> getListadoCarMefById(Integer id) {
		return getDao().getListadoCarMefById(id);
	}
	public List<CICALENDARIO_COL01_03> getListadoCalen01_03ById(Integer id) {
		return getDao().getListadoCalen01_03ById(id);
	}
	public List<CICALENDARIO_COL04> getListadoCalen04ById(Integer id) {
		return getDao().getListadoCalen04ById(id);
	}
	public List<CICALENDARIO_TRAMO_COL01_03> getListadoCalenTramoById(Integer id) {
		return getDao().getListadoCalenTramoById(id);
	}
	public List<CICALENDARIO_TRAMO_COL4> getListadoCalenTramo4ById(Integer id) {
		return getDao().getListadoCalenTramo4ById(id);
	}
	public List<CISECCION_10_01> getListadoCI_10_01ById(Integer id) {
		return getDao().getListadoCI_10_01ById(id);
	}
	public List<CISECCION_10_02> getListadoCI_10_02ById(Integer id) {
		return getDao().getListadoCI_10_02ById(id);
	}
	public List<CISECCION_10_03> getListadoCI_10_03ById(Integer id) {
		return getDao().getListadoCI_10_03ById(id);
	}
	public List<CISECCION_10_04> getListadoCI_10_04ById(Integer id) {
		return getDao().getListadoCI_10_04ById(id);
	}
	public List<CISECCION_09> getListadoCI_09ById(Integer id) {
		return getDao().getListadoCI_09ById(id);
	}
	public List<CISECCION_08> getListadoCI_08ById(Integer id) {
		return getDao().getListadoCI_08ById(id);
	}
	public List<CISECCION_05> getListadoCI_05ById(Integer id) {
		return getDao().getListadoCI_05ById(id);
	}
	public List<CISECCION_05_07> getListadoCI_05_07ById(Integer id) {
		return getDao().getListadoCI_05_07ById(id);
	}
	public List<CISECCION_04B2> getListadoCI_04B2ById(Integer id) {
		return getDao().getListadoCI_04B2ById(id);
	}
//	public List<CISECCION_04DIT> getListadoCI_DitById(Integer id) {
//		return getDao().getListadoCI_DitById(id);
//	}
	public List<CISECCION_04DIT_02> getListadoCI_Dit02ById(Integer id) {
		return getDao().getListadoCI_Dit02ById(id);
	}
	public List<CISECCION_04B_TARJETA> getListadoCI_04B_TarjetaById(Integer id) {
		return getDao().getListadoCI_04B_TarjetaById(id);
	}
	public List<CISECCION_04B> getListadoCI_04BById(Integer id) {
		return getDao().getListadoCI_04BById(id);
	}
	public List<CISECCION_04A> getListadoCI_04AById(Integer id) {
		return getDao().getListadoCI_04AById(id);
	}
	public List<CISECCION_02T> getListadoCI_02TById(Integer id) {
		return getDao().getListadoCI_02TById(id);
	}
	public List<CISECCION_02> getListadoCI_02ById(Integer id) {
		return getDao().getListadoCI_02ById(id);
	}
	public List<CISECCION_01_03> getListadoCI_01_03ById(Integer id) {
		return getDao().getListadoCI_01_03ById(id);
	}
	
	public List<Individual> getListadoIndividual(Integer id,Integer hogar_id,  SeccionCapitulo... secciones)
	{
		return getDao().getListadoIndividual(id, hogar_id, secciones);
	}
	
	public Individual getListadoIndividualPorPersona(Integer id,Integer hogar_id, Integer persona_id, SeccionCapitulo... secciones)
	{
		return getDao().getListadoIndividualPorPersona(id, hogar_id, persona_id, secciones);
	}
	
	public List<CSSECCION_08> getNiniosaModificaralBorrarPersona(Integer id,Integer hogar_id, SeccionCapitulo... secciones){
		return getDao().getNiniosaModificaralBorrarPersona(id, hogar_id, secciones);
	}
	public List<CSVISITA> getVisitasAModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getVisitasAModificarAlBorrarPersona(id, hogar_id,persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenVisitaSalud(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior){
		return getDao().ModificarNumerodeOrdenVisitaSalud(id,hogar_id,persona_id_nuevo,persona_id_anterior);
	}
	public boolean ModificarNumerodeOrdenVisitaSalud2(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer numero_visita){
		return getDao().ModificarNumerodeOrdenVisitaSalud2(id,hogar_id,persona_id_nuevo,numero_visita);
	}
	public Salud getSaludAModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getSaludAModificarAlBorrarPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenSalud01_03(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior){
		return getDao().ModificarNumerodeOrdenSalud01_03(id, hogar_id, persona_id_nuevo, persona_id_anterior);
	}
	public CAP04_07 getSalud04_07AModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getSalud04_07AModificarAlBorrarPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenSalud04_07(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior){
		return getDao().ModificarNumerodeOrdenSalud04_07(id, hogar_id, persona_id_nuevo, persona_id_anterior);
	}
	public boolean BorrarSalud1(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().BorrarSalud1(id, hogar_id, persona_id);
	}
	public boolean BorrarSalud2(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().BorrarSalud2(id, hogar_id, persona_id);
	}
	public boolean BorrarVisitaSalud(Integer id,Integer hogar_id,Integer numero_visita){
		return getDao().BorrarSalud1(id, hogar_id, numero_visita);
	}
	public List<CSSECCION_08> getInformanteSaludModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getDao().getInformanteSaludModificarAlBorrarPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenInformanteSaludSeccion08(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer persona_id_ninio){
		return getDao().ModificarNumerodeOrdenInformanteSaludSeccion08(id, hogar_id, persona_id_nuevo, persona_id_anterior, persona_id_ninio);
	}
	public List<CSSECCION_08> getSaludSeccion08AModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer pesona_id, SeccionCapitulo...secciones){
		return getDao().getSaludSeccion08AModificarAlBorrarPersona(id, hogar_id, pesona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenSaludSeccion08(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior,Integer informante){
		return getDao().ModificarNumerodeOrdenSaludSeccion08(id, hogar_id, persona_id_nuevo, persona_id_anterior,informante);
	}
	public List<Individual> getCaratulaIndividualModificarAlBorrarPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCaratulaIndividualModificarAlBorrarPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenCaratulaIndividual(Integer id,Integer hogar_id,Integer persona_id_nuevo,Integer persona_id_anterior){
		return getDao().ModificarNumerodeOrdenCaratulaIndividual(id, hogar_id, persona_id_nuevo, persona_id_anterior);
	}
	public List<ReporteHMS> getReporteHMS(String codccpp,Context ctx) {
		return getDao().getReporteHMS(codccpp,ctx);
	}
	public List<ReportDatesofBirth> getListadodeFechasporConglomerado(String conglomerado){
		return getDao().getListadodeFechasporConglomerado(conglomerado);
	}
	public Salud getSaludNavegacionS1_3(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		return getDao().getSaludNavegacionS1_3(id, hogar_id, persona_id, secciones);		
	}
	public CAP04_07 getSaludNavegacionS4_7(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) { 
		return getDao().getSaludNavegacionS4_7(id, hogar_id, persona_id, secciones);		
	}
	public CSSECCION_08 getPersonaSeccion8Salud(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getPersonaSeccion8Salud(id, hogar_id, persona_id, secciones);	
	}
	public List<Seccion03> getPersonaSeccion03(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getPersonaSeccion03(id, hogar_id, persona_id, secciones);
	}
	public boolean BorrarPersona(Integer id,Integer hogar_id, Integer persona_id){
		return getDao().BorrarPersona(id, hogar_id, persona_id);
	}
	public boolean getResultadodelaUltimaVisitaHogar(Integer id,Integer hogar_id){
	return getDao().getResultadodelaUltimaVisitaHogar(id, hogar_id);	
	}
	public boolean getExisteResultadodelasVisitas(Integer id,Integer hogar_id){
		return getDao().getExisteResultadodelasVisitas(id, hogar_id);
	}
	public boolean getHogarCompletado(Integer id,Integer hogar_id){
		return getDao().getHogarCompletado(id, hogar_id);
	}
	public boolean getSeccion04_05Completado(Integer id,Integer hogar_id){
		return getDao().getSeccion04_05Completado(id,hogar_id);
	}
	public boolean getVisitaSaludCompletada(Integer id,Integer hogar_id){
		return getDao().getVisitaSaludCompletada(id,hogar_id);
	}
	public boolean getExisteCuestionarioDeSalud(Integer id,Integer hogar_id){
		return getDao().getExisteCuestionarioDeSalud(id,hogar_id);
	}
	public boolean getSeccion08Completado(Integer id,Integer hogar_id){
		return getDao().getSeccion08Completado(id,hogar_id);
	}
	public boolean getExisteCaratulaDelaVivienda(Integer id){
		return getDao().getExisteCaratulaDelaVivienda(id);
	}
	public boolean getViviendaTieneGPS(Integer id){
		return getDao().getViviendaTieneGPS(id);
	}
	public List<Seccion01> getSeccion01ListbyMarcoyHogarMenorEdad(Integer id,Integer hogar_id,Integer persona_id,Integer qi219,Integer qh06,SeccionCapitulo... secciones) {
		return getDao().getSeccion01ListbyMarcoyHogarMenorEdad(id, hogar_id,persona_id,qi219,qh06,secciones);
	}
	
	public HashMap<String,Integer> getValoresNacimmientos(Integer id,Integer hogar_id,Integer persona_id,Integer qi212) {
		return getDao().getValoresNacimmientos(id, hogar_id, persona_id, qi212);
	}
	public HashMap<String,Integer> getValoresNacimmientos(Integer id,Integer hogar_id,Integer persona_id) {
		return getDao().getValoresNacimmientos(id, hogar_id, persona_id);
	}
	public List<CISECCION_02> getFechasdegemelos(Integer id, Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getFechasdegemelos(id, hogar_id, persona_id, secciones);
	}
	public CISECCION_08 getCISECCION_08(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_08(id,hogar_id,persona_id,secciones);
	}
	public List<CISECCION_02> getNacimientosListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getNacimientosListbyPersona(id, hogar_id, persona_id, secciones);
	}
	
	public List<CISECCION_02> getListaNacimientosCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getListaNacimientosCompletobyPersona(id, hogar_id, persona_id, secciones);
	}
	public List<CISECCION_02> getNacimientosListbyPersonaMayor1Anio(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getNacimientosListbyPersonaMayor1Anio(id, hogar_id, persona_id, secciones);
	}
	public boolean getNacimientosMayoresaunAnio(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getNacimientosMayoresaunAnio(id, hogar_id, persona_id);
	}
	public List<CISECCION_05> getParejasListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getParejasListbyPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean getCompletadoSeccion05(Integer id,Integer hogar_id,Integer persona_id) {
		return getDao().getCompletadoSeccion05(id, hogar_id, persona_id);
	}
	public List<CISECCION_02> getNacimientosVivoListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getNacimientosVivoListbyPersona(id, hogar_id, persona_id, secciones);
	}
	
	
	public List<CISECCION_02> getNacimientosMayor2011ListbyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getNacimientosMayor2011ListbyPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean ExisteninioMenoraTresAniosyVivo(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().ExisteninioMenoraTresAniosyVivo(id, hogar_id, persona_id);
	}
	public boolean VerificarFiltro481(Integer id,Integer hogar_id,Integer persona_id, Integer anio){
		return getDao().VerificarFiltro481(id, hogar_id, persona_id, anio);
	}
	public boolean 	VerificarFiltro720(Integer id,Integer hogar_id,Integer persona_id, Integer edad){
		return getDao().VerificarFiltro720(id,hogar_id,persona_id,edad);
	}
	public CARATULA_INDIVIDUAL getCAPIVISITA(Integer id, Integer hogar_id, Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getCAPIVISITA(id, hogar_id, persona_id,secciones);
	}
	public Integer getnumOrdenMaxVisitaCI(Integer id, Integer hogar_id, Integer persona_id) {
		return getDao().getnumOrdenMaxVisitaCI(id, hogar_id, persona_id);
	}
//	public CARATULA_INDIVIDUAL getCAPIVISITAPORVISTA(Integer id, Integer hogar_id, Integer persona_id, SeccionCapitulo... secciones) {
//		return getDao().getCAPIVISITAPORVISTA(id, hogar_id, persona_id, secciones);
//	}
	public List<CARATULA_INDIVIDUAL> getCAPVISITAsI(Integer id, Integer hogar_id, Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getCAPVISITAsI(id, hogar_id, persona_id, secciones);
	}
	public CARATULA_INDIVIDUAL getVisitaI(Integer id, Integer hogar_id, Integer persona_id, Integer nro_visita,
			SeccionCapitulo... secciones) {
		return getDao().getVisitaI(id, hogar_id, persona_id, nro_visita, secciones);
	}
	public boolean BorrarNacimiento(CISECCION_02 bean) {
		return getDao().borrarNacimiento(bean,null);
	}
	
	public boolean borrarEnlaceNacimiento(CISECCION_02 bean){
		return getDao().borrarEnlaceNacimiento(bean,null);
	}
	public boolean borrarTablaDiscapacidad(CISECCION_02 bean){
		return getDao().borrarTablaDiscapacidad(bean);
	}
	
	public List<CISECCION_02> getNacimientosListbyPersonaDIT(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getNacimientosListbyPersonaDIT(id, hogar_id, persona_id, secciones);
	}
	public boolean  saveOrUpdate(CISECCION_04DIT individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	
	public List<CISECCION_02> getListaNacimientos4abyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) { 
		return getDao().getListaNacimientos4abyPersona(id, hogar_id, persona_id, secciones);
	}
	
	public List<CISECCION_02> getListaNacimientos4aCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getListaNacimientos4aCompletobyPersona(id, hogar_id, persona_id, secciones);
	}
	
	public List<CISECCION_02> getListaNacimientos4bCompletobyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getListaNacimientos4bCompletobyPersona(id, hogar_id, persona_id, secciones);
	}
	
	public List<CISECCION_02> getListaNacimientos4bTarjetabyPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getListaNacimientos4bTarjetabyPersona(id, hogar_id, persona_id, secciones);
	}
	
	public boolean getSeccion2Completado(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getSeccion2Completado(id, hogar_id, persona_id);
	}
	
	public void reordenarSeccion2(CISECCION_02 bean,Integer indice_intercambio) {
		getDao().reordenarSeccion2(bean, indice_intercambio);
	}
	
	public void reordenarSeccion2Gemelos(CISECCION_02 bean,Integer indice_intercambio,Integer indice_menor,Integer indice_mayor) {
		getDao().reordenarSeccion2Gemelos(bean, indice_intercambio,indice_menor,indice_mayor);
	}
	
	public boolean getSeccion4ACompletado(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getSeccion4ACompletado(id, hogar_id, persona_id);
	}
	public boolean getSeccion4BCompletado(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getSeccion4BCompletado(id, hogar_id, persona_id);
	}
	public List<CISECCION_02T> getListarTerminaciones(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getListarTerminaciones(id, hogar_id, persona_id, secciones);
	}
	public CISECCION_02 getUltimoNacimiento(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getUltimoNacimiento(id, hogar_id, persona_id,secciones);
	}
	public CISECCION_02 getUltimoNacimientoidMayor(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getUltimoNacimientoidMayor(id, hogar_id, persona_id,secciones);
	}
	public CISECCION_02 getUltimoNacimientoidMayorVivo(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getUltimoNacimientoidMayorVivo(id, hogar_id, persona_id,secciones);
	}
	public CISECCION_01_03 getTerminaciones(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getTerminaciones(id, hogar_id, persona_id, secciones);
	}
	public List<CISECCION_02> getNacimientosparacalendario(Integer id,Integer hogar_id,Integer persona_id,Integer anio, SeccionCapitulo... secciones){
		return getDao().getNacimientosparacalendario(id, hogar_id, persona_id, anio, secciones);
	}
	public List<CICALENDARIO_TRAMO_COL01_03> getTramosdelcalendario(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...capitulos){
		return getDao().getTramosdelcalendario(id, hogar_id, persona_id, capitulos);
	}
	public Integer getUltimotramoidRegistrado(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getUltimotramoidRegistrado(id, hogar_id, persona_id);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaNacimientos(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaNacimientos(id, hogar_id, persona_id, ninio_id,  secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoporFechadeInicio(Integer id,Integer hogar_id,Integer persona_id, Integer mes,Integer anio,SeccionCapitulo...secciones){
		return getDao().getTramoporFechadeInicio(id, hogar_id, persona_id, mes, anio, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaTerminaciones(Integer id,Integer hogar_id,Integer persona_id,Integer terminacion_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaTerminaciones(id,hogar_id,persona_id,terminacion_id,secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaporTramoId(Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaporTramoId(id,hogar_id,persona_id,tramo_id,secciones);
	}
	public List<CICALENDARIO_TRAMO_COL01_03> getTramosAfectados(CICALENDARIO_TRAMO_COL01_03 tramo,SeccionCapitulo...secciones ){
		return getDao().getTramosAfectados(tramo, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaEmbarazo(Integer id,Integer hogar_id,Integer persona_id,Integer embarazo_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaEmbarazo(id, hogar_id, persona_id, embarazo_id, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaMetodoActual(Integer id,Integer hogar_id,Integer persona_id,Integer metodo_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaMetodoActual(id, hogar_id, persona_id, metodo_id, secciones);
	}
	public CICALENDARIO_TRAMO_COL01_03 getTramoparaNingunMetodo(Integer id,Integer hogar_id,Integer persona_id,Integer ninguno_id,SeccionCapitulo...secciones ){
		return getDao().getTramoparaNingunMetodo(id, hogar_id, persona_id, ninguno_id, secciones);
	}
	public Integer saveOrUpdate(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones)throws SQLException {
		return getDao().saveOrUpdate(bean, null,secciones);
	}
	public boolean saveOrUpdateTRAMO(CICALENDARIO_TRAMO_COL01_03 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdateTRAMO(bean, null, secciones);
	}
	public boolean saveOrUpdate(CICALENDARIO_COL01_03 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean CalendarioCompletado(Integer id,Integer hogar_id,Integer persona_id,Calendar fecha_actual){
		return getDao().CalendarioCompletado(id, hogar_id, persona_id,fecha_actual);
	}
	public Integer CalendarioCompletadoPorcantidad(Integer id,Integer hogar_id,Integer persona_id,Calendar fecha_actual){
		return getDao().CalendarioCompletadoPorcantidad(id, hogar_id, persona_id, fecha_actual);
	}
	public List<CICALENDARIO_COL01_03> getAllCalendario(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getDao().getAllCalendario(id, hogar_id, persona_id, secciones);
	}
	public List<CISECCION_02T> getTerminacionesParaCalendario(Integer id,Integer hogar_id,Integer persona_id,Integer anio, SeccionCapitulo...capitulos){
		return getDao().getTerminacionesParaCalendario(id, hogar_id, persona_id, anio, capitulos);
	}
	public boolean Deletetramo(String Tablename,Integer id,Integer hogar_id,Integer persona_id,Integer tramo_id){
		return getDao().Deletetramo(Tablename,id, hogar_id, persona_id, tramo_id);
	}
	public void reordenarIntermedioNumerodeOrdenNacimiento(CISECCION_02 bean)
	{	
		getDao().ReordenarIntermedioNumerodeOrdenNacimiento(bean);
	}
	public CICALENDARIO_TRAMO_COL4 getTramoCasoUnion(Integer id,Integer hogar_id, Integer persona_id,Integer conviviente_id,SeccionCapitulo...secciones){
		return getDao().getTramoCasoUnion(id, hogar_id, persona_id, conviviente_id, secciones);
	}
	public boolean  saveOrUpdate(CISECCION_02T individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public boolean saveOrUpdate(CISECCION_02 bean, SQLiteDatabase dbTX,
			SeccionCapitulo... campos) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, campos);
	}
	public CISECCION_02 getSeccion01_03Nacimiento(Integer id, Integer hogar_id,
			Integer persona_id,Integer qi212, SeccionCapitulo... secciones) {
		return getDao().getSeccion01_03Nacimiento(id, hogar_id, persona_id, qi212, secciones);
	}
	
	public boolean getExisteGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi212,Integer qi215y,String qi215m,String qi215d) {
		return getDao().getExisteGemelos(id, hogar_id, persona_id,qi212, qi215y, qi215m,qi215d); 
	}
	
	public HashMap<String,Integer> getExtremosGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi215y,String qi215m,String qi215d) {
		return getDao().getExtremosGemelos(id, hogar_id, persona_id, qi215y, qi215m,qi215d);
	}
	public boolean actualizarFechasGemelos(Integer id, Integer hogar_id,Integer persona_id,Integer qi215y,String qi215m,String qi215d,Integer qi215ynuevo,String qi215mnuevo,String qi215dnuevo,Integer qi217nuevo,Integer qi220a) {
		return getDao().actualizarFechasGemelos(id, hogar_id, persona_id, qi215y, qi215m,qi215d, qi215ynuevo, qi215mnuevo,qi215dnuevo,qi217nuevo,qi220a);
	}
	
	public CISECCION_02T getCISECCION_02T(Integer id,Integer hogar_id,Integer persona_id,Integer terminacion_id, SeccionCapitulo... secciones){
		return getDao().getCISECCION_02T(id,hogar_id,persona_id,terminacion_id,secciones);
	}
	public CICALENDARIO_COL01_03 getCAlendarioByIndice(Integer id,Integer hogar_id,Integer persona_id,Integer indice,SeccionCapitulo...secciones){
		return getDao().getCAlendarioByIndice(id, hogar_id, persona_id, indice, secciones);
	}
	public String getColumna1delCalendario(Integer id, Integer hogar_id,Integer persona_id,Integer tramo_id,String col1){
		return getDao().getColumna1delCalendario(id, hogar_id, persona_id, tramo_id, col1);
	}
	public CICALENDARIO_COL01_03 getRegistro(Integer id,Integer hogar_id,Integer persona_id,Integer indice, SeccionCapitulo... secciones){
		return getDao().getRegistro(id, hogar_id,persona_id, indice, secciones);
	}
	public String getNombreDelNacido(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id, SeccionCapitulo... secciones){
		return getDao().getNombreDelNacido(id, hogar_id, persona_id, ninio_id, secciones);
	}
	public CISECCION_04A getCISECCION_04A(Integer id,Integer hogar_id,Integer persona_id,Integer qi212,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04A(id,hogar_id,persona_id,qi212,secciones);
	}
	public boolean esPrimerRegistroCap4a(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		return getDao().esPrimerRegistroCap4a(id, hogar_id, persona_id, ninio_id);
	}
	public String PrimerNombreCap4aVivo(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().PrimerNombreCap4aVivo(id, hogar_id, persona_id);
	}
	public CISECCION_04A primerRegistroCap4a(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().primerRegistroCap4a(id, hogar_id, persona_id);
	}
	public CISECCION_02 PrimerRegistroCap2(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().PrimerRegistroCap2(id, hogar_id, persona_id);
	}
	
	public CISECCION_02 primerNacimientoCap2(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().primerNacimientoCap2(id, hogar_id, persona_id);
	}	
	
	public CISECCION_02T primeraTerminacionCap1_3(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().primeraTerminacionCap1_3(id, hogar_id, persona_id);
	}
	
	public boolean  saveOrUpdate(CISECCION_04B individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public CISECCION_04B getCISECCION_04B(Integer id,Integer hogar_id,Integer persona_id,Integer qi212,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04B(id,hogar_id,persona_id,qi212,secciones);
	}
	public boolean borrar04b(Integer id,Integer hogar_id, Integer persona_id,Integer qi212,String pregunta){
		return getDao().borrar04b(id, hogar_id,  persona_id, qi212, pregunta);
	}
	
//	public boolean borrar04bDit(Integer id,Integer hogar_id, Integer persona_id,Integer qi212){
//		return getDao().borrar04bDit(id, hogar_id,  persona_id, qi212);
//	}
	
	public boolean saveOrUpdate(CISECCION_04B_TARJETA bean,SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean,secciones);
	}
	
	public CISECCION_04B_TARJETA getSeccion04BTarjeta(Integer id, Integer hogar_id,Integer persona_id,Integer qi212,
			String pregunta,Integer indice, SeccionCapitulo... secciones) {
		return getDao().getSeccion04BTarjeta(id,hogar_id,persona_id,qi212,pregunta,indice,secciones);
	}
	
	public List<CISECCION_04B_TARJETA> getSecciones04BTarjeta(Integer id, Integer hogar_id,
			Integer persona_id,Integer qi212,String pregunta, SeccionCapitulo... secciones) {
		return getDao().getSecciones04BTarjeta(id,hogar_id,persona_id,qi212,pregunta,secciones);
	}
	public CISECCION_04B_TARJETA getMayorRegistro(Integer id, Integer hogar_id,
			Integer persona_id,Integer qi212,String pregunta)
	{
		return getDao().getMayorRegistro(id,hogar_id,persona_id,qi212,pregunta);
	}
	public List<CISECCION_04DIT> getListadoDit(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getListadoDit(id, hogar_id, persona_id, secciones);
	}
	/*
	public List<CISECCION_04DIT> getListadoCompleto4Dit(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getListadoCompleto4Dit(id, hogar_id, persona_id, secciones);
	}*/
	
	public boolean getSeccion4DITCompletado(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getSeccion4DITCompletado(id, hogar_id, persona_id);
	}
	public boolean saveOrUpdate(CISECCION_04B2 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public CISECCION_04B2 getCISECCION_04B2(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04B2(id,hogar_id,persona_id,secciones);
	}
	public boolean VerificarFiltro480(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().VerificarFiltro480(id, hogar_id, persona_id);
	}
	public String getNombreDelNacidoMenor(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getNombreDelNacidoMenor(id, hogar_id, persona_id, secciones);
	}
	public CISECCION_04DIT getCISECCION_04B_2(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_ninio,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04B_2(id,hogar_id,persona_id,nro_orden_ninio,secciones);
	}
	public CISECCION_05 getCISECCION_05(Integer id,Integer hogar_id,Integer persona_id,Integer pareja_id, SeccionCapitulo... secciones){
		return getDao().getCISECCION_05(id,hogar_id,persona_id,pareja_id,secciones);
	}
	public List<CICALENDARIO_COL01_03> getAllCalendarioDescendente(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getAllCalendarioDescendente(id,hogar_id,persona_id,secciones);
	}
	public boolean  saveOrUpdate(CISECCION_05 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public boolean  saveOrUpdate(CISECCION_08 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public List<CISECCION_09> getHermanosdeMef(Integer id,Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getHermanosdeMef(id, hogar_id, persona_id, secciones);
	}
	public List<CIHERMANOS> getHermanosdeMefdiferencia(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getHermanosdeMefdiferencia(id, hogar_id, persona_id);
	}
	public List<MORTALIDAD> getMortalidad(Integer id, Integer hogar_id, SeccionCapitulo...secciones){
		return getDao().getMortalidad(id,hogar_id,secciones);
	}
	public boolean TodoLosMiembrosMortalidadhogarCompletados(Integer id,Integer hogar_id){
		return getDao().TodoLosMiembrosMortalidadhogarCompletados(id, hogar_id);
	}
	public boolean getCompletadoTodoLosRegistrosHermanos(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getCompletadoTodoLosRegistrosHermanos(id, hogar_id, persona_id);
	}
	public boolean DeleteMortalidadMaterna(CISECCION_09 hermano){
		return getDao().DeleteMortalidadMaterna(hermano);
	}
	public boolean DeleteMortalidadHogarTodo(Integer id,Integer hogar_id, Integer persona_id){
		return getDao().DeleteMortalidadHogarTodo(id,hogar_id,persona_id);
	}
	public boolean DeleteMortalidadHogar(MORTALIDAD mortalidad){
		return getDao().DeleteMortalidadHogar(mortalidad);
	}
	public boolean saveOrUpdate(CISECCION_09 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean actualizarOrdenCISEc09(CISECCION_09 bean, Integer ordenSource, Integer ordenTarget)
			throws java.sql.SQLException {
		return getDao().actualizarOrdenCISEc09(bean, ordenSource, ordenTarget);
	}
	public boolean actualizarOrdenCHMortalidad(MORTALIDAD bean, Integer ordenSource, Integer ordenTarget)throws java.sql.SQLException {
		return getDao().actualizarOrdenCHMortalidad(bean, ordenSource, ordenTarget);
	}
	public boolean ModificacionNroOrdenMortalidad(MORTALIDAD hermano, Integer position){
		return getDao().ModificacionNroOrdenMortalidad(hermano, position);
	}
	public CISECCION_09 getMortalidadMaterna(Integer id,Integer hogar_id,Integer persona_id,Integer qinro_orden,SeccionCapitulo... secciones){
		return getDao().getMortalidadMaterna(id, hogar_id, persona_id, qinro_orden, secciones);
	}
	public MORTALIDAD getPersonafallecida(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_id, SeccionCapitulo...secciones){
		return getDao().getPersonafallecida(id,hogar_id,persona_id,nro_orden_id,secciones);
	}
	public CISECCION_10_01 getCISECCION_10_01(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_10_01(id,hogar_id,persona_id,secciones);
	}
	public boolean BorrarNacimiento(Integer id,Integer hogar_id, Integer persona_id, Integer qi212){
		return getDao().BorrarNacimiento(id, hogar_id, persona_id,qi212);
	}
	public boolean borrarNacimientos(Integer id,Integer hogar_id, Integer persona_id){
		return getDao().borrarNacimientos(id, hogar_id, persona_id,null);
	}
	
	public boolean BorrarPareja(Integer id,Integer hogar_id, Integer persona_id, Integer pareja_id){
		return getDao().BorrarPareja(id, hogar_id, persona_id,pareja_id);
	}
	public boolean BorrarTodasLasparejas(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().BorrarTodasLasparejas(id, hogar_id, persona_id);
	}
	public CISECCION_10_02 getCISECCION_10_02(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_10_02(id,hogar_id,persona_id,ninio_id,secciones);
	}
	public boolean getFiltro1038byPersona(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getFiltro1038byPersona(id, hogar_id, persona_id);
	}
	public boolean  saveOrUpdate(CISECCION_10_03 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public CISECCION_10_03 getCISECCION_10_03(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer vacuna_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_10_03(id,hogar_id,persona_id,ninio_id,vacuna_id,secciones);
	}
	public List<CISECCION_10_03> getCISECCION_10_03All(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_10_03All(id, hogar_id, persona_id, ninio_id, secciones);
	}
	public boolean BorrarControl(Integer id,Integer hogar_id, Integer persona_id, Integer ninio_id, Integer qi_ctrl){
		return getDao().BorrarControl(id, hogar_id, persona_id,ninio_id,qi_ctrl);
	}
	public CISECCION_10_04 getCISECCION_10_04(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer qi_ctrl,SeccionCapitulo... secciones){
		return getDao().getCISECCION_10_04(id,hogar_id,persona_id,ninio_id,qi_ctrl,secciones);
	}
	public CISECCION_10_04 getCICONTROL_BYFECHA(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer dia,Integer mes,Integer anio,SeccionCapitulo... secciones){
		return getDao().getCICONTROL_BYFECHA(id, hogar_id, persona_id, ninio_id, dia, mes, anio, secciones);
	}
	public CISECCION_10_04 getCICONTROL_BYFECHASOLOHASTAMES(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,Integer mes,Integer anio,SeccionCapitulo... secciones){
		return getDao().getCICONTROL_BYFECHASOLOHASTAMES(id, hogar_id, persona_id, ninio_id, mes, anio, secciones);
	}
	public boolean  saveOrUpdate(CARATULA_INDIVIDUAL individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public List<CISECCION_10_04> getCISECCION_10_04(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id,SeccionCapitulo... secciones) {
		return getDao().getCISECCION_10_04(id, hogar_id, persona_id, ninio_id, secciones);
	}
	public boolean  saveOrUpdate(CISECCION_10_04 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public List<CICALENDARIO_TRAMO_COL4> getTramoscol04ByPersona(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getTramoscol04ByPersona(id, hogar_id, persona_id, secciones);
	}
	public boolean getCompletadoTramosCol4(Integer id,Integer hogar_id,Integer persona_id,Calendar fecha_actual){
		return getDao().getCompletadoTramosCol4(id, hogar_id, persona_id,fecha_actual);
	}
	public Integer saveOrUpdate(CICALENDARIO_TRAMO_COL4 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones)throws SQLException {
		return getDao().saveOrUpdate(bean, null,secciones);
	}
	public List<CICALENDARIO_COL04> getAllCalendariocolumna04(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo...secciones){
		return getDao().getAllCalendariocolumna04(id, hogar_id, persona_id, secciones);
	}
	public boolean saveOrUpdate(CICALENDARIO_COL04 bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public CICALENDARIO_TRAMO_COL4 getTramoByTramoId(Integer id,Integer hogar_id, Integer persona_id,Integer tramo_id,SeccionCapitulo...secciones){
		return getDao().getTramoByTramoId(id, hogar_id, persona_id, tramo_id, secciones);
	}
	public boolean getAlgunNacidoVivo(Integer id,Integer hogar_id, Integer persona_id){
		return getDao().getAlgunNacidoVivo(id, hogar_id, persona_id);
	}
	public boolean getAlgunaHijaoHijoQueViveEnelHogar(Integer id, Integer hogar_id,Integer persona_id){
		return getDao().getAlgunaHijaoHijoQueViveEnelHogar(id, hogar_id, persona_id);
	}
	public boolean getCompletadoSeccion0103CI(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getCompletadoSeccion0103CI(id, hogar_id, persona_id);
	}
	public boolean getCompletadoSeccion0507CI(Integer id,Integer hogar_id,Integer persona_id){
	return getDao().getCompletadoSeccion0507CI(id, hogar_id, persona_id);	
	}
	public boolean getCompletadoSeccion008CI(Integer id, Integer hogar_id,Integer persona_id){
		return getDao().getCompletadoSeccion008CI(id, hogar_id, persona_id);
	}
	public boolean getCompletadoSeccion10CI(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getCompletadoSeccion10CI(id, hogar_id, persona_id);
	}
	public boolean getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getVisitaIndividualdelaMefQueTambienhaceSaludestaCompleta(id, hogar_id, persona_id);
	}
	public Integer getNiniosMenoresACincoAniosporMef(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getNiniosMenoresACincoAniosporMef(id, hogar_id, persona_id);
	}
	public Integer getNiniosConCarnetbyPersonaId(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getNiniosConCarnetbyPersonaId(id, hogar_id, persona_id);
	}
	public Integer getNiniosConCarnetVistabyPersonaId(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getNiniosConCarnetVistabyPersonaId(id, hogar_id, persona_id);
	}
	public boolean BorrarTerminaciones(Integer id,Integer hogar_id,Integer persona_id, Integer terminacion_id){
		return getDao().BorrarTerminaciones(id,hogar_id,persona_id,terminacion_id);
	}
	public CISECCION_04A getCISECCION_04A411H(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04A411h(id,hogar_id,persona_id,secciones);
	}
	public boolean getvalidarSiesqueLeHicieronExamenDeVIH(Integer id, Integer hogar_id, Integer persona_id){
		return getDao().getvalidarSiesqueLeHicieronExamenDeVIH(id, hogar_id, persona_id);
	}
	public CSSECCION_08 getFecha_NacimientoNinioSeccion08(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return getDao().getFecha_NacimientoNinioSeccion08(id,hogar_id,ninio,capitulos);
	}
	public Seccion04_05 getFecha_NacimientoNinioSeccion04_05(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return getDao().getFecha_NacimientoNinioSeccion04_05(id,hogar_id,ninio, capitulos);
	}
	public CISECCION_02 getFecha_NacimientoNinioSeccionCIS2(Integer id,Integer hogar_id,Integer ninio, SeccionCapitulo...capitulos){
		return getDao().getFecha_NacimientoNinioSeccionCIS2(id, hogar_id, ninio, capitulos);
	}
	public List<CISECCION_04B> getNiniosVivosYquetienenLatarjetadevacunacion(Integer id,Integer hogar_id){
		return getDao().getNiniosVivosYquetienenLatarjetadevacunacion(id,hogar_id);
	}
	public boolean getTieneAlMenosUnControl(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		return getDao().getTieneAlMenosUnControl(id, hogar_id, persona_id, ninio_id);
	}
	public boolean getTieneAlMenosUnaVacuna(Integer id,Integer hogar_id,Integer persona_id,Integer ninio_id){
		return getDao().getTieneAlMenosUnaVacuna(id, hogar_id, persona_id, ninio_id);
	}
	public List<MORTALIDAD> getAllMortalidad(Integer id){
		return getDao().getAllMortalidad(id);
	}
	public boolean getExisteAlgunHogarquefalteResponderPregunta224(Integer id,Integer hogar_id){
		return getDao().getExisteAlgunHogarquefalteResponderPregunta224(id,hogar_id);
	}
	public Endes_Resultado_vivienda getResultadovivienda_general(Integer id,SeccionCapitulo...secciones){
		return getDao().getResultadovivienda_general(id, secciones);
	}
	public Visita_Viv getUltima_visitaVivienda(Integer id) {
		return getDao().getUltima_visitaVivienda(id);
	}
	public boolean saveOrUpdate_VisitaVivienda(Visita_Viv bean, SeccionCapitulo... secciones) throws SQLException{
		return getDao().saveOrUpdate_VisitaVivienda(bean,secciones);
	}
	public Visita getResultadosVisitaHogar(Integer id) {
		return getDao().getResultadosVisitaHogar(id);
	}
//	public String getUltimaFechaDelCuestionario(Integer id) {
//		return getDao().getUltimaFechaDelCuestionario(id);
//	}
	public boolean EliminarUltimaVisitaVivienda(Integer id,Integer nro_visita){
		return getDao().EliminarUltimaVisitaVivienda(id, nro_visita);
	}
	public DISCAPACIDAD getDiscapacidad_byCuestionario_id(Integer id,Integer hogar_id,Integer cuestionario_id,Integer persona_id,SeccionCapitulo... secciones) {
		return getDao().getDiscapacidad_byCuestionario_id(id, hogar_id,cuestionario_id, persona_id, secciones);
	}
	public boolean BorrarPersonadeDiscapacidadporcuestionario(Integer id,Integer hogar_id, Integer persona_id,Integer cuestionario_id){
		return getDao().BorrarPersonadeDiscapacidadporcuestionario(id, hogar_id, persona_id, cuestionario_id);
	}
	public boolean saveOrUpdate_Discapacidad(DISCAPACIDAD bean, SeccionCapitulo... secciones)throws SQLException{
		return getDao().saveOrUpdate_Discapacidad(bean, secciones);
	}
	public DISCAPACIDAD getSeccion02IndividualParaDiscapacidad(Integer id, Integer hogar_id,Integer cuestionario_id, Integer persona_id,Integer ninio_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion02IndividualParaDiscapacidad(id,hogar_id,cuestionario_id,persona_id,ninio_id,secciones);
	}
	public List<DISCAPACIDAD> getListado_Discapacidad(Integer id) {
		return getDao().getListado_Discapacidad(id);
	}
	public boolean saveOrUpdate(DISCAPACIDAD bean, SQLiteDatabase dbwTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean,dbwTX);
	}

	public CISECCION_04DIT_02 getCISECCION_04B_dit2(Integer id,Integer hogar_id,Integer persona_id,Integer nro_orden_ninio,SeccionCapitulo... secciones){
		return getDao().getCISECCION_04B_dit2(id,hogar_id,persona_id,nro_orden_ninio,secciones);
	}
	public boolean  saveOrUpdate(CISECCION_04DIT_02 individual,SeccionCapitulo... secciones) throws java.sql.SQLException{
		return getDao().saveOrUpdate(individual,secciones);
	}
	public List<CISECCION_04DIT_02> getListadoDit_02(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getListadoDit_02(id, hogar_id, persona_id, secciones);
	}
	public boolean borrar04bDit_02(Integer id,Integer hogar_id, Integer persona_id,Integer qi212){
		return getDao().borrar04bDit_02(id, hogar_id,  persona_id, qi212);
	}

	public boolean BorrarDiscapacidad(Integer id,Integer hogar_id,Integer persona_id, Integer cuestionario_id,Integer ninio_id){
		return getDao().BorrarDiscapacidad(id, hogar_id, persona_id, cuestionario_id, ninio_id);
	}

	public List<Seccion01> getSeccion01ListbyMarcoyHogar_precenso(Integer id,Integer hogar_id,SeccionCapitulo... secciones) {
		return getDao().getSeccion01ListbyMarcoyHogar_precenso(id,hogar_id,secciones);
	}
	
	public boolean getCompletadoSeccionDiscapacidadCI(Integer id, Integer hogar_id,Integer cuestionario_id,Integer persona_id){
		return getDao().getCompletadoSeccionDiscapacidadCI(id, hogar_id, cuestionario_id, persona_id);
	}
	
	public boolean ExisteninioDe61Hasta71ParaDit(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().ExisteninioDe61Hasta71ParaDit(id, hogar_id, persona_id);
	}
	public boolean getPersonasMenoresa16Seccion1(Integer id,Integer hogar_id){
		return getDao().getPersonasMenoresa16Seccion1(id,hogar_id);
	}
	public boolean BorrarCuestionarioIndividual(Seccion01 persona) throws SQLException{
		return BorrarCuestionarioIndividualTransaccion(persona,null);
		
	}
	public boolean BorrarCuestionarioIndividualTransaccion(Seccion01 persona, SQLiteDatabase dbwTX) throws java.sql.SQLException{
		return getDao().BorrarCuestionarioIndividual(persona,dbwTX);
	}
	public boolean BorrarCuestionarioDeSalud(Seccion01 persona, SQLiteDatabase dbwTX) throws java.sql.SQLException{
		return getDao().BorrarCuestionarioDeSalud(persona,dbwTX);
	}
	public boolean ModificarRecuperacionDeLaMef(Seccion01 persona){
		return getDao().ModificarRecuperacionDeLaMef(persona);
	}
	public boolean ModificarRecuperacionDeCuestionarioSalud(Seccion01 persona){
		return getDao().ModificarRecuperacionDeCuestionarioSalud(persona);
	}
	public boolean ModificarNumeroDeVivienda(Integer id, String nselv){
		return getDao().ModificarNumeroDeVivienda(id,nselv);
	}	
	
	
	
	
	
	public boolean saveOrUpdate(MIGRACION bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	public boolean saveOrUpdate(IMIGRACION bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}
	
	public boolean saveOrUpdate(MIGRACION bean, SQLiteDatabase dbTX,
			SeccionCapitulo... campos) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, campos);
	}
	public boolean saveOrUpdate(IMIGRACION bean, SQLiteDatabase dbTX,
			SeccionCapitulo... campos) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, campos);
	}

	public boolean saveOrUpdate(MIGRACION bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	public boolean saveOrUpdate(IMIGRACION bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}
	
	public List<MIGRACION> getMigracion(Integer id, Integer hogar_id, SeccionCapitulo...secciones){
		return getDao().getMigracion(id,hogar_id,secciones);
	}
	public List<IMIGRACION> getImigracion(Integer id, Integer hogar_id, SeccionCapitulo...secciones){
		return getDao().getImigracion(id,hogar_id,secciones);
	}
	
	
	public List<MIGRACION> getAllMigracion(Integer id){
		return getDao().getAllMigracion(id);
	}
	public List<IMIGRACION> getAllImigracion(Integer id){
		return getDao().getAllImigracion(id);
	}

	
	public boolean DeleteMigracionHogar(MIGRACION mortalidad){
		return getDao().DeleteMigracionHogar(mortalidad);
	}
	public boolean DeleteImigracionHogar(IMIGRACION mortalidad){
		return getDao().DeleteImigracionHogar(mortalidad);
	}
	public boolean DeleteMigracionHogarTodo(Integer id,Integer hogar_id){
		return getDao().DeleteMigracionHogarTodo(id,hogar_id);
	}
	public boolean DeleteImigracionHogarTodo(Integer id,Integer hogar_id){
		return getDao().DeleteImigracionHogarTodo(id,hogar_id);
	}
	
	public MIGRACION getPersonaMigracion(Integer id,Integer hogar_id,Integer qh34a, SeccionCapitulo...secciones){
		return getDao().getPersonaMigracion(id,hogar_id,qh34a,secciones);
	}
	public IMIGRACION getPersonaImigracion(Integer id,Integer hogar_id,Integer qh35a, SeccionCapitulo...secciones){
		return getDao().getPersonaImigracion(id,hogar_id,qh35a,secciones);
	}
	
	public boolean TodoLosMiembrosMigracionhogarCompletados(Integer id,Integer hogar_id){
		return getDao().TodoLosMiembrosMigracionhogarCompletados(id, hogar_id);
	}
	public boolean TodoLosMiembrosImigracionhogarCompletados(Integer id,Integer hogar_id){
		return getDao().TodoLosMiembrosImigracionhogarCompletados(id, hogar_id);
	}
	
	
	
//	public boolean actualizarOrdenCHMigracion(MIGRACION bean, Integer ordenSource, Integer ordenTarget)throws java.sql.SQLException {
//		return getDao().actualizarOrdenCHMigracion(bean, ordenSource, ordenTarget);
//	}
//	public boolean actualizarOrdenCHImigracion(IMIGRACION bean, Integer ordenSource, Integer ordenTarget)throws java.sql.SQLException {
//		return getDao().actualizarOrdenCHImigracion(bean, ordenSource, ordenTarget);
//	}
//	public boolean ModificacionNroOrdenMigracion(MIGRACION hermano, Integer position){
//		return getDao().ModificacionNroOrdenMigracion(hermano, position);
//	}
//	public boolean ModificacionNroOrdenImigracion(IMIGRACION hermano, Integer position){
//		return getDao().ModificacionNroOrdenImigracion(hermano, position);
//	}


}