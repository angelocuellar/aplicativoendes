package gob.inei.endes2024.service;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.Seccion04_05DAO;
import gob.inei.endes2024.model.Auditoria;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;
import gob.inei.endes2024.model.Seccion04_05;

import java.sql.SQLException;
import java.util.List;

import android.Manifest.permission;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Seccion04_05Service extends Service{
	
	public static Seccion04_05Service INSTANCE = null;
	public Seccion04_05Service(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	public static Seccion04_05Service getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion04_05Service(ctx);
		}
		return INSTANCE;
	}
	
	private Seccion04_05DAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = Seccion04_05DAO.getInstance(this.dbh);
		}
		return (Seccion04_05DAO)sqliteDAO;
	}
	public boolean saveOrUpdate(Seccion04_05 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion04_05 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public boolean saveOrUpdate(Seccion04_05 bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}
	
	public Seccion04_05 getSeccion04_05(Integer id, Integer hogar_id,
			Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05(id, hogar_id, persona_id,persona_id_orden, secciones);
	}
	public Seccion04_05 getSeccion05_persona(Integer id,Integer hogar_id,Integer persona_id_orden, SeccionCapitulo...secciones){
		return getDao().getSeccion05_persona(id, hogar_id, persona_id_orden, secciones);
	}
	public boolean BorrarPersona_Seccion04_05(Seccion01 bean){
		return getDao().BorrarPersona_Seccion04_05(bean);
	}
	public Seccion04_05 getSeccion05Dialog(Integer id, Integer hogar_id,
			Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
		return getDao().getSeccion05Dialog(id, hogar_id, persona_id,persona_id_orden, secciones);
	}
    public Seccion04_05 getSeccion04_05Tabla(Integer id,Integer hogar_id,Integer persona_id,Integer persona_id_orden,SeccionCapitulo... secciones)
    {	return getDao().getSeccion04_05Tabla(id, hogar_id, persona_id, persona_id_orden, secciones);
	}
	public List<Seccion04_05> getSeccion04_05Lista(Integer id, Integer hogar_id,
			Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05Lista(id, hogar_id, persona_id,persona_id_orden,secciones);
	}
	public List<Seccion04_05> getSeccion04_05ListaVista(Integer id, Integer hogar_id,
			Integer persona_id,Integer persona_id_orden, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05ListaVista(id, hogar_id, persona_id,persona_id_orden,secciones);
	}
	public List<Seccion04_05> getSeccion04_05ListaVista(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getSeccion04_05ListaVista(id, hogar_id, persona_id,secciones);
	}
	public List<Seccion04_05> getSeccion04_05Lista(Integer id, Integer hogar_id,
			Integer persona_id) {
		return getDao().getSeccion04_05Lista(id, hogar_id, persona_id);
	}
	
	public List<Seccion04_05> getSeccion04_05ListarTabla(Integer id, Integer hogar_id, SeccionCapitulo... secciones) {
		return getDao().getListadoVistaSeccion4(id, hogar_id,secciones);
	}
	public boolean TodoLosMiembrosS4Completados(Integer id,Integer hogar_id,Integer persona_id)
	{
		return getDao().TodoLosMiembrosS4Completados(id, hogar_id,persona_id);
	}
	public boolean getEntraaSeccion04_05(Integer id,Integer hogar_id){
		return getDao().getEntraaSeccion04_05(id, hogar_id);
	}
	public List<Seccion04_05> getPersonasRelacionadasalborrar(Integer id,Integer hogar_id,SeccionCapitulo...secciones){
		return getDao().getPersonasRelacionadasalborrar(id, hogar_id, secciones);
	}
	public List<Seccion04_05> getPersonasAModificaralBorrar(Integer id,Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones){
		return getDao().getPersonasAModificaralBorrar(id, hogar_id,persona_id, secciones);
	}
	public boolean ModificarNumerodeOrdenSeccion04_05(Integer id,Integer hogar_id,Integer persona_id_nuevo, Integer persona_id_anterior){
		return getDao().ModificarNumerodeOrdenSeccion04_05(id, hogar_id, persona_id_nuevo, persona_id_anterior);
	}
	public List<Seccion04_05> getPersonas(Integer id){
		return getDao().getPersonas(id);
	}
	public boolean saveOrUpdate(Auditoria bean, SQLiteDatabase dbwTX)	throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean,dbwTX);
	}
	public List<Auditoria> getPersonasSeccion04_05_det(Integer id){
		return getDao().getPersonasSeccion04_05_det(id);
	}
	
}
