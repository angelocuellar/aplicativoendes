package gob.inei.endes2024.service;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.VisitaDAO;
import gob.inei.endes2024.model.CARATULA_INDIVIDUAL;
import gob.inei.endes2024.model.CSVISITA;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Incentivos;
import gob.inei.endes2024.model.Visita;
import gob.inei.endes2024.model.Visita_Viv;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class VisitaService extends Service {
	public static VisitaService INSTANCE = null;

	private VisitaService(Context ctx) {
		super(ctx);
	}

	public static VisitaService getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new VisitaService(ctx);
		}
		return INSTANCE;
	}
	
	private VisitaDAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = VisitaDAO.getInstance(this.dbh);
		}
		return (VisitaDAO)sqliteDAO;
	}

	public Visita getVisita(Integer id, Integer hogar,Integer c1_vis) {
		return getDao().getVisita(id, hogar,c1_vis);
	}

	public Visita getUltimaVisita(Integer id,Integer hogar, String... campos) {
		return getDao().getUltimaVisita(id, hogar,campos);
	}
	
	public CSVISITA getUltimaVisitaSalud(Integer id, Integer hogar_id,String... campos) {
	return getDao().getUltimaVisitaSalud(id,hogar_id,campos);
	}

	public Visita getMenorVisita(Integer id, Integer hogar_id, String... campos) {
		return getDao().getMenorVisita(id, hogar_id, campos);
	}

	public Visita getMayorVisita(Integer id, Integer hogar_id, String... campos) {
		return getDao().getMayorVisita(id, hogar_id, campos);
	}

	public List<Visita> getVisitas(Integer id,Integer hogar, String... campos) {
		return getDao().getVisitas(id,hogar, campos);
	}
	
	public List<Visita> getVisitas(Integer id) {
		return getDao().getVisitas(id);
	}
	
	public List<Visita_Viv> getVisitas_viv(Integer id) {
		return getDao().getVisitas_viv(id);
	}
	public List<Incentivos> getIncentivos(Integer id) {
		return getDao().getIncentivos(id);
	}

	public boolean saveOrUpdate(Visita bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}
	
	public boolean saveOrUpdate(Visita_Viv bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}
	public boolean saveOrUpdate(Hogar bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}
		
	public boolean saveOrUpdate(CARATULA_INDIVIDUAL bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}	

	public boolean saveOrUpdate(CSVISITA bean, String... campos) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, -1, -1, campos);
	}	 
	
	
	public boolean saveOrUpdate(Visita bean, SQLiteDatabase dbTX) throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}		
	
	public boolean eliminar(SQLiteDatabase dbTX, Integer id, Integer hogar,Integer nro_visita) {
		return getDao().eliminar(dbTX, id,hogar, nro_visita);
	}	 	
	
	public boolean eliminar(Integer id,Integer hogar, Integer c1_vis) {
		return this.eliminar(null, id,hogar, c1_vis);
	}
	public boolean eliminarSalud(SQLiteDatabase dbTX, Integer id, Integer hogar_id,
			Integer nro_visita) {
		return getDao().eliminarSalud(null,id,hogar_id,nro_visita);
	}

	public List<Visita> getUltimasVisitas(Integer id) {
		return getDao().getUltimasVisitas(id);
	}

	public Visita getPrimeraVisitaAceptada(Integer id, Integer hogar_id) {
		return getDao().getPrimeraVisitaAceptada(id, hogar_id);
	} 
	public Visita getMaximaVisitaAceptada(Integer id,Integer hogar_id){
		return getDao().getMaximaVisitaAceptada(id, hogar_id);
	}
	
	//ADD07122015
	//MODEL :  CAPVISITA 
	public CSVISITA getCAPVISITA(Integer id, Integer hogar_id,Integer persona_id, Integer nro_visita,
			SeccionCapitulo... secciones) {
		return getDao().getCAPVISITA(id, hogar_id, persona_id, 	secciones);
	}
	
	public List<CSVISITA> getCAPVISITAs(Integer id, Integer hogar_id,	Integer persona_id, 
			SeccionCapitulo... secciones) {
		return getDao().getCAPVISITAs(id, hogar_id, persona_id, secciones);
	}

  

	public boolean saveOrUpdate(CSVISITA bean, SeccionCapitulo... secciones)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, secciones);
	}

	public boolean saveOrUpdate(CSVISITA bean, SQLiteDatabase dbTX)
			throws java.sql.SQLException {
		return getDao().saveOrUpdate(bean, dbTX);
	}

	public List<CSVISITA> getCAPVISITAList(Integer id) {
		return getDao().getCAPVISITAList(id);
	}
	public Visita getVisitaCompletada(Integer id,Integer hogar_id,SeccionCapitulo... secciones)
	{
		return getDao().getVisitaCompletada(id,hogar_id,secciones); 
	}
	public CARATULA_INDIVIDUAL getVisitaIndividualCompletada(Integer id, Integer hogar_id,Integer persona_id, SeccionCapitulo...secciones){
		return getDao().getVisitaIndividualCompletada(id,hogar_id,persona_id,secciones);
	}
	public List<CSVISITA> getVisitassalud(Integer id) {
		return getDao().getVisitassalud(id);
	}
	public boolean EliminarVisitaIndividual(SQLiteDatabase dbTX, Integer id, Integer hogar_id, Integer persona_id,Integer nro_visita) {
		return getDao().EliminarVisitaIndividual(dbTX, id, hogar_id, persona_id, nro_visita);
	}
	
}