package gob.inei.endes2024.service;

import gob.inei.dnce.components.Entity.SeccionCapitulo;
import gob.inei.endes2024.dao.Seccion01DAO;
import gob.inei.endes2024.model.Hogar;
import gob.inei.endes2024.model.Seccion01;
import gob.inei.endes2024.model.Seccion03;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Seccion01Service extends Service {

	public static Seccion01Service INSTANCE = null;
	public Seccion01Service(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	public static Seccion01Service getInstance(Context ctx) {
		if (INSTANCE == null) {
			INSTANCE = new Seccion01Service(ctx);
		}
		return INSTANCE;
	}
	
	private Seccion01DAO getDao() {
		if (sqliteDAO == null) {
			sqliteDAO = Seccion01DAO.getInstance(this.dbh);
		}
		return (Seccion01DAO)sqliteDAO;
	}
	public boolean saveOrUpdate(Seccion01 bean, SeccionCapitulo... secciones) throws SQLException {			
		return this.saveOrUpdate(bean, null, secciones);
	}
	
	public boolean saveOrUpdate(Seccion01 bean, SQLiteDatabase dbTX, SeccionCapitulo... secciones) throws SQLException {
		return getDao().saveOrUpdate(bean, dbTX, secciones);
	}
	public List<Seccion01> getPersonas(Integer id) {
		return getDao().getPersonas(id);
	}
	public List<Seccion01> getPersonasbyId(Integer id) {
		return getDao().getPersonasbyId(id);
	}
	public List<Seccion01> getPersonasbyHogar(Seccion01 bean,SeccionCapitulo... secciones){
		return getDao().getPersonasbyHogar(bean,secciones);
	}
	public boolean UpdateInformante(Integer id, Integer hogar_id)
	{
		return getDao().UpdateInformante(id, hogar_id);
	}
	public Seccion01 getPersonanformanteSalud(Integer id, Integer hogar_id,Integer persona_id,SeccionCapitulo... secciones)
	{
		return getDao().getPersonanformanteSalud(id, hogar_id, persona_id, secciones);
	}
	public Seccion01 getPersona(Integer id, Integer hogar,Integer persona_id) {
		return getDao().getPersona(id, hogar, persona_id);
	}
	public Seccion01 getPersonaEdad(Integer id, Integer hogar,Integer persona_id) {
		return getDao().getPersonaEdad(id, hogar, persona_id);
	}
	public Seccion01 getPersonaEliminarSeccion3(Integer id, Integer hogar,Integer persona_id) {
		return getDao().getPersonaEliminarSeccion3(id, hogar, persona_id);
	}	
	public Seccion01 getPersonaInformante(Integer id,Integer hogar_id,Integer qhinfo)
	{
		return getDao().getPersonaInformante(id, hogar_id, qhinfo);
	}
	public boolean ModificarInformanteSH04(Seccion01 persona){
		return getDao().ModificarInformanteSH04(persona);
	}
	public Integer cantidaddeMef(Integer id, Integer hogar_id)
	{
		return getDao().CantidaddeMef(id, hogar_id);
	}
	public List<Seccion01> getMef(Integer id,Integer hogar_id)
	{
		return getDao().getMef(id, hogar_id);
	}
	public List<Seccion01> getNiniosPorRangodeEdad(Integer id,Integer hogar_id,Integer edadmin, Integer edadmax)
	{
		return getDao().getNiniosPorRangodeEdad(id, hogar_id, edadmin, edadmax);
	}
	public boolean getCantidadNiniosPorRangodeEdad(Integer id,Integer hogar_id,Integer edadmin, Integer edadmax)
	{
		return getDao().getCantidadNiniosPorRangodeEdad(id, hogar_id, edadmin, edadmax);
	}
	public boolean getCantidadMef(Integer id,Integer hogar_id,Integer edadmin, Integer edadmax)
	{
		return getDao().getCantidadMef(id, hogar_id, edadmin, edadmax);
	}
	public Integer TotalDePersonas(Integer id, Integer hogar_id)
	{
		return getDao().TotalDePersonas(id, hogar_id);
	}
	public Seccion01 getPersona(Integer id, Integer hogar,Integer persona_id, SeccionCapitulo... secciones) {
		return getDao().getPersona(id, hogar, persona_id, secciones);
	}
	
	public Seccion01 getPersona2(Integer id, Integer hogar,Integer persona_id) {
		return getDao().getPersona2(id, hogar, persona_id);
	}
	public boolean borrarPersona(Seccion01 bean) {
		return this.borrarPersona(bean, null);
	}
	public boolean borrarPersona(Seccion01 bean,SQLiteDatabase dbTX) {
		return getDao().BorrarPersona(bean,dbTX);
	}
	public boolean borrarPersonaCuandoCambiaSexo(Seccion01 bean){
		return getDao().borrarPersonaCuandoCambiaSexo(bean);
	}
	public boolean BorrarPersonaCSSeccion8(Seccion01 bean){
		return getDao().BorrarPersonaCSSeccion8(bean);
	}
	
	public List<Seccion01> getPersonasRango(Integer id, Integer hogar,SeccionCapitulo... secciones) {
		return getDao().getPersonasSeccion04(id,hogar,secciones);
	}
	
	public List<Seccion01> getSeccion1Beneficiados(Integer pregunta_id, Integer id,Integer hogar_id) {
		return getDao().getSeccion1Beneficiados(pregunta_id,id,hogar_id);
	}
	
	public Integer getSexobypersonaid(Integer id, Integer hogar_id, Integer persona_id)
	{
		return getDao().getSexobyPersonaId(id, hogar_id, persona_id);
	}
	public Integer getExisteEsposooEsposa(Integer id, Integer hogar_id){
		return getDao().getExisteEsposooEsposa(id, hogar_id);
	}
	public Integer getSexoDelaEsposaOEsposo(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getSexoDelaEsposaOEsposo(id, hogar_id, persona_id);
	}
	public boolean getExisteEsposo(Integer id,Integer hogar_id)
	{
		return getDao().getExisteEsposos(id, hogar_id);
	}
	public Integer getRelacionConJefedelHogar(Integer id,Integer hogar_id,Integer persona_id)
	{
		return getDao().getRelacionConJefe(id, hogar_id,persona_id);
	}
	public Integer getPersonaIddeEsposo(Integer id,Integer hogar_id)
	{
		return getDao().getPersonaIddeEsposo(id, hogar_id);
	}
	public List<Seccion01> getPersonaResponsable(Integer id, Integer hogar_id,Integer persona_id) {
		return getDao().getPersonaResponsable(id, hogar_id,persona_id);
	}
	public List<Seccion01> getMadres(Integer id, Integer hogar_id, Integer persona_id,Integer cod_hijoadoptivo, Integer edadPersona) {
		return getDao().getMadres(id, hogar_id, persona_id,cod_hijoadoptivo,edadPersona);
	}
	public boolean getHijoJefa(Integer id, Integer hogar_id, Integer sexo ) {
		return getDao().getHijoJefa(id, hogar_id,sexo);
	}	
	public List<Seccion01> getPadres(Integer id, Integer hogar_id,Integer persona_id,Integer cod_hijoadoptivo,Integer edadPersona)
	{
		return getDao().getPadres(id, hogar_id,persona_id,cod_hijoadoptivo,edadPersona);
	}
	public List<Seccion01> getBeca18(Integer id, Integer hogar_id,Integer persona_id_orden,Integer edadMinima,Integer edadMaxima,Integer pregunta_id)
	{
		return getDao().getBeca18(id, hogar_id,persona_id_orden,edadMinima,edadMaxima,pregunta_id);
	}
	public List<Seccion01> getCunamas(Integer id,Integer hogar_id,Integer persona_id_orden,Integer pregunta_id){
		return getDao().getCunamas(id, hogar_id,persona_id_orden,pregunta_id);
	}
	public List<Seccion01> getCunamasvalidacion(Integer id,Integer hogar_id,Integer pregunta_id){
		return getDao().getCunamasvalidacion( id, hogar_id, pregunta_id);
	}
	public List<Seccion01> getPosiblesBeneficiados(Integer id, Integer hogar_id,Integer persona_id_orden,Integer edadMinima,Integer edadMaxima,Integer pregunta_id)
	{
		return getDao().getPosiblesBeneficiados(id, hogar_id,persona_id_orden,edadMinima,edadMaxima,pregunta_id);
	}
	public List<Seccion01> getPosiblesInformantesCuestionarioSalud(Integer id,Integer hogar_id)
	{
		return getDao().getPosiblesInformantesCuestionarioSalud(id, hogar_id);
	}
	public List<Seccion01> getPosiblesInformantesCuestionarioSaludPordefecto(Integer id,Integer hogar_id){
		return getDao().getPosiblesInformantesCuestionarioSaludPordefecto(id, hogar_id);
	}
	public Seccion01 ListadoInformantedeSaludMenordeEdad(Integer id,Integer hogar_id){
		return getDao().ListadoInformantedeSaludMenordeEdad(id, hogar_id);
	}
	public boolean getExisteSoloEsposa(Integer id,Integer hogar_id)
	{
		return getDao().getExisteSoloEsposa(id, hogar_id);
	}
	public boolean getExisteSoloPadres(Integer id,Integer hogar_id)
	{
		return getDao().getExisteSoloPadres(id, hogar_id);
	}
	public Integer getEsParentescoId(Integer id, Integer hogar_id,Integer persona_id, Integer parentesco)
	{
		return getDao().getEsParentescoId(id, hogar_id, persona_id,parentesco);
	}
	public boolean getExisteSoloSuegros(Integer id,Integer hogar_id)
	{
		return getDao().getExisteSoloSuegros(id, hogar_id);
	}
	public boolean getExisteRelacionconJefe(Integer id,Integer hogar_id, Integer parentesco, Integer sexo)
	{
		return getDao().getExisteRelacionconJefe(id, hogar_id,parentesco, sexo);
	}
	public Integer getEdadByparentescoysexo(Integer id,Integer hogar_id,Integer parentesco, Integer sexo)
	{
		return getDao().getEdadByparentescoysexo(id, hogar_id, parentesco, sexo);
	}
	public Integer getEdadByPersonaId(Integer id,Integer hogar_id,Integer persona_id)
	{
		return getDao().getEdadByPersonaId(id, hogar_id, persona_id);
	}
	public Integer getEdadHijo_aMayor(Integer id,Integer hogar_id,Integer parentesco)
	{
		return getDao().getEdadHijo_aMayor(id, hogar_id,parentesco);
	}
	public Integer getEdadMinimaPadreMenor(Integer id,Integer hogar_id,Integer parentesco)
	{
		return getDao().getEdadMinimaPadreMenor(id, hogar_id, parentesco);
	}
	public boolean getCuentaRelacionconJefe(Integer id,Integer hogar_id, Integer parentesco)
	{
		return getDao().getCuentaRelacionconJefe(id, hogar_id,parentesco);
	}
	public Integer getPersonaIdByParentescoSexo(Integer id,Integer hogar_id, Integer parentesco, Integer sexo)
	{
		return getDao().getPersonaIdByParentescoSexo(id, hogar_id, parentesco,sexo);
	}
	public boolean getExisteMadre(Integer id,Integer hogar_id)
	{
		return getDao().getExisteMadre(id, hogar_id);
	}
	public boolean RegistroDePersonaCompletada(Integer id, Integer hogar_id, Integer persona_id)
	{
		return getDao().RegistroDePersonaCompletada(id,hogar_id,persona_id);
	}
	public boolean TodoLosMiembrosDelhogarCompletados(Integer id,Integer hogar_id)
	{
		return getDao().TodoLosMiembrosDelhogarCompletados(id, hogar_id);
	}
	public boolean TodasLasMefCompletadas(Integer id,Integer hogar_id){
		return getDao().TodasLasMefCompletadas(id, hogar_id);
	}
	public boolean TodosLosMiembrosDelHogarDiscapacidadCompletada(Integer id, Integer hogar_id)
	{
		return getDao().TodosLosMiembrosDelHogarDiscapacidadCompletada(id, hogar_id);
	}
	public List<Seccion01> getResponsableSalud(Integer id, Integer hogar_id)
	{
		return getDao().getResponsableSalud(id, hogar_id);
	}
	public Integer EdadMaximaDeunMiembrodelHogar(Integer id,Integer hogar_id)
	{
		return getDao().EdadMaximaDeunMiembrodelHogar(id, hogar_id);
	}
	public List<Seccion01> getEspososparalaMef(Integer id,Integer hogar_id){
		return getDao().getEspososparalaMef(id, hogar_id);
	}
	public Integer getTotalDehijosDelaMef(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getTotalDehijosDelaMef(id, hogar_id, persona_id);
	}
	public Integer getTotalDehijasDelaMef(Integer id,Integer hogar_id,Integer persona_id){
		return getDao().getTotalDehijasDelaMef(id, hogar_id, persona_id);
	}
	public boolean ModificarEnlaceInformanteDeSalud(Integer id,Integer hogar_id,Integer informantesalud_id){
		return getDao().ModificarEnlaceInformanteDeSalud(id,hogar_id,informantesalud_id);
	}

	public List<Seccion01> getresResponsablesPreCensos(Integer id, Integer hogar_id) {
		return getDao().getresResponsablesPreCensos(id, hogar_id);
	}
	public List<Seccion01> getMujerSeleccionable(Integer id, Integer hogar_id,Integer edadminima,Integer edadmaxima){
		return getDao().getMujerSeleccionable(id, hogar_id, edadminima, edadmaxima);
	}
	public List<Seccion01> getCovid(Integer id, Integer hogar_id,Integer persona_id_orden,Integer edadMinima,Integer edadMaxima,Integer pregunta_id){
		return getDao().getCovid(id, hogar_id,persona_id_orden,edadMinima,edadMaxima,pregunta_id);
	}
	public List<Seccion01> getInformateVacunas(Integer id, Integer hogar_id){
		return getDao().getInformateVacunas(id, hogar_id);
	}
	public List<Seccion01> getInformateMigracion(Integer id, Integer hogar_id){
		return getDao().getInformateMigracion(id, hogar_id);
	}
	public boolean TodoLosMiembrosDelhogarVacunasCompletados(Integer id,Integer hogar_id)
	{
		return getDao().TodoLosMiembrosDelhogarVacunasCompletados(id, hogar_id);
	}
}
